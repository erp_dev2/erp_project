﻿#region Update
/*
    09/09/2017 [TKG] Ubah warning.
    13/10/2017 [TKG] bug fixing saat save dtl+Payment type berdasarkan pos setting    
    17/10/2017 [TKG] bug fixing saat save
    09/05/2018 [TKG] tambah validasi apabila user tidak mengisi quantity item yg dikembalikan
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmPosSalesReturn : Form
    {
        private FrmTrnPOS mFrmParent;

        public FrmPosSalesReturn(FrmTrnPOS FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 7;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Transaction"+Environment.NewLine+"Date",

                        //1-5
                        "Date", 
                        "Receipt#", 
                        "Business"+Environment.NewLine+"Date", 
                        "Detail#", 
                        "Item's"+Environment.NewLine+"Code", 

                        //6-10
                        "Item's Name",
                        "Barcode",
                        "Purchased",
                        "Available",
                        "Return",

                        //11-15
                        "Price",
                        "Actual"+Environment.NewLine+"Price",
                        "Disc"+Environment.NewLine+"Amt",
                        "Tax 1"+Environment.NewLine+"Inclusive",
                        "Tax 1"+Environment.NewLine+"Amt",

                        //16 - 20
                        "Tax 2"+Environment.NewLine+"Inclusive",
                        "Tax 2"+Environment.NewLine+"Amt",
                        "Tax 3"+Environment.NewLine+"Inclusive",
                        "Tax 3"+Environment.NewLine+"Amt",
                        "Total Disc"+Environment.NewLine+"Per Item",

                        // 21-22
                        "Total Disc"+Environment.NewLine+"Amt",
                        "Total"+Environment.NewLine+"Per Item",
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 15, 17, 19, 20, 21, 22 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 7, 12 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0,1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22 });
            //Sm.SetGrdProperty(Grd1, true);
        }

        private void TxtReceiptNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.BtnRefresh_Click(sender, e);
            }

            if (e.KeyChar == 27)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        private bool IsFilterByDateInvalid()
        {
            if (Sm.CompareDtTm(Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (TxtReceiptNo.Text.Length==0)
            {
                MessageBox.Show("Receipt# is Empty !! ", "POS System");
                TxtReceiptNo.Focus();
                return;
            }

            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               IsFilterByDateInvalid()
               ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.TrnNo, A.BsDate, ");
                SQL.AppendLine(" Concat(substring(A.TrnDtTm, 1, 4), '/', ");
                SQL.AppendLine(" substring(A.TrnDtTm, 5, 2), '/', substring(A.TrnDtTm, 7, 2), ' ', ");
                SQL.AppendLine(" substring(A.TrnDtTm, 9, 2), ':', substring(A.TrnDtTm, 11, 2), ' No.', A.TrnNo, '  Total:', Convert(Format(A.Total, 2) Using utf8)) ");
                SQL.AppendLine(" As TrnGrp, ");                
                SQL.AppendLine(" Concat(substring(A.TrnDtTm, 7, 2), '/', ");
                SQL.AppendLine(" substring(A.TrnDtTm, 5, 2), '/', substring(A.TrnDtTm, 1, 4), ' ', ");
                SQL.AppendLine(" substring(A.TrnDtTm, 9, 2), ':', substring(A.TrnDtTm, 11, 2)) ");
                SQL.AppendLine(" As TrnDtTm, A.Total, 0 As DefaultQty, ");
                SQL.AppendLine(" A.Tax1Incl, A.Tax2Incl, A.Tax3Incl, ");
                SQL.AppendLine(" B.DtlNo, B.ItCode, C.ItName, B.Barcode, ");
                SQL.AppendLine(" B.Qty - ifnull((Select Sum(ifnull(Qty, 0)) From ");
                SQL.AppendLine(" tblpostrndtl R1 ");
                SQL.AppendLine(" Where R1.DtlType = 'R' And R1.RTrnNo=A.TrnNo And R1.RBsDate=A.BsDate And R1.RDtlNo=B.DtlNo), 0) As RQty, ");
                SQL.AppendLine(" B.Qty, B.UPrice, B.DiscAmt, A.TDiscAmt, B.ActPrice, ");
                SQL.AppendLine(" B.Tax1Amt, B.Tax2Amt, B.Tax3Amt, ");
                SQL.AppendLine(" Case A.TDiscAmt When 0 Then 0 ");
                SQL.AppendLine(" Else ((B.UPrice - B.DiscAmt) / (A.Total + A.TDiscAmt)) * A.TDiscAmt End As TDiscItem ");
                SQL.AppendLine(" from tblpostrnhdr A ");
                SQL.AppendLine(" Inner Join tblpostrndtl B on A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo And B.DtlType='S' ");
                SQL.AppendLine(" Inner Join tblitem C on B.ItCode=C.ItCode ");
                SQL.AppendLine(" Where (Left(A.TrnDtTm, 8) Between @DocDt1 And @DocDt2) ");
                SQL.AppendLine(" And A.TrnNo Like '%" + TxtReceiptNo.Text + "%' ");
                SQL.AppendLine(" And A.PosNo=@PosNo; ");


                Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        " Select A.TrnNo, A.BsDate, "+
                        " Concat(substring(A.TrnDtTm, 1, 4), '/', "+
                        " substring(A.TrnDtTm, 5, 2), '/', substring(A.TrnDtTm, 7, 2), ' ', "+
                        " substring(A.TrnDtTm, 9, 2), ':', substring(A.TrnDtTm, 11, 2), ' No.', A.TrnNo, '  Total:', Convert(Format(A.Total, 2) Using utf8)) " +
                        " As TrnGrp, "+                
                        " Concat(substring(A.TrnDtTm, 7, 2), '/', "+
                        " substring(A.TrnDtTm, 5, 2), '/', substring(A.TrnDtTm, 1, 4), ' ', "+
                        " substring(A.TrnDtTm, 9, 2), ':', substring(A.TrnDtTm, 11, 2)) "+
                        " As TrnDtTm, " + 
                        " A.Total, 0 As DefaultQty, " +
                        " A.Tax1Incl, A.Tax2Incl, A.Tax3Incl, " +
                        " B.DtlNo, B.ItCode, C.ItName, B.Barcode, " +
                        " B.Qty - ifnull((Select Sum(ifnull(Qty, 0)) From " +
                        " tblpostrndtl R1 "+
                        " Where R1.DtlType = 'R' And R1.RTrnNo=A.TrnNo And R1.RBsDate=A.BsDate And R1.RDtlNo=B.DtlNo), 0) As RQty, " +
                        " B.Qty, B.UPrice, B.DiscAmt, A.TDiscAmt, B.ActPrice, "+
                        " B.Tax1Amt, B.Tax2Amt, B.Tax3Amt, "+
                        " Case A.TDiscAmt When 0 Then 0 "+
                        " Else ((B.UPrice - B.DiscAmt) / (A.Total + A.TDiscAmt)) * A.TDiscAmt End As TDiscItem "+
                        " from tblpostrnhdr A "+
                        " Inner Join tblpostrndtl B on A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo And B.DtlType='S' " +
                        " Inner Join tblitem C on B.ItCode=C.ItCode "+
                        " Where (Left(A.TrnDtTm, 8) Between @DocDt1 And @DocDt2) " +
                        " And A.TrnNo Like '%"+TxtReceiptNo.Text+ "%' " +
                        " And A.PosNo=@PosNo;",
                        new string[]
                        {
                            //0
                            "TrnDtTm",
                                
                            //1-5
                            "TrnGrp", 
                            "TrnNo", 
                            "BsDate",
                            "DtlNo",
                            "ItCode",
 
                            //6-10
                            "ItName",                         
                            "BarCode",                         
                            "Qty",                         
                            "RQty",                         
                            "DefaultQty",                         

                            //11-
                            "UPrice",                         
                            "ActPrice",                         
                            "DiscAmt",   
                            "Tax1Incl",
                            "Tax1Amt",
                            
                            //16
                            "Tax2Incl",
                            "Tax2Amt",
                            "Tax3Incl",
                            "Tax3Amt",
                            "TDiscItem",                       

                            //21
                            "TDiscAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 21);

                            // Discount amount is in minus
                            Grd1.Cells[Row, 22].Value = 
                                Sm.GetGrdDec(Grd1, Row, 11) + 
                                Sm.GetGrdDec(Grd1, Row, 13) -
                                Sm.GetGrdDec(Grd1, Row, 20);

                            if (Sm.GetGrdStr(Grd1, Row, 14) == "N")
                                Grd1.Cells[Row, 22].Value = 
                                    Sm.GetGrdDec(Grd1, Row, 22) +
                                    (Sm.GetGrdDec(Grd1, Row, 15) / Sm.GetGrdDec(Grd1, Row, 8));

                            if (Sm.GetGrdStr(Grd1, Row, 16) == "N")
                                Grd1.Cells[Row, 22].Value = Sm.GetGrdDec(Grd1, Row, 22) +
                                    (Sm.GetGrdDec(Grd1, Row, 17)/ Sm.GetGrdDec(Grd1, Row, 8));

                            if (Sm.GetGrdStr(Grd1, Row, 18) == "N")
                                Grd1.Cells[Row, 22].Value = 
                                    Sm.GetGrdDec(Grd1, Row, 22) +
                                    (Sm.GetGrdDec(Grd1, Row, 19) / Sm.GetGrdDec(Grd1, Row, 8));


                        }, true, true, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

        }

        private void FrmPosSalesReturn_Load(object sender, EventArgs e)
        {
            try
            {
                var CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-7);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                SetGrd();
                TxtReceiptNo.Focus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsDataValid()
        {
            bool f = false;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 10).Length > 0 && Sm.GetGrdDec(Grd1, r, 10) > 0)
                    f = true;
            }
            if (!f)
                Sm.StdMsg(mMsgType.Warning, "You need to return minimum 1 item.");
            return f;
        }

        private void DoSelected()
        {
            // WARNING !!! Excluding the calculation of total discount
            if (Grd1.Rows.Count > 0 && IsDataValid())
            {
                decimal TotalDisc = 0;
                decimal TotalPayment = 0;
                int DtlNo = 0;
                string NewNumber = mFrmParent.GetNewNumber();
                string CommandList = "";
                string LineStr = "Are you sure you want to Return this item/s? " + Environment.NewLine + Environment.NewLine;
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdDec(Grd1, i, 10) > 0)
                    {
                        DtlNo++;
                        LineStr += Sm.GetGrdStr(Grd1, i, 10) + " x " + Sm.GetGrdStr(Grd1, i, 6) + Environment.NewLine;
                        CommandList +=
                            "Select '" + NewNumber + "' As TrnNoNew, " +
                            "'" + mFrmParent.BusinessDate + "' As BsDateNew, " +
                            "'" + Gv.PosNo + "' As PosNo, " +
                            DtlNo.ToString() + " As DtlNoNew, 'R' As DtlType, ItCode, BarCode, " +
                            Sm.GetGrdDec(Grd1, i, 10) + " As Qty, UPrice, DiscAmt, DiscRate, ActPrice, UWght, Tax1Amt, " +
                            " Tax2Amt, Tax3Amt, TrnNo, BsDate, DtlNo, " +
                            " @CreateBy As CreateBy, CurrentDateTime() " +
                            "From TblPOSTrnDtl " +
                            " Where TrnNo='" + Sm.GetGrdStr(Grd1, i, 2) +
                            "' And BsDate='" + Sm.GetGrdStr(Grd1, i, 3) +
                            "' And DtlNo='" + Sm.GetGrdStr(Grd1, i, 4) +
                            "' And PosNo='" + Gv.PosNo +
                            "' " + " Union All ";

                        TotalPayment += (Sm.GetGrdDec(Grd1, i, 10) * Sm.GetGrdDec(Grd1, i, 22));
                        TotalDisc += (Sm.GetGrdDec(Grd1, i, 10) * Sm.GetGrdDec(Grd1, i, 10) * Sm.GetGrdDec(Grd1, i, 20));
                    }
                }

                if (CommandList != "")
                {
                    CommandList = CommandList.Substring(0, CommandList.Length - 10);

                    CommandList =
                        "Insert Into tblpostrndtl (TrnNo, BsDate, PosNo, DtlNo, DtlType, ItCode, " +
                        "BarCode, Qty, UPrice, DiscAmt, DiscRate, ActPrice, UWght, Tax1Amt, " +
                        "Tax2Amt, Tax3Amt, RTrnNo, RBsDate, RDtlNo, CreateDt, CreateBy) " +
                        "Select T.* From (" + CommandList + ") T ";

                }
                if (Sm.StdMsgYN("Question", LineStr + Environment.NewLine +
                    "Total Return Amount : " + String.Format("{0:###,###,###,##0.00}", TotalPayment)) == DialogResult.Yes)
                {
                    // Save to table
                    var cml = new List<MySqlCommand>();
                    cml.Add(InsertDataHeader(NewNumber, TotalPayment));
                    cml.Add(InsertDataDetail(CommandList));
                    cml.Add(InsertDataPayment(NewNumber, TotalPayment));

                    Sm.ExecCommands(cml);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

        private MySqlCommand InsertDataHeader(string TrnNo, decimal TotalPayment)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPosTrnHdr");
            SQL.AppendLine("(TrnNo, TrnDtTm, BsDate, PosNo, ShiftNo, SlsType, UserCode, SlsCode, TDiscRt, TDiscAmt, Total, TChange, ");
            SQL.AppendLine("Tax1Code, Tax2Code, Tax3Code, Tax1Incl, Tax2Incl, Tax3Incl, Tax1Amt, Tax2Amt, Tax3Amt, ");
            SQL.AppendLine("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine(" Values");
            SQL.AppendLine("(@TrnNo, @TrnDtTm, @BsDate, @PosNo, @ShiftNo, @SlsType, @UserCode, @SlsCode, @TDiscRt, @TDiscAmt, @Total, @TChange, ");
            SQL.AppendLine("@Tax1Code, @Tax2Code, @Tax3Code, @Tax1Incl, @Tax2Incl, @Tax3Incl, @Tax1Amt, @Tax2Amt, @Tax3Amt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime(), Null, Null);");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@TrnNo", TrnNo);
            Sm.CmParam<String>(ref cm, "@TrnDtTm", Sm.ServerCurrentDateTime());
            Sm.CmParam<String>(ref cm, "@BsDate", mFrmParent.BusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@ShiftNo", mFrmParent.ShiftNumber);
            Sm.CmParam<String>(ref cm, "@SlsType", "R");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SlsCode", "");
            Sm.CmParam<Decimal>(ref cm, "@TDiscRt", 0);
            Sm.CmParam<Decimal>(ref cm, "@TDiscAmt", 0);
            Sm.CmParam<Decimal>(ref cm, "@Total", TotalPayment);
            Sm.CmParam<Decimal>(ref cm, "@TChange", 0);
            Sm.CmParam<String>(ref cm, "@Tax1Code", "");
            Sm.CmParam<String>(ref cm, "@Tax2Code", "");
            Sm.CmParam<String>(ref cm, "@Tax3Code", "");
            Sm.CmParam<String>(ref cm, "@Tax1Incl", "");
            Sm.CmParam<String>(ref cm, "@Tax2Incl", "");
            Sm.CmParam<String>(ref cm, "@Tax3Incl", "");
            Sm.CmParam<Decimal>(ref cm, "@Tax1Amt", 0);
            Sm.CmParam<Decimal>(ref cm, "@Tax2Amt", 0);
            Sm.CmParam<Decimal>(ref cm, "@Tax3Amt", 0);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertDataDetail(string CommandStr)
        {

            var cm = new MySqlCommand()
            {
                CommandText = CommandStr
            };
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertDataPayment(string TrnNo, decimal TotalPayment)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPosTrnPay(TrnNo, BsDate, PosNo, DtlNo, PayTpNo, PayAmt, PayAmtNett, Charge, CardNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@TrnNo, @BsDate, @PosNo, @DtlNo, ");
            SQL.AppendLine("(Select SetValue From TblPosSetting Where PosNo=@PosNo And SetCode='PayTpNoReturn'), ");
            SQL.AppendLine("@PayAmt, @PayAmtNett, @Charge, @CardNo, @CreateBy, CurrentDateTime());");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@TrnNo", TrnNo);
            Sm.CmParam<String>(ref cm, "@BsDate", mFrmParent.BusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<Int32>(ref cm, "@DtlNo", 1);
            //Sm.CmParam<String>(ref cm, "@PayTpNo", "CSH");
            Sm.CmParam<Decimal>(ref cm, "@PayAmt", TotalPayment);
            Sm.CmParam<Decimal>(ref cm, "@PayAmtNett", TotalPayment);
            Sm.CmParam<Decimal>(ref cm, "@Charge", 0);
            Sm.CmParam<String>(ref cm, "@CardNo", "");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void Grd1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                BtnOK_Click(sender, e);

            if (e.KeyChar == 27)
                BtnCancel_Click(sender, e);
        }

        private void FrmPosSalesReturn_Activated(object sender, EventArgs e)
        {
            TxtReceiptNo.Focus();

        }

        private void Grd1_DoubleClick(object sender, EventArgs e)
        {
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            DoSelected();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();

        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 10)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 9) < Sm.GetGrdDec(Grd1, e.RowIndex, 10))
                {
                    Sm.StdMsg(mMsgType.Warning, "Unable to return this item, There's not enough quantity available to be returned !");
                    Grd1.Cells[e.RowIndex, 10].Value = 0;
                    Sm.FocusGrd(Grd1, e.RowIndex, 10);
                }
            }
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

    }
}
