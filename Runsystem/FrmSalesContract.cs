﻿#region Update
/*
    29/03/2020 [TKG/KSM] new apps
    01/04/2020 [VIN/KSM] new printout
    04/04/2020 [TKG/KSM] tambah informasi bank account
    05/04/2020 [TKG/KSM] tambah shipping address
    06/04/2020 [VIN/KSM] tambah shipping address, bankaccount di printout
    07/04/2020 [VIN/KSM] bug di printout
    06/05/2020 [IBL/KSM] Print out berdasarkan Site (Spinning dan Weaving). Dengan parameter mSpinningSiteCode dan mWeavingSiteCode
    08/05/2020 [IBL/KSM] Menyesuaikan printout nama penjual
    12/05/2020 [DITA/KSM] Penomoran dokumen berdasarkan site
    15/05/2020 [IBL/KSM] Penyesuaian printout Sales Contract Weaving. Bagian KOP surat dimunculkan berdasarkan IsInclTax.
    27/05/2020 [IBL/KSM] Tambah kolom Product Name
    28/05/2020 [IBL/KSM] Penyesuaian printout
    03/06/2020 [VIN/KSM] penomoran dokumen di apabila sales memonya tidak ada KSM nya maka ikut tidak ada KSM nya. 
    03/06/2020 [VIN/KSM] feedback printout melebarkan kolom jumlah
    16/06/2020 [IBL/KSM] nomor dokumen dibedakan antara ppn dan non ppn
    27/06/2020 [HAR/KSM] Feedback Printout bank account tidak mandatory
    23/10/2020 [IBL/KSM] Feedback Printout nama pada ttd sales contract diubah.
                         Dengan parameter SalesContractPrintOutSignName1NonPPN dan SalesContractPrintOutSignName2NonPPN
    03/12/2020 [VIN/KSM] LocalDocNo tergenerate otomatis berdasarkan parameter=IsSalesMemoGenerateLocalDocNo
    11/12/2020 [VIN/KSM] Penyesuaian Printout
    22/02/2021 [BRI/KSM] Penambahan Customer Category pada field Customer berdasarkan parameter=mIsCustomerComboShowCategory
    22/02/2021 [IBL/KSM] Saat insert otomatis generate localdocno, tetapi masih bisa diubah
    24/02/2021 [VIN/KSM] Printout -> customer category tidak ditampilkan di customer
    18/03/2021 [ICA/KSM] Menambah field status
    24/03/2021 [VIN/KSM] penyesuaian menu
    24/03/2021 [VIN/KSM] tambah unit price after tax
    31/03/2021 [VIN/KSM] printout source price dibedakan PPN non PPN
    09/04/2021 [VIN/KSM] printout source remark dari remark header
 *  12/04/2021 [ICA/KSM] mengubah code status dari 1,2 dan 8 menjadi O, F dan M
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmSalesContract : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal string mSpinningSiteCode = string.Empty, mWeavingSiteCode = string.Empty;
        internal bool 
            mIsFilterBySite = false,
            mIsCustomerComboShowCategory = false,
            mIsSalesMemoGenerateLocalDocNo = false;
        private bool mIsDocNoFormatBasedOnSite = false;

        internal FrmSalesContractFind FrmFind;

        #endregion

        #region Constructor

        public FrmSalesContract(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Sales Contract";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                Sl.SetLuePtCode(ref LuePtCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                SetLueStatus(ref LueStatus, false);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Memo#",

                        //1-5
                        "Memo DNo",
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Quantity",

                        //6-10
                        "UoM",
                        "Price",
                        "Total",
                        "Delivery",
                        "Remark",

                        //11-13
                        "Product Name", 
                        "Unit Price After Tax",
                        "Total After Tax"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        0, 120, 200, 120, 100,  
                        
                        //6-10
                        80, 130, 150, 100, 300,

                        //11-13
                        150, 120, 120
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 7, 8, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });

            Grd1.Cols[11].Move(4);
            Grd1.Cols[12].Move(10);
            Grd1.Cols[13].Move(11);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, MeeCancelReason, LueBankAcCode, LuePtCode, 
                        LueStatus, MeeShippingAddress, MeeRemark
                    }, true);
                    ChkCancelInd.Enabled = false;
                    ChkIsInclTax.Enabled = false;
                    BtnSalesMemoDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, TxtLocalDocNo, LueBankAcCode, LuePtCode, MeeShippingAddress, 
                       MeeRemark
                    }, false);
                    ChkIsInclTax.Enabled = true;
                    BtnSalesMemoDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, LueStatus }, false); 
                    if (!(Sm.GetLue(LueStatus) == "O"))
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueStatus }, true);
                    ChkCancelInd.Enabled = true;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtLocalDocNo, TxtSalesMemoDocNo, 
                TxtSalesmemoLocalDocNo, TxtCtCode, TxtSiteCode, LueBankAcCode, LuePtCode, 
                TxtCurCode, MeeShippingAddress, MeeRemark, LueStatus, TxtTotalAmt
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt, TxtTotalAmt }, 0);
            ChkCancelInd.Checked = false;
            ChkIsInclTax.Checked = false;
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 7, 8, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesContractFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                Sm.SetLue(LueStatus, "O");
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            if (Sm.GetLue(LueStatus) == "O")
            {
                SetLueStatus(ref LueStatus, true);
                Sm.SetLue(LueStatus, "O");
            }
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            string SiteCode = Sm.GetValue("SELECT B.SiteCode FROM TblSalesContract A INNER JOIN TblSalesMemoHdr B ON A.SalesMemoDocNo = B.DocNo WHERE A.DocNo = @Param; ", TxtDocNo.Text);

            if (TxtDocNo.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;

                string[] TableName = { "SalesContractHdr", "SalesContractDtl" };
                List<IList> myLists = new List<IList>();

                var l = new List<SalesContractHdr>();
                var ldtl = new List<SalesContractDtl>();
                decimal Numb = 0;

                #region Header

                var cm = new MySqlCommand();

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT @CompanyLogo As CompanyLogo, H.CtName, ");
                SQL.AppendLine("E.SPName, C.BankName, B.BankAcNo, B.BranchAcNm, B.BankAcNm, D.TaxInd, ");
                SQL.AppendLine("case ");
                SQL.AppendLine("    when D.TaxInd='Y' then 'Include PPN' ");
                SQL.AppendLine("    ELSE 'Tidak Dipungut PPN' ");
                SQL.AppendLine("END AS status, F.TTD1, G.TTD2, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'SalesContractPrintOutSignName1NonPPN') TTD3, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'SalesContractPrintOutSignName2NonPPN') TTD4, A.Remark ");
                SQL.AppendLine("FROM TblSalesContract A ");
                SQL.AppendLine("Left Join TblBankAccount B On A.BankAcCode=B.BankAcCode ");
                SQL.AppendLine("Left Join TblBank C On B.BankCode=C.BankCode ");
                SQL.AppendLine("Inner Join TblSalesMemoHdr D ON A.SalesMemoDocNo = D.DocNo ");
                SQL.AppendLine("Left Join TblSalesPerson E On D.SPCode = E.SPCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    select B.username As TTD1 ");
                SQL.AppendLine("    From tblparameter A ");
                SQL.AppendLine("    Inner Join Tbluser B On A.ParValue = B.UserCode ");
                SQL.AppendLine("    Where A.parCode = 'SalesContractPrintOutSignName1' ");
                SQL.AppendLine(")F On 0=0 ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    select B.username As TTD2 ");
                SQL.AppendLine("    From tblparameter A ");
                SQL.AppendLine("    Inner Join Tbluser B On A.ParValue = B.UserCode ");
                SQL.AppendLine("    Where A.parCode = 'SalesContractPrintOutSignName2' ");
                SQL.AppendLine(")G On 0=0 ");
                SQL.AppendLine("Inner Join TblCustomer H On D.CtCode=H.CtCode ");
                SQL.AppendLine("WHERE A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "SPName",
                         "BankName", 
                         "BankAcNo", 
                         "BranchAcNm", 
                         "BankAcNm" ,

                         //6-10
                         "Status",
                         "TaxInd",
                         "TTD1", 
                         "TTD2",
                         "TTD3",

                         //11-13
                         "TTD4", 
                         "CtName",
                         "Remark"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new SalesContractHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                SPName = Sm.DrStr(dr, c[1]),
                                BankName = Sm.DrStr(dr, c[2]),
                                BankAcNo = Sm.DrStr(dr, c[3]),
                                BranchAcNm = Sm.DrStr(dr, c[4]),
                                BankAcNm = Sm.DrStr(dr, c[5]),

                                DocNo = TxtLocalDocNo.Text,
                                Customer = Sm.DrStr(dr, c[12]),
                                DocDt = DteDocDt.Text,
                                Status = Sm.DrStr(dr, c[6]),
                                IsInclTax = Sm.DrStr(dr, c[7]),

                                ShippingAddress = MeeShippingAddress.Text,
                                TermOfPayment = LuePtCode.Text,
                                TTD1 = Sm.DrStr(dr, c[8]),
                                TTD2 = Sm.DrStr(dr, c[9]),
                                TTD3 = Sm.DrStr(dr, c[10]),

                                TTD4 = Sm.DrStr(dr, c[11]),
                                Remark = Sm.DrStr(dr, c[13]),
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion
                

                #region Detail

                var cmdtl = new MySqlCommand();

                var SQLdtl = new StringBuilder();

                SQLdtl.AppendLine("Select A.DNo, DATE_FORMAT(C.DocDt, '%d/%M/%y') As DocDt, A.ItCode, A.ProductName, B.ItName, B.ItCodeInternal, A.Qty, B.SalesUomCode, ");
                SQLdtl.AppendLine("case "); 
				SQLdtl.AppendLine("	 when C.TaxInd='Y' then  A.UPriceAfterTax ");
				SQLdtl.AppendLine("	 ELSE A.UPrice ");
				SQLdtl.AppendLine("	 END AS UPriceAfterTax, ");
				SQLdtl.AppendLine("	 case "); 
				SQLdtl.AppendLine("	 when C.TaxInd='Y' then  (A.Qty*A.UPriceAfterTax) "); 
				SQLdtl.AppendLine("	 ELSE (A.Qty*A.UPrice) ");
                SQLdtl.AppendLine("	 END AS 'TotalPrice', "); 
                SQLdtl.AppendLine("A.DeliveryDt, A.Remark  ");
                SQLdtl.AppendLine("From TblSalesMemoDtl A    ");
                SQLdtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode    ");
                SQLdtl.AppendLine("Inner Join TblSalesMemoHdr C ON A.Docno=C.DocNo  ");
                SQLdtl.AppendLine(" WHERE A.DocNo=@DocNo ");


                using (var cndtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cndtl.Open();
                    cmdtl.Connection = cndtl;
                    cmdtl.CommandText = SQLdtl.ToString();
                    Sm.CmParam<String>(ref cmdtl, "@DocNo", TxtSalesMemoDocNo.Text);
                    var drdtl = cmdtl.ExecuteReader();
                    var cdtl = Sm.GetOrdinal(drdtl, new string[] 
                        {
                         //0
                         "DocDt",

                         "ItName",
                         "Qty",
                         "SalesUomCode",
                         "UPriceAfterTax",
                         "TotalPrice",

                         "ProductName",
                         "Remark",
                         "ItCodeInternal"
                        });
                    if (drdtl.HasRows)
                    {
                        Numb = 0;
                        while (drdtl.Read())
                        {
                            Numb = Numb + 1;

                            ldtl.Add(new SalesContractDtl()

                            {
                                Number = Numb,

                                DocDt = Sm.DrStr(drdtl, cdtl[0]),
                                ItName = Sm.DrStr(drdtl, cdtl[1]),
                                Qty = Sm.DrDec(drdtl, cdtl[2]),
                                SalesUom = Sm.DrStr(drdtl, cdtl[3]),
                                UPrice = Sm.DrDec(drdtl, cdtl[4]),
                                TotalPrice = Sm.DrDec(drdtl, cdtl[5]),
                                ProductName = Sm.DrStr(drdtl, cdtl[6]),
                                Remark = Sm.DrStr(drdtl, cdtl[7]),
                                ItCodeInternal = Sm.DrStr(drdtl, cdtl[8]),
                            });
                        }
                    }
                    drdtl.Close();
                }

                myLists.Add(ldtl);
                #endregion

                if (Sm.GetParameter("DocTitle") == "KSM")
                {
                    if (SiteCode == mSpinningSiteCode)
                        Sm.PrintReport("SalesContract_Spinning", myLists, TableName, false);
                    if (SiteCode == mWeavingSiteCode)
                        Sm.PrintReport("SalesContract_Weaving", myLists, TableName, false);
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            
            if (mIsDocNoFormatBasedOnSite)
                DocNo = GenerateDocNo2(Sm.GetDte(DteDocDt), "SalesContract", "TblSalesContract");
            else
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesContract", "TblSalesContract");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesContract(DocNo, TxtLocalDocNo.Text));

            Sm.ExecCommands(cml);
            ShowData(DocNo);

        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtSalesMemoDocNo, "Sales memo", false) ||
                Sm.IsLueEmpty(LuePtCode, "Term of payment") ||
                //Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                IsSalesMemoAlreadyCancel() ||
                IsSalesMemoAlreadyProcess();
        }

        private bool IsSalesMemoAlreadyCancel()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesMemoHdr Where DocNo=@Param And CancelInd='Y';",
                TxtSalesMemoDocNo.Text,
                "This sales's memo already cancelled."
                );
        }

        private bool IsSalesMemoAlreadyProcess()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesContract Where SalesMemoDocNo=@Param And CancelInd='N';",
                TxtSalesMemoDocNo.Text,
                "This sales's memo already processed."
                );
        }

        private MySqlCommand SaveSalesContract(string DocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSalesContract(DocNo, CancelInd, CancelReason, DocDt, Status, LocalDocNo, SalesMemoDocNo, BankAcCode, PtCode, IsInclTax, ShippingAddress, Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, 'N', Null, @DocDt, @Status, @LocalDocNo, @SalesMemoDocNo, @BankAcCode, @PtCode, @IsInclTax, @ShippingAddress, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SalesMemoDocNo", TxtSalesMemoDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@IsInclTax", ChkIsInclTax.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ShippingAddress", MeeShippingAddress.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSalesContract());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Sales contract#", false) ||
                IsDocumentAlreadyCancel();
        }

        private bool IsDocumentNotOutsanding()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSalesContract ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status != '1' or Status != 'O' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already fullfiled / manual fulfilled.");
                return true;
            }

            return false;
        }

        private bool IsDocumentAlreadyCancel()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSalesContract Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private MySqlCommand EditSalesContract()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesContract Set ");
            SQL.AppendLine("    Status=@Status, CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                SetLueStatus(ref LueStatus, false);
                ShowSalesContract(DocNo);
                if (TxtSalesMemoDocNo.Text.Length>0) ShowSalesMemo(TxtSalesMemoDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesContract(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DocDt, Status, CancelReason, CancelInd, LocalDocNo, SalesMemoDocNo, PtCode, BankAcCode, IsInclTax, ShippingAddress, Remark ");
            SQL.AppendLine("From TblSalesContract ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "LocalDocNo", "SalesMemoDocNo", 
                        
                        //6-10
                        "PtCode", "BankAcCode", "IsInclTax", "ShippingAddress", "Remark", 

                        //11 
                        "Status"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtSalesMemoDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[7]));
                        ChkIsInclTax.Checked = Sm.DrStr(dr, c[8])=="Y";
                        MeeShippingAddress.EditValue = Sm.DrStr(dr, c[9]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetLue(LueStatus, Sm.DrStr(dr, c[11]));
                    }, true
                );
        }

        internal void ShowSalesMemo(string SalesMemoDocNo)
        { 
            ShowSalesMemoHdr(SalesMemoDocNo);
            ShowSalesMemoDtl(SalesMemoDocNo);
        }

        private void ShowSalesMemoHdr(string SalesMemoDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.LocalDocNo, B.CtName, D.CtCtName, C.SiteName, A.CurCode, A.Amt, A.TotalAmt ");
            SQL.AppendLine("From TblSalesMemoHdr A ");
            SQL.AppendLine("Left Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("Left Join TblCustomerCategory D On B.CtCtCode=D.CtCtCode ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", SalesMemoDocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]{ 
                        //0
                        "LocalDocNo", 
                        
                        //1-5
                        "CtName", "CtCtName", "SiteName", "CurCode", "Amt", 
                        
                        //6
                        "TotalAmt" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSalesmemoLocalDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[1]) + (mIsCustomerComboShowCategory ? " [" + Sm.DrStr(dr, c[2]) + "]" : "");
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    }, false
                );
        }

        private void ShowSalesMemoDtl(string SalesMemoDocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", SalesMemoDocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ItCodeInternal, A.Qty, B.SalesUomCode, ");
            SQL.AppendLine("A.UPrice, A.DeliveryDt, A.Remark, A.ProductName, A.UPriceAfterTax ");
            SQL.AppendLine("From TblSalesMemoDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ItCode; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "Qty", "SalesUomCode", 
                    
                    //6-9
                    "UPrice", "DeliveryDt", "Remark", "ProductName", "UPriceAfterTax"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = SalesMemoDocNo;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                    Grd.Cells[Row, 8].Value = Sm.GetGrdDec(Grd, Row, 5) * Sm.GetGrdDec(Grd, Row, 7);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);
                    Grd.Cells[Row, 13].Value = Sm.GetGrdDec(Grd, Row, 5) * Sm.GetGrdDec(Grd, Row, 12);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 7, 8, 12, 13 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private string GenerateDocNo2(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string mDocNo = string.Empty,
            Yr = Sm.Left(DocDt, 4),
            Mth = DocDt.Substring(4, 2),
            

            DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType = @Param Limit 1; ", DocType),
            CtShortCode = Sm.GetValue("Select B.CtShortCode From TblSalesMemoHdr A Inner Join TblCustomer B On A.CtCode = B.CtCode WHERE A.DocNo = @Param; ", TxtSalesMemoDocNo.Text),
            SiteShortCode = Sm.GetValue("Select B.ShortCode From TblSalesMemoHdr A Inner Join TblSite B On A.SiteCode = B.SiteCode Where A.DocNo = @Param; ", TxtSalesMemoDocNo.Text),
            DocTitle = Sm.GetParameter("DocTitle");

            SQL.Append("Select Concat(  ");
            SQL.Append("IfNull((  ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From (  ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, LENGTH(CONCAT(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)))=Concat(IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr)  ");
            if (TxtSalesMemoDocNo.Text.Contains("KSM"))
                SQL.Append("       And Substr(DocNo, 6, 3) = 'KSM' ");
            else
                SQL.Append("       And Substr(DocNo, 6, 3) != 'KSM' ");
            SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1  ");
            SQL.Append("       ) As Temp  ");
            SQL.Append("   ), '0001')  ");
            if (TxtSalesMemoDocNo.Text.Contains("KSM"))
            {
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("'  ");
            }
            SQL.Append(", '/', IfNull(@SiteShortCode, ''), '/',@DocAbbr, '/', IfNull(@CtShortCode, ''), '/', @Mth,'/', @Yr ");
            SQL.Append(") As DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
                Sm.CmParam<String>(ref cm, "@CtShortCode", CtShortCode);
                Sm.CmParam<String>(ref cm, "@SiteShortCode", SiteShortCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return mDocNo;
        }

        private void GetParameter()
        {
            mSpinningSiteCode = Sm.GetParameter("SpinningSiteCode");
            mWeavingSiteCode = Sm.GetParameter("WeavingSiteCode");
            mIsDocNoFormatBasedOnSite = Sm.GetParameterBoo("IsDocNoFormatBasedOnSite");
            mIsSalesMemoGenerateLocalDocNo = Sm.GetParameterBoo("IsSalesMemoGenerateLocalDocNo");
            mIsCustomerComboShowCategory = Sm.GetParameterBoo("IsCustomerComboShowCategory");
        }

        private void SetLueStatus(ref LookUpEdit Lue, bool editable)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'O' As Col1, 'Outstanding' As Col2 ");
            if (!editable)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'F' As Col1, 'Fulfilled' As Col2 ");
            }
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'M' As Col1, 'Manual Fulfilled' As Col2 ");
            SQL.AppendLine("; ");

            Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnSalesMemoDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesContractDlg(this));
        }

        private void BtnSalesMemoDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSalesMemoDocNo, "Sales memo#", false))
            {
                var f1 = new FrmSalesMemo("***");
                f1.Tag = "***";
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtSalesMemoDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        #endregion

        #endregion

        #region Class

        private class SalesContractHdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Customer { get; set; }
            public string Status { get; set; }
            public string SPName { get; set; }
            public string BankName  { get; set; } 
            public string BankAcNo  { get; set; }
            public string BranchAcNm  { get; set; }
            public string BankAcNm { get; set; }
            public string ShippingAddress { get; set; }
            public string CompanyLogo { get; set; }
            public string TermOfPayment { get; set; }
            public string IsInclTax { get; set; }
            public string TTD1 { get; set; }
            public string TTD2 { get; set; }
            public string TTD3 { get; set; }
            public string TTD4 { get; set; }
            public string Remark { get; set; }
        }

        private class SalesContractDtl
        {
            public string ItName { get; set; }
            public string SalesUom { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal TotalPrice { get; set; }
            public string BankName{ get; set; }
            public string BankAcNo { get; set; }
            public string BankSite { get; set; }
            public string BankAcName { get; set; }
            public string DocDt { get; set; }
            public decimal Number { get; set; }
            public string ProductName { get; set; }
            public string Remark { get; set; }
            public string ItCodeInternal { get; set; }
        }
        #endregion
    }
}
