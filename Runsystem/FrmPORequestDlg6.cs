﻿#region Update
/*
   30/12/2021 [YOG/SIER] Penambahan Dialog baru untuk menampung dokumen outstanding PO Request yang di cancel
 */
#endregion
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.Data.SqlClient;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestDlg6 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPORequest mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPORequestDlg6(FrmPORequest FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.SiteCode, B.DNo, B.EstimatePrice As POREstPrice, B.MaterialRequestDNo, B.CancelInd, A.LocalDocNo, C.DocDt MRDate, C.PICCode, ");
            SQL.AppendLine("Case When B.Status = 'O' Then 'Outstanding' ");
            SQL.AppendLine("When B.Status = 'A' Then 'Approved' ");
            SQL.AppendLine("When B.Status = 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            if (mFrmParent.mIsSiteMandatory)
                SQL.AppendLine("L.SiteName, ");
            else
                SQL.AppendLine("Null As SiteName, ");
            SQL.AppendLine("B.MaterialRequestDocNo, E.DeptName, D.ItCode, D.EstPrice, ");
            SQL.AppendLine("F.ItName, F.ForeignName, ");
            SQL.AppendLine("B.Qty, F.PurchaseUomCode, ");
            SQL.AppendLine("I.VdName, J.PtName, K.DTName, G.CurCode, G.DocDt As QtDate, H.UPrice, (B.Qty*H.UPrice) As Total, H.DocNo As QtDocNo, H.DNo As QtDNo, H.UPriceInit, D.UsageDt, M.PropName, ");
            SQL.AppendLine("B.CreateBy, B.CreateDt, B.LastUpBy, B.LastUpDt, F.ItCodeInternal, ");
            SQL.AppendLine("ifnull(B.Remark, A.Remark) As PORRemark, ifnull(D.Remark, C.Remark) As MRRemark, F.Specification, F.ItScCode, Q.ItScName, ");

            if (mFrmParent.mIsUseECatalog) SQL.AppendLine("Case B.TakeOrderInd When 'Y' Then 'Yes' When 'N' Then 'No' Else '' End As TakeOrderInd, TakeOrderRemark, ");
            else SQL.AppendLine("Null As TakeOrderInd, Null AS TakeOrderRemark, ");

            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("O.ProjectCode, O.ProjectName, ");
            else
                SQL.AppendLine("Null as ProjectCode, Null As ProjectName, ");

            if (mFrmParent.mIsMRUseProcurementType)
                SQL.AppendLine("P.OptDesc as ProcurementType ");
            else
                SQL.AppendLine("Null as ProcurementType ");
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo=C.DocNo ");
            if (mFrmParent.mIsMRWithTenderEnabled)
            {
                if (mFrmParent.mMenuCodeForPORTender)
                    SQL.AppendLine("    And C.TenderInd = 'Y' ");
                else
                    SQL.AppendLine("    And C.TenderInd = 'N' ");
            }
            if (mFrmParent.mIsPORequestFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=C.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And C.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=C.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On B.MaterialRequestDocNo=D.DocNo And B.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Inner Join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("Inner Join TblQtHdr G On B.QtDocNo=G.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And G.PtCode Is Not Null ");
                SQL.AppendLine("And G.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl H On B.QtDocNo=H.DocNo And B.QtDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblVendor I On G.VdCode=I.VdCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm J On G.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblDeliveryType K On G.DTCode=K.DTCode ");
            if (mFrmParent.mIsSiteMandatory)
                SQL.AppendLine("Left Join TblSite L On A.SiteCode=L.SiteCode ");
            SQL.AppendLine("Left Join TblProperty M On B.PropCode=M.PropCode ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                #region Old Code
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select T1.DocNo, T8.DNo, Group_Concat(Distinct IfNull(T6.ProjectCode, T4.ProjectCode2)) ProjectCode, Group_Concat(Distinct IfNull(T6.ProjectName, T3.ProjectName)) ProjectName ");
                //SQL.AppendLine("    From TblBOMRevisionHdr T1 ");
                //SQL.AppendLine("    Inner Join TblBOQHdr T2 On T1.BOQDocNo = T2.DocNo ");
                //SQL.AppendLine("    Inner Join TblLOPHdr T3 ON T2.LOPDocNo = T3.DocNo ");
                //SQL.AppendLine("    Left Join TblSOContractHdr T4 On T2.DocNo = T4.BOQDocNo ");
                //SQL.AppendLine("    Left Join TblNoticeToProceed T5 On T3.DocNo = T5.LOPDocNo ");
                //SQL.AppendLine("    Left Join TblProjectGroup T6 On T3.PGCode = T6.PGCode ");
                //SQL.AppendLine("    Inner Join TblMaterialRequestDtl T7 ON T1.DocNo = T7.BOMRDocNo ");
                //SQL.AppendLine("    Inner Join TblBOMRevisionDtl T8 ON T1.DOcNo = T8.DocNo And T7.BOMRDNo = T8.DNo ");
                //SQL.AppendLine("    Group By T1.DocNo, T8.DNo ");
                //SQL.AppendLine(") O On D.BOMRDocNo = O.DOcNo And D.BOMRDNo = O.DNo ");
                #endregion

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T3.DocNo, T3.DNo, Group_Concat(Distinct IfNull(T7.ProjectCode, T4.ProjectCode2)) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T7.ProjectName, T6.ProjectName)) ProjectName ");
                SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
                SQL.AppendLine("    Inner join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.SOCDocNo Is not Null ");
                SQL.AppendLine("    Inner join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo ");
                SQL.AppendLine("        And T2.DNo = T3.MaterialRequestDNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T4 On T1.SOCDocNo = T4.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T5 On T4.BOQDocNo = T5.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T6 On T5.LOPDocNo = T6.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T7 On T6.PGCode = T7.PGCode ");
                SQL.AppendLine("    Group By T3.DocNo, T3.DNo ");
                SQL.AppendLine(") O On B.DocNo = O.DocNo And B.DNo = O.DNo ");
            }
            if (mFrmParent.mIsMRUseProcurementType)
                SQL.AppendLine("Left Join TblOption P On P.OptCode=A.ProcurementType AND P.OptCat='ProcurementType' ");
            SQL.AppendLine("Left Join TblItemSubCategory Q On Q.ItScCode = F.ItScCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("And B.LastUpBy = @UserCode ");
            SQL.AppendLine("And B.CancelInd = 'Y' ");
            SQL.AppendLine("And B.Status In ('O') ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 50;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "Cancel",
                    "Status",
                    "Local"+Environment.NewLine+"Document#",
                    
                    //6-10
                    "Site",
                    ((mFrmParent.Doctitle=="IMS") ? "Purchase" : "Material")+Environment.NewLine+"Request#",
                    "Department",
                    "Item's Code",
                    "Item's Name",

                    //11-15
                    "Local Code",
                    "Foreign"+Environment.NewLine+"Name",
                    "Quantity",
                    "UoM",
                    "Vendor",

                    //16-20
                    "Currency",
                    "Price",
                    "Total",
                    "Term of"+Environment.NewLine+"Payment",
                    "Delivery"+Environment.NewLine+"Type",
                    
                    //21-25
                    "Usage Date",
                    "Property",
                    "Created By",
                    "Created Date",
                    "Created Time", 
                    
                    //26-30
                    "Last Updated By",
                    "Last Updated Date",
                    "Last Updated Time",
                    "PO Request"+Environment.NewLine+"Remark",
                    ((mFrmParent.Doctitle=="IMS") ? "Purchase Request" : "Material Request")+Environment.NewLine+"Remark",
                    

                    //31-35
                    "Specification",
                    "Project's Code",
                    "Project's Name",
                    "Procurement Type",
                    "Take Order",

                    //36-40
                    "Vendor Remark",
                    "DNo",
                    "Requested"+Environment.NewLine+"D#",
                    "Requested"+Environment.NewLine+"Date",
                    "Quotation#",

                    //41-45
                    "Quotation"+Environment.NewLine+"D Number",
                    "Quotation"+Environment.NewLine+"Date",
                    "ItScCode",
                    "Sub-Category",
                    "Site Code",

                    //46-50
                    "Estimated"+Environment.NewLine+"Price",
                    "Person In Charge",
                    "PO Request's"+Environment.NewLine+"Estimate Price",
                    "Quotation Price",

                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 100, 150,  
                    
                    //6-10
                    150, 150, 200, 120, 250, 
                    
                    //11-15
                    130, 200, 80, 80, 200, 
                    
                    //16-20
                    60, 100, 120, 150, 150, 
                    
                    //21-25
                    120, 200, 130, 130, 130, 
                    
                    //26-30
                    130, 130, 130, 200, 200,

                    //31-35
                    300, 130, 200 ,120, 100,

                    //36-40
                    200 , 150, 150, 150, 150,

                    //41-45
                    150, 150, 150, 140, 150,

                    //46
                    150, 150, 150, 150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 17, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 21, 24, 27 });
            Sm.GrdFormatTime(Grd1, new int[] { 25, 28 });
            Sm.GrdColInvisible(Grd1, new int[] { 22 }, mFrmParent.mPORequestPropCodeEnabled);
            Sm.GrdColInvisible(Grd1, new int[] { 21, 23, 24, 25, 26, 27, 28, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 }, false);
            if (!mFrmParent.mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 35, 36 });
            else
            {
                Grd1.Cols[35].Move(21);
                Grd1.Cols[36].Move(22);
            }
            if (!mFrmParent.mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 6 }, false);
            if (mFrmParent.mPORequestPropCodeEnabled) Grd1.Cols[22].Move(13);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 31, 32, 33 });
            if (!mFrmParent.mIsMRUseProcurementType) Sm.GrdColInvisible(Grd1, new int[] { 34 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 ,26,27, 28, 29, 30, 31, 32, 33, 34, 35, 36 });
            Grd1.Cols[29].Move(21);
            Grd1.Cols[30].Move(22);
            Grd1.Cols[31].Move(14);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 21, 23, 24, 25, 26, 27, 28 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And Concat(C.DocNo, D.DNo) Not In (" + mFrmParent.GetSelectedMaterialRequest() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtMaterialRequestDocNo.Text, "B.MaterialRequestDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "F.ItName", "F.ItCodeInternal", "F.ForeignName" });


                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "LocalDocNo", "SiteName", 
                        
                        //6-10
                        "MaterialRequestDocNo", "DeptName", "ItCode", "ItName", "ItCodeInternal", 
                        
                        //11-15
                        "ForeignName", "Qty", "PurchaseUomCode", "VdName", "CurCode", 
                        
                        //16-20
                        "UPrice", "Total", "PtName", "DTName", "UsageDt", 
                        
                        //21-25
                        "PropName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                        //26-30
                        "PORRemark", "MRRemark", "Specification", "ProjectCode", "ProjectName",

                        //31-35
                        "ProcurementType", "TakeOrderInd", "TakeOrderRemark", "DNo", "MaterialRequestDNo",

                        //36-40
                        "MRDate", "QtDocNo", "QtDNo", "QtDate", "ItScCode",

                        //41-45
                        "ItScName", "SiteCode", "EstPrice", "PICCode", "POREstPrice",

                        //46
                        "UPriceInit"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 28, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 32);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 33);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 35);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 39, 36);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 37);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 38);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 39);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 40);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 41);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 45, 42);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 43);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 44);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 45);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 46);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            string DocNo = Sm.GetGrdStr(Grd1, Grd1.CurCell.RowIndex, 1);
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1) == DocNo)
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        mFrmParent.Grd1.Cells[Row1, 39].Value = Row1 + 1;
                        mFrmParent.TxtCopyData.Text = Sm.GetGrdStr(Grd1, Row2, 1);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 37);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 38);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 39);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 30);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 40);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 41);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 42);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 29);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 43);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 44);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 45);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 35, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 37, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 38, Grd1, Row2, 46);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 43, Grd1, Row2, 31);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 49, Grd1, Row2, 32);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 50, Grd1, Row2, 33);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 51, Grd1, Row2, 48);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 53, Grd1, Row2, 49);

                        if (Sm.GetLue(mFrmParent.LueProcType).Length == 0)
                            Sm.SetLue(mFrmParent.LueProcType, Sm.GetGrdStr(Grd1, Row2, 34));

                        for (int Rows = 0; Rows < mFrmParent.Grd1.Rows.Count; Rows++)
                        {
                            Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, Rows, new int[] { 1, 2 });
                        }

                        mFrmParent.Grd1.Rows.Add();
                    } 
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            mFrmParent.SetSeqNo();

            this.Close();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #region Misc Control Method
        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtMaterialRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkMaterialRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, (mFrmParent.Doctitle == "IMS" ? "Purchase request#" : "Material request#"));
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkProcType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Procurement Type");
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion


    }
}
