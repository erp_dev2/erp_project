﻿#region Update
/*
    16/08/2017 [WED] Tambah informasi item jasa
    01/11/2019 [WED/IMS] DocType 1 untuk DR Biasa dan CBD Biasa
 *  24/02/2021 [ICA/SIER] Menambah kolom customer category based on parameter IsCustomerComboBasedOnCategory
 *  03/03/2023 [VIN/KBN] BUG: Dtae format kolom date 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDRFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDR mFrmParent;
        string mSQL = string.Empty;
        bool mCBDInd = false;
       
        #endregion

        #region Constructor

        public FrmDRFind(FrmDR FrmParent, bool CBDInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCBDInd = CBDInd;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, A.CancelInd, L.ItName As LogisticItName, ");
            SQL.AppendLine("A.LocalDocNo, ");
            SQL.AppendLine("Case A.processInd ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'F' Then 'FulFilled' ");
            SQL.AppendLine("    When 'M' Then 'Manual FulFilled' ");
            SQL.AppendLine("    When 'P' Then 'Partial FulFilled' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("D.WhsName, C.CtName, I.ItCode, J.ItCodeInternal, J.ItName,  ");
            SQL.AppendLine("B.SODOcNo, B.QtyPackagingUnit, B.Qty, B.QtyInventory, ");
            SQL.AppendLine("F.PackagingUnitUomCode, H.PriceUomCode, J.InventoryUomCode, M.CtCtName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  ");
            SQL.AppendLine("From TblDrHdr A  ");
            SQL.AppendLine("Inner join TblDrDtl B On A.DocNo = B.DocNo AND A.DocType = '1' ");
            SQL.AppendLine("Inner join TblCustomer C On A.CtCode = C.CtCode  ");
            SQL.AppendLine("Left Join TblWarehouse D On A.WhsCode = D.WhsCode ");
            SQL.AppendLine("Left Join TblSOHdr E On B.SODOcNo = E.DOcNo ");
            SQL.AppendLine("Left Join TblSODtl F On B.SODOcNo = F.DOcNo And B.SODNo = F.DNo ");
            SQL.AppendLine("Left Join TblCtQtDtl G On E.CtQtDOcNo = G.DOcNo And F.CtQtDNo = G.DNo ");
            SQL.AppendLine("Left Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo ");
            SQL.AppendLine("Left Join TblItem J On I.ItCode = J.ItCode ");
            if(mCBDInd == true)
                SQL.AppendLine("Inner Join TblSalesInvoiceHdr K On E.Docno = K.SODocNo ");
            SQL.AppendLine("Left Join TblItem L On A.ItCode = L.ItCode ");
            SQL.AppendLine("Left Join TblCustomerCategory M On C.CtCtCode = M.CtCtCode ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Status",
                        "Customer",

                        //6-10
                        "Customer"+Environment.NewLine+"Category",
                        "Item's Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "Item's Name",

                        //11-15
                        "SO#",
                        "",
                        "Quantity"+Environment.NewLine+"(Packaging Unit)",
                        "UoM"+Environment.NewLine+"(Packaging Unit)",
                        "Quantity"+Environment.NewLine+"(Sales)",
                        
                        //16-20
                        "UoM"+Environment.NewLine+"(Sales)",
                        "Quantity"+Environment.NewLine+"(Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        
                        //21-25
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Local"+Environment.NewLine+"Document",

                        //26
                        "Logistic"
                    }, 
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 80, 200, 
                        
                        //6-10
                        130, 80, 20, 80, 200, 
                        
                        //11-15
                        130, 20, 100, 100, 100, 

                        //16-20
                        100, 100, 100, 100, 100, 
 
                        //21-25
                        100, 100, 100, 100, 130, 

                        //26
                        250
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 8, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Grd1.Cols[25].Move(2);
            Grd1.Cols[26].Move(8);
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 19, 20, 21, 22, 23, 24, 25, 26 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[6].Visible = mFrmParent.mIsCustomerComboBasedOnCategory;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 19, 20, 21, 22, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                string Filter = string.Empty;
                Cursor.Current = Cursors.WaitCursor;
                if (mCBDInd == false)
                {
                    Filter = "Where E.DocNo Not In (Select SODocNo From TblSalesInvoiceHdr Where CancelInd = 'N' and SODocno Is not null ) ";
                }

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, "B.SODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "I.ItCode", "J.ItCodeInternal", "J.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc",  "CtName", "CtCtName",      
                            
                            //6-10
                            "ItCode", "ItCodeInternal", "ItName", "SODocNo", "QtyPackagingUnit",  
                            
                            //11-15
                            "PackagingUnitUomCode", "Qty", "PriceUomCode", "QtyInventory", "InventoryUomCode",   
                
                            //16-20
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "LocalDocNo", 

                            //21
                            "LogisticItName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion
    }
}
