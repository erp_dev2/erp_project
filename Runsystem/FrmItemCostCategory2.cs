﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmItemCostCategory2 : RunSystem.FrmBase5
    {
        #region Field

        internal string 
            mMenuCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        private string mSQL = string.Empty;
        private int ColInd = 0;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmItemCostCategory2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        private string SetSQL(bool IsFilteredByDocDt)
        {
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("    Select distinct A.CCCode, E.CCName, B.ItCode, C.ItName ");
            SQL.AppendLine("    From TblDODeptHdr A ");
  	        SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.cancelInd = 'N' ");
            SQL.AppendLine("    Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("    Left Join TblItemCostCategory D On C.ItCode=D.ItCode And A.CCCode=D.CCCode ");
            SQL.AppendLine("    Left Join TblCostCenter E On A.CCCode = E.CCCode");
            SQL.AppendLine("    Where A.JournalDocNo Is Null ");
            SQL.AppendLine("    And A.CCCode Is Not Null ");
            SQL.AppendLine("    And D.CCtCode Is Null ");
            SQL.AppendLine("    And A.DocNo In ( ");
	        SQL.AppendLine("        Select A.DocNo ");
	        SQL.AppendLine("        From TblDODeptHdr A ");
	        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.cancelInd = 'N' ");
	        SQL.AppendLine("        Inner Join TblItem C On B.ItCode=C.ItCode ");
	        SQL.AppendLine("        Left Join TblItemCostCategory D On C.ItCode=D.ItCode And A.CCCode=D.CCCode ");
	        SQL.AppendLine("        Where A.JournalDocNo Is Null ");
	        SQL.AppendLine("        And A.CCCode Is Not Null ");
	        SQL.AppendLine("        And D.CCtCode Is Null ");
            if (IsFilteredByDocDt)
            {
                SQL.AppendLine(" And A.DocDt Between @DocDt1 And @DocDt2 ");
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")T ");

            return SQL.ToString();
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            SetGrd();
            SetSQL();
            LueCCtCode.Visible = false;
        }  

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;       
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "CostCenter"+Environment.NewLine+"Code",

                        //1-5
                        "CostCenter"+Environment.NewLine+"Name",
                        "Item"+Environment.NewLine+"Code",
                        "Item"+Environment.NewLine+"Name",
                        "CostCategory"+Environment.NewLine+"Code",
                        "CostCategory"+Environment.NewLine+"Name"
                    },
                    new int[] 
                    {
                        //0
                        80,

                        //
                        240, 100, 300, 100, 350
                    }

                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] {0, 2, 4 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               DteDocDt1, DteDocDt2
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                if (ChkDocDt.Checked == true)
                {
                    Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                    Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                }
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(ChkDocDt.Checked) + Filter + " Order By T.CCName",
                        new string[] 
                        { 
                            "CCCode", "CCName", "ItCode", "ItName" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.WaitCursor;
            }
        }

        override protected void SaveData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) 
                            cml.Add(SaveItemCostCategory(Row));
                        ColInd = Row;
                    }
                Sm.ExecCommands(cml);
           
            ShowData();
            FocusGrid(ColInd);   
        }

        private void FocusGrid(int Col)
        {
                Sm.FocusGrd(Grd1, Col, 0);
                Cursor.Current = Cursors.WaitCursor;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                IsGrdEmpty(); 
        }

        private MySqlCommand SaveItemCostCategory(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemCostCategory(ItCode, CCCode, CCtCode, CreateBy, CreateDt) " +
                    "Values(@ItCode, @CCCode, @CCtCode, @CreateBy, CurrentDateTime()) "+
                    "ON Duplicate Key Update CCtCode = @CCtCode,LastUpBy=@UserCode, LastUpDt=CurrentDateTime();  "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to select at least 1 item.");
                return true;
            }
            return false;
        }

        #region Grid method

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 3 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);

            if (Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueCCtCode, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                SetLueCCtCode(ref LueCCtCode, Sm.GetGrdStr(Grd1, e.RowIndex, 0));
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 2, 3, 4 }, e);
        }


        #endregion

        #region Additional Method

        #region setlue

        private void SetLueCCtCode(ref DXE.LookUpEdit Lue, string CCCode)
        {
            try
            {
                Sm.SetLue2(
                    ref Lue,
                    "Select D.CCtCode As Col1, D.CCtName As Col2 "+
                    "From TblCostCategory D Left Join TblCostCenter E "+
                    "On D.CCCode = E.CCCode Where E.CCCOde='" + CCCode + "' Order By D.CCtName",
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

       
        #endregion

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event


        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }


        private void LueCCtCode_Leave(object sender, EventArgs e)
        {
            if (LueCCtCode.Visible && fAccept && fCell.ColIndex == 5)
            {
                if (Sm.GetLue(LueCCtCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 4].Value =
                    Grd1.Cells[fCell.RowIndex, 5].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueCCtCode);
                    Grd1.Cells[fCell.RowIndex, 5].Value = LueCCtCode.GetColumnValue("Col2");
                }
                LueCCtCode.Visible = false;
            }
        }

        private void LueCCtCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCtCode, new Sm.RefreshLue2(SetLueCCtCode), Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 0));

        }

        #endregion

        #endregion
       
    }
}
