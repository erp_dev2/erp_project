﻿#region Update
/*
    08/08/2019 [WED] tidak buat SOContractRevision
    09/08/2019 [WED] tidak bisa hapus row
    12/08/2019 [WED] tambah kolom Price, Estimated Amount, Settled Amount
    12/08/2019 [WED] Kolom Bobot jadi ReadOnly berdasarkan parameter IsWBSBobotColumnAutoCompute
    30/08/2019 [WED] Amount SO Contract yang muncul ambil dari SO Contract Revision terbaru
    11/09/2019 [WED] total bobot yang nyentuh 99.5 akan dibulatkan ke 100%
    23/09/2019 [WED] parameter baru untuk kalkulasi estimated price di WBS, WBSFormula
    01/10/2019 [WED/YK] ukuran layar WBS dan Resource di per lebar
    16/10/2019 [DITA/YK] memasukkan Resource dari PRJI ke PRJI Revision
    23/10/2019 [WED/YK] approval
    20/11/2019 [DITA/YK] tambah WBS 2
    19/02/2020 [WED/IMS] tidak bisa insert dan save, berdasarkan parameter IsPRJIRevisionOnlyForShow
    02/07/2020 [WED/YK] resource bisa diedit, nilai nya minimal sebesar RBP nya (jika ada RBP nya) berdasarkan parameter IsPRJIRevisionResourceAllowedToEditAfterRBP
    03/07/2020 [DITA/YK] PRJI Revision menampilkan data sesuai history nya, bukan sesuai prji dengan revision terakhir
    15/07/2020 [DITA/YK] NILAI CONTRACT AMOUNT & PRODUCTION AMOUNT DI RESOURCE PADA PRJI REV ambil dari amt2 di socrevision
    21/07/2020 [WED/YK] validasi IsResourceTotalInvalid() di comment, ShowProjectImplementationDtl2() dibenerin query nya yg Show DocNoRev
    22/07/2020 [IBL/YK] total di tab resource tidak boleh melebihi amount tanpa ppn
    23/07/2020 [WED/YK] sequence nya ikut ditarik dari PRJI asli nya, tapi tidak bisa diedit dan tidak ditampilkan 
    02/12/2020 [DITA/IMS] hide tab resource berdasarkan parameter : IsPRJIRevisionHideTabResource
    24/11/2021 [YOG/VIR] Penambahan jumlah angka di belakang koma (decimal)
    21/11/2021 [RIS/YK] Menambahkan kolom price, estimated amount, settled amount dan field total price berdasarkan parameter IsWBS2SameAsWBS
    22/12/2021 [RIS/YK] Menambah Approval Remark berdasarkan param IsPSModuleShowApproverRemarkInfo
    13/01/2022 [RIS/YK] WBS 2 menu Project Implementaton Revision tidak bisa ditarik di Project Delivery
    20/04/2022 [BRI/PRODUCT] nilai PPN mengikuti PRJI
    25/04/2022 [DITA/YK] label pph mengikuti PRJI
    19/07/2022 [TYO/PRODUCT] Menambah Tab Control Approval Information
    10/08/2022 [VIN/YK] Bug saat save settle dt 
    12/08/2022 [VIN/YK] Bug save wbs2 kolom null 
    25/08/2022 [VIN/YK] Bug save wbs
    02/03/2023 [BRI/YK] Bug pada tab resource ketika klik kolom yang abu2, dan kita ga sengaja menekan tombol keyboard apapun muncul warning
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementationRevision : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
           mCity = string.Empty, mCnt = string.Empty, mSADNo = string.Empty, mPPNCode = string.Empty, mPPhCode = string.Empty,
           mListItCtCodeForResource = string.Empty;
        private string mWBSFormula = string.Empty,
            mItGrpCodeForRemuneration = string.Empty,
            mItGrpCodeForDirectCost = string.Empty,
            mPPN10ForProjectResourceShowZero = string.Empty,
            mProjectResourceCode = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsCustomerItemNameMandatory = false,
            mIsSOUseARDPValidated =false;
        private bool 
            IsInsert = false, 
            mIsWBSBobotColumnAutoCompute = false,
            mIsProjectImplementationDisplayTotalResource = false,
            mIsProjectImplementationApprovalBySiteMandatory = false,
            mIsPRJIUseWBS2 = false,
            mIsPRJIRevisionOnlyForShow = false,
            mIsPRJIRevisionResourceAllowedToEditAfterRBP = false,
            mIsPRJIRevisionHideTabResource = false,
            mIsPSModuleShowApproverRemarkInfo = false,
            mIsWBS2SameAsWBS = false;
        internal string 
            mIsSoUseDefaultPrintout,
            mGenerateCustomerCOAFormat = string.Empty;
        internal int mNumberOfSalesUomCode = 1;
        internal FrmProjectImplementationRevisionFind FrmFind;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmProjectImplementationRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProjectImplementationRevision");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetNumberOfSalesUomCode();


                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[6];
                SetGrd8();

                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[5];
                SetGrd7();

                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[4];
                SetGrd6();

                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2];
                Sl.SetLueProjectStageCode(ref LueStageCode);
                Sl.SetLueProjectTaskCode(ref LueTaskCode);
                LueStageCode.Visible = false;
                LueTaskCode.Visible = false;
                SetGrd5();

                //TcOutgoingPayment.SelectTab("TpWBS2"); //TcOutgoingPayment.TabPages[3];// TpWBS2;
                //TcOutgoingPayment.SelectedTabPage("TpWBS2");
                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3];
                Sl.SetLueProjectStageCode(ref LueStageCode2);
                Sl.SetLueProjectTaskCode(ref LueTaskCode2);
                LueStageCode2.Visible = false;
                LueTaskCode2.Visible = false;
                SetGrd9();

                if (!mIsPRJIUseWBS2) TcOutgoingPayment.TabPages.Remove(TpWBS2);
                if (mIsPRJIRevisionHideTabResource) TcOutgoingPayment.TabPages.Remove(TpResource);

                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[0];
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueStatus(ref SOCLueStatus);

                TcOutgoingPayment.TabPages.Remove(TpBOQ);
                TcOutgoingPayment.TabPages.Remove(TpItem);

                if (!mIsProjectImplementationDisplayTotalResource)
                {
                    LblTotalResource.Visible = TxtTotalResource.Visible = false;
                }

                if (!mIsPSModuleShowApproverRemarkInfo)
                {
                    label53.Visible = MeeApprovalRemark.Visible = false;
                    label30.Top -= 5;
                    label24.Top = TxtSOCDocNo.Top -= 5; label2.Top = SOCDteDocDt.Top -= 5;
                    label12.Top = SOCLueStatus.Top -= 5; label27.Top = SOCMeeCancelReason.Top -= 5; SOCChkCancelInd.Top -= 5;
                    label20.Top = TxtLocalDocNo.Top -= 5; label3.Top = LueCtCode.Top -= 5; label7.Top = TxtBOQDocNo.Top -= 5;
                    label4.Top = TxtCustomerContactperson.Top -= 5;
                }

                if (!mIsWBS2SameAsWBS)
                {
                    label54.Visible = TxtTotalPrice2.Visible = false;
                }

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {
            #region Grid 3 - List of Item

            Grd3.Cols.Count = 30;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "",
                    "Agent Code",
                    "Agent",
                    "Item's"+Environment.NewLine+"Code",
                    "",

                    //6-10
                    "Item's Name",
                    "Packaging",
                    "Quantity"+Environment.NewLine+"Packaging",
                    "Quantity",
                    "UoM",

                    //11-15
                    "Price"+Environment.NewLine+"(Price List)",
                    "Discount"+Environment.NewLine+"%",
                    "Discount"+Environment.NewLine+"Amount",
                    "Price After"+Environment.NewLine+"Discount",                        
                    "Promo"+Environment.NewLine+"%",

                    //16-20
                    "Price"+Environment.NewLine+"Before Tax",
                    "Tax"+Environment.NewLine+"%",
                    "Tax"+Environment.NewLine+"Amount",
                    "Price"+Environment.NewLine+"After Tax",                        
                    "Total",

                    //21-25
                    "Delivery"+Environment.NewLine+"Date",
                    "Remark",
                    "CtQtDNo",
                    "Specification",
                    "Customer's"+Environment.NewLine+"Item Code",

                    //26-29
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Volume",
                    "Total Volume",
                    "Uom"+Environment.NewLine+"Volume"
                },
                new int[] 
                {
                    //0
                    40,

                    //1-5
                    20, 0, 180, 80, 20, 
                    
                    //6-10
                    200, 100, 100, 80, 80, 
                    
                    //11-15
                    100, 80, 100, 100, 80,
                    
                    //16-20
                    100, 80, 100, 100, 120,   
                    
                    //21-25
                    100, 400, 0, 120, 120,

                    //26-29
                    250, 100, 100, 100
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 1, 5 });
            Sm.GrdFormatDate(Grd3, new int[] { 21 });
            Sm.GrdFormatDec(Grd3, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            Grd3.Cols[24].Move(7);
            Grd3.Cols[26].Move(7);
            Grd3.Cols[25].Move(7);
            Grd3.Cols[27].Move(14);
            Grd3.Cols[28].Move(15);
            Grd3.Cols[29].Move(16);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 3, 4, 5, 12, 14, 15, 17, 18, 23, 24, 25, 27, 28, 29 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd3, new int[] { 26 });

            #endregion

            #region Grid 1 - ARDP & VR

            Grd1.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "AR Downpayment#", 
                        
                        //1-5
                        "Date",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        "Voucher Request#",

                        //6-7
                        "Voucher#",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        80, 80, 100, 200, 150, 
                        
                        //6-7
                        150, 250 
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });

            #endregion

            #region Grid 2 - List of Item BOQ

            Grd2.Cols.Count = 11;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "BOQ Document", 
                        
                        //1-5
                        "BOQ Date",
                        "LOP Document",
                        "Project",
                        "BOQ Item Code",
                        "BOQ Item Name",
                        //6-10
                        "BOQ Amount",
                        "Amount",
                        "Allow",
                        "Amt",
                        "parentInd"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        120, 150, 250, 100, 250,
                        //6-8
                        150, 150, 80, 150, 80
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 7, 9 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10 });

            #endregion            
        }

        private void SetGrd5()
        {
            #region Grid 5 - WBS

            Grd5.Cols.Count = 15;
            Grd5.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd5, new string[] 
                { 
                    "StageCode", 
                    "Stage", "TaskCode", "Task", "Plan Start Date", "Plan End Date",
                    "Bobot", "Percentage", "Settled",  "Remark", "SettleDt",
                    "InvoiceInd", "Price", "Estimated Amount", "Settled Amount"
                },
                new int[] 
                { 
                    0, 
                    200, 0, 200, 100, 100,
                    100, 100, 100, 100, 100,
                    100, 100, 120, 120
                }
            );
            Sm.GrdFormatDec(Grd5, new int[] { 6, 7, 12, 13, 14 }, 8);
            Sm.GrdColCheck(Grd5, new int[] { 8 });
            Sm.GrdFormatDate(Grd5, new int[] { 4, 5, 10 });
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 2, 7, 8, 9, 10, 11, 13, 14 });
            Sm.GrdColInvisible(Grd5, new int[] { 0, 2, 10, 11 });
            Grd5.Cols[12].Move(6);

            if (mIsWBSBobotColumnAutoCompute)
                Sm.GrdColReadOnly(Grd5, new int[] { 6 });

            #endregion
        }

        private void SetGrd6()
        {
            #region Grid 6 - Resource

            Grd6.Cols.Count = 17;
            Grd6.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd6, new string[] 
                { 
                    "",
                    "ResourceItCode", "Resource", "Group", "Remark", "Quantity 1"+Environment.NewLine+"(Unit)", 
                    "Quantity 2"+Environment.NewLine+"(Time)", "Unit Price", "Tax", "Total Without Tax", "Total Tax", 
                    "Total With Tax", "Group It Code", "", "Detail#", "RBP Amount", 
                    "Sequence"
                },
                new int[] 
                { 
                    20, 
                    0, 200, 200, 200, 150, 
                    150, 180, 180, 200, 200, 
                    200,150, 20, 120, 0,
                    0
                }
            );
            Sm.GrdFormatDec(Grd6, new int[] { 7, 8, 9, 10, 11, 15 }, 0);
            Sm.GrdFormatDec(Grd6, new int[] { 5, 6 }, 8);
            Sm.GrdColButton(Grd6, new int[] { 0, 13 });
            Sm.GrdColReadOnly(Grd6, new int[] { 1, 2, 3, 9, 10, 11, 14, 15, 16 });
            Sm.GrdColInvisible(Grd6, new int[] { 12, 13, 15, 16 });

            #endregion
        }

        private void SetGrd7()
        {
            #region Grid 7 - Issue

            Grd7.Cols.Count = 2;
            Grd7.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd7, new string[] 
                { 
                    "Remark",
                    "PIC"
                },
                new int[] 
                { 
                    200, 
                    200
                }
            );

            #endregion
        }

        private void SetGrd8()
        {
            #region Grid 8 - Document

            Grd8.Cols.Count = 5;
            Grd8.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd8, new string[] 
                { 
                    "",
                    "DocCode", "Document", "Date", "Remark"
                },
                new int[] 
                { 
                    20, 
                    0, 200, 100, 200
                }
            );
            Sm.GrdColButton(Grd8, new int[] { 0 });
            Sm.GrdColReadOnly(Grd8, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd8, new int[] { 3 });

            #endregion
        }

        private void SetGrd9()
        {
            #region Grid 9 - WBS 2

            Grd9.Cols.Count = 15;
            Grd9.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd9, new string[] 
                { 
                    //0
                    "StageCode", 

                    //1-5
                    "Stage", "TaskCode", "Task", "Plan Start Date", "Plan End Date",

                    //6-10
                    "Bobot", "Percentage", "Settled",  "Remark", "SettleDt",

                    //11
                    "InvoiceInd", "Price", "Estimated Amount", "Settled Amount"
                },
                new int[] 
                { 
                    0, 
                    200, 0, 200, 100, 100,
                    100, 100, 100, 100, 100,
                    100, 100, 120, 120
                }
            );
            Sm.GrdFormatDec(Grd9, new int[] { 6, 7, 12, 13, 14}, 8);
            Sm.GrdColCheck(Grd9, new int[] { 8 });
            Sm.GrdFormatDate(Grd9, new int[] { 4, 5, 10 });
            Sm.GrdColReadOnly(Grd9, new int[] { 0, 2, 7, 8, 9, 10, 11, 13, 14});
            Sm.GrdColInvisible(Grd9, new int[] { 0, 2, 10, 11 });
            Grd9.Cols[12].Move(6);

            if (!mIsWBS2SameAsWBS)
            {
                Sm.GrdColInvisible(Grd9, new int[] { 12, 13, 14 });
            }

            if (mIsWBSBobotColumnAutoCompute)
                Sm.GrdColReadOnly(Grd9, new int[] { 6 });

            #endregion

            #region Grid10 - Approval Information
            Grd10.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd10,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd10, new int[] { 3 });
            Sm.GrdColReadOnly(Grd10, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }


        #endregion

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd3, new int[] { 3, 4, 5, 12, 14, 15, 17, 18, 24, 25, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtTotal, TxtRemunerationCost, TxtDirectCost, TxtContractAmt, TxtPPhAmt, TxtPPNAmt, TxtExclPPN, TxtNetCash, TxtPPNPercentage, TxtPPNPPhPercentage,
                        TxtTotalBobot, TxtTotalPersentase, TxtTotalPrice
                    }, true);
                    SOCChkCancelInd.Properties.ReadOnly = true;
                    BtnProjectImplementationDocNo.Enabled = false;
                    BtnImportWBS.Enabled = BtnImportResource.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 7 });
                    Grd5.ReadOnly = Grd6.ReadOnly = Grd7.ReadOnly = Grd8.ReadOnly = Grd9.ReadOnly=  true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt
                    }, false);
                    BtnProjectImplementationDocNo.Enabled = true;
                    TxtProjectImplementationDocNo.Focus();
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            mPPNCode = string.Empty;
            mPPhCode = string.Empty;
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            LblPPN.Text = "10 %";
            LblPPh.Text = "4 %";
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, SOCDteDocDt, TxtBOQDocNo, SOCLueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, SOCMeeCancelReason,
                  LueCtCode, TxtCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc, TxtStatus, DteDocDt, TxtProjectImplementationDocNo,
                  TxtSOCDocNo, MeeApprovalRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { 
            TxtAmt, TxtTotalResource, TxtTotalBobot, TxtTotalPersentase, TxtTotalPrice, TxtTotalPrice2,
            TxtTotal, TxtRemunerationCost, TxtDirectCost, TxtTotalPersentase2, TxtTotalAchievement, TxtTotalBobot2,
            TxtContractAmt, TxtPPhAmt, TxtPPNAmt, TxtExclPPN,
            TxtNetCash, TxtPPNPercentage, TxtPPNPPhPercentage,}, 0);
            SOCChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, 20 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
            Grd10.Rows.Clear();

            ClearGrdProjectImplementation();
        }

        private void ClearGrdProjectImplementation()
        {
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 6, 7 });
            Sm.ClearGrd(Grd6, true);
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 5, 6, 7, 8, 9, 10, 11, 15 });
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            Sm.ClearGrd(Grd9, true);
            Sm.SetGrdNumValueZero(ref Grd9, 0, new int[] { 6, 7, 12, 13, 14 });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotalResource, TxtTotalBobot, TxtTotalPersentase, TxtTotalPrice, TxtTotalBobot2, TxtTotalPrice2, TxtTotalPersentase2, TxtTotalAchievement }, 0);
        }

        private void ClearData2()
        {
            mSADNo = string.Empty;
            mCity = string.Empty;
            mCnt = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone, 
                TxtFax, TxtEmail, TxtMobile
            });
        }

        private void ClearData3()
        {
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, SOCDteDocDt, TxtBOQDocNo, SOCLueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd, 
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, SOCMeeCancelReason,
                  LueCtCode, TxtCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            SOCChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion
    
        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectImplementationRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            if (!mIsPRJIRevisionOnlyForShow)
            {
                try
                {
                    ClearData();
                    SetFormControl(mState.Insert);
                    Sm.SetDteCurrentDate(DteDocDt);
                    TxtStatus.EditValue = "Outstanding";
                    IsInsert = true;
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (!mIsPRJIRevisionOnlyForShow)
            {
                try
                {
                    if (TxtDocNo.Text.Length == 0)
                        InsertData();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (!mIsPRJIRevisionOnlyForShow)
            {
                if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
                ClearData();
                SetFormControl(mState.View);
            }
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (SOCChkCancelInd.Checked == false)
            {
                ParPrint();
            }
        }

        #endregion

        #region Grid Methods

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
            {
                InputAmount(e.RowIndex, e.ColIndex);
                ComputeTotalBOQ();
                ComputeItem();
            }
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
                {
                    if (Sm.GetGrdStr(Grd5, e.RowIndex, 0).Length > 0)
                    {
                        decimal mEstimatedAmt = 0m;
                        if (mWBSFormula == "1") mEstimatedAmt = Sm.GetGrdDec(Grd5, e.RowIndex, 12);

                        Grd5.Cells[e.RowIndex, 13].Value = mEstimatedAmt;
                    }
                }
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                //Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                //GrdRemoveRow(Grd5, e, BtnSave);
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 3, 4, 5, 6 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd5, LueStageCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 3) LueRequestEdit(Grd5, LueTaskCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 4) Sm.DteRequestEdit(Grd5, DtePlanStartDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd5, DtePlanEndDt, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd5, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 12, 13, 14 });
                }
            }
        }


        private void Grd9_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd9.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 3, 4, 5, 6 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd9, LueStageCode2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 3) LueRequestEdit(Grd9, LueTaskCode2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 4) Sm.DteRequestEdit(Grd9, DtePlanStartDt2, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd9, DtePlanEndDt2, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd9, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd9, Grd9.Rows.Count - 1, new int[] { 6, 7, 12, 13, 14});
                }
            }
        }

        private void Grd9_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd9.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
                {
                    if (Sm.GetGrdStr(Grd9, e.RowIndex, 0).Length > 0)
                    {
                        decimal mEstimatedAmt = 0m;
                        if (mWBSFormula == "1") mEstimatedAmt = Sm.GetGrdDec(Grd9, e.RowIndex, 12);

                        Grd9.Cells[e.RowIndex, 13].Value = mEstimatedAmt;
                    }
                }
            }
        }

        private void Grd9_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd9.ReadOnly)
            {
                //Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                //GrdRemoveRow(Grd5, e, BtnSave);
            }
        }

       
        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd6, new int[] { 5, 6, 7, 8, 9, 10, 11 }, e);
                if (Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8, 9, 10, 11 }, e.ColIndex))
                {
                    decimal mQty1 = 0m, mQty2 = 0m, mUPrice = 0m, mTax, mTotalWithoutTax = 0m, mTotalTax = 0m, mAmt = 0m;
                    mQty1 = Sm.GetGrdDec(Grd6, e.RowIndex, 5);
                    mQty2 = Sm.GetGrdDec(Grd6, e.RowIndex, 6);
                    mUPrice = Sm.GetGrdDec(Grd6, e.RowIndex, 7);
                    mTax = Sm.GetGrdDec(Grd6, e.RowIndex, 8);

                    mTotalWithoutTax = mQty1 * mQty2 * mUPrice;
                    mTotalTax = mQty1 * mQty2 * mTax;
                    mAmt = mTotalWithoutTax + mTotalTax;

                    Grd6.Cells[e.RowIndex, 9].Value = mTotalWithoutTax;
                    Grd6.Cells[e.RowIndex, 10].Value = mTotalTax;
                    Grd6.Cells[e.RowIndex, 11].Value = mAmt;
                    ComputeTotalResource();
                    ComputeRemunerationCost();
                    ComputeDirectCost();
                    ComputeDetailAmt();
                    ComputeTotal();

                }
            }
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                Sm.FormShowDialog(new FrmProjectImplementationRevisionDlg2(this));
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                //int Index = Grd6.SelectedRows[Grd6.SelectedRows.Count - 1].Index;
                //if (Sm.GetGrdStr(Grd6, Index, 14).Length == 0)
                //{
                //    Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
                //    Sm.GrdRemoveRow(Grd6, e, BtnSave);
                //}
                //ComputeTotalResource();
                //ComputeRemunerationCost();
                //ComputeDirectCost();
                //ComputeDetailAmt();
                //ComputeTotal();
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                //e.DoDefault = false;
                if (e.ColIndex == 0)
                {
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 3 });
                }
            }
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //Sm.GrdKeyDown(Grd7, e, BtnFind, BtnSave);
                //Sm.GrdRemoveRow(Grd7, e, BtnSave);
            }
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;
                if (Sm.IsGrdColSelected(new int[] { 0, 1 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd7, e.RowIndex);
                }
            }        
        }

        private void Grd8_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmProjectImplementationRevisionDlg3(this));
                }
            }
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //Sm.GrdKeyDown(Grd8, e, BtnFind, BtnSave);
                //Sm.GrdRemoveRow(Grd8, e, BtnSave);
            }
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;
                if (Sm.IsGrdColSelected(new int[] { 0, 3 }, e.ColIndex))
                {
                    if (e.ColIndex == 3) Sm.DteRequestEdit(Grd8, DteDocDt2, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd8, e.RowIndex);
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            ComputeBobot();
            ComputeBobotPercentage();
            ComputeRBPAmt();
            if (mIsWBS2SameAsWBS)
            {
                ComputeBobot2();
                ComputeBobotPercentage2();

            }
            //ComputeEstimatedAmt();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string mIndexDocNo = string.Empty;
            string DocNo = string.Concat(TxtProjectImplementationDocNo.Text, "/", GenerateDocNo());
            string SOCRDocNo = string.Concat(TxtSOCDocNo.Text/*, "/", GenerateDocNo2()*/);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProjectImplementationRevisionHdr(DocNo, SOCRDocNo));

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    cml.Add(SaveProjectImplementationRevisionDtl(DocNo, Row));

            if (!mIsPRJIRevisionHideTabResource)
            {
                for (int Row = 0; Row < Grd6.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0)
                        cml.Add(SaveProjectImplementationRevisionDtl2(DocNo, Row));
            }

            if (Grd7.Rows.Count >= 1)
                for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd7, Row, 0).Length > 0)
                        cml.Add(SaveProjectImplementationRevisionDtl3(DocNo, Row));

            if (Grd8.Rows.Count >= 1)
                for (int Row = 0; Row < Grd8.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd8, Row, 1).Length > 0)
                        cml.Add(SaveProjectImplementationRevisionDtl4(DocNo, Row));

            if (mIsWBS2SameAsWBS)
            {
                if (Grd9.Rows.Count >= 1)
                    for (int Row = 0; Row < Grd9.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd9, Row, 1).Length > 0)
                            cml.Add(SaveProjectImplementationRevisionDtl5(DocNo, Row));
            }
            

            cml.Add(UpdateProjectImplementationHdr(TxtProjectImplementationDocNo.Text, DocNo));
            cml.Add(DeleteProjectImplementationDtl(TxtProjectImplementationDocNo.Text, DocNo));

            cml.Add(DuplicateProjectImplementationDtl(DocNo));

            cml.Add(UpdateAchievement(TxtProjectImplementationDocNo.Text, DocNo));

            //cml.Add(SaveSOContractRevisionHdr(SOCRDocNo));
            //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) cml.Add(SaveSOContractRevisionDtl(SOCRDocNo, Row));
            //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveSOContractRevisionDtl2(SOCRDocNo, Row));

            //cml.Add(UpdatePrevAmt(SOCRDocNo));

            //cml.Add(UpdateSOCAmt(SOCRDocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtProjectImplementationDocNo, "Project Implementation#", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid("I") ||
                IsApprovalOnGoing() ||
                IsBobotPercentageNotValid() ||
                IsResourceTotalInvalid() ||
                IsTotalCostInvalid();
        }
        
        private bool IsTotalCostInvalid()
        {
            if (Sm.GetDecValue(TxtTotal.Text) > Sm.GetDecValue(TxtAmt.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Total cost should not be bigger than amount.");
                return true;
            }
            return false;
        }

        private bool IsResourceTotalInvalid()
        {
            if (!mIsPRJIRevisionResourceAllowedToEditAfterRBP) return false;

            //for (int i = 0; i < Grd6.Rows.Count; ++i)
            //{
            //    if (Sm.GetGrdStr(Grd6, i, 14).Length > 0)
            //    {
            //        if (Sm.GetGrdDec(Grd6, i, 11) < Sm.GetGrdDec(Grd6, i, 15)) // revision amount < rbp amount
            //        {
            //            TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2];
            //            var m = new StringBuilder();

            //            m.AppendLine("Resource              : " + Sm.GetGrdStr(Grd6, i, 2));
            //            m.AppendLine("Revision Amount : " + Sm.FormatNum(Sm.GetGrdDec(Grd6, i, 11), 0));
            //            m.AppendLine("RBP Amount         : " + Sm.FormatNum(Sm.GetGrdDec(Grd6, i, 15), 0));
            //            m.AppendLine(Environment.NewLine);
            //            m.AppendLine("Revision Amount at least should be equal or more than RBP Amount.");

            //            Sm.StdMsg(mMsgType.Warning, m.ToString());
            //            Sm.FocusGrd(Grd6, i, 11);
            //            return true;
            //        }
            //    }
            //}

            return false;
        }

        private bool IsApprovalOnGoing()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct A.DocNo ");
            SQL.AppendLine("From TblProjectImplementationRevisionHdr A ");
            SQL.AppendLine("Inner Join TblDocApproval B On A.DocNo = B.DocNo And A.Status = 'O' "); // status document masih O, tapi ada level yang sudah approved
            SQL.AppendLine("Where A.PRJIDocNo = @Param ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtProjectImplementationDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "You can't create this revision. Another revision is still on approval process. Make sure this document#" + Sm.GetValue(SQL.ToString(), TxtProjectImplementationDocNo.Text) + " is approved by all level, or cancelled via document approval.");
                return true;
            }

            return false;
        }

        private bool IsBobotPercentageNotValid()
        {
            //ComputeBobotPercentage();

            //decimal mPercent = 0m;
            //bool mFlag = false;
            //for (int i = 0; i < Grd5.Rows.Count; i++)
            //{
            //    if (Sm.GetGrdStr(Grd5, i, 1).Length > 0)
            //        mPercent += Sm.GetGrdDec(Grd5, i, 7);
            //}

            //if (mPercent > 100)
            //{
            //    for (int j = Grd5.Rows.Count - 1; j >= 0; j--)
            //    {
            //        if (Sm.GetGrdStr(Grd5, j, 1).Length > 0)
            //        {
            //            Grd5.Cells[j, 7].Value = Sm.GetGrdDec(Grd5, j, 7) - 0.01m;
            //            Sm.StdMsg(mMsgType.Info, "Bobot adjusted. Please save again.");
            //            mFlag = true;
            //            break;
            //        }
            //    }
            //}

            //return mFlag;
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd5.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 WBS.");
                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2];
                return true;
            }
            if (!mIsPRJIRevisionHideTabResource)
            {
                if (Grd6.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Resource.");
                    TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3];
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid(string mStateInd)
        {
            if (mStateInd == "I")
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 1, false, "Stage is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2]; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 3, false, "Task is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2]; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 4, false, "Plan Start Date is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2]; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 5, false, "Plan End Date is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2]; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, Row, 6, true, "Bobot could not be empty or zero.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2]; return true; }

                    if ((Sm.GetGrdDec(Grd5, Row, 13) != Sm.GetGrdDec(Grd5, Row, 14)) && Sm.GetGrdBool(Grd5, Row, 8))
                    {
                        decimal mEstAmt = Sm.GetGrdDec(Grd5, Row, 13);
                        decimal mSettledAmt = Sm.GetGrdDec(Grd5, Row, 14);
                        Sm.StdMsg(mMsgType.Warning, "Estimated Amount should be equal to Settled Amt");
                        TcOutgoingPayment.SelectedTabPage = TpWBS;
                        return true;
                    }
                }

                for (int r1 = 0; r1 < Grd5.Rows.Count - 1; r1++)
                {
                    for (int r2 = (r1 + 1); r2 < Grd5.Rows.Count; r2++)
                    {
                        if ((Sm.GetGrdStr(Grd5, r1, 0) == Sm.GetGrdStr(Grd5, r2, 0))
                            &&
                            (Sm.GetGrdStr(Grd5, r1, 2) == Sm.GetGrdStr(Grd5, r2, 2))
                            )
                        {
                            Sm.StdMsg(mMsgType.Warning, "Duplicate task found : " + Sm.GetGrdStr(Grd5, r2, 3));
                            TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2];
                            Sm.FocusGrd(Grd5, r2, 3);
                            return true;
                        }
                    }
                }

                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareDtTm(Sm.Left(Sm.GetGrdDate(Grd5, Row, 4), 8), Sm.Left(Sm.GetGrdDate(Grd5, Row, 5), 8)) > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Plan end date could not be earlier than plan start date.");
                        TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[2];
                        Sm.FocusGrd(Grd5, Row, 5);
                        return true;
                    }
                }

                if (!mIsPRJIRevisionHideTabResource)
                {
                    for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    {
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 2, false, "Resource is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 5, false, "Quantity 1 could not be empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 6, false, "Quantity 2 could not be empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 7, false, "Unit Price could not be empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 8, false, "Tax could not be empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 9, false, "Total Without Tax could not be empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 10, false, "Total Tax could not be empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                        if (Sm.IsGrdValueEmpty(Grd6, Row, 11, false, "Total With Tax could not be empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3]; return true; }
                    }

                    for (int r1 = 0; r1 < Grd6.Rows.Count - 1; r1++)
                    {
                        for (int r2 = (r1 + 1); r2 < Grd6.Rows.Count; r2++)
                        {
                            if (Sm.GetGrdStr(Grd6, r1, 1) == Sm.GetGrdStr(Grd6, r2, 1))
                            {
                                Sm.StdMsg(mMsgType.Warning, "Duplicate resource found : " + Sm.GetGrdStr(Grd6, r2, 2));
                                TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[3];
                                Sm.FocusGrd(Grd6, r2, 2);
                                return true;
                            }
                        }
                    }
                }
            }

            for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd7, Row, 0, false, "Remark is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[4]; return true; }
                if (Sm.IsGrdValueEmpty(Grd7, Row, 1, false, "PIC is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[4]; return true; }
            }

            for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd8, Row, 2, false, "Document is empty.")) { TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[5]; return true; }
            }

            for (int r1 = 0; r1 < Grd8.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd8.Rows.Count; r2++)
                {
                    if (Sm.GetGrdStr(Grd8, r1, 1) == Sm.GetGrdStr(Grd8, r2, 1))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate document found : " + Sm.GetGrdStr(Grd8, r2, 2));
                        TcOutgoingPayment.SelectedTabPage = TcOutgoingPayment.TabPages[5];
                        Sm.FocusGrd(Grd8, r2, 2);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveProjectImplementationRevisionHdr(string DocNo, string SOCRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectImplementationRevisionHdr(DocNo, SOCRDocNo, PRJIDocNo, ");
            SQL.AppendLine("DocDt, Status, TotalResource, TotalPrice, TotalBobot, TotalPersentase, ");
            SQL.AppendLine("RemunerationAmt, DirectCostAmt, PPNAmt, ExclPPNAmt, PPhAmt, ExclPPNPPhAmt, ");
            SQL.AppendLine("ExclPPNPPhPercentage, ExclPPNPercentage, ");
            if (mIsWBS2SameAsWBS)
            {
                SQL.AppendLine("TotalPrice2, TotalBobot2, TotalPersentase2, TotalAchievement, ");
            }
            SQL.AppendLine("CreateDt, CreateBy) ");
            SQL.AppendLine("Values(@DocNo, @SOCRDocNo, @PRJIDocNo, ");
            SQL.AppendLine("@DocDt, 'O', @TotalResource, @TotalPrice, @TotalBobot, @TotalPersentase, ");
            SQL.AppendLine("@RemunerationAmt, @DirectCostAmt, @PPNAmt, @ExclPPNAmt, @PPhAmt, @ExclPPNPPhAmt, ");
            SQL.AppendLine("@ExclPPNPPhPercentage, @ExclPPNPercentage, ");
            if (mIsWBS2SameAsWBS)
            {
                SQL.AppendLine("@TotalPrice2, @TotalBobot2, @TotalPersentase2, @TotalAchievement, ");
            }
            SQL.AppendLine("CurrentDateTime(), @CreateBy); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ProjectImplementationRev' ");
            if (mIsProjectImplementationApprovalBySiteMandatory)
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And T.SiteCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select X5.SiteCode ");
                SQL.AppendLine("    From TblProjectImplementationHdr X1 ");
                SQL.AppendLine("    Inner Join TblSOContractRevisionHdr X2 On X1.SOContractDocNo = X2.DocNo And X1.DocNo = @PRJIDocNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr X3 On X2.SOCDocNo = X3.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr X4 On X3.BOQDocNo = X4.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr X5 On X4.LOPDocNo = X5.DocNo ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("; ");            

            SQL.AppendLine("Update TblProjectImplementationRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ProjectImplementationRev' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOCRDocNo", SOCRDocNo);
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtProjectImplementationDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@TotalResource", Decimal.Parse(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Decimal.Parse(TxtTotalPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot", Decimal.Parse(TxtTotalBobot.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPersentase", Decimal.Parse(TxtTotalPersentase.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetDecValue(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetDecValue(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@PPNAmt", Sm.GetDecValue(TxtPPNAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNAmt", Sm.GetDecValue(TxtExclPPN.Text));
            Sm.CmParam<Decimal>(ref cm, "@PPhAmt", Sm.GetDecValue(TxtPPhAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPPhAmt", Sm.GetDecValue(TxtNetCash.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPPhPercentage", Sm.GetDecValue(TxtPPNPPhPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPercentage", Sm.GetDecValue(TxtPPNPercentage.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice2", Decimal.Parse(TxtTotalPrice2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot2", Decimal.Parse(TxtTotalBobot2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPersentase2", Decimal.Parse(TxtTotalPersentase2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalAchievement", Decimal.Parse(TxtTotalAchievement.Text));

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, ");
            SQLDtl.AppendLine("Bobot, BobotPercentage, SettledInd, SettleDt, InvoicedInd, Amt, EstimatedAmt, SettledAmt, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, ");
            SQLDtl.AppendLine("@Bobot, @BobotPercentage, @SettledInd, @SettleDt, @InvoicedInd, @Amt, @EstimatedAmt, @SettledAmt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd5, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd5, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd5, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd5, Row, 7));
            Sm.CmParam<String>(ref cm, "@SettledInd", Sm.GetGrdBool(Grd5, Row, 8) ? "Y" : "N");
            Sm.CmParamDt(ref cm, "@SettleDt", Sm.GetGrdDate(Grd5, Row, 10));
            Sm.CmParam<String>(ref cm, "@InvoicedInd", Sm.GetGrdStr(Grd5, Row, 11).Length > 0 ? Sm.GetGrdStr(Grd5, Row, 11) : "N");
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@EstimatedAmt", Sm.GetGrdDec(Grd5, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@SettledAmt", Sm.GetGrdDec(Grd5, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl2(DocNo, DNo, Sequence, ResourceItCode, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @Sequence, @ResourceItCode, @Remark, @Qty1, @Qty2, @UPrice, @Tax, @TotalWithoutTax, @TotalTax, @Amt, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ResourceItCode", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd6, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd6, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd6, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd6, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", Sm.GetGrdDec(Grd6, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 11));
            Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd6, Row, 16));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl3(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl3(DocNo, DNo, Remark, PIC, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @Remark, @PIC, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd7, Row, 0));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetGrdStr(Grd7, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl4(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl4(DocNo, DNo, DocCode, Dt, Remark, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @DocCode, @Dt, @Remark, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetGrdStr(Grd8, Row, 1));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd8, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd8, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl5(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl5(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, ");
            SQLDtl.AppendLine("Bobot, BobotPercentage, SettledInd, SettleDt, InvoicedInd, Amt, EstimatedAmt, SettledAmt, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, ");
            SQLDtl.AppendLine("@Bobot, @BobotPercentage, @SettledInd, @SettleDt, @InvoicedInd, @Amt, @EstimatedAmt, @SettledAmt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd9, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd9, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd9, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd9, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd9, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd9, Row, 7));
            Sm.CmParam<String>(ref cm, "@SettledInd", Sm.GetGrdBool(Grd9, Row, 8) ? "Y" : "N");
            Sm.CmParamDt(ref cm, "@SettleDt", Sm.GetGrdDate(Grd9, Row, 10));
            Sm.CmParam<String>(ref cm, "@InvoicedInd", Sm.GetGrdStr(Grd9, Row, 11).Length > 0 ? Sm.GetGrdStr(Grd9, Row, 11) : "N");
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd9, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@EstimatedAmt", Sm.GetGrdDec(Grd9, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@SettledAmt", Sm.GetGrdDec(Grd9, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateProjectImplementationHdr(string PRJIDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationHdr Set ");
            SQL.AppendLine("    SOContractDocNo = @SOContractDocNo, ");
            SQL.AppendLine("    TotalBobot = @TotalBobot, TotalPrice = @TotalPrice, TotalPersentase = @TotalPersentase, ");
            SQL.AppendLine("    TotalResource = @TotalResource, ");
            SQL.AppendLine("    RemunerationAmt=@RemunerationAmt, DirectCostAmt=@DirectCostAmt, PPNAmt=@PPNAmt, ExclPPNAmt=@ExclPPNAmt, ");
            SQL.AppendLine("    PPhAmt=@PPhAmt, ExclPPNPPhAmt=@ExclPPNPPhAmt, ExclPPNPPhPercentage=@ExclPPNPPhPercentage, ExclPPNPercentage=@ExclPPNPercentage, ");
            SQL.AppendLine("    LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @PRJIDocNo ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblProjectImplementationRevisionHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@TotalResource", Decimal.Parse(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Decimal.Parse(TxtTotalPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot", Decimal.Parse(TxtTotalBobot.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPersentase", Decimal.Parse(TxtTotalPersentase.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetDecValue(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetDecValue(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@PPNAmt", Sm.GetDecValue(TxtPPNAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNAmt", Sm.GetDecValue(TxtExclPPN.Text));
            Sm.CmParam<Decimal>(ref cm, "@PPhAmt", Sm.GetDecValue(TxtPPhAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPPhAmt", Sm.GetDecValue(TxtNetCash.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPPhPercentage", Sm.GetDecValue(TxtPPNPPhPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPercentage", Sm.GetDecValue(TxtPPNPercentage.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateAchievement(string PRJIDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo, Sum(BobotPercentage) BobotPercentage ");
            SQL.AppendLine("    From TblProjectImplementationDtl ");
            SQL.AppendLine("    Where DocNo = @PRJIDocNo ");
            SQL.AppendLine("    And SettledInd = 'Y' ");
            SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Set A.Achievement = B.BobotPercentage ");
            SQL.AppendLine("Where A.Achievement > 0 ");
            SQL.AppendLine("And A.DocNo = @PRJIDocNo ");
            SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationRevisionHdr Where DocNo = @DocNo And Status = 'A'); ");

            SQL.AppendLine("Update TblProjectImplementationHdr ");
            SQL.AppendLine("Set Achievement = 100 ");
            SQL.AppendLine("Where DocNo = @PRJIDocNo ");
            SQL.AppendLine("And Achievement > 99.9 ");
            SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationRevisionHdr Where DocNo = @DocNo And Status = 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        private MySqlCommand DeleteProjectImplementationDtl(string PRJIDocNo, string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Delete From TblProjectImplementationDtl Where DocNo = @PRJIDocNo And Exists(Select 1 From TblProjectImplementationRevisionHdr Where DocNo = @DocNo And Status = 'A'); ");
            SQLDtl.AppendLine("Delete From TblProjectImplementationDtl2 Where DocNo = @PRJIDocNo And Exists(Select 1 From TblProjectImplementationRevisionHdr Where DocNo = @DocNo And Status = 'A'); ");
            SQLDtl.AppendLine("Delete From TblProjectImplementationDtl3 Where DocNo = @PRJIDocNo And Exists(Select 1 From TblProjectImplementationRevisionHdr Where DocNo = @DocNo And Status = 'A'); ");
            SQLDtl.AppendLine("Delete From TblProjectImplementationDtl4 Where DocNo = @PRJIDocNo And Exists(Select 1 From TblProjectImplementationRevisionHdr Where DocNo = @DocNo And Status = 'A'); ");
            SQLDtl.AppendLine("Delete From TblProjectImplementationDtl5 Where DocNo = @PRJIDocNo And Exists(Select 1 From TblProjectImplementationRevisionHdr Where DocNo = @DocNo And Status = 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", PRJIDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        private MySqlCommand DuplicateProjectImplementationDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectImplementationDtl(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, Bobot, BobotPercentage, SettledInd, SettleDt, InvoicedInd, Amt, EstimatedAmt, SettledAmt, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select B.PRJIDocNo, A.DNo, A.StageCode, A.TaskCode, A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd, A.SettleDt, A.InvoicedInd, A.Amt, A.EstimatedAmt, A.SettledAmt, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblProjectImplementationRevisionDtl A ");
            SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr B On A.DocNo = B.DocNo And A.DocNo = @DocNo And B.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblProjectImplementationDtl2(DocNo, DNo, Sequence, ResourceItCode, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select B.PRJIDocNo, A.DNo, A.Sequence, A.ResourceItCode, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblProjectImplementationRevisionDtl2 A ");
            SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr B On A.DocNo = B.DocNo And A.DocNo = @DocNo And B.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblProjectImplementationDtl3(DocNo, DNo, Remark, PIC, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select B.PRJIDocNo, A.DNo, A.Remark, A.PIC, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblProjectImplementationRevisionDtl3 A ");
            SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr B On A.DocNo = B.DocNo And A.DocNo = @DocNo And B.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblProjectImplementationDtl4(DocNo, DNo, DocCode, Dt, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select B.PRJIDocNo, A.DNo, A.DocCode, A.Dt, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblProjectImplementationRevisionDtl4 A ");
            SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr B On A.DocNo = B.DocNo And A.DocNo = @DocNo And B.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblProjectImplementationDtl5(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, Amt, Bobot, BobotPercentage, SettledInd, SettleDt,  EstimatedAmt, SettledAmt, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select B.PRJIDocNo, A.DNo, A.StageCode, A.TaskCode, A.PlanStartDt, A.PlanEndDt, A.Amt, A.Bobot, A.BobotPercentage, A.SettledInd, A.SettleDt, A.EstimatedAmt, A.SettledAmt, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblProjectImplementationRevisionDtl5 A ");
            SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr B On A.DocNo = B.DocNo And A.DocNo = @DocNo And B.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        //private MySqlCommand SaveSOContractRevisionHdr(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSOContractRevisionHdr(DocNo, SOCDocNo, DocDt, Status, Amt, ");
        //    SQL.AppendLine("CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values ");
        //    SQL.AppendLine("(@DocNo, @SOCDocNo, @DocDt, 'O', @Amt, ");
        //    SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

        //    if (IsNeedApproval())
        //    {
        //        SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
        //        SQL.AppendLine("From TblDocApprovalSetting ");
        //        SQL.AppendLine("Where DocType='SOCRev'; ");
        //    }

        //    SQL.AppendLine("Update TblSOContractRevisionHdr Set Status='A' ");
        //    SQL.AppendLine("Where DocNo=@DocNo ");
        //    SQL.AppendLine("And Not Exists( ");
        //    SQL.AppendLine("    Select DocNo From TblDocApproval ");
        //    SQL.AppendLine("    Where DocType='SOCRev' And DocNo=@DocNo ");
        //    SQL.AppendLine("); ");
                
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveSOContractRevisionDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSOContractRevisionDtl(DocNo, SOCDocNo, DNo, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveSOContractRevisionDtl2(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSOContractRevisionDtl2(DocNo, SOCDocNo, DNo, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @Amt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand UpdatePrevAmt(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblSOContractRevisionHdr A ");
        //    SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo And A.DocNo = @DocNo ");
        //    SQL.AppendLine("Set A.PrevAmt = B.Amt; ");
            
        //    SQL.AppendLine("Update TblSOContractRevisionDtl A ");
        //    SQL.AppendLine("Inner Join TblSOContractDtl B On A.SOCDocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
        //    SQL.AppendLine("Set A.PrevAmt = B.Amt; ");

        //    SQL.AppendLine("Update TblSOContractRevisionDtl2 A ");
        //    SQL.AppendLine("Inner Join TblSOContractDtl2 B On A.SOCDocNo = B.DocNo And A.DNo = B.DNo And A.DocNo = @DocNo ");
        //    SQL.AppendLine("Set A.PrevAmt = B.Amt; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

        //    return cm;
        //}

        //private MySqlCommand UpdateSOCAmt(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblSOContractHdr A ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.DocNo = B.SOCDocNo And B.DocNo = @DocNo And B.Status = 'A' ");
        //    SQL.AppendLine("Set A.Amt = B.Amt; ");

        //    SQL.AppendLine("Update TblSOContractDtl A ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionDtl B On A.DocNo = B.SOCDocNo And A.DNo = B.DNo And B.DocNo = @DocNo ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.DocNo = C.DocNo And C.Status = 'A' ");
        //    SQL.AppendLine("Set A.Amt = B.Amt; ");

        //    SQL.AppendLine("Update TblSOContractDtl2 A ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionDtl2 B On A.DocNo = B.SOCDocNo And A.DNo = B.DNo And B.DocNo = @DocNo ");
        //    SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.DocNo = C.DocNo And C.Status = 'A' ");
        //    SQL.AppendLine("Set A.Amt = B.Amt; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

        //    return cm;
        //}

        private void InputAmount(int RowIndex, int ColIndex)
        {
            if (ColIndex == 7)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 9);
                if (Sm.GetGrdStr(Grd2, RowIndex, 8) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 4);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 4) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 7));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 7));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 7].Value = Qty;
                                    Grd2.Cells[y, 9].Value = Qty;
                                    Grd2.Cells[RowIndex, 9].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                }
            }           
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowProjectImplementationRevisionHdr(DocNo);
                ShowSOContractRevisionHdr(DocNo);
                ShowSOContractRevisionDtl(DocNo);
                ShowSOContractRevisionDtl2(DocNo);
                ShowARDownPaymentRevision(DocNo);

                string mPRJIDocNo = Sm.GetValue("Select PRJIDocNo From TblProjectImplementationRevisionHdr Where DocNo = @Param;", DocNo);

                ShowProjectImplementationHdr(mPRJIDocNo, DocNo);
                ShowProjectImplementationDtl(mPRJIDocNo, DocNo);
                if(!mIsPRJIRevisionHideTabResource)ShowProjectImplementationDtl2(mPRJIDocNo, DocNo);
                ShowProjectImplementationDtl3(mPRJIDocNo, DocNo);
                ShowProjectImplementationDtl4(mPRJIDocNo, DocNo);
                ShowProjectImplementationDtl5(mPRJIDocNo, DocNo);
                Sm.ShowDocApproval(DocNo, "ProjectImplementationRev", ref Grd10);

                ComputeTotalResource();
                ComputeRemunerationCost();
                ComputeDirectCost();
                ComputeDetailAmt();
                ComputeTotal();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectImplementationRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.PRJIDocNo, A.DocDt, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("B.Amt2, D.Remark, E.PPNCode, E.PPhCode ");
            SQL.AppendLine("From TblProjectImplementationRevisionHdr A  ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B ON A.SOCRDocNo = B.DocNo ");
            SQL.AppendLine("LEFT JOIN ( SELECT  ");
            SQL.AppendLine("		MAX(T2.`Level`) LV, T1.DocNo, T1.DNo  ");
            SQL.AppendLine("		FROM tbldocapproval T1 ");
            SQL.AppendLine("		INNER JOIN tbldocapprovalsetting T2 ON T1.DocType = T2.DocType  AND T1.ApprovalDNo = T2.DNo ");
            SQL.AppendLine("		WHERE T1.DocType = 'ProjectImplementationRev' AND T1.DocNo = @DocNo AND (T1.UserCode IS NOT NULL AND T1.UserCode <> '') ");
            SQL.AppendLine("		GROUP BY T1.DocNo, T1.DNo ");
            SQL.AppendLine("				) C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("LEFT JOIN ( SELECT  ");
            SQL.AppendLine("		T2.`Level` LV, T1.DocNo, T1.DNo, T1.Remark, T3.EmpName ");
            SQL.AppendLine("		FROM tbldocapproval T1 ");
            SQL.AppendLine("		INNER JOIN tbldocapprovalsetting T2 ON T1.DocType = T2.DocType  AND T1.ApprovalDNo = T2.DNo ");
            SQL.AppendLine("		LEFT JOIN tblemployee T3 ON T1.UserCode = T3.UserCode ");
            SQL.AppendLine("		WHERE T1.DocType = 'ProjectImplementationRev' AND T1.DocNo = @DocNo AND (T1.UserCode IS NOT NULL AND T1.UserCode <> '') ");
            SQL.AppendLine("				) D ON C.DocNo = D.DocNo AND C.DNo = D.Dno AND C.LV = D.LV ");
            SQL.AppendLine("Left Join TblProjectImplementationHdr E ON A.PRJIDocNo=E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "PRJIDocNo", "DocDt", "StatusDesc", "Amt2", "Remark",

                    //6-7
                    "PPNCode", "PPhCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtProjectImplementationDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    MeeApprovalRemark.EditValue = Sm.DrStr(dr, c[5]);
                    mPPNCode = Sm.DrStr(dr, c[6]);
                    mPPhCode = Sm.DrStr(dr, c[7]);
                }, true
            );
        }

        private void ShowSOContractRevisionHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, A.Amt, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname, H.ProjectResource, T.Amt2 As SOCRevisionAmt ");
            SQL.AppendLine("From TblSOContractRevisionHdr T ");
            SQL.AppendLine("Inner Join TblProjectImplementationRevisionHdr T1 On T.DocNo = T1.SOCRDocNo And T1.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr A On T.SOCDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                        //6-10
                        "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                        
                        //11-15
                        "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                        
                        //16-20
                        "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                        
                        //21-25
                        "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                        
                        //26-28
                        "ProjectName", "ProjectResource", "SOCRevisionAmt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(SOCDteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(SOCLueStatus, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        TxtCustomerContactperson.EditValue = Sm.DrStr(dr, c[6]);
                        TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                        //TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                        SetLueSPCode(ref LueSPCode);
                        Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                        SetLueDTCode(ref LueShpMCode);
                        Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                        mSADNo = Sm.DrStr(dr, c[13]);
                        TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                        mCity = Sm.DrStr(dr, c[15]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                        mCnt = Sm.DrStr(dr, c[17]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                        SOCMeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                        SOCChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                        mProjectResourceCode = Sm.DrStr(dr, c[27]);
                        TxtExclPPN.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                    }, true
                );
        }

        private void ShowSOContractRevisionDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, (UPriceBefore+TaxAmt) As UPriceAfterTax, (UPriceBefore+TaxAmt) As Total ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select B.Dno, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, ");
            SQL.AppendLine("   A1.Amt As Uprice, 0 As Discount, 0 As DiscountAmt, ");
            SQL.AppendLine("   A1.Amt As UPriceAfterDiscount, ");
            SQL.AppendLine("   IfNull(A1.Amt, 0) As PromoRate, ");
            SQL.AppendLine("   A1.Amt As UPriceBefore, ");
            SQL.AppendLine("   B.TaxRate, ");
            SQL.AppendLine("   0 As TaxAmt, ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,  ");
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom  ");
            SQL.AppendLine("   From TblSOContractRevisionDtl A1 ");
            SQL.AppendLine("   Inner Join TblProjectImplementationRevisionHdr A2 On A1.DocNo = A2.SOCRDocNo And A2.DocNo = @DocNo ");
            SQL.AppendLine("   Inner Join TblSOContractHdr A On A1.SOCDocNo = A.DocNo ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo And A1.DNo = B.DNo ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode  ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine(") T Order By DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-22
                    "TotalVolume", "VolUom"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractRevisionDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("A2.ItBoqCode, D.ItBOQName, A11.AMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("From TblSOContractRevisionDtl2 A11 ");
            SQL.AppendLine("Inner JOin TblProjectImplementationRevisionHdr A12 On A11.Docno = A12.SOCRDocNo And A12.Docno = @DocNo ");
            SQL.AppendLine("Inner Join tblSOContracthdr A1 On A11.SOCDocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl2 A2 On A1.DocNo = A2.Docno And A11.DNo = A2.DNo ");
            SQL.AppendLine("Inner Join tblBOQhdr A On A1.BOQDocno = A.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo  And A2.ItBoqCode = C.ItBOQCode ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit ");
            SQL.AppendLine("    From TblItemBOQ ");
            SQL.AppendLine("    Where ItBOQCode not in ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
            SQL.AppendLine("        Where parent is not null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Order By D.ItBOQCode ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "Amt", "Allow", "parentInd"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                  Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);

              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);

        }

        private void ShowARDownPaymentRevision(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X2.SOCDocNo ");
            SQL.AppendLine("    From TblProjectImplementationRevisionHdr X1 ");
            SQL.AppendLine("    Inner Join TblSOContractRevisionHdr X2 On X1.SOCRDocNo = X2.DocNo ");
            SQL.AppendLine("    Where X1.Docno = @DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowLatestSOContractRevision(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string mSOCRevDocNo = string.Empty;

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblSOContractRevisionHdr ");
            SQL.AppendLine("Where SOCDocNo = @Param ");
            SQL.AppendLine("Order By CreateDt Desc ");
            SQL.AppendLine("Limit 1; ");

            mSOCRevDocNo = Sm.GetValue(SQL.ToString(), Sm.Left(TxtSOCDocNo.Text, 17));
            if (mSOCRevDocNo.Length > 0)
            {
                TxtSOCDocNo.EditValue = mSOCRevDocNo;
                decimal mAmt = Decimal.Parse(Sm.GetValue("Select IfNull(Amt2, 0.00) As Amt From TblSOContractRevisionHdr Where DocNo = @Param Limit 1; ", mSOCRevDocNo));
                TxtAmt.EditValue = Sm.FormatNum(mAmt, 8);
                TxtExclPPN.EditValue = Sm.FormatNum(mAmt, 8);
            }
        }

        private void ShowSOContractHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select I.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, A.Amt, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, H.Projectname, H.ProjectResource ");
            SQL.AppendLine("From TblSOContractHdr A  ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Inner Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr I On A.Docno = I.SOCDocNo ");
            SQL.AppendLine("Where I.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                    //6-10
                    "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 
                    
                    //11-15
                   "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 
                    
                    //16-20
                    "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 
                    
                    //21-25
                    "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",
                    
                    //26-27
                    "ProjectName", "ProjectResource"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(SOCDteDocDt, Sm.DrStr(dr, c[2]));
                    Sm.SetLue(SOCLueStatus, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                    TxtCustomerContactperson.EditValue = Sm.DrStr(dr, c[6]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                    SetLueSPCode(ref LueSPCode);
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                    SetLueDTCode(ref LueShpMCode);
                    Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    mSADNo = Sm.DrStr(dr, c[13]);
                    TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                    mCity = Sm.DrStr(dr, c[15]);
                    TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                    mCnt = Sm.DrStr(dr, c[17]);
                    TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                    TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                    SOCMeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                    SOCChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                    mProjectResourceCode = Sm.DrStr(dr, c[27]);
                }, true
            );
        }

        private void ShowSOContractDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, (UPriceBefore+TaxAmt) As UPriceAfterTax, (UPriceBefore+TaxAmt) As Total ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("   Select B.Dno, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName,  ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty,   ");
            SQL.AppendLine("   B.Amt As Uprice, 0 As Discount, 0 As DiscountAmt,  ");
            SQL.AppendLine("   B.Amt As UPriceAfterDiscount,  ");
            SQL.AppendLine("   IfNull(B.Amt, 0) As PromoRate,   ");
            SQL.AppendLine("   B.Amt As UPriceBefore,  ");
            SQL.AppendLine("   B.TaxRate,   ");
            SQL.AppendLine("   0 As TaxAmt,  ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, J.Volume, (B.QtyPackagingUnit * J.Volume) As TotalVolume,  "); 
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom  ");
            SQL.AppendLine("   From TblSOContractRevisionHdr A1 ");
            SQL.AppendLine("   Inner Join TblSOContractHdr A On A1.SOCDocNo = A.DocNo ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode  ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine("   Where A.DocNo = @DocNo ");
            SQL.AppendLine(") T Order By DNo; ");
           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-22
                    "TotalVolume", "VolUom"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("A2.ItBoqCode, D.ItBOQName, A2.AMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("From tblSOContractRevisionHdr A1 ");
            SQL.AppendLine("Inner Join TblSOContractDtl2 A2 On A1.SOCDocNo = A2.Docno ");
            SQL.AppendLine("Inner Join TblSOContractHdr A3 On A1.SOCDocNo = A3.DocNo ");
            SQL.AppendLine("Inner Join tblBOQhdr A On A3.BOQDocno = A.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo  And A2.ItBoqCode = C.ItBOQCode ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit ");
            SQL.AppendLine("    From TblItemBOQ ");
            SQL.AppendLine("    Where ItBOQCode not in ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Parent From TblItemBOQ ");
            SQL.AppendLine("        Where parent is not null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode ");
            SQL.AppendLine("Where A1.DocNo=@DocNo ");
            SQL.AppendLine("Order By D.ItBOQCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
              ref Grd2, ref cm, SQL.ToString(),
              new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "Amt", "Allow", "parentInd"
                },
              (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
              {
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                  Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                  Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                  Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                  if (Sm.GetGrdStr(Grd2, Row, 8) == "N")
                  {
                      Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                      Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                      Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                      Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                  }

              }, false, false, true, false
          );
            Sm.FocusGrd(Grd2, 0, 0);

        }

        private void ShowARDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        public void ShowBOQ(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.Docno AS BOQDocNo, A.DocDt, B.DocNo As LOpDocNo, B.ProjectName,  ");
            SQL.AppendLine("C.ItBoqCode, D.ItBOQName, C.totalAMt, ifnull(E.AllowEdit, 'N') As Allow, if(D.parent is null, 'Y', 'N') ParentInd  ");
            SQL.AppendLine("from tblBOQhdr A ");
            SQL.AppendLine("Inner Join TblLopHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblBOQDtl C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblItemBoQ D On C.ItBOQCode = D.ItBOQCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select ItBOQCode, 'Y' As AllowEdit  ");
	        SQL.AppendLine("    From TblItemBOQ ");
	        SQL.AppendLine("    Where ItBOQCode not in ");
	        SQL.AppendLine("    ( ");
		    SQL.AppendLine("        Select Parent From TblItemBOQ	 ");
		    SQL.AppendLine("        Where parent is not null ");
	        SQL.AppendLine("    ) ");
            SQL.AppendLine(")E On C.ItBOQCode = E.ItBOQCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo, C.ItBOQCode ;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BOQDocNo", 

                    //1-5
                    "DocDt", "LOpDocNo", "ProjectName", "ItBoqCode", "ItBOQName",
 
                    //6-8
                    "TotalAmt", "Allow", "parentInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    if (Sm.GetGrdStr(Grd2, Row, 8) == "N")
                    {
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                        Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                        Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }
                    else
                    {
                        Grd2.Cells[Row, 7].Value = Sm.GetGrdDec(Grd2, Row, 6);
                    }                    
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowProjectImplementationHdr(string DocNo, string DocNoRev)
        {
            var SQL = new StringBuilder();

            if (DocNoRev.Length == 0)
            {
                SQL.AppendLine("Select TotalPrice, TotalBobot, TotalPersentase, TotalPrice2, TotalBobot2, TotalPersentase2, TotalAchievement ");
                SQL.AppendLine("From TblProjectImplementationHdr A ");
                SQL.AppendLine("Where A.DocNo = @DocNo; ");
            }
            else
            {
                SQL.AppendLine("Select A.TotalPrice, A.TotalBobot, A.TotalPersentase, A.TotalPrice2, A.TotalBobot2, A.TotalPersentase2, A.TotalAchievement ");
                SQL.AppendLine("From TblProjectImplementationRevisionHdr A ");
                SQL.AppendLine("INNER JOIN tblprojectimplementationhdr B ON A.PRJIDocNo = B.DocNo AND B.CancelInd = 'N' ");
                SQL.AppendLine("Where A.DocNo = @DocNoRev; ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocNoRev", DocNoRev);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "TotalPrice",

                    //1-5
                    "TotalBobot", "TotalPersentase", "TotalBobot2", "TotalPersentase2", "TotalAchievement",

                    //6
                    "TotalPrice2"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtTotalPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[0]), 0);
                    TxtTotalBobot.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[1]), 0);
                    TxtTotalPersentase.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                    TxtTotalBobot2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                    TxtTotalPersentase2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    TxtTotalAchievement.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    TxtTotalPrice2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                }, true
            );
        }

        private void ShowProjectImplementationDtl(string DocNo, string DocNoRev)
        {
            var SQLDtl = new StringBuilder();

            if (DocNoRev.Length == 0)
            {
                SQLDtl.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
                SQLDtl.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd, A.Remark, A.SettleDt, A.InvoicedInd, ");
                SQLDtl.AppendLine("A.Amt, A.EstimatedAmt, A.SettledAmt ");
                SQLDtl.AppendLine("From TblProjectImplementationDtl A ");
                SQLDtl.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNo ");
                SQLDtl.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
                SQLDtl.AppendLine("Order By A.DNo; ");
            }
            else
            {
                SQLDtl.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
                SQLDtl.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd, A.Remark, A.SettleDt, A.InvoicedInd, ");
                SQLDtl.AppendLine("A.Amt, A.EstimatedAmt, A.SettledAmt ");
                SQLDtl.AppendLine("From TblProjectImplementationRevisionDtl A ");
                SQLDtl.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNoRev ");
                SQLDtl.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
                SQLDtl.AppendLine("Order By A.DNo; ");
            }
            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm1, "@DocNoRev", DocNoRev);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "StageCode", 
                    "StageName", "TaskCode", "TaskName", "PlanStartDt", "PlanEndDt",
                    "Bobot", "BobotPercentage", "SettledInd", "Remark", "SettleDt",
                    "InvoicedInd", "Amt", "EstimatedAmt", "SettledAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7, 12, 13, 14 });
        }

        private void ShowProjectImplementationDtl2(string DocNo, string DocNoRev)
        {
            var SQLDtl2 = new StringBuilder();

            if (DocNoRev.Length == 0)
            {
                SQLDtl2.AppendLine("Select A.Sequence, A.ResourceItCode, B.ItName, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt, B.ItGrpCode, D.DocNo As RBPDocNo, IfNull((D.TotalRemuneration + D.TotalDirectCost), 0.00) RBPAmt ");
                SQLDtl2.AppendLine("From TblProjectImplementationDtl2 A ");
                SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
                SQLDtl2.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
                SQLDtl2.AppendLine("Left Join TblProjectImplementationRBPHdr D On A.DocNo = D.PRJIDocNo And D.ResourceItCode = A.ResourceItCode And D.CancelInd = 'N' ");
                SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
                SQLDtl2.AppendLine("Order By A.DNo; ");
            }
            else
            {
                SQLDtl2.AppendLine("Select A2.Sequence, A.ResourceItCode, B.ItName, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt, B.ItGrpCode, D.DocNo As RBPDocNo, IfNull((D.TotalRemuneration + D.TotalDirectCost), 0.00) RBPAmt ");
                SQLDtl2.AppendLine("From TblProjectImplementationRevisionDtl2 A ");
                SQLDtl2.AppendLine("INNER JOIN TblProjectImplementationRevisionHdr A1 ON A.DocNo = A1.DocNo ");
                SQLDtl2.AppendLine("INNER JOIN TblProjectImplementationDtl2 A2 ON A1.PRJIDocno = A2.DocNo AND A.DNo = A2.DNo ");
                SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
                SQLDtl2.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
                SQLDtl2.AppendLine("Left Join TblProjectImplementationRBPHdr D On A2.DocNo = D.PRJIDocNo And D.ResourceItCode = A2.ResourceItCode And D.CancelInd = 'N' ");
                SQLDtl2.AppendLine("Where A.DocNo = @DocNoRev ");
                SQLDtl2.AppendLine("Order By A.DNo; ");
            }

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm2, "@DocNoRev", DocNoRev);

            Sm.ShowDataInGrid(
                ref Grd6, ref cm2, SQLDtl2.ToString(),
                new string[] 
                { 
                    "ResourceItCode", 
                    "ItName", "ItGrpName", "Remark", "Qty1", "Qty2", 
                    "UPrice", "Tax", "TotalWithoutTax", "TotalTax", "Amt",
                    "ItGrpCode", "RBPDocNo", "RBPAmt", "Sequence"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11, 15 });
            
            Grd6Control();
        }

        private void ShowProjectImplementationDtl3(string DocNo, string DocNoRev)
        {
            var SQLDtl3 = new StringBuilder();

            if (DocNoRev.Length == 0)
            {
                SQLDtl3.AppendLine("Select Remark, PIC ");
                SQLDtl3.AppendLine("From TblProjectImplementationDtl3 ");
                SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                SQLDtl3.AppendLine("Order By DNo; ");
            }
            else
            {
                SQLDtl3.AppendLine("Select Remark, PIC ");
                SQLDtl3.AppendLine("From TblProjectImplementationRevisionDtl3 ");
                SQLDtl3.AppendLine("Where DocNo = @DocNoRev ");
                SQLDtl3.AppendLine("Order By DNo; ");
            }

            var cm3 = new MySqlCommand();
            Sm.CmParam<String>(ref cm3, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm3, "@DocNoRev", DocNoRev);

            Sm.ShowDataInGrid(
                ref Grd7, ref cm3, SQLDtl3.ToString(),
                new string[] 
                { 
                    "Remark", 
                    "PIC"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 1);
        }

        private void ShowProjectImplementationDtl4(string DocNo, string DocNoRev)
        {
            var SQLDtl4 = new StringBuilder();
            if (DocNoRev.Length == 0)
            {
                SQLDtl4.AppendLine("Select A.DocCode, B.DocName, A.Dt, A.Remark ");
                SQLDtl4.AppendLine("From TblProjectImplementationDtl4 A ");
                SQLDtl4.AppendLine("Left Join TblProjectDocument B On A.DocCode = B.DocCode ");
                SQLDtl4.AppendLine("Where A.DocNo = @DocNo ");
                SQLDtl4.AppendLine("Order By A.DNo; ");
            }
            else
            {
                SQLDtl4.AppendLine("Select A.DocCode, B.DocName, A.Dt, A.Remark ");
                SQLDtl4.AppendLine("From TblProjectImplementationRevisionDtl4 A ");
                SQLDtl4.AppendLine("Left Join TblProjectDocument B On A.DocCode = B.DocCode ");
                SQLDtl4.AppendLine("Where A.DocNo = @DocNoRev ");
                SQLDtl4.AppendLine("Order By A.DNo; ");
            }

            var cm4 = new MySqlCommand();
            Sm.CmParam<String>(ref cm4, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm4, "@DocNoRev", DocNoRev);

            Sm.ShowDataInGrid(
                ref Grd8, ref cm4, SQLDtl4.ToString(),
                new string[] 
                { 
                    "DocCode", 
                    "DocName", "Dt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd8, 0, 1);
        }

        private void ShowProjectImplementationDtl5(string DocNo, string DocNoRev)
        {
            var SQLDtl5 = new StringBuilder();

            if (DocNoRev.Length == 0)
            {
                SQLDtl5.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
                SQLDtl5.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd, A.Remark, A.SettleDt, A.InvoicedInd, ");
                SQLDtl5.AppendLine(" A.Amt, A.EstimatedAmt, A.SettledAmt ");
                SQLDtl5.AppendLine("From TblProjectImplementationDtl5 A ");
                SQLDtl5.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNo ");
                SQLDtl5.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
                SQLDtl5.AppendLine("Order By A.DNo; ");
            }
            else
            {
                SQLDtl5.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
                SQLDtl5.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd, A.Remark, A.SettleDt, A.InvoicedInd, ");
                SQLDtl5.AppendLine(" A.Amt, A.EstimatedAmt, A.SettledAmt ");
                SQLDtl5.AppendLine("From TblProjectImplementationRevisionDtl5 A ");
                SQLDtl5.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNoRev ");
                SQLDtl5.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
                SQLDtl5.AppendLine("Order By A.DNo; ");
            }

            var cm15 = new MySqlCommand();
            Sm.CmParam<String>(ref cm15, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm15, "@DocNoRev", DocNoRev);

            Sm.ShowDataInGrid(
                ref Grd9, ref cm15, SQLDtl5.ToString(),
                new string[] 
                { 
                    "StageCode", 
                    "StageName", "TaskCode", "TaskName", "PlanStartDt", "PlanEndDt",
                    "Bobot", "BobotPercentage", "SettledInd", "Remark", "SettleDt",
                    "InvoicedInd", "Amt", "EstimatedAmt", "SettledAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd9, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd9, Grd9.Rows.Count - 1, new int[] { 6, 7, 12, 13, 14 });
        }

        #endregion

        #region Additional Method

        private decimal PPNRate()
        {
            var rate = 0m;
            if (mPPNCode.Length > 0)
            {
                rate = Decimal.Parse(Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param", mPPNCode));
                rate = rate / 100;
            }
            else
                rate = 0.1m;

            return rate;
        }
        private decimal PPhRate()
        {
            var rate = 0m;
            if (mPPhCode.Length > 0)
            {
                rate = Decimal.Parse(Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param", mPPhCode));
                rate = rate / 100;
            }
            else
                rate = 0.1m;

            return rate;
        }

        private void ComputeRBPAmt()
        {
            string RBPDocNo = string.Empty;

            if (Grd6.Rows.Count > 0)
            {
                for (int i = 0; i < Grd6.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd6, i, 14).Length > 0)
                    {
                        if (RBPDocNo.Length > 0) RBPDocNo += ",";
                        RBPDocNo += Sm.GetGrdStr(Grd6, i, 14);
                    }
                }
            }

            if (RBPDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                var lRBP = new List<RBP>();

                SQL.AppendLine("Select DocNo, (TotalRemuneration + TotalDirectCost) RBPAmt ");
                SQL.AppendLine("From TblProjectImplementationRBPHdr ");
                SQL.AppendLine("Where Find_In_Set(DocNo, @Param); ");

                Sm.CmParam<String>(ref cm, "@Param", RBPDocNo);
                
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "RBPAmt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lRBP.Add(new RBP()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                RBPAmt = Sm.DrDec(dr, c[1])
                            });
                        }
                    }
                    dr.Close();
                }

                if (lRBP.Count > 0)
                {
                    for (int i = 0; i < Grd6.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd6, i, 14).Length > 0)
                        {
                            foreach (var x in lRBP.Where(w => w.DocNo == Sm.GetGrdStr(Grd6, i, 14)))
                            {
                                Grd6.Cells[i, 15].Value = x.RBPAmt;
                            }
                        }
                    }
                }

                lRBP.Clear();
            }
        }

        private void Grd6Control()
        {
            if (!mIsPRJIRevisionResourceAllowedToEditAfterRBP)
            {
                if (Grd6.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd6, Row, 14).Length > 0)
                        {
                            Grd6.Rows[Row].ReadOnly = iGBool.True;
                        }
                    }
                }
            }
        }

        private void Grd5Control()
        {
            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdBool(Grd5, Row, 8))
                    {
                        Grd5.Rows[Row].ReadOnly = iGBool.True;
                    }
                }
            }
        }

        private void ComputeBobotPercentage()
        {
            #region Old Code
            //decimal mTotalBobot = 0m;
            //decimal mMultiplier = 0m;
            //for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
            //    {
            //        if (Sm.GetGrdBool(Grd5, Row, 8))
            //        {
            //            mMultiplier += Sm.GetGrdDec(Grd5, Row, 7);
            //        }
            //        else
            //        {
            //            mTotalBobot += Sm.GetGrdDec(Grd5, Row, 6);
            //        }
            //    }
            //}

            //if (mMultiplier <= 0m) mMultiplier = 100m;
            //else mMultiplier = 100m - mMultiplier;

            //if (mTotalBobot != 0)
            //{
            //    for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0 && !Sm.GetGrdBool(Grd5, Row, 8))
            //        {
            //            Grd5.Cells[Row, 7].Value = (Sm.GetGrdDec(Grd5, Row, 6) / mTotalBobot) * mMultiplier;
            //        }
            //    }
            //}
            #endregion

            decimal mTotalBobot = 0m;
            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    mTotalBobot += Sm.GetGrdDec(Grd5, Row, 6);

            if (mTotalBobot >= 99.5m) mTotalBobot = 100m;
            if (mIsWBSBobotColumnAutoCompute) TxtTotalBobot.EditValue = Sm.FormatNum(mTotalBobot, 0);
            else
            {
                decimal mTotalBobot2 = 0m;
                for (int i = 0; i < Grd5.Rows.Count; i++)
                {
                    mTotalBobot2 += Sm.GetGrdDec(Grd5, i, 6);
                }
                TxtTotalBobot.EditValue = Sm.FormatNum(mTotalBobot2, 0);
            }
            TxtTotalPersentase.EditValue = Sm.FormatNum(mTotalBobot, 0);
            if (mTotalBobot != 0)
            {
                for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    {
                        Grd5.Cells[Row, 7].Value = Sm.Round(((Sm.GetGrdDec(Grd5, Row, 6) / mTotalBobot) * 100m), 2);
                    }
                }
            }
        }

        private void ComputeBobotPercentage2()
        {
            decimal mTotalBobot = 0m;
            for (int Row = 0; Row < Grd9.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd9, Row, 0).Length > 0)
                    mTotalBobot += Sm.GetGrdDec(Grd9, Row, 6);

            if (mTotalBobot >= 99.5m) mTotalBobot = 100m;
            if (mIsWBSBobotColumnAutoCompute) TxtTotalBobot2.EditValue = Sm.FormatNum(mTotalBobot, 0);
            else
            {
                decimal mTotalBobot2 = 0m;
                for (int i = 0; i < Grd9.Rows.Count; i++)
                {
                    mTotalBobot2 += Sm.GetGrdDec(Grd9, i, 6);
                }
                TxtTotalBobot2.EditValue = Sm.FormatNum(mTotalBobot2, 0);
            }
            TxtTotalPersentase2.EditValue = Sm.FormatNum(mTotalBobot, 0);
            if (mTotalBobot != 0)
            {
                for (int Row = 0; Row < Grd9.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd9, Row, 0).Length > 0)
                    {
                        Grd9.Cells[Row, 7].Value = Sm.Round(((Sm.GetGrdDec(Grd9, Row, 6) / mTotalBobot) * 100m), 2);
                    }
                }
            }
        }

        private void ComputeBobot()
        {
            decimal mTotalPrice = 0m;
            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                    mTotalPrice += Sm.GetGrdDec(Grd5, Row, 12);

            TxtTotalPrice.EditValue = Sm.FormatNum(mTotalPrice, 0);

            if (mIsWBSBobotColumnAutoCompute)
            {
                if (mTotalPrice != 0)
                {
                    for (int Row = 0; Row < Grd5.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                        {
                            Grd5.Cells[Row, 6].Value = Sm.Round(((Sm.GetGrdDec(Grd5, Row, 12) / mTotalPrice) * 100m), 2);
                        }
                    }
                }
            }
        }

        private void ComputeBobot2()
        {
            decimal mTotalPrice = 0m;
            for (int Row = 0; Row < Grd9.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd9, Row, 0).Length > 0)
                    mTotalPrice += Sm.GetGrdDec(Grd9, Row, 12);

            TxtTotalPrice2.EditValue = Sm.FormatNum(mTotalPrice, 0);

            if (mIsWBSBobotColumnAutoCompute)
            {
                if (mTotalPrice != 0)
                {
                    for (int Row = 0; Row < Grd9.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd9, Row, 0).Length > 0)
                        {
                            Grd9.Cells[Row, 6].Value = Sm.Round(((Sm.GetGrdDec(Grd9, Row, 12) / mTotalPrice) * 100m), 2);
                        }
                    }
                }
            }
        }

        //private void ComputeEstimatedAmt()
        //{
        //    decimal mGrandTotalSOCRev = 0m;
        //    decimal mAmt = 0m, mRevAmt = 0m;
        //    var cm = new MySqlCommand();

        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Sum(T.Amt) Amt, Sum(T.RevAmt) RevAmt ");
        //    SQL.AppendLine("From ");
        //    SQL.AppendLine("( ");
        //    SQL.AppendLine("    Select Amt, 0 As RevAmt ");
        //    SQL.AppendLine("    From TblSOContractHdr ");
        //    SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("    Select 0 As Amt, Amt As RevAmt ");
        //    SQL.AppendLine("    From TblSOContractRevisionHdr ");
        //    SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
        //    SQL.AppendLine(") T; ");

        //    Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "Amt", "RevAmt" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                mAmt = Sm.DrDec(dr, c[0]);
        //                mRevAmt = Sm.DrDec(dr, c[1]);
        //            }
        //        }
        //        dr.Close();
        //    }

        //    if (mRevAmt > 0m) mGrandTotalSOCRev = mRevAmt;
        //    else mGrandTotalSOCRev = mAmt;

        //    for (int i = 0; i < Grd5.Rows.Count; i++)
        //    {
        //        if (Sm.GetGrdStr(Grd5, i, 0).Length > 0)
        //        {
        //            if (mGrandTotalSOCRev == 0m)
        //                Grd5.Cells[i, 13].Value = 0m;
        //            else
        //                Grd5.Cells[i, 13].Value = (Sm.GetGrdDec(Grd5, i, 7) * mGrandTotalSOCRev) / 100m;
        //        }
        //    }
        //}

        private void GrdRemoveRow(iGrid Grd, KeyEventArgs e, SimpleButton Btn)
        {
            if (Btn.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd.SelectedRows.Count > 0)
                {
                    if (Grd.Rows[Grd.Rows[Grd.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                if (Sm.GetGrdBool(Grd, Grd.SelectedRows[Index].Index, 8))
                                {
                                    Sm.StdMsg(mMsgType.Warning, "You can't remove this settled data.");
                                    return;
                                }
                                Grd.Rows.RemoveAt(Grd.SelectedRows[Index].Index);
                            }
                            if (Grd.Rows.Count <= 0) Grd.Rows.Add();
                        }
                    }
                }
            }
        }

        internal void ComputeTotalResource()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtTotalResource.EditValue = Sm.FormatNum(Total, 0);
        }

        internal void ComputeRemunerationCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForRemuneration && mItGrpCodeForRemuneration.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtRemunerationCost.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeDirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForDirectCost && mItGrpCodeForDirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtDirectCost.EditValue = Sm.FormatNum(Total, 0);
        }

        internal void ComputeDetailAmt()
        {
            decimal NetCash = 0m;
            decimal PPNAmt = 0m;
            decimal PPhAmt = 0m;
            decimal ContractAmt = 0m;

            if (mPPNCode.Length > 0)
                LblPPN.Text = Sm.GetValue("Select OptDesc From TblOption Where OptCat='PPNForProject' And OptCode=@Param ", mPPNCode);
            else
                LblPPN.Text = "10 %";

            if (mPPhCode.Length > 0)
                LblPPh.Text = Sm.GetValue("Select OptDesc From TblOption Where OptCat='PPhForProject' And OptCode=@Param ", mPPhCode);
            else
                LblPPh.Text = "4 %";


            if (mProjectResourceCode == mPPN10ForProjectResourceShowZero)
                TxtPPNAmt.EditValue = Sm.FormatNum(0, 0);
            else
            {
                PPNAmt = Sm.GetDecValue(TxtExclPPN.Text) * PPNRate(); //0.1m //ExclPPN - Sm.GetDecValue(TxtContractAmt.Text);
                TxtPPNAmt.EditValue = Sm.FormatNum(PPNAmt, 0);
            }

            PPhAmt = Sm.GetDecValue(TxtExclPPN.Text) * PPhRate();
            TxtPPhAmt.EditValue = Sm.FormatNum(PPhAmt, 0);

            ContractAmt = Sm.GetDecValue(TxtExclPPN.Text) + PPNAmt;
            TxtContractAmt.EditValue = Sm.FormatNum(ContractAmt, 0);

            NetCash = Sm.GetDecValue(TxtExclPPN.Text) - PPhAmt;
            TxtNetCash.EditValue = Sm.FormatNum(NetCash, 0);
        }

        internal void ComputeTotal()
        {
            decimal PPNPercentage = 0m;
            decimal PPhPPNPercentage = 0m;
            decimal Total = 0m;

            Total = Sm.GetDecValue(TxtRemunerationCost.Text) + Sm.GetDecValue(TxtDirectCost.Text);
            TxtTotal.EditValue = Sm.FormatNum(Total, 0);

            if (Sm.GetDecValue(TxtExclPPN.Text) != 0) PPNPercentage = (Total / Sm.GetDecValue(TxtExclPPN.Text)) * 100m;
            if (PPNPercentage >= 99.5m) PPNPercentage = 100m;
            TxtPPNPercentage.EditValue = Sm.FormatNum(PPNPercentage, 2);

            if (Sm.GetDecValue(TxtNetCash.Text) != 0) PPhPPNPercentage = (Total / Sm.GetDecValue(TxtNetCash.Text)) * 100m;
            if (PPhPPNPercentage >= 99.5m) PPhPPNPercentage = 100m;
            TxtPPNPPhPercentage.EditValue = Sm.FormatNum(PPhPPNPercentage, 2);
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal string GetSelectedItCode()
        {
            var SQL = string.Empty;
            if (Grd6.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd6, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedDocCode()
        {
            var SQL = string.Empty;
            if (Grd8.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd8, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd8, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblProjectImplementationRevisionHdr ");
            SQL.AppendLine("       Where PRJIDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtProjectImplementationDocNo.Text);
        }

        private string GenerateDocNo2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblSOContractRevisionHdr ");
            SQL.AppendLine("       Where SOCDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtSOCDocNo.Text);
        }

        internal void ShowSOContract(string DocNo)
        {
            ClearData3();
            ShowSOContractHdr(DocNo);
            ShowSOContractDtl(DocNo);
            ShowSOContractDtl2(DocNo);
            ShowARDownPayment(DocNo);
            ShowProjectImplementationHdr(TxtProjectImplementationDocNo.Text, string.Empty);
            ShowProjectImplementationDtl(TxtProjectImplementationDocNo.Text, string.Empty);
            //Grd5Control();
            ShowProjectImplementationDtl2(TxtProjectImplementationDocNo.Text, string.Empty);
            ShowProjectImplementationDtl3(TxtProjectImplementationDocNo.Text, string.Empty);
            ShowProjectImplementationDtl4(TxtProjectImplementationDocNo.Text, string.Empty);
            ShowProjectImplementationDtl5(TxtProjectImplementationDocNo.Text, string.Empty);

            ShowLatestSOContractRevision(DocNo);

            ComputeTotalResource();
            ComputeRemunerationCost();
            ComputeDirectCost();
            ComputeDetailAmt();
            ComputeTotal();
            if (mIsWBS2SameAsWBS)
            {
                ComputeBobot2();
                ComputeBobotPercentage2();
            }

        }

        internal void ComputeTotalBOQ()
        {
            decimal Amt = 0m;
            string BOQDocNo = TxtBOQDocNo.Text;

            for (int i = 0; i < Grd2.Rows.Count-1; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 10).Length > 0 && Sm.GetGrdStr(Grd2, i, 10) == "Y")
                {
                    Amt += Sm.GetGrdDec(Grd2, i, 9);
                }
            }
        
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeItem()
        {
            decimal Qty = 0m, TotalAmt = 0m, PriceGrid3 = 0m;
            Qty = Sm.GetGrdDec(Grd3, 0, 8);
            TotalAmt = Decimal.Parse(TxtAmt.Text);

            if (TotalAmt > 0 && Qty>0)
            {
                PriceGrid3 = TotalAmt / Qty;
            }

            Grd3.Cells[0, 11].Value = PriceGrid3;
            Grd3.Cells[0, 14].Value = PriceGrid3;
            Grd3.Cells[0, 19].Value = PriceGrid3;
            Grd3.Cells[0, 20].Value = PriceGrid3;
        }

        private void SetNumberOfSalesUomCode()
        {
            string NumberOfSalesUomCode = Sm.GetParameter("NumberOfSalesUomCode");
            if (NumberOfSalesUomCode.Length == 0)
                mNumberOfSalesUomCode = 1;
            else
                mNumberOfSalesUomCode = int.Parse(NumberOfSalesUomCode);
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSoUseDefaultPrintout = Sm.GetParameter("IsSoUseDefaultPrintout");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsSOUseARDPValidated = Sm.GetParameter("IsSOUseARDPValidated") == "Y";
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mListItCtCodeForResource = Sm.GetParameter("ListItCtCodeForResource");
            mIsWBSBobotColumnAutoCompute = Sm.GetParameterBoo("IsWBSBobotColumnAutoCompute");
            mWBSFormula = Sm.GetParameter("WBSFormula");
            mItGrpCodeForRemuneration = Sm.GetParameter("ItGrpCodeForRemuneration");
            mItGrpCodeForDirectCost = Sm.GetParameter("ItGrpCodeForDirectCost");
            mPPN10ForProjectResourceShowZero = Sm.GetParameter("PPN10%ForProjectResourceShowZero");
            mIsProjectImplementationDisplayTotalResource = Sm.GetParameterBoo("IsProjectImplementationDisplayTotalResource");
            mIsProjectImplementationApprovalBySiteMandatory = Sm.GetParameterBoo("IsProjectImplementationApprovalBySiteMandatory");
            mIsPRJIUseWBS2 = Sm.GetParameterBoo("IsPRJIUseWBS2");
            mIsPRJIRevisionOnlyForShow = Sm.GetParameterBoo("IsPRJIRevisionOnlyForShow");
            mIsPRJIRevisionResourceAllowedToEditAfterRBP = Sm.GetParameterBoo("IsPRJIRevisionResourceAllowedToEditAfterRBP");
            mIsPRJIRevisionHideTabResource = Sm.GetParameterBoo("IsPRJIRevisionHideTabResource");
            mIsPSModuleShowApproverRemarkInfo = Sm.GetParameterBoo("IsPSModuleShowApproverRemarkInfo");
            mIsWBS2SameAsWBS = Sm.GetParameterBoo("IsWBS2SameAsWBS");

            if (mWBSFormula.Length == 0) mWBSFormula = "1";
        }

        private void ParPrint()
        {
            
        }

        private void SetBOQDocNo(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr ");
            SQL.AppendLine("Where ActInd='Y' And Status='A' And CtCode=@CtCode ");
            SQL.AppendLine("Order By DocDt Desc, DocNo Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            TxtBOQDocNo.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblBOQhdr A " +
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblBOQhdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");


            if (AllowEditSalesPerson == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, false);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueSPCode    
                }, true);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }


            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode   
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    LueShpMCode    
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }


        }

        #endregion

        #region SetLue

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType " +
                 "Union ALL Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }



        public void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson " +
                "Where CtCode= '" + CtCode + "' Order By ContactPersonName;",
                "Contact Person");
        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            Sm.SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOContractHdr " +
                "Where DocNo= '" + DocNo + "' Order By CtContactPersonName;",
                "Contact Person");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'A' As Doctype, 'Approve' As Status ");
            SQL.AppendLine("    Union ALl ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetBOQDocNo(CtCode);
                    TxtSAName.EditValue = null;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(SOCLueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueSPCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetLueSPCode(ref LueSPCode);
                    SetLueDTCode(ref LueShpMCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(SOCLueStatus, new Sm.RefreshLue1(SetLueStatus));
        }


        private void LueStageCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueStageCode, new Sm.RefreshLue1(Sl.SetLueProjectStageCode));
            }
        }

        private void LueStageCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueStageCode_Leave(object sender, EventArgs e)
        {
            if (LueStageCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueStageCode).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 0].Value =
                    Grd5.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueStageCode);
                    Grd5.Cells[fCell.RowIndex, 1].Value = LueStageCode.GetColumnValue("Col2");
                }
                LueStageCode.Visible = false;
            }
        }

        private void LueTaskCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaskCode, new Sm.RefreshLue1(Sl.SetLueProjectTaskCode));
            }
        }

        private void LueTaskCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueTaskCode_Leave(object sender, EventArgs e)
        {
            if (LueTaskCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueTaskCode).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 2].Value =
                    Grd5.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueTaskCode);
                    Grd5.Cells[fCell.RowIndex, 3].Value = LueTaskCode.GetColumnValue("Col2");
                }
                LueTaskCode.Visible = false;
            }
        }

        private void DtePlanStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DtePlanStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanStartDt, ref fCell, ref fAccept);
        }

        private void DtePlanEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DtePlanEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanEndDt, ref fCell, ref fAccept);
        }

        private void DteDocDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd8, ref fAccept, e);
        }

        private void DteDocDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDocDt2, ref fCell, ref fAccept);
        }

        private void TxtTotalResource_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtTotalResource, 0);
        }

        private void LueStageCode2_EditValueChanged(object sender, EventArgs e)
        {

            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueStageCode2, new Sm.RefreshLue1(Sl.SetLueProjectStageCode));
            }
        }

        private void LueStageCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void LueStageCode2_Leave(object sender, EventArgs e)
        {
            if (LueStageCode2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueStageCode2).Length == 0)
                    Grd9.Cells[fCell.RowIndex, 0].Value =
                    Grd9.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueStageCode2);
                    Grd9.Cells[fCell.RowIndex, 1].Value = LueStageCode2.GetColumnValue("Col2");
                }
                LueStageCode2.Visible = false;
            }
        }

        private void LueTaskCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaskCode2, new Sm.RefreshLue1(Sl.SetLueProjectTaskCode));
            }
        }

        private void LueTaskCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd9, ref fAccept, e);
        }

        private void LueTaskCode2_Leave(object sender, EventArgs e)
        {
            if (LueTaskCode2.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueTaskCode2).Length == 0)
                    Grd9.Cells[fCell.RowIndex, 2].Value =
                    Grd9.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd9.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueTaskCode2);
                    Grd9.Cells[fCell.RowIndex, 3].Value = LueTaskCode2.GetColumnValue("Col2");
                }
                LueTaskCode2.Visible = false;
            }
        }

        private void DtePlanStartDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DtePlanStartDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanStartDt2, ref fCell, ref fAccept);
        }

        private void DtePlanEndDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DtePlanEndDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanEndDt2, ref fCell, ref fAccept);
        }

        #endregion

        #region Button Event

        private void BtnProjectImplementationDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.FormShowDialog(new FrmProjectImplementationRevisionDlg(this));
        }

        private void BtnImportWBS_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ResultWBS>();

                try
                {
                    ProcessWBS1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessWBS2(ref l);
                        ProcessWBS3(ref l);
                        ProcessWBS4(ref l);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void ProcessWBS1(ref List<ResultWBS> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 5)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        return;
                    }
                    else
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            l.Add(new ResultWBS()
                            {
                                StageCode = splits[0].Trim(),
                                TaskCode = splits[1].Trim(),
                                PlanStartDt = splits[2].Trim(),
                                PlanEndDt = splits[3].Trim(),
                                Bobot = Decimal.Parse(splits[4].Trim().Length > 0 ? splits[4].Trim() : "0"),
                                StageName = string.Empty,
                                TaskName = string.Empty
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessWBS2(ref List<ResultWBS> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var StageCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(StageCode=@StageCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@StageCode0" + i.ToString(), l[i].StageCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select StageCode, StageName From TblProjectStage ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "StageCode", "StageName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        StageCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].StageCode, StageCode))
                                l[i].StageName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS3(ref List<ResultWBS> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var TaskCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(TaskCode=@TaskCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@TaskCode0" + i.ToString(), l[i].TaskCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select TaskCode, TaskName From TblProjectTask ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "TaskCode", "TaskName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        TaskCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].TaskCode, TaskCode))
                                l[i].TaskName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS4(ref List<ResultWBS> l)
        {
            iGRow r;
            Grd5.Rows.Count = Grd5.Rows.Count - 1;
            Grd5.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd5.Rows.Add();
                r.Cells[0].Value = l[i].StageCode;
                r.Cells[1].Value = l[i].StageName;
                r.Cells[2].Value = l[i].TaskCode;
                r.Cells[3].Value = l[i].TaskName;
                if (l[i].PlanStartDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].PlanStartDt);
                if (l[i].PlanEndDt.Length > 0) r.Cells[5].Value = Sm.ConvertDate(l[i].PlanEndDt);
                r.Cells[6].Value = l[i].Bobot;
                r.Cells[7].Value = 0m;
                r.Cells[8].Value = false;
            }
            r = Grd5.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd5, Grd5.Rows.Count - 1, new int[] { 6, 7 });
            Sm.SetGrdBoolValueFalse(ref Grd5, Grd5.Rows.Count - 1, new int[] { 8 });
            Grd5.EndUpdate();
        }

        private void ProcessResource1(ref List<ResultResource> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 6)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        return;
                    }
                    else
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            l.Add(new ResultResource()
                            {
                                ResourceItCode = splits[0].Trim(),
                                Remark = splits[1].Trim(),
                                Qty1 = Decimal.Parse(splits[2].Trim().Length > 0 ? splits[2].Trim() : "0"),
                                Qty2 = Decimal.Parse(splits[3].Trim().Length > 0 ? splits[3].Trim() : "0"),
                                UPrice = Decimal.Parse(splits[4].Trim().Length > 0 ? splits[4].Trim() : "0"),
                                Tax = Decimal.Parse(splits[5].Trim().Length > 0 ? splits[5].Trim() : "0"),
                                ItName = string.Empty,
                                ItGrpName = string.Empty,
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessResource2(ref List<ResultResource> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var ItCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.ItCode=@ItCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), l[i].ResourceItCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.ItCode, A.ItName, B.ItGrpName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemGroup B On A.ItGrpCode=B.ItGrpCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "ItName", "ItGrpName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].ResourceItCode, ItCode))
                            {
                                l[i].ItName = Sm.DrStr(dr, c[1]);
                                l[i].ItGrpName = Sm.DrStr(dr, c[2]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessResource3(ref List<ResultResource> l)
        {
            iGRow r;
            Grd6.Rows.Count = Grd6.Rows.Count - 1;
            Grd6.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd6.Rows.Add();
                r.Cells[1].Value = l[i].ResourceItCode;
                r.Cells[2].Value = l[i].ItName;
                r.Cells[3].Value = l[i].ItGrpName;
                r.Cells[4].Value = l[i].Remark;
                r.Cells[5].Value = l[i].Qty1;
                r.Cells[6].Value = l[i].Qty2;
                r.Cells[7].Value = l[i].UPrice;
                r.Cells[8].Value = l[i].Tax;
                r.Cells[9].Value = l[i].Qty1 * l[i].Qty2 * l[i].UPrice;
                r.Cells[10].Value = l[i].Qty1 * l[i].Qty2 * l[i].Tax;
                r.Cells[11].Value = (l[i].Qty1 * l[i].Qty2 * l[i].UPrice) + (l[i].Qty1 * l[i].Qty2 * l[i].Tax);
            }
            r = Grd6.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
            Grd6.EndUpdate();
        }

        private void BtnImportResource_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ResultResource>();

                try
                {
                    ProcessResource1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessResource2(ref l);
                        ProcessResource3(ref l);
                        ComputeTotalResource();
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #endregion

        #endregion

        #region Class

        private class ResultWBS
        {
            public string StageCode { get; set; }
            public string StageName { get; set; }
            public string TaskCode { get; set; }
            public string TaskName { get; set; }
            public string PlanStartDt { get; set; }
            public string PlanEndDt { get; set; }
            public decimal Bobot { get; set; }
        }

        private class ResultResource
        {
            public string ResourceItCode { get; set; }
            public string ItName { get; set; }
            public string ItGrpName { get; set; }
            public string Remark { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Qty2 { get; set; }
            public decimal UPrice { get; set; }
            public decimal Tax { get; set; }
        }

        private class PRJI
        {
            public string Stage { get; set; }
            public string Task { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Bobot { get; set; }
            public decimal Persentase { get; set; }
            public decimal Akumulasi { get; set; }
            public bool SettledInd { get; set; }
            public string EndDt2 { get; set; }
        }

        private class PRJIDummy
        {
            public string Stage { get; set; }
            public string Task { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Bobot { get; set; }
            public decimal Persentase { get; set; }
            public decimal Akumulasi { get; set; }
            public bool SettledInd { get; set; }
            public string EndDt2 { get; set; }
        }

        private class PRJI2
        {
            public string CurrentDt { get; set; }
            public decimal Achievment { get; set; }

        }

        private class RBP
        {
            public string DocNo { get; set; }
            public decimal RBPAmt { get; set; }
        }

        #endregion   
    }
}
