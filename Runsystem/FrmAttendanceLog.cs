﻿#region Update
/*
 27/01/2020 [DITA/SIER] Bug dataa tidak bisa masuk saat duplicate time in dan time out
 31/03/2022 [DITA/ALL] Bug saat save date di attendance log formatnya masih salah
 * */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;
#endregion

namespace RunSystem
{
    public partial class FrmAttendanceLog : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmAttendanceLogFind FrmFind;

        #endregion

        #region Constructor

        public FrmAttendanceLog(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd(int countCol)
        {
            Grd1.Cols.Count = countCol;
            Grd1.FrozenArea.ColCount = 1;
        }

        private void SetGrd2()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        "Employee Code",
                        "Employee Name", "Date", "Time", "Machine",
                    },
                     new int[] 
                    {
                        100, 
                        200, 100, 80, 100
                    }
                    );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
          }



        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFileName }, true);
                    TxtFileName.Focus();
                    DteDocDt.Visible = false;
                    Grd1.ReadOnly = true;
                    BtnBrowse.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> {  }, false);
                    TxtFileName.Focus();
                    DteDocDt.Visible = false;
                    Grd1.ReadOnly = true;
                    BtnBrowse.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> {  }, false);
                    TxtFileName.Focus();
                    DteDocDt.Visible = false;
                    Grd1.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtFileName, 
            });
            
            Sm.ClearGrd(Grd1, true);
            //Sm.FocusGrd(Grd1, 0, 0);
        }
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAttendanceLogFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetGrd(1);
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertDataLog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Event 
        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }
        #endregion 

        #region SaveData
        private void InsertDataLog()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            string Createdate = Sm.GetDte(DteDocDt).Substring(0, 8);

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            int ColEmp = Convert.ToInt32(Sm.GetParameter("AttendanceColumn1"));
            int ColDate = Convert.ToInt32(Sm.GetParameter("AttendanceColumn2"));
            int ColTimeIn = Convert.ToInt32(Sm.GetParameter("AttendanceColumn3"));
            int ColTimeOut = Convert.ToInt32(Sm.GetParameter("AttendanceColumn4"));
            int ColMachine = Convert.ToInt32(Sm.GetParameter("AttendanceColumn5"));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, ColEmp).Length > 0&& Sm.GetGrdStr(Grd1, Row, ColDate).Length > 0&&Sm.GetGrdStr(Grd1, Row, ColTimeIn).Length > 0)
                {
                    cml.Add(SaveAttendanceLog(Row, ColEmp, ColDate, ColTimeIn, ColMachine));
                }
                if (Sm.GetGrdStr(Grd1, Row, ColEmp).Length > 0 && Sm.GetGrdStr(Grd1, Row, ColDate).Length > 0 && Sm.GetGrdStr(Grd1, Row, ColTimeOut).Length > 0)
                {
                    cml.Add(SaveAttendanceLog(Row, ColEmp, ColDate, ColTimeOut, ColMachine));
                }
            }
            Sm.ExecCommands(cml);

            ShowData(Createdate);

        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtFileName, "File Name", false) 
                ;
        }

        private MySqlCommand SaveAttendanceLog(int Row, int col1, int col2, int col3, int col4)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* attendance log */ ");
            SQL.Append("Insert Into TblAttendanceLog ");
            SQL.Append("(EmpCode, Dt, Tm, Machine, Remark, ");
            SQL.Append("CreateBy, CreateDt) ");
            SQL.Append("Values(@EmpCode, @Dt, @Tm, ");
            SQL.Append("@Machine, @Remark, ");
            SQL.Append("@CreateBy, CurrentDateTime()) ");
            SQL.Append("On Duplicate Key ");
            SQL.Append("   Update Dt=@Dt, Tm=@Tm, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, col1));
            string Dt = string.Concat(Sm.Right(Sm.GetGrdStr(Grd1, Row, col2), 4), Sm.GetGrdStr(Grd1, Row, col2).Substring(2, 2), Sm.Left(Sm.GetGrdStr(Grd1, Row, col2), 2));
            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            
            string TimeLog = Sm.GetGrdStr(Grd1, Row, col3).Replace(":", "");
            if (TimeLog.Length < 4 && Decimal.Parse(Sm.Left(TimeLog, 1)) != 0)
            {
                TimeLog = string.Concat("0" + TimeLog + "00"); 
            }
            else
            {
                TimeLog = string.Concat(TimeLog + "00");
            }


            Sm.CmParam<String>(ref cm, "@Tm", TimeLog);
            Sm.CmParam<String>(ref cm, "@Machine", Sm.GetGrdStr(Grd1, Row, col4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string CreateDt)
        {
            try
            {
                ClearData();
                ShowLog(CreateDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

     
        private void ShowLog(string CreateDt)
        {
            SetGrd2();
            var cm = new MySqlCommand();

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.EmpCode, B.EmpName, A.Dt, A.Tm, A.Machine "+
                    "From tblAttendanceLog A Inner Join TblEmployee B On A.EmpCode=B.EmpCode Where left(A.CreateDt, 8)='"+CreateDt+"' ",
                    new string[] 
                    { 
                        "EmpCode", 
                        "EmpName", "Dt", "Tm", "Machine"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }
   
        #endregion

        #region Additional Method

        private void ShowCSV(string FileName)
        {
            try
            {
                int count = 0;
                using (var rd = new StreamReader("" + FileName + ""))
                {
                    int Row = 0;
                    while (!rd.EndOfStream)
                    {
                        var splits = rd.ReadLine().Split(',');
                        count = splits.Count();
                        SetGrd(count);
                        Grd1.Rows.Add();
                        for (int i = 0; i < count; i++)
                        {
                            Grd1.Cells[Row, i].Value = splits[i];
                        }
                        Row++;
                    }

                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                Sm.ClearGrd(Grd1, false);

                openFileDialog1.InitialDirectory = "c:";
                openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.ShowDialog();

                TxtFileName.Text = openFileDialog1.FileName;
                ShowCSV(TxtFileName.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ShowCSV(TxtFileName.Text);
        }

        #endregion

     

       

        

    }


}
