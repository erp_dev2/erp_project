﻿#region Update
/*
    03/11/2020 [DITA/PHT] new reporting
    18/11/2020 [WED/PHT] ganti query. bukan di bulan tersebut, tapi sampai bulan tersebut. tambah filter Active Employee
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpCountBySiteAndEmployment : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<EmploymentStatus> lEmpStatus = null;
        private int mFirstColumnForEmpStatus = -1;
        internal int ColCounts = 0;

        #endregion

        #region Constructor

        public FrmRptEmpCountBySiteAndEmployment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                SetGrd();

                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                ChkActInd.Checked = true;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string SetSQL1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT concat(CAST(T.OptCode AS CHAR CHARACTER SET UTF8), '#', T.DocType) DocType, ");
            SQL.AppendLine("Case T.DocType When '1' Then CONCAT('Jumlah\r\n', T.OptDesc, '\r\nBulan 1') ELSE CONCAT('Jumlah\r\n', T.OptDesc, '\r\n Bulan 2') END AS EmpStatusName ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("SELECT OptCode, OptDesc, '1' AS DocType ");
            SQL.AppendLine("FROM TblOption WHERE OptCat = 'EmploymentStatus' ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("SELECT OptCode, OptDesc, '2' AS DocType ");
            SQL.AppendLine("FROM TblOption WHERE OptCat = 'EmploymentStatus' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("ORDER BY T.OptDesc, T.DocType; ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private string SetSQL2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT * FROM ");
            SQL.AppendLine("(   ");
            SQL.AppendLine("SELECT D.SiteCode, D.SiteName, B.EmploymentStatus, COUNT(B.EmploymentStatus) Total, Concat(E.OptCode, '#', '1') DocType ");
            SQL.AppendLine("FROM  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    SELECT EmpCode, MAX(StartDt) StartDt  ");
            SQL.AppendLine("    FROM TblEmployeePPS  ");
            SQL.AppendLine("    WHERE LEFT(StartDt, 6) <= DATE_FORMAT(DATE(CONCAT(@Yr, @Mth, '01')- INTERVAL 1 MONTH), '%Y%m') ");
            SQL.AppendLine("    GROUP BY EmpCode  ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("INNER JOIN TblEmployeePPS B ON A.EmpCode = B.EmpCode AND A.StartDt = B.StartDt  ");
            SQL.AppendLine("INNER JOIN TblEmployee C ON A.EmpCode=C.EmpCode   ");
            SQL.AppendLine("INNER JOIN TblSite D ON C.SiteCode=D.SiteCode ");
            SQL.AppendLine("INNER JOIN TblOption E ON B.EmploymentStatus=E.OptCode AND E.OptCat='EmploymentStatus' ");
            if (ChkActInd.Checked)
                SQL.AppendLine("Where (C.ResignDt IS NULL OR (C.ResignDt Is Not Null And substring(C.ResignDt, 1, 6)>DATE_FORMAT(DATE(CONCAT(@Yr, @Mth, '01')- INTERVAL 1 MONTH), '%Y%m')))  ");
            SQL.AppendLine("GROUP BY B.SiteCode, D.SiteName, B.EmploymentStatus, Concat(E.OptCode, '#', '1') ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("SELECT D.SiteCode, D.SiteName, B.EmploymentStatus, COUNT(B.EmploymentStatus) Total, Concat(E.OptCode, '#', '2') DocType ");
            SQL.AppendLine("FROM  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    SELECT EmpCode, MAX(StartDt) StartDt  ");
            SQL.AppendLine("    FROM TblEmployeePPS  ");
            SQL.AppendLine("    WHERE LEFT(StartDt, 6) <= CONCAT(@Yr, @Mth) ");
            SQL.AppendLine("    GROUP BY EmpCode  ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("INNER JOIN TblEmployeePPS B ON A.EmpCode = B.EmpCode AND A.StartDt = B.StartDt  ");
            SQL.AppendLine("INNER JOIN TblEmployee C ON A.EmpCode=C.EmpCode   ");
            SQL.AppendLine("INNER JOIN TblSite D ON C.SiteCode=D.SiteCode ");
            SQL.AppendLine("INNER JOIN TblOption E ON B.EmploymentStatus=E.OptCode AND E.OptCat='EmploymentStatus' ");
            if (ChkActInd.Checked)
                SQL.AppendLine("Where (C.ResignDt IS NULL OR (C.ResignDt Is Not Null And substring(C.ResignDt, 1, 6)>CONCAT(@Yr, @Mth)))  ");
            SQL.AppendLine("GROUP BY B.SiteCode, D.SiteName, B.EmploymentStatus, Concat(E.OptCode, '#', '2') ");

            SQL.AppendLine(") T   ");
            

            mSQL = SQL.ToString();

            return mSQL;

        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            ColCounts = Grd1.Cols.Count - 1;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-2
                        "Site Code",
                        "Site",
                       
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-2
                        100, 200
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2});

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select Distinct T2.SiteCode, T2.SiteName From(" +  SetSQL2() + Filter + ")T2;",
                    new string[]
                    { 
                        //0
                        "SiteCode", 
                        
                        //1
                        "SiteName",
                    },

                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        
                    }, true, false, false, false
                );

                lEmpStatus = new List<EmploymentStatus>();
                SetEmpStatusToColumn(SetSQL1());
                ProcessEmpTotal(SetSQL2() + Filter);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                int a = ColCounts;
                for (int r = 0; r <= lEmpStatus.Count; r++)
                {
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                    {  
                        (a + r)
                    });
                }
                Sm.SetGrdAlwaysShowSubTotal(Grd1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void SetEmpStatusToColumn(string SQL)
        {
            lEmpStatus.Clear();
            Grd1.Cols.Count = 3;
            var LatestColumn = Grd1.Cols.Count - 1;
            var cm1 = new MySqlCommand();
            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                var EmpStatusColumn = new StringBuilder();
                string Filter = " ";
                EmpStatusColumn.AppendLine(SQL);

                cn1.Open();
                cm1.Connection = cn1;
                cm1.CommandTimeout = 600;
                cm1.CommandText = EmpStatusColumn.ToString();
                Sm.CmParam<String>(ref cm1, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm1, "@Mth", Sm.GetLue(LueMth));
                var dr1 = cm1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] { "DocType", "EmpStatusName" });
                if (dr1.HasRows)
                {
                    while (dr1.Read())
                    {
                        LatestColumn += 1;
                        if (mFirstColumnForEmpStatus == -1) mFirstColumnForEmpStatus = LatestColumn;
                        lEmpStatus.Add(new EmploymentStatus()
                        {
                            DocType = Sm.DrStr(dr1, c1[0]),
                            EmpStatusName = Sm.DrStr(dr1, c1[1]),
                            Column = LatestColumn
                        });
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = Sm.DrStr(dr1, c1[1]);
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 250;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);
                    }
                }
                dr1.Close();
            }
        }

        private void ProcessEmpTotal(string SQL)
        {
            if (lEmpStatus.Count > 0)
            {
                for (int Col = mFirstColumnForEmpStatus; Col < Grd1.Cols.Count; Col++)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, Col].Value = 0m;
                }
                var l = new List<TotalEmployee>();
                ProcessTotal1(ref l, SQL);
                if (l.Count > 0)
                {
                    ProcessTotal2(ref l, ref lEmpStatus);
                }
            }
        }

        private void ProcessTotal1(ref List<TotalEmployee> l, string SQL)
        {
            var cm = new MySqlCommand();
            string Filter = " Where 0 = 0";

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL;
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "SiteName","Total", "DocType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TotalEmployee()
                        {
                            EmpStatusToColumn = GetEmpStatusColumn(Sm.DrStr(dr, c[3])),
                            SiteCode = Sm.DrStr(dr, c[0]),
                            SiteName = Sm.DrStr(dr, c[1]),
                            Total = Sm.DrDec(dr, c[2]),
                            DocType = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }

        }

        private int GetEmpStatusColumn(string DocType)
        {
            foreach (var x in lEmpStatus.Where(x => string.Compare(x.DocType, DocType) == 0))
                return x.Column;
            return -1;
        }

        private void ProcessTotal2(ref List<TotalEmployee> l, ref List<EmploymentStatus> lEmpStatus)
        {
            for (var i = 0; i < l.Count; i++)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (string.Compare(l[i].SiteCode, Sm.GetGrdStr(Grd1, Row, 1)) == 0 )
                    {
                        if (l[i].EmpStatusToColumn >= 0)
                        {
                            Grd1.Cells[Row, l[i].EmpStatusToColumn].Value = l[i].Total;
                        }
                        break;
                    }
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            var ColCount = new int[Grd1.Cols.Count - 6];
            var i = 0;
            for (int c = 6; c < Grd1.Cols.Count; c++)
            {
                ColCount[i] = c;
                i++;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, ColCount);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");

        }

        #endregion

        #endregion

        #region Class

        private class EmploymentStatus
        {
            public string EmpStatusName { get; set; }
            public string DocType { get; set; }
            public int Column { get; set; }

        }

        private class TotalEmployee
        {
            public int EmpStatusToColumn { get; set; }
            public decimal Total { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public string DocType { get; set; }
        }

        #endregion
    }
}
