﻿#region Update
/*
    12/05/2019 [TKG] New Application 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoiceRawMaterialTax : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty, mSQL = string.Empty;
        
        #endregion

        #region Constructor

        public FrmPurchaseInvoiceRawMaterialTax(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetLueDocType(ref LueDocType);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, B.DocNo As PurchaseInvoiceDocNo, ");
            SQL.AppendLine("E.TIN, Case B.DocType When '1' Then 'Log' When '2' Then 'Balok' End As DocTypeDesc, ");
            SQL.AppendLine("E.VdName, F.CityName, B.Tax, B.TaxInd ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceRawMaterialHdr B ");
            SQL.AppendLine("    On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            SQL.AppendLine("    And B.TaxInd='N' ");
            SQL.AppendLine("    And B.Tax<>0.00 ");
            SQL.AppendLine("Inner Join TblRawMaterialVerify C On B.RawMaterialVerifyDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyHdr D On C.LegalDocVerifyDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblVendor E On D.VdCode=E.VdCode ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.DocType='11' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Month",
                        "Year",
                        "Voucher#",
                        "",
                        
                        //6-10
                        "Voucher"+Environment.NewLine+"Date",
                        "PI#",
                        "",
                        "Taxpayer" +Environment.NewLine+"Identification#",
                        "Type",
                        
                        //11-14
                        "Vendor",
                        "City",
                        "Tax",
                        "Checked"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 50, 50, 110, 20, 
                        
                        //6-10
                        100, 130, 20, 180, 100,

                        //11-14
                        200, 200, 130, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 14 });
            Sm.GrdColButton(Grd1, new int[] { 5, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 6, 7, 9, 10, 11, 12, 13, 14 });
            Grd1.Cols[14].Move(2);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        #region Standard Method

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();
                var DocDt = string.Empty;
                string Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "B.DocType", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        new string[]
                        {
                            //0
                            "DocDt", 
                            
                            //1-5
                            "DocNo", "PurchaseInvoiceDocNo", "TIN", "DocTypeDesc", "VdName", 
                            
                            //6-8
                            "CityName", "Tax", "TaxInd"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = true;
                            DocDt = dr.GetString(c[0]);
                            Grd.Cells[Row, 2].Value = DocDt.Substring(4, 2);
                            Grd.Cells[Row, 3].Value = Sm.Left(DocDt, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Grd.Cells[Row, 6].Value = Sm.ConvertDate(DocDt);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 7);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 14, 8);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void SaveData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsDataNotValid()) 
                return;

            try
            {
                bool IsProcess = false;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                for (int r = 0; r< Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 1) &&
                        !Sm.GetGrdBool(Grd1, r, 14) && 
                        Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        IsProcess = true;
                        SavePurchaseInvoiceRawMaterialTaxInd(ref SQL, ref cm, r);
                    }
                }
                if (IsProcess)
                {
                    var cml = new List<MySqlCommand>();

                    cm.CommandText = SQL.ToString();
                    cml.Add(cm);
                    Sm.ExecCommands(cml);

                    Sm.StdMsg(mMsgType.Info, "Updating process is completed.");
                    Sm.ClearGrd(Grd1, false);
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "You need to process minimum 1 document.");
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SavePurchaseInvoiceRawMaterialTaxInd(ref StringBuilder SQL, ref MySqlCommand cm, int r)
        {
            SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr Set TaxInd='Y' Where DocNo=@DocNo"+r.ToString());
            SQL.AppendLine(" And TaxInd='N'; ");

            Sm.CmParam<String>(ref cm, "@DocNo" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
        }

        #endregion

        #region Additional Method

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Log' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Balok' As Col2;");

            Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsDataNotValid()
        {
            return IsGrdEmpty() || IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsChosen = false;
            for (int r = 0; r< Grd1.Rows.Count - 1; r++)
            {
                if (!IsChosen && Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length > 0) IsChosen = true;
                if (Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 2).Length>0)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Month is empty.")) return true;
                }
            }
            if (!IsChosen)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 record.");
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
                e.DoDefault = false;

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPurchaseInvoiceRawMaterial("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmPurchaseInvoiceRawMaterial("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length>0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        #endregion

        #endregion
    }
}
