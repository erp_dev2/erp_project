﻿namespace RunSystem
{
    partial class FrmPosPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            TenTec.Windows.iGridLib.iGColPattern iGColPattern1 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern2 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern3 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern4 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern5 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern6 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern7 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern8 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern9 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGRowPattern iGRowPattern1 = new TenTec.Windows.iGridLib.iGRowPattern();
            TenTec.Windows.iGridLib.iGRowPattern iGRowPattern2 = new TenTec.Windows.iGridLib.iGRowPattern();
            TenTec.Windows.iGridLib.iGRowPattern iGRowPattern3 = new TenTec.Windows.iGridLib.iGRowPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern1 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern2 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern3 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern4 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern5 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern6 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern7 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern8 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern9 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern10 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern11 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern12 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern13 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern14 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern15 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern16 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern17 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern18 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern19 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern20 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern21 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern22 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern23 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern24 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern25 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern26 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern27 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern28 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern29 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGCellPattern iGCellPattern30 = new TenTec.Windows.iGridLib.iGCellPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern10 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern11 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern12 = new TenTec.Windows.iGridLib.iGColPattern();
            TenTec.Windows.iGridLib.iGColPattern iGColPattern13 = new TenTec.Windows.iGridLib.iGColPattern();
            this.Grd1Col0CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col0ColHdrStyle = new TenTec.Windows.iGridLib.iGColHdrStyle(true);
            this.Grd1Col1CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col2CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col3CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col4CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col5CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col6CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col7CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col8CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.Grd1Col8ColHdrStyle = new TenTec.Windows.iGridLib.iGColHdrStyle(true);
            this.iGrid1Col0CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.iGrid1Col0ColHdrStyle = new TenTec.Windows.iGridLib.iGColHdrStyle(true);
            this.iGrid1Col1CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.iGrid1Col2CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.iGrid1Col3CellStyle = new TenTec.Windows.iGridLib.iGCellStyle(true);
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtTotalSales = new System.Windows.Forms.TextBox();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtCharge = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtGrandTotal = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtOutstanding = new System.Windows.Forms.TextBox();
            this.TxtPayment = new System.Windows.Forms.TextBox();
            this.GrdPaymentBy = new TenTec.Windows.iGridLib.iGrid();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdPaymentBy)).BeginInit();
            this.SuspendLayout();
            // 
            // Grd1Col0CellStyle
            // 
            this.Grd1Col0CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // Grd1Col0ColHdrStyle
            // 
            this.Grd1Col0ColHdrStyle.BackColor = System.Drawing.Color.Silver;
            this.Grd1Col0ColHdrStyle.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd1Col0ColHdrStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.MiddleCenter;
            // 
            // Grd1Col1CellStyle
            // 
            this.Grd1Col1CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd1Col1CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.TopRight;
            // 
            // Grd1Col2CellStyle
            // 
            this.Grd1Col2CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd1Col2CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.MiddleRight;
            // 
            // Grd1Col3CellStyle
            // 
            this.Grd1Col3CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd1Col3CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.TopRight;
            // 
            // Grd1Col4CellStyle
            // 
            this.Grd1Col4CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // iGrid1Col0CellStyle
            // 
            this.iGrid1Col0CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // iGrid1Col0ColHdrStyle
            // 
            this.iGrid1Col0ColHdrStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iGrid1Col0ColHdrStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.TopCenter;
            // 
            // iGrid1Col1CellStyle
            // 
            this.iGrid1Col1CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // iGrid1Col2CellStyle
            // 
            this.iGrid1Col2CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iGrid1Col2CellStyle.TextAlign = TenTec.Windows.iGridLib.iGContentAlignment.TopRight;
            // 
            // iGrid1Col3CellStyle
            // 
            this.iGrid1Col3CellStyle.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnCancel);
            this.panel1.Controls.Add(this.BtnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 350);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(678, 51);
            this.panel1.TabIndex = 14;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.ForeColor = System.Drawing.Color.Maroon;
            this.BtnCancel.Location = new System.Drawing.Point(529, 8);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(137, 31);
            this.BtnCancel.TabIndex = 13;
            this.BtnCancel.TabStop = false;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOK
            // 
            this.BtnOK.Font = new System.Drawing.Font("Franklin Gothic Medium", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOK.ForeColor = System.Drawing.Color.Black;
            this.BtnOK.Location = new System.Drawing.Point(388, 7);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(135, 31);
            this.BtnOK.TabIndex = 12;
            this.BtnOK.TabStop = false;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(27, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 18);
            this.label1.TabIndex = 15;
            this.label1.Text = "Total Sales";
            // 
            // TxtTotalSales
            // 
            this.TxtTotalSales.BackColor = System.Drawing.SystemColors.Control;
            this.TxtTotalSales.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtTotalSales.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalSales.ForeColor = System.Drawing.Color.Black;
            this.TxtTotalSales.Location = new System.Drawing.Point(145, 12);
            this.TxtTotalSales.Name = "TxtTotalSales";
            this.TxtTotalSales.ReadOnly = true;
            this.TxtTotalSales.Size = new System.Drawing.Size(157, 20);
            this.TxtTotalSales.TabIndex = 16;
            this.TxtTotalSales.TabStop = false;
            this.TxtTotalSales.Text = "0";
            this.TxtTotalSales.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Grd1
            // 
            this.Grd1.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.BorderColor = System.Drawing.Color.White;
            this.Grd1.BorderStyle = TenTec.Windows.iGridLib.iGBorderStyle.Flat;
            iGColPattern1.CellStyle = this.Grd1Col0CellStyle;
            iGColPattern1.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern1.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern1.Text = "Payment";
            iGColPattern1.Width = 120;
            iGColPattern2.CellStyle = this.Grd1Col1CellStyle;
            iGColPattern2.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern2.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern2.Text = "Amount";
            iGColPattern2.Width = 100;
            iGColPattern3.CellStyle = this.Grd1Col2CellStyle;
            iGColPattern3.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern3.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern3.Text = "Charge";
            iGColPattern3.Width = 100;
            iGColPattern4.CellStyle = this.Grd1Col3CellStyle;
            iGColPattern4.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern4.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern4.Text = "Total";
            iGColPattern4.Width = 120;
            iGColPattern5.CellStyle = this.Grd1Col4CellStyle;
            iGColPattern5.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern5.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern5.Text = "Card No.";
            iGColPattern5.Width = 175;
            iGColPattern6.CellStyle = this.Grd1Col5CellStyle;
            iGColPattern6.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern6.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern6.Text = "PayCode";
            iGColPattern6.Visible = false;
            iGColPattern7.CellStyle = this.Grd1Col6CellStyle;
            iGColPattern7.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern7.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern7.Text = "Charge";
            iGColPattern7.Visible = false;
            iGColPattern8.CellStyle = this.Grd1Col7CellStyle;
            iGColPattern8.ColHdrStyle = this.Grd1Col0ColHdrStyle;
            iGColPattern8.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern8.Text = "IsCash";
            iGColPattern8.Visible = false;
            iGColPattern9.CellStyle = this.Grd1Col8CellStyle;
            iGColPattern9.ColHdrStyle = this.Grd1Col8ColHdrStyle;
            iGColPattern9.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            iGColPattern9.Text = "NetPayAmt";
            iGColPattern9.Visible = false;
            this.Grd1.Cols.AddRange(new TenTec.Windows.iGridLib.iGColPattern[] {
            iGColPattern1,
            iGColPattern2,
            iGColPattern3,
            iGColPattern4,
            iGColPattern5,
            iGColPattern6,
            iGColPattern7,
            iGColPattern8,
            iGColPattern9});
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Font = new System.Drawing.Font("Franklin Gothic Medium", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColsEdge = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.White, 0, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.GroupRows = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.Horizontal = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.HorizontalExtended = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GridLines.Vertical = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.Transparent, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.GridLines.VerticalLastCol = new TenTec.Windows.iGridLib.iGPenStyle(System.Drawing.Color.White, 1, System.Drawing.Drawing2D.DashStyle.Solid);
            this.Grd1.Header.AllowPress = false;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.HighlightSelCells = false;
            this.Grd1.Location = new System.Drawing.Point(12, 111);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            iGRowPattern1.Height = 20;
            iGRowPattern1.Sortable = false;
            iGRowPattern2.Height = 20;
            iGRowPattern2.Sortable = false;
            iGRowPattern3.Height = 20;
            iGRowPattern3.Sortable = false;
            this.Grd1.Rows.AddRange(new TenTec.Windows.iGridLib.iGRowPattern[] {
            iGRowPattern1,
            iGRowPattern2,
            iGRowPattern3}, new TenTec.Windows.iGridLib.iGCellPattern[] {
            iGCellPattern1,
            iGCellPattern2,
            iGCellPattern3,
            iGCellPattern4,
            iGCellPattern5,
            iGCellPattern6,
            iGCellPattern7,
            iGCellPattern8,
            iGCellPattern9,
            iGCellPattern10,
            iGCellPattern11,
            iGCellPattern12,
            iGCellPattern13,
            iGCellPattern14,
            iGCellPattern15,
            iGCellPattern16,
            iGCellPattern17,
            iGCellPattern18,
            iGCellPattern19,
            iGCellPattern20,
            iGCellPattern21,
            iGCellPattern22,
            iGCellPattern23,
            iGCellPattern24,
            iGCellPattern25,
            iGCellPattern26,
            iGCellPattern27,
            iGCellPattern28,
            iGCellPattern29,
            iGCellPattern30});
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SelCellsBackColor = System.Drawing.Color.Transparent;
            this.Grd1.SelCellsBackColorNoFocus = System.Drawing.Color.Transparent;
            this.Grd1.SelCellsForeColor = System.Drawing.Color.Transparent;
            this.Grd1.SelCellsForeColorNoFocus = System.Drawing.Color.Transparent;
            this.Grd1.Size = new System.Drawing.Size(654, 212);
            this.Grd1.TabIndex = 57;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.CellClick += new TenTec.Windows.iGridLib.iGCellClickEventHandler(this.Grd1_CellClick);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Grd1_KeyPress);
            // 
            // TxtCharge
            // 
            this.TxtCharge.BackColor = System.Drawing.SystemColors.Control;
            this.TxtCharge.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtCharge.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCharge.ForeColor = System.Drawing.Color.Black;
            this.TxtCharge.Location = new System.Drawing.Point(145, 37);
            this.TxtCharge.Name = "TxtCharge";
            this.TxtCharge.ReadOnly = true;
            this.TxtCharge.Size = new System.Drawing.Size(157, 20);
            this.TxtCharge.TabIndex = 79;
            this.TxtCharge.TabStop = false;
            this.TxtCharge.Text = "0";
            this.TxtCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(27, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 18);
            this.label2.TabIndex = 80;
            this.label2.Text = "Charge";
            // 
            // TxtGrandTotal
            // 
            this.TxtGrandTotal.BackColor = System.Drawing.SystemColors.Control;
            this.TxtGrandTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtGrandTotal.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrandTotal.ForeColor = System.Drawing.Color.Black;
            this.TxtGrandTotal.Location = new System.Drawing.Point(145, 68);
            this.TxtGrandTotal.Name = "TxtGrandTotal";
            this.TxtGrandTotal.ReadOnly = true;
            this.TxtGrandTotal.Size = new System.Drawing.Size(157, 20);
            this.TxtGrandTotal.TabIndex = 82;
            this.TxtGrandTotal.TabStop = false;
            this.TxtGrandTotal.Text = "0";
            this.TxtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(560, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 18);
            this.label6.TabIndex = 87;
            this.label6.Text = "Outstanding";
            // 
            // TxtOutstanding
            // 
            this.TxtOutstanding.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtOutstanding.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstanding.Location = new System.Drawing.Point(363, 24);
            this.TxtOutstanding.Name = "TxtOutstanding";
            this.TxtOutstanding.ReadOnly = true;
            this.TxtOutstanding.Size = new System.Drawing.Size(303, 36);
            this.TxtOutstanding.TabIndex = 88;
            this.TxtOutstanding.TabStop = false;
            this.TxtOutstanding.Text = "0";
            this.TxtOutstanding.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtPayment
            // 
            this.TxtPayment.BackColor = System.Drawing.SystemColors.Control;
            this.TxtPayment.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtPayment.Font = new System.Drawing.Font("Franklin Gothic Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPayment.ForeColor = System.Drawing.Color.Black;
            this.TxtPayment.Location = new System.Drawing.Point(467, 83);
            this.TxtPayment.Name = "TxtPayment";
            this.TxtPayment.ReadOnly = true;
            this.TxtPayment.Size = new System.Drawing.Size(199, 22);
            this.TxtPayment.TabIndex = 89;
            this.TxtPayment.TabStop = false;
            this.TxtPayment.Text = "0";
            this.TxtPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // GrdPaymentBy
            // 
            this.GrdPaymentBy.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.GrdPaymentBy.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.GrdPaymentBy.BackColorOddRows = System.Drawing.Color.White;
            this.GrdPaymentBy.BorderStyle = TenTec.Windows.iGridLib.iGBorderStyle.Flat;
            iGColPattern10.CellStyle = this.iGrid1Col0CellStyle;
            iGColPattern10.ColHdrStyle = this.iGrid1Col0ColHdrStyle;
            iGColPattern10.Text = "Pay Code";
            iGColPattern10.Width = 100;
            iGColPattern11.CellStyle = this.iGrid1Col1CellStyle;
            iGColPattern11.ColHdrStyle = this.iGrid1Col0ColHdrStyle;
            iGColPattern11.Text = "Payment Name";
            iGColPattern11.Width = 120;
            iGColPattern12.CellStyle = this.iGrid1Col2CellStyle;
            iGColPattern12.ColHdrStyle = this.iGrid1Col0ColHdrStyle;
            iGColPattern12.Text = "Charge";
            iGColPattern12.Width = 100;
            iGColPattern13.CellStyle = this.iGrid1Col3CellStyle;
            iGColPattern13.ColHdrStyle = this.iGrid1Col0ColHdrStyle;
            iGColPattern13.Text = "IsCash";
            iGColPattern13.Visible = false;
            iGColPattern13.Width = 50;
            this.GrdPaymentBy.Cols.AddRange(new TenTec.Windows.iGridLib.iGColPattern[] {
            iGColPattern10,
            iGColPattern11,
            iGColPattern12,
            iGColPattern13});
            this.GrdPaymentBy.DefaultRow.Height = 20;
            this.GrdPaymentBy.ForeColor = System.Drawing.SystemColors.WindowText;
            this.GrdPaymentBy.FrozenArea.ColCount = 3;
            this.GrdPaymentBy.FrozenArea.SortFrozenRows = true;
            this.GrdPaymentBy.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.GrdPaymentBy.Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.GrdPaymentBy.Header.UseXPStyles = false;
            this.GrdPaymentBy.HScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.GrdPaymentBy.Location = new System.Drawing.Point(30, 165);
            this.GrdPaymentBy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GrdPaymentBy.Name = "GrdPaymentBy";
            this.GrdPaymentBy.ProcessTab = false;
            this.GrdPaymentBy.ReadOnly = true;
            this.GrdPaymentBy.RowMode = true;
            this.GrdPaymentBy.RowTextStartColNear = 3;
            this.GrdPaymentBy.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.GrdPaymentBy.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.GrdPaymentBy.SearchAsType.SearchCol = null;
            this.GrdPaymentBy.Size = new System.Drawing.Size(420, 158);
            this.GrdPaymentBy.TabIndex = 92;
            this.GrdPaymentBy.TreeCol = null;
            this.GrdPaymentBy.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.GrdPaymentBy.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.GrdPaymentBy.DoubleClick += new System.EventHandler(this.GrdPaymentBy_DoubleClick);
            this.GrdPaymentBy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GrdPaymentBy_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(361, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 18);
            this.label3.TabIndex = 93;
            this.label3.Text = "Total Paid";
            // 
            // FrmPosPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(678, 401);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.GrdPaymentBy);
            this.Controls.Add(this.TxtPayment);
            this.Controls.Add(this.TxtOutstanding);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtGrandTotal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtCharge);
            this.Controls.Add(this.Grd1);
            this.Controls.Add(this.TxtTotalSales);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPosPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payment";
            this.Load += new System.EventHandler(this.FrmPosPayment_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmPosPayment_KeyPress);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrdPaymentBy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtTotalSales;
        private System.Windows.Forms.TextBox TxtCharge;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtGrandTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtPayment;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col0CellStyle;
        private TenTec.Windows.iGridLib.iGColHdrStyle Grd1Col0ColHdrStyle;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col1CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col2CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col3CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col4CellStyle;
        private TenTec.Windows.iGridLib.iGrid GrdPaymentBy;
        private TenTec.Windows.iGridLib.iGCellStyle iGrid1Col0CellStyle;
        private TenTec.Windows.iGridLib.iGColHdrStyle iGrid1Col0ColHdrStyle;
        private TenTec.Windows.iGridLib.iGCellStyle iGrid1Col1CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle iGrid1Col2CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle iGrid1Col3CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col5CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col6CellStyle;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col7CellStyle;
        public System.Windows.Forms.TextBox TxtOutstanding;
        public TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Label label3;
        private TenTec.Windows.iGridLib.iGCellStyle Grd1Col8CellStyle;
        private TenTec.Windows.iGridLib.iGColHdrStyle Grd1Col8ColHdrStyle;
    }
}