﻿#region Update
/*
    11/12/2018 [WED] tambah sequence
    30/08/2019 [DITA] Bug saat edit 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPhase : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmPhaseFind FrmFind;

        #endregion

        #region Constructor

        public FrmPhase(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPhaseCode, TxtPhaseName, TxtSequence }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    TxtPhaseCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPhaseCode, TxtPhaseName, TxtSequence }, false);
                    TxtPhaseCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtPhaseName, TxtSequence }, false);
                    ChkActiveInd.Properties.ReadOnly = false;
                    TxtPhaseName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPhaseCode, TxtPhaseName,
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtSequence }, 11);
            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPhaseFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActiveInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPhaseCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
           
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPhase(PhaseCode, PhaseName, ActInd, Sequence, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PhaseCode, @PhaseName, @ActInd, @Sequence, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update PhaseName=@PhaseName, ActInd=@ActInd, Sequence = @Sequence, ");
                SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PhaseCode", TxtPhaseCode.Text);
                Sm.CmParam<String>(ref cm, "@PhaseName", TxtPhaseName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@Sequence", (TxtSequence.Text.Length > 0) ? TxtSequence.Text : "0");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPhaseCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PhaseCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PhaseCode, A.PhaseName, A.ActInd, A.Sequence ");
            SQL.AppendLine("From TblPhase A ");
            SQL.AppendLine("Where A.PhaseCode=@PhaseCode; ");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PhaseCode", PhaseCode);
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                        new string[] 
                        {
                            "PhaseCode", 
                            "PhaseName", "ActInd", "Sequence"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPhaseCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPhaseName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                            TxtSequence.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 11);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPhaseCode, "Phase code", false) ||
                Sm.IsTxtEmpty(TxtPhaseName, "Phase name", false) ||
                IsPhaseCodeExisted() ||
                IsSequenceExists();
        }

        private bool IsSequenceExists()
        {
            if(TxtSequence.Text.Length > 0 && TxtSequence.Text != "0")
            {
                var SQL = new StringBuilder();
                string mPhaseCode = string.Empty;

                SQL.AppendLine("Select PhaseCode ");
                SQL.AppendLine("From TblPhase ");
                SQL.AppendLine("Where Sequence = @Param And Sequence != 0 ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), TxtSequence.Text) && TxtSequence.Text!=Sm.GetValue("Select Sequence From TblPhase Where PhaseCode = @Param",TxtPhaseCode.Text))
                {
                    mPhaseCode = Sm.GetValue(SQL.ToString(), TxtSequence.Text);
                    Sm.StdMsg(mMsgType.Warning, "Another phase (code : " + mPhaseCode + ") has the same sequence.");
                    return true;
                }
            }

            return false;
        }

        private bool IsPhaseCodeExisted()
        {
            if (!TxtPhaseCode.Properties.ReadOnly &&
                Sm.IsDataExist("Select PhaseCode From TblPhase Where PhaseCode=@Param;", TxtPhaseCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Phase code ( " + TxtPhaseCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtSequence_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtSequence, 11);
            }
        }

        #endregion

        #endregion
    }
}
