﻿#region Update
/*
    16/10/2017 [Ari] tambah printout
    23/10/2017 [HAR] nilai allowance ambil dari level bukan emp salary
    25/10/2017 [HAR] bug fixing PIC di voucher request ambil dari usercode di master employee
    15/12/2017 [HAR] validasi jika travel lebih dari 21 hari
    22/12/2017 [TKG] tambah filter dept+site berdasarkan group
    12/02/2018 [HAR] bisa mengedit dokumen dari web mengubah status webind dari W ke D
    02/05/2018 [HAR] waktu edit jika data dari portal web, dimunculin ratenyA
    09/05/2018 [TKG] Approval by department
    14/05/2018 [HAR] Bug saat update webInd kena kesemua document 
    22/05/2018 [HAR] tambah validasi kode transport berdaarkan parameter untuk TWC
    06/06/2018 [HAR] bug saat update voucher request dan validasi tambahan start date lbh end dari end date
    05/07/2018 [HAR] bug saat update voucher request jika sdh ada approval
    12/07/2018 [HAR] bug saat update voucher request dan validasi jika status di cancel tidak bisa mengedit
    17/09/2018 [HAR] printout ada validasi HOind dari site
    22/09/2018 [HAR] printout sppd dari voucher request pindah kesini
    27/09/2018 [TKG] Travel Request berdasarkan status jabatan + Travel Request bisa mengakomodir untuk input currency lain selain IDR
    29/10/2018 [HAR] bug saat data dari web, detasering belum diaktifkan  
    30/10/2018 [HAR] bug set day detasering beda bulan
   show
    20/12/2018 [MEY] Printout dijadikan satu dengan leader dan pengikut
    21/12/2018 [MEY] Feedback city ambil dari TblTravelRequestHdr, bukan dari TblCity
    31/12/2018 [HAR] BUG saat add dan hapus data detail maupun summary
    11/02/2019 [TKG] approval menggunakan validasi approval group
    16/04/2019 [WED] bug : kalau cancel dokumen, walaupun create nya dari desktop (WebInd = 'D'), tetep bisa cancel
    07/05/2019 [DITA] Ubah nama jabatan di print out travel request berdasarkan parameter
    18/06/2019 [DITA] Ubah nama jabatan di print out Printout Travel Management diganti labelnya untuk kantor pusat berdasarkan parameter
    11/11/2019 [WED/SRN] TblDocumentApprovalGroupHdr --> TblDocApprovalGroupHdr; TblDocumentApprovalGroupDtl --> TblDocApprovalGroupDtl
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    30/08/2021 [TKG/ALL] ubah validasi saat cancel data andaikata sudah dibuat voucher, tidak bisa dicancel lagi. 
    13/09/2022 [HAR/GSS] ShowTravelRequestHdr data dari ess ketika muncul di desktop di bagian city munculnya kode city, krn ESS dari master city desktop input manual
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTravelRequest : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "", mWebInd = "", mDocTitle = "";
        internal bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;
        iGCell fCell;
        bool fAccept;
        internal FrmTravelRequestFind FrmFind;
        private bool 
            Detasering = false, 
            mIsApprovalByDept = false;
        private string
           mPosNameTravelRequestForHO = string.Empty,
           mPosNameTravelRequestForNonHO = string.Empty,
           mSignNameTravelRequestForHO = string.Empty,
           mSignNameTravelRequestForNonHO = string.Empty;

        #endregion

        #region Constructor

        public FrmTravelRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetLueTransportTravel(ref LueTransport);
                SetGrd();
                LueAllowance.Visible = false;
                mIsApprovalByDept = IsApprovalByDept();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mWebInd = "D";
            mDocTitle = Sm.GetParameter("DocTitle");
            mPosNameTravelRequestForHO = Sm.GetParameter("PosNameTravelRequestForHO");
            mPosNameTravelRequestForNonHO = Sm.GetParameter("PosNameTravelRequestForNonHO");
            mSignNameTravelRequestForHO = Sm.GetParameter("SignNameTravelRequestForHO");
            mSignNameTravelRequestForNonHO = Sm.GetParameter("SignNameTravelRequestForNonHO");
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Dno",
                        "Employee Code", 
                        "Employee",
                        "Meal Allowance",
                        "Daily Allowance",

                        //6-10
                        "City Transport", 
                        "Transport",
                        "Accomodation", 
                        "Allowance",
                        "Detasering",

                        //11-13
                        "Total",
                        "Voucher Request#",
                        "Voucher#",
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        10, 100, 150, 150, 150, 
                        
                        //6-10
                        150,  150, 150, 150, 150, 
                        //11-13
                        150, 150, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 12 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });

            #endregion
        
            #region Grid 2
            Grd2.Header.Rows.Count = 2;
            Grd2.Cols.Count = 20;
            Grd2.FrozenArea.ColCount = 4;

            SetGrdHdr(ref Grd2, 0, 0, "Dno", 2, 80);
            SetGrdHdr(ref Grd2, 0, 1, "", 2, 20);
            SetGrdHdr(ref Grd2, 0, 2, "Employee" + Environment.NewLine + "Code", 2, 100);
            SetGrdHdr(ref Grd2, 0, 3, "Employee", 2, 200);
            SetGrdHdr2(ref Grd2, 1, 4, "Breakfast", 5);
            SetGrdHdr2(ref Grd2, 1, 9, "Lunch", 5);
            SetGrdHdr2(ref Grd2, 1, 14, "Dinner", 5);
            SetGrdHdr(ref Grd2, 0, 19, "Total", 2, 120);

            Sm.GrdColButton(Grd2, new int[] { 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 2, 4, 5, 6, 9, 10, 11, 14, 15, 16 }, false);
            Sm.GrdFormatDec(Grd2, new int[] { 7, 8, 12, 13, 17, 18, 19 }, 0);
            Sm.SetGrdProperty(Grd2, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 14, 15, 16, 17, 19 });

            #endregion
       
            #region Grid 3

            Grd3.Cols.Count = 10;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "Dno",
                        //1-5
                        "",
                        "Employee Code", 
                        "Employee",
                        "Grd Level Code",
                        "Level Code",
                        //6-9
                        "Level Code Dno",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                    },
                     new int[] 
                    {
                        //0
                        10,
                        //1-5
                        20, 100, 200, 120, 80,  
                        //6-9
                        80, 150, 100, 150
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdFormatDec(Grd3, new int[] { 7, 8, 9 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 2, 4, 5, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9 });

            #endregion
         
            #region Grid 4

            Grd4.Cols.Count = 11;
            Grd4.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "Dno",
                        //1-5
                        "",
                        "Employee Code", 
                        "Employee",
                        "GrdLvlCode",
                        "LvlCode",
                        //6-10
                        "LvlCodeDNo",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        10, 
                        //1-5
                        20, 100, 200,  120, 80,  
                        //6-10
                        80, 150, 100, 150, 200
                    }
                );
            Sm.GrdFormatDec(Grd4, new int[] { 7, 8, 9 }, 0);
            Sm.GrdColButton(Grd4, new int[] { 1 });
            Sm.GrdColInvisible(Grd4, new int[] { 0, 1, 2, 4, 5, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });

            #endregion
         
            #region Grid 5

            Grd5.Cols.Count = 12;
            Grd5.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "Dno",
                        //1-5
                        "",
                        "Employee Code", 
                        "Employee",
                        "Office",
                        "GrdLvlCode",
                        //6-10
                        "LvlCode",
                        "LvlCodeDno",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        10, 
                        //1-5
                        20, 100, 200, 50, 120,  
                        //6-10
                        80, 80, 150, 100, 150, 
                        //11
                        200
                    }
                );
            Sm.GrdColButton(Grd5, new int[] { 1 });
            Sm.GrdColCheck(Grd5, new int[] { 4 });
            Sm.GrdFormatDec(Grd5, new int[] { 8, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd5, new int[] { 0, 1, 2, 5, 6, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });

            #endregion
         
            #region Grid 6

            Grd6.Cols.Count = 12;
            Grd6.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] 
                    {
                        //0
                        "Dno",
                        //1-5
                        "",
                        "Employee Code", 
                        "Employee",
                        "Office",
                        "GrdLvlCode",
                        //6-10
                        "LvlCode",
                        "LvlDno",
                        "Rate",
                        "Quantity",
                        "Total Allowance",
                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        10, 
                        //1-5
                        20, 100, 200, 50, 120,  
                        //6-10
                        80, 80, 150, 100, 150, 
                        //11
                        200
                    }
                );
            Sm.GrdFormatDec(Grd6, new int[] { 8, 9, 10 }, 0);
            Sm.GrdColCheck(Grd6, new int[] { 4 });
            Sm.GrdColButton(Grd6, new int[] { 1 });
            Sm.GrdColInvisible(Grd6, new int[] { 0, 1, 2, 5, 6, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            #endregion

            #region Grid 7

            Grd7.Cols.Count = 13;
            Grd7.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd7,
                    new string[] 
                    {
                        //0
                        "Dno",
                        //1-5
                        "",
                        "Employee Code", 
                        "Employee",
                        "Allowance Code",
                        "Allowance",
                        //6-10
                        "GrdLvlCode",
                        "LvlCode",
                        "LvlCodeDno",
                        "Rate",
                        "Quantity",
                        //11-12
                        "Total Allowance",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        10, 
                        //1-5
                        20, 100, 200, 50, 150,  
                        //6-10
                        120, 80, 80, 150, 100,  
                        //11-12
                        100, 150, 200
                    }
                );
            Sm.GrdColButton(Grd7, new int[] { 1 });
            Sm.GrdFormatDec(Grd7, new int[] { 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd7, new int[] { 0, 1, 2, 4, 6, 7, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });

            #endregion
         }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 120;

            SetGrdHdr(ref Grd, 0, col, "GrdLvlCode", 1, 80);
            SetGrdHdr(ref Grd, 0, col + 1, "LvlCode", 1, 80);
            SetGrdHdr(ref Grd, 0, col + 2, "LvlCodeDno", 1, 80);
            SetGrdHdr(ref Grd, 0, col + 3, "Rate", 1, 80);
            SetGrdHdr(ref Grd, 0, col + 4, "Quantity", 1, 80);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd5, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd6, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd7, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, MeeCityCode,
                        DteStartDt, DteEndDt, TmeStart, TmeEnd, MeeTravelService, TxtPICCode, TxtPICName,
                        LueSite, MeeResult, LueTransport, LueSiteCode2, MeeRemark, 
                    }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;
                    Grd5.ReadOnly = true;
                    Grd6.ReadOnly = true;
                    Grd7.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt,  MeeCityCode,
                      DteStartDt, DteEndDt, TmeStart, TmeEnd, MeeTravelService,
                      LueSite, MeeResult, LueTransport, LueSiteCode2,  MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;
                    Grd5.ReadOnly = false;
                    Grd6.ReadOnly = false;
                    Grd7.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 8, 13, 18 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 8 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 7, 8 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 4, 8, 9, 11 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4, 9, 11 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 1, 5, 10, 12 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      MeeCancelReason
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    if (mWebInd == "W" && TxtStatus.Text != "Cancelled")
                    {
                        Grd1.ReadOnly = false;
                        Grd2.ReadOnly = false;
                        Grd3.ReadOnly = false;
                        Grd4.ReadOnly = false;
                        Grd5.ReadOnly = false;
                        Grd6.ReadOnly = false;
                        Grd7.ReadOnly = false;
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 8, 13, 18 });
                        Sm.GrdColReadOnly(false, true, Grd3, new int[] { 8 });
                        Sm.GrdColReadOnly(false, true, Grd4, new int[] { 7, 8 });
                        Sm.GrdColReadOnly(false, true, Grd5, new int[] { 4, 8, 9, 11 });
                        Sm.GrdColReadOnly(false, true, Grd6, new int[] { 4, 9, 11 });
                        Sm.GrdColReadOnly(false, true, Grd7, new int[] { 1, 5, 10, 12 });
                    }
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, MeeCityCode, TxtStatus,
                DteStartDt, DteEndDt, TmeStart, TmeEnd, MeeTravelService, TxtPICCode, TxtPICName,
                LueSite, MeeResult, LueTransport,  MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 8, 12, 13, 17, 18 });
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 7, 8, 9 });
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 6, 7, 8 });
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 8, 9, 10 });
            Sm.ClearGrd(Grd6, true);
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 8, 9, 10 });
            Sm.ClearGrd(Grd7, true);
            Sm.SetGrdNumValueZero(ref Grd7, 0, new int[] { 9, 10, 11 });
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTravelRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueSiteCode(ref LueSite, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueSiteCode2(ref LueSiteCode2, string.Empty);
                TxtStatus.EditValue = "Outstanding";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                ParPrint();
             //   if (Grd1.Rows.Count > 1 && Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
               // {
                  //  for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                    //{
                       // ParPrint(i + 1);
                       // ParPrint3(Sm.GetGrdStr(Grd1, i, 1));
                   // }
               // }
                ParPrint3();
                ParPrint2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            IsDetaseringActive();

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TravelRequest", "TblTravelRequestHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTravelRequestHdr(DocNo));


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl7(DocNo, Row));
            }

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl(DocNo, Row));
            }

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl2(DocNo, Row));
            }

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl3(DocNo, Row));
            }

            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl4(DocNo, Row));
            }

            for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0) cml.Add(SaveTravelRequestDtl5(DocNo, Row));
            }

            for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd7, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl6(DocNo, Row));
            }

            Sm.ExecCommands(cml);
            InsertDataVR(DocNo);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                IsDateNotValid()||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                Sm.IsLueEmpty(LueSite, "Site") ||
                Sm.IsTxtEmpty(TxtPICCode, "PIC", false) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdEmpty();
        }

        private bool IsDetaseringActive()
        {
            //decimal dt1 = Decimal.Parse(Sm.GetDte(DteEndDt).Substring(0, 8));
            //decimal dt2 = Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8));
            double Rangedt = (DteEndDt.DateTime - DteStartDt.DateTime).TotalDays + 1;
            if (Rangedt >= 21)
            {
                Sm.StdMsg(mMsgType.Warning, "Travel request exceed 21 days, detasering was active.");
                Detasering = true;
                UpdateDetaseringColumn();
                ComputeSummaryAllowance();
            }
            return false;
        }


        private bool IsDateNotValid()
        {
            decimal dt1 = Decimal.Parse(Sm.GetDte(DteEndDt).Substring(0, 8));
            decimal dt2 = Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8));

            if (dt2>dt1)
            {
                Sm.StdMsg(mMsgType.Warning, "date not valid.");
                return true;
            }
            return false;
        }

        private void UpdateDetaseringColumn()
        {
            string salaryStr =  string.Empty; 
            decimal SalaryDec = 0;

            //menginap dalam unit kali 50%
            if (Detasering && Sm.GetLue(LueSiteCode2).Length > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                {
                    string empCode = Sm.GetGrdStr(Grd1, Row, 2);
                      salaryStr = Sm.GetValue("Select C.Amt "+
                       "from TblEmpsalaryHdr A "+
                       "Inner Join  "+
                       "( "+
                       "    Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  "+
                       "    From TblEmpSalaryHdr "+
                       "    Where EmpCode = '"+empCode+"' "+
                       "    Group BY EmpCode "+
                       ")B On Concat(A.DocDt, A.DocNo) = B.KeyCode  "+
                       "Inner Join TblEmpsalaryDtl C On A.DocNo = C.DocNo "+
                       "Inner Join "+
                       "( "+
                       "    Select DocNo, MAX(Dno) Dno From TblEmpsalaryDtl "+
                       "    Group BY DocNo "+
                       ")D on C.DocNo = D.DocNo And C.Dno = D.Dno "+
                       "Where A.Status = 'A' And  A.EmpCode = '"+empCode+"' ");
                    if(salaryStr.Length>0)
                        SalaryDec = Decimal.Parse(salaryStr);
                    Grd1.Cells[Row, 10].Value = (SalaryDec * 50 /100);
                }
            }

            //menginap diluar unit 75%
            if (Detasering && Sm.GetLue(LueSiteCode2).Length == 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                {
                    string empCode = Sm.GetGrdStr(Grd1, Row, 2);
                    salaryStr = Sm.GetValue("Select C.Amt " +
                     "from TblEmpsalaryHdr A " +
                     "Inner Join  " +
                     "( " +
                     "    Select EmpCOde, Max(Concat(DocDt, DocNO)) As KeyCode  " +
                     "    From TblEmpSalaryHdr " +
                     "    Where EmpCode = '" + empCode + "'" +
                     "    Group BY EmpCode " +
                     ")B On Concat(A.DocDt, A.DocNo) = B.KeyCode  " +
                     "Inner Join TblEmpsalaryDtl C On A.DocNo = C.DocNo " +
                     "Inner Join " +
                     "( " +
                     "    Select DocNo, MAX(Dno) Dno From TblEmpsalaryDtl " +
                     "    Group BY DocNo " +
                     ")D on C.DocNo = D.DocNo And C.Dno = D.Dno " +
                     "Where A.Status = 'A' And A.EmpCode = '" + empCode + "' ");
                    if (salaryStr.Length > 0)
                        SalaryDec = Decimal.Parse(salaryStr);
                    Grd1.Cells[Row, 10].Value = (SalaryDec * 75 / 100);
                }
            }
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveTravelRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestHdr(DocNo, DocDt, `Status`, WebInd, CancelInd, CancelReason, CityCode, StartDt,  ");
            SQL.AppendLine("EndDt, StartTm,EndTm,TravelService,PICCode,SiteCode,Transportation, Result, SiteCode2, Remark,CreateBy,CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', 'D', 'N', @cancelReason, @CityCode, @StartDt, ");
            SQL.AppendLine("@EndDt, @StartTm, @EndTm, @TravelService, @PICCode, @SiteCode, @Transportation, @Result, ");
            SQL.AppendLine("@SiteCode2, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='TravelRequest' ");
            if (mIsApprovalByDept)
                SQL.AppendLine("And T.DeptCode In (Select DeptCode From TblEmployee Where EmpCode=@PICCode And DeptCode Is Not Null) ");

            if (Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='TravelRequest' And DAGCode Is Not Null Limit 1;"))
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@PICCode ");
                SQL.AppendLine("))) ");
            }

            SQL.AppendLine(";");

            SQL.AppendLine("Update TblTravelRequestHdr Set Status='A'  ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists(  ");
            SQL.AppendLine("    Select 1 From TblDocApproval  ");
            SQL.AppendLine("    Where DocType='TravelRequest' ");
            SQL.AppendLine("    And DocNo=@DocNo  ");
            SQL.AppendLine("    );  ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CityCode", MeeCityCode.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStart));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEnd));
            Sm.CmParam<String>(ref cm, "@TravelService", MeeTravelService.Text);
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSite));
            Sm.CmParam<String>(ref cm, "@Transportation", Sm.GetLue(LueTransport));
            Sm.CmParam<String>(ref cm, "@Result", MeeResult.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode2", Sm.GetLue(LueSiteCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl( DocNo, DNo, EmpCode, ");
            SQL.AppendLine("GrdLvlCode, LvlCode, LvlCodeDno, Qty, ");
            SQL.AppendLine("GrdLvlCode2, LvlCode2, LvlCodeDno2, Qty2, ");
            SQL.AppendLine("GrdLvlCode3, LvlCode3, LvlCodeDno3, Qty3, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, ");
            SQL.AppendLine("@GrdLvlCode, @LvlCode, @LvlCodeDno, @Qty, ");
            SQL.AppendLine("@GrdLvlCode2, @LvlCode2, @LvlCodeDno2, @Qty2, ");
            SQL.AppendLine("@GrdLvlCode3, @LvlCode3, @LvlCodeDno3, @Qty3, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<String>(ref cm, "@LvlCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@LvlCodeDno", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode2", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@LvlCode2", Sm.GetGrdStr(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@LvlCodeDno2", Sm.GetGrdStr(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 13));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode3", Sm.GetGrdStr(Grd2, Row, 14));
            Sm.CmParam<String>(ref cm, "@LvlCode3", Sm.GetGrdStr(Grd2, Row, 15));
            Sm.CmParam<String>(ref cm, "@LvlCodeDno3", Sm.GetGrdStr(Grd2, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd2, Row, 18));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl2( DocNo, DNo, EmpCode, GrdLvlCode, LvlCode, LvlCodeDno, Qty, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @GrdLvlCode, @LvlCode, @LvlCodeDno, @Qty, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@LvlCode", Sm.GetGrdStr(Grd3, Row, 5));
            Sm.CmParam<String>(ref cm, "@LvlCodeDno", Sm.GetGrdStr(Grd3, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl3( DocNo, DNo, EmpCode, Amt, Qty, remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @Amt, @Qty, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd4, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd4, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl4( DocNo, DNo, OfficeInd, EmpCode, Amt, Qty, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @OfficeInd, @EmpCode, @Amt, @Qty, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@OfficeInd", Sm.GetGrdBool(Grd5, Row, 4) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd5, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd5, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd5, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd5, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl5(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl5( DocNo, DNo, OfficeInd, EmpCode, GrdLvlCode, LvlCode, LvlCodeDno, Qty, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @OfficeInd, @EmpCode, @GrdLvlCode, @LvlCode, @LvlCodeDno, @Qty, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@OfficeInd", Sm.GetGrdBool(Grd6, Row, 4) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd6, Row, 2));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd6, Row, 5));
            Sm.CmParam<String>(ref cm, "@LvlCode", Sm.GetGrdStr(Grd6, Row, 6));
            Sm.CmParam<String>(ref cm, "@LvlCodeDno", Sm.GetGrdStr(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd6, Row, 9));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl6(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTravelRequestDtl6( DocNo, DNo, EmpCode, GrdLvlCode, LvlCode, LvlCodeDno,  Qty, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo,  @EmpCode, @GrdLvlCode, @LvlCode, @LvlCodeDno, @Qty, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd7, Row, 2));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetGrdStr(Grd7, Row, 6));
            Sm.CmParam<String>(ref cm, "@LvlCode", Sm.GetGrdStr(Grd7, Row, 7));
            Sm.CmParam<String>(ref cm, "@LvlCodeDno", Sm.GetGrdStr(Grd7, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd7, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd7, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTravelRequestDtl7(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            if (Detasering)
            {
                SQL.AppendLine("Insert Into TblTravelRequestDtl7( DocNo, DNo, EmpCode, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Detasering,");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, 0, 0, 0, 0, 0, 0, @Detasering, ");
                SQL.AppendLine("@CreateBy, CurrentDateTime()) ");
                SQL.AppendLine("    On Duplicate Key Update EmpCode=@EmpCode, Amt1=@Amt1, Amt2=@Amt2, Amt3=@Amt3, Amt4=@Amt4, Amt5=@Amt5, Amt6=@Amt6, Detasering=@Detasering   ");
                SQL.AppendLine("; ");
            }
            else
            {
                SQL.AppendLine("Insert Into TblTravelRequestDtl7( DocNo, DNo, EmpCode, Amt1, Amt2, Amt3, Amt4, Amt5, Amt6, Detasering,");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @Amt1, @Amt2, @Amt3, @Amt4, @Amt5, @Amt6, @Detasering, ");
                SQL.AppendLine("@CreateBy, CurrentDateTime()) ");
                SQL.AppendLine("    On Duplicate Key Update EmpCode=@EmpCode, Amt1=@Amt1, Amt2=@Amt2, Amt3=@Amt3, Amt4=@Amt4, Amt5=@Amt5, Amt6=@Amt6, Detasering=@Detasering   ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Amt4", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Amt5", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Amt6", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Detasering", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void InsertDataVR(string TRVDocNo)
        {
            if (Grd1.Rows.Count > 1)
            {
                var VoucherRequestDocNoTR = string.Empty;
                var VoucherRequestDocNo = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    var cml = new List<MySqlCommand>();
                    string DNo = Sm.Right("00" + (Row + 1).ToString(), 3);
                    VoucherRequestDocNoTR = Sm.GetValue("Select VoucherRequestDocNo From tbltravelrequestDtl7 Where DocNo = '" + TRVDocNo + "' And Dno = '"+DNo+"' ");
                    VoucherRequestDocNo = VoucherRequestDocNoTR.Length >0 ? VoucherRequestDocNoTR : GenerateVoucherRequestDocNo();
                    cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, TRVDocNo, Row));
                    cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, TRVDocNo, Row));
                    Sm.ExecCommands(cml);
                }
            }
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        //save vr
        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string TRVDocNo, int Rowx)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, OpeningDt, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', @DeptCode, '23', Null, ");
            SQL.AppendLine("'C', @PaymentType, Null, @BankCode, null, null, null, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, null, Concat('Travel Request ', @TRVDocNo, '. ', @Remark),");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("    ON DUPLICATE KEY UPDATE Amt=@Amt, Remark=Concat('Travel Request ', @TRVDocNo, '. ', @Remark) ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='TravelRequest' ");
            SQL.AppendLine("    And DocNo=@TRVDocNo ");
            SQL.AppendLine("    ); ");

            if (TxtDocNo.Text.Length > 0)
            {
                SQL.AppendLine("update tblvoucherrequesthdr A ");
                SQL.AppendLine("inner join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    select 1  ");
                SQL.AppendLine("    from tbldocapproval T1 ");
                SQL.AppendLine("    inner join tbltravelrequesthdr T2 On T1.DocNo = T2.DocNo and T1.DocNo = @TRVDocNo and T2.Status = 'A'  ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set A.Status = 'A' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ; ");


                SQL.AppendLine("update tblvoucherrequesthdr A ");
                SQL.AppendLine("inner join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    select 1 ");
                SQL.AppendLine("    from tbldocapproval T1 ");
                SQL.AppendLine("    inner join tbltravelrequesthdr T2 On T1.DocNo = T2.Docno and T1.DocNo = @TRVDocNo and T2.Status = 'C' ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set A.Status = 'C' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ; ");
            }

            SQL.AppendLine("Update TblTravelRequestDtl7 Set VoucherRequestDocNo=@DocNo ");
            SQL.AppendLine("Where DocNo=@TRVDocNo And Dno=@Rowx ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@TRVDocNo", TRVDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetValue("Select DeptCode From TblEmployee Where EmpCode='" + TxtPICCode.Text + "'"));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetParameter("BankAcCodeTravelRequest"));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetValue("Select UserCode From tblEmployee Where EmpCode = '"+TxtPICCode.Text+"'"));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetParameter("MainCurCode"));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Rowx, 11));
            Sm.CmParam<String>(ref cm, "@Rowx", Sm.Right("00" + (Rowx + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo, string TRVDocNo, int Rowx)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, Concat('Travel Request# : ', @TRVDocNo, ' - ', @EmpCode), @Amt, Null, @CreateBy, CurrentDateTime()) "+
                    "   On Duplicate key Update Description = Concat('Travel Request# : ', @TRVDocNo, ' - ', @EmpCode), Amt=@Amt ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@TRVDocNo", TRVDocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Rowx, 2));
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Rowx, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            IsDetaseringActive();

            Cursor.Current = Cursors.WaitCursor;

            String DocNo = TxtDocNo.Text;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelTravelRequestHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl7(DocNo, Row));
            }

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl(DocNo, Row));
            }

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl2(DocNo, Row));
            }

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl3(DocNo, Row));
            }

            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl4(DocNo, Row));
            }

            for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl5(DocNo, Row));
            }

            for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd7, Row, 2).Length > 0)
                    cml.Add(SaveTravelRequestDtl6(DocNo, Row));
            }

            Sm.ExecCommands(cml);
            if (ChkCancelInd.Checked == false)
            {
                InsertDataVR(DocNo);
            }

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDateNotValid() ||
                IsDataCancelledAlready() ||
                IsDocAlreadyProcessToVoucher() ||
                (!ChkCancelInd.Checked && IsDataCreateByDesktop());
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblTravelRequestHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataCreateByDesktop()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblTravelRequestHdr " +
                    "Where WebInd = 'D' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (ChkCancelInd.Checked == false && Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Only document from web can be edited.");
                return true;
            }
            return false;
        }

        private bool IsDocAlreadyProcessToVoucher()
        {
            return Sm.IsDataExist(
                "Select 1 From TblTravelRequestDtl7 A, TblVoucherHdr B " +
                "Where A.DocNo=@Param And A.VoucherRequestDocNo Is Not Null And A.VoucherRequestDocNo=B.VoucherRequestDocNo And B.CancelInd='N' Limit 1;",
                TxtDocNo.Text, "This document already processed to voucher.");
        }

        private MySqlCommand CancelTravelRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            if (ChkCancelInd.Checked == true)
            {
                SQL.AppendLine("Update TblTravelRequestHdr Set ");
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

                SQL.AppendLine("Update TblVoucherRequestHdr Set ");
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y' ");
                SQL.AppendLine("Where CancelInd='N' And Status<>'C'And DocNo In (Select VoucherrequestDocNo From TblTravelrequestDtl7 Where DocNo=@DocNo) ; ");
            }

            SQL.AppendLine("Update TblTravelRequestHdr SET WEbInd = 'D' Where DocNo = @DocNo ; ");
            SQL.AppendLine("Delete From TblTravelRequestDtl Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl2 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl3 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl4 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl5 Where DocNo=@DocNo ;");
            SQL.AppendLine("Delete From TblTravelRequestDtl6 Where DocNo=@DocNo ;");
            //SQL.AppendLine("Delete From TblTravelRequestDtl7 Where DocNo=@DocNo ;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowTravelRequestHdr(DocNo);
                string WebInd = Sm.GetValue("Select WebInd From tbltravelRequesthdr Where DocNo = '"+DocNo+"' ");
                ShowTravelRequestDtl(DocNo, WebInd);
                ShowTravelRequestDtl2(DocNo, WebInd);
                ShowTravelRequestDtl3(DocNo, WebInd);
                ShowTravelRequestDtl4(DocNo, WebInd);
                ShowTravelRequestDtl5(DocNo, WebInd);
                ShowTravelRequestDtl6(DocNo);
                ShowTravelRequestDtl7(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTravelRequestHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, Case when A.Status = 'A' then 'Approved' " +
                    "When A.Status='O' Then 'Outstanding' " +
                    "when A.Status='C' Then 'Cancelled' End As DocStatus,  " +
                    "A.CancelInd, A.CancelReason, IFNULL(C.CityName, A.CityCode) AS CityCode,  " +
                    "A.StartDt, A.EndDt, A.StartTm, A.EndTm, A.TravelService, A.PICCode, B.Empname, " +
                    "A.SiteCode, A.Transportation, A.Result, A.SiteCode2, A.WebInd, A.Remark " +
                    "From TblTravelRequesthdr A " +
                    "Left Join TblEmployee B On A.PICCode = B.EmpCode " +
                    "LEFT JOIN tblcity C ON A.CityCode = C.CityCode " +
                    "Where A.DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "DocStatus", "CancelReason", "CancelInd", "CityCode",  
                        //6-10
                        "StartDt", "EndDt", "StartTm", "EndTm", "TravelService",
                        //11-15
                        "PICCode", "Empname", "SiteCode", "Transportation", "Result",
                        //16-18
                        "SiteCode2", "WebInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = DocNo;
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                        MeeCityCode.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[7]));
                        Sm.SetTme(TmeStart, Sm.DrStr(dr, c[8]));
                        Sm.SetTme(TmeEnd, Sm.DrStr(dr, c[9]));
                        MeeTravelService.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPICCode.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPICName.EditValue = Sm.DrStr(dr, c[12]);
                        Sl.SetLueSiteCode(ref LueSite, Sm.DrStr(dr, c[13]), string.Empty);
                        Sm.SetLue(LueTransport, Sm.DrStr(dr, c[14]));
                        MeeResult.EditValue = Sm.DrStr(dr, c[15]);
                        Sl.SetLueSiteCode2(ref LueSiteCode2, Sm.DrStr(dr, c[16]));
                        mWebInd = Sm.DrStr(dr, c[17]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[18]);
                    }, true
                );
        }

        //meal
        private void ShowTravelRequestDtl(string DocNo, string WebInd)
        {
            var SQL = new StringBuilder();

            if (WebInd == "D")
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.Empname, ");
                SQL.AppendLine("A.grdlvlCode, A.LvlCode, A.LvlCodeDno, C.Amt, A.Qty, ");
                SQL.AppendLine("A.grdlvlCode2, A.LvlCode2, A.LvlCodeDno2, D.Amt As Amt2, A.Qty2, ");
                SQL.AppendLine("A.grdlvlCode3, A.LvlCode3, A.LvlCodeDno3, E.Amt As Amt3, A.Qty3, ");
                SQL.AppendLine("(C.Amt*A.Qty)+(D.Amt*A.Qty2)+(E.Amt*A.Qty3) As Total ");
                SQL.AppendLine("From tbltravelrequestdtl A ");
                SQL.AppendLine("Inner Join TblEMployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno ");
                SQL.AppendLine("Left Join TblLevelDtl D On A.LvlCode2 = D.LevelCode And A.LvlCodeDno2 = D.Dno ");
                SQL.AppendLine("Left Join TblLevelDtl E On A.LvlCode3 = E.LevelCode And A.LvlCodeDno3 = E.Dno ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }
            else
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.Empname,  ");
                SQL.AppendLine("E.GrdLvlCode grdlvlCode, E.levelCode LvlCode, E.Dno LvlCodeDno, ifnull(E.Amt, 0) Amt, A.Qty,  ");
                SQL.AppendLine("F.GrdLvlCode grdlvlCode2, F.levelCode LvlCode2, F.Dno LvlCodeDno2, ifnull(F.Amt, 0) Amt2, A.Qty2,  ");
                SQL.AppendLine("G.GrdLvlCode grdlvlCode3, G.levelCode LvlCode3, G.Dno LvlCodeDno3, ifnull(G.Amt, 0) Amt3, A.Qty3,  ");
                SQL.AppendLine("0 As Total  ");
                SQL.AppendLine("From tbltravelrequestdtl A  ");
                SQL.AppendLine("Inner Join TblEMployee B On A.EmpCode = B.EmpCode  ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt  ");
                SQL.AppendLine("    From Tblemployee A  ");
                SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode  ");
                SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode  ");
                SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode  ");
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeBreakfast')  "); 
                SQL.AppendLine(")E On A.EmpCode = E.EmpCode  ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt  ");
                SQL.AppendLine("    From Tblemployee A  ");
                SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode  ");
                SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode  ");
                SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode  ");
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeLunch')   ");
                SQL.AppendLine(")F On A.EmpCode = F.EmpCode  ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt  ");
                SQL.AppendLine("    From Tblemployee A  ");
                SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode  ");
                SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode  ");
                SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode  ");
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeDinner')   ");
                SQL.AppendLine(")G On A.EmpCode = G.EmpCode  ");
               
                
                
               
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpName", "grdlvlCode", "LvlCode", "LvlCodeDno", 
                    //6-10
                    "Amt", "Qty", "grdlvlCode2", "LvlCode2", "LvlCodeDno2",
                    //11-15
                    "Amt2", "Qty2", "grdlvlCode3", "LvlCode3", "LvlCodeDno3", 
                    //16-18
                    "Amt3", "Qty3", "Total"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 17, 16);                    
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 19, 18);
                    
                    
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        //uang saku
        private void ShowTravelRequestDtl2(string DocNo, string WebInd)
        {
            var SQL = new StringBuilder();

            if (WebInd == "D")
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.Empname,  ");
                SQL.AppendLine("A.GrdLvlCode, A.LvlCode, A.LvlCodeDno, C.Amt, A.Qty, (C.Amt*A.Qty) Total ");
                SQL.AppendLine("From tbltravelrequestdtl2 A ");
                SQL.AppendLine("Inner Join TblEMployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }
            else
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.Empname,  ");
                SQL.AppendLine("H.GrdLvlCode, H.levelCode LvlCode, H.Dno LvlCodeDno, ifnull(H.Amt, 0) Amt, A.Qty, 0 Total ");
                SQL.AppendLine("From tbltravelrequestdtl2 A ");
                SQL.AppendLine("Inner Join TblEMployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt  ");
                SQL.AppendLine("    From Tblemployee A  ");
                SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode  ");
                SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode  ");
                SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode  ");
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeDailyAllowance')  ");
                SQL.AppendLine(")H On A.EmpCode = H.EmpCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpName", "GrdLvlCode", "LvlCode", "LvlCodeDno", 
                    //6-8
                    "Amt", "Qty", "Total"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        //city transport
        private void ShowTravelRequestDtl3(string DocNo, string WebInd)
        {
            var SQL = new StringBuilder();

            if (WebInd == "D")
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.Empname,  ");
                SQL.AppendLine("A.Amt, A.Qty, (A.Amt*A.Qty) Total, A.Remark ");
                SQL.AppendLine("From tbltravelrequestdtl3 A ");
                SQL.AppendLine("Inner Join TblEMployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }
            else
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.Empname,  ");
                SQL.AppendLine("ifnull(I.Amt, 0) Amt, A.Qty, 0 as Total, A.remark ");
                SQL.AppendLine("From tbltravelrequestdtl3 A ");
                SQL.AppendLine("Inner Join TblEMployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt  ");
                SQL.AppendLine("    From Tblemployee A  ");
                SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode  ");
                SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode  ");
                SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode  ");
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeCityTransport')   ");
                SQL.AppendLine(")I On A.EmpCode = I.EmpCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpName", "Amt", "Qty", "Total", 
                    //6
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 10, 6);

                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);
        }

        //transport
        private void ShowTravelRequestDtl4(string DocNo, string WebInd)
        {
            var SQL = new StringBuilder();

            if (WebInd == "D")
            {
                SQL.AppendLine("Select A.Dno, A.OfficeInd, A.EmpCode, B.EmpName, ");
                SQL.AppendLine("A.Amt, A.Qty, if(OfficeInd='N', (A.Amt*A.Qty), 0) Total, A.Remark ");
                SQL.AppendLine("From tbltravelrequestdtl4 A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }
            else
            {
                SQL.AppendLine("Select A.Dno, A.OfficeInd, A.EmpCode, B.EmpName, ");
                SQL.AppendLine("ifnull(J.Amt, 0) Amt, A.Qty, 0 Total, A.Remark ");
                SQL.AppendLine("From tbltravelrequestdtl4 A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt  ");
                SQL.AppendLine("    From Tblemployee A  ");
                SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
                SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode  ");
                SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode  ");
                if (mDocTitle == "TWC")
                {
                    SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeTransport2')   ");
                }
                else
                {
                    SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeTransport')   ");
                }
                SQL.AppendLine(")J On A.EmpCode = J.EmpCode   ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd5, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpName", "OfficeInd", "Amt", "Qty",
                    //6
                    "Total","Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("B", Grd5, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd5, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd5, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd5, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd5, dr, c, Row, 11, 7);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 1);
        }

        //accomodation
        private void ShowTravelRequestDtl5(string DocNo, string WebInd)
        {
            var SQL = new StringBuilder();

            if (WebInd == "D")
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.EmpName, ");
                SQL.AppendLine("A.OfficeInd, A.GrdLvlCode, A.LvlCode, A.LvlCodeDno, ");
                SQL.AppendLine("C.Amt, A.Qty, if(OfficeInd='N', (C.Amt*A.Qty), 0) Total, A.Remark ");
                SQL.AppendLine("From tbltravelrequestdtl5 A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }
            else
            {
                SQL.AppendLine("Select A.Dno, A.EmpCode, B.EmpName, ");
                SQL.AppendLine("A.OfficeInd, K.GrdLvlCode GrdLvlCode, K.levelCode LvlCode, K.Dno LvlCodeDno, "); 
                SQL.AppendLine("ifnull(K.Amt, 0) Amt, A.Qty, 0 Total, A.Remark ");
                SQL.AppendLine("From tbltravelrequestdtl5 A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno ");
                SQL.AppendLine("Left Join   ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.EmpCode,  A.GrdLvlCode, C.levelCode, D.Dno, D.Amt  ");
                SQL.AppendLine("    From Tblemployee A  ");
                SQL.AppendLine("    Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode  ");
                SQL.AppendLine("    left Join TbllevelHdr C On B.LevelCode = C.LevelCode  ");
                SQL.AppendLine("    Left Join TblLevelDtl D On C.LevelCode = D.LevelCode  ");
                SQL.AppendLine("    Where D.AdCode = (Select Parvalue From tblParameter Where parCode = 'ADCodeAccomodation')   ");
                SQL.AppendLine(")K On A.EmpCode = K.EmpCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");
            }


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd6, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpName", "OfficeInd", "GrdLvlCode", "LvlCode",
                    //6-10
                    "LvlCodeDno", "Amt", "Qty", "Total", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd6, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd6, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd6, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("B", Grd6, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd6, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd6, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd6, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd6, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd6, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd6, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd6, dr, c, Row, 11, 10);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 1);
        }

        //others
        private void ShowTravelRequestDtl6(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.EmpCode, B.EmpName, C.AdCode, D.ADName, A.GrdLvlCode, A.LvlCode, A.LvlCodeDno, C.Amt, A.Qty, (C.Amt*A.Qty) Total, A.Remark ");
            SQL.AppendLine("From tbltravelrequestdtl6 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno ");
            SQL.AppendLine("Left Join TblAllowanceDeduction D On C.AdCode=D.AdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd7, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpName", "AdCode", "ADName", "GrdLvlCode", 
                    //6-10
                    "LvlCode", "LvlCodeDno", "Amt", "Qty", "Total", 
                    //11
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd7, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd7, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd7, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd7, dr, c, Row, 12, 11);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd7, 0, 1);
        }

        //summary
        private void ShowTravelRequestDtl7(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.Empname, A.Amt1, A.Amt2, ");
            SQL.AppendLine("A.Amt3, A.Amt4, A.Amt5, A.Amt6, A.Detasering, (A.Amt1+A.Amt2+A.Amt3+A.Amt4+A.Amt5+A.Amt6+A.Detasering) Total, A.VoucherRequestDocNo, C.DocNo As VoucherDocNo ");
            SQL.AppendLine("From TblTravelRequestDtl7 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Left Join TblVoucherHdr C On A.VoucherRequestDocNo=C.VoucherRequestDocNo And C.CancelInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "EmpCode", "EmpName", "Amt1", "Amt2", "Amt3",
                    //6-10
                    "Amt4", "Amt5", "Amt6", "Detasering", "Total", 
                    //11-12
                    "VoucherRequestDocNo", "VoucherDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9); 
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion      

        #endregion

        #region Additional Method

        private bool IsApprovalByDept()
        {
            return Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting " +
                "Where DocType='TravelRequest' " +
                "And DeptCode Is Not Null Limit 1;");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        internal void SetLueAllowanceCode(ref DXE.LookUpEdit Lue, string EmpCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.EmpCode, D.AdCode As Col1, E.AdName As Col2, C.levelCode As Col3, D.Dno As Col4 ");
                SQL.AppendLine("From Tblemployee A ");
                SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode ");
                SQL.AppendLine("left Join TbllevelHdr C On B.LevelCode = C.LevelCode ");
                SQL.AppendLine("Left Join TblLevelDtl D On C.LevelCode = D.LevelCode ");
                SQL.AppendLine("Left Join TblAllowanceDeduction E On D.AdCode = E.AdCode ");
                SQL.AppendLine("Where A.EmpCode='"+EmpCode+"' And D.AdCode Not in  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeBreakfast' ");
                SQL.AppendLine("    union all ");
                SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeLunch' ");
                SQL.AppendLine("    union all ");
                SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeDinner' ");
                SQL.AppendLine("    union all ");
                SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeDailyAllowance' ");
                SQL.AppendLine("    union all ");
                SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeCityTransport' ");
                SQL.AppendLine("    union all ");
                if (mDocTitle == "TWC")
                {
                    SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeTransport2' ");
                }
                else
                {
                    SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeTransport' ");
                }
                SQL.AppendLine("    union all ");
                SQL.AppendLine("    Select Parvalue From tblParameter Where parCode = 'ADCodeAccomodation' ");
                SQL.AppendLine(") ");

               
                Sm.SetLue4(
                     ref Lue,
                     SQL.ToString(),
                     0, 35, 100, 30, false, true, false, false, "Ad Code", "Allowance", "Document", "Detail", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        internal void SetLueCompanyCity(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select CityCode Col1, CityName Col2 From TblCompanyCity Order By CityName ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        internal void SetLueTransportTravel(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode Col1, OptDesc Col2 From TblOption Where Optcat = 'TransportTravel' Order By OptCode ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        internal void ComputeTotalQty(int mRow, string empCode, iGrid mGrd)
        {
            if (mGrd.Name == "Grd2")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, row, 2)))
                    {
                        Grd1.Cells[row, 4].Value = Sm.GetGrdDec(mGrd, mRow, 19);
                    }
                }
            }
            else if (mGrd.Name == "Grd3")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, row, 2)))
                    {
                        Grd1.Cells[row, 5].Value = Sm.GetGrdDec(mGrd, mRow, 9);
                    }
                }
            }
            else if (mGrd.Name == "Grd4")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, row, 2)))
                    {
                        Grd1.Cells[row, 6].Value = Sm.GetGrdDec(mGrd, mRow, 9);
                    }
                }
            }
            else if (mGrd.Name == "Grd5")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, row, 2)))
                    {
                        Grd1.Cells[row, 7].Value = Sm.GetGrdDec(mGrd, mRow, 10);
                    }
                }
            }
            else if (mGrd.Name == "Grd6")
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, row, 2)))
                    {
                        Grd1.Cells[row, 8].Value = Sm.GetGrdDec(mGrd, mRow, 10);
                    }
                }
            }
            else
            {
                for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, row, 2)))
                    {
                        Grd1.Cells[row, 9].Value = Sm.GetGrdDec(mGrd, mRow, 11);
                    }
                }
            }
            ComputeSummaryAllowance();
        }

        internal void ComputeTotalMeal(int Row)
        {
            decimal Amt1 = 0m, Amt2 = 0m, Amt3 = 0;
            decimal Rate1 = 0m;
            decimal Qty1 = 0m;
            decimal Rate2 = 0m;
            decimal Qty2 = 0m;
            decimal Rate3 = 0m;
            decimal Qty3 = 0m;

            if (Sm.GetGrdStr(Grd2, Row, 7).Length > 0)
                Rate1 = Sm.GetGrdDec(Grd2, Row, 7);

            if (Sm.GetGrdStr(Grd2, Row, 8).Length > 0)
                Qty1 = Sm.GetGrdDec(Grd2, Row, 8);

            if (Sm.GetGrdStr(Grd2, Row, 12).Length > 0)
                Rate2 = Sm.GetGrdDec(Grd2, Row, 12);

            if (Sm.GetGrdStr(Grd2, Row, 13).Length > 0)
                Qty2 = Sm.GetGrdDec(Grd2, Row, 13);

            if (Sm.GetGrdStr(Grd2, Row, 17).Length > 0)
                Rate3 = Sm.GetGrdDec(Grd2, Row, 17);

            if (Sm.GetGrdStr(Grd2, Row, 18).Length > 0)
                Qty3 = Sm.GetGrdDec(Grd2, Row, 18);

            Amt1 = Rate1 * Qty1;
            Amt2 = Rate2 * Qty2;
            Amt3 = Rate3 * Qty3;

            Grd2.Cells[Row, 19].Value = Amt1 + Amt2 + Amt3;
        }

        internal void ComputeTotalDaily(int Row)
        {
            decimal Amt1 = 0m;
            decimal Rate = 0m;
            decimal Qty = 0m;

            if (Sm.GetGrdStr(Grd3, Row, 7).Length > 0)
                Rate = Sm.GetGrdDec(Grd3, Row, 7);

            if (Sm.GetGrdStr(Grd3, Row, 8).Length > 0)
                Qty = Sm.GetGrdDec(Grd3, Row, 8);

            Amt1 = Rate * Qty;

            Grd3.Cells[Row, 9].Value = Amt1;
        }

        internal void ComputeTotalCityTransport(int Row)
        {
            decimal Amt1 = 0m;
            decimal Rate = 0m;
            decimal Qty = 0m;

            if (Sm.GetGrdStr(Grd4, Row, 7).Length > 0)
                Rate = Sm.GetGrdDec(Grd4, Row, 7);

            if (Sm.GetGrdStr(Grd4, Row, 8).Length > 0)
                Qty = Sm.GetGrdDec(Grd4, Row, 8);

            Amt1 = Rate * Qty;

            Grd4.Cells[Row, 9].Value = Amt1;
        }

        internal void ComputeTotalTransport(int Row)
        {
            decimal Amt1 = 0m;
            decimal Rate = 0m;
            decimal Qty = 0m;

            if (Sm.GetGrdStr(Grd5, Row, 8).Length > 0)
                Rate = Sm.GetGrdDec(Grd5, Row, 8);

            if (Sm.GetGrdStr(Grd5, Row, 9).Length > 0)
                Qty = Sm.GetGrdDec(Grd5, Row, 9);

            Amt1 = Rate * Qty;

            Grd5.Cells[Row, 10].Value = Amt1;
        }

        internal void ComputeTotalAccomodation(int Row)
        {
            decimal Amt1 = 0m;
            decimal Rate = 0m;
            decimal Qty = 0m;

            if (Sm.GetGrdStr(Grd6, Row, 8).Length > 0)
                Rate = Sm.GetGrdDec(Grd6, Row, 8);

            if (Sm.GetGrdStr(Grd6, Row, 9).Length > 0)
                Qty = Sm.GetGrdDec(Grd6, Row, 9);

            Amt1 = Rate * Qty;

            Grd6.Cells[Row, 10].Value = Amt1;
        }

        internal void ComputeTotalOthers(int Row)
        {
            decimal Amt1 = 0m;
            decimal Rate = 0m;
            decimal Qty = 0m;

            if (Sm.GetGrdStr(Grd7, Row, 9).Length > 0)
                Rate = Sm.GetGrdDec(Grd7, Row, 9);

            if (Sm.GetGrdStr(Grd7, Row, 10).Length > 0)
                Qty = Sm.GetGrdDec(Grd7, Row, 10);

            Amt1 = Rate * Qty;

            Grd7.Cells[Row, 11].Value = Amt1;
        }

        internal void GetDataAllowanceMeal(int Row)
        {
            decimal Amt1 = 0, Amt2 = 0, Amt3 = 0;
            string EmpCode = string.Empty;
            string ADCodeBreakfast = Sm.GetParameter("ADCodeBreakfast");
            string ADCodeLunch = Sm.GetParameter("ADCodeLunch");
            string ADCodeDinner = Sm.GetParameter("ADCodeDinner");

            string ADCodeBreakfastStr = string.Empty;
            string ADCodeLunchStr = string.Empty;
            string ADCodeDinnerStr = string.Empty;

            if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0)
                EmpCode = Sm.GetGrdStr(Grd2, Row, 2);

            ADCodeBreakfastStr = Sm.GetValue("Select Amt From TblEmployeeAllowanceDeduction Where EmpCode = '" + EmpCode + "'  And AdCode = '" + ADCodeBreakfast + "' ");
            if (ADCodeBreakfastStr.Length > 0)
                Amt1 = Decimal.Parse(ADCodeBreakfastStr);
            ADCodeLunchStr = Sm.GetValue("Select Amt From TblEmployeeAllowanceDeduction Where EmpCode = '" + EmpCode + "'  And AdCode = '" + ADCodeLunch + "' ");
            if (ADCodeLunchStr.Length > 0)
                Amt2 = Decimal.Parse(ADCodeLunchStr);
            ADCodeDinnerStr = Sm.GetValue("Select Amt From TblEmployeeAllowanceDeduction Where EmpCode = '" + EmpCode + "'  And AdCode = '" + ADCodeDinner + "' ");
            if (ADCodeDinnerStr.Length > 0)
                Amt3 = Decimal.Parse(ADCodeDinnerStr);

            Grd2.Cells[Row, 4].Value = Amt1;
            Grd2.Cells[Row, 6].Value = Amt2;
            Grd2.Cells[Row, 8].Value = Amt3;
        }

        internal void GetDataAllowanceDaily(int Row)
        {
            decimal Amt1 = 0;
            string EmpCode = string.Empty;
            string ADCodeDaily = Sm.GetParameter("ADCodeDailyAllowance");
            string ADCodeDailyStr = string.Empty;

            if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
                EmpCode = Sm.GetGrdStr(Grd3, Row, 2);

            ADCodeDailyStr = Sm.GetValue("Select Amt From TblEmployeeAllowanceDeduction Where EmpCode = '" + EmpCode + "'  And AdCode = '" + ADCodeDaily + "' ");
            if (ADCodeDailyStr.Length > 0)
                Amt1 = Decimal.Parse(ADCodeDailyStr);

            Grd3.Cells[Row, 4].Value = Amt1;
        }

        internal void GetDataAllowanceCityTransport(int Row)
        {
            decimal Amt1 = 0;
            string EmpCode = string.Empty;
            string ADCodeCityTransport = Sm.GetParameter("ADCodeCityTransport");
            string ADCodeCityTransportStr = string.Empty;

            if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0)
                EmpCode = Sm.GetGrdStr(Grd4, Row, 2);

            ADCodeCityTransportStr = Sm.GetValue("Select Amt From TblEmployeeAllowanceDeduction Where EmpCode = '" + EmpCode + "'  And AdCode = '" + ADCodeCityTransport + "' ");
            if (ADCodeCityTransportStr.Length > 0)
                Amt1 = Decimal.Parse(ADCodeCityTransportStr);

            Grd4.Cells[Row, 4].Value = Amt1;
        }

        internal void GetDataAllowanceTransport(int Row)
        {
            decimal Amt1 = 0;
            string EmpCode = string.Empty;
            string ADCodeTransport = mDocTitle == "TWC"? Sm.GetParameter("ADCodeTransport2"):Sm.GetParameter("ADCodeTransport");
            string ADCodeTransportStr = string.Empty;

            if (Sm.GetGrdStr(Grd5, Row, 2).Length > 0)
                EmpCode = Sm.GetGrdStr(Grd5, Row, 2);

            ADCodeTransportStr = Sm.GetValue("Select Amt From TblEmployeeAllowanceDeduction Where EmpCode = '" + EmpCode + "'  And AdCode = '" + ADCodeTransport + "' ");
            if (ADCodeTransportStr.Length > 0)
                Amt1 = Decimal.Parse(ADCodeTransportStr);

            Grd5.Cells[Row, 5].Value = Amt1;
        }

        internal void GetDataAccomodation(int Row)
        {
            decimal Amt1 = 0;
            string EmpCode = string.Empty;
            string ADCodeAccomodation = Sm.GetParameter("ADCodeAccomodation");
            string ADCodeAccomodationStr = string.Empty;

            if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0)
                EmpCode = Sm.GetGrdStr(Grd6, Row, 2);

            ADCodeAccomodationStr = Sm.GetValue("Select Amt From TblEmployeeAllowanceDeduction Where EmpCode = '" + EmpCode + "'  And AdCode = '" + ADCodeAccomodation + "' ");
            if (ADCodeAccomodationStr.Length > 0)
                Amt1 = Decimal.Parse(ADCodeAccomodationStr);

            Grd6.Cells[Row, 5].Value = Amt1;
        }

        internal void ComputeSummaryAllowance()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    if (Detasering)
                    {
                        Grd1.Cells[Row, 11].Value = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0);
                    }
                    else
                    {
                        Grd1.Cells[Row, 11].Value = Sm.FormatNum(
                                                    Sm.GetGrdDec(Grd1, Row, 4) + Sm.GetGrdDec(Grd1, Row, 5) + Sm.GetGrdDec(Grd1, Row, 6) +
                                                    Sm.GetGrdDec(Grd1, Row, 7) + Sm.GetGrdDec(Grd1, Row, 8) + Sm.GetGrdDec(Grd1, Row, 9) +
                                                    Sm.GetGrdDec(Grd1, Row, 10), 0);
                    }
                }
            }
        }

      
        private void ParPrint() // int dihapus
        {
            var l = new List<TravelRequest>();
            
            string[] TableName = { "TravelRequest" }; 
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.TravelService, Date_Format(A.StartDt, '%d %M %Y')As StartDt, ");
            SQL.AppendLine("Date_Format(A.EndDt,'%d %M %Y')As EndDt, Concat(Left(A.StartTm,2),':',Right(A.StartTm,2))As StartTm, A.CityCode, ");
            SQL.AppendLine("Concat(Left(A.EndTm,2),':',Right(A.EndTm,2)) As EndTm, B.Amt1 As MealAllowance, B.Amt2 As DailyAllowance, ");
            SQL.AppendLine("B.Amt3 As CityTransport, B.Amt4 As Transport, B.Amt5 As Accomadation, B.Amt6 As OtherAllowance, C.EmpName As PICName, ");
            SQL.AppendLine("D.PosName, DATEDIFF(A.EndDt, A.StartDt)As TotDt, (B.Amt1+ B.Amt2+ B.Amt3+ B.Amt4+ B.Amt5+ B.Amt6 +B.Detasering)As Total, E.CityName, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='MainCurCode') As 'MainCur', A.Result, F.OptDesc As Transportation, G.HOInd  ");
            SQL.AppendLine("From tbltravelrequesthdr A ");
            SQL.AppendLine("Inner Join tbltravelrequestdtl7 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join tblposition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join tblcity E On A.CityCode = E.CityCode ");
            SQL.AppendLine("Left Join tbloption F On F.OptCat='TransportTravel' And A.Transportation = F.OptCode ");
            SQL.AppendLine("Left Join Tblsite G On A.SiteCode = G.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo  ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                //Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("000", i.ToString()), 3));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "Docno",
                         "DocDt",
                         "TravelService",
                         "StartDt",
                         "EndDt",
                         
                         //11-15
                         "StartTm",
                         "EndTm",
                         "MealAllowance",
                         "DailyAllowance",
                         "CityTransport",
                        
                         //16-20
                         "Transport",
                         "Accomadation",
                         "OtherAllowance",
                         "PICName",
                         "PosName",

                         //21-25
                         "TotDt",
                         "Total",
                         "CityName",
                         "MainCur",
                         "Result",

                         //26-27
                         "Transportation",
                         "HOInd",
                         "CityCode"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TravelRequest()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            TravelService = Sm.DrStr(dr, c[8]),
                            StartDt = Sm.DrStr(dr, c[9]),
                            EndDt = Sm.DrStr(dr, c[10]),

                            StartTm = Sm.DrStr(dr, c[11]),
                            EndTm = Sm.DrStr(dr, c[12]),
                            MealAllowance = Sm.DrDec(dr, c[13]),
                            DailyAllowance = Sm.DrDec(dr, c[14]),
                            CityTransport = Sm.DrDec(dr, c[15]),

                            Transport = Sm.DrDec(dr, c[16]),
                            Accomadation = Sm.DrDec(dr, c[17]),
                            OtherAllowance = Sm.DrDec(dr, c[18]),
                            PICName = Sm.DrStr(dr, c[19]),
                            PosName = Sm.DrStr(dr, c[20]),

                            TotDt = Sm.DrStr(dr, c[21]),
                            Terbilang = Sm.Terbilang2(Sm.DrDec(dr, c[21])),
                            Total = Sm.DrDec(dr, c[22]),
                            CityName = Sm.DrStr(dr, c[23]),
                            MainCur = Sm.DrStr(dr, c[24]),
                            Result = Sm.DrStr(dr, c[25]),

                            Transportation = Sm.DrStr(dr, c[26]),
                            HOInd = Sm.DrStr(dr, c[27]),
                            CityCode = Sm.DrStr(dr, c[28]), 
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            PosName2 = (Sm.DrStr(dr, c[27])== "Y"? mPosNameTravelRequestForHO : mPosNameTravelRequestForNonHO),
                            SignName = (Sm.DrStr(dr, c[27]) == "Y" ? mSignNameTravelRequestForHO : mSignNameTravelRequestForNonHO)
                          
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion


            Sm.PrintReport("HINRbpd", myLists, TableName, false);

        }

        private void ParPrint2()
        {
            var l2 = new List<TravelRequest2>();
            var ldtl = new List<VoucherReqDtl>();

            string[] TableName = { "TravelRequest2", "VoucherReqDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header2
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();
            SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL2.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y')As DocDt, C.EmpName As PICName, A.TravelService, ");
            SQL2.AppendLine("Date_Format(A.StartDt, '%d %M %Y')As StartDt, Date_Format(A.EndDt,'%d %M %Y')As EndDt, A.CityCode,  ");
            SQL2.AppendLine("Concat(Left(A.StartTm,2),':',Right(A.StartTm,2))As StartTm, ");
            SQL2.AppendLine("Concat(Left(A.EndTm,2),':',Right(A.EndTm,2)) As EndTm, A.Result, D.PosName, E.CityName, ");
            SQL2.AppendLine("F.OptDesc As Transportation, G.HOInd  ");
            SQL2.AppendLine("From tbltravelrequesthdr A ");
            SQL2.AppendLine("Inner Join tbltravelrequestdtl7 B On A.DocNo=B.DocNo ");
            SQL2.AppendLine("Inner Join TblEmployee C On A.PICCode=C.EmpCode ");
            SQL2.AppendLine("Left Join tblposition D On C.PosCode=D.PosCode ");
            SQL2.AppendLine("Left Join tblcity E On A.CityCode = E.CityCode ");
            SQL2.AppendLine("Left Join tbloption F On F.OptCat='TransportTravel' And A.Transportation = F.OptCode ");
            SQL2.AppendLine("Left join TblSite G On A.SiteCode = G.SiteCode ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");


            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "Docno",
                         "DocDt",
                         "PicName",
                         "TravelService",
                         "StartDt",
                         
                         //11-15
                         "EndDt",
                         "StartTm",
                         "EndTm",
                         "Result",
                         "PosName",
                         
                         //16-19
                         "CityName",
                         "Transportation",
                         "HOInd",
                         "CityCode",
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new TravelRequest2()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),

                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            CompanyAddress = Sm.DrStr(dr2, c2[2]),
                            CompanyAddressCity = Sm.DrStr(dr2, c2[3]),
                            CompanyPhone = Sm.DrStr(dr2, c2[4]),
                            CompanyFax = Sm.DrStr(dr2, c2[5]),

                            DocNo = Sm.DrStr(dr2, c2[6]),
                            DocDt = Sm.DrStr(dr2, c2[7]),
                            PICName = Sm.DrStr(dr2, c2[8]),
                            TravelService = Sm.DrStr(dr2, c2[9]),
                            StartDt = Sm.DrStr(dr2, c2[10]),

                            EndDt = Sm.DrStr(dr2, c2[11]),
                            StartTm = Sm.DrStr(dr2, c2[12]),
                            EndTm = Sm.DrStr(dr2, c2[13]),
                            Result = Sm.DrStr(dr2, c2[14]),
                            PosName = Sm.DrStr(dr2, c2[15]),

                            CityName = Sm.DrStr(dr2, c2[16]),
                            Transportation = Sm.DrStr(dr2, c2[17]),
                            HOInd = Sm.DrStr(dr2, c2[18]),
                            CityCode = Sm.DrStr(dr2, c2[19]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            SignName = (Sm.DrStr(dr2, c2[18]) == "Y" ? mSignNameTravelRequestForHO : mSignNameTravelRequestForNonHO)
                          
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = "Select A.voucherRequestDocNo, A.DocNo, group_concat(B.EmpName)As EmpName From TblTravelRequestDtl7 A " +
                                     "Inner Join TblEmployee B On A.EmpCode=B.EmpCode Where A.DocNo=@DocNo Group by A.DocNo ";
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DocNo",

                         //1
                         "EmpName",
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new VoucherReqDtl()
                        {
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),
                            EmpNAme = Sm.DrStr(drDtl, cDtl[1]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            Sm.PrintReport("HINRpd", myLists, TableName, false);

        }


        private void ParPrint3()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string[] TableName = { "VoucherReq2", "VoucherReqDtl2", "VoucherReqDtl3", "VoucherReqDtl4" };

            var l2 = new List<VoucherReqHdr2>();
            var ldtl2 = new List<VoucherReqDtl2>();
            var ldtl3 = new List<VoucherReqDtl3>();
            var ldtl4 = new List<VoucherReqDtl4>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header2
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL2.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL2.AppendLine(" Date_Format(A.DocDt, '%d %M %Y')As DocDt, ");
            SQL2.AppendLine(" A.DocNo As DocNoVR, B.DocNo, B.DNo, D.EmpName As PICName, E.PosName As PICPosName, F.GrdLvlName As PICGrdLvlName,  C.TravelService, G.OptDesc As Transportation, ");
            SQL2.AppendLine(" Date_Format(C.StartDt, '%d %M %Y')As StartDt, Date_Format(C.EndDt,'%d %M %Y')As EndDt, ");
            SQL2.AppendLine(" if(length(C.StartTm)>0, Concat(Left(C.StartTm,2),':',Right(C.StartTm,2)), '')As StartTm, if(Length(C.EndTm)>0, Concat(Left(C.EndTm,2),':',Right(C.EndTm,2)),'') As EndTm, C.result, H.CityName, ");
            SQL2.AppendLine(" @CompanyLogo As CompanyLogo, I.Empname, J.PosName, K.GrdLvlName, C.Result, C.Remark As Remarkhdr, M.ProfitcenterName, N.HoInd, C.CityCode ");
            SQL2.AppendLine(" From Tblvoucherrequesthdr A ");
            SQL2.AppendLine(" Inner Join tbltravelrequestdtl7 B On A.DocNo=B.VoucherRequestDocNo "); // And Dno =@Dno ");
            SQL2.AppendLine(" Left Join TbltravelRequestHdr C On B.DocNo=C.DocNo ");
            SQL2.AppendLine(" Left Join TblEmployee D On A.PIC=D.UserCode ");
            SQL2.AppendLine(" Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL2.AppendLine(" Left Join tblgradelevelhdr F On D.GrdLvlCode=F.GrdLvlCode ");
            SQL2.AppendLine(" Left Join TblOption G On G.OptCat='TransportTravel' And C.Transportation = G.OptCode ");
            SQL2.AppendLine(" Left Join tblcity H On C.CityCode = H.CityCode ");
            SQL2.AppendLine(" Inner Join TblEmployee I On B.EmpCode = I.EmpCode ");
            SQL2.AppendLine(" Left Join TblPosition J On I.PosCode=J.PosCode  ");
            SQL2.AppendLine(" Left Join tblgradelevelhdr K On I.GrdLvlCode=K.GrdLvlCode  ");
            SQL2.AppendLine(" left Join TblSite L On D.SiteCode = L.SiteCode ");
            SQL2.AppendLine(" left Join TblProfitCenter M On L.ProfitCenterCode = M.ProfitCenterCode ");
            SQL2.AppendLine(" left Join TblSite N On C.SiteCode = N.SiteCode ");
            SQL2.AppendLine(" Where C.DocNo=@DocNo And A.DocType='23' ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
          //      Sm.CmParam<String>(ref cm2, "@DNo",Dno);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "DocNoVR",
                         "DocNo",
                         "DocDt",
                         "DNo",
                         "PICName",
                        
                         //6-10
                         "PICPosName",
                         "PICGrdLvlName",
                         "TravelService",
                         "Transportation",
                         "StartDt",
                         
                         //11-15
                         "EndDt",
                         "StartTm",
                         "EndTm",
                         "Result",
                         "CityNAme",

                         //16-20
                         "CompanyLogo",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "Empname", 
                         
                         //21-25
                         "PosName", 
                         "GrdLvlName", 
                         "Result", 
                         "Remarkhdr",
                         "ProfitcenterName",

                         //26-27
                         "HOInd",
                         "CityCode"
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new VoucherReqHdr2()
                        {
                            CompanyName = Sm.DrStr(dr2, c2[0]),

                            DocNoVR = Sm.DrStr(dr2, c2[1]),
                            DocNo = Sm.DrStr(dr2, c2[2]),
                            DocDt = Sm.DrStr(dr2, c2[3]),
                            DNo = Sm.DrStr(dr2, c2[4]),
                            PICEmpName = Sm.DrStr(dr2, c2[5]),
                            PICPosName = Sm.DrStr(dr2, c2[6]),
                            PICGrdLvlName = Sm.DrStr(dr2, c2[7]),
                            TravelService = Sm.DrStr(dr2, c2[8]),
                            Transportation = Sm.DrStr(dr2, c2[9]),
                            StartDt = Sm.DrStr(dr2, c2[10]),
                            EndDt = Sm.DrStr(dr2, c2[11]),
                            StartTm = Sm.DrStr(dr2, c2[12]),
                            EndTm = Sm.DrStr(dr2, c2[13]),
                            result = Sm.DrStr(dr2, c2[14]),
                            CityName = Sm.DrStr(dr2, c2[15]),
                            CompanyLogo = Sm.DrStr(dr2, c2[16]),
                            CompanyAddress = Sm.DrStr(dr2, c2[17]),
                            CompanyPhone = Sm.DrStr(dr2, c2[18]),
                            CompanyFax = Sm.DrStr(dr2, c2[19]),
                            EmpName = Sm.DrStr(dr2, c2[20]),
                            PosName = Sm.DrStr(dr2, c2[21]),
                            GrdLvlName = Sm.DrStr(dr2, c2[22]),
                            Result = Sm.DrStr(dr2, c2[23]),
                            RemarkHdr = Sm.DrStr(dr2, c2[24]),
                            ProfitCenterName = Sm.DrStr(dr2, c2[25]),
                            HOInd = Sm.DrStr(dr2, c2[26]),
                            CityCode = Sm.DrStr(dr2, c2[27]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);
            #endregion

            #region Detail2

            var cmDtl2 = new MySqlCommand();

            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                cmDtl2.CommandText = "Select A.voucherRequestDocNo, A.DocNo, group_concat(B.EmpName)As EmpName From TblTravelRequestDtl7 A " +
                                     "Inner Join TblEmployee B On A.EmpCode=B.EmpCode Where A.DocNo=@DocNo Group by A.DocNo "; //And A.Dno = @Dno

                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
             //   Sm.CmParam<String>(ref cmDtl2, "@DNo", Dno);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "voucherRequestDocNo",

                         //1-2
                         "DocNo",
                         "EmpName",
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new VoucherReqDtl2()
                        {
                            VoucherRequestDocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                            DocNo = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpNAme = Sm.DrStr(drDtl2, cDtl2[2])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail3

            var cmDtl4 = new MySqlCommand();

            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;
                cmDtl4.CommandText =
                    "Select A.DocNo, A.EmpCode, F.ADName,  (C.Amt+D.Amt+E.Amt) As Amt, (A.Qty+A.Qty2+A.Qty3) As Qty, " +
                    "((C.Amt*A.Qty) +(D.Amt*A.Qty2) + (E.Amt*A.Qty3)) As Total " +
                    "From tbltravelrequestdtl A " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno " +
                    "Left Join TblLevelDtl D On A.LvlCode2 = D.LevelCode And A.LvlCodeDno2 = D.Dno " +
                    "Left Join TblLevelDtl E On A.LvlCode3 = E.LevelCode And A.LvlCodeDno3 = E.Dno " +
                    "Inner Join TblAllowanceDeduction F On C.ADCode = F.AdCode " +
                    "Where Concat(A.DocNO, A.Dno) = @DocNo " +
                    "Union All " +
                    "Select A.DocNo, A.EmpCode, F.AdName, C.Amt, A.Qty, (C.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl2 A " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno " +
                    "Inner Join TblAllowanceDeduction F On C.ADCode = F.AdCode " +
                    "Where C.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "Union All " +
                    "Select A.DocNO, A.EmpCode, 'Trasnport' As ADName,   " +
                    "A.Amt, A.Qty, (A.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl3 A  " +
                    "Inner Join TblEMployee B On A.EmpCode = B.EmpCode  " +
                    "Where A.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "union all " +
                    "Select A.DocNo, A.EmpCode, 'Transport' As ADName,  " +
                    "A.Amt, A.Qty, if(OfficeInd='N', (A.Amt*A.Qty), 0) Total " +
                    "From tbltravelrequestdtl4 A  " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode  " +
                    "Where A.Amt <> 0 And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "Union all " +
                    "Select A.DocNo, A.EmpCode, F.AdName,  " +
                    "C.Amt, A.Qty, if(OfficeInd='N', (C.Amt*A.Qty), 0) Total " +
                    "From tbltravelrequestdtl5 A  " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode  " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno  " +
                    "Inner Join TblAllowanceDeduction F On C.ADCode = F.AdCode " +
                    "Where C.Amt <> 0  And Concat(A.DocNO, A.Dno) = @DocNo " +
                    "union all " +
                    "Select A.DocNo, A.EmpCode, D.ADName, C.Amt, A.Qty, (C.Amt*A.Qty) Total " +
                    "From tbltravelrequestdtl6 A  " +
                    "Inner Join TblEmployee B On A.EmpCode = B.EmpCode  " +
                    "Left Join TblLevelDtl C On A.LvlCode = C.LevelCode And A.LvlCodeDno = C.Dno  " +
                    "Left Join TblAllowanceDeduction D On C.AdCode=D.AdCode  " +
                    "Where C.Amt <>0 And Concat(A.DocNO, A.Dno) = @DocNo ";

                Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "EmpCode",
                         "ADName",
                         "Amt",
                         "Qty",
                         "Total"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new VoucherReqDtl4()
                        {
                            DocNo = Sm.DrStr(drDtl4, cDtl4[0]),
                            EmpCode = Sm.DrStr(drDtl4, cDtl4[1]),
                            AdName = Sm.DrStr(drDtl4, cDtl4[2]),
                            Amt = Sm.DrDec(drDtl4, cDtl4[3]),
                            Qty = Sm.DrDec(drDtl4, cDtl4[4]),
                            Total = Sm.DrDec(drDtl4, cDtl4[5]),
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            if (Doctitle == "HIN")
            {
                Sm.PrintReport("HINSppd", myLists, TableName, false);
            }
        }


        #endregion

        #region Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void BtnPIC_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmTravelRequestDlg2(this));
        }

        private void LueTransport_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTransport, new Sm.RefreshLue1(SetLueTransportTravel));
        }

        private void LueAllowance_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAllowance, new Sm.RefreshLue2(SetLueAllowanceCode), "");
        }

        private void LueAllowance_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd7, ref fAccept, e);
        }

        private void LueSite_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSite, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void LueSiteCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode2, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        private void LueAllowance_Leave(object sender, EventArgs e)
        {
            decimal AmtAllowance = 0; 
            string AmtAllowanceStr = string.Empty;
            string EmpCode = string.Empty;
            string GrdLvlCode = string.Empty;
            string LvlCode = string.Empty;
            string LvlCodeDno= string.Empty;

            if (LueAllowance.Visible && fAccept && fCell.ColIndex == 5 && Sm.GetGrdStr(Grd7, fCell.RowIndex, 2).Length>0)
            {
                if (Sm.GetLue(LueAllowance).Length == 0)
                {
                    Grd7.Cells[fCell.RowIndex, 4].Value =
                    Grd7.Cells[fCell.RowIndex, 5].Value = 
                    Grd7.Cells[fCell.RowIndex, 6].Value =
                    Grd7.Cells[fCell.RowIndex, 7].Value =
                    Grd7.Cells[fCell.RowIndex, 8].Value = null;
                    Grd7.Cells[fCell.RowIndex, 9].Value = 0;
                }
                else
                {
                    Grd7.Cells[fCell.RowIndex, 4].Value = Sm.GetLue(LueAllowance);
                    Grd7.Cells[fCell.RowIndex, 5].Value = LueAllowance.GetColumnValue("Col2");
                    Grd7.Cells[fCell.RowIndex, 6].Value = Sm.GetValue("Select grdLvlCode from TblEmployee Where EmpCode = '" + Sm.GetGrdStr(Grd7, fCell.RowIndex, 2) + "'");
                    Grd7.Cells[fCell.RowIndex, 7].Value = LueAllowance.GetColumnValue("Col3");
                    Grd7.Cells[fCell.RowIndex, 8].Value = LueAllowance.GetColumnValue("Col4");

                    EmpCode = Sm.GetGrdStr(Grd7, fCell.RowIndex, 2);
                    GrdLvlCode = Sm.GetGrdStr(Grd7, fCell.RowIndex, 6);
                    LvlCode = LueAllowance.GetColumnValue("Col3").ToString();
                    LvlCodeDno = LueAllowance.GetColumnValue("Col4").ToString();

                    AmtAllowanceStr = Sm.GetValue("Select D.Amt " +
                    "From Tblemployee A " +
                    "Inner Join TblGradeLevelHdr B On A.GrdlvlCode = B.GrdlVlCode " +
                    "Inner Join TbllevelHdr C On B.LevelCode = C.LevelCode " +
                    "Inner Join TblLevelDtl D On C.LevelCode = D.LevelCode " +
                    "Where A.EmpCode ='"+EmpCode+"' And A.GrdLvlCode ='"+GrdLvlCode+"' And C.LevelCode='"+LvlCode+"' And D.Dno='"+LvlCodeDno+"' ");


                    if (AmtAllowanceStr.Length > 0)
                        AmtAllowance = Decimal.Parse(AmtAllowanceStr);

                    Grd7.Cells[fCell.RowIndex, 9].Value = Sm.FormatNum(AmtAllowance, 0);
                }
            }
            LueAllowance.Visible = false;
        }
        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd1, Sm.GetLue(LueSite)));
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1 && !Sm.IsLueEmpty(LueSite, "site"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd2, Sm.GetLue(LueSite)));
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd3, Sm.GetLue(LueSite)));
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd4, Sm.GetLue(LueSite)));
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd5, Sm.GetLue(LueSite)));
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd6, Sm.GetLue(LueSite)));
            }
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 5 }, e.ColIndex) && Sm.GetGrdStr(Grd7, e.RowIndex, 2).Length>0)
            {
                LueRequestEdit(Grd7, LueAllowance, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
                SetLueAllowanceCode(ref LueAllowance, Sm.GetGrdStr(Grd7, e.RowIndex, 2));
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd7, Sm.GetLue(LueSite)));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueSite, "site")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd1, Sm.GetLue(LueSite)));
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueSite, "site")) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd2, Sm.GetLue(LueSite)));
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd3, Sm.GetLue(LueSite)));
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd4, Sm.GetLue(LueSite)));
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd5, Sm.GetLue(LueSite)));
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd6, Sm.GetLue(LueSite)));
        }

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmTravelRequestDlg(this, Grd7, Sm.GetLue(LueSite)));
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 8, 13, 18 }, e.ColIndex)) ComputeTotalMeal(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd2, e.RowIndex, 2), Grd2);
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex)) ComputeTotalDaily(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd3, e.RowIndex, 2), Grd3);
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7, 8 }, e.ColIndex)) ComputeTotalCityTransport(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd4, e.RowIndex, 2), Grd4);
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 8, 9 }, e.ColIndex)&&Sm.GetGrdBool(Grd5, e.RowIndex, 4) == false) 
                ComputeTotalTransport(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd5, e.RowIndex, 2), Grd5);

            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (Sm.GetGrdBool(Grd5, e.RowIndex, 4) == true)
                {
                    Grd5.Cells[e.RowIndex, 10].Value = 0;
                    ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd5, e.RowIndex, 2), Grd5);
                }
                else
                {
                    ComputeTotalTransport(e.RowIndex);
                    ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd5, e.RowIndex, 2), Grd5);
                }
            }
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex) && Sm.GetGrdBool(Grd6, e.RowIndex, 4) == false) ComputeTotalAccomodation(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd6, e.RowIndex, 2), Grd6);

            if (Sm.IsGrdColSelected(new int[] { 4 }, e.ColIndex))
            {
                if (Sm.GetGrdBool(Grd6, e.RowIndex, 4) == true)
                {
                    Grd6.Cells[e.RowIndex, 10].Value = 0;
                    ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd6, e.RowIndex, 2), Grd6);
                }
                else
                {
                    ComputeTotalAccomodation(e.RowIndex);
                    ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd6, e.RowIndex, 2), Grd6);
                }
            }
        }

        private void Grd7_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex)) ComputeTotalOthers(e.RowIndex);
            ComputeTotalQty(e.RowIndex, Sm.GetGrdStr(Grd7, e.RowIndex, 2), Grd7);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            string empCode = string.Empty; int mRow = 0;
            if ((mWebInd == "W" && BtnSave.Enabled && TxtDocNo.Text.Length > 0 && e.KeyCode == Keys.Delete) ||
                (mWebInd == "D" && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.KeyCode == Keys.Delete))
            {
                for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    empCode = Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 2);
                    mRow = Grd1.SelectedRows[Index].Index;
                    break;
                }
            }
            if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (mWebInd == "W" || TxtDocNo.Text.Length == 0)
                {
                    Grd1.Rows.RemoveAt(mRow);
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row, 2) == empCode)
                        {
                            Grd2.Rows.RemoveAt(Row);
                        }
                    }
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 2) == empCode)
                        {
                            Grd3.Rows.RemoveAt(Row);
                        }
                    }
                    for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd4, Row, 2) == empCode)
                        {
                            Grd4.Rows.RemoveAt(Row);
                        }
                    }
                    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd5, Row, 2) == empCode)
                        {
                            Grd5.Rows.RemoveAt(Row);
                        }
                    }
                    for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd6, Row, 2) == empCode)
                        {
                            Grd6.Rows.RemoveAt(Row);
                        }
                    }
                    for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd7, Row, 2) == empCode)
                        {
                            Grd7.Rows.RemoveAt(Row);
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Warning, "Only document from web can be remove employee.");
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            string empCode = string.Empty; int mRow = 0;
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    empCode = Sm.GetGrdStr(Grd2, Grd2.SelectedRows[Index].Index, 2);
                    mRow = Grd2.SelectedRows[Index].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd2.Rows.RemoveAt(mRow);
                    for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, j, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, j, 2)))
                        {
                            Grd1.Cells[j, 4].Value = 0;
                        }
                    }
                }
            }
            ComputeSummaryAllowance();
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            string empCode = string.Empty; int mRow = 0;
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                for (int Index = Grd3.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    empCode = Sm.GetGrdStr(Grd3, Grd3.SelectedRows[Index].Index, 2);
                    mRow = Grd3.SelectedRows[Index].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd3.Rows.RemoveAt(mRow);
                    for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, j, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, j, 2)))
                        {
                            Grd1.Cells[j, 5].Value = 0;
                        }
                    }
                }
            }
            ComputeSummaryAllowance();
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            string empCode = string.Empty; int mRow = 0;
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                for (int Index = Grd4.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    empCode = Sm.GetGrdStr(Grd4, Grd4.SelectedRows[Index].Index, 2);
                    mRow = Grd4.SelectedRows[Index].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd4.Rows.RemoveAt(mRow);
                    for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, j, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, j, 2)))
                        {
                            Grd1.Cells[j, 6].Value = 0;
                        }
                    }
                }
            }
            ComputeSummaryAllowance();
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            string empCode = string.Empty; int mRow = 0;
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                for (int Index = Grd5.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    empCode = Sm.GetGrdStr(Grd5, Grd5.SelectedRows[Index].Index, 2);
                    mRow = Grd5.SelectedRows[Index].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd5.Rows.RemoveAt(mRow);
                    for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, j, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, j, 2)))
                        {
                            Grd1.Cells[j, 7].Value = 0;
                        }
                    }
                }
            }
            ComputeSummaryAllowance();
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            string empCode = string.Empty; int mRow = 0;
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                for (int Index = Grd6.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    empCode = Sm.GetGrdStr(Grd6, Grd6.SelectedRows[Index].Index, 2);
                    mRow = Grd6.SelectedRows[Index].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd6.Rows.RemoveAt(mRow);
                    Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
                    for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, j, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, j, 2)))
                        {
                            Grd1.Cells[j, 8].Value = 0;
                        }
                    }
                }
            }
            ComputeSummaryAllowance();
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            string empCode = string.Empty; int mRow = 0;
            if (mWebInd == "W" && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                for (int Index = Grd7.SelectedRows.Count - 1; Index >= 0; Index--)
                {
                    empCode = Sm.GetGrdStr(Grd7, Grd7.SelectedRows[Index].Index, 2);
                    mRow = Grd7.SelectedRows[Index].Index;
                    break;
                }
                if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Grd7.Rows.RemoveAt(mRow);
                    Sm.GrdKeyDown(Grd7, e, BtnFind, BtnSave);
                    for (int j = 0; j < Grd1.Rows.Count - 1; j++)
                    {
                        if (Sm.GetGrdStr(Grd1, j, 2).Length > 0 && Sm.CompareStr(empCode, Sm.GetGrdStr(Grd1, j, 2)))
                        {
                            Grd1.Cells[j, 9].Value = 0;
                        }
                    }
                }
            }
            ComputeSummaryAllowance();
        }


        #endregion

        #region Report Class

        class TravelRequest
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string TravelService { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal MealAllowance { get; set; }
            public decimal DailyAllowance { get; set; }
            public decimal CityTransport { get; set; }
            public decimal Transport { get; set; }
            public decimal Accomadation { get; set; }
            public decimal OtherAllowance { get; set; }
            public string PICName { get; set; }
            public string PosName { get; set; }
            public string PrintBy { get; set; }
            public string TotDt { get; set; }
            public decimal Total { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string CityCode { get; set; }
            public string MainCur { get; set; }
            public string Result { get; set; }
            public string Transportation { get; set; }
            public string HOInd { get; set; }
            public string PosName2 { get; set; }
            public string SignName { get; set; }
        }

        class TravelRequest2
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string TravelService { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public string PICName { get; set; }
            public string PosName { get; set; }
            public string PrintBy { get; set; }
            public string CityName { get; set; }
            public string CityCode { get; set; }
            public string Result { get; set; }
            public string Transportation { get; set; }
            public string HOInd { get; set; }
            public string SignName { get; set; }
        }

        private class VoucherReqDtl
        {
            public string DocNo { get; set; }
            public string EmpNAme { get; set; }
        }


        private class VoucherReqHdr2
        {
            public string CompanyName { get; set; }
            public string DocNo { get; set; }
            public string DocNoVR { get; set; }
            public string DocDt { get; set; }
            public string DNo { get; set; }
            public string PICEmpName { get; set; }
            public string PICPosName { get; set; }
            public string PICGrdLvlName { get; set; }
            public string TravelService { get; set; }
            public string Transportation { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public string result { get; set; }
            public string CityName { get; set; }
            public string CityCode { get; set; }
            public string PrintBy { get; set; }
            public string CompanyLogo { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string EmpName { get; set; }
            public string PosName { get; set; }
            public string GrdLvlName { get; set; }
            public string Result { get; set; }
            public string RemarkHdr { get; set; }
            public string ProfitCenterName { get; set; }
            public string HOInd { get; set; }
        }

        private class VoucherReqDtl2
        {
            public string VoucherRequestDocNo { get; set; }
            public string DocNo { get; set; }
            public string EmpNAme { get; set; }
        }

        private class VoucherReqDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class VoucherReqDtl4
        {
            public string DocNo { get; set; }
            public string EmpCode { get; set; }
            public string AdName { get; set; }
            public decimal Amt { get; set; }
            public decimal Qty { get; set; }
            public decimal Total { get; set; }
        }
        #endregion        

    }
}
