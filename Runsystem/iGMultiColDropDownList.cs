﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

namespace RunSystem
{
    public class iGMultiColDropDownList : IiGDropDownControl 
    {
        #region iGMultiColDropDownListItem

        /// <summary>
        /// Represents an item in the multi-column drop-down list.
        /// </summary>
        private class iGMultiColDropDownListItem
        {
            #region Fields

            /// <summary>
            /// Stores the key of the row in the multi-column drop-down list 
            /// which corresponds to the this item.
            /// </summary>
            private string fRowKey;

            /// <summary>
            /// The multi-column drop-down list which this item is from.
            /// </summary>
            private iGMultiColDropDownList fList;

            #endregion

            #region Methods

            /// <summary>
            /// Creates a new instance of the iGMultiColDropDownListItem class.
            /// </summary>
            public iGMultiColDropDownListItem(string rowKey, iGMultiColDropDownList list)
            {
                fList = list;
                fRowKey = rowKey;
            }

            /// <summary>
            /// Gets the string representation of the multi-column drop-down 
            /// list item. The value returned by this method is used to
            /// display text in the cells this item is assigned to.
            /// </summary>
            public override string ToString()
            {
                if (fList.fGrid.Cols.Count == 0)
                    return String.Empty;

                return fList.fGrid.Cells[fRowKey, fList.fDisplayTextColIndex].Text;
            }

            #endregion

            #region Properties

            /// <summary>
            /// Gets the key of the row in the multi-column drop-down list 
            /// which corresponds to the this item.
            /// </summary>
            public string RowKey
            {
                get
                {
                    return fRowKey;
                }
            }

            #endregion
        }

        #endregion

        #region Fields

        /// <summary>
        /// The grid which is used to store and display the 
        /// multi-column drop-down list items.
        /// </summary>
        private iGrid fGrid;

        /// <summary>
        /// Determines whether the grid was changed and its 
        /// sizes should be adjusted before 
        /// the grid is shown.
        /// </summary>
        private bool fGridWasChanged;

        /// <summary>
        /// Determines maximal the count of rows which can 
        /// can be displayed in the multi-columns drop-down 
        /// list at a time.
        /// </summary>
        private int fMaxVisibleRowCount = 20;

        /// <summary>
        /// The maximal width of the grid used in the multi-column
        /// drop-down list.
        /// </summary>
        private int fMaxWidth = -1;

        /// <summary>
        /// The width of the grid used in the multi-column
        /// drop-down list.
        /// </summary>
        private int fGridWidth = 0;

        /// <summary>
        /// The height of the grid used in the multi-column
        /// drop-down list.
        /// </summary>
        private int fGridHeight = 0;

        /// <summary>
        /// The grid which this multi-column drop-down list is shown in.
        /// </summary>
        private iGrid fParentGrid;

        /// <summary>
        /// The last key used in the grid. Each row in the grid used in the 
        /// multi-column drop-down list has a string key. This key is used 
        /// to identify a row and is a string representation of an integer 
        /// number. When a new item is added to the drop-down list, this number
        /// is increased. So none of the rows have the same keys.
        /// </summary>
        private int fLastKey = 0;

        /// <summary>
        /// The index of the column which is used as the cell value.
        /// </summary>
        private int fValueColIndex = 0;

        /// <summary>
        /// The index of the column which is used as the cell display text.
        /// </summary>
        private int fDisplayTextColIndex = 0;

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new instance of the iGMultiColDropDownList class.
        /// </summary>
        public iGMultiColDropDownList()
        {
            // Create and set up the grid.
            fGrid = new iGrid();
            fGrid.BorderStyle = iGBorderStyle.None;
            fGrid.RowMode = true;
            fGrid.ReadOnly = true;
            fGrid.GridLines.Mode = iGGridLinesMode.None;
            fGrid.DrawAsFocused = true;
            fGrid.RightToLeft = RightToLeft.Yes;

            // Add the grid's event handlers.
            fGrid.CellMouseDown += new iGCellMouseDownEventHandler(fGrid_CellMouseDown);
            fGrid.CellMouseEnter += new iGCellMouseEnterLeaveEventHandler(fGrid_CellMouseEnter);
            fGrid.Resize += new EventHandler(fGrid_Resize);
            fGrid.KeyDown += new KeyEventHandler(fGrid_KeyDown);
        }

        /// <summary>
        /// Adds a column to the drop-down list.
        /// </summary>
        /// <param name="isValueCol">
        /// Indicates whether the column should be used as the cell value.
        /// </param>
        /// <param name="isDisplayTextCol">
        /// Indicates whether the column should be used as the cell display text.
        /// </param>
        public iGCol AddCol(string name, bool isValueCol, bool isDisplayTextCol)
        {
            iGCol myCol = fGrid.Cols.Add(name, name);

            fGridWasChanged = true;

            if (isValueCol)
                fValueColIndex = myCol.Index;

            if (isDisplayTextCol)
                fDisplayTextColIndex = myCol.Index;

            return myCol;
        }

        /// <summary>
        /// Adds a row to the drop-down list.
        /// </summary>
        public void AddRow(object[] values)
        {
            if (values == null)
                throw new ArgumentNullException();

            iGRow myRow = fGrid.Rows.Add();
            for (int myColIndex = 0; myColIndex < Math.Min(values.Length, fGrid.Cols.Count); myColIndex++)
                myRow.Cells[myColIndex].Value = values[myColIndex];

            myRow.Key = fLastKey.ToString();
            fLastKey++;

            fGridWasChanged = true;
        }

        /// <summary>
        /// Commits editing.
        /// </summary>
        private void fGrid_CellMouseDown(object sender, TenTec.Windows.iGridLib.iGCellMouseDownEventArgs e)
        {
            fGrid.CurRow = fGrid.Rows[e.RowIndex];

            fParentGrid.CommitEditCurCell();
        }

        /// <summary>
        /// Highlights the row under the mouse pointer.
        /// </summary>
        private void fGrid_CellMouseEnter(object sender, TenTec.Windows.iGridLib.iGCellMouseEnterLeaveEventArgs e)
        {
            fGrid.CurRow = fGrid.Rows[e.RowIndex];
        }

        /// <summary>
        /// Memorizes the width and height of the grid
        /// when the drop-down form is resized.
        /// </summary>
        private void fGrid_Resize(object sender, EventArgs e)
        {
            if (fGrid.Visible)
            {
                fGridWidth = fGrid.Width;
                fGridHeight = fGrid.Height;
            }
        }

        /// <summary>
        /// Commits or cancels editing.
        /// </summary>
        private void fGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (fGrid.CurRow != null)
                    fParentGrid.CommitEditCurCell();
            }
            else if (e.KeyCode == Keys.Escape)
                fParentGrid.CancelEditCurCell();
        }

        /// <summary>
        /// Calculates the width and height of the grid.
        /// </summary>
        private void AdjustGridColAndRowSizes()
        {
            fGrid.Cols.AutoWidth();

            fGrid.Rows.AutoHeight();

            fGridHeight = 0;
            bool myShowVScrollBar = false;

            if (fGrid.Header.Visible)
                fGridHeight += fGrid.Header.Height;

            foreach (iGRow myRow in fGrid.Rows)
            {
                if (myRow.Index >= fMaxVisibleRowCount)
                {
                    myShowVScrollBar = true;
                    break;
                }
                fGridHeight = (fGridHeight + myRow.Height);
            }

            fGridWidth = 0;
            bool myShowHScrollBar = false;

            foreach (iGCol myCol in fGrid.Cols)
                fGridWidth += myCol.Width;

            if (myShowVScrollBar)
                fGridWidth += fGrid.VScrollBar.Width;

            if (fMaxWidth >= 0)
            {
                if (fGridWidth > fMaxWidth)
                {
                    fGridWidth = fMaxWidth;
                    myShowHScrollBar = true;
                }
            }

            if (myShowHScrollBar)
                fGridHeight += fGrid.HScrollBar.Height;

            fGridWasChanged = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the maximal count of rows visible at a time 
        /// in the multi-column drop-down list.
        /// </summary>
        public int MaxVisibleRowCount
        {
            get
            {
                return fMaxVisibleRowCount;
            }
            set
            {
                fMaxVisibleRowCount = value;
                fGridWasChanged = true;
            }
        }

        /// <summary>
        /// Gets or sets the minimal width of the multi-column 
        /// drop-down list.
        /// </summary>
        public int MaxWidth
        {
            get
            {
                return fMaxWidth;
            }
            set
            {
                fMaxWidth = value;
                fGridWasChanged = true;
            }
        }

        /// <summary>
        /// Determines whether iGrid substitutes the text entered into
        /// a combo box cell with the corresponding item from the attached 
        /// drop-down list when you hit the ENTER key.
        /// </summary>
        public bool AutoSubstitution
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region IiGDropDownControl Members

        /// <summary>
        /// Gets a value indicating whether the value from the drop-down
        /// list should be saved into a cell when the user clicks another form or control
        /// to finish editing.
        /// </summary>
        bool IiGDropDownControl.CommitOnHide
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the control to be shown in the drop-down form.
        /// </summary>
        Control IiGDropDownControl.GetDropDownControl(iGrid grid, Font font, Type interfaceType)
        {
            fParentGrid = grid;

            if (fGrid.Font != font)
            {
                fGrid.Font = font;
                fGridWasChanged = true;
            }

            fGrid.RightToLeft = grid.RightToLeft;

            if (fGridWasChanged)
                AdjustGridColAndRowSizes();

            return fGrid;
        }

        /// <summary>
        /// Returns the drop-down list item which corresponds to the specified text.
        /// </summary>
        object IiGDropDownControl.GetItemByText(string text)
        {
            if (fGrid.Cols.Count == 0)
                return null;

            foreach (iGRow myRow in fGrid.Rows)
            {
                if (string.Compare(myRow.Cells[fDisplayTextColIndex].Text, text) == 0)
                    return new iGMultiColDropDownListItem(myRow.Key, this);
            }

            return null;
        }

        /// <summary>
        /// Returns the drop-down list item which corresponds to the specified cell value.
        /// </summary>
        object IiGDropDownControl.GetItemByValue(object value, bool firstByOrder)
        {
            if (fGrid.Cols.Count == 0)
                return null;

            foreach (iGRow myRow in fGrid.Rows)
            {
                //Change By {TKG}
                if (myRow.Cells[fValueColIndex].Value != null && value != null)
                    if (value.Equals(myRow.Cells[fValueColIndex].Value))
                        return new iGMultiColDropDownListItem(myRow.Key, this);
                else
                    if (myRow.Cells[fValueColIndex].Value == value)
                        return new iGMultiColDropDownListItem(myRow.Key, this);
            }

            return null;
        }

        /// <summary>
        /// Returns the image index of the specified drop-down list item.
        /// </summary>
        int IiGDropDownControl.GetItemImageIndex(object item)
        {
            return -1;
        }

        /// <summary>
        /// Returns the cell value which is associated with the specified drop-down list item.
        /// </summary>
        object IiGDropDownControl.GetItemValue(object item)
        {
            try
            {
                iGRow myRow = fGrid.Rows[(item as iGMultiColDropDownListItem).RowKey];
                return myRow.Cells[fValueColIndex].Value;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the height of the drop-down control.
        /// </summary>
        int IiGDropDownControl.Height
        {
            get
            {
                return fGridHeight;
            }
        }

        /// <summary>
        /// Return the image list used in the drop-down list.
        /// </summary>
        ImageList IiGDropDownControl.ImageList
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the maximal width of the drop-down control.
        /// </summary>
        int IiGDropDownControl.MaxWidth
        {
            get
            {
                return -1;
            }
        }

        /// <summary>
        /// Returns the minimal width of the drop-down control.
        /// </summary>
        int IiGDropDownControl.MinWidth
        {
            get
            {
                return -1;
            }
        }

        /// <summary>
        /// Gets or sets the item selected in the drop-down list.
        /// </summary>
        object IiGDropDownControl.SelectedItem
        {
            get
            {
                if (fGrid.CurRow == null)
                    return null;

                return new iGMultiColDropDownListItem(fGrid.CurRow.Key, this);
            }
            set
            {
                fGrid.HScrollBar.Value = 0;

                if (value == null)
                {
                    fGrid.CurRow = null;
                    fGrid.VScrollBar.Value = 0;
                    return;
                }

                try
                {
                    iGRow myRow = fGrid.Rows[(value as iGMultiColDropDownListItem).RowKey];
                    fGrid.CurRow = myRow;
                }
                catch
                { }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the drop-down form can be resized by user.
        /// </summary>
        bool IiGDropDownControl.Sizeable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the width of the drop-down control.
        /// </summary>
        int IiGDropDownControl.Width
        {
            get
            {
                return fGridWidth;
            }
        }

        /// <summary>
        /// Called by iGrid after the drop-down control has been hidden.
        /// </summary>
        void IiGDropDownControl.OnHide()
        {
            //throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Called by iGrid after the drop-down control has been shown.
        /// </summary>
        void IiGDropDownControl.OnShow()
        {
            //throw new Exception("The method or operation is not implemented.");
        }

        #endregion

    }
}
