﻿#region Update
/*
    16/12/2020 [WED/IMS] new apps (PI with approval)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoice3Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseInvoice3 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmPurchaseInvoice3Dlg2(FrmPurchaseInvoice3 FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document Number",
                        "", 
                        "Document"+Environment.NewLine+"Date",
                        "Amount Type",
                        
                        //6-8
                        "Type",
                        "Currency",
                        "Amount"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            // 1=Discount (Total), 2=Bea Cukai, 3=Uang Muka

            SQL.AppendLine("Select Tbl1.AmtType, Tbl2.OptDesc As AmtTypeDesc, Tbl1.DocNo, Tbl1.DocDt, Tbl1.VdCode, ");
            SQL.AppendLine("(   Select T3.CurCode ");
            SQL.AppendLine("    From TblPODtl T1 ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr T3 On T2.QtDocNo=T3.DocNo ");
            SQL.AppendLine("    Where T1.DocNo=Tbl1.DocNo Limit 1) As CurCode, ");
            SQL.AppendLine("Tbl1.Amt From (");
            SQL.AppendLine("    Select '1' As AmtType, A.DocNo, A.DocDt, A.VdCode, ");
            SQL.AppendLine("    (A.DiscountAmt ");
            SQL.AppendLine("        -IfNull((Select Sum(T2.Amt) From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='1' And T2.PODocNo=A.DocNo), 0) ");
            SQL.AppendLine("        +IfNull((Select Sum(T2.Amt) From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='1' And T2.PurchaseInvoiceDocNo=A.DocNo), 0) ");
            SQL.AppendLine("    )As Amt ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Where IfNull(A.DiscountAmt, 0)<>0 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '2' As AmtType, A.DocNo, A.DocDt, A.VdCode, ");
            SQL.AppendLine("    (A.CustomsTaxAmt ");
            SQL.AppendLine("        -IfNull((Select Sum(T2.Amt) From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='2' And T2.PODocNo=A.DocNo), 0) ");
            SQL.AppendLine("        +IfNull((Select Sum(T2.Amt) From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='2' And T2.PurchaseInvoiceDocNo=A.DocNo), 0) ");
            SQL.AppendLine("    )As Amt ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Where IfNull(A.CustomsTaxAmt, 0)<>0 ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '3' As AmtType, A.DocNo, A.DocDt, A.VdCode, ");
            SQL.AppendLine("    (A.DownPayment ");
            SQL.AppendLine("        -IfNull((Select Sum(T2.Amt) From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='3' And T2.PODocNo=A.DocNo), 0) ");
            SQL.AppendLine("        +IfNull((Select Sum(T2.Amt) From TblPurchaseReturnInvoiceHdr T1, TblPurchaseReturnInvoiceDtl2 T2 Where T1.DocNo=T2.DocNo And T1.CancelInd='N' And T2.AmtType='3' And T2.PurchaseInvoiceDocNo=A.DocNo), 0) ");
            SQL.AppendLine("    )As Amt ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Where IfNull(A.DownPayment, 0)<>0 ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Left Join TblOption Tbl2 On Tbl2.OptCat='InvoiceAmtType' And Tbl1.AmtType=Tbl2.OptCode ");
            SQL.AppendLine("Where IfNull(Tbl1.Amt, 0)<>0 ");
            SQL.AppendLine("And Tbl1.VdCode=@VdCode ");
            SQL.AppendLine("And Tbl1.DocNo In (Select Distinct PODocNo from TblRecvVdDtl Where CancelInd='N' And PODocNo=Tbl1.DocNo) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And Concat(Tbl1.DocNo, Tbl1.AmtType) Not In (" + mFrmParent.GetSelectedPO() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocNo, AmtTypeDesc",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            
                            //1-5
                            "DocDt", "AmtType", "AmtTypeDesc", "CurCode", "Amt"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 5);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 8);

                        mFrmParent.Grd2.Cells[Row1, 9].Value = null;

                        mFrmParent.ComputeAmt();

                        mFrmParent.Grd2.Rows.Add();
                        mFrmParent.Grd2.Cells[mFrmParent.Grd2.Rows.Count - 1, 7].Value = 0;
                        mFrmParent.Grd2.Cells[mFrmParent.Grd2.Rows.Count - 1, 8].Value = 0;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 data.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int Index = 0; Index <= mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd2, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd2, Index, 4),
                    Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 5)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmPO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        #endregion

        #endregion
    }
}
