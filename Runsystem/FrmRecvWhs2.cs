﻿#region Update
/*
    04/13/2017 [WED] tambah approval per item
    17/07/2017 [TKG] tambah journal antar entity
    15/04/2018 [TKG] ubah proses journal untuk beda entity
    17/04/2018 [TKG] ubah proses journal untuk beda entity menjadi 2 dokumen
    17/07/2018 [TKG] tambah cost center saat journal
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    27/08/2019 [WED] tambah tab kawasan berikat, BC262
    14/10/2019 [WED/MAI] masuk ke stock movement, kolom CustomsDocCode
    23/10/2019 [DITA/MAI] tambah KBContractNo, KBSubmissionNo, KBRegistrationNo saat save
    16/12/2019 [TKG/IMS] journal untuk moving average
    27/04/2020 [DITA/MMM] tambah param IsRecvWhs2AllowedToEditLotBin agar lot dan bin bisa diedit
    12/08/2020 [TKG/ASA] saat show data , informasi stock diambil dari stok yg ada di gudang pengirim
    08/09/2020 [DITA/MAI] menambahkan field Nilai Jaminan (Insurance Amount), nomor SKEP (Decree#) di Tab Kawasan Berikat
    10/09/2020 [WED/IOK] KB tidak diinput selain MAI
    05/11/2020 [DITA/IMS] Judul printout ganti : BPGM
    23/02/2021 [DITA/IMS] Format Printout baru ims
    28/04/2021 [WED/IMS] penyesuaian journal baru berdasarkan parameter RecvWhs2JournalFormula (default : 1)
 *  19/05/2021 [ICA/IMS] Mengubah JnDesc menjadi Returning Item From Production untuk mRecvWhs2JournalFormula != 1
    28/06/2021 [MYA/IMS] Menambahkan kolom spesification dan local code tidak di hide KETIKA INSERT (di detail). Menu Returning item from production
    02/12/2021 [WED/IMS] MovingAvgPrice ambil dari stockmovement saat journal
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mKBInsuranceNo = string.Empty,
            mKBInsuranceDt = string.Empty,
            mKBInsuranceDueDt = string.Empty,
            mKBDecreeDt = string.Empty;
        internal FrmRecvWhs2Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType1 = "15",
            mDocType2 = "16",
            mRecvWhs2CustomsDocCodeManualInput = string.Empty,
            mRecvWhs2JournalFormula = string.Empty
            ;            
        internal bool
            mIsItGrpCodeShow = false,
            mIsRecvWhs2ApprovalBasedOnWhs = false,
            mIsShowForeignName = false;
        private bool 
            mIsAutoJournalActived = false, 
            mIsEntityMandatory = false,
            mIsKawasanBerikatEnabled = false, 
            mIsMovingAvgEnabled = false,
            mIsRecvWhs2AllowedToEditLotBin = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmRecvWhs2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item From Other Warehouse (Without DO)";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsKawasanBerikatEnabled) Tp2.PageVisible = true; else Tp2.PageVisible = false;
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                SetLueLot(ref LueLot);
                LueLot.Visible = false;
                SetLueBin(ref LueBin);
                LueBin.Visible = false;

                if (mIsKawasanBerikatEnabled)
                {
                    TcRecvWhs.SelectedTabPage = Tp2;
                    Sl.SetLueOption(ref LueCustomsDocCode, "CustomsDocCode262");
                }

                TcRecvWhs.SelectedTabPage = Tp1;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Property Code",
                        "Property",
                        "Batch#",

                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Sender's"+Environment.NewLine+"Stock",
                        "Quantity",

                        //16-20
                        "UoM",
                        "Sender's"+Environment.NewLine+"Stock",
                        "Quantity",
                        "UoM",
                        "Sender's"+Environment.NewLine+"Stock",

                        //21-25
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Group",
                        "Foreign Name",

                        //26-27
                        "Status",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        20, 
                        //1-5
                        50, 50, 20, 100, 20, 
                        //6-10
                        150, 250, 0, 80, 200, 
                        //11-15
                        200, 100, 100, 80, 80, 
                        //16-20
                        80, 80, 80, 80, 80, 
                        //21-25
                        80, 80, 400, 100, 180,
                        //26-27
                        100, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 22, 24, 25, 26, 27 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Grd1.Cols[26].Move(2);
            Grd1.Cols[27].Move(9);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 8, 9, 11, 17, 18, 19, 20, 21, 22, 24, 25, 26 }, false);
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[24].Visible = true;
                Grd1.Cols[24].Move(7);
            }

            if (!mIsShowForeignName)
            {
                Grd1.Cols[25].Visible = true;
                Grd1.Cols[25].Move(8);
            }

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        "Total Quantity 1", "Total Quantity 2", "Total Quantity 3"
                    },
                    new int[] 
                    {
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 1, 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 11, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true); 
                Sm.GrdColInvisible(Grd2, new int[] { 1 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, ChkKBNonDocInd, LueCustomsDocCode,
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty, TxtKBInsuranceAmt, TxtKBDecreeNo
                    }, true);
                    BtnKBContractNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 15, 18, 21, 23, 12, 13 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, ChkKBNonDocInd, LueCustomsDocCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 23 });
                    if (mIsKawasanBerikatEnabled) BtnKBContractNo.Enabled = false;
                    if (mIsRecvWhs2AllowedToEditLotBin) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 12, 13 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, LueCustomsDocCode,
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty,
                TxtKBDecreeNo, TxtKBInsuranceAmt
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 1, 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvWhs2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint((int)mNumberOfInventoryUomCode);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvWhs2Dlg(this, Sm.GetLue(LueWhsCode2)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 23 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }

                    if (Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
                    {
                        LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e, 12);
                        //SetLueLot(ref LueLot);
                    }

                    if (Sm.IsGrdColSelected(new int[] { 13 }, e.ColIndex))
                    {
                        LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e, 13);
                        //SetLueBin(ref LueBin);
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalQty();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmRecvWhs2Dlg(this, Sm.GetLue(LueWhsCode2)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 23 }, e);

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
            }

            if (e.ColIndex == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 18, 15, 21, 19, 16, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 18, 21, 15, 19, 22, 16);
            }

            if (e.ColIndex == 21)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 21, 15, 18, 22, 16, 19);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 21, 18, 15, 22, 19, 16);
            }

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);

            if (Sm.IsGrdColSelected(new int[] { 1, 15, 18, 21 }, e.ColIndex)) ComputeTotalQty();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 15, 18, 21 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvWhs2", "TblRecvWhs2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvWhs2Hdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) 
                    cml.Add(SaveRecvWhs2Dtl(DocNo, Row, IsDocApprovalSettingNotExisted()));

            if (IsDocApprovalSettingNotExisted())
            {
                cml.Add(SaveStockMovement(DocNo, "", "N"));
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                        cml.Add(SaveStockSummary(1, Row));
                }
            }

            if (mRecvWhs2JournalFormula == "1")
            {
                if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournal(DocNo));
            }
            else
            {
                if (mIsAutoJournalActived)
                    cml.Add(SaveJournal2(DocNo));
            }
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocType From TblDocApprovalSetting ");
            SQL.AppendLine("Where UserCode Is not Null ");
            SQL.AppendLine("And DocType='RecvWhs2' ");
            if (mIsRecvWhs2ApprovalBasedOnWhs)
            {
                SQL.AppendLine("And WhsCode = @WhsCode ");
            }
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));

            if (!Sm.IsDataExist(cm))
                //Sm.StdMsg(mMsgType.Warning, "Nobody will approve this request.");
                return true;
            else
                return false;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Transfer from") ||
                Sm.IsLueEmpty(LueWhsCode, "Transfer to") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            ReComputeStock(Sm.GetLue(LueWhsCode2));
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                Msg =
                   "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                   "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                   "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                   "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                   "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                   "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                   "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than transferred quantity.");
                    return true;
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than transferred quantity (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[21].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than transferred quantity (3).");
                        return true;
                    }
                }
            }
            return false;
        }
        
        private MySqlCommand SaveRecvWhs2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRecvWhs2Hdr(DocNo, DocDt, WhsCode, WhsCode2, Remark, ");
            SQL.AppendLine("CustomsDocCode, KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, ");
            SQL.AppendLine("KBRegistrationDt, KBSubmissionNo, KBPackaging, KBPackagingQty, KBNonDocInd, ");
            SQL.AppendLine("KBInsuranceNo, KBInsuranceDt, KBInsuranceDueDt, KBInsuranceAmt, KBDecreeNo, KBDecreeDt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @WhsCode2, @Remark, ");
            SQL.AppendLine("@CustomsDocCode, @KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, ");
            SQL.AppendLine("@KBRegistrationDt, @KBSubmissionNo, @KBPackaging, @KBPackagingQty, @KBNonDocInd, ");
            SQL.AppendLine("@KBInsuranceNo, @KBInsuranceDt, @KBInsuranceDueDt, @KBInsuranceAmt, @KBDecreeNo, @KBDecreeDt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", Sm.GetLue(LueCustomsDocCode));
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@KBPackaging", TxtKBPackaging.Text);

            if (TxtKBPackagingQty.Text.Length == 0) Sm.CmParam<Decimal>(ref cm, "@KBPackagingQty", 0m);
            else Sm.CmParam<Decimal>(ref cm, "@KBPackagingQty", decimal.Parse(TxtKBPackagingQty.Text));

            Sm.CmParam<String>(ref cm, "@KBNonDocInd", ChkKBNonDocInd.Checked ? "Y" : "N");
            if (mIsKawasanBerikatEnabled)
            {
                Sm.CmParam<String>(ref cm, "@KBInsuranceNo", mKBInsuranceNo);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDt", mKBInsuranceDt);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDueDt", mKBInsuranceDueDt);
                Sm.CmParam<Decimal>(ref cm, "@KBInsuranceAmt", TxtKBInsuranceAmt.Text.Length > 0 ? decimal.Parse(TxtKBInsuranceAmt.Text) : 0m);
                Sm.CmParam<String>(ref cm, "@KBDecreeNo", TxtKBDecreeNo.Text);
                Sm.CmParam<String>(ref cm, "@KBDecreeDt", mKBDecreeDt);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@KBInsuranceNo", string.Empty);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDt", string.Empty);
                Sm.CmParam<String>(ref cm, "@KBInsuranceDueDt", string.Empty);
                Sm.CmParam<Decimal>(ref cm, "@KBInsuranceAmt", 0m);
                Sm.CmParam<String>(ref cm, "@KBDecreeNo", string.Empty);
                Sm.CmParam<String>(ref cm, "@KBDecreeDt", string.Empty);
            }
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvWhs2Dtl(string DocNo, int Row, bool NoNeedApproval)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblRecvWhs2Dtl(DocNo, DNo, CancelInd, Status, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, 'N', @Status, @ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQLDtl.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQLDtl.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
                SQLDtl.AppendLine("From TblDocApprovalSetting T ");
                SQLDtl.AppendLine("Where T.DocType='RecvWhs2' ");
                if (mIsRecvWhs2ApprovalBasedOnWhs)
                    SQLDtl.AppendLine("And IfNull(T.WhsCode, '')=@WhsCode ");
                SQLDtl.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo, string DNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            if (CancelInd == "N")
            {
                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, @CancelInd, A.DocDt, A.WhsCode2, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblRecvWhs2Hdr A ");
                SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And B.Status='A' ");
                SQL.AppendLine("And B.CancelInd='N'; ");

                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("CustomsDocCode, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, @CancelInd, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("A.CustomsDocCode, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblRecvWhs2Hdr A ");
                SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And B.Status='A' ");
                SQL.AppendLine("And B.CancelInd='N'; ");

                SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
                SQL.AppendLine("0, 0, 0, Null, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblRecvWhs2Hdr A ");
                SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Left Join TblStockSummary C ");
                SQL.AppendLine("    On A.WhsCode=C.WhsCode ");
                SQL.AppendLine("    And B.Lot=C.Lot ");
                SQL.AppendLine("    And B.Bin=C.Bin ");
                SQL.AppendLine("    And B.Source=C.Source ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And C.WhsCode Is Null ");
                SQL.AppendLine("And B.Status='A' ");
                SQL.AppendLine("And B.CancelInd='N'; ");
            }
            else
            {
                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, @CancelInd, A.DocDt, A.WhsCode2, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblRecvWhs2Hdr A ");
                SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And B.Status='A' ");
                SQL.AppendLine("And Position(Concat('##', B.DNo, '##') In @DNo)>0; ");

                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, @CancelInd, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblRecvWhs2Hdr A ");
                SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And B.Status='A' ");
                SQL.AppendLine("And Position(Concat('##', B.DNo, '##') In @DNo)>0; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (DNo.Length > 0) Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DocType1", mDocType1);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(Byte Type, int Row)
        {
            //Type=1 -> Insert
            //Type=2 -> Edit
            var SQL = new StringBuilder();

            if (Type == 1)
            {
                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where WhsCode=@WhsCode2 And Lot=@Lot And Bin=@Bin And Source=@Source; ");
            }
            else
            {
                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where WhsCode=@WhsCode2 And Lot=@Lot And Bin=@Bin And Source=@Source; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvWhs2Hdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo, ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            #region Journal 1

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Other Warehouse (Without DO) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs2, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs2Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On C.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            #endregion

            #region Journal 2

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo2, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Other Warehouse (Without DO) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs2Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo2 Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");
            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode2)));

            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvWhs2Hdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Returning Item From Production : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs2, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs2Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo2 As AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForDOToProductOfWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo, A.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Inner Join TblStockMovement E On A.DocNo=E.DocNo And A.DNo=E.DNo And E.CancelInd='N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, A.Qty*IfNull(F.MovingAvgPrice, 0.00) As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblStockMovement F On A.DocNo=F.DocNo And A.DNo=F.DNo And F.CancelInd='N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo2 As AcNo, 0.00 As DAmt, A.Qty*IfNull(E.MovingAvgPrice, 0.00) AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Inner Join TblStockMovement E On A.DocNo=E.DocNo And A.DNo=E.DNo And E.CancelInd='N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, 0.00 As DAmt, A.Qty*IfNull(F.MovingAvgPrice, 0.00) AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForDOToProductOfWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblStockMovement F On A.DocNo=F.DocNo And A.DNo=F.DNo And F.CancelInd='N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");

            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode2)));

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (IsCancelledDataNotValid(DNo) || Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelRecvWhsDtl(DNo));

            cml.Add(SaveStockMovement(TxtDocNo.Text, DNo, "Y"));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 &&
                    Sm.GetGrdBool(Grd1, Row, 1) &&
                    !Sm.GetGrdBool(Grd1, Row, 2) &&
                    Sm.GetGrdStr(Grd1, Row, 26) == "Approved") 
                    cml.Add(SaveStockSummary(2, Row));

            if (mRecvWhs2JournalFormula == "1")
            {
                if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())
                    cml.Add(SaveJournal());
            }
            else
            {
                if (mIsAutoJournalActived)
                    cml.Add(SaveJournal2());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblRecvWhs2Dtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            ReComputeStock(Sm.GetLue(LueWhsCode));
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) ||
                IsGrdValueNotValid2()
                ;
        }

        private bool IsGrdValueNotValid2()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                {
                    Msg =
                       "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                       "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                       "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                       "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                       "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                       "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                       "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                       "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                    
                    if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than cancelled quantity.");
                        return true;
                    }

                    if (Grd1.Cols[18].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than cancelled quantity (2).");
                            return true;
                        }
                    }

                    if (Grd1.Cols[21].Visible)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than cancelled quantity (3).");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }


        private MySqlCommand CancelRecvWhsDtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvWhs2Dtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            #region Old Code

            //SQL.AppendLine("Insert Into TblStockMovement ");
            //SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            //SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            //SQL.AppendLine("Qty, Qty2, Qty3, ");
            //SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            //SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            //SQL.AppendLine("Qty*-1, Qty2*-1, Qty3*-1, ");
            //SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblStockMovement ");
            //SQL.AppendLine("Where DocNo=@DocNo ");
            //SQL.AppendLine("And DocType In (@DocType1,@DocType2)  ");
            //SQL.AppendLine("And CancelInd='N' ");
            //SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            //SQL.AppendLine("Update TblStockSummary As T1 ");
            //SQL.AppendLine("Inner Join ( ");
            //SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source, ");
            //SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            //SQL.AppendLine("    From TblStockMovement A ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source ");
            //SQL.AppendLine("        From TblStockMovement ");
            //SQL.AppendLine("        Where DocType In (@DocType1, @DocType2) ");
            //SQL.AppendLine("        And DocNo=@DocNo ");
            //SQL.AppendLine("        And CancelInd='Y' ");
            //SQL.AppendLine("        And Position(Concat('##', DNo, '##') In @DNo)>0 ");
            //SQL.AppendLine("    ) B ");
            //SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            //SQL.AppendLine("        And A.Lot=B.Lot ");
            //SQL.AppendLine("        And A.Bin=B.Bin ");
            //SQL.AppendLine("        And A.ItCode=B.ItCode ");
            //SQL.AppendLine("        And A.PropCode=B.PropCode ");
            //SQL.AppendLine("        And A.BatchNo=B.BatchNo ");
            //SQL.AppendLine("        And A.Source=B.Source ");
            //SQL.AppendLine("    Where (A.Qty<>0 Or A.Qty2<>0 Or A.Qty3<>0) ");
            //SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode, A.PropCode, A.BatchNo, A.Source ");
            //SQL.AppendLine(") T2 ");
            //SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            //SQL.AppendLine("    And T1.Lot=T2.Lot ");
            //SQL.AppendLine("    And T1.Bin=T2.Bin ");
            //SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
            //SQL.AppendLine("    And T1.PropCode=T2.PropCode ");
            //SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
            //SQL.AppendLine("    And T1.Source=T2.Source ");
            //SQL.AppendLine("Set ");
            //SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            //SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            //SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            //SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            //SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            //Sm.CmParam<String>(ref cm, "@DocType1", mDocType1);
            //Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            #region Journal 1

            SQL.AppendLine("Update TblRecvWhs2Dtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblRecvWhs2Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblRecvWhs2Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, ");
            SQL.AppendLine("    Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On C.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            #endregion

            #region Journal 2

            SQL.AppendLine("Update TblRecvWhs2Dtl Set ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblRecvWhs2Hdr Where DocNo=@DocNo And JournalDocNo2 Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo2, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo2 From TblRecvWhs2Hdr Where DocNo=@DocNo And JournalDocNo2 Is Not Null);");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, ");
            SQL.AppendLine("    Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(F.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null And E.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg F On B.ItCode=F.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs2Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs2Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");

            #endregion

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(CurrentDt, 2));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(DocDt, 2));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            SQL.AppendLine("Update TblRecvWhs2Dtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
            SQL.AppendLine("And Exists(Select 1 From TblRecvWhs2Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblRecvWhs2Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, ");
            SQL.AppendLine("    Sum(T.DAmt) As CAmt, Sum(T.CAmt) As DAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo2 As AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'N' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForDOToProductOfWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo, A.Qty*IfNull(E.MovingAvgPrice, 0.00) As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Left Join TblItemMovingAvg E On C.ItCode = E.ItCode ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, A.Qty*IfNull(F.MovingAvgPrice, 0.00) As DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Left Join TblItemMovingAvg F On C.ItCode = F.ItCode ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, D.AcNo2 As AcNo, 0.00 As DAmt, A.Qty*IfNull(E.MovingAvgPrice, 0.00) AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Left Join TblItemMovingAvg E On C.ItCode = E.ItCode ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select @EntCodeWhs2 As EntCode, E.ParValue As AcNo, 0.00 As DAmt, A.Qty*IfNull(F.MovingAvgPrice, 0.00) AS CAmt ");
            SQL.AppendLine("        From TblRecvWhs2Dtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source = B.Source " + Filter.Replace("B.", "A."));
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode And D.AcNo2 Is Not Null And D.MovingAvgInd = 'Y' ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode = 'AcNoForDOToProductOfWIP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Left Join TblItemMovingAvg F On C.ItCode = F.ItCode ");
            SQL.AppendLine("        Where A.DocNo = @DocNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowRecvWhs2Hdr(DocNo);
                ShowRecvWhs2Dtl(DocNo);
                ComputeTotalQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvWhs2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, WhsCode, WhsCode2, Remark, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, ");
            SQL.AppendLine("KBPackaging, KBPackagingQty, KBNonDocInd, KBSubmissionNo, CustomsDocCode, ");
            SQL.AppendLine("KBInsuranceAmt, KBDecreeNo ");
            SQL.AppendLine("From TblRecvWhs2Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DocNo", 
                    "DocDt", "WhsCode", "WhsCode2", "Remark", "KBContractNo", 
                    "KBContractDt", "KBPLNo", "KBPLDt", "KBRegistrationNo", "KBRegistrationDt", 
                    "KBPackaging", "KBPackagingQty", "KBNonDocInd","KBSubmissionNo", "CustomsDocCode",
                    "KBInsuranceAmt", "KBDecreeNo" 
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                    Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[3]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    TxtKBContractNo.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[6]));
                    TxtKBPLNo.EditValue = Sm.DrStr(dr, c[7]);
                    Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[8]));
                    TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[9]);
                    Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[10]));
                    TxtKBPackaging.EditValue = Sm.DrStr(dr, c[11]);
                    TxtKBPackagingQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    ChkKBNonDocInd.Checked = Sm.DrStr(dr, c[13]) == "Y";
                    TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[14]);
                    Sm.SetLue(LueCustomsDocCode, Sm.DrStr(dr, c[15]));
                    TxtKBInsuranceAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                    TxtKBDecreeNo.EditValue = Sm.DrStr(dr, c[17]);
                }, true
            );
        }

        private void ShowRecvWhs2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("IfNull(E.Qty, 0.00) As Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0.00) As Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0.00) As Stock3, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case B.Status When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else 'Approved' End As Status, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.Remark, C.ItGrpCode, C.ForeignName, C.Specification ");
            SQL.AppendLine("From TblRecvWhs2Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode2=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "Stock", "Qty", "InventoryUomCode", "Stock2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "Stock3", "Qty3", "InventoryUomCode3", "Remark",
 
                    //21-24
                    "ItGrpCode", "ForeignName", "Status", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e,
          int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, Col).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;

        }


        public static void SetLueLot(ref LookUpEdit Lue)
        {
          Sm.SetLue1(ref Lue, "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot ;" , "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
          Sm.SetLue1(ref Lue, "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;", "Bin");
        }

        private bool IsCustomsDocCodeInputManual()
        {
            if (mRecvWhs2CustomsDocCodeManualInput.Length > 0)
            {
                var CustomsDocCode = Sm.GetLue(LueCustomsDocCode);
                if (CustomsDocCode.Length == 0) return false;
                var l = new List<string>(mRecvWhs2CustomsDocCodeManualInput.Split(',').Select(s => s));
                foreach (var s in l)
                    if (Sm.CompareStr(CustomsDocCode, s)) return true;
            }
            return false;
        }

        private string GetCostCenterWarehouse(string WhsCode)
        {
            return Sm.GetValue("Select CCCode From TblWarehouse Where WhsCode=@Param;", WhsCode);
        }

        private bool IsEntityDifferent()
        {
            var EntCode1 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var EntCode2 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2));
            return (EntCode1 != EntCode2);
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsItGrpCodeShow = Sm.GetParameter("IsItGrpCodeShow") == "Y";
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "N";
            mIsRecvWhs2ApprovalBasedOnWhs = Sm.GetParameter("IsRecvWhs2ApprovalBasedOnWhs") == "Y";
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mIsEntityMandatory = Sm.GetParameter("IsEntityMandatory") == "Y";
            mIsKawasanBerikatEnabled = Sm.GetParameter("KB_Server").Length > 0;
            mRecvWhs2CustomsDocCodeManualInput = Sm.GetParameter("RecvWhs2CustomsDocCodeManualInput");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mIsRecvWhs2AllowedToEditLotBin = Sm.GetParameterBoo("IsRecvWhs2AllowedToEditLotBin");
            mRecvWhs2JournalFormula = Sm.GetParameter("RecvWhs2JournalFormula");

            if (mRecvWhs2JournalFormula.Length == 0) mRecvWhs2JournalFormula = "1";
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock(string WhsCode)
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ParPrint(int parValue)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RecvWhs2>();
            var ldtl = new List<RecvWhs2Dtl>();
            int Numb = 0;

            string[] TableName = { "RecvWhs2", "RecvWhs2Dtl" };
            List<IList> myLists = new List<IList>();

            //NON IMS

            if (Sm.GetParameter("Doctitle") != "IMS")
            {
                #region Header
                var cm = new MySqlCommand();

                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='DocTitle') As 'DocTitle', ");
                SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, C.WhsName As WhsName2, A.Remark ");
                SQL.AppendLine("From TblRecvWhs2Hdr A");
                SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
                SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode2 = C.WhsCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo",
                         "DocDt",
                          //6-9
                         "WhsName",
                         "WhsName2",
                         "Remark",
                         "DocTitle"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new RecvWhs2()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyPhone = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),

                                WhsName = Sm.DrStr(dr, c[6]),
                                WhsName2 = Sm.DrStr(dr, c[7]),
                                HRemark = Sm.DrStr(dr, c[8]),
                                DocTitle = Sm.DrStr(dr, c[9]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail
                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;
                    SQLDtl.AppendLine("Select B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                    SQLDtl.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, ");

                    SQLDtl.AppendLine("Case When B.CancelInd='N' Then B.Qty Else 0 End +IfNull(( ");
                    SQLDtl.AppendLine("    Select Sum(Qty) From TblStockSummary ");
                    SQLDtl.AppendLine("    Where WhsCode=A.WhsCode2 And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                    SQLDtl.AppendLine("), 0) As AvailableStock, ");

                    SQLDtl.AppendLine("Case When B.CancelInd='N' Then B.Qty2 Else 0 End +IfNull(( ");
                    SQLDtl.AppendLine("    Select Sum(Qty2) From TblStockSummary ");
                    SQLDtl.AppendLine("    Where WhsCode=A.WhsCode2 And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                    SQLDtl.AppendLine("), 0) As AvailableStock2, ");

                    SQLDtl.AppendLine("Case When B.CancelInd='N' Then B.Qty3 Else 0 End +IfNull(( ");
                    SQLDtl.AppendLine("    Select Sum(Qty3) From TblStockSummary ");
                    SQLDtl.AppendLine("    Where WhsCode=A.WhsCode2 And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                    SQLDtl.AppendLine("), 0) As AvailableStock3, C.ItGrpCode ");


                    SQLDtl.AppendLine("From TblRecvWhs2Hdr A ");
                    SQLDtl.AppendLine("Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo And B.CancelInd = 'N' ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "BatchNo",
                         "Source",
                         "Lot",
                         "Bin",

                         //6-10
                         "Qty" ,
                         "Qty2",
                         "Qty3",
                         "InventoryUomCode" ,
                         "InventoryUomCode2" ,

                         //11-15
                         "InventoryUomCode3",
                         "Remark",
                         "AvailableStock",
                         "AvailableStock2",
                         "AvailableStock3",

                         //16
                         "ItGrpCode"
                        });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new RecvWhs2Dtl()
                            {
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),

                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                                Source = Sm.DrStr(drDtl, cDtl[3]),
                                Lot = Sm.DrStr(drDtl, cDtl[4]),
                                Bin = Sm.DrStr(drDtl, cDtl[5]),

                                Qty = Sm.DrDec(drDtl, cDtl[6]),
                                Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                                Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                                InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                                InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),

                                InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                                DRemark = Sm.DrStr(drDtl, cDtl[12]),
                                AvailableStock = Sm.DrDec(drDtl, cDtl[13]),
                                AvailableStock2 = Sm.DrDec(drDtl, cDtl[14]),
                                AvailableStock3 = Sm.DrDec(drDtl, cDtl[15]),

                                ItGrpCode = Sm.DrStr(drDtl, cDtl[16])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                switch (parValue)
                {
                    case 1:
                        Sm.PrintReport("RecvWhs2satu", myLists, TableName, false);
                        break;
                    case 2:
                        Sm.PrintReport("RecvWhs2dua", myLists, TableName, false);
                        break;
                    case 3:
                        Sm.PrintReport("RecvWhs2tiga", myLists, TableName, false);
                        break;
                }
            }

            //IMS

            else
            {
                #region Header

                var SQL = new StringBuilder();
                
                SQL.AppendLine("Select Group_concat(Distinct IfNull(F.DocNo, '') Separator '\r\n') SOContractDocNo ");
                SQL.AppendLine("From TblRecvWhs2Dtl A ");
                SQL.AppendLine("INNER Join TblProjectGroup B On A.BatchNo=B.ProjectCode ");
                SQL.AppendLine("INNER Join TblLOPHdr C On B.PGCode = C.PGCode ");
                SQL.AppendLine("INNER Join TblBOQHdr D On C.DocNo = D.LOPDocNo ");
                SQL.AppendLine("INNER Join TblSOContractHdr E On D.DocNo = E.BOQDocNo ");
                SQL.AppendLine("INNER Join TblSOCONtractDtl F On E.DocNo = F.DocNo  And A.ItCode = F.ItCode And E.CancelInd = 'N'  ");
                SQL.AppendLine("Where A.DocNo = @Param;  ");

                l.Add(new RecvWhs2()
                {
                    CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo", @Sm.CompanyLogo()),
                    DocNo = TxtDocNo.Text,
                    DocDt = DteDocDt.Text,
                    SOContractDocNo = Sm.GetValue(SQL.ToString(), TxtDocNo.Text),
                    WhsName = LueWhsCode2.Text,
                    WhsName2 = LueWhsCode.Text
                });

                myLists.Add(l);

                #endregion

                #region Detail

                for (int i = 0; i < Grd1.Rows.Count-1; i++)
                {
                    Numb = Numb + 1;

                    ldtl.Add(new RecvWhs2Dtl()
                    {
                        Number = Numb,
                        ItCodeInternal = Sm.GetGrdStr(Grd1, i, 6),
                        ItName = Sm.GetGrdStr(Grd1, i, 7),
                        Specification = Sm.GetValue("Select Specification From TblItem Where ItCode = @Param", Sm.GetGrdStr(Grd1, i, 4)),
                        InventoryUomCode = Sm.GetGrdStr(Grd1, i, 16),
                        Qty = Sm.GetGrdDec(Grd1, i, 15),
                        DRemark = Sm.GetGrdStr(Grd1, i, 23),
                       
                    });
                }
                myLists.Add(ldtl);

                Sm.PrintReport("RecvWhs2IMS", myLists, TableName, false);

                #endregion
            }

          
        }

        internal void ComputeTotalQty()
        {
            decimal Total = 0m;
            int col = 15;
            int col2 = 0;
            while (col <= 18)
            {
                Total = 0m;
                for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                    if (Sm.GetGrdStr(Grd1, row, col).Length != 0) Total += Sm.GetGrdDec(Grd1, row, col);

                if (col == 15) col2 = 0;
                if (col == 18) col2 = 1;
                if (col == 21) col2 = 2;
                Grd2.Cells[0, col2].Value = Total;
                col += 3;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Click

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueWhsCode2, "Transfer From") && !Sm.IsLueEmpty(LueCustomsDocCode, "Customs document code"))
                Sm.FormShowDialog(new FrmRecvWhs2Dlg2(this, Sm.GetLue(LueCustomsDocCode)));
        }

        #endregion

        #region Misc Control Event
        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            ClearGrd();
        }

        private void LueCustomsDocCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCustomsDocCode, new Sm.RefreshLue2(Sl.SetLueOption), "CustomsDocCode262");
                if (IsCustomsDocCodeInputManual())
                {
                    BtnKBContractNo.Enabled = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty,
                        TxtKBInsuranceAmt, TxtKBDecreeNo
                    }, false);
                }
                else
                {
                    BtnKBContractNo.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty,
                        TxtKBInsuranceAmt, TxtKBDecreeNo
                    }, true);
                }

                if (TxtDocNo.Text.Length == 0)
                {
                    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                    {
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBDecreeNo
                    });
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtKBPackagingQty, TxtKBInsuranceAmt }, 0);
                    ChkKBNonDocInd.Checked = false;
                    ClearGrd();
                }
            }
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 12)
            {
                string mLot = string.Empty;
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 12].Value = LueLot.GetColumnValue("Col1");
                    mLot = LueLot.GetColumnValue("Col1").ToString();
                }
                LueLot.Visible = false;
            }
        }
        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 13)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 13].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 13].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        #endregion

        #endregion
    }

    #region Report Class

    class RecvWhs2
    {
        public string CompanyLogo { set; get; }

        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }

        public string WhsName { get; set; }
        public string WhsName2 { get; set; }
        public string HRemark { get; set; }
        public string DocTitle { get; set; }
        public string PrintBy { get; set; }

        public string SOContractDocNo { get; set; }
    }

    class RecvWhs2Dtl
    {
        public int Number { get; set; }
        public string ItCode { get; set; }

        public string ItName { get; set; }
        public string Specification { get; set; }
        public string ItCodeInternal { get; set; }
        public string BatchNo { get; set; }
        public string Source { get; set; }
        public string Lot { get; set; }
        public string Bin { get; set; }

        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string InventoryUomCode { get; set; }
        public string InventoryUomCode2 { get; set; }

        public string InventoryUomCode3 { get; set; }
        public decimal AvailableStock { get; set; }
        public decimal AvailableStock2 { get; set; }
        public decimal AvailableStock3 { get; set; }
        public string DRemark { get; set; }
        public string ItGrpCode { get; set; }
    }

    #endregion
}
