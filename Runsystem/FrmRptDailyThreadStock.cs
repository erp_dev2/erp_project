﻿#region Update
/*
    21/05/2019 [DITA/KSM] Reporting baru KSM
    29/05/2019 [DITA/KSM] command condition Qty <> 0 di query A
    10/01/2020 [WED/KSM] tambah kolom uom
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDailyThreadStock : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterByItCt = false;
        int mNumberOfInventoryUomCode = 1;

        #endregion

        #region Constructor

        public FrmRptDailyThreadStock(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("Select A.WhsCode, D.WhsName, A.ItCode, C.ItName, C.ItCodeInternal, B.DocDt, B.RemarkQC, B.Lot, B.VdCode, E.VdName, B.ExSupplier, ");
            //SQL.AppendLine("B.PropCode,Sum(F.Init) As QtyInit1,Sum(F.Init2) As QtyInit2,Sum(F.Init3) As QtyInit3,Sum(F.I) As QtyI1,Sum(F.I2) As QtyI2, Sum(F.I3) As QtyI3, "); 
            //SQL.AppendLine("Sum(F.O) QtyO1, Sum(F.O2) QtyO2, Sum(F.O3) QtyO3 ");
            //SQL.AppendLine("From TblStockSummary A ");
            //SQL.AppendLine("Inner Join TblWarehouse D On D.WhsCode = A.WhsCode And D.WhsCode = @WhsCode And A.Qty <> 0 ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select Distinct Concat(T1.WhsCode, T2.Source, T1.DocDt, T2.Remark, T2.Lot, T3.VdCode, T4.Remark, T6.PropCode), ");
            //SQL.AppendLine("    T1.WhsCode, T2.Source, T1.DocDt, T2.Remark As RemarkQC, T2.Lot, T3.VdCode, T4.Remark As ExSupplier, ");
            //SQL.AppendLine("    T6.PropCode ");
            //SQL.AppendLine("    From TblRecvvdhdr T1 ");
            //SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("        And Left(T1.DocDt, 6)=Concat(@Yr, @Mth) And T1.WhsCode=@WhsCode ");
            //SQL.AppendLine("    Inner Join TblPoHdr T3 On T3.DocNo = T2.PODocNo  ");
            //SQL.AppendLine("    Inner Join TblPoDtl T4 On T4.DocNo = T3.DocNo And T4.DNo = T2.PODNo ");
            //SQL.AppendLine("    Inner Join TblPoRequestHdr T5 On T5.DocNo = T4.PORequestDocNo ");
            //SQL.AppendLine("    Inner Join TblPorequestDtl T6 On T6.DocNo = T5.DocNo And T6.DNo = T4.PORequestDNo ");
            //SQL.AppendLine(") B On A.WhsCode=B.WhsCode And A.Source = B.Source ");
            //SQL.AppendLine("Inner Join TblItem C On C.ItCode=A.ItCode ");
            //SQL.AppendLine("Left Join TblVendor E On E.VdCode=B.VdCode ");
            //SQL.AppendLine("Inner Join (  ");
            //SQL.AppendLine("                Select A.ItCode, E.ItName, ");
            //SQL.AppendLine("                E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            //SQL.AppendLine("                IfNull(B.Init, 0.00) As Init, ");
            //SQL.AppendLine("                IfNull(B.Init2, 0.00) As Init2, ");
            //SQL.AppendLine("                IfNull(B.Init3, 0.00) As Init3, ");
            //SQL.AppendLine("                IfNull(B.TInitPrice, 0.00) As TInitPrice, ");
            //SQL.AppendLine("                IfNull(C.I, 0.00) As I, ");
            //SQL.AppendLine("                IfNull(C.I2, 0.00) As I2, ");
            //SQL.AppendLine("                IfNull(C.I3, 0.00) As I3, ");
            //SQL.AppendLine("                IfNull(C.TIPrice, 0.00) As TIPrice, ");
            //SQL.AppendLine("                IfNull(D.O, 0.00) As O, ");
            //SQL.AppendLine("                IfNull(D.O2, 0.00) As O2, ");
            //SQL.AppendLine("                IfNull(D.O3, 0.00) As O3, ");
            //SQL.AppendLine("                IfNull(D.TOPrice, 0.00) As TOPrice ");
            //SQL.AppendLine("                From ( ");
            //SQL.AppendLine("                    Select Distinct ItCode ");
            //SQL.AppendLine("                    From TblStockSummary ");
            //SQL.AppendLine("                    Where WhsCode=@WhsCode ");
            //SQL.AppendLine("                   And Qty <> 0 ");
            //SQL.AppendLine("                ) A -- dapatkan item dari StockMovement selama satu bulan ");
            //SQL.AppendLine("                Left Join ( ");
            //SQL.AppendLine("                    Select Tbl.ItCode, Sum(Tbl.Qty) As Init, Sum(Tbl.Qty2) As Init2, Sum(Tbl.Qty3) As Init3, Sum(Tbl.Total) As TInitPrice ");
            //SQL.AppendLine("                    From ( ");
            //SQL.AppendLine("                        Select T1.ItCode, T1.Qty, T1.Qty2, T1.Qty3, (T1.Qty * T2.ExcRate * T2.UPrice) As Total ");
            //SQL.AppendLine("                        From ( ");
            //SQL.AppendLine("                            Select A.ItCode, A.Source, ");
            //SQL.AppendLine("                            Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3 ");
            //SQL.AppendLine("                            From TblStockMovement A ");
            //SQL.AppendLine("                            Where Left(A.DocDt, 6)<Concat(@Yr, @Mth) ");
            //SQL.AppendLine("                            And A.WhsCode=@WhsCode ");
            //SQL.AppendLine("                            Group By A.ItCode, A.Source ");
            //SQL.AppendLine("                            Having Sum(A.Qty)<>0.00 ");
            //SQL.AppendLine("                        ) T1 ");
            //SQL.AppendLine("                        Inner Join TblStockPrice T2 On T1.Source = T2.Source "); 
            //SQL.AppendLine("                    ) Tbl ");
            //SQL.AppendLine("                    Group By Tbl.ItCode ");
            //SQL.AppendLine("                ) B On A.ItCode = B.ItCode -- initial stock ");
            //SQL.AppendLine("                Left Join ( ");
            //SQL.AppendLine("                    Select T1.ItCode, ");
            //SQL.AppendLine("                    Sum(T1.Qty) As I, Sum(T1.Qty2) As I2, Sum(T1.Qty3) As I3, ");
            //SQL.AppendLine("                    Sum(T1.Qty*T2.ExcRate*T2.UPrice) As TIPrice ");
            //SQL.AppendLine("                    From ( ");
            //SQL.AppendLine("                        Select DocType, DocNo, DNo, Source, ItCode, ");
            //SQL.AppendLine("                        Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            //SQL.AppendLine("                        From TblStockMovement ");
            //SQL.AppendLine("                        Where Left(DocDt, 6)=Concat(@Yr, @Mth) ");
            //SQL.AppendLine("                        And WhsCode=@WhsCode ");
            //SQL.AppendLine("                        Group By DocType, DocNo, DNo, Source, ItCode ");
            //SQL.AppendLine("                        Having Sum(Qty)>0.00 ");
            //SQL.AppendLine("                    ) T1 ");
            //SQL.AppendLine("                    Inner Join TblStockPrice T2 On T1.Source=T2.Source ");
            //SQL.AppendLine("                    Group By T1.ItCode ");
            //SQL.AppendLine("                ) C On A.ItCode = C.ItCode -- stock in ");
            //SQL.AppendLine("                Left Join ( ");
            //SQL.AppendLine("                    Select T1.ItCode, ");
            //SQL.AppendLine("                    Sum(T1.Qty) As O, Sum(T1.Qty2) As O2, Sum(T1.Qty3) As O3, ");
            //SQL.AppendLine("                    Sum(T1.Qty*T2.ExcRate*T2.UPrice) As TOPrice ");
            //SQL.AppendLine("                    From ( ");
            //SQL.AppendLine("                        Select DocType, DocNo, DNo, Source, ItCode, ");
            //SQL.AppendLine("                        Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            //SQL.AppendLine("                        From TblStockMovement ");
            //SQL.AppendLine("                        Where Left(DocDt, 6)=Concat(@Yr, @Mth) ");
            //SQL.AppendLine("                        And WhsCode=@WhsCode ");
            //SQL.AppendLine("                        Group By DocType, DocNo, DNo, Source, ItCode ");
            //SQL.AppendLine("                        Having Sum(Qty)<0.00 ");
            //SQL.AppendLine("                    ) T1 ");
            //SQL.AppendLine("                    Inner Join TblStockPrice T2 On T1.Source=T2.Source ");
            //SQL.AppendLine("                    Group By T1.ItCode ");
            //SQL.AppendLine("                ) D On A.ItCode=D.ItCode -- stock out ");
            //SQL.AppendLine("                Inner Join TblItem E On A.ItCode = E.ItCode ");
            //SQL.AppendLine("                Inner Join TblItemCategory F On E.ItCtCode = F.ItCtCode ");
            //SQL.AppendLine("                Where (IfNull(B.Init, 0.00)<>0.00 Or IfNull(C.I, 0.00)<>0.00 Or IfNull(D.O, 0.00)<>0.00) ");
            //SQL.AppendLine("            ) F On A.ItCode = F.ItCode ");

            #endregion

            SQL.AppendLine("Select T.ItCode, T.ItName, ");
            SQL.AppendLine("T.InventoryUomCode, T.InventoryUomCode2, T.InventoryUomCode3, ");
            SQL.AppendLine("T.WhsCode, T.WhsName, T.DocDt, T.ItCodeInternal, T.RemarkQC, T.Lot, T.VdCode, T.VdName, T.ExSupplier, T.PropCode, ");
            SQL.AppendLine("Sum(T.Init) As QtyInit1, ");
            SQL.AppendLine("Sum(T.Init2) As QtyInit2, ");
            SQL.AppendLine("Sum(T.Init3) As QtyInit3, ");
            SQL.AppendLine("Sum(T.TInitPrice) As InitPrice, ");
            SQL.AppendLine("Sum(T.I) As QtyI1, ");
            SQL.AppendLine("Sum(T.I2) As QtyI2, ");
            SQL.AppendLine("Sum(T.I3) As QtyI3, ");
            SQL.AppendLine("Sum(T.TIPrice) As IPrice, ");
            SQL.AppendLine("Sum(T.O) QtyO1, ");
            SQL.AppendLine("Sum(T.O2) QtyO2, ");
            SQL.AppendLine("Sum(T.O3) QtyO3, ");
            SQL.AppendLine("Sum(T.TOPrice) OPrice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.ItCode, E.ItName, ");
            SQL.AppendLine("    E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("    A.WhsCode, A1.WhsName, G.DocDt, E.ItCodeInternal, G.RemarkQC, G.Lot, G.VdCode, G.VdName, G.ExSupplier, G.PropCode, ");
            SQL.AppendLine("    IfNull(B.Init, 0.00) As Init, ");
            SQL.AppendLine("    IfNull(B.Init2, 0.00) As Init2, ");
            SQL.AppendLine("    IfNull(B.Init3, 0.00) As Init3, ");
            SQL.AppendLine("    IfNull(B.TInitPrice, 0.00) As TInitPrice, ");
            SQL.AppendLine("    IfNull(C.I, 0.00) As I, ");
            SQL.AppendLine("    IfNull(C.I2, 0.00) As I2, ");
            SQL.AppendLine("    IfNull(C.I3, 0.00) As I3, ");
            SQL.AppendLine("    IfNull(C.TIPrice, 0.00) As TIPrice, ");
            SQL.AppendLine("    IfNull(D.O, 0.00) As O, ");
            SQL.AppendLine("    IfNull(D.O2, 0.00) As O2, ");
            SQL.AppendLine("    IfNull(D.O3, 0.00) As O3, ");
            SQL.AppendLine("    IfNull(D.TOPrice, 0.00) As TOPrice ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct Source, ItCode, WhsCode ");
            SQL.AppendLine("        From TblStockSummary ");
            SQL.AppendLine("        Where WhsCode=@WhsCode ");
            //SQL.AppendLine("        And Qty <> 0 "); // tambahan karna ada item yang tidak cocok di Stock Summary, 07/02/2019
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("    And ItCode In (");
                SQL.AppendLine("        Select T.ItCode From TblItem T ");
                SQL.AppendLine("        Where Exists( ");
                SQL.AppendLine("            Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("            Where ItCtCode=T.ItCtCode ");
                SQL.AppendLine("            And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    ) A -- dapatkan item dari StockMovement selama satu bulan ");
            SQL.AppendLine("    Inner Join TblWarehouse A1 On A.WhsCode = A1.WhsCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select Tbl.Source, Tbl.ItCode, Sum(Tbl.Qty) As Init, Sum(Tbl.Qty2) As Init2, Sum(Tbl.Qty3) As Init3, Sum(Tbl.Total) As TInitPrice ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select T1.Source, T1.ItCode, T1.Qty, T1.Qty2, T1.Qty3, (T1.Qty * T2.ExcRate * T2.UPrice) As Total ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select A.ItCode, A.Source, ");
            SQL.AppendLine("                Sum(A.Qty) Qty, Sum(A.Qty2) Qty2, Sum(A.Qty3) Qty3 ");
            SQL.AppendLine("                From TblStockMovement A ");
            SQL.AppendLine("                Where Left(A.DocDt, 6)<Concat(@Yr, @Mth) ");
            SQL.AppendLine("                And A.WhsCode=@WhsCode ");
            SQL.AppendLine("                Group By A.ItCode, A.Source ");
            SQL.AppendLine("                Having Sum(A.Qty)<>0.00 ");
            SQL.AppendLine("            ) T1 ");
            SQL.AppendLine("            Inner Join TblStockPrice T2 On T1.Source = T2.Source ");
            SQL.AppendLine("        ) Tbl ");
            SQL.AppendLine("        Group By Tbl.Source, Tbl.ItCode ");
            SQL.AppendLine("    ) B On A.Source = B.Source -- initial stock ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.Source, T1.ItCode, ");
            SQL.AppendLine("        Sum(T1.Qty) As I, Sum(T1.Qty2) As I2, Sum(T1.Qty3) As I3, ");
            SQL.AppendLine("        Sum(T1.Qty*T2.ExcRate*T2.UPrice) As TIPrice ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select DocType, DocNo, DNo, Source, ItCode, ");
            SQL.AppendLine("            Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("            From TblStockMovement ");
            SQL.AppendLine("            Where Left(DocDt, 6)=Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And WhsCode=@WhsCode  ");
            SQL.AppendLine("            Group By DocType, DocNo, DNo, Source, ItCode ");
            SQL.AppendLine("            Having Sum(Qty)>0.00 ");
            SQL.AppendLine("        ) T1 ");
            SQL.AppendLine("        Inner Join TblStockPrice T2 On T1.Source=T2.Source ");
            SQL.AppendLine("        Group By T1.Source, T1.ItCode ");
            SQL.AppendLine("    ) C On A.Source = C.Source -- stock in ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.Source, T1.ItCode, ");
            SQL.AppendLine("        Sum(T1.Qty) As O, Sum(T1.Qty2) As O2, Sum(T1.Qty3) As O3, ");
            SQL.AppendLine("        Sum(T1.Qty*T2.ExcRate*T2.UPrice) As TOPrice ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select DocType, DocNo, DNo, Source, ItCode, ");
            SQL.AppendLine("            Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("            From TblStockMovement ");
            SQL.AppendLine("            Where Left(DocDt, 6)=Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And WhsCode=@WhsCode ");
            SQL.AppendLine("            Group By DocType, DocNo, DNo, Source, ItCode ");
            SQL.AppendLine("            Having Sum(Qty)<0.00 ");
            SQL.AppendLine("        ) T1 ");
            SQL.AppendLine("        Inner Join TblStockPrice T2 On T1.Source=T2.Source ");
            SQL.AppendLine("        Group By T1.Source, T1.ItCode ");
            SQL.AppendLine("    ) D On A.Source=D.Source -- stock out ");
            SQL.AppendLine("    Inner Join TblItem E On A.ItCode = E.ItCode ");

            if (TxtItCode.Text.Length > 0)
            {
                SQL.AppendLine("        And (A.ItCode Like @ItCode Or E.ItName Like @ItCode Or E.ForeignName Like @ItCode) ");
            }

            if (TxtItCodeInternal.Text.Length > 0)
            {
                SQL.AppendLine("        And E.ItCodeInternal Like @ItCodeInternal ");
            }

            SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode = F.ItCtCode ");

            if (TxtExSupplier.Text.Length > 0 || Sm.GetLue(LueVdCode).Length > 0) SQL.AppendLine("    Inner Join ");
            else SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct Concat(X1.WhsCode, X41.WhsName, X2.Source, X1.DocDt, X2.ItCode, X7.ItName, X7.ItCodeInternal, X2.Remark, X2.Lot, X1.VdCode, X4.VdName, X5.Remark, X8.PropName), ");
            SQL.AppendLine("        X1.WhsCode, X41.WhsName, X2.Source, X1.DocDt, X2.ItCode, X7.ItName, X7.ItCodeInternal, X2.Remark As RemarkQC, X2.Lot, X1.VdCode, X4.VdName, X5.Remark As ExSupplier, X8.PropName As PropCode ");
            SQL.AppendLine("        From TblRecvVdHdr X1 ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl X2 On X1.DocNo = X2.DocNo And Left(X1.DocDt, 6) = Concat(@Yr, @Mth) ");
            SQL.AppendLine("            And X1.WhsCode = @WhsCode And X2.CancelInd = 'N' ");
            SQL.AppendLine("            And X2.Status In ('A', 'O') And X1.POInd = 'Y' ");
            SQL.AppendLine("        Inner Join TblPOHdr X3 On X2.PODocNo = X3.DocNo And X3.Status = 'A' ");
            
            if(Sm.GetLue(LueVdCode).Length > 0)
                SQL.AppendLine("            And X3.VdCode = @VdCode ");
            
            SQL.AppendLine("        Inner Join TblVendor X4 On X3.VdCode = X4.VdCode ");
            SQL.AppendLine("        Inner Join TblWarehouse X41 On X1.WhsCode = X41.WhsCode ");
            SQL.AppendLine("        Inner Join TblPODtl X5 On X2.PODocNo = X5.DocNo And X2.PODNo = X5.DNo ");

            if(TxtExSupplier.Text.Length > 0)
                SQL.AppendLine("            And X5.Remark Is Not Null And X5.Remark Like @ExSupplier ");

            SQL.AppendLine("        Inner Join TblPORequestDtl X6 On X5.PORequestDocNo = X6.DocNo And X5.PORequestDNo = X6.DNo And X6.Status = 'A' ");
            SQL.AppendLine("        Inner Join TblItem X7 On X2.ItCode = X7.ItCode ");
            if (TxtItCode.Text.Length > 0)
                SQL.AppendLine("        And (X7.ItCode Like @ItCode Or X7.ItName Like @ItCode Or X7.ForeignName Like @ItCode) ");
            
            if (TxtItCodeInternal.Text.Length > 0)
                SQL.AppendLine("        And X7.ItCodeInternal Like @ItCodeInternal ");
            
            SQL.AppendLine("        Left Join TblProperty X8 On X6.PropCode = X8.PropCode ");
            SQL.AppendLine("    ) G On A.Source = G.Source ");

            SQL.AppendLine("    Where (IfNull(B.Init, 0.00)<>0.00 Or IfNull(C.I, 0.00)<>0.00 Or IfNull(D.O, 0.00)<>0.00) ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By T.ItCode, T.ItName, ");
            SQL.AppendLine("T.InventoryUomCode, T.InventoryUomCode2, T.InventoryUomCode3, ");
            SQL.AppendLine("T.DocDt, T.ItCodeInternal, T.RemarkQC, T.Lot, T.VdCode, T.VdName, T.ExSupplier, T.PropCode ");
            SQL.AppendLine("Order By T.ItName; ");


            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 37;
            //Grd1.FrozenArea.ColCount = 3;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 100;
            Grd1.Header.Cells[0, 1].Value = "Warehouse Code";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 250;
            Grd1.Header.Cells[0, 2].Value = "Warehouse Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 100;
            Grd1.Header.Cells[0, 3].Value = "Item Code";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 250;
            Grd1.Header.Cells[0, 4].Value = "Item Name";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Cols[5].Width = 250;
            Grd1.Header.Cells[0, 5].Value = "Item Local";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Cols[6].Width = 100;
            Grd1.Header.Cells[0, 6].Value = "Vendor Code";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Cols[7].Width = 250;
            Grd1.Header.Cells[0, 7].Value = "Vendor Name";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Cols[8].Width = 250;
            Grd1.Header.Cells[0, 8].Value = "Ex-Supplier";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;

            Grd1.Cols[9].Width = 250;
            Grd1.Header.Cells[0, 9].Value = "Quality";
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 9].SpanRows = 2;

            Grd1.Cols[10].Width = 250;
            Grd1.Header.Cells[0, 10].Value = "Merge (Lot)";
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 10].SpanRows = 2;

            Grd1.Cols[11].Width = 250;
            Grd1.Header.Cells[0, 11].Value = "Arrival";
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 11].SpanRows = 2;

            Grd1.Header.Cells[1, 12].Value = "Initial Stock";
            Grd1.Header.Cells[1, 12].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 12].SpanCols = 6;
            Grd1.Header.Cells[0, 12].Value = "Quantity 1";
            Grd1.Header.Cells[0, 13].Value = "Uom 1";
            Grd1.Header.Cells[0, 14].Value = "Quantity 2";
            Grd1.Header.Cells[0, 15].Value = "Uom 2";
            Grd1.Header.Cells[0, 16].Value = "Quantity 3";
            Grd1.Header.Cells[0, 17].Value = "Uom 3";
            Grd1.Cols[12].Width = 80;
            Grd1.Cols[13].Width = 80;
            Grd1.Cols[14].Width = 80;
            Grd1.Cols[15].Width = 80;
            Grd1.Cols[16].Width = 80;
            Grd1.Cols[17].Width = 80;

            Grd1.Header.Cells[1, 18].Value = "Stock In";
            Grd1.Header.Cells[1, 18].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 18].SpanCols = 6;
            Grd1.Header.Cells[0, 18].Value = "Quantity 1";
            Grd1.Header.Cells[0, 19].Value = "Uom 1";
            Grd1.Header.Cells[0, 20].Value = "Quantity 2";
            Grd1.Header.Cells[0, 21].Value = "Uom 2";
            Grd1.Header.Cells[0, 22].Value = "Quantity 3";
            Grd1.Header.Cells[0, 23].Value = "Uom 3";
            Grd1.Cols[18].Width = 80;
            Grd1.Cols[19].Width = 80;
            Grd1.Cols[20].Width = 80;
            Grd1.Cols[21].Width = 80;
            Grd1.Cols[22].Width = 80;
            Grd1.Cols[23].Width = 80;

            Grd1.Header.Cells[1, 24].Value = "Stock Out";
            Grd1.Header.Cells[1, 24].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 24].SpanCols = 6;
            Grd1.Header.Cells[0, 24].Value = "Quantity 1";
            Grd1.Header.Cells[0, 25].Value = "Uom 1";
            Grd1.Header.Cells[0, 26].Value = "Quantity 2";
            Grd1.Header.Cells[0, 27].Value = "Uom 2";
            Grd1.Header.Cells[0, 28].Value = "Quantity 3";
            Grd1.Header.Cells[0, 29].Value = "Uom 3";
            Grd1.Cols[24].Width = 80;
            Grd1.Cols[25].Width = 80;
            Grd1.Cols[26].Width = 80;
            Grd1.Cols[27].Width = 80;
            Grd1.Cols[28].Width = 80;
            Grd1.Cols[29].Width = 80;

            Grd1.Header.Cells[1, 30].Value = "Last Stock";
            Grd1.Header.Cells[1, 30].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 30].SpanCols = 6;
            Grd1.Header.Cells[0, 30].Value = "Quantity 1";
            Grd1.Header.Cells[0, 31].Value = "Uom 1";
            Grd1.Header.Cells[0, 32].Value = "Quantity 2";
            Grd1.Header.Cells[0, 33].Value = "Uom 2";
            Grd1.Header.Cells[0, 34].Value = "Quantity 3";
            Grd1.Header.Cells[0, 35].Value = "Uom 3";
            Grd1.Cols[30].Width = 80;
            Grd1.Cols[31].Width = 80;
            Grd1.Cols[32].Width = 80;
            Grd1.Cols[33].Width = 80;
            Grd1.Cols[34].Width = 80;
            Grd1.Cols[35].Width = 80;

            Grd1.Cols[36].Width = 250;
            Grd1.Header.Cells[0, 36].Value = "Remark QC";
            Grd1.Header.Cells[0, 36].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 36].SpanRows = 2;

            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34 }, 0);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;

            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6, 14, 15, 16, 17, 20, 21, 22, 23, 26, 27, 28, 29, 32, 33, 34, 35 });
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 20, 21, 26, 27, 32, 33 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 20, 21, 22, 23, 26, 27, 28, 29, 32, 33, 34, 35 }, true);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueMth, "Month") || Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueWhsCode, "Warehouse")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                //string Filter = " Where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                Sm.CmParam<String>(ref cm, "@ItCode", string.Concat("%", TxtItCode.Text, "%"));
                Sm.CmParam<String>(ref cm, "@ItCodeInternal", string.Concat("%", TxtItCodeInternal.Text, "%"));
                Sm.CmParam<String>(ref cm, "@ExSupplier", string.Concat("%", TxtExSupplier.Text, "%"));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SetSQL(), //+ Filter + " Group By A.WhsCode, D.WhsName, A.ItCode, C.ItName, B.DocDt, B.RemarkQC, B.Lot, B.VdCode, E.VdName, B.ExSupplier, B.PropCode; ",
                        new string[]
                        { 
                            //0
                            "WhsCode", 
                            
                            //1-5
                            "WhsName", "ItCode", "ItName", "ItCodeInternal", "VdCode", 
                            
                            //6-10
                            "VdName", "ExSupplier", "PropCode", "Lot", "DocDt", 
                            
                            //11-15
                            "QtyInit1", "QtyInit2", "QtyInit3", "QtyI1", "QtyI2",

                            //16-20
                            "QtyI3", "QtyO1", "QtyO2", "QtyO3", "RemarkQC",

                            //21-23
                            "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3"
                        },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 23);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 20);
                            Grd.Cells[Row, 30].Value = Sm.GetGrdDec(Grd, Row, 12) + Sm.GetGrdDec(Grd, Row, 18) + Sm.GetGrdDec(Grd, Row, 24);
                            Grd.Cells[Row, 32].Value = Sm.GetGrdDec(Grd, Row, 14) + Sm.GetGrdDec(Grd, Row, 20) + Sm.GetGrdDec(Grd, Row, 26);
                            Grd.Cells[Row, 34].Value = Sm.GetGrdDec(Grd, Row, 16) + Sm.GetGrdDec(Grd, Row, 22) + Sm.GetGrdDec(Grd, Row, 28);
                             }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] 
                {  
                   12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34
                });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

         private void SetLueWhsCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

                SQL.AppendLine("Select WhsCode As Col1, WhsName As Col2 From TblWarehouse  ");
                SQL.AppendLine("Where Find_In_Set(IfNull(WhsCode, ''), ");
                SQL.AppendLine("IfNull((Select ParValue From TblParameter Where ParCode='DailyThreadStockWhsCode'), '')) ");
                SQL.AppendLine("Order By WhsName; ");

                Sm.SetLue2(
                        ref Lue,
                        SQL.ToString(),
                        0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion


        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(SetLueWhsCode));
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item Name");
        }

        private void TxtItCodeInternal_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCodeInternal_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item Local");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtExSupplier_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkExSupplier_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Ex-Supplier");
        }


        #endregion

    }
}
