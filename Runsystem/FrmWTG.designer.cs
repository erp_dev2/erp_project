﻿namespace RunSystem
{
    partial class FrmWTG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmWTG));
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtWTGName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtWTGCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnWTCode = new DevExpress.XtraEditors.SimpleButton();
            this.LueWTCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnWSRCode = new DevExpress.XtraEditors.SimpleButton();
            this.LueWSRCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTGName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWSRCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnWSRCode);
            this.panel2.Controls.Add(this.LueWSRCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.BtnWTCode);
            this.panel2.Controls.Add(this.LueWTCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtWTGName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtWTGCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 97);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 97);
            this.panel3.Size = new System.Drawing.Size(772, 376);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 376);
            this.Grd1.TabIndex = 21;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(336, 3);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // TxtWTGName
            // 
            this.TxtWTGName.EnterMoveNextControl = true;
            this.TxtWTGName.Location = new System.Drawing.Point(166, 27);
            this.TxtWTGName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWTGName.Name = "TxtWTGName";
            this.TxtWTGName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWTGName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWTGName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWTGName.Properties.Appearance.Options.UseFont = true;
            this.TxtWTGName.Properties.MaxLength = 40;
            this.TxtWTGName.Size = new System.Drawing.Size(371, 20);
            this.TxtWTGName.TabIndex = 14;
            this.TxtWTGName.Validated += new System.EventHandler(this.TxtWTGName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(8, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Working Time Group Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtWTGCode
            // 
            this.TxtWTGCode.EnterMoveNextControl = true;
            this.TxtWTGCode.Location = new System.Drawing.Point(166, 5);
            this.TxtWTGCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWTGCode.Name = "TxtWTGCode";
            this.TxtWTGCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWTGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWTGCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWTGCode.Properties.Appearance.Options.UseFont = true;
            this.TxtWTGCode.Properties.MaxLength = 16;
            this.TxtWTGCode.Size = new System.Drawing.Size(167, 20);
            this.TxtWTGCode.TabIndex = 11;
            this.TxtWTGCode.Validated += new System.EventHandler(this.TxtWTGCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Working Time Group Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnWTCode
            // 
            this.BtnWTCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWTCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWTCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWTCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWTCode.Appearance.Options.UseBackColor = true;
            this.BtnWTCode.Appearance.Options.UseFont = true;
            this.BtnWTCode.Appearance.Options.UseForeColor = true;
            this.BtnWTCode.Appearance.Options.UseTextOptions = true;
            this.BtnWTCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWTCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWTCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnWTCode.Image")));
            this.BtnWTCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWTCode.Location = new System.Drawing.Point(540, 48);
            this.BtnWTCode.Name = "BtnWTCode";
            this.BtnWTCode.Size = new System.Drawing.Size(24, 21);
            this.BtnWTCode.TabIndex = 17;
            this.BtnWTCode.ToolTip = "Add Contact Person";
            this.BtnWTCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWTCode.ToolTipTitle = "Run System";
            this.BtnWTCode.Click += new System.EventHandler(this.BtnWTCode_Click);
            // 
            // LueWTCode
            // 
            this.LueWTCode.EnterMoveNextControl = true;
            this.LueWTCode.Location = new System.Drawing.Point(166, 49);
            this.LueWTCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWTCode.Name = "LueWTCode";
            this.LueWTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWTCode.Properties.Appearance.Options.UseFont = true;
            this.LueWTCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWTCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWTCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWTCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWTCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWTCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWTCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWTCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWTCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWTCode.Properties.DropDownRows = 20;
            this.LueWTCode.Properties.NullText = "[Empty]";
            this.LueWTCode.Properties.PopupWidth = 300;
            this.LueWTCode.Size = new System.Drawing.Size(371, 20);
            this.LueWTCode.TabIndex = 16;
            this.LueWTCode.ToolTip = "F4 : Show/hide list";
            this.LueWTCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWTCode.EditValueChanged += new System.EventHandler(this.LueWTCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(80, 52);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Working Time";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnWSRCode
            // 
            this.BtnWSRCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnWSRCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnWSRCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWSRCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnWSRCode.Appearance.Options.UseBackColor = true;
            this.BtnWSRCode.Appearance.Options.UseFont = true;
            this.BtnWSRCode.Appearance.Options.UseForeColor = true;
            this.BtnWSRCode.Appearance.Options.UseTextOptions = true;
            this.BtnWSRCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnWSRCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnWSRCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnWSRCode.Image")));
            this.BtnWSRCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnWSRCode.Location = new System.Drawing.Point(540, 70);
            this.BtnWSRCode.Name = "BtnWSRCode";
            this.BtnWSRCode.Size = new System.Drawing.Size(24, 21);
            this.BtnWSRCode.TabIndex = 20;
            this.BtnWSRCode.ToolTip = "Add Contact Person";
            this.BtnWSRCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnWSRCode.ToolTipTitle = "Run System";
            this.BtnWSRCode.Click += new System.EventHandler(this.BtnWSRCode_Click);
            // 
            // LueWSRCode
            // 
            this.LueWSRCode.EnterMoveNextControl = true;
            this.LueWSRCode.Location = new System.Drawing.Point(166, 71);
            this.LueWSRCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWSRCode.Name = "LueWSRCode";
            this.LueWSRCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWSRCode.Properties.Appearance.Options.UseFont = true;
            this.LueWSRCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWSRCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWSRCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWSRCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWSRCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWSRCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWSRCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWSRCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWSRCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWSRCode.Properties.DropDownRows = 20;
            this.LueWSRCode.Properties.NullText = "[Empty]";
            this.LueWSRCode.Properties.PopupWidth = 300;
            this.LueWSRCode.Size = new System.Drawing.Size(371, 20);
            this.LueWSRCode.TabIndex = 19;
            this.LueWSRCode.ToolTip = "F4 : Show/hide list";
            this.LueWSRCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWSRCode.EditValueChanged += new System.EventHandler(this.LueWSRCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(46, 74);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "Work Schedule Rule";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmWTG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmWTG";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTGName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWTGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWSRCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.TextEdit TxtWTGName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtWTGCode;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnWSRCode;
        private DevExpress.XtraEditors.LookUpEdit LueWSRCode;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnWTCode;
        private DevExpress.XtraEditors.LookUpEdit LueWTCode;
        private System.Windows.Forms.Label label4;
    }
}