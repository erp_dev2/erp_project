﻿#region Update
/*
  08/11/2021 [MYA/PHT] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCompanyProcurementPlanDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptCompanyProcurementPlan mFrmParent; 
        internal string
             ItCode = string.Empty,
             ProcurementType = string.Empty,
             SiteCode = string.Empty,
             Yr = string.Empty,
             Mth = string.Empty;

        #endregion

        #region Constructor

        public FrmRptCompanyProcurementPlanDlg(FrmRptCompanyProcurementPlan FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnChoose.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[]
                    {
                        //0
                        "Material Request#",
                        
                        //1-5
                        "",
                        "Item#",
                        "Item Name",
                        "Qty",
                        "Estimate Price",

                        //6
                        "Total Estimate Price",
                    },
                   new int[]
                    {
                        //0
                        150,
 
                        //1-5
                        20, 120, 200, 80, 150,

                        //6
                        150
                    }
              );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, B.ItCode, C.ItName, B.Qty, B.EstPrice, (B.Qty * B.EstPrice) AS TotalEstimatePrice ");
            SQL.AppendLine("FROM tblmaterialrequesthdr A ");
            SQL.AppendLine("INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("AND A.Status = 'A' ");
            SQL.AppendLine("AND B.CancelInd = 'N' ");
            SQL.AppendLine("AND LEFT(A.DocDt,4) = @Yr ");
            if (Mth.Length > 0)
                SQL.AppendLine("AND MID(A.DocDt,5,2) = @Mth ");
            if (SiteCode.Length > 0)
                SQL.AppendLine("AND A.SiteCode = @SiteCode ");
            SQL.AppendLine("INNER JOIN tblitem C ON B.ItCode = C.ItCode  ");
            SQL.AppendLine("WHERE B.ItCode = @ItCode AND A.ProcurementType = @ProcurementType ");
            SQL.AppendLine("GROUP BY A.DocNo");

            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {

        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = string.Empty;
                Sm.CmParam<string>(ref cm, "@ItCode", ItCode);
                Sm.CmParam<string>(ref cm, "@ProcurementType", ProcurementType);
                Sm.CmParam<string>(ref cm, "@SiteCode", SiteCode);
                Sm.CmParam<string>(ref cm, "@Yr", Yr);
                Sm.CmParam<string>(ref cm, "@Mth", Mth);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL() + Filter,
                        new string[]
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "ItCode",
                            "ItName",
                            "Qty",
                            "EstPrice",
                            "TotalEstimatePrice"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            //Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            //if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            //{
            //    var f = new FrmRptAgingAPPaidItemDlg(this, e.RowIndex);
            //    f.Tag = mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.ShowDialog();
            //}

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0)
            {
                var f1 = new FrmMaterialRequest("***");
                f1.Tag = "***";
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
            }
        }




        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0)
            {
                var f1 = new FrmMaterialRequest("***");
                f1.Tag = "***";
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f1.ShowDialog();
                
            }

        }

        #endregion

        #endregion
    }
}
