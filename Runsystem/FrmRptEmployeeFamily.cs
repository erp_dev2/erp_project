﻿#region Update
/*
    12/04/2023 [IBL/PHT] New reporting
    13/04/2023 [IBL/PHT] Data difilter by group
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmployeeFamily : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEmployeeFamily(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode, "");
                Sl.SetLueSiteCode(ref LueSiteCode, "");
                Sl.SetLueOption(ref LueMaritalStatus, "MaritalStatus");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Employee's Name",
                    "Marital Status",
                    "Employee's"+Environment.NewLine+"Code",
                    "Old Code",
                    "Department",
                        
                    //6-10
                    "Site",
                    "Family Name",
                    "Family Emp."+Environment.NewLine+"Code",
                    "Status",
                    "Gender",

                    //11-15
                    "Birth Date",
                    "National"+Environment.NewLine+"Identity#",
                    "Profession",
                    "Latest"+Environment.NewLine+"Education",
                    "Remark"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    250, 100, 100, 150, 200, 
                        
                    //6-10
                    200, 200, 150, 100, 100,

                    //11-15
                    80, 150, 150, 130, 250
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                List<Employee> l = new List<Employee>();
                Sm.ClearGrd(Grd1, false);

                Process1(ref l);
                if (l.Count > 0)
                {
                    if(Sm.GetLue(LueMaritalStatus) == "5")
                        Process2(ref l);
                    Process3(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
            
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void Process1(ref List<Employee> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = " ";

            SQL.AppendLine("Select T1.DocType, T2.EmpName, T3.OptDesc As MaritalStatusDesc, T1.EmpCode, T2.EmpCodeOld, T4.DeptName, T5.SiteName, T6.FamilyName, ");
            SQL.AppendLine("Case T2.MaritalStatus ");
            SQL.AppendLine("    When '5' Then ");
            SQL.AppendLine("        Case T7.OptCode ");
            SQL.AppendLine("        When '1' Then T2.SpouseEmpCode ");
            SQL.AppendLine("        When '2' Then T2.SpouseEmpCode ");
            SQL.AppendLine("        Else Null End ");
            SQL.AppendLine("    Else Null ");
            SQL.AppendLine("End As SpouseEmpCode, ");
            SQL.AppendLine("T7.OptDesc As FamilyStatus, T8.OptDesc As GenderDesc, T6.BirthDt, T6.NIN, ");
            SQL.AppendLine("T9.OptDesc As ProfessionDesc, T10.OptDesc As LatestEducation, T6.Remark, T2.MaritalStatus ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And A.MaritalStatus Is Not Null ");
            SQL.AppendLine("    And A.EmpCode In (Select Distinct EmpCode From TblEmployeeFamily) ");
            SQL.AppendLine("    Union ");
            SQL.AppendLine("    Select '2' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And (A.MaritalStatus Is Not Null And A.EmpCode Not In (Select Distinct EmpCode From TblEmployeeFamily)) ");
            SQL.AppendLine("    Union ");
            SQL.AppendLine("    Select '3' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And (A.MaritalStatus Is Null And A.EmpCode In (Select Distinct EmpCode From TblEmployeeFamily)) ");
            SQL.AppendLine("    Union ");
            SQL.AppendLine("    Select '4' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And (A.MaritalStatus Is Null And A.EmpCode Not In (Select Distinct EmpCode From TblEmployeeFamily)) ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
            SQL.AppendLine("Left Join TblOption T3 On T3.OptCat = 'MaritalStatus' And T2.MaritalStatus = T3.OptCode ");
            SQL.AppendLine("Inner Join TblDepartment T4 On T2.DeptCode = T4.DeptCode ");     
            SQL.AppendLine("Inner Join TblSite T5 On T2.SiteCode = T5.SiteCode ");
            SQL.AppendLine("Left Join TblEmployeeFamily T6 On T2.EmpCode = T6.EmpCode ");
            SQL.AppendLine("Left Join TblOption T7 On T6.Status=T7.OptCode and T7.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption T8 On T6.Gender=T8.OptCode and T8.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblOption T9 On T6.ProfessionCode=T9.OptCode and T9.OptCat='Profession' ");
            SQL.AppendLine("Left Join TblOption T10 On T6.EducationLevelCode=T10.OptCode and T10.OptCat='EmployeeEducationLevel' ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupDepartment ");
            SQL.AppendLine("    Where DeptCode=T4.DeptCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupSite ");
            SQL.AppendLine("    Where SiteCode=T5.SiteCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine(") ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T2.EmpCode", "T2.EmpCodeOld", "T2.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T2.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T2.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMaritalStatus), "T2.MaritalStatus", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                
                cm.CommandText = SQL.ToString() + Filter;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "DocType",

                    //1-5
                    "EmpName", "MaritalStatusDesc", "EmpCode", "EmpCodeOld", "DeptName",

                    //6-10
                    "SiteName", "FamilyName", "SpouseEmpCode", "FamilyStatus", "GenderDesc",

                    //11-15
                    "BirthDt", "NIN", "ProfessionDesc", "LatestEducation", "Remark",

                    //16
                    "MaritalStatus"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            MaritalStatusDesc = Sm.DrStr(dr, c[2]),
                            EmpCode = Sm.DrStr(dr, c[3]),
                            EmpCodeOld = Sm.DrStr(dr, c[4]),
                            DeptName = Sm.DrStr(dr, c[5]),
                            SiteName = Sm.DrStr(dr, c[6]),
                            FamilyName = Sm.DrStr(dr, c[7]),
                            SpouseEmpCode = Sm.DrStr(dr, c[8]),
                            FamilyStatus = Sm.DrStr(dr, c[9]),
                            GenderDesc = Sm.DrStr(dr, c[10]),
                            BirthDt = Sm.DrStr(dr, c[11]),
                            NIN = Sm.DrStr(dr, c[12]),
                            ProfessionDesc = Sm.DrStr(dr, c[13]),
                            LatestEducation = Sm.DrStr(dr, c[14]),
                            Remark = Sm.DrStr(dr, c[15]),
                            MaritalStatus = Sm.DrStr(dr, c[16])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Employee> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string EmpCode = string.Empty, SpouseEmpCode = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (EmpCode.Length > 0) EmpCode += ",";
                EmpCode += "'" + l[i].EmpCode + "'";

                if (SpouseEmpCode.Length > 0) SpouseEmpCode += ",";
                SpouseEmpCode += "'" + l[i].EmpCode + "'";
            }

            SQL.AppendLine("Select T1.DocType, T2.EmpName, T3.OptDesc As MaritalStatusDesc, T1.EmpCode, T2.EmpCodeOld, T4.DeptName, T5.SiteName, T6.FamilyName, ");
            SQL.AppendLine("Case T2.MaritalStatus ");
            SQL.AppendLine("    When '5' Then ");
            SQL.AppendLine("        Case T7.OptCode ");
            SQL.AppendLine("        When '1' Then T2.SpouseEmpCode ");
            SQL.AppendLine("        When '2' Then T2.SpouseEmpCode ");
            SQL.AppendLine("        Else Null End ");
            SQL.AppendLine("    Else Null ");
            SQL.AppendLine("End As SpouseEmpCode, ");
            SQL.AppendLine("T7.OptDesc As FamilyStatus, T8.OptDesc As GenderDesc, T6.BirthDt, T6.NIN, ");
            SQL.AppendLine("T6.NIN, T9.OptDesc As ProfessionDesc, T10.OptDesc As LatestEducation, T6.Remark, T2.MaritalStatus ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1-2' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And A.MaritalStatus Is Not Null ");
            SQL.AppendLine("    And A.EmpCode In (Select Distinct EmpCode From TblEmployeeFamily) ");
            SQL.AppendLine("    Union ");
            SQL.AppendLine("    Select '2-2' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And (A.MaritalStatus Is Not Null And A.EmpCode Not In (Select Distinct EmpCode From TblEmployeeFamily)) ");
            SQL.AppendLine("    Union ");
            SQL.AppendLine("    Select '3-2' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And (A.MaritalStatus Is Null And A.EmpCode In (Select Distinct EmpCode From TblEmployeeFamily)) ");
            SQL.AppendLine("    Union ");
            SQL.AppendLine("    Select '4-2' As DocType, A.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > Left(CurrentDateTime(), 8))) ");
            SQL.AppendLine("    And (A.MaritalStatus Is Null And A.EmpCode Not In (Select Distinct EmpCode From TblEmployeeFamily)) ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
            SQL.AppendLine("Left Join TblOption T3 On T3.OptCat = 'MaritalStatus' And T2.MaritalStatus = T3.OptCode ");
            SQL.AppendLine("Inner Join TblDepartment T4 On T2.DeptCode = T4.DeptCode ");
            SQL.AppendLine("Inner Join TblSite T5 On T2.SiteCode = T5.SiteCode ");
            SQL.AppendLine("Left Join TblEmployeeFamily T6 On T2.EmpCode = T6.EmpCode ");
            SQL.AppendLine("Left Join TblOption T7 On T6.Status=T7.OptCode and T7.OptCat='FamilyStatus' ");
            SQL.AppendLine("Left Join TblOption T8 On T6.Gender=T8.OptCode and T8.OptCat='Gender' ");
            SQL.AppendLine("Left Join TblOption T9 On T6.ProfessionCode=T9.OptCode and T9.OptCat='Profession' ");
            SQL.AppendLine("Left Join TblOption T10 On T6.EducationLevelCode=T10.OptCode and T10.OptCat='EmployeeEducationLevel' ");
            SQL.AppendLine("Where T2.SpouseEmpCode In (" + SpouseEmpCode + ") ");
            SQL.AppendLine("And T2.EmpCode Not In (" + EmpCode + "); ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "DocType",

                    //1-5
                    "EmpName", "MaritalStatusDesc", "EmpCode", "EmpCodeOld", "DeptName",

                    //6-10
                    "SiteName", "FamilyName", "SpouseEmpCode", "FamilyStatus", "GenderDesc",

                    //11-15
                    "BirthDt", "NIN", "ProfessionDesc", "LatestEducation", "Remark",

                    //16
                    "MaritalStatus"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            DocType = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            MaritalStatusDesc = Sm.DrStr(dr, c[2]),
                            EmpCode = Sm.DrStr(dr, c[3]),
                            EmpCodeOld = Sm.DrStr(dr, c[4]),
                            DeptName = Sm.DrStr(dr, c[5]),
                            SiteName = Sm.DrStr(dr, c[6]),
                            FamilyName = Sm.DrStr(dr, c[7]),
                            SpouseEmpCode = Sm.DrStr(dr, c[8]),
                            FamilyStatus = Sm.DrStr(dr, c[9]),
                            GenderDesc = Sm.DrStr(dr, c[10]),
                            BirthDt = Sm.DrStr(dr, c[11]),
                            NIN = Sm.DrStr(dr, c[12]),
                            ProfessionDesc = Sm.DrStr(dr, c[13]),
                            LatestEducation = Sm.DrStr(dr, c[14]),
                            Remark = Sm.DrStr(dr, c[15]),
                            MaritalStatus = Sm.DrStr(dr, c[16])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Employee> l)
        {
            int Row = 0;
            DateTime BirthDt = new DateTime();
            foreach (var x in l.OrderBy(o => o.DocType).ThenBy(o => o.MaritalStatus).ThenBy(o => o.EmpCode).ThenBy(o => o.SpouseEmpCode))
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.EmpName;
                Grd1.Cells[Row, 2].Value = x.MaritalStatusDesc;
                Grd1.Cells[Row, 3].Value = x.EmpCode;
                Grd1.Cells[Row, 4].Value = x.EmpCodeOld;
                Grd1.Cells[Row, 5].Value = x.DeptName;
                Grd1.Cells[Row, 6].Value = x.SiteName;
                Grd1.Cells[Row, 7].Value = x.FamilyName;
                Grd1.Cells[Row, 8].Value = x.SpouseEmpCode;
                Grd1.Cells[Row, 9].Value = x.FamilyStatus;
                Grd1.Cells[Row, 10].Value = x.GenderDesc;
                if (x.BirthDt.Length > 0) Grd1.Cells[Row, 11].Value = Sm.ConvertDate(x.BirthDt);
                Grd1.Cells[Row, 12].Value = x.NIN;
                Grd1.Cells[Row, 13].Value = x.ProfessionDesc;
                Grd1.Cells[Row, 14].Value = x.LatestEducation;
                Grd1.Cells[Row, 15].Value = x.Remark;
                Row++;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), "");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), "");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueMaritalStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMaritalStatus, new Sm.RefreshLue2(Sl.SetLueOption), "MaritalStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkMaritalStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Marital Status");
        }
        #endregion

        #endregion

        #region Class

        private class Employee
        {
            public string DocType { get; set; }
            public string EmpName { get; set; }
            public string MaritalStatusDesc { get; set; }
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public string FamilyName { get; set; }
            public string SpouseEmpCode { get; set; }
            public string FamilyStatus { get; set; }
            public string GenderDesc { get; set; }
            public string BirthDt { get; set; }
            public string NIN { get; set; }
            public string ProfessionDesc { get; set; }
            public string LatestEducation { get; set; }
            public string Remark { get; set; }
            public string MaritalStatus { get; set; }
        }

        #endregion
    }
}
