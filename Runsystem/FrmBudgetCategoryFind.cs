﻿#region Update
/*
    04/05/2020 [VIN/VIR] tambah kolom local code 
    03/04/2021 [VIN/ALL] tambah export to excel 
    18/05/2021 [TKG/PHT] tambah filter cost center, tambah otorisasi berdasarkan profit center
    20/05/2021 [TKG/PHT] tambah coa account
    09/08/2022 [HPH/SIER] tambah kolom cost category
    01/09/2022 [VIN/SIER] BUG show Data kolom
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetCategoryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmBudgetCategory mFrmParent;

        #endregion

        #region Constructor

        public FrmBudgetCategoryFind(FrmBudgetCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Local Code", 
                        "Name",
                        "Active",
                        "Department",
                        
                        //6-10 
                        "Cost center",
                        "Budget Type",
                        "Cost Category",
                        "Account#",
                        "Account Description",
                        

                        //11-15
                        "Created By",
                        "Created Date",
                        "Created Time", 
                        "Last Updated By",
                        "Last Updated Date", 
                        
                        //16
                        "Last Updated Time"
                      },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 100, 200, 80, 200,   
                        
                        //6-10
                        200, 200, 150, 150, 200, 
                        
                        //11-15
                        130, 130, 130, 130, 130,

                        //16
                        130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 11, 12, 13, 14, 15, 16 }, false);
            if(mFrmParent.mIsBudgetCategoryUseCOA)
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10 }, true);
            if(mFrmParent.mIsBudgetCategoryUseCostCategory)
                Sm.GrdColInvisible(Grd1, new int[] { 8 }, true);
            if(mFrmParent.mIsBudgetCategoryUseCostCenter)
                Sm.GrdColInvisible(Grd1, new int[] { 6 }, true);


            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (mFrmParent.mIsBudgetCategoryUseProfitCenter && TxtCCCode.Text.Length==0)
            {
                if (StdMtd.StdMsgYN("Question", "Do you still want to show data without filtering cost center ?") == DialogResult.No)
                    return;
            }

            Cursor.Current = Cursors.WaitCursor;

            string Filter = " ";
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.FilterStr(ref Filter, ref cm, TxtBCName.Text, new string[] { "A.BCCode", "A.BCName" });
            Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "D.CCCode", "D.CCName" });

            SQL.AppendLine("Select A.BCCode, A.LocalCode, A.BCName, A.ActInd, B.DeptName, D.CCName, C.OptDesc As BudgetType, F.CCtName, ");
            SQL.AppendLine("E.AcNo, E.AcDesc, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblBudgetCategory A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblOption C On A.BudgetType=C.OptCode And C.OptCat='BudgetType' ");
            if (TxtCCCode.Text.Length>0)
                SQL.AppendLine("Inner ");
            else
                SQL.AppendLine("Left ");
            SQL.AppendLine("Join TblCostCenter D On A.CCCode=D.CCCode ");
            if (mFrmParent.mIsBudgetCategoryUseProfitCenter)
                SQL.AppendLine("And D.NotParentInd='Y' ");
            SQL.AppendLine("Left Join TblCOA E On A.AcNo=E.AcNo ");
            SQL.AppendLine("LEFT JOIN tblcostcategory F ON A.CCtCode = F.CCtCode");
            SQL.AppendLine("Where 1=1 ");
            if (mFrmParent.mIsBudgetCategoryUseProfitCenter)
            {
                SQL.AppendLine("And (A.CCCode Is Null Or (A.CCCode Is Not Null And A.CCCode In ( ");
                SQL.AppendLine("    Select CCCode ");
                SQL.AppendLine("    From TblCostCenter ");
                SQL.AppendLine("    Where ActInd='Y' ");
                SQL.AppendLine("    And ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    And ProfitCenterCode In (");
                SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    )))) ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine(" Order By B.DeptName, A.BCName;");

            try
            {
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "BCCode",

                            //1-5
                            "LocalCode",
                            "BCName",
                            "ActInd",
                            "DeptName",
                            "CCName",
                            
                            //6-10
                            "BudgetType",
                            "CCtName",
                            "AcNo",
                            "AcDesc",
                            "CreateBy",
                             

                            //11-13
                            "CreateDt",
                            "LastUpBy", 
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Additional method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 2].Value = "'" + Sm.GetGrdStr(Grd1, Row, 2);
                Grd1.Cells[Row, 3].Value = "'" + Sm.GetGrdStr(Grd1, Row, 3);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 2].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 2).Length - 1);
                Grd1.Cells[Row, 3].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 3), Sm.GetGrdStr(Grd1, Row, 3).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkBCName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget category");   
        }

        private void TxtBCName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost center");
        }

        #endregion

        #endregion
    }
}
