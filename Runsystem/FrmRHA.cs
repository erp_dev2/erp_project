﻿#region Update
/*
    11/05/2017 [TKG] tambah informasi holiday date
    26/05/2017 [TKG] rumus pajak thr kmi
    12/06/2017 [TKG] tambah filter payrun period, otomatis tick karyawan saat memilih employee, dan tambah grade level.
    13/06/2017 [ARI] tambah payslip THR
    13/06/2017 [WED] tambah fitur export ke CSV
    14/06/2017 [TKG] proses perhitungan untuk IOK
    15/06/2017 [TKG] perubahan proses perhitungan untuk IOK berdasarkan traing graduation date
    15/06/2017 [ARI] perubahan tampilan slip THR (IOK)
    16/06/2017 [TKG] utk IOK, perhitungan tax tanpa fixed allowance, tambah filter payrun period di find.
    16/06/2017 [WED] CSV notepad dan notepad++ dua desimal
    11/07/2017 [TKG] tambah journal. 
    31/07/2017 [TKG] Tambah payrun
    05/07/2018 [TKG] department tidak mandatory.
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    12/03/2019 [TKG] ubah proses cancel journal
    04/04/2023 [WED/IOK] CSV untuk bank Mandiri
    08/04/2023 [WED/IOK] feedback : bank Mandiri di hardcode di CSV pilihan mandiri, bank address ambil dari bank branch
    11/04/2023 [WED/IOK] email di hilangkan, kode bank diberi tanda petik satu didepan
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmRHA : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty, 
            mReportTitle1 = string.Empty,
            mBankAcNo = string.Empty;
        internal bool 
            mIsNotFilterByAuthorization = false,
            mIsFilterBySite = false,
            mIsRHAUsePayrunCode = false
            //mIsRHAUseFunctionalExpenses = false
            ;
        internal string 
            mSalaryInd = string.Empty,
            mMainCurCode = string.Empty;

        private string mCompanyCodeForMandiriPayroll = string.Empty;
        //decimal mFunctionalExpenses = 0m;

        private bool mIsEntityMandatory = false, mIsAutoJournalActived = false;
        internal FrmRHAFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmRHA(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Religius Holiday Allowance";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnCSV.Visible = BtnPrint.Visible;
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueCurCode(ref LueCurCode);
                if (mIsRHAUsePayrunCode)
                    LblPayrunCode.ForeColor = Color.Red;
                //else
                //    LblDeptCode.ForeColor = Color.Red;
                
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "", 

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Grade Level",
                        
                        //6-10
                        "Amount",
                        "Tax",
                        "Total",
                        "Payrun"+Environment.NewLine+"Code",
                        "Bank Account",

                        //11-15
                        "Payrun Period",
                        "Department",
                        "EmpBankCode",
                        "EmpBankName",
                        "EmpBankAcName",

                        //16-20
                        "EmpBankBranch",
                        "EmpEmail",
                        "EmpBankSwiftBeneCode",
                        "EmpBankKliringCode",
                        "EmpBankRTGSCode"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        80, 200, 100, 150, 150,  
                        
                        //6-10
                        0, 0, 130, 0, 0,

                        //11-15
                        150, 180, 0, 0, 0,

                        //16-20
                        0, 0, 0, 0, 0
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20 }, false);
            Grd1.Cols[11].Move(5);
            Grd1.Cols[12].Move(6);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    { 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnCSV.Enabled = BtnPrint.Enabled;
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, DteHolidayDt, LueDeptCode, 
                        LueSiteCode, LuePaymentType, LueBankAcCode, MeeRemark 
                    }, true);
                    BtnPayrunCode.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, DteHolidayDt, LuePaymentType, LueBankAcCode, MeeRemark }, false);
                    if (mIsRHAUsePayrunCode)
                        BtnPayrunCode.Enabled = true;
                    else
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueDeptCode, LueSiteCode }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, DteHolidayDt, 
                LueDeptCode, LueSiteCode, LuePaymentType, LueBankAcCode, 
                TxtPayrunCode, TxtPayrunName, LueCurCode, TxtVoucherRequestDocNo, MeeRemark, 
                TxtJournalDocNo, TxtJournalDocNo2 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            mReportTitle1 = string.Empty;
            mBankAcNo = string.Empty;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 8 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        private void ClearGrd1()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRHAFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RHA", "TblRHAHdr");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRHAHdr(DocNo, VoucherRequestDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveRHADtl(DocNo, Row));
            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteHolidayDt, "Holiday date") ||
                //Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsTxtEmpty(TxtAmt, "Total amount", true) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                (mIsRHAUsePayrunCode && Sm.IsTxtEmpty(TxtPayrunCode, "Payrun", false)) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, r, 8, true, "Total should be bigger than 0.") 
                ) return true;
            }
            return false;
        }

        private string GenerateVoucherRequestDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty;

            type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            
            var SQL = new StringBuilder();

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveRHAHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRHAHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, HolidayDt, DeptCode, SiteCode, ");
            SQL.AppendLine("PaymentType, BankAcCode, PayrunCode, CurCode, Amt, VoucherRequestDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'O', @HolidayDt, @DeptCode, @SiteCode, ");
            SQL.AppendLine("@PaymentType, @BankAcCode, @PayrunCode, @CurCode, @Amt, @VoucherRequestDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRHADtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRHADtl(DocNo, EmpCode, Value, Tax, Amt, PayrunCode, CreateBy, CreateDt) " +
                    "Values (@DocNo, @EmpCode, @Value, @Tax, @Amt, @PayrunCode, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequest(string VoucherRequestDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='VoucherRequestPayrollDeptCode'), ");
            SQL.AppendLine("'13', 'C', @PaymentType, @BankAcCode, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy), ");
            SQL.AppendLine("0, @CurCode, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl ");
            SQL.AppendLine("(DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, '001', @Remark, @Amt, @CreateBy, CurrentDateTime());");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @RHADocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='RHA'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RHA' ");
            SQL.AppendLine("    And DocNo=@RHADocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblRHAHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@RHADocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RHA' ");
            SQL.AppendLine("    And DocNo=@RHADocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@RHADocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Update TblRHAHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo And Status='A';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Religious Holiday Allowance : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblRHAHdr Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, B.DNo, B.AcNo, ");
            SQL.AppendLine("B.DAmt, B.CAMt, ");
            SQL.AppendLine("E.EntCode, C.CreateBy, C.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.AcNo, '001' As DNo, ");
            SQL.AppendLine("    Case When T.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=@DocDt And CurCode1=T.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) ");
            SQL.AppendLine("    End*T.Amt As DAmt, ");
            SQL.AppendLine("    0.00 As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select C.ParValue As AcNo, A.CurCode, ");
            SQL.AppendLine("        Sum(B.Amt+B.Tax) As Amt ");
            SQL.AppendLine("        From TblRHAHdr A ");
            SQL.AppendLine("        Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='AcNoForRHA' And C.ParValue is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Group By C.ParValue, A.CurCode ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T.AcNo, '002' As DNo, ");
            SQL.AppendLine("    0.00 As DAmt, ");
            SQL.AppendLine("    Case When T.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=@DocDt And CurCode1=T.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) ");
            SQL.AppendLine("    End*T.Amt As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select C.ParValue As AcNo, A.CurCode, ");
            SQL.AppendLine("        Sum(B.Amt) As Amt ");
            SQL.AppendLine("        From TblRHAHdr A ");
            SQL.AppendLine("        Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='AcNoForRHAPayable' And C.ParValue is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Group By C.ParValue, A.CurCode ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T.AcNo, '003' As DNo, ");
            SQL.AppendLine("    0.00 As DAmt, ");
            SQL.AppendLine("    Case When T.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=@DocDt And CurCode1=T.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) ");
            SQL.AppendLine("    End*T.Amt As CAmt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select C.ParValue As AcNo, A.CurCode, ");
            SQL.AppendLine("        Sum(B.Tax) As Amt ");
            SQL.AppendLine("        From TblRHAHdr A ");
            SQL.AppendLine("        Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='AcNoForTaxLiability' And C.ParValue is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Group By C.ParValue, A.CurCode ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Inner Join TblRHAHdr C On C.DocNo=@DocNo And C.JournalDocNo Is Not Null ");
            SQL.AppendLine("Left Join TblSite D On C.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter E On D.ProfitCenterCode=E.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelRLPHdr());
            if (mIsAutoJournalActived) cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelIndUntick() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessed();
        }

        private bool IsCancelIndUntick()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblRHAHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsDataAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "This document already processed to voucher."
                );
        }

        private MySqlCommand CancelRLPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRHAHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblRHAHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblRHAHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblRHAHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRHAHdr(DocNo);
                ShowRHADtl(DocNo);
                ShowDocApproval(DocNo);
                mBankAcNo = Sm.GetValue("Select IfNull(B.BankAcNo, '') As BankAcNo From TblRHAHdr A Left Join TblBankAccount B On A.BankAcCode = B.BankAcCode Where A.DocNo = '" + DocNo + "'; ");
                mReportTitle1 = Sm.GetParameter("ReportTitle1");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRHAHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.HolidayDt, A.DeptCode, A.SiteCode, A.PaymentType, A.BankAcCode, ");
            SQL.AppendLine("A.PayrunCode, B.PayrunName, A.CurCode, A.Amt, A.VoucherRequestDocNo, ");
            SQL.AppendLine("A.Remark, A.JournalDocNo, A.JournalDocNo2  ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Left Join TblPayrun B On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "HolidayDt", 
                    
                    //6-10
                    "DeptCode", "SiteCode", "PaymentType", "BankAcCode", "PayrunCode", 
                    
                    //11-15
                    "PayrunName", "CurCode", "Amt", "VoucherRequestDocNo", "Remark", 
                    
                    //16
                    "JournalDocNo", "JournalDocNo2"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                     TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                     Sm.SetDte(DteHolidayDt, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[6]));
                     Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[7]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[8]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[9]));
                     TxtPayrunCode.EditValue = Sm.DrStr(dr, c[10]);
                     TxtPayrunName.EditValue = Sm.DrStr(dr, c[11]);
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[12]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[14]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                     TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[16]);
                     TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[17]);
                 }, true
             );
        }

        private void ShowRHADtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.GrdLvlName, ");
            SQL.AppendLine("A.Value, A.Tax, A.Amt, A.PayrunCode, IfNull(B.BankAcNo, '') As BankAcNo, F.DeptName, E.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("B.BankCode, B.BankName, B.BankAcName, B.BankBranch, B.Email, G.SwiftBeneCode, G.KliringCode, G.RTGSCode ");
            SQL.AppendLine("From TblRHADtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And B.GrdLvlCode In ( ");
                SQL.AppendLine("    Select T2.GrdLvlCode ");
                SQL.AppendLine("    From TblPPAHdr T1 ");
                SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On B.GrdLvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption E On B.PayrunPeriod=E.OptCode And E.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblDepartment F On B.DeptCode=F.DeptCode ");
            SQL.AppendLine("Left Join TblBank G On B.BankCode = G.BankCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.EmpName;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 
                        //1-5
                        "EmpName", "EmpCodeOld", "PosName", "GrdLvlName", "Value", 
                        //6-10
                        "Tax", "Amt", "PayrunCode", "BankAcNo", "PayrunPeriodDesc",
                        //11-15
                        "DeptName", "BankCode", "BankName", "BankAcName", "BankBranch", 
                        //16-19
                        "Email", "SwiftBeneCode", "KliringCode", "RTGSCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UserName, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='RHA' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status In ('A', 'C') ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            if (Grd2.Rows.Count>0) 
                Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private bool MandiriValidation()
        {
            if (mCompanyCodeForMandiriPayroll.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Company Code (Mandiri) is empty.");
                return false;
            }

            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 10).Length == 0 || // bank account number
                        Sm.GetGrdStr(Grd1, Row, 15).Length == 0 || // bank account name
                        Sm.GetGrdStr(Grd1, Row, 19).Length == 0 || // bank kliring code
                        Sm.GetGrdStr(Grd1, Row, 20).Length == 0)   // bank rtgs code
                    {
                        Sm.StdMsg(mMsgType.Warning, "Employee " + Sm.GetGrdStr(Grd1, Row, 2) + " should have mandatory Mandiri information (Bank Account Number, Bank Account Name, Kliring Code, and RTGS Code).");
                        Sm.FocusGrd(Grd1, Row, 2);
                        return false;
                    }
                }
            }

            return true;
        }

        private void GetParameter()
        {
            //mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            //mIsRHAUseFunctionalExpenses = Sm.GetParameter("IsRHAUseFunctionalExpenses") == "Y";
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
            mIsEntityMandatory = Sm.GetParameter("IsEntityMandatory") == "Y";
            mSalaryInd = Sm.GetParameter("SalaryInd");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived")=="Y";
            mIsRHAUsePayrunCode = Sm.GetParameter("IsRHAUsePayrunCode") == "Y";
            mCompanyCodeForMandiriPayroll = Sm.GetParameter("CompanyCodeForMandiriPayroll");
        }

        internal void ComputeAmt()
        {
            var Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Amt += Sm.GetGrdDec(Grd1, r, 8);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || 
                Sm.StdMsgYN("Print", "") == DialogResult.No ||
                IsPrintedDocInvalid()
                ) return;

            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<RHA>();

            string[] TableName = { "RHA" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d-%M-%y')As DocDt, B.EmpCode, C.EmpName, C.EmpCodeOld, D.DeptName, ");
            SQL.AppendLine("B.Amt, B.Tax, (B.Amt+B.Tax)Total ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",

                         //6-10
                         "DocNo",
                         "DocDt",
                         "EmpCode",
                         "EmpName",
                         "EmpCodeOld",

                         //11-14
                         "DeptName",
                         "Amt",
                         "Tax",
                         "Total"
                         
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RHA()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),
                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            EmpCode = Sm.DrStr(dr, c[8]),
                            EmpName = Sm.DrStr(dr, c[9]),
                            EmpCodeOld = Sm.DrStr(dr, c[10]),
                            DeptName = Sm.DrStr(dr, c[11]),
                            Amt = Sm.DrDec(dr, c[12]),
                            Tax = Sm.DrDec(dr, c[13]),
                            Total = Sm.DrDec(dr, c[14]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            Sm.PrintReport("PaySlipRHA", myLists, TableName, false);
        }

        private void CreateCSVFile(ref List<CSVData> lCSVData, string FileName, decimal TotalHeader, string PeriodHeader, string Dt)
        {
            #region Old Code
            //StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));

            //sw.Write("EMPLOYEE CODE,OLD CODE,EMPLOYEE NAME,DEPARTMENT,AMOUNT");

            //foreach (var x in lCSVData)
            //{
            //    sw.Write(sw.NewLine);
            //    sw.Write(
            //        x.EmpCode + "," + x.EmpCodeOld + "," +
            //        x.EmpName + "," + x.DeptName + "," + 
            //        x.Total
            //        //"," + x.EmpCode + ",+," +
            //        //(x.Total.ToString()).Replace(",", ".") + ",C," +
            //        //x.NumberOfRow + "," + x.EmpName + ",,"
            //    );
            //}
            //sw.Close();
            //System.Diagnostics.Process.Start(FileName);
            //lCSVData.Clear();
            #endregion

            StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));
            sw.Write(
               "=" + "\"\"" + "&" + ((mBankAcNo.Replace(" ", string.Empty)) + " ").Trim() + "," + mReportTitle1 + ",IDR," +
               (TotalHeader.ToString()).Replace(",", ".") +
                //Total.ToString() + 
               "," +
                PeriodHeader + "," + lCSVData.Count + "," + Dt
                );

            foreach (var x in lCSVData)
            {
                sw.Write(sw.NewLine);
                sw.Write(
                    x.BankAcNo + "," + x.EmpName + "," + x.CurCode + "," +
                    //x.Total.ToString() +
                    (x.Total.ToString()).Replace(",", ".") +
                    "," +
                    PeriodHeader + ",," + Dt
                );

            }
            sw.Close();
            System.Diagnostics.Process.Start(FileName);
            lCSVData.Clear();
        }

        private bool IsPrintedDocInvalid()
        {
            if (!Sm.IsDataExist("Select DocNo From TblRHAHdr Where CancelInd='N' And Status='A' And DocNo=@Param;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Info, "You can't print or export to csv file.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteHolidayDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd1();
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
                ClearGrd1();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
                ClearGrd1();
            }
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherPaymentType");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (mIsRHAUsePayrunCode)
                    {
                        if (
                            e.ColIndex == 0 && 
                            !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") &&
                            !Sm.IsTxtEmpty(TxtPayrunCode, "Payrun", false)
                            )
                        {
                            e.DoDefault = false;
                            if (e.KeyChar == Char.Parse(" "))
                                Sm.FormShowDialog(new FrmRHADlg(this, mSalaryInd, Sm.GetDte(DteHolidayDt), Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode), TxtPayrunCode.Text));
                        }
                    }
                    else
                    {
                        if (e.ColIndex == 0 && 
                            !Sm.IsDteEmpty(DteHolidayDt, "Holiday date"))
                        {
                            e.DoDefault = false;
                            if (e.KeyChar == Char.Parse(" "))
                                Sm.FormShowDialog(new FrmRHADlg(this, mSalaryInd, Sm.GetDte(DteHolidayDt), Sm.GetLue(LueDeptCode), Sm.GetLue(LueSiteCode), string.Empty));
                        }
                    }
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (mIsRHAUsePayrunCode)
            {
                if (
                    e.ColIndex == 0 &&
                    !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") &&
                    !Sm.IsTxtEmpty(TxtPayrunCode, "Payrun", false)
                    )
                    Sm.FormShowDialog(
                        new FrmRHADlg(
                            this, 
                            mSalaryInd, 
                            Sm.GetDte(DteHolidayDt), 
                            Sm.GetLue(LueDeptCode), 
                            Sm.GetLue(LueSiteCode),
                            TxtPayrunCode.Text
                            ));
            }
            else
            {
                if (
                    e.ColIndex == 0 &&
                    !Sm.IsDteEmpty(DteHolidayDt, "Holiday date")
                    )
                    Sm.FormShowDialog(new FrmRHADlg(
                        this, 
                        mSalaryInd, 
                        Sm.GetDte(DteHolidayDt), 
                        Sm.GetLue(LueDeptCode), 
                        Sm.GetLue(LueSiteCode), 
                        string.Empty));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Button Event

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRHADlg2(this));
        }

        private void BtnCSV_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || IsPrintedDocInvalid()) return;

            var lCSVData = new List<CSVData>();
            var lHMandiri = new List<FrmVoucherRequestPayroll.HMandiri>();
            var lDMandiri = new List<FrmVoucherRequestPayroll.DMandiri>();
            string BankCode = Sm.GetValue("Select BankCode From TblBankAccount Where BankAcCode = @Param ", Sm.GetLue(LueBankAcCode));
            string mCreditTo = Sm.GetValue("Select BankAcNo From TblBankAccount Where BankAcCode = @Param ", Sm.GetLue(LueBankAcCode));

            if (BankCode != "022" && BankCode != "008")
            {
                Sm.StdMsg(mMsgType.Warning, "CSV format unavailable.");
                return;
            }

            int RowCount = 0;
            decimal TotalHeader = 0m;
            string PeriodHeader = DteDocDt.Text;
            decimal Amt = 0m;
            var FileName = string.Empty;
            string Dt = Sm.GetDte(DteDocDt).Substring(0, 8);
            bool IsProceed = false;

            //if (BankCode == "008") if (!MandiriValidation()) return;

            if (BankCode != "008")
            {
                var sf = new SaveFileDialog();
                sf.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
                sf.FileName = "RHA";
                if (sf.ShowDialog() == DialogResult.OK) IsProceed = true;
                FileName = sf.FileName;
            }
            else
            {
                IsProceed = true;
            }
            
            if (IsProceed)
            {                
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    RowCount += 1;
                    TotalHeader += decimal.Round(Sm.GetGrdDec(Grd1, Row, 8), 2);
                    Amt = decimal.Round(Sm.GetGrdDec(Grd1, Row, 8), 2);
                    if (BankCode == "008")
                    {
                        lDMandiri.Add(new FrmVoucherRequestPayroll.DMandiri()
                        {
                            EmpAcNo = Sm.GetGrdStr(Grd1, Row, 10),
                            EmpAcName = Sm.GetGrdStr(Grd1, Row, 2),
                            CurCode = Sm.GetLue(LueCurCode),
                            THP = Amt,
                            Remark = MeeRemark.Text,
                            FTS = "IBU",
                            BankName = Sm.GetGrdStr(Grd1, Row, 14),
                            BeneficiaryNotFlag = "N",
                            Email = Sm.GetGrdStr(Grd1, Row, 17),
                            DocNo = TxtDocNo.Text,
                            EmpBankCode = Sm.GetGrdStr(Grd1, Row, 13),
                            SwiftBeneCode = Sm.GetGrdStr(Grd1, Row, 18),
                            KliringCode = Sm.GetGrdStr(Grd1, Row, 19),
                            RTGSCode = Sm.GetGrdStr(Grd1, Row, 20),
                        });
                    }
                    else
                    {
                        lCSVData.Add(new CSVData()
                        {
                            BankAcNo = String.Concat("=" + "\"\"" + "&" + Sm.GetGrdStr(Grd1, Row, 10)),
                            EmpName = Sm.GetGrdStr(Grd1, Row, 2),
                            CurCode = Sm.GetLue(LueCurCode),
                            Total = Amt,
                            Period = DteHolidayDt.Text,
                            NumberOfRow = string.Empty,
                            DocDt = Dt
                        });
                    }                            
                }

                if (lDMandiri.Count > 0)
                {
                    string CurrTime = Sm.Right(Sm.GetValue("Select Date_Format(Now(), '%H%i%s') As Tm;"), 6);
                    var VRP = new FrmVoucherRequestPayroll(mMenuCode);
                    FileName = VRP.GenerateMandiriFileName(mCompanyCodeForMandiriPayroll, Sm.GetDte(DteDocDt), CurrTime).Replace(".txt", ".csv");
                    string BankBranch = Sm.GetValue("Select BranchAcAddress From TblBankAccount Where BankAcCode = @Param ", Sm.GetLue(LueBankAcCode));

                    lHMandiri.Add(new FrmVoucherRequestPayroll.HMandiri()
                    {
                        VRDt = Sm.GetDte(DteDocDt).Substring(0, 8),
                        CreditTo = mCreditTo,
                        CountEmp = lDMandiri.Count,
                        TotalAmt = TotalHeader
                    });

                    CreateCSVFileMandiri(ref lDMandiri, ref lHMandiri, FileName, BankBranch);
                }

                if (lCSVData.Count > 0)
                {
                    CreateCSVFile(ref lCSVData, FileName, TotalHeader, PeriodHeader, Dt);
                }

                lCSVData.Clear(); lHMandiri.Clear(); lDMandiri.Clear();
            }
                
            //var sf = new SaveFileDialog();
            //sf.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            //var DocNo = TxtDocNo.Text.Replace("/", "_");
            //sf.FileName = "Religious Holiday Allowance - " + DocNo;
            //var FileName = string.Empty;
            //string Dt = Sm.GetDte(DteDocDt).Substring(0, 8);
            //if (sf.ShowDialog() == DialogResult.OK)
            //{
            //    var lCSVData = new List<CSVData>();
            //    int RowCount = 0;
            //    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    {
            //        RowCount += 1;
            //        lCSVData.Add(new CSVData()
            //        {
            //            EmpCode = Sm.GetGrdStr(Grd1, Row, 1),
            //            EmpName = Sm.GetGrdStr(Grd1, Row, 2),
            //            EmpCodeOld = Sm.GetGrdStr(Grd1, Row, 3),
            //            DeptName = LueDeptCode.Text,
            //            CurCode = LueCurCode.Text,
            //            Total = Sm.GetGrdDec(Grd1, Row, 8),
            //            NumberOfRow = RowCount.ToString()
            //        });
                        
            //        FileName = sf.FileName;
            //    }
            //    if (lCSVData.Count > 0)
            //    {
            //        CreateCSVFile(ref lCSVData, FileName);
            //    }
            //}
            
        }

        private void CreateCSVFileMandiri(ref List<FrmVoucherRequestPayroll.DMandiri> lD, ref List<FrmVoucherRequestPayroll.HMandiri> lH, string FileName, string BankBranch)
        {
            StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1252));

            foreach (var i in lH)
            {
                #region CSV
                //sw.Write(
                //    "p," + i.VRDt + "," + i.CreditTo + "," + i.CountEmp + "," + i.TotalAmt
                //);
                #endregion

                #region TXT
                sw.Write(
                    "P," + /*Sm.Left(Sm.ServerCurrentDateTime(), 8)*/i.VRDt + "," + i.CreditTo + "," + i.CountEmp + "," + i.TotalAmt + ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"
                );
                #endregion
            }

            foreach (var x in lD)
            {
                #region CSV
                //sw.Write(sw.NewLine);
                //sw.Write(
                //    x.EmpAcNo + "," + x.EmpAcName + ",,,," + x.CurCode + "," + x.THP + "," + x.Remark + ",," + x.FTS + ",," + x.BankName +
                //    ",,,,," + x.BeneficiaryNotFlag + "," + x.Email + ",,,,,,,,,,,,,,,,,,,,,,," + x.DocNo
                //);
                #endregion

                #region TXT
                sw.Write(sw.NewLine);
                if (x.EmpBankCode == "008") // kalau employee itu Mandiri
                {
                    sw.Write(
                        x.EmpAcNo + "," + x.EmpAcName.Replace(',', ' ') + ",,,," + x.CurCode + "," + x.THP + "," + x.Remark.Replace(',', ' ') + ",," + x.FTS + ",," + "MANDIRI" +
                        "," + BankBranch + ",,,," + x.BeneficiaryNotFlag + ",,,,,,,,,,,,,,,,,,,,,,OUR,1,E,,,"
                    );
                }
                else // kalau employee itu non Mandiri
                {
                    if (x.THP > 500000000) // kalau THP diatas 500jt
                    {
                        sw.Write(
                            x.EmpAcNo + "," + x.EmpAcName.Replace(',', ' ') + ",,,," + x.CurCode + "," + x.THP + "," + x.Remark.Replace(',', ' ') + ",," + "RBU" + "," + x.RTGSCode.Replace(',', ' ') + "," + x.BankName.Replace(',', ' ') +
                            ",,,,," + x.BeneficiaryNotFlag + ",,,,,,,,,,,,,,,,,,,,,,OUR,1,E,,,"
                        );
                    }
                    else
                    {
                        sw.Write(
                            x.EmpAcNo + "," + x.EmpAcName.Replace(',', ' ') + ",,,," + x.CurCode + "," + x.THP + "," + x.Remark.Replace(',', ' ') + ",," + "LBU" + "," + x.KliringCode.Replace(',', ' ') + "," + x.BankName.Replace(',', ' ') +
                            ",,,,," + x.BeneficiaryNotFlag + ",,,,,,,,,,,,,,,,,,,,,,OUR,1,E,,,"
                        );
                    }
                }

                #endregion

            }
            sw.Close();
            Sm.StdMsg(mMsgType.Info, "Successfully export file to" + Environment.NewLine + FileName);
            lD.Clear();
            lH.Clear();
        }

        #endregion

        #endregion

        #region Class

        private class RHA
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public decimal Amt { get; set; }
            public decimal Tax { get; set; }
            public decimal Total { get; set; }
            public string PrintBy { get; set; }
        }
        
        private class CSVData
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string DeptName { get; set; }
            public string CurCode { get; set; }
            public decimal Total { get; set; }
            public string NumberOfRow { get; set; }
            public string BankAcNo { get; set; }
            public string Period { get; set; }
            public string DocDt { get; set; }
        }

        #endregion
    }
}
