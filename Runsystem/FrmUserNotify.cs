﻿#region Update
    
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmUserNotify : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool IsInsert = false;
        internal FrmUserNotifyFind FrmFind;

        #endregion

        #region Constructor

        public FrmUserNotify(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "", 

                        //1-3
                        "User Code", "User Name",
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-2
                        80, 180,
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueDocumentCode }, true);
                    LueDocumentCode.Focus();
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueDocumentCode }, false);
                    LueDocumentCode.Focus();
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueDocumentCode }, false);
                    Grd1.ReadOnly = false;
                    LueDocumentCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { LueDocumentCode });
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmUserNotifyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueDocCode(ref LueDocumentCode);
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueDocumentCode, "")) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SaveData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocumentCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowUserNotify(DocumentCode);
                ShowUserNotifyDtl(DocumentCode);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowUserNotify(string DocCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocCode", DocCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocCode From TblUserNotify Where DocCode=@DocCode",
                    new string[] { "DocCode" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        SetLueDocCode(ref LueDocumentCode);
                        Sm.SetLue(LueDocumentCode, Sm.DrStr(dr, c[0]));
                    }, true
                );
        }

        private void ShowUserNotifyDtl(string DocCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.UserCode, B.UserName "); 
            SQL.AppendLine("From TblUserNotify A ");
            SQL.AppendLine("Inner Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("Where A.DocCode=@DocCode ");

            var cmDtl = new MySqlCommand();
            Sm.CmParam<String>(ref cmDtl, "@DocCode", DocCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cmDtl,
                SQL.ToString(),
                new string[] 
                { 
                    //0-1
                    "UserCode", "UserName", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueDocumentCode, "Document");
        }

        private void SaveData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(DelUserNotify());

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveUserNotify(Row));

            Sm.ExecCommands(cml);

            ShowData(Sm.GetLue(LueDocumentCode));
        }


        private MySqlCommand DelUserNotify()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblUserNotify Where DocCode= @DocCode ; ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetLue(LueDocumentCode));

            return cm;
        }

        private MySqlCommand SaveUserNotify(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblUserNotify(DocCode, UserCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocCode, @UserCode, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetLue(LueDocumentCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        internal string GetSelectedUserCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void SetLueDocCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select '01' As Col1, 'Employee Contract Document' As Col2 ");
                SQL.AppendLine("Order By Col1 ;");    
                
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Document", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmUserNotifyDlg(this));
            }
        }


        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void LueDocumentCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocumentCode, new Sm.RefreshLue1(SetLueDocCode));
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmUserNotifyDlg(this));
        }

        #endregion

       

       

       

        

        #endregion

    }
}
