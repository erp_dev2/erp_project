﻿#region Update
/*
    21/08/2019 [TKG] tambah display name
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptDepreciationAsset3 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty
            ;
        private bool mIsAutoJournalActived = false;

        #endregion

        #region Constructor

        public FrmRptDepreciationAsset3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
        }

      
        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document", 
                    "Asset's Code",
                    "Asset's Name",
                    "Display Name",
                    "Item's Code",
                    
                    //6-10
                    "Item's Name",
                    "Asset's"+Environment.NewLine+"Value",
                    "Depreciation"+Environment.NewLine+"Method",
                    "Year",
                    "",
                    
                    //11-13
                    "Depreciation"+Environment.NewLine+"Value",
                    "Cost"+Environment.NewLine+"Value",
                    "Balance"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    120, 120, 200, 200, 100, 

                    //6-10
                    200, 130, 120, 50, 20, 

                    //11-13
                    150, 150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 11, 12, 13 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5, 6, 12 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            var lAssetDep = new List<AssetDep>();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lAssetDep);
                if (lAssetDep.Count > 0) Process2(ref lAssetDep);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #region List

        private void Process1(ref List<AssetDep> l)
        {
            string Filter = "";

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("Select Distinct A.DocNo, A.AssetCode, C.AssetName, C.DisplayName, C.ItCode, D.ItName,  ");
            SQL.AppendLine("A.AssetValue, H.OptDesc As DepMethod, B.Yr, G.DepreciationValue, G.CostValue, A.AssetValue - G.DepreciationValue As Balance ");
            SQL.AppendLine("From tbldepreciationassethdr A ");
            SQL.AppendLine("Inner Join tbldepreciationassetdtl B On B.DocNo = A.DocNo ");
            SQL.AppendLine("Inner Join tblasset C On C.AssetCode = A.AssetCode ");
            SQL.AppendLine("Inner join tblitem D On D.ItCode = C.ItCode ");
            SQL.AppendLine("Inner join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select E.DocNo, E.AssetCode, F.Yr, Sum(F.DepreciationValue) As DepreciationValue, Sum(F.CostValue) As CostValue From ");
            SQL.AppendLine("    tbldepreciationassethdr E ");
            SQL.AppendLine("    Inner Join tbldepreciationassetdtl F On E.DocNo = F.DocNo ");
            SQL.AppendLine("    Where E.CancelInd='N' ");
            SQL.AppendLine("    Group By E.DocNo, E.AssetCode, F.Yr ");
            //SQL.AppendLine("    Order By E.DocNo Asc ");
            SQL.AppendLine(")G On A.DocNo = G.DocNo And A.AssetCode = G.AssetCode  And G.Yr = B.Yr ");
            SQL.AppendLine("Left Join tbloption H On H.OptCode = A.DepreciationCode And H.OptCat = 'DepreciationMethod' ");
            SQL.AppendLine("Where A.CancelInd = 'N'  ");
            SQL.AppendLine("Order By B.Yr  ");
            SQL.AppendLine(")Tbl ");

            //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "Tbl.Yr", true);
            Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "Tbl.AssetCode", "Tbl.AssetName" });

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString() + Filter + " Order By Tbl.AssetName, Tbl.Yr;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "AssetCode", "AssetName", "DisplayName", "ItCode", "ItName", 
                    
                    //6-10
                    "AssetValue", "DepMethod", "Yr", "DepreciationValue", "CostValue"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AssetDep()
                        {
                            Document   = Sm.DrStr(dr, c[0]),
                            AssetCode  = Sm.DrStr(dr, c[1]),
                            AssetName  = Sm.DrStr(dr, c[2]),
                            DisplayName = Sm.DrStr(dr, c[3]),
                            ItemCode  = Sm.DrStr(dr, c[4]),
                            ItemName  = Sm.DrStr(dr, c[5]),
                            AssetValue  = Sm.DrDec(dr, c[6]),
                            DepreciationMethod  = Sm.DrStr(dr, c[7]),
                            Year  = Sm.DrStr(dr, c[8]),
                            Depreciationvalue  = Sm.DrDec(dr, c[9]),
                            CostValue = Sm.DrDec(dr, c[10]),
                            Balance = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<AssetDep> l)
        {
            string AssetCode = String.Empty, yr = String.Empty;
            var DepVal = 0m;
            iGRow r;
            
            Sm.ClearGrd(Grd1, false);

            Grd1.BeginUpdate();

            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].Document;
                r.Cells[2].Value = l[i].AssetCode;
                r.Cells[3].Value = l[i].AssetName;
                r.Cells[4].Value = l[i].DisplayName;
                r.Cells[5].Value = l[i].ItemCode;
                r.Cells[6].Value = l[i].ItemName;
                r.Cells[7].Value = l[i].AssetValue;
                r.Cells[8].Value = l[i].DepreciationMethod;
                r.Cells[9].Value = l[i].Year;
                r.Cells[12].Value = l[i].Depreciationvalue;
                if (l[i].AssetCode != AssetCode)
                {
                    AssetCode = l[i].AssetCode;
                    DepVal = 0m;
                    DepVal = l[i].Depreciationvalue;
                    r.Cells[11].Value = l[i].Depreciationvalue;
                    r.Cells[13].Value = l[i].AssetValue - DepVal;
                }
                else
                {
                    DepVal = DepVal + l[i].Depreciationvalue;
                    r.Cells[11].Value = DepVal;
                    r.Cells[13].Value = l[i].AssetValue - DepVal;
                }
                
            }
            Grd1.GroupObject.Add(2);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12, 13 });
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmRptDepreciationAsset3Dlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 9));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptDepreciationAsset3Dlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 9));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion       

        #region Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        #endregion

        #region Class

        private class AssetDep
        {
            public string No { get; set; }
            public string Document { get; set; }
            public string AssetCode { get; set; }
            public string AssetName { get; set; }
            public string DisplayName { get; set; }
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public decimal AssetValue { get; set; }
            public string DepreciationMethod { get; set; }
            public string Year { get; set; }
            public decimal Depreciationvalue { get; set; }
            public decimal CostValue { get; set; }
            public decimal Balance { get; set; }
        }

        #endregion
    }
}
