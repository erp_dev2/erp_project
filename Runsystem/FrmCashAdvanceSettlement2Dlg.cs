﻿#region Update
/*
    11/11/2022 [IBL/BBT] New application
    17/11/2022 [IBL/BBT] Comment prosses GetCCCode(). Karena CC tidak melihat ke dept lagi.
    09/12/2022 [IBL/BBT] Voucher yg muncul adl yg Bank Account di Debit To nya = Bank Account yg dipilih di hdr
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCashAdvanceSettlement2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmCashAdvanceSettlement2 mFrmParent;
        
        #endregion

        #region Constructor

        public FrmCashAdvanceSettlement2Dlg(FrmCashAdvanceSettlement2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                SetLuePIC(ref LueUserCode);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                SetGrd();
                //SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "User Code",
                        "Person In Charge",
                        "Department Code",
                        "Department",

                        //6-10
                        "Voucher#",
                        "",
                        "Voucher's Type",
                        "Date",
                        "SO Contract#",

                        //11-13
                        "Currency",
                        "Amount",
                        "Remark",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 0, 150, 0, 200,  
                        
                        //6-10
                        130, 20, 200, 80, 120, 
                        
                        //11-13
                        80, 130, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 7 }, false);
            if (!mFrmParent.mIsVRForBudgetUseSOContract && !mFrmParent.mIsVRUseSOContract)
                Sm.GrdColInvisible(Grd1, new int[] { 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select PIC, UserName, DeptCode, DeptName, ");
            SQL.AppendLine("DocNo, DocDt, CurCode, Remark, OptDesc, Amt, SOContractDocNo ");
            SQL.AppendLine("From ( ");
            if (mFrmParent.mIsVRBudgetUseCASBA)
            {
                //SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select PIC, UserName, DeptCode, DeptName, ");
                SQL.AppendLine("    DocNo, DocDt, CurCode, Remark, OptDesc, Amt, SOContractDocNo ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select ifnull(B.PIC, G.Empname) As PIC, C.UserName, B.DeptCode, D.DeptName, ");
                SQL.AppendLine("        A.DocNo, A.DocDt, A.CurCode, A.Remark, E.OptDesc, ");
                SQL.AppendLine("        (A.Amt-IfNull(G.Amt, 0.00)) As Amt, Null As SOContractDocNo ");
                SQL.AppendLine("        From TblVoucherHdr A ");
                SQL.AppendLine("        Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
                SQL.AppendLine("        Left Join TblUser C On B.PIC=C.UserCode ");
                SQL.AppendLine("        Inner Join TblDepartment D On B.DeptCode=D.DeptCode ");
                SQL.AppendLine("        Left Join TblOption E On A.DocType=E.OptCode And E.OptCat='VoucherDocType' ");
                SQL.AppendLine("        Left Join TblEmployee G On B.PIC = G.EmpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.DocNo, Sum(T2.Amt1) As Amt  ");
                SQL.AppendLine("            From TblVoucherHdr T1  ");
                SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl3 T2 On T1.DocNo=T2.VoucherDocNo  ");
                SQL.AppendLine("            Inner Join TblCashAdvanceSettlementHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
                SQL.AppendLine("            Where T1.DocType=@VoucherDocTypeCASBA ");
                SQL.AppendLine("            And T1.CancelInd='N' ");
                SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group By T1.DocNo ");
                SQL.AppendLine("        ) G On A.DocNo=G.DocNo ");
                SQL.AppendLine("        Where A.DocType=@VoucherDocTypeCASBA ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.DocNo Not In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct VoucherDocNo ");
                SQL.AppendLine("            From TblCashAdvanceSettlementDtl ");
                SQL.AppendLine("            Where Docno In ");
                SQL.AppendLine("            ( ");
                SQL.AppendLine("                Select DocNo ");
                SQL.AppendLine("                From TblCashAdvanceSettlementHdr ");
                SQL.AppendLine("                Where Status In ('O', 'A') ");
                SQL.AppendLine("                And CancelInd = 'N' ");
                SQL.AppendLine("                And CompletedInd = 'Y' ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                if (mFrmParent.mBankAccountTypeForCAS.Length > 0)
                {
                    // debit to nya si voucher itu, bank account nya bertipe sesuai parameter BankAccountTypeForCAS
                    //SQL.AppendLine("        And ");
                    //SQL.AppendLine("        ( ");
                    //SQL.AppendLine("            ( ");
                    //SQL.AppendLine("                A.AcType = 'D' And A.BankAcCode In ");
                    //SQL.AppendLine("                ( ");
                    //SQL.AppendLine("                    Select BankAcCode ");
                    //SQL.AppendLine("                    From TblBankAccount ");
                    //SQL.AppendLine("                    Where Find_In_Set(BankAcTp, @BankAccountTypeForCAS) ");
                    //SQL.AppendLine("                ) ");
                    //SQL.AppendLine("            ) ");
                    //SQL.AppendLine("            Or ");
                    //SQL.AppendLine("            ( ");
                    //SQL.AppendLine("                A.AcType2 = 'D' And A.BankAcCode2 In ");
                    //SQL.AppendLine("                ( ");
                    //SQL.AppendLine("                    Select BankAcCode ");
                    //SQL.AppendLine("                    From TblBankAccount ");
                    //SQL.AppendLine("                    Where Find_In_Set(BankAcTp, @BankAccountTypeForCAS) ");
                    //SQL.AppendLine("                ) ");
                    //SQL.AppendLine("            ) ");
                    //SQL.AppendLine("        ) ");
                }
                SQL.AppendLine("        And ( ");
                SQL.AppendLine("        	(A.AcType = 'D' And A.BankAcCode = @BankAcCode) ");
                SQL.AppendLine("        	Or ");
                SQL.AppendLine("        	(A.AcType2 = 'D' And A.BankAcCode2 = @BankAcCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    ) T Where T.Amt>0.00 ");
                if (mFrmParent.mIsFilterByDept)
                {
                    SQL.AppendLine("And Exists ( ");
                    SQL.AppendLine("    Select 1 From tblgroupdepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where ");
                    SQL.AppendLine("    UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }

            }
            else
            {
                SQL.AppendLine("    Select ifnull(B.PIC, G.Empname) As PIC, C.UserName, B.DeptCode, D.DeptName, ");
                SQL.AppendLine("    A.DocNo, A.DocDt, A.CurCode, A.Remark, E.OptDesc, ");
                SQL.AppendLine("    If(A.DocType ='61', ");
                if (mFrmParent.mIsCASFromTRProcessAllAmount)
                    SQL.AppendLine("    A.Amt ");
                else
                    SQL.AppendLine("    (A.Amt- ifnull(F.DailyAmt, 0.00)) ");
                SQL.AppendLine("    , A.Amt) As Amt, ");
                if (mFrmParent.mIsVRUseSOContract || mFrmParent.mIsVRForBudgetUseSOContract)
                    SQL.AppendLine("    B.SOContractDocNo ");
                else
                    SQL.AppendLine("    Null As SOContractDocNo ");
                SQL.AppendLine("    From TblVoucherHdr A ");
                SQL.AppendLine("    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
                SQL.AppendLine("    Left Join TblUser C On B.PIC=C.UserCode ");
                SQL.AppendLine("    Inner Join TblDepartment D On B.DeptCode=D.DeptCode ");
                SQL.AppendLine("    Left Join TblOption E On A.DocType=E.OptCode And E.OptCat='VoucherDocType' ");
                SQL.AppendLine("    Left Join TblEmployee G On B.PIC = G.EmpCode ");
                if (!mFrmParent.mIsCASFromTRProcessAllAmount)
                {
                    SQL.AppendLine("    Left Join ( ");
                    SQL.AppendLine("        Select A.VoucherRequestDocNo As DocNo, SUM(B.Amt2) As DailyAmt  ");
                    SQL.AppendLine("        From TblTravelRequestDtl8 A  ");
                    SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo  And A.EmpCode = B.PICCode ");
                    SQL.AppendLine("        Inner Join TblTravelRequestHdr C On A.DocNo = C.DocNo And CancelInd = 'N' ");
                    SQL.AppendLine("        Group By A.VoucherRequestDocNo ");
                    SQL.AppendLine("    )F On B.DocNo = F.DocNo ");
                }
                SQL.AppendLine("    Where A.DocType In ('56', '61') ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("    And A.DocNo Not In ( ");
                SQL.AppendLine("        Select B.VoucherDocNo ");
                SQL.AppendLine("        From TblCashAdvanceSettlementHdr A, TblCashAdvanceSettlementDtl3 B ");
                SQL.AppendLine("        Where A.DocNo=B.DocNo ");
                SQL.AppendLine("        And A.CancelInd='N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(Filter);
                if (mFrmParent.mIsFilterByDept)
                {
                    SQL.AppendLine("And Exists ( ");
                    SQL.AppendLine("    Select 1 From tblgroupdepartment ");
                    SQL.AppendLine("    Where DeptCode=B.DeptCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where ");
                    SQL.AppendLine("    UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("    And ( ");
                SQL.AppendLine("    	(A.AcType = 'D' And A.BankAcCode = @BankAcCode) ");
                SQL.AppendLine("    	Or ");
                SQL.AppendLine("    	(A.AcType2 = 'D' And A.BankAcCode2 = @BankAcCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(") Tbl Order By DeptName, UserName, DocDt, DocNo");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@VoucherDocTypeCASBA", mFrmParent.mVoucherDocTypeCASBA);
                Sm.CmParam<String>(ref cm, "@BankAccountTypeForCAS", mFrmParent.mBankAccountTypeForCAS);
                Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(mFrmParent.LueBankAcCode));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueUserCode), "B.PIC", true);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[] 
                    { 
                    
                        //0
                        "PIC",
                        
                        //1-5
                        "UserName", "DeptCode", "DeptName", "DocNo", "OptDesc", 
                        
                        //6-10
                        "DocDt", "SOContractDocNo", "CurCode", "Amt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 8);
                        if (mFrmParent.mIsVRForBudgetUseSOContract || mFrmParent.mIsVRUseSOContract)
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 10);

                        mFrmParent.Grd1.Cells[Row1, 3].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 4].Value = null;
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 6, 27, 28 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6, 7, 27, 28 });

                        mFrmParent.ComputeAmt1();
                        mFrmParent.SetCostCategoryInfo();
                        mFrmParent.SetVoucherInfo();
                        mFrmParent.ComputeAmt2();
                        mFrmParent.SetDeptCode();
                        //if (mFrmParent.mIsCASUseCostCenter) mFrmParent.GetCCCode();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private void SetLuePIC(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct B.UserCode As Col1, B.UserName As Col2  ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblUser B On A.PIC=B.UserCode ");
            SQL.AppendLine("Where A.VoucherDocNo Is Not Null ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='A' ");
            if (mFrmParent.mIsVRBudgetUseCASBA)
                SQL.AppendLine("And A.DocType In (@VoucherDocTypeCASBA) ");
            else
                SQL.AppendLine("And A.DocType In ('56','61') ");
            if (mFrmParent.mCASDlgFilterPICDataSource == "2")
            {
                SQL.AppendLine("And A.VoucherDocNo Not In ( ");
                SQL.AppendLine("    Select Distinct B.VoucherDocNo ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where A.CancelInd = 'N' ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By B.UserName;");
            
            cm.CommandText = SQL.ToString();
            Sm.CmParam<string>(ref cm, "@VoucherDocTypeCASBA", mFrmParent.mVoucherDocTypeCASBA);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueUserCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUserCode, new Sm.RefreshLue1(SetLuePIC));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkUserCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Person in charge");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #region Grid Method
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion
    }
}
