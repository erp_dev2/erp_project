﻿#region Update
/*
    04/13/2017 [WED] tambah approval per item
    17/07/2017 [TKG] tambah journal antar entity
    15/04/2018 [TKG] ubah proses journal untuk beda entity
    17/04/2018 [TKG] ubah proses journal untuk beda entity menjadi 2 dokumen
    17/07/2018 [TKG] tambah cost center saat journal
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs5 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRecvWhs5Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string
            mDocType1 = "15",
            mDocType2 = "16",
            mBarcode = string.Empty,
            mDSMenuCodeForImportCSV = string.Empty,
            mDSWhsProcessDOFrom = string.Empty,
            mDSWhsProcessDOTo = string.Empty;
        private bool mIsNew = false;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmRecvWhs5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item From Other Warehouse";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (Sm.CompareStr(mDSMenuCodeForImportCSV, mMenuCode))
                    BtnImport.Visible = true;
                else
                    BtnImport.Visible = false;
                SetLueBin(ref LueBin);
                LueBin.Visible = false;
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Bin (To)",
                        "Item's Code",
                        "Item's Name",
                        "Batch#",
                        
                        //6-10
                        "Tracking# (1)",
                        "Tracking# (2)",
                        "Source",
                        "Lot",
                        "Bin (From)",
                        
                        //11-15
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,
                        //1-5
                        20, 100, 100, 200, 100, 
                        //6-10
                        100, 100, 200, 0, 100, 
                        //11-15
                        80, 80, 80, 80, 80, 
                        //16-20
                        80, 80, 80, 80, 300
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 14, 15, 17, 18 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 4, 8, 9 }, false);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 11, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16 }, true); 
            
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, TxtBarcode }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
                    if (BtnImport.Visible) BtnImport.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, TxtBarcode }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2 });
                    if (BtnImport.Visible) BtnImport.Enabled = false;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mBarcode = string.Empty;
            mIsNew = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark,
                TxtBarcode
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 14, 15, 17, 18 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvWhs5Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueWhsCode(ref LueWhsCode2, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                    if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvWhs5Dlg(this, Sm.GetLue(LueWhsCode2)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 1, 2, 20 }, e.ColIndex))
                    {
                        if (e.ColIndex==2) LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                            
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18 });
                    }
                     
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from"))
                Sm.FormShowDialog(new FrmRecvWhs5Dlg(this, Sm.GetLue(LueWhsCode2)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 12, 15, 18 }, e);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 15, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 12);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 16)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 2 }, e.ColIndex))
            {
                var Bin = Sm.GetGrdStr(Grd1, 0, 2);
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                    Grd1.Cells[r, 2].Value = Bin;
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvWhs5", "TblRecvWhs5Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvWhs5Hdr(DocNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 3).Length > 0)
                    cml.Add(SaveRecvWhs5Dtl(DocNo, r));
            cml.Add(SaveStockMovement(DocNo));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 3).Length > 0)
                    cml.Add(SaveStockSummary(r));

            if (Sm.CompareStr(Sm.GetLue(LueWhsCode), mDSWhsProcessDOFrom))
                cml.Add(SaveDOWhs2(DocNo));    
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private MySqlCommand SaveDOWhs2(string RecvWhs5DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhs2Hdr ");
            SQL.AppendLine("(DocNo, DocDt, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, DocDt, WhsCode, @WhsCode, Remark, Createby, CreateDt ");
            SQL.AppendLine("From TblRecvWhs5Hdr Where DocNo=@RecvWhs5DocNo;");

            SQL.AppendLine("Insert Into TblDOWhs2Dtl ");
            SQL.AppendLine("(DocNo, DNo, ItCode, BatchNo, PropCode, PropCode2, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, DNo, ItCode, BatchNo, PropCode, PropCode2, Source, Lot, Bin, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs5Dtl Where DocNo=@RecvWhs5DocNo;");
           
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOWhs2", "TblDOWhs2Hdr"));
            Sm.CmParam<String>(ref cm, "@RecvWhs5DocNo", RecvWhs5DocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", mDSWhsProcessDOTo);

            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Transfer from") ||
                Sm.IsLueEmpty(LueWhsCode, "Transfer to") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()
                //Sm.IsDocDtNotValid(Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"), Sm.GetDte(DteDocDt))
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Batch# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                   "Tracking# (1) : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                   "Tracking# (2) : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                   "Source : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                   "Bin (To) : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 12) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 11) < Sm.GetGrdDec(Grd1, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than transferred quantity.");
                    return true;
                }

                if (Grd1.Cols[15].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than transferred quantity (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than transferred quantity (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveRecvWhs5Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvWhs5Hdr(DocNo, DocDt, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvWhs5Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRecvWhs5Dtl(DocNo, DNo, DOWhs2DocNo, DOWhs2DNo, ItCode, BatchNo, PropCode, PropCode2, Source, Lot, Bin, Bin2, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, Null, Null, @ItCode, @BatchNo, @PropCode, @PropCode2, @Source, @Lot, @Bin, @Bin2, @Qty, @Qty2, @Qty3, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@PropCode2", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Bin2", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, CancelInd, DocDt, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, ");
            SQL.AppendLine("PropCode2, BatchNo, Source, Qty, Qty2, ");
            SQL.AppendLine("Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, 'N', A.DocDt, ");
            SQL.AppendLine("A.WhsCode2, B.Lot, B.Bin2, B.ItCode, B.PropCode, ");
            SQL.AppendLine("B.PropCode2, B.BatchNo, B.Source, -1.00*B.Qty, -1.00*B.Qty2, ");
            SQL.AppendLine("-1.00*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs5Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs5Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, 'N', A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, ");
            SQL.AppendLine("B.PropCode2, B.BatchNo, B.Source, B.Qty, B.Qty2, ");
            SQL.AppendLine("B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs5Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs5Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");
            
            SQL.AppendLine("Insert Into TblStockSummary ");
            SQL.AppendLine("(WhsCode, Lot, Bin, ItCode, PropCode, ");
            SQL.AppendLine("PropCode2, BatchNo, Source, Qty, Qty2, ");
            SQL.AppendLine("Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, ");
            SQL.AppendLine("B.PropCode2, B.BatchNo, B.Source, 0.00, 0.00, ");
            SQL.AppendLine("0.00, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs5Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs5Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblStockSummary C ");
            SQL.AppendLine("    On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And B.Lot=C.Lot ");
            SQL.AppendLine("    And B.Bin=C.Bin ");
            SQL.AppendLine("    And B.Source=C.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And C.WhsCode Is Null; ");

            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType1", mDocType1);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode2 And Lot=@Lot And Bin=@Bin2 And Source=@Source; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Bin2", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowRecvWhs5Hdr(DocNo);
                ShowRecvWhs5Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvWhs5Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, WhsCode2, Remark From TblRecvWhs5Hdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "WhsCode2", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sl.SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowRecvWhs5Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.Bin, B.ItCode, C.ItName, ");
            SQL.AppendLine("B.BatchNo, B.PropCode, B.PropCode2, ");
            SQL.AppendLine("B.Source, B.Lot, B.Bin2, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.Remark ");
            SQL.AppendLine("From TblRecvWhs5Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs5Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "Bin", "ItCode", "ItName", "BatchNo", "PropCode", 
                    
                    //6-10
                    "PropCode2", "Source", "Lot", "Bin2", "Qty", 
                    
                    //11-15
                    "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                    
                    //16
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Grd.Cells[Row, 11].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Grd.Cells[Row, 14].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Grd.Cells[Row, 17].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 14, 15, 17, 18 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }


        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;",
                "Bin");
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mDSMenuCodeForImportCSV = Sm.GetParameter("DSMenuCodeForImportCSV");
            mDSWhsProcessDOFrom = Sm.GetParameter("DSWhsProcessDOFrom");
            mDSWhsProcessDOTo = Sm.GetParameter("DSWhsProcessDOTo");
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void ShowBarcodeInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, A.BatchNo, A.PropCode, A.PropCode2, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, A.Remark ");
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            SQL.AppendLine("And A.Qty>0 ");
            SQL.AppendLine("And A.BatchNo=@BatchNo ");
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 8);
                        Lot = Sm.GetGrdStr(Grd1, r, 9);
                        Bin = Sm.GetGrdStr(Grd1, r, 10);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (A.Source=@Source0" + r.ToString() + " And A.Lot=@Lot0" + r.ToString() + " And A.Bin=@Bin0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                            Sm.CmParam<String>(ref cm, "@Lot0" + r.ToString(), Lot);
                            Sm.CmParam<String>(ref cm, "@Bin0" + r.ToString(), Bin);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not (" + Filter + ") Limit 1;";
                else
                    Filter = " Limit 1;";

                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode2));
                Sm.CmParam<String>(ref cm, "@BatchNo", TxtBarcode.Text);

                int Row = Grd1.Rows.Count - 1;
                Grd1.BeginUpdate();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString()+Filter;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "ItCode",

                        //1-5
                        "ItName", "BatchNo", "PropCode", "PropCode2", "Source", 
                        
                        //6-10
                        "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 
                        
                        //11-14
                        "InventoryUomCode2", "Qty3", "InventoryUomCode3", "Remark"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 14);
                            Row++;
                        }
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                        Console.Beep();
                    }
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Grd1.EndUpdate();
                TxtBarcode.EditValue = null;
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            ClearGrd();
        }

        private void TxtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back) e.SuppressKeyPress = true;
        }

        private void TxtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                mIsNew = true;
                mBarcode = string.Empty;
                ShowBarcodeInfo();
                TxtBarcode.Focus();
            }
            else
            {
                if (mIsNew)
                {
                    mBarcode = string.Empty;
                    TxtBarcode.Text = string.Empty;
                }
                mBarcode = mBarcode + e.KeyChar;
                mIsNew = false;
                TxtBarcode.Focus();
            }
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueBin.GetColumnValue("Col1");
                LueBin.Visible = false;
            }
        }

        #endregion

        private void BtnImport_Click(object sender, EventArgs e)
        {
            var l = new List<Result>();
            try
            {
                ProcessImport1(ref l);
                if (l.Count>0) ProcessImport2(ref l);
                Sm.StdMsg(mMsgType.Info, "Process is completed.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
            }
        }

        private void ProcessImport1(ref List<Result> l)
        {
            string ItCodeTemp = Sm.GetParameter("DSDefaultItCode");

            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            using (var rd = new StreamReader(@openFileDialog1.FileName))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() == 5)
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            l.Add(new Result()
                            {
                                BatchNo = splits[0].Trim(),
                                PropCode = splits[1].Trim(),
                                PropCode2 = splits[2].Trim(),
                                Bin = splits[3].Trim(),
                                Remark = splits[4].Trim(),
                                Qty = 1m,
                                DocNo = string.Empty,
                                DNo = string.Empty,
                                ItCode = string.Empty
                            });

                            for (int i = 0; i < l.Count; i++)
                            {
                                l[i].ItCode = ItCodeTemp;
                                l[i].DNo = (i + 1).ToString();
                            }
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, string.Empty);
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessImport2(ref List<Result> l)
        {
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string DocDt = Sm.Left(Sm.ServerCurrentDateTime(), 8);
            string DocNo = Sm.GenerateDocNo(DocDt, "StockInitial", "TblStockInitialHdr");

            cml.Add(SaveStockInitialHdr(DocNo, DocDt));
            foreach(var i in l)
                cml.Add(SaveStockInitialDtl(DocNo, i));
            cml.Add(SaveStock(DocNo));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand SaveStockInitialHdr(string DocNo, string DocDt)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Insert Into TblStockInitialHdr ");
            SQL.AppendLine("(DocNo, DocDt, WhsCode, CurCode, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='DSDefaultWHSCodeImport'), ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='MainCurCode'), ");
            SQL.AppendLine("1.00, Null, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveStockInitialDtl(string DocNo, Result i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockInitialDtl ");
            SQL.AppendLine("(DocNo, DNo, CancelInd, ItCode, BatchNo, PropCode, PropCode2, Source, Lot, Bin, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @ItCode, ");
            SQL.AppendLine("IfNull(@BatchNo, '-'), IfNull(@PropCode, '-'), IfNull(@PropCode2, '-'), ");
            SQL.AppendLine("Concat('04*', @DocNo, '*', @DNo), '-', IfNull(@Bin, '-') , ");
            SQL.AppendLine("@Qty, @Qty, @Qty, 0.00, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", i.DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", i.ItCode);
            Sm.CmParam<String>(ref cm, "@BatchNo", i.BatchNo);
            Sm.CmParam<String>(ref cm, "@PropCode", i.PropCode);
            Sm.CmParam<String>(ref cm, "@PropCode2", i.PropCode2);
            Sm.CmParam<String>(ref cm, "@Bin", i.Bin);
            Sm.CmParam<Decimal>(ref cm, "@Qty", i.Qty);
            Sm.CmParam<String>(ref cm, "@Remark", i.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, PropCode, PropCode2, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '04', A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.PropCode, B.PropCode2, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary ");
            SQL.AppendLine("(WhsCode, Lot, Bin, ItCode, BatchNo, PropCode, PropCode2, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.PropCode, B.PropCode2, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice ");
            SQL.AppendLine("(ItCode, BatchNo, PropCode, PropCode2, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.ItCode, B.BatchNo, B.PropCode, B.PropCode2, B.Source, A.CurCode, B.UPrice, A.ExcRate, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockInitialHdr A ");
            SQL.AppendLine("Inner Join TblStockInitialDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Class

        private class Result
        {
            public string ItCode { get; set; }
            public string BatchNo { get; set; }
            public string PropCode { get; set; }
            public string PropCode2 { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public string Remark { get; set; }

            public string DocNo { get; set; }
            public string DNo { get; set; }
        }

        #endregion
    }
}
