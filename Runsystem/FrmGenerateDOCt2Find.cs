﻿#region Update
/*
    31/01/2020 [WED/SIER] new apps
    20/09/2020 [TKG/SIER] tambah kolom dan filter kategori customer
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmGenerateDOCt2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmGenerateDOCt2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGenerateDOCt2Find(FrmGenerateDOCt2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -3);
                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueCtCtCode(ref LueCtCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, D.WhsName, C.CtName, E.CtCtName, B.DOCtDocNo, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblGenerateDOCt2Hdr A ");
            SQL.AppendLine("Left Join TblGenerateDOCt2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCustomer C On B.CtCode = C.CtCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode = D.WhsCode ");
            SQL.AppendLine("Left Join TblCustomerCategory E On C.CtCtCode=E.CtCtCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Warehouse",
                    "Customer", 
                    "Customer's Category", 
                    
                    //6-10
                    "DO#",
                    "Remark",
                    "Created By",
                    "Created Date", 
                    "Created Time", 
                    
                    //11-13
                    "Last Updated By", 
                    "Last Updated Date", 
                    "Last Updated Time",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 80, 180, 250, 200, 
                    
                    //6-10
                    150, 200, 130, 130, 130, 
                    
                    //11-13
                    130, 130, 130
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCtCode), "C.CtCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDOCtDocNo.Text, new string[] { "B.DOCtDocNo" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "WhsName", "CtName", "CtCtName", "DOCtDocNo", 
                        
                        //6-10
                        "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 10);
                    }, true, true, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer's category");
        }

        private void TxtDOCtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDOCtDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        #endregion

        #endregion
    }
}
