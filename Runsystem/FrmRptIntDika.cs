﻿#region Update
/*
    23/09/2022 [har] new reporting
    08/11/2022 [har/pht] FILTER DATE
    13/12/2022 [HAR/PHT] informasi journal dihapus diganti dialog berisi list jurnal 
    09/01/2022 [HAR/PHT] bug informasi dlg jurnal
    31/01/2023 [HAR/PHT] tambah amount di tiap document yang terbentuk
    02/02/2023 [HAR/PHT] tambah pic regional
    10/02/2023 [HAR/PHT] kolom amt DO masih bisa diedit
    08/03/2023 [HAR/PHT] tambah kolom infromasi whs DIKA, Whs ERP dan Whs category
    14/03/2023 [BRI/PHT] merubah nama field agar tidak bug ketika di group
    14/03/2023 [BRI/PHT] ternyata minta semua feild
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptIntDika : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterByDeptHR = false, mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmRptIntDika(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        { 
            
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT X.DocDt, X.Receiptno, A.DOCtDocNo, (B.Qty * B.Uprice) AS DOCtAMt, ");
            SQL.AppendLine("A.SIDocNo, C.Amt AS SIAMt, A.IPDocNo, D.Amt AS IPAmt, ");
            SQL.AppendLine("A.VRDocno, E.Amt AS VRAmt, A.VCDocno, F.Amt AS VCAmt, null as JournalDocnO, A.ChannelName, X.WhsCode, G.WhsName, I.WhsCtname ");
            SQL.AppendLine("FROM PhtstagingInt.tblpayment2 X ");
            SQL.AppendLine("Left Join  tbldikahdr A  On X.Receiptno = A.Receiptno ");
            SQL.AppendLine("Left JOIN tbldoctDtl b ON a.dOCtDocNo = B.DoCNO AND Dno = '001' ");
            SQL.AppendLine("Left Join tblsalesinvoicehdr C ON A.SIDocNo = C.DocNo ");
            SQL.AppendLine("Left JOIN tblincomingpaymenthdr D ON A.IPDocNo = D.DocNo ");
            SQL.AppendLine("Left JOIN tblvoucherrequesthdr E ON A.VRDocno = E.DocNo ");
            SQL.AppendLine("Left JOIN tblvoucherhdr F ON A.VCDocno = F.DocNo ");
            SQL.AppendLine("Left Join Tblwarehouse G On X.WhsCode = G.WhsCodeOld ");
            SQL.AppendLine("Left Join TblOption H On G.WhsCtCode = H.OptCode And H.Optcat = 'WhsCategoryCostCenter'");
            SQL.AppendLine("Left Join TblWarehousecategory I On H.OptCode = I.WhsCtCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Date#", 
                    "ReceiptNo",
                    "Regional PIC",
                    "DO to Cust",
                    "Amount DO",

                    //6-10
                    "",
                    "Salesinvoice",
                    "Amount Invoice",
                    "",
                    "Incoming Payment",
                    
                    //11-15
                    "Amount Incoming Payment",
                    "",
                    "Voucher Request",
                    "Amount Voucher Request",
                    "",
                    //16-20
                    "Voucher",
                    "Amount Voucher",
                    "",
                    "Journal",
                    "",
                    //21
                    "Warehouse DIKA",
                    "Warehouse ERP",
                    "Warehouse Category",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 150,  150, 150, 120,  
                    
                    //6-10
                    20,150, 120, 20, 150,  
                    //11-15
                    180,20, 150, 150, 20,  
                    
                    //16-19
                    150,120, 20, 150, 20,

                    //21
                    120, 120, 120
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 8, 11, 14, 17 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            //Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9 }, false);
            Sm.GrdColButton(Grd1, new int[] { 6, 9, 12, 15, 18, 20});
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 21,22,23 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where X.DocDt Between @DocDt1 And @DocDt2 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.ReceiptNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt ;",
                        new string[]
                        {
                            //0
                            "DocDt",
                            //1-5
                            "Receiptno", "ChannelName", "DOctDocno", "DOCtAMt", "SIDOcNo",   
                            //6-10
                            "SIAMt", "IPDocNo", "IPAMt", "VRDocno","VRAMt", 
                            //11-15
                            "VCdocno", "VCAmt","JournalDocNo", "WhsCode", "WhsName", 
                            //16
                            "WhsCtname"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5); 
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 16);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 8, 11, 14, 17 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e) //  6, 9, 12, 15, 18, 20
        {
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmSalesInvoice3(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
            if (e.ColIndex == 12 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmIncomingPayment(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 13).Length != 0)
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }
            if (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }
            if (e.ColIndex == 20 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                //var f = new FrmJournal(mMenuCode);  5, 8, 11, 14, 17
                //f.Tag = "***";
                //f.WindowState = FormWindowState.Normal;
                //f.StartPosition = FormStartPosition.CenterScreen;
                //f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                //f.ShowDialog();

                var f = new FrmRptIntDikaDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 16));
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                //f.mvs = Sm.GetGrdStr(Grd1, e.RowIndex, 13);
                f.ShowDialog();
            }

        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }


        #endregion

        #endregion
    }
}
