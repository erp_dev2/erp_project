﻿#region Update
/*
    17/11/2020 [TKG/PHT] bug saat save (parameter @EntCode)
    12/01/2021 [IBL/PHT] menambah inputan COA Inter Office berdasarkan parameter IsProfitCenterUseInterOffice
    04/03/2021 [HAR/PHT] menambah validasi saat generate proficenter
    02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
    02/03/2022 [TKG/PHT] tambah level
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmProfitCenterFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProfitCenter mFrmParent;

        #endregion

        #region Constructor

        public FrmProfitCenterFind(FrmProfitCenter FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Profit Center Code", 
                        "Profit Center Name",
                        "Parent",
                        "Entity",
                        "Level",
                        
                        //6-10  
                        "Created By",
                        "Created Date",
                        "Created Time", 
                        "Last Updated By",
                        "Last Updated Date", 

                        //11
                        "Last Updated Time"
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 11);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 10 });
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtProfitCenterCode.Text, new string[] { "A.ProfitCenterCode", "A.ProfitCenterName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select A.ProfitCenterCode, A.ProfitCenterName, B.ProfitCenterName as Parent, C.EntName, " +
                        (mFrmParent.mIsProfitCenterLevelEnabled?"A.Level, ":"0 As Level, ") +
                        "A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  " +
                        "From TblProfitCenter A  " +
                        "Left Join TblProfitCenter B on A.Parent=B.ProfitCenterCode " +
                        "Left Join TblEntity C On A.EntCode = C.EntCode " +
                        Filter + " Order By A.ProfitCenterName",
                        new string[]
                        {
                            //0
                            "ProfitCenterCode",

                            //1-5
                            "ProfitCenterName",
                            "Parent",
                            "EntName",
                            "Level",                            
                            "CreateBy", 
                            
                            //6-8
                            "CreateDt", 
                            "LastUpBy",
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 8);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Profit Center");
        }

        private void TxtProfitCenterCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
