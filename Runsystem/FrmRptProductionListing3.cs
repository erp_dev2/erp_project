﻿#region Update
/*
    18/03/2020 [DITA/MMM] new reporting 
 *  05/05/2020 [HAR/MMM] BUG : nilai workcenter hanya muncul di sequence terakhir
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionListing3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        internal bool mIsProductionListingShowColorRow = false ;
        private List<WorkCenterDocName> lWCDocName = null;
        private int mFirstColumnForWhs = -1;
        private int mColCount = 14;

        #endregion

        #region Constructor

        public FrmRptProductionListing3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsProductionListingShowColorRow = Sm.GetParameterBoo("IsProductionListingShowColorRow");
        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DocDt, A.ItCode, C.ItName, ");
            SQL.AppendLine("A.PlannedQty, C.PlanningUomCode, ");
            SQL.AppendLine("A.PlannedQty*C.PlanningUomCodeConvert12 As PlannedQty2, ");
            SQL.AppendLine("C.PlanningUomCode2, B.Remark ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select T1.DocNo, T3.ItCode, Sum(T3.Qty) As PlannedQty ");
            SQL.AppendLine("    From TblPPHdr T1 ");
            SQL.AppendLine("    Inner Join TblPPDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblProductionOrderHdr T3 On T2.ProductionOrderDocNo=T3.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.DocNo, T3.ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblPPHdr B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = mColCount;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Planned#",
                        "",
                        "Date",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Item's Name",
                        "Planned" + Environment.NewLine + "(1)",
                        "UoM"+ Environment.NewLine + "(1)",
                        "%"+ Environment.NewLine + "(1)",
                        "Planned"+ Environment.NewLine + "(2)",

                        //11-13
                        "UoM"+ Environment.NewLine + "(2)",
                        "%"+ Environment.NewLine + "(2)",
                        "Plan's Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 20, 80, 80, 20, 
                        
                        //6-10
                        250, 100, 80, 80,100,

                        //11-13
                        80, 80, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 10 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 9, 12 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, false);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "C.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By B.DocDt, B.DocNo Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "ItCode", "ItName", "PlannedQty","PlanningUomCode",
                           
                            //6-8
                             "PlannedQty2", "PlanningUomCode2", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Grd.Cells[Row, 9].Value = 0m;               
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Grd.Cells[Row, 12].Value = 0m;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                           
                          
                           
                        }, true, false, false, false
                    );
                lWCDocName = new List<WorkCenterDocName>();
                SetDocNameToColumn();
                ProcessQty();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 4));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 4));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetDocNameToColumn()
        {
            lWCDocName.Clear();
            Grd1.Cols.Count = mColCount; // biar kolom nya di restore ke default kolom nya SetGrd()
            var LatestColumn = Grd1.Cols.Count - 1;
            var mDocNo = string.Empty;
            var cm = new MySqlCommand();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (mDocNo.Length > 0) mDocNo += ",";
                mDocNo += Sm.GetGrdStr(Grd1, Row, 1);
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                var WCColumn = new StringBuilder();

                WCColumn.AppendLine("Select Distinct A.DocNo, A.DocName ");
                WCColumn.AppendLine("From TblWorkCenterHdr A ");
                WCColumn.AppendLine("Inner Join TblShopFloorControlHdr B ON A.DocNo = B.WorkCenterDocNo ");
                WCColumn.AppendLine("Inner Join TblPPHdr C On C.DocNo = B.PPDocNo ");
                WCColumn.AppendLine("Where Find_In_Set(C.DocNo, @mDocNo) ");

                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = WCColumn.ToString();
                Sm.CmParam<String>(ref cm, "@mDocNo", mDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocName", "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LatestColumn += 1;
                        if (mFirstColumnForWhs == -1) mFirstColumnForWhs = LatestColumn;
                        lWCDocName.Add(new WorkCenterDocName()
                        {
                            DocName = Sm.DrStr(dr, c[0]),
                            DocNo = Sm.DrStr(dr, c[1]),
                            No = LatestColumn
                        });
                        Grd1.Cols.Count += 1;
                        Grd1.Header.Cells[0, LatestColumn].Value = Sm.DrStr(dr, c[0]) + Environment.NewLine + "(1)";
                        Grd1.Header.Cells[0, LatestColumn].TextAlign = iGContentAlignment.MiddleCenter;
                        Grd1.Cols[LatestColumn].Width = 200;
                        Sm.GrdFormatDec(Grd1, new int[] { LatestColumn }, 0);
                    }
                }
                dr.Close();
            }

            if (lWCDocName.Count > 0)
            {
                foreach(var x in lWCDocName)
                {
                    Grd1.Cols.Count += 1;
                    Grd1.Header.Cells[0, Grd1.Cols.Count - 1].Value = x.DocName + Environment.NewLine + "(2)";
                    Grd1.Header.Cells[0, Grd1.Cols.Count - 1].TextAlign = iGContentAlignment.MiddleCenter;
                    Grd1.Cols[Grd1.Cols.Count - 1].Width = 200;
                    Sm.GrdFormatDec(Grd1, new int[] { Grd1.Cols.Count - 1 }, 0);
                }
            }
        }

        private void ProcessQty()
        {
            if (lWCDocName.Count > 0)
            {
                for (int Col = mFirstColumnForWhs; Col < Grd1.Cols.Count; Col++)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Grd1.Cells[Row, Col].Value = 0m;
                }
                var l = new List<WorkCenterQty>();
                ProcessQty1(ref l);
                if (l.Count > 0) ProcessQty2(ref l, ref lWCDocName);

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (mIsProductionListingShowColorRow)
                    {
                        if (decimal.Parse(Sm.GetGrdStr(Grd1, Row, 12)) <= 0)
                        {
                            Grd1.Rows[Row].BackColor = Color.Red;
                        }
                        else if (decimal.Parse(Sm.GetGrdStr(Grd1, Row, 12)) >= 100)
                        {
                            Grd1.Rows[Row].BackColor = Color.Green;
                        }
                        else
                        {
                            Grd1.Rows[Row].BackColor = Color.Yellow;
                        }
                    }
                }

            }
        }

        private void ProcessQty1(ref List<WorkCenterQty> l)
        {
            var cm = new MySqlCommand();
            string PPDocNo = string.Empty;
            string ItCode = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (PPDocNo.Length > 0) PPDocNo += ",";
                    PPDocNo += Sm.GetGrdStr(Grd1, i, 1);

                    if (ItCode.Length > 0) ItCode += ",";
                    ItCode += Sm.GetGrdStr(Grd1, i, 4);
                }
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                var WCQty = new StringBuilder();

                WCQty.AppendLine("Select T4.DocNo WCDocNo, Sum(T2.Qty) As Qty, Sum(T2.Qty)*T5.PlanningUomCodeConvert12 As Qty2, T3.DocNo PPDocNo, T2.ItCode  ");
                WCQty.AppendLine("From TblShopFloorControlHdr T1 ");
                WCQty.AppendLine("Inner Join TblShopFloorControlDtl T2 On T1.DocNo=T2.DocNo ");
                WCQty.AppendLine("    And Find_In_Set(T1.PPDocNo, @PPDocNo) ");
                //WCQty.AppendLine("    And Find_In_Set(T2.ItCode, @ItCode) ");
                WCQty.AppendLine("Inner Join TblPPHdr T3 On T1.PPDocNo=T3.DocNo ");
                WCQty.AppendLine("Inner Join TblWorkCenterHdr T4 On T4.DocNo=T1.WorkCenterDocNo ");
                WCQty.AppendLine("Inner Join TblItem T5 On T2.ItCode=T5.ItCode ");
                WCQty.AppendLine("Inner Join TblItemCategory T6 On T5.ItCtCode=T6.ItCtCode ");
                WCQty.AppendLine("Where T1.CancelInd='N'  And T6.WastedInd = 'N' ");
                WCQty.AppendLine("Group By T4.DocNo, T3.DocNo, T2.ItCode  ");

                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = WCQty.ToString();
                Sm.CmParam<String>(ref cm, "@PPDocNo", PPDocNo);
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                //Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCDocNo", "Qty", "Qty2", "PPDocNo", "ItCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WorkCenterQty()
                        {
                            No = GetDocNameColumn(Sm.DrStr(dr, c[0])),
                            Qty = Sm.DrDec(dr, c[1]),
                            Qty2 = Sm.DrDec(dr, c[2]),
                            PPDocNo = Sm.DrStr(dr, c[3]),
                            ItCode = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessQty2(ref List<WorkCenterQty> l, ref List<WorkCenterDocName> lWCDocName)
        {
            int WCCount = lWCDocName.Count();
            foreach (var x in l)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (x.PPDocNo == Sm.GetGrdStr(Grd1, i, 1) )//&& x.ItCode == Sm.GetGrdStr(Grd1, i, 4))
                    {
                        Grd1.Cells[i, x.No].Value = x.Qty;
                        Grd1.Cells[i, x.No+WCCount].Value = x.Qty2;
                        if (Sm.GetGrdDec(Grd1, i, 7) != 0m) Grd1.Cells[i, 9].Value = (x.Qty / Sm.GetGrdDec(Grd1, i, 7)) * 100;
                        if (Sm.GetGrdDec(Grd1, i, 10) != 0m) Grd1.Cells[i, 12].Value = (x.Qty2 / Sm.GetGrdDec(Grd1, i, 10)) * 100;

                        break;
                    }
                }
            }
           
        }

        private int GetDocNameColumn(string WCDocNo)
        {
            foreach (var x in lWCDocName.Where(x => string.Compare(x.DocNo, WCDocNo) == 0))
                return x.No;
            return -1;
        }

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planned document");
        }

        #endregion
        
        #endregion

        #region Class

        private class WorkCenterDocName
        {
            public string DocName {get; set;}
            public string DocNo {get; set;}
            public int No {get; set;}
        }

        private class WorkCenterQty
        {
            public string WCDocNo { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public string PPDocNo { get; set; }
            public string ItCode { get; set; }
            public int No { get; set; }
        }

        #endregion

    }
}
