﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmTimbanganAD4329Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmTimbanganAD4329 mFrmParent;
        private string mSQL = string.Empty;
        private decimal LoadingQueueHideDataAfterXDays = 0m;

        #endregion

        #region Constructor

        public FrmTimbanganAD4329Find(FrmTimbanganAD4329 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                Sm.SetLue2(
                    ref LueStatus,
                    "Select 'I' As Col1, 'Timbangan ke-1' As Col2 " +
                    "Union All " +
                    "Select 'O' As Col1, 'Timbangan ke-2' As Col2 ",
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
                Sm.SetLue(LueStatus, "I");
                SetLoadingQueueHideDataAfterXDays();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLoadingQueueHideDataAfterXDays()
        {
            string Param = Sm.GetParameter("LoadingQueueHideDataAfterXDays");
            if (Param.Length != 0) LoadingQueueHideDataAfterXDays = decimal.Parse(Param);

        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, Case When A.Status='I' Then 'In' When A.Status='O' then 'Out' ");
            SQL.AppendLine("When A.Status='C' then 'Cancelled' end As StatusDesc, A.DocDateBf, ");
            SQL.AppendLine("Case When A.Status='I' Then 'Timbangan ke-1' When A.Status='O' then 'Timbangan ke-2' When A.Status='C' then 'Cancelled' end As QueueType,  ");
            SQL.AppendLine("D.TTName, A.LicenceNo, A.DrvName, C.ItName, B.LoadAreaName, A.WeightBf, ");
            SQL.AppendLine("Concat(Right(Left(A.DocDateBf, 8), 2), '/', Right(Left(A.DocDateBf, 6), 2), ");
            SQL.AppendLine("'/', Left(A.DocDateBf, 4), ' ', Left(A.DocTimeBf, 2), ':', Right(A.DocTimeBf, 2)) As WeightBfDt, ");
            SQL.AppendLine("A.WeightAf, ");
            SQL.AppendLine("Concat(Right(Left(A.DocDateAf, 8), 2), '/', Right(Left(A.DocDateAf, 6), 2), ");
            SQL.AppendLine("'/', Left(A.DocDateAf, 4), ' ', Left(A.DocTimeAf, 2), ':', Right(A.DocTimeAf, 2)) As WeightAfDt, ");
            SQL.AppendLine("A.Status, A.Netto, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblLoadingQueue A ");
            SQL.AppendLine("Left Join (Select OptCode As LoadArea, OptDesc As LoadAreaName from tblOption where OptCat='LoadingArea') B On A.LoadArea=B.LoadArea ");
            SQL.AppendLine("Left Join (Select OptCode As ItCat, OptDesc As ItName from tblOption where OptCat='LoadingItemCat') C On A.ItCat=C.ItCat ");
            SQL.AppendLine("Left Join TblTransportType D On A.TTCode=D.TTCode ");
            SQL.AppendLine("Where (A.Status<>'I' ");
            SQL.AppendLine("Or (A.Status='I' ");
            SQL.AppendLine("    And ");
            SQL.AppendLine("    Concat(Left(A.CreateDt, 4), '-', Substring(A.CreateDt, 5, 2), '-', Substring(A.CreateDt, 7, 2)) ");
            SQL.AppendLine("    >Date_Add(curdate(),Interval -1*@LoadingQueueHideDataAfterXDays Day) ");
            SQL.AppendLine(")) ");

            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Nomor Antrian", 
                        "Status", 
                        "Status"+Environment.NewLine+"Antrian",   
                        "Tipe"+Environment.NewLine+"Transportasi",   
                        "Nomor"+Environment.NewLine+"Polisi",   
                        
                        //6-10
                        "Pengemudi",   
                        "Jenis"+Environment.NewLine+"Antrian",   
                        "Area"+Environment.NewLine+"Bongkar/Muat",   
                        "Berat"+Environment.NewLine+"Sebelum Bongkar",   
                        "Tanggal/Waktu"+Environment.NewLine+"Sebelum Bongkar",  
                        
                        //11-15
                        "Berat"+Environment.NewLine+"Setelah Bongkar",   
                        "Tanggal/Waktu"+Environment.NewLine+"Setelah Bongkar",   
                        "Berat Bersih",   
                        "Dibuat"+Environment.NewLine+"Oleh", 
                        "Tanggal"+Environment.NewLine+"Buat", 

                        //16-19
                        "Waktu"+Environment.NewLine+"Buat", 
                        "Diubah"+Environment.NewLine+"Oleh", 
                        "Tanggal"+Environment.NewLine+"Ubah", 
                        "Waktu"+Environment.NewLine+"Ubah"
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 9, 11, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 12, 14, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 10, 12, 14, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<Decimal>(ref cm, "@LoadingQueueHideDataAfterXDays", LoadingQueueHideDataAfterXDays);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDateBf");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "A.Status", true);
                Sm.FilterStr(ref Filter, ref cm, TxtLicenceNo.Text, "A.LicenceNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt, A.DocNo ",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "StatusDesc", "QueueType", "TTName", "LicenceNo", "DrvName", 
                            
                            //6-10
                            "ItName", "LoadAreaName", "WeightBf", "WeightBfDt", "WeightAf", 
                            
                            //11-15
                            "WeightAfDt", "Netto", "CreateBy", "CreateDt", "LastUpBy",  
                            
                            //16-17
                            "LastUpDt", "Status"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            if (Sm.GetGrdStr(Grd1, Row, 2) == "Cancelled")
                                Grd1.Rows[Row].ForeColor = Color.Red;
                            else
                                if (Sm.GetGrdStr(Grd1, Row, 2) == "In")
                                    Grd1.Rows[Row].ForeColor = Color.DarkBlue;
                                else
                                    Grd1.Rows[Row].ForeColor = Color.Black;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 16);
                        }, true, false, false, true 
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Antrian");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Tanggal Antrian");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void TxtLicenceNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLicenceNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Polisi");
        }

        #endregion

        #endregion
    }
}
