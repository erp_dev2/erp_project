﻿#region Update
/*
    12/09/2017 [TKG] tambah site
    25/11/2020 [IBL/PHT] tambah kolom level, division. Tambah filter grade dan level
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPPS : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool
            mIsSiteMandatory = false,
            mIsRptPPSShowDivision = false;

        #endregion

        #region Constructor

        public FrmRptPPS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueGrdLvlCode(ref LueGradeCode);
                SetLueLevel(ref LueLvlCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsRptPPSShowDivision = Sm.GetParameterBoo("IsRptPPSShowDivision");
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine(" Select A.EmpCode, B.EmpCodeOld, B.EmpName, A.ProcessInd, A.StartDt, A.EndDt, C.Deptname, D.PosName, ");
            SQL.AppendLine(" E.GrdLvlname, F.PgName, G.OptDesc As EmpStat, H.OptDesc As EmpSys, I.OptDesc As EmpPay, J.SiteName, ");
            SQL.AppendLine(" K.LevelName, L.DivisionName ");
            SQL.AppendLine(" From TblEmployeePPs A ");
            SQL.AppendLine(" Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine(" Left Join TblDepartment C On A.DeptCode = C.DeptCode ");
            SQL.AppendLine(" Left Join TblPosition D On A.PosCode = D.PosCode ");
            SQL.AppendLine(" Left Join TblGradelevelHdr E On A.GrdlvlCode = E.GrdLvlCode ");
            SQL.AppendLine(" Left Join TblPayrollGrphdr F On A.PgCode = F.PgCode ");
            SQL.AppendLine(" Left Join TblOption G On A.EmploymentStatus = G.OptCode And G.OptCat = 'EmploymentStatus' ");
            SQL.AppendLine(" Left Join TblOption H On A.SystemType = H.OptCode And H.OptCat = 'EmpSystemType' ");
            SQL.AppendLine(" Left Join TblOption I On A.PayrunPeriod = I.OptCode And I.OptCat = 'PayrunPeriod'  ");
            SQL.AppendLine(" Left Join TblSite J On A.SiteCode=J.SiteCode ");
            SQL.AppendLine(" Left Join TblLevelHdr K On A.LevelCode = K.LevelCode ");
            SQL.AppendLine(" Left Join TblDivision L On B.DivisionCode = L.DivisionCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Employee's Code",
                        "Old Code",
                        "",
                        "Employee",
                        "Start Date",

                        //6-10
                        "End Date",
                        "Department",
                        "Site",
                        "Level",
                        "Division",

                        //11-15
                        "Position",
                        "Grade",
                        "Payroll"+Environment.NewLine+"Group",
                        "Employment"+Environment.NewLine+"Status",
                        "System"+Environment.NewLine+"Type",

                        //16
                        "Payrun"+Environment.NewLine+"Period",
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 100, 20, 150, 100,  
                        
                        //6-10
                        100, 150, 180, 70, 180,

                        //11-15
                        150, 150, 150, 150, 150,

                        //16
                        150,
                    }
                );
            Sm.GrdColButton(Grd1, new int[] {3});
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6});
            Sm.GrdColInvisible(Grd1, new int[] { 1,2,3 }, false);
            if (!mIsSiteMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
            if(!mIsRptPPSShowDivision)
                Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = "";

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), new string[] { "A.DeptCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueGradeCode), new string[] { "E.GrdlvlCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLvlCode), new string[] { "K.LevelCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpName", "B.EmpCodeOld" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            
                            //1-5
                            "EmpCodeOld", "EmpName", "StartDt", "EndDt", "Deptname", 
                            
                            //6-10
                            "SiteName", "LevelName", "DivisionName", "PosName","GrdLvlname",
                            
                            //11-14
                            "PgName", "EmpStat", "EmpSys", "EmpPay", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method
        private void SetLueLevel(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select LevelCode As Col1, LevelName As Col2 ");
                SQL.AppendLine("From TblLevelHdr ");
                SQL.AppendLine("Order By LevelName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Grid Method
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmEmployee(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }
        }
        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueGradeCode_EditValueChanged(object sender, EventArgs e)
        {

            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkGradeCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLvlCode, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
            Sm.FilterSetLookUpEdit(this, sender, "Grade");
        }

        private void LueLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLvlCode, new Sm.RefreshLue1(SetLueLevel));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLvlCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Level");
        }

        #endregion
    }
}
