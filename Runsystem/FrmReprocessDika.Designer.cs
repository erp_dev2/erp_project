﻿namespace RunSystem
{
    partial class FrmReprocessDika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReprocessDika));
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.BtnShow = new DevExpress.XtraEditors.SimpleButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(784, 0);
            this.panel1.Size = new System.Drawing.Size(70, 409);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.Size = new System.Drawing.Size(70, 33);
            this.BtnProcess.Click += new System.EventHandler(this.BtnProcess_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.BtnShow);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Size = new System.Drawing.Size(784, 409);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 14;
            this.label1.Text = "Date";
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(53, 13);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(99, 20);
            this.DteDocDt1.TabIndex = 15;
            // 
            // BtnShow
            // 
            this.BtnShow.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnShow.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnShow.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnShow.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnShow.Appearance.Options.UseBackColor = true;
            this.BtnShow.Appearance.Options.UseFont = true;
            this.BtnShow.Appearance.Options.UseForeColor = true;
            this.BtnShow.Appearance.Options.UseTextOptions = true;
            this.BtnShow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnShow.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnShow.Image = ((System.Drawing.Image)(resources.GetObject("BtnShow.Image")));
            this.BtnShow.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnShow.Location = new System.Drawing.Point(158, 12);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(24, 21);
            this.BtnShow.TabIndex = 25;
            this.BtnShow.ToolTip = "Show Data";
            this.BtnShow.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnShow.ToolTipTitle = "Run System";
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Grd1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 50);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(784, 359);
            this.panel3.TabIndex = 26;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(784, 359);
            this.Grd1.TabIndex = 23;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            // 
            // FrmReprocessDika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 409);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmReprocessDika";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        public DevExpress.XtraEditors.SimpleButton BtnShow;
        private System.Windows.Forms.Panel panel3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
    }
}