﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmItPricingGroup : RunSystem.FrmBase5
    {
        #region Field

        internal string 
            mMenuCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        private string mSQL = string.Empty;
        private int ColInd = 0;

        #endregion

        #region Constructor

        public FrmItPricingGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void SetSQL()
        {
            string ItCtRMPActual = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual'");
            string ItCtRMPActual2 = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'ItCtRMPActual2'");
           
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("Select D.ItCode, D.ItName,  ");
			SQL.AppendLine("	  Concat(D.ItName, ' ( ', ");
            SQL.AppendLine("                Case D.ItCtCode  ");
            SQL.AppendLine("                    When '"+ItCtRMPActual+"' Then "); 
            SQL.AppendLine("                        Concat( ");
            SQL.AppendLine("                        'L: ', Convert(Format(D.Length, 2) Using utf8), ");
            SQL.AppendLine("                        ' D: ', Convert(Format(D.Diameter, 2) Using utf8) ");
            SQL.AppendLine("                        ) ");
            SQL.AppendLine("                    When '"+ItCtRMPActual2+"' Then ");   
            SQL.AppendLine("                        Concat( ");
            SQL.AppendLine("                        'L: ', Convert(Format(D.Length, 2) Using utf8) ,  ");
            SQL.AppendLine("                        ' W: ', Convert(Format(D.Width, 2) Using utf8), ");
            SQL.AppendLine("                        ', H: ', Convert(Format(D.Height, 2) Using utf8) ");
            SQL.AppendLine("                        ) ");
            SQL.AppendLine("                End, ");
            SQL.AppendLine("            ' )') As Item, E.PGName ");
            SQL.AppendLine("From TblItem D ");
            SQL.AppendLine("Inner Join TblPricingGroup E On D.PGCode = E.PGCode ");
            SQL.AppendLine("Where D.PGCode = @PGCode ");

            mSQL = SQL.ToString();
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetGrd();
            SetSQL();
            SetLueOldPricingGroupCode(ref LueOldPriceGroup);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Item Code",
                        "Item Name",
                        "Item Detail Name",
                        "Pricing Group"
                    },
                    new int[] 
                    {
                        //0
                        20,

                        //
                        80, 250, 350, 200
                    }

                );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItCode, LueNewPriceGroup, LueOldPriceGroup,
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsLueEmpty(LueOldPriceGroup, "Old Pricing Group")) return;
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LueOldPriceGroup));

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName", "D.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "",
                        new string[] { "ItCode", "ItName", "Item", "PGName" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = ChkAutoChoose.Checked = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        }, true, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
               Sm.FocusGrd(Grd1, 0, 1);
               Cursor.Current = Cursors.WaitCursor;
            }
        }

        override protected void SaveData()
        {
            if (IsInsertedDataNotValid()  || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 0) == true)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(UpdateItemPricingGroup(Row));
                    ColInd = Row;
                }
            Sm.ExecCommands(cml);

            ShowData();
            FocusGrid(ColInd);
        }

        private void FocusGrid(int Col)
        {
            Sm.FocusGrd(Grd1, Col, 0);
            Cursor.Current = Cursors.WaitCursor;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueOldPriceGroup, "Old Pricing Group") ||
                Sm.IsLueEmpty(LueNewPriceGroup, "New Pricing Group") ||
                IsGrdEmpty();
        }

        private MySqlCommand UpdateItemPricingGroup(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblItem SET PGCode = @PGCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() Where ItCode = @ItCode  "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LueNewPriceGroup));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@LastUpBy", Gv.CurrentUserCode);
            return cm;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to select at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsChooseData()
        {
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 0))
                    {
                        if (IsChoose == true) IsChoose = false;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
                return true;
            }

            //if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
           return false;
        }

        #region setlue


        public static void SetLueNewPricingGroupCode(ref LookUpEdit Lue, string PGCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, PGName As Col2 From TblPricingGroup " +
                "Where PGCode NOT In (Select PgCode From TblPricingGroup Where PgCode = '" + PGCode + "') Order By PGName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueOldPricingGroupCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, PGName As Col2 From TblPricingGroup " +
                " Order By PGName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueItCtCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory " +
                "Where ItCtCode = (Select ParValue From Tblparameter Where ParCode = 'ItCtRMPActual') " +
                "Or ItCtCode = (Select ParValue From Tblparameter Where ParCode = 'ItCtRMPActual2') Order By ItCtName ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event 
        private void LueOldPriceGroup_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOldPriceGroup, new Sm.RefreshLue1(SetLueOldPricingGroupCode));
            ClearGrd();
            SetLueNewPricingGroupCode(ref LueNewPriceGroup, Sm.GetLue(LueOldPriceGroup));
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueNewPriceGroup_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueCCt, new Sm.RefreshLue2(SetLueCCtCode), Sm.GetLue(LueCC));

        }
        #endregion

       

    }
}
