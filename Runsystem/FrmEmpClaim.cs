﻿#region Update
/*
    16/06/2017 [ARI] tambah detail
    20/10/2017 [TKG] ubah perhitungan budget dan balance
    04/10/2018 [HAR] validasi budget berdasarkan parameter
    05/04/2019 [TKG] tambah health facility
    05/02/2020 [WED/SRN] Budget Claim lihat period, berdasarkan parameter IsEmpClaimUsePeriod
    09/03/2020 [VIN/SIER] upload multiattachment
    03/11/2020 [IBL/PHT] membuka edit pada amount header berdasarkan parameter IsEmpClaimRequestEditable
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    22/02/2021 [HAR/PHT] tambah validasi approval berdasarkan departemnt, site group approval
    21/04/2021 [ICA/PHT] mengubah lokasi upload file ke folder Claim-Request 
    16/06/2021 [VIN/PHT] Bug Attach File  
    18/06/2021 [RDA/PHT] tambah filter multi level code untuk Claim Request
    15/07/2021 [ICA/PHT] Auto save journal Claim Request saat dokumen diapprove.
    22/07/2021 [WED/PHT] tambah Budget berdasarkan parameter IsEmpClaimRequestUseBudget
    05/08/2021 [ICA/PHT] menambah cost center di journal berdasarkan parameter IsJournalCostCenterEnabled
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
    01/07/2022 [MYA/PHT] Claim Request yang membentuk VR, kolom PIC dan Dept nya ambil dari employee dan debt claim request (desktop)
    01/08/2022 [MYA/PHT] FEEDBACK : PIC saat disave jadi hilang
    23/08/2022 [RDA/PRODUCT] tambah tab approval information
    18/10/2022 [RDA/PHT] validasi monthly journal closing
    25/10/2022 [RDA/PHT] validasi monthly journal closing melihat profit center code dari employee code yang dipilih
    14/03/2023 [DITA/PHT] tambah field voucher#
    15/03/2023 [WED/PHT] rubah format penomoran journal berdasarkan parameter JournalDocNoFormat
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;


#endregion

namespace RunSystem
{
    public partial class FrmEmpClaim : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mDocTitle = string.Empty,
            mDeptCode = string.Empty,
            mSiteCode = string.Empty
            ;
        internal bool mIsFilterByDeptHR = false;
        private string mMainCurCode = string.Empty, mJournalDocNoFormat = string.Empty;
        internal FrmEmpClaimFind FrmFind;
        internal bool mIsEmpClaimValidateBudget = false, mIsEmpClaimRequestUseBudget = false,
            mIsVRClaimUsePIC = false;
        private bool
            mIsEmpClaimUsePeriod = false,
            mIsEmpClaimRequestEditable = false,
            mIsApprovalByDept = false,
            mIsApprovalBySite = false,
            mIsApprovalByDAG = false,
            mIsApprovalClaimRequestByLevelCode = false,
            mIsAutoJournalActived = false,
            mIsEmpClaimRequestJournalEnabled = false,
            mIsJournalCostCenterEnabled = false
            ;

        internal string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmEmpClaim(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {

            if (this.Text.Length == 0) this.Text = "Employee's Claim";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            if (mIsEmpClaimUsePeriod) LblBudget.Text = "Period Budget";

            TcEmpClaim.SelectedTab = TpBudget;
            TcEmpClaim.SelectedTab = TpGeneral;
            if (!mIsEmpClaimRequestUseBudget) TcEmpClaim.TabPages.Remove(TpBudget);

            SetGrd();
            SetFormControl(mState.View);
            SetLueEmployeeClaimDocType(ref LueDocType);
            SetLueEmployeeClaimDocInd(ref LueDocInd);
            LueDocType.Visible = false;
            LueDocInd.Visible = false;

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DNo", 
                        
                        //1-5
                        "Document Type Code",
                        "Type",
                        "Document#",
                        "Amount",
                        "Indicator Code", 
                        
                        //6-8
                        "Indicator",
                        "Tax Invoice"+Environment.NewLine+"Date",
                        "Health Facility"
                    },
                    new int[]
                    { 
                        //0
                        0, 

                        //1-5
                        0, 200, 150, 100, 0, 
                        
                        //6-8
                        150, 100, 250
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 5 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "Checked By",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4 });
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueClaimCode, TxtAmt,
                        MeeRemark, TxtFile, TxtFile2, TxtFile3, LueBCCode
                    }, true);
                    BtnEmpCode.Enabled = false;
                    Grd1.ReadOnly = true;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, LueClaimCode, TxtAmt, MeeRemark, LueBCCode }, false);
                    BtnEmpCode.Enabled = true;
                    Grd1.ReadOnly = false;
                    BtnFile.Enabled = true;
                    BtnDownload.Enabled = true;
                    BtnFile2.Enabled = true;
                    BtnDownload2.Enabled = true;
                    BtnFile3.Enabled = true;
                    BtnDownload3.Enabled = true;
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    if (mIsEmpClaimRequestEditable)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAmt }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mDeptCode = string.Empty;
            mSiteCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, TxtEmpCode,
                TxtEmpName, TxtDeptCode, TxtPosCode, LueClaimCode, MeeRemark,
                TxtFile, TxtFile2, TxtFile3, LueBCCode
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { TxtBudgetAmt, TxtOtherAmt, TxtAmt, TxtBalance, TxtRemainingBudget }, 0);
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
        }

        //private void ClearData2()
        //{
        //    Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
        //    { TxtEmpCode, TxtEmpName, TxtDeptCode, TxtPosCode, LueClaimCode });
        //    Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtBudgetAmt, TxtOtherAmt, TxtAmt, TxtBalance }, 0);
        //}

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4 });

            Grd2.Rows.Clear();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpClaimFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueClaimCode(ref LueClaimCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        private void BtnFile_Click_1(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click_1(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click_1(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click_1(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);

                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click_1(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click_1(object sender, EventArgs e)
        {

            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient + "/claimrequest");
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpClaim", "TblEmpClaimHdr");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpClaimHdr(DocNo, VoucherRequestDocNo));
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        cml.Add(SaveEmpClaimDtl(DocNo, r));
            }

            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));
            cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, DocNo));

            if (mIsAutoJournalActived && mIsEmpClaimRequestJournalEnabled && !IsNeedApproval(VoucherRequestDocNo))
            {
                string profitCenterCode = Sm.GetValue("Select B.ProfitCenterCode From TblEmployee A Inner Join TblCostCenter B On A.DeptCode = B.DeptCode And B.ProfitCenterCode Is Not Null Where A.EmpCode = @Param;", TxtEmpCode.Text);
                string code1 = Sm.GetCode1ForJournalDocNo("FrmEmpClaim", string.Empty, string.Empty, mJournalDocNoFormat);
                cml.Add(SaveJournal(DocNo, code1, profitCenterCode));
            }

            Sm.ExecCommands(cml);
            if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee's Code", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) ||
                //Sm.IsLueEmpty(LueClaimCode, "Claim") ||
                //Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsBudgetCategoryInvalid() ||
                IsAmountNotValid() ||
                IsUploadFileNotValid() ||
                IsUploadFileNotValid2() ||
                IsUploadFileNotValid3() ||
                IsBalanceNotValid() ||
                IsGrdValueNotValid();
        }

        private bool IsBudgetCategoryInvalid()
        {
            if (!mIsEmpClaimRequestUseBudget) return false;

            if (Sm.GetLue(LueBCCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Budget Category is empty.");
                TcEmpClaim.SelectedTab = TpBudget;
                LueBCCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsAmountNotValid()
        {
            if (!mIsEmpClaimRequestUseBudget) return false;

            ComputeRemainingBudget();
            if (Decimal.Parse(TxtAmt.Text) > Decimal.Parse(TxtRemainingBudget.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Amount should not be greater than available budget.");
                TxtAmt.Focus();
                return true;
            }

            return false;
        }

        private bool IsBalanceNotValid()
        {
            ComputeBalance();
            if (TxtBalance.Text.Length > 0 && Decimal.Parse(TxtBalance.Text) < 0m && mIsEmpClaimValidateBudget)
            {
                Sm.StdMsg(mMsgType.Warning, "Balance should not be less than 0.00.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Document type is empty.")) return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpClaimHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmpClaimHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, ClaimCode, EmpCode, Amt, VoucherRequestDocNo, ");
            if (mIsEmpClaimRequestUseBudget)
                SQL.AppendLine("BCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @ClaimCode, @EmpCode, @Amt, @VoucherRequestDocNo, ");
            if (mIsEmpClaimRequestUseBudget)
                SQL.AppendLine("@BCCode, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ClaimCode", Sm.GetLue(LueClaimCode));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            if (mIsEmpClaimRequestUseBudget) Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
        //        type = string.Empty;

        //    var SQL = new StringBuilder();

        //    if (type == string.Empty)
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
        //    }
        //    else
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
        //    }
        //    return Sm.GetValue(SQL.ToString());
        //}

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string EmpClaimDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, ");
            if (mIsVRClaimUsePIC)
            {
                SQL.AppendLine("PIC, DeptCode,");
            }
            else
            {
                SQL.AppendLine("DeptCode,");
            }
            SQL.AppendLine("DocType, AcType, PaymentType, CurCode, Amt, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', ");
            if (mIsVRClaimUsePIC)
            {
                SQL.AppendLine("@PIC, @DeptCode,");
            }
            else
            {
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='VoucherRequestPayrollDeptCode'), ");
            }
            SQL.AppendLine("'08', 'C', 'C', @CurCode, @Amt, ");

            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @EmpClaimDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='EmpClaim' ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select Amt From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("), 0.00)) ");
            if (mIsApprovalClaimRequestByLevelCode)
            {
                SQL.AppendLine("And T.LevelCode Is Not Null ");
                SQL.AppendLine("And Find_in_set( ");
                SQL.AppendLine("( ");
                SQL.AppendLine(" Select Levelcode From tblemployee Where EmpCode=@EmpCode ");
                SQL.AppendLine(" ), T.LevelCode ");
                SQL.AppendLine(") ");
            }
            if (mIsApprovalByDept)
            {
                SQL.AppendLine("And T.DeptCode Is Not Null ");
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=@EmpCode ");
                SQL.AppendLine("    And DeptCode Is Not Null ");
                SQL.AppendLine("    ) ");
            }
            if (mIsApprovalBySite)
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And T.SiteCode In ( ");
                SQL.AppendLine("    Select SiteCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=@EmpCode ");
                SQL.AppendLine("    And SiteCode Is Not Null ");
                SQL.AppendLine("    ) ");
            }
            if (mIsApprovalByDAG)
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@EmpCode ");
                SQL.AppendLine("))) ");
            }

            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpClaim' ");
            SQL.AppendLine("    And DocNo=@EmpClaimDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblEmpClaimHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@EmpClaimDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpClaim' ");
            SQL.AppendLine("    And DocNo=@EmpClaimDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpClaimDocNo", EmpClaimDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@PIC", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo, string EmpClaimDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, '001', @Description, @Amt, @Remark, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mIsVRClaimUsePIC)
                Sm.CmParam<String>(ref cm, "@Description", String.Concat("Claim # : ", EmpClaimDocNo, " - ", TxtEmpCode.Text, " - ", TxtEmpName.Text));
            else
                Sm.CmParam<String>(ref cm, "@Description", String.Concat(TxtEmpName.Text, "'s ", "Claim."));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpClaimDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpClaimDtl(DocNo, DNo, DocType, DocNumber, Amt, DocInd, TaxInvDt, HealthFacility, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @DocType, @DocNumber, @Amt, @DocInd, @TaxInvDt, @HealthFacility, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@DocNumber", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DocInd", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParamDt(ref cm, "@TaxInvDt", Sm.GetGrdDate(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@HealthFacility", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();
            string CCCode = Sm.GetValue("Select CCCode From TblEmployee A Inner Join TblCostCenter B On A.DeptCode = B.DeptCode Where A.EmpCode = @Param Limit 1", TxtEmpCode.Text);

            SQL.AppendLine("Update TblEmpClaimHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Employees Claim Request : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@CCCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine("    SELECT B.AcNo, IFNULL(A.Amt, 0.00) DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    FROM tblempclaimhdr A ");
            SQL.AppendLine("    INNER JOIN tblclaim B ON A.ClaimCode = B.ClaimCode ");
            SQL.AppendLine("        AND B.ActInd = 'Y' ");
            SQL.AppendLine("        AND A.DocNo = @DocNo ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT B.ParValue, 0.00 DAmt, A.Amt AS CAmt ");
            SQL.AppendLine("    FROM tblempclaimhdr A ");
            SQL.AppendLine("    INNER JOIN tblparameter B On parcode = 'AcNoForEmpClaimRequest' AND parcode IS NOT null ");
            SQL.AppendLine("        AND A.DocNo = @DocNo ");
            SQL.AppendLine("    )Tbl ");
            SQL.AppendLine("    WHERE AcNo IS NOT null ");
            SQL.AppendLine("    GROUP BY AcNo ");
            SQL.AppendLine(")T; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            
            if (mJournalDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1)));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.Left(Sm.GetDte(DteDocDt), 8), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CCCode", mIsJournalCostCenterEnabled ? CCCode : string.Empty);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        #endregion

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpClaim());

            if (mIsAutoJournalActived && mIsEmpClaimRequestJournalEnabled)
            {
                string profitCenterCode = Sm.GetValue("Select B.ProfitCenterCode From TblEmployee A Inner Join TblCostCenter B On A.DeptCode = B.DeptCode And B.ProfitCenterCode Is Not Null Where A.EmpCode = @Param;", TxtEmpCode.Text);
                string code1 = Sm.GetCode1ForJournalDocNo("FrmEmpClaim", string.Empty, string.Empty, mJournalDocNoFormat);
                cml.Add(SaveJournal2(code1, profitCenterCode));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) ||
                (!mIsEmpClaimRequestEditable && IsDocumentNotCancelled()) ||
                IsAmtNotAllowedToEdit() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyProcessedToVoucher();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return
                Sm.IsDataExist(
                    "Select 1 From TblEmpClaimHdr Where CancelInd='Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled.");
        }

        private bool IsDataAlreadyProcessedToVoucher()
        {
            return Sm.IsDataExist(
                    "Select 1 " +
                    "From TblEmpClaimHdr A " +
                    "Inner Join TblVoucherHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo And B.CancelInd='N' " +
                    "Where A.DocNo=@Param;",
                    TxtDocNo.Text,
                    "This data already process to voucher request.");
        }

        private bool IsAmtNotAllowedToEdit()
        {
            if (!mIsEmpClaimRequestEditable) return false;
            decimal mInitAmt = 0m;
            decimal mCurrentAmt = 0m;

            mInitAmt = Decimal.Parse(Sm.GetValue("Select Amt From TblEmpClaimHdr Where DocNo = @Param; ", TxtDocNo.Text));
            mCurrentAmt = Decimal.Parse(TxtAmt.Text);

            if (mInitAmt != mCurrentAmt)
            {
                if ((TxtStatus.Text == "Approved") || TxtStatus.Text == "Cancelled")
                {
                    Sm.StdMsg(mMsgType.Warning, "You could only edit amount when it is outstanding.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand EditEmpClaim()
        {
            var SQL = new StringBuilder();
            if (mIsEmpClaimRequestEditable && MeeCancelReason.Text.Length == 0)
            {
                SQL.AppendLine("Update TblEmpClaimHdr Set ");
                SQL.AppendLine("    Amt = @Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

                SQL.AppendLine("Update TblVoucherRequestHdr Set ");
                SQL.AppendLine("    Amt = @Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo In (Select VoucherRequestDocNo From TblEmpClaimHdr Where DocNo = @DocNo); ");

                SQL.AppendLine("Update TblVoucherRequestDtl Set ");
                SQL.AppendLine("    Amt = @Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo In (Select VoucherRequestDocNo From TblEmpClaimHdr Where DocNo = @DocNo); ");
            }
            else
            {
                SQL.AppendLine("Update TblEmpClaimHdr Set ");
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

                SQL.AppendLine("Update TblVoucherRequestHdr Set ");
                SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where CancelInd='N' ");
                SQL.AppendLine("And DocNo In (Select VoucherRequestDocNo From TblEmpClaimHdr Where DocNo=@DocNo);");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(string code1, string profitCenterCode)
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblEmpClaimHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblEmpClaimHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAmt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblEmpClaimHdr Where DocNo=@DocNo And JournalDocNo Is Not Null);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (mJournalDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1)));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.Left(Sm.GetDte(DteDocDt), 8), 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty)));
            return cm;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpClaimHdr(DocNo);
                ShowEmpClaimDtl(DocNo);
                if (mIsEmpClaimRequestUseBudget) ComputeRemainingBudget();
                ComputeBalance();
                Sm.ShowDocApproval(DocNo, "EmpClaim", ref Grd2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpClaimHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, C.DeptName, D.PosName, ");
            SQL.AppendLine("A.ClaimCode, A.Amt, A.Remark, A.FileName, A.Filename2, A.FileName3, F.DocNo VoucherDocNo ");
            if (mIsEmpClaimRequestUseBudget)
                SQL.AppendLine(", A.BCCode, B.DeptCode, B.SiteCode ");
            else
                SQL.AppendLine(", Null As BCCode, Null As DeptCode, Null As SiteCode ");
            SQL.AppendLine("From TblEmpClaimHdr A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr E On A.VoucherRequestDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblVoucherHdr F On E.DocNo=F.VoucherRequestDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "StatusDesc", "EmpCode",

                        //6-10
                        "EmpName", "DeptName", "PosName", "ClaimCode", "Amt", 
                        
                        //11-15
                        "Remark", "FileName", "FileName2", "FileName3", "BCCode", 

                        //16-18
                        "DeptCode", "SiteCode", "VoucherDocNo"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                    TxtPosCode.EditValue = Sm.DrStr(dr, c[8]);
                    Sl.SetLueClaimCode(ref LueClaimCode, Sm.DrStr(dr, c[9]));
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[12]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[13]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[14]);
                    if (mIsEmpClaimRequestUseBudget)
                    {
                        SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[15]), Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[15]));
                        mDeptCode = Sm.DrStr(dr, c[16]);
                        mSiteCode = Sm.DrStr(dr, c[17]);
                    }
                    TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[18]);
                }, true
        );

        }

        private void ShowEmpClaimDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.DNo, A.DocType, B.OptDesc As DocTypeDesc, A.DocNumber, A.Amt, A.DocInd, C.OptDesc As DocIndDesc, A.TaxInvDt, A.HealthFacility ");
            SQLDtl.AppendLine("From TblEmpClaimDtl A ");
            SQLDtl.AppendLine("Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='EmployeeClaimDocType' ");
            SQLDtl.AppendLine("Left Join TblOption C On A.DocInd=C.OptCode And C.OptCat='EmployeeClaimDocInd' ");
            SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[]
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "DocType", "DocTypeDesc", "DocNumber", "Amt", "DocInd", 
                    
                    //6-8
                    "DocIndDesc", "TaxInvDt", "HealthFacility"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 2);
        }

        #endregion

        #region Additional Method

        private string GetProfitCenterCode()
        {
            var Value = TxtEmpCode.Text;
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "SELECT C.ProfitCenterCode " +
                    "FROM tblemployee A " +
                    "INNER JOIN tbldepartment B ON A.DeptCode=B.DeptCode " +
                    "INNER JOIN tblcostcenter C ON C.CCCode=B.DeptCode " +
                    "INNER JOIN tblprofitcenter D ON C.ProfitCenterCode=D.ProfitCenterCode " +
                    "WHERE D.ProfitCenterCode Is Not Null AND A.EmpCode=@Param; ",
                    Value);
        }

        private void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                AvailableBudget = Sm.ComputeAvailableBudget(
                    (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"),
                    Sm.GetDte(DteDocDt),
                    mSiteCode,
                    mDeptCode,
                    Sm.GetLue(LueBCCode)
                    );
                RequestedBudget = Decimal.Parse(TxtAmt.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

            TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget - RequestedBudget, 0);
        }

        internal void SetLueBCCode(ref LookUpEdit Lue, string BCCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode As Col1, BCName As Col2 From TblBudgetCategory ");
            if (BCCode.Length > 0)
                SQL.AppendLine("Where BCCode=@BCCode ");
            else
                SQL.AppendLine("Where DeptCode=@DeptCode And ActInd='Y' ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (BCCode.Length > 0) Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            if (DeptCode.Length > 0) Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BCCode.Length > 0) Sm.SetLue(Lue, BCCode);
        }

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mIsFilterByDeptHR = Sm.GetParameter("IsFilterByDeptHR") == "Y";
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsEmpClaimValidateBudget = Sm.GetParameterBoo("IsEmpClaimValidateBudget");
            mIsEmpClaimUsePeriod = Sm.GetParameterBoo("IsEmpClaimUsePeriod");
            mIsEmpClaimRequestEditable = Sm.GetParameterBoo("IsEmpClaimRequestEditable");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            if (mDocTitle == "PHT") mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient") + "/claim-request";
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsApprovalByDept = IsApprovalByDept();
            mIsApprovalBySite = IsApprovalBySite();
            mIsApprovalByDAG = IsApprovalByDAG();
            mIsApprovalClaimRequestByLevelCode = Sm.GetParameterBoo("IsApprovalClaimRequestByLevelCode");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsEmpClaimRequestJournalEnabled = Sm.GetParameterBoo("IsEmpClaimRequestJournalEnabled");
            mIsEmpClaimRequestUseBudget = Sm.GetParameterBoo("IsEmpClaimRequestUseBudget");
            mIsJournalCostCenterEnabled = Sm.GetParameterBoo("IsJournalCostCenterEnabled");
            mIsVRClaimUsePIC = Sm.GetParameterBoo("IsVRClaimUsePIC");
            mJournalDocNoFormat = Sm.GetParameter("JournalDocNoFormat");

            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
        }

        internal void ComputeBalance()
        {
            string
                EmpCode = TxtEmpCode.Text,
                ClaimCode = Sm.GetLue(LueClaimCode),
                Yr = Sm.GetDte(DteDocDt),
                DocDt = Sm.GetDte(DteDocDt),
                ClaimStartDt = string.Empty,
                ClaimEndDt = string.Empty;

            if (EmpCode.Length == 0 || ClaimCode.Length == 0 || Yr.Length == 0)
            {
                TxtBudgetAmt.EditValue = Sm.FormatNum(0m, 0);
                TxtOtherAmt.EditValue = Sm.FormatNum(0m, 0);
                TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                TxtBalance.EditValue = Sm.FormatNum(0m, 0);
                return;
            }

            string Value = string.Empty;
            decimal BudgetAmt = 0m, OtherAmt = 0m, Amt = 0m;

            if (Yr.Length >= 4) Yr = Sm.Left(Yr, 4);
            if (DocDt.Length >= 8) DocDt = Sm.Left(DocDt, 8);

            if (!mIsEmpClaimUsePeriod)
            {
                Value = Sm.GetValue(
                    "Select IfNull(Amt, 0.00) As Amt From TblEmployeeClaim " +
                    "Where EmpCode=@Param1 And ClaimCode=@Param2 And Yr=@Param3 Limit 1;",
                    EmpCode,
                    ClaimCode,
                    Yr);
            }
            else
            {
                Value = Sm.GetValue(
                    "Select IfNull(Amt, 0.00) As Amt From TblEmployeeClaim " +
                    "Where EmpCode=@Param1 And ClaimCode=@Param2 And ClaimStartDt <= @Param3 And ClaimEndDt >= @Param3 And ClaimStartDt Is Not Null Limit 1;",
                    EmpCode,
                    ClaimCode,
                    DocDt);

                ClaimStartDt = Sm.GetValue("Select ClaimStartDt From TblEmployeeClaim Where EmpCode = @Param1 And ClaimCode = @Param2 And ClaimStartDt Is Not Null And ClaimStartDt <= @Param3 And ClaimEndDt >= @Param3 Limit 1; ", EmpCode, ClaimCode, DocDt);
                ClaimEndDt = Sm.GetValue("Select ClaimEndDt From TblEmployeeClaim Where EmpCode = @Param1 And ClaimCode = @Param2 And ClaimEndDt Is Not Null And ClaimStartDt <= @Param3 And ClaimEndDt >= @Param3 Limit 1; ", EmpCode, ClaimCode, DocDt);
            }

            if (Value.Length != 0) BudgetAmt = decimal.Parse(Value);

            if (!mIsEmpClaimUsePeriod)
            {
                Value = Sm.GetValue(
                    "Select Sum(Amt) As Amt " +
                    "From TblEmpClaimHdr " +
                    "Where EmpCode=@Param1 And ClaimCode=@Param2 And Left(DocDt, 4)=@Param3 " +
                    "And Status In ('O', 'A') And CancelInd='N';",
                    EmpCode, ClaimCode, Yr);
            }
            else
            {
                if (ClaimStartDt.Length >= 8 && ClaimEndDt.Length >= 8)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Sum(Amt) As Amt ");
                    SQL.AppendLine("From TblEmpClaimHdr ");
                    SQL.AppendLine("Where EmpCode=@Param1 And ClaimCode=@Param2 And (DocDt Between Left(@Param3, 8) And Right(@Param3, 8)) ");
                    if (mIsEmpClaimRequestEditable && TxtDocNo.Text.Length > 0 && BtnSave.Enabled)
                        SQL.AppendLine("And DocNo != '" + TxtDocNo.Text + "' ");
                    SQL.AppendLine("And Status In ('O', 'A') And CancelInd='N'; ");

                    Value = Sm.GetValue(SQL.ToString(), EmpCode, ClaimCode, string.Concat(ClaimStartDt, ClaimEndDt));
                }
                else
                {
                    Value = "0";
                }
            }

            if (Value.Length != 0) OtherAmt = decimal.Parse(Value);

            if (TxtAmt.Text.Length != 0) Amt = decimal.Parse(TxtAmt.Text);
            if (TxtDocNo.Text.Length > 0 && !ChkCancelInd.Checked && !Sm.CompareStr(TxtStatus.Text, "Cancel") && !BtnSave.Enabled)
                OtherAmt -= Amt;

            TxtBudgetAmt.EditValue = Sm.FormatNum(BudgetAmt, 0);
            TxtOtherAmt.EditValue = Sm.FormatNum(OtherAmt, 0);
            TxtBalance.EditValue = Sm.FormatNum(BudgetAmt - OtherAmt - Amt, 0);

            if (mIsEmpClaimRequestUseBudget) ComputeRemainingBudget();
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "EmpClaimHdr", "EmpClaimDtl" };

            var l = new List<EmpClaimHdr>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d-%M-%y')As DocDt, E.ClaimName, A.EmpCode, B.EmpName, C.DeptName, D.PosName, ");
            SQL.AppendLine(" F.CurCode, A.Amt, G.OptDesc as PaymentType, A.Remark ");
            SQL.AppendLine(" From TblEmpClaimHdr A ");
            SQL.AppendLine(" Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine(" Left Join tbldepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine(" Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine(" Left Join TblClaim E On A.ClaimCode = E.ClaimCode ");
            SQL.AppendLine(" Left Join TblVoucherrequestHdr F On A.VoucherRequestDocNo=F.DocNo ");
            SQL.AppendLine(" Left Join TblOPtion G On G.OptCat='VoucherPaymentType' And G.OptCode=F.PaymentType ");
            SQL.AppendLine(" Where A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "CompanyLogo",
                         "DocNo", 
                       
                        
                         //6-10
                         "DocDt",
                         "ClaimName",
                         "EmpCode",
                         "EmpName",
                         "DeptName", 
                        
                          
                         //11-15
                         "PosName",
                         "Curcode",
                         "Amt",
                         "PaymentType",
                         "Remark"


                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpClaimHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),
                            CompanyFax = Sm.DrStr(dr, c[3]),
                            CompanyLogo = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),
                            DocDt = Sm.DrStr(dr, c[6]),
                            ClaimName = Sm.DrStr(dr, c[7]),
                            EmpCode = Sm.DrStr(dr, c[8]),
                            EmpName = Sm.DrStr(dr, c[9]),
                            DeptName = Sm.DrStr(dr, c[10]),
                            PosName = Sm.DrStr(dr, c[11]),
                            CurCode = Sm.DrStr(dr, c[12]),
                            Amt = Sm.DrDec(dr, c[13]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[13])),
                            PaymentType = Sm.DrStr(dr, c[14]),
                            Remark = Sm.DrStr(dr, c[15]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),

                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            Sm.PrintReport("EmployeeClaim", myLists, TableName, false);

        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex - 1));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private static void SetLueEmployeeClaimDocType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='EmployeeClaimDocType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueEmployeeClaimDocInd(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='EmployeeClaimDocInd' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsApprovalByDept()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpClaim' And DeptCode Is Not Null Limit 1;");
        }

        private bool IsApprovalBySite()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpClaim' And SiteCode Is Not Null Limit 1;");
        }

        private bool IsApprovalByDAG()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='EmpClaim' And DAGCode Is Not Null Limit 1;");

        }

        private bool IsNeedApproval(string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='EmpClaim' ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select Amt From TblVoucherRequestHdr ");
            SQL.AppendLine("    Where DocNo='" + VoucherRequestDocNo + "'");
            SQL.AppendLine("), 0.00)) ");
            if (mIsApprovalClaimRequestByLevelCode)
            {
                SQL.AppendLine("And T.LevelCode Is Not Null ");
                SQL.AppendLine("And Find_in_set( ");
                SQL.AppendLine("( ");
                SQL.AppendLine(" Select Levelcode From tblemployee Where EmpCode=@Param ");
                SQL.AppendLine(" ), T.LevelCode ");
                SQL.AppendLine(") ");
            }
            if (mIsApprovalByDept)
            {
                SQL.AppendLine("And T.DeptCode Is Not Null ");
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select DeptCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=@Param ");
                SQL.AppendLine("    And DeptCode Is Not Null ");
                SQL.AppendLine("    ) ");
            }
            if (mIsApprovalBySite)
            {
                SQL.AppendLine("And T.SiteCode Is Not Null ");
                SQL.AppendLine("And T.SiteCode In ( ");
                SQL.AppendLine("    Select SiteCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=@Param ");
                SQL.AppendLine("    And SiteCode Is Not Null ");
                SQL.AppendLine("    ) ");
            }
            if (mIsApprovalByDAG)
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@Param ");
                SQL.AppendLine("))) ");
            }

            SQL.AppendLine("; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtEmpCode.Text)) return true;

            return false;
        }

        #endregion

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateEmpClaimFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateEmpClaimFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateEmpClaimFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblEmpClaimHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblEmpClaimHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblEmpClaimHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        internal void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private MySqlCommand UpdateEmpClaimFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpClaimHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateEmpClaimFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpClaimHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateEmpClaimFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpClaimHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeBalance();
            }
        }

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpClaimDlg(this, Sm.GetDte(DteDocDt).Substring(0, 8)));
        }

        private void LueClaimCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueClaimCode, new Sm.RefreshLue2(Sl.SetLueClaimCode), string.Empty);
                ComputeBalance();
            }
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmt, 0);
                ComputeBalance();
            }
        }

        private void LueDocType_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueDocType.Visible = false;
        }

        private void LueDocType_Leave(object sender, EventArgs e)
        {
            if (LueDocType.Visible && fAccept && fCell.ColIndex == 2)
            {
                if (Sm.GetLue(LueDocType).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 1].Value =
                    Grd1.Cells[fCell.RowIndex, 2].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueDocType);
                    Grd1.Cells[fCell.RowIndex, 2].Value = LueDocType.GetColumnValue("Col2");
                }
            }
        }

        private void LueDocType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueEmployeeClaimDocType));
        }

        private void LueDocInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocInd, new Sm.RefreshLue1(SetLueEmployeeClaimDocInd));
        }

        private void LueDocInd_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDocInd_Leave(object sender, EventArgs e)
        {
            if (LueDocInd.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LueDocInd).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 5].Value =
                    Grd1.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueDocInd);
                    Grd1.Cells[fCell.RowIndex, 6].Value = LueDocInd.GetColumnValue("Col2");
                }
            }
        }

        private void LueDocInd_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) LueDocInd.Visible = false;
        }

        private void DteTaxInvDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteTaxInvDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteTaxInvDt, ref fCell, ref fAccept);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }
        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(SetLueBCCode), string.Empty, mDeptCode);
                ComputeRemainingBudget();
            }
        }

        #endregion

        #region Grid Event

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 3, 8 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4 }, e);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 2, 3, 4, 6, 7, 8 }, e.ColIndex))
            {
                if (e.ColIndex == 7) Sm.DteRequestEdit(Grd1, DteTaxInvDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 2) LueRequestEdit(Grd1, LueDocType, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) LueRequestEdit(Grd1, LueDocInd, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            }
        }

        #endregion

        #endregion

        #region Report Class

        private class EmpClaimHdr
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string ClaimName { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string PaymentType { get; set; }
            public string BankName { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
        }

        private class EmpClaimDtl
        {
            public string DNo { get; set; }
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        #endregion

    }
}
