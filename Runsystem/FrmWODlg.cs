﻿#region Update
/*
    30/05/2017 [ARI] Tambah Hours Meter
    18/07/2017 [WED] Tambah kolom kode TO, kode MaintenanceType dan kode SymptomProblem, di hide (untuk keperluan ambil kode ke Lue)
    18/07/2017 [WED] WO Request yg sudah Closed (WOStatus=C) tidak muncul di list nya
    24/07/2017 [HAR] Tambah Informasi technical Object dan create by 
    01/08/2017 [WED] Tambah informasi tanggal dokumen HM, untuk dibandingkan dengan tanggal transaksi WO
    08/08/2017 [HAR] Tambah parameter wor bisa dipilih sekali atau tidak
    15/08/2017 [TKG] Tambah filter TO dan location
    21/08/2017 [HAR] Tambah parameter DOWN DATE, clear grid parent saat ambil data 
    05/09/2017 [WED] tambah informasi TO Code ke header parent WO
    11/09/2017 [WED] tambah informasi Asset Name ke header parent WO
    18/09/2017 [HAR] tambah validasi WOR yang muncul yang sdh di approve
    22/01/2018 [TKG] WO request di-filter berdasarkan site
    31/01/2018 [WED] tambah copy Display Name ke Main Form WO
    02/12/2021 [SET/IOK] menambahkan kolom Equipment
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWODlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmWO mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmWODlg(FrmWO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                GetParameter();
                SetLueLocCode(ref LueLocCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Description",
                        "Type",

                        //6-10
                        "Symptom Problem",
                        "Hours Meter",
                        "Site",
                        "MtcType",
                        "SymProblem",

                        //11-15
                        "TO Code",
                        "TO Name",
                        "Description",
                        "Create By",
                        "Remark",

                        //16-17
                        "HMDocDt",
                        "Location",
                        "EquipmentCode"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 20, 80, 250, 120, 
                        
                        //6-10
                        180, 100, 150,  100, 150,

                        //11-15
                        100, 150, 200, 80, 300,

                        //16-18
                        0, 200, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 9, 10, 11, 12, 13, 16, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Description, A.MtcType, A.SymProblem, A.TOCode, A.EquipmentCode, ");
            SQL.AppendLine("B.OptDesc As MaintenanceTypeDesc, C.OptDesc As SymptomProblemDesc, G.HoursMeter, F.SiteName, ");
            SQL.AppendLine("H.Assetname As TOName, H.Displayname As TODesc, A.Createby, A.Remark, IfNull(G.DocDt, '') As HMDocDt, E.LocName ");
            SQL.AppendLine("From TblWOR A ");
            SQL.AppendLine("Left Join TblOption B On A.MtcType=B.OptCode And B.OptCat ='MaintenanceType' ");
            SQL.AppendLine("Left Join TblOption C On A.SymProblem=C.OptCode And C.OptCat ='SymptomProblem' ");
            SQL.AppendLine("Inner Join TblTOHdr D On A.ToCode = D.AssetCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblLocation E On D.LocCode = E.LocCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From tblGroupSite  ");
                SQL.AppendLine("        Where SiteCode=E.SiteCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("Inner Join TblSite F On E.SiteCode = F.SiteCode ");
            }
            else
            {
                SQL.AppendLine("Left Join TblLocation E On D.LocCode = E.LocCode ");
                SQL.AppendLine("Left Join TblSite F On E.SiteCode = F.SiteCode ");
            }
            SQL.AppendLine("Left Join TblHoursMeter G On A.ToCode=G.ToCode And D.HMDocNo=G.DocNo ");
            SQL.AppendLine("Inner Join TblAsset H On A.ToCOde = H.AssetCode ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' And A.WOStatus = 'O' And A.Status = 'A' ");
            if(!mFrmParent.mIsWORCanBeSelectedMorethanOnce)
            {
                SQL.AppendLine("And A.DocNo Not In (Select WORDocNo From TblWOHdr Where CancelInd = 'N' ) ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtTOCode.Text, new string[] { "A.TOCode", "H.AssetName", "H.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "D.LocCode", true);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[] 
                    { 
                         //0
                         "DocNo", 
                         
                         //1-5
                         "DocDt", 
                         "Description", 
                         "MaintenanceTypeDesc", 
                         "SymptomProblemDesc", 
                         "HoursMeter",

                         //6-10
                         "SiteName",
                         "MtcType",
                         "SymProblem",
                         "TOCode",
                         "TOName",

                         //11-15
                         "TODesc",
                         "CreateBy",
                         "Remark",
                         "HMDocDt",
                         "LocName",

                         //16
                         "EquipmentCode"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);

                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                Sm.ClearGrd(mFrmParent.Grd2, true);
                mFrmParent.HoursMeter = 0;
                int Row = Grd1.CurRow.Index;
                string WORDocNo = Sm.GetGrdStr(Grd1, Row, 1);

                mFrmParent.TxtWORDocNo.EditValue = WORDocNo;
                mFrmParent.MeeDescription.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                Sm.SetLue(mFrmParent.LueMaintenanceType, Sm.GetGrdStr(Grd1, Row, 9));
                Sm.SetLue(mFrmParent.LueSymptomProblem, Sm.GetGrdStr(Grd1, Row, 10));
                Sm.SetLue(mFrmParent.LueEquipmentCode, Sm.GetGrdStr(Grd1, Row, 18));
                mFrmParent.TxtHoursMeter.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 7), 0);
                mFrmParent.mInitHoursMeter = Sm.GetGrdDec(Grd1, Row, 7);
                mFrmParent.TxtTOCode.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.mAssetCode = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.TxtAssetName.EditValue = Sm.GetGrdStr(Grd1, Row, 12);
                mFrmParent.TxtSite.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                Sm.ClearGrd(mFrmParent.Grd1, true);
                mFrmParent.ShowWORHistory(WORDocNo);
                mFrmParent.HoursMeter = Sm.GetGrdDec(Grd1, Row, 7);
                mFrmParent.mInitDt = Sm.GetGrdStr(Grd1, Row, 16);
                mFrmParent.TxtDisplayName.EditValue = Sm.GetGrdStr(Grd1, Row, 13);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWOR(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmWOR(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Additional Method

        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        private void TxtTOCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTOCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Technical Object");
        }

        #endregion

        #endregion
    }
}
