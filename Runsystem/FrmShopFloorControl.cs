﻿#region Update
// 27/04/2017 [TKG] bug fixing pada saat memanggil aplikasi make to stock
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControl : RunSystem.FrmBase3
    { 
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mProductionOrderDNo = string.Empty,
            mDocType = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmShopFloorControlFind FrmFind;
        private int mNumberOfPlanningUomCode = 1; 
        
        #endregion

        #region Constructor

        public FrmShopFloorControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Shop Floor Control";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetNumberOfPlanningUomCode();
                Sl.SetLueProductionShiftCode(ref LueProductionShiftCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1
            
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Code",
                        "",
                        "Production Result",
                        "Batch Number",
                        
                        //6-9
                        "Quantity",
                        "UoM",
                        "Quantity 2",
                        "Uom 2"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 120, 20, 200, 250,  
                        
                        //6-9
                        80, 80, 80, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 8, 9 }, false);

            ShowPlanningUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 9;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Code",
                        "",
                        "Bill of Material",
                        "Type",

                        //6-8
                        "Type",
                        "Quantity",
                        "Value"
                    },
                     new int[] 
                    {
                        //0
                        0, 
                        
                        //1-5
                        20, 120, 20, 250, 0,  
                        
                        //6-8
                        100, 80, 100
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd2, new int[] { 7, 8 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 3, 5, 8 }, false);

            #endregion

        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9 }, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, ChkCancelInd, DteDocDt, LueProductionShiftCode, TxtProductionOrderDocNo, DteProdStartDt, 
                        DteProdEndDt, TxtSODocNo, TxtItCode, TxtItName, TxtProductionOrderQty, 
                        TxtProductionOrderPlanningUomCode, TxtProductionRoutingDocNo, TxtWorkCenterDocNo, TxtWorkCenterDocName, TxtLocation,
                        TxtCapacity, TxtUomCode, TxtSequence, TxtBomDocNo, TxtBomDocName, 
                        TxtBomQty, TxtBomPlanningUomCode, LueWhsCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9  });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 4, 5, 6, 7, 8 });
                    BtnProductionOrderDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueProductionShiftCode, LueWhsCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 6, 8});
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 7, 8 });
                    BtnProductionOrderDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd
                    }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mProductionOrderDNo = string.Empty;
            mDocType = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueProductionShiftCode, TxtProductionOrderDocNo, DteProdStartDt, 
                DteProdEndDt, TxtSODocNo, TxtItCode, TxtItName, TxtProductionOrderPlanningUomCode, TxtProductionRoutingDocNo, 
                TxtWorkCenterDocNo, TxtWorkCenterDocName, TxtLocation, TxtUomCode, 
                TxtSequence, TxtBomDocNo, TxtBomDocName, TxtBomPlanningUomCode, LueWhsCode, 
                MeeRemark
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtProductionOrderQty, TxtCapacity, TxtSequence, TxtBomQty }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 8 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmShopFloorControlFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmShopFloorControlDlg2(this));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 5, 6, 8 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 1) Sm.FormShowDialog(new FrmShopFloorControlDlg2(this));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 5 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6, 8 }, e);

            if (e.ColIndex == 6)
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 6, 8, 7, 9);

            if (e.ColIndex == 8)
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 8, 6, 9, 7);

            if (e.ColIndex == 6 && Sm.CompareGrdStr(Grd1, e.RowIndex, 7, Grd1, e.RowIndex, 9))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 9, Grd1, e.RowIndex, 6);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ShopFloorControl", "TblShopFloorControlHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveShopFloorControlHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveShopFloorControlDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveShopFloorControl2Dtl(DocNo, Row));
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProductionShiftCode, "Production's shift") ||
                Sm.IsTxtEmpty(TxtProductionOrderDocNo, "Production order#", false) ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsGrd2Empty() ||
                IsGrd2ValueNotValid() ||
                IsGrd2ExceedMaxRecords();
            ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;
                Msg =
                    "Production Result" + Environment.NewLine + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine;
                    
                if (Sm.GetGrdDec(Grd1, Row, 6) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Grd1.Cols[8].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 8) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrd2Empty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data.");
                return true;
            }
            return false;
        }

        private bool IsGrd2ExceedMaxRecords()
        {
            if (Grd2.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd2.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd2ValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd2, Row, 4, false, "Bill of Material Name is empty.") ||
                    Sm.IsGrdValueEmpty(Grd2, Row, 7, true, "Quantity should be greater than 0.")) 
                    return true;
            }

            return false;
        }

        private MySqlCommand SaveShopFloorControlHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblShopFloorControlHdr(DocNo, DocDt, ProcessInd, CancelInd, ProductionShiftCode, ProductionOrderDocNo, ProductionOrderDNo, WhsCode, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'O', 'N', @ProductionShiftCode, @ProductionOrderDocNo, @ProductionOrderDNo, @WhsCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProductionShiftCode", Sm.GetLue(LueProductionShiftCode));
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProductionOrderDNo", mProductionOrderDNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveShopFloorControlDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblShopFloorControlDtl(DocNo, DNo, ProcessInd, ItCode, BatchNo, Qty, Qty2, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'O', @ItCode, IfNull(@BatchNo, '-'), @Qty, @Qty2, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveShopFloorControl2Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblShopFloorControl2Dtl(DocNo, DNo, BomCode, BomType, Qty, Value, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @BomCode, @BomType, @Qty, @Value, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BomCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@BomType", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelShopFloorControlHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblShopFloorControlHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelShopFloorControlHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblShopFloorControlHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowShopFloorControlHdr(DocNo);
                ShowShopFloorControlDtl(DocNo);
                ShowShopFloorControl2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowShopFloorControlHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ProductionShiftCode, ");
            //SQL.AppendLine("A.ProductionOrderDocNo, A.ProductionOrderDNo, B.DocType, B.ProdStartDt, B.ProdEndDt, ");
            //SQL.AppendLine("Case B.DocType When '1' Then B.SODocNo Else B.MakeToStockDocNo End As SOMTSDocNo, ");
            //SQL.AppendLine("Case B.DocType When '1' Then J.ItCode Else K.ItCode End As ItCode, ");
            //SQL.AppendLine("Case B.DocType When '1' Then J.ItName Else K.ItName End As ItName, ");
            //SQL.AppendLine("Case B.DocType When '1' Then J.PlanningUomCode Else K.PlanningUomCode End As ProductionOrderPlanningUomCode, ");
            //SQL.AppendLine("B.Qty As ProductionOrderQty, B.ProductionRoutingDocNo, ");
            //SQL.AppendLine("F.WorkCenterDocNo, G.DocName As WorkCenterDocName, G.Location, G.Capacity, G.UomCode, F.Sequence,  ");
            //SQL.AppendLine("C.BomDocNo, H.DocName As BomDocName, H.Qty As BomQty, I.PlanningUomCode As BomPlanningUomCode, A.WhsCode, A.Remark ");
            //SQL.AppendLine("From TblShopFloorControlHdr A ");
            //SQL.AppendLine("Inner Join TblProductionOrderHdr B On A.ProductionOrderDocNo=B.DocNo ");
            //SQL.AppendLine("Inner Join TblProductionOrderDtl C On A.ProductionOrderDocNo=C.DocNo And A.ProductionOrderDNo=C.DNo ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T1.DocNo, T2.DNo, T4.ItCode ");
            //SQL.AppendLine("    From TblSOHdr T1 ");
            //SQL.AppendLine("    Inner Join TblSODtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    Left Join TblCtQtDtl T3 On T1.CtQtDocNo=T3.DocNo And T2.CtQtDNo=T3.DNo ");
            //SQL.AppendLine("    Left Join TblItemPriceDtl T4 On T3.ItemPriceDocNo=T4.DocNo And T3.ItemPriceDNo=T4.DNo ");
            //SQL.AppendLine(") D On B.SODocNo=D.DocNo And B.SODNo=D.DNo ");
            //SQL.AppendLine("Left Join TblMakeToStockDtl E On B.MakeToStockDocNo=E.DocNo And B.MakeToStockDNo=E.DNo ");
            //SQL.AppendLine("Inner Join TblProductionRoutingDtl F On B.ProductionRoutingDocNo=F.DocNo And C.ProductionRoutingDNo=F.DNo ");
            //SQL.AppendLine("Inner Join TblWorkCenterHdr G On F.WorkCenterDocNo=G.DocNo ");
            //SQL.AppendLine("Inner Join TblBomHdr H On C.BomDocNo=H.DocNo ");
            //SQL.AppendLine("Inner Join TblItem I On H.ItCode=I.ItCode ");
            //SQL.AppendLine("Left Join TblItem J On D.ItCode=J.ItCode ");
            //SQL.AppendLine("Left Join TblItem K On E.ItCode=K.ItCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ProductionShiftCode, ");
            SQL.AppendLine("A.ProductionOrderDocNo, A.ProductionOrderDNo, B.DocType, B.ProdStartDt, B.ProdEndDt, ");
            SQL.AppendLine("Case B.DocType When '1' Then B.SODocNo Else B.MakeToStockDocNo End As SOMTSDocNo, ");
            SQL.AppendLine("Case B.DocType When '1' Then H.ItCode Else I.ItCode End As ItCode, ");
            SQL.AppendLine("Case B.DocType When '1' Then H.ItName Else I.ItName End As ItName, ");
            SQL.AppendLine("Case B.DocType When '1' Then H.PlanningUomCode Else I.PlanningUomCode End As ProductionOrderPlanningUomCode, ");
            SQL.AppendLine("B.Qty As ProductionOrderQty, B.ProductionRoutingDocNo, ");
            SQL.AppendLine("F.WorkCenterDocNo, G.DocName As WorkCenterDocName, G.Location, G.Capacity, G.UomCode, F.Sequence,  ");
            SQL.AppendLine("C.BomDocNo, J.DocName As BomDocName, K.Qty As BomQty, L.PlanningUomCode As BomPlanningUomCode, A.WhsCode, A.Remark ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr B On A.ProductionOrderDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl C On A.ProductionOrderDocNo=C.DocNo And A.ProductionOrderDNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T4.ItCode ");
            SQL.AppendLine("    From TblSOHdr T1 ");
            SQL.AppendLine("    Inner Join TblSODtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Left Join TblCtQtDtl T3 On T1.CtQtDocNo=T3.DocNo And T2.CtQtDNo=T3.DNo ");
            SQL.AppendLine("    Left Join TblItemPriceDtl T4 On T3.ItemPriceDocNo=T4.DocNo And T3.ItemPriceDNo=T4.DNo ");
            SQL.AppendLine(") D On B.SODocNo=D.DocNo And B.SODNo=D.DNo ");
            SQL.AppendLine("Left Join TblMakeToStockDtl E On B.MakeToStockDocNo=E.DocNo And B.MakeToStockDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl F On B.ProductionRoutingDocNo=F.DocNo And C.ProductionRoutingDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr G On F.WorkCenterDocNo=G.DocNo ");
            SQL.AppendLine("Left Join TblItem H On D.ItCode=H.ItCode ");
            SQL.AppendLine("Left Join TblItem I On E.ItCode=I.ItCode ");
            SQL.AppendLine("Inner Join TblBomHdr J On C.BomDocNo=J.DocNo ");
            SQL.AppendLine("Inner Join TblBomDtl2 K On J.DocNo=K.DocNo And K.ItType='1' ");
            SQL.AppendLine("Inner Join TblItem L On K.ItCode=L.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "ProductionShiftCode", "ProductionOrderDocNo", "ProductionOrderDNo", 

                        //6-10
                        "ProdStartDt", "ProdEndDt", "SOMTSDocNo", "ItCode", "ItName", 
                        
                        //11-15
                        "ProductionOrderQty", "ProductionOrderPlanningUomCode", "ProductionRoutingDocNo", "WorkCenterDocNo", "WorkCenterDocName", 
                        
                        //16-20
                        "Location", "Capacity", "UomCode", "Sequence", "BomDocNo", 
                        
                        //21-25
                        "BomDocName", "BomQty", "BomPlanningUomCode", "WhsCode", "Remark",
 
                        //26
                        "DocType"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueProductionShiftCode, Sm.DrStr(dr, c[3]));
                        TxtProductionOrderDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        mProductionOrderDNo = Sm.DrStr(dr, c[5]);
                        Sm.SetDte(DteProdStartDt, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteProdEndDt, Sm.DrStr(dr, c[7]));
                        TxtSODocNo.EditValue = Sm.DrStr(dr, c[8]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[10]);
                        TxtProductionOrderQty.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[11]), 0);
                        TxtProductionOrderPlanningUomCode.EditValue = Sm.DrStr(dr, c[12]);
                        TxtProductionRoutingDocNo.EditValue = Sm.DrStr(dr, c[13]);
                        TxtWorkCenterDocNo.EditValue = Sm.DrStr(dr, c[14]);
                        TxtWorkCenterDocName.EditValue = Sm.DrStr(dr, c[15]);
                        TxtLocation.EditValue = Sm.DrStr(dr, c[16]);
                        TxtCapacity.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[18]);
                        TxtSequence.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 2);
                        TxtBomDocNo.EditValue = Sm.DrStr(dr, c[20]);
                        TxtBomDocName.EditValue = Sm.DrStr(dr, c[21]);
                        TxtBomQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                        TxtBomPlanningUomCode.EditValue = Sm.DrStr(dr, c[23]);
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[24]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[25]);
                        mDocType = Sm.DrStr(dr, c[26]);
                    }, true
                );
        }

        private void ShowShopFloorControlDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select A.DNo, A.ItCode, B.ItName, A.BatchNo, A.Qty, B.PlanningUomCode, A.Qty2, B.PlanningUomCode2 " +
                "From TblShopFloorControlDtl A " +
                "Inner Join TblItem B On A.ItCode=B.ItCode " +
                "Where A.DocNo=@DocNo Order By A.DNo;",
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItName", "BatchNo", "Qty", "PlanningUomCode",
 
                    //6-7
                    "Qty2", "PlanningUomCode2" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowShopFloorControl2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.BomCode, ");
            SQL.AppendLine("Case A.BomType When '1' Then C.ItName When '2' Then D.DocName When '3' Then E.EmpName End As BomName, ");
            SQL.AppendLine("A.BomType, B.OptDesc, A.Qty, A.Value  ");
            SQL.AppendLine("From TblShopFloorControl2Dtl A ");
            SQL.AppendLine("Left Join TblOption B On A.BomType=B.OptCode And B.OptCat='BomDocType' ");
            SQL.AppendLine("Left Join TblItem C On A.BomCode=C.ItCode And '1'=A.BomType ");
            SQL.AppendLine("Left Join TblFormulaHdr D On A.BomCode=D.DocNo And '2'=A.BomType ");
            SQL.AppendLine("Left Join TblEmployee E On A.BomCode=E.EmpCode And '3'=A.BomType ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-5
                   "BomCode", "BomName", "BomType", "OptDesc", "Qty", 
                   
                   //6
                   "Value"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ShowBomInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocCode, ");
            SQL.AppendLine("Case A.DocType When '1' Then C.ItName When '2' Then D.DocName When '3' Then E.EmpName End As DocName, ");
            SQL.AppendLine("A.DocType, B.OptDesc, A.Qty, A.Value  ");
            SQL.AppendLine("From TblBomDtl A ");
            SQL.AppendLine("Left Join TblOption B On A.DocType=B.OptCode And B.OptCat='BomDocType' ");
            SQL.AppendLine("Left Join TblItem C On A.DocCode=C.ItCode And '1'=A.DocType ");
            SQL.AppendLine("Left Join TblFormulaHdr D On A.DocCode=D.DocNo And '2'=A.DocType ");
            SQL.AppendLine("Left Join TblEmployee E On A.DocCode=E.EmpCode And '3'=A.DocType ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DocCode");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DocCode",

                           //1-5
                           "DocName", "DocType", "OptDesc", "Qty", "Value"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedBom()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count-1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 2) + Sm.GetGrdStr(Grd2, Row, 5) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        private void SetNumberOfPlanningUomCode()
        {
            string NumberOfPlanningUomCode = Sm.GetParameter("NumberOfPlanningUomCode");
            if (NumberOfPlanningUomCode.Length == 0)
                mNumberOfPlanningUomCode = 1;
            else
                mNumberOfPlanningUomCode = int.Parse(NumberOfPlanningUomCode);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueProductionShiftCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProductionShiftCode, new Sm.RefreshLue1(Sl.SetLueProductionShiftCode));
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        #endregion

        #region Button Event

        private void BtnProductionOrderDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmShopFloorControlDlg(this));
        }

        private void BtnSODocNo_Click(object sender, EventArgs e)
        {
            if (mDocType == "1")
            {
                if (!Sm.IsTxtEmpty(TxtSODocNo, "Sales order#", false))
                {
                    var f1 = new FrmSO2(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = TxtSODocNo.Text;
                    f1.ShowDialog();
                }
            }
            else
            {
                if (!Sm.IsTxtEmpty(TxtSODocNo, "MTS#", false))
                {
                    var MenuCodeForMakeToStockStandard = Sm.GetParameter("MenuCodeForMakeToStockStandard");
                    var MenuCodeForMakeToStockMaklon = Sm.GetParameter("MenuCodeForMakeToStockMaklon");
                    bool IsMTSStandard = Sm.IsDataExist(
                        "Select DocNo From TblMakeToStockHdr Where DocNo=@Param And BomDocNo Is Null;",
                        TxtSODocNo.Text);

                    var f1 = new FrmMakeToStock(mMenuCode);
                    if (IsMTSStandard)
                    {
                        if (MenuCodeForMakeToStockStandard.Length > 0)
                        {
                            f1.mMenuCode = MenuCodeForMakeToStockStandard;
                            f1.Tag = MenuCodeForMakeToStockStandard;
                        }
                    }
                    else
                    {
                        if (MenuCodeForMakeToStockMaklon.Length > 0)
                        {
                            f1.mMenuCode = MenuCodeForMakeToStockMaklon;
                            f1.Tag = MenuCodeForMakeToStockMaklon;
                        }
                    }
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = TxtSODocNo.Text;
                    f1.ShowDialog();
                }
            }
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtItCode, "Item", false))
            {
                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = TxtItCode.Text;
                f1.ShowDialog();
            }
        }

        private void BtnProductionRoutingDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtProductionRoutingDocNo, "Production routing's document number", false))
            {
                var f1 = new FrmProductionRouting(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtProductionRoutingDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void BtnWorkCenterDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtWorkCenterDocNo, "Work center's document number", false))
            {
                var f1 = new FrmWorkCenter(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtWorkCenterDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void BtnBomDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBomDocNo, "Bill of material's document number", false))
            {
                var f1 = new FrmBom2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtBomDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #region Grid Event

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 7, 8 }, e);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmShopFloorControlDlg3(this));
                }

                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
                {
                    e.DoDefault = false;
                    if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 5), "1"))
                    {
                        var f1 = new FrmItem(mMenuCode);
                        f1.Tag = mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                        f1.ShowDialog();
                    }

                    if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 5), "2"))
                    {
                        var f2 = new FrmFormula(mMenuCode);
                        f2.Tag = mMenuCode;
                        f2.WindowState = FormWindowState.Normal;
                        f2.StartPosition = FormStartPosition.CenterScreen;
                        f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                        f2.ShowDialog();
                    }
                }

                if (Sm.IsGrdColSelected(new int[] { 1, 7, 8 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1) Sm.FormShowDialog(new FrmShopFloorControlDlg3(this));
            
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd2, e.RowIndex, 5), "2"))
                {
                    var f2 = new FrmFormula(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion
    }
}
