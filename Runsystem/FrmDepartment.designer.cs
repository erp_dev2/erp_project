﻿namespace RunSystem
{
    partial class FrmDepartment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDepartment));
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.LueDeptGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkControlItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.LblGroup = new System.Windows.Forms.Label();
            this.LueDivisionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtDeptName = new DevExpress.XtraEditors.TextEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.BtnAcNo10 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeAcDesc10 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAcNo10 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.BtnAcNo9 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeAcDesc9 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAcNo9 = new DevExpress.XtraEditors.TextEdit();
            this.LblAcNo9 = new System.Windows.Forms.Label();
            this.BtnAcNo8 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeAcDesc8 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAcNo8 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnAcNo7 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeAcDesc7 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAcNo7 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo6 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnAcNo5 = new DevExpress.XtraEditors.SimpleButton();
            this.MeeAcDesc6 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtAcNo4 = new DevExpress.XtraEditors.TextEdit();
            this.TxtAcNo6 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtAcNo5 = new DevExpress.XtraEditors.TextEdit();
            this.MeeAcDesc5 = new DevExpress.XtraEditors.MemoExEdit();
            this.BtnAcNo4 = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtAcNo3 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.MeeAcDesc4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeAcDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.BtnAcNo3 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.MeeAcDesc3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeAcDesc2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnSite = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.LueBranchManager = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkControlItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBranchManager.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(764, 0);
            this.panel1.Size = new System.Drawing.Size(70, 499);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(764, 253);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BtnSite);
            this.panel3.Location = new System.Drawing.Point(0, 253);
            this.panel3.Size = new System.Drawing.Size(764, 246);
            this.panel3.Controls.SetChildIndex(this.BtnSite, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 477);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 24);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(764, 222);
            this.Grd1.TabIndex = 53;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(764, 253);
            this.Tc1.TabIndex = 11;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp3,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel7);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(758, 225);
            this.Tp1.Text = "General";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.LueBranchManager);
            this.panel7.Controls.Add(this.TxtDeptCode);
            this.panel7.Controls.Add(this.LueDeptGrpCode);
            this.panel7.Controls.Add(this.ChkControlItemInd);
            this.panel7.Controls.Add(this.ChkActInd);
            this.panel7.Controls.Add(this.TxtShortCode);
            this.panel7.Controls.Add(this.label50);
            this.panel7.Controls.Add(this.LblGroup);
            this.panel7.Controls.Add(this.LueDivisionCode);
            this.panel7.Controls.Add(this.TxtDeptName);
            this.panel7.Controls.Add(this.label67);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(758, 225);
            this.panel7.TabIndex = 44;
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(123, 9);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 16;
            this.TxtDeptCode.Size = new System.Drawing.Size(182, 20);
            this.TxtDeptCode.TabIndex = 13;
            // 
            // LueDeptGrpCode
            // 
            this.LueDeptGrpCode.EnterMoveNextControl = true;
            this.LueDeptGrpCode.Location = new System.Drawing.Point(123, 93);
            this.LueDeptGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptGrpCode.Name = "LueDeptGrpCode";
            this.LueDeptGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptGrpCode.Properties.DropDownRows = 30;
            this.LueDeptGrpCode.Properties.MaxLength = 16;
            this.LueDeptGrpCode.Properties.NullText = "[Empty]";
            this.LueDeptGrpCode.Properties.PopupWidth = 400;
            this.LueDeptGrpCode.Size = new System.Drawing.Size(400, 20);
            this.LueDeptGrpCode.TabIndex = 21;
            this.LueDeptGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // ChkControlItemInd
            // 
            this.ChkControlItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkControlItemInd.Location = new System.Drawing.Point(125, 153);
            this.ChkControlItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkControlItemInd.Name = "ChkControlItemInd";
            this.ChkControlItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkControlItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkControlItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkControlItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkControlItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkControlItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkControlItemInd.Properties.Caption = "Authority To Control Certain Items";
            this.ChkControlItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkControlItemInd.Size = new System.Drawing.Size(221, 22);
            this.ChkControlItemInd.TabIndex = 25;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(125, 136);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActInd.TabIndex = 24;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(123, 72);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 5;
            this.TxtShortCode.Size = new System.Drawing.Size(109, 20);
            this.TxtShortCode.TabIndex = 19;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(50, 75);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(69, 14);
            this.label50.TabIndex = 18;
            this.label50.Text = "Short Code";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblGroup
            // 
            this.LblGroup.AutoSize = true;
            this.LblGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGroup.ForeColor = System.Drawing.Color.Black;
            this.LblGroup.Location = new System.Drawing.Point(79, 96);
            this.LblGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblGroup.Name = "LblGroup";
            this.LblGroup.Size = new System.Drawing.Size(40, 14);
            this.LblGroup.TabIndex = 20;
            this.LblGroup.Text = "Group";
            this.LblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDivisionCode
            // 
            this.LueDivisionCode.EnterMoveNextControl = true;
            this.LueDivisionCode.Location = new System.Drawing.Point(123, 51);
            this.LueDivisionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDivisionCode.Name = "LueDivisionCode";
            this.LueDivisionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.Appearance.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDivisionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDivisionCode.Properties.DropDownRows = 30;
            this.LueDivisionCode.Properties.MaxLength = 16;
            this.LueDivisionCode.Properties.NullText = "[Empty]";
            this.LueDivisionCode.Properties.PopupWidth = 400;
            this.LueDivisionCode.Size = new System.Drawing.Size(400, 20);
            this.LueDivisionCode.TabIndex = 17;
            this.LueDivisionCode.ToolTip = "F4 : Show/hide list";
            this.LueDivisionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtDeptName
            // 
            this.TxtDeptName.EnterMoveNextControl = true;
            this.TxtDeptName.Location = new System.Drawing.Point(123, 30);
            this.TxtDeptName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptName.Name = "TxtDeptName";
            this.TxtDeptName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDeptName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptName.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptName.Properties.MaxLength = 255;
            this.TxtDeptName.Size = new System.Drawing.Size(400, 20);
            this.TxtDeptName.TabIndex = 15;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(73, 54);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(46, 14);
            this.label67.TabIndex = 16;
            this.label67.Text = "Division";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(14, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Department Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(11, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Department Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.BtnAcNo10);
            this.Tp3.Controls.Add(this.MeeAcDesc10);
            this.Tp3.Controls.Add(this.TxtAcNo10);
            this.Tp3.Controls.Add(this.label12);
            this.Tp3.Controls.Add(this.BtnAcNo9);
            this.Tp3.Controls.Add(this.MeeAcDesc9);
            this.Tp3.Controls.Add(this.TxtAcNo9);
            this.Tp3.Controls.Add(this.LblAcNo9);
            this.Tp3.Controls.Add(this.BtnAcNo8);
            this.Tp3.Controls.Add(this.MeeAcDesc8);
            this.Tp3.Controls.Add(this.TxtAcNo8);
            this.Tp3.Controls.Add(this.label10);
            this.Tp3.Controls.Add(this.BtnAcNo7);
            this.Tp3.Controls.Add(this.MeeAcDesc7);
            this.Tp3.Controls.Add(this.TxtAcNo7);
            this.Tp3.Controls.Add(this.label8);
            this.Tp3.Controls.Add(this.TxtAcNo);
            this.Tp3.Controls.Add(this.BtnAcNo6);
            this.Tp3.Controls.Add(this.BtnAcNo2);
            this.Tp3.Controls.Add(this.BtnAcNo5);
            this.Tp3.Controls.Add(this.MeeAcDesc6);
            this.Tp3.Controls.Add(this.TxtAcNo4);
            this.Tp3.Controls.Add(this.TxtAcNo6);
            this.Tp3.Controls.Add(this.label9);
            this.Tp3.Controls.Add(this.TxtAcNo5);
            this.Tp3.Controls.Add(this.MeeAcDesc5);
            this.Tp3.Controls.Add(this.BtnAcNo4);
            this.Tp3.Controls.Add(this.label11);
            this.Tp3.Controls.Add(this.TxtAcNo3);
            this.Tp3.Controls.Add(this.label6);
            this.Tp3.Controls.Add(this.label7);
            this.Tp3.Controls.Add(this.MeeAcDesc4);
            this.Tp3.Controls.Add(this.MeeAcDesc);
            this.Tp3.Controls.Add(this.BtnAcNo3);
            this.Tp3.Controls.Add(this.BtnAcNo);
            this.Tp3.Controls.Add(this.MeeAcDesc3);
            this.Tp3.Controls.Add(this.MeeAcDesc2);
            this.Tp3.Controls.Add(this.label3);
            this.Tp3.Controls.Add(this.TxtAcNo2);
            this.Tp3.Controls.Add(this.label4);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 225);
            this.Tp3.Text = "COA\'s Account";
            // 
            // BtnAcNo10
            // 
            this.BtnAcNo10.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo10.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo10.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo10.Appearance.Options.UseBackColor = true;
            this.BtnAcNo10.Appearance.Options.UseFont = true;
            this.BtnAcNo10.Appearance.Options.UseForeColor = true;
            this.BtnAcNo10.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo10.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo10.Image")));
            this.BtnAcNo10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo10.Location = new System.Drawing.Point(307, 196);
            this.BtnAcNo10.Name = "BtnAcNo10";
            this.BtnAcNo10.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo10.TabIndex = 50;
            this.BtnAcNo10.ToolTip = "Find COA";
            this.BtnAcNo10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo10.ToolTipTitle = "Run System";
            this.BtnAcNo10.Click += new System.EventHandler(this.BtnAcNo10_Click);
            // 
            // MeeAcDesc10
            // 
            this.MeeAcDesc10.EnterMoveNextControl = true;
            this.MeeAcDesc10.Location = new System.Drawing.Point(337, 196);
            this.MeeAcDesc10.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc10.Name = "MeeAcDesc10";
            this.MeeAcDesc10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc10.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc10.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc10.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc10.Properties.MaxLength = 500;
            this.MeeAcDesc10.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc10.Properties.ShowIcon = false;
            this.MeeAcDesc10.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc10.TabIndex = 51;
            this.MeeAcDesc10.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc10.ToolTipTitle = "Run System";
            // 
            // TxtAcNo10
            // 
            this.TxtAcNo10.EnterMoveNextControl = true;
            this.TxtAcNo10.Location = new System.Drawing.Point(128, 195);
            this.TxtAcNo10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo10.Name = "TxtAcNo10";
            this.TxtAcNo10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo10.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo10.Properties.MaxLength = 100;
            this.TxtAcNo10.Properties.ReadOnly = true;
            this.TxtAcNo10.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo10.TabIndex = 49;
            this.TxtAcNo10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo10_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(37, 198);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 14);
            this.label12.TabIndex = 48;
            this.label12.Text = "Social Security";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo9
            // 
            this.BtnAcNo9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo9.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo9.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo9.Appearance.Options.UseBackColor = true;
            this.BtnAcNo9.Appearance.Options.UseFont = true;
            this.BtnAcNo9.Appearance.Options.UseForeColor = true;
            this.BtnAcNo9.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo9.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo9.Image")));
            this.BtnAcNo9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo9.Location = new System.Drawing.Point(307, 175);
            this.BtnAcNo9.Name = "BtnAcNo9";
            this.BtnAcNo9.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo9.TabIndex = 46;
            this.BtnAcNo9.ToolTip = "Find COA";
            this.BtnAcNo9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo9.ToolTipTitle = "Run System";
            this.BtnAcNo9.Click += new System.EventHandler(this.BtnAcNo9_Click);
            // 
            // MeeAcDesc9
            // 
            this.MeeAcDesc9.EnterMoveNextControl = true;
            this.MeeAcDesc9.Location = new System.Drawing.Point(337, 175);
            this.MeeAcDesc9.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc9.Name = "MeeAcDesc9";
            this.MeeAcDesc9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc9.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc9.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc9.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc9.Properties.MaxLength = 500;
            this.MeeAcDesc9.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc9.Properties.ShowIcon = false;
            this.MeeAcDesc9.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc9.TabIndex = 47;
            this.MeeAcDesc9.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc9.ToolTipTitle = "Run System";
            // 
            // TxtAcNo9
            // 
            this.TxtAcNo9.EnterMoveNextControl = true;
            this.TxtAcNo9.Location = new System.Drawing.Point(128, 174);
            this.TxtAcNo9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo9.Name = "TxtAcNo9";
            this.TxtAcNo9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo9.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo9.Properties.MaxLength = 100;
            this.TxtAcNo9.Properties.ReadOnly = true;
            this.TxtAcNo9.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo9.TabIndex = 45;
            this.TxtAcNo9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo9_KeyDown);
            // 
            // LblAcNo9
            // 
            this.LblAcNo9.AutoSize = true;
            this.LblAcNo9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcNo9.ForeColor = System.Drawing.Color.Black;
            this.LblAcNo9.Location = new System.Drawing.Point(24, 176);
            this.LblAcNo9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcNo9.Name = "LblAcNo9";
            this.LblAcNo9.Size = new System.Drawing.Size(98, 14);
            this.LblAcNo9.TabIndex = 44;
            this.LblAcNo9.Text = "Cash Advance S.";
            this.LblAcNo9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo8
            // 
            this.BtnAcNo8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo8.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo8.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo8.Appearance.Options.UseBackColor = true;
            this.BtnAcNo8.Appearance.Options.UseFont = true;
            this.BtnAcNo8.Appearance.Options.UseForeColor = true;
            this.BtnAcNo8.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo8.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo8.Image")));
            this.BtnAcNo8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo8.Location = new System.Drawing.Point(307, 154);
            this.BtnAcNo8.Name = "BtnAcNo8";
            this.BtnAcNo8.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo8.TabIndex = 42;
            this.BtnAcNo8.ToolTip = "Find COA";
            this.BtnAcNo8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo8.ToolTipTitle = "Run System";
            this.BtnAcNo8.Click += new System.EventHandler(this.BtnAcNo8_Click);
            // 
            // MeeAcDesc8
            // 
            this.MeeAcDesc8.EnterMoveNextControl = true;
            this.MeeAcDesc8.Location = new System.Drawing.Point(337, 154);
            this.MeeAcDesc8.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc8.Name = "MeeAcDesc8";
            this.MeeAcDesc8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc8.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc8.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc8.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc8.Properties.MaxLength = 500;
            this.MeeAcDesc8.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc8.Properties.ShowIcon = false;
            this.MeeAcDesc8.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc8.TabIndex = 43;
            this.MeeAcDesc8.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc8.ToolTipTitle = "Run System";
            // 
            // TxtAcNo8
            // 
            this.TxtAcNo8.EnterMoveNextControl = true;
            this.TxtAcNo8.Location = new System.Drawing.Point(128, 153);
            this.TxtAcNo8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo8.Name = "TxtAcNo8";
            this.TxtAcNo8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo8.Properties.MaxLength = 100;
            this.TxtAcNo8.Properties.ReadOnly = true;
            this.TxtAcNo8.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo8.TabIndex = 41;
            this.TxtAcNo8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo8_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(64, 154);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 14);
            this.label10.TabIndex = 40;
            this.label10.Text = "Incentive";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo7
            // 
            this.BtnAcNo7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo7.Appearance.Options.UseBackColor = true;
            this.BtnAcNo7.Appearance.Options.UseFont = true;
            this.BtnAcNo7.Appearance.Options.UseForeColor = true;
            this.BtnAcNo7.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo7.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo7.Image")));
            this.BtnAcNo7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo7.Location = new System.Drawing.Point(307, 132);
            this.BtnAcNo7.Name = "BtnAcNo7";
            this.BtnAcNo7.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo7.TabIndex = 38;
            this.BtnAcNo7.ToolTip = "Find COA";
            this.BtnAcNo7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo7.ToolTipTitle = "Run System";
            this.BtnAcNo7.Click += new System.EventHandler(this.BtnAcNo7_Click);
            // 
            // MeeAcDesc7
            // 
            this.MeeAcDesc7.EnterMoveNextControl = true;
            this.MeeAcDesc7.Location = new System.Drawing.Point(337, 133);
            this.MeeAcDesc7.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc7.Name = "MeeAcDesc7";
            this.MeeAcDesc7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc7.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc7.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc7.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc7.Properties.MaxLength = 500;
            this.MeeAcDesc7.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc7.Properties.ShowIcon = false;
            this.MeeAcDesc7.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc7.TabIndex = 39;
            this.MeeAcDesc7.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc7.ToolTipTitle = "Run System";
            // 
            // TxtAcNo7
            // 
            this.TxtAcNo7.EnterMoveNextControl = true;
            this.TxtAcNo7.Location = new System.Drawing.Point(128, 132);
            this.TxtAcNo7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo7.Name = "TxtAcNo7";
            this.TxtAcNo7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo7.Properties.MaxLength = 100;
            this.TxtAcNo7.Properties.ReadOnly = true;
            this.TxtAcNo7.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo7.TabIndex = 37;
            this.TxtAcNo7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo7_KeyDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(7, 135);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 14);
            this.label8.TabIndex = 36;
            this.label8.Text = "Religious Holiday All.";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(128, 6);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 100;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo.TabIndex = 13;
            this.TxtAcNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo_KeyDown);
            // 
            // BtnAcNo6
            // 
            this.BtnAcNo6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo6.Appearance.Options.UseBackColor = true;
            this.BtnAcNo6.Appearance.Options.UseFont = true;
            this.BtnAcNo6.Appearance.Options.UseForeColor = true;
            this.BtnAcNo6.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo6.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo6.Image")));
            this.BtnAcNo6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo6.Location = new System.Drawing.Point(307, 111);
            this.BtnAcNo6.Name = "BtnAcNo6";
            this.BtnAcNo6.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo6.TabIndex = 34;
            this.BtnAcNo6.ToolTip = "Find COA";
            this.BtnAcNo6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo6.ToolTipTitle = "Run System";
            this.BtnAcNo6.Click += new System.EventHandler(this.BtnAcNo6_Click);
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(307, 27);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo2.TabIndex = 18;
            this.BtnAcNo2.ToolTip = "Find COA";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo2_Click);
            // 
            // BtnAcNo5
            // 
            this.BtnAcNo5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo5.Appearance.Options.UseBackColor = true;
            this.BtnAcNo5.Appearance.Options.UseFont = true;
            this.BtnAcNo5.Appearance.Options.UseForeColor = true;
            this.BtnAcNo5.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo5.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo5.Image")));
            this.BtnAcNo5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo5.Location = new System.Drawing.Point(307, 90);
            this.BtnAcNo5.Name = "BtnAcNo5";
            this.BtnAcNo5.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo5.TabIndex = 30;
            this.BtnAcNo5.ToolTip = "Find COA";
            this.BtnAcNo5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo5.ToolTipTitle = "Run System";
            this.BtnAcNo5.Click += new System.EventHandler(this.BtnAcNo5_Click);
            // 
            // MeeAcDesc6
            // 
            this.MeeAcDesc6.EnterMoveNextControl = true;
            this.MeeAcDesc6.Location = new System.Drawing.Point(337, 112);
            this.MeeAcDesc6.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc6.Name = "MeeAcDesc6";
            this.MeeAcDesc6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc6.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc6.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc6.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc6.Properties.MaxLength = 500;
            this.MeeAcDesc6.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc6.Properties.ShowIcon = false;
            this.MeeAcDesc6.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc6.TabIndex = 35;
            this.MeeAcDesc6.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc6.ToolTipTitle = "Run System";
            // 
            // TxtAcNo4
            // 
            this.TxtAcNo4.EnterMoveNextControl = true;
            this.TxtAcNo4.Location = new System.Drawing.Point(128, 69);
            this.TxtAcNo4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo4.Name = "TxtAcNo4";
            this.TxtAcNo4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo4.Properties.MaxLength = 100;
            this.TxtAcNo4.Properties.ReadOnly = true;
            this.TxtAcNo4.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo4.TabIndex = 25;
            this.TxtAcNo4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo4_KeyDown);
            // 
            // TxtAcNo6
            // 
            this.TxtAcNo6.EnterMoveNextControl = true;
            this.TxtAcNo6.Location = new System.Drawing.Point(128, 111);
            this.TxtAcNo6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo6.Name = "TxtAcNo6";
            this.TxtAcNo6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo6.Properties.MaxLength = 100;
            this.TxtAcNo6.Properties.ReadOnly = true;
            this.TxtAcNo6.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo6.TabIndex = 33;
            this.TxtAcNo6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo6_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(36, 114);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 14);
            this.label9.TabIndex = 32;
            this.label9.Text = "Tax Allowance";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo5
            // 
            this.TxtAcNo5.EnterMoveNextControl = true;
            this.TxtAcNo5.Location = new System.Drawing.Point(128, 90);
            this.TxtAcNo5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo5.Name = "TxtAcNo5";
            this.TxtAcNo5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo5.Properties.MaxLength = 100;
            this.TxtAcNo5.Properties.ReadOnly = true;
            this.TxtAcNo5.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo5.TabIndex = 29;
            this.TxtAcNo5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo5_KeyDown);
            // 
            // MeeAcDesc5
            // 
            this.MeeAcDesc5.EnterMoveNextControl = true;
            this.MeeAcDesc5.Location = new System.Drawing.Point(337, 91);
            this.MeeAcDesc5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc5.Name = "MeeAcDesc5";
            this.MeeAcDesc5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc5.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc5.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc5.Properties.MaxLength = 500;
            this.MeeAcDesc5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc5.Properties.ShowIcon = false;
            this.MeeAcDesc5.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc5.TabIndex = 31;
            this.MeeAcDesc5.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc5.ToolTipTitle = "Run System";
            // 
            // BtnAcNo4
            // 
            this.BtnAcNo4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo4.Appearance.Options.UseBackColor = true;
            this.BtnAcNo4.Appearance.Options.UseFont = true;
            this.BtnAcNo4.Appearance.Options.UseForeColor = true;
            this.BtnAcNo4.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo4.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo4.Image")));
            this.BtnAcNo4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo4.Location = new System.Drawing.Point(307, 69);
            this.BtnAcNo4.Name = "BtnAcNo4";
            this.BtnAcNo4.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo4.TabIndex = 26;
            this.BtnAcNo4.ToolTip = "Find COA";
            this.BtnAcNo4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo4.ToolTipTitle = "Run System";
            this.BtnAcNo4.Click += new System.EventHandler(this.BtnAcNo4_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(83, 93);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 14);
            this.label11.TabIndex = 28;
            this.label11.Text = "Leave";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo3
            // 
            this.TxtAcNo3.EnterMoveNextControl = true;
            this.TxtAcNo3.Location = new System.Drawing.Point(128, 48);
            this.TxtAcNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo3.Name = "TxtAcNo3";
            this.TxtAcNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo3.Properties.MaxLength = 100;
            this.TxtAcNo3.Properties.ReadOnly = true;
            this.TxtAcNo3.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo3.TabIndex = 21;
            this.TxtAcNo3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo3_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(58, 72);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 14);
            this.label6.TabIndex = 24;
            this.label6.Text = "Over Time";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(84, 51);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 14);
            this.label7.TabIndex = 20;
            this.label7.Text = "Salary";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAcDesc4
            // 
            this.MeeAcDesc4.EnterMoveNextControl = true;
            this.MeeAcDesc4.Location = new System.Drawing.Point(337, 70);
            this.MeeAcDesc4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc4.Name = "MeeAcDesc4";
            this.MeeAcDesc4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc4.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc4.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc4.Properties.MaxLength = 500;
            this.MeeAcDesc4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc4.Properties.ShowIcon = false;
            this.MeeAcDesc4.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc4.TabIndex = 27;
            this.MeeAcDesc4.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc4.ToolTipTitle = "Run System";
            // 
            // MeeAcDesc
            // 
            this.MeeAcDesc.EnterMoveNextControl = true;
            this.MeeAcDesc.Location = new System.Drawing.Point(337, 7);
            this.MeeAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc.Name = "MeeAcDesc";
            this.MeeAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc.Properties.MaxLength = 500;
            this.MeeAcDesc.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc.Properties.ShowIcon = false;
            this.MeeAcDesc.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc.TabIndex = 15;
            this.MeeAcDesc.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc.ToolTipTitle = "Run System";
            // 
            // BtnAcNo3
            // 
            this.BtnAcNo3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo3.Appearance.Options.UseBackColor = true;
            this.BtnAcNo3.Appearance.Options.UseFont = true;
            this.BtnAcNo3.Appearance.Options.UseForeColor = true;
            this.BtnAcNo3.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo3.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo3.Image")));
            this.BtnAcNo3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo3.Location = new System.Drawing.Point(307, 48);
            this.BtnAcNo3.Name = "BtnAcNo3";
            this.BtnAcNo3.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo3.TabIndex = 22;
            this.BtnAcNo3.ToolTip = "Find COA";
            this.BtnAcNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo3.ToolTipTitle = "Run System";
            this.BtnAcNo3.Click += new System.EventHandler(this.BtnAcNo3_Click);
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(307, 6);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(24, 21);
            this.BtnAcNo.TabIndex = 14;
            this.BtnAcNo.ToolTip = "Find COA";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // MeeAcDesc3
            // 
            this.MeeAcDesc3.EnterMoveNextControl = true;
            this.MeeAcDesc3.Location = new System.Drawing.Point(337, 49);
            this.MeeAcDesc3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc3.Name = "MeeAcDesc3";
            this.MeeAcDesc3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc3.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc3.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc3.Properties.MaxLength = 500;
            this.MeeAcDesc3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc3.Properties.ShowIcon = false;
            this.MeeAcDesc3.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc3.TabIndex = 23;
            this.MeeAcDesc3.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc3.ToolTipTitle = "Run System";
            // 
            // MeeAcDesc2
            // 
            this.MeeAcDesc2.EnterMoveNextControl = true;
            this.MeeAcDesc2.Location = new System.Drawing.Point(337, 28);
            this.MeeAcDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAcDesc2.Name = "MeeAcDesc2";
            this.MeeAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.MeeAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.MeeAcDesc2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAcDesc2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAcDesc2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAcDesc2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAcDesc2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAcDesc2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAcDesc2.Properties.MaxLength = 500;
            this.MeeAcDesc2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAcDesc2.Properties.ShowIcon = false;
            this.MeeAcDesc2.Size = new System.Drawing.Size(408, 20);
            this.MeeAcDesc2.TabIndex = 19;
            this.MeeAcDesc2.ToolTip = "F4 : Show/hide text";
            this.MeeAcDesc2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAcDesc2.ToolTipTitle = "Run System";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(33, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 14);
            this.label3.TabIndex = 12;
            this.label3.Text = "Travel Request";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(128, 27);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 100;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(174, 20);
            this.TxtAcNo2.TabIndex = 17;
            this.TxtAcNo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtAcNo2_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(58, 30);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Bonus Fee";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel5);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 225);
            this.Tp2.Text = "List of Department\'s Position";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.Grd2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 225);
            this.panel5.TabIndex = 22;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 225);
            this.Grd2.TabIndex = 12;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // BtnSite
            // 
            this.BtnSite.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSite.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnSite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSite.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.BtnSite.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnSite.Location = new System.Drawing.Point(0, 0);
            this.BtnSite.Name = "BtnSite";
            this.BtnSite.Size = new System.Drawing.Size(764, 24);
            this.BtnSite.TabIndex = 52;
            this.BtnSite.Text = "Site For Budget";
            this.BtnSite.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnSite.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(26, 117);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 14);
            this.label5.TabIndex = 22;
            this.label5.Text = "Branch Manager";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBranchManager
            // 
            this.LueBranchManager.EnterMoveNextControl = true;
            this.LueBranchManager.Location = new System.Drawing.Point(123, 114);
            this.LueBranchManager.Margin = new System.Windows.Forms.Padding(5);
            this.LueBranchManager.Name = "LueBranchManager";
            this.LueBranchManager.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchManager.Properties.Appearance.Options.UseFont = true;
            this.LueBranchManager.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchManager.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBranchManager.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchManager.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBranchManager.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchManager.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBranchManager.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBranchManager.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBranchManager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBranchManager.Properties.DropDownRows = 30;
            this.LueBranchManager.Properties.NullText = "[Empty]";
            this.LueBranchManager.Properties.PopupWidth = 305;
            this.LueBranchManager.Size = new System.Drawing.Size(302, 20);
            this.LueBranchManager.TabIndex = 23;
            this.LueBranchManager.ToolTip = "F4 : Show/hide list";
            this.LueBranchManager.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBranchManager.EditValueChanged += new System.EventHandler(this.LueBranchManager_EditValueChanged);
            // 
            // FrmDepartment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 499);
            this.Name = "FrmDepartment";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkControlItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.Tp3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBranchManager.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.LookUpEdit LueDeptGrpCode;
        private System.Windows.Forms.Label LblGroup;
        internal DevExpress.XtraEditors.TextEdit TxtDeptCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtDeptName;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.CheckEdit ChkControlItemInd;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.TextEdit TxtShortCode;
        private DevExpress.XtraEditors.LookUpEdit LueDivisionCode;
        private System.Windows.Forms.Label label67;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo7;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc7;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo7;
        private System.Windows.Forms.Label label8;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo6;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo5;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc6;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo4;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo6;
        private System.Windows.Forms.Label label9;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo5;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc5;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo4;
        private System.Windows.Forms.Label label11;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc4;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo3;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc3;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc2;
        private System.Windows.Forms.Label label3;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo8;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc8;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button BtnSite;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo9;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc9;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo9;
        private System.Windows.Forms.Label LblAcNo9;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo10;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAcDesc10;
        protected internal DevExpress.XtraEditors.TextEdit TxtAcNo10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueBranchManager;
    }
}