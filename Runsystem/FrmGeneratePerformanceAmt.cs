﻿#region Update
/*
    03/11/2020 [WED/PHT] new apps
    24/01/2022 [WED/PHT] link ke level dirubah yg tadinya dari Grade Level, menjadi langsung dari master Employee
    02/02/2022 [RDA/PHT] penambahan field end date (mandatory) agar end date di menu ESADA terisi otomatis ketika insert employee (tanpa param)
    14/04/2022 [TRI/PHT] bug ketika klik tombol cancel -> ok, muncul warning
    03/06/2022 [TRI/PHT] bug duplicate entry ketika dno sudah ada di TblEmployeeAllowanceDeduction, harusnya + 1
    07/06/2022 [TRI/PHT] bug menambah digit dno menjadi 5 digit, untuk import 16 ribu karyawan 
    17/06/2022 [ICA/PHT] mempercepat process save data
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmGeneratePerformanceAmt : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmGeneratePerformanceAmtFind FrmFind;
        private string
            mADCodeSMK = string.Empty,
            EmpCodes = string.Empty;

        #endregion

        #region Constructor

        public FrmGeneratePerformanceAmt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Generate Performance Amount";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                SetGrd();
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "DNo",

                    //1-5
                    "Employee's"+Environment.NewLine+"Code",
                    "Old Code",
                    "Employee's"+Environment.NewLine+"Name",
                    "Department",
                    "Position",

                    //6-10
                    "Site",
                    "Level Position",
                    "Performance Value",
                    "Quality Point",
                    "Total Value",

                    //11-12
                    "Performance Amount",
                    "SiteCode",
                    "DNo"
                },
                new int[]
                {
                    0,
                    100, 130, 250, 180, 200,
                    200, 180, 120, 120, 120,
                    120, 130, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 7, 12, 13 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, MeeCancelReason, ChkCancelInd, TxtAmt, TxtMth,
                        MeeRemark, DteEndDt
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnImportData.Enabled = false;
                    BtnProcessData.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, TxtAmt, TxtMth, MeeRemark, DteEndDt
                    }, false);
                    BtnImportData.Enabled = true;
                    BtnProcessData.Enabled = true;
                    ChkCancelInd.Checked = false;
                    Grd1.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, MeeRemark, DteEndDt
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtAmt, TxtMth, TxtAmtMth, TxtNoOfEmployee
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 10, 11 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGeneratePerformanceAmtFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteEndDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelGeneratePerformanceAmtHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblGeneratePerformanceAmtHdr " +
                "Where (CancelInd='Y') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand CancelGeneratePerformanceAmtHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblGeneratePerformanceAmtHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowGeneratePerformanceAmtHdr(DocNo);
                ShowGeneratePerformanceAmtDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowGeneratePerformanceAmtHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("A.Amt, A.Mth, A.AmtMth, A.NoOfEmployee, A.Remark, A.EndDt ");
            SQL.AppendLine("From TblGeneratePerformanceAmtHdr A ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "Amt", "Mth", 

                    //6-9
                    "AmtMth", "NoOfEmployee", "Remark", "EndDt"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                    TxtMth.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    TxtAmtMth.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    TxtNoOfEmployee.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[9]));
                }, true
            );
        }

        private void ShowGeneratePerformanceAmtDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpCodeOld, B.EmpName, C.DeptName, ");
            SQL.AppendLine("D.PosName, E.SiteName, G.LevelName, A.PerformanceValue, ");
            SQL.AppendLine("A.QualityPoint, A.TotalValue, A.PerformanceAmt, B.SiteCode ");
            SQL.AppendLine("From TblGeneratePerformanceAmtDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode = E.SiteCode ");
            //SQL.AppendLine("Left Join TblGradeLevelHdr F On B.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine("Left Join TblLevelHdr G On B.LevelCode=G.LevelCode ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                {
                    "DNo",
                    "EmpCode", "EmpCodeOld", "EmpName", "DeptName", "PosName",
                    "SiteName", "LevelName", "PerformanceValue", "QualityPoint", "TotalValue",
                    "PerformanceAmt", "SiteCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 10, 11 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GeneratePerformanceAmt", "TblGeneratePerformanceAmtHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveGeneratePerformanceAmtHdr(DocNo));
            if (Grd1.Rows.Count > 1)
            {

                cml.Add(SaveGeneratePerformanceAmtDtl(DocNo));
                cml.Add(SaveEmployeeAllowanceDeduction());
                //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                //{
                //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                //    {
                //        cml.Add(SaveGeneratePerformanceAmtDtl(DocNo, Row));
                //        cml.Add(SaveEmployeeAllowanceDeduction(Row));
                //    }
                //}
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsTxtEmpty(TxtMth, "Month", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                BtnImportData.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Employee is empty.")) return true;
            }

            return false;
        }

        private MySqlCommand SaveGeneratePerformanceAmtHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGeneratePerformanceAmtHdr(DocNo, DocDt, EndDt, CancelInd, Amt, Mth, AmtMth, NoOfEmployee, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @EndDt, 'N', @Amt, @Mth, @AmtMth, @NoOfEmployee, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Mth", Decimal.Parse(TxtMth.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtMth", Decimal.Parse(TxtAmtMth.Text));
            Sm.CmParam<Decimal>(ref cm, "@NoOfEmployee", Decimal.Parse(TxtNoOfEmployee.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            return cm;
        }

        #region SaveGPADtlOld dan SaveADOld
        //private MySqlCommand SaveGeneratePerformanceAmtDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblGeneratePerformanceAmtDtl(DocNo, DNo, EmpCode, PerformanceValue, QualityPoint, TotalValue, PerformanceAmt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @PerformanceValue, @QualityPoint, @TotalValue, @PerformanceAmt, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000" + (Row + 1).ToString(), 5));
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@PerformanceValue", Sm.GetGrdDec(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@QualityPoint", Sm.GetGrdDec(Grd1, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@TotalValue", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@PerformanceAmt", Sm.GetGrdDec(Grd1, Row, 11));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveEmployeeAllowanceDeduction(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    string DNo = string.Empty;
        //    bool IsDataExists = false;
        //    string EmpCode = Sm.GetGrdStr(Grd1, Row, 1);

        //    IsDataExists = Sm.IsDataExist("Select 1 From TblEmployeeAllowanceDeduction Where EmpCode = @Param; ", EmpCode);

        //    if (IsDataExists)
        //        DNo = Sm.GetValue("Select Max(DNo)+1 From TblEmployeeAllowanceDeduction Where EmpCode = @Param; ", EmpCode);
        //    else
        //        DNo = "001";

        //    if (DNo.Length == 0) DNo = "001";

        //    SQL.AppendLine("Delete From TblEmployeeAllowanceDeduction Where EmpCode = @EmpCode And ADCode = @ADCode; ");

        //    SQL.AppendLine("Insert Into TblEmployeeAllowanceDeduction ");
        //    SQL.AppendLine("(EmpCode, DNo, ADCode, StartDt, EndDt, SiteCode, Amt, CreateBy, CreateDt) "); // 
        //    SQL.AppendLine("Select @EmpCode, @DNo, @ADCode, @StartDt, @EndDt, A.SiteCode, @Amt, @CreateBy, CurrentDateTime() ");
        //    SQL.AppendLine("From TblEmployee A ");
        //    SQL.AppendLine("Where A.EmpCode = @EmpCode; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo, 3));
        //    Sm.CmParam<String>(ref cm, "@ADCode", mADCodeSMK);
        //    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 11));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

        //    return cm;
        //}
        #endregion

        private MySqlCommand SaveGeneratePerformanceAmtDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* GeneratePerformanceAmt - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblGeneratePerformanceAmtDtl(DocNo, DNo, EmpCode, PerformanceValue, QualityPoint, TotalValue, PerformanceAmt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@DocNo, @DNo_" + r.ToString() +
                        ", @EmpCode_" + r.ToString() +
                        ", @PerformanceValue_" + r.ToString() +
                        ", @QualityPoint_" + r.ToString() +
                        ", @TotalValue_" + r.ToString() +
                        ", @PerformanceAmt_" + r.ToString() +
                        ", @CreateBy, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00000" + (r + 1).ToString(), 5));
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@PerformanceValue_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@QualityPoint_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@TotalValue_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@PerformanceAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployeeAllowanceDeduction()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            bool IsDataExists = false;
            string DNo = string.Empty;

            SQL.AppendLine("/* EmployeeAllowanceDeductionAmt - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                IsDataExists = false;
                DNo = string.Empty;
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    //IsDataExists = Sm.IsDataExist("Select 1 From TblEmployeeAllowanceDeduction Where EmpCode = @Param; ", Sm.GetGrdStr(Grd1, r, 1));

                    //if (IsDataExists)
                    //    DNo = Sm.GetValue("Select Max(DNo)+1 From TblEmployeeAllowanceDeduction Where EmpCode = @Param; ", Sm.GetGrdStr(Grd1, r, 1));
                    //else
                    //    DNo = "001";

                    //if (DNo.Length == 0) DNo = "001";

                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Delete From TblEmployeeAllowanceDeduction Where Find_In_Set(EmpCode, @EmpCodes) And ADCode = @ADCode; ");

                        SQL.AppendLine("Insert Into TblEmployeeAllowanceDeduction (EmpCode, DNo, ADCode, StartDt, EndDt, SiteCode, Amt, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(" (@EmpCode_" + r.ToString() +
                        ", @DNo_" + r.ToString() +
                        ", @ADCode, @StartDt, @EndDt " +
                        ", @SiteCode_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @CreateBy, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    //Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + DNo, 3));
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + Sm.GetGrdStr(Grd1, r, 13), 3));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 11));
                    Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@EmpCodes", EmpCodes);
            Sm.CmParam<String>(ref cm, "@ADCode", mADCodeSMK);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mADCodeSMK = Sm.GetParameter("ADCodeSMK");
        }

        private void ComputeAmtMth()
        {
            decimal Amt = TxtAmt.Text.Length > 0 ? Decimal.Parse(TxtAmt.Text) : 0m;
            decimal Mth = TxtMth.Text.Length > 0 ? Decimal.Parse(TxtMth.Text) : 0m;
            decimal AmtMth = 0m;

            if (Mth != 0m)
            {
                AmtMth = Amt / Mth;
            }

            TxtAmtMth.EditValue = Sm.FormatNum(AmtMth, 0);
        }

        private void ComputeNoOfEmployee()
        {
            decimal NoOfEmployee = 0m;

            if (Grd1.Rows.Count > 1)
            {
                NoOfEmployee = Grd1.Rows.Count - 1;
            }

            TxtNoOfEmployee.EditValue = Sm.FormatNum(NoOfEmployee, 0);
        }

        private void ProcessData()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to import at least one record.");
                BtnImportData.Focus();
                return;
            }

            decimal mTotalValue = 0m;

            ComputeAmtMth();
            ComputeNoOfEmployee();
            ComputeTotalValueDtl(ref mTotalValue);
            ComputePerformanceAmtDtl(ref mTotalValue);
        }

        private void ComputeTotalValueDtl(ref decimal mTotalValue)
        {
            decimal TotalValue = 0m, PerformanceValue = 0m, QualityPoint = 0m;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                PerformanceValue = Sm.GetGrdDec(Grd1, i, 8);
                QualityPoint = Sm.GetGrdDec(Grd1, i, 9);
                TotalValue = PerformanceValue * QualityPoint;

                Grd1.Cells[i, 10].Value = TotalValue;

                mTotalValue += TotalValue;
            }
        }

        private void ComputePerformanceAmtDtl(ref decimal mTotalValue)
        {
            decimal AmtMth = Decimal.Parse(TxtAmtMth.Text);
            decimal Point = 0m;

            if (mTotalValue != 0m) Point = AmtMth / mTotalValue;

            if (Point != 0m)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    decimal TotalValue = Sm.GetGrdDec(Grd1, i, 10);
                    decimal PerformanceAmt = TotalValue * Point;

                    Grd1.Cells[i, 11].Value = PerformanceAmt;
                }
            }
        }

        private void ImportData() // EmpCode, PerformanceValue
        {
            ClearGrd();

            var l = new List<GridDtl>();

            PrepData(ref l);
            if (l.Count > 0)
            {
                GetEmployeeData(ref l);
                PlotToGrid(ref l);
            }

            l.Clear();
        }

        private void PrepData(ref List<GridDtl> l)
        {
            OFD.InitialDirectory = "c:";
            OFD.Filter = "CSV files (*.csv)|*.CSV";
            OFD.FilterIndex = 2;
            OFD.ShowDialog();

            var FileName = OFD.FileName;

            if (FileName != "openFileDialog1")
            {
                bool IsFirst = true;

                using (var rd = new StreamReader(FileName))
                {
                    while (!rd.EndOfStream)
                    {
                        var splits = rd.ReadLine().Split(',');
                        if (splits.Count() != 2)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                            return;
                        }
                        else
                        {
                            if (IsFirst)
                                IsFirst = false;
                            else
                            {
                                var arr = splits.ToArray();
                                if (arr[0].Trim().Length > 0)
                                {
                                    l.Add(new GridDtl()
                                    {
                                        EmpCode = Sm.Right(string.Concat("00000000", splits[0].Trim()), 8),
                                        EmpCodeOld = string.Empty,
                                        EmpName = string.Empty,
                                        DeptName = string.Empty,
                                        PosName = string.Empty,
                                        SiteName = string.Empty,
                                        SiteCode = string.Empty,
                                        LevelName = string.Empty,
                                        PerformanceValue = Decimal.Parse(splits[1].Trim().Length > 0 ? splits[1].Trim() : "0"),
                                        QualityPoint = 0m,
                                        TotalValue = 0m,
                                        PerformanceAmt = 0m
                                    });
                                }
                                else
                                {
                                    Sm.StdMsg(mMsgType.NoData, "");
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void GetEmployeeData(ref List<GridDtl> l)
        {
            EmpCodes = string.Empty;

            foreach (var x in l)
            {
                if (EmpCodes.Length > 0) EmpCodes += ",";
                EmpCodes += x.EmpCode;
            }

            if (EmpCodes.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, B.DeptName, C.PosName, ");
                SQL.AppendLine("D.SiteName, F.LevelName, F.QualityPoint, A.SiteCode, IfNull(G.DNo, '001') DNo ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
                SQL.AppendLine("Left Join TblPosition C On A.PosCode = C.PosCode ");
                SQL.AppendLine("Left Join TblSite D On A.SiteCode = D.SiteCode ");
                //SQL.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCode=E.GrdLvlCode ");
                SQL.AppendLine("Left Join TblLevelHdr F On A.LevelCode=F.LevelCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select EmpCode, Max(DNo)+1 DNo ");
                SQL.AppendLine("    From TblEmployeeAllowanceDeduction ");
                SQL.AppendLine("    Where Find_In_Set(EmpCode, @EmpCode) ");
                SQL.AppendLine("    Group By EmpCode ");
                SQL.AppendLine(")G On A.EmpCode = G.EmpCode ");
                SQL.AppendLine("Where Find_In_Set(A.EmpCode, @EmpCode); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCodes);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                    {
                        //0
                        "EmpCode",

                        //1-5
                        "EmpCodeOld",
                        "EmpName",
                        "DeptName",
                        "PosName",
                        "SiteName",                         

                        //6-8
                        "LevelName",
                        "QualityPoint",
                        "SiteCode",
                        "DNo"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            foreach (var x in l.Where(w => w.EmpCode == Sm.DrStr(dr, c[0])))
                            {
                                x.EmpCodeOld = Sm.DrStr(dr, c[1]);
                                x.EmpName = Sm.DrStr(dr, c[2]);
                                x.DeptName = Sm.DrStr(dr, c[3]);
                                x.PosName = Sm.DrStr(dr, c[4]);
                                x.SiteName = Sm.DrStr(dr, c[5]);
                                x.LevelName = Sm.DrStr(dr, c[6]);
                                x.QualityPoint = Sm.DrDec(dr, c[7]);
                                x.SiteCode = Sm.DrStr(dr, c[8]);
                                x.ADDNo = Sm.DrStr(dr, c[9]);
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        private void PlotToGrid(ref List<GridDtl> l)
        {
            Grd1.BeginUpdate();

            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 2, 1].Value = x.EmpCode;
                Grd1.Cells[Grd1.Rows.Count - 2, 2].Value = x.EmpCodeOld;
                Grd1.Cells[Grd1.Rows.Count - 2, 3].Value = x.EmpName;
                Grd1.Cells[Grd1.Rows.Count - 2, 4].Value = x.DeptName;
                Grd1.Cells[Grd1.Rows.Count - 2, 5].Value = x.PosName;
                Grd1.Cells[Grd1.Rows.Count - 2, 6].Value = x.SiteName;
                Grd1.Cells[Grd1.Rows.Count - 2, 7].Value = x.LevelName;
                Grd1.Cells[Grd1.Rows.Count - 2, 8].Value = Sm.FormatNum(x.PerformanceValue, 0);
                Grd1.Cells[Grd1.Rows.Count - 2, 9].Value = Sm.FormatNum(x.QualityPoint, 0);
                Grd1.Cells[Grd1.Rows.Count - 2, 10].Value = Sm.FormatNum(x.TotalValue, 0);
                Grd1.Cells[Grd1.Rows.Count - 2, 11].Value = Sm.FormatNum(x.PerformanceAmt, 0);
                Grd1.Cells[Grd1.Rows.Count - 2, 12].Value = x.SiteCode;
                Grd1.Cells[Grd1.Rows.Count - 2, 13].Value = x.ADDNo;
            }

            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnImportData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) // EmpCode, PerformanceValue
            {
                ImportData();
            }
        }

        private void BtnProcessData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Grd1.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to import at least one record.");
                    BtnImportData.Focus();
                    return;
                }

                ProcessData();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmt, 0);
                ComputeAmtMth();
            }
        }

        private void DteEndDt_DateTimeChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt) != "" && Sm.GetDte(DteEndDt) != "")
            {
                string StartDt = Sm.GetDte(DteDocDt).Substring(0, 8);
                string EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);
                if (Sm.CompareDtTm(EndDt, StartDt) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "End date should not be earlier than start date.");
                    DteEndDt.Focus();
                }
            }
        }

        private void TxtMth_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtMth, 0);
                ComputeAmtMth();
            }
        }

        #endregion

        #endregion

        #region Class

        private class GridDtl
        {
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string SiteName { get; set; }
            public string SiteCode { get; set; }
            public string LevelName { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal QualityPoint { get; set; }
            public decimal TotalValue { get; set; }
            public decimal PerformanceAmt { get; set; }
            public string ADDNo { get; set; }
        }

        #endregion
    }
}
