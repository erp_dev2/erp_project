﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHADlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmRHA mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRHADlg2(FrmRHA FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                SetGrd();
                SetSQL();
                TxtPayrunCode.Text = Sm.Left(Sm.ServerCurrentDateTime(), 6);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, A.PayrunName, ");
            SQL.AppendLine("A.DeptCode, B.DeptName, A.SiteCode, C.SiteName, ");
            SQL.AppendLine("A.StartDt, A.EndDt ");
            SQL.AppendLine("From TblPayrun A");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("left Join TblSite C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Payrun Code",
                        "Payrun Name",
                        "Department Code",
                        "Department",
                        "Site Code",

                        //6-8
                        "Site",
                        "Start Date",
                        "End Date"
                    },
                    new int[]
                    {
                        40, 
                        100, 250, 130, 150, 120,
                        120, 80, 80, 0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            if (Sm.IsTxtEmpty(TxtPayrunCode, "Payrun", false)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPayrunCode.Text, new string[] { "A.PayrunCode", "A.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By Left(PayrunCode, 6) Desc, PayrunCode;",
                        new string[] 
                        { 
                            //0
                            "PayrunCode",

                            //1-5
                            "PayrunName", "DeptCode", "DeptName", "SiteCode", "SiteName", 
                            
                            //6-7
                            "StartDt", "EndDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                int Row = Grd1.CurRow.Index;
                mFrmParent.TxtPayrunCode.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtPayrunName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                Sm.SetLue(mFrmParent.LueDeptCode, Sm.GetGrdStr(Grd1, Row, 3));
                Sm.SetLue(mFrmParent.LueSiteCode, Sm.GetGrdStr(Grd1, Row, 5));
                Sm.ClearGrd(mFrmParent.Grd1, true);
                this.Close();
            }

        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPayrunCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPayrunCode);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
