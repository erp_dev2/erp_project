﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Globalization;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmIncentive : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool mIsNotFilterByAuthorization = false;
        internal FrmIncentiveFind FrmFind;

        #endregion

        #region Constructor

        public FrmIncentive(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Daily Employee's Incentive";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtDocNo }, true);
                SetFormControl(mState.View);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueAGCode(ref LueAGCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Employee"+Environment.NewLine+"Code",
                        "Old Code",
                        "Employee"+Environment.NewLine+"Name",
                        "Min.Daily"+Environment.NewLine+"Salary",
                        
                        //6-8
                        "Daily"+Environment.NewLine+"Salary",
                        "Incentive",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,
                        
                        //1-5
                        20, 100, 100, 250, 130,  

                        //6-8
                        130, 130, 300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 6, 7 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueAGCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 5, 8 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueAGCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 8 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueDeptCode, LueAGCode, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 7 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIncentiveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    EditData();
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (
                    e.ColIndex == 1 &&
                    !Sm.IsDteEmpty(DteDocDt, "Date") &&
                    !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                    !Sm.IsLueEmpty(LueAGCode, "Attedance group")
                    )
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmIncentiveDlg(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueDeptCode), Sm.GetLue(LueAGCode)));
                }
                if (Sm.IsGrdColSelected(new int[] { 1, 5, 8 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 &&
                TxtDocNo.Text.Length == 0 &&
                !Sm.IsDteEmpty(DteDocDt, "Date") &&
                !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                !Sm.IsLueEmpty(LueAGCode, "Attedance group")
                )
                Sm.FormShowDialog(new FrmIncentiveDlg(this, Sm.GetDte(DteDocDt), Sm.GetLue(LueDeptCode), Sm.GetLue(LueAGCode)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 8 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5 }, e);
            if (e.ColIndex == 5) ComputeIncentive(e.RowIndex);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Incentive", "TblIncentiveHdr");
            
            var cml = new List<MySqlCommand>();
            cml.Add(SaveIncentiveHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    cml.Add(SaveIncentiveDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(DteDocDt, "Date", false) ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueAGCode, "Attedance group") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Employee is empty.")) return true;
                if (IsIncentiveExisted(Row)) return true;
            }
            return false;
        }

        private bool IsIncentiveExisted(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblIncentiveHdr A, TblIncentiveDtl B ");
            SQL.AppendLine("Where A.DocNo=B.DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt=@DocDt ");
            SQL.AppendLine("And B.EmpCode=@EmpCode;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Old Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine +
                    "Daily incentive already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveIncentiveHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblIncentiveHdr (DocNo, DocDt, CancelInd, DeptCode, AGCode, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DocDt, 'N', @DeptCode, @AGCode, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveIncentiveDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblIncentiveDtl(DocNo, DNo, EmpCode, MinDailySalary, DailySalary, Incentive, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @MinDailySalary, @DailySalary, @Incentive, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@MinDailySalary", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@DailySalary", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Incentive", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            cml.Add(EditIncentiveHdr());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataAlreadyCancelled();
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblIncentiveHdr Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private MySqlCommand EditIncentiveHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIncentiveHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowIncentiveHdr(DocNo);
                ShowIncentiveDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIncentiveHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                         ref cm,
                         "Select DocNo, CancelInd, DocDt, DeptCode, AGCode, Remark " +
                         "From TblIncentiveHdr Where DocNo=@DocNo;",
                         new string[] { 
                             "DocNo", 
                             "CancelInd", "DocDt", "DeptCode", "AGCode", "Remark" 
                         },
                         (MySqlDataReader dr, int[] c) =>
                         {
                             TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                             ChkCancelInd.Checked = Sm.DrStr(dr, c[1])=="Y";
                             Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                             Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[3]));
                             Sm.SetLue(LueAGCode, Sm.DrStr(dr, c[4]));
                             MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                         }, true
                     );
        }

        private void ShowIncentiveDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpCodeOld, B.EmpName, ");
            SQL.AppendLine("A.MinDailySalary, A.DailySalary, A.Incentive, A.Remark ");
            SQL.AppendLine("From TblIncentiveDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "EmpCode", "EmpCodeOld", "EmpName", "MinDailySalary", "DailySalary", 
                        
                        //6-7
                        "Incentive", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 7 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
        }

        private void ComputeIncentive(int Row)
        {
            var MinDailySalary = Sm.GetGrdDec(Grd1, Row, 5);
            var DailySalary = Sm.GetGrdDec(Grd1, Row, 6);
            Grd1.Cells[Row, 7].Value = MinDailySalary - DailySalary;
        }

        private void SetLueAGCode(ref LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
            {
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')='" + DeptCode + "' ");
            }
            SQL.AppendLine("Order By AGName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            string DeptCode = Sm.GetLue(LueDeptCode);
            LueAGCode.EditValue = null;
            SetLueAGCode(ref LueAGCode, DeptCode);
            ClearGrd();
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            string DeptCode = Sm.GetLue(LueDeptCode);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), DeptCode);
            ClearGrd();
        }

        #endregion

        #endregion
    }
}
