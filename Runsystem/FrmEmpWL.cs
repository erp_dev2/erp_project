﻿#region Update
/* 
    21/08/2017 [TKG]  tambah validasi kalau karyawan tsb sudah dapat sp yg lbh berat sebelumnya, tidak bisa membuat sp yg lebih ringan. Tergantung dari tipe warning letter dan sequence.
    05/04/2018 [ARI] tambah printout utk HIN
    12/12/2019 [VIN+DITA/IMS] Printout untuk IMS
    22/10/2020 [ICA/PHT] Menambah Fitur Download File
    06/11/2020 [TKG/PHT] ubah perhitungan end date
    18/11/2020 [IBL/PHT] Membuat printout
    23/03/2021 [VIN/PHT] Bug Printout WL
    09/02/2022 [TKG/PHT] ubah GetParameter(), tambah ReferenceNo+EmpWLCt, tambah parameter IsEmpWLEditable
    23/03/2023 [SET/PHT] Edit -> 1 dokumen dapat upload file berdasar param
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using TenTec.Windows.iGridLib;

using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
#endregion

namespace RunSystem
{
    public partial class FrmEmpWL : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmEmpWLFind FrmFind;
        private bool 
            mIsEmpWLNotNeedValidation = false,
            mIsEmpWLEndDtMandatory = false,
            mIsEmpWLEditable = false,
            mIsEmpWLAllowToEditFile = false;
        internal bool 
            mIsFilterBySite = false,
            mIsEmpWLAllowToUploadFile = false;
        internal string
            mIsFormPrintOutSP,
            mFormPrintOutEmpWL1 = string.Empty,
            mFormPrintOutEmpWLSP1 = string.Empty,
            mFormPrintOutEmpWLSP2 = string.Empty,
            mFormPrintOutEmpWLSP3 = string.Empty,
            mWLCodeForUnsatisfied = string.Empty,
            mWLCodeForSP1 = string.Empty,
            mWLCodeForSP2 = string.Empty,
            mWLCodeForSP3 = string.Empty;
        internal string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;
        internal byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmEmpWL(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Warning Letter";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueWLCode(ref LueWLCode);
                Sl.SetLueOption(ref LueEmpWLCt, "EmpWLCt");
                LblEndDt.ForeColor = mIsEmpWLEndDtMandatory?Color.Red:Color.Black;
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueWLCode, DteStartDt, DteEndDt,
                        MeeReason1, MeeReason2, MeeReason3, MeeReason4, MeeReason5, 
                        TxtFile, ChkFile, LueEmpWLCt, TxtReferenceNo
                    }, true);
                    BtnEmpCode.Enabled = false;
                    BtnFile.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWLCode, DteStartDt, MeeReason1, MeeReason2, 
                        MeeReason3, MeeReason4, MeeReason5, ChkFile, LueEmpWLCt, 
                        TxtReferenceNo
                    }, false);
                    if (mIsEmpWLNotNeedValidation)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ DteEndDt }, false);
                    BtnEmpCode.Enabled = true;
                    if (mIsEmpWLAllowToUploadFile) BtnFile.Enabled = true;
                    ChkFile.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (mIsEmpWLEditable)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            LueWLCode, DteStartDt, MeeReason1, MeeReason2, MeeReason3, 
                            MeeReason4, MeeReason5, LueEmpWLCt, TxtReferenceNo
                        }, false);
                        if (mIsEmpWLNotNeedValidation)
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteEndDt }, false);
                    }
                    ChkCancelInd.Properties.ReadOnly = false;
                    if (mIsEmpWLAllowToUploadFile) BtnFile.Enabled = true;
                    ChkFile.Enabled = true;
                    ChkFile.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtEmpCode, TxtEmpName, TxtEmpCodeOld, 
                TxtPosCode, TxtDeptCode, TxtSiteCode, LueWLCode, DteStartDt, 
                DteEndDt, MeeReason1, MeeReason2, MeeReason3, MeeReason4, 
                MeeReason5, TxtFile, LueEmpWLCt, TxtReferenceNo
            });
            ChkCancelInd.Checked = false;
            PbUpload.Value = 0;
            ChkFile.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpWLFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if(ChkCancelInd.Checked==false)
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
                PrintData(TxtDocNo.Text);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpWL", "TblEmpWL");

            var cml = new List<MySqlCommand>();
            
            cml.Add(SaveEmpWL(DocNo));
            
            Sm.ExecCommands(cml);

            if (mIsEmpWLAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                Sm.IsLueEmpty(LueWLCode, "Warning letter") ||
                Sm.IsDteEmpty(DteStartDt, "Started date") ||
                Sm.IsMeeEmpty(MeeReason1, "Reason") ||
                (mIsEmpWLAllowToUploadFile && IsUploadFileNotValid()) ||
                //IsStartDtNotValid() ||
                IsEmpWLNotValid() ||
                IsEmpWLNotValid2() ||
                (mIsEmpWLEndDtMandatory && Sm.IsDteEmpty(DteEndDt, "End date"));
        }

        //private bool IsStartDtNotValid()
        //{
        //    if (mIsEmpWLNotNeedValidation) return false;

        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select A.DocNo ");
        //    SQL.AppendLine("From TblEmpWL A ");
        //    SQL.AppendLine("Where A.CancelInd='N' ");
        //    SQL.AppendLine("And A.EmpCode=@EmpCode ");
        //    SQL.AppendLine("And A.WLCode=@WLCode ");
        //    SQL.AppendLine("And ");
        //    SQL.AppendLine("Not ( ");
        //    SQL.AppendLine("    ( ");
        //    SQL.AppendLine("    STR_TO_DATE(@StartDt, '%Y%m%d')<STR_TO_DATE(A.StartDt, '%Y%m%d') And ");
        //    SQL.AppendLine("    STR_TO_DATE(@EndDt, '%Y%m%d')<STR_TO_DATE(A.StartDt, '%Y%m%d') ");
        //    SQL.AppendLine("    ) ");
        //    SQL.AppendLine("Or ( ");
        //    SQL.AppendLine("    STR_TO_DATE(@StartDt, '%Y%m%d')>STR_TO_DATE(A.EndDt, '%Y%m%d') And ");
        //    SQL.AppendLine("    STR_TO_DATE(@EndDt, '%Y%m%d')>STR_TO_DATE(A.EndDt, '%Y%m%d') ");
        //    SQL.AppendLine("    ) ");
        //    SQL.AppendLine(") ");
        //    SQL.AppendLine("Limit 1; ");

        //    var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
        //    Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWLCode));
        //    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
        //    Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
        //    var DocNo = Sm.GetValue(cm);
        //    if (DocNo.Length>0)
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "Invalid period, Pls check document# " + DocNo + ".");
        //        return true;
        //    }
        //    return false;
        //}

        private bool IsEmpWLNotValid()
        {
            if (mIsEmpWLNotNeedValidation) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblEmpWL A, TblWarningLetter B ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.WLCode=B.WLCode ");
            SQL.AppendLine("And B.WLType Is Not Null ");
            SQL.AppendLine("And B.WLType In ( ");
            SQL.AppendLine("    Select WLType From TblWarningLetter ");
            SQL.AppendLine("    Where WLCode=@WLCode ");
            SQL.AppendLine("    And WLType Is Not Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And B.SeqNo Is Not Null ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (@StartDt>=StartDt And @StartDt<=EndDt) Or  ");
            SQL.AppendLine("    (@EndDt>=StartDt And @EndDt<=EndDt) Or ");
            SQL.AppendLine("    (@StartDt<=StartDt And @EndDt>=EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And B.SeqNo>=( ");
            SQL.AppendLine("    Select SeqNo From TblWarningLetter ");
            SQL.AppendLine("    Where WLCode=@WLCode ");
            SQL.AppendLine("    And WLType Is Not Null ");
            SQL.AppendLine("    And SeqNo is Not Null ");
            SQL.AppendLine(") Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWLCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            var DocNo = Sm.GetValue(cm);
            if (DocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Invalid warning letter." + Environment.NewLine +
                    "Please check document# " + DocNo + ".");
                return true;
            }
            return false;
        }

        private bool IsEmpWLNotValid2()
        {
            if (mIsEmpWLNotNeedValidation) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblEmpWL ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And EmpCode=@EmpCode ");
            SQL.AppendLine("And WLCode=@WLCode ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (@StartDt>=StartDt And @StartDt<=EndDt) Or  ");
            SQL.AppendLine("    (@EndDt>=StartDt And @EndDt<=EndDt) Or ");
            SQL.AppendLine("    (@StartDt<=StartDt And @EndDt>=EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWLCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            var DocNo = Sm.GetValue(cm);
            if (DocNo.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Invalid warning letter period." + Environment.NewLine +
                    "Please check document# " + DocNo + ".");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpWL(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpWL ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, EmpCode, WLCode, StartDt, EndDt, ");
            SQL.AppendLine("Reason1, Reason2, Reason3, Reason4, Reason5, EmpWLCt, ReferenceNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @EmpCode, @WLCode, @StartDt, @EndDt, ");
            SQL.AppendLine("@Reason1, @Reason2, @Reason3, @Reason4, @Reason5, ");
            SQL.AppendLine("@EmpWLCt, @ReferenceNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWLCode));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Reason1", MeeReason1.Text);
            Sm.CmParam<String>(ref cm, "@Reason2", MeeReason2.Text);
            Sm.CmParam<String>(ref cm, "@Reason3", MeeReason3.Text);
            Sm.CmParam<String>(ref cm, "@Reason4", MeeReason4.Text);
            Sm.CmParam<String>(ref cm, "@Reason5", MeeReason5.Text);
            Sm.CmParam<String>(ref cm, "@EmpWLCt", Sm.GetLue(LueEmpWLCt));
            Sm.CmParam<String>(ref cm, "@ReferenceNo", TxtReferenceNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpWL());

            Sm.ExecCommands(cml);

            if (mIsEmpWLAllowToUploadFile && mIsEmpWLAllowToEditFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(TxtDocNo.Text);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready();
        }

        private bool IsDocumentNotCancelled()
        {
            if (mIsEmpWLEditable) return false;
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 From TblEmpWL Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditEmpWL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpWL Set ");
            if (mIsEmpWLEditable)
            {
                SQL.AppendLine("    WLCode=@WLCode, StartDt=@StartDt, EndDt=@EndDt, ");
                SQL.AppendLine("    Reason1=@Reason1, Reason2=@Reason2, Reason3=@Reason3, Reason4=@Reason4, Reason5=@Reason5, ");
                SQL.AppendLine("    EmpWLCt=@EmpWLCt, ReferenceNo=@ReferenceNo, ");
            }    
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (mIsEmpWLEditable)
            {
                Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWLCode));
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                Sm.CmParam<String>(ref cm, "@Reason1", MeeReason1.Text);
                Sm.CmParam<String>(ref cm, "@Reason2", MeeReason2.Text);
                Sm.CmParam<String>(ref cm, "@Reason3", MeeReason3.Text);
                Sm.CmParam<String>(ref cm, "@Reason4", MeeReason4.Text);
                Sm.CmParam<String>(ref cm, "@Reason5", MeeReason5.Text);
                Sm.CmParam<String>(ref cm, "@EmpWLCt", Sm.GetLue(LueEmpWLCt));
                Sm.CmParam<String>(ref cm, "@ReferenceNo", TxtReferenceNo.Text);
            }
            return cm;
        }

        private MySqlCommand UpdateEmpWLFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();
            if (!mIsEmpWLAllowToEditFile)
            {
                SQL.AppendLine("Update TblEmpWL Set ");
                SQL.AppendLine("    FileName=@FileName ");
                SQL.AppendLine("Where DocNo=@DocNo  ; ");
            } 
            else
            {
                SQL.AppendLine("Insert Into TblEmpWLDtl ");
                SQL.AppendLine("(DocNo, DNo, FileName, CreateBy, CreateDt)  ");
                SQL.AppendLine("Values(@DocNo, @DNo, @FileName, @UserCode, CurrentDateTime()); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            if (mIsEmpWLAllowToEditFile)
            {
                string DNo = Sm.GetValue("Select Max(DNo) DNo From TblEmpWLDtl Where DocNo='" + DocNo + "';");
                if (DNo.Length > 0)
                {
                    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Decimal.Parse(DNo) + 1).ToString(), 3));
                }
                else
                    Sm.CmParam<String>(ref cm, "@DNo", "001");
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpWL(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpWL(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, DocDt, CancelInd, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName, ");
            SQL.AppendLine("A.WLCode, A.StartDt, A.EndDt, ");
            if(mIsEmpWLAllowToEditFile)
                SQL.AppendLine("F.FileName, ");
            else
                SQL.AppendLine("A.FileName, ");
            SQL.AppendLine("A.Reason1, A.Reason2, A.Reason3, A.Reason4, A.Reason5, ");
            SQL.AppendLine("A.ReferenceNo, A.EmpWLCt ");
            SQL.AppendLine("From TblEmpWL A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            if(mIsEmpWLAllowToEditFile)
                SQL.AppendLine("Left Join (Select F1.DocNo, F1.FileName From TblEmpWLDtl F1 Where F1.DocNo=@DocNO Order By F1.DNo Desc Limit 1) F On A.DocNo=F.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "EmpCode", "EmpName", "EmpCodeOld", 
                        
                        //6-10
                        "PosName", "DeptName", "SiteName", "WLCode", "StartDt", 
                        
                        //11-15
                        "EndDt", "Reason1", "Reason2", "Reason3", "Reason4", 
                        
                        //16-19
                        "Reason5", "FileName", "ReferenceNo", "EmpWLCt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetLue(LueWLCode, Sm.DrStr(dr, c[9]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[10]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[11]));
                        MeeReason1.EditValue = Sm.DrStr(dr, c[12]);
                        MeeReason2.EditValue = Sm.DrStr(dr, c[13]);
                        MeeReason3.EditValue = Sm.DrStr(dr, c[14]);
                        MeeReason4.EditValue = Sm.DrStr(dr, c[15]);
                        MeeReason5.EditValue = Sm.DrStr(dr, c[16]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[17]);
                        TxtReferenceNo.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetLue(LueEmpWLCt, Sm.DrStr(dr, c[19]));
                    }, true
                );
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsEmpWLNotNeedValidation', 'WLCodeForSP1', 'WLCodeForSP2', 'WLCodeForSP3', 'WLCodeForUnsatisfied', ");
            SQL.AppendLine("'PortForFTPClient', 'PasswordForFTPClient', 'UsernameForFTPClient', 'SharedFolderForFTPClient', 'HostAddrForFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient', 'FormPrintOutEmpWLSP1', 'FormPrintOutEmpWLSP2', 'FormPrintOutEmpWLSP3', 'FormPrintOutEmpWL', ");
            SQL.AppendLine("'IsFilterBySite', 'IsEmpWLAllowToUploadFile', 'FormPrintOutEmpWL1', 'IsEmpWLEditable', 'IsEmpWLAllowToEditFile' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsEmpWLNotNeedValidation": mIsEmpWLNotNeedValidation = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsEmpWLAllowToUploadFile": mIsEmpWLAllowToUploadFile = ParValue == "Y"; break;
                            case "IsEmpWLEditable": mIsEmpWLEditable = ParValue == "Y"; break;
                            case "IsEmpWLAllowToEditFile": mIsEmpWLAllowToEditFile = ParValue == "Y"; break;

                            //string
                            case "FormPrintOutEmpWL1": mFormPrintOutEmpWL1 = ParValue; break;
                            case "FormPrintOutEmpWLSP1": mFormPrintOutEmpWLSP1 = ParValue; break;
                            case "FormPrintOutEmpWLSP2": mFormPrintOutEmpWLSP2 = ParValue; break;
                            case "FormPrintOutEmpWLSP3": mFormPrintOutEmpWLSP3 = ParValue; break;
                            case "FormPrintOutEmpWL": mIsFormPrintOutSP = ParValue; break;
                            case "WLCodeForSP1": mWLCodeForSP1 = ParValue; break;
                            case "WLCodeForSP2": mWLCodeForSP2 = ParValue; break;
                            case "WLCodeForSP3": mWLCodeForSP3 = ParValue; break;
                            case "WLCodeForUnsatisfied": mWLCodeForUnsatisfied = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;

                        }
                    }
                }
                dr.Close();
            }

        }

        private void SetEndDt()
        {
            if (Sm.GetLue(LueWLCode).Length == 0 || Sm.GetDte(DteStartDt).Length == 0)
            {
                DteEndDt.EditValue = null;
                return;
            }

            if (mIsEmpWLNotNeedValidation && Sm.GetDte(DteEndDt).Length > 0) return;
            
            var StartDt = Sm.ConvertDateTime(Sm.GetDte(DteStartDt));
            try 
            {
                double NoOfDays = GetNoOfDays();
                DteEndDt.EditValue = StartDt.AddDays(NoOfDays);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private double GetNoOfDays()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select NoOfDays From TblWarningLetter Where WLCode=@WLCode;"
            };
            Sm.CmParam<String>(ref cm, "@WLCode", Sm.GetLue(LueWLCode));
            var NoOfDays = Sm.GetValue(cm);
            if (NoOfDays.Length == 0)
                return 0;
            else
                return double.Parse(NoOfDays);
        }

        private void PrintData(string DocNo)
        {
            var l = new List<WLHdr>();
            var l2 = new List<WLPHT>();
           
            string[] TableName = { "WLHdr", "WLPHT" };
            string mDocTitle = Sm.GetParameter("DocTitle");
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, F.CompanyName, F.CompanyPhone, F.CompanyFax, F.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            }
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt,  ");
            SQL.AppendLine("A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, ");
            SQL.AppendLine("A.WLCode, DATE_FORMAT(A.StartDt,'%d %M %Y') As StartDt, DATE_FORMAT(A.EndDt,'%d %M %Y') As EndDt, ");
            SQL.AppendLine("A.Reason1, A.Reason2, A.Reason3, A.Reason4, A.Reason5, E.WlName, B.EmpCodeOld, A.StartDt, A.EndDt, G.UserName, ");
             if(mDocTitle == "IMS")
                 SQL.AppendLine("G.EmpName EmpNameDeptHead ");
             else
                 SQL.AppendLine("Null As EmpNameDeptHead ");
            SQL.AppendLine("From TblEmpWL A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblWarningLetter E On A.WlCode = E.WlCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, E.EntName As CompanyName, E.EntPhone As CompanyPhone, E.EntFax As CompanyFax, E.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblEmpWL A");
                SQL.AppendLine("    Inner Join TblEmployee B On A.EmpCode=B.EmpCode");
                SQL.AppendLine("    Inner Join TblSite C On B.SiteCode = C.SiteCode");
                SQL.AppendLine("    Inner Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode");
                SQL.AppendLine("    Inner Join TblEntity E On D.EntCode = E.EntCode");
                SQL.AppendLine("    Where A.DocNo=@DocNo");
                SQL.AppendLine(")F On A.DocNo = F.DocNo");
            }
            SQL.AppendLine("Left join tbluser G On A.CreateBy=G.UserCode ");
            if(mDocTitle == "IMS")
            {
                 SQL.AppendLine("Left Join ");
                 SQL.AppendLine("( ");
                 SQL.AppendLine("    SELECT DISTINCT DeptCode, EmpName ");
                 SQL.AppendLine("   FROM TblEmployee ");
                 SQL.AppendLine("   WHERE PosCode IN ('POS009') ");
                 SQL.AppendLine(") G ON G.DeptCode = B.DeptCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo "); 

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select E.EntLogoName " +
                       "From TblEmpWL A " +
                       "Inner Join TblEmployee B On A.EmpCode=B.EmpCode " +
                       "Inner Join TblSite C On B.SiteCode = C.SiteCode " +
                       "Inner Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode " +
                       "Inner Join TblEntity E On D.EntCode = E.EntCode " +
                       "Where A.DocNo='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",
                         
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                                               
                         //6-10  
                         "DocNo",  
                         "DocDt",
                         "EmpCode",
                         "EmpName" ,
                         "EmpCodeOld" ,
                         
                         //11-15
                         "DeptName",
                         "PosName",
                         "StartDt",
                         "EndDt",
                         "Reason1" ,
                        
                         //16-20
                         "Reason2",
                         "Reason3",
                         "Reason4" ,
                         "Reason5",
                         "WlName",
                         
                         //21-23
                         "WLCode",
                         "UserName",
                         "EmpNameDeptHead"
                         
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WLHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyLongAddress = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            EmpCode = Sm.DrStr(dr, c[8]),
                            EmpName= Sm.DrStr(dr, c[9]),
                            EmpCodeOld= Sm.DrStr(dr, c[10]),

                            DeptName = Sm.DrStr(dr, c[11]),
                            PosName = Sm.DrStr(dr, c[12]),
                            StartDt= Sm.DrStr(dr, c[13]),
                            EndDt= Sm.DrStr(dr, c[14]),
                            Reason1= Sm.DrStr(dr, c[15]),

                            Reason2= Sm.DrStr(dr, c[16]),
                            Reason3= Sm.DrStr(dr, c[17]),
                            Reason4= Sm.DrStr(dr, c[18]),
                            Reason5 = Sm.DrStr(dr, c[19]),
                            WlName = Sm.DrStr(dr, c[20]),

                            WLCode = Sm.DrStr(dr, c[21]),
                            UserName = Sm.DrStr(dr, c[22]),
                            EmpNameDeptHead = Sm.DrStr(dr, c[23]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            EmpName2 = Sm.GetValue("Select UserName From TblUser Where UserCode = '"+Gv.CurrentUserCode+"' "),
                            DeptName2 = Sm.GetValue("Select B.GrpName From TblUser A Inner Join TblGroup B On A.GrpCode = B.grpCode Where A.UserCode = '" + Gv.CurrentUserCode + "' ")
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region WLPHT
            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();

            if (mIsFilterBySite)
            {
                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, F2.CompanyName, F2.CompanyPhone, F2.CompanyFax, F2.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            }
            SQL2.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.EmpName, C.PosName, B.Address, ");
            SQL2.AppendLine("A.Reason1, A.Reason2, A.Reason3, A.Reason4, A.Reason5, ");
            SQL2.AppendLine("E.SiteName, F.OptDesc, D.WLName ");
            SQL2.AppendLine("From TblEmpWL A ");
            SQL2.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL2.AppendLine("Inner Join TblPosition C On B.PosCode = C.PosCode ");
            SQL2.AppendLine("Inner Join TblWarningLetter D On A.WLCode = D.WLCode ");
            SQL2.AppendLine("Inner Join TblSite E On B.SiteCode = E.SiteCode ");
            SQL2.AppendLine("Inner Join TblOption F On D.WLType = F.OptCode And F.OptCat = 'WLType' ");
            if (mIsFilterBySite)
            {
                SQL2.AppendLine("Left Join (");
                SQL2.AppendLine("    Select distinct A.DocNo, E.EntName As CompanyName, E.EntPhone As CompanyPhone, E.EntFax As CompanyFax, E.EntAddress As CompanyAddress ");
                SQL2.AppendLine("    From TblEmpWL A");
                SQL2.AppendLine("    Inner Join TblEmployee B On A.EmpCode=B.EmpCode");
                SQL2.AppendLine("    Inner Join TblSite C On B.SiteCode = C.SiteCode");
                SQL2.AppendLine("    Inner Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode");
                SQL2.AppendLine("    Inner Join TblEntity E On D.EntCode = E.EntCode");
                SQL2.AppendLine("    Where A.DocNo=@DocNo");
                SQL2.AppendLine(")F2 On A.DocNo = F2.DocNo");
            }
            SQL2.AppendLine("Where A.DocNo = @DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select E.EntLogoName " +
                       "From TblEmpWL A " +
                       "Inner Join TblEmployee B On A.EmpCode=B.EmpCode " +
                       "Inner Join TblSite C On B.SiteCode = C.SiteCode " +
                       "Inner Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode " +
                       "Inner Join TblEntity E On D.EntCode = E.EntCode " +
                       "Where A.DocNo='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                }
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocDt",

                         //6-10
                         "EmpName",
                         "PosName",
                         "Address",
                         "Reason1",
                         "Reason2",

                         //11-15
                         "Reason3",
                         "Reason4",
                         "Reason5",
                         "SiteName",
                         "OptDesc",

                         //16
                         "WLName",
                         
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new WLPHT()
                        {
                            CompanyLogo = Sm.DrStr(dr2, c2[0]),
                            CompanyName = Sm.DrStr(dr2, c2[1]),
                            CompanyAddress = Sm.DrStr(dr2, c2[2]),
                            CompanyLongAddress = Sm.DrStr(dr2, c2[3]),
                            CompanyPhone = Sm.DrStr(dr2, c2[4]),
                            CompanyFax = Sm.DrStr(dr2, c2[4]),

                            DocDt = Sm.DrStr(dr2, c2[5]),
                            EmpName = Sm.DrStr(dr2, c2[6]),
                            PosName = Sm.DrStr(dr2, c2[7]),
                            Address = Sm.DrStr(dr2, c2[8]),
                            Reason1 = Sm.DrStr(dr2, c2[9]),
                            Reason2 = Sm.DrStr(dr2, c2[10]),
                            Reason3 = Sm.DrStr(dr2, c2[11]),
                            Reason4 = Sm.DrStr(dr2, c2[12]),
                            Reason5 = Sm.DrStr(dr2, c2[13]),
                            SiteName = Sm.DrStr(dr2, c2[14]),
                            WLCategory = Sm.DrStr(dr2, c2[15]),
                            WLName = Sm.DrStr(dr2, c2[16]),

                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            if (mDocTitle == "PHT")
            {
                if (Sm.GetLue(LueWLCode) == mWLCodeForUnsatisfied)
                    Sm.PrintReport(mFormPrintOutEmpWL1, myLists, TableName, false);
                if (Sm.GetLue(LueWLCode) == mWLCodeForSP1)
                    Sm.PrintReport(mFormPrintOutEmpWLSP1, myLists, TableName, false);
                if (Sm.GetLue(LueWLCode) == mWLCodeForSP2)
                    Sm.PrintReport(mFormPrintOutEmpWLSP2, myLists, TableName, false);
                if (Sm.GetLue(LueWLCode) == mWLCodeForSP3)
                    Sm.PrintReport(mFormPrintOutEmpWLSP3, myLists, TableName, false);
            }
            else
                Sm.PrintReport(mIsFormPrintOutSP, myLists, TableName, false);

        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateEmpWLFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsEmpWLAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsEmpWLAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsEmpWLAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsEmpWLAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsEmpWLAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsEmpWLAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblEmpWL ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        internal void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #endregion
        
        #region Event

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpWLDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void LueWLCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWLCode, new Sm.RefreshLue1(Sl.SetLueWLCode));
                SetEndDt();
            }
        }        

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) SetEndDt();
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false) TxtFile.EditValue = string.Empty;
        }

        #endregion

        #endregion
    }

    #region Report Class

    class WLHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyLongAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        
        public string EmpCode{ set; get; }
        public string EmpName{ set; get; }
        public string EmpCodeOld{ set; get; }
        public string DeptName { set; get; }
        public string PosName { set; get; }
        public string StartDt{ set; get; }
        public string EndDt{ set; get; }
        public string Reason1{ set; get; }
        public string Reason2{ set; get; }
        public string Reason3{ set; get; }
        public string Reason4{ set; get; }
        public string Reason5 { set; get; }
        public string WlName { set; get; }
        public string PrintBy { set; get; }
        public string WLCode { set; get; }
        public string EmpName2 { set; get; }
        public string DeptName2 { set; get; }
        public string PosName2 { set; get; }
        public string UserName { set; get; }
        public string EmpNameDeptHead { set; get; }
    }

    class WLPHT
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyLongAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }

        public string DocDt { get; set; }
        public string EmpName { get; set; }
        public string PosName { get; set; }
        public string Address { get; set; }
        public string Reason1 { get; set; }
        public string Reason2 { get; set; }
        public string Reason3 { get; set; }
        public string Reason4 { get; set; }
        public string Reason5 { get; set; }
        public string SiteName { get; set; }
        public string WLCategory { get; set; }
        public string WLName { get; set; }
    }
    #endregion
}
