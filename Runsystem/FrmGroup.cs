﻿#region Update
/*
    19/12/2017 [HAR] tambah source untuk ambil source group
    02/01/2018 [ARI] tambah tab job transfer
    24/01/2018 [TKG] tambah tab payment term
    31/03/2018 [TKG] tambah tab level
    10/04/2018 [TKG] tambah tab Entity
    18/05/2020 [WED/YK] tambah tab COA
    08/07/2020 [WED/SIER] group tertentu tidak bisa menambahkan menu tertentu berdasarkan parameter IsGroupLimitationMenuActived, GrpCodeForSalaryMenu, MenuCodeForSalary
    21/09/2020 [TKG/SIER] tambah group's customer category
    14/03/2021 [TKG/PHT] tambah group's profit center
    14/03/2021 [TKG/IMS] merubah proses GetParameter
    27/04/2021 [TKG/PHT] mempercepat proses penyimpanan
    30/06/2022 [BRI/PHT] tambah parameter IsCostCenterGroupUseDetailInformation untuk menampilkan detail information cost center & parent
    25/07/2022 [TYO/SIER] tambah tab type untuk voucher
    02/08/2022 [TYO/SIER] tambah tab Vendor's Category
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGroup : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private string
            mGrpCodeForSalaryMenu = string.Empty,
            mMenuCodeForSalary = string.Empty;
        private bool 
            mIsGroupPaymentTermActived = false,
            mIsGroupLimitationMenuActived = false,
            mIsGroupProfitCenterActived = false,
            mIsGroupCostCenterActived = false,
            mIsGroupCOAActived = false;
        internal bool IsCOAChanged = false,
            mIsCostCenterGroupUseDetailInformation = false;
        internal FrmGroupFind FrmFind;
        internal List<GroupCOA> l = null;

        #endregion

        #region Constructor

        public FrmGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                GetParameter();
                SetGrd();
                l = new List<GroupCOA>();
                TpPaymentTerm.PageVisible = mIsGroupPaymentTermActived;
                TpProfitCenter.PageVisible = mIsGroupProfitCenterActived;
                TpCC.PageVisible = mIsGroupCostCenterActived;
                TpCOA.PageVisible = mIsGroupCOAActived;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Menu",

                        //1-5
                        "Menu Code", 
                        "View",
                        "Insert", 
                        "Edit",
                        "Delete", 

                        //6-7
                        "Print", 
                        "Spreadsheet"
                    },
                    new int[] 
                    {
                        400,
                        0, 50, 50, 50, 50,
                        50, 80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 2, 3, 4, 5, 6, 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);

            Grd2.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                    Grd2, new string[] 
                    { "", "Code", "Department" },
                    new int[] 
                    {
                        20, 0, 300
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2 });

            Grd3.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3, new string[] { "", "Code", "Warehouse" },
                    new int[] 
                    {
                        20, 0, 300
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });

            Grd4.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                    Grd4, new string[] { "", "Code", "Cost Center" },
                    new int[] 
                    {
                        20, 0, 300
                    }
                );
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdColReadOnly(Grd4, new int[] { 1, 2 });

            Grd5.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                    Grd5, new string[] { "", "Code", "Site" },
                    new int[] 
                    {
                        20, 0, 300
                    }
                );
            Sm.GrdColButton(Grd5, new int[] { 0 });
            Sm.GrdColReadOnly(Grd5, new int[] { 1, 2 });

            Grd6.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                    Grd6, new string[] { "", "Code", "Bank Account" },
                    new int[]{ 20, 0, 450 }
                );
            Sm.GrdColButton(Grd6, new int[] { 0 });
            Sm.GrdColReadOnly(Grd6, new int[] { 1, 2 });

            Grd7.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(Grd7, new string[] { "", "Code", "Item's Category" }, new int[]{ 20, 0, 300 });
            Sm.GrdColButton(Grd7, new int[] { 0 });
            Sm.GrdColReadOnly(Grd7, new int[] { 1, 2 });

            Grd8.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(Grd8, new string[] { "", "Code", "Job Transfer" }, new int[] { 20, 0, 300 });
            Sm.GrdColButton(Grd8, new int[] { 0 });
            Sm.GrdColReadOnly(Grd8, new int[] { 1, 2 });

            Grd9.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(Grd9, new string[] { "", "Code", "Term of Payment" }, new int[] { 20, 0, 300 });
            Sm.GrdColButton(Grd9, new int[] { 0 });
            Sm.GrdColReadOnly(Grd9, new int[] { 1, 2 });

            Grd10.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(Grd10, new string[] { "", "Code", "Level" }, new int[] { 20, 0, 300 });
            Sm.GrdColButton(Grd10, new int[] { 0 });
            Sm.GrdColReadOnly(Grd10, new int[] { 1, 2 });

            Grd11.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                    Grd11, new string[] { "", "Code", "Entity" },
                    new int[] 
                    {
                        20, 0, 300
                    }
                );
            Sm.GrdColButton(Grd11, new int[] { 0 });
            Sm.GrdColReadOnly(Grd11, new int[] { 1, 2 });

            Grd12.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                Grd12, new string[] { "", "COA#", "Account Description" },
                new int[] 
                {
                    20, 0, 300
                }
            );
            Sm.GrdColButton(Grd12, new int[] { 0 });
            Sm.GrdColReadOnly(Grd12, new int[] { 1, 2 });

            Grd13.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(
                    Grd13, new string[] { "", "Code", "Customer's Category" },
                    new int[] 
                    {
                        20, 0, 300
                    }
                );
            Sm.GrdColButton(Grd13, new int[] { 0 });
            Sm.GrdColReadOnly(Grd13, new int[] { 1, 2 });

            Grd14.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(Grd14, new string[] { "", "Code", "Profit Center" }, new int[] { 20, 0, 300 });
            Sm.GrdColButton(Grd14, new int[] { 0 });
            Sm.GrdColReadOnly(Grd14, new int[] { 1, 2 });

            #region Grd15
            Grd15.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(Grd15, new string[] { "", "Code", "Type" }, new int[] { 20, 0, 300 });
            Sm.GrdColButton(Grd15, new int[] { 0 });
            Sm.GrdColReadOnly(Grd15, new int[] { 1, 2 });
            #endregion

            #region Grd16
            Grd16.Cols.Count = 3;

            Sm.GrdHdrWithColWidth(Grd16, new string[] { "", "Code", "Vendor's Category Name" }, new int[] { 20, 0, 300 });
            Sm.GrdColButton(Grd16, new int[] { 0 });
            Sm.GrdColReadOnly(Grd16, new int[] { 1, 2 });
            #endregion

        }



        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtGrpCode, TxtGrpName }, true);
                    Grd1.ReadOnly = 
                    Grd2.ReadOnly =
                    Grd3.ReadOnly =
                    Grd4.ReadOnly =
                    Grd5.ReadOnly =
                    Grd6.ReadOnly =
                    Grd7.ReadOnly =
                    Grd8.ReadOnly =
                    Grd9.ReadOnly =
                    Grd10.ReadOnly =
                    Grd11.ReadOnly =
                    Grd12.ReadOnly =
                    Grd13.ReadOnly = 
                    Grd14.ReadOnly =
                    Grd15.ReadOnly = 
                    Grd16.ReadOnly = true;
                    TxtGrpCode.Focus();
                    BtnSourceGroup.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtGrpCode, TxtGrpName
                    }, false);
                    Grd1.ReadOnly =
                    Grd2.ReadOnly =
                    Grd3.ReadOnly =
                    Grd4.ReadOnly =
                    Grd5.ReadOnly =
                    Grd6.ReadOnly =
                    Grd7.ReadOnly =
                    Grd8.ReadOnly =
                    Grd9.ReadOnly =
                    Grd10.ReadOnly =
                    Grd11.ReadOnly =
                    Grd12.ReadOnly =
                    Grd13.ReadOnly =
                    Grd14.ReadOnly = 
                    Grd15.ReadOnly = 
                    Grd16.ReadOnly = false;
                    TxtGrpCode.Focus();
                    BtnSourceGroup.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtGrpCode, true);
                    Sm.SetControlReadOnly(TxtGrpName, false);
                    Grd1.ReadOnly =
                    Grd2.ReadOnly =
                    Grd3.ReadOnly =
                    Grd4.ReadOnly =
                    Grd5.ReadOnly =
                    Grd6.ReadOnly =
                    Grd7.ReadOnly =
                    Grd8.ReadOnly =
                    Grd9.ReadOnly =
                    Grd10.ReadOnly =
                    Grd11.ReadOnly =
                    Grd12.ReadOnly =
                    Grd13.ReadOnly =
                    Grd14.ReadOnly = 
                    Grd15.ReadOnly = 
                    Grd16.ReadOnly = false;
                    TxtGrpName.Focus();
                    BtnSourceGroup.Enabled = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtGrpCode, TxtGrpName });
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
            Sm.ClearGrd(Grd5, true);
            Sm.ClearGrd(Grd6, true);
            Sm.ClearGrd(Grd7, true);
            Sm.ClearGrd(Grd8, true);
            Sm.ClearGrd(Grd9, true);
            Sm.ClearGrd(Grd10, true);
            Sm.ClearGrd(Grd11, true);
            Sm.ClearGrd(Grd12, true);
            Sm.ClearGrd(Grd13, true);
            Sm.ClearGrd(Grd14, true);
            Sm.ClearGrd(Grd15, true);
            Sm.ClearGrd(Grd16, true);
            IsCOAChanged = false;
            ShowMenu();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtGrpCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveGroup());
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdBool(Grd1, Row, 2)) cml.Add(SaveGroupMenu(Row));

                if (Grd2.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd2, TxtGrpCode.Text, "Department", "Dept"));
                
                if (Grd3.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd3, TxtGrpCode.Text, "Warehouse", "Whs"));
                
                if (mIsGroupCostCenterActived && Grd4.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd4, TxtGrpCode.Text, "CostCenter", "CC"));
                
                if (Grd5.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd5, TxtGrpCode.Text, "Site", "Site"));
                
                if (Grd6.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd6, TxtGrpCode.Text, "BankAccount", "BankAc"));
                

                if (Grd7.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd7, TxtGrpCode.Text, "ItemCategory", "ItCt"));
                
                if (Grd8.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd8, TxtGrpCode.Text, "JobTransfer", "Job"));
                
                if (mIsGroupPaymentTermActived && Grd9.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd9, TxtGrpCode.Text, "PaymentTerm", "Pt"));

                if (Grd10.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd10, TxtGrpCode.Text, "Level", "Level"));

                if (Grd11.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd11, TxtGrpCode.Text, "Entity", "Ent"));
                
                if (mIsGroupCOAActived && Grd12.Rows.Count > 1 && IsCOAChanged)
                    cml.Add(SaveGroupInfo2(ref Grd12, TxtGrpCode.Text, "COA", "Ac"));
                
                if (Grd13.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd13, TxtGrpCode.Text, "CustomerCategory", "CtCt"));
                
                if (mIsGroupProfitCenterActived && Grd14.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd14, TxtGrpCode.Text, "ProfitCenter", "ProfitCenter"));

                if (Grd15.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd15, TxtGrpCode.Text, "Type", "Type"));

                if (Grd16.Rows.Count > 1)
                    cml.Add(SaveGroupInfo(ref Grd16, TxtGrpCode.Text, "VendorCategory", "VdCt"));

                Sm.ExecCommands(cml);

                Sm.StdMsg(mMsgType.Info, "Saving data is completed.");
                ClearData();
                SetFormControl(mState.View);
                //ShowData(TxtGrpCode.Text, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            TickGrdCols(e.RowIndex, e.ColIndex);
            if (e.ColIndex == 2 && !Sm.GetGrdBool(Grd1, e.RowIndex, 2))
            {
                for (int Col = 3; Col < Grd1.Cols.Count; Col++)
                {
                    Grd1.Cells[e.RowIndex, Col].Value = false;
                    TickGrdCols(e.RowIndex, Col);
                }
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Sm.GetGrdBool(Grd1, e.RowIndex, 2) &&
                Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6, 7 }, e.ColIndex) &&
                BtnSave.Enabled)
                e.DoDefault = false;

        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtGrpCode, "Group's code", false) ||
                Sm.IsTxtEmpty(TxtGrpName, "Group's name", false) ||
                IsGrdEmpty() ||
                IsGroupLimitationInvalid();
        }

        private bool IsGroupLimitationInvalid()
        {
            if (!mIsGroupLimitationMenuActived) return false;

            bool mIsGrpCodeValid = false;
            string mUserGrpCode = Sm.GetValue("Select GrpCode From TblUser Where UserCode = @Param Limit 1; ", Gv.CurrentUserCode);

            mIsGrpCodeValid = IsGrpCodeValid(mUserGrpCode);

            if (!mIsGrpCodeValid)
            {
                if (mMenuCodeForSalary.Length == 0) return false;

                string[] mMenuCodes = mMenuCodeForSalary.Split(',');
                int mTemp = 0;
                string mSelectedMenuCode = string.Empty;
                
                foreach(var x in mMenuCodes.OrderBy(o => o))
                {
                    for (int i = mTemp; i < Grd1.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                        {
                            if (x == Sm.GetGrdStr(Grd1, i, 1) &&
                                (
                                    Sm.GetGrdBool(Grd1, i, 2) ||
                                    Sm.GetGrdBool(Grd1, i, 3) ||
                                    Sm.GetGrdBool(Grd1, i, 4) ||
                                    Sm.GetGrdBool(Grd1, i, 5) ||
                                    Sm.GetGrdBool(Grd1, i, 6) ||
                                    Sm.GetGrdBool(Grd1, i, 7)
                                ))
                            {
                                if (mSelectedMenuCode.Length > 0) mSelectedMenuCode += Environment.NewLine;
                                mSelectedMenuCode += Sm.GetGrdStr(Grd1, i, 0);

                                mTemp = i;
                                break;
                            }
                        }
                    }
                }

                if (mSelectedMenuCode.Length > 0)
                {
                    var ms = new StringBuilder();

                    ms.AppendLine("You can't give access to these menu : ");
                    ms.AppendLine(Environment.NewLine);
                    ms.AppendLine(mSelectedMenuCode);

                    Sm.StdMsg(mMsgType.Warning, ms.ToString());
                    return true;
                }
            }

            return false;
        }

        private bool IsGrpCodeValid(string GrpCode)
        {
            if (mGrpCodeForSalaryMenu.Length == 0) return false;

            string[] mGrpCodes = mGrpCodeForSalaryMenu.Split(',');

            foreach (var x in mGrpCodes)
                if (GrpCode == x) 
                    return true;

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No menu.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveGroup()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGroup(GrpCode, GrpName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@GrpCode, @GrpName, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update GrpName=@GrpName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblGroupMenu Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupDepartment Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupWarehouse Where GrpCode=@GrpCode; ");
            if (mIsGroupCostCenterActived) SQL.AppendLine("Delete From TblGroupCostCenter Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupSite Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupBankAccount Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupItemCategory Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupJobTransfer Where GrpCode=@GrpCode; ");
            if (mIsGroupPaymentTermActived) SQL.AppendLine("Delete From TblGroupPaymentTerm Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupLevel Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupEntity Where GrpCode=@GrpCode; ");
            if (mIsGroupCOAActived && IsCOAChanged) SQL.AppendLine("Delete From TblGroupCOA Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupCustomerCategory Where GrpCode=@GrpCode; ");
            if (mIsGroupProfitCenterActived) SQL.AppendLine("Delete From TblGroupProfitCenter Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupType Where GrpCode=@GrpCode; ");
            SQL.AppendLine("Delete From TblGroupVendorCategory Where GrpCode=@GrpCode; ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@GrpCode", TxtGrpCode.Text);
            Sm.CmParam<String>(ref cm, "@GrpName", TxtGrpName.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveGroupMenu(int Row)
        {
            var AccessInd = new StringBuilder();
            for (int Col = 3; Col<Grd1.Cols.Count; Col++)
                AccessInd.Append(Sm.GetGrdBool(Grd1, Row, Col) ? "Y" : "N");

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblGroupMenu(GrpCode, MenuCode, AccessInd, CreateBy, CreateDt) " +
                    "Values(@GrpCode, @MenuCode, @AccessInd, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@GrpCode", TxtGrpCode.Text);
            Sm.CmParam<String>(ref cm, "@MenuCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@AccessInd", AccessInd.ToString());
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveGroupInfo(ref iGrid Grd, string GrpCode, string Tbl, string Column, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblGroup"+Tbl+"(GrpCode, "+Column+"Code, CreateBy, CreateDt) " +
        //            "Values (@GrpCode, @Code, @UserCode, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);
        //    Sm.CmParam<String>(ref cm, "@Code", Sm.GetGrdStr(Grd, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveGroupInfo(ref iGrid Grd, string GrpCode, string Tbl, string Column)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            int i = 0;

            for (int r = 0; r< Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 1).Length > 0)
                {
                    if (IsFirst)
                    { 
                        SQL.AppendLine("Insert Into TblGroup"+Tbl+"(GrpCode, "+Column+"Code, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values");
                        IsFirst = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@GrpCode, @Code"+i.ToString()+", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@Code"+i.ToString(), Sm.GetGrdStr(Grd, r, 1));
                    i++;
                }
            }
            if (SQL.Length > 0) SQL.AppendLine(";");

            cm.CommandText = "Set @Dt:=CurrentDateTime();" + SQL.ToString();
            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveGroupInfo2(ref iGrid Grd, string GrpCode, string Tbl, string Column, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblGroup" + Tbl + "(GrpCode, " + Column + "No, CreateBy, CreateDt) " +
        //            "Values (@GrpCode, @Code, @UserCode, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);
        //    Sm.CmParam<String>(ref cm, "@Code", Sm.GetGrdStr(Grd, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveGroupInfo2(ref iGrid Grd, string GrpCode, string Tbl, string Column)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            int i = 0;

            for (int r = 0; r < Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 1).Length > 0)
                {
                    if (IsFirst)
                    {
                        SQL.AppendLine("Insert Into TblGroup" + Tbl + "(GrpCode, " + Column + "No, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values");
                        IsFirst = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@GrpCode, @Code" + i.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@Code" + i.ToString(), Sm.GetGrdStr(Grd, r, 1));
                    i++;
                }
            }
            if (SQL.Length > 0) SQL.AppendLine(";");

            cm.CommandText = "Set @Dt:=CurrentDateTime();" + SQL.ToString();
            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        //Type = 0 untuk tanpa source, type = 1 untuk yg make source
        public void ShowData(string GrpCode, int CodeType)
        {
            try
            {
                ClearData();
                ShowGroup(GrpCode, CodeType);
                ShowGroupMenu(GrpCode);
                ShowGroupInfo(ref Grd2, GrpCode, "Department", "Dept");
                ShowGroupInfo(ref Grd3, GrpCode, "Warehouse", "Whs");
                if (mIsGroupCostCenterActived) ShowGroupInfo(ref Grd4, GrpCode, "CostCenter", "CC");
                ShowGroupInfo(ref Grd5, GrpCode, "Site", "Site");
                ShowGroupInfo(ref Grd6, GrpCode, "BankAccount", "BankAc");
                ShowGroupInfo(ref Grd7, GrpCode, "ItemCategory", "ItCt");
                ShowJobTransfer(GrpCode);
                if (mIsGroupPaymentTermActived) ShowGroupInfo(ref Grd9, GrpCode, "PaymentTerm", "Pt");
                ShowGroupInfo(ref Grd10, GrpCode, "Level", "Level");
                ShowGroupInfo(ref Grd11, GrpCode, "Entity", "Ent");
                if (mIsGroupCostCenterActived) ShowGroupInfo2(ref Grd12, GrpCode, "COA", "Ac");
                ShowGroupInfo(ref Grd13, GrpCode, "CustomerCategory", "CtCt");
                if (mIsGroupProfitCenterActived) ShowGroupInfo(ref Grd14, GrpCode, "ProfitCenter", "ProfitCenter");
                ShowType(GrpCode);
                ShowVendorCategory(GrpCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                if (CodeType == 0)
                {
                    SetFormControl(mState.View);
                }
                else
                {
                    SetFormControl(mState.Insert);
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowGroup(string GrpCode, int TypeShowData)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select GrpCode, GrpName From TblGroup Where GrpCode=@GrpCode",
                    new string[] { "GrpCode", "GrpName" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtGrpCode.EditValue = TypeShowData == 0?Sm.DrStr(dr, c[0]):"";
                        TxtGrpName.EditValue = TypeShowData == 0?Sm.DrStr(dr, c[1]):"";
                    }, true
                );
        }

        private void ShowGroupMenu(string GrpCode)
        {
            int Level = 0;
            string PrevMenuCode = string.Empty;
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);

            Sm.ClearGrd(Grd1, true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText =
                    "Select Concat(A.MenuCode, ' - ', A.MenuDesc) As MenuDesc, A.MenuCode, B.AccessInd " +
                    "From TblMenu A " +
                    "Left Join TblGroupMenu B On A.MenuCode=B.MenuCode And B.GrpCode=@GrpCode " +
                    "Where StdInd='Y' " +
                    "And Visible='Y' " + 
                    "Order By A.MenuCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "MenuDesc", "MenuCode", "AccessInd" });
                if (!dr.HasRows)
                    Sm.StdMsg(mMsgType.NoData, "");
                else
                {
                    int Row = 0;
                    Grd1.Rows.Count = 0;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd1.Rows.Add();
                        Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        if (dr[c[2]] == DBNull.Value)
                        {
                            for (int Col = 2; Col < Grd1.Cols.Count; Col++)
                            {
                                Grd1.Cells[Row, Col].Value = false;
                                TickGrdCols(Row, Col);
                            }
                        }
                        else
                        {
                            for (int Col = 2; Col < Grd1.Cols.Count; Col++)
                            {
                                if (Col == 2)
                                    Grd1.Cells[Row, Col].Value = true;
                                else
                                    Grd1.Cells[Row, Col].Value = Sm.CompareStr("Y", dr.GetString(c[2]).Substring(Col - 3, 1));
                                TickGrdCols(Row, Col);
                            }
                        }
                        Grd1.Rows[Row].Level = Level;
                        if (Row > 0)
                            Grd1.Rows[Row - 1].TreeButton =
                                (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                                    iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                        PrevMenuCode = Sm.DrStr(dr, 1);
                        Row++;
                    }
                    if (Row > 0)
                        Grd1.Rows[Row - 1].TreeButton =
                            (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                                iGTreeButtonState.Hidden : iGTreeButtonState.Visible;

                    Grd1.TreeLines.Visible = true;
                }
                Grd1.EndUpdate();
                dr.Close();
            }
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowGroupInfo(ref iGrid GrdTemp, string GrpCode, string Tbl, string Column)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A." + Column + "Code As Code, ");
            if (Sm.CompareStr(Column, "BankAc"))
            {
                SQL.AppendLine("    Concat( ");
                SQL.AppendLine("    Case When C.BankName Is Not Null Then Concat(C.BankName, ' : ') Else '' End, ");
                SQL.AppendLine("    Case When B.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(B.BankAcNo, ' [', IfNull(B.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(B.BankAcNm, '') End ");
                SQL.AppendLine("    ) As Name ");
            }
            else
                SQL.AppendLine("B." + Column + "Name As Name  ");
            SQL.AppendLine("From TblGroup" + Tbl + " A ");
            if (Sm.CompareStr(Column, "Level"))
                SQL.AppendLine("Inner Join Tbl" + Tbl + "Hdr B On A." + Column + "Code=B." + Column + "Code ");
            else
                SQL.AppendLine("Inner Join Tbl" + Tbl + " B On A." + Column + "Code=B." + Column + "Code ");
            if (Sm.CompareStr(Column, "BankAc"))
                SQL.AppendLine("Left Join TblBank C On B.BankCode=C.BankCode ");
            SQL.AppendLine("Where GrpCode=@GrpCode ");
            if (!Sm.CompareStr(Column, "BankAc"))
                SQL.AppendLine("Order By B." + Column + "Name ");
           

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);
            Sm.ShowDataInGrid(
                    ref GrdTemp, ref cm, SQL.ToString(),
                    new string[]{ "Code", "Name" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(GrdTemp, 0, 1);
        }

        private void ShowGroupInfo2(ref iGrid GrdTemp, string GrpCode, string Tbl, string Column)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A." + Column + "No As Code, ");
            SQL.AppendLine("Concat(A." + Column + "No, ' - ', B." + Column + "Desc) As Name  ");
            SQL.AppendLine("From TblGroup" + Tbl + " A ");
            SQL.AppendLine("Inner Join Tbl" + Tbl + " B On A." + Column + "No=B." + Column + "No ");
            SQL.AppendLine("Where GrpCode=@GrpCode ");
            SQL.AppendLine("Order By B." + Column + "No; ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);
            Sm.ShowDataInGrid(
                ref GrdTemp, ref cm, SQL.ToString(),
                new string[] { "Code", "Name" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(GrdTemp, 0, 1);
        }

        private void ShowJobTransfer(string GrpCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.JobCode, B.OptDesc from TblGroupJobTransfer A ");
            SQL.AppendLine("Inner Join TblOption B On B.OptCat='empjobtransfer' and A.JobCode=B.OptCode ");
            SQL.AppendLine("Where A.GrpCode=@GrpCode");

            Sm.ShowDataInGrid(
                ref Grd8, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        "JobCode", "OptDesc",
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd8, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd8, dr, c, Row, 2, 1);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd8, 0, 1);
        }

        private void ShowType(string GrpCode)
        {
            var cm = new MySqlCommand();
            

            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.TypeCode, B.OptDesc ");
            SQL.AppendLine("FROM TblGroupType A ");
            SQL.AppendLine("INNER JOIN TblOption B ON A.TypeCode = B.OptCode AND B.OptCat = 'VoucherDocType' ");
            SQL.AppendLine("WHERE A.GrpCode = @GrpCode ");
            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);

            Sm.ShowDataInGrid(
                ref Grd15, ref cm, SQL.ToString(),
                new string[]
                    {
                        "TypeCode", "OptDesc",
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd15, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd15, dr, c, Row, 2, 1);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd15, 0, 1);
        }

        private void ShowVendorCategory(string GrpCode)
        {
            var cm = new MySqlCommand();


            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.VdCtCode, B.VdCtName ");
            SQL.AppendLine("FROM TblGroupVendorCategory A ");
            SQL.AppendLine("INNER JOIN TblVendorCategory B ON A.VdCtCode = B.VdCtCode  ");
            SQL.AppendLine("WHERE A.GrpCode = @GrpCode ");
            Sm.CmParam<String>(ref cm, "@GrpCode", GrpCode);

            Sm.ShowDataInGrid(
                ref Grd16, ref cm, SQL.ToString(),
                new string[]
                    {
                        "VdCtCode", "VdCtName",
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd16, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd16, dr, c, Row, 2, 1);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd16, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            //mGrpCodeForSalaryMenu = Sm.GetParameter("GrpCodeForSalaryMenu");
            //mMenuCodeForSalary = Sm.GetParameter("MenuCodeForSalary");
            //mIsGroupLimitationMenuActived = Sm.GetParameterBoo("IsGroupLimitationMenuActived");
            //mIsGroupPaymentTermActived = Sm.GetParameterBoo("IsGroupPaymentTermActived");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'GrpCodeForSalaryMenu', 'MenuCodeForSalary', 'IsGroupLimitationMenuActived', 'IsGroupPaymentTermActived', 'IsGroupProfitCenterActived', ");
            SQL.AppendLine("'IsGroupCostCenterActived', 'IsGroupCOAActived', 'IsCostCenterGroupUseDetailInformation' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsGroupLimitationMenuActived": mIsGroupLimitationMenuActived = ParValue == "Y"; break;
                            case "IsGroupPaymentTermActived": mIsGroupPaymentTermActived = ParValue == "Y"; break;
                            case "IsGroupProfitCenterActived": mIsGroupProfitCenterActived = ParValue == "Y"; break;
                            case "IsGroupCostCenterActived": mIsGroupCostCenterActived = ParValue == "Y"; break;
                            case "IsGroupCOAActived": mIsGroupCOAActived = ParValue == "Y"; break;
                            case "IsCostCenterGroupUseDetailInformation": mIsCostCenterGroupUseDetailInformation = ParValue == "Y"; break;

                            //string
                            case "GrpCodeForSalaryMenu": mGrpCodeForSalaryMenu = ParValue; break;
                            case "MenuCodeForSalary": mMenuCodeForSalary = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        internal string GetSelectedDepartment()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedWarehouse()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd3, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedCostCenter()
        {
            var SQL = string.Empty;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd4, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedSite()
        {
            var SQL = string.Empty;
            if (Grd5.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd5, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedBankAccount()
        {
            var SQL = string.Empty;
            if (Grd6.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd6, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItemCategory()
        {
            var SQL = string.Empty;
            if (Grd7.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd7.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd7, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedJobTransfer()
        {
            var SQL = string.Empty;
            if (Grd8.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd8.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd8, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd8, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedEntity()
        {
            var SQL = string.Empty;
            if (Grd11.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd11.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd11, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd11, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedCOA()
        {
            var SQL = string.Empty;
            if (Grd12.Rows.Count != 1)
            {
                foreach(var x in l)
                {
                    if (SQL.Length > 0) SQL += ", ";
                    SQL += "'" + x.AcNo + "'";
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedCustomerCategory()
        {
            var SQL = string.Empty;
            if (Grd13.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd13.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd13, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd13, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedProfitCenter()
        {
            var SQL = string.Empty;
            if (Grd14.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd14.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd14, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd14, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedType()
        {
            var SQL = string.Empty;
            if (Grd15.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd15.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd15, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd15, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedVdCt()
        {
            var SQL = string.Empty;
            if (Grd16.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd16.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd16, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd16, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ShowCOAInfo(string AcNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            //l.Clear();

            SQL.AppendLine("Select AcNo, Concat(AcNo, ' - ', AcDesc) AcDesc ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("And Find_In_Set(AcNo, @AcNo) ");
            SQL.AppendLine("Order By AcNo; ");

            Sm.CmParam<String>(ref cm, "@AcNo", AcNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc" });
                if (dr.HasRows)
                {
                    int Row = Grd12.Rows.Count - 1;
                    //Sm.ClearGrd(Grd12, true);
                    Grd12.BeginUpdate();
                    while (dr.Read())
                    {
                        Grd12.Rows.Add();
                        Sm.SetGrdValue("S", Grd12, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd12, dr, c, Row, 2, 1);

                        l.Add(new GroupCOA()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1])
                        });

                        Row++;
                    }
                    Grd12.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ShowMenu()
        {
            int Level = 0;
            string PrevMenuCode = string.Empty;
            var cm = new MySqlCommand();

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText =
                        "Select Concat(MenuCode, ' - ', MenuDesc) As MenuDesc, MenuCode " +
                        "From TblMenu " +
                        "Where StdInd='Y' " +
                        "And Visible='Y' " +
                        "Order By MenuCode;";
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "MenuDesc", "MenuCode" });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd1.Rows.Count = 0;
                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Level = Sm.DrStr(dr, 1).Length - ((Sm.DrStr(dr, 1).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Grd1.Rows[Row].Level = Level;
                            if (Row > 0)
                                Grd1.Rows[Row - 1].TreeButton =
                                    (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            PrevMenuCode = Sm.DrStr(dr, 1);
                            Row++;
                        }
                        if (Row > 0)
                            Grd1.Rows[Row - 1].TreeButton =
                                (PrevMenuCode.Length >= Sm.DrStr(dr, 1).Length) ?
                                    iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                    }
                    Grd1.TreeLines.Visible = true;
                    Grd1.EndUpdate();
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void TickGrdCols(int RowIndex, int ColIndex)
        {
            try
            {
                if (ColIndex < 2) return;

                int Len = Sm.GetGrdStr(Grd1, RowIndex, 1).Length;
                bool Value = Sm.GetGrdBool(Grd1, RowIndex, ColIndex);
                string MenuCode = Sm.GetGrdStr(Grd1, RowIndex, 1);

                for (int Row = RowIndex + 1; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length <= Len) break;
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > Len)
                    {
                        if (ColIndex == 2)
                            Grd1.Cells[Row, ColIndex].Value = Value;
                        else
                        {
                            if (ColIndex > 2)
                            {
                                if (Sm.GetGrdBool(Grd1, Row, 2))
                                    Grd1.Cells[Row, ColIndex].Value = Value;
                                else
                                    Grd1.Cells[Row, ColIndex].Value = false;
                            }
                        }
                    }

                }

                for (int Row = RowIndex - 1; Row >= 0; Row--)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length < Len &&
                        ("X" + MenuCode).IndexOf("X" + Sm.GetGrdStr(Grd1, Row, 1)) >= 0 &&
                        (Value || (!Value && IsNotHaveThickedChildNode(Row, ColIndex, Sm.GetGrdStr(Grd1, Row, 1).Length))))
                        Grd1.Cells[Row, ColIndex].Value = Value;

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsNotHaveThickedChildNode(int CurRow, int CurCol, int Len)
        {
            for (int Row = CurRow + 1; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > Len)
                {
                    if (Sm.GetGrdBool(Grd1, Row, CurCol))
                        return false;
                }
                else
                    return true;
            }
            return true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtGrpCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGrpCode);
        }

        private void TxtGrpName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtGrpName);
        }

        private void BtnSourceGroup_Click(object sender, EventArgs e)
        {
            if (TxtGrpCode.Text.Length == 0) Sm.FormShowDialog(new FrmGroupDlg7(this));
        }

        #endregion

        #region Grid Event

        #region Grd 2

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg(this));
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg(this));
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 3

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg2(this));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 4

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg3(this));
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg3(this));
            }
        }

        #endregion

        #region Grd 5

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg4(this));
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd5, e, BtnSave);
            Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg4(this));
            }
        }

        #endregion

        #region Grd 6

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg5(this));
            }
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg5(this));
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd6, e, BtnSave);
            Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 7

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg6(this));
            }
        }

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg6(this));
        }

        private void Grd7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd7, e, BtnSave);
            Sm.GrdKeyDown(Grd7, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 8

        private void Grd8_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg8(this));
        }

        private void Grd8_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg8(this));
            }
        }

        private void Grd8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd8, e, BtnSave);
            Sm.GrdKeyDown(Grd8, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 9

        private void Grd9_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg9(this));
        }

        private void Grd9_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg9(this));
            }
        }

        private void Grd9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd9, e, BtnSave);
            Sm.GrdKeyDown(Grd9, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 10

        private void Grd10_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg10(this));
        }

        private void Grd10_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg10(this));
            }
        }

        private void Grd10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd10, e, BtnSave);
            Sm.GrdKeyDown(Grd10, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 11
        private void Grd11_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg11(this));
        }

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg11(this));
            }
        }

        private void Grd11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd11, e, BtnSave);
            Sm.GrdKeyDown(Grd11, e, BtnFind, BtnSave);
        }
        #endregion

        #region Grd 12

        private void Grd12_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg12(this));
        }

        private void Grd12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd12, e, BtnSave);
            Sm.GrdKeyDown(Grd12, e, BtnFind, BtnSave);
            if (Grd12.Rows.Count - 1 != l.Count()) IsCOAChanged = true;
            if (IsCOAChanged)
            {
                l.Clear();
                for (int i = 0; i < Grd12.Rows.Count - 1; ++i)
                {
                    l.Add(new GroupCOA()
                    {
                        AcNo = Sm.GetGrdStr(Grd12, i, 1),
                        AcDesc = Sm.GetGrdStr(Grd12, i, 2)
                    });
                }
            }
        }

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg12(this));
            }
        }

       

        #endregion

        #region Grd 13

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg13(this));
            }
        }

        private void Grd13_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg13(this));
        }

        private void Grd13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd13, e, BtnSave);
            Sm.GrdKeyDown(Grd13, e, BtnFind, BtnSave);
        }
        #endregion

        #region Grd 14

        private void Grd14_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg14(this));
        }

       

        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg14(this));
            }
        }

       

        private void Grd14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd14, e, BtnSave);
            Sm.GrdKeyDown(Grd14, e, BtnFind, BtnSave);
        }

        #endregion

        #region Grd 15
        private void Grd15_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg15(this));
        }

        private void Grd15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd15, e, BtnSave);
            Sm.GrdKeyDown(Grd15, e, BtnFind, BtnSave);
        }

        private void Grd15_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg15(this));
            }
        }
        #endregion

        #region Grd16
        private void Grd16_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmGroupDlg16(this));
        }

        private void Grd16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd16, e, BtnSave);
            Sm.GrdKeyDown(Grd16, e, BtnFind, BtnSave);
        }

        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmGroupDlg16(this));
            }
        }
        #endregion




        #endregion

        #endregion

        #region Class

        internal class GroupCOA
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
        }

        #endregion
    }
}
