﻿namespace RunSystem
{
    partial class FrmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCustomer));
            this.panel3 = new System.Windows.Forms.Panel();
            this.LueCtSegmentCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCtSegmentCode = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.MeeShippingMark = new DevExpress.XtraEditors.MemoExEdit();
            this.LueCtGrpCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtNPWP = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtCtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.ChkAgentInd = new DevExpress.XtraEditors.CheckEdit();
            this.LueCountryCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.BtnCopyGeneral = new DevExpress.XtraEditors.SimpleButton();
            this.LblCopyGeneral = new System.Windows.Forms.Label();
            this.TxtNIB = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtCtName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgContact = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgBankAccount = new System.Windows.Forms.TabPage();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgShipAddress = new System.Windows.Forms.TabPage();
            this.LueCntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueCity2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgNotify = new System.Windows.Forms.TabPage();
            this.MeeNotify = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgCtItCode = new System.Windows.Forms.TabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpAsset = new System.Windows.Forms.TabPage();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpVA = new System.Windows.Forms.TabPage();
            this.LueBankCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.TpFiles = new System.Windows.Forms.TabPage();
            this.PbDownload = new System.Windows.Forms.ProgressBar();
            this.BtnDownloadAll = new DevExpress.XtraEditors.SimpleButton();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtSegmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeShippingMark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtGrpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtShortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAgentInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCountryCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgContact.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.TpgBankAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgShipAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgNotify.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNotify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgCtItCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpAsset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpVA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(846, 0);
            this.panel1.Size = new System.Drawing.Size(70, 453);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(846, 453);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.LueCtSegmentCode);
            this.panel3.Controls.Add(this.LblCtSegmentCode);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.MeeShippingMark);
            this.panel3.Controls.Add(this.LueCtGrpCode);
            this.panel3.Controls.Add(this.TxtNPWP);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.TxtCtShortCode);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.ChkAgentInd);
            this.panel3.Controls.Add(this.LueCountryCode);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.LueCityCode);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.LueCtCtCode);
            this.panel3.Controls.Add(this.TxtCtName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtCtCode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(846, 222);
            this.panel3.TabIndex = 15;
            // 
            // LueCtSegmentCode
            // 
            this.LueCtSegmentCode.EnterMoveNextControl = true;
            this.LueCtSegmentCode.Location = new System.Drawing.Point(149, 192);
            this.LueCtSegmentCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtSegmentCode.Name = "LueCtSegmentCode";
            this.LueCtSegmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtSegmentCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtSegmentCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtSegmentCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtSegmentCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtSegmentCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtSegmentCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtSegmentCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtSegmentCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtSegmentCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtSegmentCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtSegmentCode.Properties.DropDownRows = 30;
            this.LueCtSegmentCode.Properties.NullText = "[Empty]";
            this.LueCtSegmentCode.Properties.PopupWidth = 310;
            this.LueCtSegmentCode.Size = new System.Drawing.Size(301, 20);
            this.LueCtSegmentCode.TabIndex = 37;
            this.LueCtSegmentCode.ToolTip = "F4 : Show/hide list";
            this.LueCtSegmentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtSegmentCode.EditValueChanged += new System.EventHandler(this.LueCtSegmentCode_EditValueChanged);
            // 
            // LblCtSegmentCode
            // 
            this.LblCtSegmentCode.AutoSize = true;
            this.LblCtSegmentCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCtSegmentCode.ForeColor = System.Drawing.Color.Red;
            this.LblCtSegmentCode.Location = new System.Drawing.Point(6, 195);
            this.LblCtSegmentCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCtSegmentCode.Name = "LblCtSegmentCode";
            this.LblCtSegmentCode.Size = new System.Drawing.Size(140, 14);
            this.LblCtSegmentCode.TabIndex = 36;
            this.LblCtSegmentCode.Text = "Customer Segmentation";
            this.LblCtSegmentCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(64, 174);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 14);
            this.label17.TabIndex = 34;
            this.label17.Text = "Shipping Mark";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(106, 90);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 14);
            this.label16.TabIndex = 26;
            this.label16.Text = "Group";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeShippingMark
            // 
            this.MeeShippingMark.EnterMoveNextControl = true;
            this.MeeShippingMark.Location = new System.Drawing.Point(149, 171);
            this.MeeShippingMark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeShippingMark.Name = "MeeShippingMark";
            this.MeeShippingMark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeShippingMark.Properties.Appearance.Options.UseFont = true;
            this.MeeShippingMark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeShippingMark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeShippingMark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeShippingMark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeShippingMark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeShippingMark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeShippingMark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeShippingMark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeShippingMark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeShippingMark.Properties.MaxLength = 400;
            this.MeeShippingMark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeShippingMark.Properties.ShowIcon = false;
            this.MeeShippingMark.Size = new System.Drawing.Size(301, 20);
            this.MeeShippingMark.TabIndex = 35;
            this.MeeShippingMark.ToolTip = "F4 : Show/hide text";
            this.MeeShippingMark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeShippingMark.ToolTipTitle = "Run System";
            // 
            // LueCtGrpCode
            // 
            this.LueCtGrpCode.EnterMoveNextControl = true;
            this.LueCtGrpCode.Location = new System.Drawing.Point(149, 87);
            this.LueCtGrpCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtGrpCode.Name = "LueCtGrpCode";
            this.LueCtGrpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtGrpCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtGrpCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtGrpCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtGrpCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtGrpCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtGrpCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtGrpCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtGrpCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtGrpCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtGrpCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtGrpCode.Properties.DropDownRows = 30;
            this.LueCtGrpCode.Properties.NullText = "[Empty]";
            this.LueCtGrpCode.Properties.PopupWidth = 310;
            this.LueCtGrpCode.Size = new System.Drawing.Size(301, 20);
            this.LueCtGrpCode.TabIndex = 27;
            this.LueCtGrpCode.ToolTip = "F4 : Show/hide list";
            this.LueCtGrpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtGrpCode.EditValueChanged += new System.EventHandler(this.LueCtGrpCode_EditValueChanged);
            // 
            // TxtNPWP
            // 
            this.TxtNPWP.EnterMoveNextControl = true;
            this.TxtNPWP.Location = new System.Drawing.Point(149, 150);
            this.TxtNPWP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNPWP.Name = "TxtNPWP";
            this.TxtNPWP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNPWP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNPWP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNPWP.Properties.Appearance.Options.UseFont = true;
            this.TxtNPWP.Properties.MaxLength = 80;
            this.TxtNPWP.Size = new System.Drawing.Size(301, 20);
            this.TxtNPWP.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(105, 153);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 14);
            this.label13.TabIndex = 32;
            this.label13.Text = "NPWP";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtShortCode
            // 
            this.TxtCtShortCode.EnterMoveNextControl = true;
            this.TxtCtShortCode.Location = new System.Drawing.Point(149, 45);
            this.TxtCtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtShortCode.Name = "TxtCtShortCode";
            this.TxtCtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtShortCode.Properties.MaxLength = 80;
            this.TxtCtShortCode.Size = new System.Drawing.Size(301, 20);
            this.TxtCtShortCode.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(77, 49);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 14);
            this.label12.TabIndex = 22;
            this.label12.Text = "Short Code";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkAgentInd
            // 
            this.ChkAgentInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkAgentInd.Location = new System.Drawing.Point(366, 2);
            this.ChkAgentInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkAgentInd.Name = "ChkAgentInd";
            this.ChkAgentInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkAgentInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAgentInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkAgentInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkAgentInd.Properties.Appearance.Options.UseFont = true;
            this.ChkAgentInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkAgentInd.Properties.Caption = "Agent";
            this.ChkAgentInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAgentInd.Size = new System.Drawing.Size(67, 22);
            this.ChkAgentInd.TabIndex = 19;
            // 
            // LueCountryCode
            // 
            this.LueCountryCode.EnterMoveNextControl = true;
            this.LueCountryCode.Location = new System.Drawing.Point(149, 108);
            this.LueCountryCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCountryCode.Name = "LueCountryCode";
            this.LueCountryCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountryCode.Properties.Appearance.Options.UseFont = true;
            this.LueCountryCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountryCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCountryCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountryCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCountryCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountryCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCountryCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCountryCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCountryCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCountryCode.Properties.DropDownRows = 30;
            this.LueCountryCode.Properties.NullText = "[Empty]";
            this.LueCountryCode.Properties.PopupWidth = 310;
            this.LueCountryCode.Size = new System.Drawing.Size(301, 20);
            this.LueCountryCode.TabIndex = 29;
            this.LueCountryCode.ToolTip = "F4 : Show/hide list";
            this.LueCountryCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCountryCode.Validated += new System.EventHandler(this.LueCountryCode_Validated);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(96, 111);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 14);
            this.label11.TabIndex = 28;
            this.label11.Text = "Country";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.BtnCopyGeneral);
            this.panel4.Controls.Add(this.LblCopyGeneral);
            this.panel4.Controls.Add(this.TxtNIB);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.TxtEmail);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TxtPostalCd);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.TxtMobile);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtPhone);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.MeeAddress);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.TxtFax);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(463, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(379, 218);
            this.panel4.TabIndex = 38;
            // 
            // BtnCopyGeneral
            // 
            this.BtnCopyGeneral.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyGeneral.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyGeneral.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyGeneral.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopyGeneral.Appearance.Options.UseBackColor = true;
            this.BtnCopyGeneral.Appearance.Options.UseFont = true;
            this.BtnCopyGeneral.Appearance.Options.UseForeColor = true;
            this.BtnCopyGeneral.Appearance.Options.UseTextOptions = true;
            this.BtnCopyGeneral.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyGeneral.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyGeneral.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopyGeneral.Image")));
            this.BtnCopyGeneral.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyGeneral.Location = new System.Drawing.Point(345, 177);
            this.BtnCopyGeneral.Name = "BtnCopyGeneral";
            this.BtnCopyGeneral.Size = new System.Drawing.Size(24, 21);
            this.BtnCopyGeneral.TabIndex = 56;
            this.BtnCopyGeneral.ToolTip = "Show Employee List";
            this.BtnCopyGeneral.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyGeneral.ToolTipTitle = "Run System";
            this.BtnCopyGeneral.Click += new System.EventHandler(this.BtnCopyGeneral_Click);
            // 
            // LblCopyGeneral
            // 
            this.LblCopyGeneral.AutoSize = true;
            this.LblCopyGeneral.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCopyGeneral.ForeColor = System.Drawing.Color.Green;
            this.LblCopyGeneral.Location = new System.Drawing.Point(270, 180);
            this.LblCopyGeneral.Name = "LblCopyGeneral";
            this.LblCopyGeneral.Size = new System.Drawing.Size(71, 14);
            this.LblCopyGeneral.TabIndex = 55;
            this.LblCopyGeneral.Tag = "Copy Data From Existing Employee";
            this.LblCopyGeneral.Text = "Copy Data";
            // 
            // TxtNIB
            // 
            this.TxtNIB.EnterMoveNextControl = true;
            this.TxtNIB.Location = new System.Drawing.Point(85, 129);
            this.TxtNIB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNIB.Name = "TxtNIB";
            this.TxtNIB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNIB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNIB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNIB.Properties.Appearance.Options.UseFont = true;
            this.TxtNIB.Properties.MaxLength = 40;
            this.TxtNIB.Size = new System.Drawing.Size(289, 20);
            this.TxtNIB.TabIndex = 52;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(54, 133);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 14);
            this.label14.TabIndex = 51;
            this.label14.Text = "NIB";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(85, 87);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 40;
            this.TxtEmail.Size = new System.Drawing.Size(289, 20);
            this.TxtEmail.TabIndex = 48;
            this.TxtEmail.Validated += new System.EventHandler(this.TxtEmail_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(33, 153);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 53;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(85, 24);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Size = new System.Drawing.Size(289, 20);
            this.TxtPostalCd.TabIndex = 42;
            this.TxtPostalCd.Validated += new System.EventHandler(this.TxtPostalCd_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(9, 28);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 14);
            this.label6.TabIndex = 41;
            this.label6.Text = "Postal Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(85, 108);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 40;
            this.TxtMobile.Size = new System.Drawing.Size(289, 20);
            this.TxtMobile.TabIndex = 50;
            this.TxtMobile.Validated += new System.EventHandler(this.TxtMobile_Validated);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(85, 150);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(289, 20);
            this.MeeRemark.TabIndex = 54;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(39, 112);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 14);
            this.label10.TabIndex = 49;
            this.label10.Text = "Mobile";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(38, 49);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 43;
            this.label7.Text = "Phone";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(85, 45);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 40;
            this.TxtPhone.Size = new System.Drawing.Size(289, 20);
            this.TxtPhone.TabIndex = 44;
            this.TxtPhone.Validated += new System.EventHandler(this.TxtPhone_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(46, 91);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 14);
            this.label9.TabIndex = 47;
            this.label9.Text = "Email";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(55, 69);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 14);
            this.label8.TabIndex = 45;
            this.label8.Text = "Fax";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(85, 3);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 400;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(289, 20);
            this.MeeAddress.TabIndex = 40;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(30, 6);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 39;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(85, 66);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 40;
            this.TxtFax.Size = new System.Drawing.Size(289, 20);
            this.TxtFax.TabIndex = 46;
            this.TxtFax.Validated += new System.EventHandler(this.TxtFax_Validated);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(300, 2);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(63, 22);
            this.ChkActInd.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(119, 132);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 14);
            this.label4.TabIndex = 30;
            this.label4.Text = "City";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCityCode
            // 
            this.LueCityCode.EnterMoveNextControl = true;
            this.LueCityCode.Location = new System.Drawing.Point(149, 129);
            this.LueCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityCode.Name = "LueCityCode";
            this.LueCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityCode.Properties.DropDownRows = 30;
            this.LueCityCode.Properties.NullText = "[Empty]";
            this.LueCityCode.Properties.PopupWidth = 400;
            this.LueCityCode.Size = new System.Drawing.Size(301, 20);
            this.LueCityCode.TabIndex = 31;
            this.LueCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityCode.Validated += new System.EventHandler(this.LueCityCode_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(90, 69);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "Category";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCtCode
            // 
            this.LueCtCtCode.EnterMoveNextControl = true;
            this.LueCtCtCode.Location = new System.Drawing.Point(149, 66);
            this.LueCtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCtCode.Name = "LueCtCtCode";
            this.LueCtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCtCode.Properties.DropDownRows = 30;
            this.LueCtCtCode.Properties.NullText = "[Empty]";
            this.LueCtCtCode.Properties.PopupWidth = 310;
            this.LueCtCtCode.Size = new System.Drawing.Size(301, 20);
            this.LueCtCtCode.TabIndex = 25;
            this.LueCtCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCtCode.EditValueChanged += new System.EventHandler(this.LueCtCtCode_EditValueChanged);
            // 
            // TxtCtName
            // 
            this.TxtCtName.EnterMoveNextControl = true;
            this.TxtCtName.Location = new System.Drawing.Point(149, 24);
            this.TxtCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtName.Name = "TxtCtName";
            this.TxtCtName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtCtName.Properties.MaxLength = 1000;
            this.TxtCtName.Size = new System.Drawing.Size(301, 20);
            this.TxtCtName.TabIndex = 21;
            this.TxtCtName.Validated += new System.EventHandler(this.TxtCtName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(52, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 14);
            this.label2.TabIndex = 20;
            this.label2.Text = "Customer Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(149, 3);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 16;
            this.TxtCtCode.Size = new System.Drawing.Size(144, 20);
            this.TxtCtCode.TabIndex = 17;
            this.TxtCtCode.Validated += new System.EventHandler(this.TxtCtCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(55, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "Customer Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgContact);
            this.tabControl1.Controls.Add(this.TpgBankAccount);
            this.tabControl1.Controls.Add(this.TpgShipAddress);
            this.tabControl1.Controls.Add(this.TpgNotify);
            this.tabControl1.Controls.Add(this.TpgCtItCode);
            this.tabControl1.Controls.Add(this.TpAsset);
            this.tabControl1.Controls.Add(this.TpVA);
            this.tabControl1.Controls.Add(this.TpFiles);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 222);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(846, 231);
            this.tabControl1.TabIndex = 57;
            // 
            // TpgContact
            // 
            this.TpgContact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgContact.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgContact.Controls.Add(this.Grd1);
            this.TpgContact.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgContact.Location = new System.Drawing.Point(4, 26);
            this.TpgContact.Name = "TpgContact";
            this.TpgContact.Size = new System.Drawing.Size(838, 201);
            this.TpgContact.TabIndex = 0;
            this.TpgContact.Text = "Contact Person";
            this.TpgContact.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(834, 197);
            this.Grd1.TabIndex = 58;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // TpgBankAccount
            // 
            this.TpgBankAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgBankAccount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgBankAccount.Controls.Add(this.LueBankCode);
            this.TpgBankAccount.Controls.Add(this.Grd2);
            this.TpgBankAccount.Location = new System.Drawing.Point(4, 26);
            this.TpgBankAccount.Name = "TpgBankAccount";
            this.TpgBankAccount.Size = new System.Drawing.Size(764, 0);
            this.TpgBankAccount.TabIndex = 1;
            this.TpgBankAccount.Text = "Bank Account";
            this.TpgBankAccount.UseVisualStyleBackColor = true;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(5, 23);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 20;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 500;
            this.LueBankCode.Size = new System.Drawing.Size(171, 20);
            this.LueBankCode.TabIndex = 59;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            this.LueBankCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBankCode_KeyDown);
            this.LueBankCode.Leave += new System.EventHandler(this.LueBankCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 0);
            this.Grd2.TabIndex = 58;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TpgShipAddress
            // 
            this.TpgShipAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgShipAddress.Controls.Add(this.LueCntCode);
            this.TpgShipAddress.Controls.Add(this.LueCity2);
            this.TpgShipAddress.Controls.Add(this.Grd3);
            this.TpgShipAddress.Location = new System.Drawing.Point(4, 26);
            this.TpgShipAddress.Name = "TpgShipAddress";
            this.TpgShipAddress.Size = new System.Drawing.Size(838, 201);
            this.TpgShipAddress.TabIndex = 3;
            this.TpgShipAddress.Text = "Shipping Address";
            this.TpgShipAddress.UseVisualStyleBackColor = true;
            // 
            // LueCntCode
            // 
            this.LueCntCode.EnterMoveNextControl = true;
            this.LueCntCode.Location = new System.Drawing.Point(20, 24);
            this.LueCntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCntCode.Name = "LueCntCode";
            this.LueCntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCntCode.Properties.Appearance.Options.UseFont = true;
            this.LueCntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCntCode.Properties.DropDownRows = 20;
            this.LueCntCode.Properties.NullText = "[Empty]";
            this.LueCntCode.Properties.PopupWidth = 500;
            this.LueCntCode.Size = new System.Drawing.Size(281, 20);
            this.LueCntCode.TabIndex = 59;
            this.LueCntCode.ToolTip = "F4 : Show/hide list";
            this.LueCntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCntCode.EditValueChanged += new System.EventHandler(this.LueCntCode_EditValueChanged);
            this.LueCntCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCntCode_KeyDown);
            this.LueCntCode.Leave += new System.EventHandler(this.LueCntCode_Leave);
            this.LueCntCode.Validated += new System.EventHandler(this.LueCntCode_Validated);
            // 
            // LueCity2
            // 
            this.LueCity2.EnterMoveNextControl = true;
            this.LueCity2.Location = new System.Drawing.Point(305, 23);
            this.LueCity2.Margin = new System.Windows.Forms.Padding(5);
            this.LueCity2.Name = "LueCity2";
            this.LueCity2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity2.Properties.Appearance.Options.UseFont = true;
            this.LueCity2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCity2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCity2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCity2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCity2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCity2.Properties.DropDownRows = 20;
            this.LueCity2.Properties.NullText = "[Empty]";
            this.LueCity2.Properties.PopupWidth = 500;
            this.LueCity2.Size = new System.Drawing.Size(267, 20);
            this.LueCity2.TabIndex = 60;
            this.LueCity2.ToolTip = "F4 : Show/hide list";
            this.LueCity2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCity2.EditValueChanged += new System.EventHandler(this.LueCity2_EditValueChanged);
            this.LueCity2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCity2_KeyDown);
            this.LueCity2.Leave += new System.EventHandler(this.LueCity2_Leave);
            this.LueCity2.Validated += new System.EventHandler(this.LueCity2_Validated);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(838, 201);
            this.Grd3.TabIndex = 58;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgNotify
            // 
            this.TpgNotify.Controls.Add(this.MeeNotify);
            this.TpgNotify.Controls.Add(this.Grd4);
            this.TpgNotify.Location = new System.Drawing.Point(4, 26);
            this.TpgNotify.Name = "TpgNotify";
            this.TpgNotify.Padding = new System.Windows.Forms.Padding(3);
            this.TpgNotify.Size = new System.Drawing.Size(764, 0);
            this.TpgNotify.TabIndex = 4;
            this.TpgNotify.Text = "Notify Party";
            this.TpgNotify.UseVisualStyleBackColor = true;
            // 
            // MeeNotify
            // 
            this.MeeNotify.EnterMoveNextControl = true;
            this.MeeNotify.Location = new System.Drawing.Point(76, 25);
            this.MeeNotify.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNotify.Name = "MeeNotify";
            this.MeeNotify.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.Appearance.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNotify.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNotify.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNotify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNotify.Properties.MaxLength = 400;
            this.MeeNotify.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeNotify.Properties.ShowIcon = false;
            this.MeeNotify.Size = new System.Drawing.Size(263, 20);
            this.MeeNotify.TabIndex = 59;
            this.MeeNotify.ToolTip = "F4 : Show/hide text";
            this.MeeNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNotify.ToolTipTitle = "Run System";
            this.MeeNotify.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeNotify_KeyDown);
            this.MeeNotify.Leave += new System.EventHandler(this.MeeNotify_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(3, 3);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(758, 0);
            this.Grd4.TabIndex = 58;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgCtItCode
            // 
            this.TpgCtItCode.Controls.Add(this.Grd5);
            this.TpgCtItCode.Location = new System.Drawing.Point(4, 26);
            this.TpgCtItCode.Name = "TpgCtItCode";
            this.TpgCtItCode.Size = new System.Drawing.Size(764, 0);
            this.TpgCtItCode.TabIndex = 5;
            this.TpgCtItCode.Text = "Customer\'s Item Code";
            this.TpgCtItCode.UseVisualStyleBackColor = true;
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(764, 0);
            this.Grd5.TabIndex = 58;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd5_EllipsisButtonClick);
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpAsset
            // 
            this.TpAsset.Controls.Add(this.DteDueDt);
            this.TpAsset.Controls.Add(this.Grd6);
            this.TpAsset.Location = new System.Drawing.Point(4, 26);
            this.TpAsset.Name = "TpAsset";
            this.TpAsset.Padding = new System.Windows.Forms.Padding(3);
            this.TpAsset.Size = new System.Drawing.Size(764, 0);
            this.TpAsset.TabIndex = 6;
            this.TpAsset.Text = "Asset";
            this.TpAsset.UseVisualStyleBackColor = true;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(148, 25);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(125, 20);
            this.DteDueDt.TabIndex = 59;
            this.DteDueDt.Visible = false;
            this.DteDueDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDueDt_KeyDown);
            this.DteDueDt.Leave += new System.EventHandler(this.DteDueDt_Leave);
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(3, 3);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(758, 0);
            this.Grd6.TabIndex = 58;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // TpVA
            // 
            this.TpVA.Controls.Add(this.LueBankCode2);
            this.TpVA.Controls.Add(this.Grd7);
            this.TpVA.Location = new System.Drawing.Point(4, 26);
            this.TpVA.Name = "TpVA";
            this.TpVA.Size = new System.Drawing.Size(764, 0);
            this.TpVA.TabIndex = 7;
            this.TpVA.Text = "Virtual Account";
            this.TpVA.UseVisualStyleBackColor = true;
            // 
            // LueBankCode2
            // 
            this.LueBankCode2.EnterMoveNextControl = true;
            this.LueBankCode2.Location = new System.Drawing.Point(80, 24);
            this.LueBankCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode2.Name = "LueBankCode2";
            this.LueBankCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode2.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode2.Properties.DropDownRows = 20;
            this.LueBankCode2.Properties.NullText = "[Empty]";
            this.LueBankCode2.Properties.PopupWidth = 500;
            this.LueBankCode2.Size = new System.Drawing.Size(171, 20);
            this.LueBankCode2.TabIndex = 59;
            this.LueBankCode2.ToolTip = "F4 : Show/hide list";
            this.LueBankCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode2.EditValueChanged += new System.EventHandler(this.LueBankCode2_EditValueChanged);
            this.LueBankCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBankCode2_KeyDown);
            this.LueBankCode2.Leave += new System.EventHandler(this.LueBankCode2_Leave);
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 0);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(764, 0);
            this.Grd7.TabIndex = 58;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd7_AfterCommitEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // TpFiles
            // 
            this.TpFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpFiles.Controls.Add(this.PbDownload);
            this.TpFiles.Controls.Add(this.BtnDownloadAll);
            this.TpFiles.Controls.Add(this.Grd8);
            this.TpFiles.Location = new System.Drawing.Point(4, 26);
            this.TpFiles.Name = "TpFiles";
            this.TpFiles.Padding = new System.Windows.Forms.Padding(3);
            this.TpFiles.Size = new System.Drawing.Size(764, 0);
            this.TpFiles.TabIndex = 8;
            this.TpFiles.Text = "Files";
            // 
            // PbDownload
            // 
            this.PbDownload.Location = new System.Drawing.Point(108, 5);
            this.PbDownload.Name = "PbDownload";
            this.PbDownload.Size = new System.Drawing.Size(680, 19);
            this.PbDownload.TabIndex = 60;
            // 
            // BtnDownloadAll
            // 
            this.BtnDownloadAll.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownloadAll.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownloadAll.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownloadAll.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnDownloadAll.Appearance.Options.UseBackColor = true;
            this.BtnDownloadAll.Appearance.Options.UseFont = true;
            this.BtnDownloadAll.Appearance.Options.UseForeColor = true;
            this.BtnDownloadAll.Appearance.Options.UseTextOptions = true;
            this.BtnDownloadAll.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownloadAll.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownloadAll.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownloadAll.Location = new System.Drawing.Point(8, 3);
            this.BtnDownloadAll.Name = "BtnDownloadAll";
            this.BtnDownloadAll.Size = new System.Drawing.Size(96, 21);
            this.BtnDownloadAll.TabIndex = 59;
            this.BtnDownloadAll.Text = "Download All";
            this.BtnDownloadAll.ToolTip = "Download All";
            this.BtnDownloadAll.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownloadAll.ToolTipTitle = "Run System";
            this.BtnDownloadAll.Click += new System.EventHandler(this.BtnDownloadAll_Click);
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(3, 28);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(788, 187);
            this.Grd8.TabIndex = 61;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd8_EllipsisButtonClick);
            this.Grd8.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd8_RequestCellToolTipText);
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // SFD
            // 
            this.SFD.FileName = " ";
            // 
            // FrmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 453);
            this.Name = "FrmCustomer";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtSegmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeShippingMark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtGrpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtShortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAgentInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCountryCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNIB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgContact.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.TpgBankAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgShipAddress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgNotify.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeNotify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgCtItCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpAsset.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpVA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpFiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgContact;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgBankAccount;
        private System.Windows.Forms.TabPage TpgShipAddress;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit TxtEmail;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit TxtFax;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit TxtPostalCd;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCityCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueCtCtCode;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.TextEdit TxtCtName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtCtCode;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private DevExpress.XtraEditors.LookUpEdit LueCity2;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LueCountryCode;
        private DevExpress.XtraEditors.LookUpEdit LueCntCode;
        private DevExpress.XtraEditors.CheckEdit ChkAgentInd;
        private System.Windows.Forms.TabPage TpgNotify;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.MemoExEdit MeeNotify;
        private System.Windows.Forms.TabPage TpgCtItCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private DevExpress.XtraEditors.TextEdit TxtNPWP;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.TextEdit TxtCtShortCode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage TpAsset;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private DevExpress.XtraEditors.TextEdit TxtNIB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueCtGrpCode;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.MemoExEdit MeeShippingMark;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.TabPage TpVA;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode2;
        public DevExpress.XtraEditors.SimpleButton BtnCopyGeneral;
        private System.Windows.Forms.Label LblCopyGeneral;
        private System.Windows.Forms.TabPage TpFiles;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        public DevExpress.XtraEditors.SimpleButton BtnDownloadAll;
        private System.Windows.Forms.ProgressBar PbDownload;
        private DevExpress.XtraEditors.LookUpEdit LueCtSegmentCode;
        private System.Windows.Forms.Label LblCtSegmentCode;
    }
}