﻿#region Update
/*
    10/10/2017 [WED] tambah parameter NumberOfInventoryUomCode
    10/10/2017 [WED] tambah kolom qty dan uom 2 dan 3
    10/10/2017 [WED] tambah reference di header, dari QC Parameter
    10/10/2017 [WED] tambah field TotalQty1, TotalQty2, TotalQty3 di header
    29/11/2017 [WED] tambah field Work Center
    11/01/2018 [WED] QC Workcenter, item yang sama bisa dipilih berkali kali
    06/03/2018 [ARI] tambah department
    12/06/2018 [TKG] konversi inventory UoM di informasi quantity
    25/11/2021 [TYO/IOK] Menambah kolom Inspected Qty berdasarkan parameter IsQCPlanningUseInspectedQty
    06/12/2021 [YOG/IOK] Pada menu QC Planning (01110103), bisa planning lebih dari 1 baris untuk 1 item/batch dalam satu dokumen. Karena parameter yang digunakan berbeda-beda untuk setiap 1 item dengan batch# yang sama tersebut. Menggunakan parameter IsQCParameterInDetail
    13/01/2022 [SET/IOK] ketika memilih mengisi kolom Location, kolom Inspected Quantity pada detail tidak bisa diisi.
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmQCPlanning : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty;
        internal FrmQCPlanningFind FrmFind;
        internal string 
            mQCDefaultStatus = string.Empty,
            mQCStatusDesc= string.Empty,
            mNumberOfInventoryUomCode = string.Empty;
        internal bool mIsQCPlanningUseInspectedQty = false,
            mIsQCParameterInDetail = false;
        private int mStateInd = 0;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmQCPlanning(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "QC Planning";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sm.SetControlNumValueZero(new List<DXE.TextEdit>
                {
                    TxtRefValue, TxtTotalQty1, TxtTotalQty2, TxtTotalQty3
                }, 0);
                SetFormControl(mState.View);

                Sl.SetLueWhsCode(ref LueWhsCode);
                SetLueWorkCenter(ref LueWorkCenter);
                SetLueQCPCode(ref LueQCPCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode);
                LueQCParameter.Visible = false;


                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mIsQCParameterInDetail)
                {
                    SetLueQCPCode(ref LueQCParameter, string.Empty);
                    
                    int y = 42;
                    label3.Visible = false;
                    label16.Visible = false;
                    LueQCPCode.Visible = false;
                    TxtRefValue.Visible = false;
                    LblTotalQty1.Top = LblTotalQty1.Top - y;
                    LblTotalQty2.Top = LblTotalQty2.Top - y;
                    LblTotalQty3.Top = LblTotalQty3.Top - y;
                    label5.Top = label5.Top - y;
                    TxtTotalQty1.Top = TxtTotalQty1.Top - y;
                    TxtTotalQty2.Top = TxtTotalQty2.Top - y;
                    TxtTotalQty3.Top = TxtTotalQty3.Top - y;
                    MeeRemark.Top = MeeRemark.Top - y;
                    panel2.Height = panel2.Height - y;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                       //0
                       "QCStatusCode",
                        
                        //1-5
                        "Status",
                        "DNo",
                        "Cancel",
                        "Old Cancel",
                        "",

                        //6-10
                        "",
                        "Status",
                        "Status",
                        "Item's Code",
                        "",
                        
                        //11-15
                        "Local Code",
                        "Item's Name",
                        "Batch#",
                        "ParameterCode",
                        "Parameter",
                        

                        //16-20
                        "Reference",
                        "Lot",
                        "Bin",
                        "Quantity",
                        "UoM",
                        

                        //21-25
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Inspected"+Environment.NewLine+"Quantity",
                        
                        //26-27
                        "UoM",
                        "Remark",

                    },
                     new int[]
                    {
                        //0
                        0, 
 
                        //1-5
                        150, 20, 50, 50, 20, 
                        
                        //6-10
                        20, 400, 400, 80, 20, 
                        
                        //11-15
                        100, 250, 200, 60, 150, 
                        
                        //16-20
                        80, 80, 80, 80, 80, 
                        
                        //21-25
                        80, 80, 80, 80, 80,

                        //26-27
                        80, 400
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5, 6, 10 });
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 19, 21, 23, 25 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 14, 15, 16 }, false);

            if (mNumberOfInventoryUomCode == "1")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24 }, false);
            }

            if (mNumberOfInventoryUomCode == "2")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 23, 24 }, false);
            }

            Sm.GrdColInvisible(Grd1, new int[] { 25, 26 }, mIsQCPlanningUseInspectedQty);
            Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, mIsQCParameterInDetail);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

       
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, MeeRemark, LueQCPCode, LueQCParameter, TxtRefValue, TxtTotalQty1, TxtTotalQty2, TxtTotalQty3, LueWorkCenter, LueDeptCode
                    }, true);

                    if (mNumberOfInventoryUomCode == "1")
                    {
                        LblTotalQty2.Visible = TxtTotalQty2.Visible = LblTotalQty3.Visible = TxtTotalQty3.Visible = false;
                    }

                    if (mNumberOfInventoryUomCode == "2")
                    {
                        LblTotalQty3.Visible = TxtTotalQty3.Visible = false;
                    }

                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 });
                    TxtDocNo.Focus();
                    ChkActiveInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, MeeRemark, LueQCPCode, LueQCParameter, LueWorkCenter, LueDeptCode
                    }, false);
                    
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 5, 6, 10, 15, 24, 27 });
                    if (mIsQCPlanningUseInspectedQty)
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 25 });
                    DteDocDt.Focus();
                    ChkActiveInd.Checked = true;
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3 });

                    if(mIsQCPlanningUseInspectedQty)
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 25 });

                    TxtDocNo.Focus();
                    ChkActiveInd.Properties.ReadOnly = false;
                    break;
                
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode,  MeeRemark, LueQCPCode, LueQCParameter, LueWorkCenter, LueDeptCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtRefValue, TxtTotalQty1, TxtTotalQty2, TxtTotalQty3
            }, 0);
            ClearGrd();
            ChkActiveInd.Checked = false;
            SetLueQCPCode(ref LueQCPCode, string.Empty);
            if(mIsQCParameterInDetail)
                SetLueQCPCode(ref LueQCParameter, string.Empty);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 16, 19, 21, 23, 25 });
            ComputeSubTotal();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmQCPlanningFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            mStateInd = 1;
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            mStateInd = 2;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0 && mStateInd == 1)
                {
                    InsertData();
                }
                else if (TxtDocNo.Text.Length != 0 && mStateInd == 2)
                {
                    Edit_Data();
                }
                else
                    CancelData();
                    
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetLueQCPCode(ref LueQCPCode, string.Empty);
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
          
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "QCPlanning", "TblQCPlanningHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveQCPlanningHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(SaveQCPlanningDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsInvalidFilter() ||
                (!mIsQCParameterInDetail && Sm.IsLueEmpty(LueQCPCode, "Parameter")) ||
                (!mIsQCParameterInDetail && Sm.IsTxtEmpty(TxtRefValue, "Reference", true)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsInspectedQtyValid();
        }

        private bool IsInvalidFilter()
        {
            if (Sm.GetLue(LueWorkCenter).Length <= 0 && Sm.GetLue(LueWhsCode).Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Location or Work Center is still empty.");
                LueWhsCode.Focus();
                return true;
            }
            else if (Sm.GetLue(LueWorkCenter).Length > 0 && Sm.GetLue(LueWhsCode).Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Fill only Location or Work Center.");
                LueWorkCenter.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Status is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 9, false, "Item is empty.") ||
                    (Sm.GetLue(LueWorkCenter).Length <= 0 && Sm.IsGrdValueEmpty(Grd1, Row, 19, true, "Quantity is empty.")) ||
                    (Sm.GetLue(LueWorkCenter).Length <= 0 && Sm.IsGrdValueEmpty(Grd1, Row, 21, true, "Quantity 2 is empty.")) ||
                    (Sm.GetLue(LueWorkCenter).Length <= 0 && Sm.IsGrdValueEmpty(Grd1, Row, 23, true, "Quantity 3 is empty.")) ||
                    (mIsQCParameterInDetail && Sm.IsGrdValueEmpty(Grd1, Row, 15, false, "Parameter is empty."))
                    ) return true;
            }
            return false;
        }

        private bool IsInspectedQtyValid()
        {
            decimal 
                Qty1 = 0m,
                Qty2 = 0m;

            for (int r = 0; r < Grd1.Rows.Count -1 ; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 21).Length != 0) Qty1 = Sm.GetGrdDec(Grd1, r, 21);
                if (Sm.GetGrdStr(Grd1, r, 25).Length != 0) Qty2 = Sm.GetGrdDec(Grd1, r, 25); 

                if (Qty2 > Qty1)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Inspected Quantity : " + Sm.GetGrdDec(Grd1, r, 25) + Environment.NewLine +
                        "is bigger than Quantity");
                    return true;
                }
            }
            

            return false;
            
            
        }

        private MySqlCommand SaveQCPlanningHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT INTO TblQCPlanningHdr(DocNo, DocDt, ActInd, WhsCode, WorkCenterDocNo, ");
            if (!mIsQCParameterInDetail)
                SQL.AppendLine("QCPCode, ");
            SQL.AppendLine("DeptCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("VALUES(@DocNo, @DocDt, 'Y', @WhsCode, @WorkCenterDocNo, ");
            if (!mIsQCParameterInDetail)
                SQL.AppendLine("@QCPCode, ");
            SQL.AppendLine("@DeptCode, @Remark, @CreateBy, CurrentDateTime()) ");
            SQL.AppendLine("ON DUPLICATE KEY ");
            SQL.AppendLine("   UPDATE ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WorkCenterDocNo", Sm.GetLue(LueWorkCenter));
            if (!mIsQCParameterInDetail)
                Sm.CmParam<String>(ref cm, "@QCPCode", Sm.GetLue(LueQCPCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveQCPlanningDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblQCPlanningDtl(DocNo, DNo, CancelInd, Status, ItCode, BatchNo, Lot, Bin, Qty, Qty2, Qty3,  ");
            if (mIsQCPlanningUseInspectedQty)
                SQLDtl.AppendLine("InspectedQty, ");
            if (mIsQCParameterInDetail)
                SQLDtl.AppendLine("QCPCode, ");
            SQLDtl.AppendLine("Remark, CreateBy, CreateDt)");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, 'N', @Status, @ItCode, @BatchNo, @Lot, @Bin, @Qty, @Qty2, @Qty3, ");
            if (mIsQCPlanningUseInspectedQty)
                SQLDtl.AppendLine("@InspectedQty, ");
            if (mIsQCParameterInDetail)
                SQLDtl.AppendLine("@QCPCode, ");
            SQLDtl.AppendLine("@Remark, @CreateBy, CurrentDateTime()) ");
            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 13));
            if (mIsQCParameterInDetail)
                Sm.CmParam<String>(ref cm, "@QCPCode", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 23));
            if(mIsQCPlanningUseInspectedQty)
                Sm.CmParam<Decimal>(ref cm, "@InspectedQty", Sm.GetGrdDec(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void Edit_Data()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;

            //string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "QCPlanning", "TblQCPlanningHdr");

            var cml = new List<MySqlCommand>();


            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    cml.Add(EditQCPlanningDtl(TxtDocNo.Text, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand EditQCPlanningDtl(string DocNo, int Row)
        {
            

            var SQL = new StringBuilder();
            SQL.AppendLine("Update TblQCPlanningDtl ");
            SQL.AppendLine("Set InspectedQty = @InspectedQty , LastUpBy=@CreateBy, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo = @DNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<string>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@InspectedQty", Sm.GetGrdDec(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion
        #region Cancel Data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            EditQCPCancel();
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Quality control document ", false) ||
                IsDataAlreadyInspected() ||
                IsCancelIndEditedAlready();
        }

        private bool IsDataAlreadyInspected()
        {
            var SQLInspected = new StringBuilder();
            string DNumber = string.Empty;
            int deactiveable = 0;

            for (int RowDtl = 0; RowDtl < Grd1.Rows.Count; RowDtl++)
            {
                DNumber = Sm.Right("00" + (RowDtl + 1).ToString(), 3);

                SQLInspected.AppendLine("SELECT * ");
                SQLInspected.AppendLine("FROM TblQCPlanningDtl A ");
                //SQLInspected.AppendLine("INNER JOIN TblMaterialQCInspectionHdr B ON A.DocNo = B.QCPlanningDocNo ");
                //SQLInspected.AppendLine("INNER JOIN TblMaterialQCInspectionDtl C ON B.DocNo = C.DocNo AND A.DNo = C.QCPlanningDNo ");
                SQLInspected.AppendLine("WHERE A.DocNo = @DocNo ");
                SQLInspected.AppendLine("AND A.Status != '2'; ");
                //SQLInspected.AppendLine("AND A.CancelInd = 'Y'; ");

                var cm = new MySqlCommand() { CommandText = SQLInspected.ToString() };

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (Sm.IsDataExist(cm))
                {
                    deactiveable = 1;
                    break;
                }
                else
                    deactiveable = 2;
            }

            if (deactiveable == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "This document is being inspected and unable to deactivate.");
                return true;
            }

            return false;
        }

        private bool IsCancelIndEditedAlready()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblQCPlanningHdr Where DocNo=@DocNo And ActInd='N'; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is already non-active.");
                return true;
            }
            return false;
        }

        private void EditQCPCancel()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblQCPlanningHdr Set ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion


        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowQCPHdr(DocNo);
                ShowQCPDtl(DocNo);
                ComputeSubTotal();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowQCPHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ActInd, WhsCode, WorkCenterDocNo, ");
            if (!mIsQCParameterInDetail)
                SQL.AppendLine("QCPCode, ");
            else
                SQL.AppendLine("Null As QCPCode, ");
            SQL.AppendLine("DeptCode, Remark From TblQCPlanningHdr Where DocNo = @DocNo ");

            Sm.ShowDataInCtrl(
                    ref cm,
                   SQL.ToString(),
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "ActInd", "WhsCode", "WorkCenterDocNo", "QCPCode", 
                        "DeptCode", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWorkCenter, Sm.DrStr(dr, c[4]));
                        if (!mIsQCParameterInDetail)
                            SetLueQCPCode(ref LueQCPCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueQCPCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[6]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowQCPDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.Status, C.OptDesc As StatusDesc, ");
            SQL.AppendLine("A.ItCode, A.BatchNo, A.Lot, A.Bin, A.Qty, A.Qty2, A.Qty3, ");
            if (mIsQCPlanningUseInspectedQty)
                SQL.AppendLine("A.InspectedQty, ");
            else
                SQL.AppendLine("NULL AS InspectedQty, ");
            if (mIsQCParameterInDetail)
                SQL.AppendLine("A.QCPCode, D.QCPDesc, ");
            else
                SQL.AppendLine("Null As QCPCode, Null As QCPDesc, ");
            SQL.AppendLine("A.Remark, B.ItCodeInternal, B.ItName, B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, B.InventoryUomCode2 InventoryUomCode4  ");
            SQL.AppendLine("From TblQCPlanningDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblOption C On C.OptCat='QCStatus' And A.Status=C.OptCode ");
            if (mIsQCParameterInDetail)
                SQL.AppendLine("Inner Join TblQCParameter D On D.QCPCode = A.QCPCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "Status", "StatusDesc", "ItCode", "ItCodeInternal",  
                    
                    //6-10
                    "ItName","BatchNo", "QCPCode", "QCPDesc", "Lot",   
                    
                    //11-15
                    "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                    
                    //16-20
                    "Qty3", "InventoryUomCode3", "InspectedQty", "InventoryUomCode4", "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 11);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 19, 12);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 13);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 21, 14);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 15);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 23, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 20);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 19, 21, 23, 25 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetLueWorkCenter(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.WorkCenterDocNo As Col1, C.DocName As Col2 ");
            SQL.AppendLine("From TblMaterialPlanningHdr A ");
            SQL.AppendLine("Inner Join TblMaterialPlanningDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo = C.DocNo ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("And C.ActiveInd = 'Y' ");
            SQL.AppendLine("Group By B.WorkCenterDocNo, C.DocName ");
            SQL.AppendLine("Order By C.DocName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void AddSubtotal()
        {
            if (Grd1.Rows.Count > 1 && iGSubtotalManager.ShowSubtotalsInCells == false)
            {
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 19, 21, 23 });
            }
        }

        internal void ComputeSubTotal()
        {
            decimal Qty1 = 0m, Qty2 = 0m, Qty3 = 0m;

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    Qty1 += Sm.GetGrdDec(Grd1, i, 19);
                    Qty2 += Sm.GetGrdDec(Grd1, i, 21);
                    Qty3 += Sm.GetGrdDec(Grd1, i, 23);
                }
            }

            TxtTotalQty1.EditValue = Sm.FormatNum(Qty1, 0);
            TxtTotalQty2.EditValue = Sm.FormatNum(Qty2, 0);
            TxtTotalQty3.EditValue = Sm.FormatNum(Qty3, 0);
        }

        private void GetParameter()
        {
            mQCDefaultStatus = Sm.GetParameter("QCDefaultStatus");
            mQCStatusDesc = Sm.GetValue("SELECT OptDesc FROM tbloption WHERE OptCode = '" + mQCDefaultStatus + "' AND OptCat = 'QCStatus'");
            mNumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            mIsQCPlanningUseInspectedQty = Sm.GetParameterBoo("IsQCPlanningUseInspectedQty");
            mIsQCParameterInDetail = Sm.GetParameterBoo("IsQCParameterInDetail");
        }

        internal void SetLueQCPCode(ref DXE.LookUpEdit Lue, string QCPCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT QCPCode AS Col1, QCPDesc AS Col2");
            SQL.AppendLine("FROM TblQCParameter ");
            SQL.AppendLine("WHERE ActInd='Y' ");

            if (QCPCode.Length != 0)
                SQL.AppendLine("AND QCPCode=@QCPCode ");            

            SQL.AppendLine("ORDER BY QCPDesc;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@QCPCode", QCPCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal string GetSelectedItem()
        {
            #region old code
            //var SQL = string.Empty;
            //if (Grd1.Rows.Count != 1)
            //{
            //    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
            //        {
            //            if (SQL.Length != 0) SQL += ", ";
            //            SQL +=
            //                "'" +
            //                Sm.GetGrdStr(Grd1, Row, 2) +
            //                "'";
            //        }
            //    }
            //}
            //return (SQL.Length == 0 ? "'XXX'" : SQL);
            #endregion

            #region new code
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 9).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            Sm.GetGrdStr(Grd1, Row, 17) +
                            Sm.GetGrdStr(Grd1, Row, 18) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
            #endregion
        }

        private void ValidateParameterInDetail(int Row)
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (i != Row)
                    for (int y = i + 1; y < Grd1.Rows.Count; y++)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 14) == Sm.GetGrdStr(Grd1, y, 14) && Sm.GetGrdStr(Grd1, i, 9) == Sm.GetGrdStr(Grd1, y, 9) && Sm.GetGrdStr(Grd1, i, 14).Length != 0 && Sm.GetGrdStr(Grd1, i, 9).Length != 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Cannot use same parameter for the same item!");
                            Grd1.Cells[Row, 14].Value = null;
                            Grd1.Cells[Row, 15].Value = null;
                        }
                    }
            }

        }
        #endregion

        #endregion

        #region Event

        #region Grid Events

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.GetLue(LueWorkCenter).Length > 0 && Sm.GetLue(LueWhsCode).Length <= 0)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 19, 21, 23, 25 }, e);

                
                //if (e.ColIndex == 16)
                //{
                //    Grd1.Cells[e.RowIndex, 18].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 16);
                //    Grd1.Cells[e.RowIndex, 20].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 16);
                //}

                //if (e.ColIndex == 18)
                //{
                //    Grd1.Cells[e.RowIndex, 20].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 18);
                //}

                if (e.ColIndex == 16)
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 9, 19, 21, 23, 20, 22, 24);

                if (e.ColIndex == 18)
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 9, 21, 19, 23, 22, 20, 24);

                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 20), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 19);

                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 20), Sm.GetGrdStr(Grd1, e.RowIndex, 24)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 23, Grd1, e.RowIndex, 19);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 22), Sm.GetGrdStr(Grd1, e.RowIndex, 24)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 23, Grd1, e.RowIndex, 21);

                ComputeSubTotal();
            }
             

        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 5 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
                    {
                        e.DoDefault = false;
                        if (Sm.GetLue(LueWorkCenter).Length <= 0 && Sm.GetLue(LueWhsCode).Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Location or Work Center is still empty.");
                            LueWhsCode.Focus();
                            return;
                        }
                        else if (Sm.GetLue(LueWorkCenter).Length > 0 && Sm.GetLue(LueWhsCode).Length > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Fill only Location or Work Center.");
                            LueWorkCenter.Focus();
                            return;
                        }
                        else
                        {
                            if (Sm.GetLue(LueWorkCenter).Length > 0 && Sm.GetLue(LueWhsCode).Length <= 0)
                            {
                                Sm.FormShowDialog(new FrmQCPlanningDlg(this, Sm.GetLue(LueWorkCenter)));
                            }
                            else
                            {
                                Sm.FormShowDialog(new FrmQCPlanningDlg(this, Sm.GetLue(LueWhsCode)));
                            }
                        }
                    }

                    if (Sm.IsGrdColSelected(new int[] { 0, 1, 5, 6 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 4 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 19, 21, 23 });
                    }
                }
                else
                {
                    if (e.ColIndex == 3 && (Sm.GetGrdBool(Grd1, e.RowIndex, 4) || Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length == 0))
                        e.DoDefault = false;
                }

                if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length == 0)
                {
                    e.DoDefault = false;
                }

                if (Sm.IsGrdColSelected(new int[] { 15 }, e.ColIndex) && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
                {
                    if (e.ColIndex == 15) LueRequestEdit(Grd1, LueQCParameter, ref fCell, ref fAccept, e);
                }

            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (Sm.GetLue(LueWorkCenter).Length <= 0 && Sm.GetLue(LueWhsCode).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Location or Work Center is still empty.");
                    LueWhsCode.Focus();
                    return;
                }
                else if (Sm.GetLue(LueWorkCenter).Length > 0 && Sm.GetLue(LueWhsCode).Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Fill only Location or Work Center.");
                    LueWorkCenter.Focus();
                    return;
                }
                else
                {
                    if (Sm.GetLue(LueWorkCenter).Length > 0 && Sm.GetLue(LueWhsCode).Length <= 0)
                    {
                        Sm.FormShowDialog(new FrmQCPlanningDlg2(this, Sm.GetLue(LueWorkCenter)));
                    }
                    else
                    {
                        Sm.FormShowDialog(new FrmQCPlanningDlg(this, Sm.GetLue(LueWhsCode)));
                    }
                }
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeSubTotal();
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            var Parameter = Sm.GetGrdStr(Grd, fCell.RowIndex, 14);
            if (Parameter.Length == 0)
                Lue.EditValue = null;
            else
            {
                SetLueQCPCode(ref LueQCParameter, string.Empty);
                Sm.SetLue(Lue, Parameter);
            }
            Lue.Visible = true;
            Lue.Focus();
            fAccept = true;
        }

        #endregion

        #region Misc Control Events

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            ClearGrd();
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 16, 18, 20, 22 });
        }

        private void LueWorkCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWorkCenter, new Sm.RefreshLue1(SetLueWorkCenter));
            ClearGrd();
        }

        private void LueQCPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueQCPCode, new Sm.RefreshLue2(SetLueQCPCode), string.Empty);
            if (Sm.GetLue(LueQCPCode).Length > 0)
            {
                decimal RefValue = Decimal.Parse(Sm.GetValue("Select IfNull(RefValue, 0) From TblQCParameter Where QCPCode = '" + Sm.GetLue(LueQCPCode) + "' Limit 1;"));
                TxtRefValue.EditValue = Sm.FormatNum(RefValue, 0);
            }
            else
            {
                TxtRefValue.EditValue = Sm.FormatNum(0m, 0);
            }
            ClearGrd();
        }

        private void LueQCParameter_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsQCParameterInDetail)
            {
                Sm.RefreshLookUpEdit(LueQCParameter, new Sm.RefreshLue2(SetLueQCPCode), string.Empty);
            }
        }

        private void LueQCParameter_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd1, ref fAccept, e);
            }
        }

        private void LueQCParameter_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {

                if (LueQCParameter.Visible && fAccept && fCell.ColIndex == 15)
                {
                    if (Sm.GetLue(LueQCParameter).Length == 0)
                        Grd1.Cells[fCell.RowIndex, 14].Value =
                        Grd1.Cells[fCell.RowIndex, 15].Value = null;
                    else
                    {
                        Grd1.Cells[fCell.RowIndex, 14].Value = Sm.GetLue(LueQCParameter);
                        Grd1.Cells[fCell.RowIndex, 15].Value = LueQCParameter.GetColumnValue("Col2");

                    }

                    ValidateParameterInDetail(fCell.RowIndex);
                    if (Sm.GetGrdStr(Grd1, fCell.RowIndex, 14).Length > 0)
                    //(Sm.GetLue(LueQCParameter).Length > 0)
                    {
                        decimal RefValue = Decimal.Parse(Sm.GetValue("Select IfNull(RefValue, 0) From TblQCParameter Where QCPCode = '" + Sm.GetGrdStr(Grd1, fCell.RowIndex, 14) + "' Limit 1;"));
                        Grd1.Cells[Grd1.CurCell.RowIndex, 16].Value = Sm.FormatNum(RefValue, 0);
                    }
                    else
                    {
                        Grd1.Cells[Grd1.CurCell.RowIndex, 16].Value = Sm.FormatNum(0m, 0);
                    }
                    LueQCParameter.Visible = false;
                }
            }
        }

        private void TxtRefValue_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtRefValue, 0);
        }

        private void TxtTotalQty1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalQty1, 0);
        }

        private void TxtTotalQty2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalQty2, 0);
        }

        private void TxtTotalQty3_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtTotalQty3, 0);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        #endregion

        #endregion
    }
}
