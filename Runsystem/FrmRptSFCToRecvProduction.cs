﻿#region Update
/*
    20/09/17 [ARI] tambah kolom Remark
    19/07/2018 [ARI] tambah FILTER DAN KOLOM ITEM CATEGORY
    05/03/2018 [TKG] tambah shift
    13/02/2019 [MEY] Menambahkan kolom Machine disamping SFC#
    08/07/2019 [DITA] Ubah source kolom Machine dari Display name asset
    28/09/2020 [TKG/MGI] kalau department di master work center kosong , data tetap muncul
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSFCToRecvProduction : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string mRuleForShowingAssetName = "";

        #endregion

        #region Constructor

        public FrmRptSFCToRecvProduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueItCtCodeActive(ref LueItCtCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.DocName, C.DeptName, J.ProductionShiftName, ");
            SQL.AppendLine("E.ItCode, E.ItName, D.BatchNo, D.Qty, E.PlanningUomCode, D.Qty2, E.PlanningUomCode2, ");
            SQL.AppendLine("F.PWGDocNo, G.PNTDocNo, H.RecvProductionDocNo, I.Remark, A.CreateBy, K.ItCtName, L.AssetName, L.DisplayName  ");
            SQL.AppendLine("From TblShopFloorControlHdr A ");
            SQL.AppendLine("Inner Join TblWorkCenterHdr B On A.WorkCenterDocNo=B.DocNo  ");
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo, ");
            SQL.AppendLine("    group_concat(Distinct T1.DocNo separator ', ') As PWGDocNo "); 
	        SQL.AppendLine("    From TblPWGHdr T1 ");
	        SQL.AppendLine("    Inner Join TblPWGDtl2 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join tblShopFloorControlHdr T3 On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("    And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join tblShopFloorControlDtl T4 ");
            SQL.AppendLine("        On T2.ShopFloorControlDocNo=T4.DocNo ");
            SQL.AppendLine("        And T2.ShopFloorControlDNo=T4.DNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo ");
            SQL.AppendLine(") F ");
            SQL.AppendLine("    On D.DocNo=F.ShopFloorControlDocNo ");
            SQL.AppendLine("    And D.DNo=F.ShopFloorControlDNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.ShopFloorControlDocNo, T.ShopFloorControlDNo, ");
            SQL.AppendLine("    group_concat(Distinct T.DocNo separator ', ') As PNTDocNo ");
            SQL.AppendLine("    From ( "); 
            SQL.AppendLine("        Select T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo, T1.DocNo ");
            SQL.AppendLine("        From TblPNTHdr T1 ");
            SQL.AppendLine("        Inner Join TblPNTDtl5 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join tblShopFloorControlHdr T3 On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Inner Join tblShopFloorControlDtl T4 ");
            SQL.AppendLine("            On T2.ShopFloorControlDocNo=T4.DocNo ");
            SQL.AppendLine("            And T2.ShopFloorControlDNo=T4.DNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T2.ShopFloorControlDocNo, T2.ShopFloorControlDNo, T1.DocNo ");
            SQL.AppendLine("        From TblPNT2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblPNT2Dtl6 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Inner Join tblShopFloorControlHdr T3 On T2.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And (T3.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("        Inner Join TblShopFloorControlDtl T4 ");
            SQL.AppendLine("            On T2.ShopFloorControlDocNo=T4.DocNo ");
            SQL.AppendLine("            And T2.ShopFloorControlDNo=T4.DNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.ShopFloorControlDocNo, T.ShopFloorControlDNo ");
            SQL.AppendLine(") G ");
            SQL.AppendLine("    On D.DocNo=G.ShopFloorControlDocNo ");
            SQL.AppendLine("    And D.DNo=G.ShopFloorControlDNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.ShopFloorControlDocNo, T1.ShopFloorControlDNo, ");
            SQL.AppendLine("    group_concat(Distinct T1.DocNo separator ', ') As RecvProductionDocNo ");
            SQL.AppendLine("    From TblRecvProductionDtl T1 ");
            SQL.AppendLine("    Inner Join tblShopFloorControlHdr T2 On T1.ShopFloorControlDocNo=T2.DocNo ");
            SQL.AppendLine("    And (T2.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T3 ");
            SQL.AppendLine("        On T1.ShopFloorControlDocNo=T3.DocNo ");
            SQL.AppendLine("        And T1.ShopFloorControlDNo=T3.DNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.ShopFloorControlDocNo, T1.ShopFloorControlDNo ");
            SQL.AppendLine(") H ");
            SQL.AppendLine("    On D.DocNo=H.ShopFloorControlDocNo ");
            SQL.AppendLine("    And D.DNo=H.ShopFloorControlDNo ");
            SQL.AppendLine("Left Join TblPPHdr I On A.PPDocNo=I.DocNo ");
            SQL.AppendLine("Left Join TblProductionShift J On A.ProductionShiftCode=J.ProductionShiftCode ");
            SQL.AppendLine("Left Join Tblitemcategory K On E.ItCtCode = K.ItCtCode  ");
            SQL.AppendLine("Left Join TblAsset L On L.AssetCode=A.MachineCode  ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.CancelInd='N' ");
            if (Sm.GetLue(LueItCtCode).Length > 0)
                 SQL.AppendLine("And E.ItCtCode=@ItCtCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "SFC#",
                        "",
                         "Machine",
                        "Date",
                        "Work Center",
                       

                        //6-10
                        "Department",
                        "Shift",
                        "Item's Code",
                        "Item's Name",
                        "Item's Category",
                       
                        
                        //11-15
                         "Batch#",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",
                       
                        
                        //16-20
                         "Wages",
                        "Penalty",
                        "Received From"+Environment.NewLine+"Production",
                        "Remark",
                        "Created By"
                    },
                     new int[] 
                    {
                        //0
                        50,

                        //1-5
                        170, 20, 200, 80, 150, 
                        
                        //6-10
                        150,  80, 150, 200, 200, 

                        //11-15
                        150,  80, 60, 80, 60,
                        
                        //16-19
                        200, 200, 130, 150, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                SetSQL();
                string Filter = " ";
                
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "E.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtWCDocNo.Text, "B.DocName", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt Desc;",
                        new string[]
                        {  
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DocName", "DeptName", "ProductionShiftName", "ItCode", 
                            
                            //6-10
                            "ItName", "BatchNo",  "Qty", "PlanningUomCode", "Qty2", 
                            
                            //11-15
                            "PlanningUomCode2", "PWGDocNo", "PNTDocNo", "RecvProductionDocNo", "Remark",

                            //16-19
                            "CreateBy", "ItCtName", "AssetName", "DisplayName",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            if(mRuleForShowingAssetName == "2")
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 19);
                            else
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                        }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }


        }

        #region Additional Methode
        private void GetParameter()
        {
            mRuleForShowingAssetName = Sm.GetParameter("RuleForShowingAssetName");
        }
        #endregion
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            //if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            //    Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 6));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            //if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            //    Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 6));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work center");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Department");
        }

        private void TxtWCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        
    }
}
