﻿#region Update
/*
    19/04/2017 [TKG] Berdasarkan parameter IsNotCopySalesLocalDocNo utk menentukan default local document#
    20/07/2018 [HAR] modififkasi function save, show, printout, tambah function computuom (ambil dri tableSI jika kosong dari packagingunit),  terkait NW dan GW 
    04/07/2018 [WED] printout untuk IOK, Cnt nya dibikin agar ada angka 0 didepannya biar urutannya bener
    18/07/2018 [ari] printout tambah qty packaging
    20/07/2018 [HAR] bug fixing waktu memunculkan HS code dan stuffing date
    01/08/2018 [ARI] label local document diganti No. invoice (pake parameter localdocument)
    03/08/2017 [TKG] ubah pengisian notify party
    13/08/2017 [TKG] tambah local document# so
    04/09/2018 [HAR] tambah  button download
    12/11/2018 [HAR] ftp tambah parameter buat nentuin format koneksi FTP
    18/11/2018 [TKG] ubah setting container size saat print
    12/11/2020 [HAR/IOK] BUG : di form ambil hs name di printout masih ambil hs code, sekarang ambilnya dari hsname semua
    19/03/2022 [TKG/PHT] merubah GetParameter() dan proses save
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
#endregion

namespace RunSystem
{
    public partial class FrmSI : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = "", mAccessInd = "", mDocNo = "",
            mIsFormPrintOutSI, 
            mIsFormPrintOutSInvPro,
            mLocalDocument = "0";
        internal bool mIsCustomerItemNameMandatory = false;
        private bool 
            mIsLocalDocNoAccesible = false,
            mIsNotCopySalesLocalDocNo = false;
        internal FrmSIFind FrmFind;
        iGCell fCell;
        bool fAccept;
        private string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty,
           mFormatFTPClient = string.Empty;
        private byte[] downloadedData;
        #endregion

        #region Constructor

        public FrmSI(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Shipment Instruction";

            try 
            { 
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                if (mLocalDocument == "1")
                    LblLocalDoc.Text = "      Invoice#";
                base.FrmLoad(sender, e);

                LueBL.Visible = false;
                SetLueCtCode(ref LueCtCode);
               // Sm.SetTme(StfTme,);
                SetLueLoading(ref LueLoading);
                SetLueDischarge(ref LueDischarge);
                SetLueVd(ref LueVdCode);
                SetLueVd(ref LueVdCode2);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
                mIsFormPrintOutSInvPro = Sm.GetParameter("FormPrintOutSInvPro");
                mIsLocalDocNoAccesible = Sm.GetParameterBoo("IsLocalDocNoAccesible");
                mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
                mIsNotCopySalesLocalDocNo = Sm.GetParameterBoo("IsNotCopySalesLocalDocNo");


            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'LocalDocument', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'PortForFTPClient', 'FileSizeMaxUploadFTPClient', 'FormatFTPClient', 'IsNotCopySalesLocalDocNo', 'FormPrintOutSI', ");
            SQL.AppendLine("'FormPrintOutSInvPro', 'IsLocalDocNoAccesible', 'IsCustomerItemNameMandatory' ");

            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsNotCopySalesLocalDocNo": mIsNotCopySalesLocalDocNo = ParValue == "Y"; break;
                            case "IsCustomerItemNameMandatory": mIsCustomerItemNameMandatory = ParValue == "Y"; break;
                            case "IsLocalDocNoAccesible": mIsLocalDocNoAccesible = ParValue == "Y"; break;

                            //string
                            case "FormPrintOutSInvPro": mIsFormPrintOutSInvPro = ParValue; break;
                            case "FormPrintOutSI": mIsFormPrintOutSI = ParValue; break;
                            case "LocalDocument": mLocalDocument = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grid1
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Item's"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "SO#",

                        //6-10
                        "",
                        "SO DNo",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Outstanding"+Environment.NewLine+"Packaging"+Environment.NewLine+"Quantity",
                        "Packaging"+Environment.NewLine+"Quantity",

                        //11-15
                        "Balance",
                        "Outstanding"+Environment.NewLine+"SO",
                        "Requested"+Environment.NewLine+"Quantity (Sales)",
                        "Balance",
                        "UoM"+Environment.NewLine+"(Sales)",

                        //16-20
                        "Stock",
                        "Requested"+Environment.NewLine+"Quantity (Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Gross Weight",
                        "Netto Weight",

                        //21-23
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark",
                        mLocalDocument=="1"?"Sales"+Environment.NewLine+"Contract#":"Local"+Environment.NewLine+"Document#"
                    },
                     new int[] 
                    {
                        20, 
                        80, 200, 20, 150, 150, 
                        20, 50, 100, 100, 100,   
                        80, 100, 100, 80, 80, 
                        40, 130, 80, 100, 100, 
                        100, 150, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 21 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 17, 19, 20 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0, 3, 6, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7, 16 }, false);
            Grd1.Cols[23].Move(8);

            #endregion

            #region Grid2
            Grd2.Cols.Count = 2;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Type",
                        "B/L Requirement",
                    },
                    new int[] 
                    {
                        20, 130
                    }
                );
            Sm.GrdColInvisible(Grd2, new int[] { 0 });
            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7, 16 }, !ChkHideInfoInGrd.Checked);

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, DteStfDt, LueCtCode, TxtSPDocNo, LueNotify, MeeNotify,
                        TxtBL, TxtHsCode, TxtKpbc, MeeReceipt, LueLoading,TxtPEB, TxtSource,
                        MeeDelivery, LueDischarge, MeeMark, MeeRemark2, TxtSize, MeeRemark, 
                        MeeConsignee, TxtLocalDocNo, TxtFeeder, TxtMotherVessel, TxtShippingLine, LueVdCode, LueVdCode2, TxtPIUser, 
                        StuffTme, DteCloseDt, CloseTme, DteLoadingDt, DteDischargeDt
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1,  4, 5, 7, 8, 9, 10, 22 });
                    BtnSP.Enabled = false;
                    BtnSource.Enabled = false;
                    BtnConsignee.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    if (mIsLocalDocNoAccesible)//nampilkan localdocno
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            { 
                            DteDocDt, LueCtCode,LueNotify, DteStfDt,MeeNotify,
                            TxtBL, TxtHsCode, TxtKpbc, MeeReceipt, LueLoading,TxtPEB,  
                            MeeDelivery, MeeMark,MeeRemark2, TxtSize, MeeRemark, MeeConsignee, 
                            TxtFeeder, TxtMotherVessel, TxtShippingLine, LueVdCode, LueVdCode2, TxtPIUser,
                            TxtLocalDocNo, StuffTme, DteCloseDt, CloseTme, DteLoadingDt, DteDischargeDt
                            }, false);
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            { 
                            DteDocDt, LueCtCode,LueNotify, DteStfDt,MeeNotify,
                            TxtBL, TxtHsCode, TxtKpbc, MeeReceipt, LueLoading,TxtPEB,  
                            MeeDelivery, MeeMark,MeeRemark2, TxtSize, MeeRemark, MeeConsignee, 
                            TxtFeeder, TxtMotherVessel, TxtShippingLine, LueVdCode, LueVdCode2, TxtPIUser, StuffTme, 
                             DteCloseDt, CloseTme, DteLoadingDt, DteDischargeDt
                            }, false);
                    }
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 6, 7, 16, 10, 22, });
                    BtnSP.Enabled = true;
                    BtnSource.Enabled = true;
                    BtnConsignee.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (mIsLocalDocNoAccesible)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueNotify, DteStfDt,
                            TxtBL, TxtHsCode, TxtKpbc, MeeReceipt, LueLoading,TxtPEB, MeeNotify,MeeDelivery, MeeMark,
                            MeeRemark2, TxtSize, MeeRemark, MeeConsignee, TxtFeeder, TxtMotherVessel, TxtShippingLine,
                            LueVdCode, LueVdCode2, TxtPIUser, TxtLocalDocNo, StuffTme,  
                            DteCloseDt, CloseTme, DteLoadingDt, DteDischargeDt
                        }, false);
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueNotify, DteStfDt,
                            TxtBL, TxtHsCode, TxtKpbc, MeeReceipt, LueLoading,TxtPEB, MeeNotify,MeeDelivery, MeeMark,
                            MeeRemark2, TxtSize, MeeRemark, MeeConsignee, TxtFeeder, TxtMotherVessel, TxtShippingLine,
                            LueVdCode, LueVdCode2, TxtPIUser, StuffTme,  DteCloseDt, CloseTme, DteLoadingDt, DteDischargeDt
                        }, false);
                    }
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 3, 6, 7, 16, 10, 22, });
                    BtnSP.Enabled = false;
                    BtnSource.Enabled = false;
                    BtnConsignee.Enabled = false;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, DteStfDt, LueCtCode, TxtSPDocNo, LueNotify, MeeNotify, MeeConsignee,
                TxtBL, TxtHsCode, TxtKpbc, MeeReceipt, LueLoading, TxtPEB, TxtSize, TxtSource,
                MeeDelivery, LueDischarge, MeeMark, MeeRemark2, MeeRemark, TxtLocalDocNo, TxtFeeder, 
                TxtMotherVessel, TxtShippingLine, LueVdCode, LueVdCode2, TxtPIUser, TxtLocalDocNo, StuffTme,
                 DteCloseDt, CloseTme, DteLoadingDt, DteDischargeDt
            });
            ChkProforma.Checked = false;
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 11, 12, 13, 14, 17, 19, 20 });
            Sm.FocusGrd(Grd2, 0, 0);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueNotify, MeeNotify, 
                TxtBL, TxtHsCode, TxtKpbc, MeeReceipt, LueLoading, TxtPEB, TxtSize,
                MeeDelivery, LueDischarge, MeeMark,MeeRemark2, MeeRemark, MeeConsignee, TxtSource, 
                TxtFeeder, TxtMotherVessel, TxtShippingLine,
            });
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9, 10, 11, 12, 13, 14, 17, 19, 20 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSIFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteStfDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                LueNotify.DataBindings.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
               InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }


        #endregion

        #region Grid Method

        #region Grid 1 Method

        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer") && !Sm.IsTxtEmpty(TxtSPDocNo, "Shipment Planning", false))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSIDlg(this, Sm.GetLue(LueCtCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 0, 6, 8, 9 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 9 });
                }

                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                        var f1 = new FrmItem(mMenuCode);
                        f1.Tag = mMenuCode;
                        f1.WindowState = FormWindowState.Normal;
                        f1.StartPosition = FormStartPosition.CenterScreen;
                        f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                        f1.ShowDialog();
                }

                if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
                {
                    var f1 = new FrmSO2(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                    f1.ShowDialog();
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeUom();
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer") && !Sm.IsTxtEmpty(TxtSPDocNo, "Shipment Planning", false)) 
                Sm.FormShowDialog(new FrmSIDlg(this, Sm.GetLue(LueCtCode)));
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                    var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.ShowDialog();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex))
                ComputeUom();
        }
   

        #endregion


        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            LueRequestEdit(Grd2, LueBL, ref fCell, ref fAccept, e);
            Sm.GrdRequestEdit(Grd2, e.RowIndex);
            SetLueBL(ref LueBL);
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void  InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = "";
            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SI", "TblSIHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSIHdr(DocNo));
            cml.Add(SaveSIDtl(DocNo));
            cml.Add(SaveSIDtl2(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveSIDtl(DocNo, Row));

            //for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            //    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveSIDtl2(DocNo, Row));

            cml.Add(UpdateSO(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date")||
                Sm.IsDteEmpty(DteStfDt, "Stuffing Date") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsTxtEmpty(TxtSPDocNo, "Shipment Planning", false) ||
                Sm.IsLueEmpty(LueNotify, "Notify Party") ||
                Sm.IsMeeEmpty(MeeConsignee, "Consignee") ||
                Sm.IsMeeEmpty(MeeReceipt, "Place of Receipt") ||
                Sm.IsLueEmpty(LueLoading, "Port of Loading") ||
                Sm.IsMeeEmpty(MeeDelivery, "Place of Delivery") ||
                Sm.IsLueEmpty(LueDischarge, "Port of Discharge")||
                Sm.IsTxtEmpty(TxtSize, "Container Size", false)||
                Sm.IsMeeEmpty(MeeRemark, "Description of Goods") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()||
                IsGrdQtyNotValid()||
                IsSPCancelled()||
                IsSPFinal()
                //IsSpDocNoExist()
                ;
        }

        private bool IsSPCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 From TblSP A " +
                    "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                    "Where A.Status = 'C' And A.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSPDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsSPFinal()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A " +
                    "Where A.Status = 'F' And A.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSPDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Final.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 SO#.");
                return true;
            }

            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 BL Requirement.");
                return true;
            }

            return false;
        }

        private bool IsGrdQtyNotValid()
        {
            string Msg = "";
            
            RecomputeBalance();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                   "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                   "Packaging Unit : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                   "Sales Uom : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                   "SO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine + Environment.NewLine;

                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 8, false, "Uom (Packaging Unit) is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 10, true, Msg + "Requested Quantity (Packaging Unit) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 13, true, Msg + "Requested Quantity (Sales) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 17, true, Msg + "Requested Quantity (Inventory) should not be 0.")
                    )
                    return true;

                if (Sm.GetGrdDec(Grd1, Row, 10) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Packaging quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0) +
                        " ) must be bigger than 0 ");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 10) > Sm.GetGrdDec(Grd1, Row, 9))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Packaging quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0) +
                        " ) should not be bigger than outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9), 0) +
                        " ).");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 13) > Sm.GetGrdDec(Grd1, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 13), 0) +
                        " ) should not be bigger than outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) +
                        " ).");
                    return true;
                }

                if (IsDataExists(
                        "Select DocNo From TblSODtl " +
                        "Where ProcessInd='F' And Locate(Concat('##', DocNo, DNo, '##'), @Param)>0 " +
                        "Limit 1; ",
                        "##" + Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) + "##",
                        Msg + "This data already fulfilled."
                    )) return true;

            }
            return false;
        }

        private bool IsSpDocNoExist()
        {
            if (TxtSPDocNo.Text.Length != 0)
            {
                if (IsDataExists(
                       "Select SPDocNo From TblSIHdr " +
                       "Where SPDocNo=@Param " +
                       "Limit 1; ", TxtSPDocNo.Text,
                       "Shipment Instruction with Shipping Planning (" + TxtSPDocNo.Text + ") already created."
                   )) return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 10, true,
                        "Item Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Quantity should be greater than 0.")
                    ) return true;
            return false;
        }

        private MySqlCommand SaveSIHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Shipment Instruction (Hdr) */ ");
            SQL.AppendLine("Insert Into TblSIHdr(DocNo, LocalDocNo, DocDt, SpDocNo, SpStfDt, SpCtNotifyParty, SpCtNotifyParty2, SpBLNo, SpHSCode, SpConsignee, SpPEB, SpKPBC, SpPlaceReceipt, ");
            SQL.AppendLine("SpPortCode1, SpPortCode1Dt, SpPortCode2, SpPortCode2Dt, SpPlaceDelivery, SpMark, Remark2, SpSize, Description, Feeder, MotherVessel, ShippingLine, VdCode, VdCode2, PIUser, StuffTme, CloseDt, CloseTme, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, @DocDt, @SpDocNo, @SpStfDt, @SpCtNotifyParty, @SpCtNotifyParty2, @SpBLNo, @SpHSCode, @SpConsignee, @SpPEB, @SpKPBC, @SpPlaceReceipt, ");
            SQL.AppendLine("@SpPortCode1, @SpPortCode1Dt, @SpPortCode2, @SpPortCode2Dt, @SpPlaceDelivery, @SpMark, @Remark2, @SpSize, @Description, @Feeder, @MotherVessel, @ShippingLine, @VdCode, @VdCode2, @PIUser, @StuffTme, @CloseDt, @CloseTme, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update LocalDocNo=@LocalDocNo, SpCtNotifyParty=@SpCtNotifyParty, SpCtNotifyParty2=@SpCtNotifyParty2, SpStfDt=@SpStfDt, SpBLNo=@SpBLNo, SpHSCode=@SpHSCode, SpConsignee=@SpConsignee, SpPEB=@SpPEB, ");
            SQL.AppendLine("    SpKPBC=@SpKPBC, SpPlaceReceipt=@SpPlaceReceipt, SpPortCode1=@SpPortCode1, SpPortCode1Dt=@SpPortCode1Dt, SpPortCode2=@SpPortCode2, SpPortCode2Dt=@SpPortCode2Dt, SpPlaceDelivery=@SpPlaceDelivery, SpMark=@SpMark, Remark2=@Remark2, ");
            SQL.AppendLine("    SpSize=@SpSize, Description=@Description, Feeder=@Feeder, MotherVessel=@MotherVessel, ShippingLine=@ShippingLine, VdCode=@VdCode, VdCode2=@VdCode2, PIUser=@PIUser, StuffTme=@StuffTme, CloseDt=@CloseDt, CloseTme=@CloseTme, LastUpBy=@UserCode, LastUpDt=CurrentDateTime();  ");
            
            SQL.AppendLine("Delete From TblSIDtl Where DocNo = @DocNo ; ");
            SQL.AppendLine("Delete From TblSIDtl2 Where DocNo = @DocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SpDocNo", TxtSPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@SpCtNotifyParty", Sm.GetLue(LueNotify));
            Sm.CmParam<String>(ref cm, "@SpCtNotifyParty2", MeeNotify.Text);
            Sm.CmParamDt(ref cm, "@SpStfDt", Sm.GetDte(DteStfDt));
            Sm.CmParam<String>(ref cm, "@StuffTme", Sm.GetTme(StuffTme));
            Sm.CmParamDt(ref cm, "@CloseDt", Sm.GetDte(DteCloseDt));
            Sm.CmParam<String>(ref cm, "@CloseTme", Sm.GetTme(CloseTme));
            Sm.CmParam<String>(ref cm, "@SpBLNo", TxtBL.Text);
            Sm.CmParam<String>(ref cm, "@SpHSCode", TxtHsCode.Text);
            Sm.CmParam<String>(ref cm, "@SpConsignee", MeeConsignee.Text);
            Sm.CmParam<String>(ref cm, "@SpPEB", TxtPEB.Text);
            Sm.CmParam<String>(ref cm, "@SpKPBC", TxtKpbc.Text);
            Sm.CmParam<String>(ref cm, "@SpPlaceReceipt", MeeReceipt.Text);
            Sm.CmParam<String>(ref cm, "@SpPortCode1", Sm.GetLue(LueLoading));
            Sm.CmParamDt(ref cm, "@SpPortCode1Dt", Sm.GetDte(DteLoadingDt));
            Sm.CmParam<String>(ref cm, "@SpPortCode2", Sm.GetLue(LueDischarge));
            Sm.CmParamDt(ref cm, "@SpPortCode2Dt", Sm.GetDte(DteDischargeDt));
            Sm.CmParam<String>(ref cm, "@SpPlaceDelivery", MeeDelivery.Text);
            Sm.CmParam<String>(ref cm, "@SpMark", MeeMark.Text);
            Sm.CmParam<String>(ref cm, "@Remark2", MeeRemark2.Text);
            Sm.CmParam<String>(ref cm, "@SpSize", TxtSize.Text);
            Sm.CmParam<String>(ref cm, "@Description", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Feeder", TxtFeeder.Text);
            Sm.CmParam<String>(ref cm, "@MotherVessel", TxtMotherVessel.Text);
            Sm.CmParam<String>(ref cm, "@ShippingLine", TxtShippingLine.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdCode2", Sm.GetLue(LueVdCode2));
            Sm.CmParam<String>(ref cm, "@PIUser", TxtPIUser.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSIDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Shipment Instruction (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSIDtl(DocNo, DNo, SODocNo, SODNo, QtyPackagingUnit, Qty, QtyInventory, NWRate, GWRate, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @SODocNo_" + r.ToString() +
                        ", @SODNo_" + r.ToString() +
                        ", @QtyPackagingUnit_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @QtyInventory_" + r.ToString() +
                        ", @NWRate_" + r.ToString() +
                        ", @GWRate_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@SODocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<String>(ref cm, "@SODNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@QtyInventory_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 17));
                    Sm.CmParam<Decimal>(ref cm, "NWRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20) / Sm.GetGrdDec(Grd1, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "GWRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 19) / Sm.GetGrdDec(Grd1, r, 10));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 22));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveSIDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();


        //     SQL.AppendLine("Insert Into TblSIDtl(DocNo, DNo, SODocNo, SODNo, QtyPackagingUnit, Qty, QtyInventory, NWRate, GWRate, Remark, CreateBy, CreateDt) ");
        //     SQL.AppendLine("Values(@DocNo, @DNo, @SODocNo, @SODNo, @QtyPackagingUnit, @Qty, @QtyInventory, @NWRate, @GWRate, @Remark, @CreateBy, CurrentDateTime()) ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@SODocNo", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@SODNo", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyInventory", Sm.GetGrdDec(Grd1, Row, 17));
        //    Sm.CmParam<Decimal>(ref cm, "NWRate", Sm.GetGrdDec(Grd1, Row, 20) / Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "GWRate", Sm.GetGrdDec(Grd1, Row, 19) / Sm.GetGrdDec(Grd1, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveSIDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Shipment Instruction (Dtl2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 0).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSIDtl2(DocNo, DNo, BLCode, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @BLCode_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@BLCode_" + r.ToString(), Sm.GetGrdStr(Grd2, r, 0));
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveSIDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblSIDtl2(DocNo, DNo, BLCode, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @BLCode, @CreateBy, CurrentDateTime()) "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@BLCode", Sm.GetGrdStr(Grd2, Row, 0));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateSO(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSODtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Distinct SODocNo, SODNo ");
            SQL.AppendLine("    From TblSIDtl  ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine(") T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.SODocNo, A.SODNo, Sum(IfNull(B.QtyPackagingUnit, 0)) As QtyPackagingUnit ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Distinct SODocNo, SODNo ");
            SQL.AppendLine("        From TblSIDtl  ");
            SQL.AppendLine("        Where DocNo=@DocNo ");
            SQL.AppendLine("    ) A ");
            SQL.AppendLine("    Inner Join TblSIDtl B On A.SODocNo=B.SODocNo And A.SODNo=B.SODNo ");
            SQL.AppendLine("    Inner Join TblSIHdr C On B.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblSP D On C.SPDocNo = D.DocNo And D.Status <> 'C' ");
            SQL.AppendLine("    Group By A.SODocNo, A.SODNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("Set T1.ProcessInd4 =  ");
            SQL.AppendLine("    Case When IfNull(T3.QtyPackagingUnit, 0)=0 Then 'O' ");
            SQL.AppendLine("    Else ");
            SQL.AppendLine("        Case When IfNull(T3.QtyPackagingUnit, 0)>=T1.QtyPackagingUnit Then 'F' Else 'P' End ");
            SQL.AppendLine("End; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowSIHdr(DocNo, 0);
                ShowSIDtl(DocNo);
                ShowSIDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowSIHdr(string DocNo, int Con)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.LocalDocNo, A.DocDt, A.SpDocNo, A.SpStfDt,  " +
                    "B.CtCode, A.SpCtNotifyParty, A.SpCtNotifyParty2, A.SpBLNo, ifnull(C.HSname, A.SpHSCode) As SPHSCode, A.SpConsignee, A.SpPEB, A.SpKPBC, A.SpPlaceReceipt, A.SpPortCode1,  A.SpPortCode1Dt, " +
                    "A.SpPortCode2,  A.SpPortCode2Dt, A.SpPlaceDelivery, A.SpMark, A.Remark2, A.SpSize, A.Description, A.Feeder, A.MotherVessel, A.ShippingLine, A.VdCode, A.VdCode2, " +
                    "A.PIUser, A.StuffTme, A.CloseDt, A.CloseTme " +
                    "From TblSIHdr A "+ 
                    "Inner Join TblSP B On A.SPDocNo = B.DocNo "+
                    "Left Join TBlHS C On A.SPHSCode = C.HSCode "+
                    "Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "LocalDocNo", "DocDt", "SpDocNo", "SpStfDt", "CtCode",   
                        
                        //6-10
                        "SpCtNotifyParty", "SpCtNotifyParty2", "SpBLNo","SpHSCode", "SpConsignee",    
 
                        //11-15
                        "SpPEB", "SpKPBC", "SpPlaceReceipt", "SpPortCode1", "SpPortCode1Dt",  
                       
                        //16-20
                        "SpPortCode2", "SpPortCode2Dt", "SpPlaceDelivery", "SpMark", "Remark2", 

                        //21-25
                        "SpSize", "Description", "Feeder", "MotherVessel", "ShippingLine", 

                        //26-30
                        "VdCode", "VdCode2", "PIUser", "StuffTme", "CloseDt", 
                        
                        //31
                        "CloseTme",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Con == 0)
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                            Sm.SetDte(DteStfDt, Sm.DrStr(dr, c[4]));
                            TxtSPDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        }
                        else
                            TxtSource.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                        SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode), Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueNotify, Sm.DrStr(dr, c[6]));
                        MeeNotify.EditValue =  Sm.DrStr(dr, c[7]);
                        TxtBL.EditValue = Sm.DrStr(dr, c[8]);
                        TxtHsCode.EditValue = Sm.DrStr(dr, c[9]);
                        MeeConsignee.EditValue = Sm.DrStr(dr, c[10]);

                        TxtPEB.EditValue = Sm.DrStr(dr, c[11]);
                        TxtKpbc.EditValue = Sm.DrStr(dr, c[12]);
                        MeeReceipt.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetLue(LueLoading, Sm.DrStr(dr, c[14]));
                        Sm.SetDte(DteLoadingDt, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueDischarge, Sm.DrStr(dr, c[16]));
                        Sm.SetDte(DteDischargeDt, Sm.DrStr(dr, c[17]));
                        MeeDelivery.EditValue = Sm.DrStr(dr, c[18]);
                        MeeMark.EditValue = Sm.DrStr(dr, c[19]);
                        MeeRemark2.EditValue = Sm.DrStr(dr, c[20]);

                        TxtSize.EditValue = Sm.DrStr(dr, c[21]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[22]);
                        TxtFeeder.EditValue = Sm.DrStr(dr, c[23]);
                        TxtMotherVessel.EditValue = Sm.DrStr(dr, c[24]);
                        TxtShippingLine.EditValue = Sm.DrStr(dr, c[25]);
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueVdCode2, Sm.DrStr(dr, c[27]));
                        TxtPIUser.EditValue = Sm.DrStr(dr, c[28]);
                        Sm.SetTme(StuffTme, Sm.DrStr(dr, c[29]));
                        Sm.SetDte(DteCloseDt, Sm.DrStr(dr, c[30]));

                        Sm.SetTme(CloseTme, Sm.DrStr(dr, c[31]));
                    }, true
                );
        }

        public void ShowSIDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.BlCode, C.OptDesc ");
            SQL.AppendLine("From TblSIHdr A ");
            SQL.AppendLine("Inner Join TblSIDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblOption C On B.BlCode=C.OptCode And C.OptCat = 'BLRequirement' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BLCode", 

                    //1-5
                    "OptDesc",
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                }, false, false, true, false
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowSIDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select H.ItCode, I.ItName, I.ItCodeInternal, D.DocNo, C.DNo,  ");
            SQL.AppendLine("C.PackagingUnitUomCode, B.QtyPackagingUnit,  ");
            SQL.AppendLine("(ifnull(C.QtyPackagingUnit, 0)-ifnull(J.QtyPackagingUnit, 0)) As OutstandingPackaging, ");
            SQL.AppendLine("B.Qty, G.PriceUomCode, (ifnull(C.Qty, 0)-ifnull(J.Qty, 0)) AS OutstandingQty, ");
            SQL.AppendLine("B.QtyInventory, I.InventoryUomCode, C.DeliveryDt, (B.QtyPackagingUnit*B.GWRate) As GW, (B.QtyPackagingUnit*B.NWRate) As NW, B.Remark, D.LocalDocNo ");
            SQL.AppendLine("From TblSIHdr A  ");
            SQL.AppendLine("Inner join TblSIDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblSODtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo  ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl H On F.ItemPriceDocNo = H.DocNo And F.ItemPriceDNo = H.Dno ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.SODocNo, T2.SODNo, ");
            SQL.AppendLine("    Sum(T2.QtyPackagingUnit) As QtyPackagingUnit, Sum(T2.Qty) As Qty ");
            SQL.AppendLine("    From TblSIHdr T1  ");
            SQL.AppendLine("    Inner Join TblSIDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblSp T3 On T1.SPDocNo = T3.DocNo And T3.Status != 'C' ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select Distinct SODocNo, SODNo From TblDrDtl Where DocNo=@DocNo ");
            //SQL.AppendLine("    ) T3 On T2.SODocNo = T3.SODocNo And T2.SODNo = T3.SODNo ");
            SQL.AppendLine("    Where T1.DocNo<>@DocNo ");
            SQL.AppendLine("    Group By T2.SODocNo, T2.SODNo ");
            SQL.AppendLine(") J On C.DocNo=J.SODocNo And C.DNo=J.SODNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo ");

           
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
                    //1-5
                    "ItName", "ItCodeInternal", "DocNo",  "DNo", "PackagingUnitUomCode", 
                    //6-10
                    "OutstandingPackaging", "QtyPackagingUnit", "OutstandingQty", "Qty", "PriceUomCode", 
                    //11-15
                    "QtyInventory", "InventoryUomCode", "DeliveryDt", "GW", "NW",
                    
                    //16-17
                    "Remark", "LocalDocNo" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 17);
                }, false, false, true, false
            );
            ComputeUom4ShowData();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 17, 19, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowSPData(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.CtCode, A.CtNotifyParty, A.CtNotifyParty2, A.BLNo, ifnull(B.HSName, A.HSCode) HSCode, "+
                    "A.Consignee, A.PEB, A.KPBC, A.PlaceReceipt, A.PortCode1, A.PortCode2, A.PlaceDelivery, A.Mark, " +
                    "A.Size, A.LocalDocno, A.StfDt From TblSP A "+
                    "Left Join TblHS B On A.HSCode = B.HSCode  "+
                    "Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "CtCode", 
                        
                        //1-5
                        "CtNotifyParty", "CtNotifyParty2", "BLNo", "HSCode", "Consignee",   
                        
                        //6-10
                        "PEB", "KPBC", "PlaceReceipt", "PortCode1","PortCode2", 
 
                        //11-15
                        "PlaceDelivery", "Mark", "Size", "LocalDocno", "StfDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[0]));
                        Sm.SetLue(LueNotify, Sm.DrStr(dr, c[1]));
                        MeeNotify.EditValue = Sm.DrStr(dr, c[2]);
                        TxtBL.EditValue = Sm.DrStr(dr, c[3]);
                        TxtHsCode.EditValue = Sm.DrStr(dr, c[4]);
                        MeeConsignee.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPEB.EditValue = Sm.DrStr(dr, c[6]);
                        TxtKpbc.EditValue = Sm.DrStr(dr, c[7]);
                        MeeReceipt.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetLue(LueLoading, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueDischarge, Sm.DrStr(dr, c[10]));
                        MeeDelivery.EditValue = Sm.DrStr(dr, c[11]);
                        MeeMark.EditValue = Sm.DrStr(dr, c[12]);
                        TxtSize.EditValue = Sm.DrStr(dr, c[13]);
                        TxtLocalDocNo.EditValue = mIsNotCopySalesLocalDocNo?"":Sm.DrStr(dr, c[14]);
                        DteStfDt.DateTime = Sm.ConvertDate(Sm.DrStr(dr, c[15]));
                    }, true
                );
        }

        #endregion

        #region Additional Method

        private void SetLueBL(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'BLRequirement'  Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal string GetSelectedItem()
        {
            var SQL = "";
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += ((SQL.Length != 0 ? ", " : "") + "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'");

            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        public void ComputeUom()
        {
            decimal QtyPackaging = 0m, QtyUomConvert12 = 0m, QtyUomConvert21 = 0m, QtyNConvert = 0m, QtyGConvert = 0m ;
            string UomSales, UomSales1, UomInv;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 10) != 0 && Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                {
                    try
                    {
                        QtyPackaging = Sm.GetGrdDec(Grd1, Row, 10);
                        UomSales = Sm.GetGrdStr(Grd1, Row, 15);
                        UomInv = Sm.GetGrdStr(Grd1, Row, 18);
                        UomSales1 = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' ");

                        string UomConvert12 = Sm.GetValue("Select ifnull(Qty, 0) From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");
                        string UomConvert21 = Sm.GetValue("Select ifnull(Qty2, 0) From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");

                        string NWcon = Sm.GetValue("Select NW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");
                        string GWCon = Sm.GetValue("Select GW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");

                        if (UomConvert12.Length > 0)
                        {
                            QtyUomConvert12 = Decimal.Parse(UomConvert12);
                        }
                        else
                        {
                            QtyUomConvert12 = 0;
                        }

                        if (UomConvert21.Length > 0)
                        {
                            QtyUomConvert21 = Decimal.Parse(UomConvert21);
                        }
                        else
                        {
                            QtyUomConvert21 = 0;
                        }



                        if (NWcon.Length > 0)
                        {
                            QtyNConvert = Decimal.Parse(NWcon);
                        }
                        else
                        {
                            QtyNConvert = 0;
                        }

                        if (GWCon.Length > 0)
                        {
                            QtyGConvert = Decimal.Parse(GWCon);
                        }
                        else
                        {
                            QtyGConvert = 0;
                        }

                        #region UOMSALES
                        if (UomSales1 == UomSales)
                        {
                            Grd1.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert12;
                            decimal bal1 = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            if (bal1 <= 0)
                            {
                                Grd1.Cells[Row, 11].Value = 0m;
                            }
                            else
                            {
                                Grd1.Cells[Row, 11].Value = bal1;
                            }

                            decimal bal2 = Sm.GetGrdDec(Grd1, Row, 12) - Sm.GetGrdDec(Grd1, Row, 13);
                            if (bal2 <= 0)
                            {
                                Grd1.Cells[Row, 14].Value = 0m;
                            }
                            else
                            {
                                Grd1.Cells[Row, 14].Value = bal2;
                            }

                            if (UomSales == UomInv)
                            {
                                Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 13);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }


                        else
                        {
                            Grd1.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert21;
                            decimal bal1 = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                            if (bal1 <= 0)
                            {
                                Grd1.Cells[Row, 11].Value = 0m;
                            }
                            else
                            {
                                Grd1.Cells[Row, 11].Value = bal1;
                            }

                            decimal bal2 = Sm.GetGrdDec(Grd1, Row, 12) - Sm.GetGrdDec(Grd1, Row, 13);
                            if (bal2 <= 0)
                            {
                                Grd1.Cells[Row, 14].Value = 0m;
                            }
                            else
                            {
                                Grd1.Cells[Row, 14].Value = bal2;
                            }


                            if (UomSales == UomInv)
                            {
                                Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 13);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    Grd1.Cells[Row, 17].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                        #endregion

                        Grd1.Cells[Row, 19].Value = Sm.GetGrdDec(Grd1, Row, 10) * QtyGConvert;
                        Grd1.Cells[Row, 20].Value = Sm.GetGrdDec(Grd1, Row, 10) * QtyNConvert;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }

        public void ComputeUom4ShowData()
        {
            decimal  QtyNConvert = 0m, QtyGConvert = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 10) != 0 && Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                {
                    try
                    {
                        Grd1.Cells[Row, 11].Value = Sm.GetGrdDec(Grd1, Row, 9) - Sm.GetGrdDec(Grd1, Row, 10);
                        Grd1.Cells[Row, 14].Value = Sm.GetGrdDec(Grd1, Row, 12) - Sm.GetGrdDec(Grd1, Row, 13);

                        string NWcon = Sm.GetValue("Select NW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");
                        string GWCon = Sm.GetValue("Select GW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ");

                        if (NWcon.Length > 0)
                        {
                            QtyNConvert = Decimal.Parse(NWcon);
                        }
                        else
                        {
                            QtyNConvert = 0;
                        }

                        if (GWCon.Length > 0)
                        {
                            QtyGConvert = Decimal.Parse(GWCon);
                        }
                        else
                        {
                            QtyGConvert = 0;
                        }

                        if (Sm.GetGrdDec(Grd1, Row, 19) == 0)
                        {
                            Grd1.Cells[Row, 19].Value = Sm.GetGrdDec(Grd1, Row, 10) * QtyGConvert;
                        }

                        if (Sm.GetGrdDec(Grd1, Row, 20) == 0)
                        {
                            Grd1.Cells[Row, 20].Value = Sm.GetGrdDec(Grd1, Row, 10) * QtyNConvert;
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }


        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        public static void SetLueLoading(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PortCode As Col1, PortName As Col2 From TblPort " +
                "Where LoadingInd = 'Y' ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueDischarge(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PortCode As Col1, PortName As Col2 From TblPort " +
                "Where DischargeInd = 'Y' ",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueNotify(ref LookUpEdit Lue, string CtCode, string NotifyParty)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct Col1 From (");
            SQL.AppendLine("    Select NotifyParty As Col1 From TblCustomerNotifyParty ");
            SQL.AppendLine("    Where CtCode=@CtCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @NotifyParty As Col1 ");
            SQL.AppendLine(") T Where Col1<>'' Order By Col1;");

            Sm.CmParam<String>(ref cm, "@NotifyParty", NotifyParty);
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            cm.CommandText = SQL.ToString();

            Sm.SetLue1(ref Lue, ref cm, "Notify Party");
        }

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            //if (TxtDocNo.Text.Length == 0)
            //{
            //    SQL.AppendLine("Select Distinct A.CtCode As Col1, B.CtName As Col2 ");
            //    SQL.AppendLine("From ( ");
            //    SQL.AppendLine("Select Distinct CtCode ");
            //    SQL.AppendLine("From TblSOHdr ");
            //    SQL.AppendLine("Where OverSeaInd = 'Y' And Status = 'O' And CancelInd= 'N' ");
            //}
            //else
            //{
                SQL.AppendLine("Select Distinct A.CtCode As Col1, B.CtName As Col2 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Distinct CtCode From TblSOHdr ");
                SQL.AppendLine("Where OverSeaInd = 'Y' And Status = 'O' And CancelInd= 'N' ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Distinct CtCode From TblSP ");
            //}
            SQL.AppendLine(")A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode= B.CtCode And B.ActInd = 'Y' ");
            SQL.AppendLine("Order By B.CtName");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void RecomputeBalance()
        {
            return;

            //var SQL = new StringBuilder();

            //SQL.AppendLine("Select A.DocNo, B.DNo, ");
            //SQL.AppendLine("(B.QtyPackagingUnit-IfNull(C.QtyPackagingUnit, 0)) As OutstandingQtyPackagingUnit,  ");
            //SQL.AppendLine("(B.Qty-IfNull(C.Qty, 0)) As OutstandingQty ");
            //SQL.AppendLine("From TblSOHdr A ");
            //SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd<>'F' ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select T2.SODocNo As DocNo, T2.SODNo As DNo,  ");
            //SQL.AppendLine("    Sum(IfNull(T2.QtyPackagingUnit, 0)) As QtyPackagingUnit, Sum(IfNull(T2.Qty, 0)) As Qty  ");
            //SQL.AppendLine("    From TblSIHdr T1  ");
            //SQL.AppendLine("    Inner Join TblSIDtl T2 On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo And T3.CancelInd='N' And T3.Status Not In ('M', 'F') And T3.CtCode=@CtCode ");
            //SQL.AppendLine("    Inner Join TblSODtl T4 On T2.SODocNo=T4.DocNo And T2.SODNo=T4.DNo and T4.ProcessInd<>'F' ");

            //SQL.AppendLine("    Where Concat(T2.SODocNo, T2.SODNo) In (" + GetSelectedSO2() + ")  ");
            ////tambahan
            //SQL.AppendLine("    And T1.DocNo !=@DocNo ");
            ////
            //SQL.AppendLine("    Group By T2.SODocNo, T2.SODNo ");
            //SQL.AppendLine(") C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            //SQL.AppendLine("Where A.CancelInd='N'  ");
            //SQL.AppendLine("And A.Status Not In ('M', 'F') ");
            //SQL.AppendLine("And A.CtCode=@CtCode ");
            //SQL.AppendLine("And Locate(Concat('##', A.DocNo, B.DNo, '##'), @SelectedSO)>0 ");
            //SQL.AppendLine("Order By A.DocNo, B.DNo; ");

            //var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            //Sm.CmParam<String>(ref cm, "@SelectedSO", GetSelectedSO());
            //Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            //using (var cn = new MySqlConnection(Gv.ConnectionString))
            //{
            //    cn.Open();
            //    cm.Connection = cn;
            //    cm.CommandText = SQL.ToString();
            //    cm.CommandTimeout = 100;
            //    using (var dr = cm.ExecuteReader())
            //    {
            //        var c = Sm.GetOrdinal(dr,
            //            new string[] 
            //            { 
            //                //0
            //               "DocNo",  

            //                //1-3
            //               "DNo", "OutstandingQtyPackagingUnit", "OutstandingQty"
            //            }
            //            );
            //        if (dr.HasRows)
            //        {
            //            Grd1.ProcessTab = true;
            //            Grd1.BeginUpdate();
            //            while (dr.Read())
            //            {
            //                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            //                {
            //                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 0)) &&
            //                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), Sm.DrStr(dr, 1)))
            //                    {
            //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 2);
            //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 3);
            //                        break;
            //                    }
            //                }
            //            }
            //            Grd1.EndUpdate();
            //        }
            //        dr.Close();
            //        ComputeUom();
            //    }
            //}
        }

        private string GetSelectedSO()
        {
            var SQL = "";
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedSO2()
        {
            var SQL = "";
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private void SetLueVd(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select VdCode As Col1,VdName As Col2 from tblvendor Where VdCtCode=(Select Parvalue from tblparameter where parcode='VdCtCodeExim')",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ParPrint()
        {
           if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
           string Doctitle = Sm.GetParameter("DocTitle");
           
           #region PrintOut IOK

           if (Doctitle == "IOK")
            {
                var l = new List<SIHdr>();
                var ldtl = new List<SIDtl>();
                var ldtl2 = new List<SIDtl2>();
                var ldtl5 = new List<SIDtl5>();
                var ldtl6 = new List<SIDtl6>();
                var ldtl7 = new List<SIDtl7>();

                string[] TableName = { "SIHdr", "SIDtl", "SIDtl2","SIDtl5", "SIDtl6", "SIDtl7" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();
               
                #region Header
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL.AppendLine("A.DocNo, A.LocalDocno, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.SPBLNo, A.SpCtNotifyParty, A.SpCtNotifyParty2, A.SPPEB, ifnull(H.HSname, A.SpHSCode) As SPHSCOde, A.SPKPBC, ");
                SQL.AppendLine("A.SPConsignee, A.SpPlaceReceipt, D.PortName As PortLoading,A.SPPlaceDelivery, E.PortName As PortDischarge, A.SpMark, A.SpSize, A.Description, "); 
                SQL.AppendLine("DATE_FORMAT(F.StfDt,'%d %M %Y') As StfDt,  F.SPName, G.UserName, A.Remark2 As Note, A2.Account, A2.SalesContractNo,  A2.LCNo, A2.Remark, ");
                SQL.AppendLine("DATE_FORMAT(A2.LcDt,'%M %d, %Y') As LcDt,  A2.Issued1, A2.Issued2, A2.Issued3, A2.Issued4, A2.ToOrder1, A2.ToOrder2, A2.ToOrder3, A2.ToOrder4 ");
                SQL.AppendLine("From TblSIHdr A ");
                SQL.AppendLine("Inner Join TblPLHdr A2 On A.DocNo= A2.SiDocNo "); //new
                // SQL.AppendLine("Inner Join TblSinv A3 On A2.DocNo=A3.PLDocNo "); //new
                SQL.AppendLine("Left Join TblSIDtl2 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Left Join TblOpTion C On B.BlCode = C.OptCode And C.OPtCat = 'BLRequirement' ");
                SQL.AppendLine("Inner Join TblPort D On A.SPPortCode1 = D.PortCode ");
                SQL.AppendLine("Inner Join TblPort E On A.SPPortCode2 = E.PortCode ");
                SQL.AppendLine("Inner Join TblSP F On A.SpDocNo = F.DocNo ");
                SQL.AppendLine("Inner Join TblUser G On A.CreateBy = G.Usercode ");
                SQL.AppendLine("Left Join TBlHS H On A.SPHSCode = H.HSCode");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "Shipper1",
                         "Shipper2",
                         "Shipper3",
                         "Shipper4",
                         "DocNo",
                         //11-15
                         "LocalDocNo",
                         "DocDt",
                         "SPBLNo",
                         "SpCtNotifyParty",
                         "SpCtNotifyParty2",
                         //16-20
                         "SPPEB",
                         "SPHSCOde",
                         "SPKPBC",
                         "SpConsignee",
                         "SpPlaceReceipt",
                         //21-25
                         "PortLoading",
                         "SPPlaceDelivery",
                         "PortDischarge",
                         "SpMark",
                         "SpSize",
                         //26-30
                         "Description",
                         "StfDt",
                         "SPName",
                         "UserName",
                         "Note",
                         //31-35
                         "Account",
                         "SalesContractNo",
                         "LCNo",
                         "Remark",
                         "LcDt",
                         //36-40
                         "Issued1",
                         "Issued2",
                         "Issued3",
                         "Issued4",
                         "ToOrder1",
                         //41-43
                         "ToOrder2",
                         "ToOrder3",
                         "ToOrder4",
                 
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new SIHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressCity = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                Shipper1 = Sm.DrStr(dr, c[6]),
                                Shipper2 = Sm.DrStr(dr, c[7]),
                                Shipper3 = Sm.DrStr(dr, c[8]),
                                Shipper4 = Sm.DrStr(dr, c[9]),
                                DocNo = Sm.DrStr(dr, c[10]),

                                LocalDocNo = Sm.DrStr(dr, c[11]),
                                DocDt = Sm.DrStr(dr, c[12]),
                                SPBLNo = Sm.DrStr(dr, c[13]),
                                SpCtNotifyParty = Sm.DrStr(dr, c[14]),
                                SpCtNotifyParty2 = Sm.DrStr(dr, c[15]),

                                SPPEB = Sm.DrStr(dr, c[16]),
                                SPHSCOde = Sm.DrStr(dr, c[17]),
                                SPKPBC = Sm.DrStr(dr, c[18]),
                                SpConsignee = Sm.DrStr(dr, c[19]),
                                SpPlaceReceipt = Sm.DrStr(dr, c[20]),

                                PortLoading = Sm.DrStr(dr, c[21]),
                                SPPlaceDelivery = Sm.DrStr(dr, c[22]),
                                PortDischarge = Sm.DrStr(dr, c[23]),
                                SpMark = Sm.DrStr(dr, c[24]),
                                SpSize = Sm.DrStr(dr, c[25]),

                                Description = Sm.DrStr(dr, c[26]),
                                Stufing = Sm.DrStr(dr, c[27]),
                                SPName = Sm.DrStr(dr, c[28]),
                                UserName = Sm.DrStr(dr, c[29]),
                                Note = Sm.DrStr(dr, c[30]),

                                Account = Sm.DrStr(dr, c[31]),
                                SalesContractNo =Sm.DrStr(dr, c[32]),
                                LCNo = Sm.DrStr(dr, c[33]),
                                Remark = Sm.DrStr(dr, c[34]),
                                LcDt = Sm.DrStr(dr, c[35]),
                                 //36-40
                                Issued1 = Sm.DrStr(dr, c[36]),
                                Issued2 = Sm.DrStr(dr, c[37]),
                                Issued3 = Sm.DrStr(dr, c[38]),
                                Issued4 = Sm.DrStr(dr, c[39]),
                                ToOrder1 = Sm.DrStr(dr, c[40]),
                                 //41-43
                                ToOrder2 = Sm.DrStr(dr, c[41]),
                                ToOrder3 = Sm.DrStr(dr, c[42]),
                                ToOrder4 = Sm.DrStr(dr, c[43]),
                                
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion 

                #region Detail

                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select X.DocNo, X.SiDocNo, X.SectionNo, X.Cnt, SUM(X.Qty) As Qty, X.PriceUomCode, SUM(X.NW) As QtyNetto, ");
                    SQLDtl.AppendLine("X.NGUom, SUM(X.GW) As QtyGross, X.NGUom, SUM(X.QtyInventory) As QtyInventory, X.InventoryUomCode, SUM(QtyPL) As QtyPL, group_concat(Distinct X.UserName) As UserName, SUM(X.Qty2) As Qty2 ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("    Select A.DocNo, A.SIDocNo, Right(Concat('00', A2.SectionNo), 2) SectionNo, A2.ItCode, C.PackagingUnitUomCode, ");
                    SQLDtl.AppendLine("    A2.QtyPackagingUnit, A2.Qty, G.PriceUomCode, Round(A2.QtyInventory,4) As QtyInventory, B.InventoryUomCode, ifnull(A2.QtyPL, 0) As QtyPL,  ");
                    SQLDtl.AppendLine("    (A2.QtyPackagingUnit * H.NW) As NW, (Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom,  ");
                    SQLDtl.AppendLine("    (A2.QtyPackagingUnit * H.GW) As GW, F3.userName,  ");
                    SQLDtl.AppendLine("    Case When A2.SectionNo = '1' Then Concat(A.Cnt1, '/', A.seal1) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '2' Then Concat(A.Cnt2, '/', A.seal2) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '3' Then Concat(A.Cnt3, '/', A.seal3) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '4' Then Concat(A.Cnt4, '/', A.seal4) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '5' Then Concat(A.Cnt5, '/', A.seal5) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '6' Then Concat(A.Cnt6, '/', A.seal6) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '7' Then Concat(A.Cnt7, '/', A.seal7) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '8' Then Concat(A.Cnt8, '/', A.seal8) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '9' Then Concat(A.Cnt9, '/', A.seal9) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '10' Then Concat(A.Cnt10, '/', A.seal10) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '11' Then Concat(A.Cnt11, '/', A.seal11) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '12' Then Concat(A.Cnt12, '/', A.seal12) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '13' Then Concat(A.Cnt13, '/', A.seal13) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '14' Then Concat(A.Cnt14, '/', A.seal14) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '15' Then Concat(A.Cnt15, '/', A.seal15) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '16' Then Concat(A.Cnt16, '/', A.seal16) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '17' Then Concat(A.Cnt17, '/', A.seal17) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '18' Then Concat(A.Cnt18, '/', A.seal18) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '19' Then Concat(A.Cnt19, '/', A.seal19) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '20' Then Concat(A.Cnt20, '/', A.seal20) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '21' Then Concat(A.Cnt21, '/', A.seal21) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '22' Then Concat(A.Cnt22, '/', A.seal22) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '23' Then Concat(A.Cnt23, '/', A.seal23) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '24' Then Concat(A.Cnt24, '/', A.seal24) ");
                    SQLDtl.AppendLine("    When A2.SectionNo = '25' Then Concat(A.Cnt25, '/', A.seal25) ");
                    SQLDtl.AppendLine("    end as Cnt, ");
                    SQLDtl.AppendLine("    if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))) As Qty2 ");
                    SQLDtl.AppendLine("    From TblPlhdr A ");
                    SQLDtl.AppendLine("    Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                    SQLDtl.AppendLine("    Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl.AppendLine("    Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo  ");
                    SQLDtl.AppendLine("    Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl.AppendLine("    Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblUser F3 On F2.SpCode = F3.UserCode ");
                    SQLDtl.AppendLine("    Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                    SQLDtl.AppendLine("    Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode ");
                    SQLDtl.AppendLine("    left Join ");
                    SQLDtl.AppendLine("    ( ");
                    SQLDtl.AppendLine("        select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl.AppendLine("    )H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl.AppendLine("    order by A2.SectionNo ");
                    SQLDtl.AppendLine(")X ");
                    SQLDtl.AppendLine("Where X.SIDocNo=@DocNo ");
                    SQLDtl.AppendLine("Group By X.DocNo, X.SiDocNo, X.Cnt order By X.SectionNo ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "Cnt" ,

                     //1-5
                     "Qty" ,
                     "PriceUomCode",
                     "QtyNetto",
                     "NGUoM",
                     "QtyGross",

                     //6-8
                     "QtyInventory",
                     "InventoryUomCode",
                     "QtyPL",
                     "Username",
                     "Qty2"
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new SIDtl()
                            {
                                Cnt = Sm.DrStr(drDtl, cDtl[0]),
                                Qty = Sm.DrDec(drDtl, cDtl[1]),
                                PriceUomCode = Sm.DrStr(drDtl, cDtl[2]),
                                QtyNetto = Sm.DrDec(drDtl, cDtl[3]),
                                NGUom = Sm.DrStr(drDtl, cDtl[4]),
                                QtyGross = Sm.DrDec(drDtl, cDtl[5]),
                                QtyInventory = Sm.DrDec(drDtl, cDtl[6]),
                                InventoryUomCode = Sm.DrStr(drDtl, cDtl[7]),
                                QtyPL = Sm.DrStr(drDtl, cDtl[8]),
                                Username = Sm.DrStr(drDtl, cDtl[9]),
                                Qty2 = Sm.DrDec(drDtl, cDtl[10]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail2
                var cmDtl2 = new MySqlCommand();

                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select A.DocNo, B.OptDesc As BL ");
                    SQLDtl2.AppendLine("From TblSIDtl2 A ");
                    SQLDtl2.AppendLine("Inner Join TblOption B On A.BlCode = B.OptCode And B.OptCat = 'BLRequirement' ");
                    SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "BL" ,
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new SIDtl2()
                            {
                                BL = Sm.DrStr(drDtl2, cDtl2[0]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Detail5
                var cmDtl5 = new MySqlCommand();


                var SQLDtl5 = new StringBuilder();
                using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                {
                    string thc = Sm.GetValue("Select Concat(Right(OptDesc, 1),' ', 'HC') From TblOption Where OptCat = 'ContainerSize' Limit 1;");

                    cnDtl5.Open();
                    cmDtl5.Connection = cnDtl5;

                    SQLDtl5.AppendLine("Select X.DocNo, A2.Dno, A2.SectionNo, A2.ItCode, B.ForeignName As ItName, ");
                    SQLDtl5.AppendLine("A2.Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode, A2.PLNo, ifnull(A2.QtyPL, 0) As QtyPL,  ");
                    SQLDtl5.AppendLine("(A2.QtyPackagingUnit * H.NW) As NW, (Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom,  ");
                    SQLDtl5.AppendLine("(A2.QtyPackagingUnit * H.GW) As GW, F3.userName,  ");
                    SQLDtl5.AppendLine("G.CurCode, F.UPrice, Round((F.UPrice * Round(A2.Qty, 4)), 2) As Amount,");
                    SQLDtl5.AppendLine("Case ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '1' Then Concat('Container', ': ', A.Cnt1, '   ', ifnull(concat('1x',A.Size1, @thc), ''), '   ', 'Seal', ': ', A.seal1) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '2' Then Concat('Container', ': ', A.Cnt2, '   ', ifnull(concat('1x',A.Size2, @thc), ''), '   ', 'Seal', ': ', A.seal2) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '3' Then Concat('Container', ': ', A.Cnt3, '   ', ifnull(concat('1x',A.Size3, @thc), ''), '   ', 'Seal', ': ', A.seal3) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '4' Then Concat('Container', ': ', A.Cnt4, '   ', ifnull(concat('1x',A.Size4, @thc), ''), '   ', 'Seal', ': ', A.seal4) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '5' Then Concat('Container', ': ', A.Cnt5, '   ', ifnull(concat('1x',A.Size5, @thc), ''), '   ', 'Seal', ': ', A.seal5) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '6' Then Concat('Container', ': ', A.Cnt6, '   ', ifnull(concat('1x',A.Size6, @thc), ''), '   ', 'Seal', ': ', A.seal6) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '7' Then Concat('Container', ': ', A.Cnt7, '   ', ifnull(concat('1x',A.Size7, @thc), ''), '   ', 'Seal', ': ', A.seal7) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '8' Then Concat('Container', ': ', A.Cnt8, '   ', ifnull(concat('1x',A.Size8, @thc), ''), '   ', 'Seal', ': ', A.seal8) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '9' Then Concat('Container', ': ', A.Cnt9, '   ', ifnull(concat('1x',A.Size9, @thc), ''), '   ', 'Seal', ': ', A.seal9) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '10' Then Concat('Container', ': ', A.Cnt10, '   ', ifnull(concat('1x',A.Size10, @thc), ''), '   ', 'Seal', ': ', A.seal10) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '11' Then Concat('Container', ': ', A.Cnt11, '   ', ifnull(concat('1x',A.Size11, @thc), ''), '   ', 'Seal', ': ', A.seal11) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '12' Then Concat('Container', ': ', A.Cnt12, '   ', ifnull(concat('1x',A.Size12, @thc), ''), '   ', 'Seal', ': ', A.seal12) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '13' Then Concat('Container', ': ', A.Cnt13, '   ', ifnull(concat('1x',A.Size13, @thc), ''), '   ', 'Seal', ': ', A.seal13) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '14' Then Concat('Container', ': ', A.Cnt14, '   ', ifnull(concat('1x',A.Size14, @thc), ''), '   ', 'Seal', ': ', A.seal14) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '15' Then Concat('Container', ': ', A.Cnt15, '   ', ifnull(concat('1x',A.Size15, @thc), ''), '   ', 'Seal', ': ', A.seal15) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '16' Then Concat('Container', ': ', A.Cnt16, '   ', ifnull(concat('1x',A.Size16, @thc), ''), '   ', 'Seal', ': ', A.seal16) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '17' Then Concat('Container', ': ', A.Cnt17, '   ', ifnull(concat('1x',A.Size17, @thc), ''), '   ', 'Seal', ': ', A.seal17) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '18' Then Concat('Container', ': ', A.Cnt18, '   ', ifnull(concat('1x',A.Size18, @thc), ''), '   ', 'Seal', ': ', A.seal18) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '19' Then Concat('Container', ': ', A.Cnt19, '   ', ifnull(concat('1x',A.Size19, @thc), ''), '   ', 'Seal', ': ', A.seal19) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '20' Then Concat('Container', ': ', A.Cnt20, '   ', ifnull(concat('1x',A.Size20, @thc), ''), '   ', 'Seal', ': ', A.seal20) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '21' Then Concat('Container', ': ', A.Cnt21, '   ', ifnull(concat('1x',A.Size21, @thc), ''), '   ', 'Seal', ': ', A.seal21) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '22' Then Concat('Container', ': ', A.Cnt22, '   ', ifnull(concat('1x',A.Size22, @thc), ''), '   ', 'Seal', ': ', A.seal22) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '23' Then Concat('Container', ': ', A.Cnt23, '   ', ifnull(concat('1x',A.Size23, @thc), ''), '   ', 'Seal', ': ', A.seal23) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '24' Then Concat('Container', ': ', A.Cnt24, '   ', ifnull(concat('1x',A.Size24, @thc), ''), '   ', 'Seal', ': ', A.seal24) ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '25' Then Concat('Container', ': ', A.Cnt25, '   ', ifnull(concat('1x',A.Size25, @thc), ''), '   ', 'Seal', ': ', A.seal25) ");
                    SQLDtl5.AppendLine("end as Cnt, ");
                    SQLDtl5.AppendLine("Case ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '1' Then A.Freight1 - I.THC  ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '2' Then A.Freight2 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '3' Then A.Freight3 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '4' Then A.Freight4 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '5' Then A.Freight5 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '6' Then A.Freight6 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '7' Then A.Freight7 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '8' Then A.Freight8 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '9' Then A.Freight9 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '10' Then A.Freight10 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '11' Then A.Freight11 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '12' Then A.Freight12 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '13' Then A.Freight13 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '14' Then A.Freight14 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '15' Then A.Freight15 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '16' Then A.Freight16 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '17' Then A.Freight17 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '18' Then A.Freight18 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '19' Then A.Freight19 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '20' Then A.Freight20 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '21' Then A.Freight21 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '22' Then A.Freight22 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '23' Then A.Freight23 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '24' Then A.Freight24 - I.THC ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '25' Then A.Freight25 - I.THC ");
                    SQLDtl5.AppendLine("end as Freight, ");
                    SQLDtl5.AppendLine("Case ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '1' Then A.Cnt1 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '2' Then A.Cnt2 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '3' Then A.Cnt3 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '4' Then A.Cnt4 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '5' Then A.Cnt5 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '6' Then A.Cnt6 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '7' Then A.Cnt7 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '8' Then A.Cnt8 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '9' Then A.Cnt9 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '10' Then A.Cnt10 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '11' Then A.Cnt11 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '12' Then A.Cnt12 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '13' Then A.Cnt13 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '14' Then A.Cnt14 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '15' Then A.Cnt15 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '16' Then A.Cnt16 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '17' Then A.Cnt17 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '18' Then A.Cnt18 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '19' Then A.Cnt19 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '20' Then A.Cnt20 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '21' Then A.Cnt21 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '22' Then A.Cnt22 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '23' Then A.Cnt23 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '24' Then A.Cnt24 ");
                    SQLDtl5.AppendLine("When A2.SectionNo = '25' Then A.Cnt25 ");
                    SQLDtl5.AppendLine("end as CntName, ");
                    SQLDtl5.AppendLine("if(J.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, (J.Qty2*A2.Qty)/J.Qty)) As Qty2, C.QtyPackagingUnit, A2.Remark ");
                    SQLDtl5.AppendLine("From TblSInv X ");
                    SQLDtl5.AppendLine("Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                    SQLDtl5.AppendLine("Inner Join TblSIHdr A1 On A.SiDocNo = A1.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                    SQLDtl5.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl5.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo  ");
                    SQLDtl5.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl5.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode ");
                    SQLDtl5.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                    SQLDtl5.AppendLine("Left Join TblItemPackagingUnit J On C.PackagingUnitUomCode = J.UomCOde And A2.ItCode = J.ItCode ");
                    SQLDtl5.AppendLine("left Join ");
                    SQLDtl5.AppendLine("( ");
                    SQLDtl5.AppendLine("  select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl5.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl5.AppendLine("Left Join (Select ParValue As THC From tblParameter Where ParCode = 'THC') I On 0=0 ");
                    SQLDtl5.AppendLine("Where A1.DocNo=@DocNo ");
                    SQLDtl5.AppendLine("Order by  CntName ");

                    cmDtl5.CommandText = SQLDtl5.ToString();

                    Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmDtl5, "@thc", thc);

                    var drDtl5 = cmDtl5.ExecuteReader();
                    var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                    {
                     //0
                     "Cnt" ,

                     //1-5
                     "ItCode" ,
                     "ItName",
                     "PLNo",
                     "QtyPL",
                     "Qty",

                     //6-8
                     "QtyInventory",
                     "NW",
                     "GW",
                     "Username",
                     "SectionNo",
                     //11-15
                     "CurCode",
                     "UPrice",
                     "Amount",
                     "Freight",
                     "Qty2",

                     //16-18
                     "QtyPackagingUnit",
                     "Remark",
                     "DNo"
                    });
                    if (drDtl5.HasRows)
                    {
                        while (drDtl5.Read())
                        {
                            ldtl5.Add(new SIDtl5()
                            {
                                Cnt = Sm.DrStr(drDtl5, cDtl5[0]),
                                ItCode = Sm.DrStr(drDtl5, cDtl5[1]),
                                ItName = Sm.DrStr(drDtl5, cDtl5[2]),
                                PLNo = Sm.DrStr(drDtl5, cDtl5[3]),
                                QtyPL = Sm.DrDec(drDtl5, cDtl5[4]),
                                Qty = Sm.DrDec(drDtl5, cDtl5[5]),
                                QtyInventory = Sm.DrDec(drDtl5, cDtl5[6]),
                                NW = Sm.DrDec(drDtl5, cDtl5[7]),
                                GW = Sm.DrDec(drDtl5, cDtl5[8]),
                                Username = Sm.DrStr(drDtl5, cDtl5[9]),
                                SectionNo = Sm.DrStr(drDtl5, cDtl5[10]),
                                Currency = Sm.DrStr(drDtl5, cDtl5[11]),
                                Price = Sm.DrDec(drDtl5, cDtl5[12]),
                                Amount = Sm.DrDec(drDtl5, cDtl5[13]),
                                Freight = Sm.DrDec(drDtl5, cDtl5[14]),
                                Qty2 = Sm.DrDec(drDtl5, cDtl5[15]),
                                QtyPackagingUnit = Sm.DrDec(drDtl5, cDtl5[16]),
                                Remark = Sm.DrStr(drDtl5, cDtl5[17]),
                                Dno = Sm.DrStr(drDtl5, cDtl5[18]),
                            });
                        }
                    }
                    drDtl5.Close();
                }
                myLists.Add(ldtl5);
                #endregion

                #region Detail6
                var cmDtl6 = new MySqlCommand();

                var SQLDtl6 = new StringBuilder();
                using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl6.Open();
                    cmDtl6.Connection = cnDtl6;

                    SQLDtl6.AppendLine("Select Count(X.Size) As Jumlah, X.Size ");
                    SQLDtl6.AppendLine("From ( ");
                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size1 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size2 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size3 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size4 = B.OptCode And B.OptCat = 'ContainerSize'   ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size5 = B.OptCode And B.OptCat = 'ContainerSize'   ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size6 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size7 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size8 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size9 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size10 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size11 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size12 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size13 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size14 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size15 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size16 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size17 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Union all ");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size18 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size19 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size20 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size21 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size22 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size23 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size24 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");
                    SQLDtl6.AppendLine("Union all");

                    SQLDtl6.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl6.AppendLine("Inner Join TblOpTion B On A.Size25 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl6.AppendLine("Inner Join TblSiHdr C On A.SiDocno=C.DocNo ");
                    SQLDtl6.AppendLine("Where C.DocNo = @DocNo");

                    SQLDtl6.AppendLine(")X ");
                    SQLDtl6.AppendLine("group By X.Size");
                    SQLDtl6.AppendLine("having count(X.Size)>=1");

                    cmDtl6.CommandText = SQLDtl6.ToString();

                    Sm.CmParam<String>(ref cmDtl6, "@DocNo", TxtDocNo.Text);

                    var drDtl6 = cmDtl6.ExecuteReader();
                    var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                    {
                     //0
                     "Jumlah" ,

                     //1-5
                     "Size" ,
                    });
                    if (drDtl6.HasRows)
                    {
                        while (drDtl6.Read())
                        {
                            ldtl6.Add(new SIDtl6()
                            {
                                Jumlah = Sm.DrStr(drDtl6, cDtl6[0]),
                                Size = Sm.DrStr(drDtl6, cDtl6[1]),
                            });
                        }
                    }
                    drDtl6.Close();
                }
                myLists.Add(ldtl6);
                #endregion

                #region Detail7
                var cmDtl7 = new MySqlCommand();

                var SQLDtl7 = new StringBuilder();
                using (var cnDtl7 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl7.Open();
                    cmDtl7.Connection = cnDtl7;

                    SQLDtl7.AppendLine("Select X2.DocNo, SUM(X2.Amount) As Amount, (SUM(X2.Amount) - X2.TotalFre) As Totally, X2.TotalFre From ( ");
                    SQLDtl7.AppendLine("Select A1.DocNo, Round(F.UPrice * Round(A2.Qty, 4), 2)As Amount, G.TotalFre  ");
                    SQLDtl7.AppendLine("From TblSInv X  ");
                    SQLDtl7.AppendLine("Inner Join TblPlhdr A On A.DocNo = X.PlDocNo  ");
                    SQLDtl7.AppendLine("Inner Join TblSiHdr A1 On A.SiDocNo= A1.DocNo  ");
                    SQLDtl7.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl7.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                    SQLDtl7.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl7.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                    SQLDtl7.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl7.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                    SQLDtl7.AppendLine("Inner Join ( ");
                    SQLDtl7.AppendLine("Select S.Docno, SUM(S.f1+S.f2+S.f3+S.f4+S.f5+ ");
                    SQLDtl7.AppendLine("S.f6+S.F7+S.F8+S.f9+S.f10+S.f11+S.f12+S.f13+S.f14+S.f15+S.f16+S.f17+S.f18+S.f19+S.f20+S.f21+S.f22+S.f23+S.f24+S.f25) As totalFre ");
                    SQLDtl7.AppendLine("From ( ");
                    SQLDtl7.AppendLine("Select Distinct A1.DocNo,  ");
                    SQLDtl7.AppendLine("    if(A.Freight1 != 0, (A.Freight1-B.THC), 0) As F1, ");
                    SQLDtl7.AppendLine("     if(A.Freight2 != 0, (A.Freight2-B.THC), 0) As F2, ");
                    SQLDtl7.AppendLine("     if(A.Freight3 != 0, (A.Freight3-B.THC), 0) As F3, ");
                    SQLDtl7.AppendLine("     if(A.Freight4 != 0, (A.Freight4-B.THC), 0) As F4, ");
                    SQLDtl7.AppendLine("     if(A.Freight5 != 0, (A.Freight5-B.THC), 0) As F5, ");
                    SQLDtl7.AppendLine("     if(A.Freight6 != 0, (A.Freight6-B.THC), 0) As F6, ");
                    SQLDtl7.AppendLine("     if(A.Freight7 != 0, (A.Freight7-B.THC), 0) As F7, ");
                    SQLDtl7.AppendLine("     if(A.Freight8 != 0, (A.Freight8-B.THC), 0) As F8, ");
                    SQLDtl7.AppendLine("     if(A.Freight9 != 0, (A.Freight9-B.THC), 0) As F9, ");
                    SQLDtl7.AppendLine("     if(A.Freight10 != 0, (A.Freight10-B.THC), 0) As F10, ");
                    SQLDtl7.AppendLine("     if(A.Freight11 != 0, (A.Freight11-B.THC), 0) As F11, ");
                    SQLDtl7.AppendLine("     if(A.Freight12 != 0, (A.Freight12-B.THC), 0) As F12, ");
                    SQLDtl7.AppendLine("     if(A.Freight13 != 0, (A.Freight13-B.THC), 0) As F13, ");
                    SQLDtl7.AppendLine("     if(A.Freight14 != 0, (A.Freight14-B.THC), 0) As F14, ");
                    SQLDtl7.AppendLine("     if(A.Freight15 != 0, (A.Freight15-B.THC), 0) As F15, ");
                    SQLDtl7.AppendLine("     if(A.Freight16 != 0, (A.Freight16-B.THC), 0) As F16, ");
                    SQLDtl7.AppendLine("     if(A.Freight17 != 0, (A.Freight17-B.THC), 0) As F17, ");
                    SQLDtl7.AppendLine("     if(A.Freight18 != 0, (A.Freight18-B.THC), 0) As F18, ");
                    SQLDtl7.AppendLine("     if(A.Freight19 != 0, (A.Freight19-B.THC), 0) As F19, ");
                    SQLDtl7.AppendLine("     if(A.Freight20 != 0, (A.Freight20-B.THC), 0) As F20, ");
                    SQLDtl7.AppendLine("     if(A.Freight21 != 0, (A.Freight21-B.THC), 0) As F21, ");
                    SQLDtl7.AppendLine("     if(A.Freight22 != 0, (A.Freight22-B.THC), 0) As F22, ");
                    SQLDtl7.AppendLine("     if(A.Freight23 != 0, (A.Freight23-B.THC), 0) As F23, ");
                    SQLDtl7.AppendLine("     if(A.Freight24 != 0, (A.Freight24-B.THC), 0) As F24, ");
                    SQLDtl7.AppendLine("     if(A.Freight25 != 0, (A.Freight25-B.THC), 0) As F25 ");
                    SQLDtl7.AppendLine("    From TblSInv X   ");
                    SQLDtl7.AppendLine("    Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                    SQLDtl7.AppendLine("    Inner Join TblSiHdr A1 On A.SiDocNo= A1.DocNo ");
                    SQLDtl7.AppendLine("    Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo   ");
                    SQLDtl7.AppendLine("    Left Join ( ");
                    SQLDtl7.AppendLine("     Select ParValue As THC From tblParameter Where ParCode = 'THC' ");
                    SQLDtl7.AppendLine("     ) B On 0=0  ");
                    SQLDtl7.AppendLine("    Where A1.DocNo = @Docno ");
                    SQLDtl7.AppendLine(" )S ");
                    SQLDtl7.AppendLine("    Where S.DocNo = @Docno )G On A1.DocNo = G.Docno ");
                    SQLDtl7.AppendLine("Where A1.DocNo=@DocNo ");
                    SQLDtl7.AppendLine(")X2 ");
                    SQLDtl7.AppendLine("Group by X2.DocNo ");
                
                    cmDtl7.CommandText = SQLDtl7.ToString();

                    Sm.CmParam<String>(ref cmDtl7, "@DocNo", TxtDocNo.Text);

                    var drDtl7 = cmDtl7.ExecuteReader();
                    var cDtl7 = Sm.GetOrdinal(drDtl7, new string[] 
                    {
                     //0
                     "Amount",
                     "Totally",
                     "TotalFre",
                    });
                    if (drDtl7.HasRows)
                    {
                        while (drDtl7.Read())
                        {
                            ldtl7.Add(new SIDtl7()
                            {
                                Amount = Sm.DrDec(drDtl7, cDtl7[0]),
                                Terbilang = Convert(Sm.DrDec(drDtl7, cDtl7[0])),
                                Totally = Sm.DrDec(drDtl7, cDtl7[1]),
                                TotalFre = Sm.DrDec(drDtl7, cDtl7[2]),
                            });
                        }
                    }
                    drDtl7.Close();
                }
                myLists.Add(ldtl7);
                #endregion

            
                if (ChkProforma.Checked == true)
                {
                    Sm.PrintReport(mIsFormPrintOutSInvPro, myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport(mIsFormPrintOutSI, myLists, TableName, false);
                }

            }

            #endregion

           #region PrintOut Mai

           if (Doctitle == "MAI")
            {
                var l2 = new List<SIHdr2>();
                var ldtl3 = new List<SIDtl3>();
                var ldtl4 = new List<SIDtl4>();
                var ldtl5 = new List<SIDtl5P>();
                var ldtl8 = new List<SIDtl8>();
                var cm1 = new MySqlCommand();

                string[] TableName = { "SIHdr2", "SIDtl3", "SIDtl4", "SIDtl5P", "SIDtl8" };
                List<IList> myLists = new List<IList>();

                #region Header
                var SQL1 = new StringBuilder();
                SQL1.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL1.AppendLine("A.DocNo, A.LocalDocno, DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt, F.BLNo As SPBLNo, A.SpCtNotifyParty, A.SpCtNotifyParty2, A.SPPEB, A.SPHSCOde, A.SPKPBC, A.SPConsignee, ");
                SQL1.AppendLine("A.SpPlaceReceipt, D.PortName As PortLoading,A.SPPlaceDelivery, E.PortName As PortDischarge, A.SpMark, A.SpSize, A.Description, ");
                SQL1.AppendLine("DATE_FORMAT(F.StfDt,'%M %d, %Y') As StfDt,  F.SPName, G.UserName, A.Remark2 As Note, A2.SoLocal, H.DocNo As PLDocNo, A.Feeder, A.MotherVessel, A.ShippingLine, ");
                SQL1.AppendLine("I.VdName As VdForwarder, J.VdName As VdTransport, I.Address As AddressForwarder, K.CityName As CityForwarder, I.Phone As PhoneForwarder, I.fax As FaxForwarder, ");
                SQL1.AppendLine("J.Address As AddressTransport, K.CityName As CityTransport, J.Phone As PhoneTransport, J.fax As FaxTransport, A.PIUser, Concat(Left(A.StuffTme,2),':',Right(A.StuffTme,2))As StuffTme,");
                SQL1.AppendLine("DATE_FORMAT(A.CloseDt,'%M %d, %Y')As CloseDt, Concat(Left(A.CloseTme,2),':',Right(A.CloseTme,2))As CloseTme, A.SpBLNo, H.LocalDocNo As PLLocal, A2.CtPONo, ");
                SQL1.AppendLine("(Select Parvalue From Tblparameter Where parCode = 'SIVolumeUom') As Vuom,(Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom ");
                SQL1.AppendLine("From TblSIHdr A ");
                SQL1.AppendLine("Left Join ( ");
                SQL1.AppendLine("		Select  A.DocNo As SIDOCno, Group_concat(Distinct C.LocalDocNo separator ', ' )As SoLocal, Group_concat(Distinct C.CtPONo separator ', ' )As CtPONo ");
                SQL1.AppendLine("       From tblsidtl A ");
                SQL1.AppendLine("		Inner Join TblSodtl B On A.SODocNo=B.DocNo And A.SODNo=B.Dno ");
                SQL1.AppendLine("		Inner JOin TblSoHdr C On B.DocNo=C.DocNo ");
                SQL1.AppendLine("		Group By A.DocNo");
                SQL1.AppendLine(")A2 On A.DocNo=A2.SiDocNo ");
                SQL1.AppendLine("Left Join TblSIDtl2 B On A.DocNo = B.DocNo ");
                SQL1.AppendLine("Left Join TblOpTion C On B.BlCode = C.OptCode And C.OPtCat = 'BLRequirement' ");
                SQL1.AppendLine("Inner Join TblPort D On A.SPPortCode1 = D.PortCode ");
                SQL1.AppendLine("Inner Join TblPort E On A.SPPortCode2 = E.PortCode ");
                SQL1.AppendLine("Inner Join TblSP F On A.SpDocNo = F.DocNo ");
                SQL1.AppendLine("Inner Join TblUser G On A.CreateBy = G.Usercode ");
                SQL1.AppendLine("Left Join TblPLHdr H On A.DocNo=H.SiDocNo ");
                SQL1.AppendLine("left Join TblPlDtl H2 On H.DocNo = H2.DocNo ");
                SQL1.AppendLine("Left Join TblVendor I On A.VdCode=I.VdCode ");
                SQL1.AppendLine("Left Join TblVendor J On A.VdCode2=J.VdCode ");
                SQL1.AppendLine("Left Join TblCity K On I.CityCode=K.CityCode ");
                

                SQL1.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn1 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn1.Open();
                    cm1.Connection = cn1;
                    cm1.CommandText = SQL1.ToString();
                    Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm1, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr1 = cm1.ExecuteReader();
                    var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "Shipper1",
                         //6-10
                         "Shipper2",
                         "Shipper3",
                         "Shipper4",
                         "DocNo",
                         "DocDt",
                         //11-15
                         "SPBLNo",
                         "SpCtNotifyParty",
                         "SpCtNotifyParty2",
                         "SPPEB",
                         "SPHSCOde",
                         //16-20
                         "SpConsignee",
                         "SPKPBC",
                         "SpPlaceReceipt",
                         "PortLoading",
                         "SPPlaceDelivery",
                         //21-24
                         "PortDischarge",
                         "SpMark",
                         "SpSize",
                         "Description",
                         "StfDt",
                         //26-30
                         "SPName",
                         "CompanyFax",
                         "LocalDocNo",
                         "UserName",
                         "Note",
                         //31-35
                         "SoLocal",
                         "PLDocNo",
                         "Feeder",
                         "MotherVessel",
                         "ShippingLine",

                         //36-40
                         "VdForwarder",
                         "VdTransport",
                         "AddressForwarder",
                         "CityForwarder",
                         "PhoneForwarder",

                         //41-45
                         "FaxForwarder",
                         "AddressTransport",
                         "CityTransport",
                         "PhoneTransport",
                         "FaxTransport",

                         //46-50
                         "PIUser", 
                         "StuffTme", 
                         "CloseDt", 
                         "CloseTme", 
                         "SpBLNo",

                         //51-54
                         "PLLocal",
                         "CtPONo",
                         "Vuom",
                         "Nguom"

                        });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            l2.Add(new SIHdr2()
                            {
                                CompanyLogo = Sm.DrStr(dr1, c1[0]),

                                CompanyName = Sm.DrStr(dr1, c1[1]),
                                CompanyAddress = Sm.DrStr(dr1, c1[2]),
                                CompanyAddressCity = Sm.DrStr(dr1, c1[3]),
                                CompanyPhone = Sm.DrStr(dr1, c1[4]),
                                Shipper1 = Sm.DrStr(dr1, c1[5]),

                                Shipper2 = Sm.DrStr(dr1, c1[6]),
                                Shipper3 = Sm.DrStr(dr1, c1[7]),
                                Shipper4 = Sm.DrStr(dr1, c1[8]),
                                DocNo = Sm.DrStr(dr1, c1[9]),
                                DocDt = Sm.DrStr(dr1, c1[10]),

                                SPBLNo = Sm.DrStr(dr1, c1[11]),
                                SpCtNotifyParty = Sm.DrStr(dr1, c1[12]),
                                SpCtNotifyParty2 = Sm.DrStr(dr1, c1[13]),
                                SPPEB = Sm.DrStr(dr1, c1[14]),
                                SPHSCOde = Sm.DrStr(dr1, c1[15]),

                                SpConsignee = Sm.DrStr(dr1, c1[16]),
                                SPKPBC = Sm.DrStr(dr1, c1[17]),
                                SpPlaceReceipt = Sm.DrStr(dr1, c1[18]),
                                PortLoading = Sm.DrStr(dr1, c1[19]),
                                SPPlaceDelivery = Sm.DrStr(dr1, c1[20]),

                                PortDischarge = Sm.DrStr(dr1, c1[21]),
                                SpMark = Sm.DrStr(dr1, c1[22]),
                                SpSize = Sm.DrStr(dr1, c1[23]),
                                Description = Sm.DrStr(dr1, c1[24]),
                                Stufing = Sm.DrStr(dr1, c1[25]),

                                SPName = Sm.DrStr(dr1, c1[26]),
                                CompanyFax = Sm.DrStr(dr1, c1[27]),
                                LocalDocNo = Sm.DrStr(dr1, c1[28]),
                                UserName = Sm.DrStr(dr1, c1[29]),
                                Note = Sm.DrStr(dr1, c1[30]),

                                SoLocal = Sm.DrStr(dr1, c1[31]),
                                PLDocNo = Sm.DrStr(dr1, c1[32]),
                                Feeder = Sm.DrStr(dr1, c1[33]),
                                MotherVessel = Sm.DrStr(dr1, c1[34]),
                                ShippingLine = Sm.DrStr(dr1, c1[35]),
                                VdForwarder = Sm.DrStr(dr1, c1[36]),
                                VdTransport = Sm.DrStr(dr1, c1[37]),
                                AddressForwarder = Sm.DrStr(dr1, c1[38]),
                                CityForwarder = Sm.DrStr(dr1, c1[39]),
                                PhoneForwarder = Sm.DrStr(dr1, c1[40]),
                                FaxForwarder = Sm.DrStr(dr1, c1[41]),
                                AddressTransport = Sm.DrStr(dr1, c1[42]),
                                CityTransport = Sm.DrStr(dr1, c1[43]),
                                PhoneTransport = Sm.DrStr(dr1, c1[44]),
                                FaxTransport = Sm.DrStr(dr1, c1[45]),
                                PIUser = Sm.DrStr(dr1, c1[46]),
                                StuffTme = Sm.DrStr(dr1, c1[47]),
                                CloseDt = Sm.DrStr(dr1, c1[48]),
                                CloseTme = Sm.DrStr(dr1, c1[49]),
                                SpBLNo = Sm.DrStr(dr1, c1[50]),
                                PLLocal = Sm.DrStr(dr1, c1[51]),
                                CtPONo = Sm.DrStr(dr1, c1[52]),
                                Vuom = Sm.DrStr(dr1, c1[53]),
                                Nguom = Sm.DrStr(dr1, c1[54]),
                                    
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr1.Close();
                }
                myLists.Add(l2);
                #endregion

                #region Detail
                
                //class detail 3
                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("    Select A.DocNo, I.ItCode, ");

                    if (mIsCustomerItemNameMandatory)
                        SQLDtl3.AppendLine("IfNull(L.CtItName, I.ItName) As ItName, ");
                    else
                        SQLDtl3.AppendLine("I.ItName, ");

                    SQLDtl3.AppendLine("I.ItCodeInternal, J.ItGrpName, L.CtItCode,  I.HSCode, ");
                    SQLDtl3.AppendLine("O.OptDesc As Colour,N.OptDesc As Material,  P.PtName As Top, D.CtPONo, D.DocNo As SoDocNo,  ");
                    SQLDtl3.AppendLine("B.QtyPackagingUnit, C.PackagingUnitUomCode, B.Qty As QtySales, G.PriceUomCode As SalesUomCode, G.CurCode, F1.UPrice,  ");
                    SQLDtl3.AppendLine("Round((B.Qty * F1.UPrice),2) As Amount, ");
                    SQLDtl3.AppendLine("Round(B.QtyInventory,4) As QtyInventory, I.InventoryUomCode,  ");
                    SQLDtl3.AppendLine("(B.Qty /B.QtyPackagingUnit) QtyPcs, if(B.NWRate =0, (B.QtyPackagingUnit * K.NW), NWRate) As NW,  ");
                    SQLDtl3.AppendLine("if(B.GWRate=0, (B.QtyPackagingUnit * K.GW), GWRate) As GW, ");
                    SQLDtl3.AppendLine("(Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom, M.DTName   ");
                    SQLDtl3.AppendLine("From TblSIHdr A "); 
                    SQLDtl3.AppendLine("Inner join TblSIDtl B On A.DocNo = B.DocNo ");
                    SQLDtl3.AppendLine("Inner Join TblSODtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo "); 
                    SQLDtl3.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo ");  
                    SQLDtl3.AppendLine("Inner Join TblCtQtHdr F On D.CtQtDocNo = F.DocNo ");
                    SQLDtl3.AppendLine("Inner Join TblCtQtDtl F1 On D.CtQtDocNo = F1.DocNo And C.CtQtDNo = F1.Dno ");
                    SQLDtl3.AppendLine("Inner Join TblItemPriceHdr G On F1.ItemPriceDocNo = G.DocNo "); 
                    SQLDtl3.AppendLine("Inner Join TblitemPriceDtl H On F1.ItemPriceDocNo = H.DocNo And F1.ItemPriceDNo = H.Dno ");
                    SQLDtl3.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode ");
                    SQLDtl3.AppendLine("Left Join TblItemGroup J On I.ItGrpCode=J.ItGrpCode ");
                    SQLDtl3.AppendLine("left Join ");
                    SQLDtl3.AppendLine("( ");
                    SQLDtl3.AppendLine("Select ItCode, Qty, Qty2, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl3.AppendLine(")K On K.ItCode = H.ItCode And C.PackagingUnitUomCode = K.UomCode ");
                    SQLDtl3.AppendLine("Left Join TblCustomerItem L On I.ItCode=L.ItCode And D.CtCode=L.CtCode ");
                    SQLDtl3.AppendLine("Left Join TblDeliveryType M On F.ShpMCode=M.DTCode ");
                    SQLDtl3.AppendLine("Left Join TblOption N On N.OptCat='ItemInformation5' And I.Information5=N.OptCode ");
                    SQLDtl3.AppendLine("Left Join TblOption O On O.OptCat='ItemInformation1' And I.Information1=O.OptCode ");
                    SQLDtl3.AppendLine("Left join TblPaymentTerm P On F.PtCode=P.PtCode ");
                    SQLDtl3.AppendLine("Where A.DocNo=@DocNo");

                    
                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                    "DocNo", 
                    //1-5
                    "ItCode", "ItName", "ItCodeInternal", "ItGrpName", "CtItCode", 
                    //6-10
                    "HSCode", "Colour", "Material", "Top", "CtPONo",       
                     //11-15
                    "SoDocNo",  "QtyPackagingUnit", "PackagingUnitUomCode",  "QtySales", "SalesUomCode",    
                    //16-20
                    "CurCode", "UPrice", "Amount",  "QtyInventory", "InventoryUomCode",  
                    //21-25
                    "QtyPcs", "NW", "GW", "NGuom",  "DTName",  

                    });
                    if (drDtl3.HasRows)
                    {
                        int nomor = 0;
                        while (drDtl3.Read())
                        {
                            nomor= nomor + 1;

                            ldtl3.Add(new SIDtl3()
                            {
                                nomor = nomor,
                                DocNo = Sm.DrStr(drDtl3, cDtl3[0]),
                                ItCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                ItName = Sm.DrStr(drDtl3, cDtl3[2]),
                                ItCodeInternal = Sm.DrStr(drDtl3, cDtl3[3]),
                                ItGrpName = Sm.DrStr(drDtl3, cDtl3[4]),
                                CtItCode = Sm.DrStr(drDtl3, cDtl3[5]),

                                SPHSCOde = Sm.DrStr(drDtl3, cDtl3[6]),
                                Colour = Sm.DrStr(drDtl3, cDtl3[7]),
                                Material = Sm.DrStr(drDtl3, cDtl3[8]),
                                Top = Sm.DrStr(drDtl3, cDtl3[9]),
                                CtPONo = Sm.DrStr(drDtl3, cDtl3[10]),

                                SoDocNo = Sm.DrStr(drDtl3, cDtl3[11]),
                                QtyPackagingUnit = Sm.DrDec(drDtl3, cDtl3[12]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl3, cDtl3[13]),
                                QtySales = Sm.DrDec(drDtl3, cDtl3[14]),
                                SalesUomCode = Sm.DrStr(drDtl3, cDtl3[15]),
                               
                                Currency = Sm.DrStr(drDtl3, cDtl3[16]),
                                UPrice = Sm.DrDec(drDtl3, cDtl3[17]),
                                Amount = Sm.DrDec(drDtl3, cDtl3[18]),
                                QtyInventory = Sm.DrDec(drDtl3, cDtl3[19]),
                                InventoryUomCode = Sm.DrStr(drDtl3, cDtl3[20]),
                                QtyPcs = Sm.DrDec(drDtl3, cDtl3[21]),
                                NW = Sm.DrDec(drDtl3, cDtl3[22]),
                                GW = Sm.DrDec(drDtl3, cDtl3[23]),
                                NGuom = Sm.DrStr(drDtl3, cDtl3[24]),
                                DTName = Sm.DrStr(drDtl3, cDtl3[25]),

                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

                #region Detail 2

                //informasi sum amount, volume, NW, GW
                var cmDtl4 = new MySqlCommand();

                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    SQLDtl4.AppendLine(" Select X.SIDocNo, X.CurCode, Round(SUM(X.Amount), 2) As Amount, SUM(X.GW) As GW, SUM(X.NW) As NW, X2.TotalVolume ");
                    SQLDtl4.AppendLine(" From ( ");
	                SQLDtl4.AppendLine("     Select A.DocNo As SIDocNo, G.CurCode, (B.Qty * F1.UPrice) As Amount, ");
	                SQLDtl4.AppendLine("     if(B.GWRate=0, (B.QtyPackagingUnit * I.GW), GWRate) As GW, ");
	                SQLDtl4.AppendLine("     if(B.NWRate =0, (B.QtyPackagingUnit * I.NW), NWRate) As NW ");
	                SQLDtl4.AppendLine("     From TblSIHdr A   ");
	                SQLDtl4.AppendLine("     Inner join TblSIDtl B On A.DocNo = B.DocNo  ");
	                SQLDtl4.AppendLine("     Inner Join TblSODtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo   ");
	                SQLDtl4.AppendLine("     Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo   "); 
	                SQLDtl4.AppendLine("     Inner Join TblCtQtHdr F On D.CtQtDocNo = F.DocNo  ");
	                SQLDtl4.AppendLine("     Inner Join TblCtQtDtl F1 On D.CtQtDocNo = F1.DocNo And C.CtQtDNo = F1.Dno  ");
	                SQLDtl4.AppendLine("     Inner Join TblItemPriceHdr G On F1.ItemPriceDocNo = G.DocNo   ");
	                SQLDtl4.AppendLine("     Inner Join TblitemPriceDtl H On F1.ItemPriceDocNo = H.DocNo And F1.ItemPriceDNo = H.Dno  ");
	                SQLDtl4.AppendLine("     left Join  ");
	                SQLDtl4.AppendLine("     (  ");
		            SQLDtl4.AppendLine("         Select ItCode, Qty, Qty2, UomCode, Nw, GW From TblItempackagingunit  ");
	                SQLDtl4.AppendLine("     )I On I.ItCode = H.ItCode And C.PackagingUnitUomCode = I.UomCode  ");
                    SQLDtl4.AppendLine(" )X  ");
                    SQLDtl4.AppendLine(" Left Join ");
                    SQLDtl4.AppendLine(" ( ");
	                SQLDtl4.AppendLine("     Select A.SIDocNo, SUM(B.TotalVolume) As TotalVolume ");
	                SQLDtl4.AppendLine("     from TblPLhdr A ");
	                SQLDtl4.AppendLine("     Inner Join TblPlDtl B On A.DocNo = B.DocNo ");
	                SQLDtl4.AppendLine("     Group By A.SIDocNo ");
                    SQLDtl4.AppendLine(" )X2 On X.SIDocNo =X2.SIDocNo ");
                    SQLDtl4.AppendLine(" Where X.SIDocNo = @DocNo ");
                    SQLDtl4.AppendLine(" Group by X.SIDocNo, X.CurCode ");
                    
                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                    {
                     //0
                     "SiDocNo", 
                     //1-5
                     "Amount", "NW", "GW", "TotalVolume"
                    });
                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {
                            ldtl4.Add(new SIDtl4()
                            {

                                SiDocNo = Sm.DrStr(drDtl4, cDtl4[0]),

                                Amount = Sm.DrDec(drDtl4, cDtl4[1]),
                                Terbilang = Convert(Sm.DrDec(drDtl4, cDtl4[1])),
                                NW = Sm.DrDec(drDtl4, cDtl4[2]),
                                GW = Sm.DrDec(drDtl4, cDtl4[3]),
                                TotalVolume = Sm.DrDec(drDtl4, cDtl4[4]),

                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);
                #endregion

                #region Detail 3
                //proforma MAI
                var cmDtl5 = new MySqlCommand();

                var SQLDtl5 = new StringBuilder();
                using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl5.Open();
                    cmDtl5.Connection = cnDtl5;

                    SQLDtl5.AppendLine("Select X2.DocNo, SUM(X2.Amount) As Amount , Round(X2.Additional,2)As Additional, Round(X2.Disc,2)As Disc, ");
                    SQLDtl5.AppendLine("SUM(X2.Amount)+Round(X2.Additional,2)-Round(X2.Disc,2)As GrandTot ");
                    SQLDtl5.AppendLine("From ( ");
                    SQLDtl5.AppendLine("	Select ");
                    SQLDtl5.AppendLine("	A1.DocNo,  Round((A2.Qty * G1.UPrice),2)As Amount,  Ifnull(K.Additional,0) As Additional, ");
                    SQLDtl5.AppendLine("	Ifnull(L.Disc,0) As Disc ");
                    SQLDtl5.AppendLine("	From TblSIHdr A1 ");
                    SQLDtl5.AppendLine("	Inner join TblSIDtl A2 On A1.DocNo = A2.DocNo ");
                    SQLDtl5.AppendLine("	Left Join TblPlhdr A On A1.DocNo = A.SiDocNo ");
                    SQLDtl5.AppendLine("	Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl5.AppendLine("	Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                    SQLDtl5.AppendLine("	Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl5.AppendLine("	Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                    SQLDtl5.AppendLine("	Inner Join TblitemPriceDtl G1 On F.ItemPriceDocNo = G1.DocNo And F.ItemPriceDNo = G1.Dno ");
                    SQLDtl5.AppendLine("	Inner Join TblItem H On G1.ItCode = H.ItCode ");
                    SQLDtl5.AppendLine("	Left Join TblDOCt2Hdr I On A.DocNo=I.PLDocNo ");
                    SQLDtl5.AppendLine("    Left Join ");
                    SQLDtl5.AppendLine("	( ");
                    SQLDtl5.AppendLine("	    select Distinct T.DocNo As SIDocNo, T1.DoctDocNo, T.Additional from ");
                    SQLDtl5.AppendLine("	    ( ");
                    SQLDtl5.AppendLine("		    Select A.Docno, sum(if(Damt=0, Camt, Damt)) As Additional ");
                    SQLDtl5.AppendLine("		    From TblSalesInvoiceHdr A ");
                    SQLDtl5.AppendLine("		    left Join TblsalesInvoiceDtl2 C On A.DocNo = C.DocNo ");
                    SQLDtl5.AppendLine("		    Where A.CancelInd = 'N' And OptAcDesc = '1' ");
                    SQLDtl5.AppendLine("		    Group By  A.DocNo ");
                    SQLDtl5.AppendLine("		)T ");
                    SQLDtl5.AppendLine("	    Inner Join TblSalesInvoicedtl T1 On T.DocNo=T1.Docno ");
                    SQLDtl5.AppendLine("	)K On I.DocNo = K.DOCtDocNo ");
                    SQLDtl5.AppendLine("	Left Join ");
                    SQLDtl5.AppendLine("	( ");
                    SQLDtl5.AppendLine("		select Distinct T2.DocNo As SIDocNo, T3.DoctDocNo, T2.Disc from ");
                    SQLDtl5.AppendLine("		( ");
                    SQLDtl5.AppendLine("			Select A.Docno, sum(if(Damt=0, Camt, Damt)) As Disc ");
                    SQLDtl5.AppendLine("			From TblSalesInvoiceHdr A ");
                    SQLDtl5.AppendLine("			left Join TblsalesInvoiceDtl2 C On A.DocNo = C.DocNo ");
                    SQLDtl5.AppendLine("			Where A.CancelInd = 'N' And OptAcDesc = '2' ");
                    SQLDtl5.AppendLine("			Group By  A.DocNo ");
                    SQLDtl5.AppendLine("		)T2 ");
                    SQLDtl5.AppendLine("		Inner Join TblSalesInvoicedtl T3 On T2.DocNo=T3.Docno	");
                    SQLDtl5.AppendLine("	)L On I.DocNo = L.DOCtDocNo ");
                    SQLDtl5.AppendLine("	where A1.Docno=@DocNo ");
                    SQLDtl5.AppendLine("	)X2 ");
                    SQLDtl5.AppendLine("Group by X2.DocNo ");

                    cmDtl5.CommandText = SQLDtl5.ToString();

                    Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);

                    var drDtl5 = cmDtl5.ExecuteReader();
                    var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                    {
                     //0
                     "DocNo",
                     //1-4
                     "Amount", "Additional", "Disc", "GrandTot"

                    });
                    if (drDtl5.HasRows)
                    {
                        while (drDtl5.Read())
                        {
                            ldtl5.Add(new SIDtl5P()
                            {
                                DocNo = Sm.DrStr(drDtl5, cDtl5[0]),
                                Amount = Sm.DrDec(drDtl5, cDtl5[1]),
                                Additional = Sm.DrStr(drDtl5, cDtl5[2]),
                                Disc = Sm.DrStr(drDtl5, cDtl5[3]),
                                GrandTot = Sm.DrDec(drDtl5, cDtl5[4]),
                                Terbilang = Convert(Sm.DrDec(drDtl5, cDtl5[4])),
                            });
                        }
                    }
                    drDtl5.Close();
                }
                myLists.Add(ldtl5);
                #endregion

                #region Detail4
                var cmDtl8 = new MySqlCommand();

                var SQLDtl8 = new StringBuilder();
                using (var cnDtl8 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl8.Open();
                    cmDtl8.Connection = cnDtl8;

                    SQLDtl8.AppendLine("Select  B.Route, DATE_FORMAT(B.EstimatedDt,'%d/%m/%Y') As EstimatedDt, ");
                    SQLDtl8.AppendLine("case when B.Estimatedtype = 'D' Then 'ETD'  ");
                    SQLDtl8.AppendLine("When B.EstimatedType = 'A' Then 'ETA'  ");
                    SQLDtl8.AppendLine("End As EstType ");
                    SQLDtl8.AppendLine("From TblSIHdr A  ");
                    SQLDtl8.AppendLine("Inner Join TblSPDtl B On A.SPDocNo = B.DocNo  ");
                    SQLDtl8.AppendLine("Where A.DocNo=@DocNo ");

                    cmDtl8.CommandText = SQLDtl8.ToString();

                    Sm.CmParam<String>(ref cmDtl8, "@DocNo", TxtDocNo.Text);

                    var drDtl8 = cmDtl8.ExecuteReader();
                    var cDtl8 = Sm.GetOrdinal(drDtl8, new string[] 
                    {
                     //0
                     "Route" ,
                     "EstimatedDt", "EstType"
                    });
                    if (drDtl8.HasRows)
                    {
                        while (drDtl8.Read())
                        {
                            ldtl8.Add(new SIDtl8()
                            {
                                Route = Sm.DrStr(drDtl8, cDtl8[0]),
                                Date = Sm.DrStr(drDtl8, cDtl8[1]),
                                Type = Sm.DrStr(drDtl8, cDtl8[2]),
                            });
                        }
                    }
                    drDtl8.Close();
                }
                myLists.Add(ldtl8);
                #endregion

                if (ChkProforma.Checked == true)
                {
                    Sm.PrintReport(mIsFormPrintOutSInvPro, myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport(mIsFormPrintOutSI, myLists, TableName, false);
                }

            }
          #endregion
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };


        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }

        #endregion


        private void DownloadFileKu(string TxtFile)
        {
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (SFD1.ShowDialog() == DialogResult.OK)
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                Application.DoEvents();

                //Write the bytes to a file
                FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                newFile.Write(downloadedData, 0, downloadedData.Length);
                newFile.Close();
                MessageBox.Show("Saved Successfully");
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = "Connecting...";
                Application.DoEvents();

               if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception exc)
            {
                Sm.StdMsg(mMsgType.Warning, exc.ToString());
            }
        }


        #endregion
       
        #endregion

        #region Event

        #region Misc Control Event

        private void BtnSP_Click(object sender, EventArgs e)
        {
            TxtSPDocNo.Text = "";
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                Sm.FormShowDialog(new FrmSIDlg2(this, Sm.GetLue(LueCtCode)));
            }
            if(TxtSPDocNo.Text.Length != 0)
            {
                TxtLocalDocNo.EditValue = mIsNotCopySalesLocalDocNo ? "" : Sm.GetValue("Select LocalDocNo From TblSp Where DocNo='" + TxtSPDocNo.Text + "' ");
            }
        }

        private void BtnSP2_Click(object sender, EventArgs e)
        {
            if (TxtSPDocNo.Text.Length != 0)
            {
                var f1 = new FrmSP(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtSPDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void LueCtCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
                ClearData2();
                if (TxtDocNo.Text.Length == 0) TxtSPDocNo.EditValue = "";
                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueNotify.EditValue = null;
                    LueNotify.DataBindings.Clear();
                    Sm.SetControlReadOnly(LueNotify, true);
                }
                else
                {
                    SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode), string.Empty);
                    Sm.SetControlReadOnly(LueNotify, false);
                }
            }
        }

        private void LueBL_Leave(object sender, EventArgs e)
        {
            if (LueBL.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueBL).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 0].Value =
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueBL);
                    Grd2.Cells[fCell.RowIndex, 1].Value = LueBL.GetColumnValue("Col2");
                }
                LueBL.Visible = false;
            }
        }

        private void LueBL_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBL, new Sm.RefreshLue1(SetLueBL));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueNotify_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueNotify, new Sm.RefreshLue3(SetLueNotify), Sm.GetLue(LueCtCode), string.Empty);
        }

        private void LueLoading_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLoading, new Sm.RefreshLue1(SetLueLoading));
        }

        private void LueDischarge_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLoading, new Sm.RefreshLue1(SetLueDischarge));
        }

        private void BtnSource_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSIDlg3(this, Sm.GetLue(LueCtCode)));
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(SetLueVd));
        }

        private void LueVdCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode2, new Sm.RefreshLue1(SetLueVd));
        }

        private void BtnConsignee_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueNotify, "Notify party")) return;
            MeeConsignee.EditValue = Sm.GetLue(LueNotify);
        }

        #endregion

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            List<string> FileName = new List<string>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.FileName1, A.FileName2, A.FileName3, A.FileName4, A.FileName5, ");
            SQL.AppendLine("A.FileName6, A.FileName7, A.FileName8, A.FileName9, A.FileName10, ");
            SQL.AppendLine("A.FileName11, A.FileName12, A.FileName13, A.FileName14, A.FileName15, ");
            SQL.AppendLine("A.FileName16, A.FileName17, A.FileName18, A.FileName19, A.FileName20 ");
            SQL.AppendLine("From TblAttachmentFile A ");
            SQL.AppendLine("Where A.LocalDocNo=@LocalDocNo;");

            Sm.CmParam<String>(ref cm, "@localDocNo", TxtLocalDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "FileName1", 
                        //1-5
                        "FileName2", "FileName3", "FileName4", "FileName5", "FileName6",  
                        //6-10
                        "FileName7", "FileName8", "FileName9", "FileName10", "FileName11", 
                        //11-15
                        "FileName12", "FileName13", "FileName14", "FileName15", "FileName16", 
                        //16-19
                        "FileName17", "FileName18", "FileName19", "FileName20"                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[0]));
                        if (Sm.DrStr(dr, c[1]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[1]));
                        if (Sm.DrStr(dr, c[2]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[2]));
                        if (Sm.DrStr(dr, c[3]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[3]));
                        if (Sm.DrStr(dr, c[4]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[4]));
                        if (Sm.DrStr(dr, c[5]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[5]));
                        if (Sm.DrStr(dr, c[6]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[6]));
                        if (Sm.DrStr(dr, c[7]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[7]));
                        if (Sm.DrStr(dr, c[8]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[8]));
                        if (Sm.DrStr(dr, c[9]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[9]));
                        if (Sm.DrStr(dr, c[10]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[10]));
                        if (Sm.DrStr(dr, c[11]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[11]));
                        if (Sm.DrStr(dr, c[12]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[12]));
                        if (Sm.DrStr(dr, c[13]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[13]));
                        if (Sm.DrStr(dr, c[14]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[14]));
                        if (Sm.DrStr(dr, c[15]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[15]));
                        if (Sm.DrStr(dr, c[16]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[16]));
                        if (Sm.DrStr(dr, c[17]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[17]));
                        if (Sm.DrStr(dr, c[18]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[18]));
                        if (Sm.DrStr(dr, c[19]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[19]));
                    }, false
                );
            if (FileName.Count > 0)
            {
                for (int i = 0; i < FileName.Count; i++)
                {
                    DownloadFileKu(FileName[i]);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No Attachment File");
            }
        }

        #endregion
    }

    #region Report Class

    class SIHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public string Shipper1 { get; set; }
        public string Shipper2 { get; set; }
        public string Shipper3 { get; set; }
        public string Shipper4 { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string SPBLNo { get; set; }
        public string SpCtNotifyParty { get; set; }
        public string SpCtNotifyParty2 { get; set; }
        public string SPPEB { get; set; }
        public string SPHSCOde { get; set; }
        public string SpConsignee { get; set; }
        public string SPKPBC { get; set; }
        public string SpPlaceReceipt { get; set; }
        public string PortLoading { get; set; }
        public string SPPlaceDelivery { get; set; }
        public string PortDischarge { get; set; }
        public string SpMark { get; set; }
        public string SpSize { get; set; }
        public string Description { get; set; }
        public string Stufing { get; set; }
        public string PrintBy { get; set; }
        public string SPName { get; set; }
        public string LocalDocNo { get; set; }
        public string UserName { get; set; }
        public string Note { get; set; }
        public string Account { get; set; }
        public string SalesContractNo { get; set; }
        public string LCNo { get; set; }
        public string Remark { get; set; }
        public string LcDt { get; set; }
        public string Issued1 { get; set; }
        public string Issued2 { get; set; }
        public string Issued3 { get; set; }
        public string Issued4 { get; set; }
        public string ToOrder1 { get; set; }
        public string ToOrder2 { get; set; }
        public string ToOrder3 { get; set; }
        public string ToOrder4 { get; set; }
    }

    class SIDtl
    {
        public string Cnt { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public string PriceUomCode { get; set; }
        public decimal QtyNetto { get; set; }
        public string NGUom { get; set; }
        public decimal QtyGross { get; set; }
        public decimal QtyInventory { get; set; }
        public string InventoryUomCode { get; set; }
        public string QtyPL { get; set; }
        public string Username { get; set; }
    }

    class SIDtl2
    {
        public string BL { get; set; }
    }

    class SIDtl5
    {
        public string Cnt { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string PLNo { get; set; }
        public decimal QtyPL { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal QtyInventory { get; set; }
        public decimal NW { get; set; }
        public decimal GW { get; set; }
        public string Username { get; set; }
        public string SectionNo { get; set; }
        public string Currency { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }
        public decimal Freight { get; set; }
        public string Remark { get; set; }
        public decimal QtyPackagingUnit { get; set; }
        public string Dno { get; set; }
    }

    class SIDtl6
    {
        public string Jumlah { get; set; }
        public string Size { get; set; }
    }

    class SIDtl7
    {
        public decimal Amount { get; set; }
        public string Terbilang { get; set; }
        public decimal Totally { get; set; }
        public decimal TotalFre { get; set; }
    }

    class SIHdr2
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public string Shipper1 { get; set; }
        public string Shipper2 { get; set; }
        public string Shipper3 { get; set; }
        public string Shipper4 { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string SPBLNo { get; set; }
        public string SpCtNotifyParty { get; set; }
        public string SpCtNotifyParty2 { get; set; }
        public string SPPEB { get; set; }
        public string SPHSCOde { get; set; }
        public string SpConsignee { get; set; }
        public string SPKPBC { get; set; }
        public string SpPlaceReceipt { get; set; }
        public string PortLoading { get; set; }
        public string SPPlaceDelivery { get; set; }
        public string PortDischarge { get; set; }
        public string SpMark { get; set; }
        public string SpSize { get; set; }
        public string Description { get; set; }
        public string Stufing { get; set; }
        public string PrintBy { get; set; }
        public string SPName { get; set; }
        public string LocalDocNo { get; set; }
        public string UserName { get; set; }
        public string SoLocal { get; set; }
        public string Note { get; set; }
        public string PLDocNo { get; set; }
        public string Feeder { get; set; }
        public string MotherVessel { get; set; }
        public string ShippingLine { get; set; }
        public string VdForwarder { get; set; }
        public string VdTransport { get; set; }
        public string AddressForwarder { get; set; }
        public string CityForwarder { get; set; }
        public string PhoneForwarder { get; set; }
        public string FaxForwarder { get; set; }
        public string AddressTransport { get; set; }
        public string CityTransport { get; set; }
        public string PhoneTransport { get; set; }
        public string FaxTransport { get; set; }
        public string PIUser { get; set; }
        public string StuffTme { get; set; }
        public string CloseDt { get; set; }
        public string CloseTme { get; set; }
        public string SpBLNo { get; set; }
        public string PLLocal { get; set; }
        public string CtPONo { get; set; }
        public string Vuom { get; set; }
        public string Nguom { get; set; }
    }

    class SIDtl3
    {
        public int nomor { get; set; }
        public string DocNo { get; set; }
        public string SoDocNo { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string ItCodeInternal  { get; set; }
        public decimal QtyPackagingUnit { get; set; }
        public decimal OutstandingQty  { get; set; }
        public decimal QtySales { get; set; }
        public decimal QtyInventory { get; set; }
        public decimal NW { get; set; }
        public decimal GW { get; set; }
        public string SalesUomCode { get; set; }
        public string InventoryUomCode { get; set; }
        public string Currency { get; set; }
        public string DeliveryDt { get; set; }
        public string PackagingUnitUomCode { get; set; }
        public string OutstandingPackaging  { get; set; }
        public string SPHSCOde { get; set; }
        public decimal UPrice { get; set; }
        public decimal Amount { get; set; }
        public string ItGrpName { get; set; }
        public string CtItCode { get; set; }
        public string DTName { get; set; }
        public string NGuom { get; set; }
        public string Material { get; set; }
        public string Colour { get; set; }
        public string Top { get; set; }
        public string CtPONo { get; set; }
        public decimal QtyPL { get; set; }
        public decimal QtyPcs { get; set; }
        
    }

    class SIDtl4
    {
      
        public string SiDocNo { get; set; }
        public decimal Amount { get; set; }
        public string NGuom { get; set; }
        public string Terbilang { get; set; }
        public decimal NW { get; set; }
        public decimal GW { get; set; }
        public decimal TotalVolume { get; set; }
    }

    class SIDtl5P
    {

        public string DocNo { get; set; }
        public decimal Amount { get; set; }
        public string Additional { get; set; }
        public string Disc { get; set; }
        public decimal GrandTot { get; set; }
        public string Terbilang { get; set; }
     
    }

    class SIDtl8
    {
        public string Route { get; set; }
        public string Date { get; set; }
        public string Type { get; set; }
    }


    #endregion
}
