﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmAssetDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAsset mFrmParent;
        private string mSQL = string.Empty;
        private int mCodeCOA = 1;

        #endregion

        #region Constructor

        public FrmAssetDlg2(FrmAsset FrmParent, int CodeCOA)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCodeCOA = CodeCOA;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    "No.",

                    "Account#", 
                    "Description",
                    "Type",
                    "Alias"
                },
                new int[]{ 
                    50, 
                    150, 350, 100, 150
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Grd1.Cols[4].Visible = mFrmParent.mIsCOAUseAlias;
            Grd1.Cols[4].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo, AcDesc, Alias, ");
            SQL.AppendLine("Case AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcType ");
            SQL.AppendLine("From TblCoa ");
            if (mCodeCOA == 1)
            {
                SQL.AppendLine("Where Parent is not null ");
                SQL.AppendLine("And AcNo Not In(Select parent From TblCoa Where parent is not null ) ");
            }
            else
            {
                SQL.AppendLine("Where Find_In_Set(Left(AcNo, 1), ");
                SQL.AppendLine("IfNull((Select ParValue From TblParameter Where ParCode='AcCtCodeForAssetAccmDepr' And ParValue Is Not Null), '1')) ");
                SQL.AppendLine("And Parent Is Not Null ");
                SQL.AppendLine("And AcNo Not In (Select Parent From TblCoa Where Parent Is Not Null ) "); 
            }
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "AcNo", "AcDesc" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By AcNo;",
                        new string[] 
                        { 
                            "AcNo",
                            "AcDesc",
                            "AcType",
                            "Alias"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                if (mCodeCOA == 1)
                {
                    mFrmParent.TxtAcNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    if (mFrmParent.mIsCOAUseAlias && Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4).Length>0)
                        mFrmParent.TxtAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2) + " [" + Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4) + "]";
                    else
                        mFrmParent.TxtAcDesc.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                }
                else
                {
                    mFrmParent.TxtAcNo2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    if (mFrmParent.mIsCOAUseAlias && Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4).Length > 0)
                        mFrmParent.TxtAcDesc2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2) + " [" + Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4) + "]";
                    else
                        mFrmParent.TxtAcDesc2.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                }
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA's Account");
        }

        #endregion

    }
}
