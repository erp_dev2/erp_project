﻿#region Update
// 04/04/2017 [WED] Tambah kolom Site
// 19/09/2017 [WED] tambah kolom Term of Payment
// 10/11/2021 [RIS/AMKA] Membuat field Department terfilter berdasarkan group dan param IsFilterByDept
// 29/11/2021 [RIS/AMKA] Feedback : Data yang tampil terfilter berdasarkan department group user dan parameter IsFilterByDept
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingAPLedgerPaidItem : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsSiteMandatory = false;
        private bool mIsFilterByDept = false;

        #endregion

        #region Constructor

        public FrmRptAgingAPLedgerPaidItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                SetLueType();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.*, Tbl2.VdName, Tbl3.DeptName, Tbl4.OptDesc As PaymentTypes, Tbl5.SiteName, ");
            SQL.AppendLine("Case Tbl1.Status When 'O' Then 'Outstanding' When 'F' Then 'Fulfilled' When 'P' Then 'Partial' End As StatusDesc, ");
            SQL.AppendLine("Case Tbl1.Type When '1' Then Tbl6.DocDt When '2' Then Tbl7.DocDt End As PaidDt ");
            SQL.AppendLine("From (");

            #region Purchase Invoice

            SQL.AppendLine("    Select DocDt, Period, '1' As Type, 'Purchase Invoice' As TypeDesc, CurCode, PaymentType, ");
            SQL.AppendLine("    VdCode, DeptCode, DocNo, VdInvNo, ");
            SQL.AppendLine("    InvoiceAmt, DownPayment, PaidAmt, Balance, SiteCode, ");
            SQL.AppendLine("    DueDt, EarliestPaidDt, DueToPaidDays, AgingDays, PtName, ");
            SQL.AppendLine("    Case When Balance<=0 Then 'F' Else ");
            SQL.AppendLine("        Case When Balance>InvoiceAmt Then 'P' Else 'O' End ");
            SQL.AppendLine("    End As Status, ");
            SQL.AppendLine("    Case When (AgingDays < 0) then Balance Else 0.00 End As AgingCurrent, ");
            SQL.AppendLine("    Case When (AgingDays > 0 And AgingDays < 31) then Balance Else 0.00 End As Aging1To30,	");
            SQL.AppendLine("    Case When (AgingDays > 30 And AgingDays < 61) then Balance Else 0.00 End As Aging31To60, ");
            SQL.AppendLine("    Case When (AgingDays > 60 And AgingDays < 91) then Balance Else 0.00 End As Aging61To90,  ");
            SQL.AppendLine("    Case When (AgingDays > 90 And AgingDays < 121) then Balance Else 0.00 End As Aging91To120, ");
            SQL.AppendLine("    Case When (AgingDays > 120) then Balance Else 0.00 End As AgingOver120 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.DocDt, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("        A.CurCode, A.PaymentType, A.VdCode, A.DeptCode, A.SiteCode, A.DocNo, A.VdInvNo, ");
            SQL.AppendLine("        (A.Amt+A.TaxAmt) As InvoiceAmt, A.DownPayment As DownPayment, IfNull(B.PaidAmt, 0.00) As PaidAmt, ");
            SQL.AppendLine("        A.Amt+A.TaxAmt-A.DownPayment-IfNull(B.PaidAmt, 0) As Balance, ");
            SQL.AppendLine("        A.DueDt, B.EarliestPaidDt, ");
            SQL.AppendLine("        IfNull(DateDiff(Date_Format(B.EarliestPaidDt, '%Y%m%d'), Date_Format(A.DueDt, '%Y%m%d')), 0) As DueToPaidDays, ");
            SQL.AppendLine("        DateDiff(C.CurrentDateTime, Date_Format(A.DueDt, '%Y%m%d')) As AgingDays, D.PtName ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select DocNo, Min(DocDt) As EarliestPaidDt, Sum(Amt) As PaidAMt ");
            SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select T2.InvoiceDocNo As DocNo, T3.DocDt, T2.Amt ");
            SQL.AppendLine("                From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("                Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("                Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("                Inner Join TblPurchaseInvoiceHdr T4 ");
            SQL.AppendLine("                    On T2.InvoiceDocNo=T4.DocNo ");
            SQL.AppendLine("                    And T4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("                Where T1.CancelInd='N' ");
            SQL.AppendLine("                And T1.Status In ('O', 'A') ");
            SQL.AppendLine("                Union All ");
            SQL.AppendLine("                Select T1.PurchaseInvoiceDocNo As DocNo, T1.DocDt, T1.Amt ");
            SQL.AppendLine("                From TblAPSHdr T1 ");
            SQL.AppendLine("                Inner Join TblPurchaseInvoiceHdr T2 ");
            SQL.AppendLine("                    On T1.PurchaseInvoiceDocNo=T2.DocNo ");
            SQL.AppendLine("                    And T2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("                Where T1.CancelInd='N' ");
            SQL.AppendLine("            ) Tbl Group By DocNo ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Left Join (Select Left(CurrentDateTime(), 8) As  CurrentDateTime) C On 0=0 ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
        	SQL.AppendLine("	         Select Y.DocNo As PIDOcNo, Group_Concat(Distinct K.PtName Separator ', ') As PtName ");
        	SQL.AppendLine("	         From TblPurchaseInvoiceHdr Y ");
			SQL.AppendLine("	         Inner Join TblPurchaseInvoiceDtl X On Y.DocNo = X.DocNo ");
	        SQL.AppendLine("             Inner Join TblRecvVdDtl E On X.RecvVdDocNo=E.DocNo And X.RecvVdDNo=E.DNo ");
	        SQL.AppendLine("             Inner Join TblRecvVdhdr F On X.RecvVdDocNo=F.DocNo ");
	        SQL.AppendLine("             Inner Join TblPOHdr G On E.PODocNo=G.DocNo ");
	        SQL.AppendLine("             Inner Join TblPODtl H On E.PODocNo=H.DocNo And E.PODNo=H.DNo ");
	        SQL.AppendLine("             Inner Join TblPORequestDtl I On H.PORequestDocNo=I.DocNo And H.PORequestDNo=I.DNo ");
	        SQL.AppendLine("             Inner Join TblQtHdr J On I.QtDocNo=J.DocNo ");
	        SQL.AppendLine("             Left Join TblPaymentTerm K On J.PtCode=K.PtCode ");
	        SQL.AppendLine("             Where (Y.DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("             And Y.CancelInd = 'N' ");
	        SQL.AppendLine("             Group By Y.DocNo ");
            SQL.AppendLine("        )D On A.DocNo = D.PIDOcNo ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) T ");

            #endregion

            SQL.AppendLine("    Union All ");

            #region Purchase Return Invoice

            SQL.AppendLine("    Select DocDt, Period, '2' As Type, 'Purchase Return Invoice' As TypeDesc, ");
            SQL.AppendLine("    CurCode, Null As PaymentType, VdCode, DeptCode, DocNo, Null As VdInvNo, ");
            SQL.AppendLine("    InvoiceAmt, DownPayment, PaidAmt, Balance, SiteCode, ");
            SQL.AppendLine("    DueDt, EarliestPaidDt, DueToPaidDays, AgingDays, PtName, ");
            SQL.AppendLine("    Case When Balance<=0 Then 'F' Else ");
            SQL.AppendLine("        Case When Balance>InvoiceAmt Then 'P' Else 'O' End ");
            SQL.AppendLine("    End As Status, ");
            SQL.AppendLine("    0.00 As AgingCurrent, 0.00 As Aging1To30, 0.00 As Aging31To60, 0.00 As Aging61To90, 0.00 As Aging91To120, 0.00 As AgingOver120 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.DocDt, ");
            SQL.AppendLine("        Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("        A.CurCode, A.VdCode, A.DeptCode, A.SiteCode, A.DocNo, ");
            SQL.AppendLine("        A.Amt As InvoiceAmt, 0 As DownPayment, IfNull(B.PaidAmt, 0) As PaidAmt, ");
            SQL.AppendLine("        A.Amt-IfNull(B.PaidAmt, 0) As Balance, ");
            SQL.AppendLine("        Null As DueDt, B.EarliestPaidDt, ");
            SQL.AppendLine("        0.00 As DueToPaidDays, 0 As AgingDays, C.PtName ");
            SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("             Select T2.InvoiceDocNo As DocNo, Min(T3.DocDt) As EarliestPaidDt, Sum(T2.Amt) As PaidAMt ");
            SQL.AppendLine("            From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("            Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("            Inner Join TblPurchaseInvoiceHdr T4 ");
            SQL.AppendLine("                On T2.InvoiceDocNo=T4.DocNo ");
            SQL.AppendLine("                And T4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("            Where T1.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("            Group By T2.InvoiceDocNo ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
        	SQL.AppendLine("	         Select Y.DocNo As PRIDocNo, Group_Concat(Distinct I.PtName Separator ', ') As PtName ");
        	SQL.AppendLine("	         From TblPurchaseReturnInvoiceHdr Y ");
			SQL.AppendLine("	         Inner Join TblPurchaseReturnInvoiceDtl X On Y.DocNo = X.DocNo ");
	        SQL.AppendLine("             Left Join TblDOVdDtl D On X.DOVdDocNo=D.DocNo And X.DOVdDNo=D.DNo ");
	        SQL.AppendLine("             Left Join TblRecvVdDtl E On D.RecvVdDocNo=E.DocNo And D.RecvVdDNo=E.DNo ");
	        SQL.AppendLine("             Left Join TblPODtl F On E.PODocNo=F.DocNo And E.PODNo=F.DNo ");
	        SQL.AppendLine("             Left Join TblPORequestDtl G On F.PORequestDocNo=G.DocNo And F.PORequestDNo=G.DNo ");
	        SQL.AppendLine("             Left Join TblQtHdr H On G.QtDocNo=H.DocNo ");
	        SQL.AppendLine("             Left Join TblPaymentTerm I On H.PtCode=I.PtCode ");
	        SQL.AppendLine("             Where (Y.DocDt Between @DocDt1 And @DocDt2) ");
	        SQL.AppendLine("             And Y.CancelInd = 'N' ");
	        SQL.AppendLine("             Group By X.DocNo ");
            SQL.AppendLine("        )C On A.DocNo = C.PRIDocNo ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) T ");

            #endregion

            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Inner Join TblVendor Tbl2 On Tbl1.VdCode=Tbl2.VdCode ");
            
            if (mIsFilterByDept)
            {
                SQL.AppendLine("INNER Join TblDepartment Tbl3 On Tbl1.DeptCode = Tbl3.DeptCode ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=Tbl3.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Left Join TblDepartment Tbl3 On Tbl1.DeptCode = Tbl3.DeptCode ");
            }
            SQL.AppendLine("Left Join TblOption Tbl4 On Tbl1.PaymentType = Tbl4.OptCode And Tbl4.OptCat = 'VoucherPaymentType' ");
            SQL.AppendLine("Left Join TblSite Tbl5 On Tbl1.SiteCode = Tbl5.SiteCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Date_Format(DocDt, '%d/%m/%Y') Order By DocDt ASC Separator ', ') As DocDt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, T3.DocDt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("        Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T4 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T4.DocNo ");
            SQL.AppendLine("            And T4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T1.PurchaseInvoiceDocNo As DocNo, ");
            SQL.AppendLine("        T1.DocDt ");
            SQL.AppendLine("        From TblAPSHdr T1 ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T2 ");
            SQL.AppendLine("            On T1.PurchaseInvoiceDocNo=T2.DocNo ");
            SQL.AppendLine("            And T2.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("    ) Tbl Group By DocNo ");
            SQL.AppendLine(") Tbl6 On Tbl1.DocNo=Tbl6.DocNo ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Date_Format(DocDt, '%d/%m/%Y') Order By DocDt ASC Separator ', ') As DocDt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.InvoiceDocNo As DocNo, T3.DocDt ");
            SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("        Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceHdr T4 ");
            SQL.AppendLine("            On T2.InvoiceDocNo=T4.DocNo ");
            SQL.AppendLine("            And T4.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    ) Tbl Group By DocNo ");
            SQL.AppendLine(") Tbl7 On Tbl1.DocNo=Tbl7.DocNo ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Period",
                        "Type",
                        "Currency", 
                        "Vendor", 
                        "Department",
                        
                        //6-10
                        "Invoice#",
                        "Type",
                        "",
                        "",
                        "Vendor's" + Environment.NewLine + "Invoice#",
                        
                        
                        //11-15
                        "Status",
                        "Invoice Amount",
                        "Downpayment",
                        "Paid Amount",
                        "Balance",
                        
                        //16-20
                        "Due Date",
                        "Paid Date",
                        "Due To Paid"+Environment.NewLine+"(Days)",
                        "Aging"+Environment.NewLine+"(Days)",
                        "Aging"+Environment.NewLine+"Current AP",
                        
                        //21-25
                        "Over Due"+Environment.NewLine+"1-30 Days",
                        "Over Due"+Environment.NewLine+"31-60 Days",
                        "Over Due"+Environment.NewLine+"61-90 Days",
                        "Over Due"+Environment.NewLine+"91-120 Days",
                        "Over 120 Days",
                        
                        //26-30
                        "Payment Type",
                        "Term of"+Environment.NewLine+"Payment",
                        "DocDt",
                        "Rate",
                        "Invoice"+Environment.NewLine+"Conversion",

                        //31-32
                        "Balance"+Environment.NewLine+"Conversion",
                        "Site",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 150, 60, 250, 130, 
                        
                        //6-10
                        130, 0, 20, 20, 100, 

                        //11-15
                        80, 120, 120, 120, 120, 
 
                        //16-20
                        80, 160, 100, 100, 100, 

                        //21-25
                        100,100, 100, 100, 100,

                        //26-30
                        100, 150, 100, 100, 120, 

                        //31-32
                        120, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 8, 9 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 18, 19, 20, 21, 22, 23, 24, 25, 29, 30, 31 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 18, 21, 22, 23, 24, 25, 27, 28 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 30, 31, 32 });
            if (mIsSiteMandatory)
                Grd1.Cols[32].Move(6);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 32 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {4, 6,  8, 9, 10, 18, 27 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDueDt1), Sm.GetDte(DteDueDt2), "Tbl1.DueDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "Tbl1.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "Tbl1.Type", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "Tbl1.DeptCode", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By Tbl1.Period, Tbl1.Type, Tbl1.CurCode, Tbl2.VdName; ",
                        new string[]
                            { 
                                //0
                                "Period", 

                                //1-5
                                "TypeDesc", "CurCode", "VdName", "DeptName", "DocNo", 
                                
                                //6-10
                                 "Type", "VdInvNo", "StatusDesc", "InvoiceAmt", "Downpayment",   
                                
                                //11-15
                                "PaidAmt","Balance", "DueDt", "PaidDt", "DueToPaidDays",   
                                
                                //16-20
                                "AgingDays","AgingCurrent", "Aging1To30", "Aging31To60", "Aging61To90",
                                
                                //21-25
                                "Aging91To120", "AgingOver120", "PaymentTypes", "DocDt", "SiteName",

                                //26
                                "PtName"
                            },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);

                            string curcode1 = Sm.GetGrdStr(Grd1, Row, 3);
                            string datetrx = Sm.GetGrdStr(Grd1, Row, 28);

                            string AmtRate = Sm.GetValue("Select ifnull(Amt, 0) from TblCurrencyRate "+
                                                "Where RateDt  = "+
                                                "(Select MAX(rateDt) "+
                                                "From  TblCurrencyrate "+
                                                "Where CurCode2='IDR' And CurCode1='"+curcode1+"' And RateDt < " + datetrx + " " +
                                                "Group by CurCode1) And CurCode2='IDR' And CurCode1='" + curcode1 + "' And RateDt < " + datetrx + " ;");
                            if(curcode1 == "IDR")
                            {
                                Grd1.Cells[Row, 29].Value = 1;
                            }
                            else
                            {
                            Grd1.Cells[Row, 29].Value = AmtRate.Length>0? Decimal.Parse(AmtRate):0;
                            }
                            Grd1.Cells[Row, 30].Value = Sm.GetGrdDec(Grd1, Row, 12) * Sm.GetGrdDec(Grd1, Row, 29);
                            Grd1.Cells[Row, 31].Value = Sm.GetGrdDec(Grd1, Row, 15) * Sm.GetGrdDec(Grd1, Row, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 25);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13, 14, 15, 30, 31 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            AdjustSubtotals();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "1")
                {
                    var f1 = new FrmPurchaseInvoice(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f2.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 7) == "1")
                {
                    var f1 = new FrmPurchaseInvoice(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f2.ShowDialog();
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetLueType()
        {
            Sm.SetLue2(
                ref LueType,
                "Select '1' As Col1, 'Purchase Invoice' As Col2 " +
                "Union All Select '2' As Col1, 'Purchase Return Invoice' As Col2;",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDueDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDueDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDueDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueType), "<Refresh>")) LueType.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        
    }
}
