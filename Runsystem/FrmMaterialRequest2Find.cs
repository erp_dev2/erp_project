﻿#region Update
/*
    17/10/2019 [TKG/TWC] tambah informasi site
    23/10/2019 [WED/TWC] bug saat refresh dan hasil nya 1 row doang
    31/10/2019 [WED/TWC] hanya menampilkan MR yg Exim Ind = 'Y'
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    30/03/2020 [IBL] tambah kolom budget category
    06/11/2020 [WED/IMS] ambil data yang kolom MaterialRequestServiceDocNo dan MaterialRequestServiceDNo nya Null
    09/11/2020 [WED/MAI] ambil data yang kolom MaterialRequestServiceDocNo dan MaterialRequestServiceDNo nya Null
    22/02/2023 [BRI/HEX] tambah kolom MR Type & Request Type berdasarkan param IsMRRoutineUseMRRoutineType
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmMaterialRequest2 mFrmParent;
        private string mSQL = string.Empty;
        internal bool mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmMaterialRequest2Find(FrmMaterialRequest2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, C.DeptName, F.SiteName, B.CancelInd, G.BCName, ");
            SQL.AppendLine("A.LocalDocNo, ");
            if (mFrmParent.mIsMRRoutineUseMRRoutineType)
                SQL.AppendLine("H.OptDesc MRType, I.OptDesc ReqType, ");
            else
                SQL.AppendLine("Null MRType, Null ReqType, ");
            if (Sm.GetParameterBoo("IsMREximSplitDocument"))
                SQL.AppendLine("Case B.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            else
                SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("B.ItCode, D.ItName, B.Qty, D.PurchaseUomCode, E.DocDt As QtDocDt, B.UPrice, (B.Qty*B.UPrice) As Total, B.UsageDt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And A.EximInd = 'Y' ");
            SQL.AppendLine("    And B.MaterialRequestServiceDocNo Is Null And B.MaterialRequestServiceDNo Is Null ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblQtHdr E On B.QtDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode=F.SiteCode ");
            SQL.AppendLine("Left Join TblBudgetCategory G On A.BCCode=G.BCCode ");
            SQL.AppendLine("Left Join TblOption H On H.OptCat='MRRoutineType' And A.MRType=H.OptCode ");
            SQL.AppendLine("Left Join TblOption I On I.OptCat='ReqType' And A.ReqType=I.OptCode ");
            SQL.AppendLine("Where A.WODocNo Is Null ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Status",
                        "Local Document",
                        
                        //6-10
                        "Department",
                        "Site",
                        "Item's Code",
                        "Item's Name",
                        "Quantity",
                        
                        //11-15
                        "UoM",
                        "Usage Date",
                        "Created By",
                        "Created Date", 
                        "Created Time",

                        //16-20
                        "Last Updated By", 
                        "Last Updated Date", 
                        "Last Updated Time",
                        "Budget Category",
                        "MR Type",

                        //21
                        "Request Type"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 60, 100, 130, 
                        
                        //6-10
                        200, 200, 120, 250, 100, 
                        
                        //11-15
                        80, 130, 130, 130, 130,     
                        
                        //16-20
                        130, 130, 130, 150, 130,

                        //21
                        130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 14, 17 });
            Sm.GrdFormatTime(Grd1, new int[] { 15, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17, 18 }, false);
            if (!mFrmParent.mIsMRRoutineUseMRRoutineType)
                Sm.GrdColInvisible(Grd1, new int[] { 20, 21 }, false);
            Sm.SetGrdProperty(Grd1 , false);
            Grd1.Cols[19].Move(13);
            Grd1.Cols[20].Move(6);
            Grd1.Cols[21].Move(7);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17, 18 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();
                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, string.Concat(mSQL, Filter, " Order By A.CreateDt Desc;"),
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "LocalDocNo", "DeptName", 
                            
                            //6-10
                            "SiteName", "ItCode", "ItName", "Qty", "PurchaseUomCode", 
                            
                            //11-15
                            "UsageDt", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                            //16-18
                            "BCName", "MRType", "ReqType"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

        }
        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
