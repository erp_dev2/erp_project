﻿namespace RunSystem
{
    partial class FrmTransferRequestProjectDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTransferRequestProjectDlg));
            this.ChkItCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueItCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkItCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.BtnBom = new DevExpress.XtraEditors.SimpleButton();
            this.TxtBom = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkBom = new DevExpress.XtraEditors.CheckEdit();
            this.ChkWhsCode = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.ChkItCtCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueItCtCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.ChkItCode);
            this.panel2.Size = new System.Drawing.Size(672, 53);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 420);
            this.Grd1.TabIndex = 23;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkItCtCode
            // 
            this.ChkItCtCode.Location = new System.Drawing.Point(322, 24);
            this.ChkItCtCode.Name = "ChkItCtCode";
            this.ChkItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCtCode.Properties.Caption = " ";
            this.ChkItCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCtCode.Size = new System.Drawing.Size(31, 22);
            this.ChkItCtCode.TabIndex = 15;
            this.ChkItCtCode.ToolTip = "Remove filter";
            this.ChkItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCtCode.ToolTipTitle = "Run System";
            this.ChkItCtCode.CheckedChanged += new System.EventHandler(this.ChkItCtCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItCtCode
            // 
            this.LueItCtCode.EnterMoveNextControl = true;
            this.LueItCtCode.Location = new System.Drawing.Point(64, 26);
            this.LueItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode.Name = "LueItCtCode";
            this.LueItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode.Properties.DropDownRows = 25;
            this.LueItCtCode.Properties.NullText = "[Empty]";
            this.LueItCtCode.Properties.PopupWidth = 500;
            this.LueItCtCode.Size = new System.Drawing.Size(255, 20);
            this.LueItCtCode.TabIndex = 14;
            this.LueItCtCode.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode.EditValueChanged += new System.EventHandler(this.LueItCtCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(26, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "Item";
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(64, 4);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Size = new System.Drawing.Size(255, 20);
            this.TxtItCode.TabIndex = 11;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // ChkItCode
            // 
            this.ChkItCode.Location = new System.Drawing.Point(322, 2);
            this.ChkItCode.Name = "ChkItCode";
            this.ChkItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCode.Properties.Caption = " ";
            this.ChkItCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCode.Size = new System.Drawing.Size(31, 22);
            this.ChkItCode.TabIndex = 12;
            this.ChkItCode.ToolTip = "Remove filter";
            this.ChkItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCode.ToolTipTitle = "Run System";
            this.ChkItCode.CheckedChanged += new System.EventHandler(this.ChkItCode_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkWhsCode);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.LueWhsCode);
            this.panel4.Controls.Add(this.BtnBom);
            this.panel4.Controls.Add(this.TxtBom);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.ChkBom);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(343, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(325, 49);
            this.panel4.TabIndex = 16;
            // 
            // BtnBom
            // 
            this.BtnBom.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBom.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBom.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBom.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBom.Appearance.Options.UseBackColor = true;
            this.BtnBom.Appearance.Options.UseFont = true;
            this.BtnBom.Appearance.Options.UseForeColor = true;
            this.BtnBom.Appearance.Options.UseTextOptions = true;
            this.BtnBom.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBom.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBom.Image = ((System.Drawing.Image)(resources.GetObject("BtnBom.Image")));
            this.BtnBom.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBom.Location = new System.Drawing.Point(268, 3);
            this.BtnBom.Name = "BtnBom";
            this.BtnBom.Size = new System.Drawing.Size(24, 21);
            this.BtnBom.TabIndex = 18;
            this.BtnBom.ToolTip = "List of Bom";
            this.BtnBom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBom.ToolTipTitle = "Run System";
            this.BtnBom.Click += new System.EventHandler(this.BtnBom_Click);
            // 
            // TxtBom
            // 
            this.TxtBom.EnterMoveNextControl = true;
            this.TxtBom.Location = new System.Drawing.Point(67, 3);
            this.TxtBom.Name = "TxtBom";
            this.TxtBom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBom.Properties.Appearance.Options.UseFont = true;
            this.TxtBom.Properties.MaxLength = 250;
            this.TxtBom.Size = new System.Drawing.Size(203, 20);
            this.TxtBom.TabIndex = 17;
            this.TxtBom.Validated += new System.EventHandler(this.TxtBom_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "BOM";
            // 
            // ChkBom
            // 
            this.ChkBom.Location = new System.Drawing.Point(298, 2);
            this.ChkBom.Name = "ChkBom";
            this.ChkBom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBom.Properties.Appearance.Options.UseFont = true;
            this.ChkBom.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBom.Properties.Caption = " ";
            this.ChkBom.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBom.Size = new System.Drawing.Size(19, 22);
            this.ChkBom.TabIndex = 19;
            this.ChkBom.ToolTip = "Remove filter";
            this.ChkBom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBom.ToolTipTitle = "Run System";
            this.ChkBom.CheckedChanged += new System.EventHandler(this.ChkBom_CheckedChanged);
            // 
            // ChkWhsCode
            // 
            this.ChkWhsCode.Location = new System.Drawing.Point(298, 26);
            this.ChkWhsCode.Name = "ChkWhsCode";
            this.ChkWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWhsCode.Properties.Appearance.Options.UseFont = true;
            this.ChkWhsCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkWhsCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkWhsCode.Properties.Caption = " ";
            this.ChkWhsCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWhsCode.Size = new System.Drawing.Size(23, 22);
            this.ChkWhsCode.TabIndex = 22;
            this.ChkWhsCode.ToolTip = "Remove filter";
            this.ChkWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkWhsCode.ToolTipTitle = "Run System";
            this.ChkWhsCode.CheckedChanged += new System.EventHandler(this.ChkWhsCode_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(-1, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 14);
            this.label2.TabIndex = 20;
            this.label2.Text = "Warehouse";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(68, 25);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 300;
            this.LueWhsCode.Size = new System.Drawing.Size(225, 20);
            this.LueWhsCode.TabIndex = 21;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // FrmTransferRequestProjectDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmTransferRequestProjectDlg";
            this.Text = "List of Item";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkItCtCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtItCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCode;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnBom;
        public DevExpress.XtraEditors.TextEdit TxtBom;
        public DevExpress.XtraEditors.CheckEdit ChkBom;
        private DevExpress.XtraEditors.CheckEdit ChkWhsCode;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
    }
}