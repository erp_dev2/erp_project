﻿#region Update
/*
    21/07/2018 [TKG] tambah filter batch#
    07/01/2020 [VIN/SIER] Parameter baru (mIsFilterByItCt) untuk Item Category per masing-masing Group
    19/01/2020 [TKG/IMS] tambah informasi local code, specification
    04/02/2020 [WED/MMM] tambah filter dan informasi dari Production Order, berdasarkan parameter IsDOWhsDisplayProductionOrderInfo
    01/11/2020 [TKG/IMS] memunculkan keterangan vendor
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Text.RegularExpressions;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhsDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOWhs mFrmParent;
        string mSQL = string.Empty, mWhsCode = string.Empty;
        private bool mIsInventoryShowTotalQty = false, mIsReCompute = false;
        internal bool mIsFilterByItCt = false;
        internal string mAccessInd = string.Empty, mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmDOWhsDlg(FrmDOWhs FrmParent, string WhsCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mWhsCode = WhsCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
                GetParameter();
                if (!mFrmParent.mIsDOWhsDisplayProductionOrderInfo)
                {
                    LblProductionOrderDocNo.Visible = TxtProductionOrderDocNo.Visible =
                    ChkProductionOrderDocNo.Visible = BtnProductionOrderDocNo.Visible = false;
                }
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);
                mAccessInd = mFrmParent.mAccessInd;
                mMenuCode = mFrmParent.mMenuCode;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, A.BatchNo, A.Source, C.ItCtName, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty-IfNull(D.Qty, 0) As Qty, A.Qty2-IfNull(D.Qty2, 0) As Qty2, A.Qty3-IfNull(D.Qty3, 0) As Qty3, ");
            SQL.AppendLine("B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, B.ItGrpCode, B.ForeignName, B.Specification, ");
            if (mFrmParent.mIsDOWhsDisplayProductionOrderInfo && TxtProductionOrderDocNo.Text.Length > 0)
                SQL.AppendLine("E.RecommendedQty, ");
            else
                SQL.AppendLine("0.00 As RecommendedQty, ");            
            if (mFrmParent.mIsDOWhsShowItemVendor)
                SQL.AppendLine("H.VdName ");
            else
                SQL.AppendLine("Null As VdName ");            
            SQL.AppendLine("From TblStockSummary A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.Source, T2.Lot, T2.Bin, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2, Sum(T2.Qty3) As Qty3  ");
            SQL.AppendLine("    From TblDOWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.ProcessInd='O' ");
            SQL.AppendLine("    Where T1.WhsCode=@WhsCode And T1.CancelInd='N' And T1.Status<>'C' And T1.TransferRequestWhsDocNo Is Null ");
            SQL.AppendLine("    Group By T2.Source, T2.Lot, T2.Bin ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.Source=D.Source ");
            SQL.AppendLine("    And A.Lot=D.Lot ");
            SQL.AppendLine("    And A.Bin=D.Bin ");

            if (mFrmParent.mIsDOWhsDisplayProductionOrderInfo)
            {
                if (TxtProductionOrderDocNo.Text.Length > 0)
                {
                    SQL.AppendLine("Inner Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT Distinct C.ItCode, IfNull((A.Qty * C.Qty), 0.00) RecommendedQty ");
                    SQL.AppendLine("    FROM TblProductionOrderHdr A ");
                    SQL.AppendLine("    INNER JOIN TblProductionOrderDtl B ON A.DocNo = B.DocNo ");
                    SQL.AppendLine("        AND A.DocNo = @ProductionOrderDocNo ");
                    SQL.AppendLine("    INNER JOIN TblBOMDtl2 C ON B.BOMDocNo = C.DocNo ");
                    SQL.AppendLine("        AND C.ItCode NOT IN ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            SELECT DISTINCT X2.DocCode ");
                    SQL.AppendLine("            FROM TblProductionOrderDtl X1 ");
                    SQL.AppendLine("            INNER JOIN TblBOMDtl X2 ON X1.BOMDocNo = X2.DocNo ");
                    SQL.AppendLine("                AND X1.DocNo = @ProductionOrderDocNo ");
                    SQL.AppendLine("                AND X2.DocType = '1' ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(") E On A.ItCode = E.ItCode ");
                }
            }
            if (mFrmParent.mIsDOWhsShowItemVendor)
            {
                SQL.AppendLine("Left Join TblRecvVdDtl F On A.Source=F.Source ");
                SQL.AppendLine("Left Join TblRecvVdHdr G On F.DocNo=G.DocNo ");
                SQL.AppendLine("Left Join TblVendor H On G.VdCode=H.VdCode ");
            }

            SQL.AppendLine("Where A.WhsCode=@WhsCode ");
            SQL.AppendLine("And A.Qty > 0 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
           
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item's Code", 
                    "", 
                    "Item's Name", 
                    "Category",

                    //6-10
                    "Batch#",
                    "Source",
                    "Lot",
                    "Bin", 
                    "Stock",

                    //11-15
                    "UoM",
                    "Stock",
                    "UoM",
                    "Stock",
                    "UoM",

                    //16-20
                    "Group",
                    "Foreign Name",
                    "ItCodeInternal",
                    "Specification",
                    "Recommended"+Environment.NewLine+"Quantity",

                    //21
                    "Vendor"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 120, 20, 250, 150,
                    
                    //6-10
                    200, 200, 60, 80, 80, 
                    
                    //11-15
                    80, 80, 80, 80, 80,

                    //16-20
                    100, 150, 120, 200, 120,

                    //21
                    200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 12, 14, 20 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7, 12, 13, 14, 15, 16, 19, 21 }, false);
            if (!mFrmParent.mIsDOWhsDisplayProductionOrderInfo) Sm.GrdColInvisible(Grd1, new int [] { 20 });
            ShowInventoryUomCode();
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[16].Visible = true;
                Grd1.Cols[16].Move(4);
            }
            Grd1.Cols[18].Move(6);
            Grd1.Cols[19].Move(7);
            Grd1.Cols[17].Move(5);

            if (mFrmParent.mIsDOWhsShowItemVendor) Sm.GrdColInvisible(Grd1, new int[] { 21 }, true);

            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7, 19 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 12, 13 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15 }, true);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter2 = string.Empty;

                Sm.GenerateSQLFilterForInventory(ref cm, ref Filter2, "A", ref mFrmParent.Grd1, 9, 10, 11);
                var Filter = (Filter2.Length > 0) ? " And (" + Filter2 + ") " : " And 0=0 ";

                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@WhsCode", mWhsCode);
                Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtProductionOrderDocNo.Text);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItCodeInternal", "B.ItName", "B.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, new string[] { "A.Lot" });
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, new string[] { "A.Bin" });
                if (ChkBatchNo.Checked)
                    FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", "_1");
                if (ChkBatchNo2.Checked)
                    FilterStr(ref Filter, ref cm, TxtBatchNo2.Text, "A.BatchNo", "_2");
                
                mIsReCompute = false;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select * From ( " + GetSQL() + Filter + 
                        ") T Where (Qty>0) Order By ItName, BatchNo;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItName", "ItCtName", "BatchNo", "Source", "Lot", 
                            
                            //6-10
                            "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //11-15
                            "Qty3", "InventoryUomCode3", "ItGrpCode", "ForeignName", "ItCodeInternal",
 
                            //16-18
                            "Specification", "RecommendedQty", "VdName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        }, true, false, false, false
                    );
                    if (mIsInventoryShowTotalQty)
                    {
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 12, 14 });
                    }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 19);
                        if (mFrmParent.mDOWHSDefaultQty == "1")
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 13, 16, 19 });
                        else
                        {
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 10);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 12);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 14);
                        }
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 21);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 8), Sm.GetGrdStr(Grd1, Row, 6)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 9), Sm.GetGrdStr(Grd1, Row, 7)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 10), Sm.GetGrdStr(Grd1, Row, 8)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 11), Sm.GetGrdStr(Grd1, Row, 9))
                    )
                    return true;
            }
            return false;
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 )
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsInventoryShowTotalQty && mIsReCompute) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");

        }
        #endregion

        #region Event

        #region Button Clicks

        private void BtnProductionOrderDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOWhsDlg2(this));
        }

        #endregion

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtProductionOrderDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProductionOrderDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Order#");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtBatchNo2_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
