﻿#region Update
/* 
    16/12/2017 [TKG] new reporting
 *  27/07/2020 [HAR/IMS] tambah informasi resign date
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptLeaveSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptLeaveSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                SetGrd();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Yr, B.LeaveName, A.EmpCode, C.EmpName, C.EmpCodeOld, ");
            SQL.AppendLine("D.SiteName, E.DeptName, F.PosName, ");
            SQL.AppendLine("C.JoinDt, A.InitialDt, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.NoOfDays1, A.NoOfDays2, A.NoOfDays3, A.Balance, C.ResignDt  ");
            SQL.AppendLine("From TblLeaveSummary A ");
            SQL.AppendLine("Inner Join TblLeave B On A.LeaveCode=B.LeaveCode ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And C.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And C.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(C.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblSite D On C.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblPosition F On C.PosCode=F.PosCode ");
            SQL.AppendLine("Where A.Yr=@Yr ");

            mSQL = SQL.ToString();
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Year",
                        "Leave",
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's"+Environment.NewLine+"Name",
                        "Old Code",

                        //6-10
                        "Site",
                        "Department",
                        "Position",
                        "Join",
                        "Permanent",
                        
                        //11-15
                        "Start",
                        "End",
                        "Quota",
                        "Used",
                        "Unused",

                        //16
                        "Balance",
                        "Resign"+Environment.NewLine+"Date"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        60, 180, 100, 200, 100, 
                        
                        //6-10
                        180, 180, 180, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 100, 
                        
                        //16
                        100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10, 11, 12, 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 15, 16 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5, 8 }, false);
            Grd1.Cols[17].Move(10);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "C.EmpCode", "C.EmpCodeOld", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "C.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.Yr, D.SiteName, E.DeptName, C.EmpName;",
                        new string[]
                        {
                            //0
                            "Yr",

                            //1-5
                            "LeaveName", "EmpCode", "EmpName", "EmpCodeOld", "SiteName", 
                            
                            //6-10
                            "DeptName", "PosName", "JoinDt", "InitialDt", "StartDt", 

                            //11-15
                            "EndDt", "NoOfDays1", "NoOfDays2", "NoOfDays3", "Balance", 
                            //16
                            "ResignDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion       

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
