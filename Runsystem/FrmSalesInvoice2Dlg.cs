﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesInvoice2 mFrmParent;
        string mSQL = string.Empty, mCtCode = string.Empty, mMenuCode="XXX";

        #endregion

        #region Constructor

        public FrmSalesInvoice2Dlg(FrmSalesInvoice2 FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, (UPriceBefore+TaxAmt) As UPriceAfterTax, (UPriceBefore+TaxAmt)*Qty As Total From ( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, B.DNo, A.SAName, E.ItCode, F.ItCodeInternal, F.ItName, ");
            SQL.AppendLine("B.QtyPackagingUnit, B.PackagingUnitUomCode,  B.Qty, D.PriceUomCode, A.CurCode, B.DeliveryDt, ");
            SQL.AppendLine("ifnull((E.UPrice*C.Discount *0.01), 0) As DiscountAmt, "); 
            SQL.AppendLine("C.UPrice As UPriceAfterDiscount,  ");
            SQL.AppendLine("IfNull(G.DiscRate, 0) As PromoRate,   ");
            SQL.AppendLine("(C.UPrice-(C.UPrice*0.01*IfNull(G.DiscRate, 0))) As UPriceBefore,"); 
            SQL.AppendLine("B.TaxRate,  ");
            SQL.AppendLine("((C.UPrice-(C.UPrice*0.01*IfNull(G.DiscRate, 0)))*0.01*B.TaxRate) As TaxAmt");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd5<>'F' ");
            SQL.AppendLine("Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblCtQtHdr C2 On A.CtQtDocNo = C2.DocNo And C2.PtCode = '"+Sm.GetParameter("CBDCode")+"' ");
            SQL.AppendLine("Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DOcNo ");
            SQL.AppendLine("Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DOcNo And C.ItemPriceDNo = E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode ");
            SQL.AppendLine("Left Join TblSOQuotPromoItem G On A.SOQuotPromoDocNo=G.DocNo And E.ItCode=G.ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status Not In ('M', 'F') ");
            SQL.AppendLine("And A.CtCode=@CtCode ");
            SQL.AppendLine("And Concat(B.DocNo, B.Dno) Not In (Select Concat(DOctDocNo,DOCtDno) As keyDocNo From TblSalesInvoicehdr A Inner Join TblSalesInvoiceDtl B On A.DocNo=B.DocNo Where A.SODocNo Is Not Null And CancelInd = 'N') ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Document Number",

                         //1-5
                        "Document Date",
                        "",
                        "Shipping Name",
                        "Item Code",
                        "",
                        
                        //6-10
                        "Local Item Code",
                        "Item Name",
                        "Packaging" + Environment.NewLine + "Quantity",
                        "Packaging" + Environment.NewLine + "UoM",
                        "Quantity",

                        //11-15
                        "Sales" + Environment.NewLine + "UoM",
                        "Currency",
                        "Unit Price" + Environment.NewLine + "(Before Tax)",
                        "Tax"+ Environment.NewLine + "Rate",
                        "Tax"+ Environment.NewLine + "Amount",
                        
                        //16-19
                        "Unit Price" + Environment.NewLine + "(After Tax)",
                        "Amount",
                    },
                     new int[] 
                    {
                        //0
                        150, 

                        //1-5
                        100, 20, 150, 150, 20,
 
                        //6-10
                        130, 150, 100, 80, 100, 
                        
                        //11-15
                        80, 80, 100, 80, 100, 
                        
                        //16-17
                        100, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2,5 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 13, 14, 15, 16, 17}, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0,1,3,4,6,7,8,9,10,11,12,13,14,15,16,17 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By DocDt, DocNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "SAName", "ItCode", "ItCodeInternal", "ItName",   

                            //6-10
                            "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", "CurCode", 

                            //11-14
                            "UPriceBefore", "TaxRate", "TaxAmt", "UPriceAfterTax", "Total"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        }, true, false, false, false
                    ); 
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        protected override void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtSODocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 0);
                mFrmParent.ShowSODtl(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 0));
                mFrmParent.ComputeAmt();
                this.Close();
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
