﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRawMaterialVerifyOld : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = "", mAccessInd = "",
            mDocNo = ""; //if this application is called from other application;
        internal FrmRawMaterialVerifyFind FrmFind;

        #endregion

        #region Constructor

        public FrmRawMaterialVerifyOld(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            base.FrmLoad(sender, e);

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, EventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtRecvRawMaterialDocNo, MeeRemark, DteDocDt,
                        TxtLegalDocVerifyDocNo, TxtQueueNo, TxtVdCode, TxtRawMaterialOpnameDocNo
                    }, true);
                    TxtDocNo.Focus();
                    BtnRecvRawMaterialDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, 
                    }, false);
                    BtnRecvRawMaterialDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNo, TxtRecvRawMaterialDocNo, MeeRemark, DteDocDt, TxtLegalDocVerifyDocNo, 
               TxtQueueNo, TxtVdCode, TxtRawMaterialOpnameDocNo
            });
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRawMaterialVerifyFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<RecvRawMat>();
            var ldtl = new List<RecvRawMatDtl>();
            var ldtl2 = new List<RecvRawMatDtlEmployee>();

            string[] TableName = { "RecvRawMat", "RecvRawMatDtl", "RecvRawMatDtlEmployee" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();


            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.QueueNo, B.VehicleRegNo, A.LegalDocVerifyDocNo, C.VdName, ");
            SQL.AppendLine("E.TTName, A.UnloadStartDt, A.UnloadStartTm, A.UnloadEndDt, A.UnloadEndTm, A.Remark, ");
            SQL.AppendLine("A.Shelf1, A.Shelf2, A.Shelf3, A.Shelf4, A.Shelf5, A.Shelf6, A.Shelf7, A.Shelf8, A.Shelf9, A.Shelf10, F.UserName As CreateBy ");
            SQL.AppendLine("From TblRecvRawMaterialHdr A ");
            SQL.AppendLine("Left Join TblLegalDocVerifyHdr B On A.LegalDocVerifyDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblVendor C On B.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblLoadingQueue D On B.QueueNo=D.DocNo ");
            SQL.AppendLine("Left Join TblTransportType E On D.TTCode=E.TTCode ");
            SQL.AppendLine("Left Join TblUser F On A.CreateBy = F.UserCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvRawMaterialDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo", 
                         "DocDt", 
                         
                         //6-10
                         "QueueNo", 
                         "VehicleRegNo", 
                         "LegalDocVerifyDocNo", 
                         "VdName", 
                         "TTName", 
                        
                         //11-15
                         "UnloadStartDt", 
                         "UnloadStartTm", 
                         "UnloadEndDt", 
                         "UnloadEndTm",                        
                         "Remark", 
                        
                         //16-20
                         "Shelf1", 
                         "Shelf2", 
                         "Shelf3", 
                         "Shelf4", 
                         "Shelf5", 
                         
                         //21 - 25
                         "Shelf6", 
                         "Shelf7", 
                         "Shelf8", 
                         "Shelf9",                                                
                         "Shelf10", 

                         "CreateBy"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RecvRawMat()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            QueueNo = Sm.DrStr(dr, c[6]),
                            VehicleReqNo = Sm.DrStr(dr, c[7]),
                            LegalDocVerifyDocNo = Sm.DrStr(dr, c[8]),
                            VdName = Sm.DrStr(dr, c[9]),
                            TTName = Sm.DrStr(dr, c[10]),
                            UnloadStartDtTm = Sm.DrStr(dr, c[11]) + " " + Sm.DrStr(dr, c[12]),
                            UnloadEndDtTm = Sm.DrStr(dr, c[13]) + " " + Sm.DrStr(dr, c[14]),
                            Remark = Sm.DrStr(dr, c[15]),
                            Shelf1 = Sm.DrStr(dr, c[16]),
                            Shelf2 = Sm.DrStr(dr, c[17]),
                            Shelf3 = Sm.DrStr(dr, c[18]),
                            Shelf4 = Sm.DrStr(dr, c[19]),
                            Shelf5 = Sm.DrStr(dr, c[20]),
                            Shelf6 = Sm.DrStr(dr, c[21]),
                            Shelf7 = Sm.DrStr(dr, c[22]),
                            Shelf8 = Sm.DrStr(dr, c[23]),
                            Shelf9 = Sm.DrStr(dr, c[24]),
                            Shelf10 = Sm.DrStr(dr, c[25]),
                            CreateBy = Sm.DrStr(dr, c[26]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.SectionNo, A.ItCode, B.ItName, A.Diameter, A.Qty, B.PurchaseUomCode, ");
                SQLDtl.AppendLine("C.Shelf1, C.Shelf2, C.Shelf3, C.Shelf4, C.Shelf5, C.Shelf6, C.Shelf7, C.Shelf8, ");
                SQLDtl.AppendLine("C.Shelf9, C.Shelf10 ");
                SQLDtl.AppendLine("From TblRecvRawMaterialDtl2 A ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQLDtl.AppendLine("Inner Join TblRecvRawMaterialHdr C On A.DocNo=C.DocNo ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl.AppendLine("Order By A.ItCode, A.SectionNo, A.Diameter");
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtRecvRawMaterialDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "SectionNo" ,

                         //1-5
                         "ItCode" ,
                         "ItName" ,
                         "Diameter",
                         "Qty",
                         "PurchaseUomCode",

                         "Shelf1",
                         "Shelf2",
                         "Shelf3",
                         "Shelf4",
                         "Shelf5",
                         "Shelf6",
                         "Shelf7",
                         "Shelf8",
                         "Shelf9",
                         "Shelf10"
                        });
                if (drDtl.HasRows)
                {
                    string ItCode = "";
                    string CurrentSection = "";
                    int FirstItemRow = 0;
                    int TotalSectionRow = 0;
                    bool AddNewRow = false;
                    string SectionTitle = "";
                    int MyCol = 0;
                    while (drDtl.Read())
                    {
                        AddNewRow = false;
                        if (ItCode != Sm.DrStr(drDtl, cDtl[1]))
                        {
                            MyCol = 0;
                            FirstItemRow = ldtl.Count;
                            ItCode = Sm.DrStr(drDtl, cDtl[1]);
                            CurrentSection = Sm.DrStr(drDtl, cDtl[0]);
                            TotalSectionRow = 0;
                            AddNewRow = true;
                            SectionTitle = Math.Floor((Decimal)MyCol / 3).ToString();
                            //(Sm.DrStr(drDtl, cDtl[0]) == "1" || Sm.DrStr(drDtl, cDtl[0]) == "2" || Sm.DrStr(drDtl, cDtl[0]) == "3") ? "A" :
                            //((Sm.DrStr(drDtl, cDtl[0]) == "4" || Sm.DrStr(drDtl, cDtl[0]) == "5" || Sm.DrStr(drDtl, cDtl[0]) == "6") ? "B" :
                            //((Sm.DrStr(drDtl, cDtl[0]) == "7" || Sm.DrStr(drDtl, cDtl[0]) == "8" || Sm.DrStr(drDtl, cDtl[0]) == "9") ? "C" : "D"));
                        }
                        else
                        {
                            if (CurrentSection != Sm.DrStr(drDtl, cDtl[0]))
                            {
                                MyCol++;
                                TotalSectionRow = 0;
                                CurrentSection = Sm.DrStr(drDtl, cDtl[0]);
                            }
                            if (SectionTitle != Math.Floor((Decimal)MyCol / 3).ToString())
                            //((Sm.DrStr(drDtl, cDtl[0]) == "1" || Sm.DrStr(drDtl, cDtl[0]) == "2" || Sm.DrStr(drDtl, cDtl[0]) == "3") ? "A" :
                            //((Sm.DrStr(drDtl, cDtl[0]) == "4" || Sm.DrStr(drDtl, cDtl[0]) == "5" || Sm.DrStr(drDtl, cDtl[0]) == "6") ? "B" :
                            //((Sm.DrStr(drDtl, cDtl[0]) == "7" || Sm.DrStr(drDtl, cDtl[0]) == "8" || Sm.DrStr(drDtl, cDtl[0]) == "9") ? "C" : "D"))))
                            {
                                FirstItemRow = ldtl.Count;
                                ItCode = Sm.DrStr(drDtl, cDtl[1]);
                                CurrentSection = Sm.DrStr(drDtl, cDtl[0]);
                                TotalSectionRow = 0;
                                AddNewRow = true;
                                SectionTitle = Math.Floor((Decimal)MyCol / 3).ToString();
                                //(Sm.DrStr(drDtl, cDtl[0]) == "1" || Sm.DrStr(drDtl, cDtl[0]) == "2" || Sm.DrStr(drDtl, cDtl[0]) == "3") ? "A" :
                                //((Sm.DrStr(drDtl, cDtl[0]) == "4" || Sm.DrStr(drDtl, cDtl[0]) == "5" || Sm.DrStr(drDtl, cDtl[0]) == "6") ? "B" :
                                //((Sm.DrStr(drDtl, cDtl[0]) == "7" || Sm.DrStr(drDtl, cDtl[0]) == "8" || Sm.DrStr(drDtl, cDtl[0]) == "9") ? "C" : "D"));
                            }
                            if ((FirstItemRow + TotalSectionRow) > (ldtl.Count - 1))
                                AddNewRow = true;

                        }

                        if (AddNewRow)
                        {
                            ldtl.Add(new RecvRawMatDtl()
                            {
                                ItCode = Sm.DrStr(drDtl, cDtl[1]),
                                ItName = Sm.DrStr(drDtl, cDtl[2]),
                                SectionTitle = Math.Floor((Decimal)MyCol / 3).ToString(),
                                OrderNumber = ldtl.Count,
                                Section1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? GetShelfName(Sm.DrStr(drDtl, cDtl[0]), Sm.DrStr(drDtl, cDtl[6]), Sm.DrStr(drDtl, cDtl[7]), Sm.DrStr(drDtl, cDtl[8]), Sm.DrStr(drDtl, cDtl[9]), Sm.DrStr(drDtl, cDtl[10]), Sm.DrStr(drDtl, cDtl[11]), Sm.DrStr(drDtl, cDtl[12]), Sm.DrStr(drDtl, cDtl[13]), Sm.DrStr(drDtl, cDtl[14]), Sm.DrStr(drDtl, cDtl[15])) : ""),
                                Diameter1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? Sm.DrDec(drDtl, cDtl[3]) : 0),
                                Qty1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? Sm.DrDec(drDtl, cDtl[4]) : 0),
                                PurchaseUomCode1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? Sm.DrStr(drDtl, cDtl[5]) : ""),
                                Section2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? GetShelfName(Sm.DrStr(drDtl, cDtl[0]), Sm.DrStr(drDtl, cDtl[6]), Sm.DrStr(drDtl, cDtl[7]), Sm.DrStr(drDtl, cDtl[8]), Sm.DrStr(drDtl, cDtl[9]), Sm.DrStr(drDtl, cDtl[10]), Sm.DrStr(drDtl, cDtl[11]), Sm.DrStr(drDtl, cDtl[12]), Sm.DrStr(drDtl, cDtl[13]), Sm.DrStr(drDtl, cDtl[14]), Sm.DrStr(drDtl, cDtl[15])) : ""),
                                Diameter2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? Sm.DrDec(drDtl, cDtl[3]) : 0),
                                Qty2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? Sm.DrDec(drDtl, cDtl[4]) : 0),
                                PurchaseUomCode2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? Sm.DrStr(drDtl, cDtl[5]) : ""),
                                Section3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? GetShelfName(Sm.DrStr(drDtl, cDtl[0]), Sm.DrStr(drDtl, cDtl[6]), Sm.DrStr(drDtl, cDtl[7]), Sm.DrStr(drDtl, cDtl[8]), Sm.DrStr(drDtl, cDtl[9]), Sm.DrStr(drDtl, cDtl[10]), Sm.DrStr(drDtl, cDtl[11]), Sm.DrStr(drDtl, cDtl[12]), Sm.DrStr(drDtl, cDtl[13]), Sm.DrStr(drDtl, cDtl[14]), Sm.DrStr(drDtl, cDtl[15])) : ""),
                                Diameter3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? Sm.DrDec(drDtl, cDtl[3]) : 0),
                                Qty3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? Sm.DrDec(drDtl, cDtl[4]) : 0),
                                PurchaseUomCode3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? Sm.DrStr(drDtl, cDtl[5]) : "")
                            });
                        }
                        else
                        {
                            int CurrentRow = FirstItemRow + TotalSectionRow;
                            ldtl[CurrentRow].Section1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? GetShelfName(Sm.DrStr(drDtl, cDtl[0]), Sm.DrStr(drDtl, cDtl[6]), Sm.DrStr(drDtl, cDtl[7]), Sm.DrStr(drDtl, cDtl[8]), Sm.DrStr(drDtl, cDtl[9]), Sm.DrStr(drDtl, cDtl[10]), Sm.DrStr(drDtl, cDtl[11]), Sm.DrStr(drDtl, cDtl[12]), Sm.DrStr(drDtl, cDtl[13]), Sm.DrStr(drDtl, cDtl[14]), Sm.DrStr(drDtl, cDtl[15])) : ldtl[CurrentRow].Section1);
                            //((Sm.DrStr(drDtl, cDtl[0]) == "1" || Sm.DrStr(drDtl, cDtl[0]) == "4" || Sm.DrStr(drDtl, cDtl[0]) == "7" || Sm.DrStr(drDtl, cDtl[0]) == "10") ? Sm.DrStr(drDtl, cDtl[6]) : ldtl[CurrentRow].Section1);
                            ldtl[CurrentRow].Diameter1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? Sm.DrDec(drDtl, cDtl[3]) : ldtl[CurrentRow].Diameter1);
                            ldtl[CurrentRow].Qty1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? Sm.DrDec(drDtl, cDtl[4]) : ldtl[CurrentRow].Qty1);
                            ldtl[CurrentRow].PurchaseUomCode1 = ((Math.Floor((Decimal)MyCol % 3) == 0) ? Sm.DrStr(drDtl, cDtl[5]) : ldtl[CurrentRow].PurchaseUomCode1);
                            ldtl[CurrentRow].Section2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? GetShelfName(Sm.DrStr(drDtl, cDtl[0]), Sm.DrStr(drDtl, cDtl[6]), Sm.DrStr(drDtl, cDtl[7]), Sm.DrStr(drDtl, cDtl[8]), Sm.DrStr(drDtl, cDtl[9]), Sm.DrStr(drDtl, cDtl[10]), Sm.DrStr(drDtl, cDtl[11]), Sm.DrStr(drDtl, cDtl[12]), Sm.DrStr(drDtl, cDtl[13]), Sm.DrStr(drDtl, cDtl[14]), Sm.DrStr(drDtl, cDtl[15])) : ldtl[CurrentRow].Section2);
                            ldtl[CurrentRow].Diameter2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? Sm.DrDec(drDtl, cDtl[3]) : ldtl[CurrentRow].Diameter2);
                            ldtl[CurrentRow].Qty2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? Sm.DrDec(drDtl, cDtl[4]) : ldtl[CurrentRow].Qty2);
                            ldtl[CurrentRow].PurchaseUomCode2 = ((Math.Floor((Decimal)MyCol % 3) == 1) ? Sm.DrStr(drDtl, cDtl[5]) : ldtl[CurrentRow].PurchaseUomCode2);
                            ldtl[CurrentRow].Section3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? GetShelfName(Sm.DrStr(drDtl, cDtl[0]), Sm.DrStr(drDtl, cDtl[6]), Sm.DrStr(drDtl, cDtl[7]), Sm.DrStr(drDtl, cDtl[8]), Sm.DrStr(drDtl, cDtl[9]), Sm.DrStr(drDtl, cDtl[10]), Sm.DrStr(drDtl, cDtl[11]), Sm.DrStr(drDtl, cDtl[12]), Sm.DrStr(drDtl, cDtl[13]), Sm.DrStr(drDtl, cDtl[14]), Sm.DrStr(drDtl, cDtl[15])) : ldtl[CurrentRow].Section3);
                            ldtl[CurrentRow].Diameter3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? Sm.DrDec(drDtl, cDtl[3]) : ldtl[CurrentRow].Diameter3);
                            ldtl[CurrentRow].Qty3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? Sm.DrDec(drDtl, cDtl[4]) : ldtl[CurrentRow].Qty3);
                            ldtl[CurrentRow].PurchaseUomCode3 = ((Math.Floor((Decimal)MyCol % 3) == 2) ? Sm.DrStr(drDtl, cDtl[5]) : ldtl[CurrentRow].PurchaseUomCode3);
                        }
                        TotalSectionRow++;

                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Employee
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.EmpType, A.EmpCode, Concat(B.EmpName, '  ') As EmpName, ");
                SQLDtl2.AppendLine("Case When A.EmpType='1' then 'Grader' When A.EmpType='2' then 'Asisten Grader' ");
                SQLDtl2.AppendLine("When A.EmpType='3' then 'Bongkar Dalam' else 'Bongkar Luar' end As EmpTypeName ");
                SQLDtl2.AppendLine("From TblRecvRawMaterialDtl A ");
                SQLDtl2.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");
                SQLDtl2.AppendLine("Order By B.EmpName");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtRecvRawMaterialDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "EmpType" ,

                         //1-5
                         "EmpCode" ,
                         "EmpName" ,
                         "EmpTypeName" ,
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new RecvRawMatDtlEmployee()
                        {
                            EmpType = Sm.DrStr(drDtl2, cDtl2[0]),
                            EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpName = Sm.DrStr(drDtl2, cDtl2[2]),
                            EmpTypeName = Sm.DrStr(drDtl2, cDtl2[3]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            Sm.PrintReport("RecvRawMaterial", myLists, TableName, false);

        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RawMaterialVerify", "TblRawMaterialVerify");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRawMaterialVerify(DocNo));
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsTxtEmpty(TxtRecvRawMaterialDocNo, "Receiving Raw Material", false) ||
                IsRecvRawMaterialNotValid() ||
                IsRawMaterialOpnameNotValid();
        }

        private bool IsRecvRawMaterialNotValid()
        {
            var cm = new MySqlCommand()
            {
                CommandText = 
                    "Select DocNo From TblRecvRawMaterialHdr " +
                    "Where DocNo=@RecvRawMaterialDocNo And CancelInd= 'N' And IfNull(ProcessInd, 'O')='O';"
            };
            Sm.CmParam<String>(ref cm, "@RecvRawMaterialDocNo", TxtRecvRawMaterialDocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Dokumen penerimaan tidak valid.");
                return true;
            }
            return false;
        }

        private bool IsRawMaterialOpnameNotValid()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblRawMaterialOpnameHdr " +
                    "Where DocNo=@RawMaterialOpnameDocNo And CancelInd= 'N' And IfNull(ProcessInd, 'O')='O';"
            };
            Sm.CmParam<String>(ref cm, "@RawMaterialOpnameDocNo", TxtRawMaterialOpnameDocNo.Text);
            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Dokumen opname tidak valid.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRawMaterialVerify(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRawMaterialVerify(DocNo, DocDt, CancelInd, ProcessInd, RecvRawMaterialDocNo, RawMaterialOpnameDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @RecvRawMaterialDocNo, @RawMaterialOpnameDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblRecvRawMaterialHdr Set ");
            SQL.AppendLine("    ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@RecvRawMaterialDocNo And CancelInd='N' And IfNull(ProcessInd, 'O')='O'; ");

            SQL.AppendLine("Update TblRawMaterialOpnameHdr Set ");
            SQL.AppendLine("    ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CancelInd='N' And IfNull(ProcessInd, 'O')='O' And LegalDocVerifyDocNo=@LegalDocVerifyDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@RecvRawMaterialDocNo", TxtRecvRawMaterialDocNo.Text);
            Sm.CmParam<String>(ref cm, "@RawMaterialOpnameDocNo", TxtRawMaterialOpnameDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            UpdateRawMaterialVerify();
            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Dokumen verifikasi", false) ||
                IsCancelIndNotValid() ||
                IsCancelIndEditedAlready();
        }

        private bool IsCancelIndNotValid()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "Dokumen ini belum akan dibatalkan.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblRawMaterialVerify Where DocNo=@DocNo And CancelInd= 'Y';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Dokumen ini telah dibatalkan.");
                return true;
            }
            return false;
        }

        private void UpdateRawMaterialVerify()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRawMaterialVerify Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblRecvRawMaterialHdr Set ");
            SQL.AppendLine("    ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@RecvRawMaterialDocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblRawMaterialOpnameHdr Set ");
            SQL.AppendLine("    ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where LegalDocVerifyDocNo=@LegalDocVerifyDocNo And CancelInd='N'; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@RecvRawMaterialDocNo", TxtRecvRawMaterialDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LegalDocVerifyDocNo", TxtLegalDocVerifyDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.ExecCommand(cm);
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRawMaterialVerify(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRawMaterialVerify(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.RecvRawMaterialDocNo, A.RawMaterialOpnameDocNo, A.Remark, F.VdName, E.QueueNo, B.LegalDocVerifyDocNo ");
            SQL.AppendLine("From TblRawMaterialVerify A");
            SQL.AppendLine("Left Join TblRecvrawMaterialHdr B ON A.RecvRawMaterialDocNo = B.DocNo ");
            SQL.AppendLine("left Join TblLegalDocVerifyHdr E On B.LegalDocVerifyDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblVendor F On E.VdCode=F.VdCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "RecvRawMaterialDocNo", "Remark", "VdName", 
                        
                        //6-8
                        "QueueNo", "LegalDocVerifyDocNo", "RawMaterialOpnameDocNo"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    TxtRecvRawMaterialDocNo.EditValue = Sm.DrStr(dr, c[3]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    TxtVdCode.EditValue = Sm.DrStr(dr, c[5]);
                    TxtQueueNo.EditValue = Sm.DrStr(dr, c[6]);
                    TxtLegalDocVerifyDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtRawMaterialOpnameDocNo.EditValue = Sm.DrStr(dr, c[8]);
                }, true
        );

        }

        #endregion

        #region Additional Method

        private string GetShelfName(
            string ShelfNo, string Shelf1, string Shelf2, string Shelf3, string Shelf4,
            string Shelf5, string Shelf6, string Shelf7, string Shelf8, string Shelf9, string Shelf10)
        {
            return 
                (ShelfNo == "1" ? Shelf1 : (ShelfNo == "2" ? Shelf2 : (ShelfNo == "3" ? Shelf3 :
                (ShelfNo == "4" ? Shelf4 : (ShelfNo == "5" ? Shelf5 : (ShelfNo == "6" ? Shelf6 :
                (ShelfNo == "7" ? Shelf7 : (ShelfNo == "8" ? Shelf8 : (ShelfNo == "9" ? Shelf9 : Shelf10)))))))));
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnRecvRawMaterialDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRawMaterialVerifyDlg(this));
        }

        private void BtnRecvRawMaterialDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRecvRawMaterialDocNo, "Dokumen penerimaan bahan baku", false))
            {
                try
                {
                    var f = new FrmRecvRawMaterial(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtRecvRawMaterialDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnRawMaterialOpnameDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRawMaterialOpnameDocNo, "Dokumen opname", false))
            {
                try
                {
                    var f = new FrmRawMaterialOpname(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtRawMaterialOpnameDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnLegalDocVerify_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtLegalDocVerifyDocNo, "Dokumen Legalitas", false))
            {
                try
                {
                    var f = new FrmLegalDocVerify(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtLegalDocVerifyDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

        #endregion
    }

    #region Report Class

    class RecvRawMat
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string QueueNo { get; set; }
        public string VehicleReqNo { get; set; }
        public string LegalDocVerifyDocNo { get; set; }
        public string VdName { get; set; }
        public string TTName { get; set; }
        public string UnloadStartDtTm { get; set; }
        public string UnloadEndDtTm { get; set; }
        public string Remark { get; set; }
        public string Shelf1 { get; set; }
        public string Shelf2 { get; set; }
        public string Shelf3 { get; set; }
        public string Shelf4 { get; set; }
        public string Shelf5 { get; set; }
        public string Shelf6 { get; set; }
        public string Shelf7 { get; set; }
        public string Shelf8 { get; set; }
        public string Shelf9 { get; set; }
        public string Shelf10 { get; set; }
        public string CreateBy { get; set; }
        public string PrintBy { get; set; }
    }

    class RecvRawMatDtl
    {
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public decimal OrderNumber { get; set; }
        public string SectionTitle { get; set; }
        public string Section1 { get; set; }
        public decimal Diameter1 { get; set; }
        public decimal Qty1 { get; set; }
        public string PurchaseUomCode1 { get; set; }
        public string Section2 { get; set; }
        public decimal Diameter2 { get; set; }
        public decimal Qty2 { get; set; }
        public string PurchaseUomCode2 { get; set; }
        public string Section3 { get; set; }
        public decimal Diameter3 { get; set; }
        public decimal Qty3 { get; set; }
        public string PurchaseUomCode3 { get; set; }
    }

    class RecvRawMatDtlEmployee
    {
        public string EmpType { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string EmpTypeName { get; set; }
    }

    #endregion
}
