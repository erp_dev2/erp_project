﻿namespace RunSystem
{
    partial class FrmPrintCashBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.LueKasir = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueMgrKeu = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueDirKeu = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueKasir.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMgrKeu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDirKeu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(784, 0);
            this.panel1.Size = new System.Drawing.Size(70, 122);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.DteDocDt2);
            this.panel2.Controls.Add(this.LueDirKeu);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueMgrKeu);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueKasir);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.DteDocDt1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Size = new System.Drawing.Size(784, 122);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(112, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 14);
            this.label2.TabIndex = 3;
            this.label2.Text = "Period";
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(164, 14);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(129, 20);
            this.DteDocDt1.TabIndex = 4;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // LueKasir
            // 
            this.LueKasir.EnterMoveNextControl = true;
            this.LueKasir.Location = new System.Drawing.Point(164, 35);
            this.LueKasir.Margin = new System.Windows.Forms.Padding(5);
            this.LueKasir.Name = "LueKasir";
            this.LueKasir.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueKasir.Properties.Appearance.Options.UseFont = true;
            this.LueKasir.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueKasir.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueKasir.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueKasir.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueKasir.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueKasir.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueKasir.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueKasir.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueKasir.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueKasir.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueKasir.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueKasir.Properties.DropDownRows = 30;
            this.LueKasir.Properties.NullText = "[Empty]";
            this.LueKasir.Properties.PopupWidth = 500;
            this.LueKasir.Size = new System.Drawing.Size(276, 20);
            this.LueKasir.TabIndex = 8;
            this.LueKasir.ToolTip = "F4 : Show/hide list";
            this.LueKasir.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueKasir.EditValueChanged += new System.EventHandler(this.LueKasir_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(122, 38);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 14);
            this.label5.TabIndex = 7;
            this.label5.Text = "Kasir";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueMgrKeu
            // 
            this.LueMgrKeu.EnterMoveNextControl = true;
            this.LueMgrKeu.Location = new System.Drawing.Point(164, 57);
            this.LueMgrKeu.Margin = new System.Windows.Forms.Padding(5);
            this.LueMgrKeu.Name = "LueMgrKeu";
            this.LueMgrKeu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMgrKeu.Properties.Appearance.Options.UseFont = true;
            this.LueMgrKeu.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueMgrKeu.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMgrKeu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueMgrKeu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMgrKeu.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMgrKeu.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMgrKeu.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMgrKeu.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMgrKeu.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueMgrKeu.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueMgrKeu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMgrKeu.Properties.DropDownRows = 30;
            this.LueMgrKeu.Properties.NullText = "[Empty]";
            this.LueMgrKeu.Properties.PopupWidth = 500;
            this.LueMgrKeu.Size = new System.Drawing.Size(276, 20);
            this.LueMgrKeu.TabIndex = 10;
            this.LueMgrKeu.ToolTip = "F4 : Show/hide list";
            this.LueMgrKeu.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMgrKeu.EditValueChanged += new System.EventHandler(this.LueMgrKeu_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(42, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Manager Keuangan";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDirKeu
            // 
            this.LueDirKeu.EnterMoveNextControl = true;
            this.LueDirKeu.Location = new System.Drawing.Point(164, 79);
            this.LueDirKeu.Margin = new System.Windows.Forms.Padding(5);
            this.LueDirKeu.Name = "LueDirKeu";
            this.LueDirKeu.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirKeu.Properties.Appearance.Options.UseFont = true;
            this.LueDirKeu.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueDirKeu.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDirKeu.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueDirKeu.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDirKeu.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirKeu.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDirKeu.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDirKeu.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDirKeu.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.LueDirKeu.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueDirKeu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDirKeu.Properties.DropDownRows = 30;
            this.LueDirKeu.Properties.NullText = "[Empty]";
            this.LueDirKeu.Properties.PopupWidth = 500;
            this.LueDirKeu.Size = new System.Drawing.Size(276, 20);
            this.LueDirKeu.TabIndex = 12;
            this.LueDirKeu.ToolTip = "F4 : Show/hide list";
            this.LueDirKeu.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDirKeu.EditValueChanged += new System.EventHandler(this.LueDirKeu_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(45, 82);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 14);
            this.label4.TabIndex = 11;
            this.label4.Text = "Direktur Keuangan";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(311, 14);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(129, 20);
            this.DteDocDt2.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(296, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 14);
            this.label3.TabIndex = 14;
            this.label3.Text = "-";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmPrintCashBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 122);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmPrintCashBook";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueKasir.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMgrKeu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDirKeu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private DevExpress.XtraEditors.LookUpEdit LueKasir;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueDirKeu;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueMgrKeu;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        private System.Windows.Forms.Label label3;

    }
}