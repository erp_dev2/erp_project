﻿#region Update
/* 
   30/11/2020 [DITA/PHT] New Reporting
   22/12/2020 [DITA/PHT] Reporting dibuat yearly
   */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;


#endregion

namespace RunSystem
{
    public partial class FrmRptCompanyBudgetPlanProfitLoss : RunSystem.FrmBase6
    {
        #region Field

        private string
           mMenuCode = string.Empty,
           mAccessInd = string.Empty,
           mSQL = string.Empty;
        private bool
            mIsAccountingRptUseJournalPeriod = false;

        #endregion

        #region Constructor

        public FrmRptCompanyBudgetPlanProfitLoss(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueProfitCenterCode(ref LueProfitCenter);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
                SetLueCCCode(ref LueCCCode, string.Empty);
                SetGrd();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Account#",
                    "Account Name", 
                    "Total RKAP",
                    "Total Journal",
                    "RKAP (Periode)",

                    //6
                    "Journal (Periode)"
                    
                },
                new int[] 
                {
                    //0
                    25,

                    //1-5
                    250, 250, 150, 150, 150,

                    //6
                    150

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6 }, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year") 
                ) return;
            var Yr = Sm.GetLue(LueYr);
            var Dt1 = Sm.Left(Sm.GetDte(DteDocDt1), 8);
            var Dt2 = Sm.Left(Sm.GetDte(DteDocDt2), 8);
            string SelectedCOA = string.Empty;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<CBP>();
                var l2 = new List<COACBP>();
                var l3 = new List<COAJournal>();
                var l4 = new List<JournalTemp>();
                var l5 = new List<COAJournalPeriod>();
                var l6 = new List<JournalTempPeriod>();

                PrepDataCBP(ref l, Yr);
                if (l.Count > 0)
                {
                    SelectedCOA = GetSelectedCOA(ref l);
                    PrepCOACBP(ref l2, SelectedCOA);

                    //proses total nilai RKAP
                    if (l2.Count > 0)
                    {
                        Process1(ref l, ref l2, Yr, false, string.Empty, string.Empty);
                        if (Dt1.Length > 0 && Dt2.Length > 0)
                        {
                            Process1(ref l, ref l2, string.Empty, true, Dt1, Dt2);
                        }
                    }
                    PrepCOAJournal(ref l3, SelectedCOA, Yr);
                    PrepJournalTemp(ref l4, SelectedCOA, Yr);
                    if (Dt1.Length > 0 && Dt2.Length > 0)
                    {
                        PrepCOAJournalPeriod(ref l5, SelectedCOA, Dt1, Dt2);
                        PrepJournalTempPeriod(ref l6, SelectedCOA, Dt1, Dt2);
                        Process2(ref l3, ref l4, ref l5, ref l6, true);
                        Process3(ref l2, ref l3, ref l5, true);
                    }

                    Process2(ref l3, ref l4, ref l5, ref l6, false);
                    Process3(ref l2, ref l3, ref l5, false);
                    Process4(ref l2);
                    Process5(ref l2);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6 });

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        public static void SetLueCCCode(ref LookUpEdit Lue, string ProfitCenterCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode As Col1, CCName As Col2 From TblCostCenter Where CBPInd = 'Y' And ProfitCenterCode = @ProfitCenterCode Order By CCName ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        private void GetParameter()
        {
            mIsAccountingRptUseJournalPeriod = Sm.GetParameterBoo("IsAccountingRptUseJournalPeriod");

        }

        private void PrepDataCBP(ref List<CBP> l, string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.Yr, B.AcNo, SUM(B.Amt)Amt ");
            SQL.AppendLine("FROM TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("INNER JOIN TblCompanyBudgetPlanDtl B ON A.DocNo=B.DocNo AND A.CancelInd != 'Y'  ");
            if (Sm.GetLue(LueProfitCenter).Length != 0)
                SQL.AppendLine("INNER JOIN TblCostCenter C On A.CCCode = C.CCCode And C.ProfitCenterCode=@ProfitCenterCode ");
            if (Sm.GetLue(LueCCCode).Length != 0)
                SQL.AppendLine("And A.CCCode=@CCCode ");
            SQL.AppendLine("WHERE A.Yr = @Yr ");
            SQL.AppendLine("And A.DocType = '2' ");
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenter));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "Yr", 

                    //1-2
                    "AcNo", 
                    "Amt" ,
                   
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new CBP()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepCOACBP(ref List<COACBP> l2, string SelectedCOA)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcDesc, LEVEL, Parent ");
            SQL.AppendLine("FROM TblCOA ");
            SQL.AppendLine("WHERE Find_In_Set(AcNo, @SelectedCOA); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcDesc", "Level", "Parent" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COACBP()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcDesc = Sm.DrStr(dr, c[1]),
                            Level = Sm.DrDec(dr, c[2]),
                            Parent = Sm.DrStr(dr, c[3]),
                            AmtCBP = 0m,
                            AmtJN = 0m,
                            AmtCBPPeriode = 0m,
                            AmtJNPeriode = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedCOA(ref List<CBP> l)
        {
            string AcNo = string.Empty;

            foreach (var x in l)
            {
                if (AcNo.Length > 0) AcNo += ",";
                AcNo += x.AcNo;
            }

            return AcNo;
        }

        //Total RKAP
        private void Process1(ref List<CBP> l, ref List<COACBP> l2, string Yr, bool IsPeriod, string Dt1, string Dt2)
        {
            if (IsPeriod)
            {
                for (var i = Int32.Parse(Sm.Left(Dt1, 4)); i <= Int32.Parse(Sm.Left(Dt2, 4)) ; i++)
                {
                    
                    foreach (var x in l.Where(w => Int32.Parse(w.Yr) == i))
                    {
                        foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                        {
                            if(i != Int32.Parse(Sm.Left(Dt1, 4)) && i != Int32.Parse(Sm.Left(Dt2, 4)) )
                                y.AmtCBPPeriode += x.Amt;
                            else
                            {
                                if (i == Int32.Parse(Sm.Left(Dt1, 4)))
                                {
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 1)
                                        y.AmtCBPPeriode += x.Amt01;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 2)
                                        y.AmtCBPPeriode += x.Amt02;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 3)
                                        y.AmtCBPPeriode += x.Amt03;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 4)
                                        y.AmtCBPPeriode += x.Amt04;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 5)
                                        y.AmtCBPPeriode += x.Amt05;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 6)
                                        y.AmtCBPPeriode += x.Amt06;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 7)
                                        y.AmtCBPPeriode += x.Amt07;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 8)
                                        y.AmtCBPPeriode += x.Amt08;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 9)
                                        y.AmtCBPPeriode += x.Amt09;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 10)
                                        y.AmtCBPPeriode += x.Amt10;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 11)
                                        y.AmtCBPPeriode += x.Amt11;
                                    if (Int32.Parse(Dt1.Substring(4, 2)) <= 12)
                                        y.AmtCBPPeriode += x.Amt12;
                                }
                                else
                                {
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 1)
                                        y.AmtCBPPeriode += x.Amt01;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 2)
                                        y.AmtCBPPeriode += x.Amt02;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 3)
                                        y.AmtCBPPeriode += x.Amt03;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 4)
                                        y.AmtCBPPeriode += x.Amt04;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 5)
                                        y.AmtCBPPeriode += x.Amt05;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 6)
                                        y.AmtCBPPeriode += x.Amt06;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 7)
                                        y.AmtCBPPeriode += x.Amt07;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 8)
                                        y.AmtCBPPeriode += x.Amt08;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 9)
                                        y.AmtCBPPeriode += x.Amt09;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 10)
                                        y.AmtCBPPeriode += x.Amt10;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 11)
                                        y.AmtCBPPeriode += x.Amt11;
                                    if (Int32.Parse(Dt2.Substring(4, 2)) >= 12)
                                        y.AmtCBPPeriode += x.Amt12;
                                }
                                
                                  
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var x in l)
                {
                    foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                    {
                        y.AmtCBP += x.Amt;
                    }
                }  
                
            }
        }

        private void PrepCOAJournal(ref List<COAJournal> l3, string SelectedCOA, string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcType ");
            SQL.AppendLine("FROM TblCOA  ");
            SQL.AppendLine("WHERE AcNo IN( ");
            SQL.AppendLine("	SELECT DISTINCT B.AcNo ");
            SQL.AppendLine("	FROM TblJournalHdr A ");
            SQL.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("	WHERE FIND_IN_SET(B.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '01') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '12') ");
            }
            else
            {
              SQL.AppendLine("	AND LEFT(A.DocDt, 4) = @Yr ");
            } 
            SQL.AppendLine(") ");

            SQL.AppendLine("AND ActInd = 'Y' ");
            SQL.AppendLine("AND Parent IS NOT NULL  ");
            SQL.AppendLine("AND AcNo NOT IN ( ");
            SQL.AppendLine("SELECT Parent FROM TblCOA WHERE Parent IS NOT null ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new COAJournal()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            DAmt = 0m,
                            CAmt = 0m,

                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepCOAJournalPeriod(ref List<COAJournalPeriod> l5, string SelectedCOA, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT AcNo, AcType ");
            SQL.AppendLine("FROM TblCOA  ");
            SQL.AppendLine("WHERE AcNo IN( ");
            SQL.AppendLine("	SELECT DISTINCT B.AcNo ");
            SQL.AppendLine("	FROM TblJournalHdr A ");
            SQL.AppendLine("	INNER JOIN TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("	WHERE FIND_IN_SET(B.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=Left(@Dt1, 6) ");
                SQL.AppendLine("And Left(B.Period, 6)<=Left(@Dt2, 6) ");
            }
            else
            {
                SQL.AppendLine("	AND A.DocDt BETWEEN @Dt1 AND @Dt2 ");           
            }
            SQL.AppendLine(") ");

            SQL.AppendLine("AND ActInd = 'Y' ");
            SQL.AppendLine("AND Parent IS NOT NULL  ");
            SQL.AppendLine("AND AcNo NOT IN ( ");
            SQL.AppendLine("SELECT Parent FROM TblCOA WHERE Parent IS NOT null ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
                Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "AcType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l5.Add(new COAJournalPeriod()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            DAmt = 0m,
                            CAmt = 0m,

                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepJournalTemp(ref List<JournalTemp> l4, string SelectedCOA, string Yr)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.AcNo, SUM(A.DAmt) DAmt, SUM(A.CAmt) CAmt ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=CONCAT(@Yr, '01') ");
                SQL.AppendLine("And Left(B.Period, 6)<=CONCAT(@Yr, '12') ");
            }
            else
            {
              SQL.AppendLine("AND LEFT(B.DocDt, 4) = @Yr ");
            } 
            SQL.AppendLine("GROUP BY A.AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new JournalTemp()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DAmt = Sm.DrDec(dr, c[1]),
                            CAmt = Sm.DrDec(dr, c[2]),

                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepJournalTempPeriod(ref List<JournalTempPeriod> l6, string SelectedCOA, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.AcNo, SUM(A.DAmt) DAmt, SUM(A.CAmt) CAmt ");
            SQL.AppendLine("FROM TblJournalDtl A ");
            SQL.AppendLine("INNER JOIN TblJournalHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE FIND_IN_SET(A.AcNo, @SelectedCOA) ");
            if (mIsAccountingRptUseJournalPeriod)
            {
                SQL.AppendLine("And B.Period Is Not Null ");
                SQL.AppendLine("And Left(B.Period, 6)>=Left(@Dt1, 6) ");
                SQL.AppendLine("And Left(B.Period, 6)<=Left(@Dt2, 6) ");
            }
            else
            {
                SQL.AppendLine("AND B.DocDt BETWEEN @Dt1 AND @Dt2 ");
            }
            SQL.AppendLine("GROUP BY A.AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
                Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
                Sm.CmParam<String>(ref cm, "@SelectedCOA", SelectedCOA);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l6.Add(new JournalTempPeriod()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            DAmt = Sm.DrDec(dr, c[1]),
                            CAmt = Sm.DrDec(dr, c[2]),

                        });
                    }
                }
                dr.Close();
            }
        }


        // ceplokin DAmt & CAmt ke list
        private void Process2(ref List<COAJournal> l3, ref List<JournalTemp> l4, ref List<COAJournalPeriod> l5, ref List<JournalTempPeriod> l6, bool IsPeriod)
        {
            if (IsPeriod)
            {
                foreach (var x in l5)
                {
                    foreach (var y in l6.Where(w => w.AcNo == x.AcNo))
                    {
                        x.DAmt = y.DAmt;
                        x.CAmt = y.CAmt;
                    }
                }
            }
            else
            {
                foreach (var x in l3)
                {
                    foreach (var y in l4.Where(w => w.AcNo == x.AcNo))
                    {
                        x.DAmt = y.DAmt;
                        x.CAmt = y.CAmt;
                    }
                }
            }
        }

        // Total Journal
        private void Process3(ref List<COACBP> l2, ref List<COAJournal> l3, ref List<COAJournalPeriod> l5, bool IsPeriod)
        {
            if (IsPeriod)
            {
                foreach (var x in l5)
                {
                    foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                    {
                        y.AmtJNPeriode += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }
                }
            }
            else
            {
                foreach (var x in l3)
                {
                    foreach (var y in l2.Where(w => w.AcNo == x.AcNo))
                    {
                        y.AmtJN += (x.AcType == "C") ? (x.CAmt - x.DAmt) : (x.DAmt - x.CAmt);
                    }
                }
            }
        }

        // proses masukin amount journal ke parent dari si amount bontotnya
        private void Process4(ref List<COACBP> l2)
        {
            int MaxLvlCOA = Int32.Parse(Sm.GetValue("SELECT MAX(LEVEL) FROM TblCOA"));

            for (int i = MaxLvlCOA; i > 0; i--)
            {
                foreach (var x in l2.Where(w => w.Level == i).OrderBy(o => o.Parent))
                {
                    foreach (var y in l2.Where(w => w.AcNo == x.Parent))
                    {
                        y.AmtJN += x.AmtJN;
                        y.AmtJNPeriode += x.AmtJNPeriode;
                    }
                }
            }
        }

        // ceplokin data ke grid
        private void Process5(ref List<COACBP> l2)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l2)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.AcDesc;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = Sm.FormatNum(x.AmtCBP, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.AmtJN, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.FormatNum(x.AmtCBPPeriode, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = Sm.FormatNum(x.AmtJNPeriode, 0);

                Row += 1;
            }

            Grd1.EndUpdate();
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueProfitCenter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenter, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
            SetLueCCCode(ref LueCCCode, Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue2(SetLueCCCode), Sm.GetLue(LueProfitCenter));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProfitCenter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Profit Center");
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost Center");
        }

        #endregion

        #endregion

        #region Class

        private class CBP
        {
            public string Yr { get; set; }
            public string AcNo { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt01 { get; set; }
            public decimal Amt02 { get; set; }
            public decimal Amt03 { get; set; }
            public decimal Amt04 { get; set; }
            public decimal Amt05 { get; set; }
            public decimal Amt06 { get; set; }
            public decimal Amt07 { get; set; }
            public decimal Amt08 { get; set; }
            public decimal Amt09 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }

        }

        private class COACBP
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string Parent { get; set; }
            public decimal Level { get; set; }
            public decimal AmtCBP { get; set; }
            public decimal AmtJN { get; set; }
            public decimal AmtCBPPeriode { get; set; }
            public decimal AmtJNPeriode { get; set; }
        }

        private class COAJournal
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class JournalTemp
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class COAJournalPeriod
        {
            public string AcNo { get; set; }
            public string AcType { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class JournalTempPeriod
        {
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion

        
    }
}
