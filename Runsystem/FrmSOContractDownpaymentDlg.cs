﻿#region Update
/*
    12/03/2020 [TKG/IMS] New Application
    14/04/2020 [TKG/IMS] tambah so contract's amount
    04/06/2020 [IBL/IMS] Tetap memunculkan SO Contract walaupun sudah dipakai di SOContract Termin/DP (I)
    21/05/2021 [BRI/IMS] tambah CurCode untuk master
    11/06/2021 [RDH/IMS] Mengotomatiskan tax di tab service/iventory sesuai dengan BOQ nya dari SOC namun masih bisa di edit (ARDP)
    11/06/2021 [RDH/IMS] Menambahkan "SO Contract amount after tax" diambil dari total amount after tax di soc (ditaruh di SOC Information) (ARDP for Project)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDownpaymentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSOContractDownpayment mFrmParent;
        private string DocNo = string.Empty;
        
        #endregion

        #region Constructor

        public FrmSOContractDownpaymentDlg(FrmSOContractDownpayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TaxCode, A.DocNo, A.DocDt, B.CtName, F.ProjectCode, F.ProjectName, A.PONo, C.PtName, A.Remark, A.Amt+AmtBOM As Amt, A.CurCode ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Left Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join TblPaymentTerm C On A.PtCode=C.PtCode ");
            SQL.AppendLine("Left Join TblBOQHdr D On A.BOQDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr E On D.LOPDocNo=E.DocNO ");
            SQL.AppendLine("Left Join TblProjectGroup F On E.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.DocDt Desc, A.DocNo; ");

            return SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Customer",
                    "Project's Code",

                    //6-10
                    "Project's Name",
                    "Customer's PO#",
                    "Amount",
                    "Term of Payment",
                    "Remark",

                    //11-12
                    "CurCode",
                    "TaxCode"
                },
                new int[]
                {
                    50,
                    //1-5
                    150, 20, 80, 200, 150,
                    //5-10
                    200, 150, 130, 200, 200, 
                    //11-12
                    0, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.SetGrdProperty(Grd1, false);
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter),
                    new string[]
                    {
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CtName", "ProjectCode", "ProjectName", "PONo", 
                        //6-10
                        "Amt", "PtName", "Remark", "CurCode", "TaxCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                var r = Grd1.CurRow.Index;
                DocNo = Sm.GetGrdStr(Grd1, r, 1);
                mFrmParent.TxtSOContractDocNo.EditValue = DocNo;
                mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, r, 4);
                mFrmParent.TxtProjectCode.EditValue = Sm.GetGrdStr(Grd1, r, 5);
                mFrmParent.TxtProjectName.EditValue = Sm.GetGrdStr(Grd1, r, 6);
                mFrmParent.TxtPONo.EditValue = Sm.GetGrdStr(Grd1, r, 7);
                mFrmParent.TxtSOContractAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, r, 8), 0);
                mFrmParent.TxtPtCode.EditValue = Sm.GetGrdStr(Grd1, r, 9);
                mFrmParent.MeeSOContractRemark.EditValue = Sm.GetGrdStr(Grd1, r, 10);
                mFrmParent.ShowSOContractInfo(DocNo);
                mFrmParent.LueCurCode.EditValue = Sm.GetGrdStr(Grd1, r, 11);
                if(isSOCservice()) // service
                {
                    mFrmParent.LueTaxCode2.EditValue = Sm.GetGrdStr(Grd1,r,12);
                }
                if (isSOCInventory()) // inventory
                {
                    mFrmParent.LueTaxCode.EditValue = Sm.GetGrdStr(Grd1,r,12);
                }
                mFrmParent.TxtSOContractAmtAfterTax.EditValue = getSoContractAfterTax();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Additional Method
            private string getSoContractAfterTax()
            {
                var r = Grd1.CurRow.Index;
                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT SUM(Total) ");
                SQL.AppendLine("FROM ( ");
                SQL.AppendLine("SELECT B.Amt AS Total ");
                SQL.AppendLine("FROM TblSOContractHdr A ");
                SQL.AppendLine("INNER JOIN TblSOContractDtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("WHERE A.DocNo=@DocNo");
                SQL.AppendLine(") T");

                var cm = new MySqlCommand()
                {
                    CommandText = SQL.ToString()
                };

                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 1));
                Decimal value = Sm.GetValueDec(cm);
                return Sm.FormatNum(value, 2);
            }

            private bool isSOCInventory()
            {
                var r = Grd1.CurRow.Index;
                var SQL = new StringBuilder();
                SQL.AppendLine("Select 1 From TblSOContractDtl5 where DocNo = @DocNo limit 1; ");

                var cm = new MySqlCommand()
                {
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 1));


                if (!Sm.IsDataExist(cm))
                    return false;
                else
                    return true;
            }

            private bool isSOCservice()
            {
                var r = Grd1.CurRow.Index;
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblSOContractDtl4 where DocNo = @DocNo limit 1; ");

                var cm = new MySqlCommand()
                {
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 1));

                if (!Sm.IsDataExist(cm))
                    return false;
                else
                    return true;
            }
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        #endregion

        #endregion

    }
}
