﻿#region Update 
  /*
   12/03/2019 [MEY] dialog dari fasilitas copy data LOP 
   */
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmLOPDlg2 : RunSystem.FrmBase2
    {
        #region Field

        private FrmLOP mFrmParent;
        private string mSQL = string.Empty, mMenuCode = string.Empty;

        #endregion

        #region Constructor

         public FrmLOPDlg2(FrmLOP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

         #region Method

         override protected void FrmLoad(object sender, EventArgs e)
         {
             try
             {
                 base.FrmLoad(sender, e);                 
                 Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                 SetGrd();
                 SetSQL();
                 Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
             }
             catch (Exception Exc)
             {
                 Sm.ShowErrorMsg(Exc);
             }
         }

         override protected void SetSQL()
         {
             var SQL = new StringBuilder();

             SQL.AppendLine("Select A.DocNo, A.DocDt, A.ProjectName ");
             SQL.AppendLine("From (");
             SQL.AppendLine("   Select T.* From TblLopHdr T ");
             SQL.AppendLine("   Where T.DocDt Between  @DocDt1 And @DocDt2 ");
             SQL.AppendLine("   And T.CancelInd = 'N' ");
             SQL.AppendLine("   And T.Status = 'A' ");
             if (mFrmParent.mIsFilterBySite)
             {
                 SQL.AppendLine("And (T.SiteCode Is Null Or ( ");
                 SQL.AppendLine("    T.SiteCode Is Not Null ");
                 SQL.AppendLine("    And Exists( ");
                 SQL.AppendLine("        Select 1 From TblGroupSite ");
                 SQL.AppendLine("        Where SiteCode=IfNull(T.SiteCode, '') ");
                 SQL.AppendLine("        And GrpCode In ( ");
                 SQL.AppendLine("            Select GrpCode From TblUser ");
                 SQL.AppendLine("            Where UserCode=@UserCode ");
                 SQL.AppendLine("            ) ");
                 SQL.AppendLine("        ) ");
                 SQL.AppendLine(")) ");
             }
             SQL.AppendLine(") A ");

             mSQL = SQL.ToString();
         }

         private void SetGrd()
         {
             Grd1.Cols.Count = 5;
             Grd1.FrozenArea.ColCount = 2;
             Sm.GrdHdrWithColWidth(
                 Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "Project Name",
                },
                 new int[] 
                {
                    //0
                    50,
                    //1-5
                    100, 20, 100,200, 
                }
             );
             Sm.GrdColButton(Grd1, new int[] { 2 });
             Sm.GrdFormatDate(Grd1, new int[] { 3 });
             Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4 }, true);
             Sm.SetGrdProperty(Grd1, false);
         }

         override protected void ShowData()
         {
             Sm.ClearGrd(Grd1, false);
             if (
                 Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                 Sm.IsDteEmpty(DteDocDt2, "End date") ||
                 Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                 ) return;
             try
             {
                 Cursor.Current = Cursors.WaitCursor;
                 string Filter = "Where 0=0";

                 var cm = new MySqlCommand();

                 Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                 Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                 Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                 Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "A.DocNo", "A.ProjectName" });
                 Sm.ShowDataInGrid(
                         ref Grd1, ref cm,
                         mSQL + Filter + " Order By A.DocDt Desc, A.DocNo;",
                         new string[]
                        {
                            //0
                            "DocNo",
                            //1-2
                            "DocDt","ProjectName",
                        },
                         (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                         {
                             Grd.Cells[Row, 0].Value = Row + 1;
                             Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                             Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                             Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                         }, true, false, false, false
                     );
             }
             catch (Exception Exc)
             {
                 Sm.ShowErrorMsg(Exc);
             }
             finally
             {
                 Sm.FocusGrd(Grd1, 0, 0);
                 Cursor.Current = Cursors.Default;
             }
         }

         override protected void ChooseData()
         {
             if (Sm.IsFindGridValid(Grd1, 1))
             {
               mFrmParent.ShowLOPDetail(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
             }
         }

         #region Grid Method

         override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
         {

         }

         private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
         {
             if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
             {
                 var f = new FrmLOP(mMenuCode);
                 f.Tag = mMenuCode;
                 f.WindowState = FormWindowState.Normal;
                 f.StartPosition = FormStartPosition.CenterScreen;
                 f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                 f.ShowDialog();
             }
         }

         private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
         {
             if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
             {
                 e.DoDefault = false;
                 var f = new FrmLOP(mMenuCode);
                 f.Tag = mMenuCode;
                 f.WindowState = FormWindowState.Normal;
                 f.StartPosition = FormStartPosition.CenterScreen;
                 f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                 f.ShowDialog();
             }
         }

      
         #endregion

         #endregion

         #region Event

         private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
         {
             Sm.FilterSetTextEdit(this, sender, "Project");
         }
         private void TxtProjectName_Validated(object sender, EventArgs e)
         {
             Sm.FilterTxtSetCheckEdit(this, sender);
         }
         private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
         {
             if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
         }
         #endregion

    }

}
