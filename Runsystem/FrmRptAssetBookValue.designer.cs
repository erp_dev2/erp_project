namespace RunSystem
{
    partial class FrmRptAssetBookValue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LueAssetCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkAssetCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkSiteCode = new DevExpress.XtraEditors.CheckEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAssetName = new DevExpress.XtraEditors.TextEdit();
            this.ChkAssetName = new DevExpress.XtraEditors.CheckEdit();
            this.ChkProfitCenterCode2 = new DevExpress.XtraEditors.CheckEdit();
            this.CcbProfitCenterCode2 = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueProfitCenterCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueProfitCenterCode);
            this.panel2.Controls.Add(this.ChkProfitCenterCode);
            this.panel2.Controls.Add(this.ChkProfitCenterCode2);
            this.panel2.Controls.Add(this.CcbProfitCenterCode2);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtAssetName);
            this.panel2.Controls.Add(this.ChkAssetName);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.ChkSiteCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteEndDt);
            this.panel2.Controls.Add(this.DteStartDt);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.LueAssetCtCode);
            this.panel2.Controls.Add(this.ChkAssetCtCode);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(772, 177);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 296);
            this.Grd1.TabIndex = 26;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 177);
            this.panel3.Size = new System.Drawing.Size(772, 296);
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(115, 3);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 25;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 9;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueYr.EditValueChanged += new System.EventHandler(this.LueYr_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(80, 5);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Year";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 14);
            this.label9.TabIndex = 17;
            this.label9.Text = "Asset\'s Category";
            // 
            // LueAssetCtCode
            // 
            this.LueAssetCtCode.EnterMoveNextControl = true;
            this.LueAssetCtCode.Location = new System.Drawing.Point(115, 87);
            this.LueAssetCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueAssetCtCode.Name = "LueAssetCtCode";
            this.LueAssetCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueAssetCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAssetCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAssetCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAssetCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAssetCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAssetCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAssetCtCode.Properties.DropDownRows = 30;
            this.LueAssetCtCode.Properties.NullText = "[Empty]";
            this.LueAssetCtCode.Properties.PopupWidth = 220;
            this.LueAssetCtCode.Size = new System.Drawing.Size(307, 20);
            this.LueAssetCtCode.TabIndex = 18;
            this.LueAssetCtCode.ToolTip = "F4 : Show/hide list";
            this.LueAssetCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAssetCtCode.EditValueChanged += new System.EventHandler(this.LueAssetCtCode_EditValueChanged);
            // 
            // ChkAssetCtCode
            // 
            this.ChkAssetCtCode.Location = new System.Drawing.Point(424, 85);
            this.ChkAssetCtCode.Name = "ChkAssetCtCode";
            this.ChkAssetCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAssetCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkAssetCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAssetCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAssetCtCode.Properties.Caption = " ";
            this.ChkAssetCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAssetCtCode.Size = new System.Drawing.Size(19, 22);
            this.ChkAssetCtCode.TabIndex = 19;
            this.ChkAssetCtCode.ToolTip = "Remove filter";
            this.ChkAssetCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAssetCtCode.ToolTipTitle = "Run System";
            this.ChkAssetCtCode.CheckedChanged += new System.EventHandler(this.ChkAssetCtCode_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(54, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "End Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(48, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Start Date";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(115, 45);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(120, 20);
            this.DteEndDt.TabIndex = 13;
            this.DteEndDt.EditValueChanged += new System.EventHandler(this.DteEndDt_EditValueChanged);
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(115, 24);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(120, 20);
            this.DteStartDt.TabIndex = 11;
            this.DteStartDt.EditValueChanged += new System.EventHandler(this.DteStartDt_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(84, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 14);
            this.label5.TabIndex = 26;
            this.label5.Text = "Site";
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(115, 150);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 220;
            this.LueSiteCode.Size = new System.Drawing.Size(307, 20);
            this.LueSiteCode.TabIndex = 27;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // ChkSiteCode
            // 
            this.ChkSiteCode.Location = new System.Drawing.Point(424, 148);
            this.ChkSiteCode.Name = "ChkSiteCode";
            this.ChkSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSiteCode.Properties.Appearance.Options.UseFont = true;
            this.ChkSiteCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSiteCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSiteCode.Properties.Caption = " ";
            this.ChkSiteCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSiteCode.Size = new System.Drawing.Size(19, 22);
            this.ChkSiteCode.TabIndex = 28;
            this.ChkSiteCode.ToolTip = "Remove filter";
            this.ChkSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSiteCode.ToolTipTitle = "Run System";
            this.ChkSiteCode.CheckedChanged += new System.EventHandler(this.ChkSiteCode_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(70, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 14);
            this.label6.TabIndex = 14;
            this.label6.Text = "Asset ";
            // 
            // TxtAssetName
            // 
            this.TxtAssetName.EnterMoveNextControl = true;
            this.TxtAssetName.Location = new System.Drawing.Point(115, 66);
            this.TxtAssetName.Name = "TxtAssetName";
            this.TxtAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAssetName.Properties.Appearance.Options.UseFont = true;
            this.TxtAssetName.Properties.MaxLength = 250;
            this.TxtAssetName.Size = new System.Drawing.Size(307, 20);
            this.TxtAssetName.TabIndex = 15;
            this.TxtAssetName.Validated += new System.EventHandler(this.TxtAssetName_Validated);
            // 
            // ChkAssetName
            // 
            this.ChkAssetName.Location = new System.Drawing.Point(423, 64);
            this.ChkAssetName.Name = "ChkAssetName";
            this.ChkAssetName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAssetName.Properties.Appearance.Options.UseFont = true;
            this.ChkAssetName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAssetName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkAssetName.Properties.Caption = " ";
            this.ChkAssetName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAssetName.Size = new System.Drawing.Size(20, 22);
            this.ChkAssetName.TabIndex = 16;
            this.ChkAssetName.ToolTip = "Remove filter";
            this.ChkAssetName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAssetName.ToolTipTitle = "Run System";
            this.ChkAssetName.CheckedChanged += new System.EventHandler(this.ChkAssetName_CheckedChanged);
            // 
            // ChkProfitCenterCode2
            // 
            this.ChkProfitCenterCode2.Location = new System.Drawing.Point(424, 128);
            this.ChkProfitCenterCode2.Name = "ChkProfitCenterCode2";
            this.ChkProfitCenterCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode2.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode2.Properties.Caption = " ";
            this.ChkProfitCenterCode2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode2.Size = new System.Drawing.Size(19, 22);
            this.ChkProfitCenterCode2.TabIndex = 25;
            this.ChkProfitCenterCode2.ToolTip = "Remove filter";
            this.ChkProfitCenterCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode2.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode2.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode2_CheckedChanged);
            // 
            // CcbProfitCenterCode2
            // 
            this.CcbProfitCenterCode2.Location = new System.Drawing.Point(115, 129);
            this.CcbProfitCenterCode2.Name = "CcbProfitCenterCode2";
            this.CcbProfitCenterCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode2.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode2.Size = new System.Drawing.Size(307, 20);
            this.CcbProfitCenterCode2.TabIndex = 24;
            this.CcbProfitCenterCode2.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode2_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(4, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Multi Profit Center";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 14);
            this.label3.TabIndex = 20;
            this.label3.Text = "Profit Center";
            // 
            // LueProfitCenterCode
            // 
            this.LueProfitCenterCode.EnterMoveNextControl = true;
            this.LueProfitCenterCode.Location = new System.Drawing.Point(115, 108);
            this.LueProfitCenterCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProfitCenterCode.Name = "LueProfitCenterCode";
            this.LueProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfitCenterCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProfitCenterCode.Properties.DropDownRows = 30;
            this.LueProfitCenterCode.Properties.NullText = "[Empty]";
            this.LueProfitCenterCode.Properties.PopupWidth = 220;
            this.LueProfitCenterCode.Size = new System.Drawing.Size(307, 20);
            this.LueProfitCenterCode.TabIndex = 21;
            this.LueProfitCenterCode.ToolTip = "F4 : Show/hide list";
            this.LueProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProfitCenterCode.EditValueChanged += new System.EventHandler(this.LueProfitCenterCode_EditValueChanged);
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(424, 106);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(19, 22);
            this.ChkProfitCenterCode.TabIndex = 22;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // FrmRptAssetBookValue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptAssetBookValue";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAssetCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAssetName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAssetName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.LookUpEdit LueAssetCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkAssetCtCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private DevExpress.XtraEditors.CheckEdit ChkSiteCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtAssetName;
        private DevExpress.XtraEditors.CheckEdit ChkAssetName;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode2;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueProfitCenterCode;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
    }
}