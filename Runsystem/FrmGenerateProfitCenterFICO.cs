﻿#region Update
/*
 *  07/09/2021 [ICA/AMKA] new apps 
 *  17/11/2021 [ICA/AMKA] Cost center parent dimunculkan semua (termasuk child) dan berdasarkan Group
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmGenerateProfitCenterFICO : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        private string
            mEntCode = string.Empty,
            mCityCode = string.Empty,
            mBudgetType = string.Empty,
            mCodeOfCostCategoryInGenerateProfitCenter = string.Empty,
            mCodeOfBudgetCategoryInGenerateProfitCenter = string.Empty;
        private bool mIsFilterByCC = false;
        internal FrmGenerateProfitCenterFICOFind FrmFind;

        #endregion

        #region Constructor

        public FrmGenerateProfitCenterFICO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            GetParameter();
            SetFormControl(mState.View);
            SetLueCCCodeParent(ref LueCCCodeParent, string.Empty);
            Sl.SetLueProfitCenterCode(ref LueProfitCenterCode);
            SetLueSiteCode(ref LueSiteCode, string.Empty);
            Sl.SetLueDeptCode(ref LueDeptCode);
            Sl.SetLueCCCode(ref LueCCCode);
            Sl.SetLueCCtCode(ref LueCCtCode);
            Sl.SetLueWhsCode(ref LueWhsCode);
            Sl.SetLueBCCode(ref LueBCCode, string.Empty);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtProfitCenterPackageName, LueCCCodeParent, LueProfitCenterCode, LueSiteCode, LueDeptCode, 
                        LueCCCode, LueCCtCode, LueWhsCode, LueBCCode, ChkActInd
                    }, true);
                    TxtProfitCenterPackageName.Focus();
                    BtnProfitCenterCode.Enabled = BtnSiteCode.Enabled = BtnDeptCode.Enabled = BtnCCCode.Enabled = BtnCCtCode.Enabled = BtnWhsCode.Enabled = BtnBCCode.Enabled = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtProfitCenterPackageName, LueCCCodeParent
                    }, false);
                    ChkActInd.Checked = true;
                    BtnProfitCenterCode.Enabled = BtnSiteCode.Enabled = BtnDeptCode.Enabled = BtnCCCode.Enabled = BtnCCtCode.Enabled = BtnWhsCode.Enabled = BtnBCCode.Enabled = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkActInd
                    }, false);

                    BtnProfitCenterCode.Enabled = BtnSiteCode.Enabled = BtnDeptCode.Enabled = BtnCCCode.Enabled = BtnCCtCode.Enabled = BtnWhsCode.Enabled = BtnBCCode.Enabled = true;
                    ChkActInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                DteDocDt, TxtDocNo, TxtProfitCenterPackageName, LueCCCodeParent, LueProfitCenterCode, LueSiteCode, LueDeptCode, 
                LueCCCode, LueCCtCode, LueWhsCode, LueBCCode
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGenerateProfitCenterFICOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            Sm.SetDteCurrentDate(DteDocDt);
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowProfitCenterPackage(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowProfitCenterPackage(string DocNo)
        {
            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.ActInd, A.ProfitCenterPackageName, A.CCCode, A.ProfitCenterCode, A.SiteCode, ");
                SQL.AppendLine("A.DeptCode, A.CCCode2, B.CCtCode, A.WhsCode, C.BCCode ");
                SQL.AppendLine("FROM Tblgenerateprofitcenterhdr A ");
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("	SELECT DocNo, CCtCode ");
                SQL.AppendLine("	FROM Tblgenerateprofitcenterdtl ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("	LIMIT 1");
                SQL.AppendLine(")B ON A.DocNo = B.DocNo "); 
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("	SELECT DocNo, BCCode ");
                SQL.AppendLine("	FROM Tblgenerateprofitcenterdtl2 ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("	LIMIT 1");
                SQL.AppendLine(")C ON A.DocNo = C.DocNo ");
                SQL.AppendLine("Where A.DocNo = @DocNo");

                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(), 
                        new string[] 
                        {
                           "DocNo", 
                           "DocDt", "ProfitCenterPackageName", "ActInd", "CCCode", "CCCode2", 
                           "ProfitCenterCode", "SiteCode", "DeptCode", "CCtCode", "WhsCode", 
                           "BCCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtProfitCenterPackageName.EditValue = Sm.DrStr(dr, c[2]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[3]) == "Y" ? true : false;
                            Sm.SetLue(LueCCCodeParent, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[5]));
                            Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[6]));
                            Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[7]));
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[8]));
                            Sm.SetLue(LueCCtCode, Sm.DrStr(dr, c[9]));
                            Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[10]));
                            Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[11]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtProfitCenterPackageName, "Profit Center Package Name", false) ||
                Sm.IsLueEmpty(LueCCCodeParent, "Cost Center Parent") ||
                IsEntityNotValid() ||
                IsCityNotValid() ||
                IsBudgetCategoryNotValid() ||
                IsCostCategoryNotValid();
        }

        private bool IsEntityNotValid()
        {
            if (mEntCode.Length == 0 || !Sm.IsDataExist("Select 1 From TblEntity Where EntCode=@Param", mEntCode))
            {
                Sm.StdMsg(mMsgType.Warning, "Entity is invalid.");
                return true;
            }
            return false;
        }

        private bool IsCityNotValid()
        {
            if (mCityCode.Length == 0 || !Sm.IsDataExist("Select 1 From TblCity Where CityCode = @Param", mCityCode))
            {
                Sm.StdMsg(mMsgType.Warning, "City is invalid");
                return true;
            }
            return false;
        }

        private bool IsBudgetCategoryNotValid()
        {
            if (mBudgetType.Length == 0 || !Sm.IsDataExist("Select 1 From TblOption Where OptCat = 'BudgetType' And OptCode = @Param", mBudgetType))
            {
                Sm.StdMsg(mMsgType.Warning, "Budget Type is invalid");
                return true;
            }

            if (mCodeOfBudgetCategoryInGenerateProfitCenter.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Budget Category is empty.");
                return true;
            }
            else
            {
                string[] mBudgetCategory = mCodeOfBudgetCategoryInGenerateProfitCenter.Split(',');
                bool invalid = false;
                for (int i = 0; i < mBudgetCategory.Count(); i++)
                    if (!Sm.IsDataExist("Select 1 From TblBudgetCategory Where BCCode = @Param", mBudgetCategory[i]))
                        invalid = true;
                    
                if (invalid == true)
                {
                    Sm.StdMsg(mMsgType.Warning, "Budget Category is invalid.");
                    return true;
                }
            }
            return false;
        }

        private bool IsCostCategoryNotValid()
        {
            if (mCodeOfCostCategoryInGenerateProfitCenter.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Cost Category is empty.");
                return true;
            }
            else
            {
                string[] mCostCategory = mCodeOfCostCategoryInGenerateProfitCenter.Split(',');
                bool invalid = false;
                for (int i = 0; i < mCostCategory.Count(); i++)
                {
                    string cek = mCostCategory[i];
                    if (!Sm.IsDataExist("Select 1 From TblCostCategory Where CCtCode = @Param", mCostCategory[i]))
                        invalid = true;
                }

                if (invalid == true)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost Category is invalid.");
                    return true;
                }
            }
            return false;
        }

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            string 
                DocNo = string.Empty, 
                mProfitCenterCode = string.Empty, 
                mSiteCode = string.Empty, 
                mDeptCode = string.Empty, 
                mCCCode = string.Empty, 
                mCCtCodeFirst = string.Empty, 
                mCCtCode =string.Empty,
                mWhsCode = string.Empty, 
                mBCCodeFirst = string.Empty, 
                mBCCode = string.Empty;

            //Generate DocNo n Code 
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GenerateProfitCenter", "TblGenerateProfitCenterHdr");
            mProfitCenterCode = GenerateCode("ProfitCenterCode", "TblProfitCenter");
            mSiteCode = GenerateCode("SiteCode", "TblSite");
            mDeptCode = GenerateCode("DeptCode", "TblDepartment");
            mCCCode = GenerateCode("CCCode", "TblCostCenter");
            mWhsCode = GenerateCode("WhsCode", "TblWarehouse"); 
            mCCtCodeFirst = GenerateCode("CCtCode", "TblCostCategory");
            mCCtCode = mCCtCodeFirst;
            mBCCodeFirst = GenerateCode("BCCode", "TblBudgetCategory");
            mBCCode = mBCCodeFirst;

            string[] mCostCategory = mCodeOfCostCategoryInGenerateProfitCenter.Split(',');
            string[] mBudgetCategory = mCodeOfBudgetCategoryInGenerateProfitCenter.Split(',');

            cml.Add(SaveProfitCenter(mProfitCenterCode));
            cml.Add(SaveSite(mSiteCode, mProfitCenterCode));
            cml.Add(SaveDepartment(mDeptCode, mSiteCode));
            cml.Add(SaveCostCenter(mCCCode, mProfitCenterCode, mDeptCode));

            for (int i = 0; i < mCostCategory.Count(); ++i)
            {
                if (i == 0)
                    cml.Add(SaveCostCategory(DocNo, mCostCategory[i], mCCtCodeFirst, mCCCode));
                else
                {
                    mCCtCode = GenerateBCCtCode(mCCtCode);
                    cml.Add(SaveCostCategory(DocNo, mCostCategory[i], mCCtCode, mCCCode));
                }
            }

            cml.Add(SaveWarehouse(mWhsCode, mCCCode, mCCtCodeFirst));

            for (int i = 0; i < mBudgetCategory.Count(); i++)
            {
                if (i == 0)
                    cml.Add(SaveBudgetCategory(DocNo, mBudgetCategory[i], mBCCodeFirst, mDeptCode, mCCCode));
                else
                {
                    mBCCode = GenerateBCCtCode(mBCCode);
                    cml.Add(SaveBudgetCategory(DocNo, mBudgetCategory[i], mBCCode, mDeptCode, mCCCode));
                }
            }

            cml.Add(SaveData(DocNo));
            Sm.ExecCommands(cml);

            //refresh lue
            Sl.SetLueProfitCenterCode(ref LueProfitCenterCode);
            SetLueSiteCode(ref LueSiteCode, string.Empty);
            Sl.SetLueDeptCode(ref LueDeptCode);
            Sl.SetLueCCCode(ref LueCCCode);
            Sl.SetLueCCtCode(ref LueCCtCode);
            Sl.SetLueWhsCode(ref LueWhsCode);
            Sl.SetLueBCCode(ref LueBCCode, string.Empty);

            ShowData(DocNo);
        }

        private MySqlCommand SaveData(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGenerateProfitCenterHdr(DocNo, DocDt, ProfitCenterPackageName, ActInd, CCCode, CCCode2, ProfitCenterCode, ");
            SQL.AppendLine("SiteCode, DeptCode, WhsCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ProfitCenterPackageName, 'Y', @CCCode, @CCCode2, @ProfitCenterCode, ");
            SQL.AppendLine("@SiteCode, @DeptCode, @WhsCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ProfitCenterPackageName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCodeParent));
            Sm.CmParam<String>(ref cm, "@CCCode2", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveProfitCenter(string ProfitCenterCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProfitCenter(ProfitCenterCode, ProfitCenterName, Parent, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ProfitCenterCode, @ProfitCenterName, @Parent, @EntCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.CmParam<String>(ref cm, "@ProfitCenterName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueCCCodeParent));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue(LueProfitCenterCode, ProfitCenterCode);
            return cm;
        }

        private MySqlCommand SaveSite(string SiteCode, string ProfitCenterCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblSite(SiteCode, SiteName, ActInd, HOInd, ProfitCenterCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SiteCode, @SiteName, 'Y', 'N', @ProfitCenterCode, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Delete From TblSiteDivision Where SiteCode=@SiteCode; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.CmParam<String>(ref cm, "@SiteName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue(LueSiteCode, SiteCode);
            return cm;
        }

        private MySqlCommand SaveDepartment(string DeptCode, string SiteCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblDepartment(DeptCode, DeptName, ActInd, ControlItemInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DeptCode, @DeptName, 'Y', 'N', @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Delete From TblDepartmentBudgetSite Where DeptCode=@DeptCode; ");
            SQL.AppendLine("Delete From TblDepartmentPosition Where DeptCode=@DeptCode; ");

            SQL.AppendLine("Insert Into TblDepartmentBudgetSite (DeptCode, SiteCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DeptCode, @SiteCode, @UserCode, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@DeptName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue(LueDeptCode, DeptCode);
            return cm;
        }

        private MySqlCommand SaveCostCenter(string CCCode, string ProfitCenterCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCostCenter(CCCode, CCName, ActInd, IsCCPayrollJournalEnabled, Parent, ProfitCenterCode, DeptCode, NotParentInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CCCode, @CCName, 'Y', 'N', @Parent, @ProfitCenterCode, @DeptCode, 'Y', @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueCCCodeParent));
            Sm.CmParam<String>(ref cm, "@CCName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", ProfitCenterCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue(LueCCCode, CCCode);
            return cm;
        }

        private MySqlCommand SaveCostCategory(string DocNo, string CCtCodeOld, string CCtCodeNew, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCostCategory(CCtCode, CCtName, CCCode, AcNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @CCtCodeNew, Concat(CCtName, '-', @CCtName) CCtName, @CCCode, AcNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblCostCategory ");
            SQL.AppendLine("Where CCtCode = @CCtCodeOld ;");

            SQL.AppendLine("Insert Into TblGenerateProfitCenterDtl(DocNo, CCtCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Value(@DocNo, @CCtCodeNew, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CCtCodeNew", CCtCodeNew);
            Sm.CmParam<String>(ref cm, "@CCtCodeOld", CCtCodeOld);
            Sm.CmParam<String>(ref cm, "@CCtName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue(LueCCtCode, CCtCodeNew);
            return cm;
        }

        private MySqlCommand SaveWarehouse(string WhsCode, string CCCode, string CCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWarehouse(WhsCode, WhsName, CityCode, CCCode, CCtCode, AcNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @WhsCode, @WhsName, @CityCode, @CCCode, @CCtCode, AcNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblCostCategory ");
            SQL.AppendLine("Where CCtCode = @CCtCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
            Sm.CmParam<String>(ref cm, "@WhsName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@CCtCode", CCtCode);
            Sm.CmParam<String>(ref cm, "@CityCode", mCityCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue(LueWhsCode, WhsCode);
            return cm;
        }

        private MySqlCommand SaveBudgetCategory(string DocNo, string BCCodeOld, string BCCodeNew, string DeptCode, string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBudgetCategory(BCCode, BCName, DeptCode, BudgetType, CCCode, AcNo, ActInd, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @BCCodeNew, Concat(BCName, '-', @BCName) BCName, @DeptCode, @BudgetType, @CCCode, AcNo, 'Y', @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblBudgetCategory ");
            SQL.AppendLine("Where BCCode = @BCCodeOld; ");

            SQL.AppendLine("Insert Into TblGenerateProfitCenterDtl2(DocNo, BCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Value(@DocNo, @BCCodeNew, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@BCCodeNew", BCCodeNew);
            Sm.CmParam<String>(ref cm, "@BCCodeOld", BCCodeOld);
            Sm.CmParam<String>(ref cm, "@BCName", TxtProfitCenterPackageName.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@BudgetType", mBudgetType);
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue(LueBCCode, BCCodeNew);
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditProfitCenterPackage());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataAlreadyCancelled();
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblGenerateProfitCenterHdr Where ActInd='N' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already non active.");
        }

        private MySqlCommand EditProfitCenterPackage()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblGenerateProfitCenterHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And ActInd='Y'; ");

            //site 
            SQL.AppendLine("Update TblSite Set ");
            SQL.AppendLine("    ActInd=@ActInd, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where SiteCode=@SiteCode ");
            SQL.AppendLine("And ActInd='Y'; ");

            //Department
            SQL.AppendLine("Update TblDepartment Set ");
            SQL.AppendLine("    ActInd=@ActInd, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DeptCode=@DeptCode ");
            SQL.AppendLine("And ActInd='Y'; ");

            //CostCenter
            SQL.AppendLine("Update TblCostCenter Set ");
            SQL.AppendLine("    ActInd=@ActInd, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where CCCode=@CCCode ");
            SQL.AppendLine("And ActInd='Y'; ");

            //BudgetCategory
            SQL.AppendLine("Update TblBudgetCategory A ");
            SQL.AppendLine("Inner Join TblGenerateProfitCenterDtl2 B On A.BCCode = B.BCCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.ActInd=@ActInd, ");
            SQL.AppendLine("    A.LastUpBy=@UserCode, ");
            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo=@DocNo ");
            SQL.AppendLine("And A.ActInd='Y'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mEntCode = Sm.GetParameter("EntCodeForGenerateProfitCenter");
            mCityCode = Sm.GetParameter("CityCodeForGenerateProfitCenter");
            mBudgetType = Sm.GetParameter("BudgetTypeForGenerateProfitCenter");
            mCodeOfCostCategoryInGenerateProfitCenter = Sm.GetParameter("CodeOfCostCategoryInGenerateProfitCenter");
            mCodeOfBudgetCategoryInGenerateProfitCenter = Sm.GetParameter("CodeOfBudgetCategoryInGenerateProfitCenter");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
        }

        private void SetLueCCCodeParent(ref LookUpEdit Lue, string CCCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.CCCode As Col1, A.CCName As Col2 ");
            SQL.AppendLine("From TblCostCenter A ");
            //SQL.AppendLine("Where Parent Is Null ");
            SQL.AppendLine("Where 0=0 ");
            if (CCCode.Length > 0)
                SQL.AppendLine("And A.CCCode = @CCCode ");
            else
            {
                if (mIsFilterByCC)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                    SQL.AppendLine("    Where CCCode=A.CCCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            SQL.AppendLine("Order By A.CCName; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSiteCode(ref LookUpEdit Lue, string SiteCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode As Col1, T.SiteName As Col2 From TblSite T ");
            if (SiteCode.Length > 0)
                SQL.AppendLine("Where T.SiteCode=@Code;");
            else
                SQL.AppendLine("Where T.ActInd='Y' ");
            SQL.AppendLine("Order By T.SiteName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (SiteCode.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", SiteCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GenerateCode(string Column, string Table)
        {
            var SQL = new StringBuilder();
            int lenCode = 5;

            var MaxCode = Sm.GetValue("SELECT MAX(LENGTH(" + Column + ")) FROM " + Table + " ");
            if (MaxCode.Length > 0)
                lenCode = Convert.ToInt32(MaxCode);

            SQL.AppendLine("Select Right(Concat('0000', IfNull(( ");
            SQL.AppendLine("    Select Cast(IfNull(Max(" + Column + "), '00000') As UNSIGNED)+1 As Value ");
            SQL.AppendLine("    From " + Table + " ");
            SQL.AppendLine("    WHERE LENGTH(" + Column + ") = " + lenCode + " ");
            SQL.AppendLine("    Order By " + Column + " Desc Limit 1 ");
            SQL.AppendLine("), 1)), " + lenCode + ") As " + Column + " ");

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateBCCtCode(string Code)
        {
            return Sm.GetValue("Select Right(Concat('0000', IFNULL((Select Cast(IFNULL('" + Code + "', '00000') As UNSIGNED)+1 As VALUE), 1)), " + Code.Length + ")");
        }

        #endregion

        #endregion

        #region Event

        private void BtnProfitCenterCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueProfitCenterCode, ""))
            {
                try
                {
                    var f = new FrmProfitCenter(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mProfitCenterCode = Sm.GetLue(LueProfitCenterCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnSiteCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueSiteCode, ""))
            {
                try
                {
                    var f = new FrmSite(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mSiteCode = Sm.GetLue(LueSiteCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnDeptCodeCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueDeptCode, ""))
            {
                try
                {
                    var f = new FrmDepartment(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDeptCode = Sm.GetLue(LueDeptCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnCCtCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCCtCode, ""))
            {
                try
                {
                    Sm.FormShowDialog(new FrmGenerateProfitCenterFICODlg(this, TxtDocNo.Text));
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnCCCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCCCode, ""))
            {
                try
                {
                    var f = new FrmCostCenter(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCCCode = Sm.GetLue(LueCCCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnWhsCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueWhsCode, ""))
            {
                try
                {
                    var f = new FrmWarehouse(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mWhsCode = Sm.GetLue(LueWhsCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnBCCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueBCCode, ""))
            {
                try
                {
                    Sm.FormShowDialog(new FrmGenerateProfitCenterFICODlg2(this, TxtDocNo.Text));
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion
    }
}
