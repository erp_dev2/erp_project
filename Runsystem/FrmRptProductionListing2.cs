﻿#region Update
/*
    13/03/2018 [HAR] tambah info batch number, SFC remark, button SFC, Filter SFC, hanya kategori finishgood
    15/03/2018 [HAR] bug fixing item planning tidak muncul
    19/03/2018 [TKG] tambah total summary
    24/09/2018 [TKG] Kategori item yg ditampilkan bisa diisi lebih dari 1 item category.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProductionListing2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProductionListing2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
           var SQL = new StringBuilder();

           SQL.AppendLine(" 	Select A.DocNo, C.DocDt, ifnull(B.ItCode, A.ItCode) As ItCode, B.BatchNo,  B.SFCDocNo, if(length(B.SFCDocNo), if(B.Code ='1', 'Product', 'Co-Product'), null) As CodeItem, ");
           SQL.AppendLine("     ifnull(E.ItNAme, D.ItName) As ItName, ");
	       SQL.AppendLine("     A.PlannedQty As PlannedQty, IfNull(B.SFCQty, 0) As SFCQty, D.PlanningUomCode,  ");
	       SQL.AppendLine("     A.PlannedQty *D.PlanningUomCodeConvert12 As PlannedQty2,  ");
	       SQL.AppendLine("     IfNull(B.SFCQty, 0)*D.PlanningUomCodeConvert12 As SFCQty2,  ");
	       SQL.AppendLine("     D.PlanningUomCode2, C.Remark, B.Remark As SFCRemark  ");
	       SQL.AppendLine("     From ( ");
	       SQL.AppendLine("         Select T1.DocNo, T3.ItCode, Sum(T3.Qty) As PlannedQty  ");
	       SQL.AppendLine("         From TblPPHdr T1  ");
	       SQL.AppendLine("         Inner Join TblPPDtl T2 On T1.DocNo=T2.DocNo  ");
	       SQL.AppendLine("         Inner Join TblProductionOrderHdr T3 On T2.ProductionOrderDocNo=T3.DocNo  ");
	       SQL.AppendLine("         Where T1.CancelInd='N'  And T1.DocDt Between @DocDt1 And @DocDt2  ");
	       SQL.AppendLine("         Group By T1.DocNo, T3.ItCode  ");
	       SQL.AppendLine("     ) A  ");
	       SQL.AppendLine("     Left Join ( ");
           SQL.AppendLine("         Select Distinct '1' as Code,  T1.PPDocNo, T1.DocNo As SFCDocNo, T2.ItCode, T2.BatchNo, T2.Qty As SFCQty, T1.Remark  ");
		   SQL.AppendLine("         From TblShopFloorControlHdr T1  ");
		   SQL.AppendLine("         Inner Join TblShopFloorControlDtl T2 On T1.DocNo=T2.DocNo  ");
		   SQL.AppendLine("         Inner Join TblPPHdr T3 On T1.PPDocNo=T3.DocNo  And T3.DocDt Between @DocDt1 And @DocDt2 ");
		   SQL.AppendLine("         Inner Join TblPPDtl T4 On T3.DocNo = T4.DocNO  ");
		   SQL.AppendLine("         Inner Join TblProductionOrderHdr T5 On T4.ProductionOrderDocNo=T5.DocNo  And T2.ItCode = T5.ItCode ");
           SQL.AppendLine("         Inner Join Tblitem T6 On T2.ItCode = T6.ItCode ");
           SQL.AppendLine("         Where T1.CancelInd='N' "); 
		   SQL.AppendLine("         Union All ");
           SQL.AppendLine("         Select '2', T1.PPDocNo, T1.DocNo As SFCDocNo, T2.ItCode, T2.BatchNo, T2.Qty As SFCQty, T1.Remark  ");
		   SQL.AppendLine("         From TblShopFloorControlHdr T1  ");
		   SQL.AppendLine("         Inner Join TblShopFloorControlDtl T2 On T1.DocNo=T2.DocNo  ");
           SQL.AppendLine("             And T2.ItCode Not In ( ");
           SQL.AppendLine("	                Select A.ItCode ");
           SQL.AppendLine("	                From TblProductionOrderHdr A, TblItem B ");
           SQL.AppendLine("	                Where A.ItCode=B.ItCode ");
           SQL.AppendLine("                 And Find_In_Set(");
           SQL.AppendLine("                 IfNull(B.ItCtCode, ''), ");
           SQL.AppendLine("                 IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ItCtCodeForFinishedGood'), '') ");
           SQL.AppendLine("                 ) ");
           SQL.AppendLine("         ) ");
		   SQL.AppendLine("         Inner Join TblPPHdr T3 On T1.PPDocNo=T3.DocNo  And T3.DocDt Between @DocDt1 And @DocDt2  ");
           SQL.AppendLine("         Inner Join Tblitem T4 On T2.ItCode = T4.ItCode ");
           SQL.AppendLine("             And Find_In_Set(");
           SQL.AppendLine("             IfNull(T4.ItCtCode, ''), ");
           SQL.AppendLine("             IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='ItCtCodeForFinishedGood'), '') ");
           SQL.AppendLine("             ) ");
           SQL.AppendLine("         Where T1.CancelInd='N' "); 
           SQL.AppendLine("    ) B On A.DocNo=B.PPDocNo  ");
           SQL.AppendLine("    Inner Join TblPPHdr C On A.DocNo=C.DocNo  ");
           SQL.AppendLine("    Inner Join TblItem D On A.ItCode=D.ItCode  ");
           SQL.AppendLine("    Left Join TblItem E On B.ItCode=E.ItCode  ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Planned#",
                        "",
                        "Date",
                        "SFC#",
                        "",
                        
                        //6-10
                        "Type",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Batch",

                        //11-15
                        "Planned",
                        "SFC",
                        "UoM",
                        "%",
                        "Planned (2)",

                        //16-20
                        "SFC (2)",
                        "UoM",
                        "%",
                        "Plan's Remark",
                        "SFC's Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
                        //1-5
                        130, 20, 80, 150, 20,  
                        //6-10
                        120, 80, 20, 250, 150, 
                        //11-15
                        80, 80, 80, 80, 80,  
                        //16-20
                        80, 80, 80, 200, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 15, 16 }, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 14, 18 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7, 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSFCDocNo.Text, "B.SFCDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By C.DocDt, B.SFCDocNo, B.Code;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "SFCDocNo", "CodeItem", "ItCode", "ItName", 
                           
                            //6-10
                            "BatchNo", "PlannedQty", "SFCQty", "PlanningUomCode", "PlannedQty2",  
                            
                            //11-14
                            "SFCQty2", "PlanningUomCode2", "Remark", "SFCRemark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            if (dr.GetDecimal(c[7])==0)
                                Grd.Cells[Row, 14].Value = 0m;               
                            else
                                Grd.Cells[Row, 14].Value = dr.GetDecimal(c[8]) / dr.GetDecimal(c[7]) * 100;
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                            if (dr.GetDecimal(c[10]) == 0)
                                Grd.Cells[Row, 18].Value = 0m;
                            else
                                Grd.Cells[Row, 18].Value = dr.GetDecimal(c[11]) / dr.GetDecimal(c[10]) * 100;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12, 14, 15, 16, 18 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmShopFloorControl2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Planned document");
        }

        private void ChkSFCDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SFC document");
        }

        private void TxtSFCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

       
        #endregion
    }
}
