﻿#region Update
/*
    24/05/2022 [MYA/HIN] Reporting baru Performance Review KPI
    23/11/2022 [HPH/HIN] merubah nama kolom
    07/12/2022 [WED/HIN] tambah kolom Goals Setting dan hanya Goals Process yg approved dan aktif saja yang muncul
    09/12/2022 [WED/HIN] tambah approval status dan created indicator
    14/12/2022 [WED/HIN] tambah checkbox exclude cancel
    15/12/2022 [WED/HIN] checkbox juga berpengaruh ke approval status di GoalsProcess dan NewPerformanceReview
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPerformanceReview : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        internal bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptPerformanceReview(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            GetParameter();
            SetGrd();
            Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sl.SetLueYr(LueYr, string.Empty);
            Sm.SetLue(LueYr, Sm.ServerCurrentDateTime().Substring(0, 4));
            Sl.SetLueDivisionCode(ref LueDivisionCode);
            Sl.SetLueOption(ref LuePeriod, "GoalsProcessPeriode");
            ChkExcludedCancelInd.Checked = true;
            base.FrmLoad(sender, e);
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT E.EmpCode, E.EmpName, F.DeptName, G.SiteName, H.DivisionName, ");
            SQL.AppendLine("A.GoalsName, A.DocNo AS GoalsDocNo, Left(A.DocDt, 4) as Yr, D.GrdStatus,  ");
            SQL.AppendLine("B.DocNo AS GoalsProcessDocNo, C.DocNo AS PerformanceReviewDocNo, C.TotalScore as Total, ");
            SQL.AppendLine("A.Status As GoalsSettingStatus, B.Status As GoalsProcessStatus, C.Status As NewPerformanceReviewStatus ");
            SQL.AppendLine("FROM tblgoalssettinghdr A ");
            SQL.AppendLine("Left JOIN tblgoalsprocesshdr B ON B.GoalsDocNo = A.DocNo And B.CancelInd = 'N' ");
            if (ChkExcludedCancelInd.Checked) SQL.AppendLine("    And B.Status In ('O', 'A') ");
            SQL.AppendLine("LEFT JOIN tblnewperformancereviewhdr C ON C.GoalsProcessDocNo = B.DocNo And C.CancelInd = 'N' ");
            if (ChkExcludedCancelInd.Checked) SQL.AppendLine("    And C.Status In ('O', 'A') ");
            SQL.AppendLine("LEFT JOIN tblperformancegrade D ON D.GrdCode = C.GrdCode ");
            SQL.AppendLine("INNER JOIN tblemployee E ON E.EmpCode = A.PICCode ");
            SQL.AppendLine("Inner JOIN tbldepartment F ON F.DeptCode = E.DeptCode ");
            SQL.AppendLine("LEFT JOIN tblsite G ON G.SiteCode = E.SiteCode ");
            SQL.AppendLine("LEFT JOIN tbldivision H ON H.DivisionCode = E.DivisionCode ");
            SQL.AppendLine("WHERE Left(A.DocDt, 4) = @Yr And A.Actind = 'Y' ");
            if (ChkExcludedCancelInd.Checked) SQL.AppendLine("    And A.Status In ('O', 'A') ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Employee's Code",
                    "Employee's Name",
                    "Department",
                    "Site",
                    "Division",
                        
                    //6-10
                    "Goal Setting",
                    "Goal Setting#",
                    "Goals Setting's Status",
                    "Year",
                    "Performance Review" + Environment.NewLine + "(HIN)#",

                    //11-15
                    "Performance Review Indicator",
                    "Performance Review Status",
                    "KPI Result (HIN)#",
                    "KPI Result (HIN) Indicator",
                    "KPI Result (HIN) Status",

                    //16-17
                    "Total",
                    "Grade"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    100, 180, 150, 150, 200, 
                        
                    //6-10
                    200, 150, 160, 60, 150, 

                    //11-15
                    160, 160, 150, 160, 160,

                    //16-17
                    100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 11, 14 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            try
            {
                SetSQL();
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                if (Sm.GetLue(LueSiteCode).Length > 0) Filter += " And E.SiteCode = @SiteCode ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtGoalsName.Text, new string[] { "A.GoalsName" });
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "E.EmpCode", "E.EmpName" });
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePeriod), "B.Period", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "E.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDivisionCode), "E.DivisionCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL() + Filter + " Group By A.DocNo Order By A.DocNo;",
                    new string[]
                    {
                        //0
                        "EmpCode", 

                        //1-5
                        "EmpName", "DeptName", "SiteName", "DivisionName", "GoalsName",

                        //6-10
                        "GoalsDocNo", "GoalsSettingStatus" , "Yr", "GoalsProcessDocNo", "GoalsProcessStatus", 

                        //11-14
                        "PerformanceReviewDocNo", "NewPerformanceReviewStatus", "Total", "GrdStatus"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Grd.Cells[Row, 8].Value = ApprovalDesc(Sm.DrStr(dr, c[7]));
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Grd.Cells[Row, 11].Value = Sm.DrStr(dr, c[9]).Length != 0;
                        Grd.Cells[Row, 12].Value = ApprovalDesc(Sm.DrStr(dr, c[10]));
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Grd.Cells[Row, 14].Value = Sm.DrStr(dr, c[11]).Length != 0;
                        Grd.Cells[Row, 15].Value = ApprovalDesc(Sm.DrStr(dr, c[12]));
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private string ApprovalDesc(string ApprovalCode)
        {
            string approvalDesc = string.Empty;

            switch(ApprovalCode.ToUpper())
            {
                case "A": approvalDesc = "Approved"; break;
                case "O": approvalDesc = "Outstanding"; break;
                case "C": approvalDesc = "Cancelled"; break;
            }

            return approvalDesc;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

        #region Event

        private void ChkGoalsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Goals Name");
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtGoalsName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueDivisionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDivisionCode, new Sm.RefreshLue1(Sl.SetLueDivisionCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDivisionCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Division");
        }

        private void LuePeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePeriod, new Sm.RefreshLue2(Sl.SetLueOption), "GoalsProcessPeriode");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion
    }
}
