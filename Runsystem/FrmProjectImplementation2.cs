﻿#region Update
/*
    14/10/2019 [WED/IMS] new apps, resource nya ambil dari BOQ dan BOM
    25/10/2019 [DITA/IMS] Left join ke resource dan tblsite karna tidak mandatory di lop
    28/11/2019 [DITA/IMS] fitur Attachment file di wbs
    11/12/2019 [WED/IMS] tab Resource hanya bisa dilihat oleh beberapa group, berdasarkan parameter GrpCodeForAccessResourcePRJI2
    13/12/2019 [WED/IMS] WBS tidak pakai price, tambah combo box item di detail nya (ambil dari item di SO Contract nya)
    06/01/2020 [WED/IMS] yg disave di ProjectImplementationDtl2 hanya ItCode saja (tanpa BOM dan BOQ nya)
    17/02/2020 [TKG/IMS] tambah item's local code
    19/02/2020 [WED/IMS] tambah outstanding delivery nya
    12/05/2020 [WED/IMS] semua amount FrmSOContract2 di hide (grid) dan dibuat 0 (header) berdasarkan parameter IsSOContract2AmtIsZero
    28/05/2020 [WED/IMS] di tab Resource tambah kolom baru --> Finised Good. item nya ambil dari list of items nya SO Contract
    16/07/2020 [WED/IMS] item resource bisa dipilih berkali kali
    31/08/2020 [DITA/IMS] BUG : waktu show tidak readonly di grid WBS
    27/10/2020 [DITA/IMS] PENAMBAHAN KOLOM NOMOR ITEM di WBS
    06/11/2020 [DITA/IMS] membuat parameter sehingga task/stage/itemnya bisa duplicate : IsWBSDuplicateAllowed
    12/11/2020 [IBL/IMS] Menambah kolom sequence number
    12/11/2020 [IBL/IMS] Hide loop untuk view dokumen SOContract, BOQ, dan LOP berdasarkan group menu
    12/11/2020 [IBL/IMS] Tambah field Number di header
    12/11/2020 [IBL/IMS] Membuat printout PRJI
    12/11/2020 [IBL/IMS] BUG : Project Code belum mengikuti project code di master project group 
    23/11/2020 [ICA/IMS] Mengubah rumus kolom persentase menjadi ((unit-outstanding delivery)/unit)*100%
    05/12/2020 [WED/IMS] yg tidak boleh sama adalah : No, Item, Stage, Task.
    18/05/2020 [VIN/IMS] tambah info no, dan item code di lue item 
    21/05/2020 [VIN/IMS] ganti item code jadi ItCodeInternal di lue item 
    15/06/2021 [VIN/IMS] Saat Draft Achievement ttp terupdate
    09/07/2021 [VIN/IMS] Feedback Lue Item order by NO SOC 
    04/10/2021 [VIN/IMS] Total Percentage header --> ComputeTotalPercentage()
    14/01/2022 [ICA/IMS] Bug ketika memilih item dengan itcode yg sama dan No yg berbeda, item yg muncul belum sesuai. SetLueItCode -> Col 1 : Concat ItCode dan No
    20/04/2022 [BRI/PRODUCT] PPN bisa diganti dan set default ppn dengan param PPNDefaultSetting
    25/04/2022 [DITA/PRODUCT] PPh source nya dari option baru dan set default pph dengan param PPhDefaultSetting
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using System.IO;

using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementation2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mListItCtCodeForResource = string.Empty
            ;
        internal FrmProjectImplementation2Find FrmFind;
        private string
            mItGrpCodeForRemuneration = string.Empty,
            mItGrpCodeForDirectCost = string.Empty,
            mPPN10ForProjectResourceShowZero = string.Empty,
            mProjectResourceCode = string.Empty,
            mStateInd = string.Empty,
            mWBSFormula = string.Empty,
            mRemunerationItCtCode = string.Empty,
            mDirectCostItCtCode = string.Empty,
            mPRJIDocType = "2",
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mGrpCodeForAccessResourcePRJI2 = string.Empty;
        private bool 
            mIsProjectImplementationApprovalBySiteMandatory = false,
            mIsProjectImplementationDisplayTotalResource = false,
            mIsProjectImplementationDisplayTaxFields = false,
            mIsWBSBobotColumnAutoCompute = false,
            IsInsert = false,
            mIsUserAllowedToAccessResource = false,
            mIsWBSDuplicateAllowed = false
            ;
        internal bool mIsFilterBySite = false, mIsSOContract2AmtIsZero = false;
        private byte[] downloadedData;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmProjectImplementation2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmProjectImplementation2");

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();

                BtnSOContractDocNo2.Visible = GetAccessInd("FrmSOContract2");
                BtnBOQDocNo.Visible = GetAccessInd("FrmBOQ2");
                BtnLOPDocNo.Visible = GetAccessInd("FrmLOP");

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueSPCode(ref LueSPCode);
                Sl.SetLueOption(ref LueProcessInd, "ProjectImplementationProcessInd");

                TcProjectImplementation.SelectTab("TpgDocument");
                SetGrd4();

                TcProjectImplementation.SelectTab("TpgIssue");
                SetGrd3();

                if (mGrpCodeForAccessResourcePRJI2.Length > 0)
                {
                    string[] s = mGrpCodeForAccessResourcePRJI2.Split(',');
                    string mGrpCode = Sm.GetValue("Select GrpCode From TblUser Where UserCode = @Param Limit 1;", Gv.CurrentUserCode);
                    foreach (string d in s)
                    {
                        if (mGrpCode == d)
                        {
                            mIsUserAllowedToAccessResource = true;
                            break;
                        }
                    }
                }
                TcProjectImplementation.SelectTab("TpgResource");
                Sl.SetLueOption(ref LuePPN, "PPNForProject");
                Sl.SetLueOption(ref LuePPh, "PPhForProject");
                Sm.SetLue(LuePPN, Sm.GetParameter("PPNDefaultSetting"));
                Sm.SetLue(LuePPh, Sm.GetParameter("PPhDefaultSetting"));
                SetGrd2();
                if (!mIsProjectImplementationDisplayTotalResource)
                {
                    LblTotalResource.Visible = TxtTotalResource.Visible = false;
                }

                if (!mIsProjectImplementationDisplayTaxFields)
                {
                    panel7.Visible = panel8.Visible = false;
                }

                if (!mIsUserAllowedToAccessResource) TcProjectImplementation.TabPages.Remove(TpgResource);

                TcProjectImplementation.SelectTab("TpgWBS");
                Sl.SetLueProjectStageCode(ref LueStageCode);
                Sl.SetLueProjectTaskCode(ref LueTaskCode);
                SetLueItCode(ref LueItCode);
                LueStageCode.Visible = false;
                LueTaskCode.Visible = false;
                LueItCode.Visible = false;
                LblTotalPrice.Visible = TxtTotalPrice.Visible = false;
                SetGrd();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTotalResource,
                        TxtTotalPrice,
                        TxtTotalPersentase,
                        TxtTotalBobot
                    }, true);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Grid 1 - WBS

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                { 
                    //0
                    "StageCode", 
                    //1-5
                    "Stage", "TaskCode", "Task", "Plan Start Date", "Plan End Date",
                    //6-10
                    "Price", "Unit", "Percentage", "Settled",  "Remark",
                    //11-15
                    "Estimated Amount", "Settled Amount", "Settled Date", "File Name",  "", 
                    //16-20
                    "", "File Name 2", "Item's Code", "Item's Name", "Local Code",
                    //21-25
                    "Outstanding Delivery", "No", "Sequence"+Environment.NewLine+"Number", "Notes", "Notes Detail",
                    //26-27
                    "SO Contract"+Environment.NewLine+"Remark", "UoM"
                },
                new int[] 
                { 
                    0, 
                    200, 0, 200, 100, 100,
                    100, 100, 100, 100, 100,
                    120, 120, 100, 200, 20,
                    20, 200, 130, 200, 130,
                    150, 100, 80, 120, 120,
                    500, 0
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 11, 12, 21 }, 8);
            Sm.GrdColButton(Grd1, new int[] { 15, 16 });
            Sm.GrdColCheck(Grd1, new int[] { 9 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5, 13 });
            if (mIsWBSBobotColumnAutoCompute)
                Sm.GrdColReadOnly(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 6, 11, 12, 17, 18, 27 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 });
            Grd1.Cols[19].Move(4);
            Grd1.Cols[20].Move(5);
            Grd1.Cols[21].Move(11);
            Grd1.Cols[22].Move(0);
            Grd1.Cols[23].Move(0);
        }

        #endregion

        #region Grid 2 - Resource

        private void SetGrd2()
        {
            Grd2.Cols.Count = 23;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                { 
                    "",
                    "ResourceItCode", "Resource", "Group", "Remark", "Quantity 1"+Environment.NewLine+"(Unit)", 
                    "Quantity 2"+Environment.NewLine+"(Time)", "Unit Price", "Tax", "Total Without Tax", "Total Tax", 
                    "Total With Tax", "Group It Code", "", "Detail#", "DocType",
                    "BOQ#", "BOM#", "BOMD#", "", "ItCode", 
                    "Finished Good", "Sequence"+Environment.NewLine+"Number"
                },
                new int[] 
                { 
                    20, 
                    0, 200, 200, 200, 150, 
                    150, 180, 180, 200, 200, 
                    200,150, 20, 120, 0,
                    180, 180, 0, 20, 0, 
                    200, 80
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7, 8, 9, 10, 11 }, 8);
            Sm.GrdColButton(Grd2, new int[] { 0, 13, 19 });
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2, 3, 9, 10, 11, 14, 15, 16, 17, 18, 20, 21, 22 });
            Sm.GrdColInvisible(Grd2, new int[] { 12, 15, 18, 20 });
            Grd2.Cols[21].Move(3);
            Grd2.Cols[19].Move(3);
            Grd2.Cols[22].Move(0);
        }

        #endregion

        #region Grid 3 - Issue

        private void SetGrd3()
        {
            Grd3.Cols.Count = 2;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd3, new string[] 
                { 
                    "Remark",
                    "PIC"
                },
                new int[] 
                { 
                    200, 
                    200
                }
            );
        }

        #endregion

        #region Grid 4 - Document

        private void SetGrd4()
        {
            Grd4.Cols.Count = 5;
            Grd4.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd4, new string[] 
                { 
                    "",
                    "DocCode", "Document", "Date", "Remark"
                },
                new int[] 
                { 
                    20, 
                    0, 200, 100, 200
                }
            );
            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdColReadOnly(Grd4, new int[] { 1, 2 });
            Sm.GrdFormatDate(Grd4, new int[] { 3 });
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtSOContractDocNo, TxtAchievement,
                        TxtBOQDocNo, TxtLOPDocNo, LueCtCode, LueCtContactPersonName, LueSPCode, LueStageCode, LueTaskCode,
                        DteDocDt2, DtePlanEndDt, DtePlanStartDt, MeeRemark, LueProcessInd, TxtProjectName, TxtTotalResource,
                        TxtProjectCode, TxtType, TxtTotal, TxtRemunerationCost, TxtDirectCost, TxtContractAmt, TxtPPhAmt, TxtPPNAmt, TxtExclPPN, TxtNetCash, TxtPPNPercentage, TxtPPNPPhPercentage,
                        TxtTotalBobot, TxtTotalPersentase, TxtTotalPrice, LueItCode, TxtNumber, LuePPN, LuePPh
                    }, true);
                    BtnSOContractDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = true;
                    if (TxtDocNo.Text.Length > 0)
                    {
                        Grd2.ReadOnly = false;
                        Sm.GrdColReadOnly(Grd2, new int[] { 4, 5, 6, 7, 8 });
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 16 });
                    }
                    BtnImportWBS.Enabled = false;
                    BtnImportResource.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueStageCode, LueTaskCode, DteDocDt2, DtePlanStartDt, DtePlanEndDt, MeeRemark, LueItCode, TxtNumber, LuePPN, LuePPh
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, true);
                    BtnSOContractDocNo.Enabled = true;
                    Grd1.ReadOnly = Grd2.ReadOnly = Grd3.ReadOnly = Grd4.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 4, 5, 6, 7, 8, 22 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 4, 5, 7, 19, 22, 23, 24, 25 });
                    BtnImportWBS.Enabled = true;
                    BtnImportResource.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    if (Sm.GetLue(LueProcessInd) == "D")
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd, TxtNumber, LuePPN, LuePPh }, false);
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 22 });
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23, 24, 25 });
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd, LuePPN, LuePPh }, true);
                        Sm.GrdColReadOnly(true, false, Grd2, new int[] { 22 });
                        Sm.GrdColReadOnly(true, false, Grd1, new int[] { 23, 24, 25 });
                    }

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();

                    //if (Sm.GetLue(LueProcessInd) == "D")
                    //{
                    //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    //    { 
                    //        LueStageCode, LueTaskCode, DtePlanStartDt, DtePlanEndDt, LueItCode
                    //    }, false);
                    //    Grd1.ReadOnly = Grd2.ReadOnly = false;
                    //    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 4, 5, 6, 7, 8 });
                    //}
                    //else Grd1.ReadOnly = Grd2.ReadOnly = true;

                    if (TxtDocNo.Text.Length > 0)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LueStageCode, LueTaskCode, DtePlanStartDt, DtePlanEndDt, LueItCode
                        }, false);
                        Grd1.ReadOnly = Grd2.ReadOnly = false;
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 4, 5, 6, 7, 8 });
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 4, 5, 7, 19, 22 });
                        for (int i = 0; i < Grd1.Rows.Count; ++i)
                        {
                            if (Sm.GetGrdDec(Grd1, i, 7) != Sm.GetGrdDec(Grd1, i, 21))
                            {
                                Grd1.Rows[i].ReadOnly = iGBool.True;
                            }
                        }
                    }

                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt2
                    }, false);
                    Grd3.ReadOnly = Grd4.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            IsInsert = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, TxtSOContractDocNo,
                TxtBOQDocNo, TxtLOPDocNo, LueCtCode, LueCtContactPersonName, LueSPCode, LueStageCode, LueTaskCode,
                DteDocDt2, DtePlanEndDt, DtePlanStartDt, MeeRemark, TxtStatus, LueProcessInd, TxtProjectName,
                TxtProjectCode, TxtType, TxtTotal, TxtRemunerationCost, TxtDirectCost, TxtContractAmt, TxtPPhAmt, TxtPPNAmt, TxtExclPPN, TxtNetCash, TxtPPNPercentage, TxtPPNPPhPercentage,
                TxtTotalBobot, TxtTotalPersentase, TxtTotalPrice, LueItCode, TxtNumber
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAchievement, TxtTotalResource, TxtTotal, TxtRemunerationCost, TxtDirectCost, TxtContractAmt, TxtPPhAmt, TxtPPNAmt, TxtExclPPN, TxtNetCash, TxtPPNPercentage, TxtPPNPPhPercentage, TxtTotalBobot, TxtTotalPersentase, TxtTotalPrice }, 0);
            ChkCancelInd.Checked = false;
            Sm.SetLue(LuePPN, Sm.GetParameter("PPNDefaultSetting"));
            Sm.SetLue(LuePPh, Sm.GetParameter("PPhDefaultSetting"));
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            mStateInd = string.Empty;
        }

        #region Clear Grid

        internal void ClearGrd()
        {
            ClearGrdWBS();
            ClearGrdResource();
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd4, true);
        }

        private void ClearGrdWBS()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6, 7, 8, 11, 12, 21 });
        }

        private void ClearGrdResource()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7, 8, 9, 10, 11 });
        }

        #endregion

        #endregion

        #region Additional Methods

        private decimal PPNRate()
        {
            var rate = Decimal.Parse(Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param", Sm.GetLue(LuePPN)));
            rate = rate / 100;
            return rate;
        }
        private decimal PPhRate()
        {
            var rate = Decimal.Parse(Sm.GetValue("Select TaxRate From TblTax Where TaxCode=@Param", Sm.GetLue(LuePPh)));
            rate = rate / 100;
            return rate;
        }


        private bool IsExceedMaxChar(string Data)
        {
            return Data.Length > 4;
        }

        private MySqlCommand UpdateAchievement()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select DocNo, Sum(If((SettledBobot >= Bobot), 1, (SettledBobot / Bobot)) * BobotPercentage) BobotPercentage ");
            SQL.AppendLine("    From TblProjectImplementationDtl ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And SettledBobot != 0 ");
            SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") B On A.DocNO = B.DocNo ");
            SQL.AppendLine("Set A.Achievement = B.BobotPercentage, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.CancelInd = 'N' And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateLatestSOContractRevision(string SOContractDocNo)
        {
            string sSQL = string.Empty;            

            sSQL += "Update TblProjectImplementationHdr ";
            sSQL += "Set SOContractDocNo = @SOContractDocNo ";
            sSQL += "Where DocNo = @DocNo; ";

            var cm = new MySqlCommand() { CommandText = sSQL };

            Sm.CmParam<String>(ref cm, "@SOContractDocNo", SOContractDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblProjectImplementationRevisionHdr ");
            SQL.AppendLine("       Where PRJIDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtDocNo.Text);
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Concat(A.ItCode, A.No) Col1, Concat('|', A.No, '|',B.ItCodeInternal, '|', B.ItName, '|') Col2 ");
            SQL.AppendLine("From TblSOContractDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select SOCDocNo ");
            SQL.AppendLine("    From TblSOContractRevisionHdr ");
            SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By CAST(A.No AS DECIMAL(10,0)), B.ItName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

             if (!IsInsert) Grd1.Cells[Row, 14].Value = toUpload.Name;
             if (IsInsert)
            {
                var cml = new List<MySqlCommand>();
                cml.Add(UpdateWBSFile(DocNo, Row, toUpload.Name));
                Sm.ExecCommands(cml);
            }
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblProjectImplementationDtl ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        internal void ComputeTotalResource()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd2, row, 11);
                }
            }
            TxtTotalResource.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeRemunerationCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0 && Sm.GetGrdStr(Grd2, row, 12).ToString() == mItGrpCodeForRemuneration && mItGrpCodeForRemuneration.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd2, row, 11);
                }
            }
            TxtRemunerationCost.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeDirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length != 0 && Sm.GetGrdStr(Grd2, row, 12).ToString() == mItGrpCodeForDirectCost && mItGrpCodeForDirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd2, row, 11);
                }
            }
            TxtDirectCost.EditValue = Sm.FormatNum(Total, 0);
        }

        internal void ComputeDetailAmt()
        {
            decimal NetCash= 0m;
            decimal PPNAmt = 0m;
            decimal PPhAmt = 0m;
            decimal ContractAmt = 0m;

            if (mProjectResourceCode == mPPN10ForProjectResourceShowZero)
                TxtPPNAmt.EditValue = Sm.FormatNum(0, 0);
            else
            {
                PPNAmt = Sm.GetDecValue(TxtExclPPN.Text) * PPNRate(); //0.1m //ExclPPN - Sm.GetDecValue(TxtContractAmt.Text);
                TxtPPNAmt.EditValue = Sm.FormatNum(PPNAmt, 0);
            }

            PPhAmt = Sm.GetDecValue(TxtExclPPN.Text) *PPhRate();
            TxtPPhAmt.EditValue = Sm.FormatNum(PPhAmt, 0);

            ContractAmt = Sm.GetDecValue(TxtExclPPN.Text) + PPNAmt;
            TxtContractAmt.EditValue = Sm.FormatNum(ContractAmt, 0);

            NetCash = Sm.GetDecValue(TxtExclPPN.Text) - PPhAmt;
            TxtNetCash.EditValue = Sm.FormatNum(NetCash, 0);    
        }

        internal void ComputeTotal()
        {
            decimal PPNPercentage = 0m;
            decimal PPhPPNPercentage = 0m;
            decimal Total = 0m;
            
            Total = Sm.GetDecValue(TxtRemunerationCost.Text) + Sm.GetDecValue(TxtDirectCost.Text);
            TxtTotal.EditValue = Sm.FormatNum(Total, 0);

            if (Sm.GetDecValue(TxtExclPPN.Text) != 0) PPNPercentage = (Total / Sm.GetDecValue(TxtExclPPN.Text)) * 100m;
            TxtPPNPercentage.EditValue = Sm.FormatNum(PPNPercentage, 0);

            if (Sm.GetDecValue(TxtNetCash.Text) != 0) PPhPPNPercentage = (Total / Sm.GetDecValue(TxtNetCash.Text)) * 100m;
            TxtPPNPPhPercentage.EditValue = Sm.FormatNum(PPhPPNPercentage, 0);
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'ProjectImplementation' ");
            if (mIsProjectImplementationApprovalBySiteMandatory)
            {
                SQL.AppendLine("And SiteCode Is Not Null ");
                SQL.AppendLine("And SiteCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select SiteCode ");
                SQL.AppendLine("    From TblLOPHdr ");
                SQL.AppendLine("    Where DocNo = @Param ");
                SQL.AppendLine(") ");
            }

            return Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text);
        }

        private bool IsRevisionNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'ProjectImplementationRev' Limit 1; ");
        }

        internal string GetSelectedItCode()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 1) + "##";
            }
            //return (SQL.Length == 0 ? "##XXX##" : SQL);
            return "#XXX#";
        }

        internal string GetSelectedDocCode()
        {
            var SQL = string.Empty;
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd4, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mListItCtCodeForResource = Sm.GetParameter("ListItCtCodeForResource");
            mIsProjectImplementationApprovalBySiteMandatory = Sm.GetParameterBoo("IsProjectImplementationApprovalBySiteMandatory");
            mItGrpCodeForRemuneration = Sm.GetParameter("ItGrpCodeForRemuneration");
            mItGrpCodeForDirectCost = Sm.GetParameter("ItGrpCodeForDirectCost");
            mPPN10ForProjectResourceShowZero = Sm.GetParameter("PPN10%ForProjectResourceShowZero");
            mIsProjectImplementationDisplayTaxFields = Sm.GetParameterBoo("IsProjectImplementationDisplayTaxFields");
            mIsProjectImplementationDisplayTotalResource = Sm.GetParameterBoo("IsProjectImplementationDisplayTotalResource");
            mIsWBSBobotColumnAutoCompute = Sm.GetParameterBoo("IsWBSBobotColumnAutoCompute");
            mWBSFormula = Sm.GetParameter("WBSFormula");
            mRemunerationItCtCode = Sm.GetParameter("RemunerationItCtCode");
            mDirectCostItCtCode = Sm.GetParameter("DirectCostItCtCode");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mGrpCodeForAccessResourcePRJI2 = Sm.GetParameter("GrpCodeForAccessResourcePRJI2");
            mIsSOContract2AmtIsZero = Sm.GetParameterBoo("IsSOContract2AmtIsZero");
            mIsWBSDuplicateAllowed = Sm.GetParameterBoo("IsWBSDuplicateAllowed");

            if (mWBSFormula.Length == 0) mWBSFormula = "1";
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowSOContractDocument(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select X1.DocNo, A.BOQDocNo, B.LOPDocNo, B.CtCode, B.CtContactPersonName, B.SPCode, F.ProjectCode, F.ProjectName, X1.Amt, D.OptDesc, C.ProjectResource, A.ProjectCode2 ");
            SQL.AppendLine("From TblSOContractRevisionHdr X1 ");
            SQL.AppendLine("Inner Join TblSOContractHdr A On X1.SOCDocNo = A.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr C On B.LOPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblOption D On C.ProjectType = D.OptCode And D.OptCat = 'ProjectType' ");
            SQL.AppendLine("Left Join TblOption E On C.ProjectType = E.OptCode And E.OptCat = 'ProjectResource' ");
            SQL.AppendLine("Left Join TblProjectGroup F ON C.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By X1.CreateDt Desc Limit 1; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "BOQDocNo", "LOPDocNo", "CtCode", "CtContactPersonName", "SPCode",

                    //6-10
                    "ProjectName", "Amt", "OptDesc", "ProjectResource", "ProjectCode2",

                    //11
                    "ProjectCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[5]));
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtExclPPN.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtType.EditValue = Sm.DrStr(dr, c[8]);
                    mProjectResourceCode = Sm.DrStr(dr, c[9]);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[11]);
                }, true
            );

            ComputeDetailAmt();
            SetLueItCode(ref LueItCode);
        }

        private void ComputeTotalPercentage()
        {
            decimal TotalPersen = 0m;
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0) TotalPersen += Sm.GetGrdDec(Grd1, Row, 8);
            TxtTotalPersentase.EditValue = Sm.FormatNum(TotalPersen / (Grd1.Rows.Count - 1), 2);
        }
        private void ComputeBobotPercentage()
        {
            decimal mTotalBobot = 0m;
            decimal mTotalBobot2 = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    mTotalBobot += Sm.GetGrdDec(Grd1, Row, 7);

            if (mTotalBobot >= 99.5m) mTotalBobot = 100m;
            if (mIsWBSBobotColumnAutoCompute) TxtTotalBobot.EditValue = Sm.FormatNum(mTotalBobot, 0);
            else
            {

                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    mTotalBobot2 += Sm.GetGrdDec(Grd1, i, 7);
                }
                TxtTotalBobot.EditValue = Sm.FormatNum(mTotalBobot2, 0);
            }
            TxtTotalPersentase.EditValue = 100m;
            if (mTotalBobot != 0 && mIsWBSBobotColumnAutoCompute)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    {
                        Grd1.Cells[Row, 8].Value = Sm.Round(((Sm.GetGrdDec(Grd1, Row, 7) - (Sm.GetGrdDec(Grd1, Row, 21))) / (Sm.GetGrdDec(Grd1, Row, 7)) * 100m), 2);
                    }
                }
            }

            if (!mIsWBSBobotColumnAutoCompute && mTotalBobot2 != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    {
                        Grd1.Cells[Row, 8].Value = Sm.Round(((Sm.GetGrdDec(Grd1, Row, 7) - (Sm.GetGrdDec(Grd1, Row, 21))) / (Sm.GetGrdDec(Grd1, Row, 7)) * 100m), 2);
                    }
                }
            }
        }

        private void ComputeBobot()
        {
            decimal mTotalPrice = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    mTotalPrice += Sm.GetGrdDec(Grd1, Row, 6);

            TxtTotalPrice.EditValue = Sm.FormatNum(mTotalPrice, 0);

            if (mIsWBSBobotColumnAutoCompute)
            {
                if (mTotalPrice != 0)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                        {
                            Grd1.Cells[Row, 7].Value = Sm.Round(((Sm.GetGrdDec(Grd1, Row, 6) / mTotalPrice) * 100m), 2);
                        }
                    }
                }
            }
        }

        //private void ComputeEstimatedAmt()
        //{
        //    decimal mGrandTotalSOCRev = 0m;
        //    decimal mAmt = 0m, mRevAmt = 0m;
        //    var cm = new MySqlCommand();

        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Sum(T.Amt) Amt, Sum(T.RevAmt) RevAmt ");
        //    SQL.AppendLine("From ");
        //    SQL.AppendLine("( ");
        //    SQL.AppendLine("    Select Amt, 0 As RevAmt ");
        //    SQL.AppendLine("    From TblSOContractHdr ");
        //    SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("    Select 0 As Amt, Amt As RevAmt ");
        //    SQL.AppendLine("    From TblSOContractRevisionHdr ");
        //    SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
        //    SQL.AppendLine(") T; ");

        //    Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "Amt", "RevAmt" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                mAmt = Sm.DrDec(dr, c[0]);
        //                mRevAmt = Sm.DrDec(dr, c[1]);
        //            }
        //        }
        //        dr.Close();
        //    }

        //    if (mRevAmt > 0m) mGrandTotalSOCRev = mRevAmt;
        //    else mGrandTotalSOCRev = mAmt;

        //    for (int i = 0; i < Grd1.Rows.Count; i++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, i, 0).Length > 0)
        //        {
        //            if (mGrandTotalSOCRev == 0m)
        //                Grd1.Cells[i, 11].Value = 0m;
        //            else
        //                Grd1.Cells[i, 11].Value = (Sm.GetGrdDec(Grd1, i, 8) * mGrandTotalSOCRev) / 100m;

        //            Grd1.Cells[i, 12].Value = 0m; // Settled Amount masih 0 saat insert data
        //        }
        //    }
        //}

        private void SetLueCtPersonCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode= '" + CtCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        internal bool GetAccessInd(string FormName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Left(AccessInd, 1), 'N') ");
            SQL.AppendLine("From TblGroupMenu ");
            SQL.AppendLine("Where MenuCode In ( Select MenuCode From TblMenu Where Param = @Param1 ) ");
            SQL.AppendLine("And GrpCode In ");
            SQL.AppendLine("(Select GrpCode From TblUser Where UserCode = @Param2) ");
            SQL.AppendLine("Limit 1; ");

            return Sm.GetValue(SQL.ToString(), FormName, Gv.CurrentUserCode, string.Empty) == "Y";
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectImplementation2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetLue(LueProcessInd,"D");
                mStateInd = "I";
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateInd = "E";
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                    IsInsert = true;
                }
                else
                {
                    EditData();
                    IsInsert = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0)
            {
                if (Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;

                string[] TableName = { "PRJI", "PRJIDtl", "PRJIDtl2" };
                List<IList> myLists = new List<IList>();
                var l = new List<PRJI>();
                var lDtl = new List<PRJIDtl>();
                var lDtl2 = new List<PRJIDtl2>();

                #region Header

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
                SQL.AppendLine("A.Number, A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A1.DocNo SOCDocNo, A1.PONo, E.ProjectName, A.Remark, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'PRJIPrintoutSignPosition') PositionName, ");
                SQL.AppendLine("(Select B.EmpName ");
	            SQL.AppendLine("    From TblParameter A ");
	            SQL.AppendLine("    Inner Join TblEmployee B On A.ParValue = B.EmpCode ");
	            SQL.AppendLine("    Where ParCode = 'PRJIPrintoutSignName' ");
                SQL.AppendLine(") EmployeeName ");
                SQL.AppendLine("From TblProjectImplementationHdr A ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocno = A1.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr B on A1.SOCDocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblBOQhdr C On B.BOQDocno = C.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr D On C.LOPDocNo = D.DocNo ");
                SQL.AppendLine("Left Join TblProjectGroup E On D.PGCode = E.PGCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "Number",
                         "DocNo",
                         "DocDt",
                         "SOCDocNo",
                         
                         //6-10
                         "PONo",
                         "ProjectName",
                         "Remark",
                         "PositionName",
                         "EmployeeName"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PRJI()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                Number = Sm.DrStr(dr, c[2]),
                                DocNo = Sm.DrStr(dr, c[3]),
                                DocDt = Sm.DrStr(dr, c[4]),
                                SOCDocNo = Sm.DrStr(dr, c[5]),
                                PONo = Sm.DrStr(dr, c[6]),
                                ProjectName = Sm.DrStr(dr, c[7]),
                                Remark = Sm.DrStr(dr, c[8]),
                                PositionName = Sm.DrStr(dr, c[9]),
                                EmployeeName = Sm.DrStr(dr, c[10]),
                            });
                        }
                    }
                    dr.Close();
                }

                myLists.Add(l);
                #endregion

                #region Detail 1

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                
                SQLDtl.AppendLine("Select A.DocNo, A.DNo, B.* ");
                SQLDtl.AppendLine("From TblProjectImplementationDtl A ");
                SQLDtl.AppendLine("Left Join ");
                SQLDtl.AppendLine("( ");
	            SQLDtl.AppendLine("    Select X1.DocNo, X1.DNo, Sum(X3.Bobot) Progress ");
	            SQLDtl.AppendLine("    From TblProjectImplementationDtl X1 ");
	            SQLDtl.AppendLine("    Inner Join TblProjectDeliveryHdr X2 On X1.DocNo = X2.PRJIDocNo ");
	            SQLDtl.AppendLine("    Inner Join TblProjectDeliveryDtl X3 On X1.DocNo = X3.PRJIDocNo And X1.DNo = X3.PRJIDNo ");
	            SQLDtl.AppendLine("    Where X1.DocNo = @DocNo ");
	            SQLDtl.AppendLine("    And X2.CancelInd = 'N' ");
	            SQLDtl.AppendLine("    Group By X1.DocNo, X1.DNo ");
                SQLDtl.AppendLine(") B On A.DocNo = B.DocNo And A.DNo = B.DNo ");
                SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
                SQLDtl.AppendLine("Order By Right(COncat('0000000000', IfNull(A.SeqNo, '0')), 10); ");

                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-2
                         "DNo",
                         "Progress",
                        });
                    if (drDtl.HasRows)
                    {
                        int Row = 0;
                        while (drDtl.Read())
                        {
                            string mUnit = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 7), 0);
                            string mProgress = Sm.FormatNum(Sm.DrDec(drDtl, cDtl[2]), 0);
                            string mOutstandingDelivery = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 21), 0);

                            lDtl.Add(new PRJIDtl()
                            {
                                PRJIDocNo = Sm.DrStr(drDtl, cDtl[0]),
                                PRJIDNo = Sm.DrStr(drDtl, cDtl[1]),
                                No = Sm.GetGrdStr(Grd1, Row, 22),
                                ItName = Sm.GetGrdStr(Grd1, Row, 19),
                                Specification = Sm.GetGrdStr(Grd1, Row, 26),
                                Unit = Convert.ToDecimal(mUnit),
                                UoM = Sm.GetGrdStr(Grd1, Row, 27),
                                Progress = Convert.ToDecimal(mProgress),
                                TotalProgress = 0m,
                                OutstandingDelivery = Convert.ToDecimal(mOutstandingDelivery),
                            });
                            Row++;
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(lDtl);

                #endregion

                #region Detail 2

                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();

                SQLDtl2.AppendLine("Select DNo, Notes, NotesDetail ");
                SQLDtl2.AppendLine("From TblProjectImplementationDtl ");
                SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                SQLDtl2.AppendLine("And ");
                SQLDtl2.AppendLine("( ");
	            SQLDtl2.AppendLine("    Notes Is Not Null Or ");
	            SQLDtl2.AppendLine("    NotesDetail Is Not Null ");
                SQLDtl2.AppendLine(") ");
                SQLDtl2.AppendLine("Order By Right(COncat('0000000000', IfNull(SeqNo, '0')), 10); ");

                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;
                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                        //0
                        "DNo",

                        //1-2
                        "Notes", 
                        "NotesDetail",
                    });
                    if (drDtl2.HasRows)
                    {

                        while (drDtl2.Read())
                        {
                          
                            lDtl2.Add(new PRJIDtl2()
                            {
                                PRJIDNo = Sm.DrStr(drDtl2, cDtl2[0]),
                                Notes = Sm.DrStr(drDtl2, cDtl2[1]),
                                NotesDetail = Sm.DrStr(drDtl2, cDtl2[2]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(lDtl2);

                foreach (var x in lDtl)
                {
                    foreach (var y in lDtl2.Where(w => w.PRJIDNo == x.PRJIDNo))
                    {
                        y.Unit = x.Unit;
                        y.UoM = x.UoM;
                        y.Progress = x.Progress;
                        y.TotalProgress = x.TotalProgress;
                        y.OutstandingDelivery = x.OutstandingDelivery;
                    }
                }

                #endregion

                Sm.PrintReport("ProjectImplementation2", myLists, TableName, false);
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

            if (e.ColIndex == 16)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 14), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 14, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 15)
                {
                    try
                    {
                    OD.InitialDirectory = "c:";
                    OD.Filter = "PDF files (*.pdf)|*.pdf";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd1.Cells[e.RowIndex, 14].Value = OD.FileName;
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }

                }
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0)
                    {
                        decimal mEstimatedAmt = 0m;
                        if (mWBSFormula == "1") mEstimatedAmt = Sm.GetGrdDec(Grd1, e.RowIndex, 6);

                        Grd1.Cells[e.RowIndex, 11].Value = mEstimatedAmt;
                    }
                }

                if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
                {
                    if (Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length > 0)
                    {
                        Grd1.Cells[e.RowIndex, 21].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 7);
                    }
                }
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 3, 4, 5, 6, 19, 22, 23 }, e.ColIndex))
                {
                    //e.DoDefault = false;
                    if (e.ColIndex == 1) LueRequestEdit(Grd1, LueStageCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 3) LueRequestEdit(Grd1, LueTaskCode, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 4) Sm.DteRequestEdit(Grd1, DtePlanStartDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5) Sm.DteRequestEdit(Grd1, DtePlanEndDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 19) LueRequestEdit(Grd1, LueItCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
                }
            }
            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                //e.DoDefault = false;
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 14), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 14, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && e.KeyCode == Keys.Enter) Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            if (BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                //if (Sm.GetLue(LueProcessInd) == "F"|| Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 14).Length > 0)
                                if (Sm.GetGrdDec(Grd1, Grd1.SelectedRows[Index].Index, 21) != Sm.GetGrdDec(Grd1, Grd1.SelectedRows[Index].Index, 7) || Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 14).Length > 0)
                                {
                                    Sm.StdMsg(mMsgType.Warning, "You can't remove this selected data.");
                                    return;
                                }
                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);
                            }
                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                        }
                    }
                }
            }

            
            

            //if (BtnSave.Enabled && !Grd1.ReadOnly)
            //{
            //    Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            //    Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //}
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd2, new int[] { 5, 6, 7, 8, 9, 10, 11 }, e);
                if (Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8, 9, 10, 11 }, e.ColIndex))
                {
                    decimal mQty1 = 0m, mQty2 = 0m, mUPrice = 0m, mTax, mTotalWithoutTax = 0m, mTotalTax = 0m, mAmt = 0m;
                    mQty1 = Sm.GetGrdDec(Grd2, e.RowIndex, 5);
                    mQty2 = Sm.GetGrdDec(Grd2, e.RowIndex, 6);
                    mUPrice = Sm.GetGrdDec(Grd2, e.RowIndex, 7);
                    mTax = Sm.GetGrdDec(Grd2, e.RowIndex, 8);

                    mTotalWithoutTax = mQty1 * mQty2 * mUPrice;
                    mTotalTax = mQty1 * mQty2 * mTax;
                    mAmt = mTotalWithoutTax + mTotalTax;

                    Grd2.Cells[e.RowIndex, 9].Value = Math.Round(mTotalWithoutTax, 2);
                    Grd2.Cells[e.RowIndex, 10].Value = Math.Round(mTotalTax, 2);
                    Grd2.Cells[e.RowIndex, 11].Value = Math.Round(mAmt, 2);
                    ComputeTotalResource();
                    ComputeDirectCost(); ComputeRemunerationCost();
                }
            }

        }
        
        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd2.ReadOnly)
            {
                if(e.ColIndex == 0 && mStateInd.Length > 0 && !Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false)) Sm.FormShowDialog(new FrmProjectImplementation2Dlg2(this));
                if (e.ColIndex == 13 && Sm.GetGrdStr(Grd2, e.RowIndex, 14).Length > 0)
                {
                    var f = new FrmProjectImplementationRBP(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 14);
                    f.ShowDialog();
                }

                if (e.ColIndex == 19 && !Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 2, false, "Resource is empty."))
                {
                    Sm.FormShowDialog(new FrmProjectImplementation2Dlg4(this, e.RowIndex));
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd2.ReadOnly)
            {
                //e.DoDefault = false;
                if (e.ColIndex == 0 && TxtDocNo.Text.Length <= 0)
                {
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3 });
                }
                if (e.ColIndex == 13 && Sm.GetGrdStr(Grd2, e.RowIndex, 14).Length > 0)
                {
                    //e.DoDefault = false;
                    var f = new FrmProjectImplementationRBP(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 14);
                    f.ShowDialog();
                }
                if (e.ColIndex == 19 && !Sm.IsGrdValueEmpty(Grd2, e.RowIndex, 2, false, "Resource is empty."))
                {
                    Sm.FormShowDialog(new FrmProjectImplementation2Dlg4(this, e.RowIndex));
                }

                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                ComputeTotalResource();
            }
           
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;
                if (Sm.IsGrdColSelected(new int[] { 0, 1 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                }
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if(e.ColIndex == 0)
                {
                    Sm.FormShowDialog(new FrmProjectImplementation2Dlg3(this));
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //e.DoDefault = false;
                if (Sm.IsGrdColSelected(new int[] { 0, 3 }, e.ColIndex))
                {
                    if (e.ColIndex == 3) Sm.DteRequestEdit(Grd4, DteDocDt2, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                }
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProjectImplementation", "TblProjectImplementationHdr");

            var cml = new List<MySqlCommand>();
            ComputeBobot();
            ComputeBobotPercentage();
            //ComputeEstimatedAmt();

            cml.Add(SaveProjectImplementationHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) 
                    cml.Add(SaveProjectImplementationDtl(DocNo, Row));

            if (mIsUserAllowedToAccessResource)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                        cml.Add(SaveProjectImplementationDtl2(DocNo, Row));
            }

            if (Grd3.Rows.Count >= 1)
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0)
                        cml.Add(SaveProjectImplementationDtl3(DocNo, Row));

            if (Grd4.Rows.Count >= 1)
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                        cml.Add(SaveProjectImplementationDtl4(DocNo, Row));

            Sm.ExecCommands(cml);

             for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0 && Sm.GetGrdStr(Grd1, Row, 14) != "openFileDialog1")
                        {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd1, Row, 14));
                        }
                    }
                }
          

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false) ||
                Sm.IsLueEmpty(LuePPN, "PPN") ||
                Sm.IsLueEmpty(LuePPh, "PPh") ||
                IsSOContractAlreadyProcessed() ||
                IsSOContractCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid("I")||
                IsUploadFileNotValid();
                //IsBobotPercentageNotValid();
        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14) != Sm.GetGrdStr(Grd1, Row, 17))
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0 && Sm.GetGrdStr(Grd1, Row, 14) != "openFileDialog1")
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd1, Row, 14))) return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsSOContractCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSOContractRevisionHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("And (B.CancelInd = 'Y' Or B.Status = 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtSOContractDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO Contract is cancelled.");
                return true;
            }

            return false;
        }

        private bool IsSOContractAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr ");
            SQL.AppendLine("Where SOContractDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('A', 'O') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtSOContractDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO Contract document has already processed to Project Implementation #" + Sm.GetValue(SQL.ToString(), TxtSOContractDocNo.Text));
                TxtSOContractDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsBobotPercentageNotValid()
        {
            ComputeBobotPercentage();

            decimal mPercent = 0m;
            bool mFlag = false;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    mPercent += Sm.GetGrdDec(Grd1, i, 7);
            }

            if (mPercent > 100)
            {
                for (int j = Grd1.Rows.Count-1; j >= 0; j--)
                {
                    if (Sm.GetGrdStr(Grd1, j, 1).Length > 0)
                    {
                        Grd1.Cells[j, 6].Value = Sm.GetGrdDec(Grd1, j, 6) - 0.00000001m;
                        Sm.StdMsg(mMsgType.Info, "Bobot adjusted. Please save again.");
                        mFlag = true;
                        break;
                    }
                }
            }
                
            return mFlag;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 WBS.");
                TcProjectImplementation.SelectTab("TpgWBS");
                return true;
            }

            if (mIsUserAllowedToAccessResource)
            {
                if (Grd2.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Resource.");
                    TcProjectImplementation.SelectTab("TpgResource");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid(string mStateInd)
        {
            //if (mStateInd == "I")
            //{
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 22, false, "No in WBS is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd1, Row, 22)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "No in WBS exceed maximum length character (4).");
                        return true;
                    }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Stage is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Task is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 19, false, "Item is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Plan Start Date is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Plan End Date is empty.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                    if (mIsWBSBobotColumnAutoCompute) if (Sm.IsGrdValueEmpty(Grd1, Row, 6, true, "Price could not be empty or zero.")) { TcProjectImplementation.SelectTab("TpgWBS"); return true; }
                }

                if (!mIsWBSDuplicateAllowed)
                {
                    for (int r1 = 0; r1 < Grd1.Rows.Count - 1; r1++)
                    {
                        for (int r2 = (r1 + 1); r2 < Grd1.Rows.Count; r2++)
                        {
                            if ((Sm.GetGrdStr(Grd1, r1, 0) == Sm.GetGrdStr(Grd1, r2, 0))
                                &&
                                (Sm.GetGrdStr(Grd1, r1, 2) == Sm.GetGrdStr(Grd1, r2, 2))
                                &&
                                (Sm.GetGrdStr(Grd1, r1, 18) == Sm.GetGrdStr(Grd1, r2, 18))
                                &&
                                (Sm.GetGrdStr(Grd1, r1, 22) == Sm.GetGrdStr(Grd1, r2, 22))
                                )
                            {
                                string sSQL = string.Empty;

                                sSQL += "Item  : " + Sm.GetGrdStr(Grd1, r2, 19) + Environment.NewLine;
                                sSQL += "Stage : " + Sm.GetGrdStr(Grd1, r2, 1) + Environment.NewLine;
                                sSQL += "Task  : " + Sm.GetGrdStr(Grd1, r2, 3) + Environment.NewLine;
                                sSQL += "No    : " + Sm.GetGrdStr(Grd1, r2, 22) + Environment.NewLine + Environment.NewLine;
                                sSQL += "Duplicate data found. ";

                                Sm.StdMsg(mMsgType.Warning, sSQL);
                                TcProjectImplementation.SelectTab("TpgWBS");
                                Sm.FocusGrd(Grd1, r2, 19);
                                return true;
                            }
                        }
                    }
                }

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.CompareDtTm(Sm.Left(Sm.GetGrdDate(Grd1, Row, 4), 8), Sm.Left(Sm.GetGrdDate(Grd1, Row, 5), 8)) > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Plan end date could not be earlier than plan start date.");
                        TcProjectImplementation.SelectTab("TpgWBS");
                        Sm.FocusGrd(Grd1, Row, 5);
                        return true;
                    }
                }

                if (mIsUserAllowedToAccessResource)
                {
                    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    {
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Resource is empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 21, false, "Finished Good is empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 5, false, "Quantity 1 could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 6, false, "Quantity 2 could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 7, false, "Unit Price could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 8, false, "Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 9, false, "Total Without Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 10, false, "Total Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                        if (Sm.IsGrdValueEmpty(Grd2, Row, 11, false, "Total With Tax could not be empty.")) { TcProjectImplementation.SelectTab("TpgResource"); return true; }
                    }

                    for (int r1 = 0; r1 < Grd2.Rows.Count - 1; r1++)
                    {
                        for (int r2 = (r1 + 1); r2 < Grd2.Rows.Count; r2++)
                        {
                            if (Sm.GetGrdStr(Grd2, r1, 1) == Sm.GetGrdStr(Grd2, r2, 1) && // resource
                                Sm.GetGrdStr(Grd2, r1, 20) == Sm.GetGrdStr(Grd2, r2, 20) // finished good
                                )
                            {
                                Sm.StdMsg(mMsgType.Warning, "Duplicate resource & finished good found : " + Sm.GetGrdStr(Grd2, r2, 2));
                                TcProjectImplementation.SelectTab("TpgResource");
                                Sm.FocusGrd(Grd2, r2, 2);
                                return true;
                            }
                        }
                    }
                }
            //}


            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd3, Row, 0, false, "Remark is empty.")) { TcProjectImplementation.SelectTab("TpgIssue"); return true; }
                if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "PIC is empty.")) { TcProjectImplementation.SelectTab("TpgIssue"); return true; }
            }

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd4, Row, 2, false, "Document is empty.")) { TcProjectImplementation.SelectTab("TpgDocument"); return true; }
            }

            for (int r1 = 0; r1 < Grd4.Rows.Count - 1; r1++)
            {
                for (int r2 = (r1 + 1); r2 < Grd4.Rows.Count; r2++)
                {
                    if (Sm.GetGrdStr(Grd4, r1, 1) == Sm.GetGrdStr(Grd4, r2, 1))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate document found : " + Sm.GetGrdStr(Grd4, r2, 2));
                        TcProjectImplementation.SelectTab("TpgDocument");
                        Sm.FocusGrd(Grd4, r2, 2);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveProjectImplementationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectImplementationHdr ");
            SQL.AppendLine("(DocNo, DocDt, DocType, Status,ProcessInd, CancelInd, SOContractDocNo, Achievement,  TotalResource, ");
            SQL.AppendLine(" RemunerationAmt, DirectCostAmt, PPNAmt, ExclPPNAmt, PPhAmt, ExclPPNPPhAmt, ExclPPNPPhPercentage, ExclPPNPercentage, TotalPrice, TotalBobot, TotalPersentase, Remark, PPNCode, PPhCode, Number, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @DocType, 'O','D', 'N', @SOContractDocNo, 0, @TotalResource, @RemunerationAmt, ");
            SQL.AppendLine(" @DirectCostAmt, @PPNAmt, @ExclPPNAmt, @PPhAmt, @ExclPPNPPhAmt, @ExclPPNPPhPercentage, @ExclPPNPercentage, @TotalPrice, @TotalBobot, @TotalPersentase, @Remark, @PPNCode, @PPhCode, @Number, @CreateBy, CurrentDateTime()); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='ProjectImplementation' ");
                if (mIsProjectImplementationApprovalBySiteMandatory)
                {
                    SQL.AppendLine("And T.SiteCode Is Not Null ");
                    SQL.AppendLine("And T.SiteCode In ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select SiteCode ");
                    SQL.AppendLine("    From TblLOPHdr ");
                    SQL.AppendLine("    Where DocNo = @LOPDocNo ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("; ");
            }

            SQL.AppendLine("Update TblProjectImplementationHdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'ProjectImplementation' ");
            SQL.AppendLine(");     ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mPRJIDocType);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Number", TxtNumber.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalResource", Sm.GetDecValue(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetDecValue(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetDecValue(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@PPNAmt", Sm.GetDecValue(TxtPPNAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNAmt", Sm.GetDecValue(TxtExclPPN.Text));
            Sm.CmParam<Decimal>(ref cm, "@PPhAmt", Sm.GetDecValue(TxtPPhAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPPhAmt", Sm.GetDecValue(TxtNetCash.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPPhPercentage", Sm.GetDecValue(TxtPPNPPhPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPercentage", Sm.GetDecValue(TxtPPNPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Sm.GetDecValue(TxtTotalPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot", Sm.GetDecValue(TxtTotalBobot.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPersentase", Sm.GetDecValue(TxtTotalPersentase.Text));
            Sm.CmParam<String>(ref cm, "@PPNCode", Sm.GetLue(LuePPN));
            Sm.CmParam<String>(ref cm, "@PPhCode", Sm.GetLue(LuePPh));
            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl(DocNo, DNo, StageCode, TaskCode, ItCode, PlanStartDt, PlanEndDt, ");
            SQLDtl.AppendLine("Amt, Bobot, BobotPercentage, SettledInd, Remark, SettleDt, EstimatedAmt, SettledAmt, SettledBobot, FileName, No, SeqNo, Notes, NotesDetail, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @StageCode, @TaskCode, @ItCode, @PlanStartDt, @PlanEndDt, ");
            SQLDtl.AppendLine("@Amt, @Bobot, @BobotPercentage, @SettledInd, @Remark, @SettleDt, @EstimatedAmt, @SettledAmt, @SettledBobot, @FileName, @No, @SeqNo, @Notes, @NotesDetail, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@SettledInd", Sm.GetGrdBool(Grd1, Row, 9) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@EstimatedAmt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@SettledAmt", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParamDt(ref cm, "@SettleDt", Sm.GetGrdDate(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@SettledBobot", Sm.GetGrdDec(Grd1, Row, 7) - Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 18));
            Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@NotesDetail", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl2(DocNo, DNo, BOQType, BOMDocNo, BOMDNo, ResourceItCode, FGItCode, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, SeqNo, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, null, null, null, @ResourceItCode, @FGItCode, @Remark, @Qty1, @Qty2, @UPrice, @Tax, @TotalWithoutTax, @TotalTax, @Amt, @SeqNo, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ResourceItCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
            //Sm.CmParam<String>(ref cm, "@BOQType", Sm.GetGrdStr(Grd2, Row, 15));
            //Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd2, Row, 17));
            //Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd2, Row, 18));
            Sm.CmParam<String>(ref cm, "@FGItCode", Sm.GetGrdStr(Grd2, Row, 20));
            Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd2, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl3(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl3(DocNo, DNo, Remark, PIC, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @Remark, @PIC, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationDtl4(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationDtl4(DocNo, DNo, DocCode, Dt, Remark, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @DocCode, @Dt, @Remark, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateWBSFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationDtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;
            
            var cml = new List<MySqlCommand>();
            ComputeBobot();
            ComputeBobotPercentage();
            //ComputeEstimatedAmt();

            if (MeeCancelReason.Text.Length <= 0)
            {
                //if (Sm.GetLue(LueProcessInd) == "D")
                //{
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 14) != Sm.GetGrdStr(Grd1, Row, 17))
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0 && Sm.GetGrdStr(Grd1, Row, 14) != "openFileDialog1")
                            {
                                UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd1, Row, 14));
                            }
                        }
                    }
                //}
            }


            cml.Add(EditProjectImplementationHdr());
            if (MeeCancelReason.Text.Length <= 0)
            {
                //if (Sm.GetLue(LueProcessInd) == "D")
                //{
                    
                    cml.Add(DeleteProjectImplementationDtl12());
                    
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                            cml.Add(SaveProjectImplementationDtl(TxtDocNo.Text, Row));

                    if (mIsUserAllowedToAccessResource)
                    {
                        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                                cml.Add(SaveProjectImplementationDtl2(TxtDocNo.Text, Row));
                    }
                //}

                cml.Add(DeleteProjectImplementationDtl34());

                if (Grd3.Rows.Count >= 1)
                    for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0)
                            cml.Add(SaveProjectImplementationDtl3(TxtDocNo.Text, Row));

                if (Grd4.Rows.Count >= 1)
                    for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                            cml.Add(SaveProjectImplementationDtl4(TxtDocNo.Text, Row));

                if (Sm.GetLue(LueProcessInd) == "F")
                {
                    string SOContractDocNo = Sm.GetValue("Select Max(DocNo) From TblSOContractRevisionHdr Where SOCDocNo In (Select T.SOCDocNo From TblSOContractRevisionHdr T Where T.DocNo = @Param); ", TxtSOContractDocNo.Text);

                    TxtSOContractDocNo.EditValue = SOContractDocNo;
                    
                    cml.Add(UpdateLatestSOContractRevision(SOContractDocNo));
                    cml.Add(UpdateAchievement());

                    // save Project Implementation Revision
                    string RevisionDocNo = string.Concat(TxtDocNo.Text, "/", GenerateDocNo());
                    cml.Add(SaveProjectImplementationRevisionHdr(RevisionDocNo));

                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                            cml.Add(SaveProjectImplementationRevisionDtl(RevisionDocNo, Row));

                    if (mIsUserAllowedToAccessResource)
                    {
                        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                                cml.Add(SaveProjectImplementationRevisionDtl2(RevisionDocNo, Row));
                    }

                    if (Grd3.Rows.Count >= 1)
                        for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0)
                                cml.Add(SaveProjectImplementationRevisionDtl3(RevisionDocNo, Row));

                    if (Grd4.Rows.Count >= 1)
                        for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                                cml.Add(SaveProjectImplementationRevisionDtl4(RevisionDocNo, Row));
                }
            }

            if (MeeCancelReason.Text.Length > 0)
            {
                cml.Add(UpdateWBSHdr(TxtDocNo.Text));
            }

            Sm.ExecCommands(cml);
           
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                //IsDataAlreadyFinal() ||
                IsDataCancelledAlready() ||
                IsDataAlreadyInvoiced() ||
                IsGrdValueNotValid("E")||
                IsUploadFileNotValid();
               // IsBobotPercentageNotValid()
                ;
        }

        private bool IsDataAlreadyFinal()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblProjectImplementationHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already Final.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyInvoiced()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblProjectImplementationDtl ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And InvoicedInd = 'Y' ");
            SQL.AppendLine("Limit 1; ");

            if(Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "At least one of this WBS data has been invoiced.");
                TcProjectImplementation.SelectTab("TpgWBS");
                return true;
            }

            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProjectImplementationHdr ");
            SQL.AppendLine("Where (CancelInd='Y' Or Status = 'C') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditProjectImplementationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationHdr Set ");
            SQL.AppendLine(" ProcessInd=@ProcessInd,CancelReason=@CancelReason, CancelInd=@CancelInd, Number=@Number, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(), ");
            SQL.AppendLine(" TotalResource=@TotalResource,TotalPrice=@TotalPrice, TotalBobot=@TotalBobot, TotalPersentase=@TotalPersentase, RemunerationAmt=@RemunerationAmt, ");
            SQL.AppendLine(" DirectCostAmt=@DirectCostAmt, ExclPPNPPhPercentage=@ExclPPNPPhPercentage, ExclPPNPercentage=@ExclPPNPercentage, PPNCode=@PPNCode, PPhCode=@PPhCode ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status <> 'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Number", TxtNumber.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@TotalResource", Sm.GetDecValue(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Sm.GetDecValue(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostAmt", Sm.GetDecValue(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPPhPercentage", Sm.GetDecValue(TxtPPNPPhPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExclPPNPercentage", Sm.GetDecValue(TxtPPNPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPrice", Sm.GetDecValue(TxtTotalPrice.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalBobot", Sm.GetDecValue(TxtTotalBobot.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalPersentase", Sm.GetDecValue(TxtTotalPersentase.Text));
            Sm.CmParam<String>(ref cm, "@PPNCode", Sm.GetLue(LuePPN));
            Sm.CmParam<String>(ref cm, "@PPhCode", Sm.GetLue(LuePPh));

            return cm;
        }

        private MySqlCommand DeleteProjectImplementationDtl34()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblProjectImplementationDtl3 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            SQL.AppendLine("Delete From TblProjectImplementationDtl4 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand DeleteProjectImplementationDtl12()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblProjectImplementationDtl Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            SQL.AppendLine("Delete From TblProjectImplementationDtl2 Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblProjectImplementationHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        private MySqlCommand UpdateWBSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWBSHdr Set ");
            SQL.AppendLine("    PRJIDocNo = null, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where PRJIDocNo = @DocNo ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblProjectImplementationHdr ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'Y' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProjectImplementationRevisionHdr(DocNo, SOCRDocNo, PRJIDocNo, DocDt, Status, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCRDocNo, @PRJIDocNo, @DocDt, 'O', @CreateBy, CurrentDateTime()); ");

            if (IsRevisionNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='ProjectImplementationRev'; ");
            }

            SQL.AppendLine("Update TblProjectImplementationRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ProjectImplementationRev' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo));
            Sm.CmParam<String>(ref cm, "@SOCRDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl(DocNo, DNo, StageCode, TaskCode, PlanStartDt, PlanEndDt, ");
            SQLDtl.AppendLine("Bobot, BobotPercentage, Amt, EstimatedAmt, SettledAmt, SettledInd, SettleDt, InvoicedInd, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @StageCode, @TaskCode, @PlanStartDt, @PlanEndDt, ");
            SQLDtl.AppendLine("@Bobot, @BobotPercentage, @Amt, @EstimatedAmt, @SettledAmt, 'N', NULL, 'N', @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@StageCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@TaskCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@PlanStartDt", Sm.GetGrdDate(Grd1, Row, 4));
            Sm.CmParamDt(ref cm, "@PlanEndDt", Sm.GetGrdDate(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@BobotPercentage", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@EstimatedAmt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@SettledAmt", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl2(DocNo, DNo, ResourceItCode, FGItCode, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @ResourceItCode, @FGItCode, @Remark, @Qty1, @Qty2, @UPrice, @Tax, @TotalWithoutTax, @TotalTax, @Amt, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ResourceItCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<String>(ref cm, "@FGItCode", Sm.GetGrdStr(Grd2, Row, 20));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl3(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl3(DocNo, DNo, Remark, PIC, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @Remark, @PIC, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProjectImplementationRevisionDtl4(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblProjectImplementationRevisionDtl4(DocNo, DNo, DocCode, Dt, Remark, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @DocCode, @Dt, @Remark, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", string.Concat(DocNo));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocCode", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetGrdDate(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd4, Row, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                var cml = new List<MySqlCommand>();

                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowProjectImplementationHdr(DocNo);
                ShowProjectImplementationDtl(DocNo);
                ComputeTotalPercentage();
                if (mIsUserAllowedToAccessResource) ShowProjectImplementationDtl2(DocNo);
                ShowProjectImplementationDtl3(DocNo);
                ShowProjectImplementationDtl4(DocNo);
                cml.Add(UpdateAchievement());
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProjectImplementationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.SOContractDocNo, A.Achievement, A.Remark, ");
            SQL.AppendLine("B.BOQDocNo, C.LOPDocNo, C.CtCode, C.CtContactPersonName, C.SPCode, F.ProjectName, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc, A.ProcessInd, A.TotalResource, ");
            SQL.AppendLine(" A.RemunerationAmt, A.DirectCostAmt, A.PPNAmt, A.ExclPPNAmt, A.PPhAmt, A.ExclPPNPPhAmt, A.ExclPPNPPhPercentage, A.ExclPPNPercentage, ");
            SQL.AppendLine(" E.OptDesc, ( A.RemunerationAmt + A.DirectCostAmt) As Total, ( A.PPNAmt + A.ExclPPNAmt) As ContractAmt, A.TotalPrice, A.TotalBobot, A.TotalPersentase, F.ProjectCode, A.Number, A.PPNCode, A.PPhCode ");  
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A1.SOCDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblLopHdr D On C.LOPDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblOption E On D.ProjectType = E.OptCode And OptCat = 'ProjectType' ");
            SQL.AppendLine("Left Join TblProjectGroup F ON D.PGCode=F.PGCode ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "SOContractDocNo", "Achievement",  
                    
                    //6-10
                    "Remark", "BOQDocNo", "LOPDocNo", "CtCode", "CtContactPersonName",   
                    
                    //11-15
                    "SPCode", "StatusDesc", "ProcessInd", "ProjectName", "TotalResource",

                    //16-20
                    "RemunerationAmt", "DirectCostAmt", "PPNAmt", "ExclPPNAmt", "PPhAmt", 
                    
                    //21-25
                    "ExclPPNPPhAmt", "ExclPPNPPhPercentage", "ExclPPNPercentage", "OptDesc", "Total",

                    //26-30
                    "ContractAmt", "TotalPrice", "TotalBobot", "TotalPersentase", "ProjectCode",

                    //31-33
                    "Number", "PPNCode", "PPhCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtAchievement.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[8]);
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[9]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[11]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[12]);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[13]));
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[14]);
                    TxtTotalResource.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                    TxtRemunerationCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                    TxtDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                    TxtPPNAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                    TxtExclPPN.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                    TxtPPhAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                    TxtNetCash.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                    TxtPPNPPhPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 2);
                    TxtPPNPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 2);
                    TxtType.EditValue = Sm.DrStr(dr, c[24]);
                    TxtTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                    TxtContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[26]), 0);
                    TxtTotalPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                    TxtTotalBobot.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 2);
                    //TxtTotalPersentase.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[29]), 2);
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[30]);
                    TxtNumber.EditValue = Sm.DrStr(dr, c[31]);
                    Sm.SetLue(LuePPN, Sm.DrStr(dr, c[32]));
                    if (Sm.GetLue(LuePPN).Length == 0)
                        Sm.SetLue(LuePPN, Sm.GetParameter("PPNDefaultSetting"));
                    Sm.SetLue(LuePPh, Sm.DrStr(dr, c[33]));
                    if (Sm.GetLue(LuePPh).Length == 0)
                        Sm.SetLue(LuePPh, Sm.GetParameter("PPhDefaultSetting"));
                }, true
            );
           
        }

        private void ShowProjectImplementationDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select A.StageCode, B.StageName, A.TaskCode, C.TaskName, ");
            SQLDtl.AppendLine("A.PlanStartDt, A.PlanEndDt, A.Bobot, A.BobotPercentage, A.SettledInd, A.Remark, A.Amt, ");
            SQLDtl.AppendLine("A.EstimatedAmt, A.SettledAmt, A.SettleDt, A.FileName, A.ItCode, D.ItName, D.ItCodeInternal, ");
            SQLDtl.AppendLine("(A.Bobot - A.SettledBobot) As OutstandingDelivery, A.No, A.SeqNo, A.Notes, A.NotesDetail, E.SOContractRemark, E.SOContractUoM ");
            SQLDtl.AppendLine("From TblProjectImplementationDtl A ");
            SQLDtl.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode And A.DocNo = @DocNo ");
            SQLDtl.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode ");
            SQLDtl.AppendLine("Left Join TblItem D On A.ItCode = D.ItCode ");
            SQLDtl.AppendLine("Left Join ");
            SQLDtl.AppendLine("( ");
            SQLDtl.AppendLine("    Select X1.DocNo, X3.DNo, Group_Concat(Distinct X4.Remark) SOContractRemark, Group_Concat(Distinct X4.PackagingUnitUomCode) SOContractUoM ");
	        SQLDtl.AppendLine("    From TblProjectImplementationHdr X1 ");
	        SQLDtl.AppendLine("    Left Join TblSOContractrevisionHdr X2 On X1.SOContractDocNo = X2.DocNo ");
	        SQLDtl.AppendLine("    Left Join TblProjectImplementationDtl X3 On X1.DocNo = X3.DocNo ");
	        SQLDtl.AppendLine("    Left Join TblSOContractDtl X4 On X3.No = X4.No And X3.ItCode = X4.ItCode ");
	        SQLDtl.AppendLine("    Where X1.DocNo = @DocNo ");
	        SQLDtl.AppendLine("    Group By X3.DocNo, X3.DNo ");
            SQLDtl.AppendLine(") E On A.DocNo = E.DocNo And A.DNo = E.DNo ");
            SQLDtl.AppendLine("Order By Right(Concat('0000000000', IfNull(A.SeqNo, '0')), 10); ");

            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "StageCode", 
                    "StageName", "TaskCode", "TaskName", "PlanStartDt", "PlanEndDt",
                    "Amt","Bobot", "BobotPercentage", "SettledInd", "Remark",
                    "EstimatedAmt", "SettledAmt", "SettleDt", "FileName", "ItCode",
                    "ItName", "ItCodeInternal", "OutstandingDelivery", "No", "SeqNo",
                    "Notes", "NotesDetail", "SOContractRemark", "SOContractUoM"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8, 11, 12, 21 });

        }

        private void ShowProjectImplementationDtl2(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.ResourceItCode, B.ItName,C.ItGrpCode, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt, ");
            SQLDtl2.AppendLine("A.BOQType, G.BOQDocNo, A.BOMDocNo, A.BOMDNo ");
            SQLDtl2.AppendLine(", D.DocNo As RBPDocNo, A.FGItCode, A.SeqNo, H.ItName FGItName ");
            SQLDtl2.AppendLine("From TblProjectImplementationDtl2 A ");
            SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQLDtl2.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
            SQLDtl2.AppendLine("Left Join TblProjectImplementationRBPHdr D On A.DocNo = D.PRJIDocNo And D.ResourceItCode = A.ResourceItCode And D.CancelInd = 'N' ");
            SQLDtl2.AppendLine("Inner Join TblProjectImplementationHdr E On A.DocNo = E.DocNo ");
            SQLDtl2.AppendLine("Inner Join TblSOContractRevisionHdr F On E.SOContractDocNo = F.Docno ");
            SQLDtl2.AppendLine("Inner JOin TblSOContractHdr G On F.SOCDocNo = G.DocNo ");
            SQLDtl2.AppendLine("Left Join TblItem H On A.FGItCode = H.ItCode ");
            SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl2.AppendLine("Order By Right(Concat('0000000000', IfNull(A.SeqNo, '0')), 10); ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm2, SQLDtl2.ToString(),
                new string[] 
                { 
                    "ResourceItCode", 
                    "ItName", "ItGrpName", "Remark", "Qty1", "Qty2", 
                    "UPrice", "Tax", "TotalWithoutTax", "TotalTax", "Amt",
                    "ItGrpCode", "RBPDocNo", "BOQType", "BOQDocNo", "BOMDocNo",
                    "BOMDNo", "FGItCode", "FGItName", "SeqNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
               
        }

        private void ShowProjectImplementationDtl3(string DocNo)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Select Remark, PIC ");
            SQLDtl3.AppendLine("From TblProjectImplementationDtl3 ");
            SQLDtl3.AppendLine("Where DocNo = @DocNo ");
            SQLDtl3.AppendLine("Order By DNo; ");

            var cm3 = new MySqlCommand();
            Sm.CmParam<String>(ref cm3, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd3, ref cm3, SQLDtl3.ToString(),
                new string[] 
                { 
                    "Remark", 
                    "PIC"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowProjectImplementationDtl4(string DocNo)
        {
            var SQLDtl4 = new StringBuilder();

            SQLDtl4.AppendLine("Select A.DocCode, B.DocName, A.Dt, A.Remark ");
            SQLDtl4.AppendLine("From TblProjectImplementationDtl4 A ");
            SQLDtl4.AppendLine("Left Join TblProjectDocument B On A.DocCode = B.DocCode ");
            SQLDtl4.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl4.AppendLine("Order By A.DNo; ");

            var cm4 = new MySqlCommand();
            Sm.CmParam<String>(ref cm4, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd4, ref cm4, SQLDtl4.ToString(),
                new string[] 
                { 
                    "DocCode", 
                    "DocName", "Dt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {       
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #endregion

        #region Events

        #region Button Clicks

        private void BtnImportWBS_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ResultWBS>();

                ClearGrdWBS();
                try
                {
                    ProcessWBS1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessWBS2(ref l);
                        ProcessWBS3(ref l);
                        ProcessWBS4(ref l);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void ProcessWBS1(ref List<ResultWBS> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 5)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        return;
                    }
                    else
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            l.Add(new ResultWBS()
                            {
                                StageCode = splits[0].Trim(),
                                TaskCode = splits[1].Trim(),
                                PlanStartDt = splits[2].Trim(),
                                PlanEndDt = splits[3].Trim(),
                                Bobot = Decimal.Parse(splits[4].Trim().Length > 0 ? splits[4].Trim() : "0"),
                                StageName = string.Empty,
                                TaskName = string.Empty
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessWBS2(ref List<ResultWBS> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var StageCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(StageCode=@StageCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@StageCode0" + i.ToString(), l[i].StageCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select StageCode, StageName From TblProjectStage ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "StageCode", "StageName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        StageCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].StageCode, StageCode))
                                l[i].StageName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS3(ref List<ResultWBS> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var TaskCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(TaskCode=@TaskCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@TaskCode0" + i.ToString(), l[i].TaskCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select TaskCode, TaskName From TblProjectTask ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "TaskCode", "TaskName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        TaskCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].TaskCode, TaskCode))
                                l[i].TaskName = Sm.DrStr(dr, c[1]);
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWBS4(ref List<ResultWBS> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = l[i].StageCode;
                r.Cells[1].Value = l[i].StageName;
                r.Cells[2].Value = l[i].TaskCode;
                r.Cells[3].Value = l[i].TaskName;
                if (l[i].PlanStartDt.Length > 0) r.Cells[4].Value = Sm.ConvertDate(l[i].PlanStartDt);
                if (l[i].PlanEndDt.Length > 0) r.Cells[5].Value = Sm.ConvertDate(l[i].PlanEndDt);
                r.Cells[6].Value = 0m;
                r.Cells[7].Value = l[i].Bobot;
                r.Cells[8].Value = 0m;
                r.Cells[9].Value = false;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6, 7, 8 });
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
            Grd1.EndUpdate();
        }

        private void ProcessResource1(ref List<ResultResource> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;

            using (var rd = new StreamReader(FileName))
            {

                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 6)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        return;
                    }
                    else
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            l.Add(new ResultResource()
                            {
                                ResourceItCode = splits[0].Trim(),
                                Remark = splits[1].Trim(),
                                Qty1 = Decimal.Parse(splits[2].Trim().Length > 0 ? splits[2].Trim() : "0"),
                                Qty2 = Decimal.Parse(splits[3].Trim().Length > 0 ? splits[3].Trim() : "0"),
                                UPrice = Decimal.Parse(splits[4].Trim().Length > 0 ? splits[4].Trim() : "0"),
                                Tax = Decimal.Parse(splits[5].Trim().Length > 0 ? splits[5].Trim() : "0"),
                                ItName = string.Empty,
                                ItGrpName = string.Empty,
                            });
                        }
                        else
                        {
                            Sm.StdMsg(mMsgType.NoData, "");
                            return;
                        }
                    }
                }
            }
        }

        private void ProcessResource2(ref List<ResultResource> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var ItCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.ItCode=@ItCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@ItCode0" + i.ToString(), l[i].ResourceItCode);
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.ItCode, A.ItName, B.ItGrpName ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemGroup B On A.ItGrpCode=B.ItGrpCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "ItName", "ItGrpName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].ResourceItCode, ItCode))
                            {
                                l[i].ItName = Sm.DrStr(dr, c[1]);
                                l[i].ItGrpName = Sm.DrStr(dr, c[2]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessResource3(ref List<ResultResource> l)
        {
            iGRow r;
            Grd2.BeginUpdate();
            Grd2.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd2.Rows.Add();
                r.Cells[1].Value = l[i].ResourceItCode;
                r.Cells[2].Value = l[i].ItName;
                r.Cells[3].Value = l[i].ItGrpName;
                r.Cells[4].Value = l[i].Remark;
                r.Cells[5].Value = l[i].Qty1;
                r.Cells[6].Value = l[i].Qty2;
                r.Cells[7].Value = l[i].UPrice;
                r.Cells[8].Value = l[i].Tax;
                r.Cells[9].Value = l[i].Qty1*l[i].Qty2*l[i].UPrice;
                r.Cells[10].Value = l[i].Qty1*l[i].Qty2*l[i].Tax;
                r.Cells[11].Value = (l[i].Qty1*l[i].Qty2*l[i].UPrice) + (l[i].Qty1*l[i].Qty2*l[i].Tax);
            }
            r = Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });
            Grd2.EndUpdate();
        }

        private void BtnImportResource_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ResultResource>();

                ClearGrdResource();
                TxtTotalResource.EditValue = Sm.FormatNum(0m, 0);
                try
                {
                    ProcessResource1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessResource2(ref l);
                        ProcessResource3(ref l);
                        ComputeTotalResource();
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmProjectImplementation2Dlg(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if(!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                var f = new FrmSOContractRevision2(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmSOContractRevision");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ#", false))
            {
                var f = new FrmBOQ(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmBOQ");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtBOQDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnLOPDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false))
            {
                var f = new FrmLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = Sm.GetMenuDesc("FrmLOP");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtLOPDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueStageCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueStageCode, new Sm.RefreshLue1(Sl.SetLueProjectStageCode));
            }
        }

        private void LueStageCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueStageCode_Leave(object sender, EventArgs e)
        {
            if (LueStageCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueStageCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 0].Value =
                    Grd1.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueStageCode);
                    Grd1.Cells[fCell.RowIndex, 1].Value = LueStageCode.GetColumnValue("Col2");
                }
                LueStageCode.Visible = false;
            }
        }

        private void LueTaskCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaskCode, new Sm.RefreshLue1(Sl.SetLueProjectTaskCode));
            }
        }

        private void LueTaskCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTaskCode_Leave(object sender, EventArgs e)
        {
            if (LueTaskCode.Visible && fAccept && fCell.ColIndex == 3)
            {
                if (Sm.GetLue(LueTaskCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 2].Value =
                    Grd1.Cells[fCell.RowIndex, 3].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 2].Value = Sm.GetLue(LueTaskCode);
                    Grd1.Cells[fCell.RowIndex, 3].Value = LueTaskCode.GetColumnValue("Col2");
                }
                LueTaskCode.Visible = false;
            }
        }

        private void DtePlanStartDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DtePlanStartDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanStartDt, ref fCell, ref fAccept);
        }

        private void DtePlanEndDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DtePlanEndDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DtePlanEndDt, ref fCell, ref fAccept);
        }

        private void DteDocDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd4, ref fAccept, e);
        }

        private void DteDocDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDocDt2, ref fCell, ref fAccept);
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectImplementationProcessInd");
        }

        private void LueItCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueItCode, new Sm.RefreshLue1(SetLueItCode));
            }
        }

        private void LueItCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd1, ref fAccept, e);
            }
        }

        private void LueItCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueItCode.Visible && fAccept && fCell.ColIndex == 19)
                {
                    if (Sm.GetLue(LueItCode).Length == 0)
                    {
                        Grd1.Cells[fCell.RowIndex, 18].Value = null;
                        Grd1.Cells[fCell.RowIndex, 19].Value = null;
                        Grd1.Cells[fCell.RowIndex, 20].Value = null;
                    }
                    else
                    {
                        var ItCode = Sm.GetLue(LueItCode);
                        Grd1.Cells[fCell.RowIndex, 20].Value = null;
                        Grd1.Cells[fCell.RowIndex, 18].Value = ItCode;
                        Grd1.Cells[fCell.RowIndex, 19].Value = LueItCode.GetColumnValue("Col2");
                        Grd1.Cells[fCell.RowIndex, 20].Value = Sm.GetValue("Select ItCodeInternal From TblItem where ItCode=@Param;", ItCode);
                    }
                    LueItCode.Visible = false;
                }
            }
        }

        private void LuePPN_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePPN, new Sm.RefreshLue2(Sl.SetLueOption), "PPNForProject");
                if (Sm.GetLue(LuePPN).Length == 0)
                    Sm.SetLue(LuePPN, Sm.GetParameter("PPNDefaultSetting"));
                ComputeDetailAmt();
            }
        }
        private void LuePPh_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePPh, new Sm.RefreshLue2(Sl.SetLueOption), "PPhForProject");
                if (Sm.GetLue(LuePPh).Length == 0)
                    Sm.SetLue(LuePPh, Sm.GetParameter("PPhDefaultSetting"));
                ComputeDetailAmt();
            }
        }

        #endregion

        #endregion

        #region Class
        private class ResultWBS
        {
            public string StageCode { get; set; }
            public string StageName { get; set; }
            public string TaskCode { get; set; }
            public string TaskName { get; set; }
            public string PlanStartDt { get; set; }
            public string PlanEndDt { get; set; }
            public decimal Bobot { get; set; }
        }

        private class ResultResource
        {
            public string ResourceItCode { get; set; }
            public string ItName { get; set; }
            public string ItGrpName { get; set; }
            public string Remark { get; set; }
            public decimal Qty1 { get; set; }
            public decimal Qty2 { get; set; }
            public decimal UPrice { get; set; }
            public decimal Tax { get; set; }
        }

        private class PRJI
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string Number { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SOCDocNo { get; set; }
            public string PONo { get; set; }
            public string ProjectName { get; set; }
            public string Remark { get; set; }
            public string PositionName { get; set; }
            public string EmployeeName { get; set; }
        }

        private class PRJIDtl
        {
            public string PRJIDocNo { get; set; }
            public string PRJIDNo { get; set; }
            public string No { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Unit { get; set; }
            public string UoM { get; set; }
            public decimal Progress { get; set; }
            public decimal TotalProgress { get; set; }
            public decimal OutstandingDelivery { get; set; }
        }

        private class PRJIDtl2
        {
            public string PRJIDNo { get; set; }
            public string Notes { get; set; }
            public string NotesDetail { get; set; }
            public decimal Unit { get; set; }
            public string UoM { get; set; }
            public decimal Progress { get; set; }
            public decimal TotalProgress { get; set; }
            public decimal OutstandingDelivery { get; set; }
        }

        #endregion   

    }
}
