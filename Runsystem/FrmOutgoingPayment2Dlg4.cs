﻿#region Update
/*
    27/09/2021 [ICA/AMKA] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOutgoingPayment2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmOutgoingPayment2 mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmOutgoingPayment2Dlg4(FrmOutgoingPayment2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Invoice#",
                        "InvoiceDNo",
                        "Item's Code", 
                        "Item's Name",

                        //6-7
                        "Invoice's Amount", 
                        "Outstanding Amount",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 150, 150, 250, 

                        //6-7
                        100, 100, 
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 6, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string InvoiceDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.InvoiceType, B.DNo, C.ItCode, D.ItName, C.Total, (ifnull(C.Total, 0) - ifnull(D.Amt, 0)) AS OutstandingAmt ");
            SQL.AppendLine("FROM TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("INNER JOIN TblPurchaseInvoiceDtl B ON A.DocNo = B.DocNo AND A.CancelInd = 'N' ");
            SQL.AppendLine("INNER JOIN ( ");
            SQL.AppendLine("    SELECT A.DocNo, B.DNo, B.ItCode, ");
            SQL.AppendLine("    ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+((B.QtyPurchase/D.Qty)*D.RoundingValue)) As Total ");
            SQL.AppendLine("    FROM tblRecvVdHdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvVdDtl B ON A.DocNo = B.DocNo And B.CancelInd='N' And IfNull(B.Status, 'O')='A' ");
            SQL.AppendLine("    Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            SQL.AppendLine("    Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            SQL.AppendLine("    Inner Join TblItem I On B.ItCode=I.ItCode ");
            SQL.AppendLine(")C ON B.RecvVdDocNo = C.DocNo AND B.RecvVdDNo = C.DNo ");
            SQL.AppendLine("INNER JOIN tblitem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("    SELECT C.DocNo, D.DNo, SUM(B.Amt) Amt ");
            SQL.AppendLine("    FROM tbloutgoingpaymenthdr A ");
            SQL.AppendLine("    INNER JOIN tbloutgoingpaymentdtl5 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.CancelInd = 'N' ");
            SQL.AppendLine("        AND IFNULL(A.`Status`, 'O') <> 'C' ");
            SQL.AppendLine("    INNER JOIN tblpurchaseinvoicehdr C ON B.InvoiceDocNo = C.DocNo ");
            SQL.AppendLine("        AND C.CancelInd = 'N' ");
            SQL.AppendLine("        AND IFNULL(C.`Status`, 'O') <> 'F' ");
            SQL.AppendLine("    INNER JOIN tblpurchaseinvoicedtl D ON C.DocNo = D.DocNo ");
            SQL.AppendLine("    GROUP BY C.DocNo, D.DNo ");
            SQL.AppendLine(")D ON A.DocNo = D.DocNo AND B.DNo = D.DNo ");
            SQL.AppendLine("Where (" + InvoiceDocNo + ") And Locate(Concat('##', A.DocNo, C.ItCode, '##'), @SelectedInvoiceItem)<1 ");

            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string mInvoiceDocNo = string.Empty;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, "C.ItCode, D.ItName", false);
                Sm.CmParam<String>(ref cm, "@SelectedInvoiceItem", mFrmParent.GetSelectedInvoiceItem());
                for (int row = 0; row < mFrmParent.Grd1.Rows.Count; row++)
                {
                    if (Sm.GetGrdStr(mFrmParent.Grd1, row, 2).Length > 0)
                    {
                        if (mInvoiceDocNo.Length > 0) mInvoiceDocNo += " Or ";
                        mInvoiceDocNo += " (A.DocNo=@DocNo00" + row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo00" + row.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, row, 2));
                    }
                }

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(mInvoiceDocNo) + Filter + " Order By DocNo, DNo;",
                        new string[] 
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DNo", "ItCode", "ItName", "Total", "OutstandingAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd5.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd5, Row1, 8, Grd1, Row2, 7);
                        mFrmParent.Grd5.Rows.Add();
                        mFrmParent.ComputeItemsOutstandingAmt("");
                        Sm.SetGrdNumValueZero(mFrmParent.Grd5, mFrmParent.Grd5.Rows.Count - 1, new int[] { 5, 8 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 purchase invoice or purchase return invoice document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 5);
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key,
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 5)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice3(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                {
                    var f2 = new FrmPurchaseReturnInvoice(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "1"))
                {
                    var f1 = new FrmPurchaseInvoice3(mFrmParent.mMenuCode);
                    f1.Tag = mFrmParent.mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f1.ShowDialog();
                }

                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 5), "2"))
                {
                    var f2 = new FrmPurchaseReturnInvoice(mFrmParent.mMenuCode);
                    f2.Tag = mFrmParent.mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f2.ShowDialog();
                }
            }
        }
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {

        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion

    }
}
