﻿#region Update
/*
    HIN
    02/11/2017 [TKG] ubah perhitungan take home pay dan tax.
    02/12/2017 [TKG] jika work schedule double shift akan menyebakan karyawan mendapatkan tunjangan makan+transport 2x lipat.
    26/12/2017 [TKG] Proses perhitungan pajak.
    09/01/2018 [TKG] perhitungan meal/transport berdasarkan tunjangan per karyawan
    19/01/2018 [TKG] Update proses terlambat
    09/02/2018 [TKG] update proses perhitungan salary dan fixed allowance saat karyawan resign
    09/03/2018 [TKG] tambah allowance annual leave
    15/06/2018 [TKG] tambah indikator Pensiun dan apabila pensiun, nilai salary, dll menjadi 0.
    22/03/2018 [TKG] ubah rumus brutto
    24/03/2018 [TKG] ubah cara proses advance payment
    25/04/2018 [TKG] payrun di-filter berdasarkan site
    18/05/2018 [TKG] setiap kali proses, data yg sudah ada utk payrun tsb dihapus.
    10/07/2018 [TKG] ubah rumus perhitungan tax.
    25/07/2018 [TKG] Bug perhitungan tax allowance menggunakan total imbalan
    07/06/2020 [TKG/HIN] berdasarkan parameter IsGovTaxSubsidiesValidationEnabled dan MaxTHPForGovSubsidies, ditambah validasi tidak perlu bayar payak apabila ada subsidi pemerintah (thp < X rupiah)
    18/06/2020 [TKG/HIN] menambah nontaxable salary adjustment dan taxable salary adjustment, merubah validasi subsidi pemerintah
    24/06/2020 [TKG/HIN] merubah proses perhitungan THP, tax, tax allowance untuk Non HO
    25/08/2020 [TKG/HIN] merubah perhitungan pajak
    22/09/2020 [TKG/HIN] perubahan perhitungan tax+tax allowance
    14/02/2022 [TKG/HIN] perubahan rumus take home pay untuk gaji per tahun kurang dari 200 juta.
    16/02/2022 [TKG/HIN] ubah lagi rumus take home pay untuk gaji per tahun kurang dari 200 juta.
    17/02/2022 [TKG/HIN] ubah rumus take home pay untuk gaji per tahun kurang dari 200 juta untuk kantor pusat
    03/04/2022 [TKG/HIN] ubah GetParameter dan ubah proses save data
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollProcessMultiPayrun3 : Form
    {
        #region Field, Property


        #region Multi Payrun

        private string
            mPayrunCode = string.Empty,
            mStartDt = string.Empty,
            mEndDt = string.Empty;

        #endregion

        private string
            mDeptCode = string.Empty,
            mSystemType = string.Empty,
            mPayrunPeriod = string.Empty,
            mPGCode = string.Empty,
            mSiteCode = string.Empty,
            mOTRequestType = "1",
            mEmpSystemTypeHarian = "1",
            mEmploymentStatusTetap = "1",
            mEmploymentStatusLepas = "2",
            mEmploymentStatusSpesial = "3",
            mPayrunPeriodBulanan = "1",
            mADCodeFieldAssignment = "02",
            mADCodeTransport = "03",
            mADCodeMeal = "05",
            mSSPCodeForHealth = "BPJS-KES",
            mSSPCodeForEmployment = "BPJS-TNK",
            mSSForRetiring = "Pens",
            mSSOldAgeInsurance = "JHT",
            mSalaryInd = "3",
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty,
            mADCodeEmploymentPeriod = string.Empty,
            mADCodeSalary = string.Empty,
            mSSCodePension = string.Empty,
            mSSCodePension2 = string.Empty,
            mSSCodeHealth = string.Empty;

        private decimal
            mDirectLaborDailyWorkHour = 0m,
            mStdHolidayIndex = 2m,
            mWorkingHr = 7m,
            mNoWorkDay2Mingguan = 12,
            mNoWorkDayBulanan = 25,
            mFunctionalExpenses = 5m,
            mFunctionalExpensesMaxAmt = 500000m,
            mOTFormulaHr1 = 1m,
            mOTFormulaHr2 = 24m,
            mOTFormulaIndex1 = 1.5m,
            mOTFormulaIndex2 = 2m,
            mHOWorkingDayPerMth = 25m,
            mSiteWorkingDayPerMth = 21m,
            mHoliday1stHrForHO = 8m,
            mHoliday1stHrForSite = 7m,
            mHoliday2ndHrForHO = 1m,
            mHoliday2ndHrForSite = 1m,
            mHoliday1stHrIndex = 2m,
            mHoliday2ndHrIndex = 3m,
            mHoliday3rdHrIndex = 4m,
            mNonNPWPTaxPercentage = 120m,
            mADCodeEmploymentPeriodYr = 5m,
            mSSEmploymentErPerc = 0m,
            mSSOldAgeEePerc = 0m,
            mSSPensionEePerc = 0m,
            mFieldAssignmentMinWorkDuration = 0m,
            mXMinToleranceLateMealTransportDeduction = 0m,
            mXPercentageLateMealTransportDeduction = 0m,
            mEmployerPercSSHealth = 0m,
            mEmployerPercSSLifeInsurance = 0m,
            mEmployerPercSSWorkingAccident = 0m,
            mEmployeePercSSOldAgeInsurance = 0m,
            mEmployeePercSSForRetiring = 0m,
            mSalaryMaxLimitSSHealth = 0m,
            mSalaryMaxLimitSSLifeInsurance = 0m,
            mSalaryMaxLimitSSWorkingAccident = 0m,
            mSalaryMaxLimitSSOldAgeInsurance = 0m,
            mSalaryMaxLimitSSForRetiring = 0m,
            mMaxTHPForGovSubsidies = 0m
            ;

        private bool
            mIsFilterBySiteHR = false,
            mIsUseLateValidation = false,
            mIsUseOTReplaceLeave = false,
            mIsOTRoutineRounding = false,
            mIsOTBasedOnOTRequest = false,
            mIsWorkingHrBasedOnSchedule = false,
            mIsPayrollProcessUseSiteWorkingDayPerMth = false,
            mHOInd = true,
            mIsOTHolidayDeduct1Hr = false,
            mIsUseLateMealTransportDeduction = false,
            mIsPPProcessAllOutstandingSS = false,
            mIsWSDoubleShiftTwiceAllowance = false,
            mIsGovTaxSubsidiesValidationEnabled  = false;

        private List<PayrunEmployee> mlPayrunEmployee = null;
        private List<AttendanceLog> mlAttendanceLog = null;
        private List<OT> mlOT = null;
        private List<OTAdjustment> mlOTAdjustment = null;
        private List<AdvancePaymentProcess> mlAdvancePaymentProcess = null;
        private List<EmployeeMealTransport> mEmployeeMealTransport = null;
        
        #endregion

        #region Constructor

        public FrmPayrollProcessMultiPayrun3(string MenuCode)
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Setting

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'NoWorkDayBulanan', 'FunctionalExpenses', 'FunctionalExpensesMaxAmt', 'HOWorkingDayPerMth', 'SiteWorkingDayPerMth', ");
            SQL.AppendLine("'NoWorkDay2Mingguan', 'DirectLaborDailyWorkHour', 'ADCodeEmploymentPeriodYr', 'NonNPWPTaxPercentage', 'MaxTHPForGovSubsidies', ");
            SQL.AppendLine("'Holiday2ndHrForSite', 'Holiday1stHrIndex', 'Holiday2ndHrIndex', 'Holiday3rdHrIndex', 'WorkingHr', ");
            SQL.AppendLine("'XMinToleranceLateMealTransportDeduction', 'XPercentageLateMealTransportDeduction', 'Holiday1stHrForHO', 'Holiday1stHrForSite', 'FieldAssignmentMinWorkDuration', ");
            SQL.AppendLine("'SSCodePension', 'SSCodePension2', 'IsUseLateValidation', 'IsWorkingHrBasedOnSchedule', 'Holiday2ndHrForHO', ");
            SQL.AppendLine("'IsUseLateMealTransportDeduction', 'IsPPProcessAllOutstandingSS', 'IsWSDoubleShiftTwiceAllowance', 'IsGovTaxSubsidiesValidationEnabled', 'IsFilterBySiteHR', ");
            SQL.AppendLine("'IsOTHolidayDeduct1Hr', 'IsPayrollProcessUseSiteWorkingDayPerMth', 'IsOTBasedOnOTRequest', 'IsOTRoutineRounding', 'IsUseOTReplaceLeave', ");
            SQL.AppendLine("'SSPCodeForEmployment', 'SSForRetiring', 'SSOldAgeInsurance', 'ADCodeEmploymentPeriod', 'ADCodeSalary', ");
            SQL.AppendLine("'PayrunPeriodBulanan', 'ADCodeTransport', 'ADCodeMeal', 'ADCodeFieldAssignment', 'SSPCodeForHealth', ");
            SQL.AppendLine("'SalaryInd', 'EmpSystemTypeHarian', 'EmploymentStatusTetap', 'EmploymentStatusLepas', 'EmploymentStatusSpesial', ");
            SQL.AppendLine("'SSCodeHealth', 'OTRequestType', 'WSCodesForNationalHoliday', 'WSCodesForHoliday' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //decimal
                            case "Holiday2ndHrForHO": if (ParValue.Length > 0) mHoliday2ndHrForHO = decimal.Parse(ParValue); break;
                            case "Holiday1stHrForSite": if (ParValue.Length > 0) mHoliday1stHrForSite = decimal.Parse(ParValue); break;
                            case "Holiday1stHrForHO": if (ParValue.Length > 0) mHoliday1stHrForHO = decimal.Parse(ParValue); break;
                            case "XPercentageLateMealTransportDeduction": if (ParValue.Length > 0) mXPercentageLateMealTransportDeduction = decimal.Parse(ParValue); break;
                            case "XMinToleranceLateMealTransportDeduction": if (ParValue.Length > 0) mXMinToleranceLateMealTransportDeduction = decimal.Parse(ParValue); break;
                            case "FieldAssignmentMinWorkDuration": if (ParValue.Length > 0) mFieldAssignmentMinWorkDuration = decimal.Parse(ParValue); break;
                            case "Holiday3rdHrIndex": if (ParValue.Length > 0) mHoliday3rdHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrIndex": if (ParValue.Length > 0) mHoliday2ndHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday1stHrIndex": if (ParValue.Length > 0) mHoliday1stHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrForSite": if (ParValue.Length > 0) mHoliday2ndHrForSite = decimal.Parse(ParValue); break;
                            case "WorkingHr": if (ParValue.Length > 0) mWorkingHr = decimal.Parse(ParValue); break;
                            case "NonNPWPTaxPercentage": if (ParValue.Length > 0) mNonNPWPTaxPercentage = decimal.Parse(ParValue); break;
                            case "ADCodeEmploymentPeriodYr": if (ParValue.Length > 0) mADCodeEmploymentPeriodYr = decimal.Parse(ParValue); break;
                            case "DirectLaborDailyWorkHour": if (ParValue.Length > 0) mDirectLaborDailyWorkHour = decimal.Parse(ParValue); break;
                            case "NoWorkDay2Mingguan": if (ParValue.Length > 0) mNoWorkDay2Mingguan = decimal.Parse(ParValue); break;
                            case "MaxTHPForGovSubsidies": if (ParValue.Length > 0) mMaxTHPForGovSubsidies = decimal.Parse(ParValue); break;
                            case "SiteWorkingDayPerMth": if (ParValue.Length > 0) mSiteWorkingDayPerMth = decimal.Parse(ParValue); break;
                            case "HOWorkingDayPerMth": if (ParValue.Length > 0) mHOWorkingDayPerMth = decimal.Parse(ParValue); break;
                            case "FunctionalExpensesMaxAmt": if (ParValue.Length > 0) mFunctionalExpensesMaxAmt = decimal.Parse(ParValue); break;
                            case "FunctionalExpenses": if (ParValue.Length > 0) mFunctionalExpenses = decimal.Parse(ParValue); break;
                            case "NoWorkDayBulanan": if (ParValue.Length > 0) mNoWorkDayBulanan = decimal.Parse(ParValue); break;

                            //string
                            case "WSCodesForHoliday": mWSCodesForHoliday = ParValue; break;
                            case "WSCodesForNationalHoliday": mWSCodesForNationalHoliday = ParValue; break;
                            case "OTRequestType": mOTRequestType = ParValue; break;
                            case "SSCodeHealth": mSSCodeHealth = ParValue; break;
                            case "EmploymentStatusSpesial": mEmploymentStatusSpesial = ParValue; break;
                            case "EmploymentStatusLepas": mEmploymentStatusLepas = ParValue; break;
                            case "EmploymentStatusTetap": mEmploymentStatusTetap = ParValue; break;
                            case "EmpSystemTypeHarian": mEmpSystemTypeHarian = ParValue; break;
                            case "SalaryInd": mSalaryInd = ParValue; break;
                            case "SSPCodeForHealth": mSSPCodeForHealth = ParValue; break;
                            case "ADCodeFieldAssignment": mADCodeFieldAssignment = ParValue; break;
                            case "ADCodeMeal": mADCodeMeal = ParValue; break;
                            case "ADCodeTransport": mADCodeTransport = ParValue; break;
                            case "PayrunPeriodBulanan": mPayrunPeriodBulanan = ParValue; break;
                            case "SSCodePension": mSSCodePension = ParValue; break;
                            case "SSCodePension2": mSSCodePension2 = ParValue; break;
                            case "SSPCodeForEmployment": mSSPCodeForEmployment = ParValue; break;
                            case "SSForRetiring": mSSForRetiring = ParValue; break;
                            case "SSOldAgeInsurance": mSSOldAgeInsurance = ParValue; break;
                            case "ADCodeEmploymentPeriod": mADCodeEmploymentPeriod = ParValue; break;
                            case "ADCodeSalary": mADCodeSalary = ParValue; break;


                            //boolean
                            case "IsOTHolidayDeduct1Hr": mIsOTHolidayDeduct1Hr = ParValue == "Y"; break;
                            case "IsPayrollProcessUseSiteWorkingDayPerMth": mIsPayrollProcessUseSiteWorkingDayPerMth = ParValue == "Y"; break;
                            case "IsOTBasedOnOTRequest": mIsOTBasedOnOTRequest = ParValue == "Y"; break;
                            case "IsOTRoutineRounding": mIsOTRoutineRounding = ParValue == "Y"; break;
                            case "IsUseOTReplaceLeave": mIsUseOTReplaceLeave = ParValue == "Y"; break;

                            case "IsUseLateValidation": mIsUseLateValidation = ParValue == "Y"; break;
                            case "IsWorkingHrBasedOnSchedule": mIsWorkingHrBasedOnSchedule = ParValue == "Y"; break;
                            case "IsUseLateMealTransportDeduction": mIsUseLateMealTransportDeduction = ParValue == "Y"; break;
                            case "IsPPProcessAllOutstandingSS": mIsPPProcessAllOutstandingSS = ParValue == "Y"; break;
                            case "IsWSDoubleShiftTwiceAllowance": mIsWSDoubleShiftTwiceAllowance = ParValue == "Y"; break;
                            case "IsGovTaxSubsidiesValidationEnabled": mIsGovTaxSubsidiesValidationEnabled = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetValue()
        {
            var Value = string.Empty;            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmployerPerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSCodeHealth' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployerPercSSHealth = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployerPerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSLifeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployerPercSSLifeInsurance = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployerPerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSWorkingAccident' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployerPercSSWorkingAccident = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSCodeHealth' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSHealth = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSLifeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSLifeInsurance = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSWorkingAccident' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSWorkingAccident = decimal.Parse(Value);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployeePerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSOldAgeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployeePercSSOldAgeInsurance = decimal.Parse(Value);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployeePerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSForRetiring' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployeePercSSForRetiring = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSOldAgeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSOldAgeInsurance = decimal.Parse(Value);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSForRetiring' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSForRetiring = decimal.Parse(Value);

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Payrun"+Environment.NewLine+"Code",
                        "payrun"+Environment.NewLine+"Name",
                        "Completed",
                        "Start",
                        "End",

                        //6-10
                        "Site Code",
                        "Site",
                        "Department Code",
                        "Department",
                        "System Type Code",

                        //11-15
                        "Type",
                        "Payrun Period Code",
                        "Period",
                        "Group Code",
                        "Group",

                        //16-17
                        "Head Office",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 250, 80, 80, 80, 
                        
                        //6-10
                        0, 200, 0, 200, 0,

                        //11-15
                        150, 0, 150, 0, 150,

                        //16-17
                        0, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0, 3, 16 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 10, 11, 12, 13, 14 }, false);
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 0 });
        }

        //private void SetLuePayrunCode(ref DXE.LookUpEdit Lue)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Distinct PayrunCode As Col1, Concat(PayrunCode, ' : ', PayrunName) As Col2 ");
        //    SQL.AppendLine("From TblPayrun ");
        //    SQL.AppendLine("Where CancelInd='N' ");
        //    SQL.AppendLine("And IfNull(Status, 'O')='O' ");
        //    SQL.AppendLine("Order By Left(PayrunCode, 6) Desc, PayrunName, StartDt;");
        //    Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        #endregion

        #region Show Data

        private void ShowPayrunInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.PayrunCode, A.PayrunName, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.SiteCode, C.SiteName, A.DeptCode, B.DeptName, ");
            SQL.AppendLine("A.SystemType, D.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("A.PayrunPeriod, E.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.PGCode, F.PGName, C.HOInd ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("Left Join TblOption D On A.SystemType=D.OptCode And D.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblOption E On A.PayrunPeriod=E.OptCode And E.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr F On A.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='O' ");
            SQL.AppendLine("And Left(A.PayrunCode, 6)=@YrMth ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (ChkSiteCode.Checked)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null And A.SiteCode=@SiteCode ");
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            }
            SQL.AppendLine("Order By A.PayrunCode;");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "PayrunCode", 

                    //1-5
                    "PayrunName", "StartDt", "EndDt", "SiteCode", "SiteName", 
                    
                    //6-10
                    "DeptCode", "DeptName", "SystemType", "SystemTypeDesc", "PayrunPeriod", 
                    
                    //11-14
                    "PayrunPeriodDesc", "PGCode", "PGName", "HOInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = true;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Grd.Cells[Row, 3].Value = false;
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 16, 14);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 3, 16 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Check Data

        private bool IsProcessedDataNotValid()
        {
            return
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "List of payrun is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsExisted = false;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 0))
                {
                    IsExisted = true;
                    break;
                }
            }
            if (!IsExisted)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 payrun.");
                return true;
            }
            return false;
        }

        #endregion

        #region Save Data

        private MySqlCommand DeletePayrollProcess()
        {
            string EmpCode = string.Empty
                //Filter = string.Empty
                ;
            var cm = new MySqlCommand();

            //for (int i = 0; i < mlPayrunEmployee.Count; i++)
            //{                
            //    if (Filter.Length > 0) Filter += " Or ";
            //    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
            //    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), mlPayrunEmployee[i].EmpCode);   
            //}
            
            //if (Filter.Length > 0)
            //    Filter = " And (" + Filter + ") ";
            //else
            //    Filter = " And 0=1 ";
            

            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess Where PayrunCode=@PayrunCode;");
            SQL.AppendLine("Delete From TblPayrollProcess1 Where PayrunCode=@PayrunCode;") ;
            SQL.AppendLine("Delete From TblPayrollProcess2 Where PayrunCode=@PayrunCode;");
            SQL.AppendLine("Delete From TblPayrollProcessAD ");
            SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode; ");
            SQL.AppendLine("Delete From TblPayrollProcessADOT ");
            SQL.AppendLine("Where IfNull(PayrunCode, '')=@PayrunCode; ");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

            SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=Null ");
            SQL.AppendLine("Where PayrunCode Is Not Null And PayrunCode=@PayrunCode ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select A.DocNo ");
            SQL.AppendLine("    From TblEmpInsPntHdr A ");
            SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine(");");

            SQL.AppendLine("Update TblEmpSCIDtl A ");
            SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Set A.PayrunCode=Null ");
            SQL.AppendLine("Where A.PayrunCode Is Not Null ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode; ");

            SQL.AppendLine("Update TblAnnualLeaveAllowanceDtl A ");
            SQL.AppendLine("Inner Join TblAnnualLeaveAllowanceHdr B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.Status='A' ");
            SQL.AppendLine("Set A.PayrunCode=Null ");
            SQL.AppendLine("Where A.PayrunCode Is Not Null ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode; ");
         
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            return cm;
        }

        private MySqlCommand SavePayrollProcess()
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;
            var cm = new MySqlCommand();

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {                
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), mlPayrunEmployee[i].EmpCode);   
            }
            
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='Y' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ((T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) Or T.PayrunCode Is Null) ");
            SQL.AppendLine("And T.Docno In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblSalaryAdjustment2Hdr ");
            SQL.AppendLine("    Where PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (PayrunCode Is Not Null And PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And DocNo In ( ");
            SQL.AppendLine("    Select A.DocNo ");
            SQL.AppendLine("    From TblEmpInsPntHdr A ");
            SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSCIDtl A ");
            SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Set A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblAnnualLeaveAllowanceDtl A ");
            SQL.AppendLine("Inner Join TblAnnualLeaveAllowanceHdr B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.Status='A' ");
            SQL.AppendLine("Set A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region old code
        //private MySqlCommand SavePayrollProcess1(ref Result2 r)
        //{ 
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Insert Into TblPayrollProcess1 ");
        //    SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, PensionInd, NPWP, ");
        //    SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
        //    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
        //    SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, EmploymentPeriodAllowance, ");
        //    SQL.AppendLine("IncEmployee, IncMinWages, IncProduction, IncPerformance, PresenceReward, ");
        //    SQL.AppendLine("HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, Functional, Housing, MobileCredit, Zakat, ServiceChargeIncentive, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, SSEmployerPension2, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
        //    SQL.AppendLine("DedEmployee, DedProduction, DedProdLeave, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployeePension2, ");
        //    SQL.AppendLine("SalaryPension, SSErLifeInsurance, SSEeLifeInsurance, SSErWorkingAccident, SSEeWorkingAccident, SSErRetirement, SSEeRetirement, SSErPension, SSEePension, ");
        //    SQL.AppendLine("CreditCode1, CreditCode2, CreditCode3, CreditCode4, CreditCode5, CreditCode6, CreditCode7, CreditCode8, CreditCode9, CreditCode10, ");
        //    SQL.AppendLine("CreditAdvancePayment1, CreditAdvancePayment2, CreditAdvancePayment3, CreditAdvancePayment4, CreditAdvancePayment5, CreditAdvancePayment6, CreditAdvancePayment7, CreditAdvancePayment8, CreditAdvancePayment9, CreditAdvancePayment10, ");
        //    SQL.AppendLine("ADLeave, SalaryAdjustment, Tax, TaxAllowance, Amt, NontaxableSalaryAdjustment, TaxableSalaryAdjustment, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@PayrunCode, @EmpCode, @JoinDt, @ResignDt, @PensionInd, @NPWP, ");
        //    SQL.AppendLine("@PTKP, @Salary, @WorkingDay, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
	       // SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, ");
	       // SQL.AppendLine("@OT1Amt, @OT2Amt, @OTHolidayAmt, @NonTaxableFixAllowance, @TaxableFixAllowance, @FixAllowance, @EmploymentPeriodAllowance, ");
	       // SQL.AppendLine("@IncEmployee, @IncMinWages, @IncProduction, @IncPerformance, @PresenceReward, ");
        //    SQL.AppendLine("@HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @Functional, @Housing, @MobileCredit, @Zakat, @ServiceChargeIncentive, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @SSEmployerPension2, @NonTaxableFixDeduction, @TaxableFixDeduction, @FixDeduction, ");
        //    SQL.AppendLine("@DedEmployee, @DedProduction, @DedProdLeave, @EmpAdvancePayment, @SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, @SSEmployeePension2, ");
        //    SQL.AppendLine("@SalaryPension, @SSErLifeInsurance, @SSEeLifeInsurance, @SSErWorkingAccident, @SSEeWorkingAccident, @SSErRetirement, @SSEeRetirement, @SSErPension, @SSEePension, ");
        //    SQL.AppendLine("@CreditCode1, @CreditCode2, @CreditCode3, @CreditCode4, @CreditCode5, @CreditCode6, @CreditCode7, @CreditCode8, @CreditCode9, @CreditCode10, ");
        //    SQL.AppendLine("@CreditAdvancePayment1, @CreditAdvancePayment2, @CreditAdvancePayment3, @CreditAdvancePayment4, @CreditAdvancePayment5, @CreditAdvancePayment6, @CreditAdvancePayment7, @CreditAdvancePayment8, @CreditAdvancePayment9, @CreditAdvancePayment10, ");
        //    SQL.AppendLine("@ADLeave, @SalaryAdjustment, @Tax, @TaxAllowance, @Amt, @NontaxableSalaryAdjustment, @TaxableSalaryAdjustment, @CreateBy, CurrentDateTime()); ");

        //    cm.CommandText = SQL.ToString();
        //    Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
        //    Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
        //    Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
        //    Sm.CmParam<String>(ref cm, "@PensionInd", r.PensionInd?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@NPWP", r.NPWP);
        //    Sm.CmParam<String>(ref cm, "@PTKP", r.PTKP);
	       // Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
	       // Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
	       // Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
	       // Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
	       // Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
	       // Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
	       // Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
	       // Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
	       // Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
	       // Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
	       // Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
	       // Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance", r.NonTaxableFixAllowance);
	       // Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance", r.TaxableFixAllowance);
        //    Sm.CmParam<Decimal>(ref cm, "@FixAllowance", r.FixAllowance);
	       // Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance", r.EmploymentPeriodAllowance);
	       // Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
	       // Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
	       // Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
	       // Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
	       // Sm.CmParam<Decimal>(ref cm, "@PresenceReward", r.PresenceReward);
	       // Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
	       // Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
        //    Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
        //    Sm.CmParam<Decimal>(ref cm, "@Transport", r.Transport);
        //    Sm.CmParam<Decimal>(ref cm, "@Meal", r.Meal);
        //    Sm.CmParam<Decimal>(ref cm, "@Functional", r.Functional);
        //    Sm.CmParam<Decimal>(ref cm, "@Housing", r.Housing);
        //    Sm.CmParam<Decimal>(ref cm, "@MobileCredit", r.MobileCredit);
        //    Sm.CmParam<Decimal>(ref cm, "@Zakat", r.Zakat);
	       // Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", r.SSEmployerHealth);
	       // Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", r.SSEmployerEmployment);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", r.SSEmployerPension);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2", r.SSEmployerPension2);
        //    Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction", r.NonTaxableFixDeduction);
        //    Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction", r.TaxableFixDeduction);
	       // Sm.CmParam<Decimal>(ref cm, "@FixDeduction", r.FixDeduction);
	       // Sm.CmParam<Decimal>(ref cm, "@DedEmployee", r.DedEmployee);
	       // Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
	       // Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
        //    Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment", r.EmpAdvancePayment);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", r.SSEmployeeHealth);
	       // Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", r.SSEmployeeEmployment);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", r.SSEmployeePension);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2", r.SSEmployeePension2);
        //    Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
        //    Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance", r.SSErLifeInsurance);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance", r.SSEeLifeInsurance);
        //    Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident", r.SSErWorkingAccident);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident", r.SSEeWorkingAccident);
        //    Sm.CmParam<Decimal>(ref cm, "@SSErRetirement", r.SSErRetirement);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement", r.SSEeRetirement);
        //    Sm.CmParam<Decimal>(ref cm, "@SSErPension", r.SSErPension);
        //    Sm.CmParam<Decimal>(ref cm, "@SSEePension", r.SSEePension);
        //    Sm.CmParam<Decimal>(ref cm, "@ADLeave", r.ADLeave);
	       // Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment", r.SalaryAdjustment);
        //    Sm.CmParam<String>(ref cm, "@CreditCode1", r.CreditCode1);
        //    Sm.CmParam<String>(ref cm, "@CreditCode2", r.CreditCode2);
        //    Sm.CmParam<String>(ref cm, "@CreditCode3", r.CreditCode3);
        //    Sm.CmParam<String>(ref cm, "@CreditCode4", r.CreditCode4);
        //    Sm.CmParam<String>(ref cm, "@CreditCode5", r.CreditCode5);
        //    Sm.CmParam<String>(ref cm, "@CreditCode6", r.CreditCode6);
        //    Sm.CmParam<String>(ref cm, "@CreditCode7", r.CreditCode7);
        //    Sm.CmParam<String>(ref cm, "@CreditCode8", r.CreditCode8);
        //    Sm.CmParam<String>(ref cm, "@CreditCode9", r.CreditCode9);
        //    Sm.CmParam<String>(ref cm, "@CreditCode10", r.CreditCode10);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1", r.CreditAdvancePayment1);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2", r.CreditAdvancePayment2);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3", r.CreditAdvancePayment3);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4", r.CreditAdvancePayment4);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5", r.CreditAdvancePayment5);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6", r.CreditAdvancePayment6);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7", r.CreditAdvancePayment7);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8", r.CreditAdvancePayment8);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9", r.CreditAdvancePayment9);
        //    Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10", r.CreditAdvancePayment10);
	       // Sm.CmParam<Decimal>(ref cm, "@Tax", r.Tax);
        //    Sm.CmParam<Decimal>(ref cm, "@TaxAllowance", r.TaxAllowance);
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", r.Amt);
        //    Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
        //    Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
        //    Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
        //    Sm.CmParam<Decimal>(ref cm, "@NontaxableSalaryAdjustment", r.NontaxableSalaryAdjustment);
        //    Sm.CmParam<Decimal>(ref cm, "@TaxableSalaryAdjustment", r.TaxableSalaryAdjustment);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SavePayrollProcess2(ref Result1 r)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPayrollProcess2 ");
        //    SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
        //    SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
        //    SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
        //    SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
        //    SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
        //    SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
        //    SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
        //    SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
        //    SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
        //    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
        //    SQL.AppendLine("OT2Amt, OTHolidayAmt, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
        //    SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
        //    SQL.AppendLine("DedProdLeave, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values( ");
        //    SQL.AppendLine("@PayrunCode, @EmpCode, @Dt, @ProcessInd, @LatestPaidDt, ");
        //    SQL.AppendLine("@JoinDt, @ResignDt, @PosCode, @DeptCode, @GrdLvlCode, @LevelCode, ");
        //    SQL.AppendLine("@SystemType, @EmploymentStatus, @PayrunPeriod, @PGCode, @SiteCode, @WorkingDay, ");
        //    SQL.AppendLine("@WSCode, @HolInd, @HolidayIndex, @WSHolidayInd, @WSIn1, ");
        //    SQL.AppendLine("@WSOut1, @WSIn2, @WSOut2, @WSIn3, @WSOut3, ");
        //    SQL.AppendLine("@OneDayInd, @LateInd, @ActualIn, @ActualOut, @WorkingIn, ");
        //    SQL.AppendLine("@WorkingOut, @WorkingDuration, @EmpSalary, @EmpSalary2, @BasicSalary, @BasicSalary2, @SalaryPension, @ProductionWages, ");
        //    SQL.AppendLine("@Salary, @LeaveCode, @LeaveType, @PaidLeaveInd, @LeaveStartTm,");
        //    SQL.AppendLine("@LeaveEndTm, @LeaveDuration, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
        //    SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, @OT1Amt, ");
        //    SQL.AppendLine("@OT2Amt, @OTHolidayAmt, @OTToLeaveInd, @IncMinWages, @IncProduction, @IncEmployee, ");
        //    SQL.AppendLine("@IncPerformance, @PresenceRewardInd, @HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @ServiceChargeIncentive, @DedProduction, ");
        //    SQL.AppendLine("@DedProdLeave, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
        //    Sm.CmParam<String>(ref cm, "@Dt", r.Dt);
        //    Sm.CmParam<String>(ref cm, "@ProcessInd", r.ProcessInd?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@LatestPaidDt", r.LatestPaidDt);
        //    Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
        //    Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
        //    Sm.CmParam<String>(ref cm, "@PosCode", r.PosCode);
        //    Sm.CmParam<String>(ref cm, "@DeptCode", r.DeptCode);
        //    Sm.CmParam<String>(ref cm, "@GrdLvlCode", r.GrdLvlCode);
        //    Sm.CmParam<String>(ref cm, "@LevelCode", r.LevelCode);
        //    Sm.CmParam<String>(ref cm, "@SystemType", r.SystemType);
        //    Sm.CmParam<String>(ref cm, "@EmploymentStatus", r.EmploymentStatus);
        //    Sm.CmParam<String>(ref cm, "@PayrunPeriod", r.PayrunPeriod);
        //    Sm.CmParam<String>(ref cm, "@PGCode", r.PGCode);
        //    Sm.CmParam<String>(ref cm, "@SiteCode", r.SiteCode);
        //    Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
        //    Sm.CmParam<String>(ref cm, "@WSCode", r.WSCode);
        //    Sm.CmParam<String>(ref cm, "@HolInd", r.HolInd?"Y":"N");
        //    Sm.CmParam<Decimal>(ref cm, "@HolidayIndex", r.HolidayIndex);
        //    Sm.CmParam<String>(ref cm, "@WSHolidayInd", r.WSHolidayInd?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@WSIn1", r.WSIn1);
        //    Sm.CmParam<String>(ref cm, "@WSOut1", r.WSOut1);
        //    Sm.CmParam<String>(ref cm, "@WSIn2", r.WSIn2);
        //    Sm.CmParam<String>(ref cm, "@WSOut2", r.WSOut2);
        //    Sm.CmParam<String>(ref cm, "@WSIn3", r.WSIn3);
        //    Sm.CmParam<String>(ref cm, "@WSOut3", r.WSOut3);
        //    Sm.CmParam<String>(ref cm, "@OneDayInd", r.OneDayInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@LateInd", r.LateInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@ActualIn", r.ActualIn);
        //    Sm.CmParam<String>(ref cm, "@ActualOut", r.ActualOut);
        //    Sm.CmParam<String>(ref cm, "@WorkingIn", r.WorkingIn);
        //    Sm.CmParam<String>(ref cm, "@WorkingOut", r.WorkingOut);
        //    Sm.CmParam<Decimal>(ref cm, "@WorkingDuration", r.WorkingDuration);
        //    Sm.CmParam<Decimal>(ref cm, "@EmpSalary", r.EmpSalary);
        //    Sm.CmParam<Decimal>(ref cm, "@EmpSalary2", r.EmpSalary2);
        //    Sm.CmParam<Decimal>(ref cm, "@BasicSalary", r.BasicSalary);
        //    Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", r.BasicSalary2);
        //    Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
        //    Sm.CmParam<Decimal>(ref cm, "@ProductionWages", r.ProductionWages);
        //    Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
        //    Sm.CmParam<String>(ref cm, "@LeaveCode", r.LeaveCode);
        //    Sm.CmParam<String>(ref cm, "@LeaveType", r.LeaveType);
        //    Sm.CmParam<String>(ref cm, "@PaidLeaveInd", r.PaidLeaveInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@LeaveStartTm", r.LeaveStartTm);
        //    Sm.CmParam<String>(ref cm, "@LeaveEndTm", r.LeaveEndTm);
        //    Sm.CmParam<Decimal>(ref cm, "@LeaveDuration", r.LeaveDuration);
        //    Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
        //    Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
        //    Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
        //    Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
        //    Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
        //    Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
        //    Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
        //    Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
        //    Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
        //    Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
        //    Sm.CmParam<String>(ref cm, "@OTToLeaveInd", r.OTToLeaveInd ? "Y" : "N");
        //    Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
        //    Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
        //    Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
        //    Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
        //    Sm.CmParam<String>(ref cm, "@PresenceRewardInd", r.PresenceRewardInd ? "Y" : "N");
        //    Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
        //    Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
        //    Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
        //    Sm.CmParam<Decimal>(ref cm, "Transport", r.Transport);
        //    Sm.CmParam<Decimal>(ref cm, "Meal", r.Meal);
        //    Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
        //    Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
        //    Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);     
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveAdvancePaymentProcess(ref AdvancePaymentProcess x)
        //{
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();

        //    SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
        //    SQL.AppendLine("Where DocNo=@DocNo And Yr=@Yr And Mth=@Mth; ");

        //    SQL.AppendLine("Insert Into TblAdvancePaymentProcess ");
        //    SQL.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @Yr, @Mth, @EmpCode, @Amt, @PayrunCode, @CreateBy, CurrentDateTime()); ");

        //    cm.CommandText = SQL.ToString();
        //    Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
        //    Sm.CmParam<String>(ref cm, "DocNo", x.DocNo);
        //    Sm.CmParam<String>(ref cm, "Yr", x.Yr);
        //    Sm.CmParam<String>(ref cm, "Mth", x.Mth);
        //    Sm.CmParam<String>(ref cm, "EmpCode", x.EmpCode);
        //    Sm.CmParam<Decimal>(ref cm, "Amt", x.Amt);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            int r = 0;

            SQL.AppendLine("/* Payroll Processing (Advance Payment Processs) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            foreach (var x in l)
            {
                if (IsFirstOrExisted)
                {
                    SQL2.AppendLine("Delete From TblAdvancePaymentProcess ");
                    SQL2.AppendLine("Where ");

                    SQL.AppendLine("Insert Into TblAdvancePaymentProcess(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");

                    IsFirstOrExisted = false;
                }
                else
                {
                    SQL.AppendLine(", ");
                    SQL2.AppendLine(" Or ");
                }

                SQL2.AppendLine(" (DocNo=@DocNo_" + r.ToString() + " And Yr=@Yr_" + r.ToString() + " And Mth=@Mth_" + r.ToString() + ") ");

                SQL.AppendLine(
                    " (@DocNo_" + r.ToString() +
                    ", @Yr_" + r.ToString() +
                    ", @Mth_" + r.ToString() +
                    ", @EmpCode_" + r.ToString() +
                    ", @Amt_" + r.ToString() +
                    ", @PayrunCode, @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "DocNo_" + r.ToString(), x.DocNo);
                Sm.CmParam<String>(ref cm, "Yr_" + r.ToString(), x.Yr);
                Sm.CmParam<String>(ref cm, "Mth_" + r.ToString(), x.Mth);
                Sm.CmParam<String>(ref cm, "EmpCode_" + r.ToString(), x.EmpCode);
                Sm.CmParam<Decimal>(ref cm, "Amt_" + r.ToString(), x.Amt);

                r++;
            }
            if (!IsFirstOrExisted)
            {
                SQL.AppendLine("; ");
                SQL2.AppendLine("; ");
            }
                

            cm.CommandText = SQL2.ToString() + SQL.ToString();

            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePayrollProcess1(ref List<Result2> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int r = 0;
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Payroll Processing (PayrollProcess1) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            foreach (var x in l)
            {
                if (IsFirstOrExisted)
                {
                    SQL.AppendLine("Insert Into TblPayrollProcess1 ");
                    SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, PensionInd, NPWP, ");
                    SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
                    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
                    SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, EmploymentPeriodAllowance, ");
                    SQL.AppendLine("IncEmployee, IncMinWages, IncProduction, IncPerformance, PresenceReward, ");
                    SQL.AppendLine("HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, Functional, Housing, MobileCredit, Zakat, ServiceChargeIncentive, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, SSEmployerPension2, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
                    SQL.AppendLine("DedEmployee, DedProduction, DedProdLeave, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployeePension2, ");
                    SQL.AppendLine("SalaryPension, SSErLifeInsurance, SSEeLifeInsurance, SSErWorkingAccident, SSEeWorkingAccident, SSErRetirement, SSEeRetirement, SSErPension, SSEePension, ");
                    SQL.AppendLine("CreditCode1, CreditCode2, CreditCode3, CreditCode4, CreditCode5, CreditCode6, CreditCode7, CreditCode8, CreditCode9, CreditCode10, ");
                    SQL.AppendLine("CreditAdvancePayment1, CreditAdvancePayment2, CreditAdvancePayment3, CreditAdvancePayment4, CreditAdvancePayment5, CreditAdvancePayment6, CreditAdvancePayment7, CreditAdvancePayment8, CreditAdvancePayment9, CreditAdvancePayment10, ");
                    SQL.AppendLine("ADLeave, SalaryAdjustment, Tax, TaxAllowance, Amt, NontaxableSalaryAdjustment, TaxableSalaryAdjustment, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");
                    IsFirstOrExisted = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine(
                    " (@PayrunCode_" + r.ToString() +
                    ", @EmpCode_" + r.ToString() +
                    ", @JoinDt_" + r.ToString() +
                    ", @ResignDt_" + r.ToString() +
                    ", @PensionInd_" + r.ToString() +
                    ", @NPWP_" + r.ToString() +
                    ", @PTKP_" + r.ToString() +
                    ", @Salary_" + r.ToString() +
                    ", @WorkingDay_" + r.ToString() +
                    ", @PLDay_" + r.ToString() +
                    ", @PLHr_" + r.ToString() +
                    ", @PLAmt_" + r.ToString() +
                    ", @ProcessPLAmt_" + r.ToString() +
                    ", @UPLDay_" + r.ToString() +
                    ", @UPLHr_" + r.ToString() +
                    ", @UPLAmt_" + r.ToString() +
                    ", @ProcessUPLAmt_" + r.ToString() +
                    ", @OT1Hr_" + r.ToString() +
                    ", @OT2Hr_" + r.ToString() +
                    ", @OTHolidayHr_" + r.ToString() +
                    ", @OT1Amt_" + r.ToString() +
                    ", @OT2Amt_" + r.ToString() +
                    ", @OTHolidayAmt_" + r.ToString() +
                    ", @NonTaxableFixAllowance_" + r.ToString() +
                    ", @TaxableFixAllowance_" + r.ToString() +
                    ", @FixAllowance_" + r.ToString() +
                    ", @EmploymentPeriodAllowance_" + r.ToString() +
                    ", @IncEmployee_" + r.ToString() +
                    ", @IncMinWages_" + r.ToString() +
                    ", @IncProduction_" + r.ToString() +
                    ", @IncPerformance_" + r.ToString() +
                    ", @PresenceReward_" + r.ToString() +
                    ", @HolidayEarning_" + r.ToString() +
                    ", @ExtraFooding_" + r.ToString() +
                    ", @FieldAssignment_" + r.ToString() +
                    ", @Transport_" + r.ToString() +
                    ", @Meal_" + r.ToString() +
                    ", @Functional_" + r.ToString() +
                    ", @Housing_" + r.ToString() +
                    ", @MobileCredit_" + r.ToString() +
                    ", @Zakat_" + r.ToString() +
                    ", @ServiceChargeIncentive_" + r.ToString() +
                    ", @SSEmployerHealth_" + r.ToString() +
                    ", @SSEmployerEmployment_" + r.ToString() +
                    ", @SSEmployerPension_" + r.ToString() +
                    ", @SSEmployerPension2_" + r.ToString() +
                    ", @NonTaxableFixDeduction_" + r.ToString() +
                    ", @TaxableFixDeduction_" + r.ToString() +
                    ", @FixDeduction_" + r.ToString() +
                    ", @DedEmployee_" + r.ToString() +
                    ", @DedProduction_" + r.ToString() +
                    ", @DedProdLeave_" + r.ToString() +
                    ", @EmpAdvancePayment_" + r.ToString() +
                    ", @SSEmployeeHealth_" + r.ToString() +
                    ", @SSEmployeeEmployment_" + r.ToString() +
                    ", @SSEmployeePension_" + r.ToString() +
                    ", @SSEmployeePension2_" + r.ToString() +
                    ", @SalaryPension_" + r.ToString() +
                    ", @SSErLifeInsurance_" + r.ToString() +
                    ", @SSEeLifeInsurance_" + r.ToString() +
                    ", @SSErWorkingAccident_" + r.ToString() +
                    ", @SSEeWorkingAccident_" + r.ToString() +
                    ", @SSErRetirement_" + r.ToString() +
                    ", @SSEeRetirement_" + r.ToString() +
                    ", @SSErPension_" + r.ToString() +
                    ", @SSEePension_" + r.ToString() +
                    ", @CreditCode1_" + r.ToString() +
                    ", @CreditCode2_" + r.ToString() +
                    ", @CreditCode3_" + r.ToString() +
                    ", @CreditCode4_" + r.ToString() +
                    ", @CreditCode5_" + r.ToString() +
                    ", @CreditCode6_" + r.ToString() +
                    ", @CreditCode7_" + r.ToString() +
                    ", @CreditCode8_" + r.ToString() +
                    ", @CreditCode9_" + r.ToString() +
                    ", @CreditCode10_" + r.ToString() +
                    ", @CreditAdvancePayment1_" + r.ToString() +
                    ", @CreditAdvancePayment2_" + r.ToString() +
                    ", @CreditAdvancePayment3_" + r.ToString() +
                    ", @CreditAdvancePayment4_" + r.ToString() +
                    ", @CreditAdvancePayment5_" + r.ToString() +
                    ", @CreditAdvancePayment6_" + r.ToString() +
                    ", @CreditAdvancePayment7_" + r.ToString() +
                    ", @CreditAdvancePayment8_" + r.ToString() +
                    ", @CreditAdvancePayment9_" + r.ToString() +
                    ", @CreditAdvancePayment10_" + r.ToString() +
                    ", @ADLeave_" + r.ToString() +
                    ", @SalaryAdjustment_" + r.ToString() +
                    ", @Tax_" + r.ToString() +
                    ", @TaxAllowance_" + r.ToString() +
                    ", @Amt_" + r.ToString() +
                    ", @NontaxableSalaryAdjustment_" + r.ToString() +
                    ", @TaxableSalaryAdjustment_" + r.ToString() +
                    ", @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "@PayrunCode_"+r.ToString(), x.PayrunCode);
                Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), x.EmpCode);
                Sm.CmParam<String>(ref cm, "@JoinDt_" + r.ToString(), x.JoinDt);
                Sm.CmParam<String>(ref cm, "@ResignDt_" + r.ToString(), x.ResignDt);
                Sm.CmParam<String>(ref cm, "@PensionInd_" + r.ToString(), x.PensionInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@NPWP_" + r.ToString(), x.NPWP);
                Sm.CmParam<String>(ref cm, "@PTKP_" + r.ToString(), x.PTKP);
                Sm.CmParam<Decimal>(ref cm, "@Salary_" + r.ToString(), x.Salary);
                Sm.CmParam<Decimal>(ref cm, "@WorkingDay_" + r.ToString(), x.WorkingDay);
                Sm.CmParam<Decimal>(ref cm, "@PLDay_" + r.ToString(), x.PLDay);
                Sm.CmParam<Decimal>(ref cm, "@PLHr_" + r.ToString(), x.PLHr);
                Sm.CmParam<Decimal>(ref cm, "@PLAmt_" + r.ToString(), x.PLAmt);
                Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt_" + r.ToString(), x.ProcessPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@UPLDay_" + r.ToString(), x.UPLDay);
                Sm.CmParam<Decimal>(ref cm, "@UPLHr_" + r.ToString(), x.UPLHr);
                Sm.CmParam<Decimal>(ref cm, "@UPLAmt_" + r.ToString(), x.UPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt_" + r.ToString(), x.ProcessUPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@OT1Hr_" + r.ToString(), x.OT1Hr);
                Sm.CmParam<Decimal>(ref cm, "@OT2Hr_" + r.ToString(), x.OT2Hr);
                Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr_" + r.ToString(), x.OTHolidayHr);
                Sm.CmParam<Decimal>(ref cm, "@OT1Amt_" + r.ToString(), x.OT1Amt);
                Sm.CmParam<Decimal>(ref cm, "@OT2Amt_" + r.ToString(), x.OT2Amt);
                Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt_" + r.ToString(), x.OTHolidayAmt);
                Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance_" + r.ToString(), x.NonTaxableFixAllowance);
                Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance_" + r.ToString(), x.TaxableFixAllowance);
                Sm.CmParam<Decimal>(ref cm, "@FixAllowance_" + r.ToString(), x.FixAllowance);
                Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance_" + r.ToString(), x.EmploymentPeriodAllowance);
                Sm.CmParam<Decimal>(ref cm, "@IncEmployee_" + r.ToString(), x.IncEmployee);
                Sm.CmParam<Decimal>(ref cm, "@IncMinWages_" + r.ToString(), x.IncMinWages);
                Sm.CmParam<Decimal>(ref cm, "@IncProduction_" + r.ToString(), x.IncProduction);
                Sm.CmParam<Decimal>(ref cm, "@IncPerformance_" + r.ToString(), x.IncPerformance);
                Sm.CmParam<Decimal>(ref cm, "@PresenceReward_" + r.ToString(), x.PresenceReward);
                Sm.CmParam<Decimal>(ref cm, "@HolidayEarning_" + r.ToString(), x.HolidayEarning);
                Sm.CmParam<Decimal>(ref cm, "@ExtraFooding_" + r.ToString(), x.ExtraFooding);
                Sm.CmParam<Decimal>(ref cm, "@FieldAssignment_" + r.ToString(), x.FieldAssignment);
                Sm.CmParam<Decimal>(ref cm, "@Transport_" + r.ToString(), x.Transport);
                Sm.CmParam<Decimal>(ref cm, "@Meal_" + r.ToString(), x.Meal);
                Sm.CmParam<Decimal>(ref cm, "@Functional_" + r.ToString(), x.Functional);
                Sm.CmParam<Decimal>(ref cm, "@Housing_" + r.ToString(), x.Housing);
                Sm.CmParam<Decimal>(ref cm, "@MobileCredit_" + r.ToString(), x.MobileCredit);
                Sm.CmParam<Decimal>(ref cm, "@Zakat_" + r.ToString(), x.Zakat);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth_" + r.ToString(), x.SSEmployerHealth);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment_" + r.ToString(), x.SSEmployerEmployment);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension_" + r.ToString(), x.SSEmployerPension);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2_" + r.ToString(), x.SSEmployerPension2);
                Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction_" + r.ToString(), x.NonTaxableFixDeduction);
                Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction_" + r.ToString(), x.TaxableFixDeduction);
                Sm.CmParam<Decimal>(ref cm, "@FixDeduction_" + r.ToString(), x.FixDeduction);
                Sm.CmParam<Decimal>(ref cm, "@DedEmployee_" + r.ToString(), x.DedEmployee);
                Sm.CmParam<Decimal>(ref cm, "@DedProduction_" + r.ToString(), x.DedProduction);
                Sm.CmParam<Decimal>(ref cm, "@DedProdLeave_" + r.ToString(), x.DedProdLeave);
                Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment_" + r.ToString(), x.EmpAdvancePayment);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth_" + r.ToString(), x.SSEmployeeHealth);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment_" + r.ToString(), x.SSEmployeeEmployment);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension_" + r.ToString(), x.SSEmployeePension);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2_" + r.ToString(), x.SSEmployeePension2);
                Sm.CmParam<Decimal>(ref cm, "@SalaryPension_" + r.ToString(), x.SalaryPension);
                Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance_" + r.ToString(), x.SSErLifeInsurance);
                Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance_" + r.ToString(), x.SSEeLifeInsurance);
                Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident_" + r.ToString(), x.SSErWorkingAccident);
                Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident_" + r.ToString(), x.SSEeWorkingAccident);
                Sm.CmParam<Decimal>(ref cm, "@SSErRetirement_" + r.ToString(), x.SSErRetirement);
                Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement_" + r.ToString(), x.SSEeRetirement);
                Sm.CmParam<Decimal>(ref cm, "@SSErPension_" + r.ToString(), x.SSErPension);
                Sm.CmParam<Decimal>(ref cm, "@SSEePension_" + r.ToString(), x.SSEePension);
                Sm.CmParam<Decimal>(ref cm, "@ADLeave_" + r.ToString(), x.ADLeave);
                Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment_" + r.ToString(), x.SalaryAdjustment);
                Sm.CmParam<String>(ref cm, "@CreditCode1_" + r.ToString(), x.CreditCode1);
                Sm.CmParam<String>(ref cm, "@CreditCode2_" + r.ToString(), x.CreditCode2);
                Sm.CmParam<String>(ref cm, "@CreditCode3_" + r.ToString(), x.CreditCode3);
                Sm.CmParam<String>(ref cm, "@CreditCode4_" + r.ToString(), x.CreditCode4);
                Sm.CmParam<String>(ref cm, "@CreditCode5_" + r.ToString(), x.CreditCode5);
                Sm.CmParam<String>(ref cm, "@CreditCode6_" + r.ToString(), x.CreditCode6);
                Sm.CmParam<String>(ref cm, "@CreditCode7_" + r.ToString(), x.CreditCode7);
                Sm.CmParam<String>(ref cm, "@CreditCode8_" + r.ToString(), x.CreditCode8);
                Sm.CmParam<String>(ref cm, "@CreditCode9_" + r.ToString(), x.CreditCode9);
                Sm.CmParam<String>(ref cm, "@CreditCode10_" + r.ToString(), x.CreditCode10);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1_" + r.ToString(), x.CreditAdvancePayment1);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2_" + r.ToString(), x.CreditAdvancePayment2);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3_" + r.ToString(), x.CreditAdvancePayment3);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4_" + r.ToString(), x.CreditAdvancePayment4);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5_" + r.ToString(), x.CreditAdvancePayment5);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6_" + r.ToString(), x.CreditAdvancePayment6);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7_" + r.ToString(), x.CreditAdvancePayment7);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8_" + r.ToString(), x.CreditAdvancePayment8);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9_" + r.ToString(), x.CreditAdvancePayment9);
                Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10_" + r.ToString(), x.CreditAdvancePayment10);
                Sm.CmParam<Decimal>(ref cm, "@Tax_" + r.ToString(), x.Tax);
                Sm.CmParam<Decimal>(ref cm, "@TaxAllowance_" + r.ToString(), x.TaxAllowance);
                Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), x.Amt);
                Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive_" + r.ToString(), x.ServiceChargeIncentive);
                Sm.CmParam<Decimal>(ref cm, "@NontaxableSalaryAdjustment_" + r.ToString(), x.NontaxableSalaryAdjustment);
                Sm.CmParam<Decimal>(ref cm, "@TaxableSalaryAdjustment_" + r.ToString(), x.TaxableSalaryAdjustment);

                r++;
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePayrollProcess2(ref List<Result1> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int r = 0;
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Payroll Processing (PayrollProcess2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            foreach (var x in l)
            {
                if (IsFirstOrExisted)
                {
                    SQL.AppendLine("Insert Into TblPayrollProcess2 ");
                    SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
                    SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
                    SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
                    SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
                    SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
                    SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
                    SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
                    SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
                    SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
                    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
                    SQL.AppendLine("OT2Amt, OTHolidayAmt, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
                    SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
                    SQL.AppendLine("DedProdLeave, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");
                    IsFirstOrExisted = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine(
                    " (@PayrunCode_" + r.ToString() +
                    ", @EmpCode_" + r.ToString() +
                    ", @Dt_" + r.ToString() +
                    ", @ProcessInd_" + r.ToString() +
                    ", @LatestPaidDt_" + r.ToString() +
                    ", @JoinDt_" + r.ToString() +
                    ", @ResignDt_" + r.ToString() +
                    ", @PosCode_" + r.ToString() +
                    ", @DeptCode_" + r.ToString() +
                    ", @GrdLvlCode_" + r.ToString() +
                    ", @LevelCode_" + r.ToString() +
                    ", @SystemType_" + r.ToString() +
                    ", @EmploymentStatus_" + r.ToString() +
                    ", @PayrunPeriod_" + r.ToString() +
                    ", @PGCode_" + r.ToString() +
                    ", @SiteCode_" + r.ToString() +
                    ", @WorkingDay_" + r.ToString() +
                    ", @WSCode_" + r.ToString() +
                    ", @HolInd_" + r.ToString() +
                    ", @HolidayIndex_" + r.ToString() +
                    ", @WSHolidayInd_" + r.ToString() +
                    ", @WSIn1_" + r.ToString() +
                    ", @WSOut1_" + r.ToString() +
                    ", @WSIn2_" + r.ToString() +
                    ", @WSOut2_" + r.ToString() +
                    ", @WSIn3_" + r.ToString() +
                    ", @WSOut3_" + r.ToString() +
                    ", @OneDayInd_" + r.ToString() +
                    ", @LateInd_" + r.ToString() +
                    ", @ActualIn_" + r.ToString() +
                    ", @ActualOut_" + r.ToString() +
                    ", @WorkingIn_" + r.ToString() +
                    ", @WorkingOut_" + r.ToString() +
                    ", @WorkingDuration_" + r.ToString() +
                    ", @EmpSalary_" + r.ToString() +
                    ", @EmpSalary2_" + r.ToString() +
                    ", @BasicSalary_" + r.ToString() +
                    ", @BasicSalary2_" + r.ToString() +
                    ", @SalaryPension_" + r.ToString() +
                    ", @ProductionWages_" + r.ToString() +
                    ", @Salary_" + r.ToString() +
                    ", @LeaveCode_" + r.ToString() +
                    ", @LeaveType_" + r.ToString() +
                    ", @PaidLeaveInd_" + r.ToString() +
                    ", @LeaveStartTm_" + r.ToString() +
                    ", @LeaveEndTm_" + r.ToString() +
                    ", @LeaveDuration_" + r.ToString() +
                    ", @PLDay_" + r.ToString() +
                    ", @PLHr_" + r.ToString() +
                    ", @PLAmt_" + r.ToString() +
                    ", @ProcessPLAmt_" + r.ToString() +
                    ", @UPLDay_" + r.ToString() +
                    ", @UPLHr_" + r.ToString() +
                    ", @UPLAmt_" + r.ToString() +
                    ", @ProcessUPLAmt_" + r.ToString() +
                    ", @OT1Hr_" + r.ToString() +
                    ", @OT2Hr_" + r.ToString() +
                    ", @OTHolidayHr_" + r.ToString() +
                    ", @OT1Amt_" + r.ToString() +
                    ", @OT2Amt_" + r.ToString() +
                    ", @OTHolidayAmt_" + r.ToString() +
                    ", @OTToLeaveInd_" + r.ToString() +
                    ", @IncMinWages_" + r.ToString() +
                    ", @IncProduction_" + r.ToString() +
                    ", @IncEmployee_" + r.ToString() +
                    ", @IncPerformance_" + r.ToString() +
                    ", @PresenceRewardInd_" + r.ToString() +
                    ", @HolidayEarning_" + r.ToString() +
                    ", @ExtraFooding_" + r.ToString() +
                    ", @FieldAssignment_" + r.ToString() +
                    ", @Transport_" + r.ToString() +
                    ", @Meal_" + r.ToString() +
                    ", @ServiceChargeIncentive_" + r.ToString() +
                    ", @DedProduction_" + r.ToString() +
                    ", @DedProdLeave_" + r.ToString() +
                    ", @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "@PayrunCode_" + r.ToString(), x.PayrunCode);
                Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), x.EmpCode);
                Sm.CmParam<String>(ref cm, "@Dt_" + r.ToString(), x.Dt);
                Sm.CmParam<String>(ref cm, "@ProcessInd_" + r.ToString(), x.ProcessInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LatestPaidDt_" + r.ToString(), x.LatestPaidDt);
                Sm.CmParam<String>(ref cm, "@JoinDt_" + r.ToString(), x.JoinDt);
                Sm.CmParam<String>(ref cm, "@ResignDt_" + r.ToString(), x.ResignDt);
                Sm.CmParam<String>(ref cm, "@PosCode_" + r.ToString(), x.PosCode);
                Sm.CmParam<String>(ref cm, "@DeptCode_" + r.ToString(), x.DeptCode);
                Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + r.ToString(), x.GrdLvlCode);
                Sm.CmParam<String>(ref cm, "@LevelCode_" + r.ToString(), x.LevelCode);
                Sm.CmParam<String>(ref cm, "@SystemType_" + r.ToString(), x.SystemType);
                Sm.CmParam<String>(ref cm, "@EmploymentStatus_" + r.ToString(), x.EmploymentStatus);
                Sm.CmParam<String>(ref cm, "@PayrunPeriod_" + r.ToString(), x.PayrunPeriod);
                Sm.CmParam<String>(ref cm, "@PGCode_" + r.ToString(), x.PGCode);
                Sm.CmParam<String>(ref cm, "@SiteCode_" + r.ToString(), x.SiteCode);
                Sm.CmParam<Decimal>(ref cm, "@WorkingDay_" + r.ToString(), x.WorkingDay);
                Sm.CmParam<String>(ref cm, "@WSCode_" + r.ToString(), x.WSCode);
                Sm.CmParam<String>(ref cm, "@HolInd_" + r.ToString(), x.HolInd ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@HolidayIndex_" + r.ToString(), x.HolidayIndex);
                Sm.CmParam<String>(ref cm, "@WSHolidayInd_" + r.ToString(), x.WSHolidayInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@WSIn1_" + r.ToString(), x.WSIn1);
                Sm.CmParam<String>(ref cm, "@WSOut1_" + r.ToString(), x.WSOut1);
                Sm.CmParam<String>(ref cm, "@WSIn2_" + r.ToString(), x.WSIn2);
                Sm.CmParam<String>(ref cm, "@WSOut2_" + r.ToString(), x.WSOut2);
                Sm.CmParam<String>(ref cm, "@WSIn3_" + r.ToString(), x.WSIn3);
                Sm.CmParam<String>(ref cm, "@WSOut3_" + r.ToString(), x.WSOut3);
                Sm.CmParam<String>(ref cm, "@OneDayInd_" + r.ToString(), x.OneDayInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LateInd_" + r.ToString(), x.LateInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ActualIn_" + r.ToString(), x.ActualIn);
                Sm.CmParam<String>(ref cm, "@ActualOut_" + r.ToString(), x.ActualOut);
                Sm.CmParam<String>(ref cm, "@WorkingIn_" + r.ToString(), x.WorkingIn);
                Sm.CmParam<String>(ref cm, "@WorkingOut_" + r.ToString(), x.WorkingOut);
                Sm.CmParam<Decimal>(ref cm, "@WorkingDuration_" + r.ToString(), x.WorkingDuration);
                Sm.CmParam<Decimal>(ref cm, "@EmpSalary_" + r.ToString(), x.EmpSalary);
                Sm.CmParam<Decimal>(ref cm, "@EmpSalary2_" + r.ToString(), x.EmpSalary2);
                Sm.CmParam<Decimal>(ref cm, "@BasicSalary_" + r.ToString(), x.BasicSalary);
                Sm.CmParam<Decimal>(ref cm, "@BasicSalary2_" + r.ToString(), x.BasicSalary2);
                Sm.CmParam<Decimal>(ref cm, "@SalaryPension_" + r.ToString(), x.SalaryPension);
                Sm.CmParam<Decimal>(ref cm, "@ProductionWages_" + r.ToString(), x.ProductionWages);
                Sm.CmParam<Decimal>(ref cm, "@Salary_" + r.ToString(), x.Salary);
                Sm.CmParam<String>(ref cm, "@LeaveCode_" + r.ToString(), x.LeaveCode);
                Sm.CmParam<String>(ref cm, "@LeaveType_" + r.ToString(), x.LeaveType);
                Sm.CmParam<String>(ref cm, "@PaidLeaveInd_" + r.ToString(), x.PaidLeaveInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LeaveStartTm_" + r.ToString(), x.LeaveStartTm);
                Sm.CmParam<String>(ref cm, "@LeaveEndTm_" + r.ToString(), x.LeaveEndTm);
                Sm.CmParam<Decimal>(ref cm, "@LeaveDuration_" + r.ToString(), x.LeaveDuration);
                Sm.CmParam<Decimal>(ref cm, "@UPLDay_" + r.ToString(), x.UPLDay);
                Sm.CmParam<Decimal>(ref cm, "@UPLHr_" + r.ToString(), x.UPLHr);
                Sm.CmParam<Decimal>(ref cm, "@UPLAmt_" + r.ToString(), x.UPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt_" + r.ToString(), x.ProcessUPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@PLDay_" + r.ToString(), x.PLDay);
                Sm.CmParam<Decimal>(ref cm, "@PLHr_" + r.ToString(), x.PLHr);
                Sm.CmParam<Decimal>(ref cm, "@PLAmt_" + r.ToString(), x.PLAmt);
                Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt_" + r.ToString(), x.ProcessPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@OT1Hr_" + r.ToString(), x.OT1Hr);
                Sm.CmParam<Decimal>(ref cm, "@OT2Hr_" + r.ToString(), x.OT2Hr);
                Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr_" + r.ToString(), x.OTHolidayHr);
                Sm.CmParam<Decimal>(ref cm, "@OT1Amt_" + r.ToString(), x.OT1Amt);
                Sm.CmParam<Decimal>(ref cm, "@OT2Amt_" + r.ToString(), x.OT2Amt);
                Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt_" + r.ToString(), x.OTHolidayAmt);
                Sm.CmParam<String>(ref cm, "@OTToLeaveInd_" + r.ToString(), x.OTToLeaveInd ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@IncMinWages_" + r.ToString(), x.IncMinWages);
                Sm.CmParam<Decimal>(ref cm, "@IncProduction_" + r.ToString(), x.IncProduction);
                Sm.CmParam<Decimal>(ref cm, "@IncEmployee_" + r.ToString(), x.IncEmployee);
                Sm.CmParam<Decimal>(ref cm, "@IncPerformance_" + r.ToString(), x.IncPerformance);
                Sm.CmParam<String>(ref cm, "@PresenceRewardInd_" + r.ToString(), x.PresenceRewardInd ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@HolidayEarning_" + r.ToString(), x.HolidayEarning);
                Sm.CmParam<Decimal>(ref cm, "@ExtraFooding_" + r.ToString(), x.ExtraFooding);
                Sm.CmParam<Decimal>(ref cm, "@FieldAssignment_" + r.ToString(), x.FieldAssignment);
                Sm.CmParam<Decimal>(ref cm, "Transport_" + r.ToString(), x.Transport);
                Sm.CmParam<Decimal>(ref cm, "Meal_" + r.ToString(), x.Meal);
                Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive_" + r.ToString(), x.ServiceChargeIncentive);
                Sm.CmParam<Decimal>(ref cm, "@DedProduction_" + r.ToString(), x.DedProduction);
                Sm.CmParam<Decimal>(ref cm, "@DedProdLeave_" + r.ToString(), x.DedProdLeave);

                r++;
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Process Data

        private void ProcessData1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Distinct T1.EmpCode ");
            SQL.AppendLine("    From TblEmployeePPS T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select X.EmpCode, Max(X.StartDt) As StartDt ");
            SQL.AppendLine("        From TblEmployeePPS X ");
            SQL.AppendLine("        Where Exists ( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblEmployee T ");
            SQL.AppendLine("            Where T.EmpCode=X.EmpCode ");
            SQL.AppendLine("            And T.JoinDt<=@EndDt ");
            SQL.AppendLine("            And ( ");
            SQL.AppendLine("                T.ResignDt Is Null Or ");
            SQL.AppendLine("                (T.ResignDt Is Not Null And T.ResignDt>@StartDt) Or ");
            SQL.AppendLine("                ( ");
            SQL.AppendLine("                    T.ResignDt Is Not Null ");
            SQL.AppendLine("                    And T.ResignDt<=@StartDt ");
            SQL.AppendLine("                    And ( ");

            SQL.AppendLine("                        T.EmpCode In ( ");
            SQL.AppendLine("                            Select Tbl2.EmpCode ");
            SQL.AppendLine("                            From TblEmpSCIHdr Tbl1, TblEmpSCIDtl Tbl2 ");
            SQL.AppendLine("                            Where Tbl1.DocNo=Tbl2.DocNo ");
            SQL.AppendLine("                            And Tbl1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("                            And (Tbl2.PayrunCode Is Null Or (Tbl2.PayrunCode Is Not Null And Tbl2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("                            And Tbl1.CancelInd='N' ");
            SQL.AppendLine("                            ) Or ");

            SQL.AppendLine("                        T.EmpCode In ( ");
            SQL.AppendLine("                            Select EmpCode ");
            SQL.AppendLine("                            From TblSalaryAdjustmentHdr ");
            SQL.AppendLine("                            Where CancelInd='N' ");
            SQL.AppendLine("                            And PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("                            And (PayrunCode Is Null Or (PayrunCode Is Not Null And PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("                            ) Or ");

            SQL.AppendLine("                        T.EmpCode In ( ");
            SQL.AppendLine("                            Select Tbl2.EmpCode ");
            SQL.AppendLine("                            From TblSalaryAdjustment2Hdr Tbl1, TblSalaryAdjustment2Dtl Tbl2 ");
            SQL.AppendLine("                            Where Tbl1.DocNo=Tbl2.DocNo ");
            SQL.AppendLine("                            And Tbl1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("                            And (Tbl2.PayrunCode Is Null Or (Tbl2.PayrunCode Is Not Null And Tbl2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("                            And Tbl1.CancelInd='N' ");
            SQL.AppendLine("                            ) ");

            SQL.AppendLine("                        ) ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And X.DeptCode=@DeptCode ");
            SQL.AppendLine("        And X.SystemType=@SystemType ");
            SQL.AppendLine("        And X.PayrunPeriod=@PayrunPeriod ");
            SQL.AppendLine("        And IfNull(X.PGCode, '')=IfNull(@PGCode, '') ");
            SQL.AppendLine("        And IfNull(X.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And X.StartDt<=@EndDt ");
            SQL.AppendLine("        And (X.EndDt Is Null Or X.EndDt>=@StartDt)");
            SQL.AppendLine("        Group By X.EmpCode ");
            SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode And T1.StartDt=T3.StartDt ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On B.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On B.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Order By A.EmpCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@SystemType", mSystemType);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", mPayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        mlPayrunEmployee.Add(
                            new PayrunEmployee(){ EmpCode = Sm.DrStr(dr, c[0]) }
                            );
                }
                dr.Close();
            }
        }

        private void ProcessData2()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            #region Get Table Data

            var lEmployeePPS = new List<EmployeePPS>();
            var lOT = new List<OT>();
            var lNTI = new List<NTI>();
            var lTI = new List<TI>();

            ProcessOT(ref lOT);
            ProcessNTI(ref lNTI);
            ProcessTI(ref lTI);
            ProcessAdvancePaymentProcess(ref mlAdvancePaymentProcess);

            #endregion

            var lResult1 = new List<Result1>();

            Process1(ref lResult1);
            Process2(ref lResult1);
            Process3(ref lResult1);

            if (lResult1.Count > 0)
            {
                lResult1.ForEach(r => 
                    { Process4(ref r); });
                lResult1.Sort(
                    delegate(Result1 r1a, Result1 r1b)
                    {
                        int f = r1a.EmpCode.CompareTo(r1b.EmpCode);
                        return f != 0 ? f : r1a.Dt.CompareTo(r1b.Dt);
                    });
            }

            var lResult2 = new List<Result2>();

            Process5(ref lResult2);

            lResult2.ForEach(r => { Process6(ref r, ref lResult1, ref lNTI, ref lTI); });

            #region Save Data

            var cml = new List<MySqlCommand>();

            cml.Add(DeletePayrollProcess());

            //lResult2.ForEach(r => { cml.Add(SavePayrollProcess1(ref r)); });
            //lResult1.ForEach(r => { cml.Add(SavePayrollProcess2(ref r)); });

            if (lResult2.Count>0) cml.Add(SavePayrollProcess1(ref lResult2));
            if (lResult1.Count > 0) cml.Add(SavePayrollProcess2(ref lResult1));

            cml.Add(SavePayrollProcess());

            if (mlAdvancePaymentProcess.Count > 0)
                cml.Add(SaveAdvancePaymentProcess(ref mlAdvancePaymentProcess));
                // mlAdvancePaymentProcess.ForEach(x => { cml.Add(SaveAdvancePaymentProcess(ref x)); });

            Sm.ExecCommands(cml);

            #endregion
        }

        private void Process1(ref List<Result1> lResult1)
        {
            var lDt = new List<string>();
            GetDt(ref lDt);

            var lEmployeePPS = new List<EmployeePPS>();
            ProcessEmployeePPS(ref lEmployeePPS);

            if (lDt.Count>0 && lEmployeePPS.Count>0)
            {
                var PayrunCodeTemp = mPayrunCode;
                var EmpCodeTemp = string.Empty;
                var DeptCodeTemp = string.Empty;
                var GrdLvlCodeTemp = string.Empty;
                var LevelCodeTemp = string.Empty;
                var EmploymentStatusTemp = string.Empty;
                var SystemTypeTemp = string.Empty;
                var PayrunPeriodTemp = string.Empty;
                var PGCodeTemp = string.Empty;
                var SiteCodeTemp = string.Empty;
                var PosCodeTemp = string.Empty;
                var HOIndTemp = string.Empty;

                for (int i = 0; i < mlPayrunEmployee.Count; i++)
                {
                    EmpCodeTemp = mlPayrunEmployee[i].EmpCode;
                    if (EmpCodeTemp.Length != 0)
                    {
                        foreach (string DtTemp in lDt)
                        {
                            DeptCodeTemp = string.Empty;
                            GrdLvlCodeTemp = string.Empty;
                            LevelCodeTemp = string.Empty;
                            EmploymentStatusTemp = string.Empty;
                            SystemTypeTemp = string.Empty;
                            PayrunPeriodTemp = string.Empty;
                            PGCodeTemp = string.Empty;
                            SiteCodeTemp = string.Empty;
                            PosCodeTemp = string.Empty;
                            HOIndTemp = string.Empty;

                            foreach (var x in 
                                lEmployeePPS
                                    .Where(Index => Sm.CompareStr(Index.EmpCode, EmpCodeTemp))
                                    .OrderByDescending(x=>x.StartDt)) 
                            {
                                if(Sm.CompareDtTm(DtTemp, x.StartDt)>=0)
                                {
                                    DeptCodeTemp = x.DeptCode;
                                    GrdLvlCodeTemp = x.GrdLvlCode;
                                    LevelCodeTemp = x.LevelCode;
                                    EmploymentStatusTemp = x.EmploymentStatus;
                                    SystemTypeTemp = x.SystemType;
                                    PayrunPeriodTemp = x.PayrunPeriod;
                                    PGCodeTemp = x.PGCode;
                                    SiteCodeTemp = x.SiteCode;
                                    PosCodeTemp = x.PosCode;
                                    HOIndTemp = x.HOInd;
                                    break;
                                };
                            }

                            lResult1.Add(new Result1()
                                {
                                    PayrunCode = PayrunCodeTemp,
                                    EmpCode = EmpCodeTemp,
                                    Dt = DtTemp,
                                    DeptCode=DeptCodeTemp,
                                    GrdLvlCode = GrdLvlCodeTemp,
                                    LevelCode = LevelCodeTemp,
                                    EmploymentStatus=EmploymentStatusTemp,
                                    SystemType=SystemTypeTemp,
                                    PayrunPeriod=PayrunPeriodTemp,
                                    PGCode = PGCodeTemp,
                                    SiteCode = SiteCodeTemp,
                                    PosCode=PosCodeTemp,
                                    _IsHOInd = HOIndTemp=="Y",
                                    ProcessInd = true,
                                    LatestPaidDt = string.Empty,
                                    JoinDt = string.Empty,
                                    ResignDt = string.Empty,
                                    WorkingDay = 0m,
                                    WSCode  = string.Empty,
                                    HolInd = false,
                                    HolidayIndex = mStdHolidayIndex,
                                    WSHolidayInd = false,
                                    WSIn1  = string.Empty,
                                    WSOut1  = string.Empty,
                                    WSIn2  = string.Empty,
                                    WSOut2  = string.Empty,
                                    WSIn3  = string.Empty,
                                    WSOut3  = string.Empty,
                                    OneDayInd = true,
                                    LateInd = false,
                                    ActualIn  = string.Empty,
                                    ActualOut  = string.Empty,
                                    WorkingIn  = string.Empty,
                                    WorkingOut  = string.Empty,
                                    WorkingDuration = 0m,
                                    BasicSalary = 0m,
                                    BasicSalary2 = 0m,
                                    _LatestMonthlyGrdLvlSalary = 0m,
                                    _LatestDailyGrdLvlSalary = 0m,
                                    ProductionWages = 0m,
                                    Salary = 0m,
                                    LeaveCode  = string.Empty,
                                    LeaveType = string.Empty,
                                    PaidLeaveInd = false,
                                    CompulsoryLeaveInd = false,
                                    _DeductTHPInd = false,
                                    LeaveStartTm  = string.Empty,
                                    LeaveEndTm  = string.Empty,
                                    LeaveDuration = 0m,
                                    PLDay = 0m,
                                    PLHr = 0m,
                                    PLAmt = 0m,
                                    ProcessPLAmt = 0m,
                                    OT1Hr = 0m,
                                    OT2Hr = 0m,
                                    OTHolidayHr = 0m,
                                    OT1Amt = 0m,
                                    OT2Amt = 0m,
                                    OTHolidayAmt = 0m,
                                    OTToLeaveInd = false,
                                    IncMinWages = 0m,
                                    IncProduction = 0m,
                                    IncPerformance = 0m,
                                    PresenceRewardInd = false,
                                    HolidayEarning = 0m,
                                    ExtraFooding = 0m,
                                    FieldAssignment = 0m,
                                    DedProduction = 0m,
                                    DedProdLeave = 0m,
                                    EmpSalary  = 0m,
                                    EmpSalary2 = 0m,
                                    SalaryPension = 0m,
                                    UPLDay  = 0m,
                                    UPLHr  = 0m,
                                    UPLAmt = 0m,
                                    ProcessUPLAmt = 0m,
                                    Transport = 0m,
                                    Meal = 0m,
                                    ServiceChargeIncentive = 0m,
                                    IncEmployee = 0m,
                                    _ExtraFooding = 0m,
                                    _FieldAssignment = 0m,
                                    _IncPerformance = 0m,
                                    _IsFullDayInd = false,
                                    _IsUseLatestGrdLvlSalary = false
                                });
                        }
                    }
                }
                lDt.Clear();
                lEmployeePPS.Clear();
            }
        }

        private void Process2(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            #region Employee's Work Schedule

            var lEmpWorkSchedule = new List<EmpWorkSchedule>();
            ProcessEmpWorkSchedule(ref lEmpWorkSchedule);
            if (lEmpWorkSchedule.Count > 0)
            {
                var WSCodesForHoliday = mWSCodesForHoliday.Split('#');
                var WSCodesForNationalHoliday = mWSCodesForNationalHoliday.Split('#');

               foreach (var ews in lEmpWorkSchedule)
               {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ews.EmpCode && Index.Dt == ews.Dt))
                    {
                        r.WSCode = ews.WSCode;
                        r.WSHolidayInd = ews.HolidayInd=="Y";
                        r._DoubleShiftInd = ews.DoubleShiftInd == "Y";
                        r.WSIn1 = ews.In1;
                        r.WSOut1 = ews.Out1;
                        r.WSIn2 = ews.In2;
                        r.WSOut2 = ews.Out2;
                        r.WSIn3 = ews.In3;
                        r.WSOut3 = ews.Out3;
                    }
               }
               lEmpWorkSchedule.Clear();
            }

            #endregion

            #region Employee's Paid Date

            var lEmpPaidDt = new List<EmpPaidDt>();
            ProcessEmpPaidDt(ref lEmpPaidDt);
            if (lEmpPaidDt.Count > 0)
            {
                lResult1.ForEach(A =>
                {
                    foreach (var x in lEmpPaidDt.Where(x => x.EmpCode == A.EmpCode && x.Dt == A.Dt))
                    {
                        A.LatestPaidDt = x.Dt;
                        break;
                    }
                });
               lEmpPaidDt.Clear();
            }

            #endregion

            #region Attendance

            var lAtd = new List<Atd>();
            ProcessAtd(ref lAtd);
            if (lAtd.Count > 0)
            {
                foreach (var atd in lAtd)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == atd.EmpCode && Index.Dt == atd.Dt))
                    {
                        r.ActualIn = atd.ActualIn;
                        r.ActualOut = atd.ActualOut;
                        break;
                    }
                }
                lAtd.Clear();
            }

            #endregion

            #region Leave

            var lLeave = new List<Leave>();
            ProcessLeave(ref lLeave);

            var lLeaveDtl = new List<LeaveDtl>();
            ProcessLeaveDtl(ref lLeaveDtl);

            if (lLeave.Count > 0)
            {
                foreach (var l in lLeave)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == l.EmpCode && Index.Dt == l.Dt))
                    {
                        r.LeaveCode = l.LeaveCode;
                        r.LeaveType = l.LeaveType;
                        r.LeaveStartTm = l.StartTm;
                        r.LeaveEndTm = l.EndTm;
                        r.LeaveDuration = l.DurationHr;
                        r.PaidLeaveInd = l.PaidInd=="Y";
                        r.CompulsoryLeaveInd = l.CompulsoryLeaveInd == "Y";
                        foreach (var ld in lLeaveDtl.Where(
                            Index => 
                                Index.LeaveCode == l.LeaveCode && 
                                Index.SystemType == r.SystemType &&
                                Index.PayrunPeriod == r.PayrunPeriod
                                ))
                        {
                            r._DeductTHPInd = ld.DeductTHPInd;
                        }
                        break;
                    }
                }
                lLeave.Clear();
                lLeaveDtl.Clear();
            }

            #endregion

            #region Employee

            var lEmployee = new List<Employee>();
            ProcessEmployee(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                foreach (var e in lEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == e.EmpCode))
                    {
                        r.JoinDt = e.JoinDt;
                        r.ResignDt = e.ResignDt;

                        if (Sm.CompareDtTm(r.Dt, r.JoinDt) < 0) r.ProcessInd = false;

                        if (r.ResignDt.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.Dt, r.ResignDt) >= 0) r.ProcessInd = false;
                        }

                        if (r.LatestPaidDt.Length > 0) r.ProcessInd = false;
                        
                    }
                }
                lEmployee.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalary = new List<EmployeeSalary>();
            ProcessEmployeeSalary(ref lEmployeeSalary);
            if (lEmployeeSalary.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var esl in lEmployeeSalary
                        .Where(Index => Index.EmpCode == r.EmpCode)
                        .OrderByDescending(Index => Index.StartDt))
                    {
                        if (Sm.CompareDtTm(esl.StartDt, r.Dt) <= 0)
                        {
                            r.EmpSalary = esl.Amt;
                            r.EmpSalary2 = esl.Amt2;
                            break;
                        }
                    }
                }
                lEmployeeSalary.Clear();
            }

            #endregion

            #region EmployeeSalaryss

            var lEmployeeSalarySS = new List<EmployeeSalarySS>();
            ProcessEmployeeSalarySSPension(ref lEmployeeSalarySS);
            if (lEmployeeSalarySS.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var i in lEmployeeSalarySS
                        .Where(w => Sm.CompareStr(w.EmpCode, r.EmpCode))
                        .OrderByDescending(o => o.StartDt))
                    {
                        if (Sm.CompareDtTm(i.StartDt, r.Dt) <= 0)
                        {
                            r.SalaryPension = i.Amt;
                            break;
                        }
                    }
                }
                lEmployeeSalarySS.Clear();
            }

            #endregion

            #region Employee Meal Transport

            ProcessEmployeeMealTransport(ref mEmployeeMealTransport);
           
            #endregion

            #region Attendance

            mlAttendanceLog.Clear();
            ProcessAttendanceLog(ref mlAttendanceLog);

            #endregion

            #region OT

            mlOT.Clear();
            ProcessOT(ref mlOT);

            #endregion

            #region OT Adjustment

            mlOTAdjustment.Clear();
            ProcessOTAdjustment(ref mlOTAdjustment);

            #endregion

            #region Employee Incentive

            var lIncEmployee = new List<IncEmployee>();
            ProcessIncEmployee(ref lIncEmployee);
            if (lIncEmployee.Count > 0)
            {
                foreach (var ie in lIncEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ie.EmpCode && Index.Dt == ie.DocDt))
                    {
                        r.IncEmployee = ie.Amt;
                        break;
                    }
                }
                lIncEmployee.Clear();
            }

            #endregion

            #region Employee's Service Charge Incentive

            var lEmpSCI = new List<EmpSCI>();
            ProcessEmpSCI(ref lEmpSCI);
            if (lEmpSCI.Count > 0)
            {
                foreach (var x in lEmpSCI)
                {
                    foreach (var r in lResult1
                        .Where(w => 
                            Sm.CompareStr(w.EmpCode, x.EmpCode) && 
                            Sm.CompareStr(w.Dt, x.DocDt)))
                    {
                        r.ServiceChargeIncentive = x.Amt;
                        break;
                    }
                }
                lEmpSCI.Clear();
            }

            #endregion
        }

        private void Process3(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            var EmpCodeTemp = string.Empty;
            var PayrunPeriodTemp = string.Empty;
            string 
                SystemTypeTemp = string.Empty, 
                GrdLvlCodeTemp = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCodeTemp = mlPayrunEmployee[i].EmpCode;
                if (EmpCodeTemp.Length != 0)
                {
                    SystemTypeTemp = string.Empty;
                    PayrunPeriodTemp = string.Empty;
                    GrdLvlCodeTemp = string.Empty;
                    
                    foreach (var x in lResult1.Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0).OrderBy(x => x.Dt))
                    {
                        x.Salary = x.EmpSalary2; // Berdasarkan Employee Salary
                        
                        if (x.ProcessInd)
                        {
                            if (!(
                                Sm.CompareStr(x.DeptCode, mDeptCode) &&
                                Sm.CompareStr(x.SystemType, mSystemType) &&
                                Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                                Sm.CompareStr(x.PGCode, mPGCode) &&
                                Sm.CompareStr(x.SiteCode, mSiteCode)
                                ))
                                x.ProcessInd = false;       
                        }
                    }
                }
            }
            
        }

        private void Process4(ref Result1 r)
        {
            string
                ActualDtIn = string.Empty, 
                ActualDtOut = string.Empty,
                ActualTmIn = string.Empty, 
                ActualTmOut = string.Empty,
                EmpCode = r.EmpCode, 
                Dt = r.Dt;

            var Value = 0m;

            #region Set Date2 untuk Shift 3

            var Dt2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(1)).Substring(0, 8);

            #endregion

            #region Set Join Date And Resign Date

            var JoinDt = Sm.ConvertDateTime(r.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (r.ResignDt.Length > 0) ResignDt = Sm.ConvertDateTime(r.ResignDt + "2359");

            #endregion

            #region Set Actual Date/Time

            if (r.ActualIn.Length > 0)
            {
                ActualDtIn = Sm.Left(r.ActualIn, 8);
                ActualTmIn = r.ActualIn.Substring(8, 4);
            }

            if (r.ActualOut.Length > 0)
            {
                ActualDtOut = Sm.Left(r.ActualOut, 8);
                ActualTmOut = r.ActualOut.Substring(8, 4);
            }

            #endregion

            #region set shift 1,2 atau shift 3

            if (r.WSIn1.Length > 0 && 
                r.WSOut1.Length > 0 && 
                Sm.CompareDtTm(r.WSIn1, r.WSOut1)>0)
                r.OneDayInd = false;

            #endregion

            #region cek apakah karyawan terlambat

            if (ActualTmIn.Length > 0 && r.WSIn1.Length > 0)
            {
                if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                    r.LateInd = true;
            }

            #endregion

            #region set jam masuk

            if (r.Dt.Length > 0 && r.WSIn1.Length > 0 && ActualDtIn.Length > 0 && ActualTmIn.Length > 0)
            {
                if (mIsWorkingHrBasedOnSchedule)
                {
                    r.WorkingIn = r.Dt + r.WSIn1;
                }
                else
                {
                    if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                        r.WorkingIn = ActualDtIn + ActualTmIn;
                    else
                        r.WorkingIn = r.Dt + r.WSIn1;
                }
            }

            #endregion

            #region set jam keluar

            if (r.Dt.Length > 0 && r.WSOut1.Length > 0 && ActualDtOut.Length > 0 && ActualTmOut.Length > 0)
            {
                if (r.OneDayInd)
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = r.Dt + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = r.Dt + r.WSOut1;
                    }
                }
                else
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = Dt2 + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = Dt2 + r.WSOut1;
                    }
                }
            }

            #endregion

            #region set OT rutin

            string ROT1 = string.Empty, ROT2 = string.Empty;

            #region Set OT Rutin (In)

            if (ActualDtIn.Length > 0 && ActualTmIn.Length > 0 && r.WSIn3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    //Shift 1, 2
                    if (Sm.CompareDtTm(r.Dt + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //1500 < 1600
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //1500 > 1400
                        ROT1 = r.Dt + r.WSIn3;
                    }
                }
                else
                {
                    //Shift 3
                    if (Sm.CompareDtTm(Dt2 + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //0400<0500
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //0400>0300
                        ROT1 = Dt2 + r.WSIn3;
                    }
                }
            }

            #endregion

            #region Set OT Rutin (Out)

            if (ActualDtOut.Length > 0 && ActualTmOut.Length > 0 && r.WSOut3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    ////Shift 1, 2
                    if (Sm.CompareDtTm(r.Dt + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                    {
                        //1900<2000
                        ROT2 = r.Dt + r.WSOut3;
                    }
                    else
                    {
                        //1900>1800
                        ROT2 = ActualDtOut + ActualTmOut;
                    }
                }
                else
                {
                    //Shift 3
                    if (Sm.CompareDtTm(Dt2 + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                    {
                        //0300<0400
                        ROT2 = Dt2 + r.WSOut3;
                    }
                    else
                    {
                        //0300>0200
                        ROT2 = ActualDtOut + ActualTmOut;
                    }
                }
            }

            #endregion

            #endregion

            #region set OT rutin duration

            var ROTDuration = 0m;
            if (ROT1.Length > 0 && ROT2.Length > 0)
            {
                ROTDuration = ComputeDuration(ROT2, ROT1);
                if (ROTDuration < 0) ROTDuration = 0m;
                if (mIsOTRoutineRounding && ROTDuration > 0)
                {
                    ROTDuration = RoundTo1Hour((double)ROTDuration);
                }
            }

            #endregion

            #region set working duration

            decimal 
                WorkingDuration = 0m, 
                BreakDuration = 0m;

            if (r.WorkingIn.Length > 0 && r.WorkingOut.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    //Full Day Leave
                    WorkingDuration = 0m;
                    ROTDuration = 0m;
                }
                else
                {
                    if (r.LeaveType.Length == 0)
                    {
                        //No Leave
                        WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn) + ROTDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                        {
                            // Menghitung lama waktu istirahat
                            if (r.OneDayInd)
                            {
                                // Shift 1, 2
                                if (Sm.CompareDtTm(r.WorkingOut, r.Dt+r.WSIn2) > 0)
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                            }
                            else
                            {
                                //Shift 3
                                if (Sm.CompareDtTm(r.WSIn2, r.WSOut2)<=0)
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                                else
                                    BreakDuration = ComputeDuration(Dt2 + r.WSOut2, r.Dt + r.WSIn2);
                            }
                        }
                        WorkingDuration -= BreakDuration;
                    }
                    else
                    {
                        //Hourly Leave
                        string
                            LeaveStartDtTm = string.Empty,
                            LeaveEndDtTm = string.Empty,
                            BreakStartDtTm = string.Empty,
                            BreakEndDtTm = string.Empty;

                        if (r.OneDayInd)
                        {
                            //shift 1, 2
                            LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                            LeaveEndDtTm = r.Dt + r.LeaveEndTm;
                            BreakStartDtTm = r.Dt + r.WSIn2;
                            BreakEndDtTm = r.Dt + r.WSOut2;
                        }
                        else
                        {
                            //Shift 3
                            if (Sm.CompareDtTm(r.LeaveStartTm, r.WSIn1) < 0)
                            {
                                LeaveStartDtTm = Dt2 + r.LeaveStartTm;
                                LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                            }
                            else
                            {
                                LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                                if (Sm.CompareDtTm(r.LeaveEndTm, r.WSOut1) <= 0)
                                    LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                                else
                                    LeaveEndDtTm = Dt + r.LeaveEndTm;
                            }

                            if (Sm.CompareDtTm(r.WSIn2, r.WSIn1) < 0)
                                BreakStartDtTm = Dt2 + r.WSIn2;
                            else
                                BreakStartDtTm = Dt + r.WSIn2;
                            
                            if (Sm.CompareDtTm(r.WSOut2, r.WSOut1) < 0)
                                BreakEndDtTm = Dt2 + r.WSOut2;                            
                            else
                                BreakEndDtTm = Dt + r.WSOut2;
                        }
                        if (r.OneDayInd)
                        {
                            if (r.WSOut3.Length > 0)
                            {
                                if (mSalaryInd == "2")
                                {
                                    WorkingDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn1) + ROTDuration;
                                }
                                else
                                {
                                    WorkingDuration = ComputeDuration(Dt + r.WSOut1, Dt + r.WSIn1);
                                }
                            }
                            else
                                WorkingDuration = ComputeDuration(Dt + r.WSOut1, Dt + r.WSIn1);
                        }
                        else
                        {
                            if (r.WSOut3.Length > 0)
                            {
                                if (mSalaryInd == "2")
                                {
                                    WorkingDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn1) + ROTDuration;
                                }
                                else
                                {
                                    WorkingDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn1);
                                }
                            }
                            else
                                WorkingDuration = ComputeDuration(Dt2 + r.WSOut1, Dt + r.WSIn1);
                        }
                        //WorkingDuration += ROTDuration;
                        WorkingDuration -= r.LeaveDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                            WorkingDuration -= ComputeDuration(BreakEndDtTm, BreakStartDtTm);
                    }
                }
            }

            if (WorkingDuration > 1000)
            {
                var a = "";
            }

            r.WorkingDuration = Convert.ToDecimal(WorkingDuration);

            #endregion

            #region set leave duration (fullday leave)

            if (r.LeaveType == "F")
            {
                if (!r.WSHolidayInd && !r.HolInd)
                    r.LeaveDuration = mWorkingHr;
                else
                {
                    r.LeaveCode = string.Empty;
                    r.LeaveType = string.Empty;
                    r.PaidLeaveInd = false;
                    r.CompulsoryLeaveInd = false;
                    r.LeaveDuration = 0m;
                }
            }
            
            #endregion

            #region Menghitung OT

            decimal TotalOTHr = 0m;

            if (r.SystemType == mEmpSystemTypeHarian)
            {
                decimal OTHr = 0m;
                string 
                    StartTm = string.Empty, 
                    EndTm = string.Empty;

                if (mOTRequestType=="2")
                {
                    #region Use OT Request

                    TotalOTHr += ROTDuration;
                    foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                    {
                        OTHr = 0m;
                        StartTm = x.Tm1;
                        EndTm = x.Tm2;

                        if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                        {
                            if (EndTm == "0000")
                                OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                            else
                            {
                                if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                    OTHr = ComputeDuration(Dt + EndTm, Dt + StartTm);
                                else
                                    OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                            }
                        }
                        if (OTHr < 0) OTHr = 0;
                        TotalOTHr += OTHr;
                    }

                    #endregion
                }
                else
                {
                    bool
                        IsOTAdjusted = false,
                        IsOTSpecial = false; // beda tanggal

                    string
                        StartTmTemp = string.Empty,
                        EndTmTemp = string.Empty,
                        OTDtTemp = r.OneDayInd ? Dt : Dt2,
                        SpecialStartDt = string.Empty,
                        SpecialStartTm = string.Empty,
                        SpecialEndDt = string.Empty,
                        SpecialEndTm = string.Empty
                        ;

                    foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                    {
                        IsOTAdjusted = false;
                        OTHr = 0m;
                        StartTm = x.Tm1;
                        EndTm = x.Tm2;

                        #region Use OT Adjustment

                        foreach (var ota in mlOTAdjustment.Where(
                            ota =>
                                ota.OTDocNo == x.OTDocNo &&
                                ota.EmpCode == EmpCode &&
                                ota.Dt == Dt
                                ))
                        {
                            IsOTAdjusted = true;
                            StartTm = ota.Tm1;
                            EndTm = ota.Tm2;
                        }

                        #endregion

                        #region Use Attendance Barcode

                        if (!IsOTAdjusted)
                        {
                            StartTmTemp = string.Empty;
                            EndTmTemp = string.Empty;

                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            var IsOTOneDayInd = true;
                            var Tm1Temp = string.Empty;

                            if (r.OneDayInd)
                            {
                                #region Shift 1 & 2

                                if (Sm.CompareDtTm(StartTm, EndTm) > 0)
                                {
                                    #region Case 1

                                    IsOTSpecial = true;

                                    foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt &&
                                                            Sm.CompareDtTm(Dt + "0000", (i.Dt + i.Tm)) <= 0
                                                        )
                                                    .OrderBy(o1 => o1.Dt)
                                                    .ThenBy(o2 => o2.Tm)
                                                    .Take(1)
                                                    )
                                    {
                                        SpecialStartDt = index.Dt;
                                        SpecialStartTm = index.Tm;
                                    }

                                    if (
                                        SpecialStartDt.Length > 0 &&
                                        SpecialStartTm.Length > 0 &&
                                        Sm.CompareDtTm(SpecialStartDt + SpecialStartTm, Dt + StartTm) <= 0
                                        )
                                    {
                                        SpecialStartDt = Dt;
                                        SpecialStartTm = StartTm;
                                    }

                                    if (SpecialStartDt.Length > 0 && SpecialStartTm.Length > 0)
                                    {
                                        foreach (var index in mlAttendanceLog
                                               .Where(
                                                   i =>
                                                       i.EmpCode == EmpCode &&
                                                       (i.Dt == Dt || i.Dt == Dt2) &&
                                                       Sm.CompareDtTm(Dt + StartTm, (i.Dt + i.Tm)) < 0 &&
                                                       Sm.CompareDtTm((i.Dt + i.Tm), Dt2 + EndTm) <= 0
                                                   )
                                               .OrderByDescending(o1 => o1.Dt)
                                               .ThenByDescending(o2 => o2.Tm)
                                               .Take(1)
                                               )
                                        {
                                            SpecialEndDt = index.Dt;
                                            SpecialEndTm = index.Tm;
                                        }


                                        if (SpecialEndDt.Length <= 0 || SpecialEndTm.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                   .Where(
                                                       i =>
                                                           i.EmpCode == EmpCode &&
                                                           i.Dt == Dt2 &&
                                                           Sm.CompareDtTm(Dt2 + EndTm, i.Dt + i.Tm) <= 0
                                                       )
                                                   .OrderBy(o1 => o1.Dt)
                                                   .ThenBy(o2 => o2.Tm)
                                                   .Take(1)
                                                   )
                                            {
                                                SpecialEndDt = index.Dt;
                                                SpecialEndTm = index.Tm;
                                            }

                                            if (
                                                SpecialEndDt.Length > 0 &&
                                                SpecialEndTm.Length > 0 &&
                                                Sm.CompareDtTm(Dt2 + EndTm, SpecialEndDt + SpecialEndTm) <= 0
                                            )
                                            {
                                                SpecialEndDt = Dt2;
                                                SpecialEndTm = EndTm;
                                            }
                                        }
                                    }
                                    StartTmTemp = SpecialStartTm;
                                    EndTmTemp = SpecialEndTm;

                                    #endregion
                                }

                                if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                {
                                    #region case 2
                                    // Standard

                                    foreach (var index in mlAttendanceLog
                                    .Where(
                                        i =>
                                            i.EmpCode == EmpCode &&
                                            i.Dt == Dt &&
                                            Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                        )
                                    .OrderByDescending(o => o.Tm)
                                    .Take(1)
                                    )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }

                                    if (StartTmTemp.Length > 0)
                                    {
                                        if (x.Tm2 == "0000")
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, "2359") <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            if (EndTmTemp.Length <= 0)
                                            {
                                                foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt2 &&
                                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                        )
                                                    .OrderBy(o => o.Tm)
                                                    .Take(1)
                                                    )
                                                {
                                                    EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (var index in mlAttendanceLog
                                            .Where(
                                                i =>
                                                    i.EmpCode == EmpCode &&
                                                    i.Dt == Dt &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm1) > 0 &&
                                                    Sm.CompareDtTm(i.Tm, x.Tm2) <= 0
                                                )
                                            .OrderByDescending(o => o.Tm)
                                            .Take(1)
                                            )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }

                                            //if (EndTmTemp.Length <= 0)
                                            //{
                                                foreach (var index in mlAttendanceLog
                                                    .Where(
                                                        i =>
                                                            i.EmpCode == EmpCode &&
                                                            i.Dt == Dt &&
                                                            Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                        )
                                                    .OrderBy(o => o.Tm)
                                                    .Take(1)
                                                    )
                                                {
                                                    EndTmTemp = Sm.Left(index.Tm, 4);
                                                }
                                            //}
                                            if (EndTmTemp.Length <= 0)
                                            {
                                                EndTmTemp = Sm.Left(x.Tm2, 4);
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                #region Shift 3
                                if (Sm.CompareDtTm(r.WSOut1, x.Tm1) <= 0)
                                {
                                    if (Sm.CompareDtTm(r.WSOut1, ActualTmOut) <= 0)
                                        StartTmTemp = r.WSOut1;
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            StartTmTemp = Sm.Left(index.Tm, 4);
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                i.Dt == OTDtTemp &&
                                                Sm.CompareDtTm(i.Tm, x.Tm1) <= 0
                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                    {
                                        StartTmTemp = Sm.Left(index.Tm, 4);
                                    }
                                }

                                if (StartTmTemp.Length > 0)
                                {
                                    if (x.Tm2 == "0000")
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + "2359") <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            var OTDtTemp2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp2 &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = x.Tm2; // Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var index in mlAttendanceLog
                                        .Where(
                                            i =>
                                                i.EmpCode == EmpCode &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm2) <= 0 &&
                                                Sm.CompareDtTm(i.Dt + i.Tm, OTDtTemp + x.Tm1) > 0

                                            )
                                        .OrderByDescending(o => o.Tm)
                                        .Take(1)
                                        )
                                        {
                                            EndTmTemp = Sm.Left(index.Tm, 4);
                                        }

                                        if (EndTmTemp.Length <= 0)
                                        {
                                            foreach (var index in mlAttendanceLog
                                                .Where(
                                                    i =>
                                                        i.EmpCode == EmpCode &&
                                                        i.Dt == OTDtTemp &&
                                                        Sm.CompareDtTm(i.Tm, x.Tm2) > 0
                                                    )
                                                .OrderBy(o => o.Tm)
                                                .Take(1)
                                                )
                                            {
                                                EndTmTemp = Sm.Left(index.Tm, 4);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (StartTmTemp.Length <= 0 || EndTmTemp.Length <= 0)
                            {
                                StartTm = string.Empty;
                                EndTm = string.Empty;
                            }
                            else
                            {
                                if (!IsOTSpecial)
                                {
                                    StartTm = x.Tm1;
                                    if (x.Tm2 == "0000")
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) != 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                    else
                                    {
                                        if (Sm.CompareDtTm(EndTmTemp, x.Tm2) < 0)
                                            EndTm = EndTmTemp;
                                        else
                                            EndTm = x.Tm2;
                                    }
                                }
                            }
                        }

                        #endregion

                        if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                        {
                            //Value1<Value2 : -1
                            //Value1=Value2 : 0
                            //Value1>Value2 : 1
                            if (IsOTSpecial)
                            {
                                OTHr = ComputeDuration(SpecialEndDt + SpecialEndTm, SpecialStartDt + SpecialStartTm);
                            }
                            else
                            {
                                if (EndTm == "0000")
                                {
                                    if (r.OneDayInd)
                                        OTHr = ComputeDuration(Dt2 + EndTm, OTDtTemp + StartTm);
                                    else
                                    {
                                        var Dt3 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(2)).Substring(0, 8);
                                        OTHr = ComputeDuration(Dt3 + EndTm, OTDtTemp + StartTm);
                                    }
                                }
                                else
                                    OTHr = ComputeDuration(OTDtTemp + EndTm, OTDtTemp + StartTm);
                            }
                        }
                        if (OTHr < 0) OTHr = 0;
                        TotalOTHr += OTHr;
                    }
                }
            }

            if (mSalaryInd == "2")
            {
                if (mIsOTHolidayDeduct1Hr && (r.HolInd || r.WSHolidayInd))
                    if (TotalOTHr>=4) TotalOTHr = TotalOTHr-1;
            }

            #endregion

            #region OT menggantikan leave

            if (mIsUseOTReplaceLeave)
            {
                var NonROTDuration = RoundTo30Minutes((double)(TotalOTHr));
                TotalOTHr = RoundTo30Minutes((double)(TotalOTHr + ROTDuration));
                if (TotalOTHr > 0m && r.LeaveDuration > 0m)
                {
                    r.OTToLeaveInd = true;
                    if (r.LeaveType != "F")
                    {
                        r.WorkingDuration = r.WorkingDuration + r.LeaveDuration;
                        if (r.LeaveDuration >= NonROTDuration)
                        {
                            r.LeaveDuration -= NonROTDuration;
                            NonROTDuration = 0m;
                        }
                        else
                        {
                            NonROTDuration -= r.LeaveDuration;
                            r.LeaveDuration = 0;
                        }
                        r.WorkingDuration = r.WorkingDuration - r.LeaveDuration;

                        if (r.WSOut3.Length > 0)
                        {
                            if (Sm.CompareDtTm(r.WSIn3, r.WSOut3) <= 0)
                                ROTDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn3);
                            else
                                ROTDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn3);
                        }
                        else
                            ROTDuration = 0m;
                        
                        if (r.LeaveDuration >= ROTDuration)
                        {
                            r.LeaveDuration -= ROTDuration;
                            ROTDuration = 0m;
                        }
                        else
                        {
                            ROTDuration -= r.LeaveDuration;
                            r.LeaveDuration = 0;
                        }
                        TotalOTHr = RoundTo30Minutes((double)(NonROTDuration + ROTDuration));
                    }
                }
            }

            #endregion

            #region Set leave information (PLDay, PLHr, PLAmt)

            if (r.PaidLeaveInd && r.LeaveType.Length>0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.PLDay = 1;
                }
                else
                    r.PLHr = r.LeaveDuration;

                if (r.LeaveDuration != 0m && mWorkingHr != 0m)
                {
                    r.PLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r.PLAmt > 0)
                    r.ProcessPLAmt = r.PLAmt;
            }

            if (!r.PaidLeaveInd && r.LeaveType.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.UPLDay = 1;
                }
                else
                    r.UPLHr = r.LeaveDuration;

                if (r.LeaveDuration != 0m && mWorkingHr != 0m) 
                    r.UPLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                
                if (r._DeductTHPInd)
                    r.ProcessUPLAmt = r.UPLAmt;
            }

            #endregion

            #region Menghitung OT Amount

            if (r.SystemType == mEmpSystemTypeHarian && r.EmpSalary != 0m && TotalOTHr >= 0.5m)
            {
                decimal TotalOTHrTemp = TotalOTHr;
                if (r.HolInd || r.WSHolidayInd)
                {
                    r.OTHolidayHr = TotalOTHrTemp;

                    int i = 1;

                    while (TotalOTHrTemp > 0)
                    {
                        if (i == 1)
                        {
                            if (TotalOTHrTemp >= mHoliday1stHrForHO)
                            {
                                r.OTHolidayAmt = mHoliday1stHrForHO * mHoliday1stHrIndex * ((r.EmpSalary-r.SalaryPension) / 173);
                                TotalOTHrTemp -= mHoliday1stHrForHO;
                            }
                            else
                            {
                                r.OTHolidayAmt = TotalOTHrTemp * mHoliday1stHrIndex * ((r.EmpSalary - r.SalaryPension) / 173);
                                TotalOTHrTemp = 0m;
                            }
                        }
                        if (i == 2)
                        {
                            if (TotalOTHrTemp >= mHoliday2ndHrForHO)
                            {
                                r.OTHolidayAmt += (mHoliday2ndHrForHO * mHoliday2ndHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
                                TotalOTHrTemp -= mHoliday2ndHrForHO;
                            }
                            else
                            {
                                r.OTHolidayAmt += (TotalOTHrTemp * mHoliday2ndHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
                                TotalOTHrTemp = 0m;
                            }
                        }
                        if (i == 3)
                        {
                            r.OTHolidayAmt += (TotalOTHrTemp * mHoliday3rdHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
                            TotalOTHrTemp = 0m;
                        }
                        i++;
                    }
            
                }
                else
                {
                    if (TotalOTHrTemp <= mOTFormulaHr1)
                    {
                        r.OT1Hr = TotalOTHrTemp;
                        r.OT1Amt = TotalOTHrTemp * mOTFormulaIndex1 * ((r.EmpSalary - r.SalaryPension)/ 173);
                        TotalOTHrTemp = 0m;
                    }
                    else
                    {
                        r.OT1Hr = mOTFormulaHr1;
                        r.OT1Amt = mOTFormulaHr1 * mOTFormulaIndex1 * ((r.EmpSalary - r.SalaryPension)/ 173);
                        TotalOTHrTemp -= mOTFormulaHr1;
                    }

                    if (TotalOTHrTemp>0m && TotalOTHrTemp <= mOTFormulaHr2)
                    {
                        r.OT2Hr = TotalOTHrTemp;
                        r.OT2Amt = TotalOTHrTemp * mOTFormulaIndex2 * ((r.EmpSalary - r.SalaryPension)/ 173);
                        TotalOTHrTemp = 0m;
                    }
                }
            }

            #endregion

            #region Compute Transport/Meal

            if (r.WorkingDuration > 0)
            {
                if (mADCodeTransport.Length > 0)
                {
                    foreach (
                        var t in mEmployeeMealTransport.Where(Index =>
                            Sm.CompareStr(Index.EmpCode, EmpCode) &&
                            Sm.CompareStr(Index.ADCode, mADCodeTransport) &&
                            Sm.CompareDtTm(Index.StartDt, Dt)<=0 &&
                            Sm.CompareDtTm(Dt, Index.EndDt)<=0
                            ))
                    {
                        r.Transport = t.Amt;
                        if (mIsWSDoubleShiftTwiceAllowance && r._DoubleShiftInd)
                            r.Transport = 2m * t.Amt;
                    }
                }

                if (mADCodeMeal.Length > 0)
                {
                    foreach (
                        var m in mEmployeeMealTransport.Where(Index =>
                            Sm.CompareStr(Index.EmpCode, EmpCode) &&
                            Sm.CompareStr(Index.ADCode, mADCodeMeal) &&
                            Sm.CompareDtTm(Index.StartDt, Dt)<=0 &&
                            Sm.CompareDtTm(Dt, Index.EndDt)<=0
                            ))
                    {
                        r.Meal = m.Amt;
                        if (mIsWSDoubleShiftTwiceAllowance && r._DoubleShiftInd)
                            r.Meal = 2m * m.Amt;
                    }
                }
            }

            #endregion

            #region Unprocessed data

            if (!r.ProcessInd)
            {
                r._LatestMonthlyGrdLvlSalary = 0m;
                r._LatestDailyGrdLvlSalary = 0m;
                r._IsFullDayInd = false;
                r.ActualIn = string.Empty;
                r.ActualOut = string.Empty;
                r.WorkingIn = string.Empty;
                r.WorkingOut = string.Empty;
                r.WorkingDuration = 0m;
                r.ProductionWages = 0m;
                r.LeaveCode = string.Empty;
                r.LeaveType = string.Empty;
                r.PaidLeaveInd = false;
                r.CompulsoryLeaveInd = false;
                r.LeaveStartTm = string.Empty;
                r.LeaveEndTm = string.Empty;
                r.LeaveDuration = 0m;
                r.PLDay = 0m;
                r.PLHr = 0m;
                r.PLAmt  = 0m;
                r.UPLDay = 0m;
                r.UPLHr = 0m;
                r.UPLAmt = 0m;
                r.ProcessPLAmt = 0m;
                r.OT1Hr  = 0m;
                r.OT2Hr  = 0m;
                r.OTHolidayHr  = 0m;
                r.OT1Amt = 0m;
                r.OT2Amt  = 0m;
                r.OTHolidayAmt  = 0m;
                r.OTToLeaveInd  = false;
                r.IncMinWages  = 0m;
                r.IncPerformance  = 0m;
                r.FieldAssignment = 0m;
                r.Meal = 0m;
                r.Transport = 0m;
                //r.ServiceChargeIncentive = 0m;
                r.DedProduction  = 0m;
                r.DedProdLeave  = 0m;
                r.IncEmployee = 0m;
            }

            #endregion
        }

        private void Process5(ref List<Result2> r2)
        {
            string
                PayrunCode = mPayrunCode,
                Filter = string.Empty;
            var cm = new MySqlCommand();

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                if (Filter.Length>0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), mlPayrunEmployee[i].EmpCode);                        
            }
            
            if (Filter.Length>0)
                Filter = " (" + Filter + ")";
            else
                Filter = " (0=1) ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, A.NPWP, A.PTKP, ");
            SQL.AppendLine("IfNull(B.DedEmployee, 0) As DedEmployee, ");
            SQL.AppendLine("IfNull(C.SSEmployerHealth, 0) As SSEmployerHealth, ");
            SQL.AppendLine("IfNull(C.SSEmployeeHealth, 0) As SSEmployeeHealth, ");
            SQL.AppendLine("IfNull(D.SSEmployerEmployment, 0) As SSEmployerEmployment, ");
            SQL.AppendLine("IfNull(D.SSEmployeeEmployment, 0) As SSEmployeeEmployment, ");
            SQL.AppendLine("IfNull(E.SalaryAdjustment, 0) As SalaryAdjustment, ");
            SQL.AppendLine("IfNull(F.NonTaxableFixedAllowance, 0) As NonTaxableFixedAllowance, ");
            SQL.AppendLine("IfNull(G.TaxableFixedAllowance, 0) As TaxableFixedAllowance, ");
            SQL.AppendLine("IfNull(H.NonTaxableFixedDeduction, 0) As NonTaxableFixedDeduction, ");
            SQL.AppendLine("IfNull(I.TaxableFixedDeduction, 0) As TaxableFixedDeduction, ");
            SQL.AppendLine("IfNull(J.EmploymentPeriodAllowance, 0) As EmploymentPeriodAllowance, ");
            SQL.AppendLine("IfNull(K.SSEmployerPension, 0.00) As SSEmployerPension, ");
            SQL.AppendLine("IfNull(K.SSEmployeePension, 0.00) As SSEmployeePension, ");
            SQL.AppendLine("IfNull(L.SSEmployerPension2, 0.00) As SSEmployerPension2, ");
            SQL.AppendLine("IfNull(L.SSEmployeePension2, 0.00) As SSEmployeePension2, ");
            SQL.AppendLine("IfNull(M.SSErLifeInsurance, 0.00) As SSErLifeInsurance, ");
            SQL.AppendLine("IfNull(M.SSEeLifeInsurance, 0.00) As SSEeLifeInsurance, ");
            SQL.AppendLine("IfNull(N.SSErWorkingAccident, 0.00) As SSErWorkingAccident, ");
            SQL.AppendLine("IfNull(N.SSEeWorkingAccident, 0.00) As SSEeWorkingAccident, ");
            SQL.AppendLine("IfNull(O.SSErRetirement, 0.00) As SSErRetirement, ");
            SQL.AppendLine("IfNull(O.SSEeRetirement, 0.00) As SSEeRetirement, ");
            SQL.AppendLine("IfNull(P.SSErPension, 0.00) As SSErPension, ");
            SQL.AppendLine("IfNull(P.SSEePension, 0.00) As SSEePension, ");
            SQL.AppendLine("IfNull(Q.Functional, 0.00) As Functional, ");
            SQL.AppendLine("IfNull(R.Housing, 0.00) As Housing, ");
            SQL.AppendLine("IfNull(S.MobileCredit, 0.00) As MobileCredit, ");
            SQL.AppendLine("IfNull(T.Zakat, 0.00) As Zakat, ");
            SQL.AppendLine("IfNull(U.AnnualLeaveAllowance, 0.00) As AnnualLeaveAllowance, ");
            SQL.AppendLine("IfNull(V.Amt, 0.00) As ServiceChargeIncentive, ");
            SQL.AppendLine("Case When W.EmpCode Is Null Then 'N' Else 'Y' End As PensionInd, ");
            SQL.AppendLine("IfNull(X.NontaxableSalaryAdjustment, 0.00) As NontaxableSalaryAdjustment, ");
            SQL.AppendLine("IfNull(Y.TaxableSalaryAdjustment, 0.00) As TaxableSalaryAdjustment ");

            SQL.AppendLine("From TblEmployee A ");

            //Deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.AmtInspnt) As DedEmployee ");
            SQL.AppendLine("    From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpInsPntDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("    Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("    Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='02' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode ");

            //SS Health
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerHealth, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForHealth' And ParValue Is Not Null ");
            SQL.AppendLine("            ) ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");

            //SS Employment (dikurangi BPJS Pensiun)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerEmployment, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        And T2.SSCode Not In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue Is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForEmployment' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");

            //Salary Adjustment
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As SalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr A ");
            SQL.AppendLine("        Where A.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.EmpCode, A.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustment2Hdr A ");
            SQL.AppendLine("        Inner Join TblSalaryAdjustment2Dtl B ");
            SQL.AppendLine("            On A.DocNo=B.DocNo ");
            SQL.AppendLine("            And (B.PayrunCode Is Null Or (B.PayrunCode Is Not Null And B.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("            And (" + Filter.Replace("A.", "B.") + ") ");
            SQL.AppendLine("        Where A.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");

            //NonTaxable Fixed Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As NonTaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='N' And B.AmtType='1' ");
            //SQL.AppendLine("    Where (A.StartDt Is Null Or (A.StartDt Is Not Null And A.StartDt<=@EndDt)) ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");

            //Taxable Fixed Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='Y' And B.AmtType='1' ");
            //SQL.AppendLine("    Where (A.StartDt Is Null Or (A.StartDt Is Not Null And A.StartDt<=@EndDt)) ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");

            //NonTaxable Fixed Deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As NonTaxableFixedDeduction ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.TaxInd='N' And B.AmtType='1' ");
            //SQL.AppendLine("    Where (A.StartDt Is Null Or (A.StartDt Is Not Null And A.StartDt<=@EndDt)) ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") H On A.EmpCode=H.EmpCode ");

            //Taxable Fixed Deduction
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedDeduction ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.TaxInd='Y' And B.AmtType='1' ");
            //SQL.AppendLine("    Where (A.StartDt Is Null Or (A.StartDt Is Not Null And A.StartDt<=@EndDt)) ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") I On A.EmpCode=I.EmpCode ");

            //Employment Period Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As EmploymentPeriodAllowance ");
            SQL.AppendLine("    From TblEmpAD T ");
            SQL.AppendLine("    Where (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    And (T.Dt Is Not Null And T.Dt Between @StartDt And @EndDt) ");
            SQL.AppendLine("    And T.CancelInd='N' ");
            SQL.AppendLine("    And (" + Filter.Replace("A.", "T.") + ") ");
            SQL.AppendLine("    Group By T.EmpCode ");
            SQL.AppendLine(") J On A.EmpCode=J.EmpCode ");

            //Pension 1 (P-HII)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, Sum(T2.EmployerAmt) As SSEmployerPension, Sum(T2.EmployeeAmt) As SSEmployeePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSCodePension' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") K On A.EmpCode=K.EmpCode ");

            //Pension 2 (P-NAT)
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerPension2, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeePension2 ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSCodePension2' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") L On A.EmpCode=L.EmpCode ");

            //BPJS Jaminan Kematian
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErLifeInsurance, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeLifeInsurance ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSLifeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") M On A.EmpCode=M.EmpCode ");

            //BPJS Jaminan Kecelakaan Kerja
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErWorkingAccident, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeWorkingAccident ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSWorkingAccident' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") N On A.EmpCode=N.EmpCode ");

            //BPJS Jaminan Hari Tua
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErRetirement, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeRetirement ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSOldAgeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") O On A.EmpCode=O.EmpCode ");

            //BPJS Jaminan Pensiun
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErPension, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") P On A.EmpCode=P.EmpCode ");

            //Functional
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Functional ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeFunctional' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") Q On A.EmpCode=Q.EmpCode ");

            //Housing
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Housing ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeHousing' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") R On A.EmpCode=R.EmpCode ");

            //Mobile Credit
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As MobileCredit ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeMobileCredit' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") S On A.EmpCode=S.EmpCode ");

            //Zakat
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Zakat ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeZakat' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") T On A.EmpCode=T.EmpCode ");

            //Annual Leave Allowance
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As AnnualLeaveAllowance ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select B.EmpCode, B.Amt ");
            SQL.AppendLine("        From TblAnnualLeaveAllowanceHdr A ");
            SQL.AppendLine("        Inner Join TblAnnualLeaveAllowanceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And (B.PayrunCode Is Null Or (B.PayrunCode Is Not Null And B.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "B.") + ") ");
            SQL.AppendLine("        Where A.Status='A' ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") U On A.EmpCode=U.EmpCode ");

            //Service Charge Incentive
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.EmpCode, Sum(B.Amt) As Amt ");
            SQL.AppendLine("    From TblEmpSCIHdr A ");
            SQL.AppendLine("    Inner Join TblEmpSCIDtl B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And (B.PayrunCode Is Null Or (B.PayrunCode Is Not Null And B.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "B.") + ") ");
            SQL.AppendLine("    Where A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    Group By B.EmpCode ");
            SQL.AppendLine(") V On A.EmpCode=V.EmpCode ");

            //Pension Indicator
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.EmpCode ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Inner Join TblPPS B ");
            SQL.AppendLine("        On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("        And B.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status='A' ");
            SQL.AppendLine("        And B.JobTransfer='T' ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And A.ResignDt Is Not Null ");
            SQL.AppendLine("    And A.ResignDt Between @StartDt And @EndDt ");
            SQL.AppendLine(") W On A.EmpCode=W.EmpCode ");

            //Nontaxable Salary Adjustment
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As NontaxableSalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr A ");
            SQL.AppendLine("        Where A.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Nontaxable='Y' ");
            SQL.AppendLine("        And (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") X On A.EmpCode=X.EmpCode ");

            //Taxable Salary Adjustment
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As TaxableSalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.EmpCode, A.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr A ");
            SQL.AppendLine("        Where A.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Nontaxable='N' ");
            SQL.AppendLine("        And (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter + ") ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") Y On A.EmpCode=Y.EmpCode ");

            SQL.AppendLine("Where " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
                Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode", 

                    //1-5
                    "JoinDt", 
                    "ResignDt", 
                    "NPWP", 
                    "PTKP", 
                    "DedEmployee", 

                    //6-10
                    "SSEmployerHealth", 
                    "SSEmployeeHealth", 
                    "SSEmployerEmployment", 
                    "SSEmployeeEmployment",
                    "SalaryAdjustment", 

                    //11-15
                    "NonTaxableFixedAllowance", 
                    "TaxableFixedAllowance", 
                    "NonTaxableFixedDeduction", 
                    "TaxableFixedDeduction",
                    "EmploymentPeriodAllowance",

                    //16-20
                    "SSEmployerPension",
                    "SSEmployeePension",
                    "SSEmployerPension2", 
                    "SSEmployeePension2",
                    "SSErLifeInsurance",

                    //21-25
                    "SSEeLifeInsurance",
                    "SSErWorkingAccident",
                    "SSEeWorkingAccident",
                    "SSErRetirement",
                    "SSEeRetirement",

                    //26-30
                    "SSErPension",
                    "SSEePension",
                    "Functional",
                    "Housing",
                    "MobileCredit",

                    //31-35
                    "Zakat",
                    "AnnualLeaveAllowance",
                    "ServiceChargeIncentive",
                    "PensionInd",
                    "NontaxableSalaryAdjustment",

                    //36
                    "TaxableSalaryAdjustment"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r2.Add(new Result2()
                        {
                            PayrunCode = PayrunCode,
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            NPWP = Sm.DrStr(dr, c[3]),
                            PTKP = Sm.DrStr(dr, c[4]),
                            DedEmployee = Sm.DrDec(dr, c[5]),
                            SSEmployerHealth = Sm.DrDec(dr, c[6]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[7]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[8]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[9]),
                            SalaryAdjustment = Sm.DrDec(dr, c[10]),
                            NonTaxableFixAllowance = Sm.DrDec(dr, c[11]),
                            TaxableFixAllowance = Sm.DrDec(dr, c[12]),
                            NonTaxableFixDeduction = Sm.DrDec(dr, c[13]),
                            TaxableFixDeduction = Sm.DrDec(dr, c[14]),
                            FixAllowance = Sm.DrDec(dr, c[11]) + Sm.DrDec(dr, c[12]),
                            FixDeduction = Sm.DrDec(dr, c[13]) + Sm.DrDec(dr, c[14]),
                            EmploymentPeriodAllowance = Sm.DrDec(dr, c[15]),
                            SSEmployerPension = Sm.DrDec(dr, c[16]),
                            SSEmployeePension = Sm.DrDec(dr, c[17]),
                            SSEmployerPension2 = Sm.DrDec(dr, c[18]),
                            SSEmployeePension2 = Sm.DrDec(dr, c[19]),
                            SSErLifeInsurance= Sm.DrDec(dr, c[20]),
                            SSEeLifeInsurance= Sm.DrDec(dr, c[21]),
                            SSErWorkingAccident= Sm.DrDec(dr, c[22]),
                            SSEeWorkingAccident= Sm.DrDec(dr, c[23]),
                            SSErRetirement= Sm.DrDec(dr, c[24]),
                            SSEeRetirement = Sm.DrDec(dr, c[25]),
                            SSErPension = Sm.DrDec(dr, c[26]),
                            SSEePension = Sm.DrDec(dr, c[27]),
                            Functional = Sm.DrDec(dr, c[28]),
                            Housing = Sm.DrDec(dr, c[29]),
                            MobileCredit = Sm.DrDec(dr, c[30]),
                            Zakat = Sm.DrDec(dr, c[31]),
                            ADLeave = Sm.DrDec(dr, c[32]),
                            ServiceChargeIncentive = Sm.DrDec(dr, c[33]),
                            PensionInd = Sm.DrStr(dr, c[34]) == "Y",
                            NontaxableSalaryAdjustment = Sm.DrDec(dr, c[35]),
                            TaxableSalaryAdjustment = Sm.DrDec(dr, c[36]),
                            DedProduction = 0m,
                            IncProduction = 0m,
                            SSEmployerNonRetiring = 0m,
                            SSEmployeeOldAgeInsurance = 0m,
                            Salary = 0m,
                            WorkingDay = 0m,
                            PLDay = 0m,
                            PLHr = 0m,
                            PLAmt = 0m,
                            UPLDay = 0m,
                            UPLHr = 0m,
                            UPLAmt = 0m,
                            OT1Hr = 0m,
                            OT2Hr = 0m,
                            OTHolidayHr = 0m,
                            OT1Amt = 0m,
                            OT2Amt = 0m,
                            OTHolidayAmt = 0m,
                            IncMinWages = 0m,
                            IncPerformance = 0m,
                            PresenceReward = 0m,
                            HolidayEarning = 0m,
                            ExtraFooding = 0m,
                            FieldAssignment = 0m,
                            DedProdLeave = 0m,
                            IncEmployee = 0m,
                            SalaryPension = 0m,
                            CreditCode1 = string.Empty,
                            CreditCode2 = string.Empty,
                            CreditCode3 = string.Empty,
                            CreditCode4 = string.Empty,
                            CreditCode5 = string.Empty,
                            CreditCode6 = string.Empty,
                            CreditCode7 = string.Empty,
                            CreditCode8 = string.Empty,
                            CreditCode9 = string.Empty,
                            CreditCode10 = string.Empty,
                            CreditAdvancePayment1 = 0m,
                            CreditAdvancePayment2 = 0m,
                            CreditAdvancePayment3 = 0m,
                            CreditAdvancePayment4 = 0m,
                            CreditAdvancePayment5 = 0m,
                            CreditAdvancePayment6 = 0m,
                            CreditAdvancePayment7 = 0m,
                            CreditAdvancePayment8 = 0m,
                            CreditAdvancePayment9 = 0m,
                            CreditAdvancePayment10 = 0m,
                            TaxAllowance = 0m,
                            Tax = 0m,
                            Amt = 0m,
                            _IsHOInd = true
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            var EmpCode = r.EmpCode;
            bool LatestIsHOInd = false;
            decimal
                LatestSalary = 0m,
                LatestSalary2 = 0m,
                LatestSalaryPension = 0m
                ;

            foreach (var x in lResult1.Where(x => x.EmpCode == EmpCode && x.ProcessInd).OrderBy(Index=>Index.Dt))
            {
                LatestSalary = x.EmpSalary;
                LatestSalary2 = x.EmpSalary2;
                LatestSalaryPension = x.SalaryPension;
                LatestIsHOInd = x._IsHOInd;

                if (x.ProcessInd)
                {
                    //r.Salary += x.Salary;
                    r.PLDay += x.PLDay;
                    r.PLHr += x.PLHr;
                    r.PLAmt += x.PLAmt;
                    r.UPLDay += x.UPLDay;
                    r.UPLHr += x.UPLHr;
                    r.UPLAmt += x.UPLAmt;
                    r.ProcessPLAmt += x.ProcessPLAmt;
                    r.ProcessUPLAmt += x.ProcessUPLAmt;
                    r.OT1Hr += x.OT1Hr;
                    r.OT2Hr += x.OT2Hr;
                    r.OTHolidayHr += x.OTHolidayHr;
                    r.OT1Amt += x.OT1Amt;
                    r.OT2Amt += x.OT2Amt;
                    r.OTHolidayAmt += x.OTHolidayAmt;
                    r.FieldAssignment += x.FieldAssignment;
                    r.Transport += x.Transport;
                    r.Meal += x.Meal;
                    //r.ServiceChargeIncentive += x.ServiceChargeIncentive;
                    r.IncEmployee += x.IncEmployee;

                    if (x.WorkingDuration > 0) r.WorkingDay += 1;
                }
            }

            //untuk menghitung salary dan fixed allowance apabila pada 1 period terdiri dari lbh dari 1 payrun

            //kalau resign.
            //kalau payrun info berbeda dalam 1 periode.
            //kalau sudah pernah dibayar.

            Boolean IsPayrunDifferent = false, IsLatestPaidDtExisted = false;
            decimal
                AbsentDayForJoin = 0m,
                AbsentDayForResign = 0m,
                RecomputedProcessedWorkingDay = 0m,
                RecomputedAllWorkingDay = 0m;

            foreach (var x in lResult1
                .Where(x => Sm.CompareStr(x.EmpCode, EmpCode))
                .OrderBy(Index => Index.Dt))
            {
                if (Sm.CompareDtTm(x.Dt, x.JoinDt) <= 0)
                    AbsentDayForJoin += 1;

                if (x.ResignDt.Length > 0 && Sm.CompareDtTm(x.ResignDt, x.Dt) <= 0)
                    AbsentDayForResign += 1;
                else
                {
                    if (x.LatestPaidDt.Length > 0 && Sm.CompareDtTm(x.LatestPaidDt, x.Dt) == 0)
                        IsLatestPaidDtExisted = true;

                    if (!IsPayrunDifferent &&
                        !(
                        Sm.CompareStr(x.DeptCode, mDeptCode) &&
                        Sm.CompareStr(x.SystemType, mSystemType) &&
                        Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                        Sm.CompareStr(x.PGCode, mPGCode) &&
                        Sm.CompareStr(x.SiteCode, mSiteCode)
                        ))

                        IsPayrunDifferent = true;
                }

                if (!(x.HolInd || x.WSHolidayInd))
                {
                    if (x.ProcessInd) RecomputedProcessedWorkingDay += 1;
                    //if (!(x.ResignDt.Length > 0 && Sm.CompareDtTm(x.ResignDt, x.Dt) <= 0))
                    RecomputedAllWorkingDay += 1;
                }
            }

            r.Salary = LatestSalary;

            if ((RecomputedAllWorkingDay - AbsentDayForJoin <= 14) || 
                AbsentDayForResign > 0 || IsLatestPaidDtExisted || IsPayrunDifferent || RecomputedProcessedWorkingDay < RecomputedAllWorkingDay)
            {
                if (RecomputedAllWorkingDay == 0)
                {
                    r.Salary = 0m;
                    r.TaxableFixAllowance = 0m;
                    r.NonTaxableFixAllowance = 0m;
                    r.FixAllowance = 0m;
                    r.TaxableFixDeduction = 0m;
                    r.NonTaxableFixDeduction = 0m;
                    r.FixDeduction = 0m;
                }
                else
                {
                    if (AbsentDayForJoin > 0)
                        r.Salary = LatestSalary2 * RecomputedProcessedWorkingDay;
                    else
                        r.Salary *= (RecomputedProcessedWorkingDay / RecomputedAllWorkingDay);
                    r.TaxableFixAllowance *= (RecomputedProcessedWorkingDay / RecomputedAllWorkingDay);
                    r.NonTaxableFixAllowance *= (RecomputedProcessedWorkingDay / RecomputedAllWorkingDay);
                    r.FixAllowance *= (RecomputedProcessedWorkingDay / RecomputedAllWorkingDay);
                    r.TaxableFixDeduction *= (RecomputedProcessedWorkingDay / RecomputedAllWorkingDay);
                    r.NonTaxableFixDeduction *= (RecomputedProcessedWorkingDay / RecomputedAllWorkingDay);
                    r.FixDeduction *= (RecomputedProcessedWorkingDay / RecomputedAllWorkingDay);
                }
            }

            if (r.Salary > LatestSalary) r.Salary = LatestSalary;

            if (r.PensionInd)
            {
                r.Salary = 0m;
                r.TaxableFixAllowance = 0m;
                r.NonTaxableFixAllowance = 0m;
                r.FixAllowance = 0m;
                r.TaxableFixDeduction = 0m;
                r.NonTaxableFixDeduction = 0m;
                r.FixDeduction = 0m;
            }

            //if ((RecomputedAllWorkingDay-AbsentDayForJoin>14) || AbsentDayForJoin == 0 && AbsentDayForResign == 0)
            //{
            //    if (mADCodeTransport.Length > 0)
            //    {
            //        foreach (
            //            var t in mEmployeeMealTransport.Where(Index =>
            //                Sm.CompareStr(Index.EmpCode, EmpCode) &&
            //                Sm.CompareStr(Index.ADCode, mADCodeTransport)))
            //            r.Transport = 21 * t.Amt;
            //    }
            //}

            r._IsHOInd = LatestIsHOInd;

            r.SalaryPension = LatestSalaryPension;
            if (r.SalaryPension < 0m) r.SalaryPension = 0m;

            if (r.Salary < 0m) r.Salary = 0m;
            if (r.TaxableFixAllowance < 0) r.TaxableFixAllowance = 0;
            if (r.NonTaxableFixAllowance < 0) r.NonTaxableFixAllowance = 0;
            if (r.FixAllowance < 0) r.FixAllowance = 0;

            decimal EmpAdvancePayment = 0m;
            int CreditAdvancePaymentNo = 0;
            string CreditCode = string.Empty;
            foreach (var x in mlAdvancePaymentProcess.Where(x => Sm.CompareStr(x.EmpCode, EmpCode)).OrderBy(o => o.CreditCode))
            {
                if (x.CreditCode.Length > 0)
                {
                    if (Sm.CompareStr(x.CreditCode, CreditCode))
                    {
                        if (CreditAdvancePaymentNo == 1)
                            r.CreditAdvancePayment1 += x.Amt;

                        if (CreditAdvancePaymentNo == 2)
                            r.CreditAdvancePayment2 += x.Amt;

                        if (CreditAdvancePaymentNo == 3)
                            r.CreditAdvancePayment3 += x.Amt;

                        if (CreditAdvancePaymentNo == 4)
                            r.CreditAdvancePayment4 += x.Amt;

                        if (CreditAdvancePaymentNo == 5)
                            r.CreditAdvancePayment5 += x.Amt;

                        if (CreditAdvancePaymentNo == 6)
                            r.CreditAdvancePayment6 += x.Amt;

                        if (CreditAdvancePaymentNo == 7)
                            r.CreditAdvancePayment7 += x.Amt;

                        if (CreditAdvancePaymentNo == 8)
                            r.CreditAdvancePayment8 += x.Amt;

                        if (CreditAdvancePaymentNo == 9)
                            r.CreditAdvancePayment9 += x.Amt;

                        if (CreditAdvancePaymentNo == 10)
                            r.CreditAdvancePayment10 += x.Amt;
                    }
                    else
                    {
                        CreditAdvancePaymentNo += 1;

                        if (CreditAdvancePaymentNo == 1)
                        {
                            r.CreditCode1 = x.CreditCode;
                            r.CreditAdvancePayment1 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 2)
                        {
                            r.CreditCode2 = x.CreditCode;
                            r.CreditAdvancePayment2 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 3)
                        {
                            r.CreditCode3 = x.CreditCode;
                            r.CreditAdvancePayment3 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 4)
                        {
                            r.CreditCode4 = x.CreditCode;
                            r.CreditAdvancePayment4 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 5)
                        {
                            r.CreditCode5 = x.CreditCode;
                            r.CreditAdvancePayment5 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 6)
                        {
                            r.CreditCode6 = x.CreditCode;
                            r.CreditAdvancePayment6 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 7)
                        {
                            r.CreditCode7 = x.CreditCode;
                            r.CreditAdvancePayment7 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 8)
                        {
                            r.CreditCode8 = x.CreditCode;
                            r.CreditAdvancePayment8 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 9)
                        {
                            r.CreditCode9 = x.CreditCode;
                            r.CreditAdvancePayment9 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 10)
                        {
                            r.CreditCode10 = x.CreditCode;
                            r.CreditAdvancePayment10 += x.Amt;
                        }
                    }
                    CreditCode = x.CreditCode;
                }
                EmpAdvancePayment += x.Amt;
            }

            r.EmpAdvancePayment = EmpAdvancePayment;

            r.Amt =
                r.Salary +
                r.FixAllowance+
                r.Transport +
                r.Meal +
                r.ServiceChargeIncentive+
                r.SalaryAdjustment +
                r.ADLeave +
                (r.SSEmployerHealth +
                r.SSEmployerEmployment +
                r.SSErPension +
                r.SSEmployerPension +
                r.SSEmployerPension2) -
                (r.SSEmployerHealth +
                r.SSEmployerEmployment +
                r.SSErPension +
                r.SSEmployerPension +
                r.SSEmployerPension2 +
                r.SSEmployeeHealth +
                r.SSEmployeeEmployment +
                r.SSEePension +
                r.SSEmployeePension +
                r.SSEmployeePension2)
                -r.FixDeduction
                -r.EmpAdvancePayment 
                ;

            if (r.Amt < 0m) r.Amt = 0m;

            ProcessTax(ref r, ref lNTI, ref lTI);

            //r.Amt -= r.Tax;
            if (r.Amt < 0m) r.Amt = 0m;
        }

        private void ProcessTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            decimal Brutto =
                r.Salary +
                r.TaxableFixAllowance +
                r.Transport +
                r.Meal +
                r.ADLeave +
                r.SalaryAdjustment +
                r.ServiceChargeIncentive +
                r.SSEmployerHealth +
                r.SSErLifeInsurance +
                r.SSErWorkingAccident;

            if (r.NPWP.Length == 0 || r.PTKP.Length == 0) return;

            if (r._IsHOInd)
                ProcessTax1(ref r, ref lNTI, ref lTI, Brutto);
            else
            {
                ProcessTax2a(ref r, ref lNTI, ref lTI);
                ProcessTax2b(ref r, ref lNTI, ref lTI, Brutto);
            }
        }

        private void ProcessTax1(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, decimal Brutto)
        {
            string NTI = r.PTKP, PensionEmployerPerc = string.Empty;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                PPH21Setahun = 0m,
                PPH21Sebulan = 0m;

            TotalImbalan =
                r.Salary +
                r.TaxableFixAllowance +
                r.Transport +
                r.Meal +
                r.ADLeave +
                r.TaxableSalaryAdjustment +
                //r.SalaryAdjustment +
                r.ServiceChargeIncentive +
                r.SSEmployerHealth +
                r.SSErLifeInsurance +
                r.SSErWorkingAccident -
                (
                r.SSEePension
                + r.SSEeRetirement
                + r.SSEmployeePension
                + r.SSEmployeePension2
                )
                ;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt)
                ? mFunctionalExpensesMaxAmt
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;
            GajiBersihDisetahunkan = 12m * GajiBersihSebulan;

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihDisetahunkan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihDisetahunkan - NTIAmt;

            PPH21Setahun = decimal.Truncate(PKP);
            PPH21Setahun = PPH21Setahun - (PPH21Setahun % 1000);

            if (PPH21Setahun > 0m && lTI.Count > 0)
            {
                TaxTemp = PPH21Setahun;
                Amt2Temp = 0m;
                PPH21Setahun = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            PPH21Setahun += (TaxTemp * i.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Setahun += ((i.Amt2 - Amt2Temp) * i.TaxRate * 0.01m);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }
            PPH21Sebulan = PPH21Setahun / 12m;
            if (PPH21Sebulan <= 0) PPH21Sebulan = 0m;
            r.Tax = PPH21Sebulan;
            r.TaxAllowance = PPH21Sebulan;
            //TKG1
            //if (mIsGovTaxSubsidiesValidationEnabled && mMaxTHPForGovSubsidies > 0m)
            //{
            //    if (Brutto <= mMaxTHPForGovSubsidies)
            //        r.Amt += r.TaxAllowance;
            //}
        }

        private void ProcessTax2a(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            string NTI = r.PTKP, PensionEmployerPerc = string.Empty;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                PPH21Setahun = 0m,
                PPH21Sebulan = 0m
                ;

            TotalImbalan =
                r.Salary +
                r.TaxableFixAllowance +
                r.Transport +
                r.Meal +
                r.ADLeave +
                r.TaxableSalaryAdjustment +
                //r.SalaryAdjustment +
                r.ServiceChargeIncentive +
                r.SSEmployerHealth +
                r.SSErLifeInsurance +
                r.SSErWorkingAccident -
                (
                r.SSEePension
                + r.SSEeRetirement
                + r.SSEmployeePension
                + r.SSEmployeePension2
                )
                ;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt)
                ? mFunctionalExpensesMaxAmt
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;
            GajiBersihDisetahunkan = 12m * GajiBersihSebulan;

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihDisetahunkan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihDisetahunkan - NTIAmt;

            PPH21Setahun = decimal.Truncate(PKP);
            PPH21Setahun = PPH21Setahun - (PPH21Setahun % 1000);

            if (PPH21Setahun > 0m && lTI.Count > 0)
            {
                TaxTemp = PPH21Setahun;
                Amt2Temp = 0m;
                PPH21Setahun = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            PPH21Setahun += (TaxTemp * i.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Setahun += ((i.Amt2 - Amt2Temp) * i.TaxRate * 0.01m);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }
            PPH21Sebulan = PPH21Setahun / 12m;

            if (PPH21Sebulan <= 0) PPH21Sebulan = 0m;
            r.Tax = PPH21Sebulan;
            //if (mIsGovTaxSubsidiesValidationEnabled && mMaxTHPForGovSubsidies > 0m)
            //{
            //    if (Amt > mMaxTHPForGovSubsidies) r.Amt -= r.Tax;
            //}
            //else
            //    r.Amt -= r.Tax;
            r.Amt -= r.Tax;
        }

        private void ProcessTax2b(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, decimal Amt)
        {
            string NTI = r.PTKP, PensionEmployerPerc = string.Empty;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                PPH21Setahun = 0m,
                PPH21Sebulan = 0m
                ;

            TotalImbalan =
                r.Salary +
                r.TaxableFixAllowance +
                r.Transport +
                r.Meal +
                r.ADLeave +
                r.TaxableSalaryAdjustment +
                //r.SalaryAdjustment +
                //r.ServiceChargeIncentive +
                r.SSEmployerHealth +
                r.SSErLifeInsurance +
                r.SSErWorkingAccident -
                (
                r.SSEePension
                + r.SSEeRetirement
                + r.SSEmployeePension
                + r.SSEmployeePension2
                )
                ;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt)
                ? mFunctionalExpensesMaxAmt
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;
            GajiBersihDisetahunkan = 12m * GajiBersihSebulan;

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihDisetahunkan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihDisetahunkan - NTIAmt;

            PPH21Setahun = decimal.Truncate(PKP);
            PPH21Setahun = PPH21Setahun - (PPH21Setahun % 1000);

            if (PPH21Setahun > 0m && lTI.Count > 0)
            {
                TaxTemp = PPH21Setahun;
                Amt2Temp = 0m;
                PPH21Setahun = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            PPH21Setahun += (TaxTemp * i.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Setahun += ((i.Amt2 - Amt2Temp) * i.TaxRate * 0.01m);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }
            PPH21Sebulan = PPH21Setahun / 12m;

            if (PPH21Sebulan <= 0) PPH21Sebulan = 0m;
            r.TaxAllowance = PPH21Sebulan;
            //if (mIsGovTaxSubsidiesValidationEnabled && mMaxTHPForGovSubsidies > 0m)
            //{
            //    //if (Amt <= mMaxTHPForGovSubsidies)
            //    //    r.Amt = r.Amt + r.TaxAllowance;
            //    //else
            //    //    r.Amt += PPH21Sebulan;

            //    r.Amt += r.TaxAllowance;

            //    if (Amt <= mMaxTHPForGovSubsidies)
            //        r.Amt += r.Tax;
            //}
            //else
            //    r.Amt += PPH21Sebulan;

            r.Amt += PPH21Sebulan;
            if (mIsGovTaxSubsidiesValidationEnabled && mMaxTHPForGovSubsidies > 0m)
            {
                if (Amt <= mMaxTHPForGovSubsidies)
                {
                    r.Amt = r.Amt + (r.Tax - r.TaxAllowance) - (r.Tax - r.TaxAllowance);
                    //r.Amt += r.Tax;
                }
            }
        }

        #endregion

        #region Get Tables Data

        private void ProcessEmployeePPS(ref List<EmployeePPS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0;i<mlPayrunEmployee.Count;i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }
            
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ");";
            else
                Filter = " Where 0=1;";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.PosCode, A.DeptCode, A.GrdLvlCode, C.LevelCode, A.EmploymentStatus, ");
            SQL.AppendLine("A.SystemType, A.PayrunPeriod, A.PGCode, A.SiteCode, IfNull(B.HOInd, 'N') As HOInd ");
            SQL.AppendLine("From TblEmployeePPS A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "StartDt", "EndDt", "PosCode", "DeptCode", "GrdLvlCode", 
                    
                    //6-10
                    "LevelCode", "EmploymentStatus", "SystemType", "PayrunPeriod", "PGCode", 

                    //11-12
                    "SiteCode", "HOInd"
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            GrdLvlCode = Sm.DrStr(dr, c[5]),
                            LevelCode = Sm.DrStr(dr, c[6]),
                            EmploymentStatus = Sm.DrStr(dr, c[7]),
                            SystemType = Sm.DrStr(dr, c[8]),
                            PayrunPeriod = Sm.DrStr(dr, c[9]),
                            PGCode = Sm.DrStr(dr, c[10]),
                            SiteCode = Sm.DrStr(dr, c[11]),
                            HOInd = Sm.DrStr(dr, c[12])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpWorkSchedule(ref List<EmpWorkSchedule> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 0=1;";

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, ");
            SQL.AppendLine("B.HolidayInd, B.DoubleShiftInd, ");
            SQL.AppendLine("B.In1, B.Out1, B.In2, B.Out2, B.In3, B.Out3 ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "Dt", "WSCode", "HolidayInd", "DoubleShiftInd", "In1", 
                    
                    //6-10
                    "Out1", "In2", "Out2", "In3", "Out3", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3]),
                            DoubleShiftInd = Sm.DrStr(dr, c[4]),
                            In1 = Sm.DrStr(dr, c[5]),
                            Out1 = Sm.DrStr(dr, c[6]),
                            In2 = Sm.DrStr(dr, c[7]),
                            Out2 = Sm.DrStr(dr, c[8]),
                            In3 = Sm.DrStr(dr, c[9]),
                            Out3 = Sm.DrStr(dr, c[10])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpPaidDt(ref List<EmpPaidDt> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 0=1;";

            SQL.AppendLine("Select Distinct A.EmpCode, A.Dt ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("And IfNull(A.PayrunCode, '')<>@PayrunCode ");
            SQL.AppendLine("And A.ProcessInd='Y' ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<string>(ref cm, "@PayrunCode", mPayrunCode);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpPaidDt()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAtd(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(B.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ")     ";
            else
                Filter = " And 0=1 ";


            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Left(B.InOutA1, 12) End As ActualIn, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Right(B.InOutA1, 12) End As ActualOut ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.CancelInd='N';");

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "ActualIn", "ActualOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ActualIn = Sm.DrStr(dr, c[2]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[2]),
                            ActualOut = Sm.DrStr(dr, c[3]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeave(ref List<Leave> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(Tbl.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T1.DurationHour, T3.PaidInd, 'N' As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A' ");
            SQL.AppendLine(Filter.Replace("Tbl.", "T1."));
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T2.EmpCode, T3.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T2.DurationHour, T4.PaidInd, T1.CompulsoryLeaveInd As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter.Replace("Tbl.", "T2."));
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 T3 ");
            SQL.AppendLine("    On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    And T3.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T4 On T1.LeaveCode=T4.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A';");

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "LeaveDt", "LeaveCode", "LeaveType", "StartTm", "EndTm", 
                    
                    //6-8
                    "DurationHour", "PaidInd", "CompulsoryLeaveInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            LeaveCode = Sm.DrStr(dr, c[2]),
                            LeaveType = Sm.DrStr(dr, c[3]),
                            StartTm = Sm.DrStr(dr, c[4]),
                            EndTm = Sm.DrStr(dr, c[5]),
                            DurationHr = Sm.DrDec(dr, c[6]),
                            PaidInd = Sm.DrStr(dr, c[7]),
                            CompulsoryLeaveInd = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveDtl(ref List<LeaveDtl> l)
        {
            var cm = new MySqlCommand();
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select LeaveCode, SystemType, PayrunPeriod, DeductTHPInd From TblLeaveDtl;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "LeaveCode", 
                    "SystemType", "PayrunPeriod", "DeductTHPInd" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveDtl()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),
                            SystemType = Sm.DrStr(dr, c[1]),
                            PayrunPeriod = Sm.DrStr(dr, c[2]),
                            DeductTHPInd = Sm.DrStr(dr, c[3]) == "Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ");";
            else
                Filter = " Where 0=1;";

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select EmpCode, JoinDt, ResignDt From TblEmployee " + Filter;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "JoinDt", "ResignDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalary(ref List<EmployeeSalary> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ");";
            else
                Filter = " Where 0=1;";

            SQL.AppendLine("Select A.EmpCode, B.StartDt, IfNull(B.Amt, 0) As Amt, IfNull(B.Amt2, 0) As Amt2 ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblEmployeeSalary B On A.EmpCode=B.EmpCode And B.StartDt<=@EndDt ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "Amt", "Amt2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalarySSPension(ref List<EmployeeSalarySS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner Join TblParameter B On A.SSPCode=B.ParValue And B.ParCode='SSProgramForPension' And B.ParValue Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("); ");

            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "EndDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalarySS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeMealTransport(ref List<EmployeeMealTransport> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, Filter2 = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            if (mADCodeMeal.Length > 0)
            {
                Filter2 = " ADCode=@ADCodeMeal ";
                Sm.CmParam<String>(ref cm, "@ADCodeMeal", mADCodeMeal);
            }

            if (mADCodeTransport.Length > 0)
            {
                if (Filter2.Length > 0) Filter2 += " Or ";
                Filter2 += " ADCode=@ADCodeTransport ";
                Sm.CmParam<String>(ref cm, "@ADCodeTransport", mADCodeTransport);
            }

            if (Filter2.Length > 0)
                Filter2 = " And (" + Filter2 + ") ";
            else
                Filter2 = " And 0=1 ";

            SQL.AppendLine("Select EmpCode, ADCode, StartDt, EndDt, Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(Filter2);
            SQL.AppendLine("Order By ADCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", 
                    "ADCode", "StartDt", "EndDt", "Amt" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeMealTransport()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            EndDt = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAttendanceLog(ref List<AttendanceLog> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var EmpCodeTemp = string.Empty;
            var DtTemp = string.Empty;
            var TmTemp = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 0=1;";

            SQL.AppendLine("Select EmpCode, Dt, Tm ");
            SQL.AppendLine("From TblAttendanceLog ");
            SQL.AppendLine("Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            var EndDtTemp = mEndDt;

            DateTime EndDt = new DateTime(
                      Int32.Parse(EndDtTemp.Substring(0, 4)),
                      Int32.Parse(EndDtTemp.Substring(4, 2)),
                      Int32.Parse(EndDtTemp.Substring(6, 2)),
                      0, 0, 0).AddDays(1);

            Sm.CmParamDt(ref cm, "@EndDt", EndDt.Year.ToString() +
                        ("00" + EndDt.Month.ToString()).Substring(("00" + EndDt.Month.ToString()).Length - 2, 2) +
                        ("00" + EndDt.Day.ToString()).Substring(("00" + EndDt.Day.ToString()).Length - 2, 2));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        DtTemp = Sm.DrStr(dr, c[1]);
                        TmTemp = Sm.DrStr(dr, c[2]);
                        if (TmTemp.Length > 4) TmTemp = Sm.Left(TmTemp, 4);
                        l.Add(new AttendanceLog()
                        {
                            EmpCode = EmpCodeTemp,
                            Dt = DtTemp,
                            Tm = TmTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOT(ref List<OT> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode= string.Empty, OTDt = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T2.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            SQL.AppendLine("Select T1.DocNo, T1.OTDt, T2.EmpCode, T2.OTStartTm, T2.OTEndTm ");
            SQL.AppendLine("From TblOTRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T2 On T1.DocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            SQL.AppendLine("And T1.OTDt Between @StartDt And @EndDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "OTDt", "EmpCode", "OTStartTm", "OTEndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OT()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTAdjustment(ref List<OTAdjustment> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, OTDt = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T3.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            SQL.AppendLine("Select T1.OTRequestDocNo, T2.OTDt, T3.EmpCode, T1.StartTm, T1.EndTm ");
            SQL.AppendLine("From TblOTAdjustment T1 ");
            SQL.AppendLine("Inner Join TblOTRequestHdr T2 On T1.OTRequestDocNo=T2.DocNo And T2.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T3 On T1.OTRequestDocNo=T3.DocNo And T1.OTRequestDNo=T3.DNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A';");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTRequestDocNo", "OTDt", "EmpCode", "StartTm", "EndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTAdjustment()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T1.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length>0) 
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select T1.DocNo, T1.EmpCode, T1.CreditCode, T2.Amt ");
            SQL.AppendLine("From TblAdvancePaymentHdr T1 ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.Yr=@Yr ");
            SQL.AppendLine("    And T2.Mth=@Mth ");
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine(Filter.Replace("T1.", string.Empty));
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Or Not Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine(";");

            string Year = Sm.Left(mEndDt, 4), Month = mEndDt.Substring(4, 2);

            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@Mth", Month);
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "CreditCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AdvancePaymentProcess()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            CreditCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            Yr = Year,
                            Mth = Month
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessIncEmployee(ref List<IncEmployee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T2.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select T1.DocDt, T2.EmpCode, Sum(T1.AmtInspnt) As Amt ");
            SQL.AppendLine("From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpInsPntDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='01' ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Group By T1.DocDt, T2.EmpCode;");

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "DocDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new IncEmployee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpSCI(ref List<EmpSCI> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(B.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select A.DocDt, B.EmpCode, Sum(B.Amt) As Amt ");
            SQL.AppendLine("From TblEmpSCIHdr A ");
            SQL.AppendLine("Inner Join TblEmpSCIDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (B.PayrunCode Is Null Or (B.PayrunCode Is Not Null And B.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Group By A.DocDt, B.EmpCode;");

            Sm.CmParam<string>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "DocDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpSCI()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }
        
        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            PayrunCode = mPayrunCode,
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            PayrunCode = mPayrunCode,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private void GetDt(ref List<string> l)
        {
            string StartDt = mStartDt, EndDt = mEndDt;

            DateTime
                Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    ),
                Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

            var TotalDays = (Dt2 - Dt1).Days;
            l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            for (int i = 1; i <= TotalDays; i++)
            {
                Dt1 = Dt1.AddDays(1);
                l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            }
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        private decimal RoundTo1Hour(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0)
                return (decimal)(Value2);
            else
                return (decimal)(Value);
        }

        #endregion

        #endregion

        #region Event

        private void FrmPayrollProcess_Load(object sender, EventArgs e)
        {
            try
            {
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                string 
                    Yr = Sm.Left(CurrentDateTime, 4),
                    Mth = CurrentDateTime.Substring(4, 2);

                if (Mth == "01")
                {
                    Yr = (int.Parse(Yr) - 1).ToString();
                    Mth = "12";
                }
                else
                    Mth = (int.Parse(Mth) - 1).ToString();

                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Yr);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, Mth);

                GetParameter();
                GetValue();
                SetGrd();
                mlPayrunEmployee = new List<PayrunEmployee>();
                mlAttendanceLog = new List<AttendanceLog>();
                mlOT = new List<OT>();
                mlOTAdjustment = new List<OTAdjustment>();
                mlAdvancePaymentProcess = new List<AdvancePaymentProcess>();
                mEmployeeMealTransport = new List<EmployeeMealTransport>();

                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ShowPayrunInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    Grd1.Cells[r, 3].Value = false;
                    Grd1.Cells[r, 17].Value = string.Empty;
                }
                if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsProcessedDataNotValid()) return;

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 0) && Sm.GetGrdStr(Grd1, r, 1).Length>0)
                    {
                        mPayrunCode = string.Empty;
                        mStartDt = string.Empty;
                        mEndDt = string.Empty;
                        mDeptCode = string.Empty;
                        mSiteCode = string.Empty;
                        mSystemType = string.Empty;
                        mPayrunPeriod = string.Empty;
                        mPGCode = string.Empty;
                        mHOInd = true;

                        mlPayrunEmployee.Clear();
                        mlAttendanceLog.Clear();
                        mlOT.Clear();
                        mlOTAdjustment.Clear();
                        mlAdvancePaymentProcess.Clear();
                        mEmployeeMealTransport.Clear();

                        mPayrunCode = Sm.GetGrdStr(Grd1, r, 1);
                        mStartDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 4), 8);
                        mEndDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 5), 8);
                        mSiteCode = Sm.GetGrdStr(Grd1, r, 6);
                        mDeptCode = Sm.GetGrdStr(Grd1, r, 8);
                        mSystemType = Sm.GetGrdStr(Grd1, r, 10);
                        mPayrunPeriod = Sm.GetGrdStr(Grd1, r, 12);
                        mPGCode = Sm.GetGrdStr(Grd1, r, 14);
                        mHOInd = Sm.GetGrdBool(Grd1, r, 16);

                        ProcessData1();
                        if (mlPayrunEmployee.Count > 0)
                        {
                            ProcessData2();
                            Grd1.Cells[r, 3].Value = true;
                            Grd1.Cells[r, 17].Value = "Process is completed.";
                        }
                        else
                            Grd1.Cells[r, 17].Value = "No employee.";
                    }
                }
                Sm.StdMsg(mMsgType.Info, "Process is completed.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Class

        private class PayrunEmployee
        {
            public string EmpCode { get; set; }
        }

        private class Result1
        {
            public bool _DoubleShiftInd { get; set; }
            public bool _IsUseLatestGrdLvlSalary { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string _LatestLevelCode { get; set; }
            public decimal _LatestMonthlyGrdLvlSalary { get; set; }
            public decimal _LatestDailyGrdLvlSalary { get; set; }
            public bool _IsFullDayInd { get; set; }
            public bool _IsHOInd { get; set; }
            public decimal _ExtraFooding { get; set; }
            public decimal _FieldAssignment { get; set; }
            public decimal _IncPerformance { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public string LatestPaidDt { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string SystemType { get; set; }
            public string EmploymentStatus { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public decimal WorkingDay { get; set; }
            public string WSCode { get; set; }
            public bool HolInd { get; set; }
            public decimal HolidayIndex { get; set; }
            public bool WSHolidayInd { get; set; }
            public string WSIn1 { get; set; }
            public string WSOut1 { get; set; }
            public string WSIn2 { get; set; }
            public string WSOut2 { get; set; }
            public string WSIn3 { get; set; }
            public string WSOut3 { get; set; }
            public bool OneDayInd { get; set; }
            public bool LateInd { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
            public string WorkingIn { get; set; }
            public string WorkingOut { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal ProductionWages { get; set; }
            public decimal Salary { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public bool PaidLeaveInd { get; set; }
            public bool CompulsoryLeaveInd { get; set; }
            public bool _DeductTHPInd { get; set; }
            public string LeaveStartTm { get; set; }
            public string LeaveEndTm { get; set; }
            public decimal LeaveDuration { get; set; }
            public decimal PLDay { get; set; }
            public decimal PLHr { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public bool OTToLeaveInd { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncProduction { get; set; }
            public decimal IncPerformance { get; set; }
            public bool PresenceRewardInd { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal DedProduction { get; set; }
            public decimal DedProdLeave { get; set; }
            public decimal EmpSalary { get; set; }
            public decimal EmpSalary2 { get; set; }
            public decimal SalaryAD { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal IncEmployee { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
        }

        private class Result2
        {
            public bool _IsHOInd { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
	        public decimal Salary { get; set; }
	        public decimal WorkingDay { get; set; }
	        public decimal PLDay { get; set; }
	        public decimal PLHr { get; set; }
	        public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
	        public decimal OT2Hr { get; set; }
	        public decimal OTHolidayHr { get; set; }
	        public decimal OT1Amt { get; set; }
	        public decimal OT2Amt { get; set; }
	        public decimal OTHolidayAmt { get; set; }
	        public decimal FixAllowance { get; set; }
	        public decimal EmploymentPeriodAllowance { get; set; }
	        public decimal IncEmployee { get; set; }
	        public decimal IncMinWages { get; set; }
	        public decimal IncProduction { get; set; }
	        public decimal IncPerformance { get; set; }
	        public decimal PresenceReward { get; set; }
	        public decimal HolidayEarning { get; set; }
	        public decimal ExtraFooding { get; set; }
	        public decimal SSEmployerHealth { get; set; }
	        public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployerPension2 { get; set; }
            public decimal SSEmployerNonRetiring { get; set; }
            public decimal SSEmployeeOldAgeInsurance { get; set; }
	        public decimal FixDeduction { get; set; }
	        public decimal DedEmployee { get; set; }
	        public decimal DedProduction { get; set; }
	        public decimal DedProdLeave { get; set; }
            public decimal EmpAdvancePayment { get; set; }
	        public decimal SSEmployeeHealth { get; set; }
	        public decimal SSEmployeeEmployment { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal SSEmployeePension2 { get; set; }
            public decimal SalaryAdjustment { get; set; }
	        public decimal Tax { get; set; }
            public decimal TaxAllowance { get; set; }
            public decimal Amt { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal NonTaxableFixAllowance { get; set; }
            public decimal NonTaxableFixDeduction { get; set; }
            public decimal TaxableFixAllowance { get; set; }
            public decimal TaxableFixDeduction { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal Functional { get; set; }
            public decimal Housing { get; set; }
            public decimal MobileCredit { get; set; }
            public decimal Zakat { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal SSErLifeInsurance { get; set; }
            public decimal SSEeLifeInsurance { get; set; }
            public decimal SSErWorkingAccident { get; set; }
            public decimal SSEeWorkingAccident { get; set; }
            public decimal SSErRetirement { get; set; }
            public decimal SSEeRetirement { get; set; }
            public decimal SSErPension { get; set; }
            public decimal SSEePension { get; set; }
            public string CreditCode1 { get; set; }
            public string CreditCode2 { get; set; }
            public string CreditCode3 { get; set; }
            public string CreditCode4 { get; set; }
            public string CreditCode5 { get; set; }
            public string CreditCode6 { get; set; }
            public string CreditCode7 { get; set; }
            public string CreditCode8 { get; set; }
            public string CreditCode9 { get; set; }
            public string CreditCode10 { get; set; }
            public decimal CreditAdvancePayment1 { get; set; }
            public decimal CreditAdvancePayment2 { get; set; }
            public decimal CreditAdvancePayment3 { get; set; }
            public decimal CreditAdvancePayment4 { get; set; }
            public decimal CreditAdvancePayment5 { get; set; }
            public decimal CreditAdvancePayment6 { get; set; }
            public decimal CreditAdvancePayment7 { get; set; }
            public decimal CreditAdvancePayment8 { get; set; }
            public decimal CreditAdvancePayment9 { get; set; }
            public decimal CreditAdvancePayment10 { get; set; }
            public decimal ADLeave { get; set; }
            public bool PensionInd { get; set; }
            public decimal TaxableSalaryAdjustment { get; set; }
            public decimal NontaxableSalaryAdjustment { get; set; }
        }

        #region Additional Class

        private class AdvancePaymentProcess
        {
            public string DocNo { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string EmpCode { get; set; }
            public string CreditCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeSalary
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public decimal SalaryAD { get; set; }
        }

        private class EmployeeSalarySS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class IncEmployee
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class DedEmployee
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class OT
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class OTAdjustment
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class NTI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public string PosCode { get; set; }
            public string HOInd { get; set; }
        }

        private class EmpWorkSchedule
         {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string HolidayInd { get; set; }
            public string DoubleShiftInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In3 { get; set; }
            public string Out3 { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal DurationHr { get; set; }
            public string PaidInd { get; set; }
            public string CompulsoryLeaveInd { get; set; }
        }

        private class LeaveDtl
        {
            public string LeaveCode { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public bool DeductTHPInd { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string LatestPaidDt { get; set; }
        }

        private class AttendanceLog
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class EmployeeMealTransport
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpPaidDt
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
        }

        private class EmpSCI
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion

        #endregion
    }
}
