﻿#region Update
/*
    21/12/2017 [HAR] bug fixing filter item
    13/02/2019 [MEY] tambah filter machine
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmShopFloorControl2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmShopFloorControl2 mFrmParent;
        private string mSQL = string.Empty, mPPDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmShopFloorControl2Dlg(FrmShopFloorControl2 FrmParent, string PPDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPPDocNo = PPDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                if (mPPDocNo.Length != 0)
                {
                    Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -720);
                    TxtDocNo.EditValue = mPPDocNo;
                    ChkDocNo.Checked = true;
                    ShowData();
                }
                else
                    Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -720);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Work Center#",
                        "",

                        //6-10
                        "Work Center Name",
                        "Whs Code",
                        "Warehouse",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        
                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        140, 20, 80, 130, 20,
                        
                        //6-10
                        250, 0, 200, 80, 200,
                        
                        //11
                        400
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 5 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct DocNo, DocDt, WorkCenterDocNo, DocName, WhsCode, WhsName, ItCode, ItName, Remark ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, E.WorkCenterDocNo, F.DocName, F.WhsCode, G.WhsName, E.Sequence, H.ItCode, H.ItName, A.Remark ");
            SQL.AppendLine("From TblPPHdr A ");
            SQL.AppendLine("Inner Join TblPPDtl B On A.DocNo=B.DocNo And B.DocType='1' ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr C On B.ProductionOrderDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl E On C.ProductionRoutingDocNo=E.DocNo And D.ProductionRoutingDNo=E.DNo");
            SQL.AppendLine("Inner Join TblWorkCenterHdr F On E.WorkCenterDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblWarehouse G On F.WhsCode=G.WhsCode ");
            SQL.AppendLine("Left Join TblItem H On C.ItCode=H.ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='R' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, E.WorkCenterDocNo, F.DocName, F.WhsCode, G.WhsName, E.Sequence, H.ItCode, H.ItName, A.Remark ");
            SQL.AppendLine("From TblPPHdr A ");
            SQL.AppendLine("Inner Join TblPPDtl B On A.DocNo=B.DocNo And B.DocType='2' ");
            SQL.AppendLine("Inner Join TblJCHdr C On B.JCDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblJCDtl D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblProductionRoutingDtl E On C.ProductionRoutingDocNo=E.DocNo And D.ProductionRoutingDNo=E.DNo");
            SQL.AppendLine("Inner Join TblWorkCenterHdr F On E.WorkCenterDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblWarehouse G On F.WhsCode=G.WhsCode ");
            SQL.AppendLine("Left Join TblItem H On C.ItCode=H.ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='R' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T ");
           
            //SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, E.WorkCenterDocNo, F.DocName, F.WhsCode, G.WhsName ");
            //SQL.AppendLine("From TblPPHdr A ");
            //SQL.AppendLine("Inner Join TblPPDtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("Inner Join TblProductionOrderHdr C On B.ProductionOrderDocNo=C.DocNo ");
            //SQL.AppendLine("Inner Join TblProductionOrderDtl D On C.DocNo=D.DocNo ");
            //SQL.AppendLine("Inner Join TblProductionRoutingDtl E On C.ProductionRoutingDocNo=E.DocNo And D.ProductionRoutingDNo=E.DNo");
            //SQL.AppendLine("Inner Join TblWorkCenterHdr F On E.WorkCenterDocNo=F.DocNo ");
            //SQL.AppendLine("Left Join TblWarehouse G On F.WhsCode=G.WhsCode ");
            //SQL.AppendLine("Where A.CancelInd='N' ");
            //SQL.AppendLine("And A.Status='R' ");
            //SQL.AppendLine("And A.ProcessInd<>'F' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtWorkCenterDocNo.Text, new string[] { "WorkCenterDocNo", "DocName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "Itname" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DocDt, DocNo, Sequence;",
                        new string[] 
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DocDt", 
                             "WorkCenterDocNo", 
                             "DocName",
                             "WhsCode",
                             "WhsName",

                             //6-8
                             "ItCode",
                             "ItName",
                             "Remark"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtPPDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtWorkCenterDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtWorkCenterDocName.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.mWhsCode2 = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtWhsCode2.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                mFrmParent.MeePPRemark.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.ProcessStock();
                mFrmParent.SetLueBatchNo(ref mFrmParent.LueBatchNo);
                if (mFrmParent.mSFCDefaultBatchNo.Length != 0) 
                    mFrmParent.GetSFCDefaultBatchNo();
                mFrmParent.ClearGrd();
                mFrmParent.ShowEmployeeInfo(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 4));
                mFrmParent.SetLueMachineCode(ref mFrmParent.LueMachineCode,Sm.GetGrdStr(Grd1, Row, 4));
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPP(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmPP(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtWorkCenterDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWorkCenterDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work Center#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }


        #endregion

        #endregion
    }
}
