﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemCost : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmItemCostFind FrmFind;
        internal string mDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmItemCost(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            Sl.SetLueCurCode(ref LueCurCode);
            SetGrd();
            base.FrmLoad(sender, e);

            //if this application is called from other application
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
           if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Item Code" + Environment.NewLine + "(From)",
                        "",
                        "Item Name" + Environment.NewLine + "(From)",
                        "",
                        "Item Code" + Environment.NewLine + "(To)",
                        
                        //6-9
                        "",
                        "Item Name" + Environment.NewLine + "(To)",
                        "Cost",
                        "Remark"
                    },
                     new int[] 
                    {
                        20, 
                        130, 20, 150, 20, 130, 
                        20, 150, 100, 400 
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0, 2, 4, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 5, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCurCode, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCurCode, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueCurCode, MeeRemark
            });
            Sm.FocusGrd(Grd1, 0, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8 });
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemCostFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;
                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemCost", "TblItemCostHdr");
                var cml = new List<MySqlCommand>();

                cml.Add(SaveItemCostHdr(DocNo));

                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveItemCostDtl(DocNo, Row));

                Sm.ExecCommands(cml);

                

                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmItemCostDlg(this));
            }

            if (e.ColIndex == 4 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmItemCostDlg2(this, e.RowIndex));
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0 && !IsItemEmpty(e) && Sm.IsGrdColSelected(new int[] { 2, 4, 6, 8, 9 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmItemCostDlg(this));

            if (e.ColIndex == 4 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmItemCostDlg2(this, e.RowIndex));


            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex==8 && Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0;

            if (e.ColIndex==9 && Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length != 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Trim();
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsItCodeNotValid();
        }

        private bool IsItCodeNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 5)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Code (From) : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "Item Name (From) : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                        "Item Code (To) : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                        "Item Name (To) : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                        "Items should not be the same.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item in list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Item (from) is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "Item (to) is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 8, true,
                    "Item Code (From) : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                    "Item Name (From) : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Item Code (To) : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "Item Name (To) : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Cost : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 8), 0) + Environment.NewLine + Environment.NewLine + 
                    "Item's cost should be greater than 0."
                    )) return true;

            }
            return false;
        }

        private MySqlCommand SaveItemCostHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemCostHdr(DocNo, DocDt, CurCode, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DocDt, @CurCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));    
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemCostDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemCostDtl(DocNo, DNo, ItCodeFrom, ItCodeTo, Cost, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ItCodeFrom, @ItCodeTo, @Cost, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCodeFrom", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ItCodeTo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Cost", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowItemCostHdr(DocNo);
                ShowItemCostDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowItemCostHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                        ref cm,
                        "Select DocNo, DocDt, CurCode, Remark " +
                        "From TblItemCostHdr Where DocNo=@DocNo",
                        new string[] 
                        {
                            "DocNo", "DocDt", "CurCode", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[2]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                     );
        }
        private void ShowItemCostDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.ItCodeFrom, B.ItName As ItNameFrom, A.ItCodeTo, C.ItName As ItNameTo, A.Cost, A.Remark " +
                    "From TblItemCostDtl A " +
                    "Inner Join TblItem B On A.ItCodeFrom=B.ItCode " +
                    "Inner Join TblItem C On A.ItCodeTo=C.ItCode " +
                    "Where A.DocNo=@DocNo Order By A.DNo ",
                    new string[] 
                    { 
                        //0
                       "ItCodeFrom", 
                       
                       //1-5
                       "ItNameFrom", "ItCodeTo", "ItNameTo", "Cost", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 5);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private bool IsItemEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 1) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void MeeRemark_EditValueChanged(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeRemark);
        }

        #endregion

        #endregion
        
    }
}
