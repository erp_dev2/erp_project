﻿#region Update
/*
    13/03/2020 [VIN/KSM] new Apps
    23/03/2020 [TKG/KSM] tambah remark
    27/03/2020 [TKG/KSM] tambah informasi item
    12/05/2020 [DITA/KSM] tambah lebar grid di kolom docno
    18/05/2020 [VIN/KSM] tambah kolom status 
    15/12/2020 [VIN/KSM] local DocNo kurang lebar
 *  17/12/2020 [ICA/KSM] mengganti kata outstanding yg typo
 *  18/02/2021 [RDH/KSM] Menambahkan identitas Customer's Category pada Combo Box dan FIND
 *  19/04/2021 [BRI/KSM] tambah kolom price after tax dan total after tax
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesMemoFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesMemo mFrmParent;
        private string mSQL = string.Empty;
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmSalesMemoFind(FrmSalesMemo FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.LocalDocNo, C.CtName,F.CtCtName, B.SiteName, ");
            SQL.AppendLine("E.ItCode, E.ItName, E.ItCodeInternal, D.Qty, E.SalesUomCode, A.CurCode, ");
            SQL.AppendLine("D.UPrice, D.UPrice*D.Qty As Total, D.UPriceAfterTax, D.UPriceAfterTax*D.Qty As TotalAfterTax, D.DeliveryDt, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("case A.`Status` ");
			SQL.AppendLine("    when  'A' then 'Approve' ");
			SQL.AppendLine("    when 'O' then 'Outstanding' ");
			SQL.AppendLine("    ELSE 'Canceled' ");
            SQL.AppendLine("    END AS Status ");
            SQL.AppendLine("From TblSalesMemoHdr A  ");
            SQL.AppendLine("Inner Join TblSite B ON A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Inner Join TblCustomer C ON A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblSalesMemoDtl D ON A.DocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblItem E ON D.ItCode=E.ItCode ");
            SQL.AppendLine("LEFT Join tblcustomercategory F ON C.CtCtCode=F.CtCtCode ");

            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Local#",
                    "Status",
 
                    //6-10
                    "Customer",
                    "Customer Category",
                    "Site",
                    "Item's Code",
                    "Item's Name",

                    //11-15
                    "Local Code",
                    "Quantity",
                    "UoM",
                    "Currency",
                    "Price",
                   
                    //16-20
                    "Total",
                    "Price After Tax",
                    "Total After Tax",
                    "Delivery",
                    "Remark",

                    //21-25
                    "Created By", 
                    "Created Date", 
                    "Created Time", 
                    "Last Updated By",
                    "Last Updated Date",

                    //26
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 100, 180, 100, 100,  
                    
                    //6-10
                    200, 200, 100, 200, 200,  
                    
                    //11-15
                    120, 120, 80, 80, 130,  
                    
                    //16-20
                    130, 130, 130, 100, 300,
                    
                    //21-25
                    130, 130, 130, 130, 130,

                    //26
                    130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 19, 22, 25 });
            Sm.GrdFormatTime(Grd1, new int[] { 23, 26 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 15, 16, 17, 18 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[7].Visible = mFrmParent.mIsCustomerComboShowCategory;
            
            Sm.GrdColInvisible(Grd1, new int[] {  21, 22, 23, 24, 25, 26 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();


                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "E.ItName", "E.ItCodeInternal" });
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "Status", "CtName",  

                            //6-10
                            "CtCtName", "SiteName", "ItCode", "ItName", "ItCodeInternal",  

                            //11-15
                            "Qty", "SalesUomCode", "CurCode", "UPrice", "Total",    
                            
                            //16-20
                            "UPriceAfterTax", "TotalAfterTax", "DeliveryDt", "Remark", "CreateBy", 

                            //21-23
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 26, 23);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
       
        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

       

       
        #endregion

       
    }
}
