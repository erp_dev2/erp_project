﻿namespace RunSystem
{
    partial class FrmInvestmentItemEquity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvestmentItemEquity));
            this.TpgDetail = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.DteUpdateDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtSpecification = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtStockExchange = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtIssuer = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.LueInvestmentUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueInvestmentCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.DteStatementDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtYearEnd = new DevExpress.XtraEditors.TextEdit();
            this.TxtIssuedShares = new DevExpress.XtraEditors.TextEdit();
            this.TxtMarketCap = new DevExpress.XtraEditors.TextEdit();
            this.TxtSector = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtStockIndex = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TxtSales = new DevExpress.XtraEditors.TextEdit();
            this.TxtAsset = new DevExpress.XtraEditors.TextEdit();
            this.TxtEquity = new DevExpress.XtraEditors.TextEdit();
            this.TxtNetCashFlow = new DevExpress.XtraEditors.TextEdit();
            this.TxtLiability = new DevExpress.XtraEditors.TextEdit();
            this.TxtNetProfit = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtOperatingProfit = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.DteListingDt = new DevExpress.XtraEditors.DateEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.TpgEarnings = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtShareCashEqvl = new DevExpress.XtraEditors.TextEdit();
            this.TxtShareAssetValue = new DevExpress.XtraEditors.TextEdit();
            this.TxtShareDividen = new DevExpress.XtraEditors.TextEdit();
            this.TxtShareEarning = new DevExpress.XtraEditors.TextEdit();
            this.TxtShareRevenue = new DevExpress.XtraEditors.TextEdit();
            this.TxtShareBookValue = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtShareCashFlow = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.TpgValuation = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtDividenYield = new DevExpress.XtraEditors.TextEdit();
            this.TxtPriceEarning = new DevExpress.XtraEditors.TextEdit();
            this.TxtPriceSales = new DevExpress.XtraEditors.TextEdit();
            this.TxtPriceBookvalue = new DevExpress.XtraEditors.TextEdit();
            this.TxtPriceCashFlow = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.TpgProfitability = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.TxtMarginGross = new DevExpress.XtraEditors.TextEdit();
            this.TxtMarginOperating = new DevExpress.XtraEditors.TextEdit();
            this.TxtMarginNet = new DevExpress.XtraEditors.TextEdit();
            this.TxtMarginTax = new DevExpress.XtraEditors.TextEdit();
            this.TxtDividenPayout = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.TpgLiquidity = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.TxtCurrent = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.TxtRoe = new DevExpress.XtraEditors.TextEdit();
            this.TxtRoa = new DevExpress.XtraEditors.TextEdit();
            this.TxtDebtEquity = new DevExpress.XtraEditors.TextEdit();
            this.TxtCash = new DevExpress.XtraEditors.TextEdit();
            this.TxtQuick = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.BtnPortofio = new DevExpress.XtraEditors.SimpleButton();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtForeignName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtInvestmentEquityCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtPortofolioId = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.TpgDetail.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStockExchange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteStatementDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStatementDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYearEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuedShares.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarketCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStockIndex.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSales.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEquity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetCashFlow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLiability.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetProfit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOperatingProfit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).BeginInit();
            this.TpgEarnings.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareCashEqvl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareAssetValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareDividen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareEarning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareRevenue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareBookValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareCashFlow.Properties)).BeginInit();
            this.TpgValuation.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDividenYield.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceEarning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceSales.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceBookvalue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceCashFlow.Properties)).BeginInit();
            this.TpgProfitability.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginGross.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginOperating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginNet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDividenPayout.Properties)).BeginInit();
            this.TpgLiquidity.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurrent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDebtEquity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCash.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQuick.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentEquityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortofolioId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(854, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Size = new System.Drawing.Size(70, 372);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtInvestmentName);
            this.panel2.Controls.Add(this.TxtPortofolioId);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.BtnPortofio);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtForeignName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtInvestmentEquityCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TpgDetail);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Size = new System.Drawing.Size(854, 372);
            // 
            // TpgDetail
            // 
            this.TpgDetail.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TpgDetail.Controls.Add(this.TpgGeneral);
            this.TpgDetail.Controls.Add(this.tabPage1);
            this.TpgDetail.Controls.Add(this.TpgEarnings);
            this.TpgDetail.Controls.Add(this.TpgValuation);
            this.TpgDetail.Controls.Add(this.TpgProfitability);
            this.TpgDetail.Controls.Add(this.TpgLiquidity);
            this.TpgDetail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TpgDetail.Location = new System.Drawing.Point(0, 100);
            this.TpgDetail.Multiline = true;
            this.TpgDetail.Name = "TpgDetail";
            this.TpgDetail.SelectedIndex = 0;
            this.TpgDetail.ShowToolTips = true;
            this.TpgDetail.Size = new System.Drawing.Size(854, 272);
            this.TpgDetail.TabIndex = 31;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.label11);
            this.TpgGeneral.Controls.Add(this.DteUpdateDt);
            this.TpgGeneral.Controls.Add(this.TxtSpecification);
            this.TpgGeneral.Controls.Add(this.label10);
            this.TpgGeneral.Controls.Add(this.TxtStockExchange);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.TxtIssuer);
            this.TpgGeneral.Controls.Add(this.label7);
            this.TpgGeneral.Controls.Add(this.label6);
            this.TpgGeneral.Controls.Add(this.LueCurCode);
            this.TpgGeneral.Controls.Add(this.label23);
            this.TpgGeneral.Controls.Add(this.LueInvestmentUomCode);
            this.TpgGeneral.Controls.Add(this.MeeRemark);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.LueInvestmentCtCode);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 26);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(846, 242);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(32, 157);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 33;
            this.label11.Text = "Update Date";
            // 
            // DteUpdateDt
            // 
            this.DteUpdateDt.EditValue = null;
            this.DteUpdateDt.EnterMoveNextControl = true;
            this.DteUpdateDt.Location = new System.Drawing.Point(114, 154);
            this.DteUpdateDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUpdateDt.Name = "DteUpdateDt";
            this.DteUpdateDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteUpdateDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUpdateDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteUpdateDt.Properties.Appearance.Options.UseFont = true;
            this.DteUpdateDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUpdateDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUpdateDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUpdateDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUpdateDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUpdateDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUpdateDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUpdateDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUpdateDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUpdateDt.Size = new System.Drawing.Size(116, 20);
            this.DteUpdateDt.TabIndex = 34;
            // 
            // TxtSpecification
            // 
            this.TxtSpecification.EnterMoveNextControl = true;
            this.TxtSpecification.Location = new System.Drawing.Point(114, 113);
            this.TxtSpecification.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtSpecification.Name = "TxtSpecification";
            this.TxtSpecification.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpecification.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSpecification.Properties.Appearance.Options.UseFont = true;
            this.TxtSpecification.Properties.MaxLength = 250;
            this.TxtSpecification.Size = new System.Drawing.Size(319, 20);
            this.TxtSpecification.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(32, 116);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Specification";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStockExchange
            // 
            this.TxtStockExchange.EnterMoveNextControl = true;
            this.TxtStockExchange.Location = new System.Drawing.Point(114, 71);
            this.TxtStockExchange.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtStockExchange.Name = "TxtStockExchange";
            this.TxtStockExchange.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStockExchange.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStockExchange.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStockExchange.Properties.Appearance.Options.UseFont = true;
            this.TxtStockExchange.Properties.MaxLength = 16;
            this.TxtStockExchange.Properties.ReadOnly = true;
            this.TxtStockExchange.Size = new System.Drawing.Size(319, 20);
            this.TxtStockExchange.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(14, 73);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Stock Exchange";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIssuer
            // 
            this.TxtIssuer.EnterMoveNextControl = true;
            this.TxtIssuer.Location = new System.Drawing.Point(114, 50);
            this.TxtIssuer.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtIssuer.Name = "TxtIssuer";
            this.TxtIssuer.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtIssuer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIssuer.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIssuer.Properties.Appearance.Options.UseFont = true;
            this.TxtIssuer.Properties.MaxLength = 16;
            this.TxtIssuer.Properties.ReadOnly = true;
            this.TxtIssuer.Size = new System.Drawing.Size(319, 20);
            this.TxtIssuer.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(67, 52);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Issuer";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(52, 32);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Currency";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(114, 30);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 200;
            this.LueCurCode.Size = new System.Drawing.Size(319, 20);
            this.LueCurCode.TabIndex = 22;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(76, 95);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 14);
            this.label23.TabIndex = 27;
            this.label23.Text = "UoM";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentUomCode
            // 
            this.LueInvestmentUomCode.EnterMoveNextControl = true;
            this.LueInvestmentUomCode.Location = new System.Drawing.Point(114, 92);
            this.LueInvestmentUomCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueInvestmentUomCode.Name = "LueInvestmentUomCode";
            this.LueInvestmentUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentUomCode.Properties.DropDownRows = 30;
            this.LueInvestmentUomCode.Properties.NullText = "[Empty]";
            this.LueInvestmentUomCode.Properties.PopupWidth = 200;
            this.LueInvestmentUomCode.Size = new System.Drawing.Size(319, 20);
            this.LueInvestmentUomCode.TabIndex = 28;
            this.LueInvestmentUomCode.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(114, 134);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(319, 20);
            this.MeeRemark.TabIndex = 32;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(59, 137);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 31;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(52, 12);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentCtCode
            // 
            this.LueInvestmentCtCode.EnterMoveNextControl = true;
            this.LueInvestmentCtCode.Location = new System.Drawing.Point(114, 9);
            this.LueInvestmentCtCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.LueInvestmentCtCode.Name = "LueInvestmentCtCode";
            this.LueInvestmentCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentCtCode.Properties.DropDownRows = 30;
            this.LueInvestmentCtCode.Properties.NullText = "[Empty]";
            this.LueInvestmentCtCode.Properties.PopupWidth = 350;
            this.LueInvestmentCtCode.Size = new System.Drawing.Size(319, 20);
            this.LueInvestmentCtCode.TabIndex = 20;
            this.LueInvestmentCtCode.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Size = new System.Drawing.Size(532, 242);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Fundamental";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.DteStatementDt);
            this.panel4.Controls.Add(this.TxtYearEnd);
            this.panel4.Controls.Add(this.TxtIssuedShares);
            this.panel4.Controls.Add(this.TxtMarketCap);
            this.panel4.Controls.Add(this.TxtSector);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.TxtStockIndex);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.DteListingDt);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(2, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(528, 238);
            this.panel4.TabIndex = 119;
            // 
            // DteStatementDt
            // 
            this.DteStatementDt.EditValue = null;
            this.DteStatementDt.EnterMoveNextControl = true;
            this.DteStatementDt.Location = new System.Drawing.Point(126, 10);
            this.DteStatementDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStatementDt.Name = "DteStatementDt";
            this.DteStatementDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteStatementDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStatementDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteStatementDt.Properties.Appearance.Options.UseFont = true;
            this.DteStatementDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStatementDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStatementDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStatementDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStatementDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStatementDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStatementDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStatementDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStatementDt.Properties.MaxLength = 8;
            this.DteStatementDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStatementDt.Size = new System.Drawing.Size(121, 20);
            this.DteStatementDt.TabIndex = 36;
            // 
            // TxtYearEnd
            // 
            this.TxtYearEnd.EnterMoveNextControl = true;
            this.TxtYearEnd.Location = new System.Drawing.Point(126, 31);
            this.TxtYearEnd.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtYearEnd.Name = "TxtYearEnd";
            this.TxtYearEnd.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtYearEnd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtYearEnd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtYearEnd.Properties.Appearance.Options.UseFont = true;
            this.TxtYearEnd.Properties.MaxLength = 80;
            this.TxtYearEnd.Size = new System.Drawing.Size(283, 20);
            this.TxtYearEnd.TabIndex = 38;
            // 
            // TxtIssuedShares
            // 
            this.TxtIssuedShares.EnterMoveNextControl = true;
            this.TxtIssuedShares.Location = new System.Drawing.Point(126, 53);
            this.TxtIssuedShares.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtIssuedShares.Name = "TxtIssuedShares";
            this.TxtIssuedShares.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIssuedShares.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIssuedShares.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIssuedShares.Properties.Appearance.Options.UseFont = true;
            this.TxtIssuedShares.Properties.MaxLength = 80;
            this.TxtIssuedShares.Size = new System.Drawing.Size(283, 20);
            this.TxtIssuedShares.TabIndex = 40;
            // 
            // TxtMarketCap
            // 
            this.TxtMarketCap.EnterMoveNextControl = true;
            this.TxtMarketCap.Location = new System.Drawing.Point(126, 74);
            this.TxtMarketCap.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtMarketCap.Name = "TxtMarketCap";
            this.TxtMarketCap.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMarketCap.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMarketCap.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMarketCap.Properties.Appearance.Options.UseFont = true;
            this.TxtMarketCap.Properties.MaxLength = 80;
            this.TxtMarketCap.Size = new System.Drawing.Size(283, 20);
            this.TxtMarketCap.TabIndex = 42;
            // 
            // TxtSector
            // 
            this.TxtSector.EnterMoveNextControl = true;
            this.TxtSector.Location = new System.Drawing.Point(126, 137);
            this.TxtSector.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtSector.Name = "TxtSector";
            this.TxtSector.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtSector.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSector.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSector.Properties.Appearance.Options.UseFont = true;
            this.TxtSector.Properties.MaxLength = 80;
            this.TxtSector.Size = new System.Drawing.Size(283, 20);
            this.TxtSector.TabIndex = 47;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(80, 141);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 14);
            this.label12.TabIndex = 46;
            this.label12.Text = "Sector";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStockIndex
            // 
            this.TxtStockIndex.EnterMoveNextControl = true;
            this.TxtStockIndex.Location = new System.Drawing.Point(126, 95);
            this.TxtStockIndex.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtStockIndex.Name = "TxtStockIndex";
            this.TxtStockIndex.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtStockIndex.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStockIndex.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStockIndex.Properties.Appearance.Options.UseFont = true;
            this.TxtStockIndex.Properties.MaxLength = 80;
            this.TxtStockIndex.Size = new System.Drawing.Size(283, 20);
            this.TxtStockIndex.TabIndex = 44;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(52, 76);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 14);
            this.label43.TabIndex = 41;
            this.label43.Text = "Market Cap";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(49, 98);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(73, 14);
            this.label44.TabIndex = 43;
            this.label44.Text = "Stock Index";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.TxtSales);
            this.panel6.Controls.Add(this.TxtAsset);
            this.panel6.Controls.Add(this.TxtEquity);
            this.panel6.Controls.Add(this.TxtNetCashFlow);
            this.panel6.Controls.Add(this.TxtLiability);
            this.panel6.Controls.Add(this.TxtNetProfit);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.TxtOperatingProfit);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.label33);
            this.panel6.Controls.Add(this.label42);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(104, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(424, 238);
            this.panel6.TabIndex = 52;
            // 
            // TxtSales
            // 
            this.TxtSales.EnterMoveNextControl = true;
            this.TxtSales.Location = new System.Drawing.Point(112, 10);
            this.TxtSales.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtSales.Name = "TxtSales";
            this.TxtSales.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSales.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSales.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSales.Properties.Appearance.Options.UseFont = true;
            this.TxtSales.Properties.MaxLength = 80;
            this.TxtSales.Size = new System.Drawing.Size(283, 20);
            this.TxtSales.TabIndex = 49;
            // 
            // TxtAsset
            // 
            this.TxtAsset.EnterMoveNextControl = true;
            this.TxtAsset.Location = new System.Drawing.Point(112, 53);
            this.TxtAsset.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtAsset.Name = "TxtAsset";
            this.TxtAsset.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsset.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsset.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsset.Properties.Appearance.Options.UseFont = true;
            this.TxtAsset.Properties.MaxLength = 80;
            this.TxtAsset.Size = new System.Drawing.Size(283, 20);
            this.TxtAsset.TabIndex = 53;
            // 
            // TxtEquity
            // 
            this.TxtEquity.EnterMoveNextControl = true;
            this.TxtEquity.Location = new System.Drawing.Point(112, 32);
            this.TxtEquity.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtEquity.Name = "TxtEquity";
            this.TxtEquity.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEquity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEquity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEquity.Properties.Appearance.Options.UseFont = true;
            this.TxtEquity.Properties.MaxLength = 80;
            this.TxtEquity.Size = new System.Drawing.Size(283, 20);
            this.TxtEquity.TabIndex = 51;
            // 
            // TxtNetCashFlow
            // 
            this.TxtNetCashFlow.EnterMoveNextControl = true;
            this.TxtNetCashFlow.Location = new System.Drawing.Point(112, 95);
            this.TxtNetCashFlow.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtNetCashFlow.Name = "TxtNetCashFlow";
            this.TxtNetCashFlow.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNetCashFlow.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetCashFlow.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNetCashFlow.Properties.Appearance.Options.UseFont = true;
            this.TxtNetCashFlow.Properties.MaxLength = 80;
            this.TxtNetCashFlow.Size = new System.Drawing.Size(283, 20);
            this.TxtNetCashFlow.TabIndex = 57;
            // 
            // TxtLiability
            // 
            this.TxtLiability.EnterMoveNextControl = true;
            this.TxtLiability.Location = new System.Drawing.Point(112, 74);
            this.TxtLiability.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtLiability.Name = "TxtLiability";
            this.TxtLiability.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLiability.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLiability.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLiability.Properties.Appearance.Options.UseFont = true;
            this.TxtLiability.Properties.MaxLength = 80;
            this.TxtLiability.Size = new System.Drawing.Size(283, 20);
            this.TxtLiability.TabIndex = 55;
            // 
            // TxtNetProfit
            // 
            this.TxtNetProfit.EnterMoveNextControl = true;
            this.TxtNetProfit.Location = new System.Drawing.Point(112, 137);
            this.TxtNetProfit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtNetProfit.Name = "TxtNetProfit";
            this.TxtNetProfit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNetProfit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetProfit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNetProfit.Properties.Appearance.Options.UseFont = true;
            this.TxtNetProfit.Properties.MaxLength = 80;
            this.TxtNetProfit.Size = new System.Drawing.Size(283, 20);
            this.TxtNetProfit.TabIndex = 61;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(46, 141);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 14);
            this.label14.TabIndex = 60;
            this.label14.Text = "Net Profit";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOperatingProfit
            // 
            this.TxtOperatingProfit.EnterMoveNextControl = true;
            this.TxtOperatingProfit.Location = new System.Drawing.Point(112, 116);
            this.TxtOperatingProfit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtOperatingProfit.Name = "TxtOperatingProfit";
            this.TxtOperatingProfit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtOperatingProfit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOperatingProfit.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOperatingProfit.Properties.Appearance.Options.UseFont = true;
            this.TxtOperatingProfit.Properties.MaxLength = 80;
            this.TxtOperatingProfit.Size = new System.Drawing.Size(283, 20);
            this.TxtOperatingProfit.TabIndex = 59;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(11, 120);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 14);
            this.label13.TabIndex = 58;
            this.label13.Text = "Operating Profit";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(69, 55);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 14);
            this.label25.TabIndex = 52;
            this.label25.Text = "Asset";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(71, 13);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 14);
            this.label20.TabIndex = 48;
            this.label20.Text = "Sales";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(64, 34);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 14);
            this.label21.TabIndex = 50;
            this.label21.Text = "Equity";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(20, 97);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 14);
            this.label33.TabIndex = 56;
            this.label33.Text = "Net Cash Flow";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(57, 76);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(45, 14);
            this.label42.TabIndex = 54;
            this.label42.Text = "Liability";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(5, 12);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(116, 14);
            this.label35.TabIndex = 35;
            this.label35.Text = "Financial Stmt. Date";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteListingDt
            // 
            this.DteListingDt.EditValue = null;
            this.DteListingDt.EnterMoveNextControl = true;
            this.DteListingDt.Location = new System.Drawing.Point(126, 116);
            this.DteListingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteListingDt.Name = "DteListingDt";
            this.DteListingDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteListingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteListingDt.Properties.Appearance.Options.UseFont = true;
            this.DteListingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteListingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteListingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteListingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteListingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteListingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteListingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteListingDt.Properties.MaxLength = 8;
            this.DteListingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteListingDt.Size = new System.Drawing.Size(121, 20);
            this.DteListingDt.TabIndex = 46;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(50, 119);
            this.label34.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 14);
            this.label34.TabIndex = 45;
            this.label34.Text = "Listing Date";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(38, 55);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(82, 14);
            this.label36.TabIndex = 39;
            this.label36.Text = "Issued Shares";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(15, 33);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(105, 14);
            this.label37.TabIndex = 37;
            this.label37.Text = "Financial Year End";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgEarnings
            // 
            this.TpgEarnings.Controls.Add(this.panel3);
            this.TpgEarnings.Location = new System.Drawing.Point(4, 26);
            this.TpgEarnings.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgEarnings.Name = "TpgEarnings";
            this.TpgEarnings.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgEarnings.Size = new System.Drawing.Size(532, 242);
            this.TpgEarnings.TabIndex = 2;
            this.TpgEarnings.Text = "Earnings";
            this.TpgEarnings.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.TxtShareCashEqvl);
            this.panel3.Controls.Add(this.TxtShareAssetValue);
            this.panel3.Controls.Add(this.TxtShareDividen);
            this.panel3.Controls.Add(this.TxtShareEarning);
            this.panel3.Controls.Add(this.TxtShareRevenue);
            this.panel3.Controls.Add(this.TxtShareBookValue);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.TxtShareCashFlow);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(528, 238);
            this.panel3.TabIndex = 119;
            // 
            // TxtShareCashEqvl
            // 
            this.TxtShareCashEqvl.EnterMoveNextControl = true;
            this.TxtShareCashEqvl.Location = new System.Drawing.Point(178, 111);
            this.TxtShareCashEqvl.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtShareCashEqvl.Name = "TxtShareCashEqvl";
            this.TxtShareCashEqvl.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShareCashEqvl.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShareCashEqvl.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShareCashEqvl.Properties.Appearance.Options.UseFont = true;
            this.TxtShareCashEqvl.Properties.MaxLength = 80;
            this.TxtShareCashEqvl.Size = new System.Drawing.Size(283, 20);
            this.TxtShareCashEqvl.TabIndex = 73;
            this.TxtShareCashEqvl.Validated += new System.EventHandler(this.TxtShareCashEqvl_Validated);
            // 
            // TxtShareAssetValue
            // 
            this.TxtShareAssetValue.EnterMoveNextControl = true;
            this.TxtShareAssetValue.Location = new System.Drawing.Point(178, 131);
            this.TxtShareAssetValue.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtShareAssetValue.Name = "TxtShareAssetValue";
            this.TxtShareAssetValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShareAssetValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShareAssetValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShareAssetValue.Properties.Appearance.Options.UseFont = true;
            this.TxtShareAssetValue.Properties.MaxLength = 80;
            this.TxtShareAssetValue.Size = new System.Drawing.Size(283, 20);
            this.TxtShareAssetValue.TabIndex = 75;
            this.TxtShareAssetValue.Validated += new System.EventHandler(this.TxtShareAssetValue_Validated);
            // 
            // TxtShareDividen
            // 
            this.TxtShareDividen.EnterMoveNextControl = true;
            this.TxtShareDividen.Location = new System.Drawing.Point(178, 7);
            this.TxtShareDividen.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtShareDividen.Name = "TxtShareDividen";
            this.TxtShareDividen.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShareDividen.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShareDividen.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShareDividen.Properties.Appearance.Options.UseFont = true;
            this.TxtShareDividen.Properties.MaxLength = 80;
            this.TxtShareDividen.Size = new System.Drawing.Size(283, 20);
            this.TxtShareDividen.TabIndex = 63;
            this.TxtShareDividen.Validated += new System.EventHandler(this.TxtShareDividen_Validated);
            // 
            // TxtShareEarning
            // 
            this.TxtShareEarning.EnterMoveNextControl = true;
            this.TxtShareEarning.Location = new System.Drawing.Point(178, 27);
            this.TxtShareEarning.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtShareEarning.Name = "TxtShareEarning";
            this.TxtShareEarning.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShareEarning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShareEarning.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShareEarning.Properties.Appearance.Options.UseFont = true;
            this.TxtShareEarning.Properties.MaxLength = 80;
            this.TxtShareEarning.Size = new System.Drawing.Size(283, 20);
            this.TxtShareEarning.TabIndex = 65;
            this.TxtShareEarning.Validated += new System.EventHandler(this.TxtShareEarning_Validated);
            // 
            // TxtShareRevenue
            // 
            this.TxtShareRevenue.EnterMoveNextControl = true;
            this.TxtShareRevenue.Location = new System.Drawing.Point(178, 48);
            this.TxtShareRevenue.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtShareRevenue.Name = "TxtShareRevenue";
            this.TxtShareRevenue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShareRevenue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShareRevenue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShareRevenue.Properties.Appearance.Options.UseFont = true;
            this.TxtShareRevenue.Properties.MaxLength = 80;
            this.TxtShareRevenue.Size = new System.Drawing.Size(283, 20);
            this.TxtShareRevenue.TabIndex = 67;
            this.TxtShareRevenue.Validated += new System.EventHandler(this.TxtShareRevenue_Validated);
            // 
            // TxtShareBookValue
            // 
            this.TxtShareBookValue.EnterMoveNextControl = true;
            this.TxtShareBookValue.Location = new System.Drawing.Point(178, 69);
            this.TxtShareBookValue.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtShareBookValue.Name = "TxtShareBookValue";
            this.TxtShareBookValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShareBookValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShareBookValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShareBookValue.Properties.Appearance.Options.UseFont = true;
            this.TxtShareBookValue.Properties.MaxLength = 80;
            this.TxtShareBookValue.Size = new System.Drawing.Size(283, 20);
            this.TxtShareBookValue.TabIndex = 69;
            this.TxtShareBookValue.Validated += new System.EventHandler(this.TxtShareBookValue_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(36, 133);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(128, 14);
            this.label17.TabIndex = 74;
            this.label17.Text = "Asset Value Per Share";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShareCashFlow
            // 
            this.TxtShareCashFlow.EnterMoveNextControl = true;
            this.TxtShareCashFlow.Location = new System.Drawing.Point(178, 90);
            this.TxtShareCashFlow.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtShareCashFlow.Name = "TxtShareCashFlow";
            this.TxtShareCashFlow.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShareCashFlow.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShareCashFlow.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShareCashFlow.Properties.Appearance.Options.UseFont = true;
            this.TxtShareCashFlow.Properties.MaxLength = 80;
            this.TxtShareCashFlow.Size = new System.Drawing.Size(283, 20);
            this.TxtShareCashFlow.TabIndex = 71;
            this.TxtShareCashFlow.Validated += new System.EventHandler(this.TxtShareCashFlow_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(37, 71);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 14);
            this.label15.TabIndex = 68;
            this.label15.Text = "Book Value Per Share";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(43, 92);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 14);
            this.label16.TabIndex = 70;
            this.label16.Text = "Cash Flow Per Share";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(52, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(110, 14);
            this.label26.TabIndex = 62;
            this.label26.Text = "Dividend Per Share";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(45, 113);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(115, 14);
            this.label27.TabIndex = 72;
            this.label27.Text = "Cash Eqvl Per Share";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(52, 49);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(112, 14);
            this.label28.TabIndex = 66;
            this.label28.Text = "Revenue Per Share";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(52, 28);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(109, 14);
            this.label29.TabIndex = 64;
            this.label29.Text = "Earnings Per Share";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgValuation
            // 
            this.TpgValuation.Controls.Add(this.panel7);
            this.TpgValuation.Location = new System.Drawing.Point(4, 26);
            this.TpgValuation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgValuation.Name = "TpgValuation";
            this.TpgValuation.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgValuation.Size = new System.Drawing.Size(532, 242);
            this.TpgValuation.TabIndex = 3;
            this.TpgValuation.Text = "Valuation";
            this.TpgValuation.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.TxtDividenYield);
            this.panel7.Controls.Add(this.TxtPriceEarning);
            this.panel7.Controls.Add(this.TxtPriceSales);
            this.panel7.Controls.Add(this.TxtPriceBookvalue);
            this.panel7.Controls.Add(this.TxtPriceCashFlow);
            this.panel7.Controls.Add(this.label19);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.label31);
            this.panel7.Controls.Add(this.label32);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(2, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(528, 238);
            this.panel7.TabIndex = 119;
            // 
            // TxtDividenYield
            // 
            this.TxtDividenYield.EnterMoveNextControl = true;
            this.TxtDividenYield.Location = new System.Drawing.Point(187, 11);
            this.TxtDividenYield.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtDividenYield.Name = "TxtDividenYield";
            this.TxtDividenYield.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDividenYield.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDividenYield.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDividenYield.Properties.Appearance.Options.UseFont = true;
            this.TxtDividenYield.Properties.MaxLength = 80;
            this.TxtDividenYield.Size = new System.Drawing.Size(283, 20);
            this.TxtDividenYield.TabIndex = 77;
            this.TxtDividenYield.Validated += new System.EventHandler(this.TxtDividenYield_Validated);
            // 
            // TxtPriceEarning
            // 
            this.TxtPriceEarning.EnterMoveNextControl = true;
            this.TxtPriceEarning.Location = new System.Drawing.Point(187, 31);
            this.TxtPriceEarning.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtPriceEarning.Name = "TxtPriceEarning";
            this.TxtPriceEarning.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPriceEarning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPriceEarning.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPriceEarning.Properties.Appearance.Options.UseFont = true;
            this.TxtPriceEarning.Properties.MaxLength = 80;
            this.TxtPriceEarning.Size = new System.Drawing.Size(283, 20);
            this.TxtPriceEarning.TabIndex = 79;
            this.TxtPriceEarning.Validated += new System.EventHandler(this.TxtPriceEarning_Validated);
            // 
            // TxtPriceSales
            // 
            this.TxtPriceSales.EnterMoveNextControl = true;
            this.TxtPriceSales.Location = new System.Drawing.Point(187, 52);
            this.TxtPriceSales.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtPriceSales.Name = "TxtPriceSales";
            this.TxtPriceSales.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPriceSales.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPriceSales.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPriceSales.Properties.Appearance.Options.UseFont = true;
            this.TxtPriceSales.Properties.MaxLength = 80;
            this.TxtPriceSales.Size = new System.Drawing.Size(283, 20);
            this.TxtPriceSales.TabIndex = 81;
            this.TxtPriceSales.Validated += new System.EventHandler(this.TxtPriceSales_Validated);
            // 
            // TxtPriceBookvalue
            // 
            this.TxtPriceBookvalue.EnterMoveNextControl = true;
            this.TxtPriceBookvalue.Location = new System.Drawing.Point(187, 73);
            this.TxtPriceBookvalue.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtPriceBookvalue.Name = "TxtPriceBookvalue";
            this.TxtPriceBookvalue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPriceBookvalue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPriceBookvalue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPriceBookvalue.Properties.Appearance.Options.UseFont = true;
            this.TxtPriceBookvalue.Properties.MaxLength = 80;
            this.TxtPriceBookvalue.Size = new System.Drawing.Size(283, 20);
            this.TxtPriceBookvalue.TabIndex = 83;
            this.TxtPriceBookvalue.Validated += new System.EventHandler(this.TxtPriceBookvalue_Validated);
            // 
            // TxtPriceCashFlow
            // 
            this.TxtPriceCashFlow.EnterMoveNextControl = true;
            this.TxtPriceCashFlow.Location = new System.Drawing.Point(187, 94);
            this.TxtPriceCashFlow.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtPriceCashFlow.Name = "TxtPriceCashFlow";
            this.TxtPriceCashFlow.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPriceCashFlow.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPriceCashFlow.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPriceCashFlow.Properties.Appearance.Options.UseFont = true;
            this.TxtPriceCashFlow.Properties.MaxLength = 80;
            this.TxtPriceCashFlow.Size = new System.Drawing.Size(283, 20);
            this.TxtPriceCashFlow.TabIndex = 85;
            this.TxtPriceCashFlow.Validated += new System.EventHandler(this.TxtPriceCashFlow_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(5, 75);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(172, 14);
            this.label19.TabIndex = 82;
            this.label19.Text = "Price Book Value Ratio (PBVR)";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(13, 96);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(163, 14);
            this.label22.TabIndex = 84;
            this.label22.Text = "Price Cash Flow Ratio (PCFR)";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(96, 13);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 14);
            this.label24.TabIndex = 76;
            this.label24.Text = "Dividend Yield";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(48, 53);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(130, 14);
            this.label31.TabIndex = 80;
            this.label31.Text = "Price Sales Ratio (PSR)";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(29, 33);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(148, 14);
            this.label32.TabIndex = 78;
            this.label32.Text = "Price Earnings Ratio (PER)";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgProfitability
            // 
            this.TpgProfitability.Controls.Add(this.panel9);
            this.TpgProfitability.Location = new System.Drawing.Point(4, 26);
            this.TpgProfitability.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgProfitability.Name = "TpgProfitability";
            this.TpgProfitability.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgProfitability.Size = new System.Drawing.Size(532, 242);
            this.TpgProfitability.TabIndex = 4;
            this.TpgProfitability.Text = "Profitability";
            this.TpgProfitability.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.TxtMarginGross);
            this.panel9.Controls.Add(this.TxtMarginOperating);
            this.panel9.Controls.Add(this.TxtMarginNet);
            this.panel9.Controls.Add(this.TxtMarginTax);
            this.panel9.Controls.Add(this.TxtDividenPayout);
            this.panel9.Controls.Add(this.label18);
            this.panel9.Controls.Add(this.label30);
            this.panel9.Controls.Add(this.label38);
            this.panel9.Controls.Add(this.label39);
            this.panel9.Controls.Add(this.label40);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(2, 2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(528, 238);
            this.panel9.TabIndex = 120;
            // 
            // TxtMarginGross
            // 
            this.TxtMarginGross.EnterMoveNextControl = true;
            this.TxtMarginGross.Location = new System.Drawing.Point(211, 11);
            this.TxtMarginGross.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtMarginGross.Name = "TxtMarginGross";
            this.TxtMarginGross.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMarginGross.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMarginGross.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMarginGross.Properties.Appearance.Options.UseFont = true;
            this.TxtMarginGross.Properties.MaxLength = 80;
            this.TxtMarginGross.Size = new System.Drawing.Size(283, 20);
            this.TxtMarginGross.TabIndex = 87;
            this.TxtMarginGross.Validated += new System.EventHandler(this.TxtMarginGross_Validated);
            // 
            // TxtMarginOperating
            // 
            this.TxtMarginOperating.EnterMoveNextControl = true;
            this.TxtMarginOperating.Location = new System.Drawing.Point(211, 32);
            this.TxtMarginOperating.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtMarginOperating.Name = "TxtMarginOperating";
            this.TxtMarginOperating.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMarginOperating.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMarginOperating.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMarginOperating.Properties.Appearance.Options.UseFont = true;
            this.TxtMarginOperating.Properties.MaxLength = 80;
            this.TxtMarginOperating.Size = new System.Drawing.Size(283, 20);
            this.TxtMarginOperating.TabIndex = 89;
            this.TxtMarginOperating.Validated += new System.EventHandler(this.TxtMarginOperating_Validated);
            // 
            // TxtMarginNet
            // 
            this.TxtMarginNet.EnterMoveNextControl = true;
            this.TxtMarginNet.Location = new System.Drawing.Point(211, 53);
            this.TxtMarginNet.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtMarginNet.Name = "TxtMarginNet";
            this.TxtMarginNet.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMarginNet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMarginNet.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMarginNet.Properties.Appearance.Options.UseFont = true;
            this.TxtMarginNet.Properties.MaxLength = 80;
            this.TxtMarginNet.Size = new System.Drawing.Size(283, 20);
            this.TxtMarginNet.TabIndex = 91;
            this.TxtMarginNet.Validated += new System.EventHandler(this.TxtMarginNet_Validated);
            // 
            // TxtMarginTax
            // 
            this.TxtMarginTax.EnterMoveNextControl = true;
            this.TxtMarginTax.Location = new System.Drawing.Point(211, 75);
            this.TxtMarginTax.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtMarginTax.Name = "TxtMarginTax";
            this.TxtMarginTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMarginTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMarginTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMarginTax.Properties.Appearance.Options.UseFont = true;
            this.TxtMarginTax.Properties.MaxLength = 80;
            this.TxtMarginTax.Size = new System.Drawing.Size(283, 20);
            this.TxtMarginTax.TabIndex = 93;
            this.TxtMarginTax.Validated += new System.EventHandler(this.TxtMarginTax_Validated);
            // 
            // TxtDividenPayout
            // 
            this.TxtDividenPayout.EnterMoveNextControl = true;
            this.TxtDividenPayout.Location = new System.Drawing.Point(211, 96);
            this.TxtDividenPayout.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtDividenPayout.Name = "TxtDividenPayout";
            this.TxtDividenPayout.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDividenPayout.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDividenPayout.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDividenPayout.Properties.Appearance.Options.UseFont = true;
            this.TxtDividenPayout.Properties.MaxLength = 80;
            this.TxtDividenPayout.Size = new System.Drawing.Size(283, 20);
            this.TxtDividenPayout.TabIndex = 95;
            this.TxtDividenPayout.Validated += new System.EventHandler(this.TxtDividenPayout_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(10, 78);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(184, 14);
            this.label18.TabIndex = 92;
            this.label18.Text = "Earnings-Int.&Tax Margin (EBITM)";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(73, 99);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(126, 14);
            this.label30.TabIndex = 94;
            this.label30.Text = "Dividend Payout Ratio";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(89, 13);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(108, 14);
            this.label38.TabIndex = 86;
            this.label38.Text = "Gross Profit Margin";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(101, 55);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(99, 14);
            this.label39.TabIndex = 90;
            this.label39.Text = "Net Profit Margin";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(66, 33);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(133, 14);
            this.label40.TabIndex = 88;
            this.label40.Text = "Operating Profit Margin";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgLiquidity
            // 
            this.TpgLiquidity.Controls.Add(this.panel11);
            this.TpgLiquidity.Location = new System.Drawing.Point(4, 26);
            this.TpgLiquidity.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgLiquidity.Name = "TpgLiquidity";
            this.TpgLiquidity.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.TpgLiquidity.Size = new System.Drawing.Size(532, 242);
            this.TpgLiquidity.TabIndex = 5;
            this.TpgLiquidity.Text = "Liquidity";
            this.TpgLiquidity.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.TxtCurrent);
            this.panel11.Controls.Add(this.label49);
            this.panel11.Controls.Add(this.TxtRoe);
            this.panel11.Controls.Add(this.TxtRoa);
            this.panel11.Controls.Add(this.TxtDebtEquity);
            this.panel11.Controls.Add(this.TxtCash);
            this.panel11.Controls.Add(this.TxtQuick);
            this.panel11.Controls.Add(this.label41);
            this.panel11.Controls.Add(this.label45);
            this.panel11.Controls.Add(this.label46);
            this.panel11.Controls.Add(this.label47);
            this.panel11.Controls.Add(this.label48);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(2, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(528, 238);
            this.panel11.TabIndex = 120;
            // 
            // TxtCurrent
            // 
            this.TxtCurrent.EnterMoveNextControl = true;
            this.TxtCurrent.Location = new System.Drawing.Point(123, 112);
            this.TxtCurrent.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtCurrent.Name = "TxtCurrent";
            this.TxtCurrent.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCurrent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurrent.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurrent.Properties.Appearance.Options.UseFont = true;
            this.TxtCurrent.Properties.MaxLength = 80;
            this.TxtCurrent.Size = new System.Drawing.Size(283, 20);
            this.TxtCurrent.TabIndex = 107;
            this.TxtCurrent.Validated += new System.EventHandler(this.TxtCurrent_Validated);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(34, 113);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(79, 14);
            this.label49.TabIndex = 106;
            this.label49.Text = "Current Ratio";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRoe
            // 
            this.TxtRoe.EnterMoveNextControl = true;
            this.TxtRoe.Location = new System.Drawing.Point(123, 7);
            this.TxtRoe.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtRoe.Name = "TxtRoe";
            this.TxtRoe.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRoe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoe.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRoe.Properties.Appearance.Options.UseFont = true;
            this.TxtRoe.Properties.MaxLength = 80;
            this.TxtRoe.Size = new System.Drawing.Size(283, 20);
            this.TxtRoe.TabIndex = 97;
            this.TxtRoe.Validated += new System.EventHandler(this.TxtRoe_Validated);
            // 
            // TxtRoa
            // 
            this.TxtRoa.EnterMoveNextControl = true;
            this.TxtRoa.Location = new System.Drawing.Point(123, 27);
            this.TxtRoa.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtRoa.Name = "TxtRoa";
            this.TxtRoa.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRoa.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoa.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRoa.Properties.Appearance.Options.UseFont = true;
            this.TxtRoa.Properties.MaxLength = 80;
            this.TxtRoa.Size = new System.Drawing.Size(283, 20);
            this.TxtRoa.TabIndex = 99;
            this.TxtRoa.Validated += new System.EventHandler(this.TxtRoa_Validated);
            // 
            // TxtDebtEquity
            // 
            this.TxtDebtEquity.EnterMoveNextControl = true;
            this.TxtDebtEquity.Location = new System.Drawing.Point(123, 48);
            this.TxtDebtEquity.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtDebtEquity.Name = "TxtDebtEquity";
            this.TxtDebtEquity.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDebtEquity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDebtEquity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDebtEquity.Properties.Appearance.Options.UseFont = true;
            this.TxtDebtEquity.Properties.MaxLength = 80;
            this.TxtDebtEquity.Size = new System.Drawing.Size(283, 20);
            this.TxtDebtEquity.TabIndex = 101;
            this.TxtDebtEquity.Validated += new System.EventHandler(this.TxtDebtEquity_Validated);
            // 
            // TxtCash
            // 
            this.TxtCash.EnterMoveNextControl = true;
            this.TxtCash.Location = new System.Drawing.Point(123, 70);
            this.TxtCash.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtCash.Name = "TxtCash";
            this.TxtCash.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCash.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCash.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCash.Properties.Appearance.Options.UseFont = true;
            this.TxtCash.Properties.MaxLength = 80;
            this.TxtCash.Size = new System.Drawing.Size(283, 20);
            this.TxtCash.TabIndex = 103;
            this.TxtCash.Validated += new System.EventHandler(this.TxtCash_Validated);
            // 
            // TxtQuick
            // 
            this.TxtQuick.EnterMoveNextControl = true;
            this.TxtQuick.Location = new System.Drawing.Point(123, 91);
            this.TxtQuick.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtQuick.Name = "TxtQuick";
            this.TxtQuick.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQuick.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQuick.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQuick.Properties.Appearance.Options.UseFont = true;
            this.TxtQuick.Properties.MaxLength = 80;
            this.TxtQuick.Size = new System.Drawing.Size(283, 20);
            this.TxtQuick.TabIndex = 105;
            this.TxtQuick.Validated += new System.EventHandler(this.TxtQuick_Validated);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(48, 71);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(63, 14);
            this.label41.TabIndex = 102;
            this.label41.Text = "Cash Ratio";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(43, 92);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(68, 14);
            this.label45.TabIndex = 104;
            this.label45.Text = "Quick Ratio";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(83, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(30, 14);
            this.label46.TabIndex = 96;
            this.label46.Text = "ROE";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(10, 51);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(103, 14);
            this.label47.TabIndex = 100;
            this.label47.Text = "Debt Equity Ratio";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(82, 30);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(31, 14);
            this.label48.TabIndex = 98;
            this.label48.Text = "ROA";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPortofio
            // 
            this.BtnPortofio.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPortofio.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPortofio.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPortofio.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPortofio.Appearance.Options.UseBackColor = true;
            this.BtnPortofio.Appearance.Options.UseFont = true;
            this.BtnPortofio.Appearance.Options.UseForeColor = true;
            this.BtnPortofio.Appearance.Options.UseTextOptions = true;
            this.BtnPortofio.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPortofio.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPortofio.Image = ((System.Drawing.Image)(resources.GetObject("BtnPortofio.Image")));
            this.BtnPortofio.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPortofio.Location = new System.Drawing.Point(423, 30);
            this.BtnPortofio.Name = "BtnPortofio";
            this.BtnPortofio.Size = new System.Drawing.Size(24, 21);
            this.BtnPortofio.TabIndex = 14;
            this.BtnPortofio.ToolTip = "Find Item";
            this.BtnPortofio.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPortofio.ToolTipTitle = "Run System";
            this.BtnPortofio.Click += new System.EventHandler(this.BtnPortofio_Click);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(422, 10);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // TxtForeignName
            // 
            this.TxtForeignName.EnterMoveNextControl = true;
            this.TxtForeignName.Location = new System.Drawing.Point(155, 74);
            this.TxtForeignName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtForeignName.Name = "TxtForeignName";
            this.TxtForeignName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtForeignName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtForeignName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtForeignName.Properties.Appearance.Options.UseFont = true;
            this.TxtForeignName.Properties.MaxLength = 250;
            this.TxtForeignName.Size = new System.Drawing.Size(451, 20);
            this.TxtForeignName.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(66, 77);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Foreign Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(45, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "Investment Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentEquityCode
            // 
            this.TxtInvestmentEquityCode.EnterMoveNextControl = true;
            this.TxtInvestmentEquityCode.Location = new System.Drawing.Point(155, 11);
            this.TxtInvestmentEquityCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtInvestmentEquityCode.Name = "TxtInvestmentEquityCode";
            this.TxtInvestmentEquityCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentEquityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentEquityCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentEquityCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentEquityCode.Properties.MaxLength = 16;
            this.TxtInvestmentEquityCode.Properties.ReadOnly = true;
            this.TxtInvestmentEquityCode.Size = new System.Drawing.Size(256, 20);
            this.TxtInvestmentEquityCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(10, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Investment Equity Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPortofolioId
            // 
            this.TxtPortofolioId.EnterMoveNextControl = true;
            this.TxtPortofolioId.Location = new System.Drawing.Point(155, 32);
            this.TxtPortofolioId.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtPortofolioId.Name = "TxtPortofolioId";
            this.TxtPortofolioId.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPortofolioId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPortofolioId.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPortofolioId.Properties.Appearance.Options.UseFont = true;
            this.TxtPortofolioId.Properties.MaxLength = 16;
            this.TxtPortofolioId.Properties.ReadOnly = true;
            this.TxtPortofolioId.Size = new System.Drawing.Size(256, 20);
            this.TxtPortofolioId.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(48, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "Investment Code";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(155, 53);
            this.TxtInvestmentName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.MaxLength = 16;
            this.TxtInvestmentName.Properties.ReadOnly = true;
            this.TxtInvestmentName.Size = new System.Drawing.Size(451, 20);
            this.TxtInvestmentName.TabIndex = 16;
            // 
            // FrmInvestmentItemEquity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 372);
            this.Name = "FrmInvestmentItemEquity";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.TpgDetail.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUpdateDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStockExchange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteStatementDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStatementDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYearEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIssuedShares.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarketCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStockIndex.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSales.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEquity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetCashFlow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLiability.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetProfit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOperatingProfit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteListingDt.Properties)).EndInit();
            this.TpgEarnings.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareCashEqvl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareAssetValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareDividen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareEarning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareRevenue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareBookValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShareCashFlow.Properties)).EndInit();
            this.TpgValuation.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDividenYield.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceEarning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceSales.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceBookvalue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPriceCashFlow.Properties)).EndInit();
            this.TpgProfitability.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginGross.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginOperating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginNet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarginTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDividenPayout.Properties)).EndInit();
            this.TpgLiquidity.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurrent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDebtEquity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCash.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQuick.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentEquityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPortofolioId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TpgDetail;
        private System.Windows.Forms.TabPage TpgGeneral;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.SimpleButton BtnPortofio;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        internal DevExpress.XtraEditors.TextEdit TxtForeignName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentEquityCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtPortofolioId;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtIssuer;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtStockExchange;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtSpecification;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteUpdateDt;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtStockIndex;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.DateEdit DteListingDt;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        protected System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.TextEdit TxtSector;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtOperatingProfit;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtNetProfit;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage TpgEarnings;
        protected System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.TextEdit TxtShareCashFlow;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage TpgValuation;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TabPage TpgProfitability;
        private System.Windows.Forms.TabPage TpgLiquidity;
        protected System.Windows.Forms.Panel panel9;
        protected System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtShareCashEqvl;
        internal DevExpress.XtraEditors.TextEdit TxtShareAssetValue;
        internal DevExpress.XtraEditors.TextEdit TxtShareDividen;
        internal DevExpress.XtraEditors.TextEdit TxtShareEarning;
        internal DevExpress.XtraEditors.TextEdit TxtShareRevenue;
        internal DevExpress.XtraEditors.TextEdit TxtShareBookValue;
        internal DevExpress.XtraEditors.TextEdit TxtDividenYield;
        internal DevExpress.XtraEditors.TextEdit TxtPriceEarning;
        internal DevExpress.XtraEditors.TextEdit TxtPriceSales;
        internal DevExpress.XtraEditors.TextEdit TxtPriceBookvalue;
        internal DevExpress.XtraEditors.TextEdit TxtPriceCashFlow;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtMarginGross;
        internal DevExpress.XtraEditors.TextEdit TxtMarginOperating;
        internal DevExpress.XtraEditors.TextEdit TxtMarginNet;
        internal DevExpress.XtraEditors.TextEdit TxtMarginTax;
        internal DevExpress.XtraEditors.TextEdit TxtDividenPayout;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtRoe;
        internal DevExpress.XtraEditors.TextEdit TxtRoa;
        internal DevExpress.XtraEditors.TextEdit TxtDebtEquity;
        internal DevExpress.XtraEditors.TextEdit TxtCash;
        internal DevExpress.XtraEditors.TextEdit TxtQuick;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.TextEdit TxtCurrent;
        private System.Windows.Forms.Label label49;
        internal DevExpress.XtraEditors.TextEdit TxtYearEnd;
        internal DevExpress.XtraEditors.TextEdit TxtIssuedShares;
        internal DevExpress.XtraEditors.TextEdit TxtMarketCap;
        internal DevExpress.XtraEditors.TextEdit TxtSales;
        internal DevExpress.XtraEditors.TextEdit TxtAsset;
        internal DevExpress.XtraEditors.TextEdit TxtEquity;
        internal DevExpress.XtraEditors.TextEdit TxtNetCashFlow;
        internal DevExpress.XtraEditors.TextEdit TxtLiability;
        internal DevExpress.XtraEditors.DateEdit DteStatementDt;
        public DevExpress.XtraEditors.LookUpEdit LueInvestmentCtCode;
        public DevExpress.XtraEditors.LookUpEdit LueCurCode;
        public DevExpress.XtraEditors.LookUpEdit LueInvestmentUomCode;
    }
}