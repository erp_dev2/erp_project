﻿#region Update
/*
    01/03/2023 [WED/MNET] new reporting
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProgressProject : RunSystem.FrmBase6
    {
        #region Field

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProgressProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sm.SetDteCurrentDate(DteAchievementDt);
                SetLueProjectStatus(ref LueProjectCode);
                SetGrd();
                SetSQL();

                Sm.SetLue(LueProjectCode, "3");
                ChkProjectCode.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, E.ProjectName, F.CtName,  ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc, ");
            SQL.AppendLine("Case A.ProcessInd When 'F' Then 'Final' When 'D' Then 'Draft' Else '' End As ProcessInd, ");
            SQL.AppendLine("G.PlanStartDt, G.PlanEndDt, H.StageName, I.TaskName, G.BobotPercentage, G.SettledRevAmt, ");
            SQL.AppendLine("G.EstimatedCost, G.SettleDt, J.ProjectDeliveryDocNo, If(G.SettleDt Is Null, Null, Concat(DateDiff(G.SettleDt, G.PlanEndDt), ' days')) Deviation ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("Inner Join TblSOContracthdr C On B.SOCDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer F On E.CtCode = F.CtCode ");
            SQL.AppendLine("Inner Join TblProjectImplementationDtl5 G On A.DocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblProjectStage H On G.StageCode = H.StageCode ");
            SQL.AppendLine("Inner Join TblProjectTask I On G.TaskCode = I.TaskCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Group_Concat(Distinct T1.DocNo) ProjectDeliveryDocNo, T2.PRJIDocNo, T2.PRJIDNo ");
            SQL.AppendLine("    From TblProjectDeliveryHdr T1 ");
            SQL.AppendLine("    Inner Join TblProjectDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.Status In('A')  ");
            SQL.AppendLine("    Group By T2.PRJIDocNo, T2.PRJIDNo ");
            SQL.AppendLine(") J On A.DocNo = J.PRJIDocNo And G.DNo = J.PRJIDNo ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "Date",
                    "Project Implementation#",
                    "",
                    "Project Name",
                    "Customer",
                    
                    //6-10
                    "Status",
                    "Process",
                    "Plan Start Date",
                    "Plan End Date",
                    "Stage",

                    //11-15
                    "Task",
                    "Weight (%)",
                    "Cumulative Weight (%)",
                    "Settled Revenue"+Environment.NewLine+"Amount",
                    "Estimated Cost Recorded",

                    //16-19
                    "Project Delivery#",
                    "",
                    "Settled Date",
                    "Deviation Days"
                },
                new int[]
                {
                    //0
                    50, 

                    //1-5
                    80, 180, 20, 200, 200, 
                    
                    //6-10
                    100, 100, 100, 100, 150, 

                    //11-15
                    150, 100, 100, 150, 150,

                    //16-19
                    180, 20, 100, 100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1, 8, 9, 18 });
            Sm.GrdColButton(Grd1, new int[] { 3, 17 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19 });            
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                if (Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                    IsTaskDeviationInvalid()
                ) return;

                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = " Where 0 = 0 ";
                string PRJIDocNo = string.Empty;
                decimal CumulativeWeight = 0m;

                if (TxtPDDocNo.Text.Length > 0)
                {
                    Filter += " And J.ProjectDeliveryDocNo Is Not Null And J.ProjectDeliveryDocNo Like '%" + TxtPDDocNo.Text + "%' ";
                }

                if (ChkProjectCode.Checked)
                {
                    if (Sm.GetLue(LueProjectCode) == "1") Filter += " And G.SettledInd = 'N' ";
                    if (Sm.GetLue(LueProjectCode) == "2") Filter += " And G.SettledInd = 'Y' ";
                }

                if (ChkAchievementDt.Checked)
                {
                    Filter += " And IfNull(G.SettleDt, G.PlanEndDt) <= @AchievementDt ";
                }

                if (ChkDeviation.Checked)
                {
                    Filter += " And G.SettleDt Is Not Null And DateDiff(G.SettleDt, G.PlanEndDt) Between " + TxtDeviation.Text.Trim() + " And " + TxtDeviation2.Text.Trim() + " ";
                }

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParamDt(ref cm, "@AchievementDt", Sm.GetDte(DteAchievementDt));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt, A.DocNo, G.DNo; ",
                    new string[]
                    {
                        //0
                        "DocDt",  
                        //1-5
                        "DocNo", "ProjectName", "CtName", "StatusDesc", "ProcessInd",
                        //6-10
                        "PlanStartDt", "PlanEndDt", "StageName", "TaskName", "BobotPercentage",
                        //11-15
                        "SettledRevAmt", "EstimatedCost", "SettleDt", "ProjectDeliveryDocNo", "Deviation"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        if (Row == 0) PRJIDocNo = Sm.DrStr(dr, c[1]);

                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);                        
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        
                        if (PRJIDocNo == Sm.DrStr(dr, c[1]))
                        {
                            CumulativeWeight += Sm.DrDec(dr, c[10]);
                        }

                        if (PRJIDocNo != Sm.DrStr(dr, c[1]))
                        {
                            CumulativeWeight = Sm.DrDec(dr, c[10]);
                            PRJIDocNo = Sm.DrStr(dr, c[1]);
                        }

                        Grd.Cells[Row, 13].Value = CumulativeWeight;

                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0)
            {
                var f = new FrmProjectImplementation3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length > 0)
            {
                int counts = 0;

                string[] docs = Sm.GetGrdStr(Grd1, e.RowIndex, 16).Split(',');
                foreach (var x in docs.Where(w => w.Length > 0))
                {
                    counts++;
                }

                if (counts > 1)
                {
                    Sm.FormShowDialog(new FrmRptProgressProjectDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 16)));
                }
                else
                {
                    var f = new FrmProjectDelivery(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private bool IsTaskDeviationInvalid()
        {
            if (
                (TxtDeviation.Text.Length > 0 && TxtDeviation2.Text.Length == 0) ||
                (TxtDeviation2.Text.Length > 0 && TxtDeviation.Text.Length == 0)
                )
            {
                Sm.StdMsg(mMsgType.Warning, "Both deviation range should be filled.");
                if (TxtDeviation.Text.Length == 0) TxtDeviation.Focus();
                else if (TxtDeviation2.Text.Length == 0) TxtDeviation2.Focus();
                return true;
            }

            if (TxtDeviation.Text.Length > 0 && TxtDeviation2.Text.Length > 0)
            {
                decimal d1 = Decimal.Parse(TxtDeviation.Text);
                decimal d2 = Decimal.Parse(TxtDeviation2.Text);

                if (d1 > d2)
                {
                    Sm.StdMsg(mMsgType.Warning, "Start range should be less or equal to end range.");
                    TxtDeviation.Focus();
                    return true;
                }
            }

            return false;
        }

        private void SetLueProjectStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'On Going' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Finished' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '3' As Col1, 'On Going / Finished' As Col2 ");
            SQL.AppendLine("; ");

            Sm.SetLue2(
             ref Lue,
             SQL.ToString(),
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Impl.#");
        }

        private void TxtPDDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPDDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Delivery#");
        }

        private void TxtDeviation_Validated(object sender, EventArgs e)
        {
            if (TxtDeviation.Text.Length > 0 && TxtDeviation2.Text.Length > 0) ChkDeviation.Checked = true;
        }

        private void TxtDeviation2_Validated(object sender, EventArgs e)
        {
            if (TxtDeviation.Text.Length > 0 && TxtDeviation2.Text.Length > 0) ChkDeviation.Checked = true;
        }

        private void ChkDeviation_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkDeviation.Checked)
            {
                if (TxtDeviation.Text.Length == 0 || TxtDeviation2.Text.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Task Deviation range should be both filled.");

                    if (TxtDeviation.Text.Length == 0) TxtDeviation.Focus();
                    else if (TxtDeviation2.Text.Length == 0) TxtDeviation2.Focus();

                    ChkDeviation.Checked = false;

                    return;
                }
            }
            else
                TxtDeviation.EditValue = TxtDeviation2.EditValue = null;
        }

        private void LueProjectCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProjectCode, new Sm.RefreshLue1(SetLueProjectStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project Status");
        }

        private void DteAchievementDt_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FilterDteSetCheckEdit(this, sender);
            if (DteAchievementDt.EditValue != null) ChkAchievementDt.Checked = true;
            else ChkAchievementDt.Checked = false;
        }

        private void ChkAchievementDt_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkAchievementDt.Checked)
            {
                if (DteAchievementDt.EditValue == null)
                {
                    Sm.StdMsg(mMsgType.Warning, "Achievement Date is empty.");
                    DteAchievementDt.Focus();
                    ChkAchievementDt.Checked = false;
                    return;
                }
            }
            else
            {
                DteAchievementDt.EditValue = null;
            }
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
