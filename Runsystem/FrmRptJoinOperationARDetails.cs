﻿#region Update
/*
    23/08/2021 [TKG/AMKA] New reporting
    25/08/2021 [TKG/AMKA] hanya nomor rekening coa yg tidak menjadi parent saja yg diproses
    16/09/2021 [ICA/AMKA] mengubah formula -> beberapa kolom include OB
    24/09/2021 [WED/AMKA] ubah formula kalkulasi journal berdasarkan parameter COACalculationFormula
                          Profit Center dibuat mandatory
    05/10/2021 [DITA/AMKA] desainer untuk bagian button ngebug UI nya karna sebelumnya di deploy di VS 2012
    10/11/2021 [MYA/AMKA] Membuat filter profit center di header reporting JO - AR Detail terfilter berdasarkan group
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptJoinOperationARDetails : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<String> mlProfitCenter;
        private bool mIsAllProfitCenterSelected = false,
            mIsFilterByProfitCenter = false;

        #endregion

        #region Constructor

        public FrmRptJoinOperationARDetails(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                mlProfitCenter = new List<String>();
                ChkProfitCenterCode.Visible = false;
                var CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueStartFrom, string.Empty);
                Sm.SetLue(LueStartFrom, CurrentDateTime.Substring(0, 4));
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "CODE",
                        "NAME", 
                        "PENDAPATAN/LABA"+Environment.NewLine+"BULAN INI",
                        "PENDAPATAN/LABA"+Environment.NewLine+"TAHUN INI",
                        "PENDAPATAN"+Environment.NewLine+"DARI AWAL S.D"+Environment.NewLine+"BULAN INI",
                        
                        //6-10
                        "PIUTANG"+Environment.NewLine+"(AGING AR)",
                        "UM SETOR LABA"+Environment.NewLine+"BULAN INI",
                        "UM SETOR LABA"+Environment.NewLine+"S.D BULAN INI",
                        "KURANG/LEBIH"+Environment.NewLine+"SETORAN LABA",
                        "BI PEMASARAN"+Environment.NewLine+"DITANGGUHKAN"+Environment.NewLine+"BULAN INI",                       
                        
                        //11-13
                        "BI PEMASARAN"+Environment.NewLine+"YG DITANGGUHKAN"+Environment.NewLine+"S.D BULAN INI",
                        "JUMLAH"+Environment.NewLine+"R/K JO",
                        "TOTAL +/-"+Environment.NewLine+"SETOR"
                    });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        private string GetSQL(string Alias, string subSQL1, string filter, string subSQL2, string COAParent, bool IsNeedOpeningBalance)
        {
            var SQL = new StringBuilder();
            //string AcType = Sm.GetValue("Select AcType From TblCOA Where AcNo = @Param", COAParent);

            if (IsNeedOpeningBalance)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select ProfitCenterCode, ");
                SQL.AppendLine("    Sum( ");
                //SQL.AppendLine("        Case ");
                //SQL.AppendLine("            When T.AcType = '"+AcType+"' then T.Amt ");
                //SQL.AppendLine("            Else T.Amt*-1 ");
                //SQL.AppendLine("        End ");
                SQL.AppendLine("        T.Amt ");
                SQL.AppendLine("    ) As Amt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select T3.ProfitCenterCode, ");
                //SQL.AppendLine("        Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("        Sum( ");
                SQL.AppendLine("            Case When T6.ParValue='2' Then  ");
                SQL.AppendLine("                Case When T5.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("            Else ");
                SQL.AppendLine("                Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("        ) As Amt ");
                SQL.AppendLine("        From TblJournalHdr T1 ");                
                SQL.AppendLine("        Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CCCode Is Not Null ");
                SQL.AppendLine("        Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("            And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        Inner Join TblCOA T4 On T2.AcNo = T4.AcNo ");
                SQL.AppendLine("            And T4.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("        Inner Join TblCOA T5 On T5.Parent Is Null And T5.Level = 1 And Left(T2.AcNo, Length(T5.AcNo)) = T5.AcNo ");
                SQL.AppendLine("        Left Join TblParameter T6 On T6.ParCode = 'COACalculationFormula' ");
                SQL.AppendLine("        Where 0 = 0 ");
                SQL.AppendLine(subSQL1+filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("        Group By T3.ProfitCenterCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select T1.ProfitCenterCode, Sum(T2.Amt) As Amt ");
                SQL.AppendLine("        From TblCOAOpeningBalanceHdr T1 ");
                SQL.AppendLine("        Inner Join TblCOAOpeningBalanceDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("            And T1.CancelInd='N' ");
                SQL.AppendLine("            And T1.Yr=@StartFrom ");
                SQL.AppendLine("        Inner Join TblCOA T3 On T2.AcNo = T3.AcNo ");
                SQL.AppendLine("            And T3.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("        Where 0 = 0 ");
                SQL.AppendLine(subSQL1);
                SQL.AppendLine(subSQL2.Replace("A.", "T1."));
                SQL.AppendLine("        Group By T1.ProfitCenterCode ");
                SQL.AppendLine("    ) T Group By ProfitCenterCode ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");

            }
            else
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select T3.ProfitCenterCode, ");
                //SQL.AppendLine("    Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("    Sum( ");
                SQL.AppendLine("        Case When T6.ParValue='2' Then  ");
                SQL.AppendLine("            Case When T5.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    ) As Amt ");
                SQL.AppendLine("    From TblJournalHdr T1 ");
                SQL.AppendLine("    Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CCCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("        And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblCOA T4 On T2.AcNo = T4.AcNo ");
                SQL.AppendLine("        And T4.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("    Inner Join TblCOA T5 On T5.Parent Is Null And T5.Level = 1 And Left(T2.AcNo, Length(T5.AcNo)) = T5.AcNo ");
                SQL.AppendLine("    Left Join TblParameter T6 On T6.ParCode = 'COACalculationFormula' ");
                SQL.AppendLine("    Where 0 = 0 ");
                SQL.AppendLine(subSQL1+filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("    Group By T3.ProfitCenterCode ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");
            }

            return SQL.ToString();
        }

        private string GetSQL()
        { 
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();

            var Filter1 = " And Left(T1.DocDt, 6)=@YrMth "; // hanya bulan ini saja
            var Filter2 = " And T1.DocDt>=@Dt1 And T1.DocDt<@Dt2 "; // awal tahun sampai bulan ini
            var Filter3 = " And T1.DocDt>=@Dt3 And T1.DocDt<@Dt2 "; // awal ada data sampai bulan ini

            if (!mIsAllProfitCenterSelected)
            {
                var Filter = string.Empty;
                int i = 0;
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                    i++;
                }
                if (Filter.Length == 0)
                    SQL2.AppendLine("    And 1=0 ");
                else
                    SQL2.AppendLine("    And (" + Filter + ") ");
            }
            else
            {
                if (ChkProfitCenterCode.Checked) SQL2.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
            }

            SQL.AppendLine("Select A.ProfitCenterCode, A.ProfitCenterName, ");
            SQL.AppendLine("IfNull(B.Amt, 0.00) As Value1, ");
            SQL.AppendLine("IfNull(C.Amt, 0.00) As Value2, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00) As Value3, ");
            SQL.AppendLine("IfNull(E.Amt, 0.00) As Value4, ");
            SQL.AppendLine("IfNull(F.Amt, 0.00) As Value5, ");
            SQL.AppendLine("IfNull(G.Amt, 0.00) As Value6, ");
            SQL.AppendLine("IfNull(H.Amt, 0.00) As Value7, ");
            SQL.AppendLine("IfNull(I.Amt, 0.00) As Value8, ");
            SQL.AppendLine("IfNull(J.Amt, 0.00) As Value9 ");
            SQL.AppendLine("From TblProfitCenter A ");
            SQL.AppendLine(GetSQL("B", " And T2.AcNo Like '4%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("C", " And T2.AcNo Like '4%' ", Filter2, SQL2.ToString(), "4", true));
            SQL.AppendLine(GetSQL("D", " And T2.AcNo Like '4%' ", Filter3, SQL2.ToString(), "4", true));
            SQL.AppendLine(GetSQL("E", " And T2.AcNo Like '1.1.3.3%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine(GetSQL("F", " And T2.AcNo Like '2.1.5.2%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("G", " And T2.AcNo Like '2.1.5.2%' ", Filter2, SQL2.ToString(), "2", true));
            SQL.AppendLine(GetSQL("H", " And T2.AcNo Like '1.3.2.1%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("I", " And T2.AcNo Like '1.3.2.1%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine(GetSQL("J", " And T2.AcNo Like '1.1.7%' ", Filter2, SQL2.ToString(), "1", true));
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine(SQL2.ToString());
          
            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (IsProfitCenterInvalid() || Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month") || Sm.IsLueEmpty(LueYr, "Start from")) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var cm = new MySqlCommand();
            string Yr = Sm.GetLue(LueYr), StartFrom = Sm.GetLue(LueStartFrom);
            string Dt1 = string.Concat(Yr, "0101");
            string YrMth = string.Concat(Yr, Sm.GetLue(LueMth));
            string Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(string.Concat(YrMth, "01")).AddMonths(1)), 8);
            string Dt3 = string.Concat(StartFrom, "0101");

            Sm.CmParam<String>(ref cm, "@StartFrom", StartFrom);
            Sm.CmParam<String>(ref cm, "@YrMth", YrMth); // bulan ini
            Sm.CmParamDt(ref cm, "@Dt1", Dt1); // 1 jan 
            Sm.CmParamDt(ref cm, "@Dt2", Dt2); // 1 bulan berikutnya
            Sm.CmParamDt(ref cm, "@Dt3", Dt3); // 1 jan dari start from

            try
            {
                SetProfitCenter();

                if (!mIsAllProfitCenterSelected)
                {
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[]
                    {
                        //0
                        "ProfitCenterCode",

                        //1-5
                        "ProfitCenterName", "Value1", "Value2", "Value3", "Value4", 

                        //6-10
                        "Value5", "Value6", "Value7", "Value8", "Value9" 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Grd.Cells[Row, 9].Value = Sm.GetGrdDec(Grd, Row, 6) - Sm.GetGrdDec(Grd, Row, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Grd.Cells[Row, 13].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 12);
                    }, true, false, false, true
                );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        #endregion

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T1.ProfitCenterName As Col, T1.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T1 ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T1.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }

        #endregion

        #endregion
    }
}
