﻿#region Update
/*
   14/01/2022 [WED/R1] Bug query total freq dan total amount saat show data, param UserCode belum ikut ter alias
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasing4 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string DocDt = string.Empty;
        internal bool mIsReportingFilterMonthMandatory = false;
        private bool mIsShowForeignName = false, mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmRptPurchasing4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                
                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueCurCode(ref LueCurCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsReportingFilterMonthMandatory = Sm.GetParameterBoo("IsReportingFilterMonthMandatory");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.FilterMonth, T.Year, T.Month, T.ItCode, T1.ItCtCode, T.CurCode,  ");
            SQL.AppendLine("T1.Itname, T1.ForeignName, T2.ItCtname, T.FreqOrder, T.TotalOrderAmount, T.AvgPtDay, T.AvgPrice  ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select "); 
            SQL.AppendLine("    Left(T1.DocDt, 6) As FilterMonth, left(T1.DocDt, 4) As Year, substring(T1.DocDt, 5, 2) As Month,  ");
	        SQL.AppendLine("     T4.ItCode, T5.CurCode, ");
            SQL.AppendLine("    Round(Avg(T9.PtDay), 2) As AvgPtDay, Avg(T6.UPrice) As AvgPrice, ");
            SQL.AppendLine("    Count(T1.DocNo) As FreqOrder, Sum(T2.Qty*T6.UPrice) As TotalOrderAmount ");
            SQL.AppendLine("    From TblPOHdr T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.POREquestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo And T3.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T4 On T3.MaterialRequestDocNo=T4.DocNo And T3.MaterialRequestDNo=T4.DNo And T4.CancelInd = 'N'  ");
            SQL.AppendLine("    Inner Join TblQtHdr T5 On T3.QtDocNo=T5.DocNo ");
            SQL.AppendLine("    Inner Join TblQtDtl T6 On T3.QtDocNo=T6.DocNo And T3.QtDNo=T6.DNo ");
            SQL.AppendLine("    Left Join TblPaymentTerm T9 On T5.PtCode=T9.PtCode ");
            SQL.AppendLine("    Group By Left(T1.DocDt, 6), left(T1.DocDt, 4), Substring(T1.DocDt, 5, 2), T4.ItCode, T5.CurCode ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Inner Join TblItem T1 On T.ItCode=T1.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T1.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblItemcategory T2 On T1.ItCtCode = T2.ItCtCode ");
            SQL.AppendLine("Where T.FilterMonth Like @FilterMonth ");
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Currency",
                        "Month",
                        "Item's Category",
                        "Item's"+Environment.NewLine+"Code", 
                        "Item's Name",
                        
                        //6-10
                        "Average"+Environment.NewLine+"TOP (days)", 
                        "Average"+Environment.NewLine+"Price",
                        "Total Order"+Environment.NewLine+"Frequency",
                        "Order Frequency"+Environment.NewLine+"%",
                        "Total Order"+Environment.NewLine+"Amount",

                        //11-12
                        "Order Amount"+Environment.NewLine+"%",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        60, 0, 150, 80, 200, 
                        
                        //6-10
                        150, 150, 100, 150, 180, 
                        
                        //11-12
                        100, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 8, 9, 10, 11 }, 0);
            Grd1.Cols[12].Move(6);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4 }, false);
            if (!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            Sm.GrdColPercentBar(Grd1, new int[] { 9, 11 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.GetLue(LueYr) == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }

            else
            {
                try
                {
                    string Year = Sm.GetLue(LueYr);
                    string TotalFreqOrder = string.Empty;
                    string TotalOrderAmt = string.Empty; 

                    if (Sm.GetLue(LueMth) != string.Empty)
                    {
                        string Month = Sm.GetLue(LueMth);
                        DocDt = string.Concat(Year, Month);
                        TotalFreqOrder = Sm.GetValue("Select Sum(X.FreqOrder) As Total From (" + mSQL.Replace("@FilterMonth", "'" + DocDt + "'").Replace("@UserCode", string.Concat("'", Gv.CurrentUserCode, "'"))+ ") X");
                        TotalOrderAmt = Sm.GetValue("Select Sum(X.TotalOrderAmount) As Total From (" + mSQL.Replace("@FilterMonth", "'" + DocDt + "'").Replace("@UserCode", string.Concat("'", Gv.CurrentUserCode, "'")) + ") X");
                    }
                    else
                    {
                        DocDt = string.Concat(Year, '%');
                        TotalFreqOrder = Sm.GetValue("Select Sum(X.FreqOrder) As Total From (" + mSQL.Replace("@FilterMonth", "'" + DocDt + "'").Replace("@UserCode", string.Concat("'", Gv.CurrentUserCode, "'")) + ") X");
                        TotalOrderAmt = Sm.GetValue("Select Sum(X.TotalOrderAmount) As Total From (" + mSQL.Replace("@FilterMonth", "'" + DocDt + "'").Replace("@UserCode", string.Concat("'", Gv.CurrentUserCode, "'")) + ") X");
                    }

                    
                    decimal TotalFreq = ((TotalFreqOrder == string.Empty) || (TotalFreqOrder == "0")) ? 0 : Convert.ToDecimal(TotalFreqOrder);
                    decimal TotalAmt = ((TotalOrderAmt == string.Empty) || (TotalOrderAmt == "0")) ? 0 : Convert.ToDecimal(TotalOrderAmt);

                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " ";

                    var cm = new MySqlCommand();

                    Sm.CmParam<String>(ref cm, "@FilterMonth", DocDt);
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                    Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T.ItCode", "T1.ItName", "T1.ForeignName" });
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "T1.ItCtCode", true);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCurCode), "T.CurCode", true);

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By T.CurCode, T.Year, T.Month, T1.ItName, T1.ItCtCode ;",
                            new string[]
                            { 
                                //0
                                "CurCode",  

                                //1-5
                                "FilterMonth", "ItCtName", "ItCode", "ItName", "AvgPtDay", 
                                
                                //6-9
                                "AvgPrice", "FreqOrder", "TotalOrderAmount", "ForeignName"
                            },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdVal("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                                Sm.SetGrdVal("S", Grd, dr, c, Row, 3, 2);
                                Sm.SetGrdVal("S", Grd, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            }, true, true, false, false
                        );
                    for (int intX = 0; intX < Grd1.Rows.Count; intX++)
                    {
                        if (TotalFreq == 0)
                            Grd1.Cells[intX, 9].Value = 0;
                        else
                            Grd1.Cells[intX, 9].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, intX, 8) / TotalFreq);
                    }
                    for (int i = 0; i < Grd1.Rows.Count; i++)
                    {
                        if (TotalAmt == 0)
                            Grd1.Cells[i, 11].Value = 0;
                        else
                            Grd1.Cells[i, 11].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, i, 10) / TotalAmt);
                    }
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 10 });
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #region Grid

        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            Sm.GrdCustomDrawCellForeground(Grd1, new int[] { 9, 11 }, sender, e);
        }

        private void Grd1_CustomDrawCellGetHeight(object sender, iGCustomDrawCellGetHeightEventArgs e)
        {
            e.Height = Font.Height + 4;
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCodeFilterByItCt), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCurCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Currency");
        }

        #endregion
    }
}
