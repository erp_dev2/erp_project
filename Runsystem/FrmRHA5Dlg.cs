﻿#region Update
/* 
    24/01/2019 [TKG] THR KIM
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRHA5Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRHA5 mFrmParent;
        private string
            mSQL = string.Empty,
            mHolidayDt = string.Empty;
        private decimal
            mFunctionalExpenses = 0m,
            mFunctionalExpensesMaxAmt = 0m;
        private List<TI> mlTI = null;
        private List<NTI> mlNTI = null;

        #endregion

        #region Constructor

        public FrmRHA5Dlg(FrmRHA5 FrmParent, string HolidayDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mHolidayDt = HolidayDt;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                GetData();
                SetGrd();
                Sl.SetLueOption(ref LueReligion, "Religion");
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        

                        //6-10
                        "Religion",
                        "Status",
                        "Join",
                        "Resign",
                        "Bank",
                        
                        //11-15                        
                        "Bank"+Environment.NewLine+"Account#",
                        "Value",
                        "Tax",
                        "Amount",
                        "PTKP",

                        //16
                        "NPWP"
                      },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20,  80, 200, 150, 150,   
                        
                        //6-10
                        150, 100, 100, 100, 150, 

                        //11-15
                        150, 120, 120, 130, 120, 
                        
                        //16
                        120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode, EmpName, PosName, DeptName, Religion, EmploymentStatusDesc, ");
            SQL.AppendLine("JoinDt, ResignDt, BankName, BankAcNo, ");
            SQL.AppendLine("Value, Tax, ");
            SQL.AppendLine("Case When Prorate<12.00 Then Prorate/12.00 Else 1.00 End * Amt As Amt, ");
            SQL.AppendLine("PTKP, NPWP ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.EmpCode, A.EmpName, B.PosName, C.DeptName, ");
            SQL.AppendLine("    F.OptDesc As Religion, H.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("    A.JoinDt, A.ResignDt, G.BankName, A.BankAcNo, ");
            SQL.AppendLine("    IfNull(D.Amt, 0.00)+IfNull(E.Amt, 0.00) As Value, ");
            SQL.AppendLine("    0.00 As Tax, ");
            SQL.AppendLine("    IfNull(D.Amt, 0.00)+IfNull(E.Amt, 0.00) As Amt, ");
            SQL.AppendLine("    A.PTKP, A.NPWP, ");
            SQL.AppendLine("    TIMESTAMPDIFF(Month, ");
            SQL.AppendLine("    Concat(Left(A.JoinDt, 4), '-', substring(A.JoinDt, 5, 2), '-', '01') ");
            SQL.AppendLine("    , @HolidayDate) As Prorate ");
            SQL.AppendLine("    From TblEmployee A ");
            SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("    Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblEmployeeSalary T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select EmpCode, Max(StartDt) Dt ");
            SQL.AppendLine("            From TblEmployeeSalary ");
            SQL.AppendLine("            Where StartDt<=@HolidayDt ");
            SQL.AppendLine("            Group By EmpCode ");
            SQL.AppendLine("        ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.Dt ");
            SQL.AppendLine("        Inner Join TblEmployee T3 ");
            SQL.AppendLine("            On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("            And (T3.ResignDt Is Null Or (T3.ResignDt Is Not Null And T3.ResignDt>@HolidayDt)) ");
            SQL.AppendLine(Filter.Replace("A.", "T3."));
            SQL.AppendLine("    ) D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.EmpCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction T2 on T1.ADCode=T2.ADCode And T2.ADType='A' And T2.AmtType='1' ");
            SQL.AppendLine("        Inner Join TblEmployee T3 ");
            SQL.AppendLine("            On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("            And (T3.ResignDt Is Null Or (T3.ResignDt Is Not Null And T3.ResignDt>@HolidayDt)) ");
            SQL.AppendLine(Filter.Replace("A.", "T3."));
            SQL.AppendLine("        Where ( ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt<=@HolidayDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Null And T1.EndDt Is Not Null And @HolidayDt<=T1.EndDt) Or ");
            SQL.AppendLine("        (T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt<=@HolidayDt And @HolidayDt<=T1.EndDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        Group By T1.EmpCode ");
            SQL.AppendLine("    ) E On A.EmpCode=E.EmpCode ");
            SQL.AppendLine("    Left Join TblOption F On A.Religion=F.OptCode And F.OptCat='Religion' ");
            SQL.AppendLine("    Left Join TblBank G On A.BankCode=G.BankCode ");
            SQL.AppendLine("    Left Join TblOption H On A.EmploymentStatus=H.OptCode And H.OptCat='EmploymentStatus' ");
            SQL.AppendLine("    Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@HolidayDt)) ");
            SQL.AppendLine(Filter);
            
            return SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 15, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter =
                    " And A.EmpCode Not In ( " +
                    "   Select Distinct T2.EmpCode " +
                    "   From TblRHAHdr T1 " +
                    "   Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo " +
                    "   Where T1.CancelInd='N' " +
                    "   And T1.HolidayDt=@HolidayDt And T1.Status In ('O', 'A') " +
                    "   ) ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@HolidayDt", mHolidayDt);
                Sm.CmParam<String>(ref cm, "@HolidayDate", 
                    string.Concat(
                        Sm.Left(mHolidayDt, 4), "-",
                        mHolidayDt.Substring(4, 2), "-",
                        "01"
                        ));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueReligion), "A.Religion", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEmploymentStatus), "A.EmploymentStatus", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter) + ") T Order By T.EmpName;",
                        new string[]
                        {
                            //0
                            "EmpCode",
                            
                            //1-5
                            "EmpName", "PosName", "DeptName", "Religion", "EmploymentStatusDesc",
                            
                            //6-10
                            "JoinDt", "ResignDt", "BankName", "BankAcNo", "Value", 
                            
                            //11-14
                            "Tax", "Amt", "PTKP", "NPWP"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        }, true, false, false, false
                    );
                ComputeTax();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 14);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 11, 12 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 1), EmpCode)) return true;
            return false;
        }

        private void ComputeTax()
        {
            var l = new List<RHA>();
            try
            {
                string EmpCodeTemp = string.Empty;

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCodeTemp = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCodeTemp.Length > 0)
                    {
                        l.Add(new RHA()
                        {
                            EmpCode = EmpCodeTemp,
                            Value = Sm.GetGrdDec(Grd1, r, 12),
                            Tax = Sm.GetGrdDec(Grd1, r, 13),
                            THR = Sm.GetGrdDec(Grd1, r, 14),
                            PTKP = Sm.GetGrdStr(Grd1, r, 15),
                            NPWP = Sm.GetGrdStr(Grd1, r, 16)
                        });
                    }
                }

                if (l.Count>0)
                {
                    for(int i =0;i<l.Count;i++)
                    {
                        if (l[i].NPWP.Length != 0 && l[i].PTKP.Length != 0)
                        {
                            ProcessTax(ref l, i);
                            for (int r = 0; r < Grd1.Rows.Count; r++)
                            {
                                if (Sm.CompareStr(l[i].EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                                {
                                    Grd1.Cells[r, 13].Value = l[i].Tax;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                l.Clear();
            }
        }

        private void ProcessTax(ref List<RHA> l, int i)
        {
            string NTI = l[i].PTKP;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                //GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                //PPH21Setahun = 0m,
                PPH21Sebulan = 0m,

                TotalImbalan2 = 0m,
                TotalPengurangan2 = 0m,
                GajiBersihSebulan2 = 0m,
                //GajiBersihDisetahunkan = 0m,
                NTIAmt2 = 0m,
                PKP2 = 0m,
                TaxTemp2 = PKP,
                Amt2Temp2 = 0m,
                //PPH21Setahun = 0m,
                PPH21Sebulan2 = 0m;

            TotalImbalan = (l[i].Value)*12m;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt*12m)
                ? mFunctionalExpensesMaxAmt*12m
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;
            
            foreach (var x in mlNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihSebulan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihSebulan - NTIAmt;

            PPH21Sebulan = decimal.Truncate(PKP);
            PPH21Sebulan = PPH21Sebulan - (PPH21Sebulan % 1000);
            

            if (PPH21Sebulan > 0m && mlTI.Count > 0)
            {
                TaxTemp = PPH21Sebulan;
                Amt2Temp = 0m;
                PPH21Sebulan = 0m;

                foreach (TI x in mlTI.OrderBy(o =>o.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (x.Amt2 - Amt2Temp))
                        {
                            PPH21Sebulan += (TaxTemp * x.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Sebulan += ((x.Amt2 - Amt2Temp) * x.TaxRate * 0.01m);
                            TaxTemp -= (x.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = x.Amt2;
                }
            }

            TotalImbalan2 = TotalImbalan+l[i].THR;

            TotalPengurangan2 =
                ((mFunctionalExpenses * 0.01m * TotalImbalan2 > mFunctionalExpensesMaxAmt*12m)
                ? mFunctionalExpensesMaxAmt*12m
                : mFunctionalExpenses * 0.01m * TotalImbalan2);

            GajiBersihSebulan2 = TotalImbalan2 - TotalPengurangan2;

            foreach (var x in mlNTI.Where(x => x.Status == NTI))
                NTIAmt2 = x.Amt;

            if (GajiBersihSebulan2 - NTIAmt2 < 0m)
                PKP2 = 0m;
            else
                PKP2 = GajiBersihSebulan2 - NTIAmt2;

            PPH21Sebulan2 = decimal.Truncate(PKP2);
            PPH21Sebulan2 = PPH21Sebulan2 - (PPH21Sebulan2 % 1000);


            if (PPH21Sebulan2 > 0m && mlTI.Count > 0)
            {
                TaxTemp2 = PPH21Sebulan2;
                Amt2Temp2 = 0m;
                PPH21Sebulan2 = 0m;

                foreach (TI x in mlTI.OrderBy(o => o.SeqNo))
                {
                    if (TaxTemp2 > 0m)
                    {
                        if (TaxTemp2 <= (x.Amt2 - Amt2Temp2))
                        {
                            PPH21Sebulan2 += (TaxTemp2 * x.TaxRate * 0.01m);
                            TaxTemp2 = 0m;
                        }
                        else
                        {
                            PPH21Sebulan2 += ((x.Amt2 - Amt2Temp2) * x.TaxRate * 0.01m);
                            TaxTemp2 -= (x.Amt2 - Amt2Temp2);
                        }
                    }
                    Amt2Temp2 = x.Amt2;
                }
            }
            l[i].Tax = PPH21Sebulan2 - PPH21Sebulan;
            if (l[i].Tax <= 0) l[i].Tax = 0m;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mFunctionalExpenses = Sm.GetParameterDec("FunctionalExpenses");
            mFunctionalExpensesMaxAmt = Sm.GetParameterDec("FunctionalExpensesMaxAmt");
        }

        private void GetData()
        {
            mlTI = new List<TI>();
            mlNTI = new List<NTI>();

            ProcessTI(ref mlTI);
            ProcessNTI(ref mlNTI);
        }

        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueReligion_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueReligion, new Sm.RefreshLue2(Sl.SetLueOption), "Religion");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkReligion_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Religion");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment status");
        }

        #endregion
       
        #endregion

        #region Class

        private class RHA
        {
            public string EmpCode { get; set; }
            public decimal Value { get; set; }
            public decimal Tax { get; set; }
            public decimal THR { get; set; }
            public string PTKP { get; set; }
            public string NPWP { get; set; }
        }

        private class NTI
        {
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        #endregion
    }
}
