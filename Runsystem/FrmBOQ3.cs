﻿#region Update
/*
    03/01/2023 [MYA/MNET] Penambahan Field upload file di menu Bill Of Quantity
    21/02/2023 [BRI/MNET] tambah validasi, kolom, filter tab revenue item BOQ
    06/03/2023 [MYA/MNET] Membuat FRM baru untuk Bill of Quantity
    15/03/2023 [MYA/MNET] Penyesuaian Replacement Quotation BOQ
    13/04/2023 [MYA/MNET] Mengubah BOQ duration menjadi BOQ End date dan penyesuaian kolom ketika find BOQ
    13/04/2023 [MYA/MNET] Penyesuaian Print out Bill Of Quantity
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmBOQ3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mCtCode = string.Empty;
        internal FrmBOQ3Find FrmFind;
        private decimal mBOQTaxPercentage = 10m;
        private string
            mBOQPrintOutBODName = string.Empty,
            mRptName = string.Empty,
            mBOQDocType = "1",
            mPPNDefaultSetting = string.Empty,
            mItemBOQCalculationFormat = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mItGrpCodeForDirectCost = string.Empty,
            mItGrpCodeForRemuneration = string.Empty,
            mItGrpCodeForIndirectCost = string.Empty;
        private string mStateIndicator = string.Empty;
        internal string 
            mBOQStatusDisplayedInFind = String.Empty,
            mListItCtCodeForResource = string.Empty;
        private bool
            mIsBOQPrintOutShowCreatedUser = false,
            mIsPSModuleShowApproverRemarkInfo = false,
            isinsert = false,
            mIsCustomerContactPersonNonMandatory = false,
            mIsBOQUploadFileMandatory = false;
        internal bool mIsFilterBySite = false, 
            mIsFilterByItCt = false, 
            mIsBOQExcludeCancelledData = false,
            mIsBOQAllowToUploadFile = false;
        
        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmBOQ3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmBOQ3");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                Sl.SetLueCtCode(ref LueCtCode);
                Sl.SetLueRingCode(ref LueRingCode);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLuePtCode(ref LuePtCode);
                Sl.SetLueUserCode(ref LuePICCode);
                //SetLueSPCode(ref LuePICCode);
                Sl.SetLueAgingAP(ref LueAgingAP);
                Sl.SetLueOption(ref LueProcessInd, "ProjectImplementationProcessInd");

                TcBOQ.SelectTab("Tp2");
                TcBOQ.SelectTab("Tp3");
                TcBOQ.SelectTab("Tp4");
                SetGrd2();
                TcBOQ.SelectTab("Tp1");
                SetGrd();
                //TcBOQ.SelectTab("tabPage3");
                //SetGrd3();
                SetLueResourceName(ref LueResourceName);
                if (!mIsBOQAllowToUploadFile)
                {
                    TcBOQ.TabPages.Remove(Tp5);
                }
                LueResourceName.Visible = false;
                Sl.SetLueOption(ref LueResourceType, "ResourceType");
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                LueResourceType.Visible = false;
                LueTaxCode1.Visible = false;
                LueTaxCode2.Visible = false;


                //if (!mIsPSModuleShowApproverRemarkInfo) 
                //{
                //    int ypoint = 21;
                //    label24.Top = label24.Top - ypoint; LueProcessInd.Top = LueProcessInd.Top - ypoint;
                //    label9.Top = label9.Top - ypoint; TxtLOPDocNo.Top = TxtLOPDocNo.Top - ypoint; BtnLOPDocNo.Top = BtnLOPDocNo.Top - ypoint;
                //    label3.Top = label3.Top - ypoint; LueCtCode.Top = LueCtCode.Top - ypoint;
                //    label5.Top = label5.Top - ypoint; LueCtContactPersonName.Top = LueCtContactPersonName.Top - ypoint; BtnCtContactPersonName.Top = BtnCtContactPersonName.Top - ypoint;
                //    label6.Top = label6.Top - ypoint; LueRingCode.Top = LueRingCode.Top - ypoint;
                //    label16.Top = label16.Top - ypoint; LuePICCode.Top = LuePICCode.Top - ypoint;
                //    label8.Top = label8.Top - ypoint; DteBOQStartDt.Top = DteBOQStartDt.Top - ypoint;
                //    label7.Top = label7.Top - ypoint; TxtQtMth.Top = TxtQtMth.Top - ypoint;
                //    label10.Top = label10.Top - ypoint; LuePtCode.Top = LuePtCode.Top - ypoint;
                //    Grd2.Height = Grd2.Height + ypoint;
                //}    
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mCtCode.Length != 0)
                {
                    if (IsUserAbleToInsert())
                    {
                        BtnInsertClick();
                        Sm.SetLue(LueCtCode, mCtCode);
                        Sm.SetControlReadOnly(LueCtCode, true);
                        SetLueCtPersonCode(ref LueCtContactPersonName, mCtCode, string.Empty);
                    }
                    else
                    {
                        BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                    }
                }

                if (mIsCustomerContactPersonNonMandatory)
                    label5.ForeColor = Color.Black;

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd2.Cols.Count = 23;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd2, new string[] 
                {
                    //0
                    "Item BOQ",

                    //1-5
                    "Item BOQ#",
                    "Required",
                    "ResourceTypeCode",
                    "Resource Type",
                    "ResourceNameCode",

                    //6-10
                    "Resource Name",
                    "Quantity",
                    "UoM",
                    "Duration",
                    "Total" + Environment.NewLine + "Unit",
                    
                    //11-15
                    "Unit Price",
                    "Allow",
                    "Amt",
                    "Previous Amount",
                    "Remark",

                    //16-20
                    "TotalAmt",
                    "TotalAmt2",
                    "Total Price",
                    "ParentInd",
                    "Quantity Tmp",

                    //21-22
                    "Duration Tmp",
                    "Total Unit Tmp"
                },
                new int[] 
                {
                    400,
                    150, 100, 0, 150, 0, 
                    200, 100, 100, 80, 100, 
                    150, 20, 150, 150, 200,
                    200, 200, 130, 0, 150,
                    150,150
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 7, 9, 10, 11, 13, 14, 16, 17, 18, 20, 21, 22 }, 0);
            Sm.GrdColCheck(Grd2, new int[] { 2 });
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 3, 5, 10, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22 });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 12, 13, 14, 16, 17, 19, 20, 21, 22 });


            #region Grid Upload File

            if (mIsBOQAllowToUploadFile)
            {
                Grd4.Cols.Count = 7;
                Grd4.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd4,
                        new string[]
                        {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "",
                        "D",
                        "Upload By",
                        "Date",

                        //6
                        "Time"
                        },
                         new int[]
                        {
                        0,
                        250, 20, 20, 100, 100,
                        100
                        }
                    );

                Sm.GrdColInvisible(Grd4, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd4, new int[] { 5 });
                Sm.GrdFormatTime(Grd4, new int[] { 6 });
                Sm.GrdColButton(Grd4, new int[] { 2, 3 });
                Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                Grd4.Cols[2].Move(1);
            }

            #endregion

            #region Grid Revenue Item

            Grd5.Cols.Count = 17;
            Grd5.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd5, new string[]
                {
                    //0
                    "",

                    //1-5
                    "Item's Code",
                    "Item's Name",
                    "Quantity",
                    "UoM",
                    "Price (Price List)",

                    //6-10
                    "Discount Amount",
                    "Revenue Before Tax",
                    "TaxCode1",
                    "Tax 1",
                    "Tax Amount",
                    
                    //11-15
                    "TaxCode2",
                    "Tax 2",
                    "Tax Amount",
                    "Total Revenue Amount",
                    "Remark",

                    //16
                    "Item's Category"
                },
                new int[]
                {
                    20,
                    0, 200, 100, 100, 200, 
                    200, 200, 0, 100, 200, 
                    0, 100, 200, 200, 400,
                    0
                }
            );
            Sm.GrdFormatDec(Grd5, new int[] { 3, 5, 6, 7, 10, 13, 14 }, 0);
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdColButton(Grd5, new int[] { 0 });
            //Sm.GrdColInvisible(Grd5, new int[] { 1 });

            #endregion

            #region Grid Resource Item

            Grd6.Cols.Count = 16;
            Grd6.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd6, new string[]
                {
                    //0
                    "",

                    //1-5
                    "ResourceItCode", 
                    "Resource", 
                    "Group", 
                    "Remark", 
                    "Quantity 1"+Environment.NewLine+"(Unit)",
                    
                    //6-10
                    "Quantity 2"+Environment.NewLine+"(Time)", 
                    "Unit Price", 
                    "Tax", 
                    "Total Without Tax", 
                    "Total Tax",

                    //11-15
                    "Total With Tax", 
                    "Group It Code", 
                    "", 
                    "Detail#", 
                    "Sequence"
                },
                new int[]
                {
                    20,
                    0, 200, 200, 200, 150,
                    150, 180, 180, 200, 200,
                    200,150, 20, 120, 65
                }
            );
            Sm.GrdFormatDec(Grd6, new int[] { 5, 6, 7, 8, 9, 10, 11 }, 8);
            Sm.GrdColButton(Grd6, new int[] { 0, 13 });
            Sm.GrdColReadOnly(Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            //Sm.GrdColReadOnly(Grd6, new int[] { 1, 2, 3, 9, 10, 11, 14 });
            Sm.GrdColInvisible(Grd6, new int[] { 12 });
            Grd6.Cols[15].Move(1);

            #endregion

        }

        private void SetGrd2()
        {
            Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd3, new int[] { 3 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 2, 3, 4 });
        }

        //private void SetGrd3()
        //{
        //    if (mIsBOQAllowToUploadFile)
        //    {
        //        Grd4.Cols.Count = 7;
        //        Grd4.FrozenArea.ColCount = 3;

        //        Sm.GrdHdrWithColWidth(
        //                Grd4,
        //                new string[]
        //                {
        //                //0
        //                "D No",
        //                //1-5
        //                "File Name",
        //                "",
        //                "D",
        //                "Upload By",
        //                "Date",

        //                //6
        //                "Time"
        //                },
        //                 new int[]
        //                {
        //                0,
        //                250, 20, 20, 100, 100,
        //                100
        //                }
        //            );
        //        Sm.GrdColInvisible(Grd4, new int[] { 0 }, false);
        //        Sm.GrdFormatDate(Grd4, new int[] { 5 });
        //        Sm.GrdFormatTime(Grd4, new int[] { 6 });
        //        Sm.GrdColButton(Grd4, new int[] { 2, 3 });
        //        Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6 });
        //        Grd4.Cols[2].Move(1);
        //    }
        //}

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, LueProcessInd, TxtLOPDocNo, TxtProjectName, LueCtCode, LueCtContactPersonName, LueRingCode,
                        LuePICCode, DteBOQStartDt, DteBOQEndDt, TxtSOCDocNo, LueAgingAP, LueCurCode, TxtCreditLimit, LuePtCode, TxtQtReplace,
                        TxtBOQAmt, TxtEstRev, TxtEstRes, TxtMargin, TxtPercentage, MeeRemark
                    }, true);
                    BtnLOPDocNo.Enabled = false;
                    BtnLOP2DocNo.Enabled = true;
                    BtnCt.Enabled = false;
                    BtnCtContactPersonName.Enabled = false;
                    BtnSOCDocNo.Enabled = false;
                    BtnQtReplace.Enabled = false;
                    ChkActInd.Enabled = false;
                    ChkBankGuaranteeInd.Properties.ReadOnly = true;
                    ChkPrintSignatureInd.Properties.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
                    if (mIsBOQAllowToUploadFile)
                    {
                        Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                            {
                                Grd4.Cells[Row, 2].ReadOnly = iGBool.False;
                                Grd4.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd4.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                        }
                    }
                    TxtDocNo.Focus();

                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {
                        DteDocDt, LueRingCode, DteBOQStartDt, DteBOQEndDt, 
                        LueAgingAP, LueCurCode, TxtCreditLimit, LuePtCode, MeeRemark
                    }, false);
                    DteDocDt.Focus();
                    ChkActInd.Checked = true;
                    BtnLOPDocNo.Enabled = true;
                    BtnCt.Enabled = true;
                    BtnCtContactPersonName.Enabled = true;
                    BtnLOPDocNo.Enabled = true;
                    BtnSOCDocNo.Enabled = true;
                    BtnQtReplace.Enabled = true;
                    ChkActInd.Enabled = true;
                    ChkBankGuaranteeInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Properties.ReadOnly = false;
                    ChkPrintSignatureInd.Checked = true;
                    //Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 13 });
                    if (mIsBOQAllowToUploadFile) Sm.GrdColReadOnly(false, true, Grd4, new int[] { 2, 3 });
                    Sm.GrdColReadOnly(false, true, Grd5, new int[] { 0, 3, 5, 6, 9, 12, 15 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 0, 4, 5, 6, 7, 8, 12, 13, 15 });
                    //Sm.GrdColReadOnly(Grd6, new int[] { 1, 2, 3, 9, 10, 11, 14 });
                    Grd2.ReadOnly = false;
                    break;
                case mState.Edit:
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblBOQHdr ");
                    SQL.AppendLine("Where DocNo = @Param ");
                    SQL.AppendLine("And ProcessInd = 'D'; ");


                    if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
                    {
                        if(Sm.GetLue(LueProcessInd) != "F")
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                            {
                                LueProcessInd, LueRingCode, DteBOQStartDt, DteBOQEndDt, LueAgingAP, 
                                LueCurCode, TxtCreditLimit, LuePtCode, MeeRemark
                            }, false);
                            BtnCt.Enabled = true;
                            ChkActInd.Enabled = true;
                            BtnCtContactPersonName.Enabled = true;
                            Sm.GrdColReadOnly(false, true, Grd5, new int[] {0, 3, 5, 6, 9, 11, 15 });
                            Sm.GrdColReadOnly(false, true, Grd6, new int[] {0, 4, 5, 6, 7, 8, 12, 13, 15 });
                        }
                        Grd2.ReadOnly = false;
                        ChkBankGuaranteeInd.Properties.ReadOnly = false;
                    }
                    else ChkBankGuaranteeInd.Properties.ReadOnly = true;
                    BtnQtReplace.Enabled = false;
                    break;
            }
        }

        private void ClearData()
        {
            mStateIndicator = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueProcessInd, TxtLOPDocNo, TxtProjectName, LueCtCode, LueCtContactPersonName, LueRingCode,
                LuePICCode, DteBOQStartDt, DteBOQEndDt, TxtSOCDocNo, LueAgingAP, LueCurCode, LuePtCode, TxtQtReplace, MeeRemark
            });
            ClearGrd();
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>
            {
                TxtCreditLimit, TxtBOQAmt, TxtEstRev, TxtEstRes, TxtMargin, TxtPercentage, TxtRemunerationCost, TxtRemunerationCostPerc,
                TxtDirectCost, TxtDirectCostPerc, TxtIndirectCost, TxtIndirectCostPerc, TxtTotalResource, TxtTotalPerc, TxtCostPerc
            }, 0);
            ChkActInd.Checked = false;
            ChkPrintSignatureInd.Checked = false;
            ChkBankGuaranteeInd.Checked = false;
            if (mIsBOQAllowToUploadFile)
            {
                if(mStateIndicator == "E" && Sm.GetLue(LueProcessInd) == "F")
                {
                    Sm.ClearGrd(Grd4, false);
                }
                else
                {
                    Sm.ClearGrd(Grd4, true);
                }
            }
            //Sm.SetLue(LuePPN, Sm.GetParameter("PPNDefaultSetting"));
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd5, true);
            Sm.SetGrdNumValueZero(ref Grd5, 0, new int[] { 3, 5, 6, 7, 10, 13, 14 });
            Sm.ClearGrd(Grd6, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 7, 9, 10, 11, 13, 14, 16, 17, 18, 20, 21, 22 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBOQ3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteBOQStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                DteBOQEndDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
                Sm.SetLue(LueProcessInd, "D");
                isinsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            mStateIndicator = "E";
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            isinsert = true;

            if (Sm.GetLue(LueProcessInd) == "F")
            {
                Grd4.Rows.Count = Grd4.Rows.Count - 1;
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                    {
                        Grd4.Cells[Row, 2].ReadOnly = iGBool.False;
                        Grd4.Cells[Row, 3].ReadOnly = iGBool.True;
                    }
                    else
                    {
                        Grd4.Cells[Row, 2].ReadOnly = iGBool.True;
                        Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                    }
                }
            }
            else
            {
                for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                {
                    Grd4.Cells[Row, 3].ReadOnly = iGBool.False;
                }
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            isinsert = false;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 11 }, e.ColIndex))
            {
                
            }

            if(e.ColIndex == 2)
            {
                TickGrdCols(e.RowIndex, e.ColIndex);
            }

            if (Sm.IsGrdColSelected(new int[] { 7, 9 }, e.ColIndex))
            {
                decimal mTotal = 0m, mQty = 0m, mMth = 0m;
                if (Sm.GetGrdBool(Grd2, e.RowIndex, 2))
                {
                    mQty = Sm.GetGrdDec(Grd2, e.RowIndex, 7);
                    mMth = Sm.GetGrdDec(Grd2, e.RowIndex, 9);
                    mTotal = mQty * mMth;
                }
                Grd2.Cells[e.RowIndex, 10].Value = mTotal;

            }

            if (Sm.IsGrdColSelected(new int[] { 7, 9, 11 }, e.ColIndex))
            {
                InputAmount(e.RowIndex, e.ColIndex);
                ComputeTotalPrice(e.RowIndex);
                InputAmount2(e.RowIndex, e.ColIndex);
                CalculateParentAmt(e.RowIndex, e.ColIndex);
                ComputeSubTotal();
                //ComputePercentage();

                ComputeAmt();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 4) LueRequestEdit(Grd2, LueResourceType, ref fCell, ref fAccept, e);
                if (e.ColIndex == 6) LueRequestEdit(Grd2, LueResourceName, ref fCell, ref fAccept, e);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //Sm.GrdRemoveRow(Grd2, e, BtnSave);
            //Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            if (e.KeyCode == Keys.Enter && Grd2.CurRow.Index != Grd2.Rows.Count - 1)
                Sm.FocusGrd(Grd2, Grd2.CurRow.Index + 1, Grd2.CurCell.Col.Index);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            //if (Grd2.Rows.Count > 0)
            //{
            //    decimal mQty = Sm.GetGrdDec(Grd2, 0, 7);
            //    string mUoM = Sm.GetGrdStr(Grd2, 0, 8);
            //    decimal mMth = Sm.GetGrdDec(Grd2, 0, 9);

            //    if (e.ColIndex == 2)
            //    {
            //        bool IsRequired = Sm.GetGrdBool(Grd2, 0, 2);
            //        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            //            Grd2.Cells[Row, 2].Value = IsRequired;
            //    }

            //if (e.ColIndex == 7)
            //{
            //    for (int r = 0; r < Grd2.Rows.Count; r++)
            //    {
            //        if (Sm.GetGrdBool(Grd2, r, 2))
            //        {
            //            Grd2.Cells[r, 7].Value = mQty;
            //            if (Sm.GetGrdStr(Grd2, r, 9).Length > 0)
            //                Grd2.Cells[r, 10].Value = mQty * Sm.GetGrdDec(Grd2, r, 9);
            //            ComputeTotalPrice(r);
            //        }
            //    }
            //}

            //    if (e.ColIndex == 8)
            //    {
            //        for (int r = 0; r < Grd2.Rows.Count; r++)
            //        {
            //            if (Sm.GetGrdBool(Grd2, r, 2))
            //            {
            //                Grd2.Cells[r, 8].Value = mUoM;
            //            }
            //        }
            //    }

            //if (e.ColIndex == 9)
            //{
            //    for (int r = 0; r < Grd2.Rows.Count; r++)
            //    {
            //        if (Sm.GetGrdBool(Grd2, r, 2))
            //        {
            //            Grd2.Cells[r, 9].Value = mMth;
            //            if (Sm.GetGrdStr(Grd2, r, 7).Length > 0)
            //                Grd2.Cells[r, 10].Value = mMth * Sm.GetGrdDec(Grd2, r, 7);
            //            ComputeTotalPrice(r);
            //        }
            //    }
            //}


            //    if (Sm.IsGrdColSelected(new int[] { 4, 6 }, e.ColIndex))
            //    {
            //        if (BtnSave.Enabled)
            //        {
            //            if (e.ColIndex == 4)
            //            {
            //                string mResourceType = Sm.GetGrdStr(Grd2, 0, 3);

            //                for (int x = 0; x < Grd2.Rows.Count; x++)
            //                {
            //                    if(Sm.GetGrdBool(Grd2, x, 2))
            //                    {
            //                        Grd2.Cells[x, 3].Value = mResourceType;
            //                        Grd2.Cells[x, 4].Value = LueResourceType.GetColumnValue("Col2");
            //                    }
            //                }
            //            }

            //            if (e.ColIndex == 6)
            //            {
            //                string mResourceName = Sm.GetGrdStr(Grd2, 0, 5);

            //                for (int Row = 1; Row < Grd2.Rows.Count; Row++)
            //                {
            //                    if (Sm.GetGrdBool(Grd2, Row, 2))
            //                    {
            //                        Grd2.Cells[Row, 5].Value = mResourceName;
            //                        Grd2.Cells[Row, 6].Value = LueResourceName.GetColumnValue("Col2");
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
        }

        protected void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd5.ReadOnly)
            {
                if (BtnSave.Enabled)
                {
                    if (e.ColIndex == 9) LueRequestEdit(Grd5, LueTaxCode1, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 12) LueRequestEdit(Grd5, LueTaxCode2, ref fCell, ref fAccept, e);
                }

                if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer")) Sm.FormShowDialog(new FrmBOQ3Dlg3(this));
            }
        }

        private void Grd5_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd5, new int[] { 3, 5, 6, 7, 10, 13, 14 }, e);
                if (Sm.IsGrdColSelected(new int[] { 3, 5, 6, 8, 9, 11, 12 }, e.ColIndex))
                {
                    ComputeRevItem(e.RowIndex);
                    ComputeAmt();
                    //ComputeDirectCost(); ComputeRemunerationCost();
                }
            }
        }

        private void Grd5_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd5.ReadOnly)
            {
                 if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer")) Sm.FormShowDialog(new FrmBOQ3Dlg3(this));
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
            }

        }

        protected void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!Grd6.ReadOnly)
            {
                if (BtnSave.Enabled)
                {
                    if (e.ColIndex == 9) LueRequestEdit(Grd6, LueTaxCode1, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 12) LueRequestEdit(Grd6, LueTaxCode2, ref fCell, ref fAccept, e);
                }

                if (e.ColIndex == 0) Sm.FormShowDialog(new FrmBOQ3Dlg3(this));
            }
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd6, new int[] { 5, 7, 8, 9, 10, 11 }, e);
                if (Sm.IsGrdColSelected(new int[] { 5, 6, 7, 8, 9, 10, 11 }, e.ColIndex))
                {
                    decimal mQty1 = 0m, mQty2 = 0m, mUPrice = 0m, mTax, mTotalWithoutTax = 0m, mTotalTax = 0m, mAmt = 0m;
                    mQty1 = Sm.GetGrdDec(Grd6, e.RowIndex, 5);
                    mQty2 = Sm.GetGrdDec(Grd6, e.RowIndex, 6);
                    mUPrice = Sm.GetGrdDec(Grd6, e.RowIndex, 7);
                    mTax = Sm.GetGrdDec(Grd6, e.RowIndex, 8);

                    mTotalWithoutTax = mQty1 * mQty2 * mUPrice;
                    mTotalTax = mQty1 * mQty2 * mTax;
                    mAmt = mTotalWithoutTax + mTotalTax;

                    Grd6.Cells[e.RowIndex, 9].Value = Math.Round(mTotalWithoutTax, 2);
                    Grd6.Cells[e.RowIndex, 10].Value = Math.Round(mTotalTax, 2);
                    Grd6.Cells[e.RowIndex, 11].Value = Math.Round(mAmt, 2);
                    ComputeTotalResource();
                    ComputeDirectCost(); ComputeRemunerationCost(); ComputeIndirectCost();
                    ComputeResourcePerc();
                    ComputeAmt();
                }
            }
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (!Grd6.ReadOnly)
            {
                if (e.ColIndex == 0) Sm.FormShowDialog(new FrmBOQ3Dlg4(this));
                if (e.ColIndex == 13 && Sm.GetGrdStr(Grd6, e.RowIndex, 14).Length > 0)
                {
                    var f = new FrmProjectImplementationRBP(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd6, e.RowIndex, 14);
                    f.ShowDialog();
                }
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd6.ReadOnly)
            {
                Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd6, e, BtnSave);
                ComputeTotalResource(); ComputeDirectCost(); ComputeRemunerationCost(); ComputeIndirectCost();
                ComputeResourcePerc();
                ComputeAmt();
            }

        }


        //UPLOAD FILE - START

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd4.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                if (Sm.GetGrdStr(Grd4, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd4, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd4, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd4, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd4_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedIndex = 0;

            if (Grd4.SelectedRows.Count > 0)
            {
                for (int Index = Grd4.SelectedRows.Count - 1; Index >= 0; Index--)
                    SelectedIndex = Grd4.SelectedRows[Index].Index;
            }

            if (mStateIndicator == "E" && Sm.GetLue(LueProcessInd) == "F")
            {
                if (Sm.GetGrdStr(Grd4, SelectedIndex, 4).Length == 0)
                {
                    Sm.GrdRemoveRow(Grd4, e, BtnSave);
                    Sm.GrdEnter(Grd4, e);
                    Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
                }
            }
            else if (mStateIndicator == "I")
            {
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
                Sm.GrdEnter(Grd4, e);
                Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
            }
        }


        //UPLOAD FILE - END

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid() 
               ) return;

            if (decimal.Parse(TxtDirectCost.Text) == 0 || decimal.Parse(TxtIndirectCost.Text) == 0 )
            {
                if (
                Sm.StdMsgYN("Question",
                       "Direct Cost: " + TxtDirectCost.Text + Environment.NewLine +
                       "Indirect Cost: " + TxtIndirectCost.Text + Environment.NewLine +
                       "Do you want to save this data ?"
                      ) == DialogResult.No
                    ) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "BOQ", "TblBOQHdr");

            var cml = new List<MySqlCommand>();

            //ComputeTotalAmt();

            cml.Add(SaveBOQHdr(DocNo));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveBOQDtl(DocNo, Row));
            }

            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0) cml.Add(SaveBOQDtl2(DocNo, Row));
            }

            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0) cml.Add(SaveBOQDtl3(DocNo, Row));
            }

            if (TxtQtReplace.Text.Length != 0)
                cml.Add(CancelBOQReplacement(TxtQtReplace.Text));

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    if (mIsBOQAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd4, Row, 1))) return;
                    }
                }
            }

            if(mIsBOQAllowToUploadFile) cml.Add(SaveUploadFile(DocNo));

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                {
                    if (mIsBOQAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1")
                    {
                        //if (Sm.GetGrdStr(Grd4, Row, 1) != Sm.GetGrdStr(Grd4, Row, 4))
                        //{
                        UploadFile(DocNo, Row, Sm.GetGrdStr(Grd4, Row, 1));
                        //}
                    }
                }
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                //IsLueCtContactPersonNonMandatory() ||
                (!mIsCustomerContactPersonNonMandatory && Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person")) ||
                Sm.IsLueEmpty(LuePICCode, "Sales Person") ||
                Sm.IsDteEmpty(DteBOQStartDt, "Quotation Start Date") ||
                Sm.IsDteEmpty(DteBOQEndDt, "Quotation End Date") ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true) ||
                IsLOPAlreadyProceed() ||
                IsLOPCancelled() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsItCtCodeNotValid() ||
                (mIsBOQUploadFileMandatory && IsUploadFileMandatory());
        }

        private bool IsLOPAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblBOQHdr ");
            SQL.AppendLine("Where LOPDocNo = @Param ");
            SQL.AppendLine("And ActInd = 'Y' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP data has been proceed in BOQ#" + Sm.GetValue(SQL.ToString(), TxtLOPDocNo.Text));
                return true;
            }

            return false;
        }
        
        private bool IsLueCtContactPersonNonMandatory()
        {
            if (!mIsCustomerContactPersonNonMandatory)
            {
                Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person");
                return true;
            }
            else
                return false;
        }

        private bool IsLOPCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblLOPHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And (CancelInd = 'Y' Or Status = 'C' Or ProcessInd = 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtLOPDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP document is cancelled.");
                TxtLOPDocNo.Focus();
                return true;
            }

            return false;
        }

        private bool IsUploadFileMandatory()
        {

            if (Grd4.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                TcBOQ.SelectTab("Tp5");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if(Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                {
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 3).Length != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The resource type should be empty due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 4);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 5).Length != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The resource name should be empty due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 6);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdDec(Grd2, Row, 7) != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The quantity should be zero due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 7);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 8).Length != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The uom should be empty due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 8);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdDec(Grd2, Row, 9) != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The duration should be zero due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 9);
                        return true;
                    }
                    if (!Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdDec(Grd2, Row, 11) != 0 && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                    {
                        Sm.StdMsg(mMsgType.Warning, "The unit price should be zero due to its BOQ item is not required.");
                        Sm.FocusGrd(Grd2, Row, 11);
                        return true;
                    }
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 12) == "Y" && Sm.IsGrdValueEmpty(Grd2, Row, 4, false, "Resource Type should not be empty.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 12) == "Y" && Sm.IsGrdValueEmpty(Grd2, Row, 6, false, "Resource Name should not be empty.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 7, true, "Quantity should not be zero.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 8, false, "UoM should not be empty.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 9, true, "Duration should not be zero.")) return true;
                    if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.IsGrdValueEmpty(Grd2, Row, 10, true, "Total Unit should not be zero.")) return true;
                }
            }

            return false;
        }

        private bool IsItCtCodeNotValid()
        {
            for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd5, 0, 16) != Sm.GetGrdStr(Grd5, Row, 16))
                {
                    Sm.StdMsg(mMsgType.Warning, "Item's Category is deferent.");
                    Sm.FocusGrd(Grd5, Row, 1);
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "There is no BOQ item listed.");
                return true;
            }
            
            return false;
        }

        private MySqlCommand SaveBOQHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOQHdr(DocNo, DocDt, DocType, ActInd, Status, ProcessInd, LOPDocNo, CtCode, CtContactPersonName, RingCode, SPCode, QtStartDt, QtEndDt, ");
            SQL.AppendLine("PtCode, AgingAp, CurCode, CreditLimit, PrintSignatureInd, CtQtDocNoReplace, EstResAmt, GrandTotal, DirectCost, IndirectCost, EstRevAmt, Remark, CreateBy, CreateDt, BankGuaranteeInd, ");
            SQL.AppendLine("RemunerationAmt, RemunerationPerc, DirectCostPerc, IndirectCostPerc, TotalCost, CostPerc, TotalCostPerc, ProfitPerc, BOQAmt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, @ActInd, 'O', 'D', @LOPDocNo, @CtCode, @CtContactPersonName, @RingCode, @SPCode, @QtStartDt, @QtEndDt, ");
            SQL.AppendLine("@PtCode, @AgingAp, @CurCode, @CreditLimit, @PrintSignatureInd, @CtQtDocNoReplace, @EstResAmt, @GrandTotal, @DirectCost, @IndirectCost, @EstRevAmt, @Remark, @UserCode, CurrentDateTime(), @BankGuaranteeInd, ");
            SQL.AppendLine("@RemunerationAmt, @RemunerationPerc, @DirectCostPerc, @IndirectCostPerc, @TotalCost, @CostPerc, @TotalCostPerc, @ProfitPerc, @BOQAmt); ");

            //SQL.AppendLine("Update TblBOQHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where CtCode=@CtCode And DocNo<>@DocNo; ");

            //SQL.AppendLine("Insert Into TblBOQDtl(DocNo, ItBOQCode, Amt, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select @DocNo, A.ItBOQCode, 0, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblItemBOQ A ");
            //SQL.AppendLine("Where A.ActInd = 'Y' ");
            //SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mBOQDocType);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LuePICCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteBOQStartDt));
            Sm.CmParamDt(ref cm, "@QtEndDt", Sm.GetDte(DteBOQEndDt));
            //Sm.CmParam<String>(ref cm, "@QtMth", TxtQtMth.Text);
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAp", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtQtDocNoReplace", TxtQtReplace.Text);
            Sm.CmParam<Decimal>(ref cm, "@EstResAmt", Decimal.Parse(TxtEstRes.Text));
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", Decimal.Parse(TxtMargin.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCost", Decimal.Parse(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCost", Decimal.Parse(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@EstRevAmt", Decimal.Parse(TxtEstRev.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@RemunerationAmt", Decimal.Parse(TxtRemunerationCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@RemunerationPerc", Decimal.Parse(TxtRemunerationCostPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCostPerc", Decimal.Parse(TxtDirectCostPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCostPerc", Decimal.Parse(TxtIndirectCostPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalCost", Decimal.Parse(TxtTotalResource.Text));
            Sm.CmParam<Decimal>(ref cm, "@CostPerc", Decimal.Parse(TxtCostPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalCostPerc", Decimal.Parse(TxtTotalPerc.Text));
            Sm.CmParam<Decimal>(ref cm, "@ProfitPerc", Decimal.Parse(TxtPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@BOQAmt", Decimal.Parse(TxtBOQAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankGuaranteeInd", ChkBankGuaranteeInd.Checked ? "Y" : "N");
            return cm;
        }

        private MySqlCommand SaveBOQDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblBOQDtl(DocNo, ItBOQCode, RequiredInd, Amt, ");
            SQL.AppendLine("ResourceTypeCode, ResourceNameCode, Qty, UoM, Mth, Total, TotalAmt, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @ItBOQCode, @RequiredInd, @Amt, ");
            SQL.AppendLine("@ResourceTypeCode, @ResourceNameCode, @Qty, @UoM, @Mth, @Total, @TotalAmt, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()) ");
            
            SQL.AppendLine("On Duplicate Key Update ");

            SQL.AppendLine("    RequiredInd = @RequiredInd, Amt = @Amt, ");
            SQL.AppendLine("    ResourceTypeCode = @ResourceTypeCode, ResourceNameCode = @ResourceNameCode, ");
            SQL.AppendLine("    Qty = @Qty, UoM = @UoM, ");
            SQL.AppendLine("    Mth = @Mth, Total = @Total, ");
            SQL.AppendLine("    TotalAmt = @TotalAmt, Remark = @Remark, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            //SQL.AppendLine("Update TblBOQDtl ");
            //SQL.AppendLine("    Set RequiredInd = @RequiredInd, Amt = @Amt, ");
            //SQL.AppendLine("    ResourceTypeCode = @ResourceTypeCode, ResourceNameCode = @ResourceNameCode, ");
            //SQL.AppendLine("    Qty = @Qty, UoM = @UoM, ");
            //SQL.AppendLine("    Mth = @Mth, Total = @Total, ");
            //SQL.AppendLine("    TotalAmt = @TotalAmt, Remark = @Remark ");
            //SQL.AppendLine("Where DocNo = @DocNo And ItBOQCode = @ItBOQCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ItBOQCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@RequiredInd", Sm.GetGrdBool(Grd2, Row, 2) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ResourceTypeCode", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@ResourceNameCode", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<String>(ref cm, "@UoM", Sm.GetGrdStr(Grd2, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Mth", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Total", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd2, Row, 18));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBOQDtl2(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblBOQDtl5(DocNo, DNo, RevenueItCode, Qty, Price, DiscAmt, RevenueBefTax, TaxCode1, TaxAmt1, TaxCode2, TaxAmt2, TotalRevenueAmt, Remark, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @RevenueItCode, @Qty, @Price, @DiscAmt, @RevenueBefTax, @TaxCode1, @TaxAmt1, @TaxCode2, @TaxAmt2, @TotalRevenueAmt, @Remark,");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@RevenueItCode", Sm.GetGrdStr(Grd5, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd5, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Price", Sm.GetGrdDec(Grd5, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@DiscAmt", Sm.GetGrdDec(Grd5, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@RevenueBefTax", Sm.GetGrdDec(Grd5, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@TaxCode1", Sm.GetGrdDec(Grd5, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", Sm.GetGrdDec(Grd5, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@TaxCode2", Sm.GetGrdDec(Grd5, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Sm.GetGrdDec(Grd5, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@TotalRevenueAmt", Sm.GetGrdDec(Grd5, Row, 14));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd5, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveBOQDtl3(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblBOQDtl6(DocNo, DNo, ResourceItCode, Sequence, Remark, Qty1, Qty2, UPrice, Tax, TotalWithoutTax, TotalTax, Amt, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@DocNo, @DNo, @ResourceItCode, @Sequence, @Remark, @Qty1, @Qty2, @UPrice, @Tax, @TotalWithoutTax, @TotalTax, @Amt, ");
            SQLDtl.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ResourceItCode", Sm.GetGrdStr(Grd6, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty1", Sm.GetGrdDec(Grd6, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd6, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd6, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Tax", Sm.GetGrdDec(Grd6, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd6, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", Sm.GetGrdDec(Grd6, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 11));
            Sm.CmParam<String>(ref cm, "@Sequence", Sm.GetGrdStr(Grd6, Row, 15));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand CancelBOQReplacement(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOQHdr Set ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsActIndDataNotValid()) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblBOQHdr ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And ProcessInd = 'F'; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                cml.Add(UpdateBOQFinal(TxtDocNo.Text));
            }
            else cml.Add(UpdateBOQHdr(TxtDocNo.Text));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveBOQDtl(TxtDocNo.Text, Row));
            }
            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd5, Row, 1).Length > 0) cml.Add(SaveBOQDtl2(TxtDocNo.Text, Row));
            }

            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd6, Row, 1).Length > 0) cml.Add(SaveBOQDtl3(TxtDocNo.Text, Row));
            }

            cml.Add(AddApprovalData());

            //EDIT UPLOAD

            if (mIsBOQAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    {
                        if (mIsBOQAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd4, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFile(TxtDocNo.Text));

                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    {
                        if (mIsBOQAllowToUploadFile && Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd4, Row, 4).Length == 0)
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd4, Row, 1));
                        }
                    }
                }
            }
            //EDIT UPLOAD


            ShowData(TxtDocNo.Text);
            if (mIsBOQAllowToUploadFile) ShowUploadFile(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsLueEmpty(LueProcessInd, "Process") ||
                Sm.IsTxtEmpty(TxtLOPDocNo, "LOP#", false) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                //IsLueCtContactPersonNonMandatory() ||
                (!mIsCustomerContactPersonNonMandatory && Sm.IsLueEmpty(LueCtContactPersonName, "Contact Person")) ||
                Sm.IsLueEmpty(LuePICCode, "Sales Person") ||
                Sm.IsDteEmpty(DteBOQStartDt, "Quotation Start Date") ||
                Sm.IsDteEmpty(DteBOQEndDt, "Quotation End Date") ||
                //Sm.IsTxtEmpty(TxtQtMth, "Quotation Month", true) ||
                Sm.IsLueEmpty(LuePtCode, "Payment Term") ||
                Sm.IsLueEmpty(LueCurCode, "Currency Code") ||
                Sm.IsTxtEmpty(TxtCreditLimit, "Credit Limit", true) ||
                IsLOPCancelled() ||
                IsActIndEditedAlready() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsActIndEditedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblBOQHdr Where DocNo=@DocNo And (ActInd='N' Or Status = 'C'); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already inactive.");
                return true;
            }
            return false;
        }
        private MySqlCommand UpdateBOQFinal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Update TblBOQHdr Set ");
            SQL.AppendLine("    ActInd = @ActInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");

            SQL.AppendLine("Delete From TblBOQDtl Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");
            SQL.AppendLine("Delete From TblBOQDtl5 Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");
            SQL.AppendLine("Delete From TblBOQDtl6 Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", (ChkActInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateBOQHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();


            SQL.AppendLine("Delete From TblBOQDtl Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");
            SQL.AppendLine("Delete From TblBOQDtl5 Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");
            SQL.AppendLine("Delete From TblBOQDtl6 Where DocNo = @DocNo And Exists (Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");

            SQL.AppendLine("Update TblBOQHdr Set ");
            SQL.AppendLine("    ProcessInd = @ProcessInd, ");
            SQL.AppendLine("    LOPDocNo = @LOPDocNo, CtContactPersonName = @CtContactPersonName, ");
            SQL.AppendLine("    RingCode = @RingCode, SPCode = @SPCode, QtStartDt = @QtStartDt, ");
            SQL.AppendLine("    QtEndDt = @QtEndDt, PtCode = @PtCode, AgingAp = @AgingAp,  ");
            SQL.AppendLine("    CurCode = @CurCode, CreditLimit = @CreditLimit,  ");
            SQL.AppendLine("    PrintSignatureInd = @PrintSignatureInd, Remark = @Remark, ");
            SQL.AppendLine("    ActInd = @ActInd, LastUpBy = @UserCode, LastUpDt = CurrentDateTime(), BankGuaranteeInd=@BankGuaranteeInd ");
            SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");

            //if (ChkActInd.Checked) // remarked by WED. di remark dulu untuk sementara, sampai ada clear bispro nya kek gimana
            //{

                SQL.AppendLine("Update TblBOQHdr Set  ");
                SQL.AppendLine("    SubTotal = @SubTotal, GrandTotal = @GrandTotal, ");
                SQL.AppendLine("    DirectCost = @DirectCost, IndirectCost = @IndirectCost, SubTotalRBPS = @SubTotalRBPS ");
                SQL.AppendLine("Where DocNo = @DocNo And ActInd = 'Y'; ");


                //SQL.AppendLine("Insert Into TblBOQDtl(DocNo, ItBOQCode, Amt, CreateBy, CreateDt) ");
                //SQL.AppendLine("Select @DocNo, A.ItBOQCode, 0, @UserCode, CurrentDateTime() ");
                //SQL.AppendLine("From TblItemBOQ A ");
                ////SQL.AppendLine("Where A.ActInd = 'Y' ");
                //SQL.AppendLine("; ");
            //}

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetLue(LueProcessInd));
            Sm.CmParam<Decimal>(ref cm, "@SubTotal", Decimal.Parse(TxtEstRes.Text));
            Sm.CmParam<Decimal>(ref cm, "@GrandTotal", Decimal.Parse(TxtMargin.Text));
            Sm.CmParam<Decimal>(ref cm, "@DirectCost", Decimal.Parse(TxtDirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@IndirectCost", Decimal.Parse(TxtIndirectCost.Text));
            Sm.CmParam<Decimal>(ref cm, "@SubTotalRBPS", Decimal.Parse(TxtEstRev.Text));
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", Sm.GetLue(LueCtContactPersonName));
            Sm.CmParam<String>(ref cm, "@RingCode", Sm.GetLue(LueRingCode));
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LuePICCode));
            Sm.CmParamDt(ref cm, "@QtStartDt", Sm.GetDte(DteBOQStartDt));
            Sm.CmParamDt(ref cm, "@QtEndDt", Sm.GetDte(DteBOQEndDt));
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@AgingAp", Sm.GetLue(LueAgingAP));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@CreditLimit", Decimal.Parse(TxtCreditLimit.Text));
            Sm.CmParam<String>(ref cm, "@PrintSignatureInd", ChkPrintSignatureInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankGuaranteeInd", ChkBankGuaranteeInd.Checked ? "Y" : "N");

            return cm;
        }

        private MySqlCommand AddApprovalData()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='BOQ' ");
                SQL.AppendLine("And T.SiteCode In ( Select SiteCode From TblLOPHdr Where DocNo = @LOPDocNo ) ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblBOQHdr ");
                SQL.AppendLine("    Where DocNo = @DocNo ");
                SQL.AppendLine("    And ActInd = 'Y' ");
                SQL.AppendLine("    And ProcessInd = 'F' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.GrandTotal*1 ");
                SQL.AppendLine("    From TblBOQHdr A ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblBOQHdr Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And ProcessInd = 'F' ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType = 'BOQ' ");
            SQL.AppendLine("    And DocNo = @DocNo ");
            SQL.AppendLine("); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@LOPDocNo", TxtLOPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand UpdateBOQDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblBOQDtl ");
        //    SQL.AppendLine("    Set RequiredInd = @RequiredInd, Amt = @Amt, ResourceTypeCode = @ResourceTypeCode, ResourceNameCode = @ResourceNameCode, ");
        //    SQL.AppendLine("    Qty = @Qty, UoM = @UoM, Mth = @Mth, Total = @Total, ");
        //    SQL.AppendLine("    TotalAmt = @TotalAmt, Remark = @Remark ");
        //    SQL.AppendLine("Where DocNo = @DocNo And ItBOQCode = @ItBOQCode ");
        //    SQL.AppendLine("And Exists ");
        //    SQL.AppendLine("(Select DocNo From TblBOQHdr Where DocNo = @DocNo And ActInd = 'Y'); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@ItBOQCode", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@RequiredInd", Sm.GetGrdBool(Grd2, Row, 2) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@ResourceTypeCode", Sm.GetGrdStr(Grd2, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@ResourceNameCode", Sm.GetGrdStr(Grd2, Row, 5));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@UoM", Sm.GetGrdStr(Grd2, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Mth", Sm.GetGrdDec(Grd2, Row, 9));
        //    Sm.CmParam<Decimal>(ref cm, "@Total", Sm.GetGrdDec(Grd2, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 11));
        //    Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Sm.GetGrdDec(Grd2, Row, 18));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 15));

        //    return cm;
        //}

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowBOQHdr(DocNo);
                LoadAllItemBOQ();
                ShowBOQDtl(DocNo);
                ShowBOQDtl2(DocNo);
                if (mIsBOQAllowToUploadFile) ShowUploadFile(DocNo);
                Sm.ShowDocApproval(DocNo, "BOQ", ref Grd3);
                ComputeSubTotal();
                ComputePercentage();
                ComputeTotalResource();
                ComputeDirectCost(); ComputeRemunerationCost(); ComputeIndirectCost();
                ComputeResourcePerc();
                ComputeAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void ShowBOQHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, A.CtCode, A.CtContactPersonName, A.RingCode, A.SPCode, A.QtStartDt, ");
            SQL.AppendLine("A.QtEndDt, A.PtCode, A.AgingAP, A.CurCode, A.CreditLimit, A.PrintSignatureInd, A.CtQtDocNoReplace, A.Remark, ");
            SQL.AppendLine("A.LOPDocNo, A.Status, A.SubTotal, A.Tax, A.GrandTotal, A.TaxInd, A.DirectCost, A.IndirectCost, A.Policy, ");
            SQL.AppendLine("A.SubTotalRBPS, A.ProcessInd, A.BankGuaranteeInd, A.TaxCode, D.ProjectName ");
            if (mIsPSModuleShowApproverRemarkInfo)
                SQL.AppendLine(", C.Remark as ApprovalRemark ");
            else
                SQL.AppendLine(", Null as ApprovalRemark ");
            SQL.AppendLine("From TblBOQHdr A ");
            if (mIsPSModuleShowApproverRemarkInfo)
            {
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("	Select Max(T2.`Level`) LV, T1.DocNo   ");
                SQL.AppendLine("	From TblDocApproval T1   ");
                SQL.AppendLine("	Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo   ");
                SQL.AppendLine("	Where T1.DocType = 'BOQ' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '') ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine("	Group By T1.DocNo ");
                SQL.AppendLine(") B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("Left Join (   ");
                SQL.AppendLine("   Select T2.`Level` As LV, T1.DocNo, T1.Remark, T3.EmpName, T1.UserCode ");
                SQL.AppendLine("   From TblDocApproval T1   ");
                SQL.AppendLine("   Inner Join TblDocApprovalSetting T2 On T1.DocType = T2.DocType And T1.ApprovalDNo = T2.DNo ");
                SQL.AppendLine("	Inner Join TblEmployee T3 On T1.UserCode = T3.UserCode  ");
                SQL.AppendLine("   Where T1.DocType = 'BOQ' And T1.DocNo = @DocNo ");
                SQL.AppendLine("	And (T1.UserCode Is Not Null And T1.UserCode <> '')   ");
                SQL.AppendLine("	And T1.Status Is Not Null ");
                SQL.AppendLine(") C On B.DocNo = C.DocNo And B.LV = C.LV ");
            }
            SQL.AppendLine("Left Join TblLOPHdr D on A.LOPDocNo = D.DocNo");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "ActInd", "CtCode", "CtContactPersonName", "RingCode", 
                    
                    //6-10
                    "SPCode", "QtStartDt", "QtEndDt", "PtCode", "AgingAP", 

                    //11-15
                    "CurCode", "CreditLimit", "PrintSignatureInd", "CtQtDocNoReplace", "Remark",

                    //16-20
                    "LOPDocNo", "Status", "SubTotal", "Tax", "GrandTotal", 
                    
                    //21-25
                    "TaxInd", "DirectCost", "IndirectCost", "Policy", "SubTotalRBPS",
                    
                    //26-30
                    "ProcessInd", "BankGuaranteeInd", "ApprovalRemark", "TaxCode", "ProjectName"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.DrStr(dr, c[3]), Sm.DrStr(dr, c[4]));
                    //Sm.SetLue(LueCtContactPersonName, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueRingCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LuePICCode, Sm.DrStr(dr, c[6]));
                    Sm.SetDte(DteBOQStartDt, Sm.DrStr(dr, c[7]));
                    Sm.SetDte(DteBOQEndDt, Sm.DrStr(dr, c[8]));
                    
                    Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[9]));
                    Sm.SetLue(LueAgingAP, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[11]));
                    TxtCreditLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    ChkPrintSignatureInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[13]), "Y");
                    TxtQtReplace.EditValue = Sm.DrStr(dr, c[14]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    TxtLOPDocNo.EditValue = Sm.DrStr(dr, c[16]);
                    string mStatusDesc = string.Empty;
                    if (Sm.DrStr(dr, c[17]) == "A") mStatusDesc = "Approved";
                    if (Sm.DrStr(dr, c[17]) == "C") mStatusDesc = "Cancelled";
                    if (Sm.DrStr(dr, c[17]) == "O") mStatusDesc = "Outstanding";
                    TxtStatus.EditValue = mStatusDesc;
                    TxtEstRes.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                    //TxtTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                    TxtMargin.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                    //ChkTaxInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[21]), "Y");
                    TxtDirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                    TxtIndirectCost.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                    //TxtPolicy.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                    TxtEstRev.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[26]));
                    ChkBankGuaranteeInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[27]), "Y");
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[30]);
            //MeeApprovalRemark.EditValue = Sm.DrStr(dr, c[28]);
            //if (Sm.DrStr(dr, c[29]) == "")
            //    Sm.SetLue(LuePPN, mPPNDefaultSetting);
            //else
            //    Sm.SetLue(LuePPN, Sm.DrStr(dr, c[29]));

        }, true
            );
            ComputePercentage();
        }

        internal void ShowBOQDtl(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("SELECT A.RevenueItCode, B.ItName, B.ItCtCode, A.Qty, B.InventoryUOMCode, A.Price, A.DiscAmt, ");
            SQLDtl2.AppendLine("	   A.RevenueBefTax, A.TaxCode1, A.TaxAmt1, A.TaxCode2, A.TaxAmt2, ");
            SQLDtl2.AppendLine("	   A.TotalRevenueAmt, A.Remark ");
            SQLDtl2.AppendLine("FROM tblboqdtl5 A ");
            SQLDtl2.AppendLine("INNER JOIN tblitem B ON A.RevenueItCode = B.ItCode ");
            SQLDtl2.AppendLine("WHERE A.DocNo = @DocNo ");
            SQLDtl2.AppendLine("Order By A.DNo; ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm2, SQLDtl2.ToString(),
                new string[]
                {
                    "RevenueItCode",
                    "ItName", "Qty", "InventoryUOMCode", "Price", "DiscAmt", 
                    "RevenueBefTax", "TaxCode1", "TaxAmt1", "TaxCode2", "TaxAmt2", 
                    "TotalRevenueAmt", "Remark", "ItCtCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 1);
            //Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });

        }

        internal void ShowBOQDtl2(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.ResourceItCode, A.Sequence, B.ItName, C.ItGrpCode, C.ItGrpName, A.Remark, A.Qty1, A.Qty2, A.UPrice, A.Tax, A.TotalWithoutTax, A.TotalTax, A.Amt ");
            SQLDtl2.AppendLine(", D.DocNo As RBPDocNo ");
            SQLDtl2.AppendLine("From TblBOQDtl6 A ");
            SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQLDtl2.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");
            SQLDtl2.AppendLine("Left Join TblProjectImplementationRBPHdr D On A.DocNo = D.PRJIDocNo And D.ResourceItCode = A.ResourceItCode And D.CancelInd = 'N' ");
            SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
            SQLDtl2.AppendLine("Order By A.DNo; ");

            var cm2 = new MySqlCommand();
            Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd6, ref cm2, SQLDtl2.ToString(),
                new string[]
                {
                    "ResourceItCode",
                    "ItName", "ItGrpName", "Remark", "Qty1", "Qty2",
                    "UPrice", "Tax", "TotalWithoutTax", "TotalTax", "Amt",
                    "ItGrpCode", "RBPDocNo", "Sequence"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
            //Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11 });

        }

        internal void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd4, ref cm,
                   "select DNo, FileName, CreateBy, CreateDt from  TblBOQDtl4 Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd4, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd4, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd4, 0, 0);
        }

        #endregion

        #region Additional Method

        //UPLOAD FILE - START
        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Bill Of Quantity - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblBOQDtl4(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsBOQAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsBOQAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsBOQAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsBOQAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsBOQAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsBOQAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd4, Row, 1) != Sm.GetGrdStr(Grd4, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblBOQDtl4 ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblBOQDtl4 Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand DeleteEmployeeFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblBOQDtl4 Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtDocNo.Text);

            return cm;
        }


        //UPLOAD FILE - END

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mBOQTaxPercentage = Sm.GetParameterDec("BOQTaxPercentage");
            mBOQPrintOutBODName = Sm.GetParameter("BOQPrintOutBODName");
            mRptName = Sm.GetParameter("FormPrintBOQ");
            mIsBOQPrintOutShowCreatedUser = Sm.GetParameterBoo("IsBOQPrintOutShowCreatedUser");
            mIsBOQExcludeCancelledData = Sm.GetParameterBoo("IsBOQExcludeCancelledData");
            mBOQStatusDisplayedInFind = Sm.GetParameter("BOQStatusDisplayedInFind");
            mIsPSModuleShowApproverRemarkInfo = Sm.GetParameterBoo("IsPSModuleShowApproverRemarkInfo");
            mPPNDefaultSetting = Sm.GetParameter("PPNDefaultSetting");
            mIsCustomerContactPersonNonMandatory = Sm.GetParameterBoo("IsCustomerContactPersonNonMandatory");
            mItemBOQCalculationFormat = Sm.GetParameter("ItemBOQCalculationFormat");
            mIsBOQAllowToUploadFile = Sm.GetParameterBoo("IsBOQAllowToUploadFile");
            if (mItemBOQCalculationFormat.Length == 0) mItemBOQCalculationFormat = "1";
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mListItCtCodeForResource = Sm.GetParameter("ListItCtCodeForResource");
            mItGrpCodeForDirectCost = Sm.GetParameter("ItGrpCodeForDirectCost");
            mItGrpCodeForRemuneration = Sm.GetParameter("ItGrpCodeForRemuneration");
            mIsBOQUploadFileMandatory = Sm.GetParameterBoo("IsBOQUploadFileMandatory");
            mItGrpCodeForIndirectCost = Sm.GetParameter("ItGrpCodeForIndirectCost");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var TaxRate = Sm.GetValue("Select TaxRate from TblTax Where TaxCode=@Param;", TaxCode);
            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        internal string GetSelectedItCode()
        {
            var SQL = string.Empty;
            if (Grd6.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd6, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }
        private void ComputeRevItem(int r)
        {
            decimal mQty = 0m, mUPrice = 0m, mDiscAmt = 0m, mRefBefTax = 0m, mTax1, mTax2,
                        mTotalWithoutTax = 0m, mTotalTax = 0m, mAmt = 0m;

            mQty = Sm.GetGrdDec(Grd5, r, 3);
            mUPrice = Sm.GetGrdDec(Grd5, r, 5);
            mDiscAmt = Sm.GetGrdDec(Grd5, r, 6);
            mRefBefTax = (mQty * mUPrice) - mDiscAmt;
            mTax1 = GetTaxRate(Sm.GetGrdStr(Grd5, r, 8)) * 0.01m * mRefBefTax;
            mTax2 = GetTaxRate(Sm.GetGrdStr(Grd5, r, 11)) * 0.01m * mRefBefTax;

            Grd5.Cells[r, 7].Value = mRefBefTax;
            Grd5.Cells[r, 10].Value = mTax1;
            Grd5.Cells[r, 13].Value = mTax2;
            Grd5.Cells[r, 14].Value = mRefBefTax + (mTax1 + mTax2);
            ComputeAmt();
        }

        internal void ComputeAmt()
        {
            decimal mEstRevAmt = 0m, mEstResAmt = 0m, mProfitMargin = 0m;

            if (Grd5.Rows.Count > 1)
            {
                for (int i = 0; i < Grd5.Rows.Count; i++)
                {
                    mEstRevAmt += Sm.GetGrdDec(Grd5, i, 14);
                }
            }

            if (Grd6.Rows.Count > 1)
            {
                for (int i = 0; i < Grd6.Rows.Count; i++)
                {
                    mEstResAmt += Sm.GetGrdDec(Grd6, i, 11);
                }
            }

            mProfitMargin = mEstRevAmt - mEstResAmt;

            TxtEstRev.EditValue = Sm.FormatNum(mEstRevAmt, 0);
            TxtEstRes.EditValue = Sm.FormatNum(mEstResAmt, 0);
            TxtMargin.EditValue = Sm.FormatNum(mProfitMargin, 0);
            TxtTotalResource.EditValue = Sm.FormatNum(mEstResAmt, 0);

            if (mProfitMargin != 0 && mEstRevAmt != 0)
            {
                TxtPercentage.EditValue = Sm.FormatNum(mProfitMargin / (mEstRevAmt * 0.01m), 2);
                TxtCostPerc.EditValue = Sm.FormatNum(mEstResAmt / (mEstRevAmt * 0.01m), 2);
            }
            
        }

        internal void ComputeTotalResource()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtTotalResource.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeDirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForDirectCost && mItGrpCodeForDirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtDirectCost.EditValue = Sm.FormatNum(Total, 0);
        }

        internal void ComputeIndirectCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForIndirectCost && mItGrpCodeForIndirectCost.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtIndirectCost.EditValue = Sm.FormatNum(Total, 0);
        }

        internal void ComputeRemunerationCost()
        {
            decimal Total = 0m;
            for (int row = 0; row < Grd6.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd6, row, 2).Length != 0 && Sm.GetGrdStr(Grd6, row, 12).ToString() == mItGrpCodeForRemuneration && mItGrpCodeForRemuneration.Length > 0)
                {
                    Total += Sm.GetGrdDec(Grd6, row, 11);
                }
            }
            TxtRemunerationCost.EditValue = Sm.FormatNum(Total, 0);

            ComputeTotal();
        }

        internal void ComputeResourcePerc()
        {
            decimal RemunerationAmt = 0m;
            decimal DirectAmt = 0m;
            decimal IndirectAmt = 0m;
            decimal Divide = 0m;

            RemunerationAmt = Convert.ToDecimal(TxtRemunerationCost.Text);
            DirectAmt = Convert.ToDecimal(TxtDirectCost.Text);
            IndirectAmt = Convert.ToDecimal(TxtIndirectCost.Text);
            Divide = (RemunerationAmt + DirectAmt + IndirectAmt);

            if(Divide > 0)
            {
                TxtRemunerationCostPerc.EditValue = Sm.FormatNum(RemunerationAmt / (Divide * 0.01m), 2);
                TxtDirectCostPerc.EditValue = Sm.FormatNum(DirectAmt / (Divide * 0.01m), 2);
                TxtIndirectCostPerc.EditValue = Sm.FormatNum(IndirectAmt / (Divide * 0.01m), 2);
                TxtTotalPerc.EditValue = Sm.FormatNum(Divide / (Divide * 0.01m), 2);
            }

        }

        internal void ComputeTotal()
        {
            decimal PPNPercentage = 0m;
            decimal PPhPPNPercentage = 0m;
            decimal Total = 0m;

            Total = Sm.GetDecValue(TxtRemunerationCost.Text) + Sm.GetDecValue(TxtDirectCost.Text);
            TxtTotalResource.EditValue = Sm.FormatNum(Total, 0);

            //if (Sm.GetDecValue(TxtExclPPN.Text) != 0) PPNPercentage = (Total / Sm.GetDecValue(TxtExclPPN.Text)) * 100m;
            //TxtPPNPercentage.EditValue = Sm.FormatNum(PPNPercentage, 0);

            //if (Sm.GetDecValue(TxtNetCash.Text) != 0) PPhPPNPercentage = (Total / Sm.GetDecValue(TxtNetCash.Text)) * 100m;
            //TxtPPNPPhPercentage.EditValue = Sm.FormatNum(PPhPPNPercentage, 0);
        }

        internal void ComputeSubTotal()
        {
            decimal mSubtotal = 0m;

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd2, i, 0).Length > 0 && 
                        Sm.GetGrdBool(Grd2, i, 2) && // item tersebut required
                        Sm.GetGrdStr(Grd2, i, 19) == "Y") // item tersebut merupakan parent
                    {
                        mSubtotal += Sm.GetGrdDec(Grd2, i, 18);
                    }
                }
            }

            TxtBOQAmt.EditValue = Sm.FormatNum(mSubtotal, 0);
            ComputeTax();
        }

        private void ComputeTax()
        {
            decimal mTax = 0m, mSubtotal = 0m, tax = 0m;
            mSubtotal = Decimal.Parse(TxtEstRes.Text);


            //var taxcode = Sm.GetLue(LuePPN);

            //if (ChkTaxInd.Checked)
            //{
            //    tax = taxcode.Length > 0 ? Decimal.Parse(Sm.GetValue("select taxrate from tbltax where taxcode = @param ", taxcode)) : 0m;
            //}

            //if (ChkTaxInd.Checked) mTax = (tax * 0.01m) * mSubtotal;

            //TxtTax.EditValue = Sm.FormatNum(mTax, 0);
            ComputeGrandTotal();
        }

        private void ComputeGrandTotal()
        {
            decimal mGrandTotal = 0m, mSubtotal = 0m, mTax = 0m;
            mSubtotal = Decimal.Parse(TxtEstRes.Text);
            //mTax = Decimal.Parse(TxtTax.Text);
            //mGrandTotal = mSubtotal + mTax;
            //TxtMargin.EditValue = Sm.FormatNum(mGrandTotal, 0);
        }

        internal void ComputePercentage()
        {
            decimal mGrandTotal = 0m, mCreditLimit = 0m, mPercentage = 0m;
            mGrandTotal = decimal.Parse(TxtMargin.Text);
            mCreditLimit = decimal.Parse(TxtCreditLimit.Text);
            if (mCreditLimit > 0)
            {
                mPercentage = (mGrandTotal / mCreditLimit) * 100 ;
            }
            else
                mPercentage = 0;
            TxtPercentage.EditValue = Sm.FormatNum(mPercentage, 0);
        }
        private void ComputeSubTotalRBPS()
        {
            decimal mSubTotalRBPS = 0m, mDirectCost = 0m, mPolicy = 0m, mIndirectCost = 0m;
            mDirectCost = Decimal.Parse(TxtDirectCost.Text);
            mIndirectCost = Decimal.Parse(TxtIndirectCost.Text);
            //mPolicy = Decimal.Parse(TxtPolicy.Text);
            //mSubTotalRBPS = mDirectCost + mIndirectCost + mPolicy;
            //TxtEstRev.EditValue = Sm.FormatNum(mSubTotalRBPS, 0);
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'BOQ' And SiteCode Is Not Null; ");
        }

        private void ComputeTotalAmt()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd2, Row, 2) && Sm.GetGrdStr(Grd2, Row, 12) == "Y")
                {
                    Grd2.Cells[Row, 16].Value = Sm.GetGrdDec(Grd2, Row, 10) * Sm.GetGrdDec(Grd2, Row, 11);

                    decimal Amt = Sm.GetGrdDec(Grd2, Row, 17);
                    string Ac = Sm.GetGrdStr(Grd2, Row, 1);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if(Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < Row; y++)
                            {
                                if(Sm.GetGrdStr(Grd2, y, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, Row, 16));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 16));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 16].Value = Qty;
                                    Grd2.Cells[y, 17].Value = Qty;
                                    Grd2.Cells[Row, 17].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ComputeTotalPrice(int r)
        {
            decimal Qty = 0m, Mth = 0m, Amt = 0m;

            Grd2.BeginUpdate();

            Qty = Sm.GetGrdDec(Grd2, r, 7);
            Mth = Sm.GetGrdDec(Grd2, r, 9);
            Amt = Sm.GetGrdDec(Grd2, r, 11);

            Grd2.Cells[r, 18].Value = Qty * Mth * Amt;

            //string AcNo1 = string.Empty, AcNo2 = string.Empty;
            //decimal Amt1 = 0m, Amt2 = 0m;
            //bool IsFirst = false;
            //int Temp = 0;
            //for (var i = 0; i < r; i++)
            //{
            //    IsFirst = false;
            //    AcNo1 = Sm.GetGrdStr(Grd2, i, 1);
            //    Amt1 = Sm.GetGrdDec(Grd2, i, 18);
            //    for (var j = Temp; j < Grd2.Rows.Count; j++)
            //    {
            //        AcNo2 = Sm.GetGrdStr(Grd2, j, 1);
            //        Amt2 = Sm.GetGrdDec(Grd2, j, 18);
            //        if (
            //            //AcNo1.Count(x => x == '.') == AcNo2.Count(x => x == '.') && Sm.CompareStr(AcNo1, AcNo2) ||
            //            AcNo1.Count(x => x == '.') != AcNo2.Count(x => x == '.') && AcNo1.StartsWith(string.Concat(AcNo2, '.'))
            //            )
            //        {
            //            if (!IsFirst)
            //            {
            //                IsFirst = true;
            //                Temp = j;
            //            }
            //            Grd2.Cells[j, 18].Value = Amt1 + Amt2;
            //            if (string.Compare(AcNo1, AcNo2) == 0)
            //                break;
            //        }
            //    }
            //}
   
            Grd2.EndUpdate();
        }

        private void InputAmount(int RowIndex, int ColIndex)
        {
            #region Old Code
            //if (ColIndex == 11)
            //{
            //    decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 13);
            //    if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y") // allowed to edit & required
            //    {
            //        string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
            //        string ccc = string.Empty;
            //        for (int i = 0; i < Ac.Length; i++)
            //        {
            //            if (Ac[i] == '.')
            //            {
            //                ccc = Ac.Substring(0, i);
            //                for (int y = 0; y < RowIndex; y++)
            //                {
            //                    if (Sm.GetGrdStr(Grd2, y, 1) == ccc)
            //                    {
            //                        decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 11));
            //                        decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 11));
            //                        decimal Qty = QtyRow + QtyRow2 - Amt;
            //                        Grd2.Cells[y, 11].Value = Qty;
            //                        Grd2.Cells[y, 13].Value = Qty;
            //                        Grd2.Cells[RowIndex, 13].Value = QtyRow;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
            //        //Grd2.Cells[RowIndex, 7].Value = 0;
            //    }
            //}
            #endregion

            if (ColIndex == 7 || ColIndex == 9 || ColIndex == 11)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 13);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 11));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 11));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 11].Value = Qty;
                                    Grd2.Cells[y, 13].Value = Qty;
                                    Grd2.Cells[RowIndex, 13].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                    //Grd2.Cells[RowIndex, 7].Value = 0;
                }
            }
        }

        private void InputAmount2(int RowIndex, int ColIndex)
        {
            if (ColIndex == 7 || ColIndex == 9 || ColIndex == 11)
            {
                decimal Amt = Sm.GetGrdDec(Grd2, RowIndex, 16);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y") // allowed to edit & required
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;
                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int y = 0; y < RowIndex; y++)
                            {
                                if (Sm.GetGrdStr(Grd2, y, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 18));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, y, 18));
                                    decimal Qty = QtyRow + QtyRow2 - Amt;
                                    Grd2.Cells[y, 18].Value = Qty;
                                    Grd2.Cells[y, 16].Value = Qty;
                                    Grd2.Cells[y, 11].Value = Qty;
                                    Grd2.Cells[RowIndex, 16].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "Can't input amount for this BOQ item");
                }
            }
        }

        private void CalculateParentAmt(int RowIndex, int ColIndex)
        {
            if (mItemBOQCalculationFormat == "1") return;

            if (ColIndex == 7)
            {
                decimal QtyTmp = Sm.GetGrdDec(Grd2, RowIndex, 20);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y")
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;

                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int j = 0; j < RowIndex; j++)
                            {
                                if (Sm.GetGrdStr(Grd2, j, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 7));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, j, 7));
                                    decimal Qty = QtyRow + QtyRow2 - QtyTmp;
                                    Grd2.Cells[j, 7].Value = Qty;
                                    Grd2.Cells[j, 20].Value = Qty;
                                    Grd2.Cells[RowIndex, 20].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }

            if (ColIndex == 9)
            {
                decimal QtyTmp = Sm.GetGrdDec(Grd2, RowIndex, 21);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y")
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;

                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int j = 0; j < RowIndex; j++)
                            {
                                if (Sm.GetGrdStr(Grd2, j, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 9));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, j, 9));
                                    decimal Qty = QtyRow + QtyRow2 - QtyTmp;
                                    Grd2.Cells[j, 9].Value = Qty;
                                    Grd2.Cells[j, 21].Value = Qty;
                                    Grd2.Cells[RowIndex, 21].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }

            if (ColIndex == 7 || ColIndex == 9 || ColIndex == 10)
            {
                decimal QtyTmp = Sm.GetGrdDec(Grd2, RowIndex, 22);
                if (Sm.GetGrdStr(Grd2, RowIndex, 12) == "Y")
                {
                    string Ac = Sm.GetGrdStr(Grd2, RowIndex, 1);
                    string ccc = string.Empty;

                    for (int i = 0; i < Ac.Length; i++)
                    {
                        if (Ac[i] == '.')
                        {
                            ccc = Ac.Substring(0, i);
                            for (int j = 0; j < RowIndex; j++)
                            {
                                if (Sm.GetGrdStr(Grd2, j, 1) == ccc)
                                {
                                    decimal QtyRow = (Sm.GetGrdDec(Grd2, RowIndex, 10));
                                    decimal QtyRow2 = (Sm.GetGrdDec(Grd2, j, 10));
                                    decimal Qty = QtyRow + QtyRow2 - QtyTmp;
                                    Grd2.Cells[j, 10].Value = Qty;
                                    Grd2.Cells[j, 22].Value = Qty;
                                    Grd2.Cells[RowIndex, 22].Value = QtyRow;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void TickGrdCols(int RowIndex, int ColIndex)
        {
            try
            {
                if (ColIndex != 2) return;

                int ColAmt = 11;
                int ColAmt2 = 13;
                int Len = Sm.GetGrdStr(Grd2, RowIndex, 1).Length;
                bool Value = Sm.GetGrdBool(Grd2, RowIndex, ColIndex);
                string MenuCode = Sm.GetGrdStr(Grd2, RowIndex, 1);

                for (int Row = RowIndex + 1; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length <= Len)
                    {
                        //Grd2.Cells[Row-1, ColAmt].ReadOnly = iGBool.True;
                        //Grd2.Cells[Row-1, ColAmt].BackColor = Color.FromArgb(224, 224, 224);
                        //Grd2.Cells[Row - 1, ColAmt2].Value = 0;
                        //InputAmount(Row-1, ColAmt);
                        break;
                    }
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > Len)
                    {
                        Grd2.Cells[Row, ColIndex].Value = Value;
                        //Grd2.Cells[Row-1, ColIndex].ReadOnly = iGBool.True;
                        //Grd2.Cells[Row-1, ColIndex].BackColor = Color.FromArgb(224, 224, 224);
                        //InputAmount(Row-1, ColAmt);
                    }

                }

                for (int Row = RowIndex - 1; Row >= 0; Row--)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length < Len &&
                        ("X" + MenuCode).IndexOf("X" + Sm.GetGrdStr(Grd2, Row, 1)) >= 0 &&
                        (Value || (!Value && IsNotHaveThickedChildNode(Row, ColIndex, Sm.GetGrdStr(Grd2, Row, 1).Length))))
                        Grd2.Cells[Row, ColIndex].Value = Value;

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsNotHaveThickedChildNode(int CurRow, int CurCol, int Len)
        {
            for (int Row = CurRow + 1; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > Len)
                {
                    if (Sm.GetGrdBool(Grd2, Row, CurCol))
                        return false;
                }
                else
                    return true;
            }
            return true;
        }

        private void SetLueResourceName(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'INTERNAL' As Col1, 'INTERNAL' As Col2 ");
	        SQL.AppendLine("Union All ");
	        SQL.AppendLine("Select X.Col1, X.Col2 ");
	        SQL.AppendLine("From ");
	        SQL.AppendLine("( ");
		    SQL.AppendLine("    Select VdCode As Col1, VdName As Col2 ");
		    SQL.AppendLine("    From TblVendor "); 
		    SQL.AppendLine("    Where 0 = 0 ");
            if (TxtDocNo.Text.Length == 0)
		        SQL.AppendLine("    And ActInd = 'Y' ");
		    SQL.AppendLine("    Order By VdName ");
	        SQL.AppendLine(")X; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void LoadAllItemBOQ()
        {
            var SQL = new StringBuilder();
            int Level = 0;
            string PrevMenuCode = string.Empty;
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                if (TxtDocNo.Text.Length == 0)
                {
                    SQL.AppendLine("Select A.ItBOQCode, A.ActInd, Concat(A.ItBOQCode, ' ', A.ItBOQName) As ItemBOQ, 0 As Amt, If(B.ItBOQCode Is Null, 'N', 'Y') As Allow, ");
                    SQL.AppendLine("null as ResourceTypeCode, null as ResourceTypeDesc, null as ResourceNameCode, null As ResourceNameDesc, 'Y' As RequiredInd, ");
                    SQL.AppendLine("0 as Qty, null as UoM, 0 as Mth, 0 As Total, null as Remark, 0 as TotalAmt, If(A.Parent is Null, 'Y', 'N') As ParentInd ");
                    SQL.AppendLine("From TblItemBOQ A ");
                    SQL.AppendLine("Left Join ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    Select ItBOQCode ");
                    SQL.AppendLine("    From TblItemBOQ ");
                    SQL.AppendLine("    Where ItBOQCode Not In ");
                    SQL.AppendLine("    ( Select Parent From TblItemBOQ Where Parent Is Not Null ) ");
                    SQL.AppendLine(") B On A.ItBOQCode = B.ItBOQCode ");
                    SQL.AppendLine("Where A.ActInd = 'Y' ");
                    SQL.AppendLine("Order By A.ItBOQCode; ");
                }
                else
                {
                    SQL.AppendLine("Select B.ItBOQCode, B.ActInd, Concat(B.ItBOQCode, ' ', B.ItBOQName) As ItemBOQ, ");
                    SQL.AppendLine("IfNull(A.Amt, 0) As Amt, If(C.ItBOQCode Is Null, 'N', 'Y') As Allow, A.ResourceTypeCode, D.OptDesc As ResourceTypeDesc, ");
                    SQL.AppendLine("A.ResourceNameCode, IfNull(If(A.ResourceNameCode = 'INTERNAL', 'INTERNAL', E.VdName), '') As ResourceNameDesc, ");
                    SQL.AppendLine("IfNull(A.RequiredInd, 'N') As RequiredInd, A.Qty, A.UoM, A.Mth, A.Total, A.Remark, A.TotalAmt, If(B.Parent is Null, 'Y', 'N') As ParentInd ");
                    SQL.AppendLine("From TblItemBOQ B ");
                    SQL.AppendLine("Left Join TblBOQDtl A On A.ItBOQCode=B.ItBOQCode And A.DocNo=@DocNo ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("   Select ItBOQCode From TblItemBOQ ");
                    SQL.AppendLine("   Where ItBOQCode Not In ");
                    SQL.AppendLine("   (Select Parent From TblItemBOQ Where Parent Is Not Null) ");
                    SQL.AppendLine(") C On B.ItBOQCode = C.ItBOQCode ");
                    SQL.AppendLine("Left Join TblOption D On A.ResourceTypeCode = D.OptCode And D.OptCat = 'ResourceType' ");
                    SQL.AppendLine("Left Join TblVendor E On A.ResourceNameCode = E.VdCode ");
                    SQL.AppendLine("Order By B.ItBOQCode; ");

                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                }

                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, 
                    new string[] 
                    { 
                        "ItBOQCode", 
                        "ItemBOQ", "Amt", "Allow", "ResourceTypeCode", "ResourceTypeDesc", 
                        "ResourceNameCode", "ResourceNameDesc", "RequiredInd", "Qty", "UoM",
                        "Mth", "Total", "Remark", "TotalAmt", "ParentInd", 
                        "ActInd"
                    });
                if (dr.HasRows)
                {
                    int Row = 0;
                    Grd2.Rows.Count = 0;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        // kalau item tersebut aktif, atau, non-aktif tapi terpakai
                        if (Sm.DrStr(dr, c[16]) == "Y" || (Sm.DrStr(dr, c[16]) == "N" && Sm.DrStr(dr, c[8]) == "Y"))
                        {
                            Grd2.Rows.Add();
                            Level = Sm.DrStr(dr, 0).Length - ((Sm.DrStr(dr, 0).Length + 1) / 2);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 1);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("B", Grd2, dr, c, Row, 2, 8);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 4);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 5);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 6);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 6, 7);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 9);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 8, 10);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 9, 11);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 10, 12);
                            if (TxtDocNo.Text.Length == 0)
                            {
                                Grd2.Cells[Row, 11].Value = 0m;
                                Grd2.Cells[Row, 13].Value = 0m;
                                Grd2.Cells[Row, 14].Value = 0m;
                            }
                            else
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 11, 2);
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 13, 2);
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 14, 2);
                            }
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 12, 3);
                            if (Sm.GetGrdStr(Grd2, Row, 12) == "N")
                            {
                                Sm.GrdColReadOnly(false, true, Grd2, new int[] { 2 });
                                Grd2.Cells[Row, 3].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 4].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 5].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 6].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 8].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 9].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 10].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 11].ReadOnly = iGBool.True;
                                //Grd2.Cells[Row, 7].ReadOnly = iGBool.True;
                                //Grd2.Cells[Row, 9].ReadOnly = iGBool.True;
                                Grd2.Cells[Row, 3].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 4].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 5].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 6].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 7].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 8].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 9].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 10].BackColor = Color.FromArgb(224, 224, 224);
                                Grd2.Cells[Row, 11].BackColor = Color.FromArgb(224, 224, 224);

                                Grd2.Cells[Row, 3].Value = string.Empty;
                                Grd2.Cells[Row, 4].Value = string.Empty;
                                Grd2.Cells[Row, 5].Value = string.Empty;
                                Grd2.Cells[Row, 6].Value = string.Empty;
                                Grd2.Cells[Row, 8].Value = "Unit";
                                if (mItemBOQCalculationFormat == "2")
                                {
                                    if (TxtDocNo.Text.Length == 0)
                                    {
                                        Grd2.Cells[Row, 7].Value = 0m;
                                        Grd2.Cells[Row, 9].Value = 0m;
                                        Grd2.Cells[Row, 10].Value = 0m;
                                        Grd2.Cells[Row, 20].Value = 0m;
                                        Grd2.Cells[Row, 21].Value = 0m;
                                        Grd2.Cells[Row, 22].Value = 0m;
                                    }
                                }
                                else
                                {
                                    Grd2.Cells[Row, 7].Value = 1m;
                                    Grd2.Cells[Row, 9].Value = 1m;
                                    Grd2.Cells[Row, 10].Value = 1m;
                                }
                            }

                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd2, dr, c, Row, 18, 14);
                            Sm.SetGrdValue("S", Grd2, dr, c, Row, 19, 15);

                            Grd2.Rows[Row].Level = Level;
                            if (Row > 0)
                            {
                                Grd2.Rows[Row - 1].TreeButton =
                                    (PrevMenuCode.Length >= Sm.DrStr(dr, 0).Length) ?
                                        iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                            }
                            PrevMenuCode = Sm.DrStr(dr, 0);
                            Row++;
                        }
                    }
                    //if (Row > 0)
                    Grd2.Rows[Grd2.Rows.Count - 1].TreeButton =
                        (PrevMenuCode.Length >= Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 1).Length) ?
                            iGTreeButtonState.Hidden : iGTreeButtonState.Visible;
                }
                Grd2.TreeLines.Visible = true;

                //for(int r = 0; r<Grd2.Rows.Count;r++)
                //    ComputeTotalPrice(r);
                Grd2.EndUpdate();
                dr.Close();
            }

        }

        private bool IsUserAbleToInsert()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblGroupMenu A, TblMenu B, TblUser C ");
            SQL.AppendLine("Where A.MenuCode=B.MenuCode ");
            SQL.AppendLine("And Left(A.AccessInd, 1)='Y' ");
            SQL.AppendLine("And B.Param='FrmBOQ3' ");
            SQL.AppendLine("And A.GrpCode=C.GrpCode ");
            SQL.AppendLine("And C.UserCode=@Param;");

            return (Sm.IsDataExist(SQL.ToString(), Gv.CurrentUserCode));
        }

        private void BtnInsertClick()
        {
            try
            {
                mStateIndicator = "I";
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                ChkActInd.Checked = true;
                Sm.SetLue(LueCurCode, Sm.GetValue("Select ParValue From TblParameter Where ParCode='MainCurCode'"));
                Sm.SetLue(LueProcessInd, "D");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueSPCode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void SetLueCtPersonCode(ref DXE.LookUpEdit Lue, string CtCode, string ContactPersonName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Col1 From ( ");
            SQL.AppendLine("Select ContactPersonName As Col1 From TblCustomerContactPerson Where CtCode=@CtCode ");
            if (ContactPersonName.Length > 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select @ContactPersonName As Col1 ");
            }
            SQL.AppendLine(") T Order By Col1;");

            var cm = new MySqlCommand();
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ContactPersonName", ContactPersonName);

            Sm.SetLue1(ref Lue, ref cm, "Contact Person");
            if (ContactPersonName.Length > 0) Sm.SetLue(Lue, ContactPersonName);

        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No || Sm.IsTxtEmpty(TxtDocNo, "Document#", false)) return;

            string[] TableName = { "BOQ", "BOQDtl", "BOQTot", "BOQDtl2", "BOQSign" };

            var l = new List<BOQ>();
            var l2 = new List<BOQDtl>();
            var l3 = new List<BOQTot>();
            var l4 = new List<BOQDtl2>();
            //var l5 = new List<BOQDtl3>();
            var lI = new List<ItemBOQ>();
            var lSign = new List<BOQSign>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As 'Kop', ");

            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, B.ProjectName, C.Address As SignedCity, D.CtName, DATE_FORMAT(A.QtStartDt,'%d %M %Y') As BOQStartDt, DATE_FORMAT(A.QtEndDt,'%d %M %Y') As EndDt,");
            SQL.AppendLine("A.Remark, A.EstRevAmt, A.EstResAmt, A.GrandTotal, A.ProfitPerc ");
            SQL.AppendLine("FROM TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblSite C On B.SiteCode = C.SiteCode ");
            SQL.AppendLine("Inner Join TblCustomer D On A.CtCode = D.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                         //0
                         "CompanyLogo",
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         
                         //6-10
                         "DocNo",
                         "DocDt",
                         "Remark",
                         "ProjectName",
                         "CtName",

                         //11-15
                         "BOQStartDt",
                         "EndDt",
                         "EstRevAmt",
                         "EstResAmt",
                         "GrandTotal",

                         //16
                         "ProfitPerc"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BOQ()
                        {

                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            ProjectName = Sm.DrStr(dr, c[9]),
                            CtName = Sm.DrStr(dr, c[10]),

                            BOQStartDt = Sm.DrStr(dr, c[11]),
                            EndDt = Sm.DrStr(dr, c[12]),
                            SubTotal1 = Sm.DrDec(dr, c[13]),
                            SubTotal2 = Sm.DrDec(dr, c[14]),
                            NetRev = Sm.DrDec(dr, c[15]),
                            GPM = Sm.DrDec(dr, c[16]),


                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select A.RevenueItCode, B.ItName, A.Remark, A.Qty, A.Price, A.Qty*A.Price As Amt ");
                SQLDtl.AppendLine("From TblBOQDtl5 A  ");
                SQLDtl.AppendLine("Inner Join TblItem B On A.RevenueItCode=B.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo ");


                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                         //0
                         "RevenueItCode" ,

                         //1-5
                         "ItName",
                         "Remark" ,
                         "Qty",
                         "Price" ,
                         "Amt"


                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        l2.Add(new BOQDtl()
                        {
                            nomor = nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Remark = Sm.DrStr(drDtl, cDtl[2]),
                            Qty = Sm.DrDec(drDtl, cDtl[3]),
                            UPrice = Sm.DrDec(drDtl, cDtl[4]),
                            Amt = Sm.DrDec(drDtl, cDtl[5]),

                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(l2);
            #endregion

            #region Detail Total

            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            #region Old Code
            //SQL3.AppendLine("Select A.DocNo, Sum(A.TotalAmt)Amt, truncate(Sum(A.TotalAmt),0)AmtBulat, Truncate((Sum(A.TotalAmt)*10)/100,0) As PPn, ");
            //SQL3.AppendLine("Truncate(Sum(A.TotalAmt),0)+Truncate((Sum(A.TotalAmt)*10)/100,0) As TotalAmt ");
            //SQL3.AppendLine("From TblItemBOQ B ");
            //SQL3.AppendLine("Left Join TblBOQDtl A On A.ItBOQCode=B.ItBOQCode And A.DocNo=@DocNo ");
            //SQL3.AppendLine("Where A.RequiredInd='Y' And B.Parent is Null ");
            //SQL3.AppendLine("Group By A.DocNo; ");
            #endregion 

            SQL3.AppendLine("Select A.DocNo, A.SubTotal Amt, Truncate(A.SubTotal, 0) AmtBulat, Truncate(A.Tax, 0) PPn, ");
            SQL3.AppendLine("Truncate(A.GrandTotal, 0) TotalAmt ");
            SQL3.AppendLine("From TblBOQHdr A ");
            SQL3.AppendLine("Where A.DocNo = @DocNo; ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-4
                         "Amt",
                         "AmtBulat",
                         "PPn", 
                         "TotalAmt",

                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new BOQTot()
                        {
                            DocNo = Sm.DrStr(dr3, c3[0]),

                            Amt = Sm.DrDec(dr3, c3[1]),
                            AmtBulat = Sm.DrDec(dr3, c3[2]),
                            PPn = Sm.DrDec(dr3, c3[3]),
                            TotalAmt = Sm.DrDec(dr3, c3[4]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr3, c3[4])),
                        });
                    }
                }
                dr3.Close();
            }

            myLists.Add(l3);
            #endregion

            #region Detail data 2
            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select A.ResourceItCode, B.ItName, A.Remark, A.Qty1, A.UPrice, A.Qty1*A.UPrice As Amt ");
                SQLDtl2.AppendLine("From TblBOQDtl6 A  ");
                SQLDtl2.AppendLine("Inner Join TblItem B On A.ResourceItCode=B.ItCode ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");


                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                        {
                         //0
                         "ResourceItCode" ,

                         //1-5
                         "ItName",
                         "Remark" ,
                         "Qty1",
                         "UPrice" ,
                         "Amt"


                        });
                if (drDtl2.HasRows)
                {
                    int nomor2 = 0;
                    while (drDtl2.Read())
                    {
                        nomor2 = nomor2 + 1;
                        l4.Add(new BOQDtl2()
                        {
                            nomor = nomor2,
                            ItCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            ItName = Sm.DrStr(drDtl2, cDtl2[1]),
                            Remark = Sm.DrStr(drDtl2, cDtl2[2]),
                            Qty = Sm.DrDec(drDtl2, cDtl2[3]),
                            UPrice = Sm.DrDec(drDtl2, cDtl2[4]),
                            Amt = Sm.DrDec(drDtl2, cDtl2[5]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(l4);
            #endregion

            #region Detail Signature
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select B.UserName, C.GrpName PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d/%m/%Y') as LastUpDt, A.EmpPict ");
                SQLDtl3.AppendLine("From ( ");
                SQLDtl3.AppendLine("    Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    From TblBOQHdr A ");
                SQLDtl3.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 4 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 5 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    Union All ");
                SQLDtl3.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQLDtl3.AppendLine("    FROM TblDocApproval A ");
                SQLDtl3.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 6 ");
                SQLDtl3.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQLDtl3.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null ");
                SQLDtl3.AppendLine("    ) A ");
                SQLDtl3.AppendLine("Left Join Tbluser B On A.UserCode = B.UserCode ");
                SQLDtl3.AppendLine("Left Join TblGroup C On B.GrpCode = C.GrpCode ");
                SQLDtl3.AppendLine("Group By A.UserCode, A.Seq, A.Title, A.LastUpDt ");
                SQLDtl3.AppendLine("Order By A.Seq Desc; ");

                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                var drDtl3 = cmDtl3.ExecuteReader();

                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                        {
                        //0-4
                        "UserName",
                        "EmpPict",
                        "Posname",
                        "LastUpDt",
                        "Seq",
                        "Title",
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        lSign.Add(new BOQSign()
                        {
                            UserName = Sm.DrStr(drDtl3, cDtl3[0]),
                            EmpPict = Sm.DrStr(drDtl3, cDtl3[1]),
                            PosName = Sm.DrStr(drDtl3, cDtl3[2]),
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[3]),
                            Sequence = Sm.DrStr(drDtl3, cDtl3[4]),
                            Title = Sm.DrStr(drDtl3, cDtl3[5]),
                            Space = "                       ",
                            LabelName = "Name : ",
                            LabelPos = "Position : ",
                            LabelDt = "Date : ",

                        });
                    }

                }
                drDtl3.Close();
            }
            myLists.Add(lSign);
            #endregion

            Sm.PrintReport("BOQMNET", myLists, TableName, false);
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnLOPDocNo_Click(object sender, EventArgs e)
        {
            if(BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmBOQ3Dlg2(this));
            }
        }

        private void BtnCtContactPersonName_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mMenuCode = "RSYYNNN";
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode), string.Empty);
                }
            }
        }

        private void BtnCtReplace_Click(object sender, EventArgs e)
        {
            var f = new FrmBOQ3Dlg(this, Sm.GetLue(LueCurCode));
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.ShowDialog();
        }

        private void BtnCtReplace2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                if (TxtQtReplace.EditValue != null && TxtQtReplace.Text.Length != 0)
                {
                    var f = new FrmBOQ3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtQtReplace.Text;
                    f.ShowDialog();
                }                
            }
        }

        #endregion

        #region Misc Control events

        private void LueResourceType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueResourceType, new Sm.RefreshLue2(Sl.SetLueOption), "ResourceType");
            }
        }

        private void LueResourceType_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueResourceType_Leave(object sender, EventArgs e)
        {
            if (LueResourceType.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LueResourceType).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 3].Value =
                    Grd2.Cells[fCell.RowIndex, 4].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueResourceType);
                    Grd2.Cells[fCell.RowIndex, 4].Value = LueResourceType.GetColumnValue("Col2");
                }
                LueResourceType.Visible = false;
            }
        }

        private void LueResourceName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueResourceName, new Sm.RefreshLue1(SetLueResourceName));
            }
        }

        private void LueResourceName_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueResourceName_Leave(object sender, EventArgs e)
        {
            if (LueResourceName.Visible && fAccept && fCell.ColIndex == 6)
            {
                if (Sm.GetLue(LueResourceName).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 5].Value =
                    Grd2.Cells[fCell.RowIndex, 6].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LueResourceName);
                    Grd2.Cells[fCell.RowIndex, 6].Value = LueResourceName.GetColumnValue("Col2");
                }
                LueResourceName.Visible = false;
            }
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            }
        }

        private void LueTaxCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueTaxCode1_Leave(object sender, EventArgs e)
        {
            if (LueTaxCode1.Visible && fAccept && fCell.ColIndex == 9)
            {
                if (Sm.GetLue(LueTaxCode1).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 8].Value =
                    Grd5.Cells[fCell.RowIndex, 9].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 8].Value = Sm.GetLue(LueTaxCode1);
                    Grd5.Cells[fCell.RowIndex, 9].Value = LueTaxCode1.GetColumnValue("Col2");
                }
                LueTaxCode1.Visible = false;
                ComputeRevItem(fCell.RowIndex);

            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            }
        }

        private void LueTaxCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueTaxCode2_Leave(object sender, EventArgs e)
        {
            if (LueTaxCode2.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueTaxCode2).Length == 0)
                    Grd5.Cells[fCell.RowIndex, 11].Value =
                    Grd5.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd5.Cells[fCell.RowIndex, 11].Value = Sm.GetLue(LueTaxCode2);
                    Grd5.Cells[fCell.RowIndex, 12].Value = LueTaxCode2.GetColumnValue("Col2");
                }
                LueTaxCode2.Visible = false;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    LueCtContactPersonName.EditValue = null;
                    Sm.SetControlReadOnly(LueCtContactPersonName, true);
                }
                else
                {
                    SetLueCtPersonCode(ref LueCtContactPersonName, Sm.GetLue(LueCtCode), string.Empty);
                    Sm.SetControlReadOnly(LueCtContactPersonName, false);
                }
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtQtReplace  
                });
                
                Sm.ClearGrd(Grd2, true);
                LoadAllItemBOQ();
            }
        }

        private void LueCtContactPersonName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueCtContactPersonName, new Sm.RefreshLue3(SetLueCtPersonCode), Sm.GetLue(LueCtCode), string.Empty);
        }

        private void LueRingCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueRingCode, new Sm.RefreshLue1(Sl.SetLueRingCode));
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePICCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueAgingAP_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) 
                Sm.RefreshLookUpEdit(LueAgingAP, new Sm.RefreshLue1(Sl.SetLueAgingAP));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
                TxtQtReplace.EditValue = null;
                Sm.ClearGrd(Grd2, true);
                if (Sm.GetLue(LueCurCode).Length > 0 && Sm.GetLue(LueCtCode).Length > 0)
                    LoadAllItemBOQ();
            }
        }

        private void TxtCreditLimit_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtCreditLimit, 0);
                ComputePercentage();
            }
        }


        private void TxtDirectCost_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDirectCost, 0);
                ComputeSubTotalRBPS();
            }
        }

        private void TxtIndirectCost_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtIndirectCost, 0);
                ComputeSubTotalRBPS();
            }
        }

        private void TxtPolicy_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //Sm.FormatNumTxt(TxtPolicy, 0);
                ComputeSubTotalRBPS();
            }
        }


        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void ChkTaxInd_CheckedChanged(object sender, EventArgs e)
        {
            //if (BtnSave.Enabled)
            //{
            //    ComputeTax();
            //    if (ChkTaxInd.Checked)
            //    {
            //        LuePPN.Properties.ReadOnly = false;
            //        LuePPN.BackColor = Color.FromArgb(255, 255, 255);
            //    }
            //    else
            //    {
            //        LuePPN.Properties.ReadOnly = true;
            //        LuePPN.BackColor = Color.FromArgb(195, 195, 195);
            //    }
            //}
        }

        private void LuePPN_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                //Sm.RefreshLookUpEdit(LuePPN, new Sm.RefreshLue2(Sl.SetLueOption), "PPNForProject");
                //if (Sm.GetLue(LuePPN) == "" && isinsert)
                //    Sm.SetLue(LuePPN, mPPNDefaultSetting);
                //if (ChkTaxInd.Checked)
                //{
                //    ComputeTax();
                //}
            }
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue2(Sl.SetLueOption), "ProjectImplementationProcessInd");
            }
        }

        #endregion

        #endregion

        #region Class

        private class BOQ
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string ProjectName { get; set; }
            public string CtName { get; set; }
            public string BOQStartDt { get; set; }
            public string EndDt { get; set; }
            public string PrintBy { get; set; }
            public decimal SubTotal1 { get; set; }
            public decimal SubTotal2 { get; set; }
            public decimal NetRev { get; set; }
            public decimal GPM { get; set; }
        }

        private class BOQDtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string Remark { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
        }

        private class ItemBOQ
        {
            public string ItBOQCode { get; set; }
            public string ItBOQName { get; set; }
            public string Parent { get; set; }
            public int Level { get; set; }
            public float Indent { get; set; }
            public string Index1 { get; set; }
            public string Index2 { get; set; }
        }

        private class BOQTot
        {
            public string DocNo { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtBulat { get; set; }
            public decimal PPn { get; set; }
            public decimal TotalAmt { get; set; }
            public string Terbilang { get; set; }
        }

        private class BOQDtl2
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string Remark { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amt { get; set; }
        }

        private class BOQDtl3
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }


        private class BOQSign
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string LastUpDt { get; set; }
            public string PosName { get; set; }
            public string Position { get; set; }
            public string Date { get; set; }
            public string Sequence { get; set; }
            public string Title { get; set; }
            public string Space { get; set; }
            public string LabelName { get; set; }
            public string LabelPos { get; set; }
            public string LabelDt { get; set; }

        }
        #endregion         

    }
}
