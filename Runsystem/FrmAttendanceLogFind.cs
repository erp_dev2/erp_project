﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmAttendanceLogFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmAttendanceLog mFrmParent;
        string mSQL;
        string CreateDt;

        #endregion

        #region Constructor

        public FrmAttendanceLogFind(FrmAttendanceLog FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion


        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();

            string CurrentDateTime = Sm.ServerCurrentDateTime();
            Sl.SetLueMth(LueMth);
            Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            Sl.SetLueYr(LueYr, "");
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, Left(A.CreateDt, 8) As Date ");
            SQL.AppendLine("From TblAttendanceLog A ");
            SQL.AppendLine("Where Left(A.CreateDt, 4)=@Year And Substring(A.CreateDt, 5, 2)=@MonthFilter ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;

            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        //6
                        "Last"+Environment.NewLine+"Updated Time",
                        "Date"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 5 });
            Sm.GrdFormatTime(Grd1, new int[] { 3, 6 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 7 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
           
            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = " ";

                    var cm = new MySqlCommand();

                    Sm.CmParam<String>(ref cm, "@MonthFilter", Sm.GetLue(LueMth));
                    Sm.CmParam<String>(ref cm, "@Year", Sm.GetLue(LueYr));

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + "  ",
                            new string[]
                        {
                            "CreateBy", 
                            "CreateDt",  "LastUpBy", "LastUpDt", "Date",
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("T", Grd1, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("T", Grd1, dr, c, Row, 6, 3);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            }, true, false, false, true
                        );
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7));
                this.Hide();
            }
        }


        #endregion

    }
}
