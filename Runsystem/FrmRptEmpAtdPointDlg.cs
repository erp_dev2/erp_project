﻿#region Namespace

using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpAtdPointDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptEmpAtdPoint mFrmParent;
        private string mSQL = string.Empty;
        private string mEmpCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEmpAtdPointDlg(FrmRptEmpAtdPoint FrmParent, string EmpCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mEmpCode = EmpCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); 
            ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtEmpCode, TxtEmpName
                    }, true);
                    TxtEmpName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtEmpCode, TxtEmpName
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "Document",

                        //1-3
                        "Date", 
                        "",
                        "Employee Code",
                        "Employee",
                        "Leave Code",
                        //6-9
                        "Leave Name",
                        "Day of Leave",
                        "Point", 
                        "Total Point"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] {2} );
            Sm.GrdFormatDec(Grd1, new int[] {7, 8, 9}, 0);
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9 });
        }

        private void HideInfoInGrd()
        {
            
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            TxtEmpCode.EditValue = mEmpCode;
            TxtEmpName.EditValue = Sm.GetValue("Select EmpName From tblEmployee Where EmpCode ='"+TxtEmpCode.Text+"' ");
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNO, A.DocDt, A.EmpCode, D.EmpName, A.LeaveCode, C.Leavename,  ");
	            SQL.AppendLine("Count(B.Docno) NoOfDay, C.Point, (Count(B.Docno)* C.Point)  LeavePoint  ");
	            SQL.AppendLine("From TblEmpLeaveHdr A  ");
	            SQL.AppendLine("Inner Join TblEmpleaveDtl B On A.Docno  = B.DocnO  ");
	            SQL.AppendLine("Inner Join tblleave C On A.LeaveCode = C.leaveCode  ");
                SQL.AppendLine("Inner JOin TblEMployee D On A.EmpCode = D.EmpCode ");
	            SQL.AppendLine("Where A.CancelInd = 'N' And A.Status = 'A'  ");
                
                mSQL = SQL.ToString();

                string Filter = "And A.EmpCode = '" + mEmpCode + "' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "Group by A.DocNo, A.EmpCode, D.EmpName, A.DocNO, A.DocDt, A.EmpCode, A.leaveCode, C.Leavename ",
                        new string[]
                        {
                            //0
                            "DocNo",
                            //1-5
                            "DocDt", "EmpCode", "EmpName", "LeaveCode", "leavename", 
                            //6
                            "NoOfDay","Point", "LeavePoint"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                        }, false, false, false, true
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

       
        #endregion

        #region event
        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmpLeave("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmEmpLeave("");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                f.ShowDialog();
            }
        }
        #endregion 

    }
}
