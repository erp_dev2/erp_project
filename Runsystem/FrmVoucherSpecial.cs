﻿#region Update
/*
    17/02/2021 [WED/IMS] new apps
    29/07/2021 [TKG/IMS] ubah journal rha
    29/07/2021 [TKG/IMS] ubah journal ot
    29/07/2021 [TKG/IMS] ubah journal employee's incentive
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherSpecial : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            mVoucherDocTypeManual = string.Empty,
            mBankAccountFormat = "0"
            ;
        internal FrmVoucherSpecialFind FrmFind;
        private string
            mVoucherCodeFormatType = "1",
            mPaymentTypeGiro = string.Empty,
            mMainCurCode = string.Empty,
            mEmpCodeVC = string.Empty,
            mVoucherDocTypeCodeSwitchingBankAccount = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty
            ;
        internal bool 
            mIsComparedToDetailDate = false,
            mIsVoucherFilteredByDept = false,
            mIsVoucherBankAccountFilteredByGrp = false,
            mIsVoucherUseCashType = false,
            mIsBankAccountCurrencyValidationInactive = false
            ;
        private List<LocalDocument> mlLocalDocument = null;
        private iGCopyPasteManager fCopyPasteManager;
        private bool
            mIsAutoJournalActived = false,
            mIsVoucherNeedCheckPosting = false,
            mIsRemarkForJournalMandatory = false,
            mIsEntityMandatory = false,
            mIsVoucherBtnDocTypeEnabled = false,
            mIsCheckCOAJournalNotExists = false,
            mIsVoucherPICNotMandatory = false
            ;
        internal string mGenerateCustomerCOAFormat = string.Empty;

        #region khusus print out IMS
        private string mPaymentUser2 = string.Empty, mDocNo2 = string.Empty, mDocDt2 = string.Empty;
        #endregion

        #endregion

        #region Constructor

        public FrmVoucherSpecial(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Voucher Special";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false; // print nya di masing2 dokumen Voucher aja (by wed)
                SetFormControl(mState.View);
                GetParameter();
                
                TcVoucher.SelectedTabPage = Tp2;

                if (mIsVoucherUseCashType) LblCashType.ForeColor = Color.Red;
                if (mVoucherDocTypeCodeSwitchingBankAccount.Length <= 0) mVoucherDocTypeCodeSwitchingBankAccount = "16";
                TcVoucher.SelectedTabPage = Tp2;
                SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueEntCode(ref LueEntCode);
                SetLueCashTypeCode(ref LueCashTypeCode, "");
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                if (mIsEntityMandatory) LblEntCode.ForeColor = Color.Red;
                if (mIsVoucherPICNotMandatory) label7.ForeColor = Color.Black; 

                TcVoucher.SelectedTabPage = Tp1;
                Sl.SetLueAcType(ref LueAcType);
                BtnDocType.Visible = false; // mIsVoucherBtnDocTypeEnabled;
                SetGrd();
                mlLocalDocument = new List<LocalDocument>();

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "Description", "Amount", "Remark" },
                    new int[]{ 400, 150, 150 }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2 });
            fCopyPasteManager = new iGCopyPasteManager(Grd1);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, TxtLocalDocNo, LuePIC, 
                        LueAcType, LueBankAcCode, LuePaymentType, 
                        LueBankCode, DteDueDt, LueEntCode, 
                        MeeRemark, LueCashTypeCode
                    }, true);
                    BtnVoucherRequest.Enabled = false;
                    BtnJournalDocNo.Enabled = BtnJournalDocNo2.Enabled = true;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueBankAcCode, LuePaymentType, LuePIC, 
                        LueEntCode, MeeRemark, LueCashTypeCode
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnJournalDocNo.Enabled = BtnJournalDocNo2.Enabled = false;
                    BtnVoucherRequest.Enabled = true;
                    Grd1.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Edit :
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeCancelReason, ChkCancelInd
                    }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mPaymentUser2 = string.Empty; mDocNo2 = string.Empty; mDocDt2 = string.Empty;
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, MeeCancelReason, TxtLocalDocNo, DteDocDt, 
                LuePIC, TxtVoucherRequestDocNo, TxtDocType, LueAcType, 
                LueBankAcCode, TxtCurCode, 
                LuePaymentType, LueBankCode, DteDueDt, 
                MeeRemark, LueEntCode, LueCashTypeCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd1();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #region Clear Grid

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 1 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherSpecialFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TcVoucher.SelectedTabPage = Tp1;
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueUserCode(ref LuePIC, string.Empty);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                Sm.FormShowDialog(new FrmVoucherSpecialDlg(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            mEntCode = Sm.GetLue(LueEntCode);

            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled &&
                e.ColIndex == 2 &&
                e.RowIndex == Grd1.Rows.Count - 1)
                e.DoDefault = false;
            Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = 0m;
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            //if (TxtDocNo.Text.Length == 0)
            //{
            //    Sm.GrdRemoveRow(Grd1, e, BtnSave);
            //    ComputeAmt();
            //}
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                    Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0m;
                ComputeAmt();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            string LocalDocNo = TxtLocalDocNo.Text;

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<DocVoucherHdr>();
            var l2 = new List<DocVoucherDtl>();
            var cml = new List<MySqlCommand>();

            PrepData(ref l, ref l2); //Persiapkan data voucher

            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherSpecial", "TblVoucherSpecialHdr");

            cml.Add(SaveVoucherSpecialHdr(DocNo));
            foreach (var x in l)
                cml.Add(SaveVoucherSpecialDtl(x, DocNo));

            foreach (var x in l)
            {
                cml.Add(SaveVoucherHdr(x, LocalDocNo));
                foreach (var y in l2.Where(w => Sm.CompareStr(w.DocNo, x.DocNo)))
                    cml.Add(SaveVoucherDtl(y));
            }

            // save journal
            int index = 1;
            foreach (var x in l)
            {
                string JournalDocNo = string.Empty;

                if (mDocNoFormat == "1")
                    JournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), index));
                else
                    JournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), mEntCode, index.ToString()));

                cml.Add(SaveJournal(x, JournalDocNo));
                index += 1;
            }

            //kurang bikin journal, trus bikin cancel nya, trus di test deh

            //if (mIsAutoJournalActived)
            //{
            //    if(mDocNoFormat == "1")
            //        mJournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            //    else
            //        mJournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), mEntCode, "1"));

            //        cml.Add(SaveJournal(DocNo, mJournalDocNo));
            //}

            Sm.ExecCommands(cml);

            ShowData(DocNo);

            l.Clear(); l2.Clear();
        }

        #region Validasi

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false) ||
                Sm.IsLueEmpty(LueAcType, "Debit/Credit") ||
                Sm.IsLueEmpty(LueBankAcCode, LblBankAcCode.Text) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                (mIsVoucherUseCashType && Sm.IsLueEmpty(LueCashTypeCode, "Cash Type")) ||
                (mIsEntityMandatory && Sm.IsLueEmpty(LueEntCode, "Entity")) ||
                (!mIsVoucherPICNotMandatory && Sm.IsLueEmpty(LuePIC, "Person in charge")) ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsPaymentTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsDateNotValid() ||
                IsEntCodeNotValid() ||
                IsBankAccountCurrencyInvalid() ||
                (mIsCheckCOAJournalNotExists && IsCOAJournalNotValid())
                ;
        }

        private bool IsBankAccountCurrencyInvalid()
        {
            if (mIsBankAccountCurrencyValidationInactive) return false;
            var CurCode = Sm.GetValue("Select CurCode From TblBankAccount Where BankAcCode=@Param;", Sm.GetLue(LueBankAcCode));

            if (!Sm.CompareStr(CurCode, TxtCurCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning,
                        "Bank Account's Currency : " + CurCode + Environment.NewLine +
                        "Voucher's Currency : " + TxtCurCode.Text + Environment.NewLine +
                        "Bank Account and voucher should have the same currency.");
                LueBankAcCode.Focus();
                return true;
            }
            return false;
        }

        private bool IsEntCodeNotValid()
        {
            var EntCode = Sm.GetLue(LueEntCode);
            if (EntCode.Length > 0)
            {
                if (!Sm.IsDataExist(
                    "Select 1 From TblBankAccount " +
                    "Where BankAcCode=@Param1 " +
                    "And EntCode Is Not Null " +
                    "And EntCode=@Param2;",
                    Sm.GetLue(LueBankAcCode), EntCode, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Invalid entity." + Environment.NewLine +
                        "Voucher request's entity should be the same as bank account's entity.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.Left(Sm.GetDte(DteDocDt), 8);

            if (mIsComparedToDetailDate)
            {
                if (TxtVoucherRequestDocNo.Text.Length != 0)
                {
                    mDetailDate = Sm.GetValue("SELECT DocDt FROM TblVoucherRequestSpecialHdr WHERE DocNo = '"+TxtVoucherRequestDocNo.Text+"'");

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Date should be the same or greater than Voucher Request's Date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
            }
            return false;
        }

        private bool IsCOAJournalNotValid()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select AcNo From ( ");
            SQL.AppendLine(GetCOAJournalSQL(ref cm));
            SQL.AppendLine(") Tbl ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
            {
                "AcNo",
            });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Sm.DrStr(dr, c[0]).Length == 0)
                        {
                            
                            Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                            return true;
                        }
                    }
                }
                dr.Close();
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Description is empty.")) return true;
                //if (Sm.IsGrdValueEmpty(Grd1, Row, 1, true, 
                //    "Description : " + Sm.GetGrdStr(Grd1, Row, 0) + Environment.NewLine + 
                //    "Amount is 0.")) return true;

            }
            return false;
        }
     
        #endregion

        private string GetCOAJournalSQL(ref MySqlCommand cm)
        {
            var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo In (Select VoucherRequestDocNo From TblVoucherRequestSpecialDtl2 Where DocNo=@Param) Limit 1;", TxtVoucherRequestDocNo.Text);
            var SQL = string.Empty;

            switch (DocType)
            {
                case "65":
                    SQL = GetCOAJournalSQL65(ref cm);
                    break;
                case "66":
                    SQL = GetCOAJournalSQL66(ref cm);
                    break;
                case "67":
                    SQL = GetCOAJournalSQL67(ref cm);
                    break;
            }

            return SQL.ToString();
        }

        private string GetCOAJournalSQL65(ref MySqlCommand cm)
        {
            //65 : Voucher Request Special - OT

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo4 AcNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Where A.DocNo = @VoucherRequestDocNo ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select COAAcNo As AcNo ");
            SQL.AppendLine("From TblBankAccount Where BankAcCode=@BankAcCode ");

            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetCOAJournalSQL66(ref MySqlCommand cm)
        {
            //66 : Voucher Request Special - RHA

            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.AcNo7 AcNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Where A.DocNo = @VoucherRequestDocNo ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("SELECT COAAcNo AcNo ");
            SQL.AppendLine("FROM TblBankAccount Where BankAcCode = @BankAcCode ");

            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GetCOAJournalSQL67(ref MySqlCommand cm)
        {
            //67 : Voucher Request Special - Incentive

            var SQL = new StringBuilder();


            SQL.AppendLine("Select B.AcNo8 AcNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Where A.DocNo = @VoucherRequestDocNo ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("Select COAAcNo As AcNo ");
            SQL.AppendLine("From TblBankAccount Where BankAcCode=@BankAcCode ");

            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));

            return SQL.ToString();
        }

        private string GenerateDocNo(string index)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
                type = string.Empty,
                DocSeqNo = "4",
                AcType = Sm.GetValue("Select C.AcType " +
                            "From TblVoucherRequestHdr C  " +
                            "Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode " +
                            "Where C.DocNo In (Select VoucherRequestDocNo From TblVoucherRequestSpecialDtl2 Where DocNo =  '" + TxtVoucherRequestDocNo.Text + "') Limit 1; "),
                BankAcCode = Sm.GetValue("Select C.BankAcCode " +
                            "From TblVoucherRequestHdr C  " +
                            "Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode " +
                            "Where C.DocNo In (Select VoucherRequestDocNo From TblVoucherRequestSpecialDtl2 Where DocNo =  '" + TxtVoucherRequestDocNo.Text + "') Limit 1; ");

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (AcType == "C")
                type = Sm.GetValue("Select ifnull(AutoNoCredit, '') From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");
            else
                type = Sm.GetValue("Select ifnull(AutoNoDebit, '') From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+" + index + ", Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("       From TblVoucherHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,"+DocSeqNo+") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '" + index + "'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+" + index + ", Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7,"+DocSeqNo+"), Decimal) As DocNoTemp ");
                SQL.Append("    From TblVoucherHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '" + index + "'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveVoucherSpecialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherSpecialHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, VoucherRequestSpecialDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @VoucherRequestSpecialDocNo, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VoucherRequestSpecialDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherSpecialDtl(DocVoucherHdr x, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherSpecialDtl(DocNo, VoucherDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @VoucherDocNo, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherHdr(DocVoucherHdr x, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, DeptCode, ");
            SQL.AppendLine("CancelInd, VoucherRequestDocNo, DocType, AcType, BankAcCode, ");
            SQL.AppendLine("CashTypeCode, PaymentType, GiroNo, BankCode, OpeningDt, DueDt, PIC, ");
            SQL.AppendLine("CurCode, Amt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @DeptCode, ");
            SQL.AppendLine("'N', @VoucherRequestDocNo, (Select DocType From TblVoucherRequestHdr Where DocNo=@VoucherRequestDocNo), ");
            SQL.AppendLine("@AcType, @BankAcCode, @CashTypeCode, @PaymentType, @GiroNo, @BankCode, @OpeningDt, @DueDt, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, @EntCode, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    VoucherDocNo=@DocNo ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", x.DeptCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", x.VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CashTypeCode", Sm.GetLue(LueCashTypeCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherDtl(DocVoucherDtl y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherDtl(DocNo,DNo,Description,Amt,Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @Description, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@Description", y.Description);
            Sm.CmParam<Decimal>(ref cm, "@Amt", y.Amt);
            Sm.CmParam<String>(ref cm, "@Remark", y.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsDocTypeValid(string DocType)
        {
            return (
                DocType == "65" ||
                DocType == "66" ||
                DocType == "67" 
                );
        }

        private MySqlCommand SaveJournal(DocVoucherHdr x, string NewJournalDocNo)
        {
            //65 : Voucher Request Special OT
            //66 : Voucher Request Special - RHA
            //67 : Voucher Request Special Incentive

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            var DocTitle = Sm.GetParameter("DocTitle");
            var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo In (Select VoucherRequestDocNo From TblVoucherRequestSpecialDtl2 Where DocNo = @Param) Limit 1;", TxtVoucherRequestDocNo.Text);
            string DocTypeDesc = string.Empty;
            switch (DocType)
            {
                case "65":
                    DocTypeDesc = "Voucher Request Special - OT";
                    break;
                case "66":
                    DocTypeDesc = "Voucher Request Special - RHA";
                    break;
                case "67":
                    DocTypeDesc = "Voucher Request Special - Incentive";
                    break;
            }

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", x.VoucherRequestDocNo);

            Sm.CmParam<String>(ref cm, "@NewJournalDocNo", NewJournalDocNo);

            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@DocType", DocTypeDesc);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    JournalDocNo = @NewJournalDocNo ");
            SQL.AppendLine(" Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Voucher (', IfNull(@DocType, 'None'), ') : ', A.DocNo, ' [', IfNull(B.DeptName, ''), ']') As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select X1.DocNo, X4.CCCode, X3.DeptName ");
            SQL.AppendLine("    From TblVoucherHdr X1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr X2 On X1.VoucherRequestDocNo = X2.DocNo ");
            SQL.AppendLine("    Inner Join TblDepartment X3 On X2.DeptCode = X3.DeptCode ");
            SQL.AppendLine("    Inner JOin TblCostCenter X4 On X3.DeptCode = X4.DeptCode And X4.DeptCode Is Not Null And X4.ActInd='Y' ");
            SQL.AppendLine("    Where X1.DocNo=@DocNo ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.JournalDocNo Is Not Null; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, ");

            if (Sm.CompareStr(TxtCurCode.Text, mMainCurCode))
            {
                SQL.AppendLine("B.DAmt, ");
                SQL.AppendLine("B.CAmt, ");
            }
            else
            {
                SQL.AppendLine("B.DAmt*( ");
                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("), 0.00) ");
                SQL.AppendLine(") As DAmt, ");
                SQL.AppendLine("B.CAMt*( ");
                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("), 0) ");
                SQL.AppendLine(") As CAmt, ");
            }
            SQL.AppendLine("IfNull(D.EntCode, C.ParValue), B.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Remark, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine(GetJournalSQL(ref cm, DocType));

            SQL.AppendLine("    ) Tbl Where AcNo Is Not Null Group By AcNo, Remark ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Left Join TblParameter C On C.ParCode='DefaultEntity' And C.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join TblVoucherHdr D On A.DocNo=D.JournalDocNo And D.JournalDocNo Is Not Null And D.DocNo=@DocNo ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            return cm;
        }

        private string GetJournalSQL(ref MySqlCommand cm, string DocType)
        {
            //65 : Voucher Request Special OT
            //66 : Voucher Request Special - RHA
            //67 : Voucher Request Special Incentive

            var SQL = string.Empty;
            switch(DocType)
            {
                case "65":
                    SQL = GetJournalSQL65(ref cm);
                    break;
                case "66":
                    SQL = GetJournalSQL66(ref cm);
                    break;
                case "67":
                    SQL = GetJournalSQL67(ref cm);
                    break;
            }
            return SQL;
        }

        private string GetJournalSQL65(ref MySqlCommand cm)
        {
            //65 : Voucher Request Special - OT

            var SQL = new StringBuilder();

            //SQL.AppendLine("Select B.AcNo4 AcNo, Null As Remark, A.Amt DAmt, 0.00 CAmt ");
            //SQL.AppendLine("From TblVoucherRequestHdr A ");
            //SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode And B.AcNo4 Is Not Null ");
            //SQL.AppendLine("Where A.DocNo = @VoucherRequestDocNo ");            

            SQL.AppendLine("Select T4.Property5 As AcNo, T4.OptDesc As Remark, T2.Amt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct B.DeptCode, D.DocNo2 As DocNo ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl2 C On B.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl D On C.DocNo=D.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblOTVerificationDtl T2 On T1.DocNo=T2.DocNo And T1.DeptCode=T2.DeptCode ");
            SQL.AppendLine("Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("Inner Join TblOption T4 On T4.OptCat='EmpCostGroup' And T3.CostGroup=T4.OptCode And T4.Property5 Is Not Null ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo AcNo, Null As Remark, 0.00 As DAmt, IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A, TblBankAccount B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");

            return SQL.ToString();
        }

        private string GetJournalSQL66(ref MySqlCommand cm)
        {
            //66 : Voucher Request Special - RHA

            var SQL = new StringBuilder();

            //SQL.AppendLine("Select B.AcNo7 AcNo, A.Amt DAmt, 0.00 CAmt ");
            //SQL.AppendLine("From TblVoucherRequestHdr A ");
            //SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode And B.AcNo7 Is Not Null ");
            //SQL.AppendLine("Where A.DocNo = @VoucherRequestDocNo ");

            SQL.AppendLine("Select T4.Property3 As AcNo, T4.OptDesc As Remark, T2.Amt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select Distinct B.DeptCode, D.DocNo2 As DocNo ");
	        SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
	        SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl2 C On B.DocNo=C.VoucherRequestDocNo ");
	        SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl D On C.DocNo=D.DocNo ");
	        SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo And T1.DeptCode=T2.DeptCode ");
            SQL.AppendLine("Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode ");
            SQL.AppendLine("Inner Join TblOption T4 On T4.OptCat='EmpCostGroup' And T3.CostGroup=T4.OptCode And T4.Property3 Is Not Null ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo AcNo, Null As Remark, 0.00 As DAmt, IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A, TblBankAccount B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");
            
            return SQL.ToString();
        }

        private string GetJournalSQL67(ref MySqlCommand cm)
        {
            //67 : Voucher Request Special - Incentive

            var SQL = new StringBuilder();

            //SQL.AppendLine("Select B.AcNo8 AcNo, Null As Remark, A.Amt DAmt, 0.00 CAmt ");
            //SQL.AppendLine("From TblVoucherRequestHdr A ");
            //SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode And B.AcNo8 Is Not Null ");
            //SQL.AppendLine("Where A.DocNo = @VoucherRequestDocNo ");

            SQL.AppendLine("Select T4.Property4 As AcNo, T4.OptDesc As Remark, T2.Amount As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct B.DeptCode, D.DocNo2 As DocNo ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl2 C On B.DocNo=C.VoucherRequestDocNo ");
            SQL.AppendLine("    Inner Join TblVoucherRequestSpecialDtl D On C.DocNo=D.DocNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmpIncentiveDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee T3 On T2.EmpCode=T3.EmpCode And T1.DeptCode=T3.DeptCode ");
            SQL.AppendLine("Inner Join TblOption T4 On T4.OptCat='EmpCostGroup' And T3.CostGroup=T4.OptCode And T4.Property4 Is Not Null ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.COAAcNo AcNo, Null As Remark, 0.00 As DAmt, IfNull(A.Amt, 0.00) As CAmt ");
            SQL.AppendLine("From TblVoucherHdr A, TblBankAccount B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.BankAcCode=B.BankAcCode And B.COAAcNo Is Not Null ");

            return SQL.ToString();
        }

        #endregion

        #region Edit data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(CancelVoucherHdr());

            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
            var l = new List<DocVoucherHdrCancel>();

            if (mIsAutoJournalActived)
            {
                PrepDataCancel(ref l);
                if (l.Count > 0)
                {
                    int index = 1;
                    foreach (var x in l)
                    {
                        string JournalDocNo = string.Empty;

                        if (IsClosingJournalUseCurrentDt)
                        {
                            if (mDocNoFormat == "1")
                                JournalDocNo = Sm.GenerateDocNoJournal(CurrentDt, index);
                            else
                                JournalDocNo = Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, index.ToString());
                        }
                        else
                        {
                            if (mDocNoFormat == "1")
                                JournalDocNo = Sm.GenerateDocNoJournal(DocDt, index);
                            else
                                JournalDocNo = Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, index.ToString());
                        }

                        cml.Add(SaveJournal2(x, JournalDocNo, IsClosingJournalUseCurrentDt));

                        index += 1;
                    }
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
            l.Clear();
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsVoucherNotCancelled() ||
                IsDataCancelledAlready()
                ;
        }

        private bool IsVoucherNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this voucher.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblVoucherSpecialHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelVoucherHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherSpecialHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo In (Select VoucherDocNo From TblVoucherSpecialDtl Where DocNo = @DocNo) And CancelInd='N'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSpecialDtl2 B On A.DocNo = B.VoucherRequestDOcNo ");
            SQL.AppendLine("Set A.VoucherDocNo = Null ");
            SQL.AppendLine("Where B.DocNo = @VoucherRequestSpecialDocNo ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestSpecialDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowVoucherSpecialHdr(DocNo);
                ShowVoucherSpecialDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherSpecialHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, C.LocalDocNo, A.VoucherRequestSpecialDocNo, ");
            SQL.AppendLine("D.OptDesc As DocTypeDesc, C.AcType, C.BankAcCode, C.CurCode, Sum(C.Amt) Amt, C.PaymentType, ");
            SQL.AppendLine("C.CashTypeCode, C.BankCode, C.DueDt, C.EntCode, C.PIC, C.Remark ");
            SQL.AppendLine("From TblVoucherSpecialHdr A ");
            SQL.AppendLine("Inner Join TblVoucherSpecialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On B.VoucherDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblOption D On C.DocType = D.OptCode And D.OptCat = 'VoucherDocType' ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "LocalDocNo", "VoucherRequestSpecialDocNo", 
                    
                    //6-10
                    "DocTypeDesc", "AcType", "BankAcCode", "CurCode", "Amt", 
                    
                    //11-15
                    "PaymentType", "CashTypeCode", "BankCode", "DueDt", "EntCode", 
                    
                    //16-17
                    "PIC", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtDocType.EditValue = Sm.DrStr(dr, c[6]);
                    Sm.SetLue(LueAcType, Sm.DrStr(dr, c[7]));
                    SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[8]));
                    TxtCurCode.EditValue = Sm.DrStr(dr, c[9]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                    Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[11]));
                    SetLueCashTypeCode(ref LueCashTypeCode, Sm.DrStr(dr, c[12]));
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[13]));
                    Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[14]));
                    Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[15]));
                    Sl.SetLueUserCode(ref LuePIC, Sm.DrStr(dr, c[16]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[17]);                        
                }, true
            );
        }

        private void ShowVoucherSpecialDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.Description, B.Amt, B.Remark ");
            SQL.AppendLine("From TblVoucherSpecialDtl A ");
            SQL.AppendLine("Inner Join TblVOucherDtl B On A.VoucherDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order by B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]{ "Description", "Amt", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowVoucherRequestInfo(string DocNo)
        {
            try
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtVoucherRequestDocNo, TxtDocType, LueAcType, LueBankAcCode, 
                    LuePaymentType, LueBankCode,
                    DteDueDt, LuePIC, LueEntCode
                });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
                ClearGrd1();
                ShowVoucherRequestHdr(DocNo);
                ShowVoucherRequestDtl(DocNo);
                ComputeAmt();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowVoucherRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.DocType, D.OptDesc As DocTypeDesc, C.AcType, C.BankAcCode, C.AcType2, C.BankAcCode2, ");
            SQL.AppendLine("C.PaymentType, C.GiroNo, C.BankCode, C.OpeningDt, C.DueDt, C.PIC, Null AS VdInvNo, ");
            SQL.AppendLine("C.CurCode, Sum(C.Amt) Amt, C.CurCode2, C.ExcRate, C.EntCode ");
            SQL.AppendLine("From TblVoucherRequestSpecialHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSpecialDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On B.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblOption D On C.DocType = D.OptCode And D.OptCAt = 'VoucherDocType' ");
            SQL.AppendLine("Group By C.DocType, D.OptDesc, C.AcType, C.BankAcCode, C.AcType2, C.BankAcCode2, ");
            SQL.AppendLine("C.PaymentType, C.GiroNo, C.BankCode, C.OpeningDt, C.DueDt, C.PIC, ");
            SQL.AppendLine("C.CurCode, C.CurCode2, C.ExcRate, C.EntCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm,
                 SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocTypeDesc",
                    
                    //1-5
                    "AcType", "BankAcCode", "AcType2", "BankAcCode2", "PaymentType",  
                    
                    //6-10
                    "GiroNo", "BankCode", "DueDt", "PIC", "VdInvNo", 
                    
                    //11-15
                    "CurCode", "Amt", "CurCode2", "ExcRate", "OpeningDt",
 
                    //16
                    "EntCode"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtVoucherRequestDocNo.EditValue = DocNo;
                     TxtDocType.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetLue(LueAcType, Sm.DrStr(dr, c[1]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[2]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[7]));
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[8]));
                     Sm.SetLue(LuePIC, Sm.DrStr(dr, c[9]));
                     TxtCurCode.EditValue = Sm.DrStr(dr, c[11]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                     Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[16]));
                 }, true
             );
        }

        private void ShowVoucherRequestDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select C.Description, C.Amt, C.Remark ");
            SQL.AppendLine("From TblVoucherRequestSpecialHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestSpecialDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo= @DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("Inner Join TblVoucherRequestDtl C On B.VoucherRequestDocNo = C.DocNo ");
            SQL.AppendLine(" ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { "Description", "Amt", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = 0m;
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void PrepData(ref List<DocVoucherHdr> l, ref List<DocVoucherDtl> l2)
        {
            PrepHdr(ref l);
            PrepDtl(ref l2);
            GetVoucherDocNo(ref l);
            GetVoucherDocNoDtl(ref l, ref l2);
        }

        private void PrepHdr(ref List<DocVoucherHdr> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.VoucherRequestDocNo, B.AcType, B.DocType, B.DeptCode, B.Amt ");
            SQL.AppendLine("From TblVoucherRequestSpecialDtl2 A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestSpecialDocNo ");
            SQL.AppendLine("Order By A.VoucherRequestDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@VoucherRequestSpecialDocNo", TxtVoucherRequestDocNo.Text); 

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    "VoucherRequestDocNo", 
                    "AcType", "DocType", "DeptCode", "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DocVoucherHdr() 
                        {
                            DocNo = string.Empty,
                            VoucherRequestDocNo = Sm.DrStr(dr, c[0]),
                            AcType = Sm.DrStr(dr, c[1]),
                            DocType = Sm.DrStr(dr, c[2]),
                            DeptCode = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepDtl(ref List<DocVoucherDtl> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.VoucherRequestDocNo, B.DNo, B.Description, B.Amt, B.Remark ");
            SQL.AppendLine("From TblVoucherRequestSpecialDtl2 A ");
            SQL.AppendLine("Inner Join TblVoucherRequestDtl B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@VoucherRequestSpecialDocNo ");
            SQL.AppendLine("Order By A.VoucherRequestDocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@VoucherRequestSpecialDocNo", TxtVoucherRequestDocNo.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    "VoucherRequestDocNo", 
                    "DNo", "Description", "Amt", "Remark"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new DocVoucherDtl() 
                        {
                            DocNo = string.Empty,
                            VoucherRequestDocNo = Sm.DrStr(dr, c[0]),
                            DNo = Sm.DrStr(dr, c[1]),
                            Description = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            Remark = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetVoucherDocNo(ref List<DocVoucherHdr> l)
        {
            int index = 1;
            foreach (var x in l)
            {
                x.DocNo = GenerateDocNo(index.ToString());
                index += 1;
            }
        }

        private void GetVoucherDocNoDtl(ref List<DocVoucherHdr> l, ref List<DocVoucherDtl> l2)
        {
            foreach (var x in l)
            {
                foreach (var y in l2.Where(w =>Sm.CompareStr(w.VoucherRequestDocNo, x.VoucherRequestDocNo)))
                    y.DocNo = x.DocNo;
            }
        }

        private void PrepDataCancel(ref List<DocVoucherHdrCancel> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.DocNo, B.JournalDocNo ");
            SQL.AppendLine("From TblVoucherSpecialDtl A ");
            SQL.AppendLine("Inner Join TblVOucherHdr B On A.VoucherDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "JournalDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DocVoucherHdrCancel()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            JournalDocNo = Sm.DrStr(dr, c[1]),
                            JournalDocNo2 = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private MySqlCommand SaveJournal2(DocVoucherHdrCancel x, string NewJournalDocNo, bool IsClosingJournalUseCurrentDt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", NewJournalDocNo);

            SQL.AppendLine("Update TblVoucherHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            cm.CommandText = SQL.ToString();

            return cm;
        }

        private void SetLueVoucherPaymentType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='VoucherPaymentType' And OptCode In ('B', 'C') Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetCurCode(string BankAcCode)
        {
            var CurCode = Sm.GetValue("Select CurCode From TblBankAccount Where bankAcCode=@Param And CurCode Is Not Null;", BankAcCode);
            if (CurCode.Length > 0) TxtCurCode.EditValue = CurCode;
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }

            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");

            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");
            else
            {
                if (mIsVoucherBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("Where Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        internal void SetLueCashTypeCode(ref LookUpEdit Lue, string CashTypeCode)
        {
            Sm.SetLue2(
                ref Lue,
                "Select CashTypeCode As Col1, CashTypeName As Col2 From TblCashType Order By CashTypeName",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CashTypeCode.Length != 0) Sm.SetLue(Lue, CashTypeCode);
        }

        private bool IsVoucherDocTypeManual()
        { 
            var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo=@Param;", TxtVoucherRequestDocNo.Text);
            return (Sm.CompareStr(mVoucherDocTypeManual, DocType));
        }

        private string FormatNum(Decimal NumValue)
        {
            try
            {
                return String.Format("{0:#,###,##0.00##########}", NumValue);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        private string FormatNum(string Value)
        {
            decimal NumValue = 0m;
            try
            {
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }
                if (NumValue < 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Value should not be less than 0.");
                    NumValue = 0m;
                }
                return String.Format("{0:#,###,##0.00##########}", NumValue);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        private void FormatNumTxt(TextEdit Txt)
        {
            Txt.EditValue = FormatNum(Txt.Text);
        }

        private void fButtonCopy_Click(object sender, EventArgs e)
        {
            if (!fCopyPasteManager.CanCopyToClipboard())
            {
                MessageBox.Show(this, "No cells are selected.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            fCopyPasteManager.CopyToClipboard();
        }

        private void GetParameter()
        {
            string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            mIsComparedToDetailDate = Sm.GetParameter("IsComparedToDetailDate") == "Y";
            mPaymentTypeGiro = Sm.GetParameter("PaymentTypeGiro");
            mIsAutoJournalActived = Sm.GetParameter("IsAutoJournalActived") == "Y";
            mIsVoucherNeedCheckPosting = Sm.GetParameter("IsVoucherNeedCheckPosting") == "Y";
            mVoucherDocTypeManual = Sm.GetParameter("VoucherDocTypeManual");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsRemarkForJournalMandatory = Sm.GetParameter("IsRemarkForJournalMandatory") == "Y";
            mIsEntityMandatory = Sm.GetParameter("IsEntityMandatory") == "Y";
            mIsVoucherFilteredByDept = Sm.GetParameter("IsVoucherFilteredByDept") == "Y";
            mIsVoucherBankAccountFilteredByGrp = Sm.GetParameter("IsVoucherBankAccountFilteredByGrp") == "Y";
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mEmpCodeVC = Sm.GetParameter("EmpCodeVC");
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mIsBankAccountCurrencyValidationInactive = Sm.GetParameterBoo("IsBankAccountCurrencyValidationInactive");
            mVoucherDocTypeCodeSwitchingBankAccount = Sm.GetParameter("VoucherDocTypeCodeSwitchingBankAccount");
            mIsVoucherBtnDocTypeEnabled = Sm.GetParameterBoo("IsVoucherBtnDocTypeEnabled");
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            mIsVoucherUseCashType = Sm.GetParameterBoo("IsVoucherUseCashType");
            mIsVoucherPICNotMandatory = Sm.GetParameterBoo("IsVoucherPICNotMandatory");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        Amt += Sm.GetGrdDec(Grd1, r, 1);
            }
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void SetEntCode()
        { 
            var BankAcCode = Sm.GetLue(LueBankAcCode);
            LueEntCode.EditValue = null;
            if (BankAcCode.Length > 0)
            {
                var EntCode = Sm.GetValue("Select EntCode From TblBankAccount Where BankAcCode=@Param;", BankAcCode);
                if (EntCode.Length > 0) Sm.SetLue(LueEntCode, EntCode);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            //if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {

                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(SetLueVoucherPaymentType));
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(DteDueDt, false);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
                Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
                if (Sm.GetLue(LueAcType) == "C")
                    LblBankAcCode.Text = "           Credit To";
                else if (Sm.GetLue(LueAcType) == "D")
                    LblBankAcCode.Text = "           Debit To";
                else
                    LblBankAcCode.Text = "Debit / Credit To";
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!mIsBankAccountCurrencyValidationInactive) TxtCurCode.EditValue = null;
                try
                {
                    Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
                    SetEntCode();
                    if (Sm.GetLue(LueBankAcCode).Length > 0)
                    {
                        if (!mIsBankAccountCurrencyValidationInactive)
                            SetCurCode(Sm.GetLue(LueBankAcCode));
                    }
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.Message);
                }
            }
        }

        private void LueCashTypeCode_EditValueChanged(object sender, EventArgs e)
        {
            if (!BtnSave.Enabled) return;
            try
            {
                Sm.RefreshLookUpEdit(LueCashTypeCode, new Sm.RefreshLue2(SetLueCashTypeCode), string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        #endregion

        #region Button Event

        private void BtnVoucherRequest_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherSpecialDlg(this));
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "VR Special#", false))
            {
                string DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo In (Select VoucherRequestDocNo From TblVoucherRequestSpecialDtl2 Where DocNo = @Param) Limit 1; ", TxtVoucherRequestDocNo.Text);
                string MenuCode = string.Empty;

                if (DocType == "65") MenuCode = Sm.GetParameter("MenuCodeForVRSOT");
                if (DocType == "66") MenuCode = Sm.GetParameter("MenuCodeForVRSRHA");
                if (DocType == "67") MenuCode = Sm.GetParameter("MenuCodeForVRSIncentive");

                if (DocType.Length == 0) MenuCode = Sm.GetParameter("MenuCodeForVRSRHA");

                var f = new FrmVoucherRequestSpecial(MenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
        }

        private void BtnDocType_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "VR Special#", false))
                {
                    var VoucherRequestDocNo = TxtVoucherRequestDocNo.Text;
                    var DocType = Sm.GetValue("Select DocType From TblVoucherRequestHdr Where DocNo=@Param;", TxtVoucherRequestDocNo.Text);
                    var DocNo = string.Empty;
                    switch (DocType)
                    {
                        case "01":
                            DocNo = VoucherRequestDocNo;
                            if (DocNo.Length > 0)
                            {
                                //var f01 = new FrmVoucherRequest(mMenuCode);
                                //f01.Tag = "***";
                                //f01.WindowState = FormWindowState.Normal;
                                //f01.StartPosition = FormStartPosition.CenterScreen;
                                //f01.mDocNo = DocNo;
                                //f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "02":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblIncomingPaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                if (Sm.GetParameter("DocTitle") == "IMS")
                                {
                                    //var f02 = new FrmIncomingPayment2(mMenuCode);
                                    //f02.Tag = "***";
                                    //f02.WindowState = FormWindowState.Normal;
                                    //f02.StartPosition = FormStartPosition.CenterScreen;
                                    //f02.mDocNo = DocNo;
                                    //f02.ShowDialog();
                                }

                                else
                                {
                                    ////var f02 = new FrmIncomingPayment(mMenuCode);
                                    ////f02.Tag = "***";
                                    ////f02.WindowState = FormWindowState.Normal;
                                    ////f02.StartPosition = FormStartPosition.CenterScreen;
                                    ////f02.mDocNo = DocNo;
                                    ////f02.ShowDialog();
                                }
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "03":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblOutgoingPaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                //var f03 = new FrmOutgoingPayment(mMenuCode);
                                //f03.Tag = "***";
                                //f03.WindowState = FormWindowState.Normal;
                                //f03.StartPosition = FormStartPosition.CenterScreen;
                                //f03.mDocNo = DocNo;
                                //f03.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "04":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblAPDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmAPDownpayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "05":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblARDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmARDownpayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "06":
                            if (Sm.GetParameter("DocTitle") == "IMS")
                            {
                                DocNo = Sm.GetValue(
                                    "Select DocNo From TblVoucherRequestPayrollHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                    VoucherRequestDocNo);
                                if (DocNo.Length > 0)
                                {
                                    var f01 = new FrmVoucherRequestPayroll16(mMenuCode);
                                    f01.Tag = "***";
                                    f01.WindowState = FormWindowState.Normal;
                                    f01.StartPosition = FormStartPosition.CenterScreen;
                                    f01.mDocNo = DocNo;
                                    f01.ShowDialog();
                                }
                                else
                                    Sm.StdMsg(mMsgType.Info, "Not available information.");
                                break;
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "07":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblVoucherRequestSSHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmVoucherRequestSS(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "08":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblEmpClaimHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmEmpClaim(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "09":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblAdvancePaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmAdvancePayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "10":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "11":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "13":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblRHAHdr Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmRHA(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "14":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblReturnAPDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmReturnAPDownpayment(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "15":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "16":
                            DocNo = VoucherRequestDocNo;
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmVoucherRequest(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "17":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblOutgoingPaymentHdr Where VoucherRequestDocNo2=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f17 = new FrmOutgoingPayment(mMenuCode);
                                f17.Tag = "***";
                                f17.WindowState = FormWindowState.Normal;
                                f17.StartPosition = FormStartPosition.CenterScreen;
                                f17.mDocNo = DocNo;
                                f17.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "18":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblIncomingPaymentHdr Where VoucherRequestDocNo2=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f18 = new FrmIncomingPayment(mMenuCode);
                                f18.Tag = "***";
                                f18.WindowState = FormWindowState.Normal;
                                f18.StartPosition = FormStartPosition.CenterScreen;
                                f18.mDocNo = DocNo;
                                f18.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "19":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblAPDownpayment Where VoucherRequestDocNo2=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f19 = new FrmAPDownpayment(mMenuCode);
                                f19.Tag = "***";
                                f19.WindowState = FormWindowState.Normal;
                                f19.StartPosition = FormStartPosition.CenterScreen;
                                f19.mDocNo = DocNo;
                                f19.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "20":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblARDownpayment Where VoucherRequestDocNo2=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f20 = new FrmARDownpayment(mMenuCode);
                                f20.Tag = "***";
                                f20.WindowState = FormWindowState.Normal;
                                f20.StartPosition = FormStartPosition.CenterScreen;
                                f20.mDocNo = DocNo;
                                f20.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "21":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblVoucherRequestTax Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f21 = new FrmVoucherRequestTax(mMenuCode);
                                f21.Tag = "***";
                                f21.WindowState = FormWindowState.Normal;
                                f21.StartPosition = FormStartPosition.CenterScreen;
                                f21.mDocNo = DocNo;
                                f21.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "22":
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "23":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblTravelRequestHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f23 = new FrmTravelRequest(mMenuCode);
                                f23.Tag = "***";
                                f23.WindowState = FormWindowState.Normal;
                                f23.StartPosition = FormStartPosition.CenterScreen;
                                f23.mDocNo = DocNo;
                                f23.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "51":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblReturnARDownpayment Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f51 = new FrmReturnARDownpayment(mMenuCode);
                                f51.Tag = "***";
                                f51.WindowState = FormWindowState.Normal;
                                f51.StartPosition = FormStartPosition.CenterScreen;
                                f51.mDocNo = DocNo;
                                f51.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "52":
                            DocNo =
                                Sm.GetValue(
                                "Select DocNo From TblSurvey Where VoucherRequestDocNo=@Param Limit 1;",
                                VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f52 = new FrmSurvey(mMenuCode);
                                f52.Tag = "***";
                                f52.WindowState = FormWindowState.Normal;
                                f52.StartPosition = FormStartPosition.CenterScreen;
                                f52.mDocNo = DocNo;
                                f52.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "53":
                            // Partner Loan Payment
                            Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "54":
                            DocNo =
                              Sm.GetValue(
                              "Select DocNo From TblBonusHdr Where VoucherRequestDocNo=@Param Limit 1;",
                              VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f54 = new FrmBonus2(mMenuCode);
                                f54.Tag = "***";
                                f54.WindowState = FormWindowState.Normal;
                                f54.StartPosition = FormStartPosition.CenterScreen;
                                f54.mDocNo = DocNo;
                                f54.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "55":
                            DocNo =
                              Sm.GetValue(
                              "Select DocNo From TblDroppingPaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                              VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f55 = new FrmDroppingPayment(mMenuCode);
                                f55.Tag = "***";
                                f55.WindowState = FormWindowState.Normal;
                                f55.StartPosition = FormStartPosition.CenterScreen;
                                f55.mDocNo = DocNo;
                                f55.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "56":
                            DocNo = VoucherRequestDocNo;
                            if (DocNo.Length > 0)
                            {
                                var f01 = new FrmVoucherRequest(mMenuCode);
                                f01.Tag = "***";
                                f01.WindowState = FormWindowState.Normal;
                                f01.StartPosition = FormStartPosition.CenterScreen;
                                f01.mDocNo = DocNo;
                                f01.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "58":
                            DocNo =
                              Sm.GetValue(
                              "Select DocNo From TblCashAdvanceSettlementHdr Where VoucherRequestDocNo=@Param Limit 1;",
                              VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f58 = new FrmCashAdvanceSettlement(mMenuCode);
                                f58.Tag = "***";
                                f58.WindowState = FormWindowState.Normal;
                                f58.StartPosition = FormStartPosition.CenterScreen;
                                f58.mDocNo = DocNo;
                                f58.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "61":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblTravelRequestDtl8 Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f61 = new FrmTravelRequest4(mMenuCode);
                                f61.Tag = "***";
                                f61.WindowState = FormWindowState.Normal;
                                f61.StartPosition = FormStartPosition.CenterScreen;
                                f61.mDocNo = DocNo;
                                f61.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "63":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblSOContractDownpaymentHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f61 = new FrmSOContractDownpayment(mMenuCode);
                                f61.Tag = "***";
                                f61.WindowState = FormWindowState.Normal;
                                f61.StartPosition = FormStartPosition.CenterScreen;
                                f61.mDocNo = DocNo;
                                f61.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "64":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblSOContractDownpayment2Hdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f61 = new FrmSOContractDownpayment2(mMenuCode);
                                f61.Tag = "***";
                                f61.WindowState = FormWindowState.Normal;
                                f61.StartPosition = FormStartPosition.CenterScreen;
                                f61.mDocNo = DocNo;
                                f61.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "65":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblVoucherRequestSpecialHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f65 = new FrmVoucherRequestSpecial(Sm.GetParameter("MenuCodeForVRSOT"));
                                f65.Tag = "***";
                                f65.WindowState = FormWindowState.Normal;
                                f65.StartPosition = FormStartPosition.CenterScreen;
                                f65.mDocNo = DocNo;
                                f65.mIsVRSOT = true;
                                f65.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "66":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblVoucherRequestSpecialHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f66 = new FrmVoucherRequestSpecial(Sm.GetParameter("MenuCodeForVRSRHA"));
                                f66.Tag = "***";
                                f66.WindowState = FormWindowState.Normal;
                                f66.StartPosition = FormStartPosition.CenterScreen;
                                f66.mIsVRSRHA = true;
                                f66.mDocNo = DocNo;
                                f66.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                        case "67":
                            DocNo =
                               Sm.GetValue(
                               "Select DocNo From TblVoucherRequestSpecialHdr Where VoucherRequestDocNo=@Param Limit 1;",
                               VoucherRequestDocNo);
                            if (DocNo.Length > 0)
                            {
                                var f67 = new FrmVoucherRequestSpecial(Sm.GetParameter("MenuCodeForVRSIncentive"));
                                f67.Tag = "***";
                                f67.WindowState = FormWindowState.Normal;
                                f67.StartPosition = FormStartPosition.CenterScreen;
                                f67.mIsVRSIncentive = true;
                                f67.mDocNo = DocNo;
                                f67.ShowDialog();
                            }
                            else
                                Sm.StdMsg(mMsgType.Info, "Not available information.");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Sm.StdMsg(mMsgType.Info, "Not available information.");
            }
        }

        #endregion

        #endregion

        #region Class

        private class DocVoucherHdr
        {
            public string DocNo { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public string DocType { get; set; }
            public string AcType { get; set; }
            public string DeptCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class DocVoucherHdrCancel
        {
            public string DocNo { get; set; }
            public string JournalDocNo { get; set; }
            public string JournalDocNo2 { get; set; }
        }

        private class DocVoucherDtl
        {
            public string DocNo { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public string DNo { get; set; }
            public string Description { get; set; }
            public string Remark { get; set; }
            public decimal Amt { get; set; }
        }

        private class VoucherHdr
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string DocType { get; set; }
            public string AcType { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocDt { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string PICName { get; set; }
            public decimal AmtHdr { get; set; }
            public string Terbilang { get; set; }
            public string RemarkHdr { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CurCode { get; set; }
            public decimal DocEnclosure { get; set; }
            public string PaymentUser { get; set; }
            public string Department { get; set; }
            public string BankAcNo { get; set; }
            public string PaidToBankAcName { get; set; }
            public string PaidToBAnkAcNo { get; set; }
            public string PaidToBankBranch { get; set; }
            public string AcNo { get; set; }
            public string SiteName { get; set; }

            // IMS
            public string PaymentUser2 { get; set; }
            public string DocNo2 { get; set; }
            public string DocDt2 { get; set; }
            public string BankName { get; set; }
            public string VoucherDocType { get; set; }
            public string JournalDocNo { get; set; }
            public string CreateBy { get; set; }
        }

        private class VoucherDtl
        {
            public string DNo { get; set; }
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        private class VoucherDtlSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        private class VoucherDtl3
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class VoucherDtl4
        {
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string PosName { get; set; }
            public string LastUpDt { get; set; }
        }

        private class JournalDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public string EntCode { get; set; }
            public string Remark { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
            public string LastUpBy { get; set; }
            public string LastUpDt { get; set; }
        }

        private class PrintJournalDtl
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        #endregion         
    }
}

