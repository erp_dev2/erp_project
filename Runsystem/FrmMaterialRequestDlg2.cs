﻿#region Update
/*
    04/11/2017 [HAR] tambah validasi jika PO qtycancel sdh terbuat maka tidak muncul lagi di list
    31/10/2019 [DITA/IMS] tambah informasi ItCodeInternal dan Specifications
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequestDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequestDlg2(FrmMaterialRequest FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Create By",
                        "Department",
                        "Item's Code",

                        //6-10
                        "Item's Name",
                        "Quantity",
                        "UoM",
                        "Usage Date",
                        "Remark",

                        //11-12
                        "Item's Code"+Environment.NewLine+"Internal",
                        "Specification"

                        
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 80, 150, 150, 100,
                        
                        //6-10
                        200, 100, 80, 80, 300,

                        //11-12
                        100, 300
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5,11 });
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 12 });
            Grd1.Cols[11].Move(6);
            Grd1.Cols[12].Move(10);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, D.CreateBy, G.DeptName, ");
            SQL.AppendLine("E.ItCode, F.ItName, A.Qty, F.PurchaseUomCode, ");
            SQL.AppendLine("E.UsageDt, A.Remark, F.ItCodeInternal, F.Specification ");
            SQL.AppendLine("From TblPOQtyCancel A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment G On D.DeptCode=G.DeptCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.NewMRInd='Y' ");
            SQL.AppendLine("And A.ProcessInd='O' ");
            SQL.AppendLine("And A.DocNo Not In (Select POQtyCancelDocNo From TblMaterialRequestHdr Where POQtyCancelDocno Is not Null) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "F.ItName" });
               
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                             
                            //1-5
                            "DocDt", 
                            "CreateBy", 
                            "DeptName", 
                            "ItCode", 
                            "ItName",  

                            //6-10
                            "Qty", 
                            "PurchaseUomCode", 
                            "UsageDt",
                            "Remark",
                            "ItCodeInternal",

                            //11
                            "Specification"

                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowPOQtyCancelInfo(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
