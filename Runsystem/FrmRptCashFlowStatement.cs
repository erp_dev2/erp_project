﻿#region Update
/*
    29/06/2020 [WED/IMS] new apps
    05/08/2020 [WED/IMS] menghilangkan tahun di label 2 terbawah
    27/11/2020 [IBL/IMS] Ubah filter jd periode. Tambah lop untuk melihat list of voucher
    27/11/2020 [IBL/IMS] Penyesuaian printout
    04/12/2020 [DITA/IMS] Perubahan tampilan reporting kolom terakhir berdasarkan Year to Date (param : IsRptCashFlowStatementYearToDate )
    04/12/2020 [DITA/IMS] Printout disesuaikan berdarakan parameter IsRptCashFlowStatementYearToDate
    12/04/2021 [TKG/PHT] tambah validasi multi profit center
    04/05/2021 [HAR/PHT] Cashflow statement  nilai loop detail tidak sama
    07/06/2021 [TKG/PHT] Mengurutkan yang menggunakan multi profit center berdasarkan parentnya
    13/07/2021 [WED/IMS] bug saat print
    25/08/2021 [RDA/AMKA] tambah kolom budget plan 
    14/10/2021 [ICA/AMKA] menambahkan cashtype2 berdasarkan parameter IsVoucherUseCashType2
    25/10/2021 [VIN/IMS] voucher non IDR * Rate
    29/10/2021 [ISD/AMKA] penyesuaian judul printout
    23/11/2021 [ICA/AMKA] Nyantolin Cashtype2 ke bankaccount2 saat filter by profit center, pindahin query di show data ke setSQL
    24/11/2021 [DITA/PHT] masih ada bug, usercode saat filter by user group belum terdefinisi
    24/11/2021 [DITA/PHT] penyesuaian printout (menggunakan parent)
    21/01/2022 [SET/PHT] Nilai voucher dari transaksi Multi VR type "CASBA" tercatat "0" di List of Voucher Cashflow Statement dengan param IsCashFlowStatementShowCASBADetail
    23/01/2022 [SET/PHT] Menampilkan nilai VC dari transaksi Manual VR type Switching Bank Account di List of Voucher Cashflow Statement dengan param IsCashFlowStatementShowSwitchingBankAccountDetail
    25/01/2022 [SET/PHT] Menampilkan nilai VC dari transaksi Multi VR type Switching Bank Account di List of Voucher Cashflow Statement dengan param IsCashFlowStatementShowCASBADetail
    14/02/2022 [VIN/PHT] bug : mCashFlowStatementOpeningBalance belum diisi dari parameter 
    17/02/2022 [SET/PHT] feedback perhitungan CashFlow Statement belum sesuai
    14/03/2022 [RIS/PHT] Menambah Profit Center pada print out header
    15/03/2022 [BRI/PHT] mengkalkulasi nilai cashflow berdasarkan level dan parent
    21/03/2022 [SET/PHT] bug perhitungan year to date di cash flow statement
    20/04/2022 [DITA/PHT] Feedback untuk tampilan kas setara di printout belum sesuai dengan yg ada di trial balance/GL -> penyebab nya ketika ada perubahan saat show data reporting task : Menampilkan nilai VC dari transaksi Manual VR type Switching Bank Account dan CASBA, printout nya belum diubah
    15/12/2022 [IBL/BBT] Untuk VC type Cash Advance dan Cash Advance Settlement yg ditarik hanya yg BankAcTp nya tidak terdaftar di param BankAccountTypeForVRCA1
                         Berdasarkan parameter IsVoucherCASAllowMultipleBankAccount
    15/12/2022 [HPH/TWC] pada print out tambah parameter IsRptCashFlowStatementUseBudgetPlan untuk hide kolom RKAP dan catatan
    21/12/2022 [VIN/PHT] bug printout
    14/03/2023 [WED/PHT] tambah parameter CashflowStatementMonthlyExchangeCashTypeDescription untuk tambahan informasi jounral Monthly Foreign Exchange
    15/03/2023 [WED/PHT] print out ditambahkan nilai dari Monthly Exchange
    30/03/2023 [RDA/BBT] tambah informasi additional amount berdasarkan parameter mIsVoucherUseAdditionalCost
    06/04/2023 [MYA/PHT] Amount di cashflow statement ketika transaksi non IDR otomatis mengalikan dari currency rate terdekat sampai dengan tanggal transaksi 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptCashFlowStatement : RunSystem.FrmBase6
    {
        #region Field

        private string
            mSQL = string.Empty,
            mRptCashFlowStatementFormat = string.Empty,
            mRptCashFlowStatementFormat2 = string.Empty,
            mFormPrintOutCashFlowStatement = string.Empty,
            mCashflowStatementMonthlyExchangeCashTypeDescription = string.Empty
            ;
        internal bool mIsRptCashFlowStatementYearToDate = false,
            mIsRptCashFlowStatementUseProfitCenter = false,
            mIsAllProfitCenterSelected = false,
            mIsRptCashFlowStatementUseBudgetPlan = false,
            mIsVoucherUseCashType2 = false,
            mIsCashTypeUseParent = false,
            mIsCashFlowStatementShowCASBADetail = false,
            mIsCashFlowStatementShowSwitchingBankAccountDetail = false,
            mIsVoucherCASAllowMultipleBankAccount = false,
            mIsVoucherUseAdditionalCost = false
            ;
        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mThisYear = string.Empty,
            mLastYear = string.Empty,
            mYr = string.Empty,
            mDt1 = string.Empty,
            mDt2 = string.Empty,
            mCashFlowStatementStartingYear= string.Empty,
            mQueryProfitCenter = string.Empty,
            mQueryProfitCenter2 = string.Empty,
            mQueryProfitCenter3 = string.Empty,
            mAcNoForCashFlowStatementOpeningBalance = string.Empty,
            mBankAccountTypeForVRCA1 = string.Empty
            ;
        internal decimal mCashFlowStatementOpeningBalance = 0m;
        internal DateTime mLastYr1, mLastYr2;
        private List<String> mlProfitCenter = null;

        #endregion

        #region Constructor

        public FrmRptCashFlowStatement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                ExecQuery();
                GetParameter();
                SetTimeStampVariable();
                SetGrd();
                SetSQL(string.Empty, string.Empty, string.Empty);
                if (mIsRptCashFlowStatementUseProfitCenter || mIsRptCashFlowStatementUseBudgetPlan)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }

                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    ChkProfitCenterCode.Visible = false;
                    LblMultiEntCode.ForeColor = System.Drawing.Color.Red;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Cash Type",
                    "Cash Group",
                    "Notes",
                    mThisYear,
                    mLastYear,

                    //6-10
                    "Total Label",
                    "Cash Type Code",
                    "Cash Type Group Code",
                    mThisYear,
                    mLastYear,

                    //11-12
                    "",
                    "Budget Plan"
                },
                new int[] 
                {
                    //0
                    50,
                    //1-5
                    300, 250, 100, 0, 0,
                    //6-10
                    0, 0, 0, 200, 200,
                    //11-12
                    20, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5 , 12}, 0);
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10 }, 2);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 5, 6, 7, 8 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12 });
            Grd1.Cols[11].Move(2);
            Grd1.Cols[12].Move(2);

            if (!mIsRptCashFlowStatementUseBudgetPlan)
                Sm.GrdColInvisible(Grd1, new int[] { 12 });
          
            Sm.SetGrdProperty(Grd1, false);
        }

        private void SetSQL(string mQueryProfitCenter, string mQueryProfitCenter2, string mQueryProfitCenter3)
        {
            var SQL = new StringBuilder();

            #region 19/11/2020 Old Code by wed
            //SQL.AppendLine("SELECT T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, SUM(T.ThisYearAmt) ThisYearAmt, SUM(T.LastYearAmt) LastYearAmt, T.TotalLabel FROM ( ");
            //SQL.AppendLine("    SELECT A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, IFNULL(C.Amt, 0.00) ThisYearAmt, 0.00 AS LastYearAmt, B.TotalLabel ");
            //SQL.AppendLine("    FROM TblCashType A ");
            //SQL.AppendLine("    INNER JOIN TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
            //SQL.AppendLine("    LEFT JOIN ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        SELECT CashTypeCode, Amt ");
            //SQL.AppendLine("        FROM TblCashTypeAmount ");
            //SQL.AppendLine("        WHERE Yr = @Yr ");
            //SQL.AppendLine("        AND Mth = @Mth ");
            //SQL.AppendLine("    ) C ON A.CashTypeCode = C.CashTypeCode ");
            //SQL.AppendLine("    UNION ALL ");
            //SQL.AppendLine("    SELECT A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, 0.00 As ThisYearAmt, IFNULL(C.Amt, 0.00) LastYearAmt, B.TotalLabel ");
            //SQL.AppendLine("    FROM TblCashType A ");
            //SQL.AppendLine("    INNER JOIN TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
            //SQL.AppendLine("    LEFT JOIN ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        SELECT CashTypeCode, Amt ");
            //SQL.AppendLine("        FROM TblCashTypeAmount ");
            //SQL.AppendLine("        WHERE Yr = @Yr2 ");
            //SQL.AppendLine("        AND Mth = @Mth ");
            //SQL.AppendLine("    ) C ON A.CashTypeCode = C.CashTypeCode ");
            //SQL.AppendLine(") T Group By T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel; ");
            #endregion

            #region Comment by ICA

            //SQL.AppendLine("Select X.CashTypeCode, X.CAshTypeName, X.CashTypeGrpCode, X.CAshTypeGrpName, X.TotalLabel, ");
            //SQL.AppendLine("Sum(X.ThisYearAmt) ThisYearAmt, Sum(X.LastYearAmt) LastYearAmt, X.Amt ");
            //SQL.AppendLine("From  ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T.AcType, T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel, ");
            //SQL.AppendLine("	 (T.ThisYearAmt + T.ThisYearAmt2) ThisYearAmt, ");
            //SQL.AppendLine("	 (T.LastYearAmt + T.LastYearAmt2) LastYearAmt, T.Amt ");
            //SQL.AppendLine("    From ( ");
            //SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
            //SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
            //SQL.AppendLine("            When 'C' Then -1 ");
            //SQL.AppendLine("            Else 1 ");
            //SQL.AppendLine("            End ");
            //SQL.AppendLine("         As ThisYearAmt, ");
            //if (mIsVoucherUseCashType2)
            //    SQL.AppendLine("0.00 As ThisYearAmt2, ");
            //else
            //{
            //    SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
            //    SQL.AppendLine("             When 'C' Then -1 ");
            //    SQL.AppendLine("             When 'D' Then 1 ");
            //    SQL.AppendLine("             Else 0 ");
            //    SQL.AppendLine("             End ");
            //    SQL.AppendLine("         As ThisYearAmt2, ");
            //}
            //SQL.AppendLine("0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
            //if (mIsRptCashFlowStatementUseBudgetPlan)
            //{
            //    SQL.AppendLine(",D.Amt ");
            //}
            //else
            //{
            //    SQL.AppendLine(", 0.00 As Amt ");
            //}
            //SQL.AppendLine("        From TblCashType A ");
            //SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
            //SQL.AppendLine("        Left Join ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2, Case When B.CurCode != 'IDR' then ifnull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
            //SQL.AppendLine("            From TblCashType A ");
            //SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
            //SQL.AppendLine("                And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
            //SQL.AppendLine("                And B.CancelInd = 'N' ");
            //SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");

            //if (mIsRptCashFlowStatementUseProfitCenter)
            //{
            //    SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            //    SQL.AppendLine(mQueryProfitCenter);
            //}
            //if (mIsVoucherUseCashType2)
            //{
            //    SQL.AppendLine("Union All ");
            //    SQL.AppendLine("Select A.CashTypeCode, B.AcType2 as AcType, B.AcType2, B.Amt ");
            //    SQL.AppendLine("From TblCashType A ");
            //    SQL.AppendLine("Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode2 ");
            //    SQL.AppendLine("And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
            //    SQL.AppendLine("And B.CancelInd = 'N' ");
            //    if (mIsRptCashFlowStatementUseProfitCenter)
            //    {
            //        SQL.AppendLine("Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            //        SQL.AppendLine(mQueryProfitCenter);
            //    }
            //}
            //SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
            //if (mIsRptCashFlowStatementUseBudgetPlan)
            //{
            //    SQL.AppendLine("Left Join ( ");
            //    SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
            //    SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
            //    SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
            //    SQL.AppendLine("    Where A.Yr = @Yr ");
            //    SQL.AppendLine(mQueryProfitCenter2);
            //    SQL.AppendLine("    Group By B.CashTypeCode ");
            //    SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
            //}
            //SQL.AppendLine("        Union All ");

            //SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
            //SQL.AppendLine("        0.00 As ThisYearAmt, 0.00 As ThisYearAmt2, ");
            //SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
            //SQL.AppendLine("            When 'C' Then -1 ");
            //SQL.AppendLine("            Else 1 ");
            //SQL.AppendLine("            End ");
            //SQL.AppendLine("         As LastYearAmt, ");
            //if (mIsVoucherUseCashType2)
            //    SQL.AppendLine("0.00 as LastYearAmt2 ");
            //else
            //{
            //    SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
            //    SQL.AppendLine("             When 'C' Then -1 ");
            //    SQL.AppendLine("             When 'D' Then 1 ");
            //    SQL.AppendLine("             Else 0 ");
            //    SQL.AppendLine("             End ");
            //    SQL.AppendLine("         As LastYearAmt2 ");
            //}
            //if (mIsRptCashFlowStatementUseBudgetPlan)
            //{
            //    SQL.AppendLine(",IfNull(D.Amt, 0.00) As Amt ");
            //}
            //else
            //{
            //    SQL.AppendLine(", 0.00 As Amt ");
            //}
            //SQL.AppendLine("        From TblCashType A ");
            //SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
            //SQL.AppendLine("        Left Join ");
            //SQL.AppendLine("        ( ");
            //SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2, Case When B.CurCode != 'IDR' then ifnull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
            //SQL.AppendLine("            From TblCashType A ");
            //SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
            //if (mIsRptCashFlowStatementYearToDate)
            //    SQL.AppendLine("            And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
            //else
            //    SQL.AppendLine("            And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2) ");
            //SQL.AppendLine("                And B.CancelInd = 'N' ");
            //SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");

            //if (mIsRptCashFlowStatementUseProfitCenter)
            //{
            //    SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            //    SQL.AppendLine(mQueryProfitCenter);
            //}
            //if (mIsVoucherUseCashType2)
            //{
            //    SQL.AppendLine("Union All ");
            //    SQL.AppendLine("Select A.CashTypeCode, B.AcType2 As AcType, B.AcType2, Case When B.CurCode != 'IDR' then ifnull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
            //    SQL.AppendLine("From TblCashType A ");
            //    SQL.AppendLine("Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode2 ");
            //    if (mIsRptCashFlowStatementYearToDate)
            //        SQL.AppendLine("    And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
            //    else
            //        SQL.AppendLine("    And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2) ");
            //    SQL.AppendLine("        And B.CancelInd = 'N' ");
            //    SQL.AppendLine("        And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");

            //    if (mIsRptCashFlowStatementUseProfitCenter)
            //    {
            //        SQL.AppendLine("Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
            //        SQL.AppendLine(mQueryProfitCenter);
            //    }
            //}
            //SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
            //if (mIsRptCashFlowStatementUseBudgetPlan)
            //{
            //    SQL.AppendLine("Left Join ( ");
            //    SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
            //    SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
            //    SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
            //    SQL.AppendLine("    Where A.Yr = @Yr ");
            //    SQL.AppendLine(mQueryProfitCenter2);
            //    SQL.AppendLine("    Group By B.CashTypeCode ");
            //    SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
            //}
            //SQL.AppendLine("    ) T ");
            //SQL.AppendLine(") X ");
            //SQL.AppendLine("Group By X.CashTypeCode, X.CashTypeName, X.CashTypeGrpCode, X.CAshTypeGrpName, X.TotalLabel; ");

            #endregion

            SQL.AppendLine("Select X.CashTypeCode, X.CAshTypeName, X.CashTypeGrpCode, X.CAshTypeGrpName, X.TotalLabel, ");
            SQL.AppendLine("Sum(X.ThisYearAmt) ThisYearAmt, Sum(X.LastYearAmt) LastYearAmt, X.Amt ");
            SQL.AppendLine("From  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T.AcType, T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel, ");
            SQL.AppendLine("	 (T.ThisYearAmt + T.ThisYearAmt2) ThisYearAmt, ");
            SQL.AppendLine("	 (T.LastYearAmt + T.LastYearAmt2) LastYearAmt, T.Amt ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
            SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
            SQL.AppendLine("            When 'C' Then -1 ");
            SQL.AppendLine("            Else 1 ");
            SQL.AppendLine("            End ");
            SQL.AppendLine("         As ThisYearAmt, ");
            if (mIsVoucherUseCashType2)
                SQL.AppendLine("        0.00 as ThisYearAmt2, ");
            else
            {
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                SQL.AppendLine("             When 'C' Then -1 ");
                SQL.AppendLine("             When 'D' Then 1 ");
                SQL.AppendLine("             Else 0 ");
                SQL.AppendLine("             End ");
                SQL.AppendLine("         As ThisYearAmt2, ");
            }
            SQL.AppendLine("0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
            if (mIsRptCashFlowStatementUseBudgetPlan)
            {
                SQL.AppendLine(",D.Amt ");
            }
            else
            {
                SQL.AppendLine(", 0.00 As Amt ");
            }
            SQL.AppendLine("        From TblCashType A ");
            SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  ");
            SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
            SQL.AppendLine("            Select Amt  ");
            SQL.AppendLine("            From tblcurrencyrate  ");
            SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
            SQL.AppendLine("            Else B.Amt End As Amt  ");
            SQL.AppendLine("            From TblCashType A ");
            SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
            SQL.AppendLine("                And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
            SQL.AppendLine("                And B.CancelInd = 'N' ");
            SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");
            if(mIsCashFlowStatementShowSwitchingBankAccountDetail)
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '16') ");
            if(mIsCashFlowStatementShowCASBADetail)
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '70') ");
            if(mIsVoucherCASAllowMultipleBankAccount)
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType In('56','58')) ");

            if (mIsRptCashFlowStatementUseProfitCenter)
            {
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                if(mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                SQL.AppendLine(mQueryProfitCenter);
            }
            if (mIsVoucherUseCashType2)
            {
                SQL.AppendLine("            Union All ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType2 As AcType, B.AcType2,  ");
                SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
                SQL.AppendLine("            Select Amt  ");
                SQL.AppendLine("            From tblcurrencyrate  ");
                SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
                SQL.AppendLine("            Else B.Amt End As Amt  ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode2 ");
                SQL.AppendLine("                And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
                SQL.AppendLine("                And B.CancelInd = 'N' ");
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");
                if (mIsVoucherCASAllowMultipleBankAccount)
                    SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType In('56','58')) ");

                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode2=C.BankAcCode ");
                    SQL.AppendLine(mQueryProfitCenter);
                }
            }
            SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
            if (mIsRptCashFlowStatementUseBudgetPlan)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
                SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
                SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where A.Yr = @Yr ");
                SQL.AppendLine(mQueryProfitCenter2);
                SQL.AppendLine("    Group By B.CashTypeCode ");
                SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
            }
            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
            SQL.AppendLine("        0.00 As ThisYearAmt, 0.00 As ThisYearAmt2, ");
            SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
            SQL.AppendLine("            When 'C' Then -1 ");
            SQL.AppendLine("            Else 1 ");
            SQL.AppendLine("            End ");
            SQL.AppendLine("         As LastYearAmt, ");
            if (mIsVoucherUseCashType2)
                SQL.AppendLine("        0.00 as LastYearAmt2 ");
            else
            {
                SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                SQL.AppendLine("             When 'C' Then -1 ");
                SQL.AppendLine("             When 'D' Then 1 ");
                SQL.AppendLine("             Else 0 ");
                SQL.AppendLine("             End ");
                SQL.AppendLine("         As LastYearAmt2 ");
            }
            if (mIsRptCashFlowStatementUseBudgetPlan)
            {
                SQL.AppendLine(",IfNull(D.Amt, 0.00) As Amt ");
            }
            else
            {
                SQL.AppendLine(", 0.00 As Amt ");
            }
            SQL.AppendLine("        From TblCashType A ");
            SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
            SQL.AppendLine("        Left Join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  ");
            SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
            SQL.AppendLine("            Select Amt  ");
            SQL.AppendLine("            From tblcurrencyrate  ");
            SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
            SQL.AppendLine("            Else B.Amt End As Amt  ");
            SQL.AppendLine("            From TblCashType A ");
            SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
            if (mIsRptCashFlowStatementYearToDate)
                SQL.AppendLine("            And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
            else
                SQL.AppendLine("            And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2) ");
            SQL.AppendLine("                And B.CancelInd = 'N' ");
            SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");
            if (mIsVoucherCASAllowMultipleBankAccount)
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType In('56','58')) ");
            if (mIsRptCashFlowStatementUseProfitCenter)
            {
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                SQL.AppendLine(mQueryProfitCenter);
            }
            if (mIsVoucherUseCashType2)
            {
                SQL.AppendLine("            Union All ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  ");
                SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
                SQL.AppendLine("            Select Amt  ");
                SQL.AppendLine("            From tblcurrencyrate  ");
                SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
                SQL.AppendLine("            Else B.Amt End As Amt  ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode2 ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
                else
                    SQL.AppendLine("            And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2) ");
                SQL.AppendLine("                And B.CancelInd = 'N' ");
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");
                if (mIsVoucherCASAllowMultipleBankAccount)
                    SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType In('56','58')) ");

                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode2=C.BankAcCode ");
                    SQL.AppendLine(mQueryProfitCenter);
                }
            }
            SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
            if (mIsRptCashFlowStatementUseBudgetPlan)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
                SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
                SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Where A.Yr = @Yr ");
                SQL.AppendLine(mQueryProfitCenter2);
                SQL.AppendLine("    Group By B.CashTypeCode ");
                SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
            }
            SQL.AppendLine("    ) T ");

            //PHT
            if(mIsCashFlowStatementShowSwitchingBankAccountDetail || mIsCashFlowStatementShowCASBADetail)
            {
                #region ngebug SET
                /*SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("    Select T.AcType, T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel, ");
                SQL.AppendLine("    T.ThisYearAmt ThisYearAmt, T.LastYearAmt LastYearAmt, T.Amt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, C.AcType2,  ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                SQL.AppendLine("        When 'C' Then -1 ");
                SQL.AppendLine("        When 'D' Then 1 ");
                SQL.AppendLine("        Else 0 END AS ThisYearAmt, ");
                SQL.AppendLine("        0.00 LastYearAmt, 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  Case When B.CurCode != 'IDR' then ifnull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                SQL.AppendLine("            And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("            AND ");
                if (mIsCashFlowStatementShowSwitchingBankAccountDetail && mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("        (B.DocType = '16' OR B.DocType = '70') ");
                else if (mIsCashFlowStatementShowSwitchingBankAccountDetail)
                    SQL.AppendLine("        B.DocType = '16' ");
                else if (mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("        B.DocType = '70' ");
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("            LEFT Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                    if (mIsRptCashFlowStatementYearToDate)
                        SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                    SQL.AppendLine(mQueryProfitCenter);
                    SQL.AppendLine("            INNER Join TblBankAccount D On B.BankAcCode2=D.BankAcCode ");
                    if (mIsRptCashFlowStatementYearToDate)
                        SQL.AppendLine("            AND D.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                    SQL.AppendLine(mQueryProfitCenter3);
                }
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                SQL.AppendLine("    UNION ALL ");
                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, C.AcType2,  ");
                SQL.AppendLine("        0.00 AS ThisYearAmt, ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                SQL.AppendLine("        When 'C' Then -1 ");
                SQL.AppendLine("        When 'D' Then 1 ");
                SQL.AppendLine("        Else 0 End As LastYearAmt, 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  Case When B.CurCode != 'IDR' then ifnull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                SQL.AppendLine("            And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("            AND ");
                if (mIsCashFlowStatementShowSwitchingBankAccountDetail && mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("        (B.DocType = '16' OR B.DocType = '70') ");
                else if (mIsCashFlowStatementShowSwitchingBankAccountDetail)
                    SQL.AppendLine("        B.DocType = '16' ");
                else if (mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("        B.DocType = '70' ");
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("            LEFT Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                    if (mIsRptCashFlowStatementYearToDate)
                        SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                    SQL.AppendLine(mQueryProfitCenter);
                    SQL.AppendLine("            INNER Join TblBankAccount D On B.BankAcCode2=D.BankAcCode ");
                    if (mIsRptCashFlowStatementYearToDate)
                        SQL.AppendLine("            AND D.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                    SQL.AppendLine(mQueryProfitCenter3);
                }
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                SQL.AppendLine("    ) T ");*/
                #endregion
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("    Select T.AcType, T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel, ");
                SQL.AppendLine("    (T.ThisYearAmt + T.ThisYearAmt2) ThisYearAmt, ");
                SQL.AppendLine("    (T.LastYearAmt + T.LastYearAmt2) LastYearAmt, T.Amt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("        When 'C' Then -1 ");
                SQL.AppendLine("        Else 1 ");
                SQL.AppendLine("        End As ThisYearAmt, ");
                SQL.AppendLine("        0.00 As ThisYearAmt2, 0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine(",IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine(", 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  ");
                SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
                SQL.AppendLine("            Select Amt  ");
                SQL.AppendLine("            From tblcurrencyrate  ");
                SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
                SQL.AppendLine("            Else B.Amt End As Amt  ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                SQL.AppendLine("            And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("            AND ");
                if (mIsCashFlowStatementShowSwitchingBankAccountDetail && mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            (B.DocType = '16' OR B.DocType = '70') ");
                else if (mIsCashFlowStatementShowSwitchingBankAccountDetail)
                    SQL.AppendLine("            B.DocType = '16' ");
                else if (mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            B.DocType = '70' ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                SQL.AppendLine(mQueryProfitCenter);
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
                    SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
                    SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("    Group By B.CashTypeCode ");
                    SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
                }
                SQL.AppendLine("        UNION ALL ");
                SQL.AppendLine("        SELECT A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("        0.00 As ThisYearAmt, ");
                SQL.AppendLine("        IFNULL(C.Amt, 0.00) * case C.AcType2 ");
                SQL.AppendLine("        when 'C' then -1 ");
                SQL.AppendLine("        when 'D' then 1 ");
                SQL.AppendLine("        ELSE 0 END As ThisYearAmt2, ");
                SQL.AppendLine("        0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine(",IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine(", 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("        Select A.CashTypeCode, B.AcType, B.AcType2,  ");
                SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
                SQL.AppendLine("            Select Amt  ");
                SQL.AppendLine("            From tblcurrencyrate  ");
                SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
                SQL.AppendLine("            Else B.Amt End As Amt  "); 
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                SQL.AppendLine("        And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
                SQL.AppendLine("        And B.CancelInd = 'N' ");
                SQL.AppendLine("            AND ");
                if (mIsCashFlowStatementShowSwitchingBankAccountDetail && mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            (B.DocType = '16' OR B.DocType = '70') ");
                else if (mIsCashFlowStatementShowSwitchingBankAccountDetail)
                    SQL.AppendLine("            B.DocType = '16' ");
                else if (mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            B.DocType = '70' ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode2=C.BankAcCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                SQL.AppendLine(mQueryProfitCenter);
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
                    SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
                    SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("    Group By B.CashTypeCode ");
                    SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
                }
                SQL.AppendLine("        UNION ALL ");
                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("        0.00 As ThisYearAmt, 0.00 As ThisYearAmt2, ");
                SQL.AppendLine("        IFNULL(C.Amt, 0.00) * case C.AcType ");
                SQL.AppendLine("        when 'C' then -1 ");
                SQL.AppendLine("        ELSE 1 END As LastYearAmt, ");
                SQL.AppendLine("        0.00  As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine(",IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine(", 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  ");
                SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
                SQL.AppendLine("            Select Amt  ");
                SQL.AppendLine("            From tblcurrencyrate  ");
                SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
                SQL.AppendLine("            Else B.Amt End As Amt  ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
                else
                    SQL.AppendLine("            And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("            AND ");
                if (mIsCashFlowStatementShowSwitchingBankAccountDetail && mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            (B.DocType = '16' OR B.DocType = '70') ");
                else if (mIsCashFlowStatementShowSwitchingBankAccountDetail)
                    SQL.AppendLine("            B.DocType = '16' ");
                else if (mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            B.DocType = '70' ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                SQL.AppendLine(mQueryProfitCenter);
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
                    SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
                    SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("    Group By B.CashTypeCode ");
                    SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
                }
                SQL.AppendLine("        UNION ALL ");
                SQL.AppendLine("        SELECT A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("        0.00 As ThisYearAmt, 0.00 As ThisYearAmt2, 0.00 As LastYearAmt, ");
                SQL.AppendLine("        IFNULL(C.Amt, 0.00) * case C.AcType2 ");
                SQL.AppendLine("        when 'C' then -1 ");
                SQL.AppendLine("        when 'D' then 1 ");
                SQL.AppendLine("        ELSE 0 END As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine(",IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine(", 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  ");
                SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
                SQL.AppendLine("            Select Amt  ");
                SQL.AppendLine("            From tblcurrencyrate  ");
                SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
                SQL.AppendLine("            Else B.Amt End As Amt  "); 
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
                else
                    SQL.AppendLine("            And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("            AND ");
                if (mIsCashFlowStatementShowSwitchingBankAccountDetail && mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            (B.DocType = '16' OR B.DocType = '70') ");
                else if (mIsCashFlowStatementShowSwitchingBankAccountDetail)
                    SQL.AppendLine("            B.DocType = '16' ");
                else if (mIsCashFlowStatementShowCASBADetail)
                    SQL.AppendLine("            B.DocType = '70' ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode2=C.BankAcCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("            AND C.COAAcNo LIKE @AcNoForCashFlowStatementOpeningBalance ");
                SQL.AppendLine(mQueryProfitCenter);
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) 'Amt' ");
                    SQL.AppendLine("    From tblbudgetplancashflowhdr A ");
                    SQL.AppendLine("    Inner Join tblbudgetplancashflowdtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("    Group By B.CashTypeCode ");
                    SQL.AppendLine(")D on A.CashTypeCode = D.CashTypeCode ");
                }
                SQL.AppendLine("    ) T ");
            }

            //BBT
            if (mIsVoucherCASAllowMultipleBankAccount)
            {
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("    Select T.AcType, T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel, ");
                SQL.AppendLine("    (T.ThisYearAmt + T.ThisYearAmt2) ThisYearAmt, ");
                SQL.AppendLine("    (T.LastYearAmt + T.LastYearAmt2) LastYearAmt, T.Amt ");
                SQL.AppendLine("    From ( ");
                #region This Year Amt
                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("        When 'C' Then -1 ");
                SQL.AppendLine("        When 'D' Then 1 ");
                SQL.AppendLine("        Else 0 ");
                SQL.AppendLine("        End As ThisYearAmt, 0.00 As ThisYearAmt2, 0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine("    , IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine("    , 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B On A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, Null As AcType2, Case When B.CurCode != 'IDR' Then IfNull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                SQL.AppendLine("                And B.DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                SQL.AppendLine("                And B.CancelInd = 'N' ");
                SQL.AppendLine("                And B.DocType In ('56','58') ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode ");
                SQL.AppendLine("                And Not Find_In_set(C.BankAcTp, @BankAccountTypeForVRCA1) ");
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    if (mIsRptCashFlowStatementYearToDate)
                    {
                        SQL.AppendLine("                And C.COAAcNo Like @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter);
                    }
                }
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) As Amt ");
                    SQL.AppendLine("            From TblBudgetPlanCashFlowHdr A ");
                    SQL.AppendLine("            Inner Join TblBudgetPlanCashFlowDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("            Group By B.CashTypeCode ");
                    SQL.AppendLine("        )D On A.CashTypeCode = D.CashTypeCode ");
                }

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("        When 'C' Then -1 ");
                SQL.AppendLine("        When 'D' Then 1 ");
                SQL.AppendLine("        Else 0 ");
                SQL.AppendLine("        End As ThisYearAmt, 0.00 As ThisYearAmt2, 0.00 As LastYearAmt, 0.00 As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine("    , IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine("    , 0.00 As Amt ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B On A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType2 As AcType, Null As AcType2, Case When B.CurCode != 'IDR' Then IfNull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                SQL.AppendLine("                And B.DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                SQL.AppendLine("                And B.CancelInd = 'N' ");
                SQL.AppendLine("                And B.DocType In ('56','58') ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode2 = C.BankAcCode ");
                SQL.AppendLine("                And Not Find_In_set(C.BankAcTp, @BankAccountTypeForVRCA1) ");
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    if (mIsRptCashFlowStatementYearToDate)
                    {
                        SQL.AppendLine("                And C.COAAcNo Like @AcNoForCashFlowStatementOpeningBalance ");
                        SQL.AppendLine(mQueryProfitCenter);
                    }
                }
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) As Amt ");
                    SQL.AppendLine("            From TblBudgetPlanCashFlowHdr A ");
                    SQL.AppendLine("            Inner Join TblBudgetPlanCashFlowDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("            Group By B.CashTypeCode ");
                    SQL.AppendLine("        )D On A.CashTypeCode = D.CashTypeCode ");
                }

                SQL.AppendLine("        Union All ");
                #endregion

                #region Last Year Amt
                SQL.AppendLine("    	Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("    	0.00 As ThisYearAmt, 0.00 As ThisYearAmt2, ");
                SQL.AppendLine("    	IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("    	When 'C' Then -1 ");
                SQL.AppendLine("    	When 'D' Then 1 ");
                SQL.AppendLine("    	Else 0 ");
                SQL.AppendLine("    	End As LastYearAmt, 0.00 As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine("    , IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine("    , 0.00 As Amt ");
                SQL.AppendLine("    	From TblCashType A ");
                SQL.AppendLine("    	Inner Join TblCashTypeGroup B On A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("    	Left Join ( ");
                SQL.AppendLine("    		Select A.CashTypeCode, B.AcType, Null As AcType2, Case When B.CurCode != 'IDR' Then IfNull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
                SQL.AppendLine("    		From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("                And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
                else
                    SQL.AppendLine("    		    And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2)");
                SQL.AppendLine("    		    And B.CancelInd = 'N' ");
                SQL.AppendLine("    		    And B.DocType In ('56','58') ");
                SQL.AppendLine("    		Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode ");
                SQL.AppendLine("    		    And Not Find_In_set(C.BankAcTp, @BankAccountTypeForVRCA1) ");
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("            And C.COAAcNo Like @AcNoForCashFlowStatementOpeningBalance ");
                    SQL.AppendLine(mQueryProfitCenter);
                }
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) As Amt ");
                    SQL.AppendLine("            From TblBudgetPlanCashFlowHdr A ");
                    SQL.AppendLine("            Inner Join TblBudgetPlanCashFlowDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("            Group By B.CashTypeCode ");
                    SQL.AppendLine("        )D On A.CashTypeCode = D.CashTypeCode ");
                }
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("    	Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType, ");
                SQL.AppendLine("    	0.00 As ThisYearAmt, 0.00 As ThisYearAmt2, ");
                SQL.AppendLine("    	IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("    	When 'C' Then -1 ");
                SQL.AppendLine("    	When 'D' Then 1 ");
                SQL.AppendLine("    	Else 0 ");
                SQL.AppendLine("    	End As LastYearAmt, 0.00 As LastYearAmt2 ");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                    SQL.AppendLine("    , IfNull(D.Amt, 0.00) As Amt ");
                else
                    SQL.AppendLine("    , 0.00 As Amt ");
                SQL.AppendLine("    	From TblCashType A ");
                SQL.AppendLine("    	Inner Join TblCashTypeGroup B On A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("    	Left Join ( ");
                SQL.AppendLine("    		Select A.CashTypeCode, B.AcType2 As AcType, Null As AcType2, Case When B.CurCode != 'IDR' Then IfNull(B.ExcRate,1)*B.Amt Else B.Amt End As Amt ");
                SQL.AppendLine("    		From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                if (mIsRptCashFlowStatementYearToDate)
                    SQL.AppendLine("    		    And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2) ");
                else
                    SQL.AppendLine("    		    And B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2)");
                SQL.AppendLine("    		    And B.CancelInd = 'N' ");
                SQL.AppendLine("    		    And B.DocType In ('56','58') ");
                SQL.AppendLine("    		Inner Join TblBankAccount C On B.BankAcCode2 = C.BankAcCode ");
                SQL.AppendLine("    		    And Not Find_In_set(C.BankAcTp, @BankAccountTypeForVRCA1) ");
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("            And C.COAAcNo Like @AcNoForCashFlowStatementOpeningBalance ");
                    SQL.AppendLine(mQueryProfitCenter);
                }
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode");
                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select A.ProfitCenterCode,  B.CashTypeCode, Sum(B.Amt) As Amt ");
                    SQL.AppendLine("            From TblBudgetPlanCashFlowHdr A ");
                    SQL.AppendLine("            Inner Join TblBudgetPlanCashFlowDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            Where A.Yr = @Yr ");
                    SQL.AppendLine(mQueryProfitCenter2);
                    SQL.AppendLine("            Group By B.CashTypeCode ");
                    SQL.AppendLine("        )D On A.CashTypeCode = D.CashTypeCode ");
                }
                #endregion
                SQL.AppendLine("    ) T ");
            }

            if (mIsVoucherUseAdditionalCost)
            {
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("   Select T.AcType, T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel,  ");
                SQL.AppendLine("	 IfNull(T.ThisYearAmt, 0.00) ThisYearAmt,  ");
                SQL.AppendLine("	 (T.LastYearAmt) LastYearAmt, T.Amt  ");
                SQL.AppendLine("    From (  ");
                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType,  ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) As ThisYearAmt, 0.00 As LastYearAmt, 0.00 As Amt  ");
                SQL.AppendLine("        From TblCashType A  ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode  ");
                SQL.AppendLine("        Left Join  ");
                SQL.AppendLine("        (  ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2,  ");
                SQL.AppendLine("            Case When B.CurCode != '@MainCurCode' then IFNULL(( ");
                SQL.AppendLine("            Select Amt  ");
                SQL.AppendLine("            From tblcurrencyrate  ");
                SQL.AppendLine("            Where RateDt<=B.DocDt And CurCode1=B.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc LIMIT 1),1)*B.Amt  ");
                SQL.AppendLine("            Else B.Amt End As Amt  ");
                SQL.AppendLine("            From TblCashType A  ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode  ");
                SQL.AppendLine("                And B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2)  ");
                SQL.AppendLine("                And B.CancelInd = 'N'  ");
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null)  ");
                SQL.AppendLine("            INNER JOIN tblvoucherdtl4 C ON B.DocNo=C.DocNo ");
                SQL.AppendLine("            INNER JOIN tblexpensestypedtl D ON C.ExpensesCode=D.ExpensesCode AND C.ExpensesDNo=D.DNo ");
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode  ");
                SQL.AppendLine("        Union All  ");
                SQL.AppendLine("        Select A.CashTypeCode, A.CashTypeName, A.CashTypeGrpCode, B.CashTypeGrpName, B.TotalLabel, C.AcType,  ");
                SQL.AppendLine("        0.00 As ThisYearAmt, IfNull(C.Amt, 0.00)  As LastYearAmt, 0.00 As Amt  ");
                SQL.AppendLine("        From TblCashType A  ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode  ");
                SQL.AppendLine("        Left Join  ");
                SQL.AppendLine("        (  ");
                SQL.AppendLine("            Select D.CashTypeCode, B.AcType, B.AcType2, Case When B.CurCode != 'IDR' then ifnull(B.ExcRate,1)*D.Amt Else D.Amt End As Amt  ");
                SQL.AppendLine("            From TblCashType A  ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode  ");
                SQL.AppendLine("            And B.DocDt Between Concat(@Yr,'0101') And Concat(@Yr,@Dt2)  ");
                SQL.AppendLine("                And B.CancelInd = 'N'  ");
                SQL.AppendLine("                And B.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null)  ");
                SQL.AppendLine("            INNER JOIN tblvoucherdtl4 C ON B.DocNo=C.DocNo ");
                SQL.AppendLine("            INNER JOIN tblexpensestypedtl D ON C.ExpensesCode=D.ExpensesCode AND C.ExpensesDNo=D.DNo ");
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode  ");
                SQL.AppendLine("    ) T  ");
            }
            SQL.AppendLine(") X ");
            SQL.AppendLine("Group By X.CashTypeCode, X.CashTypeName, X.CashTypeGrpCode, X.CAshTypeGrpName, X.TotalLabel ");

            if (mCashflowStatementMonthlyExchangeCashTypeDescription.Length > 0)
            {
                SQL.AppendLine("Union All ");

                SQL.AppendLine("Select T.CashTypeCode, T.CashTypeName, T.CashTypeGrpCode, T.CashTypeGrpName, T.TotalLabel, ");
                SQL.AppendLine("If(T.DAmt != 0, T.DAmt, If(T.CAmt != 0, If(T.CAmt < 0, T.CAmt, T.CAmt * -1), 0)) ThisYearAmt, ");
                SQL.AppendLine("If(T.DAmt != 0, T.DAmt, If(T.CAmt != 0, If(T.CAmt < 0, T.CAmt, T.CAmt * -1), 0)) LastYearAmt, 0.00 As Amt ");
                SQL.AppendLine("From( ");
                SQL.AppendLine("    Select '99FX' As CashTypeCode, A.ParValue As CashTypeName, Null As CashTypeGrpCode, Null As CashTypeGrpName, Null As TotalLabel, ");
                SQL.AppendLine("    Sum(C.DAmt) DAmt, Sum(C.CAmt) CAmt ");
                SQL.AppendLine("    From TblParameter A ");
                SQL.AppendLine("    Inner Join TblJournalHdr B On B.MenuCode In(Select MenuCode From TblMenu Where Param = 'FrmMonthlyFXJournalEntries') ");
                SQL.AppendLine("        And B.DocDt Between Concat(@Yr, @Dt1) And Concat(@Yr, @Dt2) ");
                SQL.AppendLine("    Inner Join TblJournalDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("        And C.AcNo Not In(Select ParValue From TblParameter Where ParCode In('AcNoForForeignCurrencyExchangeExpense', 'AcNoForForeignCurrencyExchangeGains') And Parvalue Is Not Null) ");
                SQL.AppendLine("    Inner Join( ");
                SQL.AppendLine("        Select Distinct JournalDocNo ");
                SQL.AppendLine("         From TblClosingBalanceInCashHdr ");
                SQL.AppendLine("         Where Yr = @Yr And Mth Between Left(@Dt1, 2) And Left(@Dt2, 2) ");
                SQL.AppendLine("        And JournalDocNo Is Not Null ");
                SQL.AppendLine("        And CancelInd = 'N' ");
                SQL.AppendLine("    ) D On B.DocNo = D.JournalDocNo ");
                SQL.AppendLine("    Where A.ParCode = 'CashflowStatementMonthlyExchangeCashTypeDescription' And A.ParValue Is Not Null ");
                SQL.AppendLine(") T ");
            }

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                IsDocDateInvalid() ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) ||
                IsProfitCenterEmpty()
                ) return;

            var SQL = new StringBuilder();
            mQueryProfitCenter = string.Empty;
            mQueryProfitCenter2 = string.Empty;
            mQueryProfitCenter3 = string.Empty;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Sm.ClearGrd(Grd1, false);
                SetTimeStampVariable();
                SetGrd();
                SetProfitCenter();

                var cm = new MySqlCommand();
                
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    mQueryProfitCenter = "    And C.SiteCode Is Not Null ";
                    mQueryProfitCenter += "    And C.SiteCode In ( ";
                    mQueryProfitCenter += "        Select Distinct SiteCode ";
                    mQueryProfitCenter += "        From TblSite ";
                    mQueryProfitCenter += "        Where ProfitCenterCode Is Not Null ";

                    mQueryProfitCenter3 = "    And D.SiteCode Is Not Null ";
                    mQueryProfitCenter3 += "    And D.SiteCode In ( ";
                    mQueryProfitCenter3 += "        Select Distinct SiteCode ";
                    mQueryProfitCenter3 += "        From TblSite ";
                    mQueryProfitCenter3 += "        Where ProfitCenterCode Is Not Null ";

                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter2 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                        {
                            mQueryProfitCenter += "    And 1=0 ";
                            mQueryProfitCenter3 += "    And 1=0 ";
                        }
                        else
                        {
                            mQueryProfitCenter += "    And (" + Filter2 + ") ";
                            mQueryProfitCenter3 += "    And (" + Filter2 + ") ";
                        }
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            mQueryProfitCenter += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            mQueryProfitCenter3 += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            mQueryProfitCenter += "    And ProfitCenterCode In ( ";
                            mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter += "    ) ";

                            mQueryProfitCenter3 += "    And ProfitCenterCode In ( ";
                            mQueryProfitCenter3 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            mQueryProfitCenter3 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter3 += "    ) ";
                        }
                    }

                    mQueryProfitCenter += "    ) ";
                    mQueryProfitCenter3 += "    ) ";

                    if (!ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter = "    And (C.SiteCode Is Null Or (C.SiteCode Is Not Null And C.SiteCode In ( ";
                        mQueryProfitCenter += "        Select Distinct SiteCode From TblGroupSite ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "        ))) ";

                        mQueryProfitCenter3 = "    And (D.SiteCode Is Null Or (D.SiteCode Is Not Null And D.SiteCode In ( ";
                        mQueryProfitCenter3 += "        Select Distinct SiteCode From TblGroupSite ";
                        mQueryProfitCenter3 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter3 += "        ))) ";
                    }
                }

                if (mIsRptCashFlowStatementUseBudgetPlan)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter3 = string.Empty;
                        int i = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter3.Length > 0) Filter3 += " Or ";
                            Filter3 += " (A.ProfitCenterCode=@ProfitCenter2_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenter2_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter3.Length == 0)
                        {
                            mQueryProfitCenter2 += "    And 1=0 ";
                        }
                        else
                        {
                            mQueryProfitCenter2 += "    And (" + Filter3 + ") ";
                        }
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            mQueryProfitCenter2 += "    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode2) ";
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            mQueryProfitCenter2 += "    And ProfitCenterCode In ( ";
                            mQueryProfitCenter2 += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                            mQueryProfitCenter2 += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                            mQueryProfitCenter2 += "    ) ";
                        }
                    }
                }

                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@Dt1", mDt1);
                Sm.CmParam<String>(ref cm, "@Dt2", mDt2);
                Sm.CmParam<String>(ref cm, "@StartingYear", mCashFlowStatementStartingYear);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@AcNoForCashFlowStatementOpeningBalance", mAcNoForCashFlowStatementOpeningBalance + "%");
                Sm.CmParam<String>(ref cm, "@BankAccountTypeForVRCA1", mBankAccountTypeForVRCA1);
                Sm.CmParam<String>(ref cm, "@MainCurCode", Sm.GetParameter("MainCurCode"));

                SetSQL(mQueryProfitCenter, mQueryProfitCenter2, mQueryProfitCenter3);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL,
                    new string[]
                    {
                        //0
                        "CashTypeName",  
                        //1-5
                        "CashTypeGrpName", "ThisYearAmt", "LastYearAmt", "TotalLabel", "CashTypeCode", 
                        //6-7
                        "CashTypeGrpCode", "Amt"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Grd.Cells[Row, 3].Value = Row + 1;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                        
                    }, true, false, false, false
                );

                ProcessNegativeBalance();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }

            ParPrint();
        }

        private bool IsProfitCenterEmpty() 
        {
            if (mIsRptCashFlowStatementUseBudgetPlan && Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty. Please choose profit center.");
                return true;
            }

            return false;
        }

        #endregion

        #region Additional Methods

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES('CashflowStatementMonthlyExchangeCashTypeDescription', 'Deskripsi Cash Type untuk Monthly Foreign Exchange', NULL, 'PHT', NULL, 'Y', 'WEDHA', '202303131600', NULL, NULL); ");

            Sm.ExecQuery(SQL.ToString());
        }

        private void GetParameter()
        {
            //mCashFlowStatementOpeningBalance = Sm.GetParameterDec("CashFlowStatementOpeningBalance");
            //mCashFlowStatementStartingYear = Sm.GetParameter("CashFlowStatementStartingYear");
            //mRptCashFlowStatementFormat = Sm.GetParameter("RptCashFlowStatementFormat");
            //mRptCashFlowStatementFormat2 = Sm.GetParameter("RptCashFlowStatementFormat2");
            //mAcNoForCashFlowStatementOpeningBalance = Sm.GetParameter("AcNoForCashFlowStatementOpeningBalance");
            //mIsRptCashFlowStatementYearToDate = Sm.GetParameterBoo("IsRptCashFlowStatementYearToDate");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'CashFlowStatementOpeningBalance', 'CashFlowStatementStartingYear', 'RptCashFlowStatementFormat', 'RptCashFlowStatementFormat2', 'AcNoForCashFlowStatementOpeningBalance', ");
            SQL.AppendLine("'IsRptCashFlowStatementYearToDate', 'IsRptCashFlowStatementUseProfitCenter', 'IsRptCashFlowStatementUseBudgetPlan', 'IsVoucherUseCashType2', 'IsCashTypeUseParent', 'FormPrintOutCashFlowStatement', ");
            SQL.AppendLine("'IsCashFlowStatementShowCASBADetail', 'IsCashFlowStatementShowSwitchingBankAccountDetail', 'IsVoucherCASAllowMultipleBankAccount', 'BankAccountTypeForVRCA1', ");
            SQL.AppendLine("'CashflowStatementMonthlyExchangeCashTypeDescription', 'IsVoucherUseAdditionalCost' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptCashFlowStatementYearToDate": mIsRptCashFlowStatementYearToDate = ParValue == "Y"; break;
                            case "IsRptCashFlowStatementUseProfitCenter": mIsRptCashFlowStatementUseProfitCenter = ParValue == "Y"; break;
                            case "IsRptCashFlowStatementUseBudgetPlan": mIsRptCashFlowStatementUseBudgetPlan = ParValue == "Y"; break;
                            case "IsVoucherUseCashType2": mIsVoucherUseCashType2 = ParValue == "Y"; break;
                            case "IsCashTypeUseParent": mIsCashTypeUseParent = ParValue == "Y"; break;
                            case "IsCashFlowStatementShowCASBADetail": mIsCashFlowStatementShowCASBADetail = ParValue == "Y"; break;
                            case "IsCashFlowStatementShowSwitchingBankAccountDetail": mIsCashFlowStatementShowSwitchingBankAccountDetail = ParValue == "Y"; break;
                            case "IsVoucherCASAllowMultipleBankAccount": mIsVoucherCASAllowMultipleBankAccount = ParValue == "Y"; break;
                            case "IsVoucherUseAdditionalCost": mIsVoucherUseAdditionalCost = ParValue == "Y"; break;

                            //string
                            case "CashFlowStatementStartingYear": mCashFlowStatementStartingYear = ParValue; break;
                            case "RptCashFlowStatementFormat": mRptCashFlowStatementFormat = ParValue; break;
                            case "RptCashFlowStatementFormat2": mRptCashFlowStatementFormat2 = ParValue; break;
                            case "AcNoForCashFlowStatementOpeningBalance": mAcNoForCashFlowStatementOpeningBalance = ParValue; break;
                            case "FormPrintOutCashFlowStatement": mFormPrintOutCashFlowStatement = ParValue; break;
                            case "BankAccountTypeForVRCA1": mBankAccountTypeForVRCA1 = ParValue; break;
                            case "CashflowStatementMonthlyExchangeCashTypeDescription": mCashflowStatementMonthlyExchangeCashTypeDescription = ParValue; break;

                            //decimal
                            case "CashFlowStatementOpeningBalance": if (ParValue.Length > 0) mCashFlowStatementOpeningBalance = decimal.Parse(ParValue); break;
                        }
                    }
                }
                dr.Close();
            }
            if (mCashFlowStatementStartingYear.Length == 0) mCashFlowStatementStartingYear = "2019";
            if (mCashFlowStatementOpeningBalance == 0m) mCashFlowStatementOpeningBalance = 100000000m;
            if (mAcNoForCashFlowStatementOpeningBalance.Length == 0) mAcNoForCashFlowStatementOpeningBalance = "1.1.1,1.1.2";
        }

        private bool IsDocDateInvalid()
        {
            string mDocDt1 = Sm.Left(Sm.GetDte(DteDocDt1), 4);
            string mDocDt2 = Sm.Left(Sm.GetDte(DteDocDt2), 4);
            if (!Sm.CompareStr(mDocDt1, mDocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "You can't filter by date in different year.");
                return true;
            }

            if (Int32.Parse(mDocDt1) <= Int32.Parse(mCashFlowStatementStartingYear) ||
                Int32.Parse(mDocDt2) <= Int32.Parse(mCashFlowStatementStartingYear))
            {
                Sm.StdMsg(mMsgType.Warning, "You can't filter by date in this range.");
                return true;
            }

            return false;
        }

        private void ProcessNegativeBalance()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        Grd1.Cells[i, 9].Value = Sm.GetGrdDec(Grd1, i, 4);
                        Grd1.Cells[i, 10].Value = Sm.GetGrdDec(Grd1, i, 5);

                        if (Sm.GetGrdDec(Grd1, i, 4) < 0m) Grd1.Cells[i, 9].Value = string.Concat("(", Sm.FormatNum((Sm.GetGrdDec(Grd1, i, 4) * -1m), 0).ToString(), ")");
                        if (Sm.GetGrdDec(Grd1, i, 5) < 0m) Grd1.Cells[i, 10].Value = string.Concat("(", Sm.FormatNum((Sm.GetGrdDec(Grd1, i, 5) * -1m), 0).ToString(), ")");
                    }
                }
            }
        }
        
        private void SetTimeStampVariable()
        {
            if (Sm.GetDte(DteDocDt1).Length > 0 && Sm.GetDte(DteDocDt2).Length > 0)
            {
                mYr = Sm.Left(Sm.FormatDate(Convert.ToDateTime(DteDocDt1.Text)),4);
                mDt1 = (Sm.FormatDate(Convert.ToDateTime(DteDocDt1.Text))).Substring(4, 4);
                mDt2 = (Sm.FormatDate(Convert.ToDateTime(DteDocDt2.Text))).Substring(4, 4);

                if (mIsRptCashFlowStatementYearToDate)
                {
                    mLastYr1 = DateTime.Parse(String.Concat(Sm.Left(Sm.FormatDate(Convert.ToDateTime(DteDocDt1.Text)), 4), "-01-01 00:00:00"));
                    mLastYr2 = Convert.ToDateTime(DteDocDt2.Text);
                }
                else
                {
                    mLastYr1 = Convert.ToDateTime(DteDocDt1.Text).AddYears(-1);
                    mLastYr2 = Convert.ToDateTime(DteDocDt2.Text).AddYears(-1);
                }

                mThisYear = DteDocDt1.Text + " - " + DteDocDt2.Text;
                mLastYear = mLastYr1.ToString("dd/MMM/yyyy") + " - " + mLastYr2.ToString("dd/MMM/yyyy");
            }
        }

        private void ParPrint()
        {

            decimal mThisYearGroupAmt = 0m, mLastYearGroupAmt = 0m;
            decimal mThisYearGroupAmt1 = 0m, mLastYearGroupAmt1 = 0m;
            string mCashTypeGrpCode = string.Empty, mYr2 = (Convert.ToDecimal(mYr)-1).ToString();
            decimal mParamYearAmt = 0m, mLastYearAmt = 0m, mFilteredYearAmt = 0m, mLastYearFilteredAmt = 0m, mThisYearInitialAmt = 0m, mLastYearInitialAmt = 0m;
            string mDocTitle = Sm.GetValue("Select ParValue From tblparameter Where ParCode='DocTitle'");

            var l = new List<RptCashFlowStatement>();
            var lg = new List<RptCashFlowStatetmentGroup>();
            var lh = new List<Hdr>();
            var lh2 = new List<Hdr2>();
            var lOB = new List<OpeningBalance>();
            string[] TableName = { "RptCashFlowStatement", "RptCashFlowStatetmentGroup", "Hdr", "Hdr2" };
            var myLists = new List<IList>();

            #region Header

            lh.Add(new Hdr()
            {
                ThisYearStartDt = DteDocDt1.Text,
                ThisYearEndDt = DteDocDt2.Text,
                LastYearStartDt = mLastYr1.ToString("dd/MMM/yyy"),
                LastYearEndDt = mLastYr2.ToString("dd/MMM/yyy"),
                CompanyName = Sm.GetValue("Select ParValue From TblParameter Where Parcode = 'ReportTitle1'").ToUpper(),
                ProfitCenter = (mDocTitle == "AMKA" ? CcbProfitCenterCode.Text : "KONSOLIDASIAN"),
                ProfitCenter2 = CcbProfitCenterCode.Text,
                IsRptCashFlowStatementUseBudgetPlan = mIsRptCashFlowStatementUseBudgetPlan

            });

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    //if (mCashTypeGrpCode.Length == 0)
                    //{
                    //    mCashTypeGrpCode = Sm.GetGrdStr(Grd1, i, 8);
                    //    lg.Add(new RptCashFlowStatetmentGroup()
                    //    {
                    //        CashTypeGrpCode = mCashTypeGrpCode,
                    //        CashTypeGrpName = Sm.GetGrdStr(Grd1, i, 2),
                    //        TotalLabel = Sm.GetGrdStr(Grd1, i, 6)
                    //    });
                    //}
                    //else
                    //{
                    //    if (mCashTypeGrpCode != Sm.GetGrdStr(Grd1, i, 8))
                    //    {
                    //        mCashTypeGrpCode = Sm.GetGrdStr(Grd1, i, 8);
                    //        lg.Add(new RptCashFlowStatetmentGroup()
                    //        {
                    //            CashTypeGrpCode = mCashTypeGrpCode,
                    //            CashTypeGrpName = Sm.GetGrdStr(Grd1, i, 2),
                    //            TotalLabel = Sm.GetGrdStr(Grd1, i, 6)
                    //        });
                    //    }
                    //}

                    if (Sm.GetGrdStr(Grd1, i, 7) == "99FX") // monthly foreign exchange
                    {
                        l.Add(new RptCashFlowStatement()
                        {
                            CashTypeCode = Sm.GetGrdStr(Grd1, i, 7),
                            CashTypeName = Sm.GetGrdStr(Grd1, i, 1),
                            CashTypeGrpCode = "99FX",
                            CashTypeGrpName = Sm.GetGrdStr(Grd1, i, 2),
                            TotalLabel = Sm.GetGrdStr(Grd1, i, 6),
                            Notes = Sm.GetGrdStr(Grd1, i, 3),
                            ThisYearAmt = Sm.GetGrdDec(Grd1, i, 4),
                            LastYearAmt = Sm.GetGrdDec(Grd1, i, 5),
                            GroupLastYearAmt = 0m,
                            GroupThisYearAmt = 0m,
                            Amt = Sm.GetGrdDec(Grd1, i, 12),
                            Level = 0f,
                            Parent = string.Empty,
                        });
                    }
                    else
                    {
                        l.Add(new RptCashFlowStatement()
                        {
                            CashTypeCode = Sm.GetGrdStr(Grd1, i, 7),
                            CashTypeName = Sm.GetGrdStr(Grd1, i, 1),
                            CashTypeGrpCode = Sm.GetGrdStr(Grd1, i, 8),
                            CashTypeGrpName = Sm.GetGrdStr(Grd1, i, 2),
                            TotalLabel = Sm.GetGrdStr(Grd1, i, 6),
                            Notes = Sm.GetGrdStr(Grd1, i, 3),
                            ThisYearAmt = Sm.GetGrdDec(Grd1, i, 4),
                            LastYearAmt = Sm.GetGrdDec(Grd1, i, 5),
                            GroupLastYearAmt = 0m,
                            GroupThisYearAmt = 0m,
                            Amt = Sm.GetGrdDec(Grd1, i, 12),
                            Level = float.Parse(Sm.GetValue("Select Level From TblCashType Where CashTypeCode = @Param ", Sm.GetGrdStr(Grd1, i, 7))),
                            Parent = Sm.GetValue("Select Parent From TblCashType Where CashTypeCode = @Param ", Sm.GetGrdStr(Grd1, i, 7)),
                        });
                    }
                }
            }

            if (l.Count > 0)
            {
                foreach(var x in l.OrderBy(o => o.CashTypeGrpCode))
                {
                    if (mCashTypeGrpCode.Length == 0)
                    {
                        mCashTypeGrpCode = x.CashTypeGrpCode;

                        lg.Add(new RptCashFlowStatetmentGroup() 
                        {
                            CashTypeGrpCode = mCashTypeGrpCode,
                            CashTypeGrpName = x.CashTypeGrpName,
                            TotalLabel = x.TotalLabel
                        });
                    }
                    else
                    {
                        if (mCashTypeGrpCode != x.CashTypeGrpCode)
                        {
                            lg.Add(new RptCashFlowStatetmentGroup()
                            {
                                CashTypeGrpCode = x.CashTypeGrpCode,
                                CashTypeGrpName = x.CashTypeGrpName,
                                TotalLabel = x.TotalLabel
                            });

                            mCashTypeGrpCode = x.CashTypeGrpCode;
                        }
                    }
                }

                foreach (var x in lg.OrderBy(o => o.CashTypeGrpCode))
                {
                    mThisYearGroupAmt = 0m; mLastYearGroupAmt = 0m;

                    foreach (var y in l
                        .Where(w => w.CashTypeGrpCode == x.CashTypeGrpCode)
                        .OrderBy(o => o.CashTypeGrpCode))
                    {
                        mThisYearGroupAmt += y.ThisYearAmt;
                        mLastYearGroupAmt += y.LastYearAmt;
                    }

                    x.ThisYearGroupAmt = mThisYearGroupAmt;
                    x.LastYearGroupAmt = mLastYearGroupAmt;
                }

                foreach (var x in lg)
                {
                    foreach (var y in l.Where(w => w.CashTypeGrpCode == x.CashTypeGrpCode))
                    {
                        y.GroupThisYearAmt = x.ThisYearGroupAmt;
                        y.GroupLastYearAmt = x.LastYearGroupAmt;
                    }

                    mThisYearGroupAmt1 += x.ThisYearGroupAmt;
                    mLastYearGroupAmt1 += x.LastYearGroupAmt;
                }
            }

            #endregion

            #region Header 2
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (!mIsRptCashFlowStatementYearToDate)
            {
                SQL.AppendLine("Select Sum(ParamYearAmt) ParamYearAmt, Sum(LastYearAmt) LastYearAmt, ");
                SQL.AppendLine("Sum(FilteredYearAmt) FilteredYearAmt, Sum(LastYearFilteredAmt) LastYearFilteredAmt ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("	 Select (T.ParamYearAmt + T.ParamYearAmt2) ParamYearAmt, ");
                SQL.AppendLine("	 (T.LastYearAmt + T.LastYearAmt2) LastYearAmt, ");
                SQL.AppendLine("	 (T.FilteredYearAmt + T.FilteredYearAmt2) FilteredYearAmt, ");
                SQL.AppendLine("	 (T.LastYearFilteredAmt + T.LastYearFilteredAmt2) LastYearFilteredAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("            When 'C' Then -1 ");
                SQL.AppendLine("            Else 1 ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("         As ParamYearAmt, ");
                SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                SQL.AppendLine("             When 'C' Then -1 ");
                SQL.AppendLine("             When 'D' Then 1 ");
                SQL.AppendLine("             Else 0 ");
                SQL.AppendLine("             End ");
                SQL.AppendLine("         As ParamYearAmt2, ");
                SQL.AppendLine("         0.00 LastYearAmt, 0.00 LastYearAmt2, 0.00 FilteredYearAmt, 0.00 FilteredYearAmt2, 0.00 LastYearFilteredAmt, 0.00 LastYearFilteredAmt2 ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2, B.Amt ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("              Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                if (mRptCashFlowStatementFormat == "1")
                    SQL.AppendLine("            Where B.DocDt Like Concat(@StartingYr, '%') ");
                else
                    SQL.AppendLine("            Where B.DocDt Between Concat(@StartingYr, @Dt1) And Concat(@StartingYr, @Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");

                if (mCashFlowStatementStartingYear != mYr2)
                {
                    if (mRptCashFlowStatementFormat2 == "1")
                    {

                        SQL.AppendLine("        Union All ");

                        SQL.AppendLine("        Select 0.00 As ParamYearAmt, 0.00 As ParamYearAmt2, ");
                        SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                        SQL.AppendLine("            When 'C' Then -1 ");
                        SQL.AppendLine("            Else 1 ");
                        SQL.AppendLine("            End ");
                        SQL.AppendLine("         As LastYearAmt, ");
                        SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                        SQL.AppendLine("             When 'C' Then -1 ");
                        SQL.AppendLine("             When 'D' Then 1 ");
                        SQL.AppendLine("             Else 0 ");
                        SQL.AppendLine("             End ");
                        SQL.AppendLine("         As LastYearAmt2, ");
                        SQL.AppendLine("         0.00 FilteredYearAmt, 0.00 FilteredYearAmt2, 0.00 LastYearFilteredAmt, 0.00 LastYearFilteredAmt2 ");
                        SQL.AppendLine("        From TblCashType A ");
                        SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                        SQL.AppendLine("        Left Join ");
                        SQL.AppendLine("        ( ");
                        SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2, B.Amt ");
                        SQL.AppendLine("            From TblCashType A ");
                        SQL.AppendLine("              Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                        if (mRptCashFlowStatementFormat == "1")
                            SQL.AppendLine("            Where B.DocDt Like Concat(@Yr-1,'%') ");
                        else
                            SQL.AppendLine("            Where B.DocDt Between Concat(@Yr-1, @Dt1) And Concat(@Yr-1, @Dt2) ");
                        SQL.AppendLine("            And B.CancelInd = 'N' ");
                        SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                    }
                    else
                    {
                        int a = 0, b = 0;

                        a = Int32.Parse(mCashFlowStatementStartingYear) + 1;
                        b = Int32.Parse(mYr2);

                        for (int i = a; i <= b; ++i)
                        {
                            SQL.AppendLine("        Union All ");

                            SQL.AppendLine("        Select 0.00 As ParamYearAmt, 0.00 As ParamYearAmt2, ");
                            SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                            SQL.AppendLine("            When 'C' Then -1 ");
                            SQL.AppendLine("            Else 1 ");
                            SQL.AppendLine("            End ");
                            SQL.AppendLine("         As LastYearAmt, ");
                            SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                            SQL.AppendLine("             When 'C' Then -1 ");
                            SQL.AppendLine("             When 'D' Then 1 ");
                            SQL.AppendLine("             Else 0 ");
                            SQL.AppendLine("             End ");
                            SQL.AppendLine("         As LastYearAmt2, ");
                            SQL.AppendLine("         0.00 FilteredYearAmt, 0.00 FilteredYearAmt2, 0.00 LastYearFilteredAmt, 0.00 LastYearFilteredAmt2 ");
                            SQL.AppendLine("        From TblCashType A ");
                            SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                            SQL.AppendLine("        Left Join ");
                            SQL.AppendLine("        ( ");
                            SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2, B.Amt ");
                            SQL.AppendLine("            From TblCashType A ");
                            SQL.AppendLine("              Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                            if (mRptCashFlowStatementFormat == "1")
                                SQL.AppendLine("            Where B.DocDt Like Concat('" + i.ToString() + "','%') ");
                            else
                                SQL.AppendLine("            Where B.DocDt Between Concat('" + i.ToString() + "', @Dt1) And Concat('" + i.ToString() + "', @Dt2) ");
                            SQL.AppendLine("            And B.CancelInd = 'N' ");
                            SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                        }
                    }
                }

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select 0.00 ParamYearAmt, 0.00 ParamYearAmt2, 0.00 LastYearAmt, 0.00 LastYearAmt2, ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("            When 'C' Then -1 ");
                SQL.AppendLine("            Else 1 ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("         As FilteredYearAmt, ");
                SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                SQL.AppendLine("             When 'C' Then -1 ");
                SQL.AppendLine("             When 'D' Then 1 ");
                SQL.AppendLine("             Else 0 ");
                SQL.AppendLine("             End ");
                SQL.AppendLine("         As FilteredYearAmt2, 0.00 LastYearFilteredAmt, 0.00 LastYearFilteredAmt2 ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2, B.Amt ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("              Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode ");
                SQL.AppendLine("            Where B.DocDt Between Concat(@Yr,@Dt1) And Concat(@Yr,@Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select 0.00 ParamYearAmt, 0.00 ParamYearAmt2, 0.00 LastYearAmt, 0.00 LastYearAmt2, ");
                SQL.AppendLine("        0.00 FilteredYearAmt, 0.00 FilteredYearAmt2, ");
                SQL.AppendLine("        IfNull(C.Amt, 0.00) * Case C.AcType ");
                SQL.AppendLine("            When 'C' Then -1 ");
                SQL.AppendLine("            Else 1 ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("         As LastYearFilteredAmt, ");
                SQL.AppendLine("         IfNull(C.Amt, 0.00) * Case C.AcType2 ");
                SQL.AppendLine("             When 'C' Then -1 ");
                SQL.AppendLine("             When 'D' Then 1 ");
                SQL.AppendLine("             Else 0 ");
                SQL.AppendLine("             End ");
                SQL.AppendLine("         As LastYearFilteredAmt2 ");
                SQL.AppendLine("        From TblCashType A ");
                SQL.AppendLine("        Inner Join TblCashTypeGroup B ON A.CashTypeGrpCode = B.CashTypeGrpCode ");
                SQL.AppendLine("        Left Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select A.CashTypeCode, B.AcType, B.AcType2, B.Amt ");
                SQL.AppendLine("            From TblCashType A ");
                SQL.AppendLine("            Inner Join TblVoucherHdr B On A.CashTypeCode = B.CashTypeCode  ");
                SQL.AppendLine("            Where B.DocDt Between Concat(@Yr-1,@Dt1) And Concat(@Yr-1,@Dt2) ");
                SQL.AppendLine("            And B.CancelInd = 'N' ");
                SQL.AppendLine("        ) C On A.CashTypeCode = C.CashTypeCode ");
                SQL.AppendLine("    ) T ");
                SQL.AppendLine(") X ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@Yr", mYr);
                    Sm.CmParam<String>(ref cm, "@Dt1", mDt1);
                    Sm.CmParam<String>(ref cm, "@Dt2", mDt2);
                    Sm.CmParam<String>(ref cm, "@StartingYr", mCashFlowStatementStartingYear);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "ParamYearAmt",

                         //1-3
                         "LastYearAmt",
                         "FilteredYearAmt",
                         "LastYearFilteredAmt",
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            mParamYearAmt = Sm.DrDec(dr, c[0]);
                            mLastYearAmt = Sm.DrDec(dr, c[1]);
                            mFilteredYearAmt = Sm.DrDec(dr, c[2]);
                            mLastYearFilteredAmt = Sm.DrDec(dr, c[3]);
                        }
                    }
                    dr.Close();
                }

                lh2.Add(new Hdr2()
                {
                    CashTypeName = "KENAIKAN  BERSIH KAS DAN SETARA KAS",
                    ThisYearAmt = mFilteredYearAmt,
                    LastYearAmt = mLastYearFilteredAmt,
                });

                lh2.Add(new Hdr2()
                {
                    CashTypeName = "KAS DAN SETARA KAS AWAL",
                    ThisYearAmt = (mYr == mCashFlowStatementStartingYear ? 0 : mCashFlowStatementOpeningBalance) +
                                  (mYr == mCashFlowStatementStartingYear ? 0 : mParamYearAmt) +
                                  (mYr != mCashFlowStatementStartingYear && Convert.ToDecimal(mYr) - Convert.ToDecimal(mCashFlowStatementStartingYear) != 1 ? mLastYearAmt : 0),
                    LastYearAmt = (mYr2 == mCashFlowStatementStartingYear ? 0 : mCashFlowStatementOpeningBalance) +
                                  (mYr2 == mCashFlowStatementStartingYear ? 0 : mParamYearAmt) +
                                  (mYr2 != mCashFlowStatementStartingYear && Convert.ToDecimal(mYr2) - Convert.ToDecimal(mCashFlowStatementStartingYear) != 1 ? mLastYearAmt : 0),
                });

                if (lh2.Count > 0)
                {
                    mThisYearInitialAmt = lh2[1].ThisYearAmt;
                    mLastYearInitialAmt = lh2[1].LastYearAmt;
                }
            }
            else
            {
                SQL.AppendLine("Select Sum(T.OBAmt) OBAmt, Sum(T.VCAmt) VCAmt From(");
                SQL.AppendLine("Select Case C.AcType When 'D' Then B.Amt*1 Else B.Amt*-1 End As OBAmt, 0.00 VCAmt ");
                SQL.AppendLine("From TblCOAOpeningBalanceHdr A  ");
                SQL.AppendLine("Inner Join TblCOAOpeningBalanceDtl B On A.DocNo = B.DocNo  ");
                SQL.AppendLine("    And A.Yr = @Yr  ");
                SQL.AppendLine("    And A.CancelInd = 'N'  ");
                SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo  ");
                SQL.AppendLine("    And C.ActInd = 'Y'  ");
                if (!mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("    And FIND_IN_SET(C.AcNo, @AcNoForCashFlowStatementOpeningBalance)  ");
                }
                else
                {
                    SQL.AppendLine("WHERE B.AcNo = @AcNoForCashFlowStatementOpeningBalance ");
                    //SQL.AppendLine("AND FIND_IN_SET(A.ProfitCenterCode, @ProfitCenterCOde) "); //SET
                    SQL.AppendLine("AND A.ProfitCenterCode LIKE @ProfitCenterCode ");
                }

                SQL.AppendLine("    UNION ALL ");

                SQL.AppendLine("SELECT 0.00 As OBAmt,  ");
                SQL.AppendLine("Case AcType When 'D'  ");
                SQL.AppendLine("Then (case when A.curcode ='IDR' then  Amt ELSE ifnull(ExcRate, 1)*Amt END ) ");
                SQL.AppendLine("Else (case when A.curcode ='IDR' then  Amt ELSE ifnull(ExcRate, 1)*Amt END )*-1 End As VCAmt  ");
                SQL.AppendLine("FROM TblVoucherHdr A  ");
                if (mIsRptCashFlowStatementUseProfitCenter)
                {
                    SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode ");
                    SQL.AppendLine("And B.SiteCode Is Not Null ");
                    SQL.AppendLine("And B.SiteCode In( ");
                    SQL.AppendLine("    Select Distinct SiteCode ");
                    SQL.AppendLine("    From TblSite ");
                    SQL.AppendLine("    Where ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    AND ProfitCenterCode LIKE @ProfitCenterCode  ");
                    SQL.AppendLine(")  ");
                    SQL.AppendLine(" AND B.COAAcNo LIKE CONCAT(@AcNoForCashFlowStatementOpeningBalance, '%') ");
                }
                SQL.AppendLine("Where Left(DocDt, 4) = @Yr And DocDt < @StartDt ");
                SQL.AppendLine("And CancelInd = 'N'  ");
                SQL.AppendLine("And AcType2 Is Null  ");
                SQL.AppendLine("And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");

                if (mIsCashFlowStatementShowSwitchingBankAccountDetail || mIsCashFlowStatementShowCASBADetail)
                {
                    SQL.AppendLine("    UNION ALL ");

                    SQL.AppendLine("SELECT 0.00 As OBAmt,  ");
                    SQL.AppendLine("Case AcType When 'D'  ");
                    SQL.AppendLine("Then (case when A.curcode ='IDR' then  Amt ELSE ifnull(ExcRate, 1)*Amt END ) ");
                    SQL.AppendLine("Else (case when A.curcode ='IDR' then  Amt ELSE ifnull(ExcRate, 1)*Amt END )*-1 End As VCAmt  ");
                    SQL.AppendLine("FROM TblVoucherHdr A  ");
                    if (mIsRptCashFlowStatementUseProfitCenter)
                    {
                        SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode=B.BankAcCode ");
                        SQL.AppendLine("And B.SiteCode Is Not Null ");
                        SQL.AppendLine("And B.SiteCode In( ");
                        SQL.AppendLine("    Select Distinct SiteCode ");
                        SQL.AppendLine("    From TblSite ");
                        SQL.AppendLine("    Where ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("    AND ProfitCenterCode LIKE @ProfitCenterCode  ");
                        SQL.AppendLine(")  ");
                        SQL.AppendLine(" AND B.COAAcNo LIKE CONCAT(@AcNoForCashFlowStatementOpeningBalance, '%') ");
                    }
                    SQL.AppendLine("Where Left(DocDt, 4) = @Yr And DocDt < @StartDt ");
                    SQL.AppendLine("And CancelInd = 'N'  ");
                    SQL.AppendLine("And (A.DocType = '16' OR A.DocType = '70')  ");
                    SQL.AppendLine("And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");


                    SQL.AppendLine("    UNION ALL ");

                    SQL.AppendLine("SELECT 0.00 As OBAmt,  ");
                    SQL.AppendLine("Case AcType2 When 'D'  ");
                    SQL.AppendLine("Then (case when A.curcode ='IDR' then  Amt ELSE ifnull(ExcRate, 1)*Amt END ) ");
                    SQL.AppendLine("Else (case when A.curcode ='IDR' then  Amt ELSE ifnull(ExcRate, 1)*Amt END )*-1 End As VCAmt  ");
                    SQL.AppendLine("FROM TblVoucherHdr A  ");
                    if (mIsRptCashFlowStatementUseProfitCenter)
                    {
                        SQL.AppendLine("Inner Join TblBankAccount B On A.BankAcCode2=B.BankAcCode ");
                        SQL.AppendLine("And B.SiteCode Is Not Null ");
                        SQL.AppendLine("And B.SiteCode In( ");
                        SQL.AppendLine("    Select Distinct SiteCode ");
                        SQL.AppendLine("    From TblSite ");
                        SQL.AppendLine("    Where ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("    AND ProfitCenterCode LIKE @ProfitCenterCode  ");
                        SQL.AppendLine(")  ");
                        SQL.AppendLine(" AND B.COAAcNo LIKE CONCAT(@AcNoForCashFlowStatementOpeningBalance, '%') ");
                    }
                    SQL.AppendLine("Where Left(DocDt, 4) = @Yr And DocDt < @StartDt ");
                    SQL.AppendLine("And CancelInd = 'N'  ");
                    SQL.AppendLine("And (A.DocType = '16' OR A.DocType = '70')  ");
                    SQL.AppendLine("And A.DocNo Not In (Select DocNo From TblVoucherHdr A Where A.DocType = '01' And A.VoucherJournalDocNo is Null) ");

                }

                SQL.AppendLine(")T; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@Yr", mYr);
                    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteDocDt1));
                    Sm.CmParam<String>(ref cm, "@AcNoForCashFlowStatementOpeningBalance", mAcNoForCashFlowStatementOpeningBalance);
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode() + "%");

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "OBAmt",

                         //1
                         "VCAmt",
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lOB.Add(new OpeningBalance()
                            {
                                OBAmt = Sm.DrDec(dr, c[0]),
                                VCAmt = Sm.DrDec(dr, c[1]),
                            });
                        }
                    }
                    dr.Close();
                }

                lh2.Add(new Hdr2()
                {
                    CashTypeName = "KENAIKAN  BERSIH KAS DAN SETARA KAS",
                    ThisYearAmt = mThisYearGroupAmt1,
                    LastYearAmt = mLastYearGroupAmt1,
                });

                for (int i = 0; i < lOB.Count; i++ )
                {
                    lh2.Add(new Hdr2()
                       {
                           CashTypeName = "KAS DAN SETARA KAS AWAL",
                           ThisYearAmt = lOB[i].OBAmt + lOB[i].VCAmt,
                           LastYearAmt = lOB[i].OBAmt,
                       });
                }

                if (lh2.Count > 0)
                {
                    mThisYearInitialAmt = lh2[1].ThisYearAmt;
                    mLastYearInitialAmt = lh2[1].LastYearAmt;
                }
            }

            lh2.Add(new Hdr2()
            {
                CashTypeName = "KAS DAN SETARA KAS AKHIR",
                ThisYearAmt = mThisYearInitialAmt + mThisYearGroupAmt1,
                LastYearAmt = mLastYearInitialAmt + mLastYearGroupAmt1,
            });
            #endregion

            if (mIsCashTypeUseParent)
            {
                if (l.Count > 0)
                {
                    foreach (var x in l.OrderByDescending(o => o.Level))
                    {
                        foreach (var y in l.Where(w => w.Parent == x.CashTypeCode && w.Level > x.Level).OrderByDescending(o => o.Level))
                        {
                            x.ThisYearAmt += y.ThisYearAmt;
                            x.LastYearAmt += y.LastYearAmt;
                        }
                    }
                }
            }

            if (mIsCashTypeUseParent) l.OrderBy(u => u.CashTypeCode);
            myLists.Add(l);
            myLists.Add(lg);
            myLists.Add(lh);
            myLists.Add(lh2);

            Sm.PrintReport(mFormPrintOutCashFlowStatement, myLists, TableName, false);
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        internal string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        internal void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptCashFlowStatementUseProfitCenter && !mIsRptCashFlowStatementUseBudgetPlan) return;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                    SQL.AppendLine("    And (" + Filter + ") ");
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");
        }

        #endregion

        #region Grid Control Events

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmRptCashFlowStatementDlg(this, mlProfitCenter);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCashTypeCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmRptCashFlowStatementDlg(this, mlProfitCenter);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCashTypeCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class RptCashFlowStatement
        {
            public string CashTypeCode { get; set; }
            public string CashTypeName { get; set; }
            public string CashTypeGrpCode { get; set; }
            public string CashTypeGrpName { get; set; }
            public string TotalLabel { get; set; }
            public string Notes { get; set; }
            public decimal ThisYearAmt { get; set; }
            public decimal LastYearAmt { get; set; }
            public decimal GroupThisYearAmt { get; set; }
            public decimal GroupLastYearAmt { get; set; }
            public decimal Amt { get; set; }
            public float Level { get; set; }
            public string Parent { get; set; }
        }

        private class RptCashFlowStatetmentGroup
        {
            public string CashTypeGrpCode { get; set; }
            public string CashTypeGrpName { get; set; }
            public string TotalLabel { get; set; }
            public decimal ThisYearGroupAmt { get; set; }
            public decimal LastYearGroupAmt { get; set; }
        }

        private class Hdr
        {
            public string ThisYearStartDt { get; set; }
            public string ThisYearEndDt { get; set; }
            public string LastYearStartDt { get; set; }
            public string LastYearEndDt { get; set; }
            public string CompanyName { get; set; }
            public string ProfitCenter { get; set; }
            public string DocTitle { get; set; }
            public string ProfitCenter2 { get; set; }
            public bool IsRptCashFlowStatementUseBudgetPlan { get; set; }

        }

        private class Hdr2
        {
            public string CashTypeName { get; set; }
            public decimal ThisYearAmt { get; set; }
            public decimal LastYearAmt { get; set; }
        }

        private class OpeningBalance
        {
            public decimal OBAmt { get; set; }
            public decimal VCAmt { get; set; }
        }

        #endregion
    }
}
