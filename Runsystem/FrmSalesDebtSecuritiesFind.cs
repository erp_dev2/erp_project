﻿#region Update
/*
    22/06/2022 [RDA/PRODUCT] new menu
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesDebtSecuritiesFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesDebtSecurities mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesDebtSecuritiesFind(FrmSalesDebtSecurities FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            SetGrd();
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Document Date",
                        "Investment Bank Account (RDN)",
                        "Investment Name",
                        "Investment Code",

                        //6-10
                        "Type",
                        "Category",
                        "Nominal Amount",
                        "Moving Average Price",
                        "Sales Amount",

                        //11-15
                        "Sales Price",
                        "Created By",
                        "Created Date",
                        "Created Time",
                        "Last Updated By",

                        //16-18
                        "Last Updated Date",
                        "Last Updated Time",
                        "Cancel"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 120, 150, 150, 150,  

                        //6-10
                        150, 150, 120, 120, 120,

                        //11-15
                        120, 100, 100, 100, 100,
                        
                        //16-18
                        100, 100, 50
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 18 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 16 });
            Sm.GrdFormatTime(Grd1, new int[] { 14, 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[18].Move(3);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 12, 13, 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.SekuritasCode, ");
            SQL.AppendLine("A.BankAcCode, B.BankAcNm, C.PortofolioName, ");
            SQL.AppendLine("CASE ");
            SQL.AppendLine("    WHEN A.VoucherDocNo IS NULL then 'Sold' ");
            SQL.AppendLine("    WHEN A.VoucherDocNo IS NOT NULL then 'Paid' ");
            SQL.AppendLine("    ELSE NULL ");
            SQL.AppendLine("END AS Status, A.VoucherDocNo, A.InvestmentCode, A.InvestmentType, E.OptDesc InvestmentTypeNm, ");
            SQL.AppendLine("A.InvestmentCtCode, F.InvestmentCtName, A.NominalAmt, A.MovingAvgPrice, A.InvestmentCost, ");
            SQL.AppendLine("A.RecordedMarketPrice, A.RecordedMarketValue, A.SalesPrice, A.SalesAmt, A.InterestFrequency, ");
            SQL.AppendLine("A.TradeDt, A.SettlementDt, A.InterestRateAmt, A.AnnualDays, A.MaturityDt, ");
            SQL.AppendLine("A.LastCouponDt, A.NextCouponDt, A.CurCode, A.CurRt, A.AccruedInterest, ");
            SQL.AppendLine("A.TotalExpenses, A.TotalSalesAmt, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.CancelInd ");
            SQL.AppendLine("FROM tblsalesdebtsecuritieshdr A ");
            SQL.AppendLine("INNER JOIN tblbankaccount B ON A.BankAcCode = B.BankAcCode  ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemdebt C ON A.InvestmentDebtCode = C.InvestmentDebtCode  ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio D ON C.PortofolioId = D.PortofolioId  ");
            SQL.AppendLine("INNER JOIN tbloption E ON A.InvestmentType = E.OptCode AND E.OptCat = 'InvestmentType'  ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory F ON D.InvestmentCtCode = F.InvestmentCtCode    ");

            mSQL = SQL.ToString();
        }

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "where 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "BankAcNm", "PortofolioName", "InvestmentCode", "InvestmentTypeNm",

                            //6-10
                            "InvestmentCtName", "NominalAmt", "MovingAvgPrice", "SalesAmt", "SalesPrice", 
                            
                            //11-15
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "CancelInd"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 18, 15);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #endregion

        #endregion


        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }


        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion


    }
}
