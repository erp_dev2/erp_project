﻿namespace RunSystem
{
    partial class FrmMaterialRequest2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaterialRequest2));
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueReqType = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRemainingBudget = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.LueTaxCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueTaxCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtTaxAmt = new DevExpress.XtraEditors.TextEdit();
            this.LueTaxCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.DteEstRecvDt = new DevExpress.XtraEditors.DateEdit();
            this.LueVdContactPersonName = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtSIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblShipInstruction = new System.Windows.Forms.Label();
            this.BtnSI = new DevExpress.XtraEditors.SimpleButton();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnDORequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDORequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDORequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblDORequestDocNo = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.LueOptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtBudgetForMR = new DevExpress.XtraEditors.TextEdit();
            this.LblMRType = new System.Windows.Forms.Label();
            this.LueMRType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblPOSignBy = new System.Windows.Forms.Label();
            this.LuePOSignBy = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdContactPersonName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBudgetForMR.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMRType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePOSignBy.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(969, 0);
            this.panel1.Size = new System.Drawing.Size(70, 579);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LblPOSignBy);
            this.panel2.Controls.Add(this.LuePOSignBy);
            this.panel2.Controls.Add(this.LblMRType);
            this.panel2.Controls.Add(this.LueMRType);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtBudgetForMR);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.LueOptCode);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.BtnDORequestDocNo2);
            this.panel2.Controls.Add(this.PbUpload3);
            this.panel2.Controls.Add(this.BtnDORequestDocNo);
            this.panel2.Controls.Add(this.PbUpload2);
            this.panel2.Controls.Add(this.TxtDORequestDocNo);
            this.panel2.Controls.Add(this.ChkFile3);
            this.panel2.Controls.Add(this.LblDORequestDocNo);
            this.panel2.Controls.Add(this.BtnDownload3);
            this.panel2.Controls.Add(this.LblSiteCode);
            this.panel2.Controls.Add(this.TxtFile3);
            this.panel2.Controls.Add(this.LueSiteCode);
            this.panel2.Controls.Add(this.BtnFile3);
            this.panel2.Controls.Add(this.BtnSI);
            this.panel2.Controls.Add(this.ChkFile2);
            this.panel2.Controls.Add(this.TxtSIDocNo);
            this.panel2.Controls.Add(this.BtnDownload2);
            this.panel2.Controls.Add(this.LblShipInstruction);
            this.panel2.Controls.Add(this.TxtFile2);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.LueBCCode);
            this.panel2.Controls.Add(this.BtnFile2);
            this.panel2.Controls.Add(this.TxtLocalDocNo);
            this.panel2.Controls.Add(this.ChkFile);
            this.panel2.Controls.Add(this.label76);
            this.panel2.Controls.Add(this.BtnDownload);
            this.panel2.Controls.Add(this.TxtAmt);
            this.panel2.Controls.Add(this.PbUpload);
            this.panel2.Controls.Add(this.TxtFile);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.LueTaxCode1);
            this.panel2.Controls.Add(this.BtnFile);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.LueTaxCode3);
            this.panel2.Controls.Add(this.TxtTaxAmt);
            this.panel2.Controls.Add(this.LueTaxCode2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtRemainingBudget);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueDeptCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueReqType);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(969, 453);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueVdContactPersonName);
            this.panel3.Controls.Add(this.DteEstRecvDt);
            this.panel3.Controls.Add(this.DteUsageDt);
            this.panel3.Location = new System.Drawing.Point(0, 453);
            this.panel3.Size = new System.Drawing.Size(969, 126);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.DteUsageDt, 0);
            this.panel3.Controls.SetChildIndex(this.DteEstRecvDt, 0);
            this.panel3.Controls.SetChildIndex(this.LueVdContactPersonName, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 557);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(969, 126);
            this.Grd1.TabIndex = 65;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(140, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(228, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(63, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(140, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 13;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(103, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(52, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Request Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReqType
            // 
            this.LueReqType.EnterMoveNextControl = true;
            this.LueReqType.Location = new System.Drawing.Point(140, 67);
            this.LueReqType.Margin = new System.Windows.Forms.Padding(5);
            this.LueReqType.Name = "LueReqType";
            this.LueReqType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.Appearance.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReqType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReqType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReqType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReqType.Properties.DropDownRows = 12;
            this.LueReqType.Properties.NullText = "[Empty]";
            this.LueReqType.Properties.PopupWidth = 350;
            this.LueReqType.Size = new System.Drawing.Size(319, 20);
            this.LueReqType.TabIndex = 17;
            this.LueReqType.ToolTip = "F4 : Show/hide list";
            this.LueReqType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReqType.EditValueChanged += new System.EventHandler(this.LueReqType_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(63, 91);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(140, 88);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 350;
            this.LueDeptCode.Size = new System.Drawing.Size(319, 20);
            this.LueDeptCode.TabIndex = 19;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TxtRemainingBudget
            // 
            this.TxtRemainingBudget.EnterMoveNextControl = true;
            this.TxtRemainingBudget.Location = new System.Drawing.Point(140, 151);
            this.TxtRemainingBudget.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRemainingBudget.Name = "TxtRemainingBudget";
            this.TxtRemainingBudget.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRemainingBudget.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRemainingBudget.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseFont = true;
            this.TxtRemainingBudget.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRemainingBudget.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRemainingBudget.Properties.ReadOnly = true;
            this.TxtRemainingBudget.Size = new System.Drawing.Size(152, 20);
            this.TxtRemainingBudget.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(39, 155);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 14);
            this.label11.TabIndex = 24;
            this.label11.Text = "Available Budget";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(140, 298);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 500;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(411, 20);
            this.MeeRemark.TabIndex = 37;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(89, 301);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 36;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(74, 21);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(125, 20);
            this.DteUsageDt.TabIndex = 59;
            this.DteUsageDt.Visible = false;
            this.DteUsageDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteUsageDt_KeyDown);
            this.DteUsageDt.Leave += new System.EventHandler(this.DteUsageDt_Leave);
            // 
            // LueTaxCode1
            // 
            this.LueTaxCode1.EnterMoveNextControl = true;
            this.LueTaxCode1.Location = new System.Drawing.Point(140, 193);
            this.LueTaxCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode1.Name = "LueTaxCode1";
            this.LueTaxCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode1.Properties.DropDownRows = 30;
            this.LueTaxCode1.Properties.NullText = "[Empty]";
            this.LueTaxCode1.Properties.PopupWidth = 350;
            this.LueTaxCode1.Size = new System.Drawing.Size(319, 20);
            this.LueTaxCode1.TabIndex = 29;
            this.LueTaxCode1.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode1.EditValueChanged += new System.EventHandler(this.LueTaxCode1_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(77, 257);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 32;
            this.label8.Text = "Total Tax";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode3
            // 
            this.LueTaxCode3.EnterMoveNextControl = true;
            this.LueTaxCode3.Location = new System.Drawing.Point(140, 235);
            this.LueTaxCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode3.Name = "LueTaxCode3";
            this.LueTaxCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode3.Properties.DropDownRows = 30;
            this.LueTaxCode3.Properties.NullText = "[Empty]";
            this.LueTaxCode3.Properties.PopupWidth = 350;
            this.LueTaxCode3.Size = new System.Drawing.Size(319, 20);
            this.LueTaxCode3.TabIndex = 31;
            this.LueTaxCode3.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode3.EditValueChanged += new System.EventHandler(this.LueTaxCode3_EditValueChanged);
            // 
            // TxtTaxAmt
            // 
            this.TxtTaxAmt.EnterMoveNextControl = true;
            this.TxtTaxAmt.Location = new System.Drawing.Point(140, 256);
            this.TxtTaxAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxAmt.Name = "TxtTaxAmt";
            this.TxtTaxAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxAmt.Properties.ReadOnly = true;
            this.TxtTaxAmt.Size = new System.Drawing.Size(152, 20);
            this.TxtTaxAmt.TabIndex = 33;
            // 
            // LueTaxCode2
            // 
            this.LueTaxCode2.EnterMoveNextControl = true;
            this.LueTaxCode2.Location = new System.Drawing.Point(140, 214);
            this.LueTaxCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode2.Name = "LueTaxCode2";
            this.LueTaxCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode2.Properties.DropDownRows = 30;
            this.LueTaxCode2.Properties.NullText = "[Empty]";
            this.LueTaxCode2.Properties.PopupWidth = 350;
            this.LueTaxCode2.Size = new System.Drawing.Size(319, 20);
            this.LueTaxCode2.TabIndex = 30;
            this.LueTaxCode2.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode2.EditValueChanged += new System.EventHandler(this.LueTaxCode2_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(109, 196);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 14);
            this.label6.TabIndex = 28;
            this.label6.Text = "Tax";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(140, 277);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(152, 20);
            this.TxtAmt.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(65, 280);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 14);
            this.label10.TabIndex = 34;
            this.label10.Text = "Grand Total";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEstRecvDt
            // 
            this.DteEstRecvDt.EditValue = null;
            this.DteEstRecvDt.EnterMoveNextControl = true;
            this.DteEstRecvDt.Location = new System.Drawing.Point(222, 21);
            this.DteEstRecvDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEstRecvDt.Name = "DteEstRecvDt";
            this.DteEstRecvDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstRecvDt.Properties.Appearance.Options.UseFont = true;
            this.DteEstRecvDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstRecvDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEstRecvDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEstRecvDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEstRecvDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstRecvDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEstRecvDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstRecvDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEstRecvDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEstRecvDt.Size = new System.Drawing.Size(125, 20);
            this.DteEstRecvDt.TabIndex = 60;
            this.DteEstRecvDt.Visible = false;
            this.DteEstRecvDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEstRecvDt_KeyDown);
            this.DteEstRecvDt.Leave += new System.EventHandler(this.DteEstRecvDt_Leave);
            // 
            // LueVdContactPersonName
            // 
            this.LueVdContactPersonName.EnterMoveNextControl = true;
            this.LueVdContactPersonName.Location = new System.Drawing.Point(380, 21);
            this.LueVdContactPersonName.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdContactPersonName.Name = "LueVdContactPersonName";
            this.LueVdContactPersonName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdContactPersonName.Properties.Appearance.Options.UseFont = true;
            this.LueVdContactPersonName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdContactPersonName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdContactPersonName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdContactPersonName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdContactPersonName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdContactPersonName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdContactPersonName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdContactPersonName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdContactPersonName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdContactPersonName.Properties.DropDownRows = 20;
            this.LueVdContactPersonName.Properties.NullText = "[Empty]";
            this.LueVdContactPersonName.Properties.PopupWidth = 300;
            this.LueVdContactPersonName.Size = new System.Drawing.Size(171, 20);
            this.LueVdContactPersonName.TabIndex = 61;
            this.LueVdContactPersonName.ToolTip = "F4 : Show/hide list";
            this.LueVdContactPersonName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdContactPersonName.EditValueChanged += new System.EventHandler(this.LueContact_EditValueChanged);
            this.LueVdContactPersonName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueVdContactPersonName_KeyDown);
            this.LueVdContactPersonName.Leave += new System.EventHandler(this.LueVdContactPersonName_Leave);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(140, 46);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(228, 20);
            this.TxtLocalDocNo.TabIndex = 15;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(41, 49);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(95, 14);
            this.label76.TabIndex = 14;
            this.label76.Text = "Local Document";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(36, 133);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 14);
            this.label7.TabIndex = 22;
            this.label7.Text = "Budget Category";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(140, 130);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 350;
            this.LueBCCode.Size = new System.Drawing.Size(319, 20);
            this.LueBCCode.TabIndex = 23;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // TxtSIDocNo
            // 
            this.TxtSIDocNo.EnterMoveNextControl = true;
            this.TxtSIDocNo.Location = new System.Drawing.Point(710, 4);
            this.TxtSIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSIDocNo.Name = "TxtSIDocNo";
            this.TxtSIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSIDocNo.Properties.MaxLength = 30;
            this.TxtSIDocNo.Properties.ReadOnly = true;
            this.TxtSIDocNo.Size = new System.Drawing.Size(228, 20);
            this.TxtSIDocNo.TabIndex = 57;
            // 
            // LblShipInstruction
            // 
            this.LblShipInstruction.AutoSize = true;
            this.LblShipInstruction.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblShipInstruction.Location = new System.Drawing.Point(584, 7);
            this.LblShipInstruction.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblShipInstruction.Name = "LblShipInstruction";
            this.LblShipInstruction.Size = new System.Drawing.Size(122, 14);
            this.LblShipInstruction.TabIndex = 56;
            this.LblShipInstruction.Text = "Shipment Instruction";
            this.LblShipInstruction.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSI
            // 
            this.BtnSI.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSI.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSI.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSI.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSI.Appearance.Options.UseBackColor = true;
            this.BtnSI.Appearance.Options.UseFont = true;
            this.BtnSI.Appearance.Options.UseForeColor = true;
            this.BtnSI.Appearance.Options.UseTextOptions = true;
            this.BtnSI.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSI.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSI.Image = ((System.Drawing.Image)(resources.GetObject("BtnSI.Image")));
            this.BtnSI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSI.Location = new System.Drawing.Point(941, 4);
            this.BtnSI.Name = "BtnSI";
            this.BtnSI.Size = new System.Drawing.Size(24, 21);
            this.BtnSI.TabIndex = 58;
            this.BtnSI.ToolTip = "Find Shipment Instruction#";
            this.BtnSI.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSI.ToolTipTitle = "Run System";
            this.BtnSI.Click += new System.EventHandler(this.BtnSI_Click);
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(108, 112);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 20;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(140, 109);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(319, 20);
            this.LueSiteCode.TabIndex = 21;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // BtnDORequestDocNo2
            // 
            this.BtnDORequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo2.Image")));
            this.BtnDORequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo2.Location = new System.Drawing.Point(941, 66);
            this.BtnDORequestDocNo2.Name = "BtnDORequestDocNo2";
            this.BtnDORequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo2.TabIndex = 66;
            this.BtnDORequestDocNo2.ToolTip = "Show DO Request Document";
            this.BtnDORequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo2.ToolTipTitle = "Run System";
            this.BtnDORequestDocNo2.Click += new System.EventHandler(this.BtnDORequestDocNo2_Click);
            // 
            // BtnDORequestDocNo
            // 
            this.BtnDORequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDORequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDORequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDORequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDORequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseFont = true;
            this.BtnDORequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDORequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDORequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDORequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDORequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDORequestDocNo.Image")));
            this.BtnDORequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDORequestDocNo.Location = new System.Drawing.Point(910, 66);
            this.BtnDORequestDocNo.Name = "BtnDORequestDocNo";
            this.BtnDORequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDORequestDocNo.TabIndex = 65;
            this.BtnDORequestDocNo.ToolTip = "Find DO Request Document";
            this.BtnDORequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDORequestDocNo.ToolTipTitle = "Run System";
            this.BtnDORequestDocNo.Click += new System.EventHandler(this.BtnDORequestDocNo_Click);
            // 
            // TxtDORequestDocNo
            // 
            this.TxtDORequestDocNo.EnterMoveNextControl = true;
            this.TxtDORequestDocNo.Location = new System.Drawing.Point(710, 66);
            this.TxtDORequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDORequestDocNo.Name = "TxtDORequestDocNo";
            this.TxtDORequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDORequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDORequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDORequestDocNo.Properties.MaxLength = 250;
            this.TxtDORequestDocNo.Size = new System.Drawing.Size(195, 20);
            this.TxtDORequestDocNo.TabIndex = 64;
            // 
            // LblDORequestDocNo
            // 
            this.LblDORequestDocNo.AutoSize = true;
            this.LblDORequestDocNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDORequestDocNo.Location = new System.Drawing.Point(624, 69);
            this.LblDORequestDocNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDORequestDocNo.Name = "LblDORequestDocNo";
            this.LblDORequestDocNo.Size = new System.Drawing.Size(82, 14);
            this.LblDORequestDocNo.TabIndex = 63;
            this.LblDORequestDocNo.Text = "DO Request#";
            this.LblDORequestDocNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(98, 409);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 14);
            this.label21.TabIndex = 50;
            this.label21.Text = "File 3";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(140, 426);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(500, 20);
            this.PbUpload3.TabIndex = 55;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(140, 384);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(500, 20);
            this.PbUpload2.TabIndex = 49;
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(557, 404);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 52;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(616, 404);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 54;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(140, 405);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(410, 20);
            this.TxtFile3.TabIndex = 51;
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(585, 404);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 53;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(557, 362);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 46;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(616, 362);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 48;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(140, 363);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(410, 20);
            this.TxtFile2.TabIndex = 45;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(98, 366);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 14);
            this.label17.TabIndex = 44;
            this.label17.Text = "File 2";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(585, 361);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 47;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(557, 317);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 40;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(616, 318);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 42;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(140, 341);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(500, 20);
            this.PbUpload.TabIndex = 43;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(140, 320);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(410, 20);
            this.TxtFile.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(109, 323);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 14);
            this.label9.TabIndex = 38;
            this.label9.Text = "File";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(585, 318);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 41;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // LueOptCode
            // 
            this.LueOptCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LueOptCode.EnterMoveNextControl = true;
            this.LueOptCode.Location = new System.Drawing.Point(710, 25);
            this.LueOptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueOptCode.Name = "LueOptCode";
            this.LueOptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.Appearance.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueOptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueOptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueOptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueOptCode.Properties.DropDownRows = 12;
            this.LueOptCode.Properties.NullText = "[Empty]";
            this.LueOptCode.Properties.PopupWidth = 350;
            this.LueOptCode.Size = new System.Drawing.Size(228, 20);
            this.LueOptCode.TabIndex = 60;
            this.LueOptCode.ToolTip = "F4 : Show/hide list";
            this.LueOptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueOptCode.EditValueChanged += new System.EventHandler(this.LueOptCode_EditValueChanged);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(596, 27);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 14);
            this.label12.TabIndex = 59;
            this.label12.Text = "Procurement Type";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(52, 174);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 14);
            this.label13.TabIndex = 26;
            this.label13.Text = "Budget for MR";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBudgetForMR
            // 
            this.TxtBudgetForMR.EnterMoveNextControl = true;
            this.TxtBudgetForMR.Location = new System.Drawing.Point(140, 172);
            this.TxtBudgetForMR.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBudgetForMR.Name = "TxtBudgetForMR";
            this.TxtBudgetForMR.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBudgetForMR.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBudgetForMR.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBudgetForMR.Properties.Appearance.Options.UseFont = true;
            this.TxtBudgetForMR.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBudgetForMR.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBudgetForMR.Properties.ReadOnly = true;
            this.TxtBudgetForMR.Size = new System.Drawing.Size(152, 20);
            this.TxtBudgetForMR.TabIndex = 27;
            // 
            // LblMRType
            // 
            this.LblMRType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblMRType.AutoSize = true;
            this.LblMRType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMRType.ForeColor = System.Drawing.Color.Red;
            this.LblMRType.Location = new System.Drawing.Point(651, 48);
            this.LblMRType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblMRType.Name = "LblMRType";
            this.LblMRType.Size = new System.Drawing.Size(55, 14);
            this.LblMRType.TabIndex = 61;
            this.LblMRType.Text = "MR Type";
            this.LblMRType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueMRType
            // 
            this.LueMRType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LueMRType.EnterMoveNextControl = true;
            this.LueMRType.Location = new System.Drawing.Point(710, 45);
            this.LueMRType.Margin = new System.Windows.Forms.Padding(5);
            this.LueMRType.Name = "LueMRType";
            this.LueMRType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.Appearance.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMRType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMRType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMRType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMRType.Properties.DropDownRows = 12;
            this.LueMRType.Properties.NullText = "[Empty]";
            this.LueMRType.Properties.PopupWidth = 350;
            this.LueMRType.Size = new System.Drawing.Size(228, 20);
            this.LueMRType.TabIndex = 62;
            this.LueMRType.ToolTip = "F4 : Show/hide list";
            this.LueMRType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMRType.EditValueChanged += new System.EventHandler(this.LueMRType_EditValueChanged);
            // 
            // LblPOSignBy
            // 
            this.LblPOSignBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblPOSignBy.AutoSize = true;
            this.LblPOSignBy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPOSignBy.ForeColor = System.Drawing.Color.Black;
            this.LblPOSignBy.Location = new System.Drawing.Point(641, 90);
            this.LblPOSignBy.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPOSignBy.Name = "LblPOSignBy";
            this.LblPOSignBy.Size = new System.Drawing.Size(67, 14);
            this.LblPOSignBy.TabIndex = 67;
            this.LblPOSignBy.Text = "PO Sign By";
            this.LblPOSignBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePOSignBy
            // 
            this.LuePOSignBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LuePOSignBy.EnterMoveNextControl = true;
            this.LuePOSignBy.Location = new System.Drawing.Point(710, 87);
            this.LuePOSignBy.Margin = new System.Windows.Forms.Padding(5);
            this.LuePOSignBy.Name = "LuePOSignBy";
            this.LuePOSignBy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePOSignBy.Properties.Appearance.Options.UseFont = true;
            this.LuePOSignBy.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePOSignBy.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePOSignBy.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePOSignBy.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePOSignBy.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePOSignBy.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePOSignBy.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePOSignBy.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePOSignBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePOSignBy.Properties.DropDownRows = 12;
            this.LuePOSignBy.Properties.NullText = "[Empty]";
            this.LuePOSignBy.Properties.PopupWidth = 350;
            this.LuePOSignBy.Size = new System.Drawing.Size(228, 20);
            this.LuePOSignBy.TabIndex = 68;
            this.LuePOSignBy.ToolTip = "F4 : Show/hide list";
            this.LuePOSignBy.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePOSignBy.EditValueChanged += new System.EventHandler(this.LuePOSignBy_EditValueChanged);
            // 
            // FrmMaterialRequest2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 579);
            this.Name = "FrmMaterialRequest2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReqType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRemainingBudget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdContactPersonName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDORequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueOptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBudgetForMR.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMRType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePOSignBy.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueReqType;
        internal DevExpress.XtraEditors.TextEdit TxtRemainingBudget;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode1;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode3;
        internal DevExpress.XtraEditors.TextEdit TxtTaxAmt;
        private DevExpress.XtraEditors.LookUpEdit LueTaxCode2;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteEstRecvDt;
        private DevExpress.XtraEditors.LookUpEdit LueVdContactPersonName;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.LookUpEdit LueBCCode;
        private System.Windows.Forms.Label LblShipInstruction;
        public DevExpress.XtraEditors.SimpleButton BtnSI;
        public DevExpress.XtraEditors.TextEdit TxtSIDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label LblSiteCode;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnDORequestDocNo;
        protected internal DevExpress.XtraEditors.TextEdit TxtDORequestDocNo;
        private System.Windows.Forms.Label LblDORequestDocNo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ProgressBar PbUpload3;
        private System.Windows.Forms.ProgressBar PbUpload2;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private DevExpress.XtraEditors.LookUpEdit LueOptCode;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtBudgetForMR;
        private System.Windows.Forms.Label LblMRType;
        private DevExpress.XtraEditors.LookUpEdit LueMRType;
        private System.Windows.Forms.Label LblPOSignBy;
        private DevExpress.XtraEditors.LookUpEdit LuePOSignBy;
    }
}