﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequest2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mIsPrintOutVR;
        internal FrmVoucherRequestFind FrmFind;
        private string mMInd = "N";
        private bool mIsRemarkForApprovalMandatory = false;
        internal bool 
            mIsUseMInd = false,
            mIsFilterByDept = false,
            mIsAutoGeneratePurchaseLocalDocNo = false;
        
        #endregion

        #region Constructor

        public FrmVoucherRequest2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Voucher Request";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { TxtDocNo, TxtVoucherDocNo }, true);
                SetFormControl(mState.View);
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueVoucherDocType(ref LueDocType);
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueAcType(ref LueAcType2);
                Sl.SetLueBankAcCode(ref LueBankAcCode2);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueCurCode(ref LueCurCode);
                SetGrd();
                SetGrd2();
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
       
        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Grid 1

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-3
                    "Description",
                    "Amount",
                    "Remark"
                },
                new int[]{ 0, 300, 130, 350 }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0}, false);
        }

        #endregion

        #region Grid 2

        private void SetGrd2()
        {
            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[]{ 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
            Sm.GrdColReadOnly(Grd2, new int[] {0, 1, 2, 3, 4});
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocalDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, LueDeptCode, 
                        LueDocType, LueAcType, LueBankAcCode, LueAcType2, LueBankAcCode2, LueCurCode, LuePaymentType, 
                        LueBankCode, TxtGiroNo, DteDueDt, TxtAmt, TxtCurCode2, TxtExcRate, 
                        LuePIC, MeeRemark, TxtPaymentUser, TxtBankAcNo, 
                        TxtBankCode, TxtBankBranch, TxtBankAcName
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                    Sm.SetLue(LueDocType, Sm.GetValue("Select OptCode From TblOption Where OptDesc = 'Manual' And OptCat = 'VoucherDocType'"));
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueAcType, LueBankAcCode, LueAcType2, 
                        LueBankAcCode2, LueCurCode, LuePaymentType, LuePIC, MeeRemark, 
                        TxtPaymentUser, TxtBankAcNo, TxtBankCode, TxtBankBranch, 
                        TxtBankAcName, MeeCancelReason, TxtCurCode2, TxtExcRate
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    ChkCancelInd.Checked = false;
                    Grd1.ReadOnly = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, TxtVoucherDocNo, DteDocDt, LueDeptCode, 
                LueDocType, LueAcType, LueBankAcCode, LueAcType2, LueBankAcCode2, 
                LueCurCode, ChkCancelInd, LuePaymentType, LueBankCode, TxtGiroNo, 
                DteDueDt, TxtAmt, LuePIC, MeeRemark, TxtPaymentUser, 
                TxtBankAcNo, TxtBankCode, TxtBankBranch, TxtBankAcName, MeeCancelReason,
                TxtCurCode2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtAmt, TxtExcRate }, 0);
            ChkCancelInd.Checked = false; 
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
            
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestFind(this, mMInd);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                SetLueDeptCode(ref LueDeptCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblVoucherRequestHdr Where DocNo=@DocNo" };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (
                        Sm.StdMsgYN("Save", "") == DialogResult.No || 
                        IsDataNotValid()
                        ) return;

                    Cursor.Current = Cursors.WaitCursor;

                    string DocNo = GenerateDocNo();
                    bool NoNeedApproval = IsDocApprovalSettingNotExisted();

                    var cml = new List<MySqlCommand>();

                    cml.Add(SaveVoucherRequestHdr(DocNo, NoNeedApproval));
                    if (Grd1.Rows.Count > 1)
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                            if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveVoucherRequestDtl(DocNo, Row));
                    }
                    Sm.ExecCommands(cml);

                    ShowData(DocNo);
                }
                else
                {
                    if (Sm.GetLue(LueDocType) == "M")
                        CancelData();
                    else
                        Sm.StdMsg(mMsgType.Warning, "Unable to cancel this voucher, " + Environment.NewLine + " please proceed to Outgoing Payment");
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "VoucherReq", "VoucherReqDtl" };

            var l = new List<VoucherReqHdr>();
            var ldtl = new List<VoucherReqDtl>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine(" A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CancelInd, A.DocType, ");
            SQL.AppendLine(" A.AcType, A.VoucherDocNo, ");
            SQL.AppendLine(" (Select DATE_FORMAT(DocDt,'%d %M %Y') As DocDt from tblvoucherhdr Where DocNo=A.VoucherDocNo Limit 1) As VoucherDocDt, ");
            SQL.AppendLine(" Concat(IfNull(C.BankAcNo, ''), ' [', IfNull(C.BankAcNm, ''), ']') As BankAcc, ");
            SQL.AppendLine(" (Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1) As PaymentType, ");
            SQL.AppendLine(" A.GiroNo,E.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, F.UserName As PICName, A.Amt As AmtHdr, A.Remark As RemarkHdr, A.CurCode, A.DocEnclosure, A.PaymentUser, G.DeptName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='IsPrintOutVoucherRequestShowDocument') As PrintFormat ");
            SQL.AppendLine(" From TblVoucherRequestHdr A ");
            SQL.AppendLine(" Left Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine(" Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine(" Left Join TblBank E On A.BankCode=E.BankCode ");
            SQL.AppendLine(" Left Join TblUser F On A.PIC=F.UserCode ");
            SQL.AppendLine(" Left Join TblDepartment G On A.DeptCode = G.DeptCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 
                         "DocDt", 
                        
 
                         //6-10
                         "CancelInd",
                         "DocType", 
                         "AcType",
                         "VoucherDocNo", 
                         "VoucherDocDt", 
                          
                         
                         //11-15
                         "BankAcc",
                         "PaymentType",
                         "GiroNo",
                         "GiroBankName", 
                         "GiroDueDt", 
                         
                         //16-20
                         "PICName",
                         "AmtHdr", 
                         "RemarkHdr",
                         "CompanyLogo",
                         "CurCode",
                         
                         //21-24
                         "DocEnclosure",
                         "PaymentUser",
                         "DeptName",
                         "PrintFormat",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VoucherReqHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),
                            CompanyFax = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            CancelInd = Sm.DrStr(dr, c[6]),
                            DocType = Sm.DrStr(dr, c[7]),
                            AcType = Sm.DrStr(dr, c[8]),
                            VoucherDocNo = Sm.DrStr(dr, c[9]),
                            VoucherDocDt = Sm.DrStr(dr, c[10]),
                            BankAcc = Sm.DrStr(dr, c[11]),
                            PaymentType = Sm.DrStr(dr, c[12]),
                            GiroNo = Sm.DrStr(dr, c[13]),
                            GiroBankName = Sm.DrStr(dr, c[14]),
                            GiroDueDt = Sm.DrStr(dr, c[15]),
                            PICName = Sm.DrStr(dr, c[16]),
                            AmtHdr = Sm.DrDec(dr, c[17]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[17])),
                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[17])),
                            RemarkHdr = Sm.DrStr(dr, c[18]),

                            CompanyLogo = Sm.DrStr(dr, c[19]),
                            CurCode = Sm.DrStr(dr, c[20]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            DocEnclosure = Sm.DrDec(dr, c[21]),
                            PaymentUser = Sm.DrStr(dr, c[22]),
                            Department = Sm.DrStr(dr, c[23]),
                            PrintFormat = Sm.DrStr(dr, c[24]),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = "Select DNo, Description, Amt, Remark from TblVoucherRequestDtl Where DocNo=@DocNo";
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "Description",
                         "Amt",
                         "Remark" 
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new VoucherReqDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            Description = Sm.DrStr(drDtl, cDtl[1]),
                            Amt = Sm.DrDec(drDtl, cDtl[2]),
                            Remark = Sm.DrStr(drDtl, cDtl[3])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            Sm.PrintReport(mIsPrintOutVR, myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = 0;
        }
       
        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }
     
        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 2 || e.ColIndex == 1 || e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                    Grd1.Cells[e.RowIndex, e.ColIndex].Value = 0m;
                ComputeAmt();
            }
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelVoucherRequestHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsVoucherRequestNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsVoucherRequestNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this voucher request.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblVoucherRequestHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
                TxtDocNo.Text, 
                "Data already processed into voucher.");
        }

        private MySqlCommand CancelVoucherRequestHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVoucherRequestHdr(DocNo);
                ShowVoucherRequestDtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowVoucherRequestHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, LocalDocNo, DocDt, DeptCode, CancelReason, CancelInd, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, ");
            SQL.AppendLine("PaymentType, GiroNo, BankCode, DueDt, PIC, CurCode, Amt, DocEnclosure, PaymentUser, Remark, ");
            SQL.AppendLine("PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, CurCode2, ExcRate ");
            SQL.AppendLine("From TblVoucherRequestHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "LocalDocNo", "DocDt", "CancelInd", "DocType", "VoucherDocNo", 

                    //6-10
                    "AcType", "BankAcCode", "AcType2", "BankAcCode2", "PaymentType", 
                    
                    //11-15
                    "GiroNo", "BankCode", "DueDt", "PIC", "CurCode",
                    
                    //16-20
                    "Amt", "CancelReason", "PaymentUser", "Remark", "DeptCode", 
                    
                    //21-25
                    "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcNo", "PaidToBankAcName", "CurCode2",

                    //26
                    "ExcRate"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                     Sm.SetLue(LueDocType, Sm.DrStr(dr, c[4]));
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[5]);
                     Sm.SetLue(LueAcType, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[7]));
                     Sm.SetLue(LueAcType2, Sm.DrStr(dr, c[8]));
                     Sm.SetLue(LueBankAcCode2, Sm.DrStr(dr, c[9]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[10]));
                     TxtGiroNo.EditValue = Sm.DrStr(dr, c[11]);
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[12]));
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[13]));
                     Sm.SetLue(LuePIC, Sm.DrStr(dr, c[14]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[15]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[17]);
                     TxtPaymentUser.EditValue = Sm.DrStr(dr, c[18]);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                     SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[20]));
                     Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[20]));
                     TxtBankCode.EditValue = Sm.DrStr(dr, c[21]);
                     TxtBankBranch.EditValue = Sm.DrStr(dr, c[22]);
                     TxtBankAcNo.EditValue = Sm.DrStr(dr, c[23]);
                     TxtBankAcName.EditValue = Sm.DrStr(dr, c[24]);
                     TxtCurCode2.EditValue = Sm.DrStr(dr, c[25]);
                     TxtExcRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[26]), 0);
                 }, true
             );
        }

        private void ShowVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select DNo,Description,Amt,Remark From TblVoucherRequestDtl "+
                    "Where DocNo=@DocNo Order By Dno",

                    new string[] 
                    { 
                        "DNo","Description","Amt","Remark",

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);

                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select A.ApprovalDNo, B.UserName, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, "+
                    "Case When A.LastUpDt Is Not Null Then  "+
	                "A.LastUpDt Else Null End As LastUpDt, A.Remark "+ 
                    "From TblDocApproval A "+
					"Left Join TblUser B On A.UserCode = B.UserCode "+
                    "Where A.DocType='VoucherRequest' "+ 
                    "And Status In ('A', 'C') "+ 
                    "And A.DocNo=@DocNo "+
                    "Order By A.ApprovalDNo ", 
                    new string[] 
                    { 
                        "ApprovalDNo",
                        "UserName","StatusDesc","LastUpDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd2, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }
    
        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueDocType, "Document Type") ||
                Sm.IsLueEmpty(LueAcType, "Debit/Credit") ||
                Sm.IsLueEmpty(LueBankAcCode, LblBankAcCode.Text) ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", false) ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                Sm.IsLueEmpty(LuePIC, "Person In Charge") ||
                (mIsRemarkForApprovalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                IsPaymentTypeNotValid() ||
                (IsBankAccountCurrencyNotValid(Sm.GetLue(LueBankAcCode), Sm.GetLue(LueCurCode))) ||
                IsAcType2NotValid() ||
                IsBankAcNo2NotValid() ||
                IsGrdEmpty()||
                IsGrdValueNotValid();
        }

        private bool IsAcType2NotValid()
        {
            var AcType2 = Sm.GetLue(LueAcType2);

            if (AcType2.Length != 0)
            {
                if (AcType2 == Sm.GetLue(LueAcType))
                {
                    Sm.StdMsg(mMsgType.Warning, "Both Debit/Credit should not be the same.");
                    return true;
                }
            
                if (Sm.IsLueEmpty(LueBankAcCode2, LblBankAcCode2.Text)) return true;
            }
            return false;
        }

        private bool IsBankAcNo2NotValid()
        {
            var BankAcCode2 = Sm.GetLue(LueBankAcCode2);
            if (BankAcCode2.Length != 0)
            {
                if (BankAcCode2 == Sm.GetLue(LueBankAcCode))
                {
                    Sm.StdMsg(mMsgType.Warning, "Both debit/credit to should not be the same.");
                    return true;
                }
                if (Sm.IsLueEmpty(LueAcType2, "Debit/Credit")) return true; 
                if (IsBankAccountCurrencyNotValid(BankAcCode2, Sm.GetLue(LueCurCode))) return true;
            }
            return false;
        }

        private bool IsBankAccountCurrencyNotValid(string BankAcCode, string CurCode)
        {
            var cm = new MySqlCommand(){ CommandText = "Select CurCode From TblBankAccount Where BankAcCode=@BankAcCode;" };
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
            var BankAccountCurCode = Sm.GetValue(cm);
            if (!Sm.CompareStr(CurCode, BankAccountCurCode))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Bank account's currency : " + CurCode + Environment.NewLine +
                    "Document's currency : " + BankAccountCurCode + Environment.NewLine + Environment.NewLine + 
                    "Both currency should not be different.");
                return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.GetLue(LuePaymentType) == "B") 
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true; 
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Bilyet Number ",false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 )
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in description list or amount list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Description is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Description : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine + "Amount is 0.")) return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocType From TblDocApprovalSetting " +
                    "Where UserCode Is not Null And DeptCode =@DeptCode And DocType='VoucherRequest' Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            if (!Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private MySqlCommand SaveVoucherRequestHdr(string DocNo, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, LocalDocNo,  DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, BankAcCode, AcType2, BankAcCode2, PaymentType, GiroNo, ");
            SQL.AppendLine("BankCode, DueDt, PIC, CurCode, Amt, CurCode2, ExcRate, PaymentUser, ");
            SQL.AppendLine("PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @LocalDocNo, @DocDt, 'N', 'O', @MInd, @DeptCode, @DocType, @VoucherDocNo, ");
            SQL.AppendLine("@AcType, @BankAcCode, @AcType2, @BankAcCode2, @PaymentType, @GiroNo, ");
            SQL.AppendLine("@BankCode, @DueDt, @PIC, @CurCode, @Amt, @CurCode2, @ExcRate, @PaymentUser,");
            SQL.AppendLine("@PaidToBankCode, @PaidToBankBranch, @PaidToBankAcNo, @PaidToBankAcName, @Remark, @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='VoucherRequest' And T.DeptCode=@DeptCode ");
                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                SQL.AppendLine("    From TblVoucherRequestHdr A ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
                SQL.AppendLine("        From TblCurrencyRate B1 ");
                SQL.AppendLine("        Inner Join ( ");
                SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                SQL.AppendLine("            From TblCurrencyRate ");
                SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                SQL.AppendLine("            Group By CurCode1 ");
                SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<String>(ref cm, "@MInd", mMInd);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", TxtVoucherDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@AcType2", Sm.GetLue(LueAcType2));
            Sm.CmParam<String>(ref cm, "@BankAcCode2", Sm.GetLue(LueBankAcCode2));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode2", TxtCurCode2.Text);
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", TxtBankCode.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo,int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo,DNo,Description,Amt,Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
       
        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameter("IsAutoGeneratePurchaseLocalDocNo") == "Y";
            mIsRemarkForApprovalMandatory = Sm.GetParameter("IsVoucherRequestRemarkForApprovalMandatory") == "Y";
            string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            if (MenuCodeForDocWithMInd.Length > 0)
                mMInd =
                    MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1 ?
                        "Y" : "N";
            mIsUseMInd = Sm.GetParameter("IsUseMInd") == "Y";
            mIsFilterByDept = Sm.GetParameter("IsFilterByDept") == "Y";
            mIsPrintOutVR = Sm.GetParameter("FormPrintOutVR");
            if (mIsPrintOutVR.Length == 0) mIsPrintOutVR = "VoucherRequest";
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");

            if (DeptCode.Length!=0)
                SQL.AppendLine("Where DeptCode=@DeptCode ");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                if (mIsFilterByDept)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
            Grd2.Rows.Clear();
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 )
                        Amt += Sm.GetGrdDec(Grd1, Row, 2);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private string GenerateDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");
            string type; 
            if (Sm.GetLue(LueAcType) == "C")
            {
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            }
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '"+Sm.GetLue(LueBankAcCode)+"' ");

            var SQL = new StringBuilder();

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '"+type.Length+"') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDeptCode), string.Empty);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void TxtVoucherDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVoucherDocNo);
        }
        
        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueBankCode, TxtGiroNo, DteDueDt
            });

            if (Sm.GetLue(LuePaymentType) == "C")
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
            else if (Sm.GetLue(LuePaymentType) == "B")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
            }
            else
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
            if (Sm.GetLue(LueAcType) == "C")
            {
                LblBankAcCode.Text = "           Credit To";
                LblPaymentUser.Text = "                   Paid To";
            }
            else if (Sm.GetLue(LueAcType) == "D")
            {
                LblBankAcCode.Text = "           Debit To";
                LblPaymentUser.Text = "             Received By";
            }
            else
            {
                LblBankAcCode.Text = "Debit / Credit To";
                LblPaymentUser.Text = "Paid To / Received By";
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueAcType2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType2, new Sm.RefreshLue1(Sl.SetLueAcType));
            if (Sm.GetLue(LueAcType2) == "C")
            {
                LblBankAcCode2.Text = "           Credit To";
            }
            else if (Sm.GetLue(LueAcType2) == "D")
            {
                LblBankAcCode2.Text = "           Debit To";
            }
            else
            {
                LblBankAcCode2.Text = "Debit / Credit To";
            }
        }

        private void LueBankAcCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode2, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new StdMtd.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtExcRate_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtExcRate, 0);
        }

        #endregion

        #endregion  
      
        #region Report Class

        private class VoucherReqHdr
        {
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CancelInd { get; set; }
            public string DocType { get; set; }
            public string AcType { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocDt { get; set; }
            public string BankAcc { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string PICName { get; set; }
            public decimal AmtHdr { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string RemarkHdr { get; set; }
            public string CompanyLogo { get; set; }
            public string PrintBy { get; set; }
            public string CurCode { get; set; }
            public decimal DocEnclosure { get; set; }
            public string PaymentUser { get; set; }
            public string Department { get; set; }
            public string PrintFormat { get; set; }
        }

        private class VoucherReqDtl
        {
            public string DNo { get; set; }
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        #endregion
    }
}
