﻿#region Update
/*
    11/01/2018 [TKG] Annual/Long Service Leave Processing
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeaveProcess : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpLeaveProcess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                var CurrentDate = Sm.ServerCurrentDateTime();
                DteStartDt.DateTime = Sm.ConvertDate(CurrentDate); 
                DteEndDt.DateTime = Sm.ConvertDate(string.Concat(Sm.Left(CurrentDate, 4), "1231"));                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsFilterByDateInvalid(ref DteStartDt, ref DteEndDt) ||
                IsYrInvalid()
                ) return;

            try
            {
                ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsYrInvalid()
        {
            if (!Sm.CompareStr(Sm.Left(Sm.GetDte(DteStartDt), 4), Sm.Left(Sm.GetDte(DteEndDt), 4)))
            {
                Sm.StdMsg(mMsgType.Warning, "Both years should be the same.");
                return true;
            }
            return false;

        }

        private void ProcessData()
        {
            var lEmployee = new List<Employee>();
            Process1(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                Process2(ref lEmployee);
            }
            lEmployee.Clear();
        }

        private void Process1(ref List<Employee> l)
        {
            // ambil data employee
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var CurrentDate = Sm.ServerCurrentDateTime();

            SQL.AppendLine("Select EmpCode, JoinDt, ResignDt, LeaveStartDt ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where JoinDt<=@EndDt ");
            SQL.AppendLine("And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>=@StartDt)) ");
            SQL.AppendLine("Order By EmpCode; ");
            
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", 
                    "JoinDt", "ResignDt", "LeaveStartDt" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            PermanentDt = Sm.DrStr(dr, c[3]),
                            LeaveCode = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Employee> l)
        {
            // Set Leave Code
            string
                AnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode"),
                LongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode"),
                LongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
            decimal
                JoinYr = 0m,
                Yr = decimal.Parse(Sm.Left(Sm.GetDte(DteStartDt), 4)),
                y = 0m,
                LongServiceLeaveNoOfYr = Sm.GetParameterDec("LongServiceLeaveNoOfYr"),
                LongServiceLeaveNoOfYrToActive = Sm.GetParameterDec("LongServiceLeaveNoOfYrToActive"),
                LongServiceLeaveNoOfYrToActive2 = Sm.GetParameterDec("LongServiceLeaveNoOfYrToActive2");

            for (int i = 0; i < l.Count; i++)
            {
                l[i].LeaveCode = AnnualLeaveCode;
                JoinYr = decimal.Parse(Sm.Left(l[i].JoinDt, 4));
                
                y = JoinYr + LongServiceLeaveNoOfYrToActive;
                while (y <= Yr)
                {
                    if (y == Yr)
                    {
                        l[i].LeaveCode = LongServiceLeaveCode;
                        break;
                    }
                    y += LongServiceLeaveNoOfYr;
                }

                y = JoinYr + LongServiceLeaveNoOfYrToActive2;
                while (y <= Yr)
                {
                    if (y == Yr)
                    {
                        l[i].LeaveCode = LongServiceLeaveCode2;
                        break;
                    }
                    y += LongServiceLeaveNoOfYr;
                }

                if (Sm.CompareStr(l[i].LeaveCode, AnnualLeaveCode))
                    l[i].StartDt = string.Concat(Yr, Sm.Right(l[i].JoinDt, 4));
                else
                    l[i].StartDt = string.Concat(Yr, Sm.Right(l[i].PermanentDt, 4));
                    
                l[i].EndDt = Sm.Left(Sm.FormatDate((Sm.ConvertDate(l[i].StartDt).AddYears(1)).AddDays(-1)), 8);
            }
        }

        #region Event

        #region Misc Control Event

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            var StartDt = Sm.GetDte(DteStartDt);

            if (StartDt.Length > 0)
                DteEndDt.DateTime = Sm.ConvertDate(string.Concat(Sm.Left(StartDt, 4), "1231"));
            else
                DteEndDt.DateTime = Sm.ConvertDate(string.Concat(Sm.Left(Sm.ServerCurrentDateTime(), 4), "1231"));
        }

        #endregion

        #endregion

        #region Class

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PermanentDt { get; set; }
            public string LeaveCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
        }

        #endregion
    }
}
