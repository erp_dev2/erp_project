﻿namespace RunSystem
{
    partial class FrmProjectDeliveryDlg2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueStageCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkStageCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueTaskCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkTaskCode = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueTypeCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkTypeCode = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStageCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaskCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTypeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTypeCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueTypeCode);
            this.panel2.Controls.Add(this.ChkTypeCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueTaskCode);
            this.panel2.Controls.Add(this.ChkTaskCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueStageCode);
            this.panel2.Controls.Add(this.ChkStageCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(672, 71);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 402);
            this.Grd1.TabIndex = 16;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // LueStageCode
            // 
            this.LueStageCode.EnterMoveNextControl = true;
            this.LueStageCode.Location = new System.Drawing.Point(47, 3);
            this.LueStageCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueStageCode.Name = "LueStageCode";
            this.LueStageCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.Appearance.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStageCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStageCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStageCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStageCode.Properties.DropDownRows = 30;
            this.LueStageCode.Properties.NullText = "[Empty]";
            this.LueStageCode.Properties.PopupWidth = 300;
            this.LueStageCode.Size = new System.Drawing.Size(245, 20);
            this.LueStageCode.TabIndex = 11;
            this.LueStageCode.ToolTip = "F4 : Show/hide list";
            this.LueStageCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStageCode.EditValueChanged += new System.EventHandler(this.LueStageCode_EditValueChanged);
            // 
            // ChkStageCode
            // 
            this.ChkStageCode.Location = new System.Drawing.Point(296, 2);
            this.ChkStageCode.Name = "ChkStageCode";
            this.ChkStageCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkStageCode.Properties.Appearance.Options.UseFont = true;
            this.ChkStageCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkStageCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkStageCode.Properties.Caption = " ";
            this.ChkStageCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkStageCode.Size = new System.Drawing.Size(19, 22);
            this.ChkStageCode.TabIndex = 12;
            this.ChkStageCode.ToolTip = "Remove filter";
            this.ChkStageCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkStageCode.ToolTipTitle = "Run System";
            this.ChkStageCode.CheckedChanged += new System.EventHandler(this.ChkStageCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(7, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 14);
            this.label4.TabIndex = 10;
            this.label4.Text = "Stage";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaskCode
            // 
            this.LueTaskCode.EnterMoveNextControl = true;
            this.LueTaskCode.Location = new System.Drawing.Point(47, 24);
            this.LueTaskCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaskCode.Name = "LueTaskCode";
            this.LueTaskCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaskCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaskCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaskCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaskCode.Properties.DropDownRows = 30;
            this.LueTaskCode.Properties.NullText = "[Empty]";
            this.LueTaskCode.Properties.PopupWidth = 300;
            this.LueTaskCode.Size = new System.Drawing.Size(245, 20);
            this.LueTaskCode.TabIndex = 14;
            this.LueTaskCode.ToolTip = "F4 : Show/hide list";
            this.LueTaskCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaskCode.EditValueChanged += new System.EventHandler(this.LueTaskCode_EditValueChanged);
            // 
            // ChkTaskCode
            // 
            this.ChkTaskCode.Location = new System.Drawing.Point(296, 23);
            this.ChkTaskCode.Name = "ChkTaskCode";
            this.ChkTaskCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaskCode.Properties.Appearance.Options.UseFont = true;
            this.ChkTaskCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTaskCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTaskCode.Properties.Caption = " ";
            this.ChkTaskCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaskCode.Size = new System.Drawing.Size(19, 22);
            this.ChkTaskCode.TabIndex = 15;
            this.ChkTaskCode.ToolTip = "Remove filter";
            this.ChkTaskCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTaskCode.ToolTipTitle = "Run System";
            this.ChkTaskCode.CheckedChanged += new System.EventHandler(this.ChkTaskCode_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(14, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Task";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTypeCode
            // 
            this.LueTypeCode.EnterMoveNextControl = true;
            this.LueTypeCode.Location = new System.Drawing.Point(47, 45);
            this.LueTypeCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTypeCode.Name = "LueTypeCode";
            this.LueTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.Appearance.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTypeCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTypeCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTypeCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTypeCode.Properties.DropDownRows = 30;
            this.LueTypeCode.Properties.NullText = "[Empty]";
            this.LueTypeCode.Properties.PopupWidth = 300;
            this.LueTypeCode.Size = new System.Drawing.Size(245, 20);
            this.LueTypeCode.TabIndex = 17;
            this.LueTypeCode.ToolTip = "F4 : Show/hide list";
            this.LueTypeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTypeCode.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            // 
            // ChkTypeCode
            // 
            this.ChkTypeCode.Location = new System.Drawing.Point(296, 44);
            this.ChkTypeCode.Name = "ChkTypeCode";
            this.ChkTypeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTypeCode.Properties.Appearance.Options.UseFont = true;
            this.ChkTypeCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkTypeCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkTypeCode.Properties.Caption = " ";
            this.ChkTypeCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTypeCode.Size = new System.Drawing.Size(19, 22);
            this.ChkTypeCode.TabIndex = 18;
            this.ChkTypeCode.ToolTip = "Remove filter";
            this.ChkTypeCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkTypeCode.ToolTipTitle = "Run System";
            this.ChkTypeCode.CheckedChanged += new System.EventHandler(this.ChkType_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(11, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 16;
            this.label2.Text = "Type";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmProjectDeliveryDlg2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmProjectDeliveryDlg2";
            this.Text = "List of WBS";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStageCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkStageCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaskCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaskCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTypeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTypeCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueTaskCode;
        private DevExpress.XtraEditors.CheckEdit ChkTaskCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueStageCode;
        private DevExpress.XtraEditors.CheckEdit ChkStageCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueTypeCode;
        private DevExpress.XtraEditors.CheckEdit ChkTypeCode;
        private System.Windows.Forms.Label label2;
    }
}