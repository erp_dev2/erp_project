﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmASAProcess : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmASAProcess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                DteDocDt.Focus();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<BaseEdit> 
            {
               DteDocDt, LueWhsCode, TxtFileName
            });
            DteDocDt.Focus();
        }

        #endregion

        #region Button Methods

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsTxtEmpty(TxtFileName, "File (CSV)", false) ||
                Sm.StdMsgYN("Process", "") == DialogResult.No)
                return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ProcessData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        private void ProcessData()
        {
            var l = new List<Process>();
            Process1(ref l);
            if (l.Count > 0)
            {
                Process2(ref l);
                Sm.StdMsg(mMsgType.Info, l.Count() + " data has been processed.");
            }
        }

        private void Process1(ref List<Process> l)
        {
            using (var rd = new StreamReader(TxtFileName.Text))
            {    
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(',');
                    if (splits.Count() != 2)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid csv's columns.");
                        return;
                    }
                    else
                    {
                        var arr = splits.ToArray();
                        if (arr[0].Trim().Length > 0)
                        {
                            l.Add(new Process()
                            {
                                Source1 = splits[0].Trim(),
                                Price2 = Decimal.Parse(splits[1].Trim().Length > 0 ? splits[1].Trim() : "0"),
                                Source2 = string.Empty
                            });
                        }
                        else
                            return;
                    }
                }
            }
        }

        private void Process2(ref List<Process> l)
        {
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Mutation", "TblMutationHdr");
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblMutationHdr (DocNo, DocDt, WhsCode, JournalDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @WhsCode, Null, Null, 'System', CurrentDateTime());");

            for (int i = 0; i < l.Count; i++)
            {
                Sm.CmParam<String>(ref cm, "@DNo0" + i.ToString(), Sm.Right("00" + (i + 1).ToString(), 3));
                Sm.CmParam<String>(ref cm, "@Source1_" + i.ToString(), l[i].Source1);
                Sm.CmParam<String>(ref cm, "@Source2_" + i.ToString(), string.Concat("12*", DocNo, "*", Sm.Right("00" + (i + 1).ToString(), 3)));
                Sm.CmParam<Decimal>(ref cm, "@UPrice2_" + i.ToString(), l[i].Price2);

                SQL.AppendLine("Insert Into TblMutationDtl(DocNo, DNo, CancelInd, ItCodeFrom, BatchNoFrom, SourceFrom, PropCodeFrom, Lot, Bin, QtyFrom, Qty2From, Qty3From, ItCodeTo, BatchNoTo, SourceTo, PropCodeTo, QtyTo, Qty2To, Qty3To, ItemCostDocNo, ItemCostDNo, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocNo, @DNo0"+i.ToString() + ", 'N', ItCode, BatchNo, Source, '-', '-', '-', Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3, ItCode, BatchNo,");
                SQL.AppendLine("@Source2_" + i.ToString() + ", '-', Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3, Null, Null, Null, 'System', CurrentDateTime() ");
                SQL.AppendLine("From TblStockMovement ");
                SQL.AppendLine(" Where Source=@Source1_" + i.ToString());
                SQL.AppendLine(" And WhsCode=@WhsCode ");
                SQL.AppendLine(" And DocDt<@DocDt ");
                SQL.AppendLine(" And Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And DocDt<@DocDt ");
                SQL.AppendLine("    Limit 1 ) ");
                SQL.AppendLine(" Group By ItCode, BatchNo, Source; ");

                SQL.AppendLine("Insert Into TblMutationDtl(DocNo, DNo, CancelInd, ItCodeFrom, BatchNoFrom, SourceFrom, PropCodeFrom, Lot, Bin, QtyFrom, Qty2From, Qty3From, ItCodeTo, BatchNoTo, SourceTo, PropCodeTo, QtyTo, Qty2To, Qty3To, ItemCostDocNo, ItemCostDNo, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocNo, @DNo0" + i.ToString() + ", 'N', ItCode, BatchNo, Source, '-', '-', '-', 0.00 Qty, 0.00 Qty2, 0.00 Qty3, ItCode, BatchNo,");
                SQL.AppendLine("@Source2_" + i.ToString() + ", '-', 0.00 Qty, 0.00 Qty2, 0.00 Qty3, Null, Null, Null, 'System', CurrentDateTime() ");
                SQL.AppendLine("From TblStockSummary ");
                SQL.AppendLine(" Where Source=@Source1_" + i.ToString());
                SQL.AppendLine(" And WhsCode=@WhsCode ");
                SQL.AppendLine(" And Not Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And DocDt<@DocDt ");
                SQL.AppendLine("    Limit 1 ) ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, PropCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select WhsCode, Lot, Bin, ItCode, BatchNo, @Source2_" + i.ToString() + ", '-', Qty, Qty2, Qty3, Null, 'System', CurrentDateTime() ");
                SQL.AppendLine("From TblStockSummary ");
                SQL.AppendLine("Where WhsCode=@WhsCode And Source=@Source1_"+i.ToString()+"; ");

                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=0.00, Qty2=0.00, Qty3=0.00, LastUpBy='System', LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where WhsCode=@WhsCode And Source=@Source1_" + i.ToString() + "; ");

                SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select ItCode, BatchNo, @Source2_" + i.ToString() + ", CurCode, @UPrice2_" + i.ToString() + ", 1.00, Null, 'System', CurrentDateTime() ");
                SQL.AppendLine("From TblStockPrice ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + "; ");

                SQL.AppendLine("Update TblDOVdDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='02' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblStockAdjustmentDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='03' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblStockInitialDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='04' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblDOCtDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='07' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblDOWhsDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType In ('09', '15') ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblRecvWhs2Dtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType In ('10', '16') ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblMutationDtl Set ");
                SQL.AppendLine("    SourceFrom=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where SourceFrom=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='11' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblMutationDtl Set ");
                SQL.AppendLine("    SourceTo=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where SourceTo=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='12' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblMutationsDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='11' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblMutationsDtl2 Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='12' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblRecvVdDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='13' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblDODeptDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='14' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblStockOpnameDtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='17' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblStockOpname2Dtl Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString() + " ");
                SQL.AppendLine("Where Source=@Source1_" + i.ToString() + " ");
                SQL.AppendLine("And DocNo In ( ");
                SQL.AppendLine("    Select DocNo ");
                SQL.AppendLine("    From TblStockMovement ");
                SQL.AppendLine("    Where DocDt>=@DocDt ");
                SQL.AppendLine("    And WhsCode=@WhsCode ");
                SQL.AppendLine("    And Source=@Source1_" + i.ToString());
                SQL.AppendLine("    And DocType='25' ");
                SQL.AppendLine("    ); ");

                SQL.AppendLine("Update TblStockMovement Set ");
                SQL.AppendLine("    Source=@Source2_" + i.ToString());
                SQL.AppendLine(" Where DocDt>=@DocDt ");
                SQL.AppendLine("And WhsCode=@WhsCode ");
                SQL.AppendLine("And Source=@Source1_" + i.ToString() + "; ");
            }

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, PropCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType1, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeFrom, B.BatchNoFrom, B.SourceFrom, '-', -1*B.QtyFrom, -1*B.Qty2From, -1*B.Qty3From, Null, ");
            SQL.AppendLine("'System', CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, PropCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCodeTo, B.BatchNoTo, B.SourceTo, '-', B.QtyTo, B.Qty2To, B.Qty3To, Null, ");
            SQL.AppendLine("'System', CurrentDateTime() ");
            SQL.AppendLine("From TblMutationHdr A ");
            SQL.AppendLine("Inner Join TblMutationDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@DocType1", "11");
            Sm.CmParam<String>(ref cm, "@DocType2", "12");

            cm.CommandText = SQL.ToString();
            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            ExecCommands(cml);

        }

        private void ExecCommands(List<MySqlCommand> cml)
        {
            var cn = new MySqlConnection(Gv.ConnectionString);
            MySqlTransaction tr = null;
            try
            {
                cn.Open();
                tr = cn.BeginTransaction();
                cml.ForEach
                (cm =>
                {
                    cm.Connection = cn;
                    cm.CommandTimeout = 7200;
                    cm.Transaction = tr;
                    cm.ExecuteNonQuery();
                }
                );
                //tr.Rollback();
                tr.Commit();
            }
            catch (Exception Exc)
            {
                if (cn.State == ConnectionState.Open) tr.Rollback();
                throw new Exception(Exc.Message);
            }
            finally
            {
                tr = null;
                cn.Close();
            }
        }
        #endregion

        #region Event

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OD.InitialDirectory = "c:";
                OD.Filter = "CSV files (*.csv)|*.CSV";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFileName.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Class

        private class Process
        {
            public string Source1 { get; set; }
            public decimal Price2 { get; set; }
            public string Source2 { get; set; }
        }

        #endregion
    }
}
