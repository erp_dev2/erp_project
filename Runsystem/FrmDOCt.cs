﻿#region Update

#region Old
/*
    12/07/2017 [TKG] tambah journal
    14/11/2017 [TKG] menggunakan nomor rekening COA sales dari item category untuk proses journal.
    22/12/2017 [HAR] Remark bisa di edit berdasarkan parameter
    03/02/2018 [TKG] ubah cara memilih shipping address
    27/03/2018 [TKG] ubah cara memilih shipping address
    02/04/2018 [ARI] amount rounding keatas menggunakan paramater AmtRoundingUp (header dan detail)
    04/04/2018 [HAR] local document dipanjangin inputannya
    16/04/2017 [TKG] ubah proses journal untuk cogs
    17/07/2018 [TKG] tambah cost center saat journal
    09/08/2018 [TKG] tambah status do
    10/08/2018 [TKG] Berdasarkan parameter IsDOCtAmtRounded, amount di-rounding (0.01-05 dibulatkan ke bawah, di atas 0.5 dibulatkan ke atas). 
    30/09/2018 [TKG] menghilangkan angka di belakang koma
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    25/02/2019 [TKG] tambah informasi kawasan berikat
    26/03/2019 [MEY] ubah layout print out untuk KSM
    11/07/2019 [WED] tambah KBSubmissionNo dan CustomsDocCode
    12/08/2019 [TKG] tambah otomatis perhitungan untuk packaging unit
    19/08/2019 [WED] tambah parameter baru untuk group code PEB, PEBGrpCode, PEBFilePath, PEBPassword
    26/08/2019 [WED] tambah parameter baru untuk type BC30
    17/09/2019 [WED] parameter PEBGrpCode bisa dibuat berkoma
    17/09/2019 [WED] UPrice otomatis lock dari CtQt, berdasarkan parameter IsDOCtUseCtQtPrice
    24/09/2019 [DITA/SIER] Copy source dari do to customer yg sudah pernah dibuat
    14/10/2019 [WED/MAI] masuk ke stock movement, kolom CustomsDocCode
    23/10/2019 [DITA/MAI] tambah KBContractNo, KBSubmissionNo, KBRegistrationNo saat save
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    12/11/2019 [WED/SIER] quantity ambil dari luasan di master customer tab shipping address, berdasarkan parameter IsCustomerShpAddressDisplayWide dan CustomerCategoryCodeForWideDOCt
    15/01/2020 [VIN/SIER] printout DO to customer menyamakan format dgn SI
    21/01/2020 [DITA/SIER] Bug printout DO
    22/01/2020 [HAR/SIER] BUG printout DO 
    31/01/2020 [TKG/MMM] tambah driver, expedition, vehicle registration#
    25/02/2020 [WED/SIER] Berdasarkan parameter, DO to Customer bisa diedit qty dan unit pricenya untuk item-item yang tidak memiliki item price
    05/03/2020 [VIN/KBN] Generate Docno 6 digit
    18/03/2020 [TKG/SRN] tambah stock price
    02/04/2020 [HAR/MMM] printout khusus MMM
    27/05/2020 [HAR/MMM] - nomor PO dari receipt- no telp ambil dari driver- nomor segel dari remark- stempel dihapus
    04/06/2020 [WED/SIER] stock yg muncul tidak ditambahkan dengan quantity DO nya (langsung ambil dari stock summary)
    12/06/2020 [HAR/MMM] - address ambil dari customer address, Nomor surat Jalan dari Local document, tanggal cetak sudat jalan dari Date, tanggal kirim barang dari Date
    16/06/2020 [DITA/SIER] menghide batch no, param --> mIsDOCtHideBatchNo
    11/08/2020 [ICA/SIER] mengubah hak akses variable mIsDOCtHideBatchNo menjadi internal.
    26/08/2020 [WED/MGI] tambah production Group, berdasarkan parameter IsUseProductionWorkGroup
    15/09/2020 [ICA/SIER] memisahkan parameter IsDOCtUseCtQtPrice dengan IsDOCtUseProcessInd
    16/09/2020 [VIN/SIER] printout: remark ambil dari remark detail
    23/09/2020 [DITA/SIER] Tambah parameter IsDOCtShowRemarkDetail
    11/11/2020 [WED/IMS] Untuk kategori jasa pengiriman consumable ke INKA alurnya dari BPM (recv item from oth whs) langsung menuju Ekspedisi untuk pembuatan SJN tanpa STTPP tetapi tidak untuk penagihan, hanya untuk proses produksi, bagaimana penginputan di sistem ? karena existingnya terdapat format SJN untuk pengiriman consumable --> tambah tab Project berdasarkan parameter IsDOCtUseSOContract
    16/11/2020 [DITA/IMS] Tambah project name di tab Project
    11/12/2020 [WED/SIER] bisa ubah detail dan remark header saat masih draft
    12/01/2020 [VIN/SRN] validasi saat savejournal baik insert maupun edit -> parameter : IsCheckCOAJournalNotExists
    12/01/2021 [ICA/SIER] membuat DO Price masih bisa diedit walaupun DO Price mengambil dari ItPrice based on parameter IsDOCtUseCtQtPrice dan IsDOCtPriceEditable
    18/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
    24/01/2021 [ICA/SIER] DO's Price diambil dari Item's Price saat insert
    27/01/2021 [TKG/SIER] IsDOCtUseProcessInd=Y, saat edit dan status masih draft, remark detail masih bisa diedit.
    01/02/2021 [DITA/PHT] data item yang diambil tidak hanya dari stock saja,tapi juga dari master item berdasarkan param : IsDOCtNotOnlyFromStock
    02/02/2021 [WED/PHT] journal di nonaktifkan berdasarkan parameter IsItemCategoryUseCOAAPAR
    11/02/2021 [VIN/PHT] tambah validasi hanya boleh narik item dengan itemcategory yg sama berdasarkan IsItemCategoryUseCOAAPAR
    13/02/2021 [ICA/KSM] tambah variable di class DOCt
    23/02/2021 [IBL/SIER] tambah inputan Customer Category berdasarakan parameter IsCustomerComboBasedOnCategory
    15/03/2021 [ICA/KSM] tambah variable SCLocalDocNo di Clas DOCtDtl
    31/03/2021 [IBL/SIER] Penyesuaian printout
    27/04/2021 [IBL/SIER] Printout: Tambah Footer Image
    29/04/2021 [BRI/SIER] tambah dialog untuk customer dan customer category berdasarkan param IsShowCustomercategory
    05/05/2021 [BRI/SIER] feedback dialog customer dan customer category
    10/05/2021 [BRI/SIER] merubah deskripsi tooltip
    31/05/2021 [RDH/SIER] menampilan item doct yang aktif saja pada dialog copyng DOCT;
    07/06/2021 [HAR/KIM] bug saat print detail
    14/06/2021 [RDA/SIER] menambahkan parameter IsItCtFilteredByGroup untuk menampilkan apakah item yang muncul berdasarkan group login atau tidak
    21/06/2021 [HAR/SIER] BUG IsGrdValueNotValid(), saat parameter IsDOCtUseProcessInd aktif maka tdk perlu cek ke stock
    05/07/2021 [VIN/Academy] BUG Update status DOCtHdr
    09/08/2021 [SET/ALL] Validasi tidak bisa SAVE untuk transaksi yang terbentuk jurnal otomatis ketika terdapat setting COA otomatis yang masih kosong (baik dari Menu Master Data maupun System's Parameter) sehingga terhindar dari jurnal yang tidak seimbang.
    26/08/2021 [IBL/AMKA] BUG tidak bisa save transaksi.
*/
#endregion

/*
    09/09/2021 [TKG/KIM] Berdasarkan parameter IsDOCtJournalUseCOGSAndStockEnabled, ubah journal do to customer 
    20/09/2021 [TKG/AMKA] Berdasarkan parameter IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled, ubah journal do to customer 
    14/10/2021 [SET/AMKA] Membuat transaksi DO to customer dapat menarik item dari dua item category yang berbeda berdasarkan parameter IsDOCtAllowMultipleItemCategory dengan parameter IsItemCategoryUseCOAAPAR = 'Y'
    29/10/2021 [IBL/AMKA] Bug : Journal cancelling masih belum membalik.
    10/11/2021 [YOG/AMKA] Membuat field Customer, warehouse, dan item di menu DO to Customer terfirlter berdasarkan grup
    24/11/2021 [RDA/AMKA] Membuat menu DO to Customer dapat mengupload file dan bersifat mandatory berdasarkan param IsDOCtUploadFileMandatory
    06/02/2022 [TKG/PHT] ubah GetParameter() dan proses save
    10/03/2022 [TKG/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    29/03/2022 [ISD/SIER] field customer category terfilter berdasarkan group dengan parameter IsFilterByCtCt
    25/04/2022 [RIS/SIER] Penyeseuaian param IsBOMShowSpecifications untuk kolom specification
    09/08/2022 [MAU/SIER] Hide kolom (Batch, Stock2, Quantity2, UoM2) pada menu DO to Customer saat INSERT dan buka dokumen
    26/08/2022 [IBL/PHT] ubah variable mIsDOCtUseProcessInd menjadi internal, agar bisa dipakai di frmChild
    13/09/2022 [MAU/PHT] DOCt membuat journal berdasarkan parameter IsDotocustomerJournalDisabled 
    06/10/2022 [VIN/PHT] DO tidak melihat stock berdasarkan param IsDOCtNotOnlyFromStock
    19/12/2022 [DITA/MNET] penyesuaian printout
    23/12/2022 [ICA/MNET] feedback/bug penyesuaian printout
    30/12/2022 [DITA/MNET] peruabhan format date di signature printout
    03/01/2023 [TYO/MNET] Menambah tab Upload file format grd berdasarkan parameter DOCtUploadFileFormat
    11/01/2023 [TYO/PHT] Bug : save dan edit data
    16/01/2023 [TYO/PHT] bug : tambah parameter mIsDOCtUploadFileMandatory di IsUploadFileMandatory()
    25/01/2023 [TYO/MNET] bug edit upload file format 2
    02/02/2023 [RDA/MNET] bug file yg sudah terupload tidak bisa dihapus lagi (edit di Grd4_KeyDown aja)
    10/02/2023 [BRI/MNET] Bug : tidak bisa edit data
    21/02/2023 [VIN/SIER] tidak ada validasi ketika cancel
    21/03/2023 [MYA/MNET] Membuat validasi approval DO to Customer berdasarkan departemen
    27/03/2023 [MYA/MNET] Menambahkan filed COA Account List di menu DO to Customer (mandatory tapi tidak di merahin)
    27/03/2023 [MYA/MNET] upload file DO to Customer
    12/04/2023 [VIN/SIER] tidak bisa edit price jika sudah final 
    03/05/2023 [MAU/KBN] penyesuaian printout DOCtDR
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.Drawing.Imaging;
using System.Threading;

using FastReport;
using FastReport.Data;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mCustomsDocCode = string.Empty,
            mCtSADNo = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mGrpCode = string.Empty,
            mMenuCodeForSOContract2 = string.Empty;
        internal FrmDOCtFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private int mRow = 0;
        private bool
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsDOCtNeedShippingAddressInd = false,
            mIsDOCtPriceEditable = false,
            mIsAutoJournalActived = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtUseCtQtPrice = false,
            mIsDOCtProsessToSalesInvoice = false,
            mIsDOCtRemarkEditable = false,
            mIsUseProductionWorkGroup = false,
            mIsCheckCOAJournalNotExists,
            IsInsert = false,
            mIsCopyingDOUseActiveDoc = false,
            isTextNotNull = false,
            mIsDOCtJournalUseCOGSAndStockEnabled = false,
            mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled = false,
            mIsDOCtAllowMultipleItemCategory = false,
            mIsDOCtUploadFileMandatory = false,
            mIsDOCtApprovalBasedOnWhs = false,
            mIsDoctUseCOAAccountList = false,
            mIsCOAAccountListDoctMandatory = false;

        internal bool mIsKawasanBerikatEnabled = false,
            mIsBOMShowSpecifications = false,
            mIsCustomerShpAddressDisplayWide = false,
            mIsDOCtShowPackagingUnit = false,
            mIsItGrpCodeShow = false,
            mIsShowLocalDocNo = false,
            mIsShowForeignName = false,
            mIsFilterBySite = false,
            //mIsAmtRoundingUp = false,
            mIsDOCtAmtRounded = false,
            mIsDOCtApprovalActived = false,
            mIsDOCtShowStockPrice = false,
            mIsDOCtHideBatchNo = false,
            mIsDOCtShowRemarkDetail = false,
            mIsDOCtUseSOContract = false,
            mIsAbleToAccessSOContractData = true,
            mIsDOCtNotOnlyFromStock = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsCustomerComboBasedOnCategory = false,
            mIsFilterByCtCt = false,
            mIsShowCustomerCategory = false,
            mIsItCtFilteredByGroup = false,
            mIsDoCtHideStockQuantityUOM2 = false,
            mIsDOCtUseProcessInd = false,
            mIsDotocustomerJournalDisabled = false,
            mIsDOCtAllowToUploadFile = false
            ;
        private string
            mDocType = "07",
            mMainCurCode = string.Empty,
            CtCode = string.Empty;
        internal string
            mCityCode = string.Empty,
            mCntCode = string.Empty,
            mISDOCtShowCustomerItem = string.Empty,
            mPEBGrpCode = string.Empty,
            mPEBFilePath = string.Empty,
            mPEBPassword = string.Empty,
            mPEBDocType = string.Empty,
            mCustomerCategoryCodeForWideDOCt = string.Empty,

            mHostAddrForFTPClient = string.Empty,
            mPortForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mDOCtUploadFileFormat = string.Empty;

        private string mStateIndicator = string.Empty;

        private byte[] downloadedData;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmDOCt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0)
                {
                    this.Text = "DO To Customer";
                    isTextNotNull = true;
                }
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsKawasanBerikatEnabled)
                {
                    Tp2.PageVisible = true;
                    if (!IsAuthorizedToAccessPEB())
                    {
                        BtnPEB.Visible = false;
                    }
                }

                if (!mIsDOCtUseSOContract)
                {
                    Tp3.PageVisible = false;
                    mIsAbleToAccessSOContractData = false;
                }
                else
                {
                    Tp3.Select();
                    mMenuCodeForSOContract2 = Sm.GetValue("Select Menucode from TblMenu where Param = 'FrmSOContract2'");
                    mGrpCode = Sm.SetFormForGroupUser(mMenuCodeForSOContract2);
                    if (mGrpCode.Length == 0)
                    {
                        BtnSOContractDocNo2.Visible = false;
                        mIsAbleToAccessSOContractData = false;
                    }
                }
                if (!mIsDoctUseCOAAccountList)
                {
                    Tp4.PageVisible = false;
                }

                GetValue();
                SetGrd();
                SetFormControl(mState.View);
                if (mIsShowCustomerCategory)
                    LblCtCtCode.ForeColor = Color.Black;
                if (!mIsShowCustomerCategory)
                    BtnCtCt.Visible = false;
                Sl.SetLueCurCode(ref LueCurCode);
                SetLueProcessInd(ref LueProcessInd);

                LblSAName.ForeColor = mIsDOCtNeedShippingAddressInd ?
                    System.Drawing.Color.Red : System.Drawing.Color.Black;

                LuePackagingUnitUomCode.Visible = false;

                if (!mIsDOCtUseProcessInd)
                {
                    LblProcessInd.Visible = LueProcessInd.Visible = false;
                }

                if (!mIsUseProductionWorkGroup)
                {
                    LblProductionWorkGroup.Visible = LueProductionWorkGroup.Visible = false;
                }
                else
                {
                    Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                }

                if (mIsCustomerComboBasedOnCategory)
                {
                    Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                    //Sl.SetLueCtCtCode(ref LueCtCtCode);
                }
                else
                {
                    LblCtCtCode.Visible = LueCtCtCode.Visible = false;
                    Sl.SetLueCtCode(ref LueCtCode);
                }

                if(!mIsDOCtAllowToUploadFile)
                {
                    Tc1.TabPages.Remove(TpgUploadFile1);
                    Tc1.TabPages.Remove(TpgUploadFile2);
                }

                else if(mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat =="1")
                    Tc1.TabPages.Remove(TpgUploadFile2);

                else if(mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat == "2")
                    Tc1.TabPages.Remove(TpgUploadFile1);


                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                if (mIsDOCtUploadFileMandatory)
                {
                    label45.ForeColor = Color.Red;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetValue()
        {
            mIsDOCtApprovalActived = Sm.IsDataExist("Select 1 from TblDocApprovalSetting Where DocType='DOCt' Limit 1;");
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'NumberOfInventoryUomCode', 'MainCurCode', 'IsDOCtJournalUseCOGSAndStockEnabled', 'IsItCtFilteredByGroup', 'IsCopyingDOUseActiveDoc', ");
            SQL.AppendLine("'IsFilterByCtCt', 'ISDOCtShowCustomerItem', 'IsAutoJournalActived', 'IsShowCustomerCategory', 'IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled', ");
            SQL.AppendLine("'PEBGrpCode', 'PEBFilePath', 'PEBPassword', 'PEBDocType', 'DocNoFormat', ");
            SQL.AppendLine("'SharedFolderForFTPClient', 'FileSizeMaxUploadFTPClient', 'FormatFTPClient', 'CustomerCategoryCodeForWideDOCt', 'KB_Server', ");
            SQL.AppendLine("'IsDOCtUploadFileMandatory', 'HostAddrForFTPClient', 'PortForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'IsCheckCOAJournalNotExists', 'IsDOCtNotOnlyFromStock', 'IsItemCategoryUseCOAAPAR', 'IsCustomerComboBasedOnCategory', 'IsDOCtAllowMultipleItemCategory', ");
            SQL.AppendLine("'IsDOCtShowStockPrice', 'IsDOCtHideBatchNo', 'IsUseProductionWorkGroup', 'IsDOCtShowRemarkDetail', 'IsDOCtUseSOContract', ");
            SQL.AppendLine("'IsDOCtUseCtQtPrice', 'IsDOCtUseProcessInd', 'IsDOCtNeedShippingAddressInd', 'IsBOMShowSpecifications', 'IsCustomerShpAddressDisplayWide', ");
            SQL.AppendLine("'IsFilterBySite', 'IsDOCtProsessToSalesInvoice', 'IsAcNoForSaleUseItemCategory', 'IsDOCtRemarkEditable', 'IsDOCtAmtRounded', ");
            SQL.AppendLine("'IsItGrpCodeShow', 'IsDOCtShowPackagingUnit', 'IsDOCtPriceEditable', 'IsShowForeignName', 'IsShowLocalDocNo', ");
            SQL.AppendLine("'IsClosingJournalBasedOnMultiProfitCenter', 'IsDoCtHideStockQuantityUOM2', 'IsDotocustomerJournalDisabled', 'IsDOCtAllowToUploadFile', 'DOCtUploadFileFormat', ");
            SQL.AppendLine("'IsDOCtApprovalBasedOnWhs', 'IsDoctUseCOAAccountList', 'IsCOAAccountListDoctMandatory' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;
                            case "IsDOCtShowPackagingUnit": mIsDOCtShowPackagingUnit = ParValue == "Y"; break;
                            case "IsDOCtPriceEditable": mIsDOCtPriceEditable = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsShowLocalDocNo": mIsShowLocalDocNo = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsDOCtProsessToSalesInvoice": mIsDOCtProsessToSalesInvoice = ParValue == "Y"; break;
                            case "IsAcNoForSaleUseItemCategory": mIsAcNoForSaleUseItemCategory = ParValue == "Y"; break;
                            case "IsDOCtRemarkEditable": mIsDOCtRemarkEditable = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsDOCtUseCtQtPrice": mIsDOCtUseCtQtPrice = ParValue == "Y"; break;
                            case "IsDOCtUseProcessInd": mIsDOCtUseProcessInd = ParValue == "Y"; break;
                            case "IsDOCtNeedShippingAddressInd": mIsDOCtNeedShippingAddressInd = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsCustomerShpAddressDisplayWide": mIsCustomerShpAddressDisplayWide = ParValue == "Y"; break;
                            case "IsDOCtShowStockPrice": mIsDOCtShowStockPrice = ParValue == "Y"; break;
                            case "IsDOCtHideBatchNo": mIsDOCtHideBatchNo = ParValue == "Y"; break;
                            case "IsUseProductionWorkGroup": mIsUseProductionWorkGroup = ParValue == "Y"; break;
                            case "IsDOCtShowRemarkDetail": mIsDOCtShowRemarkDetail = ParValue == "Y"; break;
                            case "IsDOCtUseSOContract": mIsDOCtUseSOContract = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsDOCtNotOnlyFromStock": mIsDOCtNotOnlyFromStock = ParValue == "Y"; break;
                            case "IsItemCategoryUseCOAAPAR": mIsItemCategoryUseCOAAPAR = ParValue == "Y"; break;
                            case "IsCustomerComboBasedOnCategory": mIsCustomerComboBasedOnCategory = ParValue == "Y"; break;
                            case "IsDOCtAllowMultipleItemCategory": mIsDOCtAllowMultipleItemCategory = ParValue == "Y"; break;
                            case "IsDOCtUploadFileMandatory": mIsDOCtUploadFileMandatory = ParValue == "Y"; break;
                            case "KB_Server": mIsKawasanBerikatEnabled = ParValue.Length > 0; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsDOCtJournalUseCOGSAndStockEnabled": mIsDOCtJournalUseCOGSAndStockEnabled = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsItCtFilteredByGroup": mIsItCtFilteredByGroup = ParValue == "Y"; break;
                            case "IsCopyingDOUseActiveDoc": mIsCopyingDOUseActiveDoc = ParValue == "Y"; break;
                            case "IsShowCustomerCategory": mIsShowCustomerCategory = ParValue == "Y"; break;
                            case "IsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled": mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled = ParValue == "Y"; break;
                            case "IsDoCtHideStockQuantityUOM2": mIsDoCtHideStockQuantityUOM2 = ParValue == "Y"; break;
                            case "IsDotocustomerJournalDisabled": mIsDotocustomerJournalDisabled = ParValue == "Y"; break;
                            case "IsDOCtAllowToUploadFile": mIsDOCtAllowToUploadFile = ParValue == "Y";break;
                            case "IsDOCtApprovalBasedOnWhs": mIsDOCtApprovalBasedOnWhs = ParValue == "Y";break;
                            case "IsDoctUseCOAAccountList": mIsDoctUseCOAAccountList = ParValue == "Y";break;
                            case "IsCOAAccountListDoctMandatory": mIsCOAAccountListDoctMandatory = ParValue == "Y";break;

                            //string
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "ISDOCtShowCustomerItem": mISDOCtShowCustomerItem = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "PEBDocType": mPEBDocType = ParValue; break;
                            case "PEBPassword": mPEBPassword = ParValue; break;
                            case "PEBFilePath": mPEBFilePath = ParValue; break;
                            case "PEBGrpCode": mPEBGrpCode = ParValue; break;
                            case "CustomerCategoryCodeForWideDOCt": mCustomerCategoryCodeForWideDOCt = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "DOCtUploadFileFormat": mDOCtUploadFileFormat = ParValue; break;

                            //Integer
                            case "NumberOfInventoryUomCode":
                                if (ParValue.Length == 0)
                                    mNumberOfInventoryUomCode = 1;
                                else
                                    mNumberOfInventoryUomCode = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
            if (mPEBDocType.Length <= 0) mPEBDocType = "30";
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Property Code",
                        "Property",
                        "Batch#",

                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",
                        "Quantity",

                        //16-20
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",

                        //21-25
                        "Quantity",
                        "UoM",
                        "DO's Price",
                        "Amount",
                        "Remark",

                        //26-30
                        "Group",
                        "Quantity" +Environment.NewLine + "(Packaging)",
                        "UoM" +Environment.NewLine + "(Packaging)",
                        "Customer"+Environment.NewLine+"Refference",
                        "Foreign Name",

                        //31-33
                        "Specification",
                        "CtQtInd",
                        "Stock's Price"
                    },
                     new int[]
                    {
                        //0
                        20,
                        
                        //1-5
                        50, 50, 20, 100, 20, 
                        
                        //6-10
                        100, 250, 0, 100, 200, 
                        
                        //11-15
                        180, 60, 60, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 80, 80, 
                        
                        //21-25
                        80, 80, 130, 100, 400,

                        //26-30
                        100, 100, 100, 100, 230,

                        //31-33
                        300, 0, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 22, 24, 26, 29, 30, 31, 32, 33 });
            Grd1.Cols[29].Move(11);
            Sm.GrdColInvisible(Grd1, new int[] { 31 });
            Grd1.Cols[31].Move(8);
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 11, 12, 13, 17, 18, 19, 20, 21, 22, 26, 27, 28 }, false);
                Grd1.Cols[30].Move(8);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 11, 12, 13, 17, 18, 19, 20, 21, 22, 26, 27, 28, 30 }, false);

            if (mISDOCtShowCustomerItem == "N")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, false);
            }

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[26].Visible = true;
                if (mIsBOMShowSpecifications) Grd1.Cols[26].Move(9);
                else Grd1.Cols[26].Move(7);
            }

            if (mIsDOCtShowPackagingUnit)
            {
                Grd1.Cols[27].Visible = true;
                Grd1.Cols[28].Visible = true;
                Grd1.Cols[28].Move(14);
                Grd1.Cols[27].Move(15);
            }


            Grd1.Cols[33].Visible = mIsDOCtShowStockPrice;
            if (mIsDOCtShowStockPrice) Grd1.Cols[33].Move(23);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]
                    {
                        "Total Quantity", "Total Quantity 2", "Total Quantity 3"
                    },
                    new int[]
                    {
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 1, 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);

            #endregion

            #region Grid 3

            if (mIsDOCtAllowToUploadFile)
            {
                Grd3.Cols.Count = 7;
                Grd3.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd3,
                        new string[]
                        {
                        //0
                        "D No",
                        //1-5
                        "File Name",
                        "",
                        "D",
                        "Upload By",
                        "Date",

                        //6
                        "Time"
                        },
                         new int[]
                        {
                        0,
                        250, 20, 20, 100, 100,
                        100
                        }
                    );

                Sm.GrdColInvisible(Grd3, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd3, new int[] { 5 });
                Sm.GrdFormatTime(Grd3, new int[] { 6 });
                //Sm.GrdColButton(Grd3, new int[] { 2, 3 });
                Sm.GrdColButton(Grd3, new int[] { 2 }, "1");
                Sm.GrdColButton(Grd3, new int[] { 3 }, "2");
                Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 4, 5, 6 });
                Grd3.Cols[2].Move(1);
            }


            #endregion

            #region Grid 4

            Grd4.Cols.Count = 6;
            Grd4.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit Amount",
                        "Credit Amount",
                        "Remark"
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 150, 150, 150
                    }
                );

            Sm.GrdColButton(Grd4, new int[] { 0 });
            Sm.GrdColReadOnly(Grd4, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd4, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 1 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 1, 2 });

            #endregion

            ShowInventoryUomCode();
            if (mIsDOCtHideBatchNo) Sm.GrdColInvisible(Grd1, new int[] { 10 }, false);
            if (mIsDoCtHideStockQuantityUOM2) Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, false);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 9, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            //if (!mIsDOCtHideBatchNo) Sm.GrdColInvisible(Grd1, new int[] { 10 }, !ChkHideInfoInGrd.Checked);
            if (mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 6, 31 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
            //if (!mIsDoCtHideStockQuantityUOM2) Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
            if (mIsDOCtHideBatchNo) Sm.GrdColInvisible(Grd1, new int[] { 10 }, !ChkHideInfoInGrd.Checked);
            if (mIsDoCtHideStockQuantityUOM2) Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, !ChkHideInfoInGrd.Checked);


        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueWhsCode, LueCtCode, TxtDocNoInternal, TxtSAName,
                        TxtResiNo, TxtDocNoInternal, LueCurCode, TxtDriver, TxtExpedition,
                        TxtVehicleRegNo, MeeRemark, LueProcessInd, LueProductionWorkGroup, LueCtCtCode,
                        TxtFile
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 15, 18, 21, 23, 25, 27, 28 });
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = false;
                    BtnCustomerShipAddress.Enabled = false;
                    BtnSOContractDocNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 3, 4, 5 });
                    BtnSAName.Enabled = false;
                    BtnCopyDOCt.Enabled = false;
                    BtnCtCt.Enabled = false;

                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = true;
                    if (TxtFile.Text.Length == 0) BtnDownload.Enabled = false;

                    if (mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat == "2")
                    {
                        mRow = 0;
                        //Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                            {
                                Grd3.Cells[Row, 2].ReadOnly = iGBool.False;
                                Grd3.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd3.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd3.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                            mRow++;
                        }
                    }

                    TxtDocNo.Focus();

                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueWhsCode, LueCtCode, TxtDocNoInternal, LueCurCode,
                        TxtResiNo, TxtDriver, TxtExpedition, TxtVehicleRegNo, MeeRemark, LueProductionWorkGroup
                    }, false);
                    if (mIsCustomerComboBasedOnCategory)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode }, false);
                    }
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 23, 25, 27, 28 });
                    BtnCustomerShipAddress.Enabled = true;
                    BtnSAName.Enabled = true;
                    BtnSOContractDocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 3, 4, 5 });
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = true;
                    BtnCopyDOCt.Enabled = true;
                    BtnCtCt.Enabled = true;
                    if (mIsShowCustomerCategory)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode, LueCtCode }, true);
                    }
                    DteDocDt.Focus();

                    if (mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat == "2") Sm.GrdColReadOnly(false, false, Grd3, new int[] { 2, 3 });

                    BtnFile.Enabled = true;
                    BtnDownload.Enabled = false;
                    ChkFile.Enabled = true;
                    break;

                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    if (mIsDOCtPriceEditable && Sm.GetLue(LueProcessInd) =="D" ) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23 });
                    if (mIsDOCtRemarkEditable) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark }, false);
                    if (mIsDOCtUseProcessInd)
                    {
                        if (Sm.IsDataExist("Select 1 From TblDOCtHdr Where DocNo = @Param And ProcessInd = 'D';", TxtDocNo.Text))
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd, MeeRemark }, false);
                            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 23, 25 });
                            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1 });

                            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                            {
                                //if (Sm.GetGrdStr(Grd1, i, 32).Length != 0 || Sm.GetGrdBool(Grd1, i, 1))
                                if (Sm.GetGrdStr(Grd1, i, 32).Length != 0 && Sm.GetGrdBool(Grd1, i, 1))
                                {
                                    Grd1.Cells[i, 15].ReadOnly = iGBool.True;
                                    Grd1.Cells[i, 18].ReadOnly = iGBool.True;
                                    Grd1.Cells[i, 21].ReadOnly = iGBool.True;
                                    Grd1.Cells[i, 23].ReadOnly = iGBool.True;
                                    Grd1.Cells[i, 25].ReadOnly = iGBool.True;

                                    Grd1.Cells[i, 15].BackColor = Color.FromArgb(224, 224, 224);
                                    Grd1.Cells[i, 18].BackColor = Color.FromArgb(224, 224, 224);
                                    Grd1.Cells[i, 21].BackColor = Color.FromArgb(224, 224, 224);
                                    Grd1.Cells[i, 23].BackColor = Color.FromArgb(224, 224, 224);
                                    Grd1.Cells[i, 25].BackColor = Color.FromArgb(224, 224, 224);
                                }
                            }
                        }
                    }

                    if (mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat == "2" )
                    {
                        if (TxtStatus.Text == "Approved")
                        {
                            Sm.GrdColReadOnly(true, false, Grd3, new int[] { 0, 1, 2, 4, 5, 6 });
                        }
                        else if (TxtStatus.Text == "Outstanding")
                            Sm.GrdColReadOnly(false, false, Grd3, new int[] { 2 });
                    }
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mStateIndicator = string.Empty;
            mCtSADNo = string.Empty;
            mCityCode = string.Empty;
            mCntCode = string.Empty;
            mCustomsDocCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtStatus, LueWhsCode, LueCtCode,
                TxtDocNoInternal, TxtDriver, TxtExpedition, TxtVehicleRegNo, MeeRemark,
                LueCurCode, TxtSAName, MeeSAAddress, TxtCity, TxtCountry,
                TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtResiNo,
                TxtMobile, TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt,
                TxtKBRegistrationNo, DteKBRegistrationDt, TxtKBSubmissionNo, LueProcessInd, LueProductionWorkGroup,
                TxtSOContractDocNo, TxtProjectCode, TxtPONo, TxtProjectName, LueCtCtCode,
                TxtFile
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtSAName, MeeSAAddress, TxtCity, TxtCountry, TxtPostalCd,
                TxtPhone, TxtFax, TxtEmail, TxtMobile
            });
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 1, 2 });

            if(mIsDOCtAllowToUploadFile) Sm.ClearGrd(Grd3, true);

            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 3, 4 });
            
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOCtFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                mStateIndicator = "I";
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                if (mIsCustomerComboBasedOnCategory) Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                if (mIsDOCtUseProcessInd) Sm.SetLue(LueProcessInd, "D");
                else Sm.SetLue(LueProcessInd, "F");
                IsInsert = true;
                if (mIsCustomerComboBasedOnCategory)
                {
                    Sl.SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mStateIndicator = "E";
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.EntCode ");
            SQL.AppendLine("From TblWarehouse A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("    And A.WhsCode = @Param ");
            SQL.AppendLine("Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode; ");

            mEntCode = Sm.GetValue(SQL.ToString(), Sm.GetLue(LueWhsCode));

            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    Sm.StdMsgYN("Print", "") == DialogResult.No ||
                    //IsDOCtStatusOutstanding() ||
                    IsDOCtStatusCancelled()
                    ) return;
                ParPrint((int)mNumberOfInventoryUomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsDOCtStatusOutstanding()
        {
            return Sm.IsDataExist(
                "Select 1 from TblDOCtHdr Where DocNo=@Param And Status='O';",
                TxtDocNo.Text,
                "PO's status still outstanding.");
        }

        private bool IsDOCtStatusCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 from TblDOCtHdr Where DocNo=@Param And Status='C';",
                TxtDocNo.Text,
                "PO is cancelled.");
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF Files (*.pdf)|*.pdf|MS Word (*.doc;*.docx)|*.doc;*.docx|MS Excel (*.xls;*.xlsx)|*.xls;*.xlsx|MS PowerPoint (*.ppt;*.pptx)|*.ppt;*.pptx|JPG (*.jpg)|*.jpg|PNG Files (*.png)|*.png";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOCtDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 23, 25, 27, 28 }, e.ColIndex))
                    {
                        if (e.ColIndex == 28)
                        {
                            var ItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            if (ItCode.Length == 0) ItCode = "XXX";
                            SetLuePackagingUnitUomCode(ref LuePackagingUnitUomCode, ItCode, string.Empty);
                            LueRequestEdit(Grd1, LuePackagingUnitUomCode, ref fCell, ref fAccept, e, 28);
                        }
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });
                    }
                }
                else
                {
                    if (!(
                        (e.ColIndex == 1 || (mIsDOCtPriceEditable && e.ColIndex == 23)) &&
                        !Sm.GetGrdBool(Grd1, e.RowIndex, 2) &&
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0
                        ))
                        e.DoDefault = false;

                    if (Sm.IsGrdColSelected(new int[] { 15, 18, 21, 23, 25 }, e.ColIndex) && mIsDOCtUseCtQtPrice)
                    {
                        e.DoDefault = true;
                    }


                    //if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                    //    e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalQty();
                ComputeAmt();
            }
            else
            {
                if (Sm.IsDataExist("Select 1 From TblDOCtHdr Where DocNo = @Param And ProcessInd = 'D';", TxtDocNo.Text))
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeTotalQty();
                    ComputeAmt();
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
            {
                if (mIsDOCtUseCtQtPrice)
                {
                    if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                    {
                        Sm.FormShowDialog(new FrmDOCtDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                    }
                }
                else
                {
                    Sm.FormShowDialog(new FrmDOCtDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                }
            }

            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length > 0 && mIsDOCtUseProcessInd)
            {
                if (Sm.IsDataExist("Select 1 From TblDOCtHdr Where DocNo = @Param And ProcessInd = 'D';", TxtDocNo.Text))
                {
                    if (mIsDOCtUseCtQtPrice)
                    {
                        if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                        {
                            Sm.FormShowDialog(new FrmDOCtDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                        }
                    }
                    else
                    {
                        Sm.FormShowDialog(new FrmDOCtDlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                    }
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21, 23, 27 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25 }, e);

                if (mIsDOCtShowPackagingUnit)
                {
                    if (e.ColIndex == 28) Sm.SetGrdNumValueZero(ref Grd1, e.RowIndex, new int[] { 15, 18, 21, 27 });

                    if (e.ColIndex == 27)
                    {
                        string PackagingUnitUomCode = Sm.GetGrdStr(Grd1, e.RowIndex, 28);
                        if (PackagingUnitUomCode.Length > 0)
                        {
                            decimal QtyPackagingUnit = Sm.GetGrdDec(Grd1, e.RowIndex, 27);
                            string ItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            string InventoryUomCode = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                            string SalesUomCode = string.Empty, SalesUomCode2 = string.Empty;
                            decimal Qty = 0m, Qty2 = 0m;

                            GetPackagingUnitInfo(ItCode, PackagingUnitUomCode, ref Qty, ref Qty2, ref SalesUomCode, ref SalesUomCode2);
                            if (Sm.CompareStr(InventoryUomCode, SalesUomCode))
                            {
                                Grd1.Cells[e.RowIndex, 15].Value = Qty * QtyPackagingUnit;
                                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);
                            }
                            else
                            {
                                if (Sm.CompareStr(InventoryUomCode, SalesUomCode2))
                                {
                                    Grd1.Cells[e.RowIndex, 15].Value = Qty2 * QtyPackagingUnit;
                                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
                                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                                        Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);
                                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                                        Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);
                                }
                            }
                        }
                    }
                }

                if (e.ColIndex == 15)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
                }

                if (e.ColIndex == 18)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 18, 15, 21, 19, 16, 22);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 18, 21, 15, 19, 22, 16);
                }

                if (e.ColIndex == 21)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 21, 15, 18, 22, 16, 19);
                    Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 21, 18, 15, 22, 19, 16);
                }

                if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);

                if (Sm.IsGrdColSelected(new int[] { 1, 15, 18, 21, 27, 28 }, e.ColIndex)) ComputeTotalQty();
                if (Sm.IsGrdColSelected(new int[] { 1, 15, 23, 27, 28 }, e.ColIndex)) ComputeTotal(e.RowIndex);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 15, 18, 21, 24 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            //string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");
            if (mDocNoFormat == "1")
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");
            }
            else
            {
                DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "DoCt", "TblDoctHdr", mEntCode, "1");
            }
            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOCtHdr(DocNo));
            cml.Add(SaveDOCtDtl(DocNo));
            cml.Add(SaveDOCtDtl2(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
            //    {
            //        cml.Add(SaveDOCtDtl(DocNo, Row));
            //        if (mIsDOCtNotOnlyFromStock && Sm.GetGrdStr(Grd1, Row, 11).Length <= 0)
            //        {
            //            cml.Add(SaveStock2(DocNo, Row));
            //        }
            //    }
            //}

            if (!mIsDOCtUseProcessInd)
            {
                cml.Add(SaveStock(DocNo));
                if (mIsAutoJournalActived)
                {
                    if (!mIsItemCategoryUseCOAAPAR && !mIsDotocustomerJournalDisabled)
                        cml.Add(SaveJournal(DocNo));
                    else
                    {
                        if (mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)

                            cml.Add(SaveJournal(DocNo));
                    }
                }
            }

            

            IsInsert = false;


            if (mDOCtUploadFileFormat == "1" && mIsDOCtAllowToUploadFile)
            {
                if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                    UploadFile(DocNo);
            }

            else if (mDOCtUploadFileFormat == "2" && mIsDOCtAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsDOCtAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                        {
                            if (IsUploadFileNotValidGrid(Row, Sm.GetGrdStr(Grd3, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFileGrid(DocNo));
            }

            Sm.ExecCommands(cml);

            if (mDOCtUploadFileFormat == "2" && mIsDOCtAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsDOCtAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                        {
                            UploadFileGrid(DocNo, Row, Sm.GetGrdStr(Grd3, Row, 1));
                        }
                    }
                }

            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                (mIsCustomerComboBasedOnCategory && !mIsShowCustomerCategory && Sm.IsLueEmpty(LueCtCtCode, "Customer Category")) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                (mIsDOCtNeedShippingAddressInd && Sm.IsTxtEmpty(TxtSAName, "Shipping address", false)) ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsItemCategoryInvalid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt)) ||
                IsJournalSettingInvalid() ||
                IsUploadFileMandatory() ||
                IsUploadFileNotValid();
                
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueWhsCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select B.ProfitCenterCode From TblWarehouse A, TblCostCenter B " +
                    "Where A.CCCode=B.CCCode And B.ProfitCenterCode Is Not Null And A.WhsCode=@Param;",
                    Value);
        }

        private bool IsItemCategoryInvalid()
        {
            if (mIsAutoJournalActived && mIsItemCategoryUseCOAAPAR && Grd1.Rows.Count > 2)
            {
                if (mIsDOCtAllowMultipleItemCategory) return false;
                string ItCtCode = Sm.GetValue("Select ItCtCode From TblItem Where ItCode = @Param; ", Sm.GetGrdStr(Grd1, 0, 4));

                for (int i = 1; i < Grd1.Rows.Count - 1; ++i)
                {
                    string tempItCtCode = Sm.GetValue("Select ItCtCode From TblItem Where ItCode = @Param; ", Sm.GetGrdStr(Grd1, i, 4));

                    if (tempItCtCode != ItCtCode)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Different item category detected. This will affect the journal process.");
                        Sm.FocusGrd(Grd1, i, 7);
                        return true;
                    }
                }
            }
            return false;
        }

        /*private bool IsCOAJournalNotValid()
        {
            var l = new List<COA>();
            var SQL = new StringBuilder();
            string mItCode = string.Empty;
            string mInitProcessInd = Sm.GetValue("Select ProcessInd From TblDOCtHdr Where DocNo = @Param;", TxtDocNo.Text);
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (mItCode.Length > 0) mItCode += " Or ";
                    mItCode += " (A.ItCode=@ItCode00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                }
            }

            if (mItCode.Length > 0) mItCode = " And (" + mItCode + ") ";

            SQL.AppendLine("Select AcNo From ");
            SQL.AppendLine("( ");

            if (!mIsDOCtUseProcessInd || (mIsDOCtUseProcessInd && (mInitProcessInd != Sm.GetLue(LueProcessInd) && mInitProcessInd == "D")))
            {
                if (mIsAcNoForSaleUseItemCategory)
                {

                    SQL.AppendLine("   Select B.AcNo5 As AcNo ");
                    SQL.AppendLine("   From TblItem A ");
                    SQL.AppendLine("   Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode  " + mItCode);
                    SQL.AppendLine("   Group By B.AcNo5 ");
                    SQL.AppendLine("   Union All ");

                }
                else
                {
                    SQL.AppendLine("    Select ParValue As AcNo ");
                    SQL.AppendLine("    From TblParameter Where ParCode='AcNoForCOGS' ");
                    SQL.AppendLine("    Union All ");

                }

                SQL.AppendLine("    Select B.AcNo As AcNo ");
                SQL.AppendLine("    From TblItem A ");
                SQL.AppendLine("    Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode  " + mItCode);
                SQL.AppendLine("    Union All ");

                SQL.AppendLine("    Select concat(ParValue, @CtCode) As AcNo ");
                SQL.AppendLine("    From TblParameter Where ParCode='CustomerAcNoNonInvoice' ");
                SQL.AppendLine("    Union All ");


                if (mIsAcNoForSaleUseItemCategory)
                {
                    SQL.AppendLine("    Select X.AcNo ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select B.AcNo4 As AcNo ");
                    SQL.AppendLine("        From TblItem A ");
                    SQL.AppendLine("        Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode  " + mItCode);
                    SQL.AppendLine("    ) X Group By X.AcNo ");
                }

                else
                {
                    SQL.AppendLine("    Select ParValue As AcNo ");
                    SQL.AppendLine("    From TblParameter Where ParCode='AcNoForSaleOfFinishedGoods' ");
                }

                SQL.AppendLine(")T ; ");
            }
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0])

                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
            {
                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for creating journal transaction.");
                return true;
            }

            return false;
        }*/

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }

            if (Grd4.Rows.Count == 1 && mIsCOAAccountListDoctMandatory)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 COA Account List.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                   "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                   "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                   "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                   "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                   "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (!mIsDOCtUseProcessInd)
                {
                    if (!mIsDOCtNotOnlyFromStock && Sm.GetGrdStr(Grd1, Row, 11).Length > 0)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                            return true;
                        }

                        if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than DO's quantity.");
                            return true;
                        }

                        if (Grd1.Cols[18].Visible)
                        {
                            if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                            {
                                Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                                return true;
                            }

                            if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                            {
                                Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than DO's quantity (2).");
                                return true;
                            }
                        }

                        if (Grd1.Cols[21].Visible)
                        {
                            if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                            {
                                Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                                return true;
                            }
                            if (Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21))
                            {
                                Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than DO's quantity (3).");
                                return true;
                            }
                        }
                    }
                }
            }
            if (mIsDoctUseCOAAccountList)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0 && Sm.IsGrdValueEmpty(Grd4, Row, 5, false, "Remark is empty."))
                    {
                        Sm.FocusGrd(Grd4, Row, 5);
                        return true;
                    }
                }

                //check balance
                decimal DAmtTotal = 0m, CAmtTotal = 0m;
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0)
                    {
                        if (Sm.GetGrdDec(Grd4, Row, 3) > 0)
                        {
                            DAmtTotal += Sm.GetGrdDec(Grd4, Row, 3);
                        }

                        if (Sm.GetGrdDec(Grd4, Row, 4) > 0)
                        {
                            CAmtTotal += Sm.GetGrdDec(Grd4, Row, 4);
                        }
                    }
                }
                if (DAmtTotal - CAmtTotal != 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "COA's Account List is not balance");
                    return true;
                }

            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (mIsAutoJournalActived)
            {
                var Msg =
                        "Journal's setting is invalid." + Environment.NewLine +
                        "Please contact Finance/Accounting department." + Environment.NewLine;

                if (!mIsDOCtUseProcessInd && !mIsItemCategoryUseCOAAPAR)
                {
                    if (!mIsCheckCOAJournalNotExists) return false;

                    //var SQL = new StringBuilder();
                    //var cm = new MySqlCommand();
                    string CtCode = Sm.GetLue(LueCtCode);
                    string mAcNoForCOGS = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForCOGS'");
                    string mCustomerAcNoNonInvoice = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CustomerAcNoNonInvoice'");
                    string mAcNoForSaleOfFinishedGoods = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForSaleOfFinishedGoods'");

                    if (mIsDOCtJournalUseCOGSAndStockEnabled)
                    {
                        if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo")) return true;
                        if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo5")) return true;
                    }
                    else
                    {
                        if (mIsAcNoForSaleUseItemCategory)
                        {
                            // Table
                            if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo5")) return true;
                            if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo4")) return true;
                        }
                        else
                        {
                            // Parameter
                            if (mAcNoForCOGS.Length == 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForCOGS is empty.");
                                return true;
                            }
                            // Parameter
                            if (mAcNoForSaleOfFinishedGoods.Length == 0)
                            {
                                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForSaleOfFinishedGoods is empty.");
                                return true;
                            }
                        }
                        // Table
                        if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo")) return true;
                        // Parameter
                        if (mCustomerAcNoNonInvoice.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                            return true;
                        }
                    }
                }
                if (mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
                {
                    if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo4")) return true;
                    if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo11")) return true;
                }
            }
            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;
            string AcName = string.Empty;
            if (COA == "AcNo") AcName = "(Stock)";
            if (COA == "AcNo4") AcName = "(Sales)";
            if (COA == "AcNo5") AcName = "(COGS)";
            if (COA == "AcNo11") AcName = "(AR Uninvoiced)";

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B." + COA + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 4);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                    r++;
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + AcName + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDOCtHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* DOCt - Hdr */ ");

            SQL.AppendLine("Insert Into TblDOCtHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, ");
            SQL.AppendLine("Driver, Expedition, VehicleRegNo, ProductionWorkGroup, ");
            if (mIsDOCtUseSOContract)
                SQL.AppendLine("SOContractDocNo, ");
            if(mDOCtUploadFileFormat == "1")
                SQL.AppendLine("FileName, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Status, @ProcessInd, @WhsCode, @CtCode, @DocNoInternal, @ResiNo, @CurCode, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, @SAPhone, @SAFax, @SAEmail, @SAMobile, ");
            SQL.AppendLine("@KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @KBSubmissionNo, @CustomsDocCode, ");
            SQL.AppendLine("@Driver, @Expedition, @VehicleRegNo, @ProductionWorkGroup, ");
            if (mIsDOCtUseSOContract)
                SQL.AppendLine("@SOContractDocNo, ");
            if (mDOCtUploadFileFormat == "1")
                SQL.AppendLine("@FileName, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");

            if (mIsDOCtApprovalActived)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='DOCt' ");
                if (mIsDOCtApprovalBasedOnWhs) SQL.AppendLine("And T.WhsCode=@WhsCode ");
                SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

                SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='DOCt' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And ProcessInd = 'F'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", mIsDOCtApprovalActived ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseProcessInd ? "D" : "F");
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocNoInternal", TxtDocNoInternal.Text);
            Sm.CmParam<String>(ref cm, "@ResiNo", TxtResiNo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", MeeSAAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", mCustomsDocCode);
            Sm.CmParam<String>(ref cm, "@Driver", TxtDriver.Text);
            Sm.CmParam<String>(ref cm, "@Expedition", TxtExpedition.Text);
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", TxtVehicleRegNo.Text);
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            if (mIsDOCtUseSOContract) Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (mDOCtUploadFileFormat == "1")
                Sm.CmParam<String>(ref cm, "@FileName", TxtFile.Text);
            return cm;
        }

        private MySqlCommand SaveDOCtDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var SQL4 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* DOCt - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL.AppendLine(", ");
                    }

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", 'N', @ItCode_" + r.ToString() +
                        ", @PropCode_" + r.ToString() +
                        ", @BatchNo_" + r.ToString() +
                        ", @ProcessInd, @Source_" + r.ToString() +
                        ", @Lot_" + r.ToString() +
                        ", @Bin_" + r.ToString() +
                        ", @QtyPackagingUnit_" + r.ToString() +
                        ", @PackagingUnitUomCode_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Qty2_" + r.ToString() +
                        ", @Qty3_" + r.ToString() +
                        ", @UPrice_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    if (mIsDOCtNotOnlyFromStock && Sm.GetGrdStr(Grd1, r, 11).Length <= 0)
                    {
                        SQL2.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, PropCode, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                        SQL2.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.PropCode, B.Source, 0.00, 0.00, 0.00, ");
                        SQL2.AppendLine("Case When A.Remark Is Null Then ");
                        SQL2.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                        SQL2.AppendLine("Else ");
                        SQL2.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                        SQL2.AppendLine("End As Remark, ");
                        SQL2.AppendLine("@UserCode, @Dt ");
                        SQL2.AppendLine("From TblDOCtHdr A ");
                        SQL2.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode_" + r.ToString());
                        SQL2.AppendLine(" Where A.DocNo=@DocNo; ");

                        SQL3.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                        SQL3.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
                        SQL3.AppendLine("Case When A.Remark Is Null Then ");
                        SQL3.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                        SQL3.AppendLine("Else ");
                        SQL3.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                        SQL3.AppendLine("End As Remark, ");
                        SQL3.AppendLine("@UserCode, @Dt ");
                        SQL3.AppendLine("From TblDOCtHdr A ");
                        SQL3.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode_" + r.ToString());
                        SQL3.AppendLine(" Where A.DocNo=@DocNo; ");

                        SQL4.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
                        SQL4.AppendLine("Select B.ItCode, B.BatchNo, B.Source, A.CurCode, B.UPrice, 1.00,  ");
                        SQL4.AppendLine("Case When A.Remark Is Null Then ");
                        SQL4.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                        SQL4.AppendLine("Else ");
                        SQL4.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                        SQL4.AppendLine("End As Remark, ");
                        SQL4.AppendLine("@UserCode, @Dt ");
                        SQL4.AppendLine("From TblDOCtHdr A ");
                        SQL4.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode_" + r.ToString());
                        SQL4.AppendLine(" Where A.DocNo=@DocNo; ");
                    }

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 4));
                    Sm.CmParam<String>(ref cm, "@PropCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    if (mIsDOCtNotOnlyFromStock && Sm.GetGrdStr(Grd1, r, 11).Length <= 0)
                    {
                        Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.Left(Sm.GetDte(DteDocDt), 8));
                        Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), string.Concat(mDocType, '*', DocNo, '*', Sm.Right("00" + (r + 1).ToString(), 3)));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                        Sm.CmParam<String>(ref cm, "@Source_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 11));
                    }
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 21));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 23));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 25));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 27));
                    Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 28));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQL2.ToString() + SQL3.ToString() + SQL4.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtProsessToSalesInvoice ? "O" : "F");
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);

            return cm;
        }

        private MySqlCommand SaveDOCtDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, @ProcessInd, @Source, @Lot, @Bin, @QtyPackagingUnit, @PackagingUnitUomCode, @Qty, @Qty2, @Qty3, @UPrice, @Remark, @UserCode, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            if (mIsDOCtNotOnlyFromStock && Sm.GetGrdStr(Grd1, Row, 11).Length <= 0)
            {
                Sm.CmParam<String>(ref cm, "@BatchNo", Sm.Left(Sm.GetDte(DteDocDt), 8));
                Sm.CmParam<String>(ref cm, "@Source", string.Concat(mDocType, '*', DocNo, '*', Sm.Right("00" + (Row + 1).ToString(), 3)));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
                Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            }
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 27));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (mIsDOCtProsessToSalesInvoice)
                Sm.CmParam<String>(ref cm, "@ProcessInd", "O");
            else
                Sm.CmParam<String>(ref cm, "@ProcessInd", "F");
            return cm;
        }

        private MySqlCommand SaveDOCtDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* DO To Customer - Dtl2 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd4.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblDOCtDtl2 ");
                        SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt ");
                    SQL.AppendLine(") ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd4, r, 4));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd4, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveStock2(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

        //    SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, PropCode, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.PropCode, B.Source, 0.00, 0.00, 0.00, ");
        //    SQL.AppendLine("Case When A.Remark Is Null Then ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //    SQL.AppendLine("Else ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //    SQL.AppendLine("End As Remark, ");
        //    SQL.AppendLine("@UserCode, @Dt ");
        //    SQL.AppendLine("From TblDOCtHdr A ");
        //    SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo And B.ItCode = @ItCode; ");

        //    SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
        //    SQL.AppendLine("Case When A.Remark Is Null Then ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //    SQL.AppendLine("Else ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //    SQL.AppendLine("End As Remark, ");
        //    SQL.AppendLine("@UserCode, @Dt ");
        //    SQL.AppendLine("From TblDOCtHdr A ");
        //    SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo And B.ItCode = @ItCode; ");

        //    SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select B.ItCode, B.BatchNo, B.Source, A.CurCode, B.UPrice, 1.00,  ");
        //    SQL.AppendLine("Case When A.Remark Is Null Then ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
        //    SQL.AppendLine("Else ");
        //    SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
        //    SQL.AppendLine("End As Remark, ");
        //    SQL.AppendLine("@UserCode, @Dt ");
        //    SQL.AppendLine("From TblDOCtHdr A ");
        //    SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo And B.ItCode = @ItCode; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@DocType", mDocType);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.ProcessInd = 'F' ");
            SQL.AppendLine("And B.CancelInd = 'N'; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            if (mDocNoFormat == "1")
                SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), mEntCode, "1"));
            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.ProcessInd = 'F'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
            {
                #region AMKA

                SQL.AppendLine("        Select X.AcNo, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X.Amt)) As DAmt, ");
                else
                    SQL.AppendLine("        Sum(X.Amt) As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo11 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0.00) End ");
                SQL.AppendLine("            *B.UPrice*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 1=1 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo11 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select X.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0.00) End ");
                SQL.AppendLine("            *B.UPrice*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 1=1 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");

                #endregion
            }
            else
            {
                #region Standard

                if (mIsAcNoForSaleUseItemCategory)
                {
                    SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
                    else
                        SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        Group By E.AcNo5 ");
                }
                else
                {
                    SQL.AppendLine("        Select D.ParValue As AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                    else
                        SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                else
                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");

                if (!mIsDOCtJournalUseCOGSAndStockEnabled)
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
                    else
                        SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                    SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 0=0 ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X1 ");
                    SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
                    SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");

                    SQL.AppendLine("        Union All ");
                    if (mIsAcNoForSaleUseItemCategory)
                    {
                        SQL.AppendLine("        Select X.AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                        else
                            SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                        SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                        SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                        SQL.AppendLine("            As Amt ");
                        SQL.AppendLine("            From TblDOCtHdr A ");
                        SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                        SQL.AppendLine("            Left Join ( ");
                        SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("            ) C On 0=0 ");
                        SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                        SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                        SQL.AppendLine("            Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        ) X ");
                        SQL.AppendLine("        Group By X.AcNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
                        else
                            SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                        SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                        SQL.AppendLine("            From TblDOCtHdr A ");
                        SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                        SQL.AppendLine("            Left Join ( ");
                        SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("            ) C On 1=1 ");
                        SQL.AppendLine("            Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        ) X1 ");
                        SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                        SQL.AppendLine("        Group By X2.ParValue ");
                    }
                }
                #endregion
            }
            SQL.AppendLine(JournalCOAAccountList(false));
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDOCtHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string mInitProcessInd = Sm.GetValue("Select ProcessInd From TblDOCtHdr Where DocNo = @Param;", TxtDocNo.Text);
            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsEditedDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mIsDOCtUseProcessInd)
            {
                if (mInitProcessInd == "D")
                    cml.Add(DeleteDtl(TxtDocNo.Text));

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (mInitProcessInd == "D")
                        cml.Add(SaveDOCtDtl(TxtDocNo.Text, i));

                    //cml.Add(EditDOCtDtlQty(i));

                    if (mInitProcessInd == "F")
                    {
                        if (Sm.GetGrdStr(Grd1, i, 4).Length > 0 && !Sm.GetGrdBool(Grd1, i, 2))
                        {
                            if (Sm.GetGrdBool(Grd1, i, 1))
                                cml.Add(EditDOCtDtl1(i));
                            else
                                cml.Add(EditDOCtDtl2(i));
                        }
                    }
                }

                if (mInitProcessInd == "D")
                    cml.Add(EditDOCtHdrRemarkProcess());

                if (mInitProcessInd != Sm.GetLue(LueProcessInd) && mInitProcessInd == "D")
                {
                    cml.Add(EditDOCtHdr());

                    cml.Add(SaveStock(TxtDocNo.Text));

                    if (mIsAutoJournalActived && !mIsItemCategoryUseCOAAPAR) cml.Add(SaveJournal(TxtDocNo.Text));
                }

                if (mIsDOCtApprovalActived) cml.Add(UpdateDOCtStatus(TxtDocNo.Text));

                if (mIsAutoJournalActived && !mIsItemCategoryUseCOAAPAR) cml.Add(SaveJournal());
            }
            else
            {
                if (mIsDOCtRemarkEditable) cml.Add(EditDOCtHdr());

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && !Sm.GetGrdBool(Grd1, Row, 2))
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1))
                            cml.Add(EditDOCtDtl1(Row));
                        else
                            cml.Add(EditDOCtDtl2(Row));
                    }
                }

                if (mIsDOCtApprovalActived) cml.Add(UpdateDOCtStatus(TxtDocNo.Text));

                if (mIsAutoJournalActived)
                {
                    if (!mIsItemCategoryUseCOAAPAR)
                        cml.Add(SaveJournal());
                    else
                        if (mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
                        cml.Add(SaveJournal());
                }
            }

            // EDIT UPLOAD FILE GRID
            if (mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat == "2")
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsDOCtAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                        {
                            if (Row >= mRow - 1)
                                if (IsUploadFileNotValidGrid(Row, Sm.GetGrdStr(Grd3, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFileGrid(TxtDocNo.Text));
            }

            Sm.ExecCommands(cml);

            if (mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat == "2")
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsDOCtAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                        {
                            if (Row >= mRow - 1)
                                UploadFileGrid(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd3, Row, 1));
                        }
                    }
                }
            }



            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDOCtDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            
            return
                (mIsDOCtUseProcessInd && Sm.IsLueEmpty(LueProcessInd, "Process")) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                // Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDOCtStatusCancelled() ||
                ((TxtStatus.Text == "Approved" || mDOCtUploadFileFormat != "2") && IsCancelledItemNotExisted(DNo)) ||
                (mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalEditNotValid()) ||
                IsCancelledDOCtInvalid() ||
                IsDataAlreadyUsedInRecvCt(DNo) || IsDataAlreadyUsedInSalesInvoice(DNo);
        }
        private bool IsCOAJournalEditNotValid()
        {
            var l = new List<COA>();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";


            SQL.AppendLine("Select AcNo From ");
            SQL.AppendLine("( ");


            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("Select E.AcNo5 As AcNo ");
                SQL.AppendLine("From TblDOCtHdr A ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("Select D.ParValue As AcNo ");
                SQL.AppendLine("From TblDOCtHdr A ");
                SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("Inner Join TblParameter D On D.ParCode='AcNoForCOGS'  ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select E.AcNo ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
            SQL.AppendLine("Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Concat(X2.ParValue, X3.CtCode) As AcNo ");
            SQL.AppendLine("from TblParameter X2  ");
            SQL.AppendLine("Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("where X2.ParCode='CustomerAcNoNonInvoice' ");
            SQL.AppendLine("Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("Select X.AcNo ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("    Select E.AcNo4 As AcNo ");
                SQL.AppendLine("    From TblDOCtHdr A ");
                SQL.AppendLine("    Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("    Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(") X ");
                SQL.AppendLine("Group By X.AcNo ");
            }
            else
            {

                SQL.AppendLine("Select Parvalue As AcNo ");
                SQL.AppendLine("from TblParameter where ParCode='AcNoForSaleOfFinishedGoods' ");

            }

            SQL.AppendLine(")T ; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0])

                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
            {
                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                return true;
            }

            return false;
        }

        private bool IsCancelledDOCtInvalid()
        {
            if (!mIsDOCtApprovalActived) return false;

            bool IsCancelled = Sm.GetGrdBool(Grd1, 0, 1);
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Sm.GetGrdBool(Grd1, r, 1) != IsCancelled)
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to cancel ALL items.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsEditedDataNotValid2(String DNo)
        {
            if (!mIsDOCtPriceEditable) return false;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (mIsDOCtPriceEditable)
                {
                    if (Sm.GetGrdBool(Grd1, r, 1))
                    {
                        IsEditedDataNotValid(DNo);
                        return true;
                    }
                }
            }
            return false;
        }
        private bool IsDataAlreadyUsedInRecvCt(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo From TblRecvCtDtl ");
            SQL.AppendLine("Where DOCtDocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DOCtDNo, '##') In @DNo)>0 Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);

            var key = Sm.GetValue(cm);
            if (key.Length != 0)
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                    {
                        Msg =
                           "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                           "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                           "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                           "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                           "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                           "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                           "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                           "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;
                        break;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in receiving from customer transactions.");
                return true;
            }
            return false;
        }

        

        private bool IsDataAlreadyUsedInSalesInvoice(string DNo)
        {
            if (mIsDOCtProsessToSalesInvoice)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.DOCtDno As KeyWord ");
                SQL.AppendLine("from tblSalesinvoiceHdr A ");
                SQL.AppendLine("Inner Join TblsalesinvoiceDtl B On A.DocNo = B.Docno ");
                SQL.AppendLine("Where A.cancelInd = 'N' ");
                SQL.AppendLine("And B.DOCtDocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And Position(Concat('##', DOCtDNo, '##') In @DNo)>0 Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);

                var key = Sm.GetValue(cm);
                if (key.Length != 0)
                {
                    string Msg = string.Empty;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                        {
                            Msg =
                               "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                               "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                               "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                               "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                               "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                               "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                               "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                               "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;
                            break;
                        }
                    }
                    Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in sales invoice transactions.");
                    return true;
                }
            }
            return false;

        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (mIsDOCtUseProcessInd) return false;

            if (Sm.CompareStr(DNo, "##XXX##") && !mIsDOCtRemarkEditable)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand DeleteDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblDOCtDtl ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblDOCtHdr  ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And ProcessInd = 'D' ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        private MySqlCommand EditDOCtDtlQty(int Row)
        {
            string sSQL = string.Empty;

            sSQL += "Update TblDOCtDtl ";
            sSQL += "Set Qty = @Qty, Qty2 = @Qty2, Qty3 = @Qty3, UPrice = @UPrice, ";
            sSQL += "LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ";
            sSQL += "Where DocNo = @DocNo ";
            sSQL += "And DNo = @DNo ";
            sSQL += "And CancelInd = 'N'; ";

            var cm = new MySqlCommand() { CommandText = sSQL };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtDtl1(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty*-1, Qty2*-1, Qty3*-1, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And DocType=@DocType ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And DNo=@DNo ");
            SQL.AppendLine("        And CancelInd='Y' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where A.Qty<>0 ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtDtl2(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl Set UPrice=@UPrice, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private string EditDOCtHdrRemarkProcessString()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set Remark=@Remark, ProcessInd = @ProcessInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            return SQL.ToString();
        }

        private MySqlCommand EditDOCtHdrRemarkProcess()
        {
            var cm = new MySqlCommand() { CommandText = EditDOCtHdrRemarkProcessString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseProcessInd ? Sm.GetLue(LueProcessInd) : "F");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(EditDOCtHdrRemarkProcessString());

            if (mIsDOCtApprovalActived && mIsDOCtUseProcessInd)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='DOCt' ");
                if (mIsDOCtApprovalBasedOnWhs) SQL.AppendLine("And T.WhsCode=@WhsCode ");
                SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

                SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='DOCt' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseProcessInd ? Sm.GetLue(LueProcessInd) : "F");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDOCtStatus(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    Status='C', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDOCtDtl Where CancelInd='N' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var SQL = new StringBuilder();
            var EntCode = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            if (Filter.Length > 0)
            {
                SQL.AppendLine("Update TblDOCtDtl Set JournalDocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
                SQL.AppendLine("And Exists(Select 1 From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null) ");
                SQL.AppendLine("And JournalDocNo Is Null; ");

                SQL.AppendLine("Insert Into TblJournalHdr ");
                SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @JournalDocNo, ");
                if (IsClosingJournalUseCurrentDt)
                    SQL.AppendLine("Replace(CurDate(), '-', ''), ");
                else
                    SQL.AppendLine("DocDt, ");
                SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
                SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblJournalHdr ");
                SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                if (mIsDOCtJournalUseItCtARUnInvoicedAndSalesEnabled)
                {
                    #region AMKA

                    SQL.AppendLine("        Select X.AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                    else
                        SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("            Select E.AcNo11 As AcNo, ");
                    SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0.00) End ");
                    SQL.AppendLine("            *B.UPrice*B.Qty ");
                    SQL.AppendLine("            As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine(Filter);
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 1=1 ");
                    SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo11 Is Not Null ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X ");
                    SQL.AppendLine("        Group By X.AcNo ");
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select X.AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Sum(Floor(X.Amt)) As DAmt, ");
                    else
                        SQL.AppendLine("        Sum(X.Amt) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                    SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0.00) End ");
                    SQL.AppendLine("            *B.UPrice*B.Qty ");
                    SQL.AppendLine("            As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine(Filter);
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 1=1 ");
                    SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X ");
                    SQL.AppendLine("        Group By X.AcNo ");

                    #endregion
                }
                else
                {
                    #region Standard

                    if (mIsAcNoForSaleUseItemCategory)
                    {
                        SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                        else
                            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                        SQL.AppendLine("        From TblDOCtHdr A ");
                        SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select D.ParValue As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                        else
                            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                        SQL.AppendLine("        From TblDOCtHdr A ");
                        SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@DocNo ");
                    }

                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                    else
                        SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");

                    if (!mIsDOCtJournalUseCOGSAndStockEnabled)
                    {
                        SQL.AppendLine("        Union All ");
                        SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
                        SQL.AppendLine("        0.00 As DAmt, ");
                        SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                        SQL.AppendLine("        From ( ");
                        if (mIsDOCtAmtRounded)
                            SQL.AppendLine("            Select Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                        else
                            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                        SQL.AppendLine("            From TblDOCtHdr A ");
                        SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                        SQL.AppendLine("            Left Join ( ");
                        SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                        SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                        SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                        SQL.AppendLine("            ) C On 0=0 ");
                        SQL.AppendLine("            Where A.DocNo=@DocNo ");
                        SQL.AppendLine("        ) X1 ");
                        SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
                        SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
                        SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
                        SQL.AppendLine("        Union All ");

                        if (mIsAcNoForSaleUseItemCategory)
                        {
                            SQL.AppendLine("        Select X.AcNo, ");
                            SQL.AppendLine("        Sum(X.Amt) As DAmt, ");
                            SQL.AppendLine("        0.00 As CAmt ");
                            SQL.AppendLine("        From ( ");
                            SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("            Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                            else
                                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                            SQL.AppendLine("            From TblDOCtHdr A ");
                            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("            Left Join ( ");
                            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("            ) C On 0=0 ");
                            SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                            SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                            SQL.AppendLine("            Where A.DocNo=@DocNo ");
                            SQL.AppendLine("        ) X ");
                            SQL.AppendLine("        Group By X.AcNo ");
                        }
                        else
                        {
                            SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                            SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
                            SQL.AppendLine("        0.00 As CAmt ");
                            SQL.AppendLine("        From ( ");
                            if (mIsDOCtAmtRounded)
                                SQL.AppendLine("            Select Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                            else
                                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                            SQL.AppendLine("            From TblDOCtHdr A ");
                            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                            SQL.AppendLine("            Left Join ( ");
                            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                            SQL.AppendLine("            ) C On 0=0 ");
                            SQL.AppendLine("            Where A.DocNo=@DocNo ");
                            SQL.AppendLine("        ) X1 ");
                            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                            SQL.AppendLine("        Group By X2.ParValue ");
                        }
                    }
                    #endregion
                }
                SQL.AppendLine(JournalCOAAccountList(true));
                SQL.AppendLine("    ) Tbl Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            }
            else
            {
                SQL.AppendLine("Select 1; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedIndex = 0;

            if (Grd3.SelectedRows.Count > 0)
            {
                for (int Index = Grd3.SelectedRows.Count - 1; Index >= 0; Index--)
                    SelectedIndex = Grd3.SelectedRows[Index].Index;
            }

            if (mStateIndicator == "E" && TxtStatus.Text != "Approved")
            {
                if (Sm.GetGrdStr(Grd3, SelectedIndex, 4).Length == 0)
                {
                    Sm.GrdRemoveRow(Grd3, e, BtnSave);
                    Sm.GrdEnter(Grd3, e);
                    Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
                }
            }
            else if (mStateIndicator == "I")
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdEnter(Grd3, e);
                Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOCtHdr(DocNo);
                ShowDOCtDtl(DocNo);
                ShowDOCtDtl3(DocNo);
                ComputeTotalQty();
                if (mIsDOCtAllowToUploadFile && mDOCtUploadFileFormat == "2") ShowUploadFileGrid(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowDOCt(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Sm.ClearGrd(Grd1, true);
                Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
                Sm.FocusGrd(Grd1, 0, 0);
                ShowDOCtHdr2(DocNo);
                ShowDOCtDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOCtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (mIsCustomerComboBasedOnCategory)
            {
                Sl.SetLueCtCtCode(ref LueCtCtCode);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty);
            }

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, A.CtCode, A.DocNoInternal, A.CurCode, ");
            SQL.AppendLine("A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, A.SAPostalCd, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile,  A.ResiNo, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, ");
            SQL.AppendLine("A.KBSubmissionNo, A.CustomsDocCode, A.ProductionWorkGroup, ");
            SQL.AppendLine("A.Driver, A.Expedition, A.VehicleRegNo, A.ProcessInd, E.CtCtCode, ");
            if (mIsDOCtUseSOContract)
                SQL.AppendLine("A.SOContractDocNo, D.ProjectCode, D.PONo, D.ProjectNAme, ");
            else
                SQL.AppendLine("Null As SOContractDocNo, Null As ProjectCode, Null As PONo, Null As ProjectName, ");
            SQL.AppendLine("A.FileName  ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
            if (mIsDOCtUseSOContract)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T1.SOContractDocNo, T5.ProjectCode, T2.PONo, IfNull(T5.ProjectName, T4.ProjectName) ProjectName ");
                SQL.AppendLine("    From TblDOCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T2 On T1.SOContractDocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.DocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T3 On T2.BOQDocNo = T3.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T4 On T3.LOPDocNo = T4.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T5 On T4.PGCode = T5.PGCode ");
                SQL.AppendLine(") D On A.DocNo = D.DocNo ");
            }
            SQL.AppendLine("Inner Join TblCustomer E On A.CtCode = E.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "WhsCode", "CtCode", "DocNoInternal", "SAName",  

                    //6-10
                    "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName",  

                    //11-15
                    "SAPostalCd", "SAPhone", "SAFax", "SAEmail", "SAMobile",

                    //16-20
                    "CurCode", "ResiNo", "KBContractNo", "KBContractDt", "KBPLNo", 
                    
                    //21-25
                    "KBPLDt", "KBRegistrationNo", "KBRegistrationDt", "Remark", "StatusDesc",

                    //26-30
                    "KBSubmissionNo", "CustomsDocCode", "Driver", "Expedition", "VehicleRegNo",

                    //31-35
                    "ProcessInd", "ProductionWorkGroup", "SOContractDocNo", "ProjectCode", "PONo",

                    //36-38
                    "ProjectName", "CtCtCode", "FileName"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                    Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                    TxtDocNoInternal.EditValue = Sm.DrStr(dr, c[4]);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[5]);
                    MeeSAAddress.EditValue = Sm.DrStr(dr, c[6]);
                    mCityCode = Sm.DrStr(dr, c[7]);
                    TxtCity.EditValue = Sm.DrStr(dr, c[8]);
                    mCntCode = Sm.DrStr(dr, c[9]);
                    TxtCountry.EditValue = Sm.DrStr(dr, c[10]);
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[11]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[12]);
                    TxtFax.EditValue = Sm.DrStr(dr, c[13]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[14]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[15]);
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                    TxtResiNo.EditValue = Sm.DrStr(dr, c[17]);
                    TxtKBContractNo.EditValue = Sm.DrStr(dr, c[18]);
                    Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[19]));
                    TxtKBPLNo.EditValue = Sm.DrStr(dr, c[20]);
                    Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[21]));
                    TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[22]);
                    Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[23]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[24]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[25]);
                    TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[26]);
                    mCustomsDocCode = Sm.DrStr(dr, c[27]);
                    TxtDriver.EditValue = Sm.DrStr(dr, c[28]);
                    TxtExpedition.EditValue = Sm.DrStr(dr, c[29]);
                    TxtVehicleRegNo.EditValue = Sm.DrStr(dr, c[30]);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[31]));
                    Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[32]));
                    if (mIsDOCtUseSOContract)
                    {
                        TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[33]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[34]);
                        TxtPONo.EditValue = Sm.DrStr(dr, c[35]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[36]);
                    }
                    if (mIsCustomerComboBasedOnCategory) Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[37]));
                    TxtFile.EditValue = Sm.DrStr(dr, c[38]);
                }, true
            );
        }

        private void ShowDOCtDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            //SQL.AppendLine("IfNull(E.Qty, 0) + Case When B.CancelInd='N' Then B.Qty Else 0 End As Stock, ");
            //SQL.AppendLine("IfNull(E.Qty2, 0) + Case When B.CancelInd='N' Then B.Qty2 Else 0 End As Stock2, ");
            //SQL.AppendLine("IfNull(E.Qty3, 0) + Case When B.CancelInd='N' Then B.Qty3 Else 0 End As Stock3, ");
            SQL.AppendLine("IfNull(E.Qty, 0) Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0) Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0) Stock3, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            if (mIsDOCtShowStockPrice)
                SQL.AppendLine("H.UPrice*H.ExcRate As StockPrice, ");
            else
                SQL.AppendLine("0.00 As StockPrice, ");
            SQL.AppendLine("B.UPrice, B.Remark, C.ItGrpCode, B.QtyPackagingUnit, B.PackagingUnitUomCode, F.CtItCode, C.Specification, G.DNo As CtQtInd  ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem ");
            SQL.AppendLine(")F On A.CtCode = F.CtCode And B.ItCode = F.itCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct T5.DNo ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner join TblCtQtDtl T2 On T1.DocNo = T2.Docno ");
            SQL.AppendLine("        And T1.ActInd = 'Y' ");
            SQL.AppendLine("        And T1.CtCode = (Select CtCode From TblDOCtHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("        And T1.Status = 'A' ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T3.DocNo = T4.DocNo And T2.ItemPriceDNo = T4.DNo ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T5 On T5.DocNo = @DocNo And T5.ItCode = T4.ItCode ");
            SQL.AppendLine(") G On B.DNo = G.DNo ");
            if (mIsDOCtShowStockPrice)
                SQL.AppendLine("Left Join TblStockPrice H On B.Source=H.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo");
            if (mIsCopyingDOUseActiveDoc && isTextNotNull)
            {
                SQL.AppendLine(" AND B.CancelInd = 'N' ");
            }
            SQL.AppendLine("Order By B.DNo ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "Stock", "Qty", "InventoryUomCode", "Stock2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "Stock3", "Qty3", "InventoryUomCode3", "UPrice", 
                    
                    //21-25
                    "Remark", "ItGrpCode", "QtyPackagingUnit", "PackagingUnitUomCode", "CtItCode",
 
                    //26-29
                    "ForeignName", "Specification", "CtQtInd", "StockPrice"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    ComputeTotal(Row);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 28);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 29);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDOCtDtl3(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, Null as LocalName ");
            SQL.AppendLine("From TblDOCtDtl2 A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "LocalName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        private void ShowDOCtHdr2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, A.CtCode, A.DocNoInternal, A.CurCode, ");
            SQL.AppendLine("A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, A.SAPostalCd, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile,  A.ResiNo, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, ");
            SQL.AppendLine("A.KBSubmissionNo, A.CustomsDocCode ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "WhsCode", "CtCode", "DocNoInternal", "SAName",  

                        //6-10
                        "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName",  

                        //11-15
                        "SAPostalCd", "SAPhone", "SAFax", "SAEmail", "SAMobile",
   
                        //16-20
                        "CurCode", "ResiNo", "KBContractNo", "KBContractDt", "KBPLNo", 
                        
                        //21-25
                        "KBPLDt", "KBRegistrationNo", "KBRegistrationDt", "Remark", "StatusDesc",

                        //26-27
                        "KBSubmissionNo", "CustomsDocCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {

                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                        TxtDocNoInternal.EditValue = Sm.DrStr(dr, c[4]);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[5]);
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[6]);
                        mCityCode = Sm.DrStr(dr, c[7]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[8]);
                        mCntCode = Sm.DrStr(dr, c[9]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[12]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[13]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[14]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                        TxtResiNo.EditValue = Sm.DrStr(dr, c[17]);
                        TxtKBContractNo.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[19]));
                        TxtKBPLNo.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[21]));
                        TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[23]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[24]);
                        TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[26]);
                        mCustomsDocCode = Sm.DrStr(dr, c[27]);
                    }, true
                );
        }

        private void ShowDOCtDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("IfNull(E.Qty, 0) + Case When B.CancelInd='N' Then B.Qty Else 0 End As Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0) + Case When B.CancelInd='N' Then B.Qty2 Else 0 End As Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0) + Case When B.CancelInd='N' Then B.Qty3 Else 0 End As Stock3, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.UPrice, B.Remark, C.ItGrpCode, B.QtyPackagingUnit, B.PackagingUnitUomCode, F.CtItCode, C.Specification  ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.ItCode=E.ItCode ");
            SQL.AppendLine("    And B.PropCode=E.PropCode ");
            SQL.AppendLine("    And B.BatchNo=E.BatchNo ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem ");
            SQL.AppendLine(")F On A.CtCode = F.CtCode And B.ItCode = F.itCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "Stock", "Qty", "InventoryUomCode", "Stock2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "Stock3", "Qty3", "InventoryUomCode3", "UPrice", 
                    
                    //21-25
                    "Remark", "ItGrpCode", "QtyPackagingUnit", "PackagingUnitUomCode", "CtItCode",
 
                    //26-27
                    "ForeignName", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    ComputeTotal(Row);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27 });
            Sm.FocusGrd(Grd1, 0, 1);
        }


        private void ShowShippingAddressData(string CtCode, string ShipName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ShipName", ShipName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.Address, A.CityCode, B.CityName, A.CntCode, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
                    "From TblCustomerShipAddress A " +
                    "Left Join TblCity B On A.CityCode = B.CityCode " +
                    "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Name=@ShipName ",
                    new string[]
                    {
                        "Address",
                        "CityCode", "CityName", "CntCode", "CntName", "PostalCd",
                        "Phone", "Fax", "Email", "Mobile"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[0]);
                        mCityCode = Sm.DrStr(dr, c[1]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[2]);
                        mCntCode = Sm.DrStr(dr, c[3]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[7]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
                    }, false
                );
        }

        #endregion

        #region Additional Method

        public string JournalCOAAccountList(bool IsCancel)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("UNION ALL ");
            if (!IsCancel)
                SQL.AppendLine("SELECT B.AcNo as AcNo, B.DAmt as DAmt, B.CAmt as CAmt ");
            else
                SQL.AppendLine("SELECT B.AcNo as AcNo, B.DAmt as CAmt, B.CAmt as DAmt ");
            SQL.AppendLine("FROM tbldocthdr A ");
            SQL.AppendLine("INNER JOIN tbldoctdtl2 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");

            return SQL.ToString();
        }

        #region Upload File Grid

        private MySqlCommand SaveUploadFileGrid(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* DOCt - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            //if (TxtDocNo.Text.Length > 0)
            //    SQL.AppendLine("Delete From TblDOCtFile Where DocNo=@DocNo; ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0 && Sm.GetGrdStr(Grd3, r, 4).Length == 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblDOCtFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName), LastUpBy = @UserCode , LastUpDt = @Dt; ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void ShowUploadFileGrid(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                   "Select A.DNo, A.FileName, B.UserName, A.CreateDt " +
                   "From  TblDOCtFile A " +
                   "Inner Join TblUser B On A.CreateBy = B.UserCode " +
                   "Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName", "UserName", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd3, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd3, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }
        private void DownloadFileGrid(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFileGrid(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValidGrid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFileGrid(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValidGrid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValidGrid(Row, FileName) ||
                IsFileSizeNotvalidGrid(Row, FileName) ||
                IsFileNameAlreadyExistedGrid(Row, FileName);
        }

        private bool IsFTPClientDataNotValidGrid(int Row, string FileName)
        {
            

            if (mIsDOCtAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsDOCtAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsDOCtAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsDOCtAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalidGrid(int Row, string FileName)
        {
            if (mIsDOCtAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExistedGrid(int Row, string FileName)
        {
            if (mIsDOCtAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblDOCtFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFileGrid(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }
        #endregion

        private void SetLueCtCode(ref LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.CtCode As Col1, ");
            if (Sm.GetParameterBoo("IsCustomerComboShowCategory"))
                SQL.AppendLine("CONCAT(T.CtName, ' [', IFNULL(T2.CtCtName, ' '), ']') As Col2  ");
            else
                SQL.AppendLine("T.CtName As Col2 ");
            SQL.AppendLine("From TblCustomer T ");
            SQL.AppendLine("Left Join TblCustomerCategory T2 On T.CtCtCode=T2.CtCtCode ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (CtCode.Length != 0) SQL.AppendLine("Or T.CtCode=@CtCode ");
            if (mIsCustomerComboBasedOnCategory) SQL.AppendLine("And T2.CtCtCode = @CtCtCode ");
            SQL.AppendLine("Order By T.CtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (CtCode.Length != 0) Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            if (mIsCustomerComboBasedOnCategory) Sm.CmParam<String>(ref cm, "@CtCtCode", Sm.GetLue(LueCtCtCode));

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CtCode.Length > 0) Sm.SetLue(Lue, CtCode);
        }

        private void SetLueProcessInd(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'D' As Col1, 'Draft' As Col2 ");
            SQL.AppendLine("Union All Select 'F' As Col1, 'Final' As Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(
              ref Lue, ref cm,
              0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void GetCtQtUPrice(int Row)
        {
            if (mIsDOCtUseCtQtPrice)
            {
                var SQL = new StringBuilder();

                //SQL.AppendLine("Select D.UPrice ");
                //SQL.AppendLine("From TblCtQtHdr A ");
                //SQL.AppendLine("Inner Join TblCtQtDtl B On A.DocNo = B.Docno And A.ActInd = 'Y' ");
                //SQL.AppendLine("    And A.CtCode = @Param1 ");
                //SQL.AppendLine("    And A.Status = 'A' ");
                //SQL.AppendLine("Inner Join TblItemPriceHdr C On B.ItemPriceDocNo = C.DocNo ");
                //SQL.AppendLine("Inner Join TblItemPriceDtl D On B.ItemPriceDocNo = D.DocNo And B.ItemPriceDNo = D.DNo ");
                //SQL.AppendLine("    And D.ItCode = @Param2 ");
                //SQL.AppendLine("Order By A.DocDt Desc ");
                //SQL.AppendLine("Limit 1; ");

                SQL.AppendLine("Select B.UPrice ");
                SQL.AppendLine("From TblItemPriceHdr A ");
                SQL.AppendLine("Inner Join TblItemPriceDtl B On A.DocNo = B.DocNo And B.ItCode = @Param2 ");
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                SQL.AppendLine("Order By A.DocDt Desc ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueCtCode), Sm.GetGrdStr(Grd1, Row, 4), string.Empty))
                {
                    Grd1.Cells[Row, 23].Value = Decimal.Parse(Sm.GetValue(SQL.ToString(), Sm.GetLue(LueCtCode), Sm.GetGrdStr(Grd1, Row, 4), string.Empty));
                }

                if (!mIsDOCtPriceEditable)
                {
                    Grd1.Cells[Row, 23].ReadOnly = iGBool.True;
                    Grd1.Cells[Row, 23].BackColor = Color.FromArgb(224, 224, 224);
                }
                else Grd1.Cells[Row, 23].ReadOnly = iGBool.False;
            }
        }

        private bool IsAuthorizedToAccessPEB()
        {
            bool mFlag = false;
            string mCurrentGrpCode = Sm.GetValue("Select GrpCode From TblUser Where UserCode = @Param; ", Gv.CurrentUserCode);

            string[] mPEBGrpCodeList = mPEBGrpCode.Split(',');
            foreach (string m in mPEBGrpCodeList)
            {
                if (m == mCurrentGrpCode)
                {
                    mFlag = true;
                    break;
                }
            }

            return mFlag;
        }

        private void GetPackagingUnitInfo(
            string ItCode, string PackagingUnitUomCode,
            ref decimal Qty, ref decimal Qty2, ref string SalesUomCode, ref string SalesUomCode2)
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Qty, Qty2, B.SalesUomCode, B.SalesUomCode2 ");
            SQL.AppendLine("From TblItemPackagingUnit A ");
            SQL.AppendLine("Inner Join TblItem B on A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.ItCode=@ItCode And A.UomCode=@PackagingUnitUomCode Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", PackagingUnitUomCode);

                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    { 
                        //0
                        "Qty", 
                        
                        //1-3
                        "Qty2", "SalesUomCode", "SalesUomCode2"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Qty = Sm.DrDec(dr, 0);
                        Qty2 = Sm.DrDec(dr, 1);
                        SalesUomCode = Sm.DrStr(dr, 2);
                        SalesUomCode2 = Sm.DrStr(dr, 3);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        //private void SetNumberOfInventoryUomCode()
        //{
        //    string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
        //    if (NumberOfInventoryUomCode.Length == 0)
        //        mNumberOfInventoryUomCode = 1;
        //    else
        //        mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        //}

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3"
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }
        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };



        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint(int parValue)
        {
            string Doctitle = Sm.GetParameter("Doctitle");

            var l = new List<DOCt>();
            var ldtl = new List<DOCtDtl>();
            var l2 = new List<DoctSignature>();
            var l3 = new List<InvoiceHdr>();
            var ldtl2 = new List<InvoiceDtl>();
            var l4 = new List<SignatureMNet>();

            string[] TableName = { "DOToCt", "DOToCtDtl", "DoctSignature", "InvoiceHdr", "InvoiceDtl", "SignatureMNet" };
            List<IList> myLists = new List<IList>();


            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            decimal mTotal = decimal.Parse(TxtAmt.Text);

            #region Old Query

            //SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            //SQL.AppendLine("Replace(A.DocNo, Concat('/',(Select ParValue From Tblparameter Where ParCode = 'DocTitle')),'') As DocNo,");
            //SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.DocNoInternal, C.CtName, ");
            //SQL.AppendLine("A.SAName, A.SAAddress, D.CityName As SACityCode, E.CntName As SACntCode, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.Remark, '0' As Indicator ");
            //SQL.AppendLine("From TblDOCtHdr A ");
            //SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            //SQL.AppendLine("Left Join TblCity D On A.SACityCode = D.CityCode ");
            //SQL.AppendLine("Left Join TblCountry E On A.SACntCode = E.CntCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");

            #endregion

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, IfNull(G.CompanyName,(Select ParValue From TblParameter Where Parcode='ReportTitle1'))As CompanyName, G.CompanyPhone, G.CompanyFax, ");
                SQL.AppendLine("IfNull(G.CompanyAddress,(Select ParValue From TblParameter Where Parcode='ReportTitle2'))As CompanyAddress,  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3')As CompanyAddressFull,  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4')As CompanyPhoneFull, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'CompanyNPWP', null as CompanyPhoneFull, ");
            }
            if (Sm.GetParameter("DocTitle") == "SIER" || Sm.GetParameter("DocTitle") == "MNET")
                SQL.AppendLine("A.DocNo,");

            else
                SQL.AppendLine("Replace(A.DocNo, Concat('/',(Select ParValue From Tblparameter Where ParCode = 'DocTitle')),'') As DocNo,");
            SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.DocNoInternal, C.CtName, A.SAName, A.SAAddress, D.CityName As SACityCode, ");
            SQL.AppendLine("E.CntName As SACntCode, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.Remark, '0' As Indicator, ");
            SQL.AppendLine("(Select Length(substring(parvalue,11)) As FormatNum from tblparameter Where Parcode='FormatNum0') As 'FormatNum',");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowForeignName') As IsForeignName, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowBatchNo') As IsShowBatchNo, A.Driver, A.VehicleRegNo, A.ResiNo, C.Address As CustomerAddress, ");
            SQL.AppendLine("H.CtCtName, I.ContactPersonName, A.Expedition ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("Left Join TblCity D On A.SACityCode = D.CityCode ");
            SQL.AppendLine("Left Join TblCountry E On A.SACntCode = E.CntCode ");
            SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode=F.WhsCode ");

            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, E.EntName As CompanyName, E.EntPhone As CompanyPhone, E.EntFax As CompanyFax, E.EntAddress As CompanyAddress  ");

                SQL.AppendLine("    From TblDoCtHdr A  ");
                SQL.AppendLine("    Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
                SQL.AppendLine("    Left Join Tblcostcenter C On B.CCCode = C.CCCode ");
                SQL.AppendLine("    Left Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode  ");
                SQL.AppendLine("    Left Join TblEntity E On D.EntCode = E.EntCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(") G On A.DocNo = G.DocNo ");
            }
            SQL.AppendLine("Inner Join TblCustomerCategory H On C.CtCtCode = H.CtCtCode ");
            SQL.AppendLine("Left Join TblCustomerContactPerson I On A.CtCode = I.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Limit 1 ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select E.EntLogoName " +
                       "From TblDoCtHdr A  " +
                       "Inner Join TblWarehouse B On A.WhsCode = B.WhsCode " +
                       "Left Join Tblcostcenter C On B.CCCode = C.CCCode " +
                       "Left Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode " +
                       "Left Join TblEntity E On D.EntCode = E.EntCode " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    if (CompanyLogo.Length > 0)
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyPhone",
                    "DocNo",
                    "DocDt",

                    //6-10
                    "DocNoInternal",
                    "CtName",
                    "SANAme",
                    "SAAddress",
                    "SACityCode",

                    //11-15
                    "SACntCode",
                    "SAPostalCd",
                    "SAPhone",
                    "SAFax",
                    "SAEmail",

                    //16-20
                    "SAMobile",
                    "Remark",
                    "Indicator",    
                    "FormatNum",
                    "IsForeignName",

                    //21-25
                    "IsShowBatchNo",
                    "CompanyAddressFull",
                    "CompanyPhoneFull",
                    "Driver",
                    "VehicleRegNo",
                    //26
                    "ResiNo",
                    "CustomerAddress",
                    "CtCtName",
                    "ContactPersonName",
                    "Expedition"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DOCt()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            DocNoInternal = Sm.DrStr(dr, c[6]),
                            CtName = Sm.DrStr(dr, c[7]),
                            Name = Sm.DrStr(dr, c[8]),
                            Address = Sm.DrStr(dr, c[9]),
                            CityName = Sm.DrStr(dr, c[10]),
                            CntName = Sm.DrStr(dr, c[11]),
                            PostalCd = Sm.DrStr(dr, c[12]),
                            Phone = Sm.DrStr(dr, c[13]),
                            Fax = Sm.DrStr(dr, c[14]),
                            Email = Sm.DrStr(dr, c[15]),
                            Mobile = Sm.DrStr(dr, c[16]),
                            HRemark = Sm.DrStr(dr, c[17]),
                            Indicator = Sm.DrStr(dr, c[18]),
                            FormatNum = Sm.DrStr(dr, c[19]),
                            IsForeignName = Sm.DrStr(dr, c[20]),
                            IsShowBatchNo = Sm.DrStr(dr, c[21]),
                            Terbilang = Sm.Terbilang(mTotal),
                            CompanyAddressFull = Sm.DrStr(dr, c[22]),
                            CompanyPhoneFull = Sm.DrStr(dr, c[23]),
                            ExpDriver = Sm.DrStr(dr, c[24]),
                            ExpPlatNo = Sm.DrStr(dr, c[25]),
                            ResiNo = Sm.DrStr(dr, c[26]),
                            CustomerAddress = Sm.DrStr(dr, c[27]),
                            CtCtName = Sm.DrStr(dr, c[28]),
                            ContactPersonName = Sm.DrStr(dr, c[29]),
                            ExpName = Sm.DrStr(dr, c[30]),
                            CompanyFooterImage = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png",
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            decimal Numb = 0;
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select B.ItCode, ");
                if (Sm.GetParameter("DocTitle") == "SIER")
                {
                    SQLDtl.AppendLine("C.ItName As ItName, ");
                    SQLDtl.AppendLine("IfNull(B.Remark, Concat('Bulan ', ");
                    SQLDtl.AppendLine("    Case	Date_Format(A.DocDt, '%m') ");
                    SQLDtl.AppendLine("        When '01' Then 'Januari' When '02' Then 'Februari' When '03' Then 'Maret' ");
                    SQLDtl.AppendLine("        When '04' Then 'April' When '05' Then 'Mei' When '06' Then 'Juni' ");
                    SQLDtl.AppendLine("        When '07' Then 'Juli' When '08' Then 'Agustus' When '09' Then 'September' ");
                    SQLDtl.AppendLine("        When '10' Then 'Oktober' When '11' Then 'November' When '12' Then 'Desember' ");
                    SQLDtl.AppendLine("    End, ' ', Date_Format(A.DocDt, '%Y') ");
                    SQLDtl.AppendLine(")) As Remark, ");
                }
                else
                    SQLDtl.AppendLine("B.Remark, IF((SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle')='IOK',C.ForeignName,C.ItName) AS ItName, ");
                SQLDtl.AppendLine("C.ForeignName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                SQLDtl.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
                SQLDtl.AppendLine("0 As PackingQty, '' as PackingUOMCode, C.ItGrpCode, B.UPrice, ");
                if (mIsDOCtAmtRounded)
                {
                    SQLDtl.AppendLine("Floor(B.Qty*B.UPrice)  As Amt, ");
                    SQLDtl.AppendLine("Floor(B.Qty2*B.UPrice) As Amt2, ");
                    SQLDtl.AppendLine("Floor(B.Qty3*B.UPrice) As Amt3 ");
                }
                else
                {
                    SQLDtl.AppendLine("(B.Qty*B.UPrice) As Amt, ");
                    SQLDtl.AppendLine("(B.Qty2*B.UPrice)As Amt2, ");
                    SQLDtl.AppendLine("(B.Qty3*B.UPrice)As Amt3 ");
                }
                SQLDtl.AppendLine("From TblDOCtHdr A ");
                SQLDtl.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl.AppendLine("Left Join TblItemGroup O1 On O1.ItGrpCode=C.ItGrpCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo And B.CancelInd = 'N' Order By B.DNo");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "ItCode" ,

                    //1-5
                    "ItName" ,
                    "BatchNo",
                    "Source",
                    "Lot",
                    "Bin",

                    //6-10
                    "Qty" ,
                    "Qty2",
                    "Qty3",
                    "InventoryUomCode" ,
                    "InventoryUomCode2" ,

                    //11-15
                    "InventoryUomCode3" ,
                    "Remark",
                    "PackingQty",
                    "PackingUOMCode",
                    "ItGrpCode",

                    //16-20
                    "ForeignName",
                    "UPrice",
                    "Amt",
                    "Amt2",
                    "Amt3", 
         
                });

                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        Numb = Numb + 1;
                        ldtl.Add(new DOCtDtl()
                        {
                            Number = Numb,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Source = Sm.DrStr(drDtl, cDtl[3]),
                            Lot = Sm.DrStr(drDtl, cDtl[4]),
                            Bin = Sm.DrStr(drDtl, cDtl[5]),
                            Qty = Sm.DrDec(drDtl, cDtl[6]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                            Remark = Sm.DrStr(drDtl, cDtl[12]),
                            PackingQty = Sm.DrDec(drDtl, cDtl[13]),
                            PackingUomCode = Sm.DrStr(drDtl, cDtl[14]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[15]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[16]),
                            UPrice = Sm.DrDec(drDtl, cDtl[17]),
                            Amt = Sm.DrDec(drDtl, cDtl[18]),
                            Amt2 = Sm.DrDec(drDtl, cDtl[19]),
                            Amt3 = Sm.DrDec(drDtl, cDtl[20]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            #region Signature KIM
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select B.UserName, Concat(IfNull(C.ParValue, ''), B.UserCode, '.JPG') As EmpPict ");
            SQL2.AppendLine("From TblDOCtHdr A ");
            SQL2.AppendLine("Inner Join TblUser B On A.CreateBy=B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         "UserName", "EmpPict"
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new DoctSignature()
                        {
                            UserName = Sm.DrStr(dr2, c2[0]),
                            EmpPict = Sm.DrStr(dr2, c2[1]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            #region Header SIER

            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();
            decimal mTotal3 = decimal.Parse(TxtAmt.Text);

            #region Old Query

            //SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            //SQL.AppendLine("Replace(A.DocNo, Concat('/',(Select ParValue From Tblparameter Where ParCode = 'DocTitle')),'') As DocNo,");
            //SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.DocNoInternal, C.CtName, ");
            //SQL.AppendLine("A.SAName, A.SAAddress, D.CityName As SACityCode, E.CntName As SACntCode, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.Remark, '0' As Indicator ");
            //SQL.AppendLine("From TblDOCtHdr A ");
            //SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            //SQL.AppendLine("Left Join TblCity D On A.SACityCode = D.CityCode ");
            //SQL.AppendLine("Left Join TblCountry E On A.SACntCode = E.CntCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");

            #endregion

            if (mIsFilterBySite)
            {
                SQL3.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, IfNull(G.CompanyName,(Select ParValue From TblParameter Where Parcode='ReportTitle1'))As CompanyName, G.CompanyPhone, G.CompanyFax, ");
                SQL3.AppendLine("IfNull(G.CompanyAddress,(Select ParValue From TblParameter Where Parcode='ReportTitle2'))As CompanyAddress,  ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3')As CompanyAddressFull,  ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4')As CompanyPhoneFull, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL3.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'CompanyNPWP', null as CompanyPhoneFull, ");
            }
            if (Sm.GetParameter("DocTitle") == "SIER")
                SQL3.AppendLine("A.DocNo, 'Surabaya' As AddressTTD, ");


            else
                SQL3.AppendLine("Replace(A.DocNo, Concat('/',(Select ParValue From Tblparameter Where ParCode = 'DocTitle')),'') As DocNo, null As AddressTTD,");
            SQL3.AppendLine("DATE_FORMAT(A.DocDt, '%M %Y') As DocDtMthYr, C.Address CtAddress,  ");
            SQL3.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.DocNoInternal, C.CtName, A.SAName, A.SAAddress, D.CityName As SACityCode, ");
            SQL3.AppendLine("E.CntName As SACntCode, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.Remark, '0' As Indicator, ");
            SQL3.AppendLine("(Select Length(substring(parvalue,11)) As FormatNum from tblparameter Where Parcode='FormatNum0') As 'FormatNum',");
            SQL3.AppendLine("(Select parvalue from tblparameter where parcode='IsShowForeignName') As IsForeignName, ");
            SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='SLINotes') As 'SLINotes', ");
            SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='SLINotes2') As 'SLINotes2', ");
            SQL3.AppendLine("(Select parvalue from tblparameter where parcode='IsShowBatchNo') As IsShowBatchNo, A.Driver, A.VehicleRegNo, A.ResiNo ");
            SQL3.AppendLine("From TblDOCtHdr A ");
            SQL3.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL3.AppendLine("Left Join TblCity D On A.SACityCode = D.CityCode ");
            SQL3.AppendLine("Left Join TblCountry E On A.SACntCode = E.CntCode ");
            SQL3.AppendLine("Inner Join TblWarehouse F On A.WhsCode=F.WhsCode ");

            if (mIsFilterBySite)
            {
                SQL3.AppendLine("Left Join (");
                SQL3.AppendLine("    Select distinct A.DocNo, E.EntName As CompanyName, E.EntPhone As CompanyPhone, E.EntFax As CompanyFax, E.EntAddress As CompanyAddress  ");

                SQL3.AppendLine("    From TblDoCtHdr A  ");
                SQL3.AppendLine("    Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
                SQL3.AppendLine("    Left Join Tblcostcenter C On B.CCCode = C.CCCode ");
                SQL3.AppendLine("    Left Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode  ");
                SQL3.AppendLine("    Left Join TblEntity E On D.EntCode = E.EntCode ");
                SQL3.AppendLine("    Where A.DocNo=@DocNo ");
                SQL3.AppendLine(") G On A.DocNo = G.DocNo ");
            }
            SQL3.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select E.EntLogoName " +
                       "From TblDoCtHdr A  " +
                       "Inner Join TblWarehouse B On A.WhsCode = B.WhsCode " +
                       "Left Join Tblcostcenter C On B.CCCode = C.CCCode " +
                       "Left Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode " +
                       "Left Join TblEntity E On D.EntCode = E.EntCode " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    if (CompanyLogo.Length > 0)
                    {
                        Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                    }
                }
                else
                {
                    Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                }

                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[]

                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyPhone",
                    "DocNo",
                    "DocDt",

                    //6-10
                    "DocNoInternal",
                    "CtName",
                    "SANAme",
                    "SAAddress",
                    "SACityCode",

                    //11-15
                    "SACntCode",
                    "SAPostalCd",
                    "SAPhone",
                    "SAFax",
                    "SAEmail",

                    //16-20
                    "SAMobile",
                    "Remark",
                    "Indicator",    
                    "FormatNum",
                    "IsForeignName",

                    //21-25
                    "IsShowBatchNo",
                    "CompanyAddressFull",
                    "CompanyPhoneFull",
                    "Driver",
                    "VehicleRegNo",

                    //26-30
                    "DocDtMthYr",
                    "CtAddress",
                   // "TotalTax",
                    "SLINotes",
                    "SLINotes2",

                    //31-34
                  //  "BankAcNo",
                  //  "BankAcNm",
                    "AddressTTD",
                    "ResiNo"
                });

                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new InvoiceHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr3, c3[0]),
                            CompanyName = Sm.DrStr(dr3, c3[1]),
                            CompanyAddress = Sm.DrStr(dr3, c3[2]),
                            CompanyPhone = Sm.DrStr(dr3, c3[3]),
                            DocNo = Sm.DrStr(dr3, c3[4]),
                            DocDt = Sm.DrStr(dr3, c3[5]),
                            DocNoInternal = Sm.DrStr(dr3, c3[6]),
                            CtName = Sm.DrStr(dr3, c3[7]),
                            Name = Sm.DrStr(dr3, c3[8]),
                            Address = Sm.DrStr(dr3, c3[9]),
                            CityName = Sm.DrStr(dr3, c3[10]),
                            CntName = Sm.DrStr(dr3, c3[11]),
                            PostalCd = Sm.DrStr(dr3, c3[12]),
                            Phone = Sm.DrStr(dr3, c3[13]),
                            Fax = Sm.DrStr(dr3, c3[14]),
                            Email = Sm.DrStr(dr3, c3[15]),
                            Mobile = Sm.DrStr(dr3, c3[16]),
                            HRemark = Sm.DrStr(dr3, c3[17]),
                            Indicator = Sm.DrStr(dr3, c3[18]),
                            FormatNum = Sm.DrStr(dr3, c3[19]),
                            IsForeignName = Sm.DrStr(dr3, c3[20]),
                            IsShowBatchNo = Sm.DrStr(dr3, c3[21]),
                            Terbilang = Sm.Terbilang(mTotal),
                            CompanyAddressFull = Sm.DrStr(dr3, c3[22]),
                            CompanyPhoneFull = Sm.DrStr(dr3, c3[23]),
                            ExpDriver = Sm.DrStr(dr3, c3[24]),
                            ExpPlatNo = Sm.DrStr(dr3, c3[25]),

                            DocDtMthYr = Sm.DrStr(dr3, c3[26]),
                            CtAddress = Sm.DrStr(dr3, c3[27]),
                            //TotalTax = Sm.DrStr(dr3, c3[28]),
                            SLINotes = Sm.DrStr(dr3, c3[28]),
                            SLINotes2 = Sm.DrStr(dr3, c3[29]),

                            //BankAcNo = Sm.DrStr(dr3, c3[31]),
                            //BAnkAcNm = Sm.DrStr(dr3, c3[32]),
                            AddressTTD = Sm.DrStr(dr3, c3[30]),
                            ResiNo = Sm.DrStr(dr3, c3[31]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr3.Close();
            }

            myLists.Add(l3);

            #endregion

            #region Detail SIER

            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            decimal Numb2 = 0;
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select B.ItCode, ");
                if (Sm.GetParameter("DocTitle") == "SIER")
                    SQLDtl2.AppendLine("O1.ItGrpName As ItName,  ");
                else
                    SQLDtl2.AppendLine("IF((SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle')='IOK',C.ForeignName,C.ItName) AS ItName, ");
                SQLDtl2.AppendLine("C.ForeignName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
                SQLDtl2.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, ");
                SQLDtl2.AppendLine("0 As PackingQty, '' as PackingUOMCode, C.ItGrpCode, B.UPrice, ");
                SQLDtl2.AppendLine("Case monthname(A.DocDt) ");
                SQLDtl2.AppendLine(" When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
                SQLDtl2.AppendLine(" When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'  ");
                SQLDtl2.AppendLine(" When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'Nopember' When 'December' Then 'Desember' End As Month ");
                SQLDtl2.AppendLine(" ,DATE_FORMAT(A.DocDt, '%Y') Year, ");
                SQLDtl2.AppendLine(" GROUP_CONCAT(B.Remark SEPARATOR '\n') RemarkItem, ");
                if (mIsDOCtAmtRounded)
                {
                    SQLDtl2.AppendLine("Floor(sum(B.Qty*B.UPrice))  As Amt, ");
                    SQLDtl2.AppendLine("Floor(sum(B.Qty2*B.UPrice)) As Amt2, ");
                    SQLDtl2.AppendLine("Floor(sum(B.Qty3*B.UPrice)) As Amt3 ");
                }
                else
                {
                    SQLDtl2.AppendLine("(sum(B.Qty*B.UPrice)) As Amt, ");
                    SQLDtl2.AppendLine("(sum(B.Qty2*B.UPrice))As Amt2, ");
                    SQLDtl2.AppendLine("(sum(B.Qty3*B.UPrice))As Amt3 ");
                }
                SQLDtl2.AppendLine("From TblDOCtHdr A ");
                SQLDtl2.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
                SQLDtl2.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl2.AppendLine("Left Join TblItemGroup O1 On O1.ItGrpCode=C.ItGrpCode ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.CancelInd = 'N' Group By ItGrpName Order By B.DNo");



                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                    //0
                    "ItCode" ,

                    //1-5
                    "ItName" ,
                    "BatchNo",
                    "Source",
                    "Lot",
                    "Bin",

                    //6-10
                    "Qty" ,
                    "Qty2",
                    "Qty3",
                    "InventoryUomCode" ,
                    "InventoryUomCode2" ,

                    //11-15
                    "InventoryUomCode3" ,
                    "Remark",
                    "PackingQty",
                    "PackingUOMCode",
                    "ItGrpCode",

                    //16-20
                    "ForeignName",
                    "UPrice",
                    "Amt",
                    "Amt2",
                    "Amt3", 

                    //21-23
                    "Month",
                    "Year",
                    "RemarkItem"
         
                });

                if (drDtl2.HasRows)
                {
                    int No = 0;
                    while (drDtl2.Read())
                    {
                        No = No + 1;
                        ldtl2.Add(new InvoiceDtl()
                        {
                            nomor = No,
                            Number = Numb,
                            ItCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            ItName = Sm.DrStr(drDtl2, cDtl2[1]),
                            BatchNo = Sm.DrStr(drDtl2, cDtl2[2]),
                            Source = Sm.DrStr(drDtl2, cDtl2[3]),
                            Lot = Sm.DrStr(drDtl2, cDtl2[4]),
                            Bin = Sm.DrStr(drDtl2, cDtl2[5]),
                            Qty = Sm.DrDec(drDtl2, cDtl2[6]),
                            Qty2 = Sm.DrDec(drDtl2, cDtl2[7]),
                            Qty3 = Sm.DrDec(drDtl2, cDtl2[8]),
                            InventoryUomCode = Sm.DrStr(drDtl2, cDtl2[9]),
                            InventoryUomCode2 = Sm.DrStr(drDtl2, cDtl2[10]),
                            InventoryUomCode3 = Sm.DrStr(drDtl2, cDtl2[11]),
                            Remark = Sm.DrStr(drDtl2, cDtl2[12]),
                            PackingQty = Sm.DrDec(drDtl2, cDtl2[13]),
                            PackingUomCode = Sm.DrStr(drDtl2, cDtl2[14]),
                            ItGrpCode = Sm.DrStr(drDtl2, cDtl2[15]),
                            ForeignName = Sm.DrStr(drDtl2, cDtl2[16]),
                            UPrice = Sm.DrDec(drDtl2, cDtl2[17]),
                            Amt = Sm.DrDec(drDtl2, cDtl2[18]),
                            Amt2 = Sm.DrDec(drDtl2, cDtl2[19]),
                            Amt3 = Sm.DrDec(drDtl2, cDtl2[20]),
                            Month = Sm.DrStr(drDtl2, cDtl2[21]),
                            Year = Sm.DrStr(drDtl2, cDtl2[22]),
                            RemarkItem = Sm.DrStr(drDtl2, cDtl2[23])

                        });
                    }
                }

                drDtl2.Close();
            }

            myLists.Add(ldtl2);

            #endregion

            #region Signature MNET

            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();

            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;

                SQL4.AppendLine("Select B.UserName, C.GrpName PosName, A.Seq, A.Title, DATE_FORMAT(A.LastUpDt, '%d %M %Y') as LastUpDt, A.EmpPict, @Space As Space, Null Level ");
                SQL4.AppendLine("From ( ");
                SQL4.AppendLine("    Select A.CreateBy As UserCode, 0 As Seq, 'Created By,' As Title, Left(A.CreateDt, 8) As LastUpDt, Concat(IfNull(C.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict ");
                SQL4.AppendLine("    From TblDOCtHdr A ");
                SQL4.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQL4.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine("    Where A.DocNo = @DocNo ");
                SQL4.AppendLine("    Union All ");
                SQL4.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 1 ");
                SQL4.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null And A.DocType = 'DOCt' ");
                SQL4.AppendLine("    Union All ");
                SQL4.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 2 ");
                SQL4.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null And A.DocType = 'DOCt' ");
                SQL4.AppendLine("    Union All ");
                SQL4.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 3 ");
                SQL4.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null And A.DocType = 'DOCt' ");
                SQL4.AppendLine("    Union All ");
                SQL4.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 4 ");
                SQL4.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null And A.DocType = 'DOCt' ");
                SQL4.AppendLine("    Union All ");
                SQL4.AppendLine("    SELECT A.UserCode, B.Level AS Seq, 'Approved By,' As Title, LEFT(A.LastUpDt, 8) AS LastUpDt, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') AS EmpPict ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo AND B.Level = 5 ");
                SQL4.AppendLine("    LEFT JOIN TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine("    Where A.DocNo = @DocNo And A.LastUpDt Is Not Null And A.DocType = 'DOCt' ");
                SQL4.AppendLine("    ) A ");
                SQL4.AppendLine("Left Join Tbluser B On A.UserCode = B.UserCode ");
                SQL4.AppendLine("Left Join TblGroup C On B.GrpCode = C.GrpCode ");
                SQL4.AppendLine("Group By A.UserCode, A.Seq, A.Title, A.LastUpDt ");
                SQL4.AppendLine("Order By A.Seq Asc; ");

                cm4.CommandText = SQL4.ToString();
                Sm.CmParam<String>(ref cm4, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[]
                {
                        //0
                        "EmpPict" ,

                        //1-5
                        "UserName" ,
                        "PosName",
                        "Space",
                        "Level",
                        "Title",

                        "LastupDt",
                        "Seq"
                });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {

                        l4.Add(new SignatureMNet()
                        {
                            Signature = Sm.DrStr(dr4, c4[0]),
                            UserName = Sm.DrStr(dr4, c4[1]),
                            PosName = Sm.DrStr(dr4, c4[2]),
                            Space = "",
                            DNo = Sm.DrStr(dr4, c4[4]),
                            Title = Sm.DrStr(dr4, c4[5]),
                            LastUpDt = Sm.DrStr(dr4, c4[6]),
                            Sequence = Sm.DrStr(dr4, c4[7]),
                        });
                    }
                }
                dr4.Close();
            }
            myLists.Add(l4);

            #endregion


            string RptName = Sm.GetParameter("FormPrintOutDOCT");
            string mDocTitle = Sm.GetParameter("DocTitle");
            
            if (RptName.Length > 0)
            {
                if (mDocTitle == "SIER" || mDocTitle == "MNET")
                {
                    Sm.PrintReport("DOToCt" + RptName, myLists, TableName, false);
                }
                else
                {
                    switch (parValue)
                    {
                        case 1:
                            Sm.PrintReport("DOToCt" + RptName, myLists, TableName, false);
                            break;
                        case 2:
                            Sm.PrintReport("DOToCt2" + RptName, myLists, TableName, false);
                            break;
                        case 3:
                            Sm.PrintReport("DOToCt3" + RptName, myLists, TableName, false);
                            break;
                    }
                }
            }
            else
            {
                if (mDocTitle == "SIER" || mDocTitle == "MNET")
                {
                    Sm.PrintReport("DOToCt" + mDocTitle, myLists, TableName, false);
                }
                else
                {
                    switch (parValue)
                    {
                        case 1:
                            Sm.PrintReport("DOToCt", myLists, TableName, false);
                            break;
                        case 2:
                            Sm.PrintReport("DOToCt2", myLists, TableName, false);
                            break;
                        case 3:
                            Sm.PrintReport("DOToCt3", myLists, TableName, false);
                            break;
                    }
                }
            }

        }

        internal void ComputeTotalQty()
        {
            decimal Total = 0m;
            int col = 15;
            int col2 = 0;

            while (col <= 21)
            {
                Total = 0m;
                for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, col).Length != 0 &&
                        Sm.GetGrdStr(Grd1, row, 4).Length != 0 &&
                        !Sm.GetGrdBool(Grd1, row, 1))
                        Total += Sm.GetGrdDec(Grd1, row, col);
                }
                if (col == 15) col2 = 0;
                if (col == 18) col2 = 1;
                if (col == 21) col2 = 2;
                Grd2.Cells[0, col2].Value = Total;
                col += 3;
            }
        }

        private void ComputeTotal(int row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, row, 15).Length != 0) Qty = Sm.GetGrdDec(Grd1, row, 15);
            if (Sm.GetGrdStr(Grd1, row, 23).Length != 0) UPrice = Sm.GetGrdDec(Grd1, row, 23);
            if (mIsDOCtAmtRounded)
                Grd1.Cells[row, 24].Value = Sm.FormatNum(decimal.Truncate(Qty * UPrice), 0);
            else
                Grd1.Cells[row, 24].Value = Sm.FormatNum(Qty * UPrice, 0);
            ComputeAmt();
        }

        private void ComputeAmt()
        {
            decimal Amt = 0m;
            for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                if (Sm.GetGrdStr(Grd1, row, 24).Length != 0 && !Sm.GetGrdBool(Grd1, row, 1))
                    Amt += Sm.GetGrdDec(Grd1, row, 24);
            if (mIsDOCtAmtRounded)
                //TxtAmt.EditValue = Sm.FormatNum(Math.Ceiling(Amt),0); 
                TxtAmt.EditValue = Sm.FormatNum(decimal.Truncate(Amt), 0);
            else
                TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDOCtFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private MySqlCommand UpdateDOCtFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileMandatory()
        {
            if (mIsDOCtAllowToUploadFile)
            {
                if (mDOCtUploadFileFormat == "1" && mIsDOCtUploadFileMandatory)
                    Sm.IsTxtEmpty(TxtFile, "File", false);

                else if (mDOCtUploadFileFormat == "2" && mIsDOCtUploadFileMandatory)
                {
                    if (Grd3.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsFTPClientDataNotValid()
        {

            if (TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblDocthdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And Status = 'A' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue1(SetLueProcessInd));
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsCustomerComboBasedOnCategory && IsInsert)
            {
                //Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
                Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                ClearData2();
                if (mIsDOCtUseCtQtPrice) ClearGrd();
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsCustomerComboBasedOnCategory)
                    Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue4(Sl.SetLueCtCodeBasedOnCategory), string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                else
                    Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
                ClearData2();
                if (mIsDOCtUseCtQtPrice) ClearGrd();
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFileGrid(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd3.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        #region Grid4 (List Of COA's Account Number)

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
           
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmDOCtDlg8(this, Grd4));
            }
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd4, e, BtnSave);
            //ComputeCOAAmt();
            Sm.GrdEnter(Grd4, e);
            Sm.GrdTabInLastCell(Grd4, e, BtnFind, BtnSave);
        }

        #endregion

        private void TxtDocNoInternal_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNoInternal);
        }

        private void TxtDriver_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDriver);
        }

        private void TxtExpedition_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtExpedition);
        }

        private void TxtVehicleRegNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVehicleRegNo);
        }

        private void LueShipAdd_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            //(LueSAName.Properties.DataSource as List<Lue1>).Add
            //   (
            //       new Lue1 { 
            //           Col1 = e.DisplayValue.ToString() 
            //       }
            //   );
            //e.Handled = true;
        }

        //private void LueShipAdd_EditValueChanged(object sender, EventArgs e)
        //{
        //    Sm.RefreshLookUpEdit(LueSAName, new Sm.RefreshLue3(SetLueShippingAddress), Sm.GetLue(LueCtCode), TxtDocNo.Text);
        //    try
        //    {
        //        ClearData2();
        //        ShowShippingAddressData(Sm.GetLue(LueCtCode), Sm.GetLue(LueSAName));
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void LuePackagingUnitUomCode_EditValueChanged(object sender, EventArgs e)
        {
            var ItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
            if (ItCode.Length == 0) ItCode = "XXX";
            Sm.RefreshLookUpEdit(LuePackagingUnitUomCode,
                new Sm.RefreshLue3(SetLuePackagingUnitUomCode), ItCode, string.Empty);
        }

        private void SetLuePackagingUnitUomCode(ref LookUpEdit Lue, string ItCode, string PackagingUnitUomCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select UomCode As Col1, UomCode As Col2 ");
            SQL.AppendLine("From TblItemPackagingUnit Where ItCode=@ItCode ");
            if (PackagingUnitUomCode.Length != 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select UomCode As Col1, UomCode As Col2 ");
                SQL.AppendLine("From TblUom Where UomCode=@UomCode ");
            }
            SQL.AppendLine("Order By UomCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@UomCode", PackagingUnitUomCode);

            Sm.SetLue2(
              ref Lue, ref cm,
              0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LuePackagingUnitUomCode_Leave(object sender, EventArgs e)
        {
            if (LuePackagingUnitUomCode.Visible && fAccept)
            {
                if (Sm.GetLue(LuePackagingUnitUomCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LuePackagingUnitUomCode);
                LuePackagingUnitUomCode.Visible = false;
            }
        }

        private void LuePackagingUnitUomCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void BtnCopyDOCt_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmDOCtDlg5(this, Sm.GetLue(LueCtCode)));
        }

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsUseProductionWorkGroup)
                    Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
            }
        }

        #endregion

        #region Button Event

        private void BtnCtCt_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCtDlg7(this));
        }

        private void BtnCustomerShipAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmDOCtDlg2(this, Sm.GetLue(LueCtCode)));
        }

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCtDlg3(this));
        }

        private void BtnPEB_Click(object sender, EventArgs e)
        {
            if (mIsKawasanBerikatEnabled && IsAuthorizedToAccessPEB())
            {
                if (BtnSave.Enabled)
                {
                    Sm.FormShowDialog(new FrmDOCtDlg4(this));
                }
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && mIsDOCtUseSOContract && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                Sm.FormShowDialog(new FrmDOCtDlg6(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                var f1 = new FrmSOContract2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtSOContractDocNo.Text;
                f1.ShowDialog();
            }
        }


        #endregion

        #endregion
    }

    #region Report Class


    class DOCt
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyAddressFull { get; set; }
        public string CompanyPhoneFull { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string DocNoInternal { get; set; }
        public string CtName { get; set; }
        public string HRemark { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string CntName { get; set; }
        public string PostalCd { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Indicator { get; set; }
        public string PrintBy { get; set; }
        public string ExpDriver { get; set; }
        public string ExpPlatNo { get; set; }
        public string ExpName { get; set; }
        public string FormatNum { get; set; }
        public string IsForeignName { get; set; }
        public string Account { get; set; }
        public string LocalDocNo { get; set; }
        public string SPPlaceDelivery { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyFax { get; set; }
        public string Cnt { get; set; }
        public string Seal { get; set; }
        public string SpName { get; set; }
        public string IsShowBatchNo { get; set; }
        public string OverSeaInd { get; set; }
        public string DocTitle { get; set; }
        public string DRLocalDocNo { get; set; }
        public string Note { get; set; }
        public string Peb { get; set; }
        public string PebDt { get; set; }
        public string Terbilang { get; set; }
        public string Terbilang2 { get; set; }
        public string ResiNo { get; set; }
        public string CustomerAddress { get; set; }
        public string Uom1 { get; set; }
        public string Uom2 { get; set; }
        public string Uom3 { get; set; }
        public string CtCtName { get; set; }
        public string ContactPersonName { get; set; }
        public string CompanyFooterImage { get; set; }
        public string EnterTm {get; set;}
        public string UnloadingTm {get; set;}
        public string FinishTm {get; set;}
        public string ExitProjectTm {get; set;}
        public string ExitBatchTm {get; set;}
        


    }

    class COA
    {
        public string AcNo { set; get; }
    }


    class DOCtDtl
    {
        public decimal Number { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string ForeignName { get; set; }
        public string BatchNo { get; set; }
        public string Source { get; set; }
        public string Lot { get; set; }
        public string Bin { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string InventoryUomCode { get; set; }
        public string InventoryUomCode2 { get; set; }
        public string InventoryUomCode3 { get; set; }
        public string Remark { get; set; }
        public decimal PackingQty { get; set; }
        public string PackingUomCode { get; set; }
        public string ItGrpCode { get; set; }
        public decimal UPrice { get; set; }
        public decimal Amt { get; set; }
        public decimal Amt2 { get; set; }
        public decimal Amt3 { get; set; }
        public string SCLocalDocNo { get; set; }
        public decimal QtyRit { get; set; }
        public decimal Rit { get; set; }

    }

    class DOCtDtl2
    {
        public int nomor { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string DocNo { get; set; }
        public string SpHsCode { get; set; }
        public string ItGrpName { get; set; }
        public string ItCodeInternal { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public decimal NW { get; set; }
        public decimal GW { get; set; }
        public string CtItCode { get; set; }
        public decimal QtyInventory { get; set; }

        public decimal UPrice { get; set; }
        public decimal Amount { get; set; }
        public string PriceUomCode { get; set; }
        public string InventoryUomCode { get; set; }
        public string CurCode { get; set; }
        public string Material { get; set; }
        public string Colour { get; set; }
        public decimal TotalVolume { get; set; }
        public decimal QtyPackagingUnit { get; set; }
        public decimal QtyPL { get; set; }
        public decimal QtyPcs { get; set; }
    }

    class DOCtDtl3
    {
        public string PalletNo { get; set; }
        public string ItName { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal QtyInventory { get; set; }
        public decimal QtySales { get; set; }
        public decimal QtyPackagingUnit { get; set; }
    }

    class DOCtDtl4
    {
        public int nomor { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string DocNo { get; set; }
        public string SpHsCode { get; set; }
        public string ItGrpName { get; set; }
        public string ItCodeInternal { get; set; }
        public decimal Qty { get; set; }
        public decimal NW { get; set; }
        public decimal GW { get; set; }
        public string CtItCode { get; set; }
        public decimal QtyInventory { get; set; }
    }

    class DoctSignature
    {
        public string UserName { set; get; }
        public string EmpPict { get; set; }
    }

    class InvoiceHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyAddressFull { get; set; }
        public string CompanyPhoneFull { get; set; }
        public string CompanyPhone { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string DocNoInternal { get; set; }
        public string CtName { get; set; }
        public string HRemark { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string CityName { get; set; }
        public string CntName { get; set; }
        public string PostalCd { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Indicator { get; set; }
        public string PrintBy { get; set; }
        public string ExpDriver { get; set; }
        public string ExpPlatNo { get; set; }
        public string ExpName { get; set; }
        public string FormatNum { get; set; }
        public string IsForeignName { get; set; }
        public string Account { get; set; }
        public string LocalDocNo { get; set; }
        public string SPPlaceDelivery { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyFax { get; set; }
        public string Cnt { get; set; }
        public string Seal { get; set; }
        public string SpName { get; set; }
        public string IsShowBatchNo { get; set; }
        public string OverSeaInd { get; set; }
        public string DocTitle { get; set; }
        public string DRLocalDocNo { get; set; }
        public string Note { get; set; }
        public string Peb { get; set; }
        public string PebDt { get; set; }
        public string Terbilang { get; set; }
        public string Terbilang2 { get; set; }
        public string DocDtMthYr { get; set; }
        public string CtAddress { get; set; }
        public string TotalTax { get; set; }
        public string SLINotes { get; set; }
        public string SLINotes2 { get; set; }
        public string BankAcNo { get; set; }
        public string BAnkAcNm { get; set; }
        public string AddressTTD { get; set; }
        public string ResiNo { get; set; }

    }

    class InvoiceDtl
    {
        public decimal Number { get; set; }
        public string ItCode { get; set; }
        public string ItName { get; set; }
        public string ForeignName { get; set; }
        public string BatchNo { get; set; }
        public string Source { get; set; }
        public string Lot { get; set; }
        public string Bin { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string InventoryUomCode { get; set; }
        public string InventoryUomCode2 { get; set; }
        public string InventoryUomCode3 { get; set; }
        public string Remark { get; set; }
        public decimal PackingQty { get; set; }
        public string PackingUomCode { get; set; }
        public string ItGrpCode { get; set; }
        public decimal UPrice { get; set; }
        public decimal Amt { get; set; }
        public decimal Amt2 { get; set; }
        public decimal Amt3 { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int nomor { get; set; }
        public string RemarkItem { get; set; }




    }

    class SignatureMNet
    {
        public string Signature { get; set; }
        public string UserName { get; set; }
        public string PosName { get; set; }
        public string Space { get; set; }
        public string DNo { get; set; }
        public string Title { get; set; }
        public string LastUpDt { get; set; }
        public string Sequence { get; set; }
    }

    #endregion


}
