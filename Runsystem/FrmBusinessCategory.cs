﻿#region Update
/*
    03/07/2018 [TKG] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBusinessCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmBusinessCategoryFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmBusinessCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtBCCode, TxtBCName }, true);
                    TxtBCCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtBCCode, TxtBCName }, false);
                    TxtBCCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtBCName }, false);
                    TxtBCName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtBCCode, TxtBCName });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmBusinessCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBCCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBCCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblBusinessCategory Where BCCode=@BCCode" };
                Sm.CmParam<String>(ref cm, "@BCCode", TxtBCCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblBusinessCategory(BCCode, BCName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@BCCode, @BCName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update BCName=@BCName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@BCCode", TxtBCCode.Text);
                Sm.CmParam<String>(ref cm, "@BCName", TxtBCName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtBCCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string BCCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BCCode, BCName From TblBusinessCategory Where BCCode=@BCCode;",
                        new string[]{ "BCCode", "BCName" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtBCCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtBCName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtBCCode, "Business category code", false) ||
                Sm.IsTxtEmpty(TxtBCName, "Business category name", false) ||
                IsCodeAlreadyExisted();
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtBCCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select BCCode From TblBusinessCategory Where BCCode=@Param;",
                    TxtBCCode.Text,
                    "Business category code ( " + TxtBCCode.Text + " ) already existed."
                    );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBCCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBCCode);
        }

        private void TxtBCName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBCName);
        }

        #endregion

        #endregion
    }
}
