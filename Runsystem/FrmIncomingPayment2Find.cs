﻿#region Update
/*
    08/06/2020 [DITA/IMS] New apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmIncomingPayment2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmIncomingPayment2 mFrmParent;
        private string mSQL = string.Empty, mMInd="N";

        #endregion

        #region Constructor

        public FrmIncomingPayment2Find(FrmIncomingPayment2 FrmParent, string MInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMInd = MInd;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' When 'A' Then 'Approved' End As StatusDesc, ");
            SQL.AppendLine("B.CtName, A.CurCode, A.Amt, A.CurCode2, A.Amt2, A.VoucherRequestDocNo, C.VoucherDocNo, ");
            SQL.AppendLine("D.LocalDocNo, D.InvoiceDocNo, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("E.ProjectCode, E.ProjectName, E.PONo ");
            else
                SQL.AppendLine("null as ProjectCode, null as ProjectName, null as PONo ");
            SQL.AppendLine("From TblIncomingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct IfNull(T.LocalDocNo, '-')) As LocalDocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T.InvoiceDocNo Separator ', ') InvoiceDocNo ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.DocNo, If(T2.InvoiceType = '1', T3.LocalDocNo, T4.LocalDocNo) As LocalDocNo, T2.InvoiceDocNo ");
            SQL.AppendLine("        From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Left Join TblsalesinvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo  ");
            SQL.AppendLine("        Left Join Tblsalesinvoice2Hdr T4 On T2.InvoiceDocNo=T4.DocNo ");
            SQL.AppendLine("        Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.DocNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                SQL.AppendLine("Left Join ( ");
                #region Old Code
                //SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T12.ProjectCode, T9.ProjectCode2) ProjectCode, Group_Concat(T12.ProjectName, T11.ProjectName) ProjectName, Group_Concat(Distinct T9.PONo) PONo ");
                //SQL.AppendLine("    From TblIncomingPaymentHdr T1 ");
                //SQL.AppendLine("    Inner Join TblIncomingPaymentDtl T2 On T1.DocNo = T2.DocNo ");
                //SQL.AppendLine("    Inner Join TblSalesInvoiceHdr T3 On T2.InvoiceDocNo = T3.DocNo ");
                //SQL.AppendLine("    Inner Join TblSalesInvoiceDtl T4 On T3.DocNo = T4.DocNo ");
                //SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T5 On T4.DOCtDocNo = T5.DocNo And T4.DOCtDNo = T5.DNo ");
                //SQL.AppendLine("    Inner Join TblDOCt2Hdr T6 On T5.DocNo = T6.DocNo ");
                //SQL.AppendLine("    Inner Join TblDRHdr T7 On T6.DRDocNo = T7.DocNo ");
                //SQL.AppendLine("    Inner Join TblDRDtl T8 On T6.DRDocNo = T8.DocNo And T5.DRDNo = T8.DNo ");
                //SQL.AppendLine("    Inner Join TblSOContractHdr T9 On T8.SODocNo = T9.DocNo ");
                //SQL.AppendLine("    Inner Join TblBOQHdr T10 On T9.BOQDocNo = T10.DocNo ");
                //SQL.AppendLine("    Inner Join TblLOPHdr T11 On T10.LOPDocNo = T11.DocNo ");
                //SQL.AppendLine("    Left Join TblProjectGroup T12 On T11.PGCode = T12.PGCode ");
                //SQL.AppendLine("    Group By T1.DocNo ");
                #endregion

                SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct IfNull(T6.ProjectCode, '')) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T6.ProjectName, '')) ProjectName, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T3.PONo, '')) PONo ");
                SQL.AppendLine("    From TblIncomingPaymentHdr T1 ");
                SQL.AppendLine("    Inner Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select Distinct X1.DocNo, X2.SOContractDocNo ");
                SQL.AppendLine("        From TblIncomingPaymentDtl X1 ");
                SQL.AppendLine("        Inner Join TblSOContractDownpaymentHdr X2 On X1.InvoiceDocNo = X2.DocNo And X1.InvoiceType = '6' ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Distinct X1.DocNo, X2.SOContractDocNo ");
                SQL.AppendLine("        From TblIncomingPaymentDtl X1 ");
                SQL.AppendLine("        Inner Join TblSOContractDownpayment2Hdr X2 oN X1.InvoiceDocNo = X2.DocNo And X1.InvoiceType = '7' ");
                SQL.AppendLine("    ) T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T3 On T2.SOContractDocNo = T3.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T4 On T3.BOQDocNo = T4.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T5 On T4.LOPDocNo = T5.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T6 On T5.PGCode = T6.PGCode ");
                SQL.AppendLine("    Group By T1.DocNo ");
                SQL.AppendLine(") E On A.DocNo = E.DocNo ");
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            
            if (mFrmParent.mIsFilterBySite)
            {
                if (mFrmParent.mIsIncomingPaymentProjectSystemEnabled)
                {
                    SQL.AppendLine("And A.DocNo In ( ");
                    SQL.AppendLine("    Select Distinct A0.DocNo ");
                    SQL.AppendLine("    From TblIncomingPaymentDtl A0 ");
                    SQL.AppendLine("    Inner Join TblSalesInvoice5Hdr A On A0.InvoiceDocNo = A.Docno ");
                    SQL.AppendLine("    Inner Join TblSalesInvoice5Dtl A1 On A.DocNo = A1.DocNo ");
                    SQL.AppendLine("    Inner Join TblProjectImplementationHdr B On A1.ProjectImplementationDocNo=B.DocNo ");
                    SQL.AppendLine("    Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                    SQL.AppendLine("    Inner Join TblSOContractHdr D On C.SOCDocNo=D.DocNo ");
                    SQL.AppendLine("    Inner Join TblBOQHdr E On D.BOQDocNo=E.DocNo ");
                    SQL.AppendLine("    Inner Join TblLOPHdr F On E.LOPDocNo=F.DocNo ");
                    SQL.AppendLine("        And (F.SiteCode Is Null Or ( ");
                    SQL.AppendLine("        F.SiteCode Is Not Null ");
                    SQL.AppendLine("        And Exists( ");
                    SQL.AppendLine("            Select 1 From TblGroupSite ");
                    SQL.AppendLine("            Where SiteCode=IfNull(F.SiteCode, '') ");
                    SQL.AppendLine("            And GrpCode In ( ");
                    SQL.AppendLine("                Select GrpCode From TblUser ");
                    SQL.AppendLine("                Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    )) ");
                    SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2) ");
                }
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Local Document",

                    //6-10
                    "Detail Document#",
                    "Customer",
                    "Project Code",
                    "Project Name",
                    "Customer PO#",                      
                    
                    //11-15
                    "Currency"+Environment.NewLine+"(SI)",
                    "Amount"+Environment.NewLine+"(SI)",
                    "Currency"+Environment.NewLine+"(IP)",
                    "Amount"+Environment.NewLine+"(IP)",
                    "Voucher"+Environment.NewLine+"Request#",                        
                    
                    //16-20
                    "Voucher#",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 

                    //21-22
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 50, 80, 250, 
                    
                    //6-10
                    180, 150, 100, 200, 120, 
                    
                    //11-15
                    80, 130, 80, 130, 130, 

                    //16-20
                    130, 100, 100, 100, 100, 

                    //21-22
                    100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = mFrmParent.mIsUseMInd?" And A.MInd=@MInd ":" ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MInd", mMInd);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "D.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "LocalDocNo", "InvoiceDocNo",  
                        
                        //6-10
                        "CtName", "ProjectCode", "ProjectName", "PONo", "CurCode", 
                        
                        //11-15
                        "Amt", "CurCode2", "Amt2", "VoucherRequestDocNo", "VoucherDocNo", 

                        //16-19
                        "CreateBy", "CreateDt", "LastUpBy","LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local Document#");
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
      
    }
}
