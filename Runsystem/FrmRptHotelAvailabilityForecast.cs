﻿#region Update
/*
    26/09/2017 [TKG] Availability Forecast
    28/09/2017 [TKG] tambah Out of Order
    29/09/2017 [TKG] Date harus selalu muncul
    10/02/2017 [TKG] tabel ambil dari caption (TblAvailForecast)
    10/02/2017 [WED] tambah printout
    17/10/2017 [ARI] tambah kolom month
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion
namespace RunSystem
{
    public partial class FrmRptHotelAvailabilityForecast : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptHotelAvailabilityForecast(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length != 0) this.Text = this.Text.Replace("&&", "&");
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                var Dt = string.Concat(Sm.Left(Sm.ServerCurrentDateTime(), 6), "01");
                DteDocDt1.DateTime = Sm.ConvertDate(Dt);
                DteDocDt2.DateTime = (Sm.ConvertDate(Dt).AddMonths(1)).AddDays(-1);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        protected override void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.Dt, DayName(Tbl1.Date) As DayNm, Tbl1.Month, ");
            SQL.AppendLine("Tbl2.* From ( ");
            SQL.AppendLine("    Select Dt As Date, DATE_FORMAT(Dt, '%Y%m%d') Dt, date_format(Dt, '%M')As Month From ( ");
            SQL.AppendLine("        Select AddDate(Concat(@Yr, '-01-01'), T5*10000 + T4*1000 + T3*100 + T2*10 + T1) Dt ");
            SQL.AppendLine("        From ");
            SQL.AppendLine("        (Select 0 T1 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T1, ");
            SQL.AppendLine("        (Select 0 T2 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T2, ");
            SQL.AppendLine("        (Select 0 T3 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T3, ");
            SQL.AppendLine("        (Select 0 T4 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T4,");
            SQL.AppendLine("        (Select 0 T5 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T5 ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Where Dt Between Str_To_Date(@Dt1, '%Y%m%d') And Str_To_Date(@Dt2, '%Y%m%d') ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.Date, ");
            SQL.AppendLine("    IfNull(T.room_arrival, 0.00) As room_arr,  ");
            SQL.AppendLine("    IfNull(T.room_dpt, 0.00) As room_dpt, ");
            SQL.AppendLine("    IfNull(T.room_occupied, 0.00) As room_occupied,  ");
            SQL.AppendLine("    Case When IfNull(T.total_room, 0.00)>0.00 Then  ");
            SQL.AppendLine("        (IfNull(T.room_occupied, 0.00)/IfNull(T.total_room, 0.00))*100.00  ");
	        SQL.AppendLine("    Else 0.00 End As room_occupiedP, ");
            SQL.AppendLine("    IfNull(T.total_room, 0.00) As total_room, ");
            SQL.AppendLine("    IfNull(T.room_vacant, 0.00) As Vac, ");
            SQL.AppendLine("    IfNull(T.room_oo, 0.00) As room_oo, ");
            SQL.AppendLine("    IfNull(T.avail_remain, 0.00) As room_available, ");
            SQL.AppendLine("    IfNull(nett_revenue, 0.00) As trr, ");
            SQL.AppendLine("    IfNull(arr, 0.00) As avr, ");
            SQL.AppendLine("    IfNull(revpar, 0.00) As revpar ");
	        SQL.AppendLine("    From TblAvailForecasts T ");
            SQL.AppendLine("    Where Date Between @Dt1 And @Dt2 ");
            SQL.AppendLine(") Tbl2 On Tbl1.Dt=Tbl2.Date ");
            SQL.AppendLine("Order By Tbl1.Dt, Tbl1.Month; ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Month",
                        "Day",
                        "Arrival",
                        "Departure",
                        "Occupied",

                        //6-10
                        "Occupied"+Environment.NewLine+"%",
                        "Total"+Environment.NewLine+"Room",
                        "Vacant",
                        "Out Of Order",
                        "Available",
                        
                        //11-13
                        "Room"+Environment.NewLine+"Revenue",
                        "Average"+Environment.NewLine+"Rate",
                        "RevPar"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 120, 80, 80, 80, 

                        //6-10
                        80, 80, 80, 80, 80, 

                        //11-14
                        80, 120, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 8, 9, 10, 11 }, 3);
            Sm.GrdFormatDec(Grd1, new int[] { 7, 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", Sm.Left(Sm.GetDte(DteDocDt1), 4));
                Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString(),
                    new string[]
                    { 
                        //0
                        "Dt", 

                        //1-5
                        "Month", "DayNm", "room_arr", "room_dpt", "room_occupied", 

                        //6-10
                        "room_occupiedP", "total_room", "Vac", "room_oo", "room_available",  
                        
                        //11-13
                        "trr", "avr", "revpar"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 9, 10, 11, 12, 13 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Additional Methods

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "No Data displayed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueInvalid()
        {
            if (Grd1.Rows.Count > 0)
            {
                if (Sm.GetGrdStr(Grd1, 0, 1).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "No Data displayed.");
                    return true;
                }
            }
            return false;
        }

        private void ParPrint()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Start Date") || Sm.IsDteEmpty(DteDocDt2, "End Date") || Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2) || IsGrdEmpty() || IsGrdValueInvalid() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "ResultHeader", "Result" };

            var l = new List<ResultHeader>();
            var lDtl = new List<Result>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle5')As CompanyPhone, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode = @MenuCode) As MenuDesc ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-3
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "MenuDesc"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ResultHeader()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            MenuDesc = Sm.DrStr(dr, c[4]),
                            DocDt = String.Concat(String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt1))), " - ", String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDocDt2)))) ,
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select Tbl1.Dt, DayName(Tbl1.Date) As DayNm, Concat(DayName(Tbl1.Date), ', ', DATE_FORMAT(Tbl1.Dt, '%d/%b/%Y')) As FullDayNm, ");
            SQLDtl.AppendLine("Tbl2.* From ( ");
            SQLDtl.AppendLine("    Select Dt As Date, DATE_FORMAT(Dt, '%Y%m%d') Dt From ( ");
            SQLDtl.AppendLine("        Select AddDate(Concat(@Yr, '-01-01'), T5*10000 + T4*1000 + T3*100 + T2*10 + T1) Dt ");
            SQLDtl.AppendLine("        From ");
            SQLDtl.AppendLine("        (Select 0 T1 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T1, ");
            SQLDtl.AppendLine("        (Select 0 T2 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T2, ");
            SQLDtl.AppendLine("        (Select 0 T3 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T3, ");
            SQLDtl.AppendLine("        (Select 0 T4 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T4,");
            SQLDtl.AppendLine("        (Select 0 T5 Union All Select 1 Union All Select 2 Union All select 3 Union All select 4 Union All select 5 Union All select 6 Union All select 7 Union All select 8 Union All Select 9) T5 ");
            SQLDtl.AppendLine("    ) T ");
            SQLDtl.AppendLine("    Where Dt Between Str_To_Date(@Dt1, '%Y%m%d') And Str_To_Date(@Dt2, '%Y%m%d') ");
            SQLDtl.AppendLine(") Tbl1 ");
            SQLDtl.AppendLine("Left Join ( ");
            SQLDtl.AppendLine("    Select T.Date,  ");
            SQLDtl.AppendLine("    IfNull(T.room_arrival, 0.00) As room_arr,  ");
            SQLDtl.AppendLine("    IfNull(T.room_dpt, 0.00) As room_dpt, ");
            SQLDtl.AppendLine("    IfNull(T.room_occupied, 0.00) As room_occupied,  ");
            SQLDtl.AppendLine("    Case When IfNull(T.total_room, 0.00)>0.00 Then  ");
            SQLDtl.AppendLine("        (IfNull(T.room_occupied, 0.00)/IfNull(T.total_room, 0.00))*100.00  ");
            SQLDtl.AppendLine("    Else 0.00 End As room_occupiedP, ");
            SQLDtl.AppendLine("    IfNull(T.total_room, 0.00) As total_room, ");
            SQLDtl.AppendLine("    IfNull(T.room_vacant, 0.00) As Vac, ");
            SQLDtl.AppendLine("    IfNull(T.room_oo, 0.00) As room_oo, ");
            SQLDtl.AppendLine("    IfNull(T.avail_remain, 0.00) As room_available, ");
            SQLDtl.AppendLine("    IfNull(nett_revenue, 0.00) As trr, ");
            SQLDtl.AppendLine("    IfNull(arr, 0.00) As avr, ");
            SQLDtl.AppendLine("    IfNull(revpar, 0.00) As revpar ");
            SQLDtl.AppendLine("    From TblAvailForecasts T ");
            SQLDtl.AppendLine("    Where Date Between @Dt1 And @Dt2 ");
            SQLDtl.AppendLine(") Tbl2 On Tbl1.Dt=Tbl2.Date ");
            SQLDtl.AppendLine("Order By Tbl1.Dt; ");

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@Yr", Sm.Left(Sm.GetDte(DteDocDt1), 4));
                Sm.CmParamDt(ref cmDtl, "@Dt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cmDtl, "@Dt2", Sm.GetDte(DteDocDt2));
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                { 
                    //0
                    "Dt", 

                    //1-5
                    "DayNm", "room_arr", "room_dpt", "room_occupied", "room_occupiedP", 

                    //6-10
                    "total_room", "Vac", "room_oo", "room_available", "trr", 

                    //11-13
                    "avr", "revpar", "FullDayNm"
                });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        lDtl.Add(new Result()
                        {
                            Dt = Sm.DrStr(drDtl, cDtl[0]),
                            DayNm = Sm.DrStr(drDtl, cDtl[1]),
                            room_arr = Sm.DrDec(drDtl, cDtl[2]),
                            room_dpt = Sm.DrDec(drDtl, cDtl[3]),
                            room_occupied = Sm.DrDec(drDtl, cDtl[4]),
                            room_occupiedP = Sm.DrDec(drDtl, cDtl[5]),
                            total_room = Sm.DrDec(drDtl, cDtl[6]),
                            Vac = Sm.DrDec(drDtl, cDtl[7]),
                            room_oo = Sm.DrDec(drDtl, cDtl[8]),
                            room_available = Sm.DrDec(drDtl, cDtl[9]),
                            trr = Sm.DrDec(drDtl, cDtl[10]),
                            avr = Sm.DrDec(drDtl, cDtl[11]),
                            revpar = Sm.DrDec(drDtl, cDtl[12]),
                            FullDayNm = Sm.DrStr(drDtl, cDtl[13])
                        });
                    }
                }
                drDtl.Close();
            }

            myLists.Add(lDtl);

            #endregion

            Sm.PrintReport("HotelAvailabilityForecast", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            var Dt = Sm.GetDte(DteDocDt1);
            if (Dt.Length > 0)
            {
                Dt = string.Concat(Sm.Left(Dt, 6), "01");
                DteDocDt2.DateTime = (Sm.ConvertDate(Dt).AddMonths(1)).AddDays(-1);
            }
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string Dt { get; set; }
            public string DayNm { get; set; }
            public string FullDayNm { get; set; }
            public decimal room_arr { get; set; }
            public decimal room_dpt { get; set; }
            public decimal room_occupied { get; set; }
            public decimal room_occupiedP { get; set; }
            public decimal total_room { get; set; }
            public decimal Vac { get; set; }
            public decimal room_oo { get; set; }
            public decimal room_available { get; set; }
            public decimal trr { get; set; }
            public decimal avr { get; set; }
            public decimal revpar { get; set; }
        }

        private class ResultHeader
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MenuDesc { get; set; }
            public string DocDt { get; set; }
            public string PrintBy { get; set; }
        }

        #endregion
    }
}
