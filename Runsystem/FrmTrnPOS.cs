﻿#region Update
/*
    11/09/2017 [TKG] Daftar item di Find product ditambah validasi status sudah ter-approved.
    13/10/2017 [TKG] nomor nota berdasarkan pos#
*/
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Drawing.Printing;

using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

namespace RunSystem
{
    public partial class FrmTrnPOS : Form
    {
        iGCell fCell;
        bool fAccept;

        private bool 
            mIsPOSQtyEditable = false,
            mIsPOSReceiptShowDiscount = false,
            Reprint = false;
        private string Err_InvalidProduct = ", Invalid Product";
        private string[] PosTaxDesc = new string[5];
        private decimal[] PosTaxRate = new decimal[5];

        private bool PosTax1Inclusive = false;
        private bool PosTax2Inclusive = false;
        private bool PosTax3Inclusive = false;

        private string PosTax1Code = "";
        private string PosTax2Code = "";
        private string PosTax3Code = "";

        private string PosTax1Name = "";
        private string PosTax2Name = "";
        private string PosTax3Name = "";

        private decimal PosTax1Rate = 0;
        private decimal PosTax2Rate = 0;
        private decimal PosTax3Rate = 0;


        private int MinColWidth = 35;

        private string LastMasterReloadedTime;
        private List<MasterItem> MasterItemList = new List<MasterItem>();
        private List<FunctionKey> FunctionList = new List<FunctionKey>();

        public static string ItemSearchSQL =
            " Select T.* " +
            " From ( " +
            " Select D.ItCode, E.ItName, Replace(D.ItCode, '-', '') As BarCode, C.CurCode, D.UPrice, B.Discount As DiscRate, " +
            " CurrentDatetime() As ReloadTime, 'Y' As TaxLiableInd " +
            " From TblCtQtHdr A " +
            " Inner join TblCtQtDtl B On A.DocNo=B.DocNo " +
            " Inner Join TblItemPriceHdr C On B.ItemPriceDocNo=C.DocNo " +
            " Inner Join TblItemPricedtl D On B.ItemPriceDocNo=D.DocNo And B.ItemPriceDNo=D.DNo " +
            " Inner join TblItem E on D.ItCode = E.ItCode " +
            " Where A.ActInd='Y' " +
            " And A.Status='A' " +
            " And A.CtCode In ( "+
            "   Select SetValue From TblPosSetting " +
            "   Where SetCode='StoreCtCode' " +
            "   And PosNo='" + Gv.PosNo + "' " +
            ")) T ";

            //" Select T.* From (Select '0002-00288' As ItCode, 'Testing' As ItName, '' As BarCode, 'IDR' As CurCode, 1000 As UPrice, " +
            //" 20 As DiscRate, " +
            //" CurrentDatetime() As ReloadTime, 'Y' As TaxLiableInd) T ";

        // On Printing Purpose
        private decimal PaymentChange = 0;
        private string CurrentPrintNumber = "";
        private PrintDocument pdoc = null;
        private string PrinterName = "";

        private enum GrdPosition { Row, Col };

        // Row, Col Position in grid

        #region Row, Col Position in grid

        private class GrdPos
        {
            public int Row;
            public int Col;
            public int ColWidth = 80;
            public string Title;
            public iGContentAlignment TextAlign = iGContentAlignment.TopLeft;
            public Type ValueType = typeof(string);
            public decimal BaseDecimal = 0;
            public int[] RoundValue = new int[10];
            public bool Visible = true;
        }

        private GrdPos GrdPosSalesType = new GrdPos { 
            Row = 0, Col = 0, ColWidth = 20,
            Title = "Type", 
            TextAlign = iGContentAlignment.TopCenter, 
            Visible = false,
            ValueType = typeof(string) };

        private GrdPos GrdPosProductName = new GrdPos
        {
            Row = 0,
            Col = 1,
            ColWidth = 400,
            Title = "Product Name",
            TextAlign = iGContentAlignment.TopLeft,
            Visible = true,
            ValueType = typeof(string)
        };

        private GrdPos GrdPosProductCode = new GrdPos
        { 
            Row = 1, Col = 1, ColWidth = 80, 
            Title = "Product Code", 
            TextAlign = iGContentAlignment.TopLeft,
            Visible = true,
            ValueType = typeof(string)
        };


        private GrdPos GrdPosCurrencyCode = new GrdPos {
            Row = 0, Col = 2, ColWidth = 45,
            Title = "",
            Visible = false,
            TextAlign = iGContentAlignment.TopLeft
        };

        private GrdPos GrdPosCurrencyRate = new GrdPos {
            Row = 1, Col = 2, ColWidth = 45,
            Title = "", TextAlign = iGContentAlignment.TopRight,
            Visible = false,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosActualUnitPrice = new GrdPos {
            Row = 0, Col = 3, ColWidth = 100,
            Title = "Actual Price", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false
            };

        private GrdPos GrdPosUnitPrice = new GrdPos { 
            Row = 0, Col = 4, ColWidth = 100, 
            Title = "Unit Price", 
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosProductDiscount = new GrdPos { 
            Row = 1, Col = 4, ColWidth = 100, 
            Title = "Discount", 
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosQuantity = new GrdPos { 
            Row = 0, Col = 5, ColWidth = 100, 
            Title = "Quantity", 
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal),
            RoundValue = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }};

        private GrdPos GrdPosWeight = new GrdPos { 
            Row = 0, Col = 6, ColWidth = 100,
            Title = "Weight", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight, 
            Visible = false};

        private GrdPos GrdPosSubTotal = new GrdPos { 
            Row = 0, Col = 7, ColWidth = 120, 
            Title = "Sub Total", 
            TextAlign = iGContentAlignment.TopRight,
            Visible = true,
            ValueType = typeof(decimal)
        };

        private GrdPos GrdPosTotalDisc = new GrdPos { 
            Row = 0, Col = 8, ColWidth = 100, 
            Title = "Total Disc", 
            ValueType = typeof(decimal),
            Visible = false };

        private GrdPos GrdPosProductDiscountRate = new GrdPos { 
            Row = 0, Col = 9, ColWidth = 100, 
            Title = "Discount Rate",
            Visible = false };


        private GrdPos GrdPosTax1 = new GrdPos { 
            Row = 0, Col = 10, ColWidth = 100, 
            Title = "Tax", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false };

        private GrdPos GrdPosTaxRate1 = new GrdPos {
            Row = 1, Col = 10, ColWidth = 100,
            Title = "Tax", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false };

        private GrdPos GrdPosTax2 = new GrdPos {
            Row = 0, Col = 11, ColWidth = 100,
            Title = "Tax", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false };

        private GrdPos GrdPosTaxRate2 = new GrdPos {
            Row = 1, Col = 11, ColWidth = 100,
            Title = "Tax", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false };

        private GrdPos GrdPosTax3 = new GrdPos {
            Row = 0, Col = 12, ColWidth = 100,
            Title = "Tax", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false };
        
        private GrdPos GrdPosTaxRate3 = new GrdPos {
            Row = 1, Col = 12, ColWidth = 100,
            Title = "Tax", ValueType = typeof(decimal),
            TextAlign = iGContentAlignment.TopRight,
            Visible = false };

        private GrdPos GrdPosTaxCode1 = new GrdPos {
            Row = 0, Col = 13, ColWidth = 100,
            Title = "Tax Code",
            Visible = false };

        private GrdPos GrdPosTaxCode2 = new GrdPos {
            Row = 0, Col = 14,  ColWidth = 100,
            Title = "Tax Code",
            Visible = false };

        private GrdPos GrdPosTaxCode3 = new GrdPos {
            Row = 0, Col = 15, ColWidth = 100,
            Title = "Tax Code",
            Visible = false };



        private Font GridTitleFont = new Font("Tahoma", 9, FontStyle.Bold);
        private Font FirstLineFont = new Font("Tahoma", 11, FontStyle.Regular);
        private Font SecondLineFont = new Font("Tahoma", 8, FontStyle.Regular);
        private Color NormalFontColor = Color.Black;
        private Color ErrorFontColor = Color.Red;

        private int TotalCol = 16;

        private decimal TotalBaseDecimal;
        public int[] TotalRoundValue = new int[10];

        private List<GrdPos> ListOfColumns = new List<GrdPos>();

        #endregion

        #region Global Variable

        private ArrayList TaxCodeList = new ArrayList();

        DateTime LastLocalTimeConnected = DateTime.Now;

        private string mMenuCode = "";
        public string BusinessDate = "";
        public string ShiftNumber = "";
        private string TransactionNoFormat = "YYMMDDPNNNN";


        private string LocalCurrency = "IDR";
        private List<CurrencyRate> CurrencyRateList = new List<CurrencyRate>();

        private bool IsNewLine = true;
        private string SalesType = "S";
        private int CurrentRow = 0;
        private int QuantityPos = 0;
        private decimal CurrentQuantity = 1;
        private decimal TotalDiscount = 0;


        #endregion

        // remarked for further enhancement

        #region OfflineProductList 

        private void ReloadFunctionList()
        {
            try
            {
                FunctionList.Clear();

                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = "Select * from tblPOSFunction";
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        //0
                        dr.GetOrdinal("FuncCode"),

                        //1-5
                        dr.GetOrdinal("FuncName"),
                        dr.GetOrdinal("FuncKey"),
                        dr.GetOrdinal("Password"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            FunctionList.Add(new FunctionKey()
                            {
                                FuncCode = Sm.DrStr(dr, c[0]),
                                FuncName = Sm.DrStr(dr, c[1]),
                                FuncKey = Sm.DrStr(dr, c[1]),
                                FuncPwd = Sm.DrStr(dr, c[1]),
                            });
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
        
        private void ReloadMasterList()
        {
            try
            {
                MasterItemList.Clear();
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = ItemSearchSQL;
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        //0
                        dr.GetOrdinal("ItCode"),

                        //1-5
                        dr.GetOrdinal("ItName"),
                        dr.GetOrdinal("BarCode"),
                        dr.GetOrdinal("CurCode"),
                        dr.GetOrdinal("UPrice"),                    
                        dr.GetOrdinal("DiscRate"),    
                    
                        //6
                        dr.GetOrdinal("TaxLiableInd"),       
                        dr.GetOrdinal("ReloadTime")       
                        
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            MasterItemList.Add(new MasterItem () {
                                ItCode = Sm.DrStr(dr, c[0]),
                                ItName = Sm.DrStr(dr, c[1]),
                                BarCode = Sm.DrStr(dr, c[2]),
                                CurCode = Sm.DrStr(dr, c[3]),
                                CurRate = 1,
                                UnitPrice = Sm.DrDec(dr, c[4]),
                                DiscRate = Sm.DrDec(dr, c[5]),
                                Tax1Amt = CalcTaxAmt(PosTax1Inclusive, (Sm.DrStr(dr, c[6]) == "Y"),
                                    Sm.DrDec(dr, c[4]) * ((100 - Sm.DrDec(dr, c[5])) / 100), PosTax1Rate),

                                Tax2Amt = CalcTaxAmt(PosTax2Inclusive, (Sm.DrStr(dr, c[6]) == "Y"),
                                    Sm.DrDec(dr, c[4]) * ((100 - Sm.DrDec(dr, c[5])) / 100), PosTax2Rate),

                                Tax3Amt = CalcTaxAmt(PosTax3Inclusive, (Sm.DrStr(dr, c[6]) == "Y"),
                                    Sm.DrDec(dr, c[4]) * ((100 - Sm.DrDec(dr, c[5])) / 100), PosTax3Rate)

                            });
                            LastMasterReloadedTime = Sm.DrStr(dr, c[5]);
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void LoadCurrencyRate()
        {
            try
            {
                CurrencyRateList.Clear();
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = "Select CurCode1, Amt From tblcurrencyrate " +
                            " Where CurCode2 = '" + LocalCurrency + "' " +
                            " And RateDt = '" + BusinessDate + "' ";

                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        dr.GetOrdinal("CurCode1"),
                        dr.GetOrdinal("Amt")                   
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CurrencyRateList.Add(new CurrencyRate()
                            {                         
                                CurCode = Sm.DrStr(dr, c[0]),
                                CurRate = Sm.DrDec(dr, c[1])
                            });
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion
        
        #region Format Rounding
        //       1 = {0:###,###,###,##0}
        //     0.1 = {0:###,###,###,##0.0}
        //    0.01 = {0:###,###,###,##0.00}
        //   0.001 = {0:###,###,###,##0.000}
        //  0.0001 = {0:###,###,###,##0.0000}
        // 0.00001 = {0:###,###,###,##0.00000}
        #endregion

        #region Constructor
        public FrmTrnPOS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }
        #endregion

        #region Common Functions


        private string GetComputerName()
        {
            return System.Net.Dns.GetHostName();
        }
        private string MonthName(string MonthNumber)
        {
            string rstMonthName = "";
            switch (MonthNumber)
            {
                case "01": rstMonthName = "Jan"; break;
                case "02": rstMonthName = "Feb"; break;
                case "03": rstMonthName = "Mar"; break;
                case "04": rstMonthName = "Apr"; break;
                case "05": rstMonthName = "May"; break;
                case "06": rstMonthName = "Jun"; break;
                case "07": rstMonthName = "Jul"; break;
                case "08": rstMonthName = "Aug"; break;
                case "09": rstMonthName = "Sep"; break;
                case "10": rstMonthName = "Oct"; break;
                case "11": rstMonthName = "Nov"; break;
                case "12": rstMonthName = "Dec"; break;
            }
            return rstMonthName;
        }
        private decimal GetRoundValue(decimal Value, int[] RoundValue, decimal BaseDecimal)
        {
                /// rounding per 500, Base Decimal = 100, 
                /// roundvalue[0] = 0, roundvalue[1] = 0, roundvalue[2] = 0, roundvalue[3] = 5, roundvalue[4] = 5,
                /// roundvalue[5] = 5,roundvalue[6] = 5,roundvalue[7] = 5,roundvalue[8] = 10, roundvalue[9] = 10
                
                decimal BasePoint = 1;
                decimal ReminderPoint = 1;
                if (BaseDecimal < 0)
                {
                    for (int intX = 1; intX < Math.Abs(BaseDecimal); intX++)
                        BasePoint = BasePoint * 10;

                    for (int intX = 1; intX <= Math.Abs(BaseDecimal); intX++)
                        ReminderPoint = ReminderPoint * 10;
                }
                else
                {
                    for (int intX = 1; intX <= (BaseDecimal + 1); intX++)
                        BasePoint = BasePoint / 10;

                    for (int intX = 1; intX <= BaseDecimal; intX++)
                        ReminderPoint = ReminderPoint / 10;
                }

                decimal BaseValue = Math.Floor(Value * BasePoint) / BasePoint;
                decimal ReminderRounded = Value - BaseValue;


                ReminderRounded = Math.Floor(ReminderRounded * ReminderPoint);
                ReminderRounded = RoundValue[Int32.Parse(ReminderRounded.ToString())] / ReminderPoint;
                return BaseValue + ReminderRounded;
        }
        private decimal GetBaseDecimal(decimal Value)
        {
            decimal Temp = 1;
            if (Value > 0)
            {
                for (int intX = 1; intX <= Value; intX++)
                    Temp = Temp * 10;
            }
            else
                for (int intX = 1; intX <= Math.Abs(Value); intX++)
                    Temp = Temp / 10;
            return Temp;
        }      
        private decimal RoundingFormat(string ParamName)
        {
            decimal Result = 0;
            try
            {
                Result = Decimal.Parse(Sm.GetPosSetting(ParamName));

            }
            catch (Exception Exc)
            {
                AddToAuditRoll("Error Get Initial Data : Rounding Format " + ParamName + " " + Exc.Message);
            }
            return Result;
        }

        private int[] RoundingValue(string ParamName)
        {
            int[] Result = new int[10];
            try
            {
                for (int Index = 0; Index < 10; Index++)
                    Result[Index] = Int32.Parse(Sm.GetPosSetting(ParamName + Index.ToString()));
            }
            catch (Exception Exc)
            {
                for (int Index = 0; Index < 10; Index++)
                    Result[Index] = Index;
                AddToAuditRoll("Error Get Initial Data : Rounding Value " + ParamName + " " + Exc.Message);
            }
            return Result;
        }
        private string GetNumFormat(decimal Value)
        {
            string FormatDec = "";

            if (Value >= 0)
                FormatDec = "{0:###,###,###,##0}";
            else
                if (Double.Parse(Value.ToString()) == -1)
                    FormatDec = "{0:###,###,###,##0.0}";
                else
                    if (Double.Parse(Value.ToString()) >= -2)
                        FormatDec = "{0:###,###,###,##0.00}";
                    else
                        if (Double.Parse(Value.ToString()) >= -3)
                            FormatDec = "{0:###,###,###,##0.000}";
                        else
                            if (Double.Parse(Value.ToString()) >= -4)
                                FormatDec = "{0:###,###,###,##0.0000}";
                            else
                                if (Double.Parse(Value.ToString()) >= -5)
                                    FormatDec = "{0:###,###,###,##0.00000}";
                                else
                                    FormatDec = "{0:###,###,###,##0.000000}";
            return FormatDec;
        }

        
        #endregion

        #region Grid Function

        private void ReadColumnSetting()
        {
            try
            {
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = "Select * from tblposgridsetting where ColVisible='Y' order by ColType";
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        //0
                        dr.GetOrdinal("iRow"),

                        //1-5
                        dr.GetOrdinal("iCol"),
                        dr.GetOrdinal("ColType"),
                        dr.GetOrdinal("ColWidth"),
                        dr.GetOrdinal("Title"),                    
                        dr.GetOrdinal("TAlign"),
   
                        //6
                        dr.GetOrdinal("ValueType"),                    
                        dr.GetOrdinal("ColVisible"),                    
                        dr.GetOrdinal("RoundValue")                    
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            switch (Sm.DrStr(dr, c[2]))
                            {
                                case "SalesType":
                                    GrdPosSalesType.Row = Sm.DrInt(dr, c[0]);
                                    GrdPosSalesType.Col = Sm.DrInt(dr, c[1]);
                                    GrdPosSalesType.ColWidth = Sm.DrInt(dr, c[3]);
                                    GrdPosSalesType.Title = Sm.DrStr(dr, c[4]);
                                    GrdPosSalesType.Visible = (Sm.DrStr(dr, c[7]) == "Y");
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {

            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = TotalCol;

            // Reading Column Setting from Database
            //ReadColumnSetting();

            ListOfColumns.Add(GrdPosSalesType);
            ListOfColumns.Add(GrdPosProductCode);
            ListOfColumns.Add(GrdPosProductName);
            ListOfColumns.Add(GrdPosCurrencyCode);
            ListOfColumns.Add(GrdPosCurrencyRate);
            ListOfColumns.Add(GrdPosUnitPrice);
            ListOfColumns.Add(GrdPosProductDiscount);
            ListOfColumns.Add(GrdPosActualUnitPrice);
            ListOfColumns.Add(GrdPosQuantity);
            ListOfColumns.Add(GrdPosWeight);
            ListOfColumns.Add(GrdPosSubTotal);
            ListOfColumns.Add(GrdPosTotalDisc);
            ListOfColumns.Add(GrdPosProductDiscountRate);
            ListOfColumns.Add(GrdPosTax1);
            ListOfColumns.Add(GrdPosTaxRate1);
            ListOfColumns.Add(GrdPosTax2);
            ListOfColumns.Add(GrdPosTaxRate2);
            ListOfColumns.Add(GrdPosTax3);
            ListOfColumns.Add(GrdPosTaxRate3);
            ListOfColumns.Add(GrdPosTaxCode1);
            ListOfColumns.Add(GrdPosTaxCode2);
            ListOfColumns.Add(GrdPosTaxCode3);

            for (int intX = 0; intX < ListOfColumns.Count; intX++)
            {
                SetGridPosFormat(ListOfColumns[intX]);
            }

            //// Manual Resize
            //int NumVisibleCol = 0;
            //int TotalWidth = 0;
            //int MaxWidth = Grd1.Width;
            //for (int intCol = 0; intCol < Grd1.Cols.Count; intCol++)
            //    if (Grd1.Cols[intCol].Visible == true)
            //    {
            //        TotalWidth += Grd1.Cols[intCol].Width;
            //        NumVisibleCol++;
            //    }

            //int AddWidth = 0;
            //if (NumVisibleCol > 0) AddWidth = (MaxWidth - TotalWidth) / NumVisibleCol;

            //TotalWidth = 0;
            //for (int intCol = 0; intCol < Grd1.Cols.Count; intCol++)
            //    if (Grd1.Cols[intCol].Visible == true)
            //    {
            //        Grd1.Cols[intCol].Width += AddWidth;
            //        if ((TotalWidth + Grd1.Cols[intCol].Width) < MaxWidth)
            //            TotalWidth += Grd1.Cols[intCol].Width;
            //        else
            //            Grd1.Cols[intCol].Width = MaxWidth - TotalWidth;

            //    }
            Grd1.Rows.Count = 0;
        }

        private void ResetRow()
        {
            IsNewLine = true;
            CurrentQuantity = 1;
            QuantityPos = 0;
        }

        #endregion

        #region Pos Function

        private void ClearData()
        {
            LbAuditRoll.Items.Clear();
            LblCode.Text = "";
        }

        private void NewTransaction()
        {
            CurrentRow = 0;
            PaymentChange = 0;
            Grd1.Rows.Count = 0;
            TotalDiscount = 0;
            ComputeGrandTotal();

        }

        private void GetInitialData()
        {
            try
            {
                AddToTotalList("Sub Total", "0.00");
                AddToTotalList("Disc", "0.00");

                PrinterName = Sm.GetPosSetting("PrinterName");

                // Open New Day
                if (Sm.GetPosSetting("StsDayEnd") == "C")
                {
                    // it's a new business date
                    Sm.SetPosSetting("CurrentBnsDate", Sm.ServerCurrentDateTime().Substring(0, 8));
                    Sm.SetPosSetting("StsDayEnd", "O");
                }

                if (Sm.GetPosSetting("StsShift") != "O")
                {
                    string CurrentShift = Sm.GetPosSetting("CurrentShift");
                    if (CurrentShift == "") CurrentShift = "0";
                    Sm.SetPosSetting("CurrentShift", (Int32.Parse(CurrentShift) + 1).ToString());
                    Sm.SetPosSetting("CurrentUser", Gv.CurrentUserCode);
                    Sm.SetPosSetting("StsShift", "O");

                    var frmPopUp = new FrmPosPopUp("Enter Float Amount", "Float Amount", "{0:#,###,###,##0}", true, false, "");
                    if (frmPopUp.ShowDialog(this) == DialogResult.OK)
                    {
                        Sm.SetPosSetting("FloatAmount", Decimal.Parse(frmPopUp.TxtAmount.Text).ToString());
                        AddToAuditRoll("Enter Float Amount : " + frmPopUp.TxtAmount.Text);
                        // update to tblposshiftlog
                        SetPosShiftLog(Sm.GetPosSetting("CurrentBnsDate"), Sm.GetPosSetting("CurrentShift"), Sm.GetPosSetting("CurrentUser"), Decimal.Parse(frmPopUp.TxtAmount.Text).ToString()); 
                    }

                }

                PosTax1Inclusive = (Sm.GetPosSetting("Tax1Inclusive") == "Y");
                PosTax2Inclusive = (Sm.GetPosSetting("Tax2Inclusive") == "Y");
                PosTax3Inclusive = (Sm.GetPosSetting("Tax3Inclusive") == "Y");

                PosTax1Code = Sm.GetPosSetting("Tax1Code");
                PosTax2Code = Sm.GetPosSetting("Tax2Code");
                PosTax3Code = Sm.GetPosSetting("Tax3Code");

                PosTax1Name = Sm.GetValue("Select TaxName From tblTax where TaxCode='" + PosTax1Code + "' Limit 1");
                PosTax2Name = Sm.GetValue("Select TaxName From tblTax where TaxCode='" + PosTax2Code + "' Limit 1");
                PosTax3Name = Sm.GetValue("Select TaxName From tblTax where TaxCode='" + PosTax3Code + "' Limit 1");

                if (PosTax1Inclusive) PosTax1Name = PosTax1Name + " (Incl)";
                if (PosTax2Inclusive) PosTax2Name = PosTax2Name + " (Incl)";
                if (PosTax3Inclusive) PosTax3Name = PosTax3Name + " (Incl)";

                string Tax1Rate = Sm.GetValue("Select TaxRate From tblTax where TaxCode='" + PosTax1Code + "' Limit 1");
                string Tax2Rate = Sm.GetValue("Select TaxRate From tblTax where TaxCode='" + PosTax2Code + "' Limit 1");
                string Tax3Rate = Sm.GetValue("Select TaxRate From tblTax where TaxCode='" + PosTax3Code + "' Limit 1");
                
                PosTax1Rate = (Tax1Rate == "") ? 0 : Decimal.Parse(Tax1Rate);
                PosTax2Rate = (Tax2Rate == "") ? 0 : Decimal.Parse(Tax2Rate);
                PosTax3Rate = (Tax3Rate == "") ? 0 : Decimal.Parse(Tax3Rate);

                if (PosTax1Code != "") AddToTotalList(PosTax1Name, "0.00");
                if (PosTax2Code != "") AddToTotalList(PosTax2Name, "0.00");
                if (PosTax3Code != "") AddToTotalList(PosTax3Name, "0.00");

                BusinessDate = Sm.GetPosSetting("CurrentBnsDate");
                ShiftNumber = Sm.GetPosSetting("CurrentShift");
                LblCashier.Text = Sm.GetValue("SELECT UserName FROM TblUser WHERE UserCode='" + Gv.CurrentUserCode + "'");
                LblPos.Text = Gv.PosNo;
                LblShift.Text = ShiftNumber;

                TransactionNoFormat = Sm.GetPosSetting("TransNoFormat");
                TransactionNoFormat.Replace("YY", BusinessDate.Substring(2, 2));
                TransactionNoFormat.Replace("MM", BusinessDate.Substring(4, 2));
                TransactionNoFormat.Replace("DD", BusinessDate.Substring(6, 2));
                TransactionNoFormat.Replace("P", Gv.PosNo);

                GrdPosWeight.Visible = ((Sm.GetPosSetting("IsUseWeight") == "Y") ? true : false);
                GrdPosQuantity.BaseDecimal = RoundingFormat("BaseDecQuantity");
                GrdPosUnitPrice.BaseDecimal = RoundingFormat("BaseDecUnitPrice");
                GrdPosUnitPrice.RoundValue = RoundingValue("BaseUnitPrice");
                GrdPosWeight.BaseDecimal = RoundingFormat("BaseDecUnitWeight");
                GrdPosWeight.RoundValue = RoundingValue("RndUnitWeight");
                GrdPosProductDiscount.BaseDecimal = RoundingFormat("BaseDecDiscount");
                GrdPosProductDiscount.RoundValue = RoundingValue("RndDiscount");
                GrdPosSubTotal.BaseDecimal = RoundingFormat("BaseDecSubTotal");
                GrdPosSubTotal.RoundValue = RoundingValue("RndSubTotal");

                TotalBaseDecimal = RoundingFormat("BaseDecTotal");
                TotalRoundValue = RoundingValue("RndTotal");

                ResetRow();

                ReloadMasterList();
                ReloadFunctionList();

                LocalCurrency = Sm.GetPosSetting("LocalCur");
                LoadCurrencyRate();

                //PnlRate.Visible = true;
                //TxtSellRate.Text = "0.00";
                //TxtBuyRate.Text = "0.00";
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void AddToAuditRoll(string Message)
        {
            Sm.AddToAuditRollFile(BusinessDate, Message);
            LbAuditRoll.Items.Add(Message);
        }

        private void AddToAuditRollRowInfo(int Row)
        {
            AddToAuditRoll("[" + Sm.GetGrdStr(Grd1, Row + GrdPosSalesType.Row, GrdPosSalesType.Col) + "] " +
                String.Format(GetNumFormat(GrdPosQuantity.BaseDecimal), Sm.GetGrdDec(Grd1, Row + GrdPosQuantity.Row, GrdPosQuantity.Col)) +
                " x " + Sm.GetGrdStr(Grd1, Row + GrdPosProductName.Row, GrdPosProductName.Col));

            AddToAuditRoll("[" + Sm.GetGrdStr(Grd1, Row + GrdPosSalesType.Row, GrdPosSalesType.Col) + "] " +
                Sm.GetGrdStr(Grd1, Row + GrdPosProductCode.Row, GrdPosProductCode.Col) + " @ " +
                String.Format(GetNumFormat(GrdPosUnitPrice.BaseDecimal), Sm.GetGrdDec(Grd1, Row + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col)) +
                ((Sm.GetGrdDec(Grd1, Row + GrdPosProductDiscount.Row, GrdPosProductDiscount.Col) == 0) ? "" :
                " Disc " + String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), Sm.GetGrdDec(Grd1, Row + GrdPosProductDiscount.Row, GrdPosProductDiscount.Col)) + " = ") +
                Sm.CopyStr(" ", (15 - String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.GetGrdDec(Grd1, Row + GrdPosSubTotal.Row, GrdPosSubTotal.Col)).Length)) +
                String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.GetGrdDec(Grd1, Row + GrdPosSubTotal.Row, GrdPosSubTotal.Col)));

        }

        private void SendErrorToMessageTheScreen(string Message)
        {
            LblCode.Text = Message;
            AddToAuditRoll(Message);
            ResetRow();
        }

        private decimal CalcTaxAmt(bool TaxInclusive, bool TaxLiableInd, decimal NettPrice, decimal TaxRate)
        {
            if (TaxLiableInd)
            {
                if (TaxInclusive)
                    return NettPrice - (NettPrice / ((100 + TaxRate) / 100));
                else
                    return (NettPrice * TaxRate) / 100;
            }
            else
                return 0;
        }

        private void ComputeSubTotal(int Row)
        {
            Grd1.Cells[Row + GrdPosSubTotal.Row, GrdPosSubTotal.Col].Value =
                GetRoundValue((Sm.GetGrdDec(Grd1, Row + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col) +
                Sm.GetGrdDec(Grd1, Row + GrdPosProductDiscount.Row, GrdPosProductDiscount.Col)) *
                Sm.GetGrdDec(Grd1, Row + GrdPosQuantity.Row, GrdPosQuantity.Col) *
                Sm.GetGrdDec(Grd1, Row + GrdPosWeight.Row, GrdPosWeight.Col), GrdPosSubTotal.RoundValue, GrdPosSubTotal.BaseDecimal);

            Grd1.Cells[Row + GrdPosSubTotal.Row, GrdPosSubTotal.Col].Font = (GrdPosSubTotal.Row == 0 ? FirstLineFont : SecondLineFont);

            ComputeGrandTotal();
        }

        private void ComputeGrandTotal()
        {
            decimal decSubtotal = 0;
            decimal decTotalDisc = 0;
            decimal decTax = 0;
            for (int Index = 0; Index < Grd1.Rows.Count; Index++)
            {
                if ((Index % 2) == 0)
                {
                    if (Sm.GetGrdStr(Grd1, GrdPosSalesType.Row + Index, GrdPosSalesType.Col) == "R")
                        decSubtotal = decSubtotal - Sm.GetGrdDec(Grd1, GrdPosSubTotal.Row + Index, GrdPosSubTotal.Col);
                    else
                        decSubtotal = decSubtotal + Sm.GetGrdDec(Grd1, GrdPosSubTotal.Row + Index, GrdPosSubTotal.Col);
                    Grd1.Cells[GrdPosTotalDisc.Row + Index, GrdPosTotalDisc.Col].Value = 0;
                }
            }
            decimal TtlTax1 = 0;
            decimal TtlTax2 = 0;
            decimal TtlTax3 = 0;
            for (int Index = 0; Index < Grd1.Rows.Count; Index++)
            {
                if ((Index % 2) == 0)
                {
                    if ((TotalDiscount > 0) && (decSubtotal != 0))
                    {
                        Grd1.Cells[GrdPosTotalDisc.Row + Index, GrdPosTotalDisc.Col].Value = -1 * (Sm.GetGrdDec(Grd1, GrdPosSubTotal.Row + Index, GrdPosSubTotal.Col) / decSubtotal) * TotalDiscount;
                        decTotalDisc = decTotalDisc - Sm.GetGrdDec(Grd1, GrdPosTotalDisc.Row + Index, GrdPosTotalDisc.Col);

                        if (Index == Grd1.Rows.Count - 2)
                            Grd1.Cells[GrdPosTotalDisc.Row + Index, GrdPosTotalDisc.Col].Value = (Sm.GetGrdDec(Grd1, GrdPosTotalDisc.Row + Index, GrdPosTotalDisc.Col) + (TotalDiscount - decTotalDisc));
                    }

                    TtlTax1 += Sm.GetGrdDec(Grd1, GrdPosTax1.Row + Index, GrdPosTax1.Col);
                    if (PosTax1Inclusive == false)
                        decTax = decTax + Sm.GetGrdDec(Grd1, GrdPosTax1.Row + Index, GrdPosTax1.Col);

                    TtlTax2 += Sm.GetGrdDec(Grd1, GrdPosTax2.Row + Index, GrdPosTax2.Col);
                    if (PosTax2Inclusive == false)
                        decTax = decTax + Sm.GetGrdDec(Grd1, GrdPosTax2.Row + Index, GrdPosTax2.Col);

                    TtlTax3 += Sm.GetGrdDec(Grd1, GrdPosTax3.Row + Index, GrdPosTax3.Col);
                    if (PosTax3Inclusive == false)
                        decTax = decTax + Sm.GetGrdDec(Grd1, GrdPosTax3.Row + Index, GrdPosTax3.Col);
                }
            }

            SetValueToList(0, "Total", String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), decSubtotal));
            SetValueToList(1, "Discount", String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), TotalDiscount));

            decimal TotalDiscountTax1 = 0;
            decimal TotalDiscountTax2 = 0;
            decimal TotalDiscountTax3 = 0;

            if (TotalDiscount != 0)
            {
                if (PosTax1Rate != 0)
                    TotalDiscountTax1 = (TotalDiscount * PosTax1Rate) / 100;
                if (PosTax2Rate != 0)
                    TotalDiscountTax2 = (TotalDiscount * PosTax2Rate) / 100;
                if (PosTax3Rate != 0)
                    TotalDiscountTax3 = (TotalDiscount * PosTax3Rate) / 100;

                TtlTax1 = TtlTax1 - TotalDiscountTax1;
                TtlTax2 = TtlTax2 - TotalDiscountTax2;
                TtlTax3 = TtlTax3 - TotalDiscountTax3;

                decTax = decTax - (TotalDiscountTax1 + TotalDiscountTax2 + TotalDiscountTax3);
            }

            int LastListValue = 1;
            if (PosTax1Code != "")
            {
                LastListValue++;
                SetValueToList(LastListValue, PosTax1Name, String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), TtlTax1));
            }

            if (PosTax2Code != "")
            {
                LastListValue++;
                SetValueToList(LastListValue, PosTax2Name, String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), TtlTax2));
            }

            if (PosTax3Code != "")
            {
                LastListValue++;
                SetValueToList(LastListValue, PosTax3Name, String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), TtlTax3));
            }
            
            
            //           TxtTax.Text = String.Format(GetNumFormat(GrdPosTax.RoundingFormat), decTax);
            TxtGrandTotal.Text = String.Format(GetNumFormat(TotalBaseDecimal), GetRoundValue(decSubtotal - TotalDiscount + decTax, TotalRoundValue, TotalBaseDecimal));
        }

        private void AddToTotalList(string Title, string Value)
        {
            LbTotalTitle.Items.Add(Title);
            LbTotalValue.Items.Add(Value);
            PnlTotal.Height = 30 + ((LbTotalTitle.Items.Count - 1) * 20);
        }

        private string GetValueFromList(int ItemNo)
        {
            return LbTotalValue.Items[ItemNo].ToString();
        }

        private void SetValueToList(int ItemNo, string Title, string Value)
        {
            LbTotalTitle.Items[ItemNo] = Title;
            LbTotalValue.Items[ItemNo] = Value;
        }

        private void PrintLine(Graphics graphics, string LineStr, int startX, int startY, int spaceY, int FontSize, ref int Offset)
        {
            AddToAuditRoll(LineStr);
            graphics.DrawString(LineStr, new Font("Courier New", FontSize), new SolidBrush(Color.Black), startX, startY + Offset);
            Offset = Offset + spaceY;
        }

        private void CloseApp()
        {
            Application.Exit();
        }

        private bool GetProductInfo(string TheCode, string CurrencyCode)
        {
            if (TheCode == "")
                return false;

            int intL = 0;

            while (intL < (TheCode.Length - 1))
            {
                if (TheCode.Length >= (intL + Err_InvalidProduct.Length))
                    if (TheCode.Substring(intL, Err_InvalidProduct.Length) == Err_InvalidProduct)
                        TheCode = TheCode.Substring(0, intL) + TheCode.Substring(intL + Err_InvalidProduct.Length, TheCode.Length - (Err_InvalidProduct.Length + intL));
                intL++;
            }

            bool IndexResult = true;

            List<MasterItem> ItemSearchResult = new List<MasterItem>();

            bool UseOfflineData = false;

            if (CurrencyCode != "") 
                CurrencyCode = " And (T.CurCode = '" + CurrencyCode + "') ";
            try
            {
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = ItemSearchSQL + " Where ((T.ItCode = '" + TheCode + "') Or (T.BarCode = '" + TheCode + "')) "+
                        CurrencyCode;
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        dr.GetOrdinal("ItCode"),

                        //1-5
                        dr.GetOrdinal("ItName"),
                        dr.GetOrdinal("BarCode"),
                        dr.GetOrdinal("CurCode"),
                        dr.GetOrdinal("UPrice"),                    
                        dr.GetOrdinal("DiscRate"),    
                    
                        //6
                        dr.GetOrdinal("TaxLiableInd"),       
                        dr.GetOrdinal("ReloadTime")       
                                                                
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ItemSearchResult.Add(new MasterItem()
                            {
                                ItCode = Sm.DrStr(dr, c[0]),
                                ItName = Sm.DrStr(dr, c[1]),
                                BarCode = Sm.DrStr(dr, c[2]),
                                CurCode = Sm.DrStr(dr, c[3]),
                                CurRate = 1,
                                UnitPrice = Sm.DrDec(dr, c[4]),
                                DiscRate = Sm.DrDec(dr, c[5]),
                                TaxLiableInd = Sm.DrStr(dr, c[6]),

                                Tax1Amt = CalcTaxAmt(PosTax1Inclusive, (Sm.DrStr(dr, c[6]) == "Y"),
                                    Sm.DrDec(dr, c[4]) * ((100 - Sm.DrDec(dr, c[5])) / 100), PosTax1Rate),

                                Tax2Amt = CalcTaxAmt(PosTax2Inclusive, (Sm.DrStr(dr, c[6]) == "Y"),
                                    Sm.DrDec(dr, c[4]) * ((100 - Sm.DrDec(dr, c[5])) / 100), PosTax2Rate),

                                Tax3Amt = CalcTaxAmt(PosTax3Inclusive, (Sm.DrStr(dr, c[6]) == "Y"),
                                    Sm.DrDec(dr, c[4]) * ((100 - Sm.DrDec(dr, c[5])) / 100), PosTax3Rate)


                            });
                        }
                    }
                    else
                    {
                        SendErrorToMessageTheScreen(TheCode + Err_InvalidProduct);
                        IndexResult = false;
                    }
                }
            }
            catch (Exception Exc)
            {
                if (Sm.StdMsgYN("Question", "Data Offline Warning !! Unable to connect to database !! " + Exc.Message + Environment.NewLine + 
                    "Last Offline Data downloaded is " + LastMasterReloadedTime + Environment.NewLine + 
                    "Are you sure you want to use Offline Data ?") == DialogResult.Yes)
                    UseOfflineData = true;
                    

            }

            if (UseOfflineData)
            {                
                for (int intX = 0; intX < MasterItemList.Count; intX++)
                {
                    if (TheCode == MasterItemList[intX].ItCode) ItemSearchResult.Add(MasterItemList[intX]);
                    if (TheCode == MasterItemList[intX].BarCode) ItemSearchResult.Add(MasterItemList[intX]);
                }
            }
            if (ItemSearchResult.Count > 0)
            {


                int TheChosenIndex = 0;
                if (ItemSearchResult.Count > 1)
                {
                    // Pop up selection product
                }

                // check Currency Rate
                decimal CurrencyRate = 0;
                if (ItemSearchResult[TheChosenIndex].CurCode.ToUpper() != LocalCurrency.ToUpper())
                {
                    for (int intX = 0; intX < CurrencyRateList.Count; intX++)
                    {
                        if (CurrencyRateList[intX].CurCode.ToUpper() == ItemSearchResult[TheChosenIndex].CurCode.ToUpper())
                            CurrencyRate = CurrencyRateList[intX].CurRate;
                    }
                }
                else
                    CurrencyRate = 1;

                if (CurrencyRate == 0)
                {
                    SendErrorToMessageTheScreen(TheCode + ", Currency Conversion Not Found ");
                    IndexResult = false;

                }
                else
                {
                    ItemSearchResult[TheChosenIndex].CurRate = CurrencyRate; 
                    //CurrentQuantity = GetRoundValue(CurrentQuantity, GrdPosQuantity.RoundValue, GrdPosQuantity.RoundingFormat);
                    decimal UnitPrice = GetRoundValue(
                        ItemSearchResult[TheChosenIndex].UnitPrice * ItemSearchResult[TheChosenIndex].CurRate, 
                        GrdPosUnitPrice.RoundValue, GrdPosUnitPrice.BaseDecimal);



                    decimal ActualUnitPrice = ItemSearchResult[TheChosenIndex].UnitPrice;
                    decimal UnitWeight = 1;
                    UnitWeight = GetRoundValue(UnitWeight, GrdPosWeight.RoundValue, GrdPosWeight.BaseDecimal);
                    //Discount = GetRoundValue(Discount, GrdPosProductDiscount.RoundValue, GrdPosProductDiscount.RoundingFormat);

                    string ReturnTag = "";
                    Grd1.Rows.Count = Grd1.Rows.Count + 2;
                    if (SalesType == "R")
                    {
                        ReturnTag = "(Return) ";
                        Grd1.Rows[Grd1.Rows.Count - 2].Grid.GridLines.Horizontal.Color = Color.Black;
                        Grd1.Rows[Grd1.Rows.Count - 2].BackColor = Color.LightPink;
                        Grd1.Rows[Grd1.Rows.Count - 1].BackColor = Color.LightPink;
                    }
                    else
                    {
                        Grd1.Rows[Grd1.Rows.Count - 2].Grid.GridLines.Horizontal.Color = Color.White;
                        Grd1.Rows[Grd1.Rows.Count - 2].BackColor = Color.White;
                        Grd1.Rows[Grd1.Rows.Count - 1].BackColor = Color.White;
                    }
                    SetProductInfoCellValue(GrdPosSalesType, SalesType);
                    SetProductInfoCellValue(GrdPosProductCode, ItemSearchResult[TheChosenIndex].ItCode);
                    SetProductInfoCellValue(GrdPosUnitPrice, UnitPrice);
                    SetProductInfoCellValue(GrdPosQuantity, CurrentQuantity);
                    SetProductInfoCellValue(GrdPosWeight, UnitWeight);
                    SetProductInfoCellValue(GrdPosProductName, ReturnTag + ItemSearchResult[TheChosenIndex].ItName);
                    SetProductInfoCellValue(GrdPosProductDiscount, -1 * (ItemSearchResult[TheChosenIndex].DiscRate * UnitPrice / 100));
                    SetProductInfoCellValue(GrdPosProductDiscountRate, ItemSearchResult[TheChosenIndex].DiscRate);
                    SetProductInfoCellValue(GrdPosActualUnitPrice, ActualUnitPrice);
                    SetProductInfoCellValue(GrdPosCurrencyCode, ItemSearchResult[TheChosenIndex].CurCode);
                    SetProductInfoCellValue(GrdPosCurrencyRate, ItemSearchResult[TheChosenIndex].CurRate);
                    //if ((UnitPrice - Discount) < MinPrice)
                    //{
                    //    if (PosMinPriceSetting == "W")
                    //    {
                    //        Grd1.Cells[Grd1.Rows.Count + GrdPosMinPrice.Row - 2, GrdPosMinPrice.Col].ForeColor = ErrorFontColor;
                    //        Grd1.Cells[Grd1.Rows.Count + GrdPosUnitPrice.Row - 2, GrdPosUnitPrice.Col].ForeColor = ErrorFontColor;
                    //        Grd1.Cells[Grd1.Rows.Count + GrdPosProductDiscount.Row - 2, GrdPosProductDiscount.Col].ForeColor = ErrorFontColor;
                    //        Sm.StdMsg(mMsgType.Warning, "Sales Price [ " +
                    //            String.Format(GetNumFormat(GrdPosUnitPrice.RoundingFormat), UnitPrice - Discount) +
                    //            " ] below Minimum Price [ " +
                    //            String.Format(GetNumFormat(GrdPosUnitPrice.RoundingFormat), MinPrice) + " ]");
                    //    }
                    //}
                    // if discount recalculate taxamt

                    Grd1.Cells[CurrentRow, 1].BackColor = Grd1.Cells[CurrentRow, 2].BackColor;
                    Grd1.Cells[CurrentRow + 1, 1].BackColor = Grd1.Cells[CurrentRow + 1, 2].BackColor;

                    if (ItemSearchResult[TheChosenIndex].TaxLiableInd == "Y")
                    {
                        SetProductInfoCellValue(GrdPosTax1, (ItemSearchResult[TheChosenIndex].Tax1Amt) * CurrentQuantity);
                        SetProductInfoCellValue(GrdPosTaxCode1, PosTax1Code);
                        SetProductInfoCellValue(GrdPosTaxRate1, PosTax1Rate);
                        SetProductInfoCellValue(GrdPosTax2, (ItemSearchResult[TheChosenIndex].Tax2Amt) * CurrentQuantity);
                        SetProductInfoCellValue(GrdPosTaxCode2, PosTax2Code);
                        SetProductInfoCellValue(GrdPosTaxRate2, PosTax2Rate);
                        SetProductInfoCellValue(GrdPosTax3, (ItemSearchResult[TheChosenIndex].Tax3Amt) * CurrentQuantity);
                        SetProductInfoCellValue(GrdPosTaxCode3, PosTax3Code);
                        SetProductInfoCellValue(GrdPosTaxRate3, PosTax3Rate);
                    }

                    CurrentRow = Grd1.Rows.Count - 2;

                    ComputeSubTotal(CurrentRow);

                    SetSelectedRow(CurrentRow);
                    AddToAuditRollRowInfo(CurrentRow);
                }
            }
            return IndexResult;
        }        

        private void SetGridPosFormat(GrdPos MyGrdPos)
        {
            if (MyGrdPos.Row == 0)
                Grd1.Cols[MyGrdPos.Col].Text = MyGrdPos.Title;
            Grd1.Cols[MyGrdPos.Col].Width = MyGrdPos.ColWidth;
            Grd1.Cols[MyGrdPos.Col].CellStyle.Font = SecondLineFont;
            Grd1.Cols[MyGrdPos.Col].ColHdrStyle.Font = GridTitleFont;
            Grd1.Cols[MyGrdPos.Col].ColHdrStyle.TextAlign = iGContentAlignment.TopCenter;
            Grd1.Cols[MyGrdPos.Col].CellStyle.TextAlign = MyGrdPos.TextAlign;
            Grd1.Cols[MyGrdPos.Col].CellStyle.ValueType = MyGrdPos.ValueType;

            if (MyGrdPos.ValueType == typeof(decimal))
                Grd1.Cols[MyGrdPos.Col].CellStyle.FormatString = GetNumFormat(MyGrdPos.BaseDecimal);

            Grd1.Cols[MyGrdPos.Col].Visible = MyGrdPos.Visible;
                
        }

        public void ProcessData(string TheCode, string CurrencyCode)
        {
            TheCode = TheCode.Substring(QuantityPos, TheCode.Length - QuantityPos);
            if (GetProductInfo(TheCode, CurrencyCode) == true)
            {
                LblCode.Text = "";
                SalesType = "S";
            }
            ResetRow();
            IsNewLine = true;
        }
        
        private void SetSelectedRow(int Row)
        {
            if (Grd1.Rows.Count > 0)
            {
                if (Grd1.Cells[Row, 0].Value == "S")
                {
                    Grd1.Cells[Row, 1].BackColor = Color.LightBlue;
                    Grd1.Cells[Row + 1, 1].BackColor = Color.LightBlue;
                }
                else
                {
                    Grd1.Cells[Row, 1].BackColor = Color.LightSalmon;
                    Grd1.Cells[Row + 1, 1].BackColor = Color.LightSalmon;
                }
                    //Grd1.Cells[Row, 1].Grid.GridLines.Mode. .Horizontal.Color = Color.Transparent;
                //Grd1.Cells[Row + 1, 1].Grid.GridLines.Horizontal.Color = Color.Transparent;
            }
        }

        private void SetProductInfoCellValue(GrdPos MyGridPos, object Value)
        {
            Grd1.Cells[Grd1.Rows.Count + MyGridPos.Row - 2, MyGridPos.Col].Value = Value;
            Grd1.Cells[Grd1.Rows.Count + MyGridPos.Row - 2, MyGridPos.Col].Font = (MyGridPos.Row == 0 ? FirstLineFont : SecondLineFont);
        }

        private void SetProductInfoCellValueRow(GrdPos MyGridPos, object Value, int CurRow)
        {
            Grd1.Cells[CurRow + MyGridPos.Row, MyGridPos.Col].Value = Value;
            Grd1.Cells[CurRow + MyGridPos.Row, MyGridPos.Col].Font = (MyGridPos.Row == 0 ? FirstLineFont : SecondLineFont);
        }

        #endregion

        #region Save Transaction

        private void SetPosShiftLog(string BusinessDate, string ShiftNo, string UserCode, string FloatAmount)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into tblposshiftlog (BsDate, PosNo, ShiftNo, UserCode, FloatAmount) "+
                    " Values ('" + BusinessDate + "', '"+Gv.PosNo+"', '" + ShiftNo + "', '" + UserCode + "', "+FloatAmount + ")"
            };
            Sm.ExecCommand(cm);
        }

        public string GetNewNumber()
        {
            string LastNumber = Sm.GetValue("SELECT TrnNo As NumValue FROM TblPosTrnHdr " +
                " WHERE BsDate = '" + BusinessDate + "' " +
                " And PosNo='" + Gv.PosNo + "' " +
                " ORDER BY TrnNo DESC  " +
                " LIMIT 1 ");

            if (LastNumber == "")
                LastNumber = Sm.CopyStr("0", 4) + "1";
            else
            {
                LastNumber = (int.Parse(LastNumber) + 1).ToString();
                LastNumber = Sm.CopyStr("0", 5 - LastNumber.Length) + LastNumber;
            }
            LblTrnNo.Text = LastNumber;
            return LastNumber;

            //int StartPos = 0;
            //int EndPos = 0;
            //for (int Index = 0; Index < TransactionNoFormat.Length; Index++)
            //    if (TransactionNoFormat.Substring(Index, 1) == "N")
            //    {
            //        if (StartPos == 0)
            //            StartPos = Index;
            //        EndPos = Index;
            //    }
            //string CurrentDate = Sm.ServerCurrentDateTime().Substring(0, 4) +
            //    Sm.ServerCurrentDateTime().Substring(4, 2) +
            //    Sm.ServerCurrentDateTime().Substring(6, 2);

            //string FieldName = "SUBSTRING(TrnNo, " + (StartPos + 1).ToString() + " ," + ((EndPos - StartPos) + 1).ToString() + ")";
            //string LastNumber = Sm.GetValue("SELECT " + FieldName + " As NumValue FROM TblPosTrnHdr " +
            //    " WHERE LEFT(TrnDtTm, 8) = '" + CurrentDate + "' " +
            //    " ORDER BY " + FieldName + " DESC  " +
            //    " LIMIT 1 ");
            //if (LastNumber == "")
            //    LastNumber = Sm.CopyStr("0", EndPos - StartPos) + "1";
            //else
            //{
            //    LastNumber = (int.Parse(LastNumber) + 1).ToString();
            //    LastNumber = Sm.CopyStr("0", (EndPos - StartPos) + 1 - LastNumber.Length) + LastNumber;
            //}
            //LastNumber = TransactionNoFormat.Substring(0, StartPos) + LastNumber;
            //LastNumber = LastNumber.Replace("YYYY", CurrentDate.Substring(0, 4));
            //LastNumber = LastNumber.Replace("YY", CurrentDate.Substring(2, 2));
            //LastNumber = LastNumber.Replace("MM", CurrentDate.Substring(4, 2));
            //LastNumber = LastNumber.Replace("DD", CurrentDate.Substring(6, 2));
            //LastNumber = LastNumber.Replace("P", Gv.PosNo);
            //return LastNumber;
        }

        private MySqlCommand InsertDataCashOut(Decimal Amount, string Remark)
        {
            string LastDtlNo = Sm.GetValue("SELECT DtlNo FROM TblPosCashOut WHERE LEFT(CODate, 8)='" + Sm.ServerCurrentDateTime().Substring(0, 8) + "' Limit 1");
            if (LastDtlNo == "") LastDtlNo = "1";
            else LastDtlNo = (Decimal.Parse(LastDtlNo) + 1).ToString();
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPosCashOut" +
                    "(BsDate, PosNo, ShiftNo, CODate, DtlNo, COAmt, Remark, " +
                    "CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    " Values" +
                    "(@BsDate, @PosNo, @ShiftNo, @CODate, @DtlNo, @COAmt, @Remark, " +
                    "@CreateBy, CurrentDateTime(), Null, Null) "

            };
            Sm.CmParam<String>(ref cm, "@BsDate", BusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@ShiftNo", ShiftNumber);
            Sm.CmParam<String>(ref cm, "@CODate", Sm.ServerCurrentDateTime());
            Sm.CmParam<Decimal>(ref cm, "@DtlNo", Decimal.Parse(LastDtlNo));
            Sm.CmParam<Decimal>(ref cm, "@COAmt", Amount);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertDataCashIn(Decimal Amount, string Remark)
        {
            string LastDtlNo = Sm.GetValue("SELECT DtlNo FROM TblPosCashIn WHERE LEFT(CIDate, 8)='" + Sm.ServerCurrentDateTime().Substring(0, 8) + "' Limit 1");
            if (LastDtlNo == "") LastDtlNo = "1";
            else LastDtlNo = (Decimal.Parse(LastDtlNo) + 1).ToString();
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPosCashIn" +
                    "(BsDate, PosNo, ShiftNo, CIDate, DtlNo, CIAmt, Remark, " +
                    "CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    " Values" +
                    "(@BsDate, @PosNo, @ShiftNo, @CIDate, @DtlNo, @CIAmt, @Remark, " +
                    "@CreateBy, CurrentDateTime(), Null, Null) "

            };
            Sm.CmParam<String>(ref cm, "@BsDate", BusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@ShiftNo", ShiftNumber);
            Sm.CmParam<String>(ref cm, "@CIDate", Sm.ServerCurrentDateTime());
            Sm.CmParam<Decimal>(ref cm, "@DtlNo", Decimal.Parse(LastDtlNo));
            Sm.CmParam<Decimal>(ref cm, "@CIAmt", Amount);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertDataHeader(string TrnNo, decimal Change)
        {
            decimal Tax1Amt = 0;
            decimal Tax2Amt = 0;
            decimal Tax3Amt = 0;
            int LastValCount = 1;

            if (PosTax1Code != "")
            {
                LastValCount++;
                Tax1Amt = Decimal.Parse(GetValueFromList(LastValCount));
            }

            if (PosTax2Code != "")
            {
                LastValCount++;
                Tax2Amt = Decimal.Parse(GetValueFromList(LastValCount));
            }

            if (PosTax3Code != "")
            {
                LastValCount++;
                Tax3Amt = Decimal.Parse(GetValueFromList(LastValCount));
            }

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPosTrnHdr" +
                    "(TrnNo, TrnDtTm, BsDate, PosNo, ShiftNo, SlsType, UserCode, SlsCode, TDiscRt, TDiscAmt, Total, TChange, " +
                    "Tax1Code, Tax2Code, Tax3Code, Tax1Incl, Tax2Incl, Tax3Incl, Tax1Amt, Tax2Amt, Tax3Amt, " +
                    "CreateBy, CreateDt, LastUpBy, LastUpDt) " +
                    " Values" +
                    "(@TrnNo, @TrnDtTm, @BsDate, @PosNo, @ShiftNo, @SlsType, @UserCode, @SlsCode, @TDiscRt, @TDiscAmt, @Total, @TChange, " +
                    "@Tax1Code, @Tax2Code, @Tax3Code, @Tax1Incl, @Tax2Incl, @Tax3Incl, @Tax1Amt, @Tax2Amt, @Tax3Amt, " +
                    "@CreateBy, CurrentDateTime(), Null, Null) "

            };
            Sm.CmParam<String>(ref cm, "@TrnNo", TrnNo);
            Sm.CmParam<String>(ref cm, "@TrnDtTm", Sm.ServerCurrentDateTime());
            Sm.CmParam<String>(ref cm, "@BsDate", BusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<String>(ref cm, "@ShiftNo", ShiftNumber);
            Sm.CmParam<String>(ref cm, "@SlsType", (Decimal.Parse(TxtGrandTotal.Text) < 0 ? "R" : "S"));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SlsCode", "");
            Sm.CmParam<Decimal>(ref cm, "@TDiscRt", 0);
            Sm.CmParam<Decimal>(ref cm, "@TDiscAmt", TotalDiscount);
            Sm.CmParam<Decimal>(ref cm, "@Total", Decimal.Parse(TxtGrandTotal.Text));
            Sm.CmParam<Decimal>(ref cm, "@TChange", Change);
            Sm.CmParam<String>(ref cm, "@Tax1Code", PosTax1Code);
            Sm.CmParam<String>(ref cm, "@Tax2Code", PosTax2Code);
            Sm.CmParam<String>(ref cm, "@Tax3Code", PosTax3Code);
            Sm.CmParam<String>(ref cm, "@Tax1Incl", (PosTax1Inclusive ? "Y" : "N"));
            Sm.CmParam<String>(ref cm, "@Tax2Incl", (PosTax2Inclusive ? "Y" : "N"));
            Sm.CmParam<String>(ref cm, "@Tax3Incl", (PosTax3Inclusive ? "Y" : "N"));
            Sm.CmParam<Decimal>(ref cm, "@Tax1Amt", Tax1Amt);
            Sm.CmParam<Decimal>(ref cm, "@Tax2Amt", Tax2Amt);
            Sm.CmParam<Decimal>(ref cm, "@Tax3Amt", Tax3Amt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertDataDetail(string TrnNo, int Index)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPosTrnDtl" +
                    "(TrnNo, BsDate, PosNo, DtlNo, DtlType, ItCode, Barcode, Qty, UPrice, ActPrice, UWght, DiscAmt, DiscRate, " +
                    "Tax1Amt, Tax2Amt, Tax3Amt, CreateBy, CreateDt)  " +
                    " Values" +
                    "(@TrnNo, @BsDate, @PosNo, @DtlNo, @DtlType, @ItCode, @Barcode, @Qty, @UPrice, @ActPrice, @UWght, @DiscAmt, @DiscRate, " +
                    "@Tax1Amt, @Tax2Amt, @Tax3Amt, @CreateBy, CurrentDateTime());"

            };
            Sm.CmParam<String>(ref cm, "@TrnNo", TrnNo);
            Sm.CmParam<String>(ref cm, "@BsDate", BusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<Int32>(ref cm, "@DtlNo", (Index / 2) + 1);
            Sm.CmParam<String>(ref cm, "@DtlType", Sm.GetGrdStr(Grd1, Index + GrdPosSalesType.Row, GrdPosSalesType.Col));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Index + GrdPosProductCode.Row, GrdPosProductCode.Col));
            Sm.CmParam<String>(ref cm, "@Barcode", Sm.GetGrdStr(Grd1, Index + GrdPosProductCode.Row, GrdPosProductCode.Col));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Index + GrdPosQuantity.Row, GrdPosQuantity.Col));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Index + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col));
            Sm.CmParam<Decimal>(ref cm, "@ActPrice", Sm.GetGrdDec(Grd1, Index + GrdPosActualUnitPrice.Row, GrdPosActualUnitPrice.Col));
            Sm.CmParam<Decimal>(ref cm, "@UWght", Sm.GetGrdDec(Grd1, Index + GrdPosWeight.Row, GrdPosWeight.Col));
            Sm.CmParam<Decimal>(ref cm, "@DiscAmt", Sm.GetGrdDec(Grd1, Index + GrdPosProductDiscount.Row, GrdPosProductDiscount.Col));
            Sm.CmParam<Decimal>(ref cm, "@DiscRate", Sm.GetGrdDec(Grd1, Index + GrdPosProductDiscountRate.Row, GrdPosProductDiscountRate.Col));
            Sm.CmParam<Decimal>(ref cm, "@Tax1Amt", Sm.GetGrdDec(Grd1, Index + GrdPosTax1.Row, GrdPosTax1.Col));
            Sm.CmParam<Decimal>(ref cm, "@Tax2Amt", Sm.GetGrdDec(Grd1, Index + GrdPosTax2.Row, GrdPosTax2.Col));
            Sm.CmParam<Decimal>(ref cm, "@Tax3Amt", Sm.GetGrdDec(Grd1, Index + GrdPosTax3.Row, GrdPosTax3.Col));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand InsertDataPayment(string TrnNo, TenTec.Windows.iGridLib.iGrid GrdPayment, int Index)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPosTrnPay(TrnNo, BsDate, PosNo, DtlNo, PayTpNo, PayAmt, PayAmtNett, Charge, CardNo, CreateBy, CreateDt) " +
                    "Values(@TrnNo, @BsDate, @PosNo, @DtlNo, @PayTpNo, @PayAmt, @PayAmtNett, @Charge, @CardNo, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@TrnNo", TrnNo);
            Sm.CmParam<String>(ref cm, "@BsDate", BusinessDate);
            Sm.CmParam<String>(ref cm, "@PosNo", Gv.PosNo);
            Sm.CmParam<Int32>(ref cm, "@DtlNo", Index + 1);
            Sm.CmParam<String>(ref cm, "@PayTpNo", Sm.GetGrdStr(GrdPayment, Index, 5));
            Sm.CmParam<Decimal>(ref cm, "@PayAmt", Sm.GetGrdDec(GrdPayment, Index, 1));
            Sm.CmParam<Decimal>(ref cm, "@PayAmtNett", Sm.GetGrdDec(GrdPayment, Index, 8));
            Sm.CmParam<Decimal>(ref cm, "@Charge", Sm.GetGrdDec(GrdPayment, Index, 2));
            Sm.CmParam<String>(ref cm, "@CardNo", Sm.GetGrdStr(GrdPayment, Index, 4));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void SaveToTable(string TrnNo, TenTec.Windows.iGridLib.iGrid GrdPayment, decimal Change)
        {
            var cml = new List<MySqlCommand>();
            cml.Add(InsertDataHeader(TrnNo, Change));

            for (int Index = 0; Index < Grd1.Rows.Count; Index++)
            {
                if ((Index % 2) == 0)
                    cml.Add(InsertDataDetail(TrnNo, Index));
            }

            for (int Index = 0; Index < GrdPayment.Rows.Count; Index++)
            {
                cml.Add(InsertDataPayment(TrnNo, GrdPayment, Index));
            }

            Sm.ExecCommands(cml);

        }

        #endregion
        
        #region Function Key

        private bool ProcessFunctionKey(string FunctionKey)
        {
                string FuncCode = "";
                string FuncName = "";
                string FuncPassword = "";

                FuncCode = Sm.GetValue("SELECT FuncCode FROM TblPosFunction WHERE FuncKey='" + FunctionKey + "'");
                FuncName = Sm.GetValue("SELECT FuncName FROM TblPosFunction WHERE FuncKey='" + FunctionKey + "'");
                FuncPassword = Sm.GetValue("SELECT Password FROM TblPosFunction WHERE FuncKey='" + FunctionKey + "'");

                if (FuncCode == "")
                    return false;
                else
                {
                    AddToAuditRoll("Function " + FuncName + " Pressed. ");
                    bool FuncExecute = true;
                    if (FuncPassword != "")
                    {
                        var frmPassword = new FrmPosPassword(FuncPassword, FuncName);
                        if (frmPassword.ShowDialog(this) == DialogResult.OK)
                            FuncExecute = true;
                        else
                        {
                            AddToAuditRoll("Password Form Cancelled. Function " + FuncName + " Cancelled.");
                            FuncExecute = false;
                        }
                    }
                    if (FuncExecute == true)
                    {
                        switch (FuncCode)
                        {
                            case "00001": FunctionHelpKey();
                                break;
                            case "00002": FunctionOpenPrice(FuncName);
                                break;
                            case "00003": FunctionItemVoid();
                                break;
                            case "00004": FunctionCashOut(FuncName);
                                break;
                            case "00005": FunctionOpenItem(FuncName);
                                break;
                            case "00006": FunctionCashIn(FuncName);
                                break;
                            case "00007": FunctionFindProduct(FuncName);
                                break;
                            case "00008": FunctionDiscItemAmount(FuncName);
                                break;                            
                            case "00010": FunctionPayment(FuncName);
                                break;
                            case "00011": FunctionTotalDiscount(FuncName);
                                break;
                            case "00012": FunctionSalesReturnToggle();
                                break;
                            case "00013": FunctionSalesReturnLinked(FuncName);
                                break;
                            case "00014": FunctionPrintLastReceipt(FuncName);
                                break;
                        }
                    }
                    return true;
                }
        }

        private void FunctionHelpKey()
        {
        }

        private void FunctionFindProduct(string FuncName)
        {
            var frmFindProduct = new FrmPosFindProduct(this, QuantityPos);
            if (frmFindProduct.ShowDialog(this) == DialogResult.OK)
            {
            }
            else
                AddToAuditRoll("Function " + FuncName + " Cancelled.");
        }

        private void FunctionOpenPrice(string FuncName)
        {
            try
            {
                if (Grd1.Rows.Count > 0)
                {
                    var frmPopUp = new FrmPosPopUp("Open Price", "Price", GetNumFormat(GrdPosUnitPrice.BaseDecimal), true, true, "");
                    if (frmPopUp.ShowDialog(this) == DialogResult.OK)
                    {
                        Grd1.Cells[CurrentRow + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col].Value = Decimal.Parse(frmPopUp.TxtAmount.Text);
                        ComputeSubTotal(CurrentRow);

                        AddToAuditRollRowInfo(CurrentRow);
                    }
                    else
                        AddToAuditRoll("Function " + FuncName + " Cancelled.");
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void FunctionOpenItem(string FuncName)
        {

            var frmPopUp = new FrmPosPopUp("Open Item", "Weight", GetNumFormat(GrdPosWeight.BaseDecimal), true, true, "Product Desc");
                if (frmPopUp.ShowDialog(this) == DialogResult.OK)
                {
                    Grd1.Rows.Count = Grd1.Rows.Count + 2;
                    if (SalesType == "R")
                    {
                        Grd1.Rows[Grd1.Rows.Count - 2].Grid.GridLines.Horizontal.Color = Color.Black;
                        Grd1.Rows[Grd1.Rows.Count - 2].BackColor = Color.LightPink;
                        Grd1.Rows[Grd1.Rows.Count - 1].BackColor = Color.LightPink;
                    }
                    else
                    {
                        Grd1.Rows[Grd1.Rows.Count - 2].Grid.GridLines.Horizontal.Color = Color.White;
                        Grd1.Rows[Grd1.Rows.Count - 2].BackColor = Color.White;
                        Grd1.Rows[Grd1.Rows.Count - 1].BackColor = Color.White;
                    }
                    SetProductInfoCellValue(GrdPosSalesType, SalesType);
                    SetProductInfoCellValue(GrdPosProductCode, "OPEN");
                    SetProductInfoCellValue(GrdPosUnitPrice, 0);
                    SetProductInfoCellValue(GrdPosQuantity, 1);
                    SetProductInfoCellValue(GrdPosWeight, Decimal.Parse(frmPopUp.TxtAmount.Text));
                    SetProductInfoCellValue(GrdPosProductName, frmPopUp.rTxtRemark.Text);
                    SetProductInfoCellValue(GrdPosProductDiscount, 0);
                    SetProductInfoCellValue(GrdPosProductDiscountRate, 0);
                    SetProductInfoCellValue(GrdPosActualUnitPrice, 0);
                    SetProductInfoCellValue(GrdPosCurrencyCode, LocalCurrency);
                    SetProductInfoCellValue(GrdPosCurrencyRate, 1);
                    SetProductInfoCellValue(GrdPosTax1, 0);
                    SetProductInfoCellValue(GrdPosTaxCode1, "");
                    SetProductInfoCellValue(GrdPosTax2, 0);
                    SetProductInfoCellValue(GrdPosTaxCode2, "");
                    SetProductInfoCellValue(GrdPosTax3, 0);
                    SetProductInfoCellValue(GrdPosTaxCode3, "");

                    CurrentRow = Grd1.Rows.Count - 2;

                    ComputeSubTotal(CurrentRow);

                    SetSelectedRow(CurrentRow);
                    AddToAuditRollRowInfo(CurrentRow);
                }
                else
                    AddToAuditRoll("Function " + FuncName + " Cancelled.");
        }

        private void FunctionCashOut(string FuncName)
        {
            try
            {
                var frmPopUp = new FrmPosPopUp("Cash Out", "Amount", GetNumFormat(GrdPosSubTotal.BaseDecimal), true, true, "Reason");
                if (frmPopUp.ShowDialog(this) == DialogResult.OK)
                {
                    AddToAuditRoll("Cash Out Amount : " + frmPopUp.TxtAmount.Text);
                    AddToAuditRoll("Cash Out Reason : " + frmPopUp.rTxtRemark.Text);

                    var cml = new List<MySqlCommand>();

                    cml.Add(InsertDataCashOut(Decimal.Parse(frmPopUp.TxtAmount.Text), frmPopUp.rTxtRemark.Text));

                    Sm.ExecCommands(cml);
                }
                else
                    AddToAuditRoll("Function " + FuncName + " Cancelled.");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void FunctionCashIn(string FuncName)
        {
            try
            {
                var frmPopUp = new FrmPosPopUp("Cash In", "Amount", GetNumFormat(GrdPosSubTotal.BaseDecimal), true, true, "Remark");
                if (frmPopUp.ShowDialog(this) == DialogResult.OK)
                {
                    AddToAuditRoll("Cash In Amount : " + frmPopUp.TxtAmount.Text);
                    AddToAuditRoll("Cash In Remark : " + frmPopUp.rTxtRemark.Text);

                    var cml = new List<MySqlCommand>();

                    cml.Add(InsertDataCashIn(Decimal.Parse(frmPopUp.TxtAmount.Text), frmPopUp.rTxtRemark.Text));

                    Sm.ExecCommands(cml);
                }
                else
                    AddToAuditRoll("Function " + FuncName + " Cancelled.");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void FunctionPrintLastReceipt(string FuncName)
        {
            try
            {
                if (CurrentPrintNumber == "") 
                    CurrentPrintNumber = Sm.GetValue("Select TrnNo from tblPosTrnHdr where PosNo='"+Gv.PosNo+"' And BsDate ='" + BusinessDate + "' Order By TrnNo Desc Limit 1");
                AddToAuditRoll("Print Last Receipt : " + CurrentPrintNumber.ToString());
                Reprint = true;
                PrintReceipt();

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void FunctionDiscItemAmount(string FuncName)
        {
            try
            {
                if (Grd1.Rows.Count > 0)
                {
                    decimal Tax1Amt = 0;
                    decimal Tax2Amt = 0;
                    decimal Tax3Amt = 0;

                    var frmPopUp = new FrmPosPopUp("Discount Item", "Amount", GetNumFormat(GrdPosProductDiscount.BaseDecimal), true, true, "");
                    if (frmPopUp.ShowDialog(this) == DialogResult.OK)
                    {
                        if (decimal.Parse(frmPopUp.TxtAmount.Text) > Sm.GetGrdDec(Grd1, CurrentRow + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col))
                        {
                            AddToAuditRoll("Discount is more than the unit price.");
                            Sm.StdMsg(mMsgType.Warning, "Discount is more than the unit price.");
                        }
                        else
                        {
                            AddToAuditRoll("Discount Item Amount : " + frmPopUp.TxtAmount.Text);

                            SetProductInfoCellValueRow(GrdPosProductDiscount, -1 * decimal.Parse(frmPopUp.TxtAmount.Text), CurrentRow);
                            SetProductInfoCellValueRow(GrdPosProductDiscountRate, 0, CurrentRow);

                            Tax1Amt = CalcTaxAmt(PosTax1Inclusive, true,
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col) +
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosProductDiscount.Row, GrdPosProductDiscount.Col), PosTax1Rate) *
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosQuantity.Row, GrdPosQuantity.Col);

                            Tax2Amt = CalcTaxAmt(PosTax2Inclusive, true,
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col) +
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosProductDiscount.Row, GrdPosProductDiscount.Col), PosTax2Rate) *
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosQuantity.Row, GrdPosQuantity.Col);

                            Tax3Amt = CalcTaxAmt(PosTax3Inclusive, true,
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosUnitPrice.Row, GrdPosUnitPrice.Col) +
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosProductDiscount.Row, GrdPosProductDiscount.Col), PosTax3Rate) *
                                Sm.GetGrdDec(Grd1, CurrentRow + GrdPosQuantity.Row, GrdPosQuantity.Col);

                            SetProductInfoCellValueRow(GrdPosTax1, Tax1Amt, CurrentRow);
                            SetProductInfoCellValueRow(GrdPosTax2, Tax2Amt, CurrentRow);
                            SetProductInfoCellValueRow(GrdPosTax3, Tax3Amt, CurrentRow);

                            ComputeSubTotal(CurrentRow);
                        }
                    }
                    else
                        AddToAuditRoll("Function " + FuncName + " Cancelled.");
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void FunctionPayment(string FuncName)
        {
            try
            {
                if (Grd1.Rows.Count > 0)
                {
                    AddToAuditRoll("Function " + FuncName + " Pressed.");
                    var frmPayment = new FrmPosPayment(decimal.Parse(TxtGrandTotal.Text), GetNumFormat(GrdPosSubTotal.BaseDecimal));
                    if (frmPayment.ShowDialog(this) == DialogResult.OK)
                    {
                        if (decimal.Parse(frmPayment.TxtOutstanding.Text) <= 0)
                        {
                            // Save Transaction
                            string NewTrnNo = GetNewNumber();
                            SaveToTable(NewTrnNo, frmPayment.Grd1, -1 * decimal.Parse(frmPayment.TxtOutstanding.Text));
                            PaymentChange = decimal.Parse(frmPayment.TxtOutstanding.Text);
                            if (decimal.Parse(frmPayment.TxtOutstanding.Text) < 0)
                                LblCode.Text = "Change " + String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), -1 * PaymentChange);
                            CurrentPrintNumber = NewTrnNo;
                            Reprint = false;
                            PrintReceipt();
                            NewTransaction();
                        }
                        else
                            AddToAuditRoll("Payment Cancelled. Outstanding Payment " + frmPayment.TxtOutstanding.Text);
                        //AddToAuditRoll("Cash Out Amount : " + frmPopUp.TxtAmount.Text);
                        //AddToAuditRoll("Cash Out Reason : " + frmPopUp.rTxtRemark.Text);
                    }
                    else
                        AddToAuditRoll("Function " + FuncName + " Cancelled.");
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void FunctionTotalDiscount(string FuncName)
        {

            if (Grd1.Rows.Count > 0)
            {
                var frmPopUp = new FrmPosPopUp("Total Discount", "Discount", GetNumFormat(GrdPosProductDiscount.BaseDecimal), true, true, "");
                if (frmPopUp.ShowDialog(this) == DialogResult.OK)
                {
                    if (Decimal.Parse(frmPopUp.TxtAmount.Text) > Decimal.Parse(GetValueFromList(0)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Total Discount is bigger than Sub Total");
                        AddToAuditRoll("Total Discount : " + frmPopUp.TxtAmount.Text + " too big. Function " + FuncName + " Cancelled.");
                    }
                    else
                    {
                        TotalDiscount = Decimal.Parse(frmPopUp.TxtAmount.Text);

                        ComputeSubTotal(CurrentRow);
                        AddToAuditRoll("Total Discount : " + frmPopUp.TxtAmount.Text);
                    }

                }
                else
                    AddToAuditRoll("Function " + FuncName + " Cancelled.");

            }
        }

        private void FunctionItemVoid()
        {
            if (Grd1.Rows.Count > 0)
            {
                AddToAuditRoll("----- VOID Product -----");
                AddToAuditRollRowInfo(CurrentRow);
                AddToAuditRoll("------------------------");
                Grd1.Rows.RemoveAt(CurrentRow + 1);
                Grd1.Rows.RemoveAt(CurrentRow);
                if (Grd1.Rows.Count == 0) CurrentRow = 0;
                else
                    // if Current Row is the last Row then 
                    if (CurrentRow == Grd1.Rows.Count)
                        CurrentRow = CurrentRow - 2;
                SetSelectedRow(CurrentRow);
                
                //reset Total Discount
                if (TotalDiscount != 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "WARNING !! " + Environment.NewLine + Environment.NewLine +
                        "Item Void will reset Total Discount.");
                    AddToAuditRoll("Reset Total Discount " + TotalDiscount.ToString());
                    TotalDiscount = 0;
                }
                ComputeGrandTotal();
            }
        }

        private void FunctionSalesReturnToggle()
        {
            string Msg = "Function Sales Return Toggle Pressed. ";

            if (SalesType == "S")
            {
                SalesType = "R";
                Msg = Msg + "Change Type to Return Mode ";
            }
            else
            {
                SalesType = "S";
                Msg = Msg + "Change Type to Sales Mode ";
            }
            AddToAuditRoll(Msg);
        }

        private void FunctionSalesReturnLinked(string FuncName)
        {
            try
            {
                AddToAuditRoll("Function " + FuncName + " Pressed.");
                var frmSalesReturn = new FrmPosSalesReturn(this);
                if (frmSalesReturn.ShowDialog(this) == DialogResult.OK)
                {
                }
                else
                    AddToAuditRoll("Function " + FuncName + " Cancelled.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Event

        private void FrmTrnPOS_Load(object sender, EventArgs e)
        {
            //try
            //{
                bool AllowToLoad = true;

                LbTotalTitle.Items.Clear();
                LbTotalValue.Items.Clear();
            
                ClearData();

                mIsPOSQtyEditable = Sm.GetParameter("IsPOSQtyEditable")=="Y";
                mIsPOSReceiptShowDiscount = Sm.GetParameter("IsPOSReceiptShowDiscount") == "Y";
                string CurrentBusinesDate = Sm.GetPosSetting("CurrentBnsDate");
                string CurrentDateTime = Sm.ServerCurrentDateTime().Substring(0, 8);
                if (CurrentBusinesDate == "") CurrentBusinesDate = CurrentDateTime;
                string CurrentShiftStatus = Sm.GetPosSetting("StsShift");
                string CurrentShiftLog = Sm.GetPosSetting("CurrentShift");
                string CurrentUserLog = Sm.GetPosSetting("CurrentUser");
                
                if (CurrentBusinesDate != CurrentDateTime)
                {
                    if (Sm.StdMsgYN("Question", "WARNING !! " + Environment.NewLine + Environment.NewLine + 
                        "Business Date is not the same with Current System Date. " + Environment.NewLine +
                        "Current Business Date is " + CurrentBusinesDate.Substring(6, 2) + "-" +
                        CurrentBusinesDate.Substring(4, 2) + "-" + CurrentBusinesDate.Substring(0, 4) + Environment.NewLine + Environment.NewLine +
                        "Please perform Shift Closing and Day End Closing. " + Environment.NewLine + Environment.NewLine +
                        "(Yes - Perform Shift Closing and Day End Closing) " + Environment.NewLine +
                        "(No - Continue running Business Date on " + CurrentBusinesDate.Substring(6, 2) + "-" +
                        CurrentBusinesDate.Substring(4, 2) + "-" + CurrentBusinesDate.Substring(0, 4) + ")") == DialogResult.Yes)
                    {
                        AllowToLoad = false;
                    }
                }

                if ((CurrentShiftStatus == "O") && (CurrentUserLog != "") && (CurrentUserLog != Gv.CurrentUserCode))
                {
                    Sm.StdMsg(mMsgType.Warning, "WARNING !! " + Environment.NewLine + Environment.NewLine +
                        "Unable to open POS Application." + Environment.NewLine +
                        "Current Shift " + CurrentShiftLog + ", logged by " + CurrentUserLog + " is still Open. " + Environment.NewLine +
                        "Please Perform Shift Closing ");
                    AllowToLoad = false;
                }

                if (AllowToLoad && (CurrentShiftStatus == "C"))
                {
                    if (Sm.StdMsgYN("Question", 
                        "Open New Shift " + Environment.NewLine + Environment.NewLine +
                        "Current Business Date is " + CurrentBusinesDate.Substring(6, 2) + "-" +
                        CurrentBusinesDate.Substring(4, 2) + "-" + CurrentBusinesDate.Substring(0, 4) + Environment.NewLine + Environment.NewLine +
                        "Do you want to Open Shift " + (Sm.GetDecValue(CurrentShiftLog) + 1).ToString() + " ?") == DialogResult.No)
                        AllowToLoad = false;

                }

                if (AllowToLoad)
                {
                    GetInitialData();
                    SetGrd();
                    //Grd1.ReadOnly = false;
                    Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6  });
                    AddToAuditRoll("Loading Pos " + Gv.PosNo);
                    AddToAuditRoll("Cashier:" + Sm.GetValue("SELECT UserName FROM TblUser WHERE UserCode='" + Gv.CurrentUserCode + "'") + " (" + Gv.CurrentUserCode + ")");
                    AddToAuditRoll("Shift:" + ShiftNumber);

                    NewTransaction();

                    LblDate.Text = CurrentBusinesDate.Substring(6, 2) + "/" +
                            Sm.GetMonthName(Int32.Parse(CurrentBusinesDate.Substring(4, 2))) + "/" + CurrentBusinesDate.Substring(0, 4);

                    GetNewNumber();

                    this.WindowState = FormWindowState.Maximized;
                }
                else
                    this.BeginInvoke(new MethodInvoker(this.Close));
            //}
            //catch (Exception Exc)
            //{
            //    Sm.StdMsg(mMsgType.Warning, Exc.Message);
            //}
        }

        private void FrmTrnPOS_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch ((int)e.KeyChar)
            {
                case 42: //*
                    try
                    {
                        CurrentQuantity = decimal.Parse(LblCode.Text);
                    }
                    catch
                    {
                        CurrentQuantity = 1;
                    }
                    LblCode.Text = LblCode.Text + " X ";
                    QuantityPos = LblCode.Text.Length;
                    break;
                //case 43: //+
                case 45: //-
                case 46: //.
                case 48: //0
                case 49: //1
                case 50: //2
                case 51: //3
                case 52: //4
                case 53: //5
                case 54: //6
                case 55: //7
                case 56: //8
                case 57: //9
                    if (IsNewLine == true)
                        LblCode.Text = "";
                    LblCode.Text = LblCode.Text + e.KeyChar;
                    IsNewLine = false;
                    break;
            }

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (ProcessFunctionKey(keyData.ToString()) == false)
            {
                switch (keyData)
                {
                    case Keys.Back: // Backspace
                        if (IsNewLine == true)
                            LblCode.Text = "";
                        if (LblCode.Text.Length > 0)
                        {
                            while (LblCode.Text.Substring(LblCode.Text.Length - 1, 1) == " ")
                                LblCode.Text = LblCode.Text.Substring(0, LblCode.Text.Length - 1);

                            LblCode.Text = LblCode.Text.Substring(0, LblCode.Text.Length - 1);
                            
                            if (LblCode.Text.Length > 0)
                                while (LblCode.Text.Substring(LblCode.Text.Length - 1, 1) == " ")
                                    LblCode.Text = LblCode.Text.Substring(0, LblCode.Text.Length - 1);
                        }
                        break;

                    case Keys.Enter: // Enter Key
                        ProcessData(LblCode.Text, "");
                        break;

                    case Keys.Escape: // Escape Key
                        LblCode.Text = "";
                        ResetRow();
                        break;

                    case Keys.Down:
                        if ((CurrentRow + 2) < Grd1.Rows.Count)
                        {
                            Grd1.Cells[CurrentRow, 1].BackColor = Grd1.Cells[CurrentRow, 2].BackColor;
                            Grd1.Cells[CurrentRow + 1, 1].BackColor = Grd1.Cells[CurrentRow + 1, 2].BackColor;

                            CurrentRow = CurrentRow + 2;

                            SetSelectedRow(CurrentRow);
                        }
                        break;
                    case Keys.Up:
                        if ((CurrentRow - 2) >= 0)
                        {
                            Grd1.Cells[CurrentRow, 1].BackColor = Grd1.Cells[CurrentRow, 2].BackColor;
                            Grd1.Cells[CurrentRow + 1, 1].BackColor = Grd1.Cells[CurrentRow + 1, 2].BackColor;

                            CurrentRow = CurrentRow - 2;

                            SetSelectedRow(CurrentRow);
                        }
                        break;
                    case Keys.A:
                    case Keys.B:
                    case Keys.C:
                    case Keys.D:
                    case Keys.E:
                    case Keys.F:
                    case Keys.G:
                    case Keys.H:
                    case Keys.I:
                    case Keys.J:
                    case Keys.K:
                    case Keys.L:
                    case Keys.M:
                    case Keys.N:
                    case Keys.O:
                    case Keys.P:
                    case Keys.Q:
                    case Keys.R:
                    case Keys.S:
                    case Keys.T:
                    case Keys.U:
                    case Keys.V:
                    case Keys.W:
                    case Keys.X:
                    case Keys.Y:
                    case Keys.Z:

                        if (IsNewLine == true)
                            LblCode.Text = "";

                        LblCode.Text = LblCode.Text + keyData;
                        IsNewLine = false;
                        break;
                }

            }            

            //return true;    // indicate that you handled this keystroke

            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void Grd1_CellClick(object sender, iGCellClickEventArgs e)
        {
            if (Grd1.Rows.Count == 0) CurrentRow = 0;
            else
            {

                Grd1.Cells[CurrentRow, 1].BackColor = Grd1.Cells[CurrentRow, 2].BackColor;
                Grd1.Cells[CurrentRow + 1, 1].BackColor = Grd1.Cells[CurrentRow + 1, 2].BackColor;

                if ((e.RowIndex % 2) == 0) CurrentRow = e.RowIndex;
                if ((e.RowIndex % 2) == 1) CurrentRow = e.RowIndex - 1;

                SetSelectedRow(CurrentRow);
            }

        }

        public void PrintReceipt()
        {
            try
            {
                PrintDialog pd = new PrintDialog();
                pdoc = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();
                Font font = new Font("Courier New", 15);


                PaperSize psize = new PaperSize("Custom", 100, 200);
                //ps.DefaultPageSettings.PaperSize = psize;



                pd.Document = pdoc;
                pd.Document.DefaultPageSettings.PaperSize = psize;
                //pdoc.DefaultPageSettings.PaperSize.Height =320;
                pdoc.DefaultPageSettings.PaperSize.Height = 820;

                pdoc.DefaultPageSettings.PaperSize.Width = 520;

                pdoc.PrintPage += new PrintPageEventHandler(pdoc_PrintReceiptPage);

                pdoc.PrinterSettings.PrinterName = PrinterName;
                pdoc.Print();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
      
        void pdoc_PrintReceiptPage(object sender, PrintPageEventArgs e)
        {
            AddToAuditRoll("Printing Receipt...");
           
            Graphics graphics = e.Graphics;

            graphics.DrawImage(
                    Image.FromFile(@Sm.CompanyLogo()),
                     new Rectangle() { Height = 35, Width = 120, X = 70, Y = 10 }
                    );

            Font font = new Font("Courier New", 10);
            float fontHeight = font.GetHeight();
            int startX = 3;
            int startY = 0;
            int Offset = 50;
            bool PrintHeader = true;
            decimal SubTotal = 0;

            decimal TAmount = 0;
            decimal TChange = 0;
            decimal TDisc = 0;

            decimal TTax1Amt = 0;
            decimal TTax2Amt = 0;
            decimal TTax3Amt = 0;

            string Tax1Name = "";
            string Tax2Name = "";
            string Tax3Name = "";

            // read from database
            try
            {
                var cm = new MySqlCommand();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText =
                        " Select A.TrnNo, A.BsDate, A.TrnDtTm, A.PosNo, A.ShiftNo, A.UserCode, " +
                        " A.TDiscRt, A.TDiscAmt, A.Total, A.TChange, A.Tax1Incl, A.Tax2Incl, A.Tax3Incl, " +
                        " A.Tax1Amt, A.Tax2Amt, A.Tax3Amt, D.TaxName As Tax1Name, E.TaxName As Tax2Name, F.TaxName As Tax3Name, " +
                        " B.DtlType, C.ItName, B.Qty, B.UPrice, B.DiscAmt, B.DiscRate " +
                        " from tblPosTrnHdr A " +
                        " Inner Join tblPosTrnDtl B On A.TrnNo=B.TrnNo And A.BsDate=B.BsDate And A.PosNo=B.PosNo " +
                        " Left Join tblitem C On B.ItCode=C.ItCode "+
                        " Left join tblTax D On A.Tax1Code=D.TaxCode "+
                        " Left join tblTax E On A.Tax2Code=E.TaxCode "+
                        " Left join tblTax F On A.Tax3Code=F.TaxCode "+
                        " Where A.TrnNo='" + CurrentPrintNumber + "' And A.BsDate='" + BusinessDate +"' And A.PosNo='"+Gv.PosNo+"' "; 
                    var dr = cm.ExecuteReader();
                    var c = new int[]
                    {
                        //0
                        dr.GetOrdinal("TrnNo"),

                        //1-5
                        dr.GetOrdinal("TrnDtTm"),
                        dr.GetOrdinal("PosNo"),
                        dr.GetOrdinal("ShiftNo"),
                        dr.GetOrdinal("UserCode"),                    
                        dr.GetOrdinal("TDiscRt"),
                        //6-10
                        dr.GetOrdinal("TDiscAmt"),                    
                        dr.GetOrdinal("Total"),                    
                        dr.GetOrdinal("TChange"),                    
                        dr.GetOrdinal("Tax1Amt"),                    
                        dr.GetOrdinal("Tax2Amt"), 
                        //11-15
                        dr.GetOrdinal("Tax3Amt"),                    
                        dr.GetOrdinal("DtlType"),                    
                        dr.GetOrdinal("ItName"),                    
                        dr.GetOrdinal("Qty"),                    
                        dr.GetOrdinal("UPrice"), 

                        //16-20                                       
                        dr.GetOrdinal("DiscAmt"),                                                                
                        dr.GetOrdinal("DiscRate"),                                                                
                        dr.GetOrdinal("Tax1Incl"),                                                                
                        dr.GetOrdinal("Tax2Incl"),                                                                
                        dr.GetOrdinal("Tax3Incl"),  
                        
                        //21                                      
                        dr.GetOrdinal("Tax1Name"),                                                                
                        dr.GetOrdinal("Tax2Name"),                                                                
                        dr.GetOrdinal("Tax3Name"),                                                                
                        dr.GetOrdinal("BsDate"),
                    };

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            if (PrintHeader)
                            {
                                string HeaderStr = Sm.GetPosSetting("Header1");
                                if (HeaderStr != "")
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);

                                HeaderStr = Sm.GetPosSetting("Header2");
                                if (HeaderStr != "")
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);

                                HeaderStr = Sm.GetPosSetting("Header3");
                                if (HeaderStr != "")
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);

                                HeaderStr = Sm.GetPosSetting("Header4");
                                if (HeaderStr != "")
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);

                                HeaderStr = Sm.GetPosSetting("Header5");
                                if (HeaderStr != "")
                                    PrintLine(graphics, Sm.PadCenter(HeaderStr, Gv.MaxCharLinePrint + 5), startX, startY, 10, 7, ref Offset);

                                PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 10, 8, ref Offset);

                                string TrnDateTime = Sm.DrStr(dr, c[1]);
                                if (TrnDateTime.Length == 12)
                                    TrnDateTime = 
                                        TrnDateTime.Substring(6, 2) + "." +
                                        TrnDateTime.Substring(4, 2) + "." +
                                        TrnDateTime.Substring(0, 4) + " " +
                                        TrnDateTime.Substring(8, 2) + ":" +
                                        TrnDateTime.Substring(10, 2);

                                string HeaderLine = TrnDateTime + " P" + Sm.DrStr(dr, c[2]) + "-" + Sm.DrStr(dr, c[3]) + " " + Sm.DrStr(dr, c[4]);

                                PrintLine(graphics, Sm.PadLeft(HeaderLine, Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                                PrintLine(graphics, Sm.PadLeft("T." + CurrentPrintNumber + "-" + Sm.DrStr(dr, c[24]), Gv.MaxCharLinePrint), startX, startY, 10, 8, ref Offset);
                                PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                                if (Reprint)
                                    PrintLine(graphics, "REPRINT - Receipt Copy", startX, startY, 15, 8, ref Offset);
                                TAmount = Sm.DrDec(dr, c[7]);
                                TChange = Sm.DrDec(dr, c[8]);
                                TDisc = Sm.DrDec(dr, c[6]);

                                TTax1Amt = Sm.DrDec(dr, c[9]);
                                TTax2Amt = Sm.DrDec(dr, c[10]);
                                TTax3Amt = Sm.DrDec(dr, c[11]);

                                Tax1Name = Sm.DrStr(dr, c[21]);
                                Tax2Name = Sm.DrStr(dr, c[22]);
                                Tax3Name = Sm.DrStr(dr, c[23]);

                                if (Sm.DrStr(dr, c[18]) == "Y") Tax1Name += " (Incl)";
                                if (Sm.DrStr(dr, c[19]) == "Y") Tax2Name += " (Incl)";
                                if (Sm.DrStr(dr, c[20]) == "Y") Tax3Name += " (Incl)";

                                PrintHeader = false;
                            }

                            //TCL
                            string ReturnTag = "";
                            if (Sm.DrStr(dr, c[12]) == "R") ReturnTag = "(Return) ";
                            string Line1 = ReturnTag + String.Format(GetNumFormat(GrdPosQuantity.BaseDecimal), Sm.DrDec(dr, c[14])) + " x " + Sm.DrStr(dr, c[13]);
                            //string Line1 = Sm.PadRight(String.Format(GetNumFormat(GrdPosQuantity.BaseDecimal), Sm.DrDec(dr, c[14])), 3) +
                            //    " x " + Sm.PadLeft(Sm.DrStr(dr, c[13]), Gv.MaxCharLinePrint - 6);


                            var rows = Sm.WordWrap(Line1, Gv.MaxCharLinePrint);
                            for (int intX=0; intX<rows.Count; intX++)
                            {
                                PrintLine(graphics, Sm.PadLeft(rows[intX], Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                            }

                            string Line2 = string.Empty;

                            if (mIsPOSReceiptShowDiscount)
                            {
                                //MSI
                                Line2 =
                                Sm.PadRight("@ " + String.Format(GetNumFormat(GrdPosUnitPrice.BaseDecimal), Sm.DrDec(dr, c[15])), 12) +
                                ((Sm.DrDec(dr, c[16]) == 0) ? Sm.CopyStr(" ", 10) :
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), Sm.DrDec(dr, c[16])), 10)) +
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14])), 12);
                            }
                            else
                            {
                                //IOK
                                Line2 =
                                Sm.PadRight("@ " + String.Format(GetNumFormat(GrdPosUnitPrice.BaseDecimal), Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])), 20) +
                                    //((Sm.DrDec(dr, c[16]) == 0) ? Sm.CopyStr(" ", 10) :
                                    //Sm.PadRight(String.Format(GetNumFormat(GrdPosProductDiscount.BaseDecimal), Sm.DrDec(dr, c[16])), 10)) +
                                Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14])), 12);
                            }

                            SubTotal += (Sm.DrDec(dr, c[15]) + Sm.DrDec(dr, c[16])) * Sm.DrDec(dr, c[14]);
                            PrintLine(graphics, Line2, startX, startY, 20, 8, ref Offset);
                        }

                        PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);

                        string SubTotalLine = Sm.PadRight("Sub Total ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), SubTotal), 12);
                        PrintLine(graphics, SubTotalLine, startX, startY, 15, 8, ref Offset);

                        if (TDisc != 0)
                        {
                            string DiscountLine = Sm.PadRight("Total Discount ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TDisc), 12);
                            PrintLine(graphics, DiscountLine, startX, startY, 15, 8, ref Offset);
                        }

                        if (Tax1Name != "")
                        {
                            string Tax1Line = Sm.PadRight(Tax1Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax1Amt), 12);
                            PrintLine(graphics, Tax1Line, startX, startY, 15, 8, ref Offset);
                        }
                        if (Tax2Name != "")
                        {
                            string Tax2Line = Sm.PadRight(Tax2Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax2Amt), 12);
                            PrintLine(graphics, Tax2Line, startX, startY, 15, 8, ref Offset);
                        }
                        if (Tax3Name != "")
                        {
                            string Tax3Line = Sm.PadRight(Tax3Name, Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TTax3Amt), 12);
                            PrintLine(graphics, Tax3Line, startX, startY, 15, 8, ref Offset);
                        }

                        PrintLine(graphics, Sm.CopyStr("-", Gv.MaxCharLinePrint), startX, startY, 15, 8, ref Offset);
                        string TotalLine = Sm.PadRight("Total ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TAmount), 12);
                        PrintLine(graphics, TotalLine, startX, startY, 15, 8, ref Offset);
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "Transaction Not Found !!");
                    }

                    dr.Close();
                    var cm2 = new MySqlCommand();
                    cm2.Connection = cn;

                    //TCL
                    cm2.CommandText =
                        " Select Case When C.SlsType='R' then concat(B.PayTpNm, ' Return') else B.PayTpNm end As PayTpNm, B.Charge, A.PayAmt, A.Charge As ChargeAmt, " +
                        " A.CardNo from tblpostrnpay A "+
                        " Inner join tblpospaybytype B on A.PayTpNo=B.PayTpNo "+
                        " Inner join tblpostrnhdr C on A.TrnNo=C.TrnNo And A.BsDate=C.BsDate And A.PosNo=C.PosNo "+
                        " Where A.TrnNo='" + CurrentPrintNumber + "' And A.BsDate='" + BusinessDate + "' And A.PosNo='" + Gv.PosNo + "' "+
                        " Order by A.DtlNo;";
                    var dr2 = cm2.ExecuteReader();
                    var c2 = new int[]
                    {
                        //0
                        dr2.GetOrdinal("PayTpNm"),

                        //1-5
                        dr2.GetOrdinal("Charge"),
                        dr2.GetOrdinal("PayAmt"),
                        dr2.GetOrdinal("ChargeAmt"),
                        dr2.GetOrdinal("CardNo")
                    };

                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            string PaymentLine = "";
                            if (Sm.DrDec(dr2, c2[1]) == 0)
                            {
                                PaymentLine = Sm.PadRight(Sm.DrStr(dr2, c2[0]) + " ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.DrDec(dr2, c2[2])), 12);
                                PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);
                            }
                            else
                            {
                                string CardNo = Sm.DrStr(dr2, c2[4]);
                                if (CardNo.Length > 4) CardNo = CardNo.Substring(0, CardNo.Length - 4) + Sm.CopyStr("X", 4);

                                PaymentLine = Sm.PadRight(Sm.DrStr(dr2, c2[0]) + " " + "+" + Sm.DrDec(dr2, c2[1]) + "%", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.DrDec(dr2, c2[2])), 12);
                                PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);

                                if (CardNo != "")
                                {
                                    PaymentLine = Sm.PadLeft("    " + CardNo, Gv.MaxCharLinePrint - 12);
                                    PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);
                                }
                                PaymentLine = Sm.PadLeft("    " + "Swipe:" + String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), Sm.DrDec(dr2, c2[2]) + Sm.DrDec(dr2, c2[3])), Gv.MaxCharLinePrint - 12);
                                PrintLine(graphics, PaymentLine, startX, startY, 15, 8, ref Offset);
                            }
                        }
                    }
                    if (TChange != 0)
                    {
                        string ChangeLine = Sm.PadRight("Change ", Gv.MaxCharLinePrint - 12) + Sm.PadRight(String.Format(GetNumFormat(GrdPosSubTotal.BaseDecimal), TChange), 12);
                        PrintLine(graphics, ChangeLine, startX, startY, 15, 8, ref Offset);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, "Unable to print."+Exc.Message);
            }
        }

        //private void TxtQty_Validated(object sender, EventArgs e)
        //{
        //    Sm.FormatNumTxt(TxtQty, 0);
        //    ComputeSubTotal(fCell.RowIndex);
        //}

        //private void TxtQty_KeyDown(object sender, KeyEventArgs e)
        //{
        //    TxtKeyDown(Grd1, ref fAccept, e);
        //}

        //private void TxtQty_Leave(object sender, EventArgs e)
        //{
        //    TxtLeave(TxtQty, ref fCell, ref fAccept);
        //}

        //private void TxtLeave(TextEdit Txt, ref iGCell fCell, ref bool fAccept)
        //{
        //    if (Txt.Visible && fAccept && fCell.ColIndex == GrdPosQuantity.Col)
        //    {
        //        if (Txt.Text.Length == 0)
        //            Grd1.Cells[fCell.RowIndex, GrdPosQuantity.Col].Value = 0m;
        //        else
        //        {
        //            Grd1.Cells[fCell.RowIndex, GrdPosQuantity.Col].Value = TxtQty.Text;
        //        }
        //        Txt.Visible = false;
        //    }
        //}

        //private void TxtKeyDown(iGrid Grd, ref bool fAccept, KeyEventArgs e)
        //{
        //    switch (e.KeyCode)
        //    {
        //        case Keys.Enter:
        //            Grd.Focus();
        //            break;
        //        case Keys.Escape:
        //            fAccept = false;
        //            Grd.Focus();
        //            break;
        //    }
        //}


        #endregion

        //private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        //{
        //    if (mIsPOSQtyEditable)
        //    {
        //        if (e.ColIndex == GrdPosQuantity.Col && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0) 
        //            TxtRequestEdit(Grd1, TxtQty, ref fCell, ref fAccept, e);
        //    }
        //}

        //private void TxtRequestEdit(
        //   iGrid Grd,
        //   DevExpress.XtraEditors.TextEdit Txt,
        //   ref iGCell fCell,
        //   ref bool fAccept,
        //   TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        //{
        //    e.DoDefault = false;

        //    fCell = Grd.Cells[e.RowIndex, e.ColIndex];
        //    fCell.EnsureVisible();
        //    Rectangle myBounds = fCell.Bounds;
        //    myBounds.Width -= Grd.GridLines.Vertical.Width;
        //    myBounds.Height -= Grd.GridLines.Horizontal.Width;
        //    if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

        //    Rectangle myCellsArea = Grd.CellsAreaBounds;
        //    if (myBounds.Right > myCellsArea.Right)
        //        myBounds.Width -= myBounds.Right - myCellsArea.Right;
        //    if (myBounds.Bottom > myCellsArea.Bottom)
        //        myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

        //    myBounds.Offset(Grd.Location);

        //    Txt.Bounds = myBounds;

        //    if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
        //        Txt.EditValue = 0m;
        //    else
        //        Txt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd, fCell.RowIndex, fCell.ColIndex), 0);
        //    Txt.Visible = true;
        //    Txt.Focus();

        //    fAccept = true;
        //}
    }

    class CurrencyRate
    {
        public string CurCode { set; get; }
        public decimal CurRate { set; get; }
    }

    class MasterItem
    {
        public string ItCode { set; get; }
        public string ItName { set; get; }
        public string BarCode { set; get; }
        public string CurCode { set; get; }

        public decimal UnitPrice { set; get; }
        public decimal DiscRate { set; get; }
        public decimal CurRate { set; get; }

        public string TaxLiableInd { set; get; }
        public decimal Tax1Amt { set; get; }
        public decimal Tax2Amt { set; get; }
        public decimal Tax3Amt { set; get; }
    }

    class FunctionKey
    {
        public string FuncCode { set; get; }
        public string FuncName { set; get; }
        public string FuncKey { set; get; }
        public string FuncPwd { set; get; }
    }
    
}
