﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmInsentif2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmInsentif2Find FrmFind;

        #endregion

        #region Constructor

        public FrmInsentif2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Insentif";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Employee"+Environment.NewLine+"Name",
                        "Old Employee "+Environment.NewLine+"Code",
                        //6-10
                        "Department",
                        "Position",
                        "Amount",
                        "Index (%)",
                        "Insentif"+Environment.NewLine+"Amount",
                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        //1-5
                        20, 80, 20, 200, 100, 
                        //6-10
                        150, 150, 150, 80, 150,
                        //11
                        200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 5 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 11 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, MeeRemark, DteStartDt, DteEndDt
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11 });
                    ChkCancelInd.Properties.ReadOnly = true; 
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteStartDt, DteEndDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3, 9, 10, 11 });
                    DteDocDt.Focus();
                    ChkCancelInd.Properties.ReadOnly = true;
                    break;
                case mState.Edit:
                    DteDocDt.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }


        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, DteStartDt, DteEndDt, MeeRemark
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 10 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmInsentif2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    CancelData();
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {

        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1 && !Sm.IsDteEmpty(DteStartDt, "Period's start date") && !Sm.IsDteEmpty(DteEndDt, "Period's end date"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmInsentif2Dlg(this, Sm.GetDte(DteEndDt)));
                    }
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsDteEmpty(DteStartDt, "Period's start date") && !Sm.IsDteEmpty(DteEndDt, "Period's end date"))
                Sm.FormShowDialog(new FrmInsentif2Dlg(this, Sm.GetDte(DteEndDt)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 9 }, e.ColIndex) && TxtDocNo.Text.Length == 0)
            {
                ComputeInsentif();
            }
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && TxtDocNo.Text.Length == 0)
            {
                ComputeProsen();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 9)
            {
                if (Sm.GetGrdDec(Grd1, 0, 9) != 0)
                {
                    var Insentif = Sm.GetGrdDec(Grd1, 0, 9);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0) Grd1.Cells[Row, 9].Value = Insentif;
                }
                ComputeInsentif();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Insentif2", "TblInsentif2Hdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveInsentif2Hdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveInsentif2Dtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {

            return
                Sm.IsTxtEmpty(DteDocDt, "Document Date", false) ||
                Sm.IsTxtEmpty(DteStartDt, "Periode Start Date", false) ||
                Sm.IsTxtEmpty(DteEndDt, "Periode End Date", false) ||
                IsGrdEmpty() ||
                IsDataAlreadyCreated() ||
                IsAmountNotValid()
                //IsIndexNotValid()
                ;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee in list.");
                return true;
            }
            return false;
        }

        private bool IsAmountNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdDec(Grd1, Row, 8) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Employee Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Department : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Position : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                        "Amount should be greater than 0."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsIndexNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && Sm.GetGrdDec(Grd1, Row, 9) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Employee Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Department : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Position : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                        "Index should be greater than 0."
                        );
                    return true;
                }
            }
            return false;
        }



        private bool IsDataAlreadyCreated()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select B.EmpCode From TblInsentif2Hdr A ");
                    SQL.AppendLine("Inner Join TblInsentif2Dtl B On A.DocNo = B.Docno And A.cancelInd = 'N' ");
                    SQL.AppendLine("Where EmpCode=@EmpCode And (A.StartDt between @StartDt And @EndDt Or A.EndDt between @StartDt And @EndDt) ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
                    Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                    Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

                    if (Sm.IsDataExist(cm))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                        "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "Employee Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                        "Department : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                        "Position : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine + Environment.NewLine +
                        "Already created insentif for date."
                        );
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveInsentif2Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblInsentif2Hdr (DocNo, DocDt, StartDt, EndDt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DocDt, @StartDt, @EndDt,  @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveInsentif2Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblInsentif2Dtl(DocNo, DNo, EmpCode, Amt, IndexValue, InsentifAmt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @Amt, @IndexValue, @InsentifAmt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@IndexValue", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@InsentifAmt", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelInsentifHdr());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblInsentif2Hdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }


        private MySqlCommand CancelInsentifHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblInsentif2Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowInsentif2Hdr(DocNo);
                ShowInsentif2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowInsentif2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                         ref cm,
                         "Select DocNo, CancelInd, DocDt, StartDt, EndDt, Remark From TblInsentif2Hdr Where DocNo=@DocNo ",
                         new string[] { "DocNo", "CancelInd", "DocDt", "StartDt", "EndDt", "Remark" },
                         (MySqlDataReader dr, int[] c) =>
                         {
                             TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                             ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[1]), "Y") ? true : false;
                             Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                             Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[3]));
                             Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[4])); 
                             MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                         }, true
                     );
        }

        private void ShowInsentif2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, B.EmpCodeOld, C.DeptName, D.PosName, A.Amt, A.IndexValue, A.InsentifAmt, A.Remark ");
            SQL.AppendLine("From TblInsentif2Dtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Inner Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo ");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-5
                        "EmpCode", "EmpName", "EmpCodeOld", "DeptName", "PosName", 
                    
                        //6-9
                        "Amt", "IndexValue", "InsentifAmt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 10 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowEmpAmt()
        {
            try
            {
                string Amount;
                if (Grd1.Rows.Count >= 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        {
                            Amount = Sm.GetValue("select ifnull(SUM(B.Wages*B.HolidayWagesIndex), 0) As Amount " +
                            "From TblPWGHdr A " +
                            "Inner Join tblPwgdtl5 B On A.DocNo = B.DocNo And A.CancelInd = 'N' " +
                            "Where B.CoordinatorInd = 'Y' " +
                            "And B.EmpCode = '" + Sm.GetGrdStr(Grd1, Row, 2) + "' " +
                            "And A.DocDt Between '" + Sm.GetDte(DteStartDt).Substring(0, 8) + "' And '" + Sm.GetDte(DteEndDt).Substring(0, 8) + "' ");
                            Grd1.Cells[Row, 8].Value = Decimal.Parse(Amount);
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Additional method

        internal void ComputeInsentif()
        {
            decimal Amt = 0m, Index = 0m, insentif = 0m;
            decimal Prosen = 0.01m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Amt = Sm.GetGrdDec(Grd1, Row, 8);
                Index = Sm.GetGrdDec(Grd1, Row, 9);
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                {
                    insentif = Amt * (Index * Prosen);
                    if(insentif>=0)
                    {
                        Grd1.Cells[Row, 10].Value = insentif;
                    }
                }
            }
        }

        internal void ComputeProsen()
        {
            decimal Amt = 0m, IndexP = 0m, Insentif = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Amt = Sm.GetGrdDec(Grd1, Row, 8);
                Insentif = Sm.GetGrdDec(Grd1, Row, 10);
                if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 && Amt!=0)
                {
                    IndexP = (Insentif / Amt) * 100;
                    Grd1.Cells[Row, 9].Value = Math.Round(IndexP, 2);
                }
            }
        }


        #endregion

        #endregion

        #region Event
        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
        }
        #endregion
    }
}
