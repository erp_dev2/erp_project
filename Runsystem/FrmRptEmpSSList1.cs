﻿#region Update
/*
    27/09/2017 [TKG] menggunakan employee salary for social security berdasarkan parameter SSSalaryInd
    19/12/2017 [TKG] tambah filter dept+site berdasarkan group
    16/05/2018 [TKG] tambah entity
    06/01/2019 [TKG] Untuk KMI, komponen allowance deduction berdasarkan start date dan end date-nya yg dibandingkan dengan cut off date
    01/11/2019 [TKG/SRN] perhitungan divalidasi minimum salary
    03/09/2020 [TKG/SRN] bug saat show data
    13/01/2020 [TKG/PHT] Berdasarkan parameter IsEmpSSListExclPassAwalEmp, karyawan yg sudah meninggal tidak akan diproses datanya.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;


#endregion

namespace RunSystem
{
    public partial class FrmRptEmpSSList1 : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mSSPCode = string.Empty,
            mSSSalaryInd = string.Empty, mSSAllowance = string.Empty;
        private decimal mMaxLimitSalaryForSS = 0m;
        private decimal mSSRetiredMaxAge = 0m;
        private bool 
            mIsFilterBySiteHR = false, 
            mIsFilterByDeptHR = false, 
            mIsEmpSSBasedOnEntity = false,
            mIsEmpSSListExclPassAwalEmp = false
            ;

        #endregion

        #region Constructor

        public FrmRptEmpSSList1(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                DteDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
               
                SetLueStatus();
                if (mIsEmpSSBasedOnEntity)
                    LblEntCode.ForeColor = Color.Red;
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueEntCode }, true);
                Sl.SetLueEntCode(ref LueEntCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
                Grd1.Cells[Row, 4].Value = "'" + Sm.GetGrdStr(Grd1, Row, 4);
                Grd1.Cells[Row, 11].Value = "'" + Sm.GetGrdStr(Grd1, Row, 11);
                Grd1.Cells[Row, 16].Value = "'" + Sm.GetGrdStr(Grd1, Row, 16);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
                Grd1.Cells[Row, 4].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 4), Sm.GetGrdStr(Grd1, Row, 4).Length - 1);
                Grd1.Cells[Row, 11].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 11), Sm.GetGrdStr(Grd1, Row, 11).Length - 1);
                Grd1.Cells[Row, 16].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 16), Sm.GetGrdStr(Grd1, Row, 16).Length - 1);
            }
            Grd1.EndUpdate();
        }

        private void GetParameter()
        {
            mSSPCode = Sm.GetParameter("SSProgramForHealth");
            mSSRetiredMaxAge = Sm.GetParameterDec("SSRetiredMaxAge");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mSSSalaryInd = Sm.GetParameter("SSSalaryInd");
            mSSAllowance = Sm.GetParameter("SSAllowance");
            mMaxLimitSalaryForSS = Sm.GetParameterDec("MaxLimitSalaryForSS");
            mIsEmpSSBasedOnEntity = Sm.GetParameterBoo("IsEmpSSBasedOnEntity");
            mIsEmpSSListExclPassAwalEmp = Sm.GetParameterBoo("IsEmpSSListExclPassAwalEmp");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.RecType, T.EmpCode, T.EmpName, T.EmpCodeOld, T.PosName, T.DeptCode, T.DeptName, T.SiteName, T.EntName, ");
            SQL.AppendLine("T.JoinDt, T.ResignDt, T.IDNo, T.BirthDt, ");
            SQL.AppendLine("T.Age, T.SSCode, T.SSName, T.CardNo, T.StartDt, T.EndDt, ");
            SQL.AppendLine("T.Status, T.Salary, ");
            SQL.AppendLine("T.EmployerAmt, T.EmployeeAmt, T.TotalAmt ");
            SQL.AppendLine("From ( ");

            SQL.AppendLine("Select '1' As RecType, A.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, C.DeptCode, E.DeptName, C.SiteCode, F.SiteName, H.EntName, ");
            SQL.AppendLine("C.JoinDt, C.ResignDt, C.IDNumber As IDNo, C.BirthDt, ");
            SQL.AppendLine("Case When C.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(C.BirthDt, '%Y%m%d'), STR_TO_DATE(@Dt, '%Y%m%d')) End As Age, ");
            SQL.AppendLine("A.SSCode, B.SSName, A.CardNo, A.StartDt, A.EndDt, ");
            SQL.AppendLine("'Peserta' As Status, ");

            if (mSSSalaryInd == "1")
            {
                SQL.AppendLine("Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End As Salary, ");
                SQL.AppendLine("B.EmployerPerc/100*Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End  As EmployerAmt, ");
                SQL.AppendLine("B.EmployeePerc/100*Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End  As EmployeeAmt, ");
                SQL.AppendLine("B.TotalPerc/100*Case When IfNull(G.HealthSSSalary, 0)>B.SalaryMaxLimit Then B.SalaryMaxLimit Else IfNull(G.HealthSSSalary, 0) End  As TotalAmt ");
            }
            if (mSSSalaryInd == "2" || mSSSalaryInd == "3")
            {
                SQL.AppendLine("Case When IfNull(B.SalaryMaxLimit, 0.00)=0.00 Then ");
                SQL.AppendLine("    Case When IfNull(B.SalaryMinLimit, 0.00)=0.00 Then ");
                SQL.AppendLine("        IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As Salary, ");
                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.EmployerPerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployerPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployerPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As EmployerAmt, ");
                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.EmployeePerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.EmployeePerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.EmployeePerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As EmployeeAmt, ");
                SQL.AppendLine("Case When B.SalaryMaxLimit=0.00 Then ");
                SQL.AppendLine("    Case When B.SalaryMinLimit=0.00 Then ");
                SQL.AppendLine("        B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When IfNull(G.Salary, 0.00)>B.SalaryMaxLimit Then ");
                SQL.AppendLine("        B.TotalPerc*0.01*B.SalaryMaxLimit ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        Case When IfNull(G.Salary, 0)<B.SalaryMinLimit Then ");
                SQL.AppendLine("            B.TotalPerc*0.01*B.SalaryMinLimit ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            B.TotalPerc*0.01*IfNull(G.Salary, 0.00) ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    End ");
                SQL.AppendLine("End As TotalAmt ");
            }
            SQL.AppendLine("From TblEmployeeSS A ");
            SQL.AppendLine("Inner Join TblSS B On A.SSCode=B.SSCode ");
            if (mSSSalaryInd == "2")
                SQL.AppendLine("And B.SSPCode Like '%" + mSSPCode + "%' ");
            else
                SQL.AppendLine("And B.SSPCode=@SSPCode ");
            SQL.AppendLine("Inner Join TblEmployee C ");
            SQL.AppendLine("    On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("    And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>Left(@Dt, 8))) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And (C.SiteCode Is Null Or (");
                SQL.AppendLine("    C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And (C.DeptCode Is Null Or (");
                SQL.AppendLine("    C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=C.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            if (mIsEmpSSListExclPassAwalEmp)
            {
                SQL.AppendLine("And C.EmpCode Not In ( ");
                SQL.AppendLine("    Select Distinct A.EmpCode  ");
                SQL.AppendLine("    From TblPPS A, TblParameter B ");
                SQL.AppendLine("    Where B.ParValue Is Not Null  ");
                SQL.AppendLine("    And B.ParCode='JobTransferPassAway' ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A'  ");
                SQL.AppendLine("    And A.JobTransfer=B.ParValue ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On C.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblSite F On C.SiteCode=F.SiteCode ");
            if (mSSSalaryInd == "1")
            {
                SQL.AppendLine("Left Join TblGradeLevelHdr G On C.GrdLvlCode=G.GrdLvlCode ");
            }
            if (mSSSalaryInd == "2")
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.EmpCode, T1.StartDt, T1.Amt+IfNull(T3.Amt, 0) As Salary ");
                SQL.AppendLine("    From TblEmployeeSalary T1 ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
                SQL.AppendLine("        From TblEmployeeSalary ");
                SQL.AppendLine("        Where EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee ");
                SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@Dt, 8))) ");
                SQL.AppendLine("        ) And StartDt<Left(@Dt, 8) ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select EmpCode, Sum(Amt) As Amt ");
                SQL.AppendLine("        From TblEmployeeAllowanceDeduction ");
                SQL.AppendLine("        Where Position(ADCode In @SSAllowance) ");
                SQL.AppendLine("        And ( ");
                SQL.AppendLine("        (StartDt Is Null And EndDt Is Null) Or ");
                SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Null And StartDt<=@Dt) Or ");
                SQL.AppendLine("        (StartDt Is Null And EndDt Is Not Null And @Dt<=EndDt) Or ");
                SQL.AppendLine("        (StartDt Is Not Null And EndDt Is Not Null And StartDt<=@Dt And @Dt<=EndDt) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode ");
                SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");
            }

            if (mSSSalaryInd == "3")
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T1.EmpCode, T1.Amt As Salary ");
                SQL.AppendLine("    From TblEmployeeSalarySS T1 ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select EmpCode, Max(StartDt) As StartDt ");
                SQL.AppendLine("        From TblEmployeeSalarySS ");
                SQL.AppendLine("        Where EmpCode In (");
                SQL.AppendLine("            Select EmpCode From TblEmployee ");
                SQL.AppendLine("            Where (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(@CutOffDt, 8))) ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        And Left(@Dt, 8)>=StartDt ");
                SQL.AppendLine("        And Left(@Dt, 8)<=IfNull(EndDt, '99990101') ");
                SQL.AppendLine("        And SSPCode=@SSPCode ");
                SQL.AppendLine("        Group By EmpCode ");
                SQL.AppendLine("    ) T2 On T1.EmpCode=T2.EmpCode And T1.StartDt=T2.StartDt ");
                SQL.AppendLine("    Where T1.SSPCode=@SSPCode ");
                SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");
            }
            SQL.AppendLine("Left Join TblEntity H On A.EntCode=H.EntCode ");
            if (mIsEmpSSBasedOnEntity)
            {
                SQL.AppendLine("Where A.EntCode Is Not Null ");
                SQL.AppendLine("And A.EntCode=@EntCode ");
            }

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As RecType, A.EmpCode, A.FamilyName As EmpName, Null As EmpCodeOld, Null As PosName, E.DeptCode, E.DeptName, F.SiteCode, F.SiteName, G.EntName, ");
            SQL.AppendLine("Null As JoinDt, Null As ResignDt, A.IDNo, A.BirthDt, ");
            SQL.AppendLine("Case When A.BirthDt Is Null Then 0 Else TIMESTAMPDIFF(YEAR, STR_TO_DATE(A.BirthDt, '%Y%m%d'), STR_TO_DATE(@Dt, '%Y%m%d')) End As Age, ");
            SQL.AppendLine("A.SSCode, B.SSName, A.CardNo, A.StartDt, A.EndDt, ");
            SQL.AppendLine("C.OptDesc As Status, 0.00 As Salary, ");
            SQL.AppendLine("0.00 As EmployerAmt, 0.00 As EmployeeAmt, 0.00 As TotalAmt ");
            SQL.AppendLine("From TblEmployeeFamilySS A ");
            SQL.AppendLine("Inner Join TblSS B On A.SSCode=B.SSCode ");
            if (mSSSalaryInd == "2")
                SQL.AppendLine("And B.SSPCode Like '%" + mSSPCode + "%' ");
            else
                SQL.AppendLine("And B.SSPCode=@SSPCode ");
            SQL.AppendLine("Left Join TblOption C On C.OptCat='FamilyStatus' And A.Status=C.OptCode ");
            SQL.AppendLine("Inner Join TblEmployee D ");
            SQL.AppendLine("    On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("    And (D.ResignDt Is Null Or (D.ResignDt Is Not Null And D.ResignDt>Left(@Dt, 8))) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And (D.SiteCode Is Null Or (");
                SQL.AppendLine("    D.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And (D.DeptCode Is Null Or (");
                SQL.AppendLine("    D.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=D.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            if (mIsEmpSSListExclPassAwalEmp)
            {
                SQL.AppendLine("And D.EmpCode Not In ( ");
                SQL.AppendLine("    Select Distinct A.EmpCode  ");
                SQL.AppendLine("    From TblPPS A, TblParameter B ");
                SQL.AppendLine("    Where B.ParValue Is Not Null  ");
                SQL.AppendLine("    And B.ParCode='JobTransferPassAway' ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine("    And A.Status='A'  ");
                SQL.AppendLine("    And A.JobTransfer=B.ParValue ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblDepartment E On D.DeptCode=E.DeptCode ");
            SQL.AppendLine("Left Join TblSite F On D.SiteCode=F.SiteCode ");
            SQL.AppendLine("Left Join TblEntity G On A.EntCode=G.EntCode ");
            if (mIsEmpSSBasedOnEntity)
            {
                SQL.AppendLine("Where A.EntCode Is Not Null ");
                SQL.AppendLine("And A.EntCode=@EntCode ");
            }

            SQL.AppendLine(") T Where T.EndDt Is Null ");
            SQL.AppendLine("And ");
            SQL.AppendLine("(");
            SQL.AppendLine("    T.StartDt Is Null ");
            SQL.AppendLine("    Or ( T.StartDt Is Not Null And T.StartDt<=Left(@Dt, 8))");
            SQL.AppendLine(") ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Participants",
                        "Old"+Environment.NewLine+"Code",
                        "Position",
                        
                        //6-10
                        "Department",
                        "Site",
                        "Entity",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",

                        //11-15
                        "Identity#",
                        "Birth Date",
                        "Age",
                        "Social Security Code",
                        "Social Security"+Environment.NewLine+"Name",
                        
                        //16-20
                        "Card#",
                        "Start"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Date",
                        "Status",
                        "Salary",

                        //21-23
                        "Employer"+Environment.NewLine+"(Amount)",
                        "Employee"+Environment.NewLine+"(Amount)",
                        "Total"+Environment.NewLine+"(Amount)"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 20, 200, 80, 150, 
                        
                        //6-10
                        150, 150, 200, 100, 100, 
                        
                        //11-15
                        150, 100, 80, 0, 150, 
                        
                        //16-20
                        100, 100, 100, 150, 100, 100, 
                        
                        //21-23
                        100, 100, 100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 20, 21, 22, 23 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10, 12, 17, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 9, 12, 14 }, false);
            if (!mIsEmpSSBasedOnEntity)
                Sm.GrdColInvisible(Grd1, new int[] { 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 5, 9, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                if (Sm.IsDteEmpty(DteDt, "Cut-off date")) return;
                if (mIsEmpSSBasedOnEntity && Sm.IsLueEmpty(LueEntCode, "Entity")) return;

                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 1=1 ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "T.RecType", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SSPCode", mSSPCode);
                Sm.CmParam<Decimal>(ref cm, "@SSRetiredMaxAge", mSSRetiredMaxAge);
                Sm.CmParam<String>(ref cm, "@Dt", Sm.GetDte(DteDt).Substring(0, 8));
                Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetLue(LueEntCode));
                if (mSSSalaryInd == "2")
                {
                    Sm.CmParam<String>(ref cm, "@SSAllowance", mSSAllowance);
                    Sm.CmParam<Decimal>(ref cm, "@MaxLimitSalaryForSS", mMaxLimitSalaryForSS);
                }
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, 
                    mSQL.ToString() + Filter + " Order By T.DeptName, T.SiteName, T.EmpCode, T.RecType, T.EmpName;",
                    new string[] 
                        { 
                            //0
                            "RecType",  

                            //1-5
                            "EmpCode", "EmpName", "EmpCodeOld", "PosName", "DeptName",  
                            
                            //6-10
                            "SiteName", "EntName", "JoinDt", "ResignDt", "IDNo", 
                            
                            //11-15
                            "BirthDt", "Age", "SSCode", "SSName", "CardNo", 
                            
                            //16-20
                            "StartDt", "EndDt", "Status", "Salary", "EmployerAmt", 

                            //21-22
                            "EmployeeAmt", "TotalAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row+1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            if (Sm.DrStr(dr, 0) == "1")
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            if (Sm.DrStr(dr, 0) == "1")
                            {
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                            }
                            else
                            {
                                for (int Col = 20; Col <= 23; Col++)
                                    Grd.Cells[Row, Col].Value = 0m;
                            }
                        }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 21, 22, 23 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployeeSS(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueStatus()
        {
            Sm.SetLue2(
             ref LueStatus, "Select '1' As Col1, 'Peserta' As Col2 Union All Select '2' As Col1, 'Non Peserta' As Col2;",
             0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue1(Sl.SetLueEntCode));
        }

        #endregion

        #endregion
    }
}
