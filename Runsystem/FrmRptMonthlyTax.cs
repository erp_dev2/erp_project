﻿#region
/*
    29/09/2017 [TKG] Monthly Employee's Tax reporting
    09/10/2017 [TKG] tambah total brutto dan tax
    09/12/2017 [TKG] tambah informasi dan filter payrun period
    26/01/2022 [TKG/PHT] Tambah validasi : payrun yg diproses hanya employee yg npwp + ptkp nya tidak kosong dan belum resign pada saat payrun tsb diproses.
    26/01/2022 [TKG/PHT] ubah GetParameter()
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyTax : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mSalaryInd = string.Empty;
        private bool mIsNotFilterByAuthorization = false;
        
        #endregion

        #region Constructor

        public FrmRptMonthlyTax(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                
                string Value = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth1);
                Sl.SetLueMth(LueMth2);
                Sm.SetLue(LueYr, Sm.Left(Value, 4));
                Sm.SetLue(LueMth1, "01");
                Sm.SetLue(LueMth2, Value.Substring(4, 2));
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'SalaryInd', 'IsPayrollDataFilterByAuthorization' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "SalaryInd": mSalaryInd = ParValue; break;
                            
                            //boolean
                            case "IsPayrollDataFilterByAuthorization":
                                mIsNotFilterByAuthorization = ParValue == "N"; 
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.Mth, T1.EmpCode, T1.Brutto, T1.Tax ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Mth, EmpCode, Sum(Brutto) As Brutto, Sum(Tax) As Tax ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select Substring(B.EndDt, 5, 2) As Mth, A.EmpCode, ");
            SQL.AppendLine("        (IfNull(A.Salary,0)+ IfNull(A.ProcessPLAmt,0)-IfNull(A.ProcessUPLAmt,0)+ IfNull(A.OT1Amt,0)+ IfNull(A.OT2Amt,0)+ ");
            SQL.AppendLine("        IfNull(A.OTHolidayAmt,0)+ IfNull(A.TaxableFixAllowance,0)+ IfNull(A.EmploymentPeriodAllowance,0)+ IfNull(A.IncEmployee,0)+ ");
            SQL.AppendLine("        IfNull(A.IncMinWages,0)+ IfNull(A.IncProduction,0)+ IfNull(A.IncPerformance,0)+ IfNull(A.PresenceReward,0)+ IfNull(A.HolidayEarning,0)+ ");
            SQL.AppendLine("        IfNull(A.ExtraFooding,0)+ IfNull(A.SSEmployerHealth,0)+ IfNull(A.SSEmployerEmployment,0)+ IfNull(A.SalaryAdjustment,0)- ");
            SQL.AppendLine("        IfNull(A.TaxableFixDeduction,0) - IfNull(A.DedEmployee,0)- IfNull(A.DedProduction,0)- IfNull(A.DedProdLeave,0)- IfNull(A.SSEmployeeEmployment,0)) As Brutto, ");
            SQL.AppendLine("        A.EOYtax+A.Tax As Tax ");
            SQL.AppendLine("        From TblPayrollProcess1 A ");
            SQL.AppendLine("        Inner Join TblPayrun B ");
            SQL.AppendLine("            On A.PayrunCode=B.PayrunCode ");
            SQL.AppendLine("            And B.CancelInd='N' ");
            SQL.AppendLine("            And Substring(B.EndDt, 5, 2) Between @Mth1 And @Mth2  ");
            SQL.AppendLine("            And Left(B.EndDt, 4)=@Yr ");
            SQL.AppendLine("        Where A.NPWP Is Not Null ");
            SQL.AppendLine("        And A.PTKP Is Not Null ");
            SQL.AppendLine("        And A.ResignDt Is Null ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("    And A.EmpCode In ( ");
                SQL.AppendLine("        Select EmpCode From TblEmployee ");
                SQL.AppendLine("        Where GrdLvlCode In ( ");
                SQL.AppendLine("            Select T2.GrdLvlCode ");
                SQL.AppendLine("            From TblPPAHdr T1 ");
                SQL.AppendLine("            Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("            Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select Substring(A.HolidayDt, 5, 2) As Mth, B.EmpCode, ");
            SQL.AppendLine("    B.Value As Brutto, B.Tax ");
            SQL.AppendLine("    From TblRHAHdr A ");
            SQL.AppendLine("    Inner Join TblRHADtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status='A' ");
            SQL.AppendLine("    And Substring(A.HolidayDt, 5, 2) Between @Mth1 And @Mth2 ");
            SQL.AppendLine("    And Left(A.HolidayDt, 4)=@Yr ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And B.EmpCode In ( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By Mth, EmpCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 35;
            Grd1.FrozenArea.ColCount = 4;

            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Header.Cells[0, 1].Value = "Employee's" + Environment.NewLine + "Code";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Header.Cells[0, 2].Value = "Employee's" + Environment.NewLine + "Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Header.Cells[0, 3].Value = "Old" + Environment.NewLine + "Code";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Header.Cells[0, 4].Value = "Position";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Header.Cells[0, 5].Value = "Department";
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 5].SpanRows = 2;

            Grd1.Header.Cells[0, 6].Value = "Join" + Environment.NewLine + "Date";
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 6].SpanRows = 2;

            Grd1.Header.Cells[0, 7].Value = "Resign" + Environment.NewLine + "Date";
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 7].SpanRows = 2;

            Grd1.Header.Cells[0, 8].Value = "Payrun" + Environment.NewLine + "Period";
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 8].SpanRows = 2;

            for (int c=1;c<=12;c++)
            {
                Grd1.Header.Cells[1, 7 + (2 * c)].Value = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(c);
                Grd1.Header.Cells[1, 7 + (2 * c)].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Header.Cells[1, 7 + (2 * c)].SpanCols = 2;
                Grd1.Header.Cells[0, 7 + (2 * c)].Value = "Brutto";
                Grd1.Header.Cells[0, 7 + (2 * c)].SpanRows = 1;
                Grd1.Header.Cells[0, 7 + (2 * c)].TextAlign = iGContentAlignment.MiddleCenter;
                Grd1.Header.Cells[0, 8 + (2 * c)].Value = "Tax";
                Grd1.Header.Cells[0, 8 + (2 * c)].SpanRows = 1;
                Grd1.Header.Cells[0, 8 + (2 * c)].TextAlign = iGContentAlignment.MiddleCenter;
            }

            Grd1.Header.Cells[1, 33].Value = "Total";
            Grd1.Header.Cells[1, 33].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 33].SpanCols = 2;
            Grd1.Header.Cells[0, 33].Value = "Brutto";
            Grd1.Header.Cells[0, 33].SpanRows = 1;
            Grd1.Header.Cells[0, 33].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 34].Value = "Tax";
            Grd1.Header.Cells[0, 34].SpanRows = 1;
            Grd1.Header.Cells[0, 34].TextAlign = iGContentAlignment.MiddleCenter;

            Sm.GrdFormatDec(Grd1, new int[] { 
                9, 10,  
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6, 7 }, false);
            Grd1.Cols.AutoWidth();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6, 7 }, !ChkHideInfoInGrd.Checked);
            Grd1.Cols.AutoWidth();
        }

        override protected void ShowData()
        {
            ClearGrd();
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            if (Sm.IsLueEmpty(LueMth1, "Start month")) return;
            if (Sm.IsLueEmpty(LueMth2, "End month")) return;

            var lResult1 = new List<Result1>();
            var lResult2 = new List<Result2>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref lResult1);
                if (lResult1.Count > 0)
                {
                    Process2(ref lResult1, ref lResult2);
                    Process3(ref lResult1, ref lResult2);
                    Process4(ref lResult2);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                lResult1.Clear();
                lResult2.Clear();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result1> l)
        {
            var cm = new MySqlCommand();
            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth1", Sm.GetLue(LueMth1));
            Sm.CmParam<String>(ref cm, "@Mth2", Sm.GetLue(LueMth2));

            Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T2.EmpCode", "T2.EmpName", "T2.EmpCodeOld" });
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T2.DeptCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePayrunPeriod), "T2.PayrunPeriod", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = mSQL.ToString() + Filter + " Order By T1.EmpCode";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Mth", 
                    
                    //1-3
                    "EmpCode", 
                    "Brutto", 
                    "Tax"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result1()
                        {
                            Mth = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            Brutto = Sm.DrDec(dr, c[2]),
                            Tax = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result1> l1, ref List<Result2> l2)
        {
            var cm = new MySqlCommand();
            string Filter = string.Empty, SQL = string.Empty;
            int i = 0;

            foreach (var x in l1.Select(r => new { r.EmpCode }).Distinct())
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), x.EmpCode);
                i++;
            }

            if (Filter.Length > 0)
                Filter = " Where ( " + Filter + ")  ";
            else
                Filter = " Where 1=0; ";

            SQL =
                "Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, A.JoinDt, A.ResignDt, D.OptDesc " +
                "From TblEmployee A " +
                "Left Join TblPosition B On A.PosCode=B.PosCode " +
                "Left Join TblDepartment C On A.DeptCode=C.DeptCode " +
                "Left Join TblOption D On A.PayrunPeriod=D.OptCode And D.OptCat='PayrunPeriod' " +
                Filter + " Order By A.EmpCode;";

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpName", 
                    "EmpCodeOld", 
                    "PosName", 
                    "DeptName", 
                    "JoinDt", 

                    //6-7
                    "ResignDt",
                    "OptDesc"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Result2()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            EmpCodeOld = Sm.DrStr(dr, c[2]),
                            PosName = Sm.DrStr(dr, c[3]),
                            DeptName = Sm.DrStr(dr, c[4]),
                            JoinDt = Sm.DrStr(dr, c[5]),
                            ResignDt = Sm.DrStr(dr, c[6]),
                            PayrunPeriod = Sm.DrStr(dr, c[7]),
                            Brutto1 = 0m,
                            Tax1 = 0m,
                            Brutto2 = 0m,
                            Tax2 = 0m,
                            Brutto3 = 0m,
                            Tax3 = 0m,
                            Brutto4 = 0m,
                            Tax4 = 0m,
                            Brutto5 = 0m,
                            Tax5 = 0m,
                            Brutto6 = 0m,
                            Tax6 = 0m,
                            Brutto7 = 0m,
                            Tax7 = 0m,
                            Brutto8 = 0m,
                            Tax8 = 0m,
                            Brutto9 = 0m,
                            Tax9 = 0m,
                            Brutto10 = 0m,
                            Tax10 = 0m,
                            Brutto11 = 0m,
                            Tax11 = 0m,
                            Brutto12 = 0m,
                            Tax12 = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result1> l1, ref List<Result2> l2)
        {
            int x = 0;
            for (int i = 0; i < l1.Count; i++)
            {
                for (int j = x; j < l2.Count; j++)
                {
                    if (Sm.CompareStr(l1[i].EmpCode, l2[j].EmpCode))
                    {
                        if (l1[i].Mth == "01")
                        {
                            l2[j].Brutto1 += l1[i].Brutto;
                            l2[j].Tax1 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "02")
                        {
                            l2[j].Brutto2 += l1[i].Brutto;
                            l2[j].Tax2 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "03")
                        {
                            l2[j].Brutto3 += l1[i].Brutto;
                            l2[j].Tax3 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "04")
                        {
                            l2[j].Brutto4 += l1[i].Brutto;
                            l2[j].Tax4 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "05")
                        {
                            l2[j].Brutto5 += l1[i].Brutto;
                            l2[j].Tax5 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "06")
                        {
                            l2[j].Brutto6 += l1[i].Brutto;
                            l2[j].Tax6 += l1[i].Tax;
                        }

                        if (l1[i].Mth == "07")
                        {
                            l2[j].Brutto7 += l1[i].Brutto;
                            l2[j].Tax7 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "08")
                        {
                            l2[j].Brutto8 += l1[i].Brutto;
                            l2[j].Tax8 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "09")
                        {
                            l2[j].Brutto9 += l1[i].Brutto;
                            l2[j].Tax9 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "10")
                        {
                            l2[j].Brutto10 += l1[i].Brutto;
                            l2[j].Tax10 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "11")
                        {
                            l2[j].Brutto11 += l1[i].Brutto;
                            l2[j].Tax11 += l1[i].Tax;
                        }
                        if (l1[i].Mth == "12")
                        {
                            l2[j].Brutto12 += l1[i].Brutto;
                            l2[j].Tax12 += l1[i].Tax;
                        }
                        x = j;
                        break;
                    }
                }

            }
        }

        private void Process4(ref List<Result2> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[2].Value = l[i].EmpName;
                r.Cells[3].Value = l[i].EmpCodeOld;
                r.Cells[4].Value = l[i].PosName;
                r.Cells[5].Value = l[i].DeptName;
                r.Cells[6].Value = Sm.ConvertDate(l[i].JoinDt);
                if (l[i].ResignDt.Length > 0)
                    r.Cells[7].Value = Sm.ConvertDate(l[i].ResignDt);
                else
                    r.Cells[7].Value = null;
                r.Cells[8].Value = l[i].PayrunPeriod;
                r.Cells[9].Value = l[i].Brutto1;
                r.Cells[10].Value = l[i].Tax1;
                r.Cells[11].Value = l[i].Brutto2;
                r.Cells[12].Value = l[i].Tax2;
                r.Cells[13].Value = l[i].Brutto3;
                r.Cells[14].Value = l[i].Tax3;
                r.Cells[15].Value = l[i].Brutto4;
                r.Cells[16].Value = l[i].Tax4;
                r.Cells[17].Value = l[i].Brutto5;
                r.Cells[18].Value = l[i].Tax5;
                r.Cells[19].Value = l[i].Brutto6;
                r.Cells[20].Value = l[i].Tax6;
                r.Cells[21].Value = l[i].Brutto7;
                r.Cells[22].Value = l[i].Tax7;
                r.Cells[23].Value = l[i].Brutto8;
                r.Cells[24].Value = l[i].Tax8;
                r.Cells[25].Value = l[i].Brutto9;
                r.Cells[26].Value = l[i].Tax9;
                r.Cells[27].Value = l[i].Brutto10;
                r.Cells[28].Value = l[i].Tax10;
                r.Cells[29].Value = l[i].Brutto11;
                r.Cells[30].Value = l[i].Tax11;
                r.Cells[31].Value = l[i].Brutto12;
                r.Cells[32].Value = l[i].Tax12;

                r.Cells[33].Value = 
                    l[i].Brutto1 + l[i].Brutto2 + l[i].Brutto3 + l[i].Brutto4 +
                    l[i].Brutto5 + l[i].Brutto6 + l[i].Brutto7 + l[i].Brutto8 +
                    l[i].Brutto9 + l[i].Brutto10 + l[i].Brutto11 + l[i].Brutto12;
                r.Cells[34].Value =
                    l[i].Tax1 + l[i].Tax2 + l[i].Tax3 + l[i].Tax4 +
                    l[i].Tax5 + l[i].Tax6 + l[i].Tax7 + l[i].Tax8 +
                    l[i].Tax9 + l[i].Tax10 + l[i].Tax11 + l[i].Tax12;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1,
                new int[] { 
                    9, 10,
                    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                    31, 32, 33, 34
                });
            
            bool IsHasValue = false;
            for (int c = 9; c <= 32; c++)
            {
                IsHasValue = false;
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdDec(Grd1, i, c) > 0m)
                    {
                        IsHasValue = true;
                        break;
                    }
                }
                Grd1.Cols[c].Visible = IsHasValue;
            }
            Grd1.Cols.AutoWidth();
            Grd1.EndUpdate();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Count = 0;
            for (int c = 9; c <= 32; c++)
                Grd1.Cols[c].Visible = false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPayrunPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Payrun period");
        }

        #endregion

        #endregion

        #region Class

        private class Result1
        {
            public string Mth { get; set; }
            public string EmpCode { get; set; }
            public decimal Brutto { get; set; }
            public decimal Tax { get; set; }
        }

        private class Result2
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public string DeptName { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PayrunPeriod { get; set; }
            public decimal Brutto1 { get; set; }
            public decimal Tax1 { get; set; }
            public decimal Brutto2 { get; set; }
            public decimal Tax2 { get; set; }
            public decimal Brutto3 { get; set; }
            public decimal Tax3 { get; set; }
            public decimal Brutto4 { get; set; }
            public decimal Tax4 { get; set; }
            public decimal Brutto5 { get; set; }
            public decimal Tax5 { get; set; }
            public decimal Brutto6 { get; set; }
            public decimal Tax6 { get; set; }
            public decimal Brutto7 { get; set; }
            public decimal Tax7 { get; set; }
            public decimal Brutto8 { get; set; }
            public decimal Tax8 { get; set; }
            public decimal Brutto9 { get; set; }
            public decimal Tax9 { get; set; }
            public decimal Brutto10 { get; set; }
            public decimal Tax10 { get; set; }
            public decimal Brutto11 { get; set; }
            public decimal Tax11 { get; set; }
            public decimal Brutto12 { get; set; }
            public decimal Tax12 { get; set; }
        }

        #endregion
    }
}
