﻿#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmOutgoingPaymentNoInvoice : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "";
        internal FrmOutgoingPaymentNoInvoiceFind FrmFind;

        #endregion

        #region Constructor

        public FrmOutgoingPaymentNoInvoice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Outgoing Payment Without Invoice";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);

                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueCurCode(ref LueCurCode);

                SetGrd();
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, EventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Description",

                        //1-5
                        "Amount",
                        "Remark"
                    },
                     new int[] 
                    {
                        350, 100, 350
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { });
            Sm.GrdFormatDec(Grd1, new int[] { 1 }, 0);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, ChkCancelInd, LueVdCode, 
                        TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark, 
                        LueAcType, LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, 
                        DteDueDt, LueCurCode, TxtRateAmt, TxtAmt, 
                        TxtPaymentUser, LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueVdCode, TxtPaymentUser, LuePaymentType, LueBankAcCode, 
                        LueCurCode, TxtRateAmt, MeeRemark,
                    }, false);
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueVdCode, TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark, LueAcType, 
                LueBankAcCode, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                LueCurCode, TxtPaymentUser, LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, 
                TxtPaidToBankAcNo
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtRateAmt, TxtAmt 
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 1 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOutgoingPaymentNoInvoiceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
            Sl.SetLueVdCode(ref LueVdCode);
            Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
            Sm.SetLue(LueAcType, "C");
            TxtRateAmt.EditValue = Sm.FormatNum(1, 0);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "VoucherReq", "VoucherReqDtl" };

            var l = new List<VoucherReqHdr>();
            var ldtl = new List<VoucherReqDtl>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CancelInd, A.DocType, ");
            SQL.AppendLine(" A.AcType, A.VoucherDocNo, ");
            SQL.AppendLine(" (Select DATE_FORMAT(DocDt,'%d %M %Y') As DocDt from tblvoucherhdr Where DocNo=A.VoucherDocNo Limit 1) As VoucherDocDt, ");
            SQL.AppendLine(" Concat(IfNull(C.BankAcNo, ''), ' [', IfNull(C.BankAcNm, ''), ']') As BankAcc, ");
            SQL.AppendLine(" (Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1) As PaymentType, ");
            SQL.AppendLine(" A.GiroNo,E.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, F.UserName As PICName, A.Amt As AmtHdr, A.Remark As RemarkHdr, A.CurCode, A.DocEnclosure, ");
            SQL.AppendLine(" ifnull(Concat(A.PaymentUser, '     ', I.VdName), ifnull(I.VdName, A.PaymentUser)) As PaymentUser, G.DeptName ");
            SQL.AppendLine(" From TblVoucherRequestHdr A ");
            SQL.AppendLine(" Left Join TblBankAccount C On A.BankAcCode=C.BankAcCode ");
            SQL.AppendLine(" Left Join TblBank D On C.BankCode=D.BankCode ");
            SQL.AppendLine(" Left Join TblBank E On A.BankCode=E.BankCode ");
            SQL.AppendLine(" Left Join TblUser F On A.PIC=F.UserCode ");
            SQL.AppendLine(" Left Join TblDepartment G On A.DeptCode = G.DeptCode ");
            SQL.AppendLine(" left Join TblOutgoingPaymentNoInvoiceHdr H On A.DocNo = H.VoucherRequestDocNo ");
            SQL.AppendLine(" left Join TblVendor I On H.VdCode = I.VdCode ");
            SQL.AppendLine(" Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",

                         //1-5
                         "CompanyAddress",
                         "CompanyPhone",
                         "DocNo", 
                         "DocDt", 
                         "CancelInd",
 
                         //6-10
                         "DocType", 
                         "AcType",
                         "VoucherDocNo", 
                         "VoucherDocDt", 
                         "BankAcc", 
                         
                         //11-15
                         "PaymentType",
                         "GiroNo",
                         "GiroBankName", 
                         "GiroDueDt", 
                         "PICName",
 
                         //16-20
                         "AmtHdr", 
                         "RemarkHdr",
                         "CompanyLogo",
                         "CurCode",
                         "DocEnclosure",

                         //21-22
                         "PaymentUser",
                         "DeptName"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VoucherReqHdr()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),
                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyPhone = Sm.DrStr(dr, c[2]),

                            DocNo = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            CancelInd = Sm.DrStr(dr, c[5]),
                            DocType = Sm.DrStr(dr, c[6]),
                            AcType = Sm.DrStr(dr, c[7]),
                            VoucherDocNo = Sm.DrStr(dr, c[8]),
                            VoucherDocDt = Sm.DrStr(dr, c[9]),
                            BankAcc = Sm.DrStr(dr, c[10]),
                            PaymentType = Sm.DrStr(dr, c[11]),
                            GiroNo = Sm.DrStr(dr, c[12]),
                            GiroBankName = Sm.DrStr(dr, c[13]),
                            GiroDueDt = Sm.DrStr(dr, c[14]),
                            PICName = Sm.DrStr(dr, c[15]),
                            AmtHdr = Sm.DrDec(dr, c[16]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[16])),
                            RemarkHdr = Sm.DrStr(dr, c[17]),

                            CompanyLogo = Sm.DrStr(dr, c[18]),
                            CurCode = Sm.DrStr(dr, c[19]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            DocEnclosure = Sm.DrDec(dr, c[20]),
                            PaymentUser = Sm.DrStr(dr, c[21]),
                            Department = Sm.DrStr(dr, c[22])
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = "Select B.DNo, F.Description As Description,  B.Amt, B.Remark " +
                "From TblOutgoingPaymentNoInvoiceHdr A " +
                "Inner Join TblOutgoingPaymentNoInvoiceDtl B On A.DocNo=B.DocNo " +
                "Inner Join TblVendor C On A.VdCode=C.VdCode " +
                "Left Join TblVoucherRequestDtl F On F.DocNo = A.VoucherRequestDocNo And F.Dno=B.Dno " +
                "Where A.DocNo=@DocNo Order By B.DNo ";
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DNo",

                         //1-5
                         "Description",
                         "Amt",
                         "Remark" 
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new VoucherReqDtl()
                        {
                            DNo = Sm.DrStr(drDtl, cDtl[0]),
                            Description = Sm.DrStr(drDtl, cDtl[1]),
                            Amt = Sm.DrDec(drDtl, cDtl[2]),

                            Remark = Sm.DrStr(drDtl, cDtl[3])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            Sm.PrintReport("OutgoingPayment", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0, 1 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex); ComputeAmt();
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 1 }, e);
            ComputeAmt();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;


            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OutgoingPaymentNoInvoice", "TblOutgoingPaymentNoInvoiceHdr");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOutgoingPaymentNoInvoiceHdr(DocNo, VoucherRequestDocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveOutgoingPaymentNoInvoiceDtl(DocNo, Row));


            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document date") ||
                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsPaymentTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 invoice document.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Description is empty."))
                    return true;
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private MySqlCommand SaveOutgoingPaymentNoInvoiceHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOutgoingPaymentNoInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, AcType, VoucherRequestDocNo, VdCode, PaymentUser, PaidToBankCode, PaidToBankBranch, ");
            SQL.AppendLine("PaidToBankAcName, PaidToBankAcNo, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, CurCode, RateAmt, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelInd, 'O', @AcType, @VoucherRequestDocNo, @VdCode, ");
            SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt,");
            SQL.AppendLine("@CurCode, @RateAmt, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            //SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select T.DocType, @OutgoingPaymentDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblDocApprovalSetting T ");
            //SQL.AppendLine("Where T.DocType='OutgoingPayment'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            if (Sm.GetLue(LuePaidToBankCode).Length == 0)
                Sm.CmParam<String>(ref cm, "@PaidToBankCode", "");
            else
                Sm.CmParam<String>(ref cm, "@PaidToBankCode", Sm.Left(Sm.GetLue(LuePaidToBankCode), Sm.GetLue(LuePaidToBankCode).Length - 3));
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveOutgoingPaymentNoInvoiceDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblOutgoingPaymentNoInvoiceDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");
            string type;

            if (Sm.GetLue(LueAcType) == "C")
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            var SQL = new StringBuilder();

            if (type == "")
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
                SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string OutgoingPaymentDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', (Select ParValue From TblParameter Where ParCode='OutgoingPaymentDeptCode'), 'P', Null, @AcType, @BankAcCode, @PaymentType, @GiroNo, @BankCode, @DueDt, (Select UserCode From TblUser Where UserCode=@CreateBy Limit 1), @DocEnclosure, @CurCode, @Amt*@RateAmt, @PaymentUser, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @OutgoingPaymentDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='OutgoingPaymentNoInvoice'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@OutgoingPaymentDocNo", OutgoingPaymentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@DocEnclosure", 0);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Remark", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelOutgoingPaymentHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document number", false) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblOutgoingPaymentNoInvoiceHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelOutgoingPaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOutgoingPaymentNoInvoiceHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOutgoingPaymentWithoutInvoiceHdr(DocNo);
                ShowOutgoingPaymentWithoutInvoiceDtl(DocNo);
                ComputeAmt();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowOutgoingPaymentWithoutInvoiceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, ");
            SQL.AppendLine("(Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else '' End) As StatusDesc, ");
            SQL.AppendLine("B.VoucherDocNo ");
            SQL.AppendLine("From TblOutgoingPaymentNoInvoiceHdr A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt",  "CancelInd", "StatusDesc", "AcType", "VoucherRequestDocNo",  

                        //6-10
                        "VdCode", "PaymentUser", "PaidToBankCode", "PaidToBankBranch", "PaidToBankAcName",  

                        //11-15
                        "PaidToBankAcNo", "PaymentType", "BankAcCode", "BankCode", "GiroNo",   

                        //16-20
                        "DueDt", "CurCode", "RateAmt", "Amt", "Remark", 
                        
                        //21-22
                        "StatusDesc", "VoucherDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueAcType, Sm.DrStr(dr, c[4]));
                        TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[6]));
                        TxtPaymentUser.EditValue = Sm.DrStr(dr, c[7]);
                        SetLuePaidToBankCode(ref LuePaidToBankCode, "", "1");
                        Sm.SetLue(LuePaidToBankCode, Sm.DrStr(dr, c[8]));
                        TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[9]);
                        TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[14]));
                        TxtGiroNo.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[17]));
                        TxtRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[19]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[20]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[21]);
                        TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[22]);
                    }, true
                );
        }

        private void ShowOutgoingPaymentWithoutInvoiceDtl(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Description, Amt, Remark ");
                SQL.AppendLine("From TblOutgoingPaymentNoInvoiceDtl ");
                SQL.AppendLine("Where DocNo=@DocNo Order By DNo");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "Description", "Amt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    }, false, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1 });
                Sm.FocusGrd(Grd1, 0, 1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private bool IsInvoiceEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        internal void ComputeAmt()
        {
            decimal Amt = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                  Amt += Sm.GetGrdDec(Grd1, Row, 1);
                }
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void SetLuePaidToBankCode(ref DXE.LookUpEdit Lue, string VdCode, string Type)
        {
            try
            {
                //Type = 1 -> For View
                //Type = 2 -> For Insert

                var SQL = new StringBuilder();

                if (Type == "1")
                {
                    SQL.AppendLine("Select BankCode As Col1, BankName As Col2, ");
                    SQL.AppendLine("'-' As Col3, 'xxx' As Col4  ");
                    SQL.AppendLine("From TblBank Order By BankName;");
                }
                else
                {
                    SQL.AppendLine("Select Concat(A.BankCode, A.DNo) As Col1, B.BankName As Col2, ");
                    SQL.AppendLine("Trim(Concat(");
                    SQL.AppendLine("    Case When IfNull(A.BankBranch,'')='' Then '' Else Concat('Branch : ', A.BankBranch) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcName,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Then '' Else ', ' End, 'Name : ', A.BankAcName) End,  ");
                    SQL.AppendLine("    Case When IfNull(A.BankAcNo,'')='' Then '' Else Concat(Case When IfNull(A.BankBranch,'')='' Or IfNull(A.BankAcName,'')='' Then '' Else ', ' End, 'Account Number : ', A.BankAcNo) End ");
                    SQL.AppendLine(")) As Col3, ");
                    SQL.AppendLine("DNo As Col4  ");
                    SQL.AppendLine("From TblVendorBankAccount A ");
                    SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
                    SQL.AppendLine("Where VdCode='" + VdCode + "' ");
                    SQL.AppendLine("Order By B.BankName, A.DNo; ");
                }
                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    0, 170, 300, 0, false, true, true, false, "Code", "Bank", "Bank Branch/Account Name/Account No.", "DNo", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowPaidToBankCodeInfo(string VdCode, string DNo)
        {
            var cm = new MySqlCommand();
            try
            {
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select BankBranch, BankAcName, BankAcNo " +
                        "From TblVendorBankAccount " +
                        "Where VdCode=@VdCode And DNo=@DNo; ",
                        new string[] { "BankBranch", "BankAcName", "BankAcNo" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[2]);
                        }, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedInvoice()
        {
            var SQL = "";
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion


        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LuePaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
            });

            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, true);

            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));

            if (Sm.GetLue(LueVdCode).Length != 0)
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LuePaidToBankCode }, false);
                SetLuePaidToBankCode(ref LuePaidToBankCode, Sm.GetLue(LueVdCode), "2");
            }

            ClearGrd();
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LuePaidToBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaidToBankCode, new Sm.RefreshLue3(SetLuePaidToBankCode), Sm.GetLue(LueVdCode), "2");
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo
            });
            if (Sm.GetLue(LuePaidToBankCode).Length != 0)
                ShowPaidToBankCodeInfo(
                    Sm.GetLue(LueVdCode),
                    LuePaidToBankCode.GetColumnValue("Col4").ToString()
                    );
        }

        #endregion

       

       

    }
}
