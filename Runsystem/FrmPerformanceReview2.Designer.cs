﻿namespace RunSystem
{
    partial class FrmPerformanceReview2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPerformanceReview2));
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtEvaluator = new DevExpress.XtraEditors.TextEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.TxtPICCode = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.LuePeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.LueGrdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtTotalScore = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPICName = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnGoals = new DevExpress.XtraEditors.SimpleButton();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtGoalsProcessDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.Tpg = new System.Windows.Forms.TabControl();
            this.TpgFinancialPerpective = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtTotalWeightFinancial = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalScoreFinancial = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TpgCustomerPerspective = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtTotalScoreCustomer = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalWeightCustomer = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TpgInternalPerspective = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtTotalScoreInternalProcess = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalWeightInternalProcess = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TpgLearningPerspective = new System.Windows.Forms.TabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtTotalScoreLearning = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalWeightLearning = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.TpgActionPlan = new System.Windows.Forms.TabPage();
            this.LueNoteRecommendation = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEvaluator.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePeriod.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoalsProcessDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.Tpg.SuspendLayout();
            this.TpgFinancialPerpective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightFinancial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreFinancial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            this.TpgCustomerPerspective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            this.TpgInternalPerspective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreInternalProcess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightInternalProcess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            this.TpgLearningPerspective.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreLearning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightLearning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            this.TpgActionPlan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueNoteRecommendation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 427);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tpg);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 427);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.TxtEvaluator);
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.TxtPICCode);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.MeeCancelReason);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.LuePeriod);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.LueYr);
            this.panel3.Controls.Add(this.TxtPICName);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.BtnGoals);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.TxtGoalsProcessDocNo);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 218);
            this.panel3.TabIndex = 10;
            // 
            // TxtEvaluator
            // 
            this.TxtEvaluator.EnterMoveNextControl = true;
            this.TxtEvaluator.Location = new System.Drawing.Point(146, 151);
            this.TxtEvaluator.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEvaluator.Name = "TxtEvaluator";
            this.TxtEvaluator.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEvaluator.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEvaluator.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEvaluator.Properties.Appearance.Options.UseFont = true;
            this.TxtEvaluator.Properties.MaxLength = 30;
            this.TxtEvaluator.Size = new System.Drawing.Size(286, 20);
            this.TxtEvaluator.TabIndex = 66;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(146, 45);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 250;
            this.TxtStatus.Size = new System.Drawing.Size(286, 20);
            this.TxtStatus.TabIndex = 65;
            // 
            // TxtPICCode
            // 
            this.TxtPICCode.EnterMoveNextControl = true;
            this.TxtPICCode.Location = new System.Drawing.Point(146, 109);
            this.TxtPICCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICCode.Name = "TxtPICCode";
            this.TxtPICCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPICCode.Properties.MaxLength = 30;
            this.TxtPICCode.Size = new System.Drawing.Size(286, 20);
            this.TxtPICCode.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(51, 115);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 14);
            this.label10.TabIndex = 24;
            this.label10.Text = "Employee Code";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 69);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(135, 14);
            this.label19.TabIndex = 17;
            this.label19.Text = "Reason For Cancellation";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(146, 66);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(286, 20);
            this.MeeCancelReason.TabIndex = 18;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(441, 64);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(65, 22);
            this.ChkCancelInd.TabIndex = 19;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // LuePeriod
            // 
            this.LuePeriod.EnterMoveNextControl = true;
            this.LuePeriod.Location = new System.Drawing.Point(146, 172);
            this.LuePeriod.Margin = new System.Windows.Forms.Padding(5);
            this.LuePeriod.Name = "LuePeriod";
            this.LuePeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePeriod.Properties.DropDownRows = 30;
            this.LuePeriod.Properties.NullText = "[Empty]";
            this.LuePeriod.Properties.PopupWidth = 300;
            this.LuePeriod.Size = new System.Drawing.Size(125, 20);
            this.LuePeriod.TabIndex = 31;
            this.LuePeriod.ToolTip = "F4 : Show/hide list";
            this.LuePeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(101, 48);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 15;
            this.label7.Text = "Status";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.LueGrdCode);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.TxtTotalScore);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(567, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(201, 214);
            this.panel6.TabIndex = 36;
            // 
            // LueGrdCode
            // 
            this.LueGrdCode.EnterMoveNextControl = true;
            this.LueGrdCode.Location = new System.Drawing.Point(78, 185);
            this.LueGrdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdCode.Name = "LueGrdCode";
            this.LueGrdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdCode.Properties.DropDownRows = 12;
            this.LueGrdCode.Properties.NullText = "[Empty]";
            this.LueGrdCode.Properties.PopupWidth = 500;
            this.LueGrdCode.Size = new System.Drawing.Size(108, 20);
            this.LueGrdCode.TabIndex = 40;
            this.LueGrdCode.ToolTip = "F4 : Show/hide list";
            this.LueGrdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdCode.EditValueChanged += new System.EventHandler(this.LueGrdCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 167);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 14);
            this.label4.TabIndex = 37;
            this.label4.Text = "Total Score";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalScore
            // 
            this.TxtTotalScore.EnterMoveNextControl = true;
            this.TxtTotalScore.Location = new System.Drawing.Point(78, 164);
            this.TxtTotalScore.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalScore.Name = "TxtTotalScore";
            this.TxtTotalScore.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalScore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalScore.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalScore.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalScore.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalScore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalScore.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalScore.TabIndex = 38;
            this.TxtTotalScore.EditValueChanged += new System.EventHandler(this.TxtTotalScore_EditValueChanged);
            this.TxtTotalScore.Validated += new System.EventHandler(this.TxtTotalScore_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(34, 188);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 14);
            this.label5.TabIndex = 39;
            this.label5.Text = "Grade";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(303, 175);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 14);
            this.label9.TabIndex = 32;
            this.label9.Text = "Year";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(339, 172);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(93, 20);
            this.LueYr.TabIndex = 33;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtPICName
            // 
            this.TxtPICName.EnterMoveNextControl = true;
            this.TxtPICName.Location = new System.Drawing.Point(146, 130);
            this.TxtPICName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPICName.Name = "TxtPICName";
            this.TxtPICName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPICName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPICName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPICName.Properties.Appearance.Options.UseFont = true;
            this.TxtPICName.Properties.MaxLength = 30;
            this.TxtPICName.Size = new System.Drawing.Size(286, 20);
            this.TxtPICName.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(47, 136);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 14);
            this.label6.TabIndex = 26;
            this.label6.Text = "Employee Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(102, 175);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 14);
            this.label3.TabIndex = 30;
            this.label3.Text = "Period";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnGoals
            // 
            this.BtnGoals.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnGoals.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnGoals.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGoals.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnGoals.Appearance.Options.UseBackColor = true;
            this.BtnGoals.Appearance.Options.UseFont = true;
            this.BtnGoals.Appearance.Options.UseForeColor = true;
            this.BtnGoals.Appearance.Options.UseTextOptions = true;
            this.BtnGoals.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnGoals.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnGoals.Image = ((System.Drawing.Image)(resources.GetObject("BtnGoals.Image")));
            this.BtnGoals.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnGoals.Location = new System.Drawing.Point(440, 87);
            this.BtnGoals.Name = "BtnGoals";
            this.BtnGoals.Size = new System.Drawing.Size(24, 21);
            this.BtnGoals.TabIndex = 22;
            this.BtnGoals.ToolTip = "Find Goals Document";
            this.BtnGoals.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnGoals.ToolTipTitle = "Run System";
            this.BtnGoals.Click += new System.EventHandler(this.BtnGoals_Click);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(146, 193);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 5000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(286, 20);
            this.MeeRemark.TabIndex = 35;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(96, 195);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 34;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(74, 157);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 14);
            this.label15.TabIndex = 28;
            this.label15.Text = "Appraiser 1";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGoalsProcessDocNo
            // 
            this.TxtGoalsProcessDocNo.EnterMoveNextControl = true;
            this.TxtGoalsProcessDocNo.Location = new System.Drawing.Point(146, 88);
            this.TxtGoalsProcessDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGoalsProcessDocNo.Name = "TxtGoalsProcessDocNo";
            this.TxtGoalsProcessDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGoalsProcessDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGoalsProcessDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGoalsProcessDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGoalsProcessDocNo.Properties.MaxLength = 30;
            this.TxtGoalsProcessDocNo.Size = new System.Drawing.Size(286, 20);
            this.TxtGoalsProcessDocNo.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(16, 94);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 14);
            this.label12.TabIndex = 20;
            this.label12.Text = "Performance Review#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(146, 24);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(125, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(110, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(146, 3);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(286, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(70, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tpg
            // 
            this.Tpg.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.Tpg.Controls.Add(this.TpgFinancialPerpective);
            this.Tpg.Controls.Add(this.TpgCustomerPerspective);
            this.Tpg.Controls.Add(this.TpgInternalPerspective);
            this.Tpg.Controls.Add(this.TpgLearningPerspective);
            this.Tpg.Controls.Add(this.TpgActionPlan);
            this.Tpg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tpg.Location = new System.Drawing.Point(0, 218);
            this.Tpg.Multiline = true;
            this.Tpg.Name = "Tpg";
            this.Tpg.SelectedIndex = 0;
            this.Tpg.ShowToolTips = true;
            this.Tpg.Size = new System.Drawing.Size(772, 209);
            this.Tpg.TabIndex = 41;
            // 
            // TpgFinancialPerpective
            // 
            this.TpgFinancialPerpective.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgFinancialPerpective.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgFinancialPerpective.Controls.Add(this.Grd1);
            this.TpgFinancialPerpective.Controls.Add(this.panel7);
            this.TpgFinancialPerpective.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgFinancialPerpective.Location = new System.Drawing.Point(4, 26);
            this.TpgFinancialPerpective.Name = "TpgFinancialPerpective";
            this.TpgFinancialPerpective.Size = new System.Drawing.Size(764, 179);
            this.TpgFinancialPerpective.TabIndex = 0;
            this.TpgFinancialPerpective.Text = "Financial Perspective";
            this.TpgFinancialPerpective.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 39);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(760, 136);
            this.Grd1.TabIndex = 47;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.TxtTotalWeightFinancial);
            this.panel7.Controls.Add(this.TxtTotalScoreFinancial);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Controls.Add(this.label23);
            this.panel7.Controls.Add(this.lookUpEdit3);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.textEdit9);
            this.panel7.Controls.Add(this.label25);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(760, 39);
            this.panel7.TabIndex = 42;
            // 
            // TxtTotalWeightFinancial
            // 
            this.TxtTotalWeightFinancial.EnterMoveNextControl = true;
            this.TxtTotalWeightFinancial.Location = new System.Drawing.Point(94, 9);
            this.TxtTotalWeightFinancial.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWeightFinancial.Name = "TxtTotalWeightFinancial";
            this.TxtTotalWeightFinancial.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalWeightFinancial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWeightFinancial.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWeightFinancial.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWeightFinancial.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWeightFinancial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWeightFinancial.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalWeightFinancial.TabIndex = 44;
            // 
            // TxtTotalScoreFinancial
            // 
            this.TxtTotalScoreFinancial.EnterMoveNextControl = true;
            this.TxtTotalScoreFinancial.Location = new System.Drawing.Point(326, 9);
            this.TxtTotalScoreFinancial.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalScoreFinancial.Name = "TxtTotalScoreFinancial";
            this.TxtTotalScoreFinancial.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalScoreFinancial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalScoreFinancial.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalScoreFinancial.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalScoreFinancial.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalScoreFinancial.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalScoreFinancial.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalScoreFinancial.TabIndex = 46;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(246, 11);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 14);
            this.label22.TabIndex = 45;
            this.label22.Text = "Total Score";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(5, 11);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 14);
            this.label23.TabIndex = 43;
            this.label23.Text = "Total Weight";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.EnterMoveNextControl = true;
            this.lookUpEdit3.Location = new System.Drawing.Point(78, 185);
            this.lookUpEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.DropDownRows = 12;
            this.lookUpEdit3.Properties.NullText = "[Empty]";
            this.lookUpEdit3.Properties.PopupWidth = 500;
            this.lookUpEdit3.Size = new System.Drawing.Size(108, 20);
            this.lookUpEdit3.TabIndex = 40;
            this.lookUpEdit3.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(4, 167);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 14);
            this.label24.TabIndex = 37;
            this.label24.Text = "Total Score";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit9
            // 
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(78, 164);
            this.textEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit9.Size = new System.Drawing.Size(108, 20);
            this.textEdit9.TabIndex = 38;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(34, 188);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 14);
            this.label25.TabIndex = 39;
            this.label25.Text = "Grade";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgCustomerPerspective
            // 
            this.TpgCustomerPerspective.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgCustomerPerspective.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgCustomerPerspective.Controls.Add(this.Grd2);
            this.TpgCustomerPerspective.Controls.Add(this.panel5);
            this.TpgCustomerPerspective.Location = new System.Drawing.Point(4, 26);
            this.TpgCustomerPerspective.Name = "TpgCustomerPerspective";
            this.TpgCustomerPerspective.Size = new System.Drawing.Size(764, 179);
            this.TpgCustomerPerspective.TabIndex = 1;
            this.TpgCustomerPerspective.Text = "Customer Perspective";
            this.TpgCustomerPerspective.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 44);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(760, 131);
            this.Grd2.TabIndex = 53;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtTotalScoreCustomer);
            this.panel5.Controls.Add(this.TxtTotalWeightCustomer);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.lookUpEdit2);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.textEdit6);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(760, 44);
            this.panel5.TabIndex = 47;
            // 
            // TxtTotalScoreCustomer
            // 
            this.TxtTotalScoreCustomer.EnterMoveNextControl = true;
            this.TxtTotalScoreCustomer.Location = new System.Drawing.Point(326, 9);
            this.TxtTotalScoreCustomer.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalScoreCustomer.Name = "TxtTotalScoreCustomer";
            this.TxtTotalScoreCustomer.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalScoreCustomer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalScoreCustomer.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalScoreCustomer.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalScoreCustomer.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalScoreCustomer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalScoreCustomer.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalScoreCustomer.TabIndex = 52;
            // 
            // TxtTotalWeightCustomer
            // 
            this.TxtTotalWeightCustomer.EnterMoveNextControl = true;
            this.TxtTotalWeightCustomer.Location = new System.Drawing.Point(94, 8);
            this.TxtTotalWeightCustomer.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWeightCustomer.Name = "TxtTotalWeightCustomer";
            this.TxtTotalWeightCustomer.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalWeightCustomer.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWeightCustomer.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWeightCustomer.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWeightCustomer.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWeightCustomer.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWeightCustomer.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalWeightCustomer.TabIndex = 50;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(246, 11);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 14);
            this.label17.TabIndex = 51;
            this.label17.Text = "Total Score";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(5, 11);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 14);
            this.label18.TabIndex = 49;
            this.label18.Text = "Total Weight";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EnterMoveNextControl = true;
            this.lookUpEdit2.Location = new System.Drawing.Point(78, 185);
            this.lookUpEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.DropDownRows = 12;
            this.lookUpEdit2.Properties.NullText = "[Empty]";
            this.lookUpEdit2.Properties.PopupWidth = 500;
            this.lookUpEdit2.Size = new System.Drawing.Size(108, 20);
            this.lookUpEdit2.TabIndex = 40;
            this.lookUpEdit2.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(4, 167);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 14);
            this.label20.TabIndex = 37;
            this.label20.Text = "Total Score";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(78, 164);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit6.Size = new System.Drawing.Size(108, 20);
            this.textEdit6.TabIndex = 38;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(34, 188);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 14);
            this.label21.TabIndex = 39;
            this.label21.Text = "Grade";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgInternalPerspective
            // 
            this.TpgInternalPerspective.Controls.Add(this.Grd3);
            this.TpgInternalPerspective.Controls.Add(this.panel4);
            this.TpgInternalPerspective.Location = new System.Drawing.Point(4, 26);
            this.TpgInternalPerspective.Name = "TpgInternalPerspective";
            this.TpgInternalPerspective.Padding = new System.Windows.Forms.Padding(3);
            this.TpgInternalPerspective.Size = new System.Drawing.Size(764, 179);
            this.TpgInternalPerspective.TabIndex = 3;
            this.TpgInternalPerspective.Text = "Internal Process Perspective";
            this.TpgInternalPerspective.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(3, 44);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(758, 132);
            this.Grd3.TabIndex = 60;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TxtTotalScoreInternalProcess);
            this.panel4.Controls.Add(this.TxtTotalWeightInternalProcess);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.lookUpEdit1);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.textEdit3);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(758, 41);
            this.panel4.TabIndex = 55;
            // 
            // TxtTotalScoreInternalProcess
            // 
            this.TxtTotalScoreInternalProcess.EnterMoveNextControl = true;
            this.TxtTotalScoreInternalProcess.Location = new System.Drawing.Point(325, 9);
            this.TxtTotalScoreInternalProcess.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalScoreInternalProcess.Name = "TxtTotalScoreInternalProcess";
            this.TxtTotalScoreInternalProcess.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalScoreInternalProcess.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalScoreInternalProcess.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalScoreInternalProcess.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalScoreInternalProcess.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalScoreInternalProcess.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalScoreInternalProcess.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalScoreInternalProcess.TabIndex = 59;
            // 
            // TxtTotalWeightInternalProcess
            // 
            this.TxtTotalWeightInternalProcess.EnterMoveNextControl = true;
            this.TxtTotalWeightInternalProcess.Location = new System.Drawing.Point(94, 8);
            this.TxtTotalWeightInternalProcess.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWeightInternalProcess.Name = "TxtTotalWeightInternalProcess";
            this.TxtTotalWeightInternalProcess.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalWeightInternalProcess.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWeightInternalProcess.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWeightInternalProcess.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWeightInternalProcess.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWeightInternalProcess.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWeightInternalProcess.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalWeightInternalProcess.TabIndex = 57;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(246, 11);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 14);
            this.label11.TabIndex = 58;
            this.label11.Text = "Total Score";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(5, 11);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 14);
            this.label13.TabIndex = 56;
            this.label13.Text = "Total Weight";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EnterMoveNextControl = true;
            this.lookUpEdit1.Location = new System.Drawing.Point(78, 185);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DropDownRows = 12;
            this.lookUpEdit1.Properties.NullText = "[Empty]";
            this.lookUpEdit1.Properties.PopupWidth = 500;
            this.lookUpEdit1.Size = new System.Drawing.Size(108, 20);
            this.lookUpEdit1.TabIndex = 40;
            this.lookUpEdit1.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(4, 167);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 14);
            this.label14.TabIndex = 37;
            this.label14.Text = "Total Score";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(78, 164);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit3.Size = new System.Drawing.Size(108, 20);
            this.textEdit3.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(34, 188);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 14);
            this.label16.TabIndex = 39;
            this.label16.Text = "Grade";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgLearningPerspective
            // 
            this.TpgLearningPerspective.Controls.Add(this.Grd4);
            this.TpgLearningPerspective.Controls.Add(this.panel8);
            this.TpgLearningPerspective.Location = new System.Drawing.Point(4, 26);
            this.TpgLearningPerspective.Name = "TpgLearningPerspective";
            this.TpgLearningPerspective.Padding = new System.Windows.Forms.Padding(3);
            this.TpgLearningPerspective.Size = new System.Drawing.Size(764, 179);
            this.TpgLearningPerspective.TabIndex = 4;
            this.TpgLearningPerspective.Text = "Learning & Growth Perspective";
            this.TpgLearningPerspective.UseVisualStyleBackColor = true;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(3, 45);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(758, 131);
            this.Grd4.TabIndex = 65;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.TxtTotalScoreLearning);
            this.panel8.Controls.Add(this.TxtTotalWeightLearning);
            this.panel8.Controls.Add(this.label26);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Controls.Add(this.lookUpEdit4);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Controls.Add(this.textEdit12);
            this.panel8.Controls.Add(this.label29);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(758, 42);
            this.panel8.TabIndex = 61;
            // 
            // TxtTotalScoreLearning
            // 
            this.TxtTotalScoreLearning.EnterMoveNextControl = true;
            this.TxtTotalScoreLearning.Location = new System.Drawing.Point(325, 9);
            this.TxtTotalScoreLearning.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalScoreLearning.Name = "TxtTotalScoreLearning";
            this.TxtTotalScoreLearning.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalScoreLearning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalScoreLearning.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalScoreLearning.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalScoreLearning.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalScoreLearning.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalScoreLearning.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalScoreLearning.TabIndex = 64;
            // 
            // TxtTotalWeightLearning
            // 
            this.TxtTotalWeightLearning.EnterMoveNextControl = true;
            this.TxtTotalWeightLearning.Location = new System.Drawing.Point(94, 8);
            this.TxtTotalWeightLearning.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalWeightLearning.Name = "TxtTotalWeightLearning";
            this.TxtTotalWeightLearning.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalWeightLearning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalWeightLearning.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalWeightLearning.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalWeightLearning.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalWeightLearning.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalWeightLearning.Size = new System.Drawing.Size(108, 20);
            this.TxtTotalWeightLearning.TabIndex = 63;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(246, 11);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 14);
            this.label26.TabIndex = 64;
            this.label26.Text = "Total Score";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(5, 11);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(79, 14);
            this.label27.TabIndex = 62;
            this.label27.Text = "Total Weight";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(78, 185);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 12;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 500;
            this.lookUpEdit4.Size = new System.Drawing.Size(108, 20);
            this.lookUpEdit4.TabIndex = 40;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(4, 167);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(70, 14);
            this.label28.TabIndex = 37;
            this.label28.Text = "Total Score";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit12
            // 
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(78, 164);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit12.Size = new System.Drawing.Size(108, 20);
            this.textEdit12.TabIndex = 38;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(34, 188);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(39, 14);
            this.label29.TabIndex = 39;
            this.label29.Text = "Grade";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgActionPlan
            // 
            this.TpgActionPlan.Controls.Add(this.LueNoteRecommendation);
            this.TpgActionPlan.Controls.Add(this.Grd5);
            this.TpgActionPlan.Location = new System.Drawing.Point(4, 26);
            this.TpgActionPlan.Name = "TpgActionPlan";
            this.TpgActionPlan.Size = new System.Drawing.Size(764, 179);
            this.TpgActionPlan.TabIndex = 5;
            this.TpgActionPlan.Text = "Action Plan";
            this.TpgActionPlan.UseVisualStyleBackColor = true;
            // 
            // LueNoteRecommendation
            // 
            this.LueNoteRecommendation.EnterMoveNextControl = true;
            this.LueNoteRecommendation.Location = new System.Drawing.Point(239, 79);
            this.LueNoteRecommendation.Margin = new System.Windows.Forms.Padding(5);
            this.LueNoteRecommendation.Name = "LueNoteRecommendation";
            this.LueNoteRecommendation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNoteRecommendation.Properties.Appearance.Options.UseFont = true;
            this.LueNoteRecommendation.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNoteRecommendation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueNoteRecommendation.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNoteRecommendation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueNoteRecommendation.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNoteRecommendation.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueNoteRecommendation.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNoteRecommendation.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueNoteRecommendation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueNoteRecommendation.Properties.DropDownRows = 20;
            this.LueNoteRecommendation.Properties.NullText = "[Empty]";
            this.LueNoteRecommendation.Properties.PopupWidth = 500;
            this.LueNoteRecommendation.Size = new System.Drawing.Size(286, 20);
            this.LueNoteRecommendation.TabIndex = 36;
            this.LueNoteRecommendation.ToolTip = "F4 : Show/hide list";
            this.LueNoteRecommendation.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueNoteRecommendation.EditValueChanged += new System.EventHandler(this.LueNoteRecommendation_EditValueChanged);
            this.LueNoteRecommendation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueNoteRecommendation_KeyDown);
            this.LueNoteRecommendation.Leave += new System.EventHandler(this.LueNoteRecommendation_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(764, 179);
            this.Grd5.TabIndex = 66;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // FrmPerformanceReview2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 427);
            this.Name = "FrmPerformanceReview2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEvaluator.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePeriod.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPICName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGoalsProcessDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.Tpg.ResumeLayout(false);
            this.TpgFinancialPerpective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightFinancial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreFinancial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            this.TpgCustomerPerspective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            this.TpgInternalPerspective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreInternalProcess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightInternalProcess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            this.TpgLearningPerspective.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalScoreLearning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalWeightLearning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            this.TpgActionPlan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueNoteRecommendation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label9;
        protected internal DevExpress.XtraEditors.TextEdit TxtPICName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnGoals;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label15;
        protected internal DevExpress.XtraEditors.TextEdit TxtGoalsProcessDocNo;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtTotalScore;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl Tpg;
        private System.Windows.Forms.TabPage TpgFinancialPerpective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.TabPage TpgCustomerPerspective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabPage TpgInternalPerspective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgLearningPerspective;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.TabPage TpgActionPlan;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        protected internal DevExpress.XtraEditors.TextEdit TxtPICCode;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.LookUpEdit LueYr;
        private DevExpress.XtraEditors.TextEdit TxtStatus;
        internal DevExpress.XtraEditors.LookUpEdit LueGrdCode;
        internal DevExpress.XtraEditors.LookUpEdit LuePeriod;
        protected internal DevExpress.XtraEditors.TextEdit TxtEvaluator;
        private DevExpress.XtraEditors.LookUpEdit LueNoteRecommendation;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit textEdit12;
        private System.Windows.Forms.Label label29;
        protected System.Windows.Forms.Panel panel8;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit textEdit9;
        private System.Windows.Forms.Label label25;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit textEdit6;
        private System.Windows.Forms.Label label21;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtTotalScoreFinancial;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWeightFinancial;
        internal DevExpress.XtraEditors.TextEdit TxtTotalScoreCustomer;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWeightCustomer;
        internal DevExpress.XtraEditors.TextEdit TxtTotalScoreInternalProcess;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWeightInternalProcess;
        internal DevExpress.XtraEditors.TextEdit TxtTotalScoreLearning;
        internal DevExpress.XtraEditors.TextEdit TxtTotalWeightLearning;
    }
}