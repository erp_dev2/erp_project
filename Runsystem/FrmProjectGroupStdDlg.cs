﻿#region Update
/*
    07/02/2020 [TKG/IMS] new dialog
    08/05/2020 [IBL/IMS] tambah informasi SO Contract#
    28/09/2020 [TKG/IMS] tambah informasi finished goods, informasi so contract tambah kondisi status dan cancelind
    31/05/2021 [BRI/IMS] ketika SOC tidak dipilih Project berdasarkan InternalInd terchecklist. Berdasarkan parameter IsProjectGroupUseInternalInd
    05/07/2021 [TKG/IMS] tambah column internal indicator
    06/07/2021 [TKG/IMS] tambah filter internal indicator
    06/07/2021 [VIN/IMS] tambah kolom PONo
    23/12/2021 [VIN/IMS] Customer Berdasarkan SOC bukan Project Group
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectGroupStdDlg : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty;
        internal string
            mPGCode = string.Empty,
            mProjectCode = String.Empty,
            mProjectName = string.Empty,
            mCtName = string.Empty,
            mSOCDocNo = string.Empty,
            mPONo = string.Empty;


        #endregion

        #region Constructor

        public FrmProjectGroupStdDlg()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetLueInternalInd();
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code",
                        "Project's Group",
                        "Project's Code",
                        "Project's Name",
                        "Customer",

                        //6-9
                        "SO Contract#",
                        "Finished Good",
                        "Internal", 
                        "PO No"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        0, 250, 130, 250, 250,

                        //6-9
                        130, 500, 100, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 8 });
            Grd1.Cols[6].Move(1);
            Grd1.Cols[8].Move(6);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PGCode, A.PGName, A.ProjectCode, A.ProjectName, B.CtName, C.SOContractDocNo, C.ItName, A.InternalInd, C.PONo ");
            SQL.AppendLine("From TblProjectGroup A ");
            //SQL.AppendLine("Left Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select C.PGCode, A.DocNo As SOContractDocNo, A.PONo, A.CtCode, ");
            SQL.AppendLine("    Group_Concat(Distinct E.ItName Order By E.ItName Separator ', ') As ItName ");
            SQL.AppendLine("    From TblSOContractHdr A ");
            SQL.AppendLine("    Inner Join TblBOQHdr B On A.BOQDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblLOPHdr C On B.LOPDocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblSoContractDtl D On A.DocNo=D.DocNo ");
            SQL.AppendLine("    Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("    Where A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    Group By C.PGCode, A.DocNo ");
            SQL.AppendLine(") C On A.PGCode = C.PGCode ");
            SQL.AppendLine("Left Join TblCustomer B On C.CtCode=B.CtCode ");

            SQL.AppendLine("Where A.ActInd='Y' ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "A.ProjectCode", "A.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, TxtSOCDocNo.Text, new string[] { "C.SOContractDocNo" });
                if (ChkInternalInd.Checked)
                {
                    if (Sm.Left(Sm.GetLue(LueInternalInd), 1)=="I")
                        Filter = Filter + " And A.InternalInd='Y' ";
                    else
                        Filter = Filter + " And A.InternalInd='N' ";
                }
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ProjectName, C.SOContractDocNo;",
                        new string[] 
                        { 
                             //0
                             "PGCode", 
                             
                             //1-5
                             "PGName", 
                             "ProjectCode", 
                             "ProjectName",
                             "CtName",
                             "SOContractDocNo",

                             //6-7
                             "ItName",
                             "InternalInd",
                             "PONo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int r = Grd1.CurRow.Index;
                mPGCode = Sm.GetGrdStr(Grd1, r, 1);
                mProjectCode = Sm.GetGrdStr(Grd1, r, 3);
                mProjectName = Sm.GetGrdStr(Grd1, r, 4);
                mCtName = Sm.GetGrdStr(Grd1, r, 5);
                mSOCDocNo = Sm.GetGrdStr(Grd1, r, 6);
                mPONo = Sm.GetGrdStr(Grd1, r, 7);
                this.Hide();
            }
        }

        #endregion

        #region Additional Method

        private void SetLueInternalInd()
        {
            Sm.SetLookUpEdit(LueInternalInd, new string[] { null, "Internal", "Non Internal" });
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void TxtSOCDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOCDocNo_ChekcedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void LueInternalInd_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkInternalInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project group's indicator");
        }

        #endregion


        #endregion

        #endregion
    }
}
