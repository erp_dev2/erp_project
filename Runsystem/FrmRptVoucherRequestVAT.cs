﻿#region Update
/*
    05/04/2018 [TKG] tambah amount dalam idr
    24/11/2021 [NJP/PHT] Menambah Multi Profit Center Reporting Voucher Request VAT Menggunakan Parameter IsFicoUseMultiProfitCenterFilter
    14/12/2022 [RDA/TWC] penambahan dan perubahan format reporting berdasarkan param RptVoucherRequestVATFormat
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptVoucherRequestVAT : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mMainCurCode = string.Empty;
        private bool
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsAllProfitCenterSelected = false;
        private List<String> mlProfitCenter;
        private string mRptVoucherRequestVATFormat = string.Empty;

        #endregion

        #region Constructor

        public FrmRptVoucherRequestVAT(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                ChkExcludedCancelledItem.Checked = true;
                mlProfitCenter = new List<String>();
                CcbProfitCenterCode.Visible = label4.Visible = ChkProfitCenterCode.Visible = mIsFicoUseMultiProfitCenterFilter;
                if (mIsFicoUseMultiProfitCenterFilter)
                {
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
                //else
                //{
                //        label9.Top = label4.Top;
                //        ChkExcludedCancelledItem.Top = ChkProfitCenterCode.Top;
                //        panel2.Height -= CcbProfitCenterCode.Height;
                //}

                if (mRptVoucherRequestVATFormat == "2")
                {
                    label1.ForeColor = label5.ForeColor = Color.Orange;
                    label10.Visible = LueTaxGrpCode.Visible = ChkTaxGrpCode.Visible = true;
                    Sl.SetLueTaxGrpCode(ref LueTaxGrpCode);
                }
                else
                {
                    label10.Visible = LueTaxGrpCode.Visible = ChkTaxGrpCode.Visible = false;
                    label5.Visible = TxtTaxPeriod.Visible = ChkTaxPeriod.Visible = ChkDocDt.Visible = false;
                }

                if (mRptVoucherRequestVATFormat == "1")
                {
                    if (mIsFicoUseMultiProfitCenterFilter)
                    {
                        panel2.Height -= LueTaxGrpCode.Height;
                    }
                    else
                    {
                        panel2.Height -= CcbProfitCenterCode.Height + LueTaxGrpCode.Height;
                    }
                }
                else if (mRptVoucherRequestVATFormat == "2")
                {
                    if (!mIsFicoUseMultiProfitCenterFilter)
                    {
                        label10.Top = label4.Top;
                        LueTaxGrpCode.Top = CcbProfitCenterCode.Top;
                        ChkTaxGrpCode.Top = ChkProfitCenterCode.Top;
                        panel2.Height -= CcbProfitCenterCode.Height;
                    }
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsFicoUseMultiProfitCenterFilter = Sm.GetParameterBoo("IsFicoUseMultiProfitCenterFilter");
            mRptVoucherRequestVATFormat = Sm.GetParameter("RptVoucherRequestVATFormat");
            if (mRptVoucherRequestVATFormat.Length == 0) mRptVoucherRequestVATFormat = "1";
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            string Fltr = string.Empty;

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("    Case A.Status When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' When 'A' Then 'Approved' End As StatusDesc, ");
            SQL.AppendLine("    Case A.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcTypeDesc, ");
            SQL.AppendLine("    A.CurCode, A.Amt, D.MainCurAmt, ");
            SQL.AppendLine("    Case B.DocType When '1' Then 'Purchase Invoice' When '2' Then 'Purchase Return Invoice' When '3' Then 'Voucher Request Tax' End As DocTypeDesc, ");
            SQL.AppendLine("    B.DocNoIn As DocNoIO, B.Amt As Amt2, B.Amt*IfNull(B.ExcRate, 0.00) As MainCurAmt2, B.TaxInvoiceNo, B.TaxInvoiceDt, B.Remark, ");
            SQL.AppendLine("    A.VoucherRequestDocNo, C.VoucherDocNo, A.DeptCode ");
            if (mRptVoucherRequestVATFormat == "2")
                SQL.AppendLine(" ,'In' AS TypeIO, 'Vendor' AS VdCst, G.Amt AS TaxBaseAmt, G.Amt AS TaxBaseMainCurAmt, A.TaxPeriod, A.TaxGrpCode, F.TaxGrpName ");
            else
                SQL.AppendLine(" ,NULL AS TypeIO, NULL AS VdCst, 0.00 AS TaxBaseAmt, 0.00 AS TaxBaseMainCurAmt, NULL as TaxPeriod, NULL as TaxGrpCode, NULL as TaxGrpName ");
            SQL.AppendLine("    From TblVoucherRequestPPNHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestPPNDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T1.DocNo, Sum(T2.Amt*IfNull(T2.ExcRate, 0.00)) As MainCurAmt ");
            SQL.AppendLine("        From TblVoucherRequestPPNHdr T1 ");
            SQL.AppendLine("        Inner Join TblVoucherRequestPPNDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        INNER JOIN tblcostcenter T3 ON T1.DeptCode = T3.DeptCode ");
            if (mRptVoucherRequestVATFormat == "1" || DteDocDt1.Text.Length > 0) SQL.AppendLine("            And T2.TaxInvoiceDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where 1=1 ");
            //SQL.AppendLine(Fltr.Replace("A.", "T1."));
            Fltr = Filter.Replace("A.", "T1.");
            SQL.AppendLine(Fltr.Replace("E.", "T3."));
            SQL.AppendLine("        Group By T1.DocNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo ");
            SQL.AppendLine(" INNER JOIN tblcostcenter E ON A.DeptCode = E.DeptCode ");
            if (mRptVoucherRequestVATFormat == "2") 
            {
                SQL.AppendLine("INNER JOIN tbltaxgrp F ON A.TaxGrpCode = F.TaxGrpCode  ");
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("	SELECT X1.DocNo, X1.Amt FROM tblpurchaseinvoicehdr X1  ");
                //SQL.AppendLine("	-- UNION ALL ");
                //SQL.AppendLine("	-- TaxBaseAmt untuk Purchase Return Invoice ");
                //SQL.AppendLine("	-- UNION ALL ");
                //SQL.AppendLine("	-- TaxBaseAmt untuk Voucher Request Tax ");
                SQL.AppendLine(")G ON B.DocNoIn=G.DocNo  ");
            }
            if (mRptVoucherRequestVATFormat == "1" || DteDocDt1.Text.Length > 0) SQL.AppendLine("    Where (B.TaxInvoiceDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("    Case A.Status When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' When 'A' Then 'Approved' End As StatusDesc, ");
            SQL.AppendLine("    Case A.AcType When 'D' Then 'Debit' When 'C' Then 'Credit' End As AcTypeDesc, ");
            SQL.AppendLine("    A.CurCode, A.Amt, D.MainCurAmt, ");
            SQL.AppendLine("    Case B.DocType When '1' Then 'Sales Invoice' When '2' Then 'Sales Return Invoice' When '3' Then 'Point of Sale' End As DocTypeDesc, ");
            SQL.AppendLine("    B.DocNoOut As DocNoIO, B.Amt As Amt2, B.Amt*IfNull(B.ExcRate, 0.00) As MainCurAmt2, B.TaxInvoiceNo, B.TaxInvoiceDt, B.Remark, ");
            SQL.AppendLine("    A.VoucherRequestDocNo, C.VoucherDocNo, A.DeptCode ");
            if (mRptVoucherRequestVATFormat == "2")
                SQL.AppendLine(" ,'Out' AS TypeIO, 'Customer' AS VdCst, G.Amt AS TaxBaseAmt, G.Amt AS TaxBaseMainCurAmt, A.TaxPeriod, A.TaxGrpCode, F.TaxGrpName ");
            else
                SQL.AppendLine(" ,NULL AS TypeIO, NULL AS VdCst, 0.00 AS TaxBaseAmt, 0.00 AS TaxBaseMainCurAmt, NULL as TaxPeriod, NULL as TaxGrpCode, NULL as TaxGrpName ");
            SQL.AppendLine("    From TblVoucherRequestPPNHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestPPNDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T1.DocNo, Sum(T2.Amt*IfNull(T2.ExcRate, 0.00)) As MainCurAmt ");
            SQL.AppendLine("        From TblVoucherRequestPPNHdr T1 ");
            SQL.AppendLine("        Inner Join TblVoucherRequestPPNDtl2 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        INNER JOIN tblcostcenter T3 ON T1.DeptCode = T3.DeptCode ");
            if (mRptVoucherRequestVATFormat == "1" || DteDocDt1.Text.Length > 0) SQL.AppendLine("            And T2.TaxInvoiceDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        Where 1=1 ");
            //SQL.AppendLine(Filter.Replace("A.", "T1."));
            Fltr = Filter.Replace("A.", "T1.");
            SQL.AppendLine(Fltr.Replace("E.", "T3."));
            SQL.AppendLine("        Group By T1.DocNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo ");
            SQL.AppendLine(" INNER JOIN tblcostcenter E ON A.DeptCode = E.DeptCode ");
            if (mRptVoucherRequestVATFormat == "2")
            {
                SQL.AppendLine("INNER JOIN tbltaxgrp F ON A.TaxGrpCode = F.TaxGrpCode  ");
                SQL.AppendLine("INNER JOIN ( ");
                SQL.AppendLine("		SELECT X1.DocNo, X1.Amt FROM tblsalesinvoicehdr X1  ");
                //SQL.AppendLine("		-- UNION ALL ");
                //SQL.AppendLine("		-- TaxBaseAmt untuk Sales Return Invoice ");
                //SQL.AppendLine("		-- UNION ALL ");
                //SQL.AppendLine("		-- TaxBaseAmt untuk Point of Sale ");
                SQL.AppendLine(")G ON B.DocNoOut=G.DocNo  ");
            }
            if (mRptVoucherRequestVATFormat == "1" || DteDocDt1.Text.Length > 0) SQL.AppendLine("    Where (B.TaxInvoiceDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(Filter);

            SQL.AppendLine(") T ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",   
                        "Period", //new
                        "Cancel", 
                        "Status",

                        //6-10
                        "Debit/Credit",
                        "Currency",
                        mRptVoucherRequestVATFormat == "2" ? "Total Tax Amount" : "Amount",
                        "Amount"+mMainCurCode,
                        mRptVoucherRequestVATFormat == "2" ? "Document Type" : "Type",

                        //11-15
                        "Type", //new
                        "Document (In/Out)",
                        "Vendor/Customer", //new
                        "Tax Base Amount", //new
                        "Tax Base Amount "+mMainCurCode, //new
                        
                        //16-20
                        mRptVoucherRequestVATFormat == "2" ? "Tax Amount" : "Amount",
                        mRptVoucherRequestVATFormat == "2" ? "Tax Amount "+mMainCurCode : "Amount "+mMainCurCode, 
                        "Tax Invoice#",
                        "Tax Invoice Date",
                        "Remark",

                        //21-23
                        "Voucher Request#",
                        "Voucher#",
                        "Tax Group" //new
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        140, 80, 60, 80, 80, 
                        
                        //6-10
                        80, 60, 130, 130, 180,  
                        
                        //11-15
                        80, 140, 160, 180, 180, 
                        
                        //16-20
                        130, 130, 100, 100, 350, 
                        
                        //21-23
                        130, 130, 180
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 19 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 14, 15, 16, 17 }, 0);
            if (mRptVoucherRequestVATFormat == "2")
                Sm.GrdColInvisible(Grd1, new int[] { 6, 23 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);

            if(mRptVoucherRequestVATFormat == "2")
                Sm.GrdColInvisible(Grd1, new int[] { 3, 11, 13, 14, 15 }, true);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 3, 11, 13, 14, 15, 23 }, false);
        }

        override protected void HideInfoInGrd()
        {
            if (mRptVoucherRequestVATFormat == "2")
                Sm.GrdColInvisible(Grd1, new int[] { 6, 23 }, !ChkHideInfoInGrd.Checked);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (mRptVoucherRequestVATFormat == "1") //default
            {
                if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                //|| IsProfitCenterInvalid()
                ) return;
            }
            else if (mRptVoucherRequestVATFormat == "2") // TWC
            {
                if (
                    IsTaxInvDtAndPeriodEmpty() ||
                    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            }
            

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty, Filter2 = " ";
                var SQL = new StringBuilder();

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter2 = " And A.Status In ('O', 'A') And A.CancelInd='N' ";

                SetProfitCenter();

                if (mIsFicoUseMultiProfitCenterFilter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter3 = string.Empty;
                        int i = 0;

                        SQL.AppendLine("    And E.CCCode Is Not Null ");
                        SQL.AppendLine("    And E.CCCode In ( ");
                        SQL.AppendLine("        Select Distinct CCCode ");
                        SQL.AppendLine("        From TblCostCenter ");
                        SQL.AppendLine("        Where ActInd='Y' ");
                        SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        SQL.AppendLine("        And ProfitCenterCode In ( ");
                        SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        //SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        ) ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter3.Length > 0) Filter3 += " Or ";
                            Filter3 += " (ProfitCenterCode=@ProfitCenterCode" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                            i++;
                        }
                        if (Filter3.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter3 + ") ");
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And E.CCCode Is Not Null ");
                            SQL.AppendLine("    And E.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            SQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            SQL.AppendLine("    ) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And (E.CCCode Is Null Or (E.CCCode Is Not Null And E.CCCode In ( ");
                            SQL.AppendLine("        Select Distinct CCCode ");
                            SQL.AppendLine("        From TblCostCenter ");
                            SQL.AppendLine("        Where ActInd='Y' ");
                            SQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            //SQL.AppendLine("        And ProfitCenterCode In (");
                            //SQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            //SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode)) ");
                            SQL.AppendLine("        ))) ");
                        }
                    }
                }

                Filter = string.Empty; // " Where Amt2<>MainCurAmt2 ";

                Sm.CmParam<string>(ref cm, "@MainCurCode", mMainCurCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtTaxInvoiceNo.Text, "T.DocNoIO", false);
                if (mRptVoucherRequestVATFormat == "2")
                {
                    Sm.FilterStr(ref Filter, ref cm, TxtTaxPeriod.Text, "T.TaxPeriod", false);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueTaxGrpCode), "T.TaxGrpCode", false);
                }

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                GetSQL(Filter2 + SQL.ToString()) + Filter +" Order By T.DocDt Desc, T.DocNo;",
                // GetSQL(Filter2) + Filter +  " Order By T.DocDt Desc, T.DocNo;",
                new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "TaxPeriod", "CancelInd", "StatusDesc", "AcTypeDesc", 
                        
                        //6-10
                        "CurCode", "Amt", "MainCurAmt", "DocTypeDesc", "TypeIO", 
                        
                        //11-15
                        "DocNoIO", "VdCst", "TaxBaseAmt", "TaxBaseMainCurAmt", "Amt2", 
                        
                        //16-20
                        "MainCurAmt2", "TaxInvoiceNo", "TaxInvoiceDt", "Remark", "VoucherRequestDocNo",
                        
                        //21-22
                        "VoucherDocNo", "TaxGrpName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0); 
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2); 
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15); 
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17); 
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22); 
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9, 16, 17 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional

        private bool IsTaxInvDtAndPeriodEmpty()
        {
            if (ChkDocDt.Checked == false && ChkTaxPeriod.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "Tax Invoice Date or Tax Period is empty");
                return true;
            }
            else
                return false;
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            //if (mIsFilterByProfitCenter)
            //{
            //    SQL.AppendLine("    Where ProfitCenterCode In ( ");
            //    SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            //    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            //    SQL.AppendLine("        ) ");
            //}
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0 && mIsFicoUseMultiProfitCenterFilter)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (mRptVoucherRequestVATFormat == "2")
            {
                Sm.FilterDteSetCheckEdit(this, sender);

                if (DteDocDt1.EditValue != null)
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxPeriod, ChkTaxPeriod }, true);
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxPeriod, ChkTaxPeriod }, false);
            }
            else
                if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (mRptVoucherRequestVATFormat == "2")
            {
                Sm.FilterDteSetCheckEdit(this, sender);

                if (DteDocDt2.EditValue != null)
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxPeriod, ChkTaxPeriod }, true);
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxPeriod, ChkTaxPeriod }, false);
            }
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Tax Invoice's Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtTaxPeriod_Validated(object sender, EventArgs e)
        {
            if (mRptVoucherRequestVATFormat == "2") Sm.FilterTxtSetCheckEdit(this, sender);
            
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkTaxPeriod_CheckedChanged(object sender, EventArgs e)
        {
            if (mRptVoucherRequestVATFormat == "2")
            {
                Sm.FilterSetTextEdit(this, sender, "Tax Period");

                if (TxtTaxPeriod.Text.Length > 0)
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt1, DteDocDt2, ChkDocDt }, true);
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt1, DteDocDt2, ChkDocDt }, false);
            }
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTaxInvoiceNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Tax invoice#");
        }

        private void LueTaxGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mRptVoucherRequestVATFormat == "2")
            {
                Sm.RefreshLookUpEdit(LueTaxGrpCode, new Sm.RefreshLue1(Sl.SetLueTaxGrpCode));
                Sm.FilterLueSetCheckEdit(this, sender);
            }
        }

        private void ChkTaxGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            if (mRptVoucherRequestVATFormat == "2") Sm.FilterSetLookUpEdit(this, sender, "Tax Group");
        }

        #endregion

        #endregion

    }
}
