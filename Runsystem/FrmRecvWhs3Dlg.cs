﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs3Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRecvWhs3 mFrmParent;
        private bool mIsReCompute = false;

        #endregion

        #region Constructor

        public FrmRecvWhs3Dlg(FrmRecvWhs3 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Local Code",
                        "Item's Name", 
                        
                        //6-10
                        "Item's Category",
                        "UoM",
                        "UoM",
                        "UoM",
                        "Group"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 80, 20, 80, 250, 
                        
                        //6-10
                        150, 80, 80, 80, 100                    
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6, 7, 8, 9, 10 }, false);
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[10].Visible = true;
                Grd1.Cols[10].Move(5);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                if (ChkUnselectedItem.Checked)
                {
                    Filter = " And Locate(Concat('##', A.ItCode, '##'), @SelectedItem)<1 ";
                    Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                }
                else
                    Filter = " And 0=0 ";

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                mIsReCompute = false;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select A.ItCode, A.ItCodeInternal, A.ItName, B.ItCtName, A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, A.ItGrpCode " +
                        "From TblItem A, TblItemCategory B " +
                        "Where A.ItCtCode=B.ItCtCode And A.ActInd='Y' " +
                        Filter + " Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode", 

                            //1-5
                            "ItCodeInternal", "ItName", "ItCtName", "InventoryUomCode", "InventoryUomCode2", 
                            
                            //6-7
                            "InventoryUomCode3", "ItGrpCode" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mIsReCompute = true;
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int i = 1;
            if (ChkChooseRepeatedly.Checked)
            {
                i = GetChooseRepeatedly();
                if (!(i>=1 && i<=999)) return;
            }

            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;
                        for(int Index = 1; Index<=(ChkChooseRepeatedly.Checked?i:1);Index++)
                            InsertData(Row);
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private void InsertData(int Row)
        {
            int Row1 = 0, Row2 = 0;

            Row1 = mFrmParent.Grd1.Rows.Count - 1;
            Row2 = Row;

            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 5);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 7);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 8);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 9);
            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 10);
            mFrmParent.Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
        }

        private int GetChooseRepeatedly()
        {
            int i = 0;
            try
            {
                string value = "1";
                if (Sm.InputBox("Run System", "Choose Repeatedly (1-999):", ref value) == DialogResult.OK)
                {
                    i = int.Parse(value);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return i;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (mIsReCompute) Sm.GrdExpand(Grd1);
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
