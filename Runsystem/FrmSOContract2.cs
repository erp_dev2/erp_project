﻿#region Update
/*
 * 26/09/2019 [WED/IMS] new apps
    02/10/2019 [WED/IMS] Service dan Inventory minimal terisi salah satu
    05/10/2019 [WED/YK] tambah SOCDocNo di COA
    07/10/2019 [WED/IMS] update Amt2 dan PrevAmt2 di SOContractRevision
    10/10/2019 [WED/IMS] multi date Item berdasarkan parameter
    14/10/2019 [WED/IMS] item BOQ ambil dari BOM With Co-product tab result
    05/11/2019 [WED/IMS] tambah field nomor SPMK, bisa generate otomatis dengan mencari dokumen SPMK yang sesuai dengan BOQ nya
    05/11/2019 [WED/IMS] tambah UNIT QTY CONTRACT, UNIT ITEM PRICE CONTRACT, DAN TOTAL PRICE CONTRACT
    07/11/2019 [WED/IMS] remark diisi dari SO Contract
    20/11/2019 [WED/IMS] tidak update NTPSOCInd
    21/11/2019 [WED/IMS] price di list of item ambil dari boq inventory ataupun boq service
    26/11/2019 [DITA/IMS] Attachment File untuk SOContract
    02/12/2019 [DITA/IMS] List untuk Printout
    02/12/2019 [VIN/IMS] Format untuk printout
    06/01/2020 [WED/IMS] tampilkan data tax dari BOQ. price before tax di hide, karna nilai nya sama dengan price after tax
    09/01/2020 [TKG/IMS] berdasarkan parameter, pada saat insert akan otomatis membuat BOM 
    18/02/2020 [TKG/IMS] tambah term of payment
    18/02/2020 [DITA/IMS] tambah Tax
    19/02/2020 [DITA/IMS] ganti source dari BOM ke ITEM
    19/02/2020 [DITA/IMS] price list mengambil dari unit price contract
    19/02/2020 [TKG/IMS] bug saat save dtl karena bom, issue ntp
    20/02/2020 [WED/IMS] dibuka untuk di edit, dan ada history
    20/02/2020 [WED/IMS] wadah untuk untuk transaksi yang di revisi
    24/02/2020 [DITA/IMS] BUG Saat pilih list of item
    03/03/2020 [TKG/IMS] bisa edit info boq
    20/03/2020 [TKG/IMS] bisa memilih ntp
    01/04/2020 [TKG/IMS] bug saat show, remark tidak muncul
    09/04/2020 [TKG/IMS] bug remarknya bisa diedit
    12/05/2020 [WED/IMS] semua amount di hide (grid) dan dibuat 0 (header) berdasarkan parameter IsSOContract2AmtIsZero
    15/05/2020 [WED/IMS] perbaikan print out
    03/06/2020 [IBL/IMS] Generate COA sesuai settingan di system option + Project Code
    04/06/2020 [IBL/IMS] SOContract tidak dapat diedit selama dokumen dipakai di SOContract Termin/DP(I) yg belum cancel.
    12/06/2020 [IBL/IMS] Tambah kolom Notes berdasarkan parameter IsSalesTransactionUseNotes
    15/06/2020 [TKG/IMS] Bug saat show data dari boq (berhubungan dengan revision)
    23/09/2020 [WED/IMS] tambah inputan No di detail, default ambil dari BOQ. save Amt header juga save ke Amt2 (biar bisa ditarik di SalesInvoice5)
    24/09/2020 [VIN/IMS] printout: tambah No di detail, memunculkan nama dan username di ttd 
    24/09/2020 [WED/IMS] Nomor boleh duplicate jadinya
    08/10/2020 [DITA/IMS] validasi baru terhadap total price saat mengisi item di BOQ dan List of item di SO Contract
    26/10/2020 [DITA/IMS] masih error unequal saat save, ketika tax diisi
    26/10/2020 [VIN/IMS] tambah kolom sequence Number
    26/10/2020 [VIN/IMS] sort printout berdasarkan seqno
    04/11/2020 [IBL/IMS] bug saat save dan find data
    13/11/2020 [VIN/IMS] Tax bisa edit 
    24/11/2020 [VIN/IMS] feedback printout inventory order by seqNo
    04/12/2020 [VIN/IMS] tambah item dismantle, boq service order by seqno
    07/12/2020 [DITA/IMS] item dismantle belum muncul saat show data dan masih join ke itcode nya boq
    07/12/2020 [DITA/IMS] DocDt bisa diedit berdasarkan parameter IsSOContractDocDtEditable
    28/01/2021 [DITA/IMS] tampilan detail so contract service/inventory + list of item urutannya disamakan dengan detail BOQ
    04/02/2021 [DITA/IMS] untuk menampilkan it code dismantle, sebagai pembeda item yang sama dalam 1 detail, patokan menggunakan itcode dan sequence number. dengan catatan seqno di boq dan socontract harus selalu sama
    08/03/2021 [IBL/IMS] Penyesuaian printout
    21/04/2021 [TKG/IMS] keluar informasi kalau data sudah selesai disimpan saat insert beserta dengan document# yg baru.
    22/04/2021 [TKG/IMS] ubah proses penyimpanan
    26/04/2021 [TKG/IMS] ubah proses edit
    29/04/2021 [TKG/IMS] ubah design, tambah copy boq
    05/05/2021 [VIN/IMS] feedback qty printout
    19/05/2021 [VIN/IMS] feedback source printout
    21/05/2021 [DITA/IMS] update pricelist masih bug, belum sesuai item dan No
    03/06/2021 [VIN/IMS] copy seqno dari boq - tidak bisa di edit 
    05/06/2021 [TKG/IMS] bug saat save tab inventory dan set read only column qty dan price baik yg contract maupun revision
                         buka copy boq saat edit
    06/06/2021 [TKG/IMS] tambah feature update delivery date
    11/06/2021 [VIN/IMS] penyesuaian printout
    11/06/2021 [RDH/IMS] Menambahkan "total amount after tax" diambil dari total di list of item (ditaruh di header) --> DOBLE KLIK DI JUDUL KOLOM MAKA MUNCUL INFORMASI TOTAL AFTER TAX 
    14/06/2021 [VIN/IMS] penyesuaian total ppn printout
    17/06/2021 [VIN/IMS] penyesuaian GetSelectedBOQData, & GetSelectedBOQData2
    04/08/2021 [VIN/IMS] Bug Edit Tax & delivery date bisa di edit
    26/08/2021 [IBL/IMS] Tambah approval
    26/12/2021 [SET/IMS] Menambahkan designer pada printout dan menghubungkan dengan approval
 */
#endregion

#region Update
/*
    
    19/01/2022 [DEV/IMS] Mengganti source Jabatan pada printout SO Contract
    09/03/2022 [VIN/IMS] Tambah Validasi tidak bisa cancel jika sudah di MR Service kan 
    07/04/2022 [SET/IMS] harga satuan di print out SOC tidak sesuai dengan transaksinya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using System.IO;
using System.Net;
using System.Threading;
using FastReport;
using FastReport.Data;
using System.Collections;


#endregion

namespace RunSystem
{
    public partial class FrmSOContract2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mCity = string.Empty,
            mCnt = string.Empty,
            mSADNo = string.Empty,
            mGenerateCustomerCOAFormat = string.Empty,
            mProjectAcNoFormula = string.Empty,
            mProjectCodeForSOContract2 = string.Empty;
        internal bool
            mIsFilterBySite = false,
            mIsCustomerItemNameMandatory = false,
            mIsSOUseARDPValidated = false,
            IsInsert = false,
            mIsSOContractItemMultiDeliveryDateEnabled = false,
            mIsSOContract2AmtIsZero = false,
            mIsSalesTransactionUseItemNotes = false;
        private bool
            mIsSOContractBOMEnabled = false,
            mIsSOContractDocDtEditable = false
            ;
        internal string mIsSoUseDefaultPrintout;
        internal int mNumberOfSalesUomCode = 1;
        internal FrmSOContract2Find FrmFind;
        private string
                mJointOperationKSOType = string.Empty,
                mSOCDocType = "2",
                mPortForFTPClient = string.Empty,
                mHostAddrForFTPClient = string.Empty,
                mSharedFolderForFTPClient = string.Empty,
                mUsernameForFTPClient = string.Empty,
                mPasswordForFTPClient = string.Empty,
                mFileSizeMaxUploadFTPClient = string.Empty,
                mFormatFTPClient = string.Empty,
                mSOContractAutoApprovedByEmpCode = string.Empty,
                mSOContractAutoCheckedByEmpCode = string.Empty;

        private byte[] downloadedData;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSOContract2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "SO Contract";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetNumberOfSalesUomCode();
                GetParameter();
                if (mJointOperationKSOType.Length <= 0) mJointOperationKSOType = "1";

                Sl.SetLuePtCode(ref LuePtCode);
                Sl.SetLueTaxCode(ref LueTaxCode);

                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueStatus(ref LueStatus);
                Sl.SetLueOption(ref LueJOType, "JointOperationType");

                TcOutgoingPayment.SelectedTabPage = TpBankGuarantee;
                Sl.SetLueBankCode(ref LueBankCode);
                TxtBankGuaranteeAmt.EditValue = Sm.FormatNum(0m, 0);

                TcOutgoingPayment.SelectedTabPage = TpJO;
                SetLueJOCode(ref LueJOCode, string.Empty);
                LueJOCode.Visible = false;
                DteJODocDt.Visible = false;

                TcOutgoingPayment.SelectedTabPage = TpItem;
                TcOutgoingPayment.SelectedTabPage = TpBOQ;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        #region Set Grid

        private void SetGrd()
        {
            #region Grid 3 - Item

            Grd3.Cols.Count = 32;
            Grd3.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd3, new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Agent Code",
                        "Agent",
                        "Item's Code",
                        "",

                        //6-10
                        "Item's Name",
                        "Packaging",
                        "Quantity"+Environment.NewLine+"Packaging",
                        "Quantity",
                        "UoM",

                        //11-15
                        "Price"+Environment.NewLine+"(Price List)",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Price After"+Environment.NewLine+"Discount",
                        "Promo"+Environment.NewLine+"%",

                        //16-20
                        "Price"+Environment.NewLine+"Before Tax",
                        "Tax"+Environment.NewLine+"%",
                        "Tax"+Environment.NewLine+"Amount",
                        "Price"+Environment.NewLine+"After Tax",
                        "Total",

                        //21-25
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark",
                        "CtQtDNo",
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code",

                        //26-30
                        "Customer's"+Environment.NewLine+"Item Name",
                        "Volume",
                        "Total Volume",
                        "Uom"+Environment.NewLine+"Volume",
                        "Notes",

                        //31
                        "No"
                    },
                    new int[]
                    {
                        //0
                        40,

                        //1-5
                        20, 0, 180, 120, 20, 
                        
                        //6-10
                        200, 100, 100, 80, 80, 
                        
                        //11-15
                        100, 80, 100, 100, 80,
                        
                        //16-20
                        100, 80, 100, 100, 120,   
                        
                        //21-25
                        100, 400, 0, 120, 120,

                        //26-30
                        250, 100, 100, 100, 300,

                        //31
                        100
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 1, 5 });
            Sm.GrdFormatDate(Grd3, new int[] { 21 });
            Sm.GrdFormatDec(Grd3, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 });
            Grd3.Cols[31].Move(4);
            Grd3.Cols[24].Move(7);
            Grd3.Cols[26].Move(7);
            Grd3.Cols[25].Move(7);
            Grd3.Cols[27].Move(14);
            Grd3.Cols[28].Move(15);
            Grd3.Cols[29].Move(16);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2, 3, 4, 5, 12, 14, 15, 23, 24, 25, 27, 28, 29 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd3, new int[] { 26 });

            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
            {
                Sm.GrdColInvisible(Grd3, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });
            }

            if (!mIsSalesTransactionUseItemNotes)
            {
                Sm.GrdColInvisible(Grd3, new int[] { 30 });
            }

            #endregion

            #region Grid 1 - ARDP

            Grd1.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "AR Downpayment#", 
                        
                        //1-5
                        "Date",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        "Voucher Request#",

                        //6-7
                        "Voucher#",
                        "Remark"
                    },
                    new int[]
                    {
                        //0
                        150,
 
                        //1-5
                        80, 80, 100, 200, 150, 
                        
                        //6-7
                        150, 250
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3 });
            }

            #endregion

            #region Grid 2 - BOQ Service

            Grd2.Cols.Count = 28;
            Grd2.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "BOM#",
                    "BOMDNo",
                    "Item's Code",
                    "Item's Name",
                    "Local Code",

                    //6-10
                    "Quantity",
                    "Purchase"+Environment.NewLine+"UoM",
                    "Minimal"+Environment.NewLine+"Service",
                    "Material",
                    "Total",

                    //11-15
                    "After Sales",
                    "Total"+Environment.NewLine+"(+ PPH)",
                    "Space Nego",
                    "SPH",
                    "Total Price",

                    //16-20
                    "Remark",
                    "Total Price"+Environment.NewLine+"Contract",
                    "Quantity Contract",
                    "Unit Price"+Environment.NewLine+"Contract",
                    "Quantity Revision",

                    //21-25
                    "Unit Price"+Environment.NewLine+"Revision",
                    "Total Price"+Environment.NewLine+"Revision",
                    "No",
                    "Sequence"+Environment.NewLine+"No",
                    "",

                    //26-27
                    "Item Dismantle's Code",
                    "Item Dismantle's"
                },
                new int[]
                {
                    20,
                    150, 100, 100, 180, 180,
                    100, 100, 120, 120, 120,
                    120, 120, 120, 120, 120,
                    200, 120, 120, 120, 120,
                    120, 120, 100, 100, 20,
                    100, 150
                }
            );
            Sm.GrdColButton(Grd2, new int[] { 0, 25 });
            Sm.GrdFormatDec(Grd2, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22 }, 0);
            Sm.GrdColReadOnly(Grd2, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 22, 24, 26, 27 });
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 3, 26 });
            Grd2.Cols[23].Move(4);
            Grd2.Cols[24].Move(3);
            Grd2.Cols[19].Move(17);
            Grd2.Cols[18].Move(17);
            Grd2.Cols[25].Move(8);
            Grd2.Cols[26].Move(9);
            Grd2.Cols[27].Move(10);
            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
                Sm.GrdColInvisible(Grd2, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 17, 19, 21, 22 });

            #endregion

            #region Grid 6 - BOQ Inventory

            Grd6.Cols.Count = 27;
            Grd6.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                Grd6, new string[]
                {
                    //0
                    "",

                    //1-5
                    "BOM#",
                    "BOMDNo",
                    "Item's Code",
                    "Item's Name",
                    "Local Code",

                    //6-10
                    "Quantity",
                    "Purchase"+Environment.NewLine+"UoM",
                    "Cost"+Environment.NewLine+"of Goods",
                    "After Sales",
                    "COM",

                    //11-15
                    "OH",
                    "Margin",
                    "Design Cost",
                    "Unit Price"+Environment.NewLine+"(+ PPH)",
                    "Space Nego",

                    //16-20
                    "Unit Price"+Environment.NewLine+"(SPH)",
                    "Total Price"+Environment.NewLine+"(SPH)",
                    "Remark",
                    "Total Price"+Environment.NewLine+"Contract",
                    "Quantity Contract",

                    //21-25
                    "Unit Price"+Environment.NewLine+"Contract",
                    "Quantity Revision",
                    "Unit Price"+Environment.NewLine+"Revision",
                    "Total Price"+Environment.NewLine+"Revision",
                    "No",

                    //26
                    "Sequence"+Environment.NewLine+"No"
                },
                new int[]
                {
                    20,
                    150, 80, 100, 180, 180,
                    120, 100, 120, 120, 120,
                    120, 120, 120, 120, 120,
                    120, 120, 200, 120, 120,
                    120, 120, 120, 120, 100,
                    100
                }
            );
            Sm.GrdColButton(Grd6, new int[] { 0 });
            Sm.GrdFormatDec(Grd6, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdColReadOnly(Grd6, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 24, 26 });
            Grd6.Cols[25].Move(4);
            Grd6.Cols[26].Move(3);
            Grd6.Cols[21].Move(19);
            Grd6.Cols[20].Move(19);
            Sm.GrdColInvisible(Grd6, new int[] { 1, 2, 3 });
            if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
                Sm.GrdColInvisible(Grd6, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 21, 23, 24 });

            #endregion

            #region Grid 5 - Joint Operation

            Grd5.Cols.Count = 4;

            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[]
                {
                    //0
                    "JOCode", 
                    
                    //1-3
                    "Joint Operation",
                    "Joint Operation's" + Environment.NewLine + "Date",
                    "Joint Operation's" + Environment.NewLine + "Document#",
                },
                new int[]
                {
                    //0
                    0,
                    //1-3
                    120, 150, 150
                }
            );
            Sm.GrdFormatDate(Grd5, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd5, new int[] { 0 });
            Sm.GrdColInvisible(Grd5, new int[] { 0 });

            #endregion

        }

        #endregion

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd3, new int[] { 3, 4, 5, 12, 14, 15, 17, 18, 24, 25, 27, 28, 29 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 3, 26 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd6, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, TxtBOQDocNo, TxtLocalDocNo, LueCtCode, MeeCancelReason,
                        TxtCustomerContactperson, MeeRemark,  ChkCancelInd, LueStatus, LueSPCode,
                        LueShpMCode, TxtProjectName, MeeProjectDesc, LueJOType, TxtBankGuaranteeNo,
                        LueBankCode, DteBankGuaranteeDt, LueJOCode, DteJODocDt, TxtPONo, TxtBankGuaranteeAmt,
                        TxtAmtBOM, TxtNTPDocNo, TxtFile, TxtFile2, TxtFile3, ChkFile, ChkFile2, ChkFile3, LuePtCode, LueTaxCode
                    }, true);
                    BtnBOQDocNo.Enabled = false;
                    BtnCtShippingAddress.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    BtnCustomerShipAddress.Enabled = false;
                    BtnContact.Enabled = false;
                    BtnNTPDocNo.Enabled = false;
                    BtnFile.Enabled = BtnFile2.Enabled = BtnFile3.Enabled = false;
                    BtnDownload.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = true;
                    //Sm.GrdColReadOnly(true, true, Grd2, new int[] { 7 });
                    Grd2.ReadOnly = Grd6.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 9, 1, 21, 30, 31 });
                    Grd5.ReadOnly = true;
                    BtnCopyBOQ.Visible = false;
                    BtnCopyDeliveryDt.Visible = false;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 18, 19, 20, 21 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 20, 21, 22, 23 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       TxtLocalDocNo, DteDocDt, LueCtCode, TxtCustomerContactperson, LueShpMCode, LueSPCode,
                       MeeRemark, MeeProjectDesc, LueJOCode, DteJODocDt, LueJOType, TxtBankGuaranteeNo,
                       LueBankCode, DteBankGuaranteeDt, TxtPONo, TxtBankGuaranteeAmt, LuePtCode, LueTaxCode
                    }, false);
                    BtnBOQDocNo.Enabled = true;
                    BtnCtShippingAddress.Enabled = true;
                    BtnCustomerShipAddress.Enabled = true;
                    BtnContact.Enabled = true;
                    BtnNTPDocNo.Enabled = true;
                    BtnFile.Enabled = BtnFile2.Enabled = BtnFile3.Enabled = true;
                    BtnDownload.Enabled = BtnDownload2.Enabled = BtnDownload3.Enabled = false;
                    BtnCopyBOQ.Visible = true;
                    DteDocDt.Focus();
                    //Sm.GrdColReadOnly(false, true, Grd2, new int[] { 7 });
                    Grd2.ReadOnly = Grd6.ReadOnly = false;
                    Grd5.ReadOnly = false;
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 9 });
                    //Sm.GrdColReadOnly(true, true, Grd2, new int[] { 20, 21 });
                    //Sm.GrdColReadOnly(true, true, Grd6, new int[] { 22, 23 });
                    Sm.GrdColReadOnly(false, false, Grd3, new int[] { 1 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 21, 30, 31 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 18, 19 });
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 20, 21 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    var SQL = new StringBuilder();

                    SQL.AppendLine("SELECT DocNo FROM TblSOContractDownpaymentHdr ");
                    SQL.AppendLine("WHERE SOContractDocNo = @SODocNo ");
                    SQL.AppendLine("AND CancelInd = 'N' ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);

                    if (Sm.IsDataExist(cm))
                    {
                        Sm.GrdColReadOnly(false, true, Grd3, new int[] { 21 });
                        Grd3.Focus();
                        BtnCopyBOQ.Visible = BtnCopyDeliveryDt.Visible = true;
                        BtnCopyBOQ.Enabled = BtnCopyDeliveryDt.Enabled = false;

                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, LuePtCode, DteDeliveryDt, TxtPONo, MeeRemark, LueTaxCode }, false);
                        if (mIsSOContractDocDtEditable) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt }, false);
                        LueStatus.Focus();
                        Grd2.ReadOnly = Grd6.ReadOnly = false;
                        Sm.GrdColReadOnly(false, true, Grd3, new int[] { 9, 1, 21, 31 });
                        //Sm.GrdColReadOnly(true, true, Grd2, new int[] { 18, 19 });
                        //Sm.GrdColReadOnly(true, true, Grd6, new int[] { 20, 21 });
                        Sm.GrdColReadOnly(false, true, Grd3, new int[] { 21 });
                        Sm.GrdColReadOnly(false, false, Grd3, new int[] { 1, 30 });
                        Sm.GrdColReadOnly(false, true, Grd2, new int[] { 20, 21 });
                        Sm.GrdColReadOnly(false, true, Grd6, new int[] { 22, 23 });
                        BtnCopyBOQ.Visible = true;
                        BtnCopyDeliveryDt.Visible = true;
                        ChkCancelInd.Properties.ReadOnly = false;
                        MeeCancelReason.Focus();
                    }
                    break;
            }
        }

        private void ClearData()
        {
            mSADNo = string.Empty;
            mCnt = string.Empty;
            mCity = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                  TxtDocNo, DteDocDt, TxtBOQDocNo, LueStatus, TxtLocalDocNo,
                  TxtSAName, TxtAddress, TxtCity, TxtCountry, TxtPostalCd,
                  TxtPhone, TxtFax, TxtEmail, TxtMobile, MeeCancelReason,
                  LueCtCode, TxtCustomerContactperson, MeeRemark, LueShpMCode, LueSPCode,
                  TxtProjectName, MeeProjectDesc, LueJOType, TxtBankGuaranteeNo,
                  LueBankCode, DteBankGuaranteeDt, LueJOCode, DteJODocDt, TxtPONo, TxtNTPDocNo,
                  TxtFile, TxtFile3, TxtFile2, LuePtCode, TxtProjectCode, LueTaxCode, DteDeliveryDt
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtBankGuaranteeAmt, TxtAmtBOM }, 0);
            ChkCancelInd.Checked = false;
            ChkFile.Checked = ChkFile2.Checked = ChkFile3.Checked = false;
            PbUpload.Value = PbUpload2.Value = PbUpload3.Value = 0;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.ClearGrd(Grd3, true);
            Sm.ClearGrd(Grd5, true);
            Sm.ClearGrd(Grd6, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 8, 9, 11, 12, 13, 14, 16, 17, 18, 19, 20 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22 });
            Sm.SetGrdNumValueZero(ref Grd6, 0, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 });
        }

        private void ClearData2()
        {
            mSADNo = string.Empty;
            mCity = string.Empty;
            mCnt = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtAddress, TxtCity, TxtCountry, TxtPostalCd, TxtPhone,
                TxtFax, TxtEmail, TxtMobile
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContract2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueStatus, Sm.GetValue("Select 'O' "));
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DocNo FROM TblSOContractDownpaymentHdr ");
            SQL.AppendLine("WHERE SOContractDocNo = @SODocNo ");
            SQL.AppendLine("AND CancelInd = 'N' ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
                Sm.StdMsg(mMsgType.Info, "This document is currently being used by SO Contract Termin/Downpayment (I). Only Delivery Date can change. ");

            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("SELECT DocNo FROM TblSOContractDownpaymentHdr ");
                    SQL.AppendLine("WHERE SOContractDocNo = @SODocNo ");
                    SQL.AppendLine("AND CancelInd = 'N' ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);

                    if (Sm.IsDataExist(cm))
                        EditData2();
                    else
                        EditData();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Grid Method

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled && !IsInsert && e.ColIndex == 9) ComputeItem1(e.RowIndex);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            try
            {
                if (e.ColIndex == 1 &&
                           !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                           !Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ Document", false))
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.FormShowDialog(new FrmSOContract2Dlg3(
                            this,
                            e.RowIndex,
                            mNumberOfSalesUomCode > 1,
                            Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0,
                            !BtnSave.Enabled,
                            Sm.GetLue(LueCtCode),
                            Sm.GetGrdStr(Grd3, e.RowIndex, 4),
                            TxtBOQDocNo.Text));
                }

                if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmItem("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    f.ShowDialog();
                }

                if (e.ColIndex == 21) Sm.DteRequestEdit(Grd3, DteDeliveryDt, ref fCell, ref fAccept, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            try
            {
                if (e.ColIndex == 1 && //BtnSave.Enabled && TxtDocNo.Text.Length == 0 && 
                        !Sm.IsLueEmpty(LueCtCode, "Customer") &&
                        !Sm.IsTxtEmpty(TxtBOQDocNo, "BOQ Document", false))
                    Sm.FormShowDialog(
                    new FrmSOContract2Dlg3(
                        this,
                        e.RowIndex,
                        mNumberOfSalesUomCode > 1,
                        Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0,
                        !BtnSave.Enabled,
                        Sm.GetLue(LueCtCode),
                        Sm.GetGrdStr(Grd3, e.RowIndex, 4),
                        TxtBOQDocNo.Text)
                    );

                if (e.ColIndex == 5 && Sm.GetGrdStr(Grd3, e.RowIndex, 4).Length != 0)
                {
                    var f = new FrmItem("***");
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 4);
                    f.ShowDialog();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                if (Sm.IsGrdColSelected(new int[] { 18, 19 }, e.ColIndex))
                {
                    decimal mTotal = 0m, mQty = 0m, mUPrice = 0m;
                    mQty = Sm.GetGrdDec(Grd2, e.RowIndex, 18);
                    mUPrice = Sm.GetGrdDec(Grd2, e.RowIndex, 19);
                    mTotal = mQty * mUPrice;
                    Grd2.Cells[e.RowIndex, 17].Value = mTotal;
                    ComputeTotalBOQ(IsInsert);
                    ComputeItem(IsInsert);
                    if (Sm.GetGrdDec(Grd2, e.RowIndex, 19) != 0) UpdatePriceList(Sm.GetGrdStr(Grd2, e.RowIndex, 3), Sm.GetGrdDec(Grd2, e.RowIndex, 19), Sm.GetGrdStr(Grd2, e.RowIndex, 23));
                }

                if (Sm.IsGrdColSelected(new int[] { 20, 21 }, e.ColIndex))
                {
                    decimal mTotal = 0m, mQty = 0m, mUPrice = 0m;
                    mQty = Sm.GetGrdDec(Grd2, e.RowIndex, 20);
                    mUPrice = Sm.GetGrdDec(Grd2, e.RowIndex, 21);
                    mTotal = mQty * mUPrice;
                    Grd2.Cells[e.RowIndex, 22].Value = mTotal;
                    ComputeTotalBOQ(IsInsert);
                    ComputeItem(IsInsert);
                    if (Sm.GetGrdDec(Grd2, e.RowIndex, 21) != 0) UpdatePriceList(Sm.GetGrdStr(Grd2, e.RowIndex, 3), Sm.GetGrdDec(Grd2, e.RowIndex, 21), Sm.GetGrdStr(Grd2, e.RowIndex, 23));
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled)
                {
                    if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtBOQDocNo, "Bill of Quantity", false))
                    {
                        Sm.FormShowDialog(new FrmSOContract2Dlg5(this, TxtBOQDocNo.Text));
                    }
                    if (e.ColIndex == 25 && Sm.GetGrdStr(Grd2, e.RowIndex, 26).Length != 0)
                    {
                        var f = new FrmItem("***");
                        f.Tag = "***";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 27);
                        f.ShowDialog();
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled)
                {
                    Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                    Sm.GrdRemoveRow(Grd2, e, BtnSave);
                    ComputeTotalBOQ(IsInsert);
                    ComputeItem(IsInsert);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled)
                {
                    Sm.GrdKeyDown(Grd6, e, BtnFind, BtnSave);
                    Sm.GrdRemoveRow(Grd6, e, BtnSave);
                    ComputeTotalBOM(IsInsert);
                    ComputeItem(IsInsert);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled)
                {
                    if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtBOQDocNo, "Bill of Quantity", false))
                        Sm.FormShowDialog(new FrmSOContract2Dlg6(this, TxtBOQDocNo.Text));
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd6_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                if (Sm.IsGrdColSelected(new int[] { 20, 21 }, e.ColIndex))
                {
                    decimal mTotal = 0m, mQty = 0m, mUPrice = 0m;
                    mQty = Sm.GetGrdDec(Grd6, e.RowIndex, 20);
                    mUPrice = Sm.GetGrdDec(Grd6, e.RowIndex, 21);
                    mTotal = mQty * mUPrice;
                    Grd6.Cells[e.RowIndex, 19].Value = mTotal;
                    ComputeTotalBOM(IsInsert);
                    ComputeItem(IsInsert);
                    if (Sm.GetGrdDec(Grd6, e.RowIndex, 21) != 0) UpdatePriceList(Sm.GetGrdStr(Grd6, e.RowIndex, 3), Sm.GetGrdDec(Grd6, e.RowIndex, 21), Sm.GetGrdStr(Grd6, e.RowIndex, 25));
                }

                if (Sm.IsGrdColSelected(new int[] { 22, 23 }, e.ColIndex))
                {
                    decimal mTotal = 0m, mQty = 0m, mUPrice = 0m;
                    mQty = Sm.GetGrdDec(Grd6, e.RowIndex, 22);
                    mUPrice = Sm.GetGrdDec(Grd6, e.RowIndex, 23);
                    mTotal = mQty * mUPrice;
                    Grd6.Cells[e.RowIndex, 24].Value = mTotal;
                    ComputeTotalBOM(IsInsert);
                    ComputeItem(IsInsert);
                    if (Sm.GetGrdDec(Grd6, e.RowIndex, 23) != 0) UpdatePriceList(Sm.GetGrdStr(Grd6, e.RowIndex, 3), Sm.GetGrdDec(Grd6, e.RowIndex, 23), Sm.GetGrdStr(Grd6, e.RowIndex, 25));
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd5_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            try
            {
                if (BtnSave.Enabled && !Grd5.ReadOnly)
                {
                    if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex))
                    {
                        //e.DoDefault = false;
                        if (e.ColIndex == 1) LueRequestEdit(Grd5, LueJOCode, ref fCell, ref fAccept, e);
                        if (e.ColIndex == 2) Sm.DteRequestEdit(Grd5, DteJODocDt, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd5, e.RowIndex);
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd5_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd5.ReadOnly)
            {
                Sm.GrdKeyDown(Grd5, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd5, e, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            if (PONoExists())
                if (Sm.StdMsgYN("Question", "This Customer's PO# is exists. Do you want to proceed ?", mMenuCode) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SOContract", "TblSOContractHdr");
            string RevisionDocNo = string.Concat(DocNo, "/0001");
            string mLOPDocNo = Sm.GetValue("Select LOPDocNo From TblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "' ");
            string mSiteCode = Sm.GetValue("Select SiteCode From TblLOPHdr Where DocNo = @Param ", mLOPDocNo);
            string mProjectScope = Sm.GetValue("Select ProjectScope From TblLOPHdr  Where DocNo = @Param ", mLOPDocNo);
            string ProjectSequenceNo = GenerateProjectSequenceNo(mSiteCode, mProjectScope);
            string ProjectCode = string.Concat(mSiteCode, "-", ProjectSequenceNo, mProjectScope);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractHdr(DocNo, ProjectSequenceNo, ProjectCode));

            // save SO Contract Revision
            cml.Add(SaveSOContractRevisionHdr(RevisionDocNo, DocNo, "I"));

            cml.Add(SaveSOContractDtl(DocNo, RevisionDocNo, true));

            //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0)
            //    {
            //        cml.Add(SaveSOContractDtl(DocNo, Row));
            //        cml.Add(SaveSOContractRevisionDtl(RevisionDocNo, DocNo, Row));
            //    }
            //}

            cml.Add(SaveSOContractDtl4(DocNo, RevisionDocNo, true));

            //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0)
            //    {
            //        cml.Add(SaveSOContractDtl4(DocNo, Row));
            //        cml.Add(SaveSOContractRevisionDtl4(RevisionDocNo, DocNo, Row, "I"));
            //    }
            //}

            if (Grd5.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                        cml.Add(SaveSOContractDtl3(DocNo, Row));
                }
            }

            cml.Add(SaveSOContractDtl5(DocNo, RevisionDocNo, true));

            //if (Grd6.Rows.Count > 1)
            //{
            //    for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
            //    {
            //        if (Sm.GetGrdStr(Grd6, Row, 3).Length > 0)
            //        {
            //            cml.Add(SaveSOContractDtl5(DocNo, Row));
            //            cml.Add(SaveSOContractRevisionDtl5(RevisionDocNo, DocNo, Row, "I"));
            //        }
            //    }
            //}

            //cml.Add(UpdateBOQStatusForNTPSOCInd(TxtBOQDocNo.Text, "I"));

            Sm.ExecCommands(cml);

            Sm.StdMsg(
                mMsgType.Info,
                "Document# : " + DocNo + Environment.NewLine +
                "Revision# : " + RevisionDocNo + Environment.NewLine +
                "Saving data is completed."
                );

            if (TxtFile.Text.Length > 0)
                UploadFile(DocNo);
            if (TxtFile2.Text.Length > 0)
                UploadFile2(DocNo);
            if (TxtFile3.Text.Length > 0)
                UploadFile3(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueStatus, "Status") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsTxtEmpty(TxtPONo, "Customer's PO#", false) ||
                Sm.IsTxtEmpty(TxtCustomerContactperson, "Contact Person", false) ||
                Sm.IsTxtEmpty(TxtSAName, "Shipping name", false) ||
                Sm.IsTxtEmpty(TxtAddress, "Address", false) ||
                Sm.IsTxtEmpty(TxtBOQDocNo, "Bill Of Quantity", false) ||
                Sm.IsLueEmpty(LuePtCode, "Term of Payment") ||
                IsBOQAlreadyProceed() ||
                IsDocumentLOPProcessIndInvalid() ||
                Sm.IsLueEmpty(LueSPCode, "Sales Person") ||
                Sm.IsLueEmpty(LueShpMCode, "Shipment Method") ||
                IsUploadFileNotValid() ||
                IsGrdEmpty() ||
                IsJointOperationEmpty() ||
                IsJointOperationInvalid() ||
                IsBankGuaranteeEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsTotalPriceInvalid(IsInsert) ||
                IsCOASalesEmpty() ||
                IsCOAOptionEmpty();
        }

        private bool IsTotalPriceInvalid(bool IsInsert)
        {
            //08/10/2020 [DITA] ubah validasi untuk membandingkan sum(total price) boq dan list of item untuk item yang sama
            for (int i = 0; i < Grd2.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd2, i, 3).Length > 0)
                {
                    decimal mTotalPriceItem = 0m, mTotalPriceContract = 0m;
                    bool mIsEOF = false, mIsItem = false;
                    for (int j = 0; j < Grd3.Rows.Count; ++j)
                    {
                        if (Sm.GetGrdStr(Grd2, i, 3) == Sm.GetGrdStr(Grd3, j, 4))
                        {
                            mIsItem = true;
                            //mTotal += Sm.GetGrdDec(Grd3, j, 20);
                            mTotalPriceItem += Sm.GetGrdDec(Grd3, j, 11) * Sm.GetGrdDec(Grd3, j, 9);
                        }
                        if (j == Grd3.Rows.Count - 1) mIsEOF = true;
                    }

                    for (int k = 0; k < Grd2.Rows.Count; ++k)
                    {
                        if (Sm.GetGrdStr(Grd2, i, 3) == Sm.GetGrdStr(Grd2, k, 3))
                        {
                            if (IsInsert) mTotalPriceContract += Sm.GetGrdDec(Grd2, k, 17);
                            else mTotalPriceContract += Sm.GetGrdDec(Grd2, k, 22);
                        }
                        if (k == Grd2.Rows.Count - 1) mIsEOF = true;
                    }

                    if (mIsItem && mIsEOF)
                    {
                        if (IsInsert)
                        {
                            if (mTotalPriceContract != mTotalPriceItem) //Total Price Contract
                            {
                                Sm.StdMsg(mMsgType.Warning, "Total price on list of item is unequal to BOQ for item : " + Sm.GetGrdStr(Grd2, i, 4));
                                TcOutgoingPayment.SelectedTabPage = TpItem;
                                return true;
                            }
                        }
                        else
                        {
                            if (mTotalPriceContract != mTotalPriceItem) //Total Price Revision
                            {
                                Sm.StdMsg(mMsgType.Warning, "Total price on list of item is unequal to BOQ for item : " + Sm.GetGrdStr(Grd2, i, 4));
                                TcOutgoingPayment.SelectedTabPage = TpItem;
                                return true;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < Grd6.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd6, i, 3).Length > 0)
                {
                    decimal mTotalPriceItem = 0m, mTotalPriceContract = 0m;
                    bool mIsEOF = false, mIsItem = false;

                    for (int j = 0; j < Grd3.Rows.Count; ++j)
                    {
                        if (Sm.GetGrdStr(Grd6, i, 3) == Sm.GetGrdStr(Grd3, j, 4))
                        {
                            mIsItem = true;
                            //mTotal += Sm.GetGrdDec(Grd3, j, 20);
                            mTotalPriceItem += Sm.GetGrdDec(Grd3, j, 11) * Sm.GetGrdDec(Grd3, j, 9);
                        }
                        if (j == Grd3.Rows.Count - 1) mIsEOF = true;
                    }

                    for (int k = 0; k < Grd6.Rows.Count; ++k)
                    {
                        if (Sm.GetGrdStr(Grd6, i, 3) == Sm.GetGrdStr(Grd6, k, 3))
                        {
                            if (IsInsert) mTotalPriceContract += Sm.GetGrdDec(Grd6, k, 19);
                            else mTotalPriceContract += Sm.GetGrdDec(Grd6, k, 24);
                        }
                        if (k == Grd6.Rows.Count - 1) mIsEOF = true;
                    }

                    if (mIsItem && mIsEOF)
                    {
                        if (IsInsert)
                        {
                            if (mTotalPriceContract != mTotalPriceItem) //Total Price Contract
                            {
                                Sm.StdMsg(mMsgType.Warning, "Total price on list of item is unequal to BOQ for item : " + Sm.GetGrdStr(Grd6, i, 4));
                                TcOutgoingPayment.SelectedTabPage = TpItem;
                                Sm.FocusGrd(Grd3, i, 20);
                                return true;
                            }
                        }
                        else
                        {
                            if (mTotalPriceContract != mTotalPriceItem) //Total Price Revision
                            {
                                Sm.StdMsg(mMsgType.Warning, "Total price on list of item is unequal to BOQ for item : " + Sm.GetGrdStr(Grd6, i, 4));
                                TcOutgoingPayment.SelectedTabPage = TpItem;
                                Sm.FocusGrd(Grd3, i, 20);
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool IsBOQAlreadyProceed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblSOContractHdr ");
            SQL.AppendLine("Where BOQDocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("And Status In ('O', 'A') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This BOQ data already proceed in SOC#" + Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsBankGuaranteeEmpty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr A ");
            SQL.AppendLine("Where DocNo = @Param ");
            SQL.AppendLine("And BankGuaranteeInd = 'Y' ");
            SQL.AppendLine("limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                if (Sm.IsTxtEmpty(TxtBankGuaranteeNo, "Bank Guarantee#", false)) { TcOutgoingPayment.SelectedTabPage = TpBankGuarantee; return true; }
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) { TcOutgoingPayment.SelectedTabPage = TpBankGuarantee; return true; }
                if (Sm.IsDteEmpty(DteBankGuaranteeDt, "Date")) { TcOutgoingPayment.SelectedTabPage = TpBankGuarantee; return true; }
                if (Sm.IsTxtEmpty(TxtBankGuaranteeAmt, "Amount", true)) { TcOutgoingPayment.SelectedTabPage = TpBankGuarantee; return true; }
            }

            return false;
        }

        private bool IsJointOperationEmpty()
        {
            if (Sm.GetLue(LueJOType) == mJointOperationKSOType)
            {
                if (Grd5.Rows.Count <= 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "You need to input Joint Operation data.");
                    TcOutgoingPayment.SelectedTabPage = TpJO;
                    Sm.FocusGrd(Grd5, 0, 1);
                    return true;
                }
            }

            return false;
        }

        private bool IsJointOperationInvalid()
        {
            if (Sm.GetLue(LueJOType) == mJointOperationKSOType)
            {
                for (int i = 0; i < Grd5.Rows.Count - 1; i++)
                {
                    if (Sm.IsGrdValueEmpty(Grd5, i, 1, false, "Joint Operation is empty.")) { TcOutgoingPayment.SelectedTabPage = TpJO; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, i, 2, false, "Joint Operation's Date is empty.")) { TcOutgoingPayment.SelectedTabPage = TpJO; return true; }
                    if (Sm.IsGrdValueEmpty(Grd5, i, 3, false, "Joint Operation's Number is empty.")) { TcOutgoingPayment.SelectedTabPage = TpJO; return true; }
                }
            }

            return false;
        }

        private bool IsExceedMaxChar(string Data)
        {
            return Data.Length > 4;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 31, false, "No in List of Items is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 4, false, "Item is empty.")) return true;

                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd3, Row, 31)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "No in List of Items exceed maximum length character (4).");
                        return true;
                    }

                    //for (int j = Row+1; j < Grd3.Rows.Count - 1; ++j)
                    //{
                    //    if (Sm.GetGrdStr(Grd3, Row, 31) == Sm.GetGrdStr(Grd3, j, 31))
                    //    {
                    //        Sm.StdMsg(mMsgType.Warning, "Duplicate No in List of Items found.");
                    //        return true;
                    //    }
                    //}
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; ++Row)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 23, false, "No in BOQ Service is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 24, false, "Sequence No in BOQ Service is empty.")) return true;

                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd2, Row, 23)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "No in BOQ Service exceed maximum length character (4).");
                        return true;
                    }
                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd2, Row, 24)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Sequence No in BOQ Service exceed maximum length character (3).");
                        return true;
                    }
                    //for (int j = Row+1; j < Grd2.Rows.Count - 1; ++j)
                    //{
                    //    if (Sm.GetGrdStr(Grd2, Row, 23) == Sm.GetGrdStr(Grd2, j, 23))
                    //    {
                    //        Sm.StdMsg(mMsgType.Warning, "Duplicate No in BOQ Service found.");
                    //        return true;
                    //    }
                    //}
                }
            }

            if (Grd6.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; ++Row)
                {
                    if (Sm.IsGrdValueEmpty(Grd6, Row, 25, false, "No in BOQ Inventory is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd6, Row, 26, false, "Sequence No in BOQ Inventory is empty.")) return true;

                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd6, Row, 25)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "No in BOQ Inventory exceed maximum length character (4).");
                        return true;
                    }
                    if (IsExceedMaxChar(Sm.GetGrdStr(Grd6, Row, 26)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Sequence No in BOQ Inventory exceed maximum length character (3).");
                        return true;
                    }
                    //for (int j = Row+1; j < Grd6.Rows.Count - 1; ++j)
                    //{
                    //    if (Sm.GetGrdStr(Grd6, Row, 25) == Sm.GetGrdStr(Grd6, j, 25))
                    //    {
                    //        Sm.StdMsg(mMsgType.Warning, "Duplicate No in BOQ Invetory found.");
                    //        return true;
                    //    }
                    //}
                }
            }

            return false;
        }

        private bool IsDocumentLOPProcessIndInvalid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LOPDocNo From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblLOPHdr B On A.LOPDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @Param ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("B.ProcessInd In ('S', 'C') ");
            SQL.AppendLine("Or B.CancelInd = 'Y' ");
            SQL.AppendLine("Or B.Status = 'C' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtBOQDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This LOP data is either Lose or Cancelled. LOP# : " + Sm.GetValue(SQL.ToString(), TxtBOQDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (!mIsSOContractItemMultiDeliveryDateEnabled)
            {
                if (Grd3.Rows.Count - 1 > 1)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum 1.");
                    return true;
                }
            }
            return false;
        }

        private bool IsCOASalesEmpty()
        {
            string AcNo = Sm.GetValue("Select B.AcNo4 From tblItem A " +
                            "Inner Join TblItemcategory B On A.ItCtCode = B.ItCtCode " +
                            "Where A.ItCode = '" + Sm.GetGrdStr(Grd3, 0, 4) + "'");
            if (AcNo.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Account sales on master item category, " + Environment.NewLine + " for item (" + Sm.GetGrdStr(Grd3, 0, 6) + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsCOAOptionEmpty()
        {
            string AcNo = Sm.GetValue("Select A.OptCode From TblOption A " +
                "Inner Join TblItemcategory B On A.OptCode = B.Acno4 " +
                "Inner Join TblItem C on B.ItCtCode = C.itCtCode " +
                "Where A.Optcat = 'AccountSet' And C.ItCode = '" + Sm.GetGrdStr(Grd3, 0, 4) + "'");

            if (AcNo.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Account sales on master item category, " + Environment.NewLine + " for item (" + Sm.GetGrdStr(Grd3, 0, 6) + ") " + Environment.NewLine + " should be listed on system option menu.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }

            if (Grd2.Rows.Count == 1 && Grd6.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 BOQ Service or 1 BOQ Inventory.");
                return true;
            }

            return false;
        }

        private MySqlCommand SaveSOContractHdr(string DocNo, string ProjectSequenceNo, string ProjectCode)
        {
            string ProfitCode = Sm.GetValue("Select B.profitCenterCode From tblLOPhdr A " +
                                                                  "Inner Join TblSite B On A.SiteCode = b.SiteCode " +
                                                                  "Where DocNo = (Select LopDocNo From tblBOQHdr Where DocNO = '" + TxtBOQDocNo.Text + "')");

            string CCCode = Sm.GetValue("Select A.CCCode From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "'  ) ");
            string ProjectScope = Sm.GetValue("Select A.ProjectScope From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "'  ) ");
            string ProjectResource = Sm.GetValue("Select A.projectresource From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "'  ) ");
            string ProjectType = Sm.GetValue("Select A.ProjectType From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "'  ) ");
            string ProjectName = Sm.GetValue("Select A.ProjectName From tblLOPhdr A Where A.DocNo = (Select LopDocNo From tblBOQHdr Where DocNo = '" + TxtBOQDocNo.Text + "'  ) ");
            string CoaOption = Sm.GetValue("Select OptCode From tblOption Where Optcat = 'AccountSet' limit 1 ");
            string Yr = Sm.Right(Sm.GetDte(DteDocDt).Substring(0, 4), 2);

            string Acno = string.Concat(ProfitCode, CCCode, ProjectScope, ProjectResource, ProjectType, Yr);
            //ngeceknya ke salah satu account saja buat dapetin 2 digit terakhir
            string lastDigit = GetDigitAcno(string.Concat(CoaOption, '.', Acno));

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractHdr(DocNo, DocType, LocalDocNo, DocDt, Status, CancelInd, ");
            SQL.AppendLine("CtCode, CtContactPersonName, BOQDocNo, CurCode, Amt, Amt2, ");
            SQL.AppendLine("SADNo, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, ");
            SQL.AppendLine("SAPhone, SAFax, SAEmail, SAMobile, JOType, PONo, ");
            SQL.AppendLine("SpCode, ShpMCode, Remark, ProjectDesc, ProjectCode, ProjectSequenceNo, ");
            SQL.AppendLine("ProjectCode2, CreateBy, CreateDt, BankGuaranteeNo, BankCode, BankGuaranteeDt, BankGuaranteeAmt, ");
            SQL.AppendLine("AmtBOM, NTPDocNo, PtCode, TaxCode) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocType, @LocalDocNo, @DocDt, 'O', @CancelInd, ");
            SQL.AppendLine("@CtCode, @CtContactPersonName, @BOQDocNo, ");
            SQL.AppendLine("(Select CurCode From TblBOQHdr Where DocNo=@BOQDocNo), @Amt, @Amt, ");
            SQL.AppendLine("@SADNo, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, ");
            SQL.AppendLine("@SAPhone, @SAFax, @SAEmail, @SAMobile, @JOType, @PONo, ");
            SQL.AppendLine("@SpCode, @ShpMCode, @Remark, @ProjectDesc, @AcNo, @ProjectSequenceNo, ");
            SQL.AppendLine("@ProjectCode2, @CreateBy, CurrentDateTime(), @BankGuaranteeNo, @BankCode, @BankGuaranteeDt, @BankGuaranteeAmt, ");
            SQL.AppendLine("@AmtBOM, @NTPDocNo, @PtCode, @TaxCode); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime()  ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='SOC'; ");

            SQL.AppendLine("Update TblSOContractHdr Set Status='A'  ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists(  ");
            SQL.AppendLine("    Select 1 From TblDocApproval  ");
            SQL.AppendLine("    Where DocType='SOC' ");
            SQL.AppendLine("    And DocNo=@DocNo  ");
            SQL.AppendLine("    );  ");

            if (IsInsert && mGenerateCustomerCOAFormat == "2")
            {
                SQL.AppendLine("Insert into TblCoa(AcNo, AcDesc, ActInd, Parent, Level, AcType, EntCode, SOCDocNo, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
                SQL.AppendLine("Select Concat(OptCode,'.', @AcNo) As AcNo, Concat(OptDesc, ' ', @ProjectName) As AcDesc, ");
                SQL.AppendLine("'Y' ActiveInd, OptCode As Parent,  (Length(Concat(OptCode,'.', @AcNo)) - Length(Replace(Concat(OptCode,'.', @AcNo), '.', ''))+1)   As Lvl, ");
                SQL.AppendLine("B.Actype, null, @DocNo, 'Sys', CurrentDatetime() , null, null  ");
                SQL.AppendLine("From tblOption A ");
                SQL.AppendLine("Inner Join TblCoa B On A.OptCode = B.Acno  ");
                SQL.AppendLine("Where A.OptCat = 'AccountSet'  ");

                SQL.AppendLine("On Duplicate Key Update LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mSOCDocType);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", Sm.GetLue(LueStatus));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtContactPersonName", TxtCustomerContactperson.Text);
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@SADNo", mSADNo);
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", TxtAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCity);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCnt);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@SPCode", Sm.GetLue(LueSPCode));
            Sm.CmParam<String>(ref cm, "@ShpMCode", Sm.GetLue(LueShpMCode));
            Sm.CmParam<String>(ref cm, "@JOType", Sm.GetLue(LueJOType));
            Sm.CmParam<String>(ref cm, "@PONo", TxtPONo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ProjectDesc", MeeProjectDesc.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ProjectName", ProjectName);
            if (mProjectAcNoFormula == "2") Sm.CmParam<String>(ref cm, "@AcNo", mProjectCodeForSOContract2);
            else Sm.CmParam<String>(ref cm, "@AcNo", mProjectCodeForSOContract2);
            Sm.CmParam<String>(ref cm, "@ProjectSequenceNo", ProjectSequenceNo);
            Sm.CmParam<String>(ref cm, "@ProjectCode2", mProjectCodeForSOContract2);
            Sm.CmParam<String>(ref cm, "@BankGuaranteeNo", TxtBankGuaranteeNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@BankGuaranteeDt", Sm.GetDte(DteBankGuaranteeDt));
            Sm.CmParam<Decimal>(ref cm, "@BankGuaranteeAmt", decimal.Parse(TxtBankGuaranteeAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtBOM", decimal.Parse(TxtAmtBOM.Text));
            Sm.CmParam<String>(ref cm, "@NTPDocNo", TxtNTPDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));

            return cm;
        }

        private MySqlCommand SaveSOContractDtl(string DocNo, string RevisionDocNo, bool IsInsert)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            bool IsFirst = true;

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 4).Length > 0)
                {
                    if (IsFirst)
                    {
                        SQL.AppendLine("Insert Into TblSOContractDtl ");
                        SQL.AppendLine("(DocNo, DNo, No, ItCode, PackagingUnitUomCode, QtyPackagingUnit, Qty, UPrice, UPriceBefTax, UPriceAfTax, TaxRate, TaxAmt, Amt, DeliveryDt,  Remark, Notes, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirst = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo" + r.ToString() + ", @No" + r.ToString() + ", @ItCode" + r.ToString() + ", @PackagingUnitUomCode" + r.ToString() + ", @QtyPackagingUnit" + r.ToString() + ", @Qty" + r.ToString() + ", @UPrice" + r.ToString() + ", @UPriceBefTax" + r.ToString() + ", @UPriceAfTax" + r.ToString() + ", @TaxRate" + r.ToString() + ", @TaxAmt" + r.ToString() + ", @Amt" + r.ToString() + ", @DeliveryDt" + r.ToString() + ",  @Remark" + r.ToString() + ", @Notes" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo" + r.ToString(), (r + 1).ToString());
                    Sm.CmParam<String>(ref cm, "@No" + r.ToString(), Sm.GetGrdStr(Grd3, r, 31));
                    Sm.CmParam<String>(ref cm, "@ItCode" + r.ToString(), Sm.GetGrdStr(Grd3, r, 4));
                    Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode" + r.ToString(), Sm.GetGrdStr(Grd3, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit" + r.ToString(), Sm.GetGrdDec(Grd3, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Qty" + r.ToString(), Sm.GetGrdDec(Grd3, r, 9));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice" + r.ToString(), Sm.GetGrdDec(Grd3, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceBefTax" + r.ToString(), Sm.GetGrdDec(Grd3, r, 16));
                    Sm.CmParam<Decimal>(ref cm, "@TaxRate" + r.ToString(), Sm.GetGrdDec(Grd3, r, 17));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt" + r.ToString(), Sm.GetGrdDec(Grd3, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceAfTax" + r.ToString(), Sm.GetGrdDec(Grd3, r, 19));
                    Sm.CmParam<Decimal>(ref cm, "@Amt" + r.ToString(), Sm.GetGrdDec(Grd3, r, 20));
                    Sm.CmParamDt(ref cm, "@DeliveryDt" + r.ToString(), Sm.GetGrdDate(Grd3, r, 21));
                    Sm.CmParam<String>(ref cm, "@Remark" + r.ToString(), Sm.GetGrdStr(Grd3, r, 22));
                    Sm.CmParam<String>(ref cm, "@Notes" + r.ToString(), Sm.GetGrdStr(Grd3, r, 30));
                }
            }
            if (SQL.Length > 0) SQL.AppendLine(";");

            if (IsInsert)
            {
                SQL.AppendLine("Insert Into TblSOContractBOM(SOContractDocNo, SOContractDNo, ItCode, Qty, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocNo, DNo, ItCode, Qty, CreateBy, CreateDt ");
                SQL.AppendLine("From TblSOContractDtl Where DocNo=@DocNo;");
            }
            else
            {
                SQL.AppendLine("Update TblSOContractBOM A ");
                SQL.AppendLine("Inner Join TblSOContractDtl B ");
                SQL.AppendLine("    On A.SOContractDocNo=B.DocNo ");
                SQL.AppendLine("    And A.SOContractDNo=B.DNo ");
                SQL.AppendLine("    And A.ItCode=B.ItCode ");
                SQL.AppendLine("    And B.DocNo=@DocNo ");
                SQL.AppendLine("Set A.Qty=B.Qty, A.LastUpBy=@UserCode, A.LastUpDt=@Dt ");
                SQL.AppendLine("Where A.SOContractDocNo=@DocNo And A.Qty<>B.Qty; ");

                SQL.AppendLine("Insert Into TblSOContractBOM(SOContractDocNo, SOContractDNo, ItCode, Qty, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocNo, T.DNo, T.ItCode, T.Qty, T.CreateBy, T.CreateDt ");
                SQL.AppendLine("From TblSOContractDtl T ");
                SQL.AppendLine("Where T.DocNo=@DocNo ");
                SQL.AppendLine("And Concat(T.DNo, T.ItCode) Not In ( ");
                SQL.AppendLine("    Select Concat(SOContractDNo, ItCode) From TblSOContractBOM ");
                SQL.AppendLine("    Where SOContractDocNo=@DocNo ");
                SQL.AppendLine("    );");
            }

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl(DocNo, SOCDocNo, DNo, No, Amt, PrevAmt, ");
            SQL.AppendLine("ItCode, Qty, UPrice, UPriceBefTax, UPriceAfTax, TaxRate, TaxAmt, ");
            SQL.AppendLine("DeliveryDt, PackagingUnitUomCode, QtyPackagingUnit, Remark, Notes, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @RevisionDocNo, @DocNo, DNo, No, Amt, ");
            SQL.AppendLine("Amt, ItCode, Qty, UPrice, UPriceBefTax, ");
            SQL.AppendLine("UPriceAfTax, TaxRate, TaxAmt, DeliveryDt, PackagingUnitUomCode, ");
            SQL.AppendLine("QtyPackagingUnit, Remark, Notes, CreateBy, CreateDt ");
            SQL.AppendLine("From TblSOContractDtl Where DocNo=@DocNo;");

            cm.CommandText = "Set @Dt:=CurrentDateTime(); " + SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RevisionDocNo", RevisionDocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @CurrentDt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblSOContractDtl ");
            SQL.AppendLine("(DocNo, DNo, No, ItCode, PackagingUnitUomCode, QtyPackagingUnit, Qty, UPrice, UPriceBefTax, UPriceAfTax, TaxRate, TaxAmt, Amt, DeliveryDt,  Remark, Notes, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @No, @ItCode, @PackagingUnitUomCode, @QtyPackagingUnit, @Qty, @UPrice, @UPriceBefTax, @UPriceAfTax, @TaxRate, @TaxAmt, @Amt, @DeliveryDt,  @Remark, @Notes, @UserCode, @CurrentDt); ");

            if (mIsSOContractBOMEnabled)
            {
                SQL.AppendLine("Insert Into TblSOContractBOM(SOContractDocNo, SOContractDNo, ItCode, Qty, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @Qty, @UserCode, @CurrentDt) ");
                SQL.AppendLine("On Duplicate Key Update ");
                SQL.AppendLine("    Qty = @Qty, LastUpBy = @UserCode, LastUpDt = @CurrentDt; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd3, Row, 31));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@UPriceBefTax", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd3, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd3, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfTax", Sm.GetGrdDec(Grd3, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd3, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 22));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd3, Row, 30));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl4(string DocNo, string RevisionDocNo, bool IsInsert)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            bool IsFirst = true;

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 3).Length > 0)
                {
                    if (IsFirst)
                    {
                        SQL.AppendLine("Insert Into TblSOContractDtl4 ");
                        SQL.AppendLine("(DocNo, DNo, SeqNo, No, BOQDocNo, BOMDocNo, BOMDNo, ItCode, Amt, Qty, UPrice, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirst = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo" + r.ToString() + ", @SeqNo" + r.ToString() + ", @No" + r.ToString() + ", @BOQDocNo, @BOMDocNo" + r.ToString() + ", @BOMDNo" + r.ToString() + ", @ItCode" + r.ToString() + ", @Amt" + r.ToString() + ", @Qty" + r.ToString() + ", @UPrice" + r.ToString() + ", @Remark" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo" + r.ToString(), (r + 1).ToString());
                    Sm.CmParam<String>(ref cm, "@No" + r.ToString(), Sm.GetGrdStr(Grd2, r, 23));
                    Sm.CmParam<String>(ref cm, "@SeqNo" + r.ToString(), Sm.GetGrdStr(Grd2, r, 24));
                    Sm.CmParam<String>(ref cm, "@BOMDocNo" + r.ToString(), Sm.GetGrdStr(Grd2, r, 1));
                    Sm.CmParam<String>(ref cm, "@BOMDNo" + r.ToString(), Sm.GetGrdStr(Grd2, r, 2));
                    Sm.CmParam<String>(ref cm, "@ItCode" + r.ToString(), Sm.GetGrdStr(Grd2, r, 3));
                    if (IsInsert)
                    {
                        Sm.CmParam<Decimal>(ref cm, "@Amt" + r.ToString(), Sm.GetGrdDec(Grd2, r, 17));
                        Sm.CmParam<Decimal>(ref cm, "@Qty" + r.ToString(), Sm.GetGrdDec(Grd2, r, 18));
                        Sm.CmParam<Decimal>(ref cm, "@UPrice" + r.ToString(), Sm.GetGrdDec(Grd2, r, 19));
                    }
                    else
                    {
                        Sm.CmParam<Decimal>(ref cm, "@Amt" + r.ToString(), Sm.GetGrdDec(Grd2, r, 22));
                        Sm.CmParam<Decimal>(ref cm, "@Qty" + r.ToString(), Sm.GetGrdDec(Grd2, r, 20));
                        Sm.CmParam<Decimal>(ref cm, "@UPrice" + r.ToString(), Sm.GetGrdDec(Grd2, r, 21));
                    }
                    Sm.CmParam<String>(ref cm, "@Remark" + r.ToString(), Sm.GetGrdStr(Grd2, r, 16));
                }
            }
            if (SQL.Length > 0) SQL.AppendLine(";");

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl4 ");
            SQL.AppendLine("(DocNo, SOCDocNo, DNo, No, ItCode, Amt, Qty, UPrice, PrevAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @RevisionDocNo, @DocNo, DNo, No, ItCode, Amt, Qty, UPrice, Amt, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblSOContractDtl4 Where DocNo=@DocNo;");

            cm.CommandText = "Set @Dt:=CurrentDateTime(); " + SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RevisionDocNo", RevisionDocNo);
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveSOContractDtl4(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSOContractDtl4(DocNo, DNo, SeqNo, No, BOQDocNo, BOMDocNo, BOMDNo, ItCode, Amt, Qty, UPrice, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @SeqNo, @No, @BOQDocNo, @BOMDocNo, @BOMDNo, @ItCode, @Amt, @Qty, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
        //    Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd2, Row, 23));
        //    Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd2, Row, 24));
        //    Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd2, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 3));
        //    if (IsInsert)
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 17));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 18));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 19));
        //    }
        //    else
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 22));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 20));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 21));
        //    }
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 16));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //private MySqlCommand SaveSOContractRevisionDtl4(string RevisionDocNo, string DocNo, int Row, string StateInd)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSOContractRevisionDtl4(DocNo, SOCDocNo, DNo, No, ItCode, Amt, Qty, UPrice, PrevAmt, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @No, @ItCode, @Amt, @Qty, @UPrice, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", RevisionDocNo);
        //    Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
        //    Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd2, Row, 23));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 3));
        //    if (StateInd == "I")
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 17));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 18));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 19));
        //    }
        //    else
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, Row, 22));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 20));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd2, Row, 21));
        //    }
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 16));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateSOContractDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();
            string dd = string.Empty;

            SQL.AppendLine("Update TblSOContractDtl Set ");
            SQL.AppendLine("    DeliveryDt=@DeliveryDt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            dd = Sm.GetGrdDate(Grd3, Row, 21);

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParamDt(ref cm, "@DeliveryDt", dd);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand SaveSOContractDtl3(string DocNo, int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblSOContractDtl3(DocNo, DNo, JOCode, JODocDt, JODocNo, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Values(@DocNo, @DNo, @JOCode, @JODocDt, @JODocNo, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@JOCode", Sm.GetGrdStr(Grd5, Row, 0));
            Sm.CmParamDt(ref cm, "@JODocDt", Sm.GetGrdDate(Grd5, Row, 2));
            Sm.CmParam<String>(ref cm, "@JODocNo", Sm.GetGrdStr(Grd5, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDtl5(string DocNo, string RevisionDocNo, bool IsInsert)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            bool IsFirst = true;

            for (int r = 0; r < Grd6.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd6, r, 3).Length > 0)
                {
                    if (IsFirst)
                    {
                        SQL.AppendLine("Insert Into TblSOContractDtl5 ");
                        SQL.AppendLine("(DocNo, DNo, SeqNo, No, BOQDocNo, BOMDocNo, BOMDNo, ItCode, Amt, Qty, UPrice, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirst = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("(@DocNo, @DNo" + r.ToString() + ", @SeqNo" + r.ToString() + ", @No" + r.ToString() + ", @BOQDocNo, @BOMDocNo" + r.ToString() + ", @BOMDNo" + r.ToString() + ", @ItCode" + r.ToString() + ", @Amt" + r.ToString() + ", @Qty" + r.ToString() + ", @UPrice" + r.ToString() + ", @Remark" + r.ToString() + ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo" + r.ToString(), (r + 1).ToString());
                    Sm.CmParam<String>(ref cm, "@No" + r.ToString(), Sm.GetGrdStr(Grd6, r, 25));
                    Sm.CmParam<String>(ref cm, "@SeqNo" + r.ToString(), Sm.GetGrdStr(Grd6, r, 26));
                    Sm.CmParam<String>(ref cm, "@BOMDocNo" + r.ToString(), Sm.GetGrdStr(Grd6, r, 1));
                    Sm.CmParam<String>(ref cm, "@BOMDNo" + r.ToString(), Sm.GetGrdStr(Grd6, r, 2));
                    Sm.CmParam<String>(ref cm, "@ItCode" + r.ToString(), Sm.GetGrdStr(Grd6, r, 3));
                    if (IsInsert)
                    {
                        Sm.CmParam<Decimal>(ref cm, "@Amt" + r.ToString(), Sm.GetGrdDec(Grd6, r, 19));
                        Sm.CmParam<Decimal>(ref cm, "@Qty" + r.ToString(), Sm.GetGrdDec(Grd6, r, 20));
                        Sm.CmParam<Decimal>(ref cm, "@UPrice" + r.ToString(), Sm.GetGrdDec(Grd6, r, 21));
                    }
                    else
                    {
                        Sm.CmParam<Decimal>(ref cm, "@Amt" + r.ToString(), Sm.GetGrdDec(Grd6, r, 24));
                        Sm.CmParam<Decimal>(ref cm, "@Qty" + r.ToString(), Sm.GetGrdDec(Grd6, r, 22));
                        Sm.CmParam<Decimal>(ref cm, "@UPrice" + r.ToString(), Sm.GetGrdDec(Grd6, r, 23));
                    }
                    Sm.CmParam<String>(ref cm, "@Remark" + r.ToString(), Sm.GetGrdStr(Grd6, r, 18));
                }
            }
            if (SQL.Length > 0) SQL.AppendLine(";");

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl5 ");
            SQL.AppendLine("(DocNo, SOCDocNo, DNo, No, ItCode, Amt, Qty, UPrice, PrevAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @RevisionDocNo, @DocNo, DNo, No, ItCode, Amt, Qty, UPrice, Amt, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblSOContractDtl5 Where DocNo=@DocNo;");

            cm.CommandText = "Set @Dt:=CurrentDateTime(); " + SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RevisionDocNo", RevisionDocNo);
            Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveSOContractDtl5(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSOContractDtl5(DocNo, DNo, SeqNo, No, BOQDocNo, BOMDocNo, BOMDNo, ItCode, Amt, Qty, UPrice, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @SeqNo, @No, @BOQDocNo, @BOMDocNo, @BOMDNo, @ItCode, @Amt, @Qty, @UPrice, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
        //    Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd6, Row, 25));
        //    Sm.CmParam<String>(ref cm, "@SeqNo", Sm.GetGrdStr(Grd6, Row, 26));
        //    Sm.CmParam<String>(ref cm, "@BOQDocNo", TxtBOQDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@BOMDocNo", Sm.GetGrdStr(Grd6, Row, 1));
        //    Sm.CmParam<String>(ref cm, "@BOMDNo", Sm.GetGrdStr(Grd6, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd6, Row, 3));
        //    if (IsInsert)
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 19));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd6, Row, 20));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd6, Row, 21));
        //    }
        //    else
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 24));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd6, Row, 22));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd6, Row, 23));
        //    }
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 18));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateIndicatorForSOContractDtl(string DocNo, string RevisionDocNo)
        {
            string sSQL = string.Empty;

            sSQL += "Update TblSOContractDtl A ";
            sSQL += "Inner Join (Select Max(DocNo) DocNo From TblSOContractRevisionHdr Where SOCDocNo = @DocNo And DocNo != @RevisionDocNo) B On 0 = 0 ";
            sSQL += "Inner Join TblSOContractRevisionDtl C On B.DocNo = C.DocNo ";
            sSQL += "    And C.SOCDocNo = @DocNo ";
            sSQL += "    And A.DNo = C.DNo ";
            sSQL += "    And A.ItCode = C.ItCode ";
            sSQL += "Set A.ProcessInd = C.ProcessInd, ";
            sSQL += "A.ProcessIndForDR = C.ProcessIndForDR, ";
            sSQL += "A.ProcessIndForDRCBD = C.ProcessIndForDRCBD, ";
            sSQL += "A.ProcessIndForDO = C.ProcessIndForDO ";
            sSQL += "Where A.DocNo = @DocNo; ";

            sSQL += "Update TblSOContractRevisionDtl A ";
            sSQL += "Inner Join TblSOContractDtl B On A.SOCDocNo = B.DocNo ";
            sSQL += "    And A.DocNo = @RevisionDocNo ";
            sSQL += "    And A.DNo = B.DNo ";
            sSQL += "Set A.ProcessInd = B.ProcessInd, ";
            sSQL += "A.ProcessIndForDR = B.ProcessIndForDR, ";
            sSQL += "A.ProcessIndForDRCBD = B.ProcessIndForDRCBD, ";
            sSQL += "A.ProcessIndForDO = B.ProcessIndForDO ";
            sSQL += "Where A.DocNo = @RevisionDocNo; ";

            var cm = new MySqlCommand() { CommandText = sSQL };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@RevisionDocNo", RevisionDocNo);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionHdr(string RevisionDocNo, string DocNo, string StateInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionHdr(DocNo, SOCDocNo, DocDt, PONo, PtCode, Status, DocType, Amt, PrevAmt, Amt2, PrevAmt2, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @SOCDocNo, @DocDt, @PONo, @PtCode, 'O', '2', @Amt, @Amt, @Amt2, @Amt2, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (IsRevisionNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='SOCRev'; ");
            }

            SQL.AppendLine("Update TblSOContractRevisionHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='SOCRev' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            if (StateInd == "E")
            {
                SQL.AppendLine("Delete From TblSOContractDtl Where DocNo = @SOCDocNo; ");
                SQL.AppendLine("Delete From TblSOContractDtl2 Where DocNo = @SOCDocNo; ");
                SQL.AppendLine("Delete From TblSOContractDtl3 Where DocNo = @SOCDocNo; ");
                SQL.AppendLine("Delete From TblSOContractDtl4 Where DocNo = @SOCDocNo; ");
                SQL.AppendLine("Delete From TblSOContractDtl5 Where DocNo = @SOCDocNo; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", RevisionDocNo);
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PONo", TxtPONo.Text);
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmtBOM.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractRevisionDtl(string RevisionDocNo, string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSOContractRevisionDtl(DocNo, SOCDocNo, DNo, No, Amt, PrevAmt, ");
            SQL.AppendLine("ItCode, Qty, UPrice, UPriceBefTax, UPriceAfTax, TaxRate, TaxAmt, ");
            SQL.AppendLine("DeliveryDt, PackagingUnitUomCode, QtyPackagingUnit, Remark, Notes, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @No, @Amt, @Amt, ");
            SQL.AppendLine("@ItCode, @Qty, @UPrice, @UPriceBefTax, @UPriceAfTax, @TaxRate, @TaxAmt, ");
            SQL.AppendLine("@DeliveryDt, @PackagingUnitUomCode, @QtyPackagingUnit, @Remark, @Notes, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", RevisionDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd3, Row, 31));
            Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd3, Row, 20));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd3, Row, 4));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd3, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@UPriceBefTax", Sm.GetGrdDec(Grd3, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd3, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd3, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@UPriceAfTax", Sm.GetGrdDec(Grd3, Row, 19));
            Sm.CmParamDt(ref cm, "@DeliveryDt", Sm.GetGrdDate(Grd3, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 22));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd3, Row, 30));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }



        //private MySqlCommand SaveSOContractRevisionDtl5(string RevisionDocNo, string DocNo, int Row, string StateInd)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSOContractRevisionDtl5(DocNo, SOCDocNo, DNo, No, ItCode, Amt, Qty, UPrice, PrevAmt, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @SOCDocNo, @DNo, @No, @ItCode, @Amt, @Qty, @UPrice, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", RevisionDocNo);
        //    Sm.CmParam<String>(ref cm, "@SOCDocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
        //    Sm.CmParam<String>(ref cm, "@No", Sm.GetGrdStr(Grd6, Row, 25));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd6, Row, 3));
        //    if (StateInd == "I")
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 19));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd6, Row, 20));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd6, Row, 21));
        //    }
        //    else
        //    {
        //        Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd6, Row, 24));
        //        Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd6, Row, 22));
        //        Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd6, Row, 23));
        //    }
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd6, Row, 18));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand UpdateBOQStatusForNTPSOCInd(string BOQDocNo, string StateInd)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (StateInd == "I")
            {
                SQL.AppendLine("Update TblBOQDtl2 Set ");
                SQL.AppendLine("    NTPSOCInd = '2', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And MRDocNo Is Null ");
                SQL.AppendLine("And MRDNo Is Null; ");

                SQL.AppendLine("Update TblBOQDtl3 Set ");
                SQL.AppendLine("    NTPSOCInd = '2', LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And MRDocNo Is Null ");
                SQL.AppendLine("And MRDNo Is Null; ");
            }
            else
            {
                SQL.AppendLine("Update TblBOQDtl2 Set ");
                SQL.AppendLine("    NTPSOCInd = NULL, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And MRDocNo Is Null ");
                SQL.AppendLine("And MRDNo Is Null ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblSOContractHdr ");
                SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
                SQL.AppendLine("    And CancelInd = 'Y' ");
                SQL.AppendLine("); ");

                SQL.AppendLine("Update TblBOQDtl3 Set ");
                SQL.AppendLine("    NTPSOCInd = NULL, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And MRDocNo Is Null ");
                SQL.AppendLine("And MRDNo Is Null ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblSOContractHdr ");
                SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
                SQL.AppendLine("    And CancelInd = 'Y' ");
                SQL.AppendLine("); ");

                SQL.AppendLine("Update TblBOQDtl2 Set NTPSOCInd = '1' ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblSOContractHdr ");
                SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
                SQL.AppendLine("    And CancelInd = 'Y' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblWBSHdr ");
                SQL.AppendLine("    Where BOQDocNo = @DocNo ");
                SQL.AppendLine("    And CancelInd = 'N' ");
                SQL.AppendLine("); ");

                SQL.AppendLine("Update TblBOQDtl3 Set NTPSOCInd = '1' ");
                SQL.AppendLine("Where DocNo = @DocNo ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblSOContractHdr ");
                SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
                SQL.AppendLine("    And CancelInd = 'Y' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblWBSHdr ");
                SQL.AppendLine("    Where BOQDocNo = @DocNo ");
                SQL.AppendLine("    And CancelInd = 'N' ");
                SQL.AppendLine("); ");
            }

            Sm.CmParam<String>(ref cm, "@DocNo", BOQDocNo);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand UpdateSOContractFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateSOContractFile2(string DocNo, string FileName2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    FileName2=@FileName2 ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName2", FileName2);

            return cm;
        }

        private MySqlCommand UpdateSOContractFile3(string DocNo, string FileName3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    FileName3=@FileName3 ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName3", FileName3);

            return cm;
        }

        public static string GetDigitAcno(string AcNo)
        {
            string Lastdigit = Sm.GetValue("Select MAX(Right(AcNo, 2)) From TblCoa Where Acno like '" + AcNo + "%'");

            if (Lastdigit.Length > 0)
            {
                Lastdigit = Sm.Right(Convert.ToString(string.Concat("00", (decimal.Parse(Lastdigit) + 1))), 2);
            }
            else
            {
                Lastdigit = "01";
            }

            return Lastdigit;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            /*
             * Bakal ada effort waktu di revision nya ini
             * Kalau udah di ARDP, di MR dst, cuekkin
             * Kalau udah di DR, cuekkin
             * Kalau udah di DO tapi belom di SLI :
             *     untuk item yg udah di DO tapi belom SLI, ada perubahan harga nggak ?
             *     kalau ada :
             *         bikin journal (COA nya persis kayak journal DO yg udah terbentuk)
             *         amount nya, ambil dari selisih nya
             *         misalkan : harga awal 1000 jadi 1500
             *         maka amount journal nya adalah 500
             *         kalau selisih nya naik (+, misal 1000 jadi 1500), letak debit credit nya tetep
             *         kalau selisihnya turun (-, misal 1500 jadi 1000), letak debit credit nya dibalik
             */
            if (ChkCancelInd.Checked == true)
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;
            }
            else
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid2()) return;
            }

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateSOContractHdr());
            //cml.Add(UpdateBOQStatusForNTPSOCInd(TxtBOQDocNo.Text, "E"));
            if (!ChkCancelInd.Checked)
            {
                //save SO Contract Revision
                string RevisionDocNo = string.Concat(TxtDocNo.Text, "/", GenerateDocNo());

                cml.Add(SaveSOContractRevisionHdr(RevisionDocNo, TxtDocNo.Text, "E"));

                cml.Add(SaveSOContractDtl(TxtDocNo.Text, RevisionDocNo, false));

                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //{
                //    if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0)
                //    {
                //        cml.Add(SaveSOContractDtl(TxtDocNo.Text, Row));
                //        cml.Add(SaveSOContractRevisionDtl(RevisionDocNo, TxtDocNo.Text, Row));
                //    }
                //}

                cml.Add(SaveSOContractDtl4(TxtDocNo.Text, RevisionDocNo, false));

                //for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                //{
                //    if (Sm.GetGrdStr(Grd2, Row, 3).Length > 0)
                //    {
                //        cml.Add(SaveSOContractDtl4(TxtDocNo.Text, Row));
                //        cml.Add(SaveSOContractRevisionDtl4(RevisionDocNo, TxtDocNo.Text, Row, "E"));
                //    }
                //}

                if (Grd5.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd5.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd5, Row, 0).Length > 0)
                            cml.Add(SaveSOContractDtl3(TxtDocNo.Text, Row));
                    }
                }

                cml.Add(SaveSOContractDtl5(TxtDocNo.Text, RevisionDocNo, false));

                //if (Grd6.Rows.Count > 1)
                //{
                //    for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                //    {
                //        if (Sm.GetGrdStr(Grd6, Row, 3).Length > 0)
                //        {
                //            cml.Add(SaveSOContractDtl5(TxtDocNo.Text, Row));
                //            cml.Add(SaveSOContractRevisionDtl5(RevisionDocNo, TxtDocNo.Text, Row, "E"));
                //        }
                //    }
                //}

                cml.Add(UpdateIndicatorForSOContractDtl(TxtDocNo.Text, RevisionDocNo));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void EditData2()
        {

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0)
                        cml.Add(UpdateSOContractDtl(TxtDocNo.Text, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }


        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOContractAlreadyCancelled() ||
                IsSOContractAlreadyFullfiled() ||
                IsSOContractAlreadyProcessedToMR() ||
                IsSOContractAlreadyProcessedToProjectImplementation() ||
                IsSOContractAlreadyProcessedToARDP() ||
                IsSOContractAlreadyProcessedToDR() ||
                IsTotalPriceInvalid(IsInsert) ||
                IsSOContractDownpaymentNotCancelled()||
                IsSOContractAlreadyProcessedToMRService()
                ;
        }

        private bool IsSOContractAlreadyProcessedToDR()
        {
            string sSQL = string.Empty;

            sSQL += "Select A.DocNo ";
            sSQL += "From TblDRHdr A ";
            sSQL += "Inner Join TblDRDtl B On A.DocNo = B.DocNo  ";
            sSQL += "    And B.SODocNo = @Param ";
            sSQL += "    And A.CancelInd = 'N' ";
            sSQL += "Limit 1; ";

            if (Sm.IsDataExist(sSQL, TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO is already processed to DR#" + Sm.GetValue(sSQL, TxtDocNo.Text));
                return true;
            }

            return false;
        }

        private bool IsSOContractAlreadyProcessedToMR()
        {
            string sSQL = string.Empty;

            sSQL += "Select A.DocNo ";
            sSQL += "From TblMaterialRequestHdr A ";
            sSQL += "Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo ";
            sSQL += "    And A.SOCDocNo = @Param ";
            sSQL += "Inner Join TblMaterialRequestDtl C ON A.DocNo = C.DocNo ";
            sSQL += "    And C.CancelInd = 'N' ";
            sSQL += "    And C.Status In ('O', 'A') ";
            sSQL += "Limit 1; ";

            if (Sm.IsDataExist(sSQL, TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This SO is already processed to MR#" + Sm.GetValue(sSQL, TxtDocNo.Text));
                return true;
            }

            return false;
        }
        private bool IsSOContractAlreadyProcessedToMRService()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblMaterialRequestServiceHdr A ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOCDocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblMaterialRequestServiceDtl C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("Where A.SOCDocNo = @Param ");
            SQL.AppendLine("And (C.CancelInd = 'N' And C.Status <> 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has already processed to PR for Service : " + Sm.GetValue(SQL.ToString(), TxtDocNo.Text) + ".");
                return true;
            }

            return false;
        }

        private bool IsSOContractAlreadyProcessedToProjectImplementation()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblProjectImplementationHdr A ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr B On A.SOContractDocNo = B.DocNo ");
            SQL.AppendLine("Where B.SOCDocNo = @Param ");
            SQL.AppendLine("And (A.CancelInd = 'N' And A.Status <> 'C') ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document has already processed to Project Implementation : " + Sm.GetValue(SQL.ToString(), TxtDocNo.Text) + ".");
                return true;
            }

            return false;
        }
        private bool IsCancelledDataNotValid2()
        {
            return
                Sm.IsLueEmpty(LueStatus, "Status") ||
                IsSOContractAlreadyCancelled() ||
                IsSOContractAlreadyFullfiled() ||
                IsSOContractAlreadyProcessedToARDP() ||
                IsTotalPriceInvalid(IsInsert) ||
                IsSOContractDownpaymentNotCancelled() ||
                IsGrdValueNotValid()
                ;
        }

        private MySqlCommand UpdateSOContractHdr()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, ");
            SQL.AppendLine("    PONo = @PONo, PtCode = @PtCode, Remark=@Remark, ");
            SQL.AppendLine("    Amt = @Amt, AmtBOM = @AmtBOM, DocDt=@DocDt, TaxCode=@TaxCode,  ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@PONo", TxtPONo.Text);
            Sm.CmParam<String>(ref cm, "@PtCode", Sm.GetLue(LuePtCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtBOM", Decimal.Parse(TxtAmtBOM.Text));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsSOContractAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOContractHdr " +
                    "Where DocNo=@DocNo And (CancelInd='Y' Or Status = 'C') ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already cancelled.");
                ChkCancelInd.Checked = true;
                return true;
            }
            return false;
        }

        private bool IsSOContractAlreadyFullfiled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblSOContractDtl " +
                    "Where DocNo=@DocNo And processInd = 'F';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already fullfiled.");
                Sm.SetLue(LueStatus, "F");
                return true;
            }
            return false;
        }

        private bool IsSOContractAlreadyProcessedToARDP()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SODocNo ");
            SQL.AppendLine("From TblARDownpayment ");
            SQL.AppendLine("Where SODocNo=@SODocNo And CancelInd = 'N' And Status = 'A' And CtCode=@CtCode ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

            if (Sm.IsDataExist(cm) && mIsSOUseARDPValidated)
            {
                Sm.StdMsg(mMsgType.Warning, "This document already use to AR DownPayment.");
                return true;
            }
            return false;
        }



        private bool IsSOContractDownpaymentNotCancelled()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DocNo FROM TblSOContractDownpaymentHdr ");
            SQL.AppendLine("WHERE SOContractDocNo = @SODocNo ");
            SQL.AppendLine("AND CancelInd = 'N' ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document is currently being used by SO Contract Termin/Downpayment (I).");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSOContractHdr(DocNo);
                ShowSOContractDtl(DocNo);
                ShowSOContractDtl4(DocNo);
                ShowSOContractDtl5(DocNo);
                ShowSOContractDtl3(DocNo);
                ShowARDownPayment(DocNo);

                if (mDocNo.Length > 0 && mIsSOContract2AmtIsZero)
                {
                    TxtAmt.EditValue = Sm.FormatNum(0m, 0);
                    TxtAmtBOM.EditValue = Sm.FormatNum(0m, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.LocalDocNo, A.DocDt, A.Status, A.CancelInd, ");
            SQL.AppendLine("A.CtCode, A.CtContactPersonName, ");
            SQL.AppendLine("A.BOQDocNo, A.Amt, A.SAName, ifnull(A.SpCode, B.SpCode) As SpCode, ");
            SQL.AppendLine("A.ShpMCode, A.Remark, A.SADNo, ");
            SQL.AppendLine("IfNull(A.SAAddress, E.Address) As SAAddress, ");
            SQL.AppendLine("IfNull(A.SACityCode, E.CityCode) As SACityCode, ");
            SQL.AppendLine("IfNull(C.CityName, F.CityName) As CityName, ");
            SQL.AppendLine("IfNull(A.SACntCode, E.CntCode) As SACntCode, ");
            SQL.AppendLine("IfNull(D.CntName, G.CntName) As CntName, ");
            SQL.AppendLine("IfNull(A.SAPostalCD, E.PostalCD) As SAPostalCode, ");
            SQL.AppendLine("IfNull(A.SAPhone, E.Phone) As SAPhone, ");
            SQL.AppendLine("IfNull(A.SAFax, E.Fax) As SAFax, ");
            SQL.AppendLine("IfNull(A.SAEmail, E.Email) As SAEmail, A.JOType, A.PONo, ");
            SQL.AppendLine("IfNull(A.SAMobile, E.Mobile) As SAMobile, A.CancelReason, A.ProjectDesc, ");
            SQL.AppendLine("I.Projectname,  A.BankGuaranteeNo, A.BankCode, A.BankGuaranteeDt, A.BankGuaranteeAmt, ");
            SQL.AppendLine("A.AmtBOM, A.NTPDocNo, A.FileName, A.FileName2, A.FileName3, A.PtCode, I.ProjectCode, A.TaxCode, A.Remark ");
            SQL.AppendLine("From TblSOContractHdr A  ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblCity C On A.SACityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblCountry D On A.SACntCode=D.CntCode ");
            SQL.AppendLine("Left Join TblCustomerShipAddress E ");
            SQL.AppendLine("    On A.CtCode=E.CtCode ");
            SQL.AppendLine("    And E.DNo=( ");
            SQL.AppendLine("        Select T1.DNo ");
            SQL.AppendLine("        From TblCustomerShipAddress T1 ");
            SQL.AppendLine("        Inner Join TblSOContractHdr T2 On T2.DocNo=@DocNo And T1.CtCode=T2.CtCode And Upper(T1.Name)=Upper(T2.SAName) And T1.Address=T2.SAAddress And T2.SAAddress Is Not Null ");
            SQL.AppendLine("        Where T1.Address Is Not Null ");
            SQL.AppendLine("        Limit 1 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblCity F On E.CityCode=F.CityCode ");
            SQL.AppendLine("Left Join TblCountry G On E.CntCode=G.CntCode ");
            SQL.AppendLine("Left Join TblLOphdr H On B.LOPDocNo = H.DocNO ");
            SQL.AppendLine("Left Join TblProjectGroup I On H.PGCode = I.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo",

                    //1-5
                    "LocalDocNo", "DocDt", "Status", "CancelInd","CtCode",    

                    //6-10
                    "CtContactPersonName", "BOQDocNo", "Amt", "SAName", "SpCode", 

                    //11-15
                    "ShpMCode", "Remark", "SADNo", "SAAddress", "SACityCode", 

                    //16-20
                    "CityName", "SACntCode", "CntName", "SAPostalCode", "SAPhone", 

                    //21-25
                    "SAFax", "SAEmail", "SAMobile", "CancelReason", "ProjectDesc",

                    //26-30
                    "ProjectName", "JOType",  "BankGuaranteeNo", "BankCode", "BankGuaranteeDt",

                    //31-35
                    "PONo", "BankGuaranteeAmt", "AmtBOM", "NTPDocNo", "FileName",

                    //36-40
                    "FileName2", "FileName3", "PtCode", "ProjectCode", "TaxCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                    Sm.SetLue(LueStatus, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[5]));
                    TxtCustomerContactperson.EditValue = Sm.DrStr(dr, c[6]);
                    TxtBOQDocNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[9]);
                    SetLueSPCode(ref LueSPCode);
                    Sm.SetLue(LueSPCode, Sm.DrStr(dr, c[10]));
                    SetLueDTCode(ref LueShpMCode);
                    Sm.SetLue(LueShpMCode, Sm.DrStr(dr, c[11]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[12]);
                    mSADNo = Sm.DrStr(dr, c[13]);
                    TxtAddress.EditValue = Sm.DrStr(dr, c[14]);
                    mCity = Sm.DrStr(dr, c[15]);
                    TxtCity.EditValue = Sm.DrStr(dr, c[16]);
                    mCnt = Sm.DrStr(dr, c[17]);
                    TxtCountry.EditValue = Sm.DrStr(dr, c[18]);
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[19]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[20]);
                    TxtFax.EditValue = Sm.DrStr(dr, c[21]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[22]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[23]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[24]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    MeeProjectDesc.EditValue = Sm.DrStr(dr, c[25]);
                    TxtProjectName.EditValue = Sm.DrStr(dr, c[26]);
                    Sm.SetLue(LueJOType, Sm.DrStr(dr, c[27]));
                    TxtBankGuaranteeNo.EditValue = Sm.DrStr(dr, c[28]);
                    Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[29]));
                    Sm.SetDte(DteBankGuaranteeDt, Sm.DrStr(dr, c[30]));
                    TxtPONo.EditValue = Sm.DrStr(dr, c[31]);
                    TxtBankGuaranteeAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[32]), 0);
                    TxtAmtBOM.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[33]), 0);
                    TxtNTPDocNo.EditValue = Sm.DrStr(dr, c[34]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[35]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[36]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[37]);
                    if (TxtFile.Text.Length > 0) ChkFile.Checked = true;
                    if (TxtFile2.Text.Length > 0) ChkFile2.Checked = true;
                    if (TxtFile3.Text.Length > 0) ChkFile3.Checked = true;
                    Sm.SetLue(LuePtCode, Sm.DrStr(dr, c[38]));
                    TxtProjectCode.EditValue = Sm.DrStr(dr, c[39]);
                    Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[40]));
                }, true
            );
        }

        private void ShowSOContractDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From (  ");
            SQL.AppendLine("   Select B.Dno, B.No, B.ItCode, G.ItName, G.Specification, I.CtItCode, I.CtItName, ");
            SQL.AppendLine("   B.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, ");
            SQL.AppendLine("   B.UPrice, 0 As Discount, 0 As DiscountAmt,  ");
            SQL.AppendLine("   B.UPrice As UPriceAfterDiscount, ");
            SQL.AppendLine("   0.00 As PromoRate, ");
            SQL.AppendLine("   B.UPriceBefTax As UPriceBefore, ");
            SQL.AppendLine("   B.UPriceAfTax As UPriceAfterTax, ");
            SQL.AppendLine("   B.Amt As Total, ");
            SQL.AppendLine("   B.TaxRate, ");
            SQL.AppendLine("   B.TaxAmt,  ");
            SQL.AppendLine("   B.DeliveryDt, B.Remark, IfNull(J.Volume, 0.00) As Volume, (B.QtyPackagingUnit * IfNull(J.Volume, 0.00)) As TotalVolume,  ");
            SQL.AppendLine("   (Select parValue From tblparameter Where parCode = 'ItemVolumeUom') As VolUom, ");
            SQL.AppendLine("   B.Notes , K.SeqNo ");
            SQL.AppendLine("   From TblSOContractHdr A    ");
            SQL.AppendLine("   Inner Join TblSOContractDtl B On A.DocNo=B.DocNo   ");
            SQL.AppendLine("   Inner Join TblItem G On B.ItCode=G.ItCode  ");
            SQL.AppendLine("   Left Join TblCustomerItem I On B.ItCode=I.ItCode And A.CtCode=I.CtCode  ");
            SQL.AppendLine("   Left Join tblitempackagingunit J On G.ItCode = J.ItCode And G.SalesUomCode = J.UomCode  ");
            SQL.AppendLine("   Left Join ( ");
            SQL.AppendLine("          Select DocNo, SeqNo, No, ItCode ");
            SQL.AppendLine("          From TblSOContractDtl4 ");
            SQL.AppendLine("          Union All ");
            SQL.AppendLine("          Select DocNo, SeqNo, No, ItCode ");
            SQL.AppendLine("          From TblSOContractDtl5 ");
            SQL.AppendLine("    ) K On A.DocNo=K.DocNo And B.No = K.No And B.ItCode = K.ItCode ");
            SQL.AppendLine("   Where A.DocNo=@DocNo  ");
            SQL.AppendLine(") T Group By T.DNo ORDER BY Right(concat('0000000000', T.SeqNo), 10); ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "ItCode", "ItName", "PackagingUnitUomCode",  "QtyPackagingUnit", "Qty", 
                    //6-10
                    "UPrice", "Discount", "DiscountAmt", "UPriceAfterDiscount", "UPriceBefore",
                    //11-15
                    "TaxRate" , "TaxAmt", "UPriceAfterTax", "Total", "DeliveryDt",
                    //16-20
                    "Remark", "Specification", "CtItCode", "CtItName", "Volume",
                    //21-23
                    "TotalVolume", "VolUom", "Notes", "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 24);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            Sm.FocusGrd(Grd3, 0, 1);

        }

        private void ShowSOContractDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T.No, T.SeqNo, CAST(T.seqno AS UNSIGNED) AS SeqNo, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("C.PurchaseUomCode, A.MinimalServiceAmt, A.MaterialAmt, A.Amt, A.AfterSalesAmt, ");
            SQL.AppendLine("A.TotalPPHAmt, A.SpaceNegoAmt, A.SPHAmt, A.TotalAmt, T.Remark, T.Amt As ContractAmt, ");
            SQL.AppendLine("T.Qty ContractQty, T.UPrice ContractUPrice, ");
            SQL.AppendLine("IfNull(E.UPrice, 0.00) RevisionUPrice, IfNull(E.Qty, 0.00) As RevisionQty, IfNull(E.Amt, 0.00) As RevisionAmt, C2.ItName ItNameDismantle, A.ItCodeDismantle ");
            SQL.AppendLine("From TblSOContractDtl4 T ");
            SQL.AppendLine("Inner Join TblBOQDtl2 A On T.BOQDocNo = A.DocNo ");
            SQL.AppendLine("    And T.DocNo = @DocNo And T.ItCode = A.ItCode And T.SeqNo = A.SeqNo ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblItem C2 On A.ItCodeDismantle = C2.ItCode  ");
            SQL.AppendLine("Left Join (Select Max(DocNo) DocNo From TblSOContractRevisionHdr Where SOCDocNo = @DocNo) D On 0 = 0 ");
            SQL.AppendLine("Left Join TblSOContractRevisionDtl4 E On D.DocNo = E.DocNo And T.DNo = E.DNo And T.ItCode = E.ItCode ");
            SQL.AppendLine("Group By T.DocNo, T.DNo ORDER BY Right(concat('0000000000', T.SeqNo), 10); ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                {   
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "ItCodeInternal", "Qty", "PurchaseUomCode", "MinimalServiceAmt",

                    //6-10
                    "MaterialAmt", "Amt", "AfterSalesAmt", "TotalPPHAmt", "SpaceNegoAmt",

                    //11-15
                    "SPHAmt", "TotalAmt", "Remark","ContractAmt", "ContractQty",

                    //16-20
                    "ContractUPrice", "RevisionQty", "RevisionUPrice", "RevisionAmt", "No",

                    //21-23
                    "SeqNo", "ItCodeDismantle", "ItNameDismantle"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 23);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowSOContractDtl5(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T.No, T.SeqNo, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("C.PurchaseUomCode, A.CostOfGoodsAmt, A.AfterSalesAmt, A.COMAmt, A.OHAmt, A.MarginAmt, ");
            SQL.AppendLine("A.DesignCostAmt, A.UPricePPHAmt, A.SpaceNegoAmt, A.UPriceSPHAmt, A.TotalPriceSPHAmt, T.Remark, T.Amt As ContractAmt, ");
            SQL.AppendLine("T.Qty ContractQty, T.UPrice ContractUPrice, E.UPrice RevisionUPrice, ");
            SQL.AppendLine("IfNull(E.Qty, 0.00) As RevisionQty, IfNull(E.Amt, 0.00) As RevisionAmt ");
            SQL.AppendLine("From TblSOContractDtl5 T ");
            SQL.AppendLine("Inner Join TblBOQDtl3 A On T.BOQDocNo = A.DocNo  ");
            SQL.AppendLine("    And T.DocNo = @DocNo And T.ItCode = A.ItCode And T.SeqNo = A.SeqNo  ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join (Select Max(DocNo) DocNo From TblSOContractRevisionHdr Where SOCDocNo = @DocNo) D On 0 = 0 ");
            SQL.AppendLine("Left Join TblSOContractRevisionDtl5 E On D.DocNo = E.DocNo And T.DNo = E.DNo And T.ItCode = E.ItCode ");
            SQL.AppendLine("Group By T.DocNo, T.DNo ORDER BY Right(concat('0000000000', T.SeqNo), 10);");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[]
                {   
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "ItCodeInternal", "Qty", "PurchaseUomCode", "CostOfGoodsAmt",

                    //6-10
                    "AfterSalesAmt", "COMAmt", "OHAmt","MarginAmt", "DesignCostAmt", 

                    //11-15
                    "UPricePPHAmt", "SpaceNegoAmt", "UPriceSPHAmt", "TotalPriceSPHAmt", "Remark",

                    //16-20
                    "ContractAmt", "ContractQty", "ContractUPrice", "RevisionQty", "RevisionUPrice", 

                    //21-23
                    "RevisionAmt", "No", "SeqNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowSOContractDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.JOCode, B.JOName, A.JODocDt, A.JODocNo ");
            SQL.AppendLine("FROM TblSOContractDtl3 A ");
            SQL.AppendLine("INNER JOIN TblJointOperationHdr B ON A.JOCode = B.JOCode ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo ");
            SQL.AppendLine("ORDER BY A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd5, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "JOCode", 

                    //1-3
                    "JOName", "JODocDt", "JODocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd5, 0, 0);
        }

        private void ShowARDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, ");
            SQL.AppendLine("A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblARDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.SODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CurCode", "Amt", "PIC", "VoucherRequestDocNo",  
                    
                    //6-7
                    "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ShowBOQService(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.No, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, A.ItCodeDismantle, D.ItName ItNameDisMantle, ");
            SQL.AppendLine("C.PurchaseUomCode, A.MinimalServiceAmt, A.MaterialAmt, A.Amt, A.AfterSalesAmt, ");
            SQL.AppendLine("A.TotalPPHAmt, A.SpaceNegoAmt, A.SPHAmt, A.TotalAmt, A.Remark, A.SeqNo ");
            SQL.AppendLine("From TblBOQDtl2 A ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblItem D On A.ItCodeDismantle = D.ItCode ");
            SQL.AppendLine("    And A.DocNo = @DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[]
                {    //0
                     "ItCode", 
                     
                     //1-5
                     "ItName", "ItCodeInternal", "Qty","PurchaseUomCode", "MinimalServiceAmt", 
        
                    //6-10
                     "MaterialAmt", "Amt", "AfterSalesAmt", "TotalPPHAmt", "SpaceNegoAmt", 
        
                    //11-15
                    "SPHAmt", "TotalAmt", "Remark", "No", "ItCodeDismantle",

                    //16-17
                    "ItNameDismantle", "SeqNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        internal void ShowBOQInventory(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.No, C.ItCode, C.ItName, C.ItCodeInternal, A.Qty, ");
            SQL.AppendLine("C.PurchaseUomCode, A.CostOfGoodsAmt, A.AfterSalesAmt, A.COMAmt, A.OHAmt, A.MarginAmt, ");
            SQL.AppendLine("A.DesignCostAmt, A.UPricePPHAmt, A.SpaceNegoAmt, A.UPriceSPHAmt, A.TotalPriceSPHAmt, A.Remark, A.SeqNo ");
            SQL.AppendLine("From TblBOQDtl3 A ");
            SQL.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode ");
            SQL.AppendLine("    And A.DocNo = @DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd6, ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItName", "ItCodeInternal", "Qty", "PurchaseUomCode", "CostOfGoodsAmt",
        
                    //6-10
                     "AfterSalesAmt", "COMAmt", "OHAmt","MarginAmt", "DesignCostAmt",  
        
                    //11-15
                    "UPricePPHAmt", "SpaceNegoAmt", "UPriceSPHAmt", "TotalPriceSPHAmt", "Remark", 

                    //16-17
                    "No", "SeqNo"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 17);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd6, Grd6.Rows.Count - 1, new int[] { 6, 8, 9, 10, 11, 12, 13, 14, 15, 17, 19, 20, 21 });
            Sm.FocusGrd(Grd6, 0, 0);
        }

        #endregion

        #region Additional Method

        private void CopyDeliveryDt(ref List<ItDeliveryDt> l)
        {
            CopyDeliveryDt11(ref l);
            if (l.Count > 0)
            {
                CopyDeliveryDt12(ref l); // Copy ke grid
                Sm.StdMsg(mMsgType.Info, "Copying data is completed.");
            }
            else
                Sm.StdMsg(mMsgType.Info, "No data.");
        }

        private void CopyDeliveryDt11(ref List<ItDeliveryDt> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, ItCode = string.Empty, No = string.Empty, DeliveryDt = string.Empty;

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd3, r, 4);
                if (ItCode.Length != 0)
                {
                    No = Sm.GetGrdStr(Grd3, r, 31);
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(ItCode=@ItCode_" + r.ToString() + " And No=@No_" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                    Sm.CmParam<String>(ref cm, "@No_" + r.ToString(), No);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select ItCode, No, DeliveryDt ");
            SQL.AppendLine("From TblSOContractDtl ");
            SQL.AppendLine("Where DocNo=@DocNo And DeliveryDt Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "No", "DeliveryDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ItDeliveryDt()
                        {
                            ItCode = Sm.DrStr(dr, c[0]),
                            No = Sm.DrStr(dr, c[1]),
                            DeliveryDt = Sm.DrStr(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void CopyDeliveryDt12(ref List<ItDeliveryDt> l)
        {
            string ItCode = string.Empty, No = string.Empty, DeliveryDt = string.Empty;
            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd3, r, 4);
                if (ItCode.Length > 0)
                {
                    No = Sm.GetGrdStr(Grd3, r, 31);
                    Grd3.Cells[r, 21].Value = null;
                    foreach (var x in l.Where(w => Sm.CompareStr(w.ItCode, ItCode) && Sm.CompareStr(w.No, No)))
                    {
                        Grd3.Cells[r, 21].Value = Sm.ConvertDate(x.DeliveryDt);
                        break;
                    }
                }
            }
        }

        private void CopyBOQ(ref List<ListOfItem> l)
        {
            CopyBOQ1(ref l); // Service
            CopyBOQ2(ref l); // Inventory
            if (l.Count > 0)
            {
                CopyBOQ3(ref l); // Informasi lainnya
                CopyBOQ4(ref l); // Copy ke grid
                Sm.StdMsg(mMsgType.Info, "Copying data is completed.");
            }
            else
                Sm.StdMsg(mMsgType.Info, "No data.");
        }

        private void CopyBOQ1(ref List<ListOfItem> l)
        {
            if (Grd2.Rows.Count == 0) return;

            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 3).Length > 0)
                    l.Add(new ListOfItem()
                    {
                        ItCode = Sm.GetGrdStr(Grd2, r, 3),
                        ItName = Sm.GetGrdStr(Grd2, r, 4),
                        PackagingUnitUomCode = string.Empty,
                        QtyPackagingUnit = Sm.GetGrdDec(Grd2, r, 6),
                        Qty = Sm.GetGrdDec(Grd2, r, 6),
                        UPrice = Sm.GetGrdDec(Grd2, r, 19),
                        TaxRate = 0m,
                        TaxAmt = Sm.GetGrdDec(Grd2, r, 14) - Sm.GetGrdDec(Grd2, r, 10),
                        Remark = Sm.GetGrdStr(Grd2, r, 16),
                        No = Sm.GetGrdStr(Grd2, r, 23),
                        Specification = string.Empty,
                        CtItCode = string.Empty,
                        CtItName = string.Empty
                    });
            }
        }

        private void CopyBOQ2(ref List<ListOfItem> l)
        {
            if (Grd6.Rows.Count == 0) return;

            for (int r = 0; r < Grd6.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd6, r, 3).Length > 0)
                    l.Add(new ListOfItem()
                    {
                        ItCode = Sm.GetGrdStr(Grd6, r, 3),
                        ItName = Sm.GetGrdStr(Grd6, r, 4),
                        PackagingUnitUomCode = string.Empty,
                        QtyPackagingUnit = Sm.GetGrdDec(Grd6, r, 6),
                        Qty = Sm.GetGrdDec(Grd6, r, 6),
                        UPrice = Sm.GetGrdDec(Grd6, r, 21),
                        TaxRate = 0m,
                        TaxAmt = Sm.GetGrdDec(Grd6, r, 16) - Sm.GetGrdDec(Grd6, r, 8),
                        Remark = Sm.GetGrdStr(Grd6, r, 18),
                        No = Sm.GetGrdStr(Grd6, r, 25),
                        Specification = string.Empty,
                        CtItCode = string.Empty,
                        CtItName = string.Empty
                    });
            }
        }

        private void CopyBOQ3(ref List<ListOfItem> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, ItCode = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                ItCode = l[i].ItCode;
                if (ItCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.ItCode=@ItCode_" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode_" + i.ToString(), ItCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            var TaxCode = Sm.GetLue(LueTaxCode);
            if (TaxCode.Length > 0) Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);

            SQL.AppendLine("Select A.ItCode, A.Specification, B.UomCode, C.CtItCode, C.CtItName, ");
            if (TaxCode.Length > 0)
                SQL.AppendLine("D.TaxRate ");
            else
                SQL.AppendLine("0.00 As TaxRate ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemPackagingUnit B On A.ItCode=B.ItCode And A.SalesUomCode=B.UomCode ");
            SQL.AppendLine("Left Join TblCustomerItem C On A.ItCode=C.ItCode And C.CtCode=@CtCode ");
            if (TaxCode.Length > 0) SQL.AppendLine("Left Join TblTax D On D.TaxCode=@TaxCode ");

            ItCode = string.Empty;
            string
                Specification = string.Empty,
                PackagingUnitUomCode = string.Empty,
                CtItCode = string.Empty,
                CtItName = string.Empty;
            decimal TaxRate = 0m;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {
                    "ItCode",
                    "Specification", "UomCode", "CtItCode", "CtItName", "TaxRate"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, c[0]);
                        Specification = Sm.DrStr(dr, c[1]);
                        PackagingUnitUomCode = Sm.DrStr(dr, c[2]);
                        CtItCode = Sm.DrStr(dr, c[3]);
                        CtItName = Sm.DrStr(dr, c[4]);
                        TaxRate = Sm.DrDec(dr, c[5]);
                        foreach (var x in l.Where(w => Sm.CompareStr(w.ItCode, ItCode)))
                        {
                            x.Specification = Specification;
                            x.PackagingUnitUomCode = PackagingUnitUomCode;
                            x.CtItCode = CtItCode;
                            x.CtItName = CtItName;
                            x.TaxRate = TaxRate;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void CopyBOQ4(ref List<ListOfItem> l)
        {
            decimal TaxAmt = 0m;
            Grd3.BeginUpdate();
            Grd3.Rows.Count = 0;

            iGRow r;
            r = Grd3.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            for (var i = 0; i < l.Count; i++)
            {
                r.Cells[4].Value = l[i].ItCode;
                r.Cells[6].Value = l[i].ItName;
                r.Cells[7].Value = l[i].PackagingUnitUomCode;
                r.Cells[8].Value = l[i].QtyPackagingUnit;
                r.Cells[9].Value = l[i].Qty;
                r.Cells[11].Value = l[i].UPrice;
                r.Cells[16].Value = l[i].UPrice;
                r.Cells[17].Value = l[i].TaxRate;
                TaxAmt = l[i].TaxRate * 0.01m * l[i].UPrice;
                r.Cells[18].Value = TaxAmt;
                r.Cells[19].Value = l[i].UPrice + TaxAmt;
                r.Cells[20].Value = l[i].Qty * (l[i].UPrice + TaxAmt);
                r.Cells[22].Value = l[i].Remark;
                r.Cells[24].Value = l[i].Specification;
                r.Cells[25].Value = l[i].CtItCode;
                r.Cells[26].Value = l[i].CtItName;
                r.Cells[31].Value = l[i].No;
                r = Grd3.Rows.Add();
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 27, 28 });
            }
            Grd3.EndUpdate();
        }

        private string GenerateDocNo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("IfNull(( ");
            SQL.AppendLine("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            SQL.AppendLine("       Select Convert(Right(DocNo, 4), Decimal) As DocNo From TblSOContractRevisionHdr ");
            SQL.AppendLine("       Where SOCDocNo = @Param ");
            SQL.AppendLine("       Order By Right(DocNo, 4) Desc Limit 1 ");
            SQL.AppendLine("       ) As Temp ");
            SQL.AppendLine("   ), '0001') As DocNo; ");

            return Sm.GetValue(SQL.ToString(), TxtDocNo.Text);
        }

        private void UpdatePriceList(string ItCode, decimal UPrice, string No)
        {
            //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd3, Row, 4) == ItCode)
            //    {
            //        Grd3.Rows[Row].Cells[11].Value = UPrice;
            //        Grd3.Rows[Row].Cells[16].Value = UPrice;
            //    }               
            //}

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4) == ItCode && Sm.GetGrdStr(Grd3, Row, 31) == No)
                {
                    Grd3.Rows[Row].Cells[11].Value = UPrice;
                    Grd3.Rows[Row].Cells[16].Value = UPrice;
                }
            }
        }
        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<SOContract>();
            var ldtl = new List<SOContractDtl>();
            var ldtl2 = new List<SOContractDtl2>();
            var lDtlS = new List<SOCSign>();
            var lDtlS2 = new List<SOCSign2>();
            var lST = new List<SOContractSubTotal>();

            string[] TableName = { "SOContract", "SOContractDtl", "SOCOntractDtl2", "SOCSign", "SOCSign2", "SOContractSubTotal" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();
            //  DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region SO Contract Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt, '%d %M %Y') DocDt, A.CtContactPersonName, ");
            SQL.AppendLine("A.SAName, A.Remark, A.PONo, IfNull(D.ProjectCode, A.ProjectCode) ProjectCode, IfNull(D.ProjectName, C.ProjectName) ProjectName, ");
            SQL.AppendLine("E.CtName, E.Address, F.Memo As NTPMemo ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.BOQDocNo = B.DocNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblLOphdr C On B.LOPDocNo = C.DocNO ");
            SQL.AppendLine("Left JOIN TblProjectGroup D ON C.PGCode = D.PGCode ");
            SQL.AppendLine("Inner Join TblCustomer E On C.CtCode = E.CtCode ");
            SQL.AppendLine("Left Join TblNoticeToProceed F On A.NTPDocNo = F.DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyName",

                    //1-5
                    "CompanyLogo",
                    "DocNo",
                    "DocDt",
                    "PONo",
                    "CtContactPersonName",

                    //6-10
                    "SAName",
                    "ProjectCode",
                    "ProjectName",
                    "Remark",
                    "Address",

                    //11
                    "NTPMemo"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SOContract()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),

                            CompanyLogo = Sm.DrStr(dr, c[1]),
                            DocNo = Sm.DrStr(dr, c[2]),
                            DocDt = Sm.DrStr(dr, c[3]),
                            PONo = Sm.DrStr(dr, c[4]),
                            ContactPerson = Sm.DrStr(dr, c[5]),

                            ShippingName = LueCtCode.Text + Environment.NewLine + Sm.DrStr(dr, c[10]),
                            ProjectCode = Sm.DrStr(dr, c[7]),
                            ProjectName = Sm.DrStr(dr, c[8]),
                            Remark = Sm.DrStr(dr, c[9]),
                            CityName = TxtCity.Text,

                            CntName = TxtCountry.Text,
                            PostalCd = TxtPostalCd.Text,
                            NTPDocNo = TxtNTPDocNo.Text,
                            CtName = LueCtCode.Text,
                            NTPMemo = Sm.DrStr(dr, c[11]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            #endregion

            #region SO Contract Detail
            int nomor = 0;
            if (Grd6.Rows.Count > 1)
            {
                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select Distinct T.No, CAST(T.seqno AS UNSIGNED) AS SeqNo, C.ItCode, C.ItName, C.ItCodeInternal, X2.Qty, ");
                    SQLDtl.AppendLine("C.PurchaseUomCode, A.CostOfGoodsAmt, A.AfterSalesAmt, A.COMAmt, A.OHAmt, A.MarginAmt, ");
                    SQLDtl.AppendLine("A.DesignCostAmt, A.UPricePPHAmt, A.SpaceNegoAmt, A.UPriceSPHAmt, A.TotalPriceSPHAmt, T.Remark, X2.Amt As ContractAmt, ");
                    SQLDtl.AppendLine("X2.Qty ContractQty, X2.UPrice ContractUPrice, ");
                    SQLDtl.AppendLine("case  ");
                    SQLDtl.AppendLine("when RIGHT(D.DocNo, 4)='0001' then 0 ");
                    SQLDtl.AppendLine("ELSE  ifnull(E.UPrice, 0)  ");
                    SQLDtl.AppendLine("END AS RevisionUPrice,  ");

                    SQLDtl.AppendLine("case  ");
                    SQLDtl.AppendLine("when RIGHT(D.DocNo, 4)='0001' then 0 ");
                    SQLDtl.AppendLine("else IfNull(E.Qty, 0.00)  ");
                    SQLDtl.AppendLine("end As RevisionQty,  ");

                    SQLDtl.AppendLine("case   ");
                    SQLDtl.AppendLine("when RIGHT(D.DocNo, 4)='0001' then 0  ");
                    SQLDtl.AppendLine("else IfNull(E.Amt, 0.00)  ");
                    SQLDtl.AppendLine("end As RevisionAmt,   ");
                    SQLDtl.AppendLine("date_format(G.DeliveryDt, '%d/%b/%Y') DeliveryDt ");
                    SQLDtl.AppendLine("From TblSOContractDtl5 T ");
                    SQLDtl.AppendLine("Inner Join TblBOQDtl3 A On T.BOQDocNo = A.DocNo ");
                    SQLDtl.AppendLine("    And T.DocNo = @DocNo And T.ItCode = A.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblItem C On A.ItCode = C.ItCode  ");
                    SQLDtl.AppendLine("Left Join (Select Max(DocNo) DocNo From TblSOContractRevisionHdr Where SOCDocNo = @DocNo) D On 0 = 0  ");
                    SQLDtl.AppendLine("Left Join TblSOContractRevisionDtl E On D.DocNo = E.DocNo And T.DNo = E.DNo And T.ItCode = E.ItCode ");
                    SQLDtl.AppendLine("INNER JOIN tblsocontracthdr F ON T.DocNo=F.DocNo ");
                    SQLDtl.AppendLine("INNER JOIN tblsocontractdtl G ON F.DocNo=G.DocNo AND T.Dno=G.Dno ");
                    SQLDtl.AppendLine("Left Join (Select MIN(DocNo) DocNo From TblSOContractRevisionHdr Where SOCDocNo = @DocNo) X1 On 0 = 0 ");
                    SQLDtl.AppendLine("Left Join tblsocontractrevisiondtl X2 ON X1.DocNo = X2.DocNo And T.DNo = X2.DNo And T.ItCode = X2.ItCode ");
                    SQLDtl.AppendLine("Group By T.DocNo, T.DNo ORDER BY SeqNo; ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                         //0
                         "No" ,

                         //1-5
                         "ItCodeInternal" ,
                         "ItCode",
                         "ItName",
                         "Remark",
                         "Qty",

                         //6-10
                         "UPriceSPHAmt",
                         "ContractAmt",
                         "TotalPriceSPHAmt",
                         "ContractQty",
                         "ContractUPrice",

                         //11-15
                         "RevisionUPrice",
                         "RevisionAmt",
                         "PurchaseUomCode",
                         "RevisionQty",
                         "DeliveryDt"

                        });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            nomor = nomor + 1;
                            ldtl.Add(new SOContractDtl()
                            {
                                No = nomor,
                                ItCodeInternal = Sm.DrStr(drDtl, cDtl[1]),
                                ItCode = Sm.DrStr(drDtl, cDtl[2]),
                                ItName = Sm.DrStr(drDtl, cDtl[3]),
                                Remark = Sm.DrStr(drDtl, cDtl[4]),
                                Qty = Sm.DrDec(drDtl, cDtl[5]),

                                PurchaseUom = Sm.DrStr(drDtl, cDtl[13]),
                                DeliveryDt = Sm.DrStr(drDtl, cDtl[15]),
                                UnitPrice = Sm.DrDec(drDtl, cDtl[10]),
                                TotalPrice = Sm.DrDec(drDtl, cDtl[7]),
                                tipe = "Jasa",
                                RevisionQty = Sm.DrDec(drDtl, cDtl[14]),
                                RevisionUnitPrice = Sm.DrDec(drDtl, cDtl[11]),
                                RevisionTotalPrice = Sm.DrDec(drDtl, cDtl[12]),
                                NoItem = Sm.DrStr(drDtl, cDtl[0]),
                                TotalPriceContract = Sm.DrDec(drDtl, cDtl[5]) * Sm.DrDec(drDtl, cDtl[10]),
                            });
                        }
                    }
                    drDtl.Close();
                }
            }
            else
            {
                for (int i = 0; i < Grd2.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd2, i, 3).Length > 0)
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new SOContractDtl()
                        {
                            No = nomor,
                            ItCodeInternal = Sm.GetGrdStr(Grd2, i, 5),
                            ItCode = Sm.GetGrdStr(Grd2, i, 3),
                            ItName = Sm.GetGrdStr(Grd2, i, 4),
                            Remark = Sm.GetGrdStr(Grd2, i, 16),
                            Qty = Sm.GetGrdDec(Grd2, i, 18),
                            PurchaseUom = Sm.GetGrdStr(Grd2, i, 7),
                            DeliveryDt = string.Empty,
                            UnitPrice = Sm.GetGrdDec(Grd2, i, 19),
                            TotalPrice = Sm.GetGrdDec(Grd2, i, 17),
                            tipe = "Jasa",
                            RevisionQty = Sm.GetGrdDec(Grd2, i, 20),
                            RevisionUnitPrice = Sm.GetGrdDec(Grd2, i, 21),
                            RevisionTotalPrice = Sm.GetGrdDec(Grd2, i, 22),
                            NoItem = Sm.GetGrdStr(Grd2, i, 23),
                            SeqNo = Sm.GetGrdStr(Grd2, i, 24),
                            ItNameDismantle = Sm.GetGrdStr(Grd2, i, 27),
                            TotalPriceContract = Sm.GetGrdDec(Grd2, i, 18) * Sm.GetGrdDec(Grd2, i, 19),
                        });
                    }
                }
            }
            //for (int i = 0; i < Grd6.Rows.Count; i++)
            //{
            //    if (Sm.GetGrdStr(Grd6, i, 3).Length > 0)
            //    {
            //        nomor = nomor + 1;
            //        ldtl.Add(new SOContractDtl()
            //        {
            //            No = nomor,
            //            ItCodeInternal = Sm.GetGrdStr(Grd6, i, 5),
            //            ItCode = Sm.GetGrdStr(Grd6, i, 3),
            //            ItName = Sm.GetGrdStr(Grd6, i, 4),
            //            Remark = Sm.GetGrdStr(Grd6, i, 18),
            //            Qty = Sm.GetGrdDec(Grd6, i, 20),
            //            PurchaseUom = Sm.GetGrdStr(Grd6, i, 7),
            //            DeliveryDt = string.Empty,
            //            UnitPrice = Sm.GetGrdDec(Grd6, i, 21),
            //            TotalPrice = Sm.GetGrdDec(Grd6, i, 19),
            //            tipe = "Barang",
            //            RevisionQty = Sm.GetGrdDec(Grd6, i, 22),
            //            RevisionUnitPrice = Sm.GetGrdDec(Grd6, i, 23),
            //            RevisionTotalPrice = Sm.GetGrdDec(Grd6, i, 24),
            //            NoItem = Sm.GetGrdStr(Grd6, i, 25),
            //            SeqNo = Sm.GetGrdStr(Grd6, i, 26),

            //        }); 
            //    } 

            //}

            #endregion

            #region SO Contract Detail2

            for (int i = 0; i < Grd3.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd3, i, 4).Length > 0)
                {
                    ldtl2.Add(new SOContractDtl2()
                    {
                        ItCode = Sm.GetGrdStr(Grd3, i, 4),
                        DeliveryDt = Sm.GetGrdText(Grd3, i, 21),
                    });
                }
            }

            if (ldtl2.Count() > 0)
            {
                for (int i = 0; i < ldtl.Count(); i++)
                {
                    for (int j = 0; j < ldtl2.Count(); j++)
                    {
                        if (ldtl[i].ItCode == ldtl2[j].ItCode && ldtl2[j].DeliveryDt.Length > 0)
                        {
                            if (ldtl[i].DeliveryDt.Length > 0) ldtl[i].DeliveryDt += ", ";
                            ldtl[i].DeliveryDt += ldtl2[j].DeliveryDt;
                        }
                    }
                }
            }

            #endregion

            #region Detail Signature IMS

            //Dibuat Oleh
            var cmDtlS = new MySqlCommand();

            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                SQLDtlS.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                SQLDtlS.AppendLine("T1.UserCode, T1.UserName, T5.DeptName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                SQLDtlS.AppendLine("From ( ");
                SQLDtlS.AppendLine("    Select Distinct ");
                SQLDtlS.AppendLine("    A.CreateBy As UserCode, B.UserName, '1' As DNo, 0 As Level, 'Prepared By,' As Title, Left(A.CreateDt, 8) As LastUpDt  ");
                SQLDtlS.AppendLine("    From TblSOCOntractHdr A ");
                SQLDtlS.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                SQLDtlS.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                SQLDtlS.AppendLine(") T1 ");
                SQLDtlS.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                SQLDtlS.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                SQLDtlS.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                SQLDtlS.AppendLine("Left Join tbldepartment T5 On T2.DeptCode=T5.DeptCode ");
                SQLDtlS.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T5.DeptName, T3.PosName, T1.DNo, T1.Title ");
                SQLDtlS.AppendLine("Order By T1.Level; ");

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[]
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6-7
                         "LastupDt",
                         "DeptName"

                        });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new SOCSign()
                        {
                            Signature = Sm.DrStr(drDtlS, cDtlS[0]),
                            UserName = Sm.DrStr(drDtlS, cDtlS[1]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[2]),
                            Space = Sm.DrStr(drDtlS, cDtlS[3]),
                            DNo = Sm.DrStr(drDtlS, cDtlS[4]),
                            Title = Sm.DrStr(drDtlS, cDtlS[5]),
                            LastUpDt = Sm.DrStr(drDtlS, cDtlS[6]),
                            DeptName = Sm.DrStr(drDtlS, cDtlS[7])
                        });
                    }
                }
                drDtlS.Close();
            }


            //Disetujui Oleh & Di periksa oleh
            var cmDtlS2 = new MySqlCommand();

            var SQLDtlS2 = new StringBuilder();
            using (var cnDtlS2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS2.Open();
                cmDtlS2.Connection = cnDtlS2;

                SQLDtlS2.AppendLine("Select Distinct Concat(IfNull(F.ParValue, ''), A.UserCode, '.JPG') As Signature, A.EmpName, B.PosName, D.UserName, E.DeptName, C.EmpName2, C.PosName2, C.UserName2, C.DeptName2, C.Signature2 ");
                //SQLDtlS2.AppendLine(", Distinct Concat(IfNull(F.ParValue, ''), A.UserCode, '.JPG') As Signature, C.Signature2 ");
                SQLDtlS2.AppendLine("From TblEmployee A ");
                SQLDtlS2.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                SQLDtlS2.AppendLine("Left Join ");
                SQLDtlS2.AppendLine("( ");
                SQLDtlS2.AppendLine("    Select Distinct Concat(IfNull(E.ParValue, ''), A.UserCode, '.JPG') As Signature2, C.UserName UserName2, A.EmpName As EmpName2, B.PosName As PosName2, D.DeptName DeptName2 ");
                //SQLDtlS2.AppendLine("    , Distinct Concat(IfNull(E.ParValue, ''), A.UserCode, '.JPG') As Signature2 ");
                SQLDtlS2.AppendLine("    From TblEmployee A ");
                SQLDtlS2.AppendLine("    Left Join TblPosition B On A.PosCode = B.PosCode ");
                SQLDtlS2.AppendLine("    LEFT JOIN tbluser C  ON  A.usercode= C.UserCode ");
                SQLDtlS2.AppendLine("    LEFT JOIN tbldepartment D ON A.deptcode =D.DeptCode ");
                SQLDtlS2.AppendLine("    INNER JOIN tblparameter E on E.parcode = 'ImgFileSignature' ");
                SQLDtlS2.AppendLine("    Where A.EmpCode = @EmpCode2 ");
                SQLDtlS2.AppendLine(") C On 0 = 0 ");
                SQLDtlS2.AppendLine("LEFT JOIN tbluser D  ON  A.usercode= D.UserCode ");
                SQLDtlS2.AppendLine("LEFT JOIN tbldepartment E ON A.deptcode =E.DeptCode ");
                SQLDtlS2.AppendLine("INNER JOIN tblparameter F on F.parcode = 'ImgFileSignature' ");
                SQLDtlS2.AppendLine("Where A.EmpCode = @EmpCode ");

                cmDtlS2.CommandText = SQLDtlS2.ToString();
                Sm.CmParam<String>(ref cmDtlS2, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS2, "@EmpCode", mSOContractAutoApprovedByEmpCode);
                Sm.CmParam<String>(ref cmDtlS2, "@EmpCode2", mSOContractAutoCheckedByEmpCode);
                var drDtlS2 = cmDtlS2.ExecuteReader();
                var cDtlS2 = Sm.GetOrdinal(drDtlS2, new string[] { 
                    
                    //0
                    "Signature",  
                    
                    //1-5
                    "EmpName", "PosName", "EmpName2", "PosName2", "UserName", 

                    //6-9
                    "UserName2", "DeptName", "DeptName2", "Signature2",

                });
                if (drDtlS2.HasRows)
                {
                    while (drDtlS2.Read())
                    {

                        lDtlS2.Add(new SOCSign2()
                        {
                            Signature = Sm.DrStr(drDtlS2, cDtlS2[0]),

                            EmpName = Sm.DrStr(drDtlS2, cDtlS2[1]),
                            PosName = Sm.DrStr(drDtlS2, cDtlS2[2]),
                            EmpName2 = Sm.DrStr(drDtlS2, cDtlS2[3]),
                            PosName2 = Sm.DrStr(drDtlS2, cDtlS2[4]),
                            UserName = Sm.DrStr(drDtlS2, cDtlS2[5]),

                            UserName2 = Sm.DrStr(drDtlS2, cDtlS2[6]),
                            DeptName = Sm.DrStr(drDtlS2, cDtlS2[7]),
                            DeptName2 = Sm.DrStr(drDtlS2, cDtlS2[8]),
                            Signature2 = Sm.DrStr(drDtlS2, cDtlS2[9]),

                        });
                    }
                }
                drDtlS2.Close();
            }


            #endregion

            #region Subtotal

            if (ldtl.Count > 0)
            {
                decimal mSubtotal = 0m, mPPN = 0m, mTotal = 0m;
                foreach (var x in ldtl)
                {
                    if (x.RevisionUnitPrice > 0)
                        mSubtotal += x.RevisionQty * x.RevisionUnitPrice;
                    else
                        mSubtotal += x.Qty * x.UnitPrice;
                    //else
                    //mSubtotal += x.TotalPriceContract;
                }

                if (mSubtotal != 0m)
                {
                    mPPN = mSubtotal * 0.1m;
                    mTotal = mSubtotal + mPPN;
                }

                lST.Add(new SOContractSubTotal()
                {
                    SubTotal = mSubtotal,
                    PPN = mPPN,
                    Total = mTotal
                });
            }

            #endregion

            myLists.Add(l);
            myLists.Add(ldtl);
            myLists.Add(ldtl2);
            myLists.Add(lDtlS);
            myLists.Add(lDtlS2);
            myLists.Add(lST);

            if (Grd6.Rows.Count > 1)
                Sm.PrintReport("SOContractIMS_1", myLists, TableName, false);
            else if (Grd2.Rows.Count > 1)
                Sm.PrintReport("SOContractIMS_2", myLists, TableName, false);

        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }


        private void UploadFile(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSOContractFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSOContractFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            //if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSOContractFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFile2SizeNotvalid() ||
                IsFile3SizeNotvalid() ||
                IsFileNameAlreadyExisted() ||
                IsFileName2AlreadyExisted() ||
                IsFileName3AlreadyExisted()
             ;
        }

        private bool IsFTPClientDataNotValid()
        {

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if ((TxtFile.Text.Length > 0 || TxtFile2.Text.Length > 0 || TxtFile3.Text.Length > 0) && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File 1 too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFile2SizeNotvalid()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File 2 too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFile3SizeNotvalid()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File 3 too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileName2AlreadyExisted()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileName3AlreadyExisted()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        internal string GetSelectedBOMData()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                        SQL += "##" + TxtBOQDocNo.Text + Sm.GetGrdStr(Grd2, Row, 1) + Sm.GetGrdStr(Grd2, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedBOQData()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 3).Length != 0)
                        SQL += "##" + TxtBOQDocNo.Text + Sm.GetGrdStr(Grd2, Row, 3) + Sm.GetGrdStr(Grd2, Row, 24) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedBOMData2()
        {
            var SQL = string.Empty;
            if (Grd6.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 1).Length != 0)
                        SQL += "##" + TxtBOQDocNo.Text + Sm.GetGrdStr(Grd6, Row, 1) + Sm.GetGrdStr(Grd6, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedBOQData2()
        {
            var SQL = string.Empty;
            if (Grd6.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd6, Row, 3).Length != 0)
                        SQL += "##" + TxtBOQDocNo.Text + Sm.GetGrdStr(Grd6, Row, 3) + Sm.GetGrdStr(Grd6, Row, 26) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private bool PONoExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblSOContractHdr ");
            SQL.AppendLine("Where PONo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), TxtPONo.Text);
        }

        private bool IsRevisionNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType = 'SOCRev' Limit 1; ");
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueJOCode(ref LookUpEdit Lue, string JOCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select JOCode As Col1, JOName As Col2 ");
            SQL.AppendLine("From TblJointOperationHdr ");
            SQL.AppendLine("Where 0 = 0 ");

            if (JOCode.Length > 0)
                SQL.AppendLine("And JOCode = @JOCode ");
            else
                SQL.AppendLine("And ActInd = 'Y' ");

            SQL.AppendLine("Order By JOName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@JOCode", JOCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void ComputeTotalBOQ(bool IsInsert)
        {
            decimal Amt = 0m;
            string BOQDocNo = TxtBOQDocNo.Text;
            //Amt = Decimal.Parse(Sm.GetValue("Select SUM(A.totalAmt) From TblBoqDtl A "+
            //      "Inner Join TblItemBOQ B On A.ItBOQCode = B.ItBOQCode And B.Parent Is null " +
            //      "Where A.DocNo = '"+BOQDocNo+"' ; "));

            for (int i = 0; i < Grd2.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd2, i, 3).Length > 0)
                {
                    if (IsInsert) Amt += Sm.GetGrdDec(Grd2, i, 17);
                    else Amt += Sm.GetGrdDec(Grd2, i, 22);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeTotalBOM(bool IsInsert)
        {
            decimal Amt = 0m;
            for (int i = 0; i < Grd6.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd6, i, 3).Length > 0)
                {
                    if (IsInsert) Amt += Sm.GetGrdDec(Grd6, i, 19);
                    else Amt += Sm.GetGrdDec(Grd6, i, 24);
                }
            }
            TxtAmtBOM.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeItem(bool IsInsert)
        {
            // old formula 1
            // Price (Before Tax) di list of item, ambil dari Contract Price nya boq
            // Total = Price (Before Tax) * quantity
            // old formula 2
            // Price (After Tax) di list of item, ambil dari Contract Price nya boq
            // Total = Price (After Tax) * quantity
            // new formula
            // Price (Price List) = Price Before Tax, ambil dari Contract Price nya boq
            // Tax Rate ambil dari header
            // Tax Amount = Tax Rate * Price List
            // Price (After Tax) = Price (Before Tax) + Tax Amount
            // Total = Price (After Tax) * Qty

            decimal mUprice = 0m;

            for (int i = 0; i < Grd3.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd3, i, 4).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd3, i, 4).Length > 0)
                    {
                        for (int j = 0; j < Grd2.Rows.Count; ++j)
                        {
                            if (Sm.GetGrdStr(Grd3, i, 4) == Sm.GetGrdStr(Grd2, j, 3) && Sm.GetGrdStr(Grd3, i, 31) == Sm.GetGrdStr(Grd2, j, 23))
                            {
                                if (IsInsert)
                                {
                                    Grd3.Cells[i, 11].Value = Sm.GetGrdDec(Grd2, j, 19);
                                    Grd3.Cells[i, 16].Value = Sm.GetGrdDec(Grd2, j, 19);
                                    mUprice = Sm.GetGrdDec(Grd2, j, 19);
                                }
                                else
                                {
                                    Grd3.Cells[i, 11].Value = Sm.GetGrdDec(Grd2, j, 21);
                                    Grd3.Cells[i, 16].Value = Sm.GetGrdDec(Grd2, j, 21);
                                    mUprice = Sm.GetGrdDec(Grd2, j, 21);
                                }
                            }
                        }

                        for (int k = 0; k < Grd6.Rows.Count; ++k)
                        {
                            if (Sm.GetGrdStr(Grd3, i, 4) == Sm.GetGrdStr(Grd6, k, 3) && Sm.GetGrdStr(Grd3, i, 31) == Sm.GetGrdStr(Grd6, k, 25))
                            {
                                if (IsInsert)
                                {
                                    Grd3.Cells[i, 11].Value = Sm.GetGrdDec(Grd6, k, 21);
                                    Grd3.Cells[i, 16].Value = Sm.GetGrdDec(Grd6, k, 21);
                                    mUprice = Sm.GetGrdDec(Grd6, k, 21);
                                }
                                else
                                {
                                    Grd3.Cells[i, 11].Value = Sm.GetGrdDec(Grd6, k, 23);
                                    Grd3.Cells[i, 16].Value = Sm.GetGrdDec(Grd6, k, 23);
                                    mUprice = Sm.GetGrdDec(Grd6, k, 23);
                                }
                            }
                        }
                    }

                    decimal mTaxRate = 0m;
                    if (Sm.GetLue(LueTaxCode).Length > 0)
                        mTaxRate = decimal.Parse(Sm.GetValue("Select TaxRate From TblTax where TaxCode = @Param", Sm.GetLue(LueTaxCode)));
                    decimal mTaxAmt = mTaxRate * 0.01m * mUprice;
                    decimal mQty = Sm.GetGrdDec(Grd3, i, 9);
                    decimal mPriceAfTax = mUprice + mTaxAmt;
                    decimal mTotal = mPriceAfTax * mQty;

                    Grd3.Cells[i, 17].Value = Sm.FormatNum(mTaxRate, 0);
                    Grd3.Cells[i, 18].Value = Sm.FormatNum(mTaxAmt, 0);
                    Grd3.Cells[i, 19].Value = Sm.FormatNum(mPriceAfTax, 0);
                    Grd3.Cells[i, 20].Value = mTotal;
                }
            }

            //decimal Qty = 0m, TotalAmt = 0m, PriceGrid3 = 0m;
            //Qty = Sm.GetGrdDec(Grd3, 0, 8);
            //TotalAmt = Decimal.Parse(TxtAmt.Text) + Decimal.Parse(TxtAmtBOM.Text);

            //if (TotalAmt > 0 && Qty>0)
            //{
            //    PriceGrid3 = TotalAmt / Qty;
            //}

            //Grd3.Cells[0, 11].Value = PriceGrid3;
            //Grd3.Cells[0, 14].Value = PriceGrid3;
            //Grd3.Cells[0, 19].Value = PriceGrid3;
            //Grd3.Cells[0, 20].Value = PriceGrid3;
        }

        private void ComputeItem1(int Row)
        {
            Grd3.Cells[Row, 8].Value = Sm.GetGrdDec(Grd3, Row, 9);
            Grd3.Cells[Row, 20].Value = Sm.GetGrdDec(Grd3, Row, 9) * Sm.GetGrdDec(Grd3, Row, 19);
        }

        private void SetNumberOfSalesUomCode()
        {
            string NumberOfSalesUomCode = Sm.GetParameter("NumberOfSalesUomCode");
            if (NumberOfSalesUomCode.Length == 0)
                mNumberOfSalesUomCode = 1;
            else
                mNumberOfSalesUomCode = int.Parse(NumberOfSalesUomCode);
        }

        private void GetParameter()
        {
            mIsSOContractBOMEnabled = Sm.GetParameterBoo("IsSOContractBOMEnabled");
            mIsSoUseDefaultPrintout = Sm.GetParameter("IsSoUseDefaultPrintout");
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
            mIsSOUseARDPValidated = Sm.GetParameterBoo("IsSOUseARDPValidated");
            mGenerateCustomerCOAFormat = Sm.GetParameter("GenerateCustomerCOAFormat");
            mJointOperationKSOType = Sm.GetParameter("JointOperationKSOType");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mProjectAcNoFormula = Sm.GetParameter("ProjectAcNoFormula");
            mIsSOContractItemMultiDeliveryDateEnabled = Sm.GetParameterBoo("IsSOContractItemMultiDeliveryDateEnabled");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mSOContractAutoApprovedByEmpCode = Sm.GetParameter("SOContractAutoApprovedByEmpCode");
            mSOContractAutoCheckedByEmpCode = Sm.GetParameter("SOContractAutoCheckedByEmpCode");
            mIsSOContract2AmtIsZero = Sm.GetParameterBoo("IsSOContract2AmtIsZero");
            mProjectCodeForSOContract2 = Sm.GetParameter("ProjectCodeForSOContract2");
            mIsSalesTransactionUseItemNotes = Sm.GetParameterBoo("IsSalesTransactionUseItemNotes");
            mIsSOContractDocDtEditable = Sm.GetParameterBoo("IsSOContractDocDtEditable");
        }

        private void SetBOQDocNo(string CtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblBOQHdr ");
            SQL.AppendLine("Where ActInd='Y' And Status='A' And CtCode=@CtCode ");
            SQL.AppendLine("Order By DocDt Desc, DocNo Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            TxtBOQDocNo.EditValue = Sm.GetValue(cm);

            string AllowEditSalesPerson = Sm.GetValue("Select IF( EXISTS(Select B.Spname from TblBOQhdr A " +
            "Inner Join TblSalesPerson B On A.SpCode=B.SpCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");

            string AllowEditShipmentMethod = Sm.GetValue("Select IF( EXISTS(Select B.Dtname from TblBOQhdr A  " +
            "Inner Join TblDeliveryType B On A.ShpmCode=B.DtCode " +
            "Where A.DocNo = '" + Sm.GetValue(cm) + "'), 1, 0)");


            if (AllowEditSalesPerson == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    LueSPCode
                }, false);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    LueSPCode
                }, true);
                SetLueSPCode(ref LueSPCode);
                Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }


            if (AllowEditShipmentMethod == "0")
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    LueShpMCode
                }, false);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
            else
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                {
                    LueShpMCode
                }, true);
                SetLueDTCode(ref LueShpMCode);
                Sm.SetLue(LueShpMCode, Sm.GetValue("Select ShpMCode From tblBOQHdr Where DocNo = '" + Sm.GetValue(cm) + "'"));
            }
        }

        private string GenerateProjectSequenceNo(string SiteCode, string ProjectScope)
        {
            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('000', Convert(ProjectSequenceNo+1, Char)), 3) From ( ");
            SQL.Append("       Select Convert(Left(A.ProjectSequenceNo, 3), Decimal) As ProjectSequenceNo  ");
            SQL.Append("       From TblSOContractHdr A ");
            SQL.Append("       Inner Join TblBOQHdr B On A.BOQDocNo= B.DocNo ");
            SQL.Append("       Inner Join TblLOPHdr C On B.LOPDocNo= C.DocNo ");
            SQL.Append("       Where C.SiteCode='" + SiteCode + "' And C.ProjectScope='" + ProjectScope + "' ");
            SQL.Append("       Order By Left(ProjectSequenceNo, 3) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), '001') ");
            SQL.Append(") As ProjectSequenceNo");

            return Sm.GetValue(SQL.ToString());

        }

        #endregion

        #region SetLue

        public static void SetLueDTCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select DTCode As Col1, DTName As Col2 From TblDeliveryType " +
                 "Union ALL Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union All Select 'ALL' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }



        public void SetLueCtPersonCode(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblCustomerContactPerson " +
                "Where CtCode= '" + CtCode + "' Order By ContactPersonName;",
                "Contact Person");
        }

        public static void SetLueCtPersonCodeShow(ref LookUpEdit Lue, string DocNo)
        {
            Sm.SetLue1(
                ref Lue,
                "Select CtContactPersonName As Col1 From TblSOContractHdr " +
                "Where DocNo= '" + DocNo + "' Order By CtContactPersonName;",
                "Contact Person");
        }

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocType As Col1, T.Status As Col2 From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'A' As Doctype, 'Approve' As Status ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select 'O' As Doctype, 'Outstanding' As Status ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select 'C' As Doctype, 'Cancelled' As Status ");
            SQL.AppendLine(")T ");


            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetBOQDocNo(CtCode);
                    TxtSAName.EditValue = null;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void LueJOCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueJOCode, new Sm.RefreshLue2(SetLueJOCode), string.Empty);
        }

        private void LueJOCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.LueKeyDown(Grd5, ref fAccept, e);
        }

        private void LueJOCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueJOCode.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueJOCode).Length == 0)
                        Grd5.Cells[fCell.RowIndex, 0].Value =
                        Grd5.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd5.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueJOCode);
                        Grd5.Cells[fCell.RowIndex, 1].Value = LueJOCode.GetColumnValue("Col2");
                    }
                    LueJOCode.Visible = false;
                }
            }
        }

        private void DteJODocDt_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.DteKeyDown(Grd5, ref fAccept, e);
        }

        private void DteJODocDt_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.DteLeave(DteJODocDt, ref fCell, ref fAccept);
        }

        private void TxtBankGuaranteeAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtBankGuaranteeAmt, 0);
        }

        private void TxtAmtBOM_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmtBOM, 0);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void LueJOType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueJOType, new Sm.RefreshLue2(Sl.SetLueOption), "JointOperationType");
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueSPCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void LueShpMCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueShpMCode, new Sm.RefreshLue1(SetLueDTCode));
        }

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));

                TxtBOQDocNo.EditValue = null;
                ClearData2();
                Sm.ClearGrd(Grd1, true);
                var CtCode = Sm.GetLue(LueCtCode);
                if (CtCode.Length != 0 && BtnSave.Enabled)
                {
                    SetLueSPCode(ref LueSPCode);
                    SetLueDTCode(ref LueShpMCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueStatus_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                Sm.ClearGrd(Grd3, true);
            }
        }

        private void DteDeliveryDt_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.DteKeyDown(Grd3, ref fAccept, e);
        }

        private void DteDeliveryDt_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.DteLeave(DteDeliveryDt, ref fCell, ref fAccept);
        }

        private void Grd3_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {

            if (e.ColIndex == 21)
            {
                if (Sm.GetGrdDate(Grd3, 0, 21).Length != 0)
                {
                    var Dt = Sm.ConvertDate(Sm.GetGrdDate(Grd3, 0, 21));
                    for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                        if (Sm.GetGrdStr(Grd3, r, 4).Length != 0) Grd3.Cells[r, 21].Value = Dt;
                }
            }
            else if (e.ColIndex == 20)
            {
                Decimal sum = 0;
                if (Grd3.Rows.Count > 1)
                {
                    for (int r = 0; r < Grd3.Rows.Count - 1; r++)
                    {
                        sum += Sm.GetGrdDec(Grd3, r, 20);
                    }

                    Sm.StdMsg(mMsgType.Info, "Total Amount : " + Sm.FormatNum(sum, 2));
                }
                else Sm.StdMsg(mMsgType.NoData, "No Data");
            }
        }

        #endregion

        #region Button Event

        private void BtnContact_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    //var f = new FrmCustomer("***");
                    //f.Tag = "***";
                    //f.WindowState = FormWindowState.Normal;
                    //f.StartPosition = FormStartPosition.CenterScreen;
                    //f.mCtCode = Sm.GetLue(LueCtCode);
                    //f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnSOMaster_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    {
                        if (TxtBOQDocNo.EditValue != null && TxtBOQDocNo.Text.Length != 0)
                        {
                            //var f = new FrmBOQ("***");
                            //f.Tag = "***";
                            //f.WindowState = FormWindowState.Normal;
                            //f.StartPosition = FormStartPosition.CenterScreen;
                            //f.mDocNo = TxtBOQDocNo.Text;
                            //f.ShowDialog();
                        }
                        else
                        {
                            //var f = new FrmBOQ("***");
                            //f.Tag = "***";
                            //f.WindowState = FormWindowState.Normal;
                            //f.StartPosition = FormStartPosition.CenterScreen;
                            //f.mCtCode = Sm.GetLue(LueCtCode);
                            //f.ShowDialog();
                            //TxtBOQDocNo.EditValue = Sm.GetValue("Select MAX(DocNo) From TblBOQhdr Where CtCode = '" + Sm.GetLue(LueCtCode) + "' ");
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    //var f = new FrmCustomer("***");
                    //f.Tag = "***";
                    //f.WindowState = FormWindowState.Normal;
                    //f.StartPosition = FormStartPosition.CenterScreen;
                    //f.mCtCode = Sm.GetLue(LueCtCode);
                    //f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnBOQDocNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                {
                    var f = new FrmSOContract2Dlg(this, Sm.GetLue(LueCtCode));
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.ShowDialog();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCustomerShipAddress_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSOContract2Dlg2(this, Sm.GetLue(LueCtCode), Sm.GetValue("Select CtContactPersonName From tblBOQhdr Where DocNo ='" + TxtBOQDocNo.Text + "'")));
        }

        private void BtnCtShippingAddress_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    //var f = new FrmCustomer("***");
                    //f.Tag = "***";
                    //f.WindowState = FormWindowState.Normal;
                    //f.StartPosition = FormStartPosition.CenterScreen;
                    //f.mCtCode = Sm.GetLue(LueCtCode);
                    //f.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex + "");
                }
            }
        }

        private void BtnNTPDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsTxtEmpty(TxtBOQDocNo, "Bill of quantity#", false))
                    Sm.FormShowDialog(new FrmSOContract2Dlg7(this));
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = TxtFile.Text;
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            try
            {
                DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = TxtFile2.Text;
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            try
            {
                DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = TxtFile3.Text;
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnCopyBOQ_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtBOQDocNo, "Bill of quantity", false)) return;

            if (MessageBox.Show(
                "Do you want to copy data from BOQ ?" + Environment.NewLine +
                "Note : this process will delete existing data.", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                == DialogResult.No) return;
            {
                var l = new List<ListOfItem>();

                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    Sm.ClearGrd(Grd3, true);
                    CopyBOQ(ref l);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnCopyDeliveryDt_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                "Do you want to copy delivery date (based on item and no) from revision data ?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
                == DialogResult.No) return;
            {
                var l = new List<ItDeliveryDt>();

                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    CopyDeliveryDt(ref l);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Class

        private class ListOfItem
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public decimal TaxRate { get; set; }
            public decimal TaxAmt { get; set; }
            public string Remark { get; set; }
            public string No { get; set; }
            public string Specification { get; set; }
            public string CtItCode { get; set; }
            public string CtItName { get; set; }
        }

        private class ItDeliveryDt
        {
            public string ItCode { get; set; }
            public string No { get; set; }
            public string DeliveryDt { get; set; }
        }

        private class SOContract
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string PrintBy { get; set; }
            public string DocNo { get; set; }
            public string PONo { get; set; }
            public string DocDt { get; set; }
            public string ContactPerson { get; set; }
            public string ShippingName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string CityName { get; set; }
            public string PostalCd { get; set; }
            public string CntName { get; set; }
            public string CtName { get; set; }
            public string NTPDocNo { get; set; }
            public string Remark { get; set; }
            public string NTPMemo { get; set; }
        }

        private class SOContractDtl
        {
            public int No { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string Remark { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUom { get; set; }
            public string DeliveryDt { get; set; }
            public decimal UnitPrice { get; set; }
            public decimal TotalPrice { get; set; }
            public decimal TotalPriceContract { get; set; }
            public decimal RevisionQty { get; set; }
            public decimal RevisionUnitPrice { get; set; }
            public decimal RevisionTotalPrice { get; set; }
            public string tipe { get; set; }
            public string NoItem { get; set; }
            public string SeqNo { get; set; }
            public string ItNameDismantle { get; set; }
        }

        private class SOContractSubTotal
        {
            public decimal SubTotal { get; set; }
            public decimal PPN { get; set; }
            public decimal Total { get; set; }
        }

        private class SOContractDtl2
        {
            public string ItCode { get; set; }
            public string DeliveryDt { get; set; }
        }

        private class SOCSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string DeptName { get; set; }
        }

        private class SOCSign2
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
            public string EmpName2 { get; set; }
            public string PosName2 { get; set; }
            public string UserName { get; set; }
            public string UserName2 { get; set; }
            public string DeptName { get; set; }
            public string DeptName2 { get; set; }
            public string Signature { get; set; }
            public string Signature2 { get; set; }
        }

        #endregion
    }
}
