﻿namespace RunSystem
{
    partial class FrmMaterialRequest3Dlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkItCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueItCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkItCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkAutoChoose = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtBom2DocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkBom2DocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtFinishedGood = new DevExpress.XtraEditors.TextEdit();
            this.ChkFinishedGood = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtComponent = new DevExpress.XtraEditors.TextEdit();
            this.ChkComponent = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoChoose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBom2DocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBom2DocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFinishedGood.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFinishedGood.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtComponent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkComponent.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtComponent);
            this.panel2.Controls.Add(this.ChkComponent);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtFinishedGood);
            this.panel2.Controls.Add(this.ChkFinishedGood);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtBom2DocNo);
            this.panel2.Controls.Add(this.ChkBom2DocNo);
            this.panel2.Controls.Add(this.ChkAutoChoose);
            this.panel2.Controls.Add(this.ChkItCtCode);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueItCtCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtItCode);
            this.panel2.Controls.Add(this.ChkItCode);
            this.panel2.Size = new System.Drawing.Size(672, 145);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 328);
            this.Grd1.TabIndex = 26;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkItCtCode
            // 
            this.ChkItCtCode.Location = new System.Drawing.Point(390, 24);
            this.ChkItCtCode.Name = "ChkItCtCode";
            this.ChkItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCtCode.Properties.Caption = " ";
            this.ChkItCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCtCode.Size = new System.Drawing.Size(31, 22);
            this.ChkItCtCode.TabIndex = 15;
            this.ChkItCtCode.ToolTip = "Remove filter";
            this.ChkItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCtCode.ToolTipTitle = "Run System";
            this.ChkItCtCode.CheckedChanged += new System.EventHandler(this.ChkItCtCode_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(33, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 13;
            this.label4.Text = "Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueItCtCode
            // 
            this.LueItCtCode.EnterMoveNextControl = true;
            this.LueItCtCode.Location = new System.Drawing.Point(93, 26);
            this.LueItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCtCode.Name = "LueItCtCode";
            this.LueItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCtCode.Properties.DropDownRows = 25;
            this.LueItCtCode.Properties.NullText = "[Empty]";
            this.LueItCtCode.Properties.PopupWidth = 500;
            this.LueItCtCode.Size = new System.Drawing.Size(294, 20);
            this.LueItCtCode.TabIndex = 14;
            this.LueItCtCode.ToolTip = "F4 : Show/hide list";
            this.LueItCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCtCode.EditValueChanged += new System.EventHandler(this.LueItCtCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(56, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "Item";
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(93, 4);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Size = new System.Drawing.Size(294, 20);
            this.TxtItCode.TabIndex = 11;
            this.TxtItCode.Validated += new System.EventHandler(this.TxtItCode_Validated);
            // 
            // ChkItCode
            // 
            this.ChkItCode.Location = new System.Drawing.Point(390, 3);
            this.ChkItCode.Name = "ChkItCode";
            this.ChkItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkItCode.Properties.Appearance.Options.UseFont = true;
            this.ChkItCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkItCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkItCode.Properties.Caption = " ";
            this.ChkItCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkItCode.Size = new System.Drawing.Size(31, 22);
            this.ChkItCode.TabIndex = 12;
            this.ChkItCode.ToolTip = "Remove filter";
            this.ChkItCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkItCode.ToolTipTitle = "Run System";
            this.ChkItCode.CheckedChanged += new System.EventHandler(this.ChkItCode_CheckedChanged);
            // 
            // ChkAutoChoose
            // 
            this.ChkAutoChoose.Location = new System.Drawing.Point(94, 115);
            this.ChkAutoChoose.Name = "ChkAutoChoose";
            this.ChkAutoChoose.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkAutoChoose.Properties.Appearance.Options.UseFont = true;
            this.ChkAutoChoose.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkAutoChoose.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkAutoChoose.Properties.Caption = "Automatic choose item";
            this.ChkAutoChoose.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkAutoChoose.Size = new System.Drawing.Size(160, 22);
            this.ChkAutoChoose.TabIndex = 25;
            this.ChkAutoChoose.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkAutoChoose.ToolTipTitle = "Run System";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(49, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "Bom#";
            // 
            // TxtBom2DocNo
            // 
            this.TxtBom2DocNo.EnterMoveNextControl = true;
            this.TxtBom2DocNo.Location = new System.Drawing.Point(93, 48);
            this.TxtBom2DocNo.Name = "TxtBom2DocNo";
            this.TxtBom2DocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBom2DocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBom2DocNo.Properties.MaxLength = 250;
            this.TxtBom2DocNo.Size = new System.Drawing.Size(294, 20);
            this.TxtBom2DocNo.TabIndex = 17;
            this.TxtBom2DocNo.Validated += new System.EventHandler(this.TxtBom2DocNo_Validated);
            // 
            // ChkBom2DocNo
            // 
            this.ChkBom2DocNo.Location = new System.Drawing.Point(390, 47);
            this.ChkBom2DocNo.Name = "ChkBom2DocNo";
            this.ChkBom2DocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBom2DocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkBom2DocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBom2DocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBom2DocNo.Properties.Caption = " ";
            this.ChkBom2DocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBom2DocNo.Size = new System.Drawing.Size(31, 22);
            this.ChkBom2DocNo.TabIndex = 18;
            this.ChkBom2DocNo.ToolTip = "Remove filter";
            this.ChkBom2DocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBom2DocNo.ToolTipTitle = "Run System";
            this.ChkBom2DocNo.CheckedChanged += new System.EventHandler(this.ChkBom2DocNo_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 14);
            this.label2.TabIndex = 19;
            this.label2.Text = "Finished Good";
            // 
            // TxtFinishedGood
            // 
            this.TxtFinishedGood.EnterMoveNextControl = true;
            this.TxtFinishedGood.Location = new System.Drawing.Point(93, 70);
            this.TxtFinishedGood.Name = "TxtFinishedGood";
            this.TxtFinishedGood.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinishedGood.Properties.Appearance.Options.UseFont = true;
            this.TxtFinishedGood.Properties.MaxLength = 250;
            this.TxtFinishedGood.Size = new System.Drawing.Size(294, 20);
            this.TxtFinishedGood.TabIndex = 20;
            this.TxtFinishedGood.Validated += new System.EventHandler(this.TxtFinishedGood_Validated);
            // 
            // ChkFinishedGood
            // 
            this.ChkFinishedGood.Location = new System.Drawing.Point(390, 69);
            this.ChkFinishedGood.Name = "ChkFinishedGood";
            this.ChkFinishedGood.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFinishedGood.Properties.Appearance.Options.UseFont = true;
            this.ChkFinishedGood.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFinishedGood.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFinishedGood.Properties.Caption = " ";
            this.ChkFinishedGood.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFinishedGood.Size = new System.Drawing.Size(31, 22);
            this.ChkFinishedGood.TabIndex = 21;
            this.ChkFinishedGood.ToolTip = "Remove filter";
            this.ChkFinishedGood.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFinishedGood.ToolTipTitle = "Run System";
            this.ChkFinishedGood.CheckedChanged += new System.EventHandler(this.ChkFinishedGood_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "Component";
            // 
            // TxtComponent
            // 
            this.TxtComponent.EnterMoveNextControl = true;
            this.TxtComponent.Location = new System.Drawing.Point(94, 92);
            this.TxtComponent.Name = "TxtComponent";
            this.TxtComponent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtComponent.Properties.Appearance.Options.UseFont = true;
            this.TxtComponent.Properties.MaxLength = 250;
            this.TxtComponent.Size = new System.Drawing.Size(294, 20);
            this.TxtComponent.TabIndex = 23;
            this.TxtComponent.Validated += new System.EventHandler(this.TxtComponent_Validated);
            // 
            // ChkComponent
            // 
            this.ChkComponent.Location = new System.Drawing.Point(390, 91);
            this.ChkComponent.Name = "ChkComponent";
            this.ChkComponent.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkComponent.Properties.Appearance.Options.UseFont = true;
            this.ChkComponent.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkComponent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkComponent.Properties.Caption = " ";
            this.ChkComponent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkComponent.Size = new System.Drawing.Size(31, 22);
            this.ChkComponent.TabIndex = 24;
            this.ChkComponent.ToolTip = "Remove filter";
            this.ChkComponent.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkComponent.ToolTipTitle = "Run System";
            this.ChkComponent.CheckedChanged += new System.EventHandler(this.ChkComponent_CheckedChanged);
            // 
            // FrmMaterialRequest3Dlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmMaterialRequest3Dlg";
            this.Text = "List of Item";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkAutoChoose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBom2DocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBom2DocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFinishedGood.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFinishedGood.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtComponent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkComponent.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit ChkItCtCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueItCtCode;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtItCode;
        private DevExpress.XtraEditors.CheckEdit ChkItCode;
        private DevExpress.XtraEditors.CheckEdit ChkAutoChoose;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtBom2DocNo;
        private DevExpress.XtraEditors.CheckEdit ChkBom2DocNo;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtComponent;
        private DevExpress.XtraEditors.CheckEdit ChkComponent;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtFinishedGood;
        private DevExpress.XtraEditors.CheckEdit ChkFinishedGood;
    }
}