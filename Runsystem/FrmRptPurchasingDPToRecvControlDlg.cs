﻿#region Update
/*
    25/01/2018 [TKG] Purchasing Downpayment To Receiving Control Reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasingDPToRecvControlDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptPurchasingDPToRecvControl mFrmParent;
        private string mPODocNo = string.Empty, mCurCode = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptPurchasingDPToRecvControlDlg(FrmRptPurchasingDPToRecvControl FrmParent, string PODocNo, string CurCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPODocNo = PODocNo;
            mCurCode = CurCode;

            TxtPODocNo.EditValue = mPODocNo;
            TxtCurCode.EditValue = mCurCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                base.FrmLoad(sender, e);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.RowHeader.Visible = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Item's Code", 
                        "Item's Name", 
                        "Amount Before Tax",
                        "Tax",
                        "Amount After Tax"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();
            var subSQL = string.Empty;

            subSQL = 
                "((B.Qty*E.UPrice)-" +
                "(B.Qty*E.UPrice*B.Discount*0.01)-" +
                "(B.Qty*B.DiscountAmt)+" +
                "(B.Qty*B.RoundingValue)) ";

            SQL.AppendLine("Select F.ItCode, F.ItName, ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("As Amt, ");
            SQL.AppendLine("(");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("*IfNull(G.TaxRate, 0.00)*0.01) ");
            SQL.AppendLine("+(");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("*IfNull(H.TaxRate, 0.00)*0.01) ");
            SQL.AppendLine("+(");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("*IfNull(I.TaxRate, 0.00)*0.01) ");
            SQL.AppendLine("As Tax ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B on A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtDtl E On C.QtDocNo=E.DocNo And C.QtDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem F On D.ItCode=F.ItCode ");
            SQL.AppendLine("Left Join TblTax G On A.TaxCode1=G.TaxCode ");
            SQL.AppendLine("Left Join TblTax H On A.TaxCode2=H.TaxCode ");
            SQL.AppendLine("Left Join TblTax I On A.TaxCode3=I.TaxCode ");
            SQL.AppendLine("Where A.DocNo=@PODocNo ");
            SQL.AppendLine("Order By B.DNo; ");

            return SQL.ToString();
        }

        private void ShowData()
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@PODocNo", mPODocNo);
            
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[]
                    {
                        //0
                        "ItCode",

                        //1-3
                        "ItName", "Amt", "Tax"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Grd.Cells[Row, 5].Value = Sm.GetGrdDec(Grd, Row, 3) + Sm.GetGrdDec(Grd, Row, 4);
                    }, false, false, false, false
                );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #endregion


    }
}
