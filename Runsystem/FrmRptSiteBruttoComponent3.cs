﻿#region Update
/*
    19/03/2019 [HAR] tambah rapel ambil dari salary adjustment
    23/04/2019 [TKG] payrun yg dicancel tidak ditampilkan.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSiteBruttoComponent3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mADCodeUTD = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;
       

        #endregion

        #region Constructor

        public FrmRptSiteBruttoComponent3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mADCodeUTD = Sm.GetParameter("ADCodeUTD");
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.EmploymentStatus, T1.EmpStatus, T1.Yr, T1.mth, T1.SectionCode, T1.SectionName,   ");
            SQL.AppendLine("SUM(T1.Salary) Salary, SUM(T1.meal) meal, SUm(T1.Transport) Transport, SUM(T1.variableallowance) variableallowance,  ");
            SQL.AppendLine("SUM(T1.AllVariableAllowance) AllVariableAllowance, SUM(T1.AmtUtd) UTD, SUM(T1.SalaryAdjustment) SalaryAdjustment, SUM(T1.ADLeave) ADLeave, SUM(T1.OT) OT, SUM(T1.THP) THP, SUM(t1.SSEmployerEmployment) SSEmployerEmployment,  ");
            SQL.AppendLine("SUM(T1.SSErPension) SSErPension, SUM(T1.SSEmployerHealth) SSEmployerHealth, SUM(T1.SSErDPLK) SSErDPLK, SUM(T1.Tax) Tax,  ");
            SQL.AppendLine("SUM(T1.SeveranceReserve) SeveranceReserve, SUM(T1.AddSeveranceReserve) AddSeveranceReserve, SUM(T1.Brutto) Brutto  ");
            SQL.AppendLine("From (   ");

            SQL.AppendLine("Select A.PayrunCode, C.EmploymentStatus, H.OptDesc As EmpStatus, Left(B.startDt, 4) As Yr, SubString(B.Startdt, 5,2) As Mth,  ");
            SQL.AppendLine("A.EmpCode, C.EmpName, C.EmpCodeOld, C.DeptCode, D.Deptname, E.Posname, C.SectionCode, ");
            SQL.AppendLine("F.SectionName, C.SiteCode, G.Sitename,  ");
            SQL.AppendLine("A.Salary, A.Meal, A.Transport, A.VariableAllowance, (A.AllVariableAllowance+ifnull(J.AmtOther, 0)) AllVariableAllowance, ifnull(I.AmtUtd, 0) As AmtUTD, A.ADLeave, ");
            SQL.AppendLine("(A.OT1Amt+A.OT2Amt+A.OTHolidayAmt) OT,  ");
            SQL.AppendLine("(A.Salary+A.FixAllowance+A.meal+A.transport+A.VariableAllowance+A.IncEmployee+A.ADLeave+ ");
            SQL.AppendLine("A.SalaryAdjustment+A.AllVariableAllowance+A.OT1Amt+A.OT2Amt+A.OTHolidayAmt) As THP, ");
            SQL.AppendLine("A.SSEmployerEmployment, A.SSErPension, A.SSEmployerHealth, A.SSErDPLK, A.TaxAllowance As Tax, ");
            SQL.AppendLine("A.SeveranceReserve, A.AddSeveranceReserve, ");
            //SQL.AppendLine("(A.Salary+A.FixAllowance+A.meal+A.transport+A.VariableAllowance+A.IncEmployee+A.ADLeave+ ");
            //SQL.AppendLine("A.SalaryAdjustment+A.AllVariableAllowance+A.OT1Amt+A.OT2Amt+A.OTHolidayAmt+  ");
            //SQL.AppendLine("A.SSEmployerEmployment+ A.SSErPension+ A.SSEmployerHealth+ A.SSErDPLK+ A.TaxAllowance+ ");
            //SQL.AppendLine("A.SeveranceReserve+ A.AddSeveranceReserve) As Brutto, ");
            SQL.AppendLine("(A.Salary+A.FixAllowance+A.meal+A.transport+A.VariableAllowance+");
            SQL.AppendLine("A.SSErPension+A.SSEmployerHealth+A.SSEmployerEmployment+A.SSErDPLK+A.SeveranceReserve+");
            SQL.AppendLine("A.taxAllowance+A.AddSeveranceReserve+A.IncEmployee+ADLeave+A.SalaryAdjustment+");
            SQL.AppendLine("AllVariableAllowance+A.OT1Amt+A.OT2Amt+A.OTHolidayAmt) As Brutto, ");
            SQL.AppendLine("A.SalaryAdjustment  ");
            SQL.AppendLine("From tblpayrollprocess1 A ");
            SQL.AppendLine("Inner join tblpayrun B On A.payrunCode = B.payrunCode And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join tblEmployee C On A.EmpCode = C.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode = D.DeptCode ");
            SQL.AppendLine("Left Join tblPosition E On C.posCode =E.posCode ");
            SQL.AppendLine("left join tblSection F on C.SectionCode = F.SectionCode ");
            SQL.AppendLine("Left Join TblSite G On C.SiteCode = G.SiteCode ");
            SQL.AppendLine("Inner Join tblOption H On C.EmploymentStatus = H.OptCode And H.OptCat = 'EmploymentStatus'  ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select payrunCode, EmpCode, Amt As AmtUtD  ");
	        SQL.AppendLine("    from tblpayrollprocessAd  ");
            SQL.AppendLine("    Where ADCode = @ADCodeUTD  ");
            SQL.AppendLine(")I On A.payrunCode = I.payrunCode And A.EmpCode = I.EmpCode  ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select A.payrunCode, A.empCode, A.Amt As AmtOther  ");
            SQL.AppendLine("    From tblpayrollprocessAd A ");
            SQL.AppendLine("    Inner Join tblAllowancededuction B On A.AdCode = B.AdCode And B.Adtype = 'A' ");
            SQL.AppendLine("    Where not find_in_set(A.AdCode,  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Z.AdCode From ");
            SQL.AppendLine("        ( ");
	        SQL.AppendLine("            Select group_Concat(X.parvalue) AdCode from ");
	        SQL.AppendLine("            ( ");
	        SQL.AppendLine("            Select parvalue From Tblparameter ");
	        SQL.AppendLine("            Where parCode  = 'AllowanceCodeVariable' ");
	        SQL.AppendLine("            Union all ");
	        SQL.AppendLine("            Select parvalue From Tblparameter ");
	        SQL.AppendLine("            Where parCode  = 'ADCodeVariable' ");
	        SQL.AppendLine("            union all ");
	        SQL.AppendLine("            Select parvalue From Tblparameter ");
	        SQL.AppendLine("            Where parCode  = 'ADCodemeal' ");
	        SQL.AppendLine("            union all ");
	        SQL.AppendLine("            Select parvalue From Tblparameter ");
	        SQL.AppendLine("            Where parCode  = 'ADCodetransport' ");
	        SQL.AppendLine("            Union All ");
	        SQL.AppendLine("            Select parvalue From Tblparameter ");
	        SQL.AppendLine("            Where parCode  = 'ADCodeUTD' ");
	        SQL.AppendLine("            )X ");
            SQL.AppendLine("        )Z ");
            SQL.AppendLine("    ))>0 ");
            SQL.AppendLine(")J On A.payrunCode = J.payrunCode And A.EmpCode = J.EmpCode  ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Where Left(B.startDt, 4) = @Yr And SubString(B.Startdt, 5,2) = @Mth ");

            SQL.AppendLine(")T1 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Employement"+Environment.NewLine+"Status",
                        "Year",
                        "Month",
                        "RCTR",
                        "DESCRIPTION",
                        //6-10
                        "UPAH TETAP",
                        "MAKAN",
                        "TRANSPORT",
                        "T. VARIABEL",
                        "T. LAIN",
                        //11-15
                        "UTD",
                        "RAPEL",
                        "LEAVE",
                        "OVER TIME",
                        "THP",
                        //16-20
                        "BPJS TK",
                        "BPJS JP",
                        "BPJS KES",
                        "DPLK",
                        "Tax",
                        //21-23
                        "T. PESANGON",
                        "TC. PESANGON",
                        "Total"
                   },
                     new int[] 
                    {
                        //0
                        40,
                        //1-5
                        100, 80, 80, 100, 200,                       
                        //6-10
                        120, 120, 120,120, 120, 
                        //11-15
                        120, 120, 120,120, 120, 
                        //16-20
                        120, 120, 120, 120, 120, 
                        //21
                        120, 120, 120
                   }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6,7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month") || Sm.IsLueEmpty(LueEmploymentStatus, "Employment Status")) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter ="Where 0=0 ";

                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@ADCodeUTD", mADCodeUTD);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueEmploymentStatus), "T1.EmploymentStatus", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " group by T1.EmploymentStatus, T1.EmpStatus, T1.Yr, T1.mth, T1.SectionCode, T1.SectionName Order By  T1.SectionName ;",
                        new string[]
                        {
                            //0
                            "EmpStatus", 
                            //1-5
                            "Yr", "Mth",  "SectionCode", "SectionName", "Salary", 
                            //6-10
                            "Meal", "Transport", "VariableAllowance", "AllVariableAllowance", "UTD",  
                            //11-15
                            "SalaryAdjustment", "ADleave", "OT", "THP", "SSEmployerEmployment",   
                            //16-20
                            "SSErPension", "SSEmployerHealth", "SSErDPLK", "Tax","SeveranceReserve",  
                            //21
                            "AddSeveranceReserve","Brutto"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        private void ParPrint()
        {
            if (IsGrdEmpty() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<Bruttohdr>();
            var ldtl = new List<BruttoDtl>();
            string[] TableName = { "Bruttohdr", "BruttoDtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2',");
            SQL.AppendLine("@yr As Yr, MONTHNAME(concat(@yr, '-', @mth, '-', '01')) As Mth, @EmpStatus As EmpStatus ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@Yr",  Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@EmpStatus", LueEmploymentStatus.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                        "CompanyLogo",

                         //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyPhone",
                        "Yr",
                        "Mth",
                        //6
                        "EmpStatus"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Bruttohdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            Yr = Sm.DrStr(dr, c[4]),
                            Mth = Sm.DrStr(dr, c[5]),
                            EmpStatus = Sm.DrStr(dr, c[6]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select T1.EmploymentStatus, T1.EmpStatus, T1.Yr, T1.mth, T1.SectionCode, T1.SectionName,   ");
                SQLDtl.AppendLine("SUM(T1.Salary) Salary, SUM(T1.meal) meal, SUm(T1.Transport) Transport, SUM(T1.variableallowance) variableallowance,  ");
                SQLDtl.AppendLine("SUM(T1.AllVariableAllowance) AllVariableAllowance, SUM(T1.AmtUtd) UTD, SUM(T1.SalaryAdjustment) SalaryAdjustment, SUM(T1.ADLeave) ADLeave, SUM(T1.OT) OT, SUM(T1.THP) THP, SUM(t1.SSEmployerEmployment) SSEmployerEmployment,  ");
                SQLDtl.AppendLine("SUM(T1.SSErPension) SSErPension, SUM(T1.SSEmployerHealth) SSEmployerHealth, SUM(T1.SSErDPLK) SSErDPLK, SUM(T1.Tax) Tax,  ");
                SQLDtl.AppendLine("SUM(T1.SeveranceReserve) SeveranceReserve, SUM(T1.AddSeveranceReserve) AddSeveranceReserve, SUM(T1.Brutto) Brutto  ");
                SQLDtl.AppendLine("From (   ");
                SQLDtl.AppendLine("Select A.PayrunCode, C.EmploymentStatus, H.OptDesc As EmpStatus, Left(B.startDt, 4) As Yr, SubString(B.Startdt, 5,2) As Mth,  ");
                SQLDtl.AppendLine("A.EmpCode, C.EmpName, C.EmpCodeOld, C.DeptCode, D.Deptname, E.Posname, C.SectionCode, ");
                SQLDtl.AppendLine("F.SectionName, C.SiteCode, G.Sitename,  ");
                SQLDtl.AppendLine("A.Salary, A.Meal, A.Transport, A.VariableAllowance, (A.AllVariableAllowance+ifnull(J.AmtOther, 0)) AllVariableAllowance, ifnull(I.AmtUtd, 0) As AmtUTD, A.ADLeave, ");
                SQLDtl.AppendLine("(A.OT1Amt+A.OT2Amt+A.OTHolidayAmt) OT, A.SalaryAdjustment,  ");
                SQLDtl.AppendLine("(A.Salary+A.FixAllowance+A.meal+A.transport+A.VariableAllowance+A.IncEmployee+A.ADLeave+ ");
                SQLDtl.AppendLine("A.SalaryAdjustment+A.AllVariableAllowance+A.OT1Amt+A.OT2Amt+A.OTHolidayAmt) As THP, ");
                SQLDtl.AppendLine("A.SSEmployerEmployment, A.SSErPension, A.SSEmployerHealth, A.SSErDPLK, A.TaxAllowance As Tax, ");
                SQLDtl.AppendLine("A.SeveranceReserve, A.AddSeveranceReserve, ");
                SQLDtl.AppendLine("(A.Salary+A.FixAllowance+A.meal+A.transport+A.VariableAllowance+");
                SQLDtl.AppendLine("A.SSErPension+A.SSEmployerHealth+A.SSEmployerEmployment+A.SSErDPLK+A.SeveranceReserve+");
                SQLDtl.AppendLine("A.taxAllowance+A.AddSeveranceReserve+A.IncEmployee+ADLeave+A.SalaryAdjustment+");
                SQLDtl.AppendLine("AllVariableAllowance+A.OT1Amt+A.OT2Amt+A.OTHolidayAmt) As Brutto ");
                SQLDtl.AppendLine("From tblpayrollprocess1 A ");
                SQLDtl.AppendLine("Inner join tblpayrun B On A.payrunCode = B.payrunCode And B.CancelInd='N' ");
                SQLDtl.AppendLine("Inner Join tblEmployee C On A.EmpCode = C.EmpCode ");
                SQLDtl.AppendLine("Inner Join TblDepartment D On C.DeptCode = D.DeptCode ");
                SQLDtl.AppendLine("Left Join tblPosition E On C.posCode =E.posCode ");
                SQLDtl.AppendLine("left join tblSection F on C.SectionCode = F.SectionCode ");
                SQLDtl.AppendLine("Left Join TblSite G On C.SiteCode = G.SiteCode ");
                SQLDtl.AppendLine("Inner Join tblOption H On C.EmploymentStatus = H.OptCode And H.OptCat = 'EmploymentStatus'  ");
                SQLDtl.AppendLine("Left Join  ");
                SQLDtl.AppendLine("(  ");
                SQLDtl.AppendLine("    Select payrunCode, EmpCode, Amt As AmtUtD  ");
                SQLDtl.AppendLine("    from tblpayrollprocessAd  ");
                SQLDtl.AppendLine("    Where ADCode = @ADCodeUTD  ");
                SQLDtl.AppendLine(")I On A.payrunCode = I.payrunCode And A.EmpCode = I.EmpCode  ");
                SQLDtl.AppendLine("Left Join  ");
                SQLDtl.AppendLine("(  ");
                SQLDtl.AppendLine("    Select A.payrunCode, A.empCode, A.Amt As AmtOther  ");
                SQLDtl.AppendLine("    From tblpayrollprocessAd A ");
                SQLDtl.AppendLine("    Inner Join tblAllowancededuction B On A.AdCode = B.AdCode And B.Adtype = 'A' ");
                SQLDtl.AppendLine("    Where not find_in_set(A.AdCode,  ");
                SQLDtl.AppendLine("    ( ");
                SQLDtl.AppendLine("        Select Z.AdCode From ");
                SQLDtl.AppendLine("        ( ");
                SQLDtl.AppendLine("            Select group_Concat(X.parvalue) AdCode from ");
                SQLDtl.AppendLine("            ( ");
                SQLDtl.AppendLine("            Select parvalue From Tblparameter ");
                SQLDtl.AppendLine("            Where parCode  = 'AllowanceCodeVariable' ");
                SQLDtl.AppendLine("            Union all ");
                SQLDtl.AppendLine("            Select parvalue From Tblparameter ");
                SQLDtl.AppendLine("            Where parCode  = 'ADCodeVariable' ");
                SQLDtl.AppendLine("            union all ");
                SQLDtl.AppendLine("            Select parvalue From Tblparameter ");
                SQLDtl.AppendLine("            Where parCode  = 'ADCodemeal' ");
                SQLDtl.AppendLine("            union all ");
                SQLDtl.AppendLine("            Select parvalue From Tblparameter ");
                SQLDtl.AppendLine("            Where parCode  = 'ADCodetransport' ");
                SQLDtl.AppendLine("            Union All ");
                SQLDtl.AppendLine("            Select parvalue From Tblparameter ");
                SQLDtl.AppendLine("            Where parCode  = 'ADCodeUTD' ");
                SQLDtl.AppendLine("            )X ");
                SQLDtl.AppendLine("        )Z ");
                SQLDtl.AppendLine("    ))>0 ");
                SQLDtl.AppendLine(")J On A.payrunCode = J.payrunCode And A.EmpCode = J.EmpCode  ");
                if (mIsFilterBySiteHR)
                {
                    SQLDtl.AppendLine("    And C.SiteCode Is Not Null ");
                    SQLDtl.AppendLine("    And Exists( ");
                    SQLDtl.AppendLine("        Select 1 From TblGroupSite ");
                    SQLDtl.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                    SQLDtl.AppendLine("        And GrpCode In ( ");
                    SQLDtl.AppendLine("            Select GrpCode From TblUser ");
                    SQLDtl.AppendLine("            Where UserCode=@UserCode ");
                    SQLDtl.AppendLine("        ) ");
                    SQLDtl.AppendLine("    ) ");
                }
                SQLDtl.AppendLine("Where Left(B.startDt, 4) = @Yr And SubString(B.Startdt, 5,2) = @Mth ");
                SQLDtl.AppendLine(")T1 ");
                SQLDtl.AppendLine("Where T1.EmploymentStatus = @EmpStatus ");
                SQLDtl.AppendLine("Group by T1.EmploymentStatus, T1.EmpStatus, T1.Yr, T1.mth, T1.SectionCode, T1.SectionName Order By  T1.SectionName ");
                
                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cmDtl, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cmDtl, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cmDtl, "@EmpStatus", Sm.GetLue(LueEmploymentStatus));
                Sm.CmParam<String>(ref cmDtl, "@ADCodeUTD", mADCodeUTD);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "EmpStatus",
                    //1-5
                    "Yr", 
                    "Mth",  
                    "SectionCode", 
                    "SectionName", 
                    "Salary", 
                    //6-10
                    "Meal", 
                    "Transport", 
                    "VariableAllowance", 
                    "AllVariableAllowance", 
                    "UTD",
                    //11-15
                    "SalaryAdjustment",
                    "ADleave",
                    "OT", 
                    "THP", 
                    "SSEmployerEmployment", 
                    //16-20
                    "SSErPension",
                    "SSEmployerHealth",
                    "SSErDPLK", 
                    "Tax",
                    "SeveranceReserve", 
                    //21
                    "AddSeveranceReserve",
                    "Brutto"
                   
                });

                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new BruttoDtl()
                        {
                            nomor = nomor,
                            EmpStatus = Sm.DrStr(drDtl, cDtl[0]),
                            Yr  = Sm.DrStr(drDtl, cDtl[1]),
                            Mth  = Sm.DrStr(drDtl, cDtl[2]),
                            SectionCode  = Sm.DrStr(drDtl, cDtl[3]),
                            Section  = Sm.DrStr(drDtl, cDtl[4]),
                            Salary  = Sm.DrDec(drDtl, cDtl[5]),
                            Meal = Sm.DrDec(drDtl, cDtl[6]),
                            Transport = Sm.DrDec(drDtl, cDtl[7]),
                            VarAllowance = Sm.DrDec(drDtl, cDtl[8]),
                            AllVarAllowance = Sm.DrDec(drDtl, cDtl[9]),
                            UTD = Sm.DrDec(drDtl, cDtl[10]),
                            Rapel = Sm.DrDec(drDtl, cDtl[11]),
                            ADLeave = Sm.DrDec(drDtl, cDtl[12]),
                            OverTime = Sm.DrDec(drDtl, cDtl[13]),
                            THP = Sm.DrDec(drDtl, cDtl[14]),
                            SSEmployment = Sm.DrDec(drDtl, cDtl[15]),
                            SSPension = Sm.DrDec(drDtl, cDtl[16]),
                            SSHealth = Sm.DrDec(drDtl, cDtl[17]),
                            SSDPLK = Sm.DrDec(drDtl, cDtl[18]),
                            Tax = Sm.DrDec(drDtl, cDtl[19]),
                            SeveranceReserve = Sm.DrDec(drDtl, cDtl[20]),
                            AddSeveranceReserve = Sm.DrDec(drDtl, cDtl[21]),
                            Total = Sm.DrDec(drDtl, cDtl[22]),
                        });
                    }
                }

                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            if (Sm.GetLue(LueEmploymentStatus) == "1")
            {
                Sm.PrintReport("Brutto", myLists, TableName, false);
            }
            else
            {
                Sm.PrintReport("Brutto2", myLists, TableName, false);
            }
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return true;
            }
            return false;
        }
        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmploymentStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment Status");
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
        }

        private void BtnPrintOut_Click(object sender, EventArgs e)
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        

    

        #endregion

        #region Report Class

        class Bruttohdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string EmpStatus { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string PrintBy { get; set; }
        }

        class BruttoDtl
        {
            public decimal nomor { get; set; }
             public string EmpStatus { get; set; }
             public string Yr { get; set; }
             public string Mth { get; set; }
             public string SectionCode { get; set; }
             public string Section { get; set; }
            public decimal Salary { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal VarAllowance { get; set; }
            public decimal AllVarAllowance { get; set; }
            public decimal UTD { get; set; }
            public decimal Rapel { get; set; }
            public decimal ADLeave { get; set; }
            public decimal OverTime { get; set; }
            public decimal THP { get; set; }
            public decimal SSEmployment { get; set; }
            public decimal SSPension { get; set; }
            public decimal SSHealth { get; set; }
            public decimal SSDPLK { get; set; }
            public decimal Tax { get; set; }
            public decimal SeveranceReserve { get; set; }
            public decimal AddSeveranceReserve { get; set; }
            public decimal Total { get; set; }
        }

        #endregion

    }
}
