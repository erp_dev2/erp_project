﻿#region Update
/*
    19/05/2021 [WED/ALL] new base for reporting with tabbed grid
 */
#endregion

#region Namespace
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Sm = RunSystem.StdMtd;
#endregion

namespace RunSystem
{
    public partial class FrmBase16 : Form
    {
        #region Constructor
        public FrmBase16()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
        }

        virtual protected void SetSQL()
        {
        }

        virtual protected string SetReportName()
        {
            return "";
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void ShowChart()
        {

        }

        virtual protected void HideInfoInGrd()
        {

        }

        virtual protected void ExportToExcel()
        {
        }

        virtual protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            //if (Sm.GetLue(LueFontSize).Length != 0)
            //{
            //    Grd1.Font = new Font(
            //        Grd1.Font.FontFamily.Name.ToString(),
            //        int.Parse(Sm.GetLue(LueFontSize))
            //        );
            //    Sm.SetGrdAutoSize(Grd1);
            //}
        }

        #endregion

        #region Events

        #region Form Events

        private void FrmBase16_Load(object sender, EventArgs e)
        {
            BtnChart.Visible = false;
            FrmLoad(sender, e);
        }

        #endregion

        #region Button Click

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            //PrintData();
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }

        private void BtnChart_Click(object sender, EventArgs e)
        {
            ShowChart();
        }

        #endregion

        #region Misc Control Events

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }
        #endregion

        #endregion


    }
}
