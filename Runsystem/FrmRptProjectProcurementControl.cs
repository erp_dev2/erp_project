﻿#region Update
/*
    24/04/2020 [IBL/IMS] New Reporting
    19/01/2021 [ICA/IMS] Menambah kolom Local Code based on parameter IsBOMShowSpecifications
    06/04/2021 [WED/IMS] memunculkan semua PO tanpa melihat ada SOC nya atau tidak
    31/05/2021 [BRI/IMS] PO yang muncul hanya PO-LOG yang ada SOC nya saja
    22/12/2021 [YOG/IMS] Menambahkan data pengurangan item dari Cancellation of Purchase order's Quantity
    23/12/2021 [ISD/IMS] merubah perhitungan total price
    05/01/2022 [RDA/IMS] memunculkan PO for Service yang ber SOC dengan parameter IsUsePOForServiceDocNo
    06/01/2022 [SET/IMS] Menambahkan source PO for Service ke reporting Project Procurement Control dengan parameter IsUsePOForServiceDocNo
    10/01/2022 [YOG/IMS] Feedback : Apabila nilai cancelling's quantity adalah 0, maka nilai total quantity = PO's quantity
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectProcurementControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool 
            mIsShowPrice = false,
            mIsBOMShowSpecifications = false,
            mIsUsePOForServiceDocNo = false;

        #endregion

        #region Constructor

        public FrmRptProjectProcurementControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsShowPrice = Sm.CompareStr(mMenuCode, Sm.GetParameter("MenuCodeForRptProjectProcurementControlWithPrice"));
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsUsePOForServiceDocNo = Sm.GetParameterBoo("IsUsePOForServiceDocNo");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T.* FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT B.DocDt PODate, B.DocNo PODocNo, G.SOCDocNo, K.PGCode, K.ProjectName, ");
            SQL.AppendLine("    H.PONo, D.ItCode, E.ItCodeInternal, E.ItName, E.Specification, A.Qty, L.DocNo POCancellingDocNo, L.Qty POCancellingQty, A.Qty - IfNull(L.Qty, 0) TotalQuantity, ");
            SQL.AppendLine("    E.PurchaseUOMCode UoM, F.UPrice, (A.Qty - IFNULL(L.Qty,0))*F.UPrice Total ");
            SQL.AppendLine("    FROM TblPODtl A ");
            SQL.AppendLine("    INNER JOIN TblPOHdr B ON B.DocNo=A.DocNo ");
            SQL.AppendLine("        And B.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        And B.Status = 'A' ");
            SQL.AppendLine("        And B.DocNo LIKE '%PO-LOG%' ");
            SQL.AppendLine("    INNER JOIN TblPORequestDtl C ON C.DocNo=A.PORequestDocNo AND C.DNo=A.PORequestDNo ");
            SQL.AppendLine("    INNER JOIN TblMaterialRequestDtl D ON D.DocNo=C.MaterialRequestDocNo AND D.DNo=C.MaterialRequestDNo ");
            SQL.AppendLine("    INNER JOIN TblItem E ON E.ItCode = D.ItCode ");
            SQL.AppendLine("    INNER JOIN TblQtDtl F ON F.DocNo=C.QtDocNo AND F.DNo=C.QtDNo ");
            SQL.AppendLine("    INNER JOIN TblMaterialRequestHdr G ON G.DocNo=D.DocNo ");
            SQL.AppendLine("        AND G.SOCDocNo IS NOT NULL  ");
            SQL.AppendLine("    Left JOIN TblSOContractHdr H ON H.DocNo=G.SOCDocNo ");
            SQL.AppendLine("    LEFT JOIN TblBOQHdr I ON I.DocNo=H.BOQDocNo ");
            SQL.AppendLine("    LEFT JOIN TblLOPHdr J ON J.DocNo=I.LOPDocNo ");
            SQL.AppendLine("    LEFT JOIN TblProjectGroup K ON K.PGCode=J.PgCode ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("    Select DocNo, PODocNo, PODNo, Qty ");
            SQL.AppendLine("    From TblPOQtyCancel ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    ) L On L.PODocNo = A.DocNo And L.PODNo = A.DNo ");
            if (mIsUsePOForServiceDocNo)
            {
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("    SELECT A.DocDt PODate, A.DocNo PODocNo, D.SOCDocNo, L.PGCode, L.ProjectName, ");
                SQL.AppendLine("    I.PONo, C.ItCode, G.ItCodeInternal, G.ItName, G.Specification, B.Qty, M.DocNo POCancellingDocNo, M.Qty POCancellingQty, B.Qty - IfNull(M.Qty, 0) TotalQuantity, ");
                SQL.AppendLine("    G.PurchaseUOMCode UoM, H.UPrice, (B.Qty - IFNULL(M.Qty,0))*H.UPrice Total  ");
                SQL.AppendLine("    FROM tblmaterialrequesthdr A ");
                SQL.AppendLine("    INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    AND A.DocDt BETWEEN @DocDt1 AND @DocDt2 ");
                SQL.AppendLine("    AND B.CancelInd = 'N' ");
                SQL.AppendLine("    AND B.`Status` = 'A' ");
                SQL.AppendLine("    AND B.DocNo LIKE '%POS-LOG%' ");
                SQL.AppendLine("    INNER JOIN tblmaterialrequestservicedtl C ON B.MaterialRequestServiceDocNo = C.DocNo AND B.MaterialRequestServiceDNo = C.DNo ");
                SQL.AppendLine("    INNER JOIN tblmaterialrequestservicehdr D ON C.DocNo = D.DocNo AND D.SOCDocNo IS NOT NULL ");
                SQL.AppendLine("    INNER JOIN tblporequestdtl E ON B.DocNo = E.MaterialRequestDocNo AND B.DNo = E.MaterialRequestDNo ");
                SQL.AppendLine("    INNER JOIN tblpodtl F ON E.DocNo = F.PORequestDocNo AND E.DNo = F.PORequestDNo ");
                SQL.AppendLine("    INNER JOIN tblitem G ON B.ItCode = G.ItCode ");
                SQL.AppendLine("    INNER JOIN tblqtdtl H ON E.QtDocNo = H.DocNo AND E.QtDNo = H.DNo ");
                SQL.AppendLine("    LEFT JOIN tblsocontracthdr I ON D.SOCDocNo = I.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblboqhdr J ON I.BOQDocNo = J.DocNo ");
                SQL.AppendLine("    LEFT JOIN tbllophdr K ON J.LOPDocNo = K.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblprojectgroup L ON K.PGCode = L.PGCode ");
                SQL.AppendLine("    LEFT JOIN ( ");
                SQL.AppendLine("        Select DocNo, PODocNo, PODNo, Qty ");
                SQL.AppendLine("        From TblPOQtyCancel ");
                SQL.AppendLine("        Where CancelInd = 'N' ");
                SQL.AppendLine("        ) M ON F.DocNo = M.PODocNo AND F.DNo = M.PODNo ");
            }
            SQL.AppendLine(")T ");
            SQL.AppendLine("WHERE T.PODate BETWEEN @DocDt1 AND @DocDt2 ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "PO Date", 
                        "PO#",
                        "Cancelling PO#",
                        "SO#",
                        "Project Code",
                        
                        //6-10
                        "Project Name",
                        "Customer PO",
                        "Item Code",
                        "Local Code",
                        "Item Name",
                        
                        //11-15
                        "Specification",
                        "PO's Quantity",
                        "UoM",
                        "Cancelling's Quantity",
                        "UoM",
                        
                        //16-19
                        "Total Quantity",
                        "UoM",
                        "Unit Price",
                        "Total Price",
             
                    },
                    new int[] 
                    {
                        //0
                        25,

                        //1-5
                        80, 180, 180, 160, 80,  

                        //6-10
                        180, 120, 90, 110, 180, 

                        //11-15
                        180, 100, 70, 150, 70,

                        //16-19
                        150, 70, 150, 150
                    }
                );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, mIsShowPrice);
            Sm.GrdColInvisible(Grd1, new int[] { 9 }, mIsBOMShowSpecifications);
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16, 18, 19 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, new string[] { "T.SOCDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "T.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, new string[] { "T.PODocNo" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By T.PODate;",
                        new string[]
                        { 
                            //0
                            "PODate", 

                            //1-5
                            "PODocNo", "POCancellingDocNo", "SOCDocNo", "PGCode", "ProjectName", 
                            
                            //6-10
                            "PONo", "ItCode", "ItCodeInternal", "ItName", "Specification",

                            //11-15
                            "Qty", "UoM", "POCancellingQty", "TotalQuantity", "UPrice",

                            //16
                             "Total"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);

                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14, 16, 18, 19 });
                Grd1.Rows.CollapseAll();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                Grd1.Cells[Row, 5].Value = "'" + Sm.GetGrdStr(Grd1, Row, 5);
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                Grd1.Cells[Row, 5].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 5), Sm.GetGrdStr(Grd1, Row, 5).Length - 1);
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
