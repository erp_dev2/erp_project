﻿#region Update
/*
 03/11/2020 [DITA/PHT] New reporting
 25/11/2020 [VIN/PHT] Tambah Kolom Department
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthSalaryComparison : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptMonthSalaryComparison(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT *FROM  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	SELECT E.SiteCode, E.SiteName, F.DeptCode, F.DeptName, COUNT(B.EmpCode) TotalEmpCode1, COUNT(C.EmpCode) TotalEmpCode2, SUM(B.Amt) THP1, SUM(C.Amt) THP2, ");
            SQL.AppendLine(" 	SUM(B.Brutto) Brutto1, SUM(C.Brutto) Brutto2 ");
            SQL.AppendLine("	FROM TblPayrollProcess1 A ");
            SQL.AppendLine("	LEFT JOIN  ");
            SQL.AppendLine("	( ");
            SQL.AppendLine("		SELECT PayrunCode, EmpCode, Amt, (Salary+OT1Amt+OT2Amt+OTHolidayAmt+SalaryAdjustment+FixAllowance+VarAllowance+IncEmployee+TaxAllowance+SSEmployerHealth+SSEmployerEmployment) Brutto ");
            SQL.AppendLine("		FROM TblPayrollProcess1  ");
            SQL.AppendLine("		WHERE LEFT(PayrunCode, 6) = DATE_FORMAT(DATE(CONCAT(@Yr, @Mth, '01')- INTERVAL 1 MONTH), '%Y%m') ");
            SQL.AppendLine("		AND (ResignDt IS NULL OR (ResignDt Is Not Null And substring(ResignDt, 1, 6)>DATE_FORMAT(DATE(CONCAT(@Yr, @Mth, '01')- INTERVAL 1 MONTH), '%Y%m')))  ");
            SQL.AppendLine("		GROUP BY PayrunCode, EmpCode ");
            SQL.AppendLine("	) B ON A.PayrunCode = B.PayrunCode AND A.EmpCode = B.EmpCode ");
            	
            SQL.AppendLine("	LEFT JOIN "); 
            SQL.AppendLine("	( ");
            SQL.AppendLine("		SELECT PayrunCode, EmpCode, Amt, (Salary+OT1Amt+OT2Amt+OTHolidayAmt+SalaryAdjustment+FixAllowance+VarAllowance+IncEmployee+TaxAllowance+SSEmployerHealth+SSEmployerEmployment) Brutto ");
            SQL.AppendLine("		FROM TblPayrollProcess1  ");
            SQL.AppendLine("		WHERE LEFT(PayrunCode, 6) = CONCAT(@Yr, @Mth) ");
            SQL.AppendLine("		AND (ResignDt IS NULL OR (ResignDt Is Not Null And substring(ResignDt, 1, 6)>CONCAT(@Yr, @Mth)))  ");
            SQL.AppendLine("		GROUP BY PayrunCode, EmpCode ");
            SQL.AppendLine("	) C ON A.PayrunCode = C.PayrunCode AND A.EmpCode = C.EmpCode ");
            	
            SQL.AppendLine("	Inner Join TblEmployee D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("	Left Join TblSite E On D.SiteCode=E.SiteCode ");
            SQL.AppendLine("	Left Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("	GROUP BY E.SiteCode, E.SiteName, F.DeptCode, F.DeptName ");
            SQL.AppendLine("	ORDER BY E.SiteName ");
            SQL.AppendLine(")T ");
            

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Site Code",
                    "Site",
                    "Department Code",
                    "Department",
                    "Jumlah orang" +Environment.NewLine+"bulan 1",  
                    
                    //6-10
                    "Jumlah orang" +Environment.NewLine+"bulan 2", 
                    "THP bulan 1",
                    "THP bulan 2",
                    "Bruto bulan 1",
                    "Bruto bulan 2",

                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    100, 200,100, 200, 100,   
                    
                    //6-10
                    100, 180, 180, 180, 180

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10 }, 0);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);

        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {

            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " where 0=0 ";
                var cm = new MySqlCommand();

                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + "  Order By SiteName; ",
                    new string[]
                    {
                        //0
                        "SiteCode",  

                        //1-5
                        "SiteName", "DeptCode", "DeptName", "TotalEmpCode1", "TotalEmpCode2",  

                        //6-9
                        "THP1", "THP2", "Brutto1", "Brutto2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1); 
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);

                    }, true, false, false, false
                );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] {  5, 6, 7, 8, 9, 10 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion 

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");

        }

      
        #endregion 
    }
}
