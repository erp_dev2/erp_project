﻿
namespace RunSystem
{
    partial class FrmRFQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRFQ));
            this.Tp3 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TmeStartTm = new DevExpress.XtraEditors.TimeEdit();
            this.TmeEndTm = new DevExpress.XtraEditors.TimeEdit();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtMaterialRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.MeeMaterialRequestRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LueProcurementType = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.BtnMaterialRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnMaterialRequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TmeStartTm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeEndTm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaterialRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeMaterialRequestRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Location = new System.Drawing.Point(0, 384);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 404);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Text = "  &Sent";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnMaterialRequestDocNo2);
            this.panel2.Controls.Add(this.BtnMaterialRequestDocNo);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.LueProcurementType);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.MeeMaterialRequestRemark);
            this.panel2.Controls.Add(this.TxtMaterialRequestDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.panel2.Size = new System.Drawing.Size(792, 89);
            // 
            // Tc1
            // 
            this.Tc1.Controls.Add(this.Tp3);
            this.Tc1.Margin = new System.Windows.Forms.Padding(2);
            this.Tc1.Size = new System.Drawing.Size(792, 337);
            this.Tc1.TabIndex = 15;
            this.Tc1.Controls.SetChildIndex(this.Tp3, 0);
            this.Tc1.Controls.SetChildIndex(this.Tp2, 0);
            this.Tc1.Controls.SetChildIndex(this.Tp1, 0);
            // 
            // Tp1
            // 
            this.Tp1.Margin = new System.Windows.Forms.Padding(2);
            this.Tp1.Padding = new System.Windows.Forms.Padding(2);
            this.Tp1.Size = new System.Drawing.Size(664, 357);
            this.Tp1.Text = "Sector";
            // 
            // Tp2
            // 
            this.Tp2.Controls.Add(this.panel4);
            this.Tp2.Margin = new System.Windows.Forms.Padding(2);
            this.Tp2.Padding = new System.Windows.Forms.Padding(2);
            this.Tp2.Size = new System.Drawing.Size(784, 310);
            this.Tp2.Text = "Vendor";
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.Height = 28;
            this.Grd1.Location = new System.Drawing.Point(2, 2);
            this.Grd1.Margin = new System.Windows.Forms.Padding(2);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(660, 353);
            this.Grd1.TabIndex = 16;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // Tp3
            // 
            this.Tp3.Controls.Add(this.panel5);
            this.Tp3.Location = new System.Drawing.Point(4, 23);
            this.Tp3.Margin = new System.Windows.Forms.Padding(2);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(784, 310);
            this.Tp3.TabIndex = 2;
            this.Tp3.Text = "Procurement Schedule";
            this.Tp3.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.TmeStartTm);
            this.panel5.Controls.Add(this.TmeEndTm);
            this.panel5.Controls.Add(this.DteEndDt);
            this.panel5.Controls.Add(this.DteStartDt);
            this.panel5.Controls.Add(this.Grd3);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(784, 310);
            this.panel5.TabIndex = 0;
            // 
            // TmeStartTm
            // 
            this.TmeStartTm.EditValue = null;
            this.TmeStartTm.EnterMoveNextControl = true;
            this.TmeStartTm.Location = new System.Drawing.Point(352, 58);
            this.TmeStartTm.Name = "TmeStartTm";
            this.TmeStartTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeStartTm.Properties.Appearance.Options.UseFont = true;
            this.TmeStartTm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeStartTm.Properties.Mask.EditMask = "HH:mm";
            this.TmeStartTm.Size = new System.Drawing.Size(112, 20);
            this.TmeStartTm.TabIndex = 62;
            this.TmeStartTm.Visible = false;
            this.TmeStartTm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TmeStartTm_KeyDown);
            this.TmeStartTm.Leave += new System.EventHandler(this.TmeStartTm_Leave);
            // 
            // TmeEndTm
            // 
            this.TmeEndTm.EditValue = null;
            this.TmeEndTm.EnterMoveNextControl = true;
            this.TmeEndTm.Location = new System.Drawing.Point(521, 58);
            this.TmeEndTm.Name = "TmeEndTm";
            this.TmeEndTm.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TmeEndTm.Properties.Appearance.Options.UseFont = true;
            this.TmeEndTm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TmeEndTm.Properties.Mask.EditMask = "HH:mm";
            this.TmeEndTm.Size = new System.Drawing.Size(112, 20);
            this.TmeEndTm.TabIndex = 63;
            this.TmeEndTm.Visible = false;
            this.TmeEndTm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TmeEndTm_KeyDown);
            this.TmeEndTm.Leave += new System.EventHandler(this.TmeEndTm_Leave);
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(421, 33);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(125, 20);
            this.DteEndDt.TabIndex = 61;
            this.DteEndDt.Visible = false;
            this.DteEndDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteEndDt_KeyDown);
            this.DteEndDt.Leave += new System.EventHandler(this.DteEndDt_Leave);
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(262, 33);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(125, 20);
            this.DteStartDt.TabIndex = 60;
            this.DteStartDt.Visible = false;
            this.DteStartDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStartDt_KeyDown);
            this.DteStartDt.Leave += new System.EventHandler(this.DteStartDt_Leave);
            // 
            // Grd3
            // 
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Margin = new System.Windows.Forms.Padding(2);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.Size = new System.Drawing.Size(784, 310);
            this.Grd3.TabIndex = 18;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.Grd2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(2, 2);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(780, 306);
            this.panel4.TabIndex = 0;
            // 
            // Grd2
            // 
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Margin = new System.Windows.Forms.Padding(2);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.Size = new System.Drawing.Size(780, 306);
            this.Grd2.TabIndex = 17;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TxtMaterialRequestDocNo
            // 
            this.TxtMaterialRequestDocNo.EnterMoveNextControl = true;
            this.TxtMaterialRequestDocNo.Location = new System.Drawing.Point(121, 2);
            this.TxtMaterialRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMaterialRequestDocNo.Name = "TxtMaterialRequestDocNo";
            this.TxtMaterialRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMaterialRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMaterialRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMaterialRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtMaterialRequestDocNo.Properties.MaxLength = 16;
            this.TxtMaterialRequestDocNo.Properties.ReadOnly = true;
            this.TxtMaterialRequestDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtMaterialRequestDocNo.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(83, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "MR#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 24);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 14);
            this.label5.TabIndex = 9;
            this.label5.Text = "MR\'s Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeMaterialRequestRemark
            // 
            this.MeeMaterialRequestRemark.EnterMoveNextControl = true;
            this.MeeMaterialRequestRemark.Location = new System.Drawing.Point(121, 22);
            this.MeeMaterialRequestRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeMaterialRequestRemark.Name = "MeeMaterialRequestRemark";
            this.MeeMaterialRequestRemark.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeMaterialRequestRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMaterialRequestRemark.Properties.Appearance.Options.UseBackColor = true;
            this.MeeMaterialRequestRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeMaterialRequestRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMaterialRequestRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeMaterialRequestRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMaterialRequestRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeMaterialRequestRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMaterialRequestRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeMaterialRequestRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMaterialRequestRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeMaterialRequestRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeMaterialRequestRemark.Properties.MaxLength = 2000;
            this.MeeMaterialRequestRemark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeMaterialRequestRemark.Properties.ReadOnly = true;
            this.MeeMaterialRequestRemark.Properties.ShowIcon = false;
            this.MeeMaterialRequestRemark.Size = new System.Drawing.Size(349, 22);
            this.MeeMaterialRequestRemark.TabIndex = 10;
            this.MeeMaterialRequestRemark.ToolTip = "F4 : Show/hide text";
            this.MeeMaterialRequestRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeMaterialRequestRemark.ToolTipTitle = "Run System";
            // 
            // LueProcurementType
            // 
            this.LueProcurementType.EnterMoveNextControl = true;
            this.LueProcurementType.Location = new System.Drawing.Point(121, 44);
            this.LueProcurementType.Margin = new System.Windows.Forms.Padding(5);
            this.LueProcurementType.Name = "LueProcurementType";
            this.LueProcurementType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.Appearance.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProcurementType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProcurementType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProcurementType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProcurementType.Properties.DropDownRows = 30;
            this.LueProcurementType.Properties.NullText = "[Empty]";
            this.LueProcurementType.Properties.PopupWidth = 300;
            this.LueProcurementType.Size = new System.Drawing.Size(167, 20);
            this.LueProcurementType.TabIndex = 12;
            this.LueProcurementType.ToolTip = "F4 : Show/hide list";
            this.LueProcurementType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProcurementType.EditValueChanged += new System.EventHandler(this.LueProcurementType_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(5, 45);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 14);
            this.label22.TabIndex = 11;
            this.label22.Text = "Procurement Type";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(82, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(121, 64);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(109, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // BtnMaterialRequestDocNo
            // 
            this.BtnMaterialRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMaterialRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMaterialRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMaterialRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMaterialRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnMaterialRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnMaterialRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnMaterialRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnMaterialRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMaterialRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMaterialRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnMaterialRequestDocNo.Image")));
            this.BtnMaterialRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMaterialRequestDocNo.Location = new System.Drawing.Point(288, 1);
            this.BtnMaterialRequestDocNo.Name = "BtnMaterialRequestDocNo";
            this.BtnMaterialRequestDocNo.Size = new System.Drawing.Size(22, 20);
            this.BtnMaterialRequestDocNo.TabIndex = 7;
            this.BtnMaterialRequestDocNo.ToolTip = "Find Cancellation PO Document";
            this.BtnMaterialRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMaterialRequestDocNo.ToolTipTitle = "Run System";
            this.BtnMaterialRequestDocNo.Click += new System.EventHandler(this.BtnMaterialRequestDocNo_Click);
            // 
            // BtnMaterialRequestDocNo2
            // 
            this.BtnMaterialRequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnMaterialRequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnMaterialRequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMaterialRequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnMaterialRequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnMaterialRequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnMaterialRequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnMaterialRequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnMaterialRequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnMaterialRequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnMaterialRequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnMaterialRequestDocNo2.Image")));
            this.BtnMaterialRequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnMaterialRequestDocNo2.Location = new System.Drawing.Point(309, 1);
            this.BtnMaterialRequestDocNo2.Name = "BtnMaterialRequestDocNo2";
            this.BtnMaterialRequestDocNo2.Size = new System.Drawing.Size(22, 20);
            this.BtnMaterialRequestDocNo2.TabIndex = 8;
            this.BtnMaterialRequestDocNo2.ToolTip = "Find Cancellation PO Document";
            this.BtnMaterialRequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnMaterialRequestDocNo2.ToolTipTitle = "Run System";
            this.BtnMaterialRequestDocNo2.Click += new System.EventHandler(this.BtnMaterialRequestDocNo2_Click);
            // 
            // FrmRFQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 426);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmRFQ";
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TmeStartTm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TmeEndTm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMaterialRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeMaterialRequestRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProcurementType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage Tp3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtMaterialRequestDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.MemoExEdit MeeMaterialRequestRemark;
        internal DevExpress.XtraEditors.LookUpEdit LueProcurementType;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        public DevExpress.XtraEditors.SimpleButton BtnMaterialRequestDocNo2;
        public DevExpress.XtraEditors.SimpleButton BtnMaterialRequestDocNo;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        internal DevExpress.XtraEditors.TimeEdit TmeStartTm;
        internal DevExpress.XtraEditors.TimeEdit TmeEndTm;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
    }
}