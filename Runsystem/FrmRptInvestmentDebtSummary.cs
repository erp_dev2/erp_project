﻿#region Update
/*
    17/06/2022 [SET/PRODUCT] Menu reporting baru 
    28/06/2022 [SET/PRODUCT] Penyesuaian SetSQL() source query
    08/07/2022 [SET/PRODUCT] data tidak muncul, merubah Qty -> NominalAmt di SetSQL()
*/
#endregion

#region Namespace
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;
using System.Text.RegularExpressions;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptInvestmentDebtSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private string[] mCols = null;
        private bool
            mIsStockSummaryShowOldDataNoFilter = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsStockSummaryShowZeroStock = false,
            mIsInvTrnShowItSpec = false,
            mIsStockSummaryHeatNumberEnabled = false;
        private string
            mBankAccountTypeForInvestment = string.Empty,
            mDebtInvestmentCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptInvestmentDebtSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDteCurrentDate(DteValuationDt);
                Sl.SetLueOption(ref LueInvestmentType, "InvestmentType");
                SetLueBankAcCode(ref LueBankAcCode);
                Sm.SetControlNumValueZero(new List<TextEdit> { TxtTotalGainLoss }, 0);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsStockSummaryShowOldDataNoFilter = Sm.GetParameterBoo("IsStockSummaryShowOldDataNoFilter");
            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsStockSummaryShowZeroStock = Sm.GetParameterBoo("IsStockSummaryShowZeroStock");
            mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            mIsStockSummaryHeatNumberEnabled = Sm.GetParameterBoo("IsStockSummaryHeatNumberEnabled");
            mBankAccountTypeForInvestment = Sm.GetParameter("BankAccountTypeForInvestment");
            mDebtInvestmentCtCode = Sm.GetParameter("DebtInvestmentCtCode");
        }

        private string SetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select T1.PortofolioName, T1.PortofolioId, T1.BankAcNo, T1.BankAcNm, T1.Issuer, T1.InvestmentCtCode, T1.InvestmentCtName, ");
            SQL.AppendLine("    T1.InvestmentTypeNm, IfNull(T1.NominalAmt, 0.00) As NominalAmount, 0.00 AS MovAvgPrice, 0.00 AS MovAvgCost, 0.00 AS RecordedMarketPrice, ");
            SQL.AppendLine("    0.00 AS RecordedMarketValue, 0.00 AS RecordedUnrealized, T1.MaturityDt, T1.InterestRateAmt AS InterestRate, T1.InterestFreq, ");
            SQL.AppendLine("    T1.InterestType, T1.LastCouponDt, T1.NextCouponDt, T1.AccruedInterest, T1.CreateDt, T1.InvestmentCode, T1.InvestmentType, T1.BankAcCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("    	Select C.PortofolioID, D.PortofolioName, A.InvestmentCode, F.BankAcNo, F.BankAcNm, D.Issuer, E.InvestmentCtCode, E.InvestmentCtName, ");
            SQL.AppendLine("    	G.OptDesc As InvestmentTypeNm, Sum(IfNull(A.NominalAmt, 0.00)) As NominalAmt, B.InvestmentType, F.BankAcCode, ");
            SQL.AppendLine("    	C.MaturityDt, C.InterestRateAmt, C.InterestFreq, C.InterestType, C.LastCouponDt, C.NextCouponDt, I.AccruedInterest, ");
            SQL.AppendLine("    	A.CreateDt ");
            SQL.AppendLine("    	From TblInvestmentStockSummary A ");
            SQL.AppendLine("    	Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
            SQL.AppendLine("    		And A.InvestmentCode = B.InvestmentCode ");
            SQL.AppendLine("    	Inner Join TblInvestmentItemDebt C On A.InvestmentCode = C.InvestmentDebtCode ");
            SQL.AppendLine("    	Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            //SQL.AppendLine("    		And D.InvestmentCtCode = @EquityInvestmentCtCode ");
            SQL.AppendLine("    		And FIND_IN_SET(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("    	Inner Join TblInvestmentCategory E On D.InvestmentCtCode = E.InvestmentCtCode ");
            SQL.AppendLine("    	Inner Join TblBankAccount F On A.BankAcCode = F.BankAcCode ");
            SQL.AppendLine("    	Inner Join TblOption G On G.OptCat = 'InvestmentType' And B.InvestmentType = G.OptCode ");
            SQL.AppendLine("    	Inner Join TblInvestmentStockPrice H On A.Source = H.Source ");
            SQL.AppendLine("    	LEFT JOIN tblAcquisitionDebtSecuritiesHdr I ON A.InvestmentCode = I.InvestmentDebtCode AND A.BankAcCode = I.BankAcCode AND A.Source = I.Source ");
            //SQL.AppendLine("    	INNER JOIN tblinvestmentitemdebt J ON I.InvestmentDebtCode = J.InvestmentDebtCode AND I.InvestmentCode = J.PortofolioId ");
            SQL.AppendLine("    	Where B.DocDt <= @ValuationDt ");
            SQL.AppendLine("    	Group By A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, F.BankAcCode ");
            SQL.AppendLine("        Having Sum(A.NominalAmt)<>0.00");
            SQL.AppendLine("    )T1 ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("    	Select A.InvestmentCode, D.InvestmentCtCode, B.InvestmentType, ");
            SQL.AppendLine("    	(Sum((IfNull(A.NominalAmt, 0.00) * IfNull(F.UPrice, 0.00)*0.01))) / (Sum(IfNull(A.NominalAmt, 0.00))) As MovingAvgPrice ");
            SQL.AppendLine("    	From TblInvestmentStockSummary A ");
            SQL.AppendLine("    	Inner Join TblInvestmentStockMovement B On A.Source = B.Source ");
            SQL.AppendLine("    		And A.InvestmentCode = B.InvestmentCode ");
            SQL.AppendLine("    	Inner Join TblInvestmentItemDebt C On A.InvestmentCode = C.InvestmentDebtCode ");
            SQL.AppendLine("    	Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            //SQL.AppendLine("    		And D.InvestmentCtCode = @EquityInvestmentCtCode ");
            SQL.AppendLine("    		And FIND_IN_SET(D.InvestmentCtCode, @DebtInvestmentCtCode) ");
            SQL.AppendLine("    		Inner Join TblOption E On E.OptCat = 'InvestmentType' And B.InvestmentType = E.OptCode ");
            SQL.AppendLine("    	Inner Join TblInvestmentStockPrice F On A.Source = F.Source ");
            SQL.AppendLine("    	Where B.DocDt <= @ValuationDt ");
            SQL.AppendLine("    	Group By D.PortofolioId, D.InvestmentCtCode, B.InvestmentType ");
            SQL.AppendLine("    )T2 On T1.InvestmentCode = T2.InvestmentCode ");
            SQL.AppendLine("    And T1.InvestmentCtCode = T2.InvestmentCtCode ");
            SQL.AppendLine("    And T1.InvestmentType = T2.InvestmentType ");
            //SQL.AppendLine("    Left Join ( ");
            //SQL.AppendLine("    	Select B.InvestmentCode, B.InvestmentType, B.BankAcCode, D.InvestmentCtCode, B.MarketPrice ");
            //SQL.AppendLine("    	From TblInvestmentPortofolioClosingHdr A ");
            //SQL.AppendLine("    	Inner Join TblInvestmentPortofolioClosingDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    	Inner Join TblInvestmentItemEquity C On B.InvestmentCode = C.InvestmentEquityCode ");
            //SQL.AppendLine("    	Inner Join TblInvestmentPortofolio D On C.PortofolioId = D.PortofolioId ");
            //SQL.AppendLine("    	Where A.CancelInd = 'N' ");
            //SQL.AppendLine("    	And A.ClosingDt = @ValuationDt ");
            //SQL.AppendLine("    )T3 On T1.InvestmentCode = T3.InvestmentCode ");
            //SQL.AppendLine("    And T1.InvestmentCtCode = T3.InvestmentCtCode ");
            //SQL.AppendLine("    And T1.InvestmentType = T3.InvestmentType ");
            //SQL.AppendLine("    And T1.BankAcCode = T3.BankAcCode ");
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine(Filter);

            return SQL.ToString();
        }

        private string SetSQL1(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary */ ");
            SQL.AppendLine("Select A.WhsCode, D.SekuritasName as WhsName, A.Lot, A.Bin, A.InvestmentCode, C.InvestmentCodeInternal, C.InvestmentName, C.ForeignName, ");
            SQL.AppendLine("0 As AvailableStock, A.Qty, C.InventoryUomCode ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine(", A.Qty2, C.InventoryUomCode2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine(", A.Qty3, C.InventoryUomCode3 ");
            SQL.AppendLine(",C.ItGrpCOde, null as ItGrpName, C.ActInd, null as ItScName, C.Specification, A.WhsCode, A.Uprice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.WhsCode, T1.Lot, T1.Bin, T1.InvestmentCode, T2.Uprice, ");
            SQL.AppendLine("    Sum(T1.Qty) As Qty ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , Sum(T1.Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , Sum(T1.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblInvestmentStockSummary T1 ");
            SQL.AppendLine("    Left JOIN tblInvestmentstockprice T2 ON T1.Source = T2.Source And T1.InvestmentCode = T2.InvestmentCode ");
            SQL.AppendLine("    Where 0 = 0 ");
            //if (!ChkZeroStock.Checked) SQL.AppendLine("    And (T1.Qty<>0) ");
            SQL.AppendLine("     " + Filter.Replace("X.", "T1."));
            SQL.AppendLine("    Group By T1.WhsCode, T1.Lot, T1.Bin, T1.InvestmentCode ");         
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblInvestmentItem C On A.InvestmentCode = C.InvestmentCode " + Filter2.Replace("X.", "C."));
            SQL.AppendLine("Inner Join TblInvestmentSekuritas D On A.WhsCode = D.SekuritasCode ");
            SQL.AppendLine("Order By A.WhsCode, C.InvestmentName; ");

            return SQL.ToString();
        }

        private string SetSQL2(string DocDt, string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary 2 */ ");
            SQL.AppendLine("Select A.WhsCode, D.SekuritasName as WhsName, A.Lot, A.Bin, A.InvestmentCode, C.InvestmentCodeInternal, C.InvestmentName, C.ForeignName, ");
            SQL.AppendLine("0 As AvailableStock, A.Qty, C.InventoryUomCode ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine(", A.Qty2, C.InventoryUomCode2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine(", A.Qty3, C.InventoryUomCode3 ");
            SQL.AppendLine(", C.ItGrpCode, null as ItGrpName, C.ActInd, null as ItScName, C.Specification, A.WhsCode, A.Uprice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.WhsCode, T1.Lot, T1.Bin, T1.InvestmentCode, T3.Uprice ");
            SQL.AppendLine("    Sum(T1.Qty) As Qty  ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , Sum(T1.Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , Sum(T1.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblInvestmentStockMovement T1 ");
            SQL.AppendLine("    Inner Join TblInvestmentItem T2 On T1.InvestmentCode=T2.InvestmentCode " + Filter2.Replace("X.", "T2."));
            SQL.AppendLine("    Left JOIN tblInvestmentstockprice T3 ON T1.Source = T3.Source And T1.InvestmentCode = T3.InvestmentCode ");
            SQL.AppendLine("    Where T1.DocDt<=" + DocDt);
            //if (!ChkZeroStock.Checked) SQL.AppendLine("    And (T1.Qty<>0) ");
            SQL.AppendLine("     " + Filter.Replace("X.", "T1."));
            SQL.AppendLine("Group By T1.WhsCode, T1.Lot, T1.Bin, T1.InvestmentCode ");
            SQL.AppendLine(") A ");
            //if (mIsStockSummaryShowZeroStock && ChkZeroStock.Checked) 
                SQL.AppendLine("And (A.Qty = 0 Or A.Qty > 0) ");
            //else 
                SQL.AppendLine("And A.Qty>0 ");
            
            SQL.AppendLine("Inner Join TblInvestmentItem C On A.InvestmentCode= C.InvestmentCode " + Filter2.Replace("X.", "C."));
            SQL.AppendLine("Inner Join TblInvestmentSekuritas D On A.WhsCode = D.SekuritasCode ");

            SQL.AppendLine("Order By A.WhsCode, C.InvestmentName; ");
            
            return SQL.ToString();
        }

        private string SetSQL3(string Filter, string Filter2, string Filter3)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Summary 3 */ ");

            SQL.AppendLine("Select T1.WhsCode, null as WhsName, T1.Lot, T1.Bin, T1.InvestmentCode, T5.InvestmentCodeInternal, T5.InvestmentName, T5.ForeignName, ");
            SQL.AppendLine("IfNull(T2.Qty, 0)-IfNull(T3.Qty2, 0)+IfNull(T4.Qty3, 0) As AvailableStock, ");
            SQL.AppendLine("IfNull(T2.Qty, 0) As Qty, T5.InventoryUomCode  ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , T2.Qty2, T5.InventoryUomCode2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , T2.Qty3, T5.InventoryUomCode3 ");
            SQL.AppendLine(", T5.ItGrpCOde, null as ItGrpName, T5.ActInd, T8.ItScName, T5.Specification, T6.WhsCode, 0 as Uprice ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Distinct WhsCode, Lot, Bin, InvestmentCode From ( ");
	        SQL.AppendLine("    Select Distinct A.WhsCode, A.Lot, A.Bin, A.InvestmentCode  ");
	        SQL.AppendLine("    From TblInvestmentStockSummary A ");
	        SQL.AppendLine("    Inner Join TblInvestmentItem B On A.InvestmentCode=B.InvestmentCode " + Filter.Replace("X.", "B."));
            SQL.AppendLine("    Where (A.Qty>0 ");
            SQL.AppendLine("    ) " + Filter2.Replace("X.", "A.") + Filter3.Replace("X.", "A."));
	        SQL.AppendLine("    Union All ");
	        SQL.AppendLine("    Select Distinct A.WhsCode, B.Lot, B.Bin, B.InvestmentCode ");
	        SQL.AppendLine("    From TblDOWhsHdr A ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("    Inner Join TblInvestmentItem C On B.InvestmentCode=C.InvestmentCode " + Filter.Replace("X.", "C."));
          
            SQL.AppendLine("    Where 0=0 " + Filter2.Replace("X.", "A."));
	        SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select WhsCode, Lot, Bin, InvestmentCode From (");
	        SQL.AppendLine("        Select Distinct A.WhsCode2 As WhsCode, B.Lot, B.Bin, B.InvestmentCode ");
	        SQL.AppendLine("        From TblDOWhsHdr A ");
            SQL.AppendLine("        Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("        Inner Join TblInvestmentItem C On B.InvestmentCode=C.InvestmentCode " + Filter.Replace("X.", "C."));
            SQL.AppendLine("    ) T Where 0=0 " + Filter2.Replace("X.", "T."));
            SQL.AppendLine(") X ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.InvestmentCode, Sum(A.Qty) As Qty ");
            if (mNumberOfInventoryUomCode >= 2) SQL.AppendLine("    , Sum(A.Qty2) As Qty2 ");
            if (mNumberOfInventoryUomCode >= 3) SQL.AppendLine("    , Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblInvestmentStockSummary A ");
            SQL.AppendLine("    Inner Join TblInvestmentItem B On A.InvestmentCode=B.InvestmentCode " + Filter.Replace("X.", "B."));
            SQL.AppendLine("    Where (A.Qty>0 ");
            SQL.AppendLine("    ) " + Filter2.Replace("X.", "A.") + Filter3.Replace("X.", "A."));
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode ");
            SQL.AppendLine(") T2 On T1.WhsCode=T2.WhsCode And T1.Lot=T2.Lot And T1.Bin=T2.Bin And T1.InvestmentCode=T2.InvestmentCode ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select A.WhsCode, B.Lot, B.Bin, B.InvestmentCode, Sum(B.Qty) As Qty2 ");
	        SQL.AppendLine("    From TblDOWhsHdr A ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("    Inner Join TblInvestmentItem C On B.InvestmentCode=C.InvestmentCode " + Filter.Replace("X.", "C."));
            SQL.AppendLine("    Where 0=0 " + Filter2.Replace("X.", "A."));
            SQL.AppendLine("    Group By A.WhsCode, B.Lot, B.Bin, B.InvestmentCode ");
            SQL.AppendLine(") T3 On T1.WhsCode=T3.WhsCode And T1.Lot=T3.Lot And T1.Bin=T3.Bin And T1.InvestmentCode=T3.InvestmentCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select WhsCode, Lot, Bin, InvestmentCode, Qty3 From (");
	        SQL.AppendLine("        Select A.WhsCode2 As WhsCode, B.Lot, B.Bin, B.InvestmentCode, Sum(B.Qty) As Qty3 ");
	        SQL.AppendLine("        From TblDOWhsHdr A ");
            SQL.AppendLine("        Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' " + Filter3.Replace("X.", "B."));
            SQL.AppendLine("        Inner Join TblItem C On B.InvestmentCode=C.InvestmentCode " + Filter.Replace("X.", "C."));
            SQL.AppendLine("        Group By A.WhsCode, B.Lot, B.Bin, B.InvestmentCode ");
            SQL.AppendLine("    ) T Where 0=0 " + Filter2.Replace("X.", "T."));
            SQL.AppendLine(") T4 On T1.WhsCode=T4.WhsCode And T1.Lot=T4.Lot And T1.Bin=T4.Bin And T1.InvestmentCode=T4.InvestmentCode ");
            SQL.AppendLine("Inner Join TblInvestmentItem T5 On T1.InvestmentCode=T5.InvestmentCode ");
            SQL.AppendLine("Where (IfNull(T2.Qty, 0)-IfNull(T3.Qty2, 0)+IfNull(T4.Qty3, 0)>0 ");
            SQL.AppendLine(" Or IfNull(T2.Qty, 0)>0 ");
            SQL.AppendLine(") Order By T1.WhsCode, T5.InvestmentName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment's"+Environment.NewLine+"Name",
                        "Investment's"+Environment.NewLine+"Code",
                        "Investment's" + Environment.NewLine + "Bank Account (RDN)#",
                        "Investment's" + Environment.NewLine + "Bank Account Name",
                        "Issuer", 

                        //6-10                        
                        "Category Code", 
                        "Category",
                        "Type",
                        "Nominal Amount",
                        "Moving Average"+Environment.NewLine+"Price",
                        
                        //11-15
                        "Moving Average"+Environment.NewLine+"Cost",
                        "Recorded Market"+Environment.NewLine+"Price",
                        "Recorded Market"+Environment.NewLine+"Value",
                        "Recorded Market"+Environment.NewLine+"Unrealized Gain/Loss",
                        "Maturity Date",

                        //16-20
                        "Interest Rate",
                        "Interest Frequency",
                        "Interest Type",
                        "Last Coupon Date", 
                        "Next Coupon Date",

                        //21-24
                        "Accrued Interest",
                        "CreateDt",
                        "InvestmentDetbCode",
                        "InvestmentCost"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 120, 200, 250, 250,  
                        
                        //6-10
                        80, 120, 200, 130, 150,
                        
                        //11-15
                        130, 130, 130, 130, 130,

                        //16-20
                        100, 100, 100, 100, 100,

                        //21-24
                        100, 100, 100, 100
                    }
                );

            Sm.GrdFormatDate(Grd1, new int[] { 15, 19, 20, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 16, 21, 24 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 22, 23, 24 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6/*, 22, 23, 24*/ }, !ChkHideInfoInGrd.Checked);
        }

        private void SetCols()
        {
            if (mNumberOfInventoryUomCode == 1)
                mCols = new string[] { 
                    //0
                    "WhsName", 

                    //1-5
                    "Lot", "Bin", "InvestmentCode", "InvestmentCodeInternal", "InvestmentName",

                    //6-10
                    "ForeignName", "AvailableStock", "Qty", "InventoryUomCode", "ItGrpCode",
                    
                    //11-15
                    "ItGrpName", "ActInd", "ItScName", "Specification", "WhsCode", "Uprice"
                };

            if (mNumberOfInventoryUomCode == 2)
                mCols = new string[] { 
                    //0
                    "WhsName", 
                    
                    //1-5
                    "Lot", "Bin", "InvestmentCode", "InvestmentCodeInternal", "InvestmentName",
                    
                    //6-10
                    "ForeignName", "AvailableStock", "Qty", "InventoryUomCode", "Qty2",
                    
                    //11-15
                    "InventoryUOMCode2", "ItGrpCode", "ItGrpName", "ActInd",  "ItScName",

                    //16-17
                    "Specification", "WhsCode", "Uprice"
                };

            if (mNumberOfInventoryUomCode == 3)
                mCols = new string[] { 
                    //0
                    "WhsName", 
                    
                    //1-5
                    "Lot", "Bin", "InvestmentCode", "InvestmentCodeInternal", "InvestmentName",
                    
                    //6-10
                    "ForeignName", "AvailableStock", "Qty", "InventoryUomCode", "Qty2",
                    
                    //11-15
                    "InventoryUOMCode2", "Qty3", "InventoryUOMCode3", "ItGrpCode", "ItGrpName",
                    
                    //16-19
                    "ActInd", "ItScName", "Specification", "WhsCode", "Uprice"
                };
        }

        override protected void ShowData()
        {
            if (Sm.IsDteEmpty(DteValuationDt, "Valuation Date")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();
                string Filter = string.Empty;

                Sm.CmParamDt(ref cm, "@ValuationDt", Sm.GetDte(DteValuationDt));
                Sm.CmParam<String>(ref cm, "@DebtInvestmentCtCode", mDebtInvestmentCtCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueInvestmentType), "Tbl.InvestmentType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBankAcCode), "Tbl.BankAcCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtInvestmentCode.Text, new string[] { "Tbl.PortofolioId", "Tbl.PortofolioName" });
                
                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SetSQL(Filter),
                new string[]
                {
                    //0
                    "PortofolioName",
                    //1-5
                    "PortofolioId", "BankAcNo", "BankAcNm", "Issuer", "InvestmentCtCode",
                    //6-10
                    "InvestmentCtName", "InvestmentTypeNm", "NominalAmount", "MovAvgPrice", "MovAvgCost",
                    //11-15
                    "RecordedMarketPrice", "RecordedMarketValue", "RecordedUnrealized", "MaturityDt", "InterestRate", 
                    //16-20
                    "InterestFreq", "InterestType", "LastCouponDt", "NextCouponDt", "AccruedInterest",
                    //21-22
                    "CreateDt", "InvestmentCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Grd.Cells[Row, 24].Value = 0m;
                        //Grd.Cells[Row, 14].Value = 0m;
                        //Grd.Cells[Row, 15].Value = 0m;
                    }, true, false, false, false
                );

                ComputeStockAccumulaton(Sm.GetDte(DteValuationDt), mDebtInvestmentCtCode, TxtInvestmentCode.Text, ref Grd1, 22, 23, 6, 8, 9, 24, 10);
                //ComputeRecordedValue();
                ComputeTotalInterestReceivable();
                ComputeMovingAvgCost();
                ComputeRecordedMarketValue();
                //ComputeTotalGainLoss();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] {  });
        }

        #endregion

        #region Additional Methods

        private void ComputeRecordedMarketValue()
        {
            decimal NominalAmt = 0m, RecordedMarketPrice = 0m ;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                NominalAmt = Sm.GetGrdDec(Grd1, r, 9);
                RecordedMarketPrice = Sm.GetGrdDec(Grd1, r, 12);

                Grd1.Cells[r, 13].Value = NominalAmt * RecordedMarketPrice;
            }

        }
        
        private void ComputeMovingAvgCost()
        {
            decimal NominalAmt = 0m, MovAvgPrice = 0m ;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                NominalAmt = Sm.GetGrdDec(Grd1, r, 9);
                MovAvgPrice = Sm.GetGrdDec(Grd1, r, 10);

                Grd1.Cells[r, 11].Value = NominalAmt * MovAvgPrice;
            }

        }

        private void ComputeTotalInterestReceivable()
        {
            decimal TotalInterest = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                TotalInterest += Sm.GetGrdDec(Grd1, r, 21);
            }

            TxtTotalInterestReceivable.Text = Convert.ToString(TotalInterest);
            Sm.FormatNumTxt(TxtTotalInterestReceivable, 0);
        }
        
        private void ComputeTotalGainLoss()
        {
            decimal TotalGainLoss = 0m, RecordedMarketValue = 0m, MovingAverageCost = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                RecordedMarketValue = Sm.GetGrdDec(Grd1, r, 13);
                MovingAverageCost = Sm.GetGrdDec(Grd1, r, 11);

                Grd1.Cells[r, 14].Value = RecordedMarketValue - MovingAverageCost;

                TotalGainLoss += Sm.GetGrdDec(Grd1, r, 14);
            }

            TxtTotalGainLoss.Text = Convert.ToString(TotalGainLoss);
            Sm.FormatNumTxt(TxtTotalGainLoss, 0);
        }

        private void ComputeStockAccumulaton(
            string DocDt, string InvestmentCtCode, string InvestmentCode,
            ref iGrid Grd, int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColNominalAmt, int ColInvestmentCost, int ColMovingAvgPrice
            )
        {
            var lacc = PrepDataStockAccumulation(DocDt, InvestmentCtCode, InvestmentCode);
            ProcessAccumulationStock(ref Grd, ref lacc, ColCreateDt, ColInvCode, ColInvCtCode, ColInvType, ColNominalAmt, ColInvestmentCost, ColMovingAvgPrice);
        }

        private List<StockAccumulation> PrepDataStockAccumulation(string DocDt, string InvestmentCtCode, string InvestmentCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<StockAccumulation>();
            var l2 = new List<StockAccumulation>();
            string Filter = " ";

            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@InvestmentCtCode", InvestmentCtCode);
            Sm.FilterStr(ref Filter, ref cm, InvestmentCode, new string[] { "C.PortofolioId", "C.PortofolioName" });

            SQL.AppendLine("Select A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType, E.OptDesc,  ");
            SQL.AppendLine("IfNull(A.Qty, 0.00) As NominalAmt, IfNull(A.Qty, 0.00) * (IfNull(D.UPrice, 0.00) * 0.01) As InvestmentCost  ");
            SQL.AppendLine("From TblInvestmentStockMovement A  ");
            SQL.AppendLine("Inner Join TblInvestmentItemDebt B On A.InvestmentCode = B.InvestmentDebtCode  ");
            SQL.AppendLine("Inner Join TblInvestmentPortofolio C On B.PortofolioId = C.PortofolioId ");
            SQL.AppendLine("Inner Join TblInvestmentStockPrice D On A.Source = D.Source ");
            SQL.AppendLine("	And A.InvestmentCode = D.InvestmentCode ");
            SQL.AppendLine("Inner Join TblOption E On A.InvestmentType = E.OptCode And E.OptCat = 'InvestmentType' ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            if (DocDt.Length > 0) SQL.AppendLine("And A.DocDt <= @DocDt  ");
            SQL.AppendLine("And FIND_IN_SET(C.InvestmentCtCode, @InvestmentCtCode) ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("	Select Distinct(X1.DocNo) ");
            SQL.AppendLine("	From TblInvestmentStockMovement X1 ");
            SQL.AppendLine("	Where 1=1 ");
            if (DocDt.Length > 0) SQL.AppendLine("	And X1.DocDt <= @DocDt ");
            SQL.AppendLine("	Group By X1.DocType, X1.InvestmentType, X1.DocNo, X1.DNo, X1.DocDt, X1.BankAcCode, X1.Lot, X1.BatchNo, X1.Source   ");
            SQL.AppendLine("	Having Sum(X1.Qty)=0.00 ");
            SQL.AppendLine(") ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString() + Filter + "Order By A.CreateDt, A.InvestmentCode, C.InvestmentCtCode, A.InvestmentType;",
                new string[]
                {
                    "CreateDt",
                    "InvestmentCode", "InvestmentCtCode", "InvestmentType", "OptDesc", "NominalAmt",
                    "InvestmentCost",
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new StockAccumulation()
                        {
                            CreateDt = Convert.ToDecimal(Sm.DrStr(dr, 0)),
                            InvestmentCode = Sm.DrStr(dr, 1),
                            InvestmentCtCode = Sm.DrStr(dr, 2),
                            InvestmentType = Sm.DrStr(dr, 3),
                            InvestmentTypeName = Sm.DrStr(dr, 4),
                            NominalAmt = Sm.DrDec(dr, 5),
                            InvestmentCost = Sm.DrDec(dr, 6),
                        }
                    );
               }, false
            );

            var lg = l.GroupBy(g => new { CreateDt = g.CreateDt, InvCode = g.InvestmentCode, InvCtCode = g.InvestmentCtCode, InvType = g.InvestmentType, InvTypeNm = g.InvestmentTypeName })
                .Select(group => new
                {
                    CreateDt = group.Key.CreateDt,
                    InvestmentCode = group.Key.InvCode,
                    InvestmentCtCode = group.Key.InvCtCode,
                    InvestmentType = group.Key.InvType,
                    InvestmentTypeName = group.Key.InvTypeNm,
                });

            foreach (var x in lg)
            {
                decimal mNominalAmt = 0m, mInvestmentCost = 0m;
                foreach (var y in l)
                {
                    if (y.CreateDt <= x.CreateDt &&
                        y.InvestmentCode == x.InvestmentCode &&
                        y.InvestmentCtCode == x.InvestmentCtCode &&
                        y.InvestmentType == x.InvestmentType)
                    {
                        mNominalAmt += y.NominalAmt;
                        mInvestmentCost += y.InvestmentCost;
                    }
                }
                l2.Add(
                    new StockAccumulation()
                    {
                        CreateDt2 = x.CreateDt.ToString(),
                        InvestmentCode = x.InvestmentCode,
                        InvestmentCtCode = x.InvestmentCtCode,
                        InvestmentType = x.InvestmentType,
                        InvestmentTypeName = x.InvestmentTypeName,
                        NominalAmt = mNominalAmt,
                        InvestmentCost = mInvestmentCost,
                    }
                );
            }

            return l2;
        }

        private void ProcessAccumulationStock(
            ref iGrid Grd, ref List<StockAccumulation> lacc,
            int ColCreateDt, int ColInvCode, int ColInvCtCode, int ColInvType,
            int ColNominalAmt, int ColInvestmentCost, int ColMovingAvgPrice
            )
        {
            foreach (var x in lacc)
            {
                for (int row = 0; row < Grd.Rows.Count; row++)
                {
                    if (Sm.GetGrdStr(Grd, row, 0).Length > 0 &&
                        Sm.GetGrdStr(Grd, row, ColCreateDt) == x.CreateDt2 &&
                        Sm.GetGrdStr(Grd, row, ColInvCode) == x.InvestmentCode &&
                        Sm.GetGrdStr(Grd, row, ColInvCtCode) == x.InvestmentCtCode &&
                        Sm.GetGrdStr(Grd, row, ColInvType) == x.InvestmentTypeName)
                    {
                        Grd.Cells[row, ColNominalAmt].Value = x.NominalAmt;
                        Grd.Cells[row, ColInvestmentCost].Value = x.InvestmentCost;
                        Grd.Cells[row, ColMovingAvgPrice].Value = (x.NominalAmt == 0 ? 0 : x.InvestmentCost / x.NominalAmt);
                    }
                }
            }
        }

        private void ComputeRecordedValue()
        {
            decimal mTotalUnrealizedGL = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    Grd1.Cells[r, 12].Value = Sm.GetGrdDec(Grd1, r, 9) * Sm.GetGrdDec(Grd1, r, 11);
                    Grd1.Cells[r, 14].Value = Sm.GetGrdDec(Grd1, r, 9) * Sm.GetGrdDec(Grd1, r, 13);
                    Grd1.Cells[r, 15].Value = Sm.GetGrdDec(Grd1, r, 14) - Sm.GetGrdDec(Grd1, r, 12);
                    mTotalUnrealizedGL += Sm.GetGrdDec(Grd1, r, 14) - Sm.GetGrdDec(Grd1, r, 12);
                }
            }

            TxtTotalGainLoss.EditValue = Sm.FormatNum(mTotalUnrealizedGL, 0);
        }

        private void SetLueBankAcCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (Sm.GetParameter("BankAccountFormat") == "1")
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }
            else
            {
                SQL.AppendLine("Select A.BankAcCode As Col1, ");
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
                SQL.AppendLine("From TblBankAccount A ");
                SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            }
            SQL.AppendLine("Where Find_In_Set(A.BankAcTp, @BankAccountTypeForInvestment) ");
            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@BankAccountTypeForInvestment", mBankAccountTypeForInvestment);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void FilterStr(ref string SQL, ref MySqlCommand cm, string Filter, string Column, string Param)
        {
            if (!string.IsNullOrEmpty(Filter))
            {
                string pattern = @"(""[^""]+""|\w+)\s*", SQL2 = "";
                MatchCollection mc = null;
                var group = new List<string>();
                int Index = 0;

                string Column2 = Sm.Right(Column, Column.Length - 1 - Column.IndexOf("."));

                if (Filter.IndexOf(@"""") < 0) Filter = @"""" + Filter + @"""";

                mc = Regex.Matches(Filter, pattern);

                group.Clear();

                foreach (Match m in mc)
                    group.Add(m.Groups[0].Value.Replace(@"""", "").Trim());

                Index = 0;
                foreach (string s in group)
                {
                    if (s.Length != 0)
                    {
                        Index += 1;
                        SQL2 += (SQL2.Length == 0 ? "" : " Or ") + "Upper(" + Column + ") Like @" + Column2 + Param + Index.ToString();
                        //Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), "%" + s + "%");
                        Sm.CmParam<String>(ref cm, "@" + (Column.IndexOf(".") == -1 ? Column : Column2) + Param + Index.ToString(), s);
                    }
                }
                if (SQL2.Length != 0) SQL += ((SQL.Length == 0) ? " Where (" : " And (") + SQL2 + ") ";
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            int c = e.ColIndex;
            if (Sm.IsGrdColSelected(new int[] { 8, 9, 11, 13 }, c))
            {
                decimal Total = 0m;
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, c).Length != 0) Total += Sm.GetGrdDec(Grd1, r, c);
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Misc Control Method

        internal void SetLueInvestmentCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.InvestmentCtCode As Col1, T.InvestmentCtName As Col2 From TblInvestmentCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.InvestmentCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
            }
            SQL.AppendLine("Order By T.InvestmentCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }
        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkInvestmentName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment Name");
        }

        private void TxtInvestmentName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueInvestmentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueInvestmentType, new Sm.RefreshLue2(Sl.SetLueOption), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkInvestmentType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Invesment Type");
        }

        private void TxtInvestmentCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestmentCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment Code");
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(SetLueBankAcCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Investment Bank Account");
        }
        #endregion

        #region Class

        private class StockAccumulation
        {
            public decimal CreateDt { get; set; }
            public string CreateDt2 { get; set; }
            public string InvestmentCode { get; set; }
            public string InvestmentCtCode { get; set; }
            public string InvestmentType { get; set; }
            public string InvestmentTypeName { get; set; }
            public decimal NominalAmt { get; set; }
            public decimal InvestmentCost { get; set; }
        }

        #endregion
    }
}
