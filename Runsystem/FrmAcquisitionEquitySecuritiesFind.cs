﻿#region Update

/*
 * 11/04/2022 [SET/PRODUCT] Menu find baru
 * 17/05/2022 [SET/PRODUCT] Feedback merubah & tambah source Investment Code -> PortofolioId
 * 19/05/2022 [IBL/PRODUCT] Mengganti source InvestmentEquityCode menjadi InvestmentCode
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionEquitySecuritiesFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmAcquisitionEquitySecurities mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAcquisitionEquitySecuritiesFind(FrmAcquisitionEquitySecurities FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        protected override void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            SetGrd();
            SetSQL();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Cancel",
                        "Financial Institution",
                        "Investmnent Bank Account (RDN)",

                        //6-10
                        "Trade Date",
                        "Settlement Date",
                        "Status",
                        "Remark",
                        "Investment Name",

                        //11-15
                        "Investment Code",
                        "Type",
                        "Category",
                        "Quantity",
                        "Base Price",

                        //16-20
                        "Investment Cost",
                        "Total Cost",
                        "Acquisition Price",
                        "Created By",
                        "Created Date",

                        //21-24
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 100, 50, 150, 150,  

                        //6-10
                        100, 100, 100, 100, 150,

                        //11-15
                        150, 150, 150, 150, 150,
                        
                        //16-20
                        150, 150, 150, 100, 100, 

                        //21-24
                        100, 100, 100, 100,
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 6, 7, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 16, 17, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 7, 8, 9, 19, 20, 21, 22, 23, 24 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.CancelInd, B.SekuritasName, C.BankAcNm, A.TradeDt, A.SettlementDt, A.Status, A.Remark, ");
            //test
            SQL.AppendLine("F.PortofolioName InvestmentName, E.PortofolioId InvestmentCode, H.Optdesc, G.InvestmentCtName, D.Qty, D.BasePrice, D.InvestmentCost, D.TotalCost, D.AcquisitionPrice, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("FROM tblacquisitionequitysecuritieshdr A ");
            SQL.AppendLine("INNER JOIN tblinvestmentsekuritas B ON A.SekuritasCode = B.SekuritasCode ");
            SQL.AppendLine("INNER JOIN tblbankaccount C ON A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("INNER JOIN tblacquisitionequitysecuritiesdtl D ON A.DocNo = D.DocNo ");
            SQL.AppendLine("INNER JOIN tblinvestmentitemequity E ON D.InvestmentEquityCode = E.InvestmentEquityCode ");
            SQL.AppendLine("INNER JOIN tblinvestmentportofolio F ON E.PortofolioId = F.PortofolioId ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory G ON F.InvestmentCtCode = G.InvestmentCtCode ");
            SQL.AppendLine("INNER JOIN tbloption H ON D.InvestmentType = H.OptCode AND H.OptCat = 'InvestmentType' ");

            mSQL = SQL.ToString();
        }

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "where 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "CancelInd", "SekuritasName", "BankAcNm", "TradeDt",

                            //6-10
                            "SettlementDt", "Status", "Remark", "InvestmentName", "InvestmentCode", 
                            
                            //11-15
                            "OptDesc", "InvestmentCtName", "Qty", "BasePrice", "InvestmentCost", 
                            
                            //16-20
                            "TotalCost", "AcquisitionPrice", "CreateBy", "CreateDt", "LastUpBy", 

                            //21
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 24, 21);

                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Dokumen#");
        }


        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
