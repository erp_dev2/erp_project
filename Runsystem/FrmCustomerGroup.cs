﻿#region Update
/*
    08/06/2018 [TKG] new application
    26/06/2018 [TKG] tambah group category
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmCustomerGroupFind FrmFind;

        #endregion

        #region Constructor

        public FrmCustomerGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Customer's Group";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtCtGrpCode, TxtCtGrpName, ChkActInd, LueParent, MeeRemark }, true);
                    TxtCtGrpCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtCtGrpCode, TxtCtGrpName, LueParent, MeeRemark }, false);
                    TxtCtGrpCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkActInd, TxtCtGrpName, LueParent, MeeRemark }, false);
                    TxtCtGrpName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtCtGrpCode, TxtCtGrpName, LueParent, MeeRemark });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCustomerGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
            Sl.SetLueCtGrpCode(ref LueParent, string.Empty, "Y");
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCtGrpCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtCtGrpCode, string.Empty, false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCustomerGroup Where CtGrpCode=@CtGrpCode;" };
                Sm.CmParam<String>(ref cm, "@CtGrpCode", TxtCtGrpCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblCustomerGroup(CtGrpCode, CtGrpName, ActInd, Parent, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CtGrpCode, @CtGrpName, @ActInd, @Parent, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update CtGrpName=@CtGrpName, ActInd=@ActInd, Parent=@Parent, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CtGrpCode", TxtCtGrpCode.Text);
                Sm.CmParam<String>(ref cm, "@CtGrpName", TxtCtGrpName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtCtGrpCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CtGrpCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CtGrpCode", CtGrpCode);

                var SQL = new StringBuilder();

                SQL.AppendLine("Select CtGrpCode, CtGrpName, ActInd, Parent, Remark ");
                SQL.AppendLine("From TblCustomerGroup ");
                SQL.AppendLine("Where CtGrpCode=@CtGrpCode;");

                Sm.ShowDataInCtrl(
                        ref cm,
                        SQL.ToString(),
                        new string[] 
                        {
                            "CtGrpCode", 
                            "CtGrpName", "ActInd", "Parent", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCtGrpCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCtGrpName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                            Sl.SetLueCtGrpCode(ref LueParent, Sm.DrStr(dr, c[3]), "Y");
                            MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCtGrpCode, "Customer's group code", false) ||
                Sm.IsTxtEmpty(TxtCtGrpName, "Customer's group name", false) ||
                IsCtGrpCodeExisted();
        }

        private bool IsCtGrpCodeExisted()
        {
            if (TxtCtGrpCode.Properties.ReadOnly) return false;
            return Sm.IsDataExist(
                "Select CtGrpCode From TblCustomerGroup Where CtGrpCode=@Param;", 
                TxtCtGrpCode.Text,
                "Customer's group code ( " + TxtCtGrpCode.Text + " ) already existed."
                );
            
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCtGrpCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtCtGrpCode);
        }

        private void TxtCtGrpName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtCtGrpName);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue3(Sl.SetLueCtGrpCode), string.Empty, "Y");
        }

        #endregion

        #endregion
    }
}
