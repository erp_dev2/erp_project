﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptHistoryOfStockDlg : RunSystem.FrmBase6
    {
        #region Field

        private string mAccessInd = string.Empty, mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool mIsShowForeignName = false;

        #endregion

        #region Constructor

        public FrmRptHistoryOfStockDlg(string AccessInd, string ItCode, string ItName, string BatchNo, string Source)
        {
            InitializeComponent();
            mAccessInd = AccessInd;
            TxtItCode.Text = ItCode;
            TxtItName.Text = ItName;
            TxtBatchNo.Text = BatchNo;
            TxtSource.Text = Source;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetNumberOfInventoryUomCode();
            GetParameter();
            SetGrd();
            SetSQL();
            base.FrmLoad(sender, e);
            ShowData();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document Type",
                        "From/To",
                        "Document Number",
                        "Document"+Environment.NewLine+"Date",
                        "Warehouse",

                        //6-10
                        "Lot",
                        "Bin",
                        "Item"+Environment.NewLine+"Code", 
                        "Item Name", 
                        "Batch"+Environment.NewLine+"Number",
                        
                        //11-15
                        "Source",
                        "Quantity",
                        "UoM",
                        "Quantity 2",
                        "Uom 2",

                        //16-20
                        "Quantity 3",
                        "Uom 3",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Foreign Name"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16 }, 0);
            Grd1.Cols[20].Move(10);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7, 8, 11, 14, 15, 16, 17, 18, 19 }, false);
            if(!mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 20 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7, 8, 11, 18, 19 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T2.OptDesc, T1.DocNo, T1.DocDt, T3.WhsName, T1.Lot, T1.Bin, ");
            SQL.AppendLine("T1.ItCode, T4.ItName, T4.ForeignName, T1.BatchNo, T1.Source, ");
            SQL.AppendLine("T1.Qty, T4.InventoryUomCode, T1.Qty2, ");
            SQL.AppendLine("T4.InventoryUomCode2, T1.Qty3, T4.InventoryUomCode3, T1.CreateDt, ");
            SQL.AppendLine("Case T1.DocType ");
            SQL.AppendLine("        When '01' Then T8.VdName ");
            SQL.AppendLine("        When '13' Then T8.VdName ");
            SQL.AppendLine("        When '06' Then T10.DeptName ");
            SQL.AppendLine("        When '08' Then T12.CtName ");
            SQL.AppendLine("        When '10' Then T14.WhsName ");
            SQL.AppendLine("        When '16' Then T16.WhsName ");
            SQL.AppendLine("        When '02' Then T18.VdName ");
            SQL.AppendLine("        When '05' Then T20.DeptName ");
            SQL.AppendLine("        When '14' Then T20.DeptName ");
            SQL.AppendLine("        When '07' Then T22.CtName ");
            SQL.AppendLine("        When '09' Then T24.WhsName ");
            SQL.AppendLine("        When '15' Then T26.WhsName ");
            SQL.AppendLine("        When '16' Then T28.WhsName ");
            SQL.AppendLine("        Else '' End As FromTo ");
            SQL.AppendLine("From TblStockMovement T1 ");
            SQL.AppendLine("Inner Join TblOption T2 On T1.DocType = T2.OptCode And T2.OptCat='InventoryTransType' ");
            SQL.AppendLine("Inner Join TblWarehouse T3 On T1.WhsCode = T3.WhsCode ");
            SQL.AppendLine("Inner Join TblItem T4 On T1.ItCode = T4.ItCode  ");
            SQL.AppendLine("Left Join TblRecvVdHdr T7 On T1.DocNo = T7.DocNo And T1.DocType In ('01', '13') ");
            SQL.AppendLine("Left Join TblVendor T8 On T7.VdCode = T8.VdCode ");
            SQL.AppendLine("Left Join TblRecvDeptHdr T9 On T1.DocNo = T9.DocNo And T1.DocType='06' ");
            SQL.AppendLine("Left Join TblDepartment T10 On T9.DeptCode = T10.DeptCode ");
            SQL.AppendLine("Left Join TblRecvCtHdr T11 On T1.DocNo = T11.DocNo And T1.DocType='08' ");
            SQL.AppendLine("Left Join TblCustomer T12 On T11.CtCode = T12.CtCode ");
            SQL.AppendLine("Left Join TblRecvWhsHdr T13 On T1.DocNo = T13.DocNo And T1.DocType='10' ");
            SQL.AppendLine("Left Join TblWarehouse T14 On T13.WhsCode2 = T14.WhsCode ");
            SQL.AppendLine("Left Join TblRecvWhs2Hdr T15 On T1.DocNo = T15.DocNo And T1.DocType='16' ");
            SQL.AppendLine("Left Join TblWarehouse T16 On T15.WhsCode2 = T16.WhsCode ");
            SQL.AppendLine("Left Join TblDOVdHdr T17 On T1.DocNo = T17.DocNo And T1.DocType='02' ");
            SQL.AppendLine("Left Join TblVendor T18 On T17.VdCode = T18.VdCode ");
            SQL.AppendLine("Left Join TblDODeptHdr T19 On T1.DocNo = T19.DocNo And T1.DocType In ('05', '14') ");
            SQL.AppendLine("Left Join TblDepartment T20 On T19.DeptCode = T20.DeptCode ");
            SQL.AppendLine("Left Join TblDOCtHdr T21 On T1.DocNo = T21.DocNo And T1.DocType='07' ");
            SQL.AppendLine("Left Join TblCustomer T22 On T21.CtCode = T22.CtCode ");
            SQL.AppendLine("Left Join TblDOWhsHdr T23 On T1.DocNo = T23.DocNo And T1.DocType='09' ");
            SQL.AppendLine("Left Join TblWarehouse T24 On T23.WhsCode2 = T24.WhsCode ");
            SQL.AppendLine("Left Join TblRecvWhs2Hdr T25 On T1.DocNo = T25.DocNo And T1.DocType='15' ");
            SQL.AppendLine("Left Join TblWarehouse T26 On T25.WhsCode2 = T26.WhsCode ");
            SQL.AppendLine("Left Join TblRecvWhs2Hdr T27 On T1.DocNo = T27.DocNo And T1.DocType='16' ");
            SQL.AppendLine("Left Join TblWarehouse T28 On T27.WhsCode2 = T28.WhsCode ");
            SQL.AppendLine("Where T1.Source=@Source ");
            SQL.AppendLine("Order By T1.CreateDt, T1.DocType ");
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@Source", TxtSource.Text);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL,
                new string[]
                    {
                        //0
                        "OptDesc",
                        
                        //1-5
                        "FromTo", "DocNo", "DocDt", "WhsName", "Lot", 
                        
                        //6-10
                        "Bin", "ItCode", "ItName", "BatchNo", "Source",
                        
                        //11-15
                        "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", 
                        
                        //16-18
                        "InventoryUomCode3", "CreateDt", "ForeignName"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("T", Grd1, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 18);
                }, true, false, false, true
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14, 16 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
        }

        #endregion

        #endregion
    }
}
