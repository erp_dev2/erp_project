﻿namespace RunSystem
{
    partial class FrmEmployeeRecruitment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeRecruitment));
            this.panel3 = new System.Windows.Forms.Panel();
            this.BtnShowEmployeeRequest = new DevExpress.XtraEditors.SimpleButton();
            this.BtnEmpRequest = new DevExpress.XtraEditors.SimpleButton();
            this.TxtEmpRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label168 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.LueEmploymentStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.TxtPOH = new DevExpress.XtraEditors.TextEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.LueDivisionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.LueWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.LueSection = new DevExpress.XtraEditors.LookUpEdit();
            this.label63 = new System.Windows.Forms.Label();
            this.MeeDomicile = new DevExpress.XtraEditors.MemoExEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRTRW = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.TxtVillage = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtSubDistrict = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.TxtBarcodeCode = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label68 = new System.Windows.Forms.Label();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.LueBloodType = new DevExpress.XtraEditors.LookUpEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.LueSystemType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMother = new DevExpress.XtraEditors.TextEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.LuePayrunPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.DteWeddingDt = new DevExpress.XtraEditors.DateEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.label58 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.LueMaritalStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.LueGrdLvlCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtBirthPlace = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueReligion = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.LuePTKP = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LuePayrollType = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtNPWP = new DevExpress.XtraEditors.TextEdit();
            this.LueGender = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtIdNumber = new DevExpress.XtraEditors.TextEdit();
            this.LueCity = new DevExpress.XtraEditors.LookUpEdit();
            this.DteResignDt = new DevExpress.XtraEditors.DateEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.DteJoinDt = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtUserCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtPostalCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgEmployeeFamily = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.LueEducationLevelCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueProfessionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteFamBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.LueFamGender = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnFamily = new System.Windows.Forms.Button();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.button1 = new System.Windows.Forms.Button();
            this.TpgEmployeeWorkExp = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeEducation = new System.Windows.Forms.TabPage();
            this.LueFacCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueField = new DevExpress.XtraEditors.LookUpEdit();
            this.LueMajor = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeCompetence = new System.Windows.Forms.TabPage();
            this.LueCompetenceCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgTraining = new System.Windows.Forms.TabPage();
            this.LueTrainingCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeSS = new System.Windows.Forms.TabPage();
            this.TxtGrossSalary = new DevExpress.XtraEditors.TextEdit();
            this.LueDeptCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtProbation = new DevExpress.XtraEditors.TextEdit();
            this.DteStartWorkDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtPlacement = new DevExpress.XtraEditors.TextEdit();
            this.DteInterviewDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtInterviewerBy = new DevExpress.XtraEditors.TextEdit();
            this.TxtPlace = new DevExpress.XtraEditors.TextEdit();
            this.label167 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.TpPicture = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.button2 = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPicture = new DevExpress.XtraEditors.SimpleButton();
            this.button4 = new System.Windows.Forms.Button();
            this.PicEmployee = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.TpgStage = new System.Windows.Forms.TabPage();
            this.DteStageDt = new DevExpress.XtraEditors.DateEdit();
            this.LueProgress = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeBenefit = new System.Windows.Forms.TabPage();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label176 = new System.Windows.Forms.Label();
            this.TxtTax = new DevExpress.XtraEditors.TextEdit();
            this.label175 = new System.Windows.Forms.Label();
            this.TxtBonus = new DevExpress.XtraEditors.TextEdit();
            this.label174 = new System.Windows.Forms.Label();
            this.TxtAnnualLeave = new DevExpress.XtraEditors.TextEdit();
            this.label173 = new System.Windows.Forms.Label();
            this.TxtHolidayAllowance = new DevExpress.XtraEditors.TextEdit();
            this.label172 = new System.Windows.Forms.Label();
            this.TxtInsurance = new DevExpress.XtraEditors.TextEdit();
            this.label171 = new System.Windows.Forms.Label();
            this.TxtTHP = new DevExpress.XtraEditors.TextEdit();
            this.label170 = new System.Windows.Forms.Label();
            this.TxtJobIncentives = new DevExpress.XtraEditors.TextEdit();
            this.label169 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.lookUpEdit5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lookUpEdit6 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lookUpEdit7 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.dateEdit3 = new DevExpress.XtraEditors.DateEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.memoExEdit1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.lookUpEdit8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.lookUpEdit9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.lookUpEdit10 = new DevExpress.XtraEditors.LookUpEdit();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dateEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit11 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit12 = new DevExpress.XtraEditors.LookUpEdit();
            this.iGrid1 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.iGrid2 = new TenTec.Windows.iGridLib.iGrid();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label73 = new System.Windows.Forms.Label();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label74 = new System.Windows.Forms.Label();
            this.lookUpEdit13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label75 = new System.Windows.Forms.Label();
            this.memoExEdit2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.lookUpEdit14 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.label77 = new System.Windows.Forms.Label();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.label78 = new System.Windows.Forms.Label();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.label81 = new System.Windows.Forms.Label();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.label82 = new System.Windows.Forms.Label();
            this.lookUpEdit15 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.label83 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label84 = new System.Windows.Forms.Label();
            this.lookUpEdit16 = new DevExpress.XtraEditors.LookUpEdit();
            this.label85 = new System.Windows.Forms.Label();
            this.lookUpEdit17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label86 = new System.Windows.Forms.Label();
            this.lookUpEdit18 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.lookUpEdit19 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit5 = new DevExpress.XtraEditors.DateEdit();
            this.label89 = new System.Windows.Forms.Label();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit20 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit21 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit6 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.label96 = new System.Windows.Forms.Label();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.label97 = new System.Windows.Forms.Label();
            this.lookUpEdit22 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.lookUpEdit23 = new DevExpress.XtraEditors.LookUpEdit();
            this.label100 = new System.Windows.Forms.Label();
            this.lookUpEdit24 = new DevExpress.XtraEditors.LookUpEdit();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.lookUpEdit25 = new DevExpress.XtraEditors.LookUpEdit();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit26 = new DevExpress.XtraEditors.LookUpEdit();
            this.label105 = new System.Windows.Forms.Label();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.label106 = new System.Windows.Forms.Label();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit27 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit7 = new DevExpress.XtraEditors.DateEdit();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.dateEdit8 = new DevExpress.XtraEditors.DateEdit();
            this.label109 = new System.Windows.Forms.Label();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit29 = new DevExpress.XtraEditors.TextEdit();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.memoExEdit3 = new DevExpress.XtraEditors.MemoExEdit();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.lookUpEdit28 = new DevExpress.XtraEditors.LookUpEdit();
            this.label114 = new System.Windows.Forms.Label();
            this.lookUpEdit29 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.lookUpEdit30 = new DevExpress.XtraEditors.LookUpEdit();
            this.label117 = new System.Windows.Forms.Label();
            this.lookUpEdit31 = new DevExpress.XtraEditors.LookUpEdit();
            this.label118 = new System.Windows.Forms.Label();
            this.lookUpEdit32 = new DevExpress.XtraEditors.LookUpEdit();
            this.label119 = new System.Windows.Forms.Label();
            this.memoExEdit4 = new DevExpress.XtraEditors.MemoExEdit();
            this.label120 = new System.Windows.Forms.Label();
            this.lookUpEdit33 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit31 = new DevExpress.XtraEditors.TextEdit();
            this.label121 = new System.Windows.Forms.Label();
            this.textEdit32 = new DevExpress.XtraEditors.TextEdit();
            this.label122 = new System.Windows.Forms.Label();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.textEdit34 = new DevExpress.XtraEditors.TextEdit();
            this.label125 = new System.Windows.Forms.Label();
            this.textEdit35 = new DevExpress.XtraEditors.TextEdit();
            this.label126 = new System.Windows.Forms.Label();
            this.lookUpEdit34 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit36 = new DevExpress.XtraEditors.TextEdit();
            this.label127 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label128 = new System.Windows.Forms.Label();
            this.lookUpEdit35 = new DevExpress.XtraEditors.LookUpEdit();
            this.label129 = new System.Windows.Forms.Label();
            this.lookUpEdit36 = new DevExpress.XtraEditors.LookUpEdit();
            this.label130 = new System.Windows.Forms.Label();
            this.lookUpEdit37 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit37 = new DevExpress.XtraEditors.TextEdit();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.lookUpEdit38 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit9 = new DevExpress.XtraEditors.DateEdit();
            this.label133 = new System.Windows.Forms.Label();
            this.textEdit38 = new DevExpress.XtraEditors.TextEdit();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.textEdit39 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit39 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit40 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit10 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit40 = new DevExpress.XtraEditors.TextEdit();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.textEdit41 = new DevExpress.XtraEditors.TextEdit();
            this.label140 = new System.Windows.Forms.Label();
            this.textEdit42 = new DevExpress.XtraEditors.TextEdit();
            this.label141 = new System.Windows.Forms.Label();
            this.lookUpEdit41 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit43 = new DevExpress.XtraEditors.TextEdit();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.lookUpEdit42 = new DevExpress.XtraEditors.LookUpEdit();
            this.label144 = new System.Windows.Forms.Label();
            this.lookUpEdit43 = new DevExpress.XtraEditors.LookUpEdit();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.lookUpEdit44 = new DevExpress.XtraEditors.LookUpEdit();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.textEdit44 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit45 = new DevExpress.XtraEditors.LookUpEdit();
            this.label149 = new System.Windows.Forms.Label();
            this.textEdit45 = new DevExpress.XtraEditors.TextEdit();
            this.label150 = new System.Windows.Forms.Label();
            this.textEdit46 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit46 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit11 = new DevExpress.XtraEditors.DateEdit();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.dateEdit12 = new DevExpress.XtraEditors.DateEdit();
            this.label153 = new System.Windows.Forms.Label();
            this.textEdit47 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit48 = new DevExpress.XtraEditors.TextEdit();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.memoExEdit5 = new DevExpress.XtraEditors.MemoExEdit();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.lookUpEdit47 = new DevExpress.XtraEditors.LookUpEdit();
            this.label158 = new System.Windows.Forms.Label();
            this.lookUpEdit48 = new DevExpress.XtraEditors.LookUpEdit();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarcodeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrollType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            this.TpgEmployeeFamily.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfessionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFamGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpgEmployeeWorkExp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgEmployeeEducation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFacCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMajor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgEmployeeCompetence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpgTraining.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgEmployeeSS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrossSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProbation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartWorkDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartWorkDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlacement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInterviewDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInterviewDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterviewerBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlace.Properties)).BeginInit();
            this.TpPicture.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicEmployee)).BeginInit();
            this.TpgStage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteStageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProgress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.TpgEmployeeBenefit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBonus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualLeave.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHolidayAllowance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInsurance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTHP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobIncentives.Properties)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit48.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 600);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 600);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.BtnShowEmployeeRequest);
            this.panel3.Controls.Add(this.BtnEmpRequest);
            this.panel3.Controls.Add(this.TxtEmpRequestDocNo);
            this.panel3.Controls.Add(this.label168);
            this.panel3.Controls.Add(this.label71);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label72);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.TxtDisplayName);
            this.panel3.Controls.Add(this.label66);
            this.panel3.Controls.Add(this.TxtEmpName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 95);
            this.panel3.TabIndex = 9;
            // 
            // BtnShowEmployeeRequest
            // 
            this.BtnShowEmployeeRequest.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnShowEmployeeRequest.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnShowEmployeeRequest.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnShowEmployeeRequest.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnShowEmployeeRequest.Appearance.Options.UseBackColor = true;
            this.BtnShowEmployeeRequest.Appearance.Options.UseFont = true;
            this.BtnShowEmployeeRequest.Appearance.Options.UseForeColor = true;
            this.BtnShowEmployeeRequest.Appearance.Options.UseTextOptions = true;
            this.BtnShowEmployeeRequest.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnShowEmployeeRequest.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnShowEmployeeRequest.Image = ((System.Drawing.Image)(resources.GetObject("BtnShowEmployeeRequest.Image")));
            this.BtnShowEmployeeRequest.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnShowEmployeeRequest.Location = new System.Drawing.Point(651, 4);
            this.BtnShowEmployeeRequest.Name = "BtnShowEmployeeRequest";
            this.BtnShowEmployeeRequest.Size = new System.Drawing.Size(24, 21);
            this.BtnShowEmployeeRequest.TabIndex = 21;
            this.BtnShowEmployeeRequest.ToolTip = "Show Employee Request";
            this.BtnShowEmployeeRequest.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnShowEmployeeRequest.ToolTipTitle = "Run System";
            this.BtnShowEmployeeRequest.Click += new System.EventHandler(this.BtnShowEmployeeRequest_Click);
            // 
            // BtnEmpRequest
            // 
            this.BtnEmpRequest.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpRequest.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpRequest.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpRequest.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpRequest.Appearance.Options.UseBackColor = true;
            this.BtnEmpRequest.Appearance.Options.UseFont = true;
            this.BtnEmpRequest.Appearance.Options.UseForeColor = true;
            this.BtnEmpRequest.Appearance.Options.UseTextOptions = true;
            this.BtnEmpRequest.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpRequest.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpRequest.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpRequest.Image")));
            this.BtnEmpRequest.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpRequest.Location = new System.Drawing.Point(622, 4);
            this.BtnEmpRequest.Name = "BtnEmpRequest";
            this.BtnEmpRequest.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpRequest.TabIndex = 20;
            this.BtnEmpRequest.ToolTip = "Choose Employee Request";
            this.BtnEmpRequest.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpRequest.ToolTipTitle = "Run System";
            this.BtnEmpRequest.Click += new System.EventHandler(this.BtnEmpRequest_Click);
            // 
            // TxtEmpRequestDocNo
            // 
            this.TxtEmpRequestDocNo.EnterMoveNextControl = true;
            this.TxtEmpRequestDocNo.Location = new System.Drawing.Point(380, 4);
            this.TxtEmpRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpRequestDocNo.Name = "TxtEmpRequestDocNo";
            this.TxtEmpRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpRequestDocNo.Properties.MaxLength = 16;
            this.TxtEmpRequestDocNo.Properties.ReadOnly = true;
            this.TxtEmpRequestDocNo.Size = new System.Drawing.Size(239, 20);
            this.TxtEmpRequestDocNo.TabIndex = 19;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label168.ForeColor = System.Drawing.Color.Black;
            this.label168.Location = new System.Drawing.Point(307, 7);
            this.label168.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(65, 14);
            this.label168.TabIndex = 18;
            this.label168.Text = "Request #";
            this.label168.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(67, 29);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(33, 14);
            this.label71.TabIndex = 12;
            this.label71.Text = "Date";
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(104, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(100, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(27, 8);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(73, 14);
            this.label72.TabIndex = 10;
            this.label72.Text = "Document#";
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(104, 5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(200, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(104, 68);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 20;
            this.TxtDisplayName.Size = new System.Drawing.Size(257, 20);
            this.TxtDisplayName.TabIndex = 17;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(22, 71);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(78, 14);
            this.label66.TabIndex = 16;
            this.label66.Text = "Display Name";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(104, 47);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 100;
            this.TxtEmpName.Size = new System.Drawing.Size(517, 20);
            this.TxtEmpName.TabIndex = 15;
            this.TxtEmpName.Validated += new System.EventHandler(this.TxtEmpName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(5, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Employee Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(6, 92);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(114, 14);
            this.label53.TabIndex = 29;
            this.label53.Text = "Employment Status";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEmploymentStatus
            // 
            this.LueEmploymentStatus.EnterMoveNextControl = true;
            this.LueEmploymentStatus.Location = new System.Drawing.Point(125, 90);
            this.LueEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmploymentStatus.Name = "LueEmploymentStatus";
            this.LueEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmploymentStatus.Properties.DropDownRows = 25;
            this.LueEmploymentStatus.Properties.MaxLength = 1;
            this.LueEmploymentStatus.Properties.NullText = "[Empty]";
            this.LueEmploymentStatus.Properties.PopupWidth = 500;
            this.LueEmploymentStatus.Size = new System.Drawing.Size(257, 20);
            this.LueEmploymentStatus.TabIndex = 30;
            this.LueEmploymentStatus.ToolTip = "F4 : Show/hide list";
            this.LueEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmploymentStatus.EditValueChanged += new System.EventHandler(this.LueEmploymentStatus_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 95);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 505);
            this.panel4.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgGeneral);
            this.tabControl1.Controls.Add(this.TpgEmployeeFamily);
            this.tabControl1.Controls.Add(this.TpgEmployeeWorkExp);
            this.tabControl1.Controls.Add(this.TpgEmployeeEducation);
            this.tabControl1.Controls.Add(this.TpgEmployeeCompetence);
            this.tabControl1.Controls.Add(this.TpgTraining);
            this.tabControl1.Controls.Add(this.TpgEmployeeSS);
            this.tabControl1.Controls.Add(this.TpPicture);
            this.tabControl1.Controls.Add(this.TpgStage);
            this.tabControl1.Controls.Add(this.TpgEmployeeBenefit);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(772, 505);
            this.tabControl1.TabIndex = 14;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.TxtPOH);
            this.TpgGeneral.Controls.Add(this.label69);
            this.TpgGeneral.Controls.Add(this.label67);
            this.TpgGeneral.Controls.Add(this.LueDivisionCode);
            this.TpgGeneral.Controls.Add(this.label65);
            this.TpgGeneral.Controls.Add(this.LueWorkGroup);
            this.TpgGeneral.Controls.Add(this.label64);
            this.TpgGeneral.Controls.Add(this.LueSection);
            this.TpgGeneral.Controls.Add(this.label63);
            this.TpgGeneral.Controls.Add(this.MeeDomicile);
            this.TpgGeneral.Controls.Add(this.label61);
            this.TpgGeneral.Controls.Add(this.LueSiteCode);
            this.TpgGeneral.Controls.Add(this.TxtRTRW);
            this.TpgGeneral.Controls.Add(this.label56);
            this.TpgGeneral.Controls.Add(this.TxtVillage);
            this.TpgGeneral.Controls.Add(this.label55);
            this.TpgGeneral.Controls.Add(this.TxtSubDistrict);
            this.TpgGeneral.Controls.Add(this.label54);
            this.TpgGeneral.Controls.Add(this.label53);
            this.TpgGeneral.Controls.Add(this.TxtBarcodeCode);
            this.TpgGeneral.Controls.Add(this.label52);
            this.TpgGeneral.Controls.Add(this.TxtShortCode);
            this.TpgGeneral.Controls.Add(this.label50);
            this.TpgGeneral.Controls.Add(this.LueEmploymentStatus);
            this.TpgGeneral.Controls.Add(this.TxtEmpCodeOld);
            this.TpgGeneral.Controls.Add(this.label48);
            this.TpgGeneral.Controls.Add(this.panel5);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.TxtIdNumber);
            this.TpgGeneral.Controls.Add(this.LueCity);
            this.TpgGeneral.Controls.Add(this.DteResignDt);
            this.TpgGeneral.Controls.Add(this.label11);
            this.TpgGeneral.Controls.Add(this.label10);
            this.TpgGeneral.Controls.Add(this.DteJoinDt);
            this.TpgGeneral.Controls.Add(this.label6);
            this.TpgGeneral.Controls.Add(this.TxtUserCode);
            this.TpgGeneral.Controls.Add(this.TxtPostalCode);
            this.TpgGeneral.Controls.Add(this.label3);
            this.TpgGeneral.Controls.Add(this.label15);
            this.TpgGeneral.Controls.Add(this.MeeAddress);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.label5);
            this.TpgGeneral.Controls.Add(this.LuePosCode);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.LueDeptCode);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 51);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(764, 450);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // TxtPOH
            // 
            this.TxtPOH.EnterMoveNextControl = true;
            this.TxtPOH.Location = new System.Drawing.Point(125, 237);
            this.TxtPOH.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOH.Name = "TxtPOH";
            this.TxtPOH.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPOH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOH.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOH.Properties.Appearance.Options.UseFont = true;
            this.TxtPOH.Properties.MaxLength = 40;
            this.TxtPOH.Size = new System.Drawing.Size(257, 20);
            this.TxtPOH.TabIndex = 44;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(44, 240);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(75, 14);
            this.label69.TabIndex = 43;
            this.label69.Text = "Point of Hire";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(74, 135);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(46, 14);
            this.label67.TabIndex = 33;
            this.label67.Text = "Division";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDivisionCode
            // 
            this.LueDivisionCode.EnterMoveNextControl = true;
            this.LueDivisionCode.Location = new System.Drawing.Point(125, 132);
            this.LueDivisionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDivisionCode.Name = "LueDivisionCode";
            this.LueDivisionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.Appearance.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDivisionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDivisionCode.Properties.DropDownRows = 30;
            this.LueDivisionCode.Properties.MaxLength = 16;
            this.LueDivisionCode.Properties.NullText = "[Empty]";
            this.LueDivisionCode.Properties.PopupWidth = 300;
            this.LueDivisionCode.Size = new System.Drawing.Size(257, 20);
            this.LueDivisionCode.TabIndex = 34;
            this.LueDivisionCode.ToolTip = "F4 : Show/hide list";
            this.LueDivisionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDivisionCode.EditValueChanged += new System.EventHandler(this.LueDivisionCode_EditValueChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(31, 219);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(89, 14);
            this.label65.TabIndex = 41;
            this.label65.Text = "Working Group";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWorkGroup
            // 
            this.LueWorkGroup.EnterMoveNextControl = true;
            this.LueWorkGroup.Location = new System.Drawing.Point(125, 216);
            this.LueWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueWorkGroup.Name = "LueWorkGroup";
            this.LueWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWorkGroup.Properties.DropDownRows = 30;
            this.LueWorkGroup.Properties.MaxLength = 16;
            this.LueWorkGroup.Properties.NullText = "[Empty]";
            this.LueWorkGroup.Properties.PopupWidth = 300;
            this.LueWorkGroup.Size = new System.Drawing.Size(257, 20);
            this.LueWorkGroup.TabIndex = 42;
            this.LueWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWorkGroup.EditValueChanged += new System.EventHandler(this.LueWorkGroup_EditValueChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(72, 197);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(48, 14);
            this.label64.TabIndex = 39;
            this.label64.Text = "Section";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSection
            // 
            this.LueSection.EnterMoveNextControl = true;
            this.LueSection.Location = new System.Drawing.Point(125, 195);
            this.LueSection.Margin = new System.Windows.Forms.Padding(5);
            this.LueSection.Name = "LueSection";
            this.LueSection.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.Appearance.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSection.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSection.Properties.DropDownRows = 30;
            this.LueSection.Properties.MaxLength = 16;
            this.LueSection.Properties.NullText = "[Empty]";
            this.LueSection.Properties.PopupWidth = 300;
            this.LueSection.Size = new System.Drawing.Size(257, 20);
            this.LueSection.TabIndex = 40;
            this.LueSection.ToolTip = "F4 : Show/hide list";
            this.LueSection.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSection.EditValueChanged += new System.EventHandler(this.LueSection_EditValueChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(69, 429);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(51, 14);
            this.label63.TabIndex = 61;
            this.label63.Text = "Domicile";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDomicile
            // 
            this.MeeDomicile.EnterMoveNextControl = true;
            this.MeeDomicile.Location = new System.Drawing.Point(125, 426);
            this.MeeDomicile.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDomicile.Name = "MeeDomicile";
            this.MeeDomicile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.Appearance.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDomicile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDomicile.Properties.MaxLength = 400;
            this.MeeDomicile.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDomicile.Properties.ShowIcon = false;
            this.MeeDomicile.Size = new System.Drawing.Size(257, 20);
            this.MeeDomicile.TabIndex = 62;
            this.MeeDomicile.ToolTip = "F4 : Show/hide text";
            this.MeeDomicile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDomicile.ToolTipTitle = "Run System";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(92, 114);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 14);
            this.label61.TabIndex = 31;
            this.label61.Text = "Site";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(125, 111);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(257, 20);
            this.LueSiteCode.TabIndex = 32;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // TxtRTRW
            // 
            this.TxtRTRW.EnterMoveNextControl = true;
            this.TxtRTRW.Location = new System.Drawing.Point(125, 384);
            this.TxtRTRW.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRTRW.Name = "TxtRTRW";
            this.TxtRTRW.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRTRW.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRTRW.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRTRW.Properties.Appearance.Options.UseFont = true;
            this.TxtRTRW.Properties.MaxLength = 10;
            this.TxtRTRW.Size = new System.Drawing.Size(257, 20);
            this.TxtRTRW.TabIndex = 58;
            this.TxtRTRW.Validated += new System.EventHandler(this.TxtRTRW_Validated);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(74, 387);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(46, 14);
            this.label56.TabIndex = 57;
            this.label56.Text = "RT/RW";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVillage
            // 
            this.TxtVillage.EnterMoveNextControl = true;
            this.TxtVillage.Location = new System.Drawing.Point(125, 363);
            this.TxtVillage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVillage.Name = "TxtVillage";
            this.TxtVillage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVillage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVillage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVillage.Properties.Appearance.Options.UseFont = true;
            this.TxtVillage.Properties.MaxLength = 40;
            this.TxtVillage.Size = new System.Drawing.Size(257, 20);
            this.TxtVillage.TabIndex = 56;
            this.TxtVillage.Validated += new System.EventHandler(this.TxtVillage_Validated);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(79, 366);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(41, 14);
            this.label55.TabIndex = 55;
            this.label55.Text = "Village";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSubDistrict
            // 
            this.TxtSubDistrict.EnterMoveNextControl = true;
            this.TxtSubDistrict.Location = new System.Drawing.Point(125, 342);
            this.TxtSubDistrict.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubDistrict.Name = "TxtSubDistrict";
            this.TxtSubDistrict.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSubDistrict.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubDistrict.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubDistrict.Properties.Appearance.Options.UseFont = true;
            this.TxtSubDistrict.Properties.MaxLength = 40;
            this.TxtSubDistrict.Size = new System.Drawing.Size(257, 20);
            this.TxtSubDistrict.TabIndex = 54;
            this.TxtSubDistrict.Validated += new System.EventHandler(this.TxtSubDistrict_Validated);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(51, 344);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(69, 14);
            this.label54.TabIndex = 53;
            this.label54.Text = "Sub District";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBarcodeCode
            // 
            this.TxtBarcodeCode.EnterMoveNextControl = true;
            this.TxtBarcodeCode.Location = new System.Drawing.Point(125, 69);
            this.TxtBarcodeCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBarcodeCode.Name = "TxtBarcodeCode";
            this.TxtBarcodeCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBarcodeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBarcodeCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBarcodeCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBarcodeCode.Properties.MaxLength = 16;
            this.TxtBarcodeCode.Size = new System.Drawing.Size(257, 20);
            this.TxtBarcodeCode.TabIndex = 28;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(37, 71);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(83, 14);
            this.label52.TabIndex = 27;
            this.label52.Text = "Barcode Code";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(125, 48);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 3;
            this.TxtShortCode.Size = new System.Drawing.Size(257, 20);
            this.TxtShortCode.TabIndex = 26;
            this.TxtShortCode.Validated += new System.EventHandler(this.TxtShortCode_Validated);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(51, 51);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(69, 14);
            this.label50.TabIndex = 25;
            this.label50.Text = "Short Code";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(125, 27);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 16;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(257, 20);
            this.TxtEmpCodeOld.TabIndex = 24;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(63, 29);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(57, 14);
            this.label48.TabIndex = 23;
            this.label48.Text = "Old Code";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label68);
            this.panel5.Controls.Add(this.LuePGCode);
            this.panel5.Controls.Add(this.label62);
            this.panel5.Controls.Add(this.LueBloodType);
            this.panel5.Controls.Add(this.label60);
            this.panel5.Controls.Add(this.LueSystemType);
            this.panel5.Controls.Add(this.TxtMother);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.LuePayrunPeriod);
            this.panel5.Controls.Add(this.DteWeddingDt);
            this.panel5.Controls.Add(this.label59);
            this.panel5.Controls.Add(this.TxtBankAcName);
            this.panel5.Controls.Add(this.label58);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.TxtEmail);
            this.panel5.Controls.Add(this.LueMaritalStatus);
            this.panel5.Controls.Add(this.LueGrdLvlCode);
            this.panel5.Controls.Add(this.DteBirthDt);
            this.panel5.Controls.Add(this.TxtBankBranch);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.TxtBirthPlace);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.TxtBankAcNo);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.LueReligion);
            this.panel5.Controls.Add(this.TxtMobile);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.LueBankCode);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.LuePTKP);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.LuePayrollType);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TxtNPWP);
            this.panel5.Controls.Add(this.LueGender);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.TxtPhone);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(388, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(372, 446);
            this.panel5.TabIndex = 47;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(31, 199);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(78, 14);
            this.label68.TabIndex = 84;
            this.label68.Text = "Payroll Group";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(115, 195);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 30;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 300;
            this.LuePGCode.Size = new System.Drawing.Size(245, 20);
            this.LuePGCode.TabIndex = 85;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePGCode.EditValueChanged += new System.EventHandler(this.LuePGCode_EditValueChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(40, 115);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(69, 14);
            this.label62.TabIndex = 76;
            this.label62.Text = "Blood Type";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBloodType
            // 
            this.LueBloodType.EnterMoveNextControl = true;
            this.LueBloodType.Location = new System.Drawing.Point(115, 111);
            this.LueBloodType.Margin = new System.Windows.Forms.Padding(5);
            this.LueBloodType.Name = "LueBloodType";
            this.LueBloodType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.Appearance.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBloodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBloodType.Properties.DropDownRows = 30;
            this.LueBloodType.Properties.MaxLength = 2;
            this.LueBloodType.Properties.NullText = "[Empty]";
            this.LueBloodType.Properties.PopupWidth = 300;
            this.LueBloodType.Size = new System.Drawing.Size(245, 20);
            this.LueBloodType.TabIndex = 77;
            this.LueBloodType.ToolTip = "F4 : Show/hide list";
            this.LueBloodType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBloodType.EditValueChanged += new System.EventHandler(this.LueBloodType_EditValueChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(30, 157);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(79, 14);
            this.label60.TabIndex = 80;
            this.label60.Text = "System Type";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSystemType
            // 
            this.LueSystemType.EnterMoveNextControl = true;
            this.LueSystemType.Location = new System.Drawing.Point(115, 153);
            this.LueSystemType.Margin = new System.Windows.Forms.Padding(5);
            this.LueSystemType.Name = "LueSystemType";
            this.LueSystemType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.Appearance.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSystemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSystemType.Properties.DropDownRows = 30;
            this.LueSystemType.Properties.NullText = "[Empty]";
            this.LueSystemType.Properties.PopupWidth = 300;
            this.LueSystemType.Size = new System.Drawing.Size(245, 20);
            this.LueSystemType.TabIndex = 81;
            this.LueSystemType.ToolTip = "F4 : Show/hide list";
            this.LueSystemType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSystemType.EditValueChanged += new System.EventHandler(this.LueSystemType_EditValueChanged);
            // 
            // TxtMother
            // 
            this.TxtMother.EnterMoveNextControl = true;
            this.TxtMother.Location = new System.Drawing.Point(115, 132);
            this.TxtMother.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMother.Name = "TxtMother";
            this.TxtMother.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMother.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMother.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMother.Properties.Appearance.Options.UseFont = true;
            this.TxtMother.Properties.MaxLength = 40;
            this.TxtMother.Size = new System.Drawing.Size(245, 20);
            this.TxtMother.TabIndex = 79;
            this.TxtMother.Validated += new System.EventHandler(this.TxtMother_Validated);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(11, 136);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(98, 14);
            this.label57.TabIndex = 78;
            this.label57.Text = "Biological Mother";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(27, 241);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(82, 14);
            this.label51.TabIndex = 88;
            this.label51.Text = "Payrun Period";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrunPeriod
            // 
            this.LuePayrunPeriod.EnterMoveNextControl = true;
            this.LuePayrunPeriod.Location = new System.Drawing.Point(115, 237);
            this.LuePayrunPeriod.Margin = new System.Windows.Forms.Padding(5);
            this.LuePayrunPeriod.Name = "LuePayrunPeriod";
            this.LuePayrunPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrunPeriod.Properties.DropDownRows = 30;
            this.LuePayrunPeriod.Properties.MaxLength = 1;
            this.LuePayrunPeriod.Properties.NullText = "[Empty]";
            this.LuePayrunPeriod.Properties.PopupWidth = 300;
            this.LuePayrunPeriod.Size = new System.Drawing.Size(245, 20);
            this.LuePayrunPeriod.TabIndex = 89;
            this.LuePayrunPeriod.ToolTip = "F4 : Show/hide list";
            this.LuePayrunPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePayrunPeriod.EditValueChanged += new System.EventHandler(this.LuePayrunPeriod_EditValueChanged);
            // 
            // DteWeddingDt
            // 
            this.DteWeddingDt.EditValue = null;
            this.DteWeddingDt.EnterMoveNextControl = true;
            this.DteWeddingDt.Location = new System.Drawing.Point(115, 28);
            this.DteWeddingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteWeddingDt.Name = "DteWeddingDt";
            this.DteWeddingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.Appearance.Options.UseFont = true;
            this.DteWeddingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteWeddingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteWeddingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteWeddingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.MaxLength = 16;
            this.DteWeddingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteWeddingDt.Size = new System.Drawing.Size(100, 20);
            this.DteWeddingDt.TabIndex = 68;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(23, 31);
            this.label59.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(86, 14);
            this.label59.TabIndex = 67;
            this.label59.Text = "Wedding Date";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcName
            // 
            this.TxtBankAcName.EnterMoveNextControl = true;
            this.TxtBankAcName.Location = new System.Drawing.Point(115, 342);
            this.TxtBankAcName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcName.Name = "TxtBankAcName";
            this.TxtBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcName.Properties.MaxLength = 80;
            this.TxtBankAcName.Size = new System.Drawing.Size(245, 20);
            this.TxtBankAcName.TabIndex = 99;
            this.TxtBankAcName.Validated += new System.EventHandler(this.TxtBankAcName_Validated);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(29, 10);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 14);
            this.label58.TabIndex = 65;
            this.label58.Text = "Marital Status";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(21, 346);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(88, 14);
            this.label49.TabIndex = 98;
            this.label49.Text = "Account Name";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(38, 178);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(71, 14);
            this.label47.TabIndex = 82;
            this.label47.Text = "Grade Level";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(115, 426);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 80;
            this.TxtEmail.Size = new System.Drawing.Size(245, 20);
            this.TxtEmail.TabIndex = 107;
            this.TxtEmail.Validated += new System.EventHandler(this.TxtEmail_Validated);
            // 
            // LueMaritalStatus
            // 
            this.LueMaritalStatus.EnterMoveNextControl = true;
            this.LueMaritalStatus.Location = new System.Drawing.Point(115, 7);
            this.LueMaritalStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueMaritalStatus.Name = "LueMaritalStatus";
            this.LueMaritalStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.Appearance.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMaritalStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMaritalStatus.Properties.DropDownRows = 30;
            this.LueMaritalStatus.Properties.MaxLength = 1;
            this.LueMaritalStatus.Properties.NullText = "[Empty]";
            this.LueMaritalStatus.Properties.PopupWidth = 300;
            this.LueMaritalStatus.Size = new System.Drawing.Size(245, 20);
            this.LueMaritalStatus.TabIndex = 66;
            this.LueMaritalStatus.ToolTip = "F4 : Show/hide list";
            this.LueMaritalStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMaritalStatus.EditValueChanged += new System.EventHandler(this.LueMaritalStatus_EditValueChanged);
            // 
            // LueGrdLvlCode
            // 
            this.LueGrdLvlCode.EnterMoveNextControl = true;
            this.LueGrdLvlCode.Location = new System.Drawing.Point(115, 174);
            this.LueGrdLvlCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdLvlCode.Name = "LueGrdLvlCode";
            this.LueGrdLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlCode.Properties.DropDownRows = 30;
            this.LueGrdLvlCode.Properties.NullText = "[Empty]";
            this.LueGrdLvlCode.Properties.PopupWidth = 300;
            this.LueGrdLvlCode.Size = new System.Drawing.Size(245, 20);
            this.LueGrdLvlCode.TabIndex = 83;
            this.LueGrdLvlCode.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdLvlCode.EditValueChanged += new System.EventHandler(this.LueGrdLvlCode_EditValueChanged);
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(243, 91);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.MaxLength = 8;
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(117, 20);
            this.DteBirthDt.TabIndex = 75;
            // 
            // TxtBankBranch
            // 
            this.TxtBankBranch.EnterMoveNextControl = true;
            this.TxtBankBranch.Location = new System.Drawing.Point(115, 321);
            this.TxtBankBranch.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankBranch.Name = "TxtBankBranch";
            this.TxtBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtBankBranch.Properties.MaxLength = 80;
            this.TxtBankBranch.Size = new System.Drawing.Size(245, 20);
            this.TxtBankBranch.TabIndex = 97;
            this.TxtBankBranch.Validated += new System.EventHandler(this.TxtBankBranch_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(229, 93);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 14);
            this.label14.TabIndex = 67;
            this.label14.Text = "/";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(75, 429);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 14);
            this.label18.TabIndex = 106;
            this.label18.Text = "Email";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(30, 325);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 14);
            this.label23.TabIndex = 96;
            this.label23.Text = "Branch Name";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBirthPlace
            // 
            this.TxtBirthPlace.EnterMoveNextControl = true;
            this.TxtBirthPlace.Location = new System.Drawing.Point(115, 91);
            this.TxtBirthPlace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBirthPlace.Name = "TxtBirthPlace";
            this.TxtBirthPlace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBirthPlace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirthPlace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBirthPlace.Properties.Appearance.Options.UseFont = true;
            this.TxtBirthPlace.Properties.MaxLength = 80;
            this.TxtBirthPlace.Size = new System.Drawing.Size(112, 20);
            this.TxtBirthPlace.TabIndex = 74;
            this.TxtBirthPlace.Validated += new System.EventHandler(this.TxtBirthPlace_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(14, 95);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 14);
            this.label13.TabIndex = 73;
            this.label13.Text = "Birth Place/Date";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNo
            // 
            this.TxtBankAcNo.EnterMoveNextControl = true;
            this.TxtBankAcNo.Location = new System.Drawing.Point(115, 363);
            this.TxtBankAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcNo.Name = "TxtBankAcNo";
            this.TxtBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNo.Properties.MaxLength = 80;
            this.TxtBankAcNo.Size = new System.Drawing.Size(245, 20);
            this.TxtBankAcNo.TabIndex = 101;
            this.TxtBankAcNo.Validated += new System.EventHandler(this.TxtBankAcNo_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(61, 74);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 14);
            this.label12.TabIndex = 71;
            this.label12.Text = "Religion";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReligion
            // 
            this.LueReligion.EnterMoveNextControl = true;
            this.LueReligion.Location = new System.Drawing.Point(115, 70);
            this.LueReligion.Margin = new System.Windows.Forms.Padding(5);
            this.LueReligion.Name = "LueReligion";
            this.LueReligion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.Appearance.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReligion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReligion.Properties.DropDownRows = 30;
            this.LueReligion.Properties.MaxLength = 2;
            this.LueReligion.Properties.NullText = "[Empty]";
            this.LueReligion.Properties.PopupWidth = 300;
            this.LueReligion.Size = new System.Drawing.Size(245, 20);
            this.LueReligion.TabIndex = 72;
            this.LueReligion.ToolTip = "F4 : Show/hide list";
            this.LueReligion.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReligion.EditValueChanged += new System.EventHandler(this.LueReligion_EditValueChanged);
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(115, 405);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 80;
            this.TxtMobile.Size = new System.Drawing.Size(245, 20);
            this.TxtMobile.TabIndex = 105;
            this.TxtMobile.Validated += new System.EventHandler(this.TxtMobile_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(47, 366);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(62, 14);
            this.label24.TabIndex = 100;
            this.label24.Text = "Account#";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(41, 303);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 14);
            this.label22.TabIndex = 94;
            this.label22.Text = "Bank Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(115, 300);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(245, 20);
            this.LueBankCode.TabIndex = 95;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(2, 283);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 14);
            this.label21.TabIndex = 92;
            this.label21.Text = "Non-Incoming Tax";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePTKP
            // 
            this.LuePTKP.EnterMoveNextControl = true;
            this.LuePTKP.Location = new System.Drawing.Point(115, 279);
            this.LuePTKP.Margin = new System.Windows.Forms.Padding(5);
            this.LuePTKP.Name = "LuePTKP";
            this.LuePTKP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.Appearance.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePTKP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePTKP.Properties.DropDownRows = 30;
            this.LuePTKP.Properties.NullText = "[Empty]";
            this.LuePTKP.Properties.PopupWidth = 300;
            this.LuePTKP.Size = new System.Drawing.Size(245, 20);
            this.LuePTKP.TabIndex = 93;
            this.LuePTKP.ToolTip = "F4 : Show/hide list";
            this.LuePTKP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePTKP.EditValueChanged += new System.EventHandler(this.LuePTKP_EditValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(68, 408);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 14);
            this.label17.TabIndex = 104;
            this.label17.Text = "Mobile";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(36, 220);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 14);
            this.label19.TabIndex = 86;
            this.label19.Text = "Payroll Type";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrollType
            // 
            this.LuePayrollType.EnterMoveNextControl = true;
            this.LuePayrollType.Location = new System.Drawing.Point(115, 216);
            this.LuePayrollType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePayrollType.Name = "LuePayrollType";
            this.LuePayrollType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.Appearance.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrollType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrollType.Properties.DropDownRows = 30;
            this.LuePayrollType.Properties.NullText = "[Empty]";
            this.LuePayrollType.Properties.PopupWidth = 300;
            this.LuePayrollType.Size = new System.Drawing.Size(245, 20);
            this.LuePayrollType.TabIndex = 87;
            this.LuePayrollType.ToolTip = "F4 : Show/hide list";
            this.LuePayrollType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePayrollType.EditValueChanged += new System.EventHandler(this.LuePayrollType_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(68, 262);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 14);
            this.label20.TabIndex = 90;
            this.label20.Text = "NPWP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(62, 52);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 69;
            this.label7.Text = "Gender";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNPWP
            // 
            this.TxtNPWP.EnterMoveNextControl = true;
            this.TxtNPWP.Location = new System.Drawing.Point(115, 258);
            this.TxtNPWP.Name = "TxtNPWP";
            this.TxtNPWP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNPWP.Properties.Appearance.Options.UseFont = true;
            this.TxtNPWP.Properties.MaxLength = 40;
            this.TxtNPWP.Size = new System.Drawing.Size(245, 20);
            this.TxtNPWP.TabIndex = 91;
            // 
            // LueGender
            // 
            this.LueGender.EnterMoveNextControl = true;
            this.LueGender.Location = new System.Drawing.Point(115, 49);
            this.LueGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueGender.Name = "LueGender";
            this.LueGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.Appearance.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGender.Properties.DropDownRows = 30;
            this.LueGender.Properties.MaxLength = 1;
            this.LueGender.Properties.NullText = "[Empty]";
            this.LueGender.Properties.PopupWidth = 300;
            this.LueGender.Size = new System.Drawing.Size(245, 20);
            this.LueGender.TabIndex = 70;
            this.LueGender.ToolTip = "F4 : Show/hide list";
            this.LueGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGender.EditValueChanged += new System.EventHandler(this.LueGender_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(67, 387);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 14);
            this.label16.TabIndex = 102;
            this.label16.Text = "Phone";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(115, 384);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 80;
            this.TxtPhone.Size = new System.Drawing.Size(245, 20);
            this.TxtPhone.TabIndex = 103;
            this.TxtPhone.Validated += new System.EventHandler(this.TxtPhone_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(93, 323);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 14);
            this.label9.TabIndex = 51;
            this.label9.Text = "City";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdNumber
            // 
            this.TxtIdNumber.EnterMoveNextControl = true;
            this.TxtIdNumber.Location = new System.Drawing.Point(125, 447);
            this.TxtIdNumber.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdNumber.Name = "TxtIdNumber";
            this.TxtIdNumber.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdNumber.Properties.Appearance.Options.UseFont = true;
            this.TxtIdNumber.Properties.MaxLength = 40;
            this.TxtIdNumber.Size = new System.Drawing.Size(257, 20);
            this.TxtIdNumber.TabIndex = 64;
            // 
            // LueCity
            // 
            this.LueCity.EnterMoveNextControl = true;
            this.LueCity.Location = new System.Drawing.Point(125, 321);
            this.LueCity.Margin = new System.Windows.Forms.Padding(5);
            this.LueCity.Name = "LueCity";
            this.LueCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.Appearance.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCity.Properties.DropDownRows = 30;
            this.LueCity.Properties.MaxLength = 16;
            this.LueCity.Properties.NullText = "[Empty]";
            this.LueCity.Properties.PopupWidth = 300;
            this.LueCity.Size = new System.Drawing.Size(257, 20);
            this.LueCity.TabIndex = 52;
            this.LueCity.ToolTip = "F4 : Show/hide list";
            this.LueCity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCity.EditValueChanged += new System.EventHandler(this.LueCity_EditValueChanged);
            // 
            // DteResignDt
            // 
            this.DteResignDt.EditValue = null;
            this.DteResignDt.EnterMoveNextControl = true;
            this.DteResignDt.Location = new System.Drawing.Point(125, 279);
            this.DteResignDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteResignDt.Name = "DteResignDt";
            this.DteResignDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.Appearance.Options.UseFont = true;
            this.DteResignDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteResignDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteResignDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteResignDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteResignDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteResignDt.Properties.MaxLength = 16;
            this.DteResignDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteResignDt.Size = new System.Drawing.Size(100, 20);
            this.DteResignDt.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(61, 450);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 14);
            this.label11.TabIndex = 63;
            this.label11.Text = "Identity#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(48, 282);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 14);
            this.label10.TabIndex = 47;
            this.label10.Text = "Resign Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteJoinDt
            // 
            this.DteJoinDt.EditValue = null;
            this.DteJoinDt.EnterMoveNextControl = true;
            this.DteJoinDt.Location = new System.Drawing.Point(125, 258);
            this.DteJoinDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJoinDt.Name = "DteJoinDt";
            this.DteJoinDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.Appearance.Options.UseFont = true;
            this.DteJoinDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJoinDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJoinDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJoinDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJoinDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJoinDt.Properties.MaxLength = 16;
            this.DteJoinDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJoinDt.Size = new System.Drawing.Size(100, 20);
            this.DteJoinDt.TabIndex = 46;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(62, 261);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 14);
            this.label6.TabIndex = 45;
            this.label6.Text = "Join Date";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.EnterMoveNextControl = true;
            this.TxtUserCode.Location = new System.Drawing.Point(125, 6);
            this.TxtUserCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUserCode.Properties.MaxLength = 50;
            this.TxtUserCode.Size = new System.Drawing.Size(257, 20);
            this.TxtUserCode.TabIndex = 22;
            this.TxtUserCode.Validated += new System.EventHandler(this.TxtUserCode_Validated);
            // 
            // TxtPostalCode
            // 
            this.TxtPostalCode.EnterMoveNextControl = true;
            this.TxtPostalCode.Location = new System.Drawing.Point(125, 405);
            this.TxtPostalCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCode.Name = "TxtPostalCode";
            this.TxtPostalCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCode.Properties.MaxLength = 16;
            this.TxtPostalCode.Size = new System.Drawing.Size(257, 20);
            this.TxtPostalCode.TabIndex = 60;
            this.TxtPostalCode.Validated += new System.EventHandler(this.TxtPostalCode_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(57, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 21;
            this.label3.Text = "User Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(49, 408);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 14);
            this.label15.TabIndex = 59;
            this.label15.Text = "Postal Code";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(125, 300);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 80;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(257, 20);
            this.MeeAddress.TabIndex = 50;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(70, 303);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 14);
            this.label8.TabIndex = 49;
            this.label8.Text = "Address";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(71, 176);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 37;
            this.label5.Text = "Position";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(125, 174);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 30;
            this.LuePosCode.Properties.MaxLength = 16;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 300;
            this.LuePosCode.Size = new System.Drawing.Size(257, 20);
            this.LuePosCode.TabIndex = 38;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(47, 156);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 14);
            this.label4.TabIndex = 35;
            this.label4.Text = "Department";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(125, 153);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(257, 20);
            this.LueDeptCode.TabIndex = 36;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // TpgEmployeeFamily
            // 
            this.TpgEmployeeFamily.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgEmployeeFamily.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgEmployeeFamily.Controls.Add(this.splitContainer2);
            this.TpgEmployeeFamily.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeFamily.Name = "TpgEmployeeFamily";
            this.TpgEmployeeFamily.Size = new System.Drawing.Size(764, 83);
            this.TpgEmployeeFamily.TabIndex = 1;
            this.TpgEmployeeFamily.Text = "Familly & Personal Reference";
            this.TpgEmployeeFamily.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.LueEducationLevelCode);
            this.splitContainer2.Panel1.Controls.Add(this.LueProfessionCode);
            this.splitContainer2.Panel1.Controls.Add(this.DteFamBirthDt);
            this.splitContainer2.Panel1.Controls.Add(this.LueFamGender);
            this.splitContainer2.Panel1.Controls.Add(this.LueStatus);
            this.splitContainer2.Panel1.Controls.Add(this.Grd1);
            this.splitContainer2.Panel1.Controls.Add(this.BtnFamily);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.Grd7);
            this.splitContainer2.Panel2.Controls.Add(this.button1);
            this.splitContainer2.Size = new System.Drawing.Size(760, 79);
            this.splitContainer2.SplitterDistance = 31;
            this.splitContainer2.TabIndex = 41;
            // 
            // LueEducationLevelCode
            // 
            this.LueEducationLevelCode.EnterMoveNextControl = true;
            this.LueEducationLevelCode.Location = new System.Drawing.Point(624, 44);
            this.LueEducationLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEducationLevelCode.Name = "LueEducationLevelCode";
            this.LueEducationLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.Appearance.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEducationLevelCode.Properties.DropDownRows = 25;
            this.LueEducationLevelCode.Properties.NullText = "[Empty]";
            this.LueEducationLevelCode.Properties.PopupWidth = 500;
            this.LueEducationLevelCode.Size = new System.Drawing.Size(131, 20);
            this.LueEducationLevelCode.TabIndex = 46;
            this.LueEducationLevelCode.ToolTip = "F4 : Show/hide list";
            this.LueEducationLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEducationLevelCode.EditValueChanged += new System.EventHandler(this.LueEducationLevelCode_EditValueChanged);
            this.LueEducationLevelCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEducationLevelCode_KeyDown);
            this.LueEducationLevelCode.Leave += new System.EventHandler(this.LueEducationLevelCode_Leave);
            // 
            // LueProfessionCode
            // 
            this.LueProfessionCode.EnterMoveNextControl = true;
            this.LueProfessionCode.Location = new System.Drawing.Point(483, 44);
            this.LueProfessionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProfessionCode.Name = "LueProfessionCode";
            this.LueProfessionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.Appearance.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProfessionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProfessionCode.Properties.DropDownRows = 25;
            this.LueProfessionCode.Properties.NullText = "[Empty]";
            this.LueProfessionCode.Properties.PopupWidth = 500;
            this.LueProfessionCode.Size = new System.Drawing.Size(131, 20);
            this.LueProfessionCode.TabIndex = 45;
            this.LueProfessionCode.ToolTip = "F4 : Show/hide list";
            this.LueProfessionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProfessionCode.EditValueChanged += new System.EventHandler(this.LueProfessionCode_EditValueChanged);
            this.LueProfessionCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueProfessionCode_KeyDown);
            this.LueProfessionCode.Leave += new System.EventHandler(this.LueProfessionCode_Leave);
            this.LueProfessionCode.Validated += new System.EventHandler(this.LueProfessionCode_Validated);
            // 
            // DteFamBirthDt
            // 
            this.DteFamBirthDt.EditValue = null;
            this.DteFamBirthDt.EnterMoveNextControl = true;
            this.DteFamBirthDt.Location = new System.Drawing.Point(369, 44);
            this.DteFamBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteFamBirthDt.Name = "DteFamBirthDt";
            this.DteFamBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteFamBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteFamBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteFamBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteFamBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteFamBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteFamBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteFamBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteFamBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteFamBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteFamBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteFamBirthDt.Size = new System.Drawing.Size(104, 20);
            this.DteFamBirthDt.TabIndex = 44;
            this.DteFamBirthDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteFamBirthDt_KeyDown);
            this.DteFamBirthDt.Leave += new System.EventHandler(this.DteFamBirthDt_Leave);
            this.DteFamBirthDt.Validated += new System.EventHandler(this.DteFamBirthDt_Validated);
            // 
            // LueFamGender
            // 
            this.LueFamGender.EnterMoveNextControl = true;
            this.LueFamGender.Location = new System.Drawing.Point(239, 44);
            this.LueFamGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueFamGender.Name = "LueFamGender";
            this.LueFamGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.Appearance.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFamGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFamGender.Properties.DropDownRows = 30;
            this.LueFamGender.Properties.NullText = "[Empty]";
            this.LueFamGender.Properties.PopupWidth = 300;
            this.LueFamGender.Size = new System.Drawing.Size(122, 20);
            this.LueFamGender.TabIndex = 43;
            this.LueFamGender.ToolTip = "F4 : Show/hide list";
            this.LueFamGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFamGender.EditValueChanged += new System.EventHandler(this.LueFamGender_EditValueChanged);
            this.LueFamGender.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueFamGender_KeyDown);
            this.LueFamGender.Leave += new System.EventHandler(this.LueFamGender_Leave);
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(95, 44);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 30;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 300;
            this.LueStatus.Size = new System.Drawing.Size(131, 20);
            this.LueStatus.TabIndex = 42;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            this.LueStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueStatus_KeyDown);
            this.LueStatus.Leave += new System.EventHandler(this.LueStatus_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 23);
            this.Grd1.Name = "Grd1";
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(760, 8);
            this.Grd1.TabIndex = 41;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // BtnFamily
            // 
            this.BtnFamily.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFamily.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnFamily.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFamily.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFamily.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnFamily.Location = new System.Drawing.Point(0, 0);
            this.BtnFamily.Name = "BtnFamily";
            this.BtnFamily.Size = new System.Drawing.Size(760, 23);
            this.BtnFamily.TabIndex = 36;
            this.BtnFamily.Text = "Family";
            this.BtnFamily.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnFamily.UseVisualStyleBackColor = false;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 23);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(760, 21);
            this.Grd7.TabIndex = 38;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.AliceBlue;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(760, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Personal Reference";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // TpgEmployeeWorkExp
            // 
            this.TpgEmployeeWorkExp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgEmployeeWorkExp.Controls.Add(this.Grd2);
            this.TpgEmployeeWorkExp.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeWorkExp.Name = "TpgEmployeeWorkExp";
            this.TpgEmployeeWorkExp.Size = new System.Drawing.Size(764, 83);
            this.TpgEmployeeWorkExp.TabIndex = 3;
            this.TpgEmployeeWorkExp.Text = "Work Experience";
            this.TpgEmployeeWorkExp.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 83);
            this.Grd2.TabIndex = 47;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TpgEmployeeEducation
            // 
            this.TpgEmployeeEducation.Controls.Add(this.LueFacCode);
            this.TpgEmployeeEducation.Controls.Add(this.LueField);
            this.TpgEmployeeEducation.Controls.Add(this.LueMajor);
            this.TpgEmployeeEducation.Controls.Add(this.LueLevel);
            this.TpgEmployeeEducation.Controls.Add(this.Grd3);
            this.TpgEmployeeEducation.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeEducation.Name = "TpgEmployeeEducation";
            this.TpgEmployeeEducation.Size = new System.Drawing.Size(764, 83);
            this.TpgEmployeeEducation.TabIndex = 4;
            this.TpgEmployeeEducation.Text = "Education";
            this.TpgEmployeeEducation.UseVisualStyleBackColor = true;
            // 
            // LueFacCode
            // 
            this.LueFacCode.EnterMoveNextControl = true;
            this.LueFacCode.Location = new System.Drawing.Point(526, 18);
            this.LueFacCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueFacCode.Name = "LueFacCode";
            this.LueFacCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.Appearance.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFacCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFacCode.Properties.DropDownRows = 25;
            this.LueFacCode.Properties.NullText = "[Empty]";
            this.LueFacCode.Properties.PopupWidth = 500;
            this.LueFacCode.Size = new System.Drawing.Size(118, 20);
            this.LueFacCode.TabIndex = 28;
            this.LueFacCode.ToolTip = "F4 : Show/hide list";
            this.LueFacCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFacCode.EditValueChanged += new System.EventHandler(this.LueFacCode_EditValueChanged);
            this.LueFacCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueFacCode_KeyDown);
            this.LueFacCode.Leave += new System.EventHandler(this.LueFacCode_Leave);
            this.LueFacCode.Validated += new System.EventHandler(this.LueFacCode_Validated);
            // 
            // LueField
            // 
            this.LueField.EnterMoveNextControl = true;
            this.LueField.Location = new System.Drawing.Point(336, 20);
            this.LueField.Margin = new System.Windows.Forms.Padding(5);
            this.LueField.Name = "LueField";
            this.LueField.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.Appearance.Options.UseFont = true;
            this.LueField.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueField.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueField.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueField.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueField.Properties.DropDownRows = 25;
            this.LueField.Properties.NullText = "[Empty]";
            this.LueField.Properties.PopupWidth = 500;
            this.LueField.Size = new System.Drawing.Size(118, 20);
            this.LueField.TabIndex = 27;
            this.LueField.ToolTip = "F4 : Show/hide list";
            this.LueField.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueField.EditValueChanged += new System.EventHandler(this.LueField_EditValueChanged);
            this.LueField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueField_KeyDown);
            this.LueField.Leave += new System.EventHandler(this.LueField_Leave);
            this.LueField.Validated += new System.EventHandler(this.LueField_Validated);
            // 
            // LueMajor
            // 
            this.LueMajor.EnterMoveNextControl = true;
            this.LueMajor.Location = new System.Drawing.Point(202, 22);
            this.LueMajor.Margin = new System.Windows.Forms.Padding(5);
            this.LueMajor.Name = "LueMajor";
            this.LueMajor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.Appearance.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMajor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMajor.Properties.DropDownRows = 30;
            this.LueMajor.Properties.NullText = "[Empty]";
            this.LueMajor.Properties.PopupWidth = 300;
            this.LueMajor.Size = new System.Drawing.Size(111, 20);
            this.LueMajor.TabIndex = 17;
            this.LueMajor.ToolTip = "F4 : Show/hide list";
            this.LueMajor.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMajor.EditValueChanged += new System.EventHandler(this.LueMajor_EditValueChanged);
            this.LueMajor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueMajor_KeyDown);
            this.LueMajor.Leave += new System.EventHandler(this.LueMajor_Leave);
            this.LueMajor.Validated += new System.EventHandler(this.LueMajor_Validated);
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(72, 22);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 30;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 300;
            this.LueLevel.Size = new System.Drawing.Size(113, 20);
            this.LueLevel.TabIndex = 16;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel.EditValueChanged += new System.EventHandler(this.LueLevel_EditValueChanged);
            this.LueLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLevel_KeyDown);
            this.LueLevel.Leave += new System.EventHandler(this.LueLevel_Leave);
            this.LueLevel.Validated += new System.EventHandler(this.LueLevel_Validated);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(764, 83);
            this.Grd3.TabIndex = 15;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgEmployeeCompetence
            // 
            this.TpgEmployeeCompetence.Controls.Add(this.LueCompetenceCode);
            this.TpgEmployeeCompetence.Controls.Add(this.Grd5);
            this.TpgEmployeeCompetence.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeCompetence.Name = "TpgEmployeeCompetence";
            this.TpgEmployeeCompetence.Size = new System.Drawing.Size(764, 83);
            this.TpgEmployeeCompetence.TabIndex = 9;
            this.TpgEmployeeCompetence.Text = "Competence";
            this.TpgEmployeeCompetence.UseVisualStyleBackColor = true;
            // 
            // LueCompetenceCode
            // 
            this.LueCompetenceCode.EnterMoveNextControl = true;
            this.LueCompetenceCode.Location = new System.Drawing.Point(226, 21);
            this.LueCompetenceCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompetenceCode.Name = "LueCompetenceCode";
            this.LueCompetenceCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.Appearance.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompetenceCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompetenceCode.Properties.DropDownRows = 30;
            this.LueCompetenceCode.Properties.NullText = "[Empty]";
            this.LueCompetenceCode.Properties.PopupWidth = 300;
            this.LueCompetenceCode.Size = new System.Drawing.Size(300, 20);
            this.LueCompetenceCode.TabIndex = 16;
            this.LueCompetenceCode.ToolTip = "F4 : Show/hide list";
            this.LueCompetenceCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompetenceCode.EditValueChanged += new System.EventHandler(this.LueCompetenceCode_EditValueChanged);
            this.LueCompetenceCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCompetenceCode_KeyDown);
            this.LueCompetenceCode.Leave += new System.EventHandler(this.LueCompetenceCode_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(764, 83);
            this.Grd5.TabIndex = 15;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd5_AfterCommitEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpgTraining
            // 
            this.TpgTraining.Controls.Add(this.LueTrainingCode);
            this.TpgTraining.Controls.Add(this.Grd4);
            this.TpgTraining.Location = new System.Drawing.Point(4, 51);
            this.TpgTraining.Name = "TpgTraining";
            this.TpgTraining.Size = new System.Drawing.Size(764, 83);
            this.TpgTraining.TabIndex = 5;
            this.TpgTraining.Text = "Training";
            this.TpgTraining.UseVisualStyleBackColor = true;
            // 
            // LueTrainingCode
            // 
            this.LueTrainingCode.EnterMoveNextControl = true;
            this.LueTrainingCode.Location = new System.Drawing.Point(71, 23);
            this.LueTrainingCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingCode.Name = "LueTrainingCode";
            this.LueTrainingCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingCode.Properties.DropDownRows = 25;
            this.LueTrainingCode.Properties.NullText = "[Empty]";
            this.LueTrainingCode.Properties.PopupWidth = 500;
            this.LueTrainingCode.Size = new System.Drawing.Size(131, 20);
            this.LueTrainingCode.TabIndex = 32;
            this.LueTrainingCode.ToolTip = "F4 : Show/hide list";
            this.LueTrainingCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingCode.EditValueChanged += new System.EventHandler(this.LueTrainingCode_EditValueChanged);
            this.LueTrainingCode.Leave += new System.EventHandler(this.LueTrainingCode_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(764, 83);
            this.Grd4.TabIndex = 31;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgEmployeeSS
            // 
            this.TpgEmployeeSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgEmployeeSS.Controls.Add(this.TxtGrossSalary);
            this.TpgEmployeeSS.Controls.Add(this.LueDeptCode2);
            this.TpgEmployeeSS.Controls.Add(this.TxtProbation);
            this.TpgEmployeeSS.Controls.Add(this.DteStartWorkDt);
            this.TpgEmployeeSS.Controls.Add(this.TxtPlacement);
            this.TpgEmployeeSS.Controls.Add(this.DteInterviewDt);
            this.TpgEmployeeSS.Controls.Add(this.TxtInterviewerBy);
            this.TpgEmployeeSS.Controls.Add(this.TxtPlace);
            this.TpgEmployeeSS.Controls.Add(this.label167);
            this.TpgEmployeeSS.Controls.Add(this.label166);
            this.TpgEmployeeSS.Controls.Add(this.label165);
            this.TpgEmployeeSS.Controls.Add(this.label164);
            this.TpgEmployeeSS.Controls.Add(this.label163);
            this.TpgEmployeeSS.Controls.Add(this.label162);
            this.TpgEmployeeSS.Controls.Add(this.label161);
            this.TpgEmployeeSS.Controls.Add(this.label160);
            this.TpgEmployeeSS.Controls.Add(this.label159);
            this.TpgEmployeeSS.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeSS.Name = "TpgEmployeeSS";
            this.TpgEmployeeSS.Size = new System.Drawing.Size(764, 450);
            this.TpgEmployeeSS.TabIndex = 8;
            this.TpgEmployeeSS.Text = "Company";
            this.TpgEmployeeSS.UseVisualStyleBackColor = true;
            // 
            // TxtGrossSalary
            // 
            this.TxtGrossSalary.EnterMoveNextControl = true;
            this.TxtGrossSalary.Location = new System.Drawing.Point(102, 129);
            this.TxtGrossSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrossSalary.Name = "TxtGrossSalary";
            this.TxtGrossSalary.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGrossSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrossSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrossSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtGrossSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrossSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGrossSalary.Size = new System.Drawing.Size(128, 20);
            this.TxtGrossSalary.TabIndex = 17;
            this.TxtGrossSalary.Validated += new System.EventHandler(this.TxtGrossSalary_Validated);
            // 
            // LueDeptCode2
            // 
            this.LueDeptCode2.EnterMoveNextControl = true;
            this.LueDeptCode2.Location = new System.Drawing.Point(102, 193);
            this.LueDeptCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode2.Name = "LueDeptCode2";
            this.LueDeptCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode2.Properties.DropDownRows = 30;
            this.LueDeptCode2.Properties.NullText = "[Empty]";
            this.LueDeptCode2.Properties.PopupWidth = 300;
            this.LueDeptCode2.Size = new System.Drawing.Size(269, 20);
            this.LueDeptCode2.TabIndex = 23;
            this.LueDeptCode2.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode2.EditValueChanged += new System.EventHandler(this.LueDeptCode2_EditValueChanged);
            // 
            // TxtProbation
            // 
            this.TxtProbation.EnterMoveNextControl = true;
            this.TxtProbation.Location = new System.Drawing.Point(102, 214);
            this.TxtProbation.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProbation.Name = "TxtProbation";
            this.TxtProbation.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProbation.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProbation.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProbation.Properties.Appearance.Options.UseFont = true;
            this.TxtProbation.Properties.MaxLength = 13;
            this.TxtProbation.Size = new System.Drawing.Size(200, 20);
            this.TxtProbation.TabIndex = 25;
            // 
            // DteStartWorkDt
            // 
            this.DteStartWorkDt.EditValue = null;
            this.DteStartWorkDt.EnterMoveNextControl = true;
            this.DteStartWorkDt.Location = new System.Drawing.Point(102, 151);
            this.DteStartWorkDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartWorkDt.Name = "DteStartWorkDt";
            this.DteStartWorkDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartWorkDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartWorkDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartWorkDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartWorkDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartWorkDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartWorkDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartWorkDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartWorkDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartWorkDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartWorkDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartWorkDt.Size = new System.Drawing.Size(111, 20);
            this.DteStartWorkDt.TabIndex = 19;
            // 
            // TxtPlacement
            // 
            this.TxtPlacement.EnterMoveNextControl = true;
            this.TxtPlacement.Location = new System.Drawing.Point(102, 172);
            this.TxtPlacement.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlacement.Name = "TxtPlacement";
            this.TxtPlacement.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPlacement.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlacement.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlacement.Properties.Appearance.Options.UseFont = true;
            this.TxtPlacement.Properties.MaxLength = 40;
            this.TxtPlacement.Size = new System.Drawing.Size(269, 20);
            this.TxtPlacement.TabIndex = 21;
            // 
            // DteInterviewDt
            // 
            this.DteInterviewDt.EditValue = null;
            this.DteInterviewDt.EnterMoveNextControl = true;
            this.DteInterviewDt.Location = new System.Drawing.Point(102, 93);
            this.DteInterviewDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteInterviewDt.Name = "DteInterviewDt";
            this.DteInterviewDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteInterviewDt.Properties.Appearance.Options.UseFont = true;
            this.DteInterviewDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteInterviewDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteInterviewDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteInterviewDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteInterviewDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteInterviewDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteInterviewDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteInterviewDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteInterviewDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteInterviewDt.Size = new System.Drawing.Size(111, 20);
            this.DteInterviewDt.TabIndex = 15;
            // 
            // TxtInterviewerBy
            // 
            this.TxtInterviewerBy.EnterMoveNextControl = true;
            this.TxtInterviewerBy.Location = new System.Drawing.Point(102, 51);
            this.TxtInterviewerBy.Name = "TxtInterviewerBy";
            this.TxtInterviewerBy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterviewerBy.Properties.Appearance.Options.UseFont = true;
            this.TxtInterviewerBy.Properties.MaxLength = 30;
            this.TxtInterviewerBy.Size = new System.Drawing.Size(200, 20);
            this.TxtInterviewerBy.TabIndex = 11;
            // 
            // TxtPlace
            // 
            this.TxtPlace.EnterMoveNextControl = true;
            this.TxtPlace.Location = new System.Drawing.Point(102, 72);
            this.TxtPlace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlace.Name = "TxtPlace";
            this.TxtPlace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPlace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlace.Properties.Appearance.Options.UseFont = true;
            this.TxtPlace.Properties.MaxLength = 40;
            this.TxtPlace.Size = new System.Drawing.Size(272, 20);
            this.TxtPlace.TabIndex = 13;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label167.ForeColor = System.Drawing.Color.Black;
            this.label167.Location = new System.Drawing.Point(26, 196);
            this.label167.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(73, 14);
            this.label167.TabIndex = 22;
            this.label167.Text = "Department";
            this.label167.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.ForeColor = System.Drawing.Color.Black;
            this.label166.Location = new System.Drawing.Point(40, 216);
            this.label166.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(59, 14);
            this.label166.TabIndex = 24;
            this.label166.Text = "Probation";
            this.label166.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label165.ForeColor = System.Drawing.Color.Black;
            this.label165.Location = new System.Drawing.Point(35, 174);
            this.label165.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(64, 14);
            this.label165.TabIndex = 20;
            this.label165.Text = "Placement";
            this.label165.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.ForeColor = System.Drawing.Color.Black;
            this.label164.Location = new System.Drawing.Point(32, 153);
            this.label164.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(67, 14);
            this.label164.TabIndex = 18;
            this.label164.Text = "Start Work";
            this.label164.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label163.ForeColor = System.Drawing.Color.Black;
            this.label163.Location = new System.Drawing.Point(28, 132);
            this.label163.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(71, 14);
            this.label163.TabIndex = 16;
            this.label163.Text = "Gross Salary";
            this.label163.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.ForeColor = System.Drawing.Color.Black;
            this.label162.Location = new System.Drawing.Point(66, 96);
            this.label162.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(33, 14);
            this.label162.TabIndex = 14;
            this.label162.Text = "Date";
            this.label162.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label161.ForeColor = System.Drawing.Color.Black;
            this.label161.Location = new System.Drawing.Point(64, 75);
            this.label161.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(35, 14);
            this.label161.TabIndex = 12;
            this.label161.Text = "Place";
            this.label161.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.ForeColor = System.Drawing.Color.Black;
            this.label160.Location = new System.Drawing.Point(12, 54);
            this.label160.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(87, 14);
            this.label160.TabIndex = 10;
            this.label160.Text = "Interviewer By";
            this.label160.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(8, 22);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(114, 14);
            this.label159.TabIndex = 11;
            this.label159.Text = "Create By Company";
            // 
            // TpPicture
            // 
            this.TpPicture.Controls.Add(this.panel10);
            this.TpPicture.Controls.Add(this.panel9);
            this.TpPicture.Location = new System.Drawing.Point(4, 51);
            this.TpPicture.Name = "TpPicture";
            this.TpPicture.Padding = new System.Windows.Forms.Padding(3);
            this.TpPicture.Size = new System.Drawing.Size(764, 83);
            this.TpPicture.TabIndex = 10;
            this.TpPicture.Text = "Picture & File";
            this.TpPicture.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.Grd6);
            this.panel10.Controls.Add(this.button2);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel10.Location = new System.Drawing.Point(376, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(385, 77);
            this.panel10.TabIndex = 1;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 23);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(385, 54);
            this.Grd6.TabIndex = 29;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            this.Grd6.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd6_RequestCellToolTipText);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.SteelBlue;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.AliceBlue;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(385, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "Attachment File";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.BtnDownload);
            this.panel9.Controls.Add(this.BtnPicture);
            this.panel9.Controls.Add(this.button4);
            this.panel9.Controls.Add(this.PicEmployee);
            this.panel9.Controls.Add(this.button3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(369, 77);
            this.panel9.TabIndex = 0;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(56, 29);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 36;
            this.BtnDownload.ToolTip = "Download Employee\'s Picture";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // BtnPicture
            // 
            this.BtnPicture.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPicture.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPicture.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPicture.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPicture.Appearance.Options.UseBackColor = true;
            this.BtnPicture.Appearance.Options.UseFont = true;
            this.BtnPicture.Appearance.Options.UseForeColor = true;
            this.BtnPicture.Appearance.Options.UseTextOptions = true;
            this.BtnPicture.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPicture.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPicture.Image = ((System.Drawing.Image)(resources.GetObject("BtnPicture.Image")));
            this.BtnPicture.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPicture.Location = new System.Drawing.Point(15, 29);
            this.BtnPicture.Name = "BtnPicture";
            this.BtnPicture.Size = new System.Drawing.Size(24, 21);
            this.BtnPicture.TabIndex = 35;
            this.BtnPicture.ToolTip = "Show Employee\'s Picture";
            this.BtnPicture.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPicture.ToolTipTitle = "Run System";
            this.BtnPicture.Click += new System.EventHandler(this.BtnPicture_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.AliceBlue;
            this.button4.Location = new System.Drawing.Point(0, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(369, 40);
            this.button4.TabIndex = 34;
            this.button4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // PicEmployee
            // 
            this.PicEmployee.Location = new System.Drawing.Point(6, 108);
            this.PicEmployee.Name = "PicEmployee";
            this.PicEmployee.Size = new System.Drawing.Size(318, 333);
            this.PicEmployee.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicEmployee.TabIndex = 33;
            this.PicEmployee.TabStop = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.SteelBlue;
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.AliceBlue;
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(369, 23);
            this.button3.TabIndex = 23;
            this.button3.Text = "Picture";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // TpgStage
            // 
            this.TpgStage.Controls.Add(this.DteStageDt);
            this.TpgStage.Controls.Add(this.LueProgress);
            this.TpgStage.Controls.Add(this.Grd8);
            this.TpgStage.Location = new System.Drawing.Point(4, 51);
            this.TpgStage.Name = "TpgStage";
            this.TpgStage.Size = new System.Drawing.Size(764, 450);
            this.TpgStage.TabIndex = 11;
            this.TpgStage.Text = "Stage";
            this.TpgStage.UseVisualStyleBackColor = true;
            // 
            // DteStageDt
            // 
            this.DteStageDt.EditValue = null;
            this.DteStageDt.EnterMoveNextControl = true;
            this.DteStageDt.Location = new System.Drawing.Point(226, 26);
            this.DteStageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStageDt.Name = "DteStageDt";
            this.DteStageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStageDt.Properties.Appearance.Options.UseFont = true;
            this.DteStageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStageDt.Size = new System.Drawing.Size(104, 20);
            this.DteStageDt.TabIndex = 50;
            this.DteStageDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteStageDt_KeyDown);
            this.DteStageDt.Leave += new System.EventHandler(this.DteStageDt_Leave);
            this.DteStageDt.Validated += new System.EventHandler(this.DteStageDt_Validated);
            // 
            // LueProgress
            // 
            this.LueProgress.EnterMoveNextControl = true;
            this.LueProgress.Location = new System.Drawing.Point(87, 26);
            this.LueProgress.Margin = new System.Windows.Forms.Padding(5);
            this.LueProgress.Name = "LueProgress";
            this.LueProgress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgress.Properties.Appearance.Options.UseFont = true;
            this.LueProgress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProgress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProgress.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgress.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProgress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProgress.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProgress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProgress.Properties.DropDownRows = 30;
            this.LueProgress.Properties.NullText = "[Empty]";
            this.LueProgress.Properties.PopupWidth = 300;
            this.LueProgress.Size = new System.Drawing.Size(131, 20);
            this.LueProgress.TabIndex = 49;
            this.LueProgress.ToolTip = "F4 : Show/hide list";
            this.LueProgress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProgress.EditValueChanged += new System.EventHandler(this.LueProgress_EditValueChanged);
            this.LueProgress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueProgress_KeyDown);
            this.LueProgress.Leave += new System.EventHandler(this.LueProgress_Leave);
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(764, 450);
            this.Grd8.TabIndex = 48;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // TpgEmployeeBenefit
            // 
            this.TpgEmployeeBenefit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgEmployeeBenefit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgEmployeeBenefit.Controls.Add(this.MeeRemark);
            this.TpgEmployeeBenefit.Controls.Add(this.label176);
            this.TpgEmployeeBenefit.Controls.Add(this.TxtTax);
            this.TpgEmployeeBenefit.Controls.Add(this.label175);
            this.TpgEmployeeBenefit.Controls.Add(this.TxtBonus);
            this.TpgEmployeeBenefit.Controls.Add(this.label174);
            this.TpgEmployeeBenefit.Controls.Add(this.TxtAnnualLeave);
            this.TpgEmployeeBenefit.Controls.Add(this.label173);
            this.TpgEmployeeBenefit.Controls.Add(this.TxtHolidayAllowance);
            this.TpgEmployeeBenefit.Controls.Add(this.label172);
            this.TpgEmployeeBenefit.Controls.Add(this.TxtInsurance);
            this.TpgEmployeeBenefit.Controls.Add(this.label171);
            this.TpgEmployeeBenefit.Controls.Add(this.TxtTHP);
            this.TpgEmployeeBenefit.Controls.Add(this.label170);
            this.TpgEmployeeBenefit.Controls.Add(this.TxtJobIncentives);
            this.TpgEmployeeBenefit.Controls.Add(this.label169);
            this.TpgEmployeeBenefit.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeBenefit.Name = "TpgEmployeeBenefit";
            this.TpgEmployeeBenefit.Size = new System.Drawing.Size(764, 450);
            this.TpgEmployeeBenefit.TabIndex = 12;
            this.TpgEmployeeBenefit.Text = "Employee Benefit";
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(111, 165);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 10000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(257, 20);
            this.MeeRemark.TabIndex = 66;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.ForeColor = System.Drawing.Color.Black;
            this.label176.Location = new System.Drawing.Point(60, 167);
            this.label176.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(47, 14);
            this.label176.TabIndex = 65;
            this.label176.Text = "Remark";
            this.label176.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTax
            // 
            this.TxtTax.EnterMoveNextControl = true;
            this.TxtTax.Location = new System.Drawing.Point(111, 144);
            this.TxtTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTax.Name = "TxtTax";
            this.TxtTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTax.Properties.MaxLength = 50;
            this.TxtTax.Size = new System.Drawing.Size(257, 20);
            this.TxtTax.TabIndex = 64;
            this.TxtTax.Validated += new System.EventHandler(this.TxtTax_Validated);
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.ForeColor = System.Drawing.Color.Black;
            this.label175.Location = new System.Drawing.Point(80, 148);
            this.label175.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(27, 14);
            this.label175.TabIndex = 63;
            this.label175.Text = "Tax";
            this.label175.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBonus
            // 
            this.TxtBonus.EnterMoveNextControl = true;
            this.TxtBonus.Location = new System.Drawing.Point(111, 123);
            this.TxtBonus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBonus.Name = "TxtBonus";
            this.TxtBonus.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBonus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBonus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBonus.Properties.Appearance.Options.UseFont = true;
            this.TxtBonus.Properties.MaxLength = 50;
            this.TxtBonus.Size = new System.Drawing.Size(257, 20);
            this.TxtBonus.TabIndex = 62;
            this.TxtBonus.Validated += new System.EventHandler(this.TxtBonus_Validated);
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.ForeColor = System.Drawing.Color.Black;
            this.label174.Location = new System.Drawing.Point(67, 126);
            this.label174.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(40, 14);
            this.label174.TabIndex = 61;
            this.label174.Text = "Bonus";
            this.label174.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAnnualLeave
            // 
            this.TxtAnnualLeave.EnterMoveNextControl = true;
            this.TxtAnnualLeave.Location = new System.Drawing.Point(111, 102);
            this.TxtAnnualLeave.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAnnualLeave.Name = "TxtAnnualLeave";
            this.TxtAnnualLeave.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAnnualLeave.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAnnualLeave.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAnnualLeave.Properties.Appearance.Options.UseFont = true;
            this.TxtAnnualLeave.Properties.MaxLength = 50;
            this.TxtAnnualLeave.Size = new System.Drawing.Size(257, 20);
            this.TxtAnnualLeave.TabIndex = 60;
            this.TxtAnnualLeave.Validated += new System.EventHandler(this.TxtAnnualLeave_Validated);
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.ForeColor = System.Drawing.Color.Black;
            this.label173.Location = new System.Drawing.Point(28, 105);
            this.label173.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(80, 14);
            this.label173.TabIndex = 59;
            this.label173.Text = "Annual Leave";
            this.label173.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtHolidayAllowance
            // 
            this.TxtHolidayAllowance.EnterMoveNextControl = true;
            this.TxtHolidayAllowance.Location = new System.Drawing.Point(111, 81);
            this.TxtHolidayAllowance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtHolidayAllowance.Name = "TxtHolidayAllowance";
            this.TxtHolidayAllowance.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtHolidayAllowance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtHolidayAllowance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtHolidayAllowance.Properties.Appearance.Options.UseFont = true;
            this.TxtHolidayAllowance.Properties.MaxLength = 50;
            this.TxtHolidayAllowance.Size = new System.Drawing.Size(257, 20);
            this.TxtHolidayAllowance.TabIndex = 58;
            this.TxtHolidayAllowance.Validated += new System.EventHandler(this.TxtHolidayAllowance_Validated);
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.ForeColor = System.Drawing.Color.Black;
            this.label172.Location = new System.Drawing.Point(4, 85);
            this.label172.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(104, 14);
            this.label172.TabIndex = 57;
            this.label172.Text = "Holiday Allowance";
            this.label172.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInsurance
            // 
            this.TxtInsurance.EnterMoveNextControl = true;
            this.TxtInsurance.Location = new System.Drawing.Point(111, 60);
            this.TxtInsurance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInsurance.Name = "TxtInsurance";
            this.TxtInsurance.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInsurance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInsurance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInsurance.Properties.Appearance.Options.UseFont = true;
            this.TxtInsurance.Properties.MaxLength = 50;
            this.TxtInsurance.Size = new System.Drawing.Size(257, 20);
            this.TxtInsurance.TabIndex = 56;
            this.TxtInsurance.Validated += new System.EventHandler(this.TxtInsurance_Validated);
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.Black;
            this.label171.Location = new System.Drawing.Point(48, 63);
            this.label171.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(60, 14);
            this.label171.TabIndex = 55;
            this.label171.Text = "Insurance";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTHP
            // 
            this.TxtTHP.EnterMoveNextControl = true;
            this.TxtTHP.Location = new System.Drawing.Point(111, 39);
            this.TxtTHP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTHP.Name = "TxtTHP";
            this.TxtTHP.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTHP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTHP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTHP.Properties.Appearance.Options.UseFont = true;
            this.TxtTHP.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTHP.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTHP.Properties.MaxLength = 50;
            this.TxtTHP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TxtTHP.Size = new System.Drawing.Size(257, 20);
            this.TxtTHP.TabIndex = 54;
            this.TxtTHP.Validated += new System.EventHandler(this.TxtTHP_Validated);
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.ForeColor = System.Drawing.Color.Black;
            this.label170.Location = new System.Drawing.Point(46, 42);
            this.label170.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(62, 14);
            this.label170.TabIndex = 53;
            this.label170.Text = "Total THP";
            this.label170.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJobIncentives
            // 
            this.TxtJobIncentives.EnterMoveNextControl = true;
            this.TxtJobIncentives.Location = new System.Drawing.Point(111, 18);
            this.TxtJobIncentives.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJobIncentives.Name = "TxtJobIncentives";
            this.TxtJobIncentives.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtJobIncentives.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJobIncentives.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJobIncentives.Properties.Appearance.Options.UseFont = true;
            this.TxtJobIncentives.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtJobIncentives.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtJobIncentives.Properties.MaxLength = 50;
            this.TxtJobIncentives.Size = new System.Drawing.Size(257, 20);
            this.TxtJobIncentives.TabIndex = 52;
            this.TxtJobIncentives.Validated += new System.EventHandler(this.TxtJobIncentives_Validated);
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.ForeColor = System.Drawing.Color.Black;
            this.label169.Location = new System.Drawing.Point(22, 22);
            this.label169.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(86, 14);
            this.label169.TabIndex = 51;
            this.label169.Text = "Job Incentives";
            this.label169.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.textEdit6);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.textEdit7);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.textEdit8);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.textEdit9);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.lookUpEdit6);
            this.tabPage1.Controls.Add(this.dateEdit1);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.textEdit10);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.lookUpEdit7);
            this.tabPage1.Controls.Add(this.textEdit11);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.dateEdit2);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.dateEdit3);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.textEdit12);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.memoExEdit1);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.label44);
            this.tabPage1.Controls.Add(this.lookUpEdit8);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.lookUpEdit9);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.lookUpEdit10);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(764, 373);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.textEdit3);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.textEdit4);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.lookUpEdit3);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.lookUpEdit4);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.lookUpEdit5);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.textEdit5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(408, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(352, 369);
            this.panel6.TabIndex = 46;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(93, 115);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.MaxLength = 80;
            this.textEdit3.Size = new System.Drawing.Size(243, 20);
            this.textEdit3.TabIndex = 57;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(17, 119);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 14);
            this.label25.TabIndex = 56;
            this.label25.Text = "Bank Branch";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit4
            // 
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(93, 93);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.MaxLength = 80;
            this.textEdit4.Size = new System.Drawing.Size(242, 20);
            this.textEdit4.TabIndex = 55;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(8, 97);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 14);
            this.label26.TabIndex = 54;
            this.label26.Text = "Bank Account";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(23, 75);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 14);
            this.label27.TabIndex = 52;
            this.label27.Text = "Bank Name";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.EnterMoveNextControl = true;
            this.lookUpEdit3.Location = new System.Drawing.Point(93, 71);
            this.lookUpEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.DropDownRows = 12;
            this.lookUpEdit3.Properties.NullText = "[Empty]";
            this.lookUpEdit3.Properties.PopupWidth = 500;
            this.lookUpEdit3.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit3.TabIndex = 53;
            this.lookUpEdit3.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(55, 53);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(36, 14);
            this.label28.TabIndex = 50;
            this.label28.Text = "PTKP";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(93, 49);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 12;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 500;
            this.lookUpEdit4.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit4.TabIndex = 51;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(18, 31);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 14);
            this.label29.TabIndex = 48;
            this.label29.Text = "Payroll Type";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit5
            // 
            this.lookUpEdit5.EnterMoveNextControl = true;
            this.lookUpEdit5.Location = new System.Drawing.Point(93, 27);
            this.lookUpEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit5.Name = "lookUpEdit5";
            this.lookUpEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit5.Properties.DropDownRows = 12;
            this.lookUpEdit5.Properties.NullText = "[Empty]";
            this.lookUpEdit5.Properties.PopupWidth = 500;
            this.lookUpEdit5.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit5.TabIndex = 49;
            this.lookUpEdit5.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(50, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 14);
            this.label30.TabIndex = 46;
            this.label30.Text = "NPWP";
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(93, 5);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.MaxLength = 40;
            this.textEdit5.Size = new System.Drawing.Size(243, 20);
            this.textEdit5.TabIndex = 47;
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(94, 334);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.MaxLength = 40;
            this.textEdit6.Size = new System.Drawing.Size(219, 20);
            this.textEdit6.TabIndex = 45;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(58, 337);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 14);
            this.label31.TabIndex = 44;
            this.label31.Text = "Email";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit7
            // 
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(94, 312);
            this.textEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.MaxLength = 40;
            this.textEdit7.Size = new System.Drawing.Size(185, 20);
            this.textEdit7.TabIndex = 43;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(51, 315);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 14);
            this.label32.TabIndex = 42;
            this.label32.Text = "Mobile";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit8
            // 
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(94, 290);
            this.textEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.MaxLength = 40;
            this.textEdit8.Size = new System.Drawing.Size(185, 20);
            this.textEdit8.TabIndex = 41;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(50, 293);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 14);
            this.label33.TabIndex = 40;
            this.label33.Text = "Phone";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit9
            // 
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(94, 268);
            this.textEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.MaxLength = 20;
            this.textEdit9.Size = new System.Drawing.Size(185, 20);
            this.textEdit9.TabIndex = 39;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(21, 271);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 14);
            this.label34.TabIndex = 38;
            this.label34.Text = "Postal Code";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(65, 249);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(27, 14);
            this.label35.TabIndex = 36;
            this.label35.Text = "City";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit6
            // 
            this.lookUpEdit6.EnterMoveNextControl = true;
            this.lookUpEdit6.Location = new System.Drawing.Point(94, 246);
            this.lookUpEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit6.Name = "lookUpEdit6";
            this.lookUpEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit6.Properties.DropDownRows = 12;
            this.lookUpEdit6.Properties.MaxLength = 16;
            this.lookUpEdit6.Properties.NullText = "[Empty]";
            this.lookUpEdit6.Properties.PopupWidth = 500;
            this.lookUpEdit6.Size = new System.Drawing.Size(295, 20);
            this.lookUpEdit6.TabIndex = 37;
            this.lookUpEdit6.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.EnterMoveNextControl = true;
            this.dateEdit1.Location = new System.Drawing.Point(94, 202);
            this.dateEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit1.Properties.MaxLength = 8;
            this.dateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(102, 20);
            this.dateEdit1.TabIndex = 33;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(30, 205);
            this.label36.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(62, 14);
            this.label36.TabIndex = 32;
            this.label36.Text = "Birth Date";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit10
            // 
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(94, 180);
            this.textEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.MaxLength = 80;
            this.textEdit10.Size = new System.Drawing.Size(298, 20);
            this.textEdit10.TabIndex = 31;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(28, 183);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 14);
            this.label37.TabIndex = 30;
            this.label37.Text = "Birth Place";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(44, 161);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(48, 14);
            this.label38.TabIndex = 28;
            this.label38.Text = "Religion";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit7
            // 
            this.lookUpEdit7.EnterMoveNextControl = true;
            this.lookUpEdit7.Location = new System.Drawing.Point(94, 158);
            this.lookUpEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit7.Name = "lookUpEdit7";
            this.lookUpEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit7.Properties.DropDownRows = 12;
            this.lookUpEdit7.Properties.MaxLength = 2;
            this.lookUpEdit7.Properties.NullText = "[Empty]";
            this.lookUpEdit7.Properties.PopupWidth = 500;
            this.lookUpEdit7.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit7.TabIndex = 29;
            this.lookUpEdit7.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit11
            // 
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(94, 114);
            this.textEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.MaxLength = 40;
            this.textEdit11.Size = new System.Drawing.Size(299, 20);
            this.textEdit11.TabIndex = 25;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(27, 117);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 14);
            this.label39.TabIndex = 24;
            this.label39.Text = "Id Number";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.EnterMoveNextControl = true;
            this.dateEdit2.Location = new System.Drawing.Point(94, 92);
            this.dateEdit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.Appearance.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit2.Properties.MaxLength = 16;
            this.dateEdit2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit2.Size = new System.Drawing.Size(99, 20);
            this.dateEdit2.TabIndex = 23;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(20, 95);
            this.label40.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(72, 14);
            this.label40.TabIndex = 22;
            this.label40.Text = "Resign Date";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit3
            // 
            this.dateEdit3.EditValue = null;
            this.dateEdit3.EnterMoveNextControl = true;
            this.dateEdit3.Location = new System.Drawing.Point(94, 70);
            this.dateEdit3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit3.Name = "dateEdit3";
            this.dateEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.Appearance.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit3.Properties.MaxLength = 16;
            this.dateEdit3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit3.Size = new System.Drawing.Size(99, 20);
            this.dateEdit3.TabIndex = 21;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(34, 73);
            this.label41.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 14);
            this.label41.TabIndex = 20;
            this.label41.Text = "Join Date";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit12
            // 
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(94, 4);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.MaxLength = 16;
            this.textEdit12.Size = new System.Drawing.Size(136, 20);
            this.textEdit12.TabIndex = 15;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(29, 7);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(63, 14);
            this.label42.TabIndex = 14;
            this.label42.Text = "User Code";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit1
            // 
            this.memoExEdit1.EnterMoveNextControl = true;
            this.memoExEdit1.Location = new System.Drawing.Point(94, 224);
            this.memoExEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit1.Name = "memoExEdit1";
            this.memoExEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit1.Properties.MaxLength = 80;
            this.memoExEdit1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit1.Properties.ShowIcon = false;
            this.memoExEdit1.Size = new System.Drawing.Size(295, 20);
            this.memoExEdit1.TabIndex = 35;
            this.memoExEdit1.ToolTip = "F4 : Show/hide text";
            this.memoExEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit1.ToolTipTitle = "Run System";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(42, 227);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(50, 14);
            this.label43.TabIndex = 34;
            this.label43.Text = "Address";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(45, 139);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(47, 14);
            this.label44.TabIndex = 26;
            this.label44.Text = "Gender";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit8
            // 
            this.lookUpEdit8.EnterMoveNextControl = true;
            this.lookUpEdit8.Location = new System.Drawing.Point(94, 136);
            this.lookUpEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit8.Name = "lookUpEdit8";
            this.lookUpEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit8.Properties.DropDownRows = 12;
            this.lookUpEdit8.Properties.MaxLength = 1;
            this.lookUpEdit8.Properties.NullText = "[Empty]";
            this.lookUpEdit8.Properties.PopupWidth = 500;
            this.lookUpEdit8.Size = new System.Drawing.Size(99, 20);
            this.lookUpEdit8.TabIndex = 27;
            this.lookUpEdit8.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(43, 51);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(49, 14);
            this.label45.TabIndex = 18;
            this.label45.Text = "Position";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit9
            // 
            this.lookUpEdit9.EnterMoveNextControl = true;
            this.lookUpEdit9.Location = new System.Drawing.Point(94, 48);
            this.lookUpEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit9.Name = "lookUpEdit9";
            this.lookUpEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit9.Properties.DropDownRows = 12;
            this.lookUpEdit9.Properties.MaxLength = 16;
            this.lookUpEdit9.Properties.NullText = "[Empty]";
            this.lookUpEdit9.Properties.PopupWidth = 500;
            this.lookUpEdit9.Size = new System.Drawing.Size(177, 20);
            this.lookUpEdit9.TabIndex = 19;
            this.lookUpEdit9.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(12, 29);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(80, 14);
            this.label46.TabIndex = 16;
            this.label46.Text = "Departement";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit10
            // 
            this.lookUpEdit10.EnterMoveNextControl = true;
            this.lookUpEdit10.Location = new System.Drawing.Point(94, 26);
            this.lookUpEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit10.Name = "lookUpEdit10";
            this.lookUpEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit10.Properties.DropDownRows = 12;
            this.lookUpEdit10.Properties.MaxLength = 16;
            this.lookUpEdit10.Properties.NullText = "[Empty]";
            this.lookUpEdit10.Properties.PopupWidth = 500;
            this.lookUpEdit10.Size = new System.Drawing.Size(223, 20);
            this.lookUpEdit10.TabIndex = 17;
            this.lookUpEdit10.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage2.Controls.Add(this.dateEdit4);
            this.tabPage2.Controls.Add(this.lookUpEdit11);
            this.tabPage2.Controls.Add(this.lookUpEdit12);
            this.tabPage2.Controls.Add(this.iGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(764, 373);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Employee Familly";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dateEdit4
            // 
            this.dateEdit4.EditValue = null;
            this.dateEdit4.EnterMoveNextControl = true;
            this.dateEdit4.Location = new System.Drawing.Point(359, 26);
            this.dateEdit4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit4.Name = "dateEdit4";
            this.dateEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.Appearance.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit4.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit4.Size = new System.Drawing.Size(104, 20);
            this.dateEdit4.TabIndex = 40;
            // 
            // lookUpEdit11
            // 
            this.lookUpEdit11.EnterMoveNextControl = true;
            this.lookUpEdit11.Location = new System.Drawing.Point(236, 25);
            this.lookUpEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit11.Name = "lookUpEdit11";
            this.lookUpEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit11.Properties.DropDownRows = 12;
            this.lookUpEdit11.Properties.NullText = "[Empty]";
            this.lookUpEdit11.Properties.PopupWidth = 500;
            this.lookUpEdit11.Size = new System.Drawing.Size(122, 20);
            this.lookUpEdit11.TabIndex = 37;
            this.lookUpEdit11.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit12
            // 
            this.lookUpEdit12.EnterMoveNextControl = true;
            this.lookUpEdit12.Location = new System.Drawing.Point(104, 24);
            this.lookUpEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit12.Name = "lookUpEdit12";
            this.lookUpEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit12.Properties.DropDownRows = 12;
            this.lookUpEdit12.Properties.NullText = "[Empty]";
            this.lookUpEdit12.Properties.PopupWidth = 500;
            this.lookUpEdit12.Size = new System.Drawing.Size(131, 20);
            this.lookUpEdit12.TabIndex = 36;
            this.lookUpEdit12.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // iGrid1
            // 
            this.iGrid1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid1.DefaultRow.Height = 20;
            this.iGrid1.DefaultRow.Sortable = false;
            this.iGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid1.Header.Height = 19;
            this.iGrid1.Location = new System.Drawing.Point(0, 0);
            this.iGrid1.Name = "iGrid1";
            this.iGrid1.ReadOnly = true;
            this.iGrid1.RowHeader.Visible = true;
            this.iGrid1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid1.SingleClickEdit = true;
            this.iGrid1.Size = new System.Drawing.Size(760, 369);
            this.iGrid1.TabIndex = 35;
            this.iGrid1.TreeCol = null;
            this.iGrid1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage3.Controls.Add(this.iGrid2);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 373);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Employee Work Exp";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // iGrid2
            // 
            this.iGrid2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid2.DefaultRow.Height = 20;
            this.iGrid2.DefaultRow.Sortable = false;
            this.iGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid2.Header.Height = 19;
            this.iGrid2.Location = new System.Drawing.Point(0, 0);
            this.iGrid2.Name = "iGrid2";
            this.iGrid2.RowHeader.Visible = true;
            this.iGrid2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid2.SingleClickEdit = true;
            this.iGrid2.Size = new System.Drawing.Size(764, 373);
            this.iGrid2.TabIndex = 47;
            this.iGrid2.TreeCol = null;
            this.iGrid2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(125, 237);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.MaxLength = 40;
            this.textEdit1.Size = new System.Drawing.Size(257, 20);
            this.textEdit1.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(44, 240);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 14);
            this.label1.TabIndex = 43;
            this.label1.Text = "Point of Hire";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(74, 135);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(46, 14);
            this.label70.TabIndex = 33;
            this.label70.Text = "Division";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EnterMoveNextControl = true;
            this.lookUpEdit1.Location = new System.Drawing.Point(125, 132);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DropDownRows = 25;
            this.lookUpEdit1.Properties.MaxLength = 16;
            this.lookUpEdit1.Properties.NullText = "[Empty]";
            this.lookUpEdit1.Properties.PopupWidth = 500;
            this.lookUpEdit1.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit1.TabIndex = 34;
            this.lookUpEdit1.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(31, 219);
            this.label73.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(89, 14);
            this.label73.TabIndex = 41;
            this.label73.Text = "Working Group";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EnterMoveNextControl = true;
            this.lookUpEdit2.Location = new System.Drawing.Point(125, 216);
            this.lookUpEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.DropDownRows = 25;
            this.lookUpEdit2.Properties.MaxLength = 16;
            this.lookUpEdit2.Properties.NullText = "[Empty]";
            this.lookUpEdit2.Properties.PopupWidth = 500;
            this.lookUpEdit2.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit2.TabIndex = 42;
            this.lookUpEdit2.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(72, 197);
            this.label74.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(48, 14);
            this.label74.TabIndex = 39;
            this.label74.Text = "Section";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit13
            // 
            this.lookUpEdit13.EnterMoveNextControl = true;
            this.lookUpEdit13.Location = new System.Drawing.Point(125, 195);
            this.lookUpEdit13.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit13.Name = "lookUpEdit13";
            this.lookUpEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit13.Properties.DropDownRows = 25;
            this.lookUpEdit13.Properties.MaxLength = 16;
            this.lookUpEdit13.Properties.NullText = "[Empty]";
            this.lookUpEdit13.Properties.PopupWidth = 500;
            this.lookUpEdit13.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit13.TabIndex = 40;
            this.lookUpEdit13.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(69, 429);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(51, 14);
            this.label75.TabIndex = 61;
            this.label75.Text = "Domicile";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit2
            // 
            this.memoExEdit2.EnterMoveNextControl = true;
            this.memoExEdit2.Location = new System.Drawing.Point(125, 426);
            this.memoExEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit2.Name = "memoExEdit2";
            this.memoExEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit2.Properties.MaxLength = 400;
            this.memoExEdit2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit2.Properties.ShowIcon = false;
            this.memoExEdit2.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit2.TabIndex = 62;
            this.memoExEdit2.ToolTip = "F4 : Show/hide text";
            this.memoExEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit2.ToolTipTitle = "Run System";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(92, 114);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(28, 14);
            this.label76.TabIndex = 31;
            this.label76.Text = "Site";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit14
            // 
            this.lookUpEdit14.EnterMoveNextControl = true;
            this.lookUpEdit14.Location = new System.Drawing.Point(125, 111);
            this.lookUpEdit14.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit14.Name = "lookUpEdit14";
            this.lookUpEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit14.Properties.DropDownRows = 25;
            this.lookUpEdit14.Properties.MaxLength = 16;
            this.lookUpEdit14.Properties.NullText = "[Empty]";
            this.lookUpEdit14.Properties.PopupWidth = 500;
            this.lookUpEdit14.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit14.TabIndex = 32;
            this.lookUpEdit14.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit2
            // 
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(125, 384);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.MaxLength = 10;
            this.textEdit2.Size = new System.Drawing.Size(257, 20);
            this.textEdit2.TabIndex = 58;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(74, 387);
            this.label77.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(46, 14);
            this.label77.TabIndex = 57;
            this.label77.Text = "RT/RW";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit13
            // 
            this.textEdit13.EnterMoveNextControl = true;
            this.textEdit13.Location = new System.Drawing.Point(125, 363);
            this.textEdit13.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.MaxLength = 40;
            this.textEdit13.Size = new System.Drawing.Size(257, 20);
            this.textEdit13.TabIndex = 56;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(79, 366);
            this.label78.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(41, 14);
            this.label78.TabIndex = 55;
            this.label78.Text = "Village";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit14
            // 
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(125, 342);
            this.textEdit14.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.MaxLength = 40;
            this.textEdit14.Size = new System.Drawing.Size(257, 20);
            this.textEdit14.TabIndex = 54;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(51, 344);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(69, 14);
            this.label79.TabIndex = 53;
            this.label79.Text = "Sub District";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(6, 92);
            this.label80.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(114, 14);
            this.label80.TabIndex = 29;
            this.label80.Text = "Employment Status";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit15
            // 
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(125, 69);
            this.textEdit15.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.MaxLength = 16;
            this.textEdit15.Size = new System.Drawing.Size(257, 20);
            this.textEdit15.TabIndex = 28;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(37, 71);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(83, 14);
            this.label81.TabIndex = 27;
            this.label81.Text = "Barcode Code";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit16
            // 
            this.textEdit16.EnterMoveNextControl = true;
            this.textEdit16.Location = new System.Drawing.Point(125, 48);
            this.textEdit16.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit16.Properties.Appearance.Options.UseFont = true;
            this.textEdit16.Properties.MaxLength = 3;
            this.textEdit16.Size = new System.Drawing.Size(257, 20);
            this.textEdit16.TabIndex = 26;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(51, 51);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(69, 14);
            this.label82.TabIndex = 25;
            this.label82.Text = "Short Code";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit15
            // 
            this.lookUpEdit15.EnterMoveNextControl = true;
            this.lookUpEdit15.Location = new System.Drawing.Point(125, 90);
            this.lookUpEdit15.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit15.Name = "lookUpEdit15";
            this.lookUpEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit15.Properties.DropDownRows = 25;
            this.lookUpEdit15.Properties.MaxLength = 1;
            this.lookUpEdit15.Properties.NullText = "[Empty]";
            this.lookUpEdit15.Properties.PopupWidth = 500;
            this.lookUpEdit15.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit15.TabIndex = 30;
            this.lookUpEdit15.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit17
            // 
            this.textEdit17.EnterMoveNextControl = true;
            this.textEdit17.Location = new System.Drawing.Point(125, 27);
            this.textEdit17.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit17.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit17.Properties.Appearance.Options.UseFont = true;
            this.textEdit17.Properties.MaxLength = 16;
            this.textEdit17.Size = new System.Drawing.Size(257, 20);
            this.textEdit17.TabIndex = 24;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(63, 29);
            this.label83.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(57, 14);
            this.label83.TabIndex = 23;
            this.label83.Text = "Old Code";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.label84);
            this.panel7.Controls.Add(this.lookUpEdit16);
            this.panel7.Controls.Add(this.label85);
            this.panel7.Controls.Add(this.lookUpEdit17);
            this.panel7.Controls.Add(this.label86);
            this.panel7.Controls.Add(this.lookUpEdit18);
            this.panel7.Controls.Add(this.textEdit18);
            this.panel7.Controls.Add(this.label87);
            this.panel7.Controls.Add(this.label88);
            this.panel7.Controls.Add(this.lookUpEdit19);
            this.panel7.Controls.Add(this.dateEdit5);
            this.panel7.Controls.Add(this.label89);
            this.panel7.Controls.Add(this.textEdit19);
            this.panel7.Controls.Add(this.label90);
            this.panel7.Controls.Add(this.label91);
            this.panel7.Controls.Add(this.label92);
            this.panel7.Controls.Add(this.textEdit20);
            this.panel7.Controls.Add(this.lookUpEdit20);
            this.panel7.Controls.Add(this.lookUpEdit21);
            this.panel7.Controls.Add(this.dateEdit6);
            this.panel7.Controls.Add(this.textEdit21);
            this.panel7.Controls.Add(this.label93);
            this.panel7.Controls.Add(this.label94);
            this.panel7.Controls.Add(this.label95);
            this.panel7.Controls.Add(this.textEdit22);
            this.panel7.Controls.Add(this.label96);
            this.panel7.Controls.Add(this.textEdit23);
            this.panel7.Controls.Add(this.label97);
            this.panel7.Controls.Add(this.lookUpEdit22);
            this.panel7.Controls.Add(this.textEdit24);
            this.panel7.Controls.Add(this.label98);
            this.panel7.Controls.Add(this.label99);
            this.panel7.Controls.Add(this.lookUpEdit23);
            this.panel7.Controls.Add(this.label100);
            this.panel7.Controls.Add(this.lookUpEdit24);
            this.panel7.Controls.Add(this.label101);
            this.panel7.Controls.Add(this.label102);
            this.panel7.Controls.Add(this.lookUpEdit25);
            this.panel7.Controls.Add(this.label103);
            this.panel7.Controls.Add(this.label104);
            this.panel7.Controls.Add(this.textEdit25);
            this.panel7.Controls.Add(this.lookUpEdit26);
            this.panel7.Controls.Add(this.label105);
            this.panel7.Controls.Add(this.textEdit26);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(388, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(372, 494);
            this.panel7.TabIndex = 47;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(31, 199);
            this.label84.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(78, 14);
            this.label84.TabIndex = 84;
            this.label84.Text = "Payroll Group";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit16
            // 
            this.lookUpEdit16.EnterMoveNextControl = true;
            this.lookUpEdit16.Location = new System.Drawing.Point(115, 195);
            this.lookUpEdit16.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit16.Name = "lookUpEdit16";
            this.lookUpEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit16.Properties.DropDownRows = 25;
            this.lookUpEdit16.Properties.NullText = "[Empty]";
            this.lookUpEdit16.Properties.PopupWidth = 500;
            this.lookUpEdit16.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit16.TabIndex = 85;
            this.lookUpEdit16.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(40, 115);
            this.label85.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(69, 14);
            this.label85.TabIndex = 76;
            this.label85.Text = "Blood Type";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit17
            // 
            this.lookUpEdit17.EnterMoveNextControl = true;
            this.lookUpEdit17.Location = new System.Drawing.Point(115, 111);
            this.lookUpEdit17.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit17.Name = "lookUpEdit17";
            this.lookUpEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit17.Properties.DropDownRows = 25;
            this.lookUpEdit17.Properties.MaxLength = 2;
            this.lookUpEdit17.Properties.NullText = "[Empty]";
            this.lookUpEdit17.Properties.PopupWidth = 500;
            this.lookUpEdit17.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit17.TabIndex = 77;
            this.lookUpEdit17.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(30, 157);
            this.label86.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(79, 14);
            this.label86.TabIndex = 80;
            this.label86.Text = "System Type";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit18
            // 
            this.lookUpEdit18.EnterMoveNextControl = true;
            this.lookUpEdit18.Location = new System.Drawing.Point(115, 153);
            this.lookUpEdit18.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit18.Name = "lookUpEdit18";
            this.lookUpEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit18.Properties.DropDownRows = 25;
            this.lookUpEdit18.Properties.NullText = "[Empty]";
            this.lookUpEdit18.Properties.PopupWidth = 500;
            this.lookUpEdit18.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit18.TabIndex = 81;
            this.lookUpEdit18.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit18
            // 
            this.textEdit18.EnterMoveNextControl = true;
            this.textEdit18.Location = new System.Drawing.Point(115, 132);
            this.textEdit18.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit18.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit18.Properties.Appearance.Options.UseFont = true;
            this.textEdit18.Properties.MaxLength = 40;
            this.textEdit18.Size = new System.Drawing.Size(245, 20);
            this.textEdit18.TabIndex = 79;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(11, 136);
            this.label87.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(98, 14);
            this.label87.TabIndex = 78;
            this.label87.Text = "Biological Mother";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(27, 241);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(82, 14);
            this.label88.TabIndex = 88;
            this.label88.Text = "Payrun Period";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit19
            // 
            this.lookUpEdit19.EnterMoveNextControl = true;
            this.lookUpEdit19.Location = new System.Drawing.Point(115, 237);
            this.lookUpEdit19.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit19.Name = "lookUpEdit19";
            this.lookUpEdit19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit19.Properties.DropDownRows = 25;
            this.lookUpEdit19.Properties.MaxLength = 1;
            this.lookUpEdit19.Properties.NullText = "[Empty]";
            this.lookUpEdit19.Properties.PopupWidth = 500;
            this.lookUpEdit19.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit19.TabIndex = 89;
            this.lookUpEdit19.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit5
            // 
            this.dateEdit5.EditValue = null;
            this.dateEdit5.EnterMoveNextControl = true;
            this.dateEdit5.Location = new System.Drawing.Point(115, 28);
            this.dateEdit5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit5.Name = "dateEdit5";
            this.dateEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit5.Properties.Appearance.Options.UseFont = true;
            this.dateEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit5.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit5.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit5.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit5.Properties.MaxLength = 16;
            this.dateEdit5.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit5.Size = new System.Drawing.Size(125, 20);
            this.dateEdit5.TabIndex = 68;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(23, 31);
            this.label89.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(86, 14);
            this.label89.TabIndex = 67;
            this.label89.Text = "Wedding Date";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit19
            // 
            this.textEdit19.EnterMoveNextControl = true;
            this.textEdit19.Location = new System.Drawing.Point(115, 342);
            this.textEdit19.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit19.Properties.Appearance.Options.UseFont = true;
            this.textEdit19.Properties.MaxLength = 80;
            this.textEdit19.Size = new System.Drawing.Size(245, 20);
            this.textEdit19.TabIndex = 99;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(29, 10);
            this.label90.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(80, 14);
            this.label90.TabIndex = 65;
            this.label90.Text = "Marital Status";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(21, 346);
            this.label91.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(88, 14);
            this.label91.TabIndex = 98;
            this.label91.Text = "Account Name";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(38, 178);
            this.label92.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(71, 14);
            this.label92.TabIndex = 82;
            this.label92.Text = "Grade Level";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit20
            // 
            this.textEdit20.EnterMoveNextControl = true;
            this.textEdit20.Location = new System.Drawing.Point(115, 426);
            this.textEdit20.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit20.Properties.Appearance.Options.UseFont = true;
            this.textEdit20.Properties.MaxLength = 80;
            this.textEdit20.Size = new System.Drawing.Size(245, 20);
            this.textEdit20.TabIndex = 107;
            // 
            // lookUpEdit20
            // 
            this.lookUpEdit20.EnterMoveNextControl = true;
            this.lookUpEdit20.Location = new System.Drawing.Point(115, 7);
            this.lookUpEdit20.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit20.Name = "lookUpEdit20";
            this.lookUpEdit20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit20.Properties.DropDownRows = 25;
            this.lookUpEdit20.Properties.MaxLength = 1;
            this.lookUpEdit20.Properties.NullText = "[Empty]";
            this.lookUpEdit20.Properties.PopupWidth = 500;
            this.lookUpEdit20.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit20.TabIndex = 66;
            this.lookUpEdit20.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit21
            // 
            this.lookUpEdit21.EnterMoveNextControl = true;
            this.lookUpEdit21.Location = new System.Drawing.Point(115, 174);
            this.lookUpEdit21.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit21.Name = "lookUpEdit21";
            this.lookUpEdit21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit21.Properties.DropDownRows = 25;
            this.lookUpEdit21.Properties.NullText = "[Empty]";
            this.lookUpEdit21.Properties.PopupWidth = 500;
            this.lookUpEdit21.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit21.TabIndex = 83;
            this.lookUpEdit21.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit6
            // 
            this.dateEdit6.EditValue = null;
            this.dateEdit6.EnterMoveNextControl = true;
            this.dateEdit6.Location = new System.Drawing.Point(243, 91);
            this.dateEdit6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit6.Name = "dateEdit6";
            this.dateEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit6.Properties.Appearance.Options.UseFont = true;
            this.dateEdit6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit6.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit6.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit6.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit6.Properties.MaxLength = 8;
            this.dateEdit6.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit6.Size = new System.Drawing.Size(117, 20);
            this.dateEdit6.TabIndex = 75;
            // 
            // textEdit21
            // 
            this.textEdit21.EnterMoveNextControl = true;
            this.textEdit21.Location = new System.Drawing.Point(115, 321);
            this.textEdit21.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit21.Properties.Appearance.Options.UseFont = true;
            this.textEdit21.Properties.MaxLength = 80;
            this.textEdit21.Size = new System.Drawing.Size(245, 20);
            this.textEdit21.TabIndex = 97;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(229, 93);
            this.label93.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(12, 14);
            this.label93.TabIndex = 67;
            this.label93.Text = "/";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(75, 429);
            this.label94.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(34, 14);
            this.label94.TabIndex = 106;
            this.label94.Text = "Email";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(30, 325);
            this.label95.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(79, 14);
            this.label95.TabIndex = 96;
            this.label95.Text = "Branch Name";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit22
            // 
            this.textEdit22.EnterMoveNextControl = true;
            this.textEdit22.Location = new System.Drawing.Point(115, 91);
            this.textEdit22.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit22.Properties.Appearance.Options.UseFont = true;
            this.textEdit22.Properties.MaxLength = 80;
            this.textEdit22.Size = new System.Drawing.Size(112, 20);
            this.textEdit22.TabIndex = 74;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(14, 95);
            this.label96.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(95, 14);
            this.label96.TabIndex = 73;
            this.label96.Text = "Birth Place/Date";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit23
            // 
            this.textEdit23.EnterMoveNextControl = true;
            this.textEdit23.Location = new System.Drawing.Point(115, 363);
            this.textEdit23.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit23.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit23.Properties.Appearance.Options.UseFont = true;
            this.textEdit23.Properties.MaxLength = 80;
            this.textEdit23.Size = new System.Drawing.Size(245, 20);
            this.textEdit23.TabIndex = 101;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(61, 74);
            this.label97.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(48, 14);
            this.label97.TabIndex = 71;
            this.label97.Text = "Religion";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit22
            // 
            this.lookUpEdit22.EnterMoveNextControl = true;
            this.lookUpEdit22.Location = new System.Drawing.Point(115, 70);
            this.lookUpEdit22.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit22.Name = "lookUpEdit22";
            this.lookUpEdit22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit22.Properties.DropDownRows = 25;
            this.lookUpEdit22.Properties.MaxLength = 2;
            this.lookUpEdit22.Properties.NullText = "[Empty]";
            this.lookUpEdit22.Properties.PopupWidth = 500;
            this.lookUpEdit22.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit22.TabIndex = 72;
            this.lookUpEdit22.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit24
            // 
            this.textEdit24.EnterMoveNextControl = true;
            this.textEdit24.Location = new System.Drawing.Point(115, 405);
            this.textEdit24.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit24.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit24.Properties.Appearance.Options.UseFont = true;
            this.textEdit24.Properties.MaxLength = 80;
            this.textEdit24.Size = new System.Drawing.Size(245, 20);
            this.textEdit24.TabIndex = 105;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(47, 366);
            this.label98.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(62, 14);
            this.label98.TabIndex = 100;
            this.label98.Text = "Account#";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(41, 303);
            this.label99.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(68, 14);
            this.label99.TabIndex = 94;
            this.label99.Text = "Bank Name";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit23
            // 
            this.lookUpEdit23.EnterMoveNextControl = true;
            this.lookUpEdit23.Location = new System.Drawing.Point(115, 300);
            this.lookUpEdit23.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit23.Name = "lookUpEdit23";
            this.lookUpEdit23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit23.Properties.DropDownRows = 25;
            this.lookUpEdit23.Properties.NullText = "[Empty]";
            this.lookUpEdit23.Properties.PopupWidth = 500;
            this.lookUpEdit23.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit23.TabIndex = 95;
            this.lookUpEdit23.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(2, 283);
            this.label100.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(107, 14);
            this.label100.TabIndex = 92;
            this.label100.Text = "Non-Incoming Tax";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit24
            // 
            this.lookUpEdit24.EnterMoveNextControl = true;
            this.lookUpEdit24.Location = new System.Drawing.Point(115, 279);
            this.lookUpEdit24.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit24.Name = "lookUpEdit24";
            this.lookUpEdit24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit24.Properties.DropDownRows = 25;
            this.lookUpEdit24.Properties.NullText = "[Empty]";
            this.lookUpEdit24.Properties.PopupWidth = 500;
            this.lookUpEdit24.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit24.TabIndex = 93;
            this.lookUpEdit24.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(68, 408);
            this.label101.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(41, 14);
            this.label101.TabIndex = 104;
            this.label101.Text = "Mobile";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(36, 220);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(73, 14);
            this.label102.TabIndex = 86;
            this.label102.Text = "Payroll Type";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit25
            // 
            this.lookUpEdit25.EnterMoveNextControl = true;
            this.lookUpEdit25.Location = new System.Drawing.Point(115, 216);
            this.lookUpEdit25.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit25.Name = "lookUpEdit25";
            this.lookUpEdit25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit25.Properties.DropDownRows = 25;
            this.lookUpEdit25.Properties.NullText = "[Empty]";
            this.lookUpEdit25.Properties.PopupWidth = 500;
            this.lookUpEdit25.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit25.TabIndex = 87;
            this.lookUpEdit25.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(68, 262);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(41, 14);
            this.label103.TabIndex = 90;
            this.label103.Text = "NPWP";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(62, 52);
            this.label104.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(47, 14);
            this.label104.TabIndex = 69;
            this.label104.Text = "Gender";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit25
            // 
            this.textEdit25.EnterMoveNextControl = true;
            this.textEdit25.Location = new System.Drawing.Point(115, 258);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit25.Properties.Appearance.Options.UseFont = true;
            this.textEdit25.Properties.MaxLength = 40;
            this.textEdit25.Size = new System.Drawing.Size(245, 20);
            this.textEdit25.TabIndex = 91;
            // 
            // lookUpEdit26
            // 
            this.lookUpEdit26.EnterMoveNextControl = true;
            this.lookUpEdit26.Location = new System.Drawing.Point(115, 49);
            this.lookUpEdit26.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit26.Name = "lookUpEdit26";
            this.lookUpEdit26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit26.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit26.Properties.DropDownRows = 25;
            this.lookUpEdit26.Properties.MaxLength = 1;
            this.lookUpEdit26.Properties.NullText = "[Empty]";
            this.lookUpEdit26.Properties.PopupWidth = 500;
            this.lookUpEdit26.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit26.TabIndex = 70;
            this.lookUpEdit26.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit26.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(67, 387);
            this.label105.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(42, 14);
            this.label105.TabIndex = 102;
            this.label105.Text = "Phone";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit26
            // 
            this.textEdit26.EnterMoveNextControl = true;
            this.textEdit26.Location = new System.Drawing.Point(115, 384);
            this.textEdit26.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit26.Properties.Appearance.Options.UseFont = true;
            this.textEdit26.Properties.MaxLength = 80;
            this.textEdit26.Size = new System.Drawing.Size(245, 20);
            this.textEdit26.TabIndex = 103;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(93, 323);
            this.label106.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(27, 14);
            this.label106.TabIndex = 51;
            this.label106.Text = "City";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit27
            // 
            this.textEdit27.EnterMoveNextControl = true;
            this.textEdit27.Location = new System.Drawing.Point(125, 447);
            this.textEdit27.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit27.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit27.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit27.Properties.Appearance.Options.UseFont = true;
            this.textEdit27.Properties.MaxLength = 40;
            this.textEdit27.Size = new System.Drawing.Size(257, 20);
            this.textEdit27.TabIndex = 64;
            // 
            // lookUpEdit27
            // 
            this.lookUpEdit27.EnterMoveNextControl = true;
            this.lookUpEdit27.Location = new System.Drawing.Point(125, 321);
            this.lookUpEdit27.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit27.Name = "lookUpEdit27";
            this.lookUpEdit27.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit27.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit27.Properties.DropDownRows = 25;
            this.lookUpEdit27.Properties.MaxLength = 16;
            this.lookUpEdit27.Properties.NullText = "[Empty]";
            this.lookUpEdit27.Properties.PopupWidth = 500;
            this.lookUpEdit27.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit27.TabIndex = 52;
            this.lookUpEdit27.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit27.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit7
            // 
            this.dateEdit7.EditValue = null;
            this.dateEdit7.EnterMoveNextControl = true;
            this.dateEdit7.Location = new System.Drawing.Point(125, 279);
            this.dateEdit7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit7.Name = "dateEdit7";
            this.dateEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit7.Properties.Appearance.Options.UseFont = true;
            this.dateEdit7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit7.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit7.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit7.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit7.Properties.MaxLength = 16;
            this.dateEdit7.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit7.Size = new System.Drawing.Size(125, 20);
            this.dateEdit7.TabIndex = 48;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(61, 450);
            this.label107.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(59, 14);
            this.label107.TabIndex = 63;
            this.label107.Text = "Identity#";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(48, 282);
            this.label108.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(72, 14);
            this.label108.TabIndex = 47;
            this.label108.Text = "Resign Date";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit8
            // 
            this.dateEdit8.EditValue = null;
            this.dateEdit8.EnterMoveNextControl = true;
            this.dateEdit8.Location = new System.Drawing.Point(125, 258);
            this.dateEdit8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit8.Name = "dateEdit8";
            this.dateEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit8.Properties.Appearance.Options.UseFont = true;
            this.dateEdit8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit8.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit8.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit8.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit8.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit8.Properties.MaxLength = 16;
            this.dateEdit8.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit8.Size = new System.Drawing.Size(125, 20);
            this.dateEdit8.TabIndex = 46;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Red;
            this.label109.Location = new System.Drawing.Point(62, 261);
            this.label109.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(58, 14);
            this.label109.TabIndex = 45;
            this.label109.Text = "Join Date";
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit28
            // 
            this.textEdit28.EnterMoveNextControl = true;
            this.textEdit28.Location = new System.Drawing.Point(125, 6);
            this.textEdit28.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit28.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit28.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit28.Properties.Appearance.Options.UseFont = true;
            this.textEdit28.Properties.MaxLength = 16;
            this.textEdit28.Size = new System.Drawing.Size(257, 20);
            this.textEdit28.TabIndex = 22;
            // 
            // textEdit29
            // 
            this.textEdit29.EnterMoveNextControl = true;
            this.textEdit29.Location = new System.Drawing.Point(125, 405);
            this.textEdit29.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit29.Name = "textEdit29";
            this.textEdit29.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit29.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit29.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit29.Properties.Appearance.Options.UseFont = true;
            this.textEdit29.Properties.MaxLength = 16;
            this.textEdit29.Size = new System.Drawing.Size(257, 20);
            this.textEdit29.TabIndex = 60;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(57, 9);
            this.label110.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(63, 14);
            this.label110.TabIndex = 21;
            this.label110.Text = "User Code";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(49, 408);
            this.label111.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(71, 14);
            this.label111.TabIndex = 59;
            this.label111.Text = "Postal Code";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit3
            // 
            this.memoExEdit3.EnterMoveNextControl = true;
            this.memoExEdit3.Location = new System.Drawing.Point(125, 300);
            this.memoExEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit3.Name = "memoExEdit3";
            this.memoExEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit3.Properties.MaxLength = 80;
            this.memoExEdit3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit3.Properties.ShowIcon = false;
            this.memoExEdit3.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit3.TabIndex = 50;
            this.memoExEdit3.ToolTip = "F4 : Show/hide text";
            this.memoExEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit3.ToolTipTitle = "Run System";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(70, 303);
            this.label112.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(50, 14);
            this.label112.TabIndex = 49;
            this.label112.Text = "Address";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(71, 176);
            this.label113.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(49, 14);
            this.label113.TabIndex = 37;
            this.label113.Text = "Position";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit28
            // 
            this.lookUpEdit28.EnterMoveNextControl = true;
            this.lookUpEdit28.Location = new System.Drawing.Point(125, 174);
            this.lookUpEdit28.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit28.Name = "lookUpEdit28";
            this.lookUpEdit28.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit28.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit28.Properties.DropDownRows = 25;
            this.lookUpEdit28.Properties.MaxLength = 16;
            this.lookUpEdit28.Properties.NullText = "[Empty]";
            this.lookUpEdit28.Properties.PopupWidth = 500;
            this.lookUpEdit28.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit28.TabIndex = 38;
            this.lookUpEdit28.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit28.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(47, 156);
            this.label114.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(73, 14);
            this.label114.TabIndex = 35;
            this.label114.Text = "Department";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit29
            // 
            this.lookUpEdit29.EnterMoveNextControl = true;
            this.lookUpEdit29.Location = new System.Drawing.Point(125, 153);
            this.lookUpEdit29.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit29.Name = "lookUpEdit29";
            this.lookUpEdit29.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit29.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit29.Properties.DropDownRows = 25;
            this.lookUpEdit29.Properties.MaxLength = 16;
            this.lookUpEdit29.Properties.NullText = "[Empty]";
            this.lookUpEdit29.Properties.PopupWidth = 500;
            this.lookUpEdit29.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit29.TabIndex = 36;
            this.lookUpEdit29.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit29.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit30
            // 
            this.textEdit30.EnterMoveNextControl = true;
            this.textEdit30.Location = new System.Drawing.Point(125, 237);
            this.textEdit30.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit30.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit30.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit30.Properties.Appearance.Options.UseFont = true;
            this.textEdit30.Properties.MaxLength = 40;
            this.textEdit30.Size = new System.Drawing.Size(257, 20);
            this.textEdit30.TabIndex = 44;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(44, 240);
            this.label115.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(75, 14);
            this.label115.TabIndex = 43;
            this.label115.Text = "Point of Hire";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Black;
            this.label116.Location = new System.Drawing.Point(74, 135);
            this.label116.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(46, 14);
            this.label116.TabIndex = 33;
            this.label116.Text = "Division";
            this.label116.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit30
            // 
            this.lookUpEdit30.EnterMoveNextControl = true;
            this.lookUpEdit30.Location = new System.Drawing.Point(125, 132);
            this.lookUpEdit30.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit30.Name = "lookUpEdit30";
            this.lookUpEdit30.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit30.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit30.Properties.DropDownRows = 25;
            this.lookUpEdit30.Properties.MaxLength = 16;
            this.lookUpEdit30.Properties.NullText = "[Empty]";
            this.lookUpEdit30.Properties.PopupWidth = 500;
            this.lookUpEdit30.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit30.TabIndex = 34;
            this.lookUpEdit30.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit30.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(31, 219);
            this.label117.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(89, 14);
            this.label117.TabIndex = 41;
            this.label117.Text = "Working Group";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit31
            // 
            this.lookUpEdit31.EnterMoveNextControl = true;
            this.lookUpEdit31.Location = new System.Drawing.Point(125, 216);
            this.lookUpEdit31.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit31.Name = "lookUpEdit31";
            this.lookUpEdit31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit31.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit31.Properties.DropDownRows = 25;
            this.lookUpEdit31.Properties.MaxLength = 16;
            this.lookUpEdit31.Properties.NullText = "[Empty]";
            this.lookUpEdit31.Properties.PopupWidth = 500;
            this.lookUpEdit31.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit31.TabIndex = 42;
            this.lookUpEdit31.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit31.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(72, 197);
            this.label118.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(48, 14);
            this.label118.TabIndex = 39;
            this.label118.Text = "Section";
            this.label118.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit32
            // 
            this.lookUpEdit32.EnterMoveNextControl = true;
            this.lookUpEdit32.Location = new System.Drawing.Point(125, 195);
            this.lookUpEdit32.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit32.Name = "lookUpEdit32";
            this.lookUpEdit32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit32.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit32.Properties.DropDownRows = 25;
            this.lookUpEdit32.Properties.MaxLength = 16;
            this.lookUpEdit32.Properties.NullText = "[Empty]";
            this.lookUpEdit32.Properties.PopupWidth = 500;
            this.lookUpEdit32.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit32.TabIndex = 40;
            this.lookUpEdit32.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit32.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(69, 429);
            this.label119.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(51, 14);
            this.label119.TabIndex = 61;
            this.label119.Text = "Domicile";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit4
            // 
            this.memoExEdit4.EnterMoveNextControl = true;
            this.memoExEdit4.Location = new System.Drawing.Point(125, 426);
            this.memoExEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit4.Name = "memoExEdit4";
            this.memoExEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit4.Properties.MaxLength = 400;
            this.memoExEdit4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit4.Properties.ShowIcon = false;
            this.memoExEdit4.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit4.TabIndex = 62;
            this.memoExEdit4.ToolTip = "F4 : Show/hide text";
            this.memoExEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit4.ToolTipTitle = "Run System";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(92, 114);
            this.label120.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(28, 14);
            this.label120.TabIndex = 31;
            this.label120.Text = "Site";
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit33
            // 
            this.lookUpEdit33.EnterMoveNextControl = true;
            this.lookUpEdit33.Location = new System.Drawing.Point(125, 111);
            this.lookUpEdit33.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit33.Name = "lookUpEdit33";
            this.lookUpEdit33.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit33.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit33.Properties.DropDownRows = 25;
            this.lookUpEdit33.Properties.MaxLength = 16;
            this.lookUpEdit33.Properties.NullText = "[Empty]";
            this.lookUpEdit33.Properties.PopupWidth = 500;
            this.lookUpEdit33.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit33.TabIndex = 32;
            this.lookUpEdit33.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit33.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit31
            // 
            this.textEdit31.EnterMoveNextControl = true;
            this.textEdit31.Location = new System.Drawing.Point(125, 384);
            this.textEdit31.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit31.Name = "textEdit31";
            this.textEdit31.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit31.Properties.Appearance.Options.UseFont = true;
            this.textEdit31.Properties.MaxLength = 10;
            this.textEdit31.Size = new System.Drawing.Size(257, 20);
            this.textEdit31.TabIndex = 58;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(74, 387);
            this.label121.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(46, 14);
            this.label121.TabIndex = 57;
            this.label121.Text = "RT/RW";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit32
            // 
            this.textEdit32.EnterMoveNextControl = true;
            this.textEdit32.Location = new System.Drawing.Point(125, 363);
            this.textEdit32.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit32.Name = "textEdit32";
            this.textEdit32.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit32.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit32.Properties.Appearance.Options.UseFont = true;
            this.textEdit32.Properties.MaxLength = 40;
            this.textEdit32.Size = new System.Drawing.Size(257, 20);
            this.textEdit32.TabIndex = 56;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Black;
            this.label122.Location = new System.Drawing.Point(79, 366);
            this.label122.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(41, 14);
            this.label122.TabIndex = 55;
            this.label122.Text = "Village";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit33
            // 
            this.textEdit33.EnterMoveNextControl = true;
            this.textEdit33.Location = new System.Drawing.Point(125, 342);
            this.textEdit33.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit33.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit33.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit33.Properties.Appearance.Options.UseFont = true;
            this.textEdit33.Properties.MaxLength = 40;
            this.textEdit33.Size = new System.Drawing.Size(257, 20);
            this.textEdit33.TabIndex = 54;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Black;
            this.label123.Location = new System.Drawing.Point(51, 344);
            this.label123.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(69, 14);
            this.label123.TabIndex = 53;
            this.label123.Text = "Sub District";
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.Black;
            this.label124.Location = new System.Drawing.Point(6, 92);
            this.label124.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(114, 14);
            this.label124.TabIndex = 29;
            this.label124.Text = "Employment Status";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit34
            // 
            this.textEdit34.EnterMoveNextControl = true;
            this.textEdit34.Location = new System.Drawing.Point(125, 69);
            this.textEdit34.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit34.Name = "textEdit34";
            this.textEdit34.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit34.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit34.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit34.Properties.Appearance.Options.UseFont = true;
            this.textEdit34.Properties.MaxLength = 16;
            this.textEdit34.Size = new System.Drawing.Size(257, 20);
            this.textEdit34.TabIndex = 28;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.Black;
            this.label125.Location = new System.Drawing.Point(37, 71);
            this.label125.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(83, 14);
            this.label125.TabIndex = 27;
            this.label125.Text = "Barcode Code";
            this.label125.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit35
            // 
            this.textEdit35.EnterMoveNextControl = true;
            this.textEdit35.Location = new System.Drawing.Point(125, 48);
            this.textEdit35.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit35.Name = "textEdit35";
            this.textEdit35.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit35.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit35.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit35.Properties.Appearance.Options.UseFont = true;
            this.textEdit35.Properties.MaxLength = 3;
            this.textEdit35.Size = new System.Drawing.Size(257, 20);
            this.textEdit35.TabIndex = 26;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Black;
            this.label126.Location = new System.Drawing.Point(51, 51);
            this.label126.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(69, 14);
            this.label126.TabIndex = 25;
            this.label126.Text = "Short Code";
            this.label126.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit34
            // 
            this.lookUpEdit34.EnterMoveNextControl = true;
            this.lookUpEdit34.Location = new System.Drawing.Point(125, 90);
            this.lookUpEdit34.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit34.Name = "lookUpEdit34";
            this.lookUpEdit34.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit34.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit34.Properties.DropDownRows = 25;
            this.lookUpEdit34.Properties.MaxLength = 1;
            this.lookUpEdit34.Properties.NullText = "[Empty]";
            this.lookUpEdit34.Properties.PopupWidth = 500;
            this.lookUpEdit34.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit34.TabIndex = 30;
            this.lookUpEdit34.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit34.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit36
            // 
            this.textEdit36.EnterMoveNextControl = true;
            this.textEdit36.Location = new System.Drawing.Point(125, 27);
            this.textEdit36.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit36.Name = "textEdit36";
            this.textEdit36.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit36.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit36.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit36.Properties.Appearance.Options.UseFont = true;
            this.textEdit36.Properties.MaxLength = 16;
            this.textEdit36.Size = new System.Drawing.Size(257, 20);
            this.textEdit36.TabIndex = 24;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Black;
            this.label127.Location = new System.Drawing.Point(63, 29);
            this.label127.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(57, 14);
            this.label127.TabIndex = 23;
            this.label127.Text = "Old Code";
            this.label127.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.label128);
            this.panel8.Controls.Add(this.lookUpEdit35);
            this.panel8.Controls.Add(this.label129);
            this.panel8.Controls.Add(this.lookUpEdit36);
            this.panel8.Controls.Add(this.label130);
            this.panel8.Controls.Add(this.lookUpEdit37);
            this.panel8.Controls.Add(this.textEdit37);
            this.panel8.Controls.Add(this.label131);
            this.panel8.Controls.Add(this.label132);
            this.panel8.Controls.Add(this.lookUpEdit38);
            this.panel8.Controls.Add(this.dateEdit9);
            this.panel8.Controls.Add(this.label133);
            this.panel8.Controls.Add(this.textEdit38);
            this.panel8.Controls.Add(this.label134);
            this.panel8.Controls.Add(this.label135);
            this.panel8.Controls.Add(this.label136);
            this.panel8.Controls.Add(this.textEdit39);
            this.panel8.Controls.Add(this.lookUpEdit39);
            this.panel8.Controls.Add(this.lookUpEdit40);
            this.panel8.Controls.Add(this.dateEdit10);
            this.panel8.Controls.Add(this.textEdit40);
            this.panel8.Controls.Add(this.label137);
            this.panel8.Controls.Add(this.label138);
            this.panel8.Controls.Add(this.label139);
            this.panel8.Controls.Add(this.textEdit41);
            this.panel8.Controls.Add(this.label140);
            this.panel8.Controls.Add(this.textEdit42);
            this.panel8.Controls.Add(this.label141);
            this.panel8.Controls.Add(this.lookUpEdit41);
            this.panel8.Controls.Add(this.textEdit43);
            this.panel8.Controls.Add(this.label142);
            this.panel8.Controls.Add(this.label143);
            this.panel8.Controls.Add(this.lookUpEdit42);
            this.panel8.Controls.Add(this.label144);
            this.panel8.Controls.Add(this.lookUpEdit43);
            this.panel8.Controls.Add(this.label145);
            this.panel8.Controls.Add(this.label146);
            this.panel8.Controls.Add(this.lookUpEdit44);
            this.panel8.Controls.Add(this.label147);
            this.panel8.Controls.Add(this.label148);
            this.panel8.Controls.Add(this.textEdit44);
            this.panel8.Controls.Add(this.lookUpEdit45);
            this.panel8.Controls.Add(this.label149);
            this.panel8.Controls.Add(this.textEdit45);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(388, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(372, 494);
            this.panel8.TabIndex = 47;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Black;
            this.label128.Location = new System.Drawing.Point(31, 199);
            this.label128.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(78, 14);
            this.label128.TabIndex = 84;
            this.label128.Text = "Payroll Group";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit35
            // 
            this.lookUpEdit35.EnterMoveNextControl = true;
            this.lookUpEdit35.Location = new System.Drawing.Point(115, 195);
            this.lookUpEdit35.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit35.Name = "lookUpEdit35";
            this.lookUpEdit35.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit35.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit35.Properties.DropDownRows = 25;
            this.lookUpEdit35.Properties.NullText = "[Empty]";
            this.lookUpEdit35.Properties.PopupWidth = 500;
            this.lookUpEdit35.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit35.TabIndex = 85;
            this.lookUpEdit35.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit35.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Black;
            this.label129.Location = new System.Drawing.Point(40, 115);
            this.label129.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(69, 14);
            this.label129.TabIndex = 76;
            this.label129.Text = "Blood Type";
            this.label129.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit36
            // 
            this.lookUpEdit36.EnterMoveNextControl = true;
            this.lookUpEdit36.Location = new System.Drawing.Point(115, 111);
            this.lookUpEdit36.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit36.Name = "lookUpEdit36";
            this.lookUpEdit36.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit36.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit36.Properties.DropDownRows = 25;
            this.lookUpEdit36.Properties.MaxLength = 2;
            this.lookUpEdit36.Properties.NullText = "[Empty]";
            this.lookUpEdit36.Properties.PopupWidth = 500;
            this.lookUpEdit36.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit36.TabIndex = 77;
            this.lookUpEdit36.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit36.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Black;
            this.label130.Location = new System.Drawing.Point(30, 157);
            this.label130.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(79, 14);
            this.label130.TabIndex = 80;
            this.label130.Text = "System Type";
            this.label130.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit37
            // 
            this.lookUpEdit37.EnterMoveNextControl = true;
            this.lookUpEdit37.Location = new System.Drawing.Point(115, 153);
            this.lookUpEdit37.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit37.Name = "lookUpEdit37";
            this.lookUpEdit37.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit37.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit37.Properties.DropDownRows = 25;
            this.lookUpEdit37.Properties.NullText = "[Empty]";
            this.lookUpEdit37.Properties.PopupWidth = 500;
            this.lookUpEdit37.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit37.TabIndex = 81;
            this.lookUpEdit37.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit37.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit37
            // 
            this.textEdit37.EnterMoveNextControl = true;
            this.textEdit37.Location = new System.Drawing.Point(115, 132);
            this.textEdit37.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit37.Name = "textEdit37";
            this.textEdit37.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit37.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit37.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit37.Properties.Appearance.Options.UseFont = true;
            this.textEdit37.Properties.MaxLength = 40;
            this.textEdit37.Size = new System.Drawing.Size(245, 20);
            this.textEdit37.TabIndex = 79;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Black;
            this.label131.Location = new System.Drawing.Point(11, 136);
            this.label131.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(98, 14);
            this.label131.TabIndex = 78;
            this.label131.Text = "Biological Mother";
            this.label131.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.Black;
            this.label132.Location = new System.Drawing.Point(27, 241);
            this.label132.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(82, 14);
            this.label132.TabIndex = 88;
            this.label132.Text = "Payrun Period";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit38
            // 
            this.lookUpEdit38.EnterMoveNextControl = true;
            this.lookUpEdit38.Location = new System.Drawing.Point(115, 237);
            this.lookUpEdit38.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit38.Name = "lookUpEdit38";
            this.lookUpEdit38.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit38.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit38.Properties.DropDownRows = 25;
            this.lookUpEdit38.Properties.MaxLength = 1;
            this.lookUpEdit38.Properties.NullText = "[Empty]";
            this.lookUpEdit38.Properties.PopupWidth = 500;
            this.lookUpEdit38.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit38.TabIndex = 89;
            this.lookUpEdit38.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit38.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit9
            // 
            this.dateEdit9.EditValue = null;
            this.dateEdit9.EnterMoveNextControl = true;
            this.dateEdit9.Location = new System.Drawing.Point(115, 28);
            this.dateEdit9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit9.Name = "dateEdit9";
            this.dateEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit9.Properties.Appearance.Options.UseFont = true;
            this.dateEdit9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit9.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit9.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit9.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit9.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit9.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit9.Properties.MaxLength = 16;
            this.dateEdit9.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit9.Size = new System.Drawing.Size(125, 20);
            this.dateEdit9.TabIndex = 68;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.Black;
            this.label133.Location = new System.Drawing.Point(23, 31);
            this.label133.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(86, 14);
            this.label133.TabIndex = 67;
            this.label133.Text = "Wedding Date";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit38
            // 
            this.textEdit38.EnterMoveNextControl = true;
            this.textEdit38.Location = new System.Drawing.Point(115, 342);
            this.textEdit38.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit38.Name = "textEdit38";
            this.textEdit38.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit38.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit38.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit38.Properties.Appearance.Options.UseFont = true;
            this.textEdit38.Properties.MaxLength = 80;
            this.textEdit38.Size = new System.Drawing.Size(245, 20);
            this.textEdit38.TabIndex = 99;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.ForeColor = System.Drawing.Color.Black;
            this.label134.Location = new System.Drawing.Point(29, 10);
            this.label134.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(80, 14);
            this.label134.TabIndex = 65;
            this.label134.Text = "Marital Status";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.ForeColor = System.Drawing.Color.Black;
            this.label135.Location = new System.Drawing.Point(21, 346);
            this.label135.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(88, 14);
            this.label135.TabIndex = 98;
            this.label135.Text = "Account Name";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.Color.Black;
            this.label136.Location = new System.Drawing.Point(38, 178);
            this.label136.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(71, 14);
            this.label136.TabIndex = 82;
            this.label136.Text = "Grade Level";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit39
            // 
            this.textEdit39.EnterMoveNextControl = true;
            this.textEdit39.Location = new System.Drawing.Point(115, 426);
            this.textEdit39.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit39.Name = "textEdit39";
            this.textEdit39.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit39.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit39.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit39.Properties.Appearance.Options.UseFont = true;
            this.textEdit39.Properties.MaxLength = 80;
            this.textEdit39.Size = new System.Drawing.Size(245, 20);
            this.textEdit39.TabIndex = 107;
            // 
            // lookUpEdit39
            // 
            this.lookUpEdit39.EnterMoveNextControl = true;
            this.lookUpEdit39.Location = new System.Drawing.Point(115, 7);
            this.lookUpEdit39.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit39.Name = "lookUpEdit39";
            this.lookUpEdit39.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit39.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit39.Properties.DropDownRows = 25;
            this.lookUpEdit39.Properties.MaxLength = 1;
            this.lookUpEdit39.Properties.NullText = "[Empty]";
            this.lookUpEdit39.Properties.PopupWidth = 500;
            this.lookUpEdit39.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit39.TabIndex = 66;
            this.lookUpEdit39.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit39.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit40
            // 
            this.lookUpEdit40.EnterMoveNextControl = true;
            this.lookUpEdit40.Location = new System.Drawing.Point(115, 174);
            this.lookUpEdit40.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit40.Name = "lookUpEdit40";
            this.lookUpEdit40.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit40.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit40.Properties.DropDownRows = 25;
            this.lookUpEdit40.Properties.NullText = "[Empty]";
            this.lookUpEdit40.Properties.PopupWidth = 500;
            this.lookUpEdit40.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit40.TabIndex = 83;
            this.lookUpEdit40.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit40.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit10
            // 
            this.dateEdit10.EditValue = null;
            this.dateEdit10.EnterMoveNextControl = true;
            this.dateEdit10.Location = new System.Drawing.Point(243, 91);
            this.dateEdit10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit10.Name = "dateEdit10";
            this.dateEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit10.Properties.Appearance.Options.UseFont = true;
            this.dateEdit10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit10.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit10.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit10.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit10.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit10.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit10.Properties.MaxLength = 8;
            this.dateEdit10.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit10.Size = new System.Drawing.Size(117, 20);
            this.dateEdit10.TabIndex = 75;
            // 
            // textEdit40
            // 
            this.textEdit40.EnterMoveNextControl = true;
            this.textEdit40.Location = new System.Drawing.Point(115, 321);
            this.textEdit40.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit40.Name = "textEdit40";
            this.textEdit40.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit40.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit40.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit40.Properties.Appearance.Options.UseFont = true;
            this.textEdit40.Properties.MaxLength = 80;
            this.textEdit40.Size = new System.Drawing.Size(245, 20);
            this.textEdit40.TabIndex = 97;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.ForeColor = System.Drawing.Color.Black;
            this.label137.Location = new System.Drawing.Point(229, 93);
            this.label137.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(12, 14);
            this.label137.TabIndex = 67;
            this.label137.Text = "/";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.ForeColor = System.Drawing.Color.Black;
            this.label138.Location = new System.Drawing.Point(75, 429);
            this.label138.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(34, 14);
            this.label138.TabIndex = 106;
            this.label138.Text = "Email";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.ForeColor = System.Drawing.Color.Black;
            this.label139.Location = new System.Drawing.Point(30, 325);
            this.label139.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(79, 14);
            this.label139.TabIndex = 96;
            this.label139.Text = "Branch Name";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit41
            // 
            this.textEdit41.EnterMoveNextControl = true;
            this.textEdit41.Location = new System.Drawing.Point(115, 91);
            this.textEdit41.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit41.Name = "textEdit41";
            this.textEdit41.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit41.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit41.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit41.Properties.Appearance.Options.UseFont = true;
            this.textEdit41.Properties.MaxLength = 80;
            this.textEdit41.Size = new System.Drawing.Size(112, 20);
            this.textEdit41.TabIndex = 74;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.ForeColor = System.Drawing.Color.Black;
            this.label140.Location = new System.Drawing.Point(14, 95);
            this.label140.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(95, 14);
            this.label140.TabIndex = 73;
            this.label140.Text = "Birth Place/Date";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit42
            // 
            this.textEdit42.EnterMoveNextControl = true;
            this.textEdit42.Location = new System.Drawing.Point(115, 363);
            this.textEdit42.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit42.Name = "textEdit42";
            this.textEdit42.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit42.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit42.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit42.Properties.Appearance.Options.UseFont = true;
            this.textEdit42.Properties.MaxLength = 80;
            this.textEdit42.Size = new System.Drawing.Size(245, 20);
            this.textEdit42.TabIndex = 101;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.ForeColor = System.Drawing.Color.Black;
            this.label141.Location = new System.Drawing.Point(61, 74);
            this.label141.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(48, 14);
            this.label141.TabIndex = 71;
            this.label141.Text = "Religion";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit41
            // 
            this.lookUpEdit41.EnterMoveNextControl = true;
            this.lookUpEdit41.Location = new System.Drawing.Point(115, 70);
            this.lookUpEdit41.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit41.Name = "lookUpEdit41";
            this.lookUpEdit41.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit41.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit41.Properties.DropDownRows = 25;
            this.lookUpEdit41.Properties.MaxLength = 2;
            this.lookUpEdit41.Properties.NullText = "[Empty]";
            this.lookUpEdit41.Properties.PopupWidth = 500;
            this.lookUpEdit41.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit41.TabIndex = 72;
            this.lookUpEdit41.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit41.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit43
            // 
            this.textEdit43.EnterMoveNextControl = true;
            this.textEdit43.Location = new System.Drawing.Point(115, 405);
            this.textEdit43.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit43.Name = "textEdit43";
            this.textEdit43.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit43.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit43.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit43.Properties.Appearance.Options.UseFont = true;
            this.textEdit43.Properties.MaxLength = 80;
            this.textEdit43.Size = new System.Drawing.Size(245, 20);
            this.textEdit43.TabIndex = 105;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.ForeColor = System.Drawing.Color.Black;
            this.label142.Location = new System.Drawing.Point(47, 366);
            this.label142.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(62, 14);
            this.label142.TabIndex = 100;
            this.label142.Text = "Account#";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.ForeColor = System.Drawing.Color.Black;
            this.label143.Location = new System.Drawing.Point(41, 303);
            this.label143.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(68, 14);
            this.label143.TabIndex = 94;
            this.label143.Text = "Bank Name";
            this.label143.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit42
            // 
            this.lookUpEdit42.EnterMoveNextControl = true;
            this.lookUpEdit42.Location = new System.Drawing.Point(115, 300);
            this.lookUpEdit42.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit42.Name = "lookUpEdit42";
            this.lookUpEdit42.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit42.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit42.Properties.DropDownRows = 25;
            this.lookUpEdit42.Properties.NullText = "[Empty]";
            this.lookUpEdit42.Properties.PopupWidth = 500;
            this.lookUpEdit42.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit42.TabIndex = 95;
            this.lookUpEdit42.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit42.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.Black;
            this.label144.Location = new System.Drawing.Point(2, 283);
            this.label144.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(107, 14);
            this.label144.TabIndex = 92;
            this.label144.Text = "Non-Incoming Tax";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit43
            // 
            this.lookUpEdit43.EnterMoveNextControl = true;
            this.lookUpEdit43.Location = new System.Drawing.Point(115, 279);
            this.lookUpEdit43.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit43.Name = "lookUpEdit43";
            this.lookUpEdit43.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit43.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit43.Properties.DropDownRows = 25;
            this.lookUpEdit43.Properties.NullText = "[Empty]";
            this.lookUpEdit43.Properties.PopupWidth = 500;
            this.lookUpEdit43.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit43.TabIndex = 93;
            this.lookUpEdit43.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit43.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.ForeColor = System.Drawing.Color.Black;
            this.label145.Location = new System.Drawing.Point(68, 408);
            this.label145.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(41, 14);
            this.label145.TabIndex = 104;
            this.label145.Text = "Mobile";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.Black;
            this.label146.Location = new System.Drawing.Point(36, 220);
            this.label146.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(73, 14);
            this.label146.TabIndex = 86;
            this.label146.Text = "Payroll Type";
            this.label146.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit44
            // 
            this.lookUpEdit44.EnterMoveNextControl = true;
            this.lookUpEdit44.Location = new System.Drawing.Point(115, 216);
            this.lookUpEdit44.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit44.Name = "lookUpEdit44";
            this.lookUpEdit44.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit44.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit44.Properties.DropDownRows = 25;
            this.lookUpEdit44.Properties.NullText = "[Empty]";
            this.lookUpEdit44.Properties.PopupWidth = 500;
            this.lookUpEdit44.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit44.TabIndex = 87;
            this.lookUpEdit44.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit44.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.Location = new System.Drawing.Point(68, 262);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(41, 14);
            this.label147.TabIndex = 90;
            this.label147.Text = "NPWP";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.Black;
            this.label148.Location = new System.Drawing.Point(62, 52);
            this.label148.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(47, 14);
            this.label148.TabIndex = 69;
            this.label148.Text = "Gender";
            this.label148.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit44
            // 
            this.textEdit44.EnterMoveNextControl = true;
            this.textEdit44.Location = new System.Drawing.Point(115, 258);
            this.textEdit44.Name = "textEdit44";
            this.textEdit44.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit44.Properties.Appearance.Options.UseFont = true;
            this.textEdit44.Properties.MaxLength = 40;
            this.textEdit44.Size = new System.Drawing.Size(245, 20);
            this.textEdit44.TabIndex = 91;
            // 
            // lookUpEdit45
            // 
            this.lookUpEdit45.EnterMoveNextControl = true;
            this.lookUpEdit45.Location = new System.Drawing.Point(115, 49);
            this.lookUpEdit45.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit45.Name = "lookUpEdit45";
            this.lookUpEdit45.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit45.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit45.Properties.DropDownRows = 25;
            this.lookUpEdit45.Properties.MaxLength = 1;
            this.lookUpEdit45.Properties.NullText = "[Empty]";
            this.lookUpEdit45.Properties.PopupWidth = 500;
            this.lookUpEdit45.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit45.TabIndex = 70;
            this.lookUpEdit45.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit45.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.Black;
            this.label149.Location = new System.Drawing.Point(67, 387);
            this.label149.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(42, 14);
            this.label149.TabIndex = 102;
            this.label149.Text = "Phone";
            this.label149.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit45
            // 
            this.textEdit45.EnterMoveNextControl = true;
            this.textEdit45.Location = new System.Drawing.Point(115, 384);
            this.textEdit45.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit45.Name = "textEdit45";
            this.textEdit45.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit45.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit45.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit45.Properties.Appearance.Options.UseFont = true;
            this.textEdit45.Properties.MaxLength = 80;
            this.textEdit45.Size = new System.Drawing.Size(245, 20);
            this.textEdit45.TabIndex = 103;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.Black;
            this.label150.Location = new System.Drawing.Point(93, 323);
            this.label150.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(27, 14);
            this.label150.TabIndex = 51;
            this.label150.Text = "City";
            this.label150.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit46
            // 
            this.textEdit46.EnterMoveNextControl = true;
            this.textEdit46.Location = new System.Drawing.Point(125, 447);
            this.textEdit46.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit46.Name = "textEdit46";
            this.textEdit46.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit46.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit46.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit46.Properties.Appearance.Options.UseFont = true;
            this.textEdit46.Properties.MaxLength = 40;
            this.textEdit46.Size = new System.Drawing.Size(257, 20);
            this.textEdit46.TabIndex = 64;
            // 
            // lookUpEdit46
            // 
            this.lookUpEdit46.EnterMoveNextControl = true;
            this.lookUpEdit46.Location = new System.Drawing.Point(125, 321);
            this.lookUpEdit46.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit46.Name = "lookUpEdit46";
            this.lookUpEdit46.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit46.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit46.Properties.DropDownRows = 25;
            this.lookUpEdit46.Properties.MaxLength = 16;
            this.lookUpEdit46.Properties.NullText = "[Empty]";
            this.lookUpEdit46.Properties.PopupWidth = 500;
            this.lookUpEdit46.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit46.TabIndex = 52;
            this.lookUpEdit46.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit46.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit11
            // 
            this.dateEdit11.EditValue = null;
            this.dateEdit11.EnterMoveNextControl = true;
            this.dateEdit11.Location = new System.Drawing.Point(125, 279);
            this.dateEdit11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit11.Name = "dateEdit11";
            this.dateEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit11.Properties.Appearance.Options.UseFont = true;
            this.dateEdit11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit11.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit11.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit11.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit11.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit11.Properties.MaxLength = 16;
            this.dateEdit11.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit11.Size = new System.Drawing.Size(125, 20);
            this.dateEdit11.TabIndex = 48;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.ForeColor = System.Drawing.Color.Black;
            this.label151.Location = new System.Drawing.Point(61, 450);
            this.label151.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(59, 14);
            this.label151.TabIndex = 63;
            this.label151.Text = "Identity#";
            this.label151.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.ForeColor = System.Drawing.Color.Black;
            this.label152.Location = new System.Drawing.Point(48, 282);
            this.label152.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(72, 14);
            this.label152.TabIndex = 47;
            this.label152.Text = "Resign Date";
            this.label152.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit12
            // 
            this.dateEdit12.EditValue = null;
            this.dateEdit12.EnterMoveNextControl = true;
            this.dateEdit12.Location = new System.Drawing.Point(125, 258);
            this.dateEdit12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit12.Name = "dateEdit12";
            this.dateEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit12.Properties.Appearance.Options.UseFont = true;
            this.dateEdit12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit12.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit12.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit12.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit12.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit12.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit12.Properties.MaxLength = 16;
            this.dateEdit12.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit12.Size = new System.Drawing.Size(125, 20);
            this.dateEdit12.TabIndex = 46;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.ForeColor = System.Drawing.Color.Red;
            this.label153.Location = new System.Drawing.Point(62, 261);
            this.label153.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(58, 14);
            this.label153.TabIndex = 45;
            this.label153.Text = "Join Date";
            this.label153.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit47
            // 
            this.textEdit47.EnterMoveNextControl = true;
            this.textEdit47.Location = new System.Drawing.Point(125, 6);
            this.textEdit47.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit47.Name = "textEdit47";
            this.textEdit47.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit47.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit47.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit47.Properties.Appearance.Options.UseFont = true;
            this.textEdit47.Properties.MaxLength = 16;
            this.textEdit47.Size = new System.Drawing.Size(257, 20);
            this.textEdit47.TabIndex = 22;
            // 
            // textEdit48
            // 
            this.textEdit48.EnterMoveNextControl = true;
            this.textEdit48.Location = new System.Drawing.Point(125, 405);
            this.textEdit48.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit48.Name = "textEdit48";
            this.textEdit48.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit48.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit48.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit48.Properties.Appearance.Options.UseFont = true;
            this.textEdit48.Properties.MaxLength = 16;
            this.textEdit48.Size = new System.Drawing.Size(257, 20);
            this.textEdit48.TabIndex = 60;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.ForeColor = System.Drawing.Color.Black;
            this.label154.Location = new System.Drawing.Point(57, 9);
            this.label154.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(63, 14);
            this.label154.TabIndex = 21;
            this.label154.Text = "User Code";
            this.label154.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.ForeColor = System.Drawing.Color.Black;
            this.label155.Location = new System.Drawing.Point(49, 408);
            this.label155.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(71, 14);
            this.label155.TabIndex = 59;
            this.label155.Text = "Postal Code";
            this.label155.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit5
            // 
            this.memoExEdit5.EnterMoveNextControl = true;
            this.memoExEdit5.Location = new System.Drawing.Point(125, 300);
            this.memoExEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit5.Name = "memoExEdit5";
            this.memoExEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit5.Properties.MaxLength = 80;
            this.memoExEdit5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit5.Properties.ShowIcon = false;
            this.memoExEdit5.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit5.TabIndex = 50;
            this.memoExEdit5.ToolTip = "F4 : Show/hide text";
            this.memoExEdit5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit5.ToolTipTitle = "Run System";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(70, 303);
            this.label156.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(50, 14);
            this.label156.TabIndex = 49;
            this.label156.Text = "Address";
            this.label156.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.ForeColor = System.Drawing.Color.Black;
            this.label157.Location = new System.Drawing.Point(71, 176);
            this.label157.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(49, 14);
            this.label157.TabIndex = 37;
            this.label157.Text = "Position";
            this.label157.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit47
            // 
            this.lookUpEdit47.EnterMoveNextControl = true;
            this.lookUpEdit47.Location = new System.Drawing.Point(125, 174);
            this.lookUpEdit47.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit47.Name = "lookUpEdit47";
            this.lookUpEdit47.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit47.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit47.Properties.DropDownRows = 25;
            this.lookUpEdit47.Properties.MaxLength = 16;
            this.lookUpEdit47.Properties.NullText = "[Empty]";
            this.lookUpEdit47.Properties.PopupWidth = 500;
            this.lookUpEdit47.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit47.TabIndex = 38;
            this.lookUpEdit47.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit47.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.ForeColor = System.Drawing.Color.Black;
            this.label158.Location = new System.Drawing.Point(47, 156);
            this.label158.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(73, 14);
            this.label158.TabIndex = 35;
            this.label158.Text = "Department";
            this.label158.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit48
            // 
            this.lookUpEdit48.EnterMoveNextControl = true;
            this.lookUpEdit48.Location = new System.Drawing.Point(125, 153);
            this.lookUpEdit48.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit48.Name = "lookUpEdit48";
            this.lookUpEdit48.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit48.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit48.Properties.DropDownRows = 25;
            this.lookUpEdit48.Properties.MaxLength = 16;
            this.lookUpEdit48.Properties.NullText = "[Empty]";
            this.lookUpEdit48.Properties.PopupWidth = 500;
            this.lookUpEdit48.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit48.TabIndex = 36;
            this.lookUpEdit48.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit48.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmEmployeeRecruitment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 600);
            this.Name = "FrmEmployeeRecruitment";
            this.Activated += new System.EventHandler(this.FrmEmployee_Activated);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarcodeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrollType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            this.TpgEmployeeFamily.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfessionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFamGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpgEmployeeWorkExp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgEmployeeEducation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueFacCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMajor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgEmployeeCompetence.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpgTraining.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgEmployeeSS.ResumeLayout(false);
            this.TpgEmployeeSS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrossSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProbation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartWorkDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartWorkDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlacement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInterviewDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteInterviewDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterviewerBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlace.Properties)).EndInit();
            this.TpPicture.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicEmployee)).EndInit();
            this.TpgStage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteStageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProgress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.TpgEmployeeBenefit.ResumeLayout(false);
            this.TpgEmployeeBenefit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBonus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAnnualLeave.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtHolidayAllowance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInsurance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTHP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobIncentives.Properties)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit48.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgGeneral;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCode;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueCity;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.TextEdit TxtBirthPlace;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueReligion;
        internal DevExpress.XtraEditors.TextEdit TxtIdNumber;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteResignDt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteJoinDt;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtUserCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueGender;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage TpgEmployeeFamily;
        private System.Windows.Forms.TabPage TpgEmployeeWorkExp;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LuePTKP;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.LookUpEdit LuePayrollType;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.TextEdit TxtNPWP;
        internal DevExpress.XtraEditors.TextEdit TxtBankBranch;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit textEdit4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.Label label29;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit5;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        internal DevExpress.XtraEditors.TextEdit textEdit6;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit textEdit7;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit textEdit8;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit textEdit9;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit6;
        internal DevExpress.XtraEditors.DateEdit dateEdit1;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit7;
        internal DevExpress.XtraEditors.TextEdit textEdit11;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.DateEdit dateEdit2;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.DateEdit dateEdit3;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit textEdit12;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit8;
        private System.Windows.Forms.Label label45;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit9;
        private System.Windows.Forms.Label label46;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit10;
        private System.Windows.Forms.TabPage tabPage2;
        internal DevExpress.XtraEditors.DateEdit dateEdit4;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit11;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit12;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid1;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid2;
        private System.Windows.Forms.TabPage TpgEmployeeEducation;
        private System.Windows.Forms.TabPage TpgTraining;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraEditors.LookUpEdit LueLevel;
        private System.Windows.Forms.Label label47;
        private DevExpress.XtraEditors.LookUpEdit LueGrdLvlCode;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabPage TpgEmployeeSS;
        internal DevExpress.XtraEditors.TextEdit TxtShortCode;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.TextEdit TxtBarcodeCode;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private DevExpress.XtraEditors.LookUpEdit LuePayrunPeriod;
        private System.Windows.Forms.Label label53;
        private DevExpress.XtraEditors.LookUpEdit LueEmploymentStatus;
        internal DevExpress.XtraEditors.TextEdit TxtRTRW;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.TextEdit TxtVillage;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.TextEdit TxtSubDistrict;
        private System.Windows.Forms.Label label54;
        private DevExpress.XtraEditors.TextEdit TxtMother;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private DevExpress.XtraEditors.LookUpEdit LueMaritalStatus;
        internal DevExpress.XtraEditors.DateEdit DteWeddingDt;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private DevExpress.XtraEditors.LookUpEdit LueSystemType;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private DevExpress.XtraEditors.LookUpEdit LueBloodType;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.MemoExEdit MeeDomicile;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button BtnFamily;
        private System.Windows.Forms.Button button1;
        internal DevExpress.XtraEditors.DateEdit DteFamBirthDt;
        private DevExpress.XtraEditors.LookUpEdit LueFamGender;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private System.Windows.Forms.Label label65;
        private DevExpress.XtraEditors.LookUpEdit LueWorkGroup;
        private System.Windows.Forms.Label label64;
        private DevExpress.XtraEditors.LookUpEdit LueSection;
        private DevExpress.XtraEditors.TextEdit TxtDisplayName;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private DevExpress.XtraEditors.LookUpEdit LueDivisionCode;
        private System.Windows.Forms.Label label68;
        private DevExpress.XtraEditors.LookUpEdit LuePGCode;
        internal DevExpress.XtraEditors.TextEdit TxtPOH;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label71;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label159;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label70;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.Label label73;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private System.Windows.Forms.Label label74;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit13;
        private System.Windows.Forms.Label label75;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit2;
        private System.Windows.Forms.Label label76;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit14;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label77;
        internal DevExpress.XtraEditors.TextEdit textEdit13;
        private System.Windows.Forms.Label label78;
        internal DevExpress.XtraEditors.TextEdit textEdit14;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        internal DevExpress.XtraEditors.TextEdit textEdit15;
        private System.Windows.Forms.Label label81;
        internal DevExpress.XtraEditors.TextEdit textEdit16;
        private System.Windows.Forms.Label label82;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit15;
        internal DevExpress.XtraEditors.TextEdit textEdit17;
        private System.Windows.Forms.Label label83;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label84;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit16;
        private System.Windows.Forms.Label label85;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit17;
        private System.Windows.Forms.Label label86;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit18;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit19;
        internal DevExpress.XtraEditors.DateEdit dateEdit5;
        private System.Windows.Forms.Label label89;
        internal DevExpress.XtraEditors.TextEdit textEdit19;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        internal DevExpress.XtraEditors.TextEdit textEdit20;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit20;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit21;
        internal DevExpress.XtraEditors.DateEdit dateEdit6;
        internal DevExpress.XtraEditors.TextEdit textEdit21;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private System.Windows.Forms.Label label96;
        internal DevExpress.XtraEditors.TextEdit textEdit23;
        private System.Windows.Forms.Label label97;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit22;
        internal DevExpress.XtraEditors.TextEdit textEdit24;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit23;
        private System.Windows.Forms.Label label100;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit24;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit25;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private DevExpress.XtraEditors.TextEdit textEdit25;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit26;
        private System.Windows.Forms.Label label105;
        internal DevExpress.XtraEditors.TextEdit textEdit26;
        private System.Windows.Forms.Label label106;
        internal DevExpress.XtraEditors.TextEdit textEdit27;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit27;
        internal DevExpress.XtraEditors.DateEdit dateEdit7;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        internal DevExpress.XtraEditors.DateEdit dateEdit8;
        private System.Windows.Forms.Label label109;
        internal DevExpress.XtraEditors.TextEdit textEdit28;
        internal DevExpress.XtraEditors.TextEdit textEdit29;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit3;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit28;
        private System.Windows.Forms.Label label114;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit29;
        internal DevExpress.XtraEditors.TextEdit textEdit30;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit30;
        private System.Windows.Forms.Label label117;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit31;
        private System.Windows.Forms.Label label118;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit32;
        private System.Windows.Forms.Label label119;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit4;
        private System.Windows.Forms.Label label120;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit33;
        internal DevExpress.XtraEditors.TextEdit textEdit31;
        private System.Windows.Forms.Label label121;
        internal DevExpress.XtraEditors.TextEdit textEdit32;
        private System.Windows.Forms.Label label122;
        internal DevExpress.XtraEditors.TextEdit textEdit33;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        internal DevExpress.XtraEditors.TextEdit textEdit34;
        private System.Windows.Forms.Label label125;
        internal DevExpress.XtraEditors.TextEdit textEdit35;
        private System.Windows.Forms.Label label126;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit34;
        internal DevExpress.XtraEditors.TextEdit textEdit36;
        private System.Windows.Forms.Label label127;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label128;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit35;
        private System.Windows.Forms.Label label129;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit36;
        private System.Windows.Forms.Label label130;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit37;
        private DevExpress.XtraEditors.TextEdit textEdit37;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit38;
        internal DevExpress.XtraEditors.DateEdit dateEdit9;
        private System.Windows.Forms.Label label133;
        internal DevExpress.XtraEditors.TextEdit textEdit38;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        internal DevExpress.XtraEditors.TextEdit textEdit39;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit39;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit40;
        internal DevExpress.XtraEditors.DateEdit dateEdit10;
        internal DevExpress.XtraEditors.TextEdit textEdit40;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private DevExpress.XtraEditors.TextEdit textEdit41;
        private System.Windows.Forms.Label label140;
        internal DevExpress.XtraEditors.TextEdit textEdit42;
        private System.Windows.Forms.Label label141;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit41;
        internal DevExpress.XtraEditors.TextEdit textEdit43;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit42;
        private System.Windows.Forms.Label label144;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit43;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit44;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private DevExpress.XtraEditors.TextEdit textEdit44;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit45;
        private System.Windows.Forms.Label label149;
        internal DevExpress.XtraEditors.TextEdit textEdit45;
        private System.Windows.Forms.Label label150;
        internal DevExpress.XtraEditors.TextEdit textEdit46;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit46;
        internal DevExpress.XtraEditors.DateEdit dateEdit11;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        internal DevExpress.XtraEditors.DateEdit dateEdit12;
        private System.Windows.Forms.Label label153;
        internal DevExpress.XtraEditors.TextEdit textEdit47;
        internal DevExpress.XtraEditors.TextEdit textEdit48;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit5;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit47;
        private System.Windows.Forms.Label label158;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit48;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label164;
        internal DevExpress.XtraEditors.DateEdit DteInterviewDt;
        private DevExpress.XtraEditors.TextEdit TxtInterviewerBy;
        private DevExpress.XtraEditors.TextEdit TxtPlace;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label165;
        internal DevExpress.XtraEditors.DateEdit DteStartWorkDt;
        private DevExpress.XtraEditors.TextEdit TxtPlacement;
        private DevExpress.XtraEditors.TextEdit TxtProbation;
        internal DevExpress.XtraEditors.LookUpEdit LueDeptCode2;
        internal DevExpress.XtraEditors.TextEdit TxtGrossSalary;
        public DevExpress.XtraEditors.SimpleButton BtnEmpRequest;
        private System.Windows.Forms.Label label168;
        public DevExpress.XtraEditors.TextEdit TxtEmpRequestDocNo;
        public DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        public DevExpress.XtraEditors.SimpleButton BtnShowEmployeeRequest;
        private System.Windows.Forms.TabPage TpgEmployeeCompetence;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private DevExpress.XtraEditors.LookUpEdit LueCompetenceCode;
        private DevExpress.XtraEditors.LookUpEdit LueMajor;
        private System.Windows.Forms.TabPage TpPicture;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private System.Windows.Forms.PictureBox PicEmployee;
        private System.Windows.Forms.Button button4;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        public DevExpress.XtraEditors.SimpleButton BtnPicture;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.TabPage TpgStage;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        internal DevExpress.XtraEditors.DateEdit DteStageDt;
        private DevExpress.XtraEditors.LookUpEdit LueProgress;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingCode;
        private DevExpress.XtraEditors.LookUpEdit LueFacCode;
        private DevExpress.XtraEditors.LookUpEdit LueField;
        private DevExpress.XtraEditors.LookUpEdit LueEducationLevelCode;
        private DevExpress.XtraEditors.LookUpEdit LueProfessionCode;
        private System.Windows.Forms.TabPage TpgEmployeeBenefit;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label176;
        internal DevExpress.XtraEditors.TextEdit TxtTax;
        private System.Windows.Forms.Label label175;
        internal DevExpress.XtraEditors.TextEdit TxtBonus;
        private System.Windows.Forms.Label label174;
        internal DevExpress.XtraEditors.TextEdit TxtAnnualLeave;
        private System.Windows.Forms.Label label173;
        internal DevExpress.XtraEditors.TextEdit TxtHolidayAllowance;
        private System.Windows.Forms.Label label172;
        internal DevExpress.XtraEditors.TextEdit TxtInsurance;
        private System.Windows.Forms.Label label171;
        internal DevExpress.XtraEditors.TextEdit TxtTHP;
        private System.Windows.Forms.Label label170;
        internal DevExpress.XtraEditors.TextEdit TxtJobIncentives;
        private System.Windows.Forms.Label label169;
    }
}