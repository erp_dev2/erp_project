﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSubLocation : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmSubLocationFind FrmFind;

        #endregion

        #region Constructor

        public FrmSubLocation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSubLocCode, TxtSubLocName
                    }, true);
                    TxtSubLocCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSubLocCode, TxtSubLocName, 
                    }, false);
                    TxtSubLocCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSubLocName, 
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSubLocCode, 
                    }, true);
                    TxtSubLocName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtSubLocCode, TxtSubLocName, 
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSubLocationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSubLocCode, "", false)) return;
            SetFormControl(mState.Edit);
        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblSublocation(SubLocCode, SubLocName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@SubLocCode, @SubLocName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");

                SQL.AppendLine("Update SubLocName=@SubLocName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@SubLocCode", TxtSubLocCode.Text);
                Sm.CmParam<String>(ref cm, "@SubLocName", TxtSubLocName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtSubLocCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSubLocCode, "Sub Location Code", false) ||
                Sm.IsTxtEmpty(TxtSubLocName, "Sub Location Name", false) ||
                IsLocCodeExisted();
        }

        private bool IsLocCodeExisted()
        {
            if (!TxtSubLocCode.Properties.ReadOnly && Sm.IsDataExist("Select SubLocCode From TblSubLocation Where SubLocCode='" + TxtSubLocCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Sub Location Code ( " + TxtSubLocCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion


        #endregion

        #region  Show Data

        public void ShowData(string SubLocCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@SubLocCode", SubLocCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select  SubLocCode, SubLocName From TblSubLocation Where SubLocCode=@SubLocCode",
                        new string[] 
                        {
                            "SubLocCode", "SubLocName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtSubLocCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtSubLocName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        #endregion

        #endregion
    }
}
