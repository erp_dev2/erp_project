﻿#region Update
/*
    09/08/2018 [TKG] tambah filter status do
    16/05/2019 [WED] tambah kolom margin dan average price, berdasarkan parameter IsRptDOCtShowMarginInfo
    23/05/2019 [WED] tambah label prosentase di margin nya
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDOToCustomer : FrmBase6
    {
        #region Field

        internal bool grpcode = false;
        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool 
            mIsItGrpCodeShow = false, 
            mIsRptDOCtShowPriceInfo = false,
            mIsShowForeignName = false,
            mIsFilterByItCt = false,
            mIsRptDOCtShowMarginInfo = false;

        #endregion

        #region Constructor

        public FrmRptDOToCustomer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();

                TxtAvgSellingPrice.EditValue = Sm.FormatNum(0m, 0);
                TxtAvgPrice.EditValue = Sm.FormatNum(0m, 0);
                TxtAvgMargin.EditValue = Sm.FormatNum(0m, 0);

                if (mIsRptDOCtShowPriceInfo && mIsRptDOCtShowMarginInfo)
                {
                    LblAvgPrice.Visible = LblAvgSellingPrice.Visible = LblAvgMargin.Visible = LblMarginPercentage.Visible =
                    TxtAvgPrice.Visible = TxtAvgSellingPrice.Visible = TxtAvgMargin.Visible =
                    true;
                }
                else
                {
                    LblAvgPrice.Visible = LblAvgSellingPrice.Visible = LblAvgMargin.Visible = LblMarginPercentage.Visible =
                    TxtAvgPrice.Visible = TxtAvgSellingPrice.Visible = TxtAvgMargin.Visible =
                    false;
                }
                
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsRptDOCtShowPriceInfo = Sm.GetParameterBoo("IsRptDOCtShowPriceInfo");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptDOCtShowMarginInfo = Sm.GetParameterBoo("IsRptDOCtShowMarginInfo");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocDt, A.DocNo, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("C.CtName, B.ItCode, D.ItName, D.ForeignName, B.BatchNo, ");
            SQL.AppendLine("B.Qty, D.InventoryUOMCode, ");
            if (mIsItGrpCodeShow)
                SQL.AppendLine("E.ItGrpName, ");
            if (mIsRptDOCtShowPriceInfo)
                SQL.AppendLine("B.UPrice As UPrice1, F.UPrice As UPrice2, ");
            if (mIsRptDOCtShowPriceInfo && mIsRptDOCtShowMarginInfo)
            {
                SQL.AppendLine("(((IfNull(B.UPrice, 0.00) - IfNull(F.UPrice, 0.00)) / IfNull(F.UPrice, 1.00)) * 100) As Margin, ");
            }
            SQL.AppendLine("A.Remark ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblCustomer C on A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblItem D on B.ItCode=D.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=D.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsItGrpCodeShow)
                SQL.AppendLine("Left join TblItemGroup E On D.ItGrpCode=E.ItGrpCode ");
            if (mIsRptDOCtShowPriceInfo)
                SQL.AppendLine("Left join TblStockPrice F On B.Source=F.Source ");
            SQL.AppendLine("Where A.Status In ('O', 'A') ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                   Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Period", 
                        "Document#",
                        "Date",
                        "Status",
                        "Customer",
                        
                        //6-10
                        "Item's Code",
                        "Item's Name",
                        "Foreign Name",
                        "Batch#",
                        "Quantity",
                        
                        //11-15
                        "UoM",
                        "Group",
                        "Selling Price",
                        "Price",
                        "Margin",

                        //16
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 130, 100, 100, 200,  

                        //6-10
                        100, 200, 230, 200, 100, 
                      
                        //11-15
                        100, 150, 130, 130, 130, 
                        
                        //16
                        250
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 13, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 15 });
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 16 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8, 16 }, false);
            if (!mIsItGrpCodeShow) Sm.GrdColInvisible(Grd1, new int[] { 12 }, false);
            if (!mIsRptDOCtShowPriceInfo) Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, false);

            //if (mIsRptDOCtShowPriceInfo && mIsRptDOCtShowMarginInfo) Sm.GrdColInvisible(Grd1, new int[] { 15 }, true);
            //else Sm.GrdColInvisible(Grd1, new int[] { 15 }, false);
            
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 16 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " And 0 = 0 ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "D.ItName", "D.ForeignName" });

                var s = new List<string>()
                    {
                        "DocDt", 
                        "DocNo", "StatusDesc", "CtName", "ItCode", "ItName", 
                        "ForeignName", "BatchNo", "Qty", "InventoryUomCode"
                    };
                if (mIsItGrpCodeShow) s.Add("ItGrpName");
                if (mIsRptDOCtShowPriceInfo) 
                {
                    s.Add("UPrice1");
                    s.Add("UPrice2");
                }
                if (mIsRptDOCtShowPriceInfo && mIsRptDOCtShowMarginInfo)
                {
                    s.Add("Margin");
                }
                s.Add("Remark");

                var DocDt = string.Empty;
                int i = 0;

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                        s.ToArray(),
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            DocDt = dr.GetString(c[0]);
                            Grd.Cells[Row, 1].Value = Sm.Left(DocDt, 4) + "-" + DocDt.Substring(4, 2);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Grd.Cells[Row, 3].Value = Sm.ConvertDate(DocDt);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            i = 10;
                            if (mIsItGrpCodeShow)
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 12, i++);
                            if (mIsRptDOCtShowPriceInfo)
                            {
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 13, i++);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 14, i++);
                            }
                            if (mIsRptDOCtShowPriceInfo && mIsRptDOCtShowMarginInfo)
                            {
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 15, i++);
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, i++);

                        }, true, false, false, false
                    );
                if (ChkShowTotal.Checked)
                {
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10 });
                }

                if (mIsRptDOCtShowMarginInfo && mIsRptDOCtShowPriceInfo) ComputeAvg();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void ComputeAvg()
        {
            decimal mAvgPrice = 0m, mAvgSellingPrice = 0m, mAvgMargin = 0m;
            int mRowCount = 0;

            if (Grd1.Rows.Count > 0)
            {
                for (int x = 0; x < Grd1.Rows.Count; x++)
                {
                    if (Sm.GetGrdStr(Grd1, x, 2).Length > 0)
                    {
                        mAvgSellingPrice += Sm.GetGrdDec(Grd1, x, 13);
                        mAvgPrice += Sm.GetGrdDec(Grd1, x, 14);
                        mAvgMargin += Sm.GetGrdDec(Grd1, x, 15);
                        mRowCount += 1;
                    }
                }

                if (mRowCount > 0)
                {
                    mAvgSellingPrice = mAvgSellingPrice / mRowCount;
                    mAvgPrice = mAvgPrice / mRowCount;
                    mAvgMargin = mAvgMargin / mRowCount;
                }
            }

            TxtAvgPrice.EditValue = Sm.FormatNum(mAvgPrice, 0);
            TxtAvgSellingPrice.EditValue = Sm.FormatNum(mAvgSellingPrice, 0);
            TxtAvgMargin.EditValue = Sm.FormatNum(mAvgMargin, 0);
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtAvgPrice_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAvgPrice, 0);
        }

        private void TxtAvgSellingPrice_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAvgSellingPrice, 0);
        }

        private void TxtAvgMargin_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAvgMargin, 0);
        }

        #endregion
    }
}
