﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmEmpPenalty : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmEmpPenaltyFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpPenalty(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employe Penalty";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtEmpCode, TxtEmpName,TxtDeptCode, TxtPosCode, 
                        LuePenalty, TxtAmt, LuePayment, TxtValuePayment
                    }, true);

                base.FrmLoad(sender, e);
                SetLuePenalty(ref LuePenalty);
                SetLuePayment(ref LuePayment);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LuePenalty, TxtAmt, LuePayment, TxtValuePayment, MeeRemark
                    }, true);
                    BtnEmpCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LuePenalty, TxtAmt, LuePayment, TxtValuePayment, MeeRemark
                    }, false);
                    BtnEmpCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtEmpCode, TxtEmpName, TxtDeptCode, 
                TxtPosCode, LuePenalty, TxtAmt, LuePayment, TxtValuePayment, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt, TxtValuePayment }, 0);
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmEmpPenaltyFind(this);
           Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpPenalty", "tblemppenalty");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpPenalty(DocNo));
            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                Sm.IsLueEmpty(LuePenalty, "Penalty") ||
                Sm.IsTxtEmpty(TxtAmt, "Penalty amount", true) ||
                Sm.IsLueEmpty(LuePayment, "Payment type") ||
                Sm.IsTxtEmpty(TxtValuePayment, "Number of payment", true);
                
        }

        private MySqlCommand SaveEmpPenalty(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmppenalty ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, EmpCode, PenaltyCode, AmtPenalty, PaymentType, PaymentValue, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @DocDt, 'N', EmpCode, @PenaltyCode, @AmtPenalty, @PaymentType, @PaymentValue, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblEmployee Where EmpCode=@EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@PenaltyCode", Sm.GetLue(LuePenalty));
            Sm.CmParam<Decimal>(ref cm, "@AmtPenalty", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePayment));
            Sm.CmParam<Decimal>(ref cm, "@PaymentValue", Decimal.Parse(TxtValuePayment.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpPenalty());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", false) ||
                Sm.IsLueEmpty(LuePenalty, "Penalty") ||
                IsDataCancelledAlready();
        }
        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblEmpPenalty " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }
        private MySqlCommand EditEmpPenalty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpPenalty Set ");
            SQL.AppendLine("CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            //Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpPenalty(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpPenalty(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, CancelInd, A.EmpCode, B.EmpName, C.DeptName, D.PosName, ");
            SQL.AppendLine("A.PenaltyCode, A.AmtPenalty, A.PaymentType, A.PaymentValue, A.Remark  ");
            SQL.AppendLine("From TblEmpPenalty A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode  ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode  ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "EmpCode", "EmpName",  "DeptName",   

                        //6-10
                        "PosName", "PenaltyCode", "AmtPenalty", "PaymentType", "PaymentValue",
                        
                        //11
                        "Remark"
                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);

                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[4]);
                    TxtDeptCode.EditValue = Sm.DrStr(dr, c[5]);

                    TxtPosCode.EditValue = Sm.DrStr(dr, c[6]);
                    Sm.SetLue(LuePenalty, Sm.DrStr(dr, c[7]));
                    TxtAmt.EditValue = Sm.DrDec(dr, c[8]);
                    Sm.SetLue(LuePayment, Sm.DrStr(dr, c[9]));
                    TxtValuePayment.EditValue = Sm.DrDec(dr, c[10]);

                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                }, true
        );

        }

        #endregion

        #region Additional Method

        private void SetLuePenalty(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PenaltyCode As Col1, PenaltyName As Col2 From TblPenalty",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLuePayment(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='PenaltyPaymentType'",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
         Sm.FormShowDialog(new FrmEmpPenaltyDlg(this));
        }

        private void LuePenalty_EditValueChanged(object sender, EventArgs e)
        {
           
            if (Sm.GetLue(LuePenalty).Length > 0)
            {
                string penaltycode = Sm.GetLue(LuePenalty);
                String Amt = Sm.GetValue("Select Amt From tblpenalty Where PenaltyCode='" + penaltycode + "' ");
                TxtAmt.EditValue = Amt;
            }
            Sm.RefreshLookUpEdit(LuePenalty, new Sm.RefreshLue1(SetLuePenalty));
            Sm.FormatNumTxt(TxtAmt, 0);
           
        }

        private void LuePayment_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayment, new Sm.RefreshLue1(SetLuePayment));
        }
        private void TxtAmt_Validated_1(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
            TxtAmt.EditValue = Math.Truncate(decimal.Parse(TxtAmt.Text));
            Sm.FormatNumTxt(TxtAmt, 0);
        }
        private void TxtValuePayment_Validated_1(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtValuePayment, 0);
            TxtValuePayment.EditValue = Math.Truncate(decimal.Parse(TxtValuePayment.Text));
            Sm.FormatNumTxt(TxtValuePayment, 0);
        }
     
        #endregion

    }
}
