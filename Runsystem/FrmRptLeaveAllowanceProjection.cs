﻿#region Update
/*
    12/03/2018 [WED] Leave yang muncul lihat ke Start Dt Leave nya sesuai tahun yang di filter
    15/03/2018 [WED] tambah total
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptLeaveAllowanceProjection : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty,
            mAnnualLeaveCode = string.Empty,
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty;

        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptLeaveAllowanceProjection(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mLongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueOption(ref LueEmployment, "EmploymentStatus");
                Sl.SetLueLevelCode(ref LueLevel);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                SetLueStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("Select T1.EmpCode, T2.EmpName, T1.LeaveCode, T3.LeaveName, T1.LeaveDt, Count(T1.LeaveDt) As MonthlyLeaveCount ");
            //SQL.AppendLine("From ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select B.DocNo, A.DNo, B.EmpCode, B.LeaveCode, Left(A.LeaveDt, 6) As LeaveDt ");
            //SQL.AppendLine("    From tblempleavedtl A ");
            //SQL.AppendLine("    Inner Join tblempleavehdr B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    Where B.CancelInd = 'N' ");
            //SQL.AppendLine("    And B.`Status` <> 'C' ");
            //SQL.AppendLine("    And Left(A.LeaveDt, 4) = @Yr ");
            //SQL.AppendLine("    And B.LeaveCode In (@AnnualLeaveCode , @LongServiceLeaveCode, @LongServiceLeaveCode2) ");
            //SQL.AppendLine(")T1 ");
            //SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");

            #endregion

            SQL.AppendLine("Select T1.EmpCode, T2.EmpName, T1.LeaveCode, T3.LeaveName, Left(T1.StartDt, 6) As LeaveDt, Count(T1.StartDt) As MonthlyLeaveCount ");
            SQL.AppendLine("From TblLeaveSummary T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");

            if (Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine(" And T2.SiteCode = @SiteCode ");

            if(Sm.GetLue(LueDeptCode).Length > 0)
                SQL.AppendLine(" And T2.DeptCode = @DeptCode ");

            if(Sm.GetLue(LueEmployment).Length > 0)
                SQL.AppendLine(" And T2.EmploymentStatus = @EmploymentStatus ");

            if(Sm.GetLue(LueStatus).Length > 0)
            {
                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        SQL.AppendLine(" And (T2.ResignDt Is Null Or (T2.ResignDt Is Not Null And T2.ResignDt>@CurrentDate)) ");
                        break;
                    case "2":
                        SQL.AppendLine(" And T2.ResignDt Is Not Null And T2.ResignDt<=@CurrentDate ");
                        break;
                }
            }

            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode = T3.LeaveCode ");
            if (Sm.GetLue(LueLevel).Length > 0)
            {
                SQL.AppendLine("Inner Join TblGradeLevelHdr T4 On T2.GrdLvlCode=T4.GrdLvlCode ");
                SQL.AppendLine("Inner Join TblLevelHdr T5 On T4.LevelCode=T5.LevelCode ");
                SQL.AppendLine("And T4.LevelCode = @LevelCode ");
            }
            
            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 31;
            Grd1.FrozenArea.ColCount = 5;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;

            Grd1.Cols[1].Width = 100;
            Grd1.Header.Cells[0, 1].Value = "Employee#";
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 1].SpanRows = 2;

            Grd1.Cols[2].Width = 200;
            Grd1.Header.Cells[0, 2].Value = "Employee Name";
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 2].SpanRows = 2;

            Grd1.Cols[3].Width = 0;
            Grd1.Header.Cells[0, 3].Value = "Leave Code";
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 3].SpanRows = 2;

            Grd1.Cols[4].Width = 160;
            Grd1.Header.Cells[0, 4].Value = "Leave Name";
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 4].SpanRows = 2;

            Grd1.Header.Cells[1, 5].Value = "Total";
            Grd1.Header.Cells[1, 5].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 5].SpanCols = 2;
            Grd1.Header.Cells[0, 5].Value = "Qty";
            Grd1.Header.Cells[0, 6].Value = "Amount";
            Grd1.Cols[5].Width = 130;
            Grd1.Cols[6].Width = 130;

            Grd1.Header.Cells[1, 7].Value = "January";
            Grd1.Header.Cells[1, 7].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 7].SpanCols = 2;
            Grd1.Header.Cells[0, 7].Value = "Qty";
            Grd1.Header.Cells[0, 8].Value = "Amount";
            Grd1.Cols[7].Width = 130;
            Grd1.Cols[8].Width = 130;

            Grd1.Header.Cells[1, 9].Value = "February";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 2;
            Grd1.Header.Cells[0, 9].Value = "Qty";
            Grd1.Header.Cells[0, 10].Value = "Amount";
            Grd1.Cols[9].Width = 130;
            Grd1.Cols[10].Width = 130;

            Grd1.Header.Cells[1, 11].Value = "March";
            Grd1.Header.Cells[1, 11].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 11].SpanCols = 2;
            Grd1.Header.Cells[0, 11].Value = "Qty";
            Grd1.Header.Cells[0, 12].Value = "Amount";
            Grd1.Cols[11].Width = 130;
            Grd1.Cols[12].Width = 130;

            Grd1.Header.Cells[1, 13].Value = "April";
            Grd1.Header.Cells[1, 13].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 13].SpanCols = 2;
            Grd1.Header.Cells[0, 13].Value = "Qty";
            Grd1.Header.Cells[0, 14].Value = "Amount";
            Grd1.Cols[13].Width = 130;
            Grd1.Cols[14].Width = 130;

            Grd1.Header.Cells[1, 15].Value = "May";
            Grd1.Header.Cells[1, 15].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 15].SpanCols = 2;
            Grd1.Header.Cells[0, 15].Value = "Qty";
            Grd1.Header.Cells[0, 16].Value = "Amount";
            Grd1.Cols[15].Width = 130;
            Grd1.Cols[16].Width = 130;

            Grd1.Header.Cells[1, 17].Value = "June";
            Grd1.Header.Cells[1, 17].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 17].SpanCols = 2;
            Grd1.Header.Cells[0, 17].Value = "Qty";
            Grd1.Header.Cells[0, 18].Value = "Amount";
            Grd1.Cols[17].Width = 130;
            Grd1.Cols[18].Width = 130;

            Grd1.Header.Cells[1, 19].Value = "July";
            Grd1.Header.Cells[1, 19].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 19].SpanCols = 2;
            Grd1.Header.Cells[0, 19].Value = "Qty";
            Grd1.Header.Cells[0, 20].Value = "Amount";
            Grd1.Cols[19].Width = 130;
            Grd1.Cols[20].Width = 130;

            Grd1.Header.Cells[1, 21].Value = "August";
            Grd1.Header.Cells[1, 21].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 21].SpanCols = 2;
            Grd1.Header.Cells[0, 21].Value = "Qty";
            Grd1.Header.Cells[0, 22].Value = "Amount";
            Grd1.Cols[21].Width = 130;
            Grd1.Cols[22].Width = 130;

            Grd1.Header.Cells[1, 23].Value = "September";
            Grd1.Header.Cells[1, 23].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 23].SpanCols = 2;
            Grd1.Header.Cells[0, 23].Value = "Qty";
            Grd1.Header.Cells[0, 24].Value = "Amount";
            Grd1.Cols[23].Width = 130;
            Grd1.Cols[24].Width = 130;

            Grd1.Header.Cells[1, 25].Value = "October";
            Grd1.Header.Cells[1, 25].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 25].SpanCols = 2;
            Grd1.Header.Cells[0, 25].Value = "Qty";
            Grd1.Header.Cells[0, 26].Value = "Amount";
            Grd1.Cols[25].Width = 130;
            Grd1.Cols[26].Width = 130;

            Grd1.Header.Cells[1, 27].Value = "November";
            Grd1.Header.Cells[1, 27].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 27].SpanCols = 2;
            Grd1.Header.Cells[0, 27].Value = "Qty";
            Grd1.Header.Cells[0, 28].Value = "Amount";
            Grd1.Cols[27].Width = 130;
            Grd1.Cols[28].Width = 130;

            Grd1.Header.Cells[1, 29].Value = "December";
            Grd1.Header.Cells[1, 29].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 29].SpanCols = 2;
            Grd1.Header.Cells[0, 29].Value = "Qty";
            Grd1.Header.Cells[0, 30].Value = "Amount";
            Grd1.Cols[29].Width = 130;
            Grd1.Cols[30].Width = 130;

            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 }, 2);
            for (int Index = 0; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var lLeaveTrx = new List<LeaveTrx>();
                //var lDLT = new List<DistinctLeaveTrx>();
                PrepareLeaveTrx(ref lLeaveTrx);
                if (lLeaveTrx.Count > 0)
                {
                    ShowLeaveTrx(ref lLeaveTrx);
                    ShowQty(ref lLeaveTrx);
                    //PrepareQty(ref lDLT);
                    //ShowQtyOld(ref lDLT);
                    CalculateAmt();

                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    return;
                }
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void SetLueStatus(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select '1' As Col1, 'Active Employee' As Col2 Union All " +
                "Select '2' As Col1, 'Resignee' As Col2; ",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        private void PrepareLeaveTrx(ref List<LeaveTrx> lLeaveTrx)
        {
            lLeaveTrx.Clear();
            string Filter = " Where Left(T1.StartDt, 4) = @Yr ";

            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@AnnualLeaveCode", mAnnualLeaveCode);
                Sm.CmParam<String>(ref cm, "@LongServiceLeaveCode", mLongServiceLeaveCode);
                Sm.CmParam<String>(ref cm, "@LongServiceLeaveCode2", mLongServiceLeaveCode2);
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmployment));
                Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetLue(LueLevel));
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

                cm.CommandText = GetSQL() + Filter + " Group By T1.EmpCode, T2.EmpName, T1.LeaveCode, T3.LeaveName, T1.StartDt Order By T2.EmpName, T1.LeaveCode, T1.StartDt; ";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "EmpName", "LeaveCode", "LeaveName", "LeaveDt", "MonthlyLeaveCount" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lLeaveTrx.Add(new LeaveTrx()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            LeaveCode= Sm.DrStr(dr, c[2]),
                            LeaveName = Sm.DrStr(dr, c[3]),
                            LeaveDt = Sm.DrStr(dr, c[4]),
                            MonthlyLeaveCount = Sm.DrDec(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowLeaveTrx(ref List<LeaveTrx> lLeaveTrx)
        {
            #region Old Code
            //var dlt = from x in lLeaveTrx
            //          group x by new
            //          {
            //              x.EmpCode,
            //              x.EmpName,
            //              x.LeaveCode,
            //              x.LeaveName
            //          }
            //              into distinctx
            //              select new DistinctLeaveTrx()
            //              {
            //                  EmpCode = distinctx.Key.EmpCode,
            //                  EmpName = distinctx.Key.EmpName,
            //                  LeaveCode = distinctx.Key.LeaveCode,
            //                  LeaveName = distinctx.Key.LeaveName,
            //                  DLT = distinctx.ToList(),
            //              };
            #endregion

            iGRow r;
            int no = 1;
            foreach (var t in lLeaveTrx)
            {
                r = Grd1.Rows.Add();

                r.Cells[0].Value = no++;
                r.Cells[1].Value = t.EmpCode;
                r.Cells[2].Value = t.EmpName;
                r.Cells[3].Value = t.LeaveCode;
                r.Cells[4].Value = t.LeaveName;
                r.Cells[5].Value = t.MonthlyLeaveCount;
                for (int j = 6; j < Grd1.Cols.Count; j++)
                    r.Cells[j].Value = 0m;
            }
        }

        private void ShowQty(ref List<LeaveTrx> lLeaveTrx)
        {
            for (int i = 0; i < lLeaveTrx.Count; i++)
            {
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "01") Grd1.Cells[i, 7].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "02") Grd1.Cells[i, 9].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "03") Grd1.Cells[i, 11].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "04") Grd1.Cells[i, 13].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "05") Grd1.Cells[i, 15].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "06") Grd1.Cells[i, 17].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "07") Grd1.Cells[i, 19].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "08") Grd1.Cells[i, 21].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "09") Grd1.Cells[i, 23].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "10") Grd1.Cells[i, 25].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "11") Grd1.Cells[i, 27].Value = lLeaveTrx[i].MonthlyLeaveCount;
                if (Sm.Right(lLeaveTrx[i].LeaveDt, 2) == "12") Grd1.Cells[i, 29].Value = lLeaveTrx[i].MonthlyLeaveCount;
            }
        }

        #region Old Function

        //private void PrepareQty(ref List<DistinctLeaveTrx> lDLT)
        //{
        //    lDLT.Clear();
        //    string Filter = " Where 0 = 0 ";

        //    var cm = new MySqlCommand();

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
        //        Sm.CmParam<String>(ref cm, "@AnnualLeaveCode", mAnnualLeaveCode);
        //        Sm.CmParam<String>(ref cm, "@LongServiceLeaveCode", mLongServiceLeaveCode);
        //        Sm.CmParam<String>(ref cm, "@LongServiceLeaveCode2", mLongServiceLeaveCode2);
        //        Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
        //        Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
        //        Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmployment));
        //        Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetLue(LueLevel));
        //        Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

        //        cm.CommandText = GetSQL() + Filter + " Group By T1.EmpCode, T2.EmpName, T1.LeaveCode, T3.LeaveName, T1.LeaveDt Order By T2.EmpName, T1.LeaveCode, T1.LeaveDt; ";
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "EmpName", "LeaveCode", "LeaveName", "LeaveDt", "MonthlyLeaveCount" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                lDLT.Add(new DistinctLeaveTrx()
        //                {
        //                    EmpCode = Sm.DrStr(dr, c[0]),
        //                    EmpName = Sm.DrStr(dr, c[1]),
        //                    LeaveCode = Sm.DrStr(dr, c[2]),
        //                    LeaveName = Sm.DrStr(dr, c[3]),
        //                    LeaveDt = Sm.DrStr(dr, c[4]),
        //                    MonthlyLeaveCount = Sm.DrDec(dr, c[5]),
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private void ShowQtyOld(ref List<DistinctLeaveTrx> lDLT)
        //{
        //    for (int i = 0; i < Grd1.Rows.Count; i++)
        //    {
        //        for (int j = 0; j < lDLT.Count; j++)
        //        {
        //            if(Sm.GetGrdStr(Grd1, i, 1) == lDLT[j].EmpCode && Sm.GetGrdStr(Grd1, i, 3) == lDLT[j].LeaveCode)
        //            {
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "01") Grd1.Cells[i, 7].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "02") Grd1.Cells[i, 9].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "03") Grd1.Cells[i, 11].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "04") Grd1.Cells[i, 13].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "05") Grd1.Cells[i, 15].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "06") Grd1.Cells[i, 17].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "07") Grd1.Cells[i, 19].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "08") Grd1.Cells[i, 21].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "09") Grd1.Cells[i, 23].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "10") Grd1.Cells[i, 25].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "11") Grd1.Cells[i, 27].Value = lDLT[j].MonthlyLeaveCount;
        //                if (Sm.Right(lDLT[j].LeaveDt, 2) == "12") Grd1.Cells[i, 29].Value = lDLT[j].MonthlyLeaveCount;
        //            }
        //        }

        //        for (int k = 0; k < lDLT.Count(); k++)
        //        {
        //            if (Sm.GetGrdStr(Grd1, i, 1) == lDLT[k].EmpCode && Sm.GetGrdStr(Grd1, i, 3) == lDLT[k].LeaveCode)
        //            {
        //                Grd1.Cells[i, 5].Value = Sm.GetGrdDec(Grd1, i, 5) + lDLT[k].MonthlyLeaveCount;
        //            }
        //        }
        //    }
        //}

        #endregion

        private void CalculateAmt()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                for (int j = 7; j < Grd1.Cols.Count; j++)
                {
                    if((j % 2) != 0)
                    {
                        if ((Sm.GetGrdStr(Grd1, i, 3) == mAnnualLeaveCode || Sm.GetGrdStr(Grd1, i, 3) == mLongServiceLeaveCode2) && Sm.GetGrdDec(Grd1, i, j) > 0)
                            Grd1.Cells[i, (j + 1)].Value = 1 * Decimal.Parse(Sm.GetValue("Select IfNull(Amt, 0) As Amt From TblEmployeeSalary Where EmpCode = '" + Sm.GetGrdStr(Grd1, i, 1) + "' And ActInd = 'Y' Order By StartDt Desc Limit 1;"));

                        if ((Sm.GetGrdStr(Grd1, i, 3) == mLongServiceLeaveCode) && Sm.GetGrdDec(Grd1, i, j) > 0)
                            Grd1.Cells[i, (j + 1)].Value = 2 * Decimal.Parse(Sm.GetValue("Select IfNull(Amt, 0) As Amt From TblEmployeeSalary Where EmpCode = '" + Sm.GetGrdStr(Grd1, i, 1) + "' And ActInd = 'Y' Order By StartDt Desc Limit 1;"));
                    }
                }

                for (int k = 7; k < Grd1.Cols.Count; k++)
                {
                    if((k % 2) == 0)
                    {
                        Grd1.Cells[i, 6].Value = Sm.GetGrdDec(Grd1, i, 6) + Sm.GetGrdDec(Grd1, i, k);
                    }
                }
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueEmployment_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmployment, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkEmployment_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Employment");
        }

        private void LueLevel_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLevel, new Sm.RefreshLue1(Sl.SetLueLevelCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLevel_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Level");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(SetLueStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

        #endregion

        #region Class

        class LeaveTrx
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveName { get; set; }
            public string LeaveDt { get; set; }
            public decimal MonthlyLeaveCount { get; set; }
        }

        //class DistinctLeaveTrx
        //{
        //    public string EmpCode { get; set; }
        //    public string EmpName { get; set; }
        //    public string LeaveCode { get; set; }
        //    public string LeaveName { get; set; }
        //    public string LeaveDt { get; set; }
        //    public decimal MonthlyLeaveCount { get; set; }
        //    public List<LeaveTrx> DLT { get; set; }
        //}

        #endregion

    }
}
