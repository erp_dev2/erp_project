﻿namespace RunSystem
{
    partial class FrmRptBalanceSheet3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueMth = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStartFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.ChkEntCode = new DevExpress.XtraEditors.CheckEdit();
            this.LblLevel2 = new System.Windows.Forms.Label();
            this.LueLevelTo = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLevelFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.LblLevel = new System.Windows.Forms.Label();
            this.LueDt = new DevExpress.XtraEditors.LookUpEdit();
            this.LblDt = new System.Windows.Forms.Label();
            this.LuePeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.LblPeriod = new System.Windows.Forms.Label();
            this.ChkPeriod = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStartFrom.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPeriod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(780, 0);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LuePeriod);
            this.panel2.Controls.Add(this.LblPeriod);
            this.panel2.Controls.Add(this.ChkPeriod);
            this.panel2.Controls.Add(this.LueDt);
            this.panel2.Controls.Add(this.LblDt);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.LueStartFrom);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueMth);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(780, 120);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.White;
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(780, 353);
            this.Grd1.TabIndex = 30;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 120);
            this.panel3.Size = new System.Drawing.Size(780, 353);
            // 
            // LueMth
            // 
            this.LueMth.EnterMoveNextControl = true;
            this.LueMth.Location = new System.Drawing.Point(69, 27);
            this.LueMth.Margin = new System.Windows.Forms.Padding(5);
            this.LueMth.Name = "LueMth";
            this.LueMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.Appearance.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMth.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMth.Properties.DropDownRows = 13;
            this.LueMth.Properties.NullText = "[Empty]";
            this.LueMth.Properties.PopupWidth = 100;
            this.LueMth.Size = new System.Drawing.Size(75, 20);
            this.LueMth.TabIndex = 11;
            this.LueMth.ToolTip = "F4 : Show/hide list";
            this.LueMth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(24, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Month";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(69, 5);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 100;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 9;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(34, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Year";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(76, 91);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 27;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(120, 88);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 300;
            this.LueEntCode.Size = new System.Drawing.Size(300, 20);
            this.LueEntCode.TabIndex = 28;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // LueStartFrom
            // 
            this.LueStartFrom.EnterMoveNextControl = true;
            this.LueStartFrom.Location = new System.Drawing.Point(69, 49);
            this.LueStartFrom.Margin = new System.Windows.Forms.Padding(5);
            this.LueStartFrom.Name = "LueStartFrom";
            this.LueStartFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.Appearance.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStartFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStartFrom.Properties.DropDownRows = 12;
            this.LueStartFrom.Properties.NullText = "[Empty]";
            this.LueStartFrom.Properties.PopupWidth = 100;
            this.LueStartFrom.Size = new System.Drawing.Size(75, 20);
            this.LueStartFrom.TabIndex = 13;
            this.LueStartFrom.ToolTip = "F4 : Show/hide list";
            this.LueStartFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Start from";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkProfitCenterCode);
            this.panel4.Controls.Add(this.CcbProfitCenterCode);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.ChkEntCode);
            this.panel4.Controls.Add(this.LblLevel2);
            this.panel4.Controls.Add(this.LueLevelTo);
            this.panel4.Controls.Add(this.LueLevelFrom);
            this.panel4.Controls.Add(this.LblLevel);
            this.panel4.Controls.Add(this.LueEntCode);
            this.panel4.Controls.Add(this.LblEntCode);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(323, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(453, 116);
            this.panel4.TabIndex = 19;
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(424, 65);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenterCode.TabIndex = 26;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(120, 66);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(300, 20);
            this.CcbProfitCenterCode.TabIndex = 25;
            this.CcbProfitCenterCode.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(9, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "Multi Profit Center";
            // 
            // ChkEntCode
            // 
            this.ChkEntCode.Location = new System.Drawing.Point(424, 87);
            this.ChkEntCode.Name = "ChkEntCode";
            this.ChkEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEntCode.Properties.Appearance.Options.UseFont = true;
            this.ChkEntCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEntCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkEntCode.Properties.Caption = " ";
            this.ChkEntCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEntCode.Size = new System.Drawing.Size(18, 22);
            this.ChkEntCode.TabIndex = 29;
            this.ChkEntCode.ToolTip = "Remove filter";
            this.ChkEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEntCode.ToolTipTitle = "Run System";
            this.ChkEntCode.CheckedChanged += new System.EventHandler(this.ChkEntCode_CheckedChanged);
            // 
            // LblLevel2
            // 
            this.LblLevel2.AutoSize = true;
            this.LblLevel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLevel2.ForeColor = System.Drawing.Color.Black;
            this.LblLevel2.Location = new System.Drawing.Point(192, 47);
            this.LblLevel2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLevel2.Name = "LblLevel2";
            this.LblLevel2.Size = new System.Drawing.Size(19, 14);
            this.LblLevel2.TabIndex = 22;
            this.LblLevel2.Text = "to";
            this.LblLevel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevelTo
            // 
            this.LueLevelTo.EnterMoveNextControl = true;
            this.LueLevelTo.Location = new System.Drawing.Point(215, 44);
            this.LueLevelTo.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelTo.Name = "LueLevelTo";
            this.LueLevelTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.Appearance.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelTo.Properties.DropDownRows = 30;
            this.LueLevelTo.Properties.NullText = "[Empty]";
            this.LueLevelTo.Properties.PopupWidth = 300;
            this.LueLevelTo.Size = new System.Drawing.Size(68, 20);
            this.LueLevelTo.TabIndex = 23;
            this.LueLevelTo.ToolTip = "F4 : Show/hide list";
            this.LueLevelTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelTo.EditValueChanged += new System.EventHandler(this.LueLevelTo_EditValueChanged);
            // 
            // LueLevelFrom
            // 
            this.LueLevelFrom.EnterMoveNextControl = true;
            this.LueLevelFrom.Location = new System.Drawing.Point(120, 44);
            this.LueLevelFrom.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelFrom.Name = "LueLevelFrom";
            this.LueLevelFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.Appearance.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelFrom.Properties.DropDownRows = 30;
            this.LueLevelFrom.Properties.NullText = "[Empty]";
            this.LueLevelFrom.Properties.PopupWidth = 300;
            this.LueLevelFrom.Size = new System.Drawing.Size(68, 20);
            this.LueLevelFrom.TabIndex = 21;
            this.LueLevelFrom.ToolTip = "F4 : Show/hide list";
            this.LueLevelFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelFrom.EditValueChanged += new System.EventHandler(this.LueLevelFrom_EditValueChanged);
            // 
            // LblLevel
            // 
            this.LblLevel.AutoSize = true;
            this.LblLevel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLevel.ForeColor = System.Drawing.Color.Black;
            this.LblLevel.Location = new System.Drawing.Point(80, 47);
            this.LblLevel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLevel.Name = "LblLevel";
            this.LblLevel.Size = new System.Drawing.Size(35, 14);
            this.LblLevel.TabIndex = 20;
            this.LblLevel.Text = "Level";
            this.LblLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDt
            // 
            this.LueDt.EnterMoveNextControl = true;
            this.LueDt.Location = new System.Drawing.Point(69, 71);
            this.LueDt.Margin = new System.Windows.Forms.Padding(5);
            this.LueDt.Name = "LueDt";
            this.LueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDt.Properties.Appearance.Options.UseFont = true;
            this.LueDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDt.Properties.DropDownRows = 13;
            this.LueDt.Properties.NullText = "[Empty]";
            this.LueDt.Properties.PopupWidth = 100;
            this.LueDt.Size = new System.Drawing.Size(125, 20);
            this.LueDt.TabIndex = 15;
            this.LueDt.ToolTip = "F4 : Show/hide list";
            this.LueDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LblDt
            // 
            this.LblDt.AutoSize = true;
            this.LblDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDt.ForeColor = System.Drawing.Color.Red;
            this.LblDt.Location = new System.Drawing.Point(33, 74);
            this.LblDt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDt.Name = "LblDt";
            this.LblDt.Size = new System.Drawing.Size(33, 14);
            this.LblDt.TabIndex = 14;
            this.LblDt.Text = "Date";
            this.LblDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePeriod
            // 
            this.LuePeriod.EnterMoveNextControl = true;
            this.LuePeriod.Location = new System.Drawing.Point(69, 93);
            this.LuePeriod.Margin = new System.Windows.Forms.Padding(5);
            this.LuePeriod.Name = "LuePeriod";
            this.LuePeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePeriod.Properties.DropDownRows = 21;
            this.LuePeriod.Properties.NullText = "[Empty]";
            this.LuePeriod.Properties.PopupWidth = 100;
            this.LuePeriod.Size = new System.Drawing.Size(100, 20);
            this.LuePeriod.TabIndex = 17;
            this.LuePeriod.ToolTip = "F4 : Show/hide list";
            this.LuePeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePeriod.EditValueChanged += new System.EventHandler(this.LuePeriod_EditValueChanged);
            // 
            // LblPeriod
            // 
            this.LblPeriod.AutoSize = true;
            this.LblPeriod.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPeriod.Location = new System.Drawing.Point(24, 96);
            this.LblPeriod.Name = "LblPeriod";
            this.LblPeriod.Size = new System.Drawing.Size(41, 14);
            this.LblPeriod.TabIndex = 16;
            this.LblPeriod.Text = "Period";
            // 
            // ChkPeriod
            // 
            this.ChkPeriod.Location = new System.Drawing.Point(171, 91);
            this.ChkPeriod.Name = "ChkPeriod";
            this.ChkPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPeriod.Properties.Appearance.Options.UseFont = true;
            this.ChkPeriod.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPeriod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPeriod.Properties.Caption = " ";
            this.ChkPeriod.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPeriod.Size = new System.Drawing.Size(18, 22);
            this.ChkPeriod.TabIndex = 18;
            this.ChkPeriod.ToolTip = "Remove filter";
            this.ChkPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPeriod.ToolTipTitle = "Run System";
            this.ChkPeriod.CheckedChanged += new System.EventHandler(this.ChkPeriod_CheckedChanged);
            // 
            // FrmRptBalanceSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 473);
            this.Name = "FrmRptBalanceSheet";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStartFrom.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPeriod.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueMth;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LblEntCode;
        public DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private DevExpress.XtraEditors.LookUpEdit LueStartFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label LblLevel2;
        public DevExpress.XtraEditors.LookUpEdit LueLevelTo;
        public DevExpress.XtraEditors.LookUpEdit LueLevelFrom;
        private System.Windows.Forms.Label LblLevel;
        private DevExpress.XtraEditors.CheckEdit ChkEntCode;
        private DevExpress.XtraEditors.LookUpEdit LueDt;
        private System.Windows.Forms.Label LblDt;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LuePeriod;
        private System.Windows.Forms.Label LblPeriod;
        private DevExpress.XtraEditors.CheckEdit ChkPeriod;
    }
}