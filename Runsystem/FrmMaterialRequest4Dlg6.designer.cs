﻿namespace RunSystem
{
    partial class FrmMaterialRequest4Dlg6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtItCtName = new DevExpress.XtraEditors.TextEdit();
            this.TxtItCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.LueUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(520, 0);
            this.panel1.Size = new System.Drawing.Size(70, 94);
            // 
            // BtnProcess
            // 
            this.BtnProcess.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnProcess.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnProcess.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnProcess.Appearance.Options.UseBackColor = true;
            this.BtnProcess.Appearance.Options.UseFont = true;
            this.BtnProcess.Appearance.Options.UseForeColor = true;
            this.BtnProcess.Appearance.Options.UseTextOptions = true;
            this.BtnProcess.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnProcess.Size = new System.Drawing.Size(70, 33);
            this.BtnProcess.Text = "  &Save";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueUomCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtItName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtItCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtItCtName);
            this.panel2.Size = new System.Drawing.Size(520, 94);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Category";
            // 
            // TxtItCtName
            // 
            this.TxtItCtName.EnterMoveNextControl = true;
            this.TxtItCtName.Location = new System.Drawing.Point(67, 26);
            this.TxtItCtName.Name = "TxtItCtName";
            this.TxtItCtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtItCtName.Properties.MaxLength = 400;
            this.TxtItCtName.Properties.ReadOnly = true;
            this.TxtItCtName.Size = new System.Drawing.Size(447, 20);
            this.TxtItCtName.TabIndex = 13;
            // 
            // TxtItCtCode
            // 
            this.TxtItCtCode.EnterMoveNextControl = true;
            this.TxtItCtCode.Location = new System.Drawing.Point(67, 5);
            this.TxtItCtCode.Name = "TxtItCtCode";
            this.TxtItCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCtCode.Properties.MaxLength = 400;
            this.TxtItCtCode.Properties.ReadOnly = true;
            this.TxtItCtCode.Size = new System.Drawing.Size(123, 20);
            this.TxtItCtCode.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(33, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "UoM";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(30, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Item";
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(67, 68);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Size = new System.Drawing.Size(448, 20);
            this.TxtItName.TabIndex = 18;
            // 
            // LueUomCode
            // 
            this.LueUomCode.EnterMoveNextControl = true;
            this.LueUomCode.Location = new System.Drawing.Point(67, 47);
            this.LueUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueUomCode.Name = "LueUomCode";
            this.LueUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUomCode.Properties.DropDownRows = 30;
            this.LueUomCode.Properties.NullText = "[Empty]";
            this.LueUomCode.Properties.PopupWidth = 350;
            this.LueUomCode.Size = new System.Drawing.Size(123, 20);
            this.LueUomCode.TabIndex = 20;
            this.LueUomCode.ToolTip = "F4 : Show/hide list";
            this.LueUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUomCode.EditValueChanged += new System.EventHandler(this.LueUomCode_EditValueChanged);
            // 
            // FrmMaterialRequest4Dlg6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 94);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmMaterialRequest4Dlg6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Create New Item (Equal Category)";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueUomCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtItCtName;
        private DevExpress.XtraEditors.TextEdit TxtItCtCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtItName;
        private DevExpress.XtraEditors.LookUpEdit LueUomCode;
    }
}