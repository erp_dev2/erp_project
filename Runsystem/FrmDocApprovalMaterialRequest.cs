﻿#region Update
/*
    25/09/2020 [WED/IMS] tambah Item's Local Code dan Specification
    29/10/2021 [ARI/PHT] Menambah informasi approval time terakhir di Document approval saat klik loop Material Request
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalMaterialRequest : RunSystem.FrmBase9
    {
        #region Field

        internal string mDocNo = string.Empty, mDNo = string.Empty;
        private bool mIsUseItemConsumption = false;

        #endregion

        #region Constructor

        public FrmDocApprovalMaterialRequest(string DocNo, string DNo, bool IsUseItemConsumption)
        {
            InitializeComponent();
            mDocNo = DocNo;
            mDNo = DNo;
            mIsUseItemConsumption = IsUseItemConsumption;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetGrd();
                ShowData();
                ShowDocApproval();
                if (!mIsUseItemConsumption)
                {
                    label15.Visible = label16.Visible = label17.Visible =
                    label18.Visible = label19.Visible = label20.Visible = false;
                    TxtMth01.Visible = TxtMth03.Visible = TxtMth06.Visible =
                    TxtMth09.Visible = TxtMth12.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            ShowPicture();
            TxtDocNo.Focus();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        150, 
                        100, 130, 400
                    }
                );

                Grd1.Cols[2].CellStyle.FormatString = "{0:dd/MMM/yyyy HH:mm}";

        }

        private string GetItCode()
        {
            var cm = new MySqlCommand() 
            {
                CommandText = "Select ItCode From TblMaterialRequestDtl Where DocNo=@DocNo And DNo=@DNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", mDNo);

            return (Sm.GetValue(cm));
        }

        private void ShowData()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DNo, DocDt, DeptName, ");
            SQL.AppendLine("OptDesc, ItCode, ItName, ItCodeInternal, Specification, Qty, ");
            SQL.AppendLine("PurchaseUomCode, UsageDt, MinStock, ROP, InStock, OutStandingMaterialRequest, OutstandingPO, Mth01, Mth03, Mth06, Mth09, Mth12, Remark "); 
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select 'Material Request From Department' As DocType, T1.DocNo, T2.DNo, T1.DocDt, ");
            SQL.AppendLine("    T3.DeptName, T4.OptDesc, T2.ItCode, T5.ItName, T5.ItCodeInternal, T5.Specification, T2.Qty, T5.PurchaseUomCode, T2.UsageDt, ifnull(T5.MinStock, 0) As MinStock, Ifnull(T5.ReOrderStock, 0) As ROP, ");
            SQL.AppendLine("    IfNull(T6.InStock, 0) As InStock, ");
            SQL.AppendLine("    IfNull(T7.OutstandingPO, 0) As OutstandingPO, ");
            SQL.AppendLine("    IfNull(T8.OutstandingMaterialRequest, 0) As OutstandingMaterialRequest, ");
            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("ifnull(T9.Qty01, 0) As Mth01, ifnull(T9.Qty03, 0) As Mth03, ifnull(T9.Qty06, 0) As Mth06, ");
                SQL.AppendLine("ifnull(T9.Qty09, 0) As Mth09, ifnull(T9.Qty12, 0) As Mth12, ");
            }
            else
            {
                SQL.AppendLine("0 As Mth01, 0 As Mth03, 0 As Mth06, 0 As Mth09, 0 As Mth12, ");
            }
            SQL.AppendLine("Concat(IfNull(T1.Remark, ''), Case When IfNull(T1.Remark, '')<>'' And IfNull(T2.Remark, '')<>'' Then '\n' Else '' End, IfNull(T2.Remark, '')) As Remark  ");
            
            SQL.AppendLine("From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.DNo=@DNo "); 
            SQL.AppendLine("Inner Join TblDepartment T3 On T1.DeptCode=T3.DeptCode  ");
            SQL.AppendLine("Left Join TblOption T4 On T4.OptCat='ReqType' And T1.ReqType=T4.OptCode ");
            SQL.AppendLine("Inner Join TblItem T5 On T2.ItCode=T5.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select @ItCode As ItCode, Sum(Qty) As InStock ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where ItCode=@ItCode ");
            SQL.AppendLine("    And Qty>0 ");
            SQL.AppendLine(") T6 On T2.ItCode=T6.ItCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select ItCode, Sum(Qty) OutstandingPO From (");
            SQL.AppendLine("        Select C.ItCode, A.Qty-IfNull(Qty2, 0)-IfNull(Qty3, 0) As Qty ");
            SQL.AppendLine("        From TblPODtl A ");
            SQL.AppendLine("        Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl C ");
            SQL.AppendLine("            On B.MaterialRequestDocNo=C.DocNo ");
            SQL.AppendLine("            And B.MaterialRequestDNo=C.DNo ");
            SQL.AppendLine("		    And ItCode=@ItCode ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select D1.PODocNo As DocNo, D1.PODNo As DNo, Sum(D1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("            From TblRecvVdDtl D1 ");
            SQL.AppendLine("            Inner Join TblPODtl D2 On D1.PODocNo=D2.DocNo And D1.PODNo=D2.DNo And D2.ProcessInd<>'F' And D2.CancelInd='N' ");
            SQL.AppendLine("		    Where D1.ItCode=@ItCode ");
            SQL.AppendLine("            And D1.CancelInd='N' ");
            SQL.AppendLine("            Group By D1.PODocNo, D1.PODNo ");
            SQL.AppendLine("        ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select E1.PODocNo As DocNo, E1.PODNo As DNo, Sum(E1.Qty) As Qty3 ");
            SQL.AppendLine("            From TblPOQtyCancel E1 ");
            SQL.AppendLine("            Inner Join TblPODtl E2 On E1.PODocNo=E2.DocNo And E1.PODNo=E2.DNo And E2.ProcessInd<>'F' And E2.CancelInd='N' ");
            SQL.AppendLine("            Inner Join TblPORequestDtl E3 On E2.PORequestDocNo=E3.DocNo And E2.PORequestDNo=E3.DNo ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl E4 ");
            SQL.AppendLine("                On E3.MaterialRequestDocNo=E4.DocNo ");
            SQL.AppendLine("                And E3.MaterialRequestDNo=E4.DNo ");
            SQL.AppendLine("		        And E4.ItCode=@ItCode ");
            SQL.AppendLine("            Where E1.CancelInd='N' ");
            SQL.AppendLine("            Group By E1.PODocNo, E1.PODNo ");
            SQL.AppendLine("        ) E On A.DocNo=E.DocNo And A.DNo=E.DNo ");
            SQL.AppendLine("		Where A.ProcessInd<>'F' And A.CancelInd='N' ");
            SQL.AppendLine("    ) Tbl Where Qty>0 Group By ItCode ");
            SQL.AppendLine(") T7 On T2.ItCode=T7.ItCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select ItCode, Sum(Qty) OutstandingMaterialRequest From (");
            SQL.AppendLine("        Select A.ItCode, (A.Qty-IfNull(B.Qty, 0)) As Qty ");
            SQL.AppendLine("        From TblMaterialRequestDtl A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select B1.DocNo As DocNo, B1.DNo As DNo, Sum(B1.Qty) As Qty ");
            SQL.AppendLine("            From TblPODtl B1 ");
            SQL.AppendLine("            Inner Join TblPORequestDtl B2 On B1.PORequestDocNo=B2.DocNo And B1.PORequestDNo=B2.DNo ");
            SQL.AppendLine("            Inner Join TblMaterialRequestDtl B3 ");
            SQL.AppendLine("                On B2.MaterialRequestDocNo=B3.DocNo ");
            SQL.AppendLine("                And B2.MaterialRequestDNo=B3.DNo ");
            SQL.AppendLine("                And B3.ProcessInd<>'F' ");
            SQL.AppendLine("		        And B3.ItCode=@ItCode ");
            SQL.AppendLine("            Where B1.CancelInd='N' ");
            SQL.AppendLine("            Group By B1.DocNo, B1.DNo ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("        Where A.CancelInd='N' And A.ProcessInd<>'F' And IfNull(A.Status, '')<>'C' ");
            SQL.AppendLine("        And A.ItCode In ( ");
            SQL.AppendLine("		    Select Distinct ItCode From TblMaterialRequestDtl ");
            SQL.AppendLine("		    Where CancelInd='N' And IfNull(Status, '') Not In ('A', 'C') ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) Tbl Where Qty>0 Group By ItCode ");
            SQL.AppendLine(") T8 On T2.ItCode=T8.ItCode ");

            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select @ItCode As ItCode, Sum(Qty01) As Qty01, Sum(Qty03) As Qty03, Sum(Qty06) As Qty06, Sum(Qty09) As Qty09, Sum(Qty12) As Qty12 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select T1.Mth, Sum(T2.Qty)  As Qty ");
                SQL.AppendLine("            From ( ");
                SQL.AppendLine("                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("            ) T1 ");
                SQL.AppendLine("            Inner Join ( ");
                SQL.AppendLine("                Select  convert('01' using latin1) As Mth, Sum(B.Qty) As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' And B.ItCode=@ItCode ");
                SQL.AppendLine("	            Where A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('03' using latin1) As Mth, Sum(B.Qty)/3 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('06' using latin1) As Mth, Sum(B.Qty)/6 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N'  And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('09' using latin1) As Mth, Sum(B.Qty)/9 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('12' using latin1) As Mth, Sum(B.Qty)/12 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N'  And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("            ) T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.Mth ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("    )Z2 ");
                SQL.AppendLine(") T9 On T2.ItCode=T9.ItCode ");
            }
            SQL.AppendLine("Where T1.DocNo=@DocNo ");
            SQL.AppendLine(") ST ");

            var cm = new MySqlCommand();

            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", mDNo);
            Sm.CmParam<String>(ref cm, "@ItCode", GetItCode());

            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "DeptName", "OptDesc", "ItCode", "ItName", 
                        
                        //6-10
                        "Qty", "PurchaseUomCode", "UsageDt", "MinStock", "ROP", 
                        
                        //11-15
                        "InStock", "OutstandingMaterialRequest", "OutstandingPO", "Mth01", "Mth03",
                        
                        //16-20
                        "Mth06", "Mth09", "Mth12", "Remark", "ItCodeInternal", 
                        
                        //21
                        "Specification"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[2]);
                        TxtReqType.EditValue = Sm.DrStr(dr, c[3]);
                        TxtItCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        TxtPurchaseUomCode.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetDte(DteUsageDt, Sm.DrStr(dr, c[8]));
                        TxtMinStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtReorderPoint.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtInStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        TxtOutstanding.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        TxtOrdered.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                        if (mIsUseItemConsumption)
                        {
                            TxtMth01.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                            TxtMth03.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                            TxtMth06.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                            TxtMth09.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                            TxtMth12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        }
                        MeeRemark.EditValue = Sm.DrStr(dr, c[19]);
                        TxtItCodeInternal.EditValue = Sm.DrStr(dr, c[20]);
                        TxtSpecification.EditValue = Sm.DrStr(dr, c[21]);
                    }, false
                );
        }

        private void ShowDocApproval()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='MaterialRequest' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DNo=@DNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", mDNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        private void ShowPicture()
        {
            if (TxtItCode.Text.Length != 0)
            {
                try
                {
                    string DBServer = Sm.GetParameter("DBServer");
                    string location = Sm.GetParameter("ImgFileItem");
                    if (DBServer.Length != 0 && location.Length != 0 && Sm.CompareStr(DBServer, Gv.Server))
                    {
                        if (Directory.Exists(location))
                        {
                            string ImageFile = String.Concat(location, "\\", TxtItCode.Text);
                            foreach (string fe in new string[11] 
                                { ".jpg", ".png", ".bmp", ".jpeg", ".gif", ".dib", 
                                  ".rle", ".jpe", ".jfif", ".tiff", ".tif" })
                            {
                                if (File.Exists(String.Concat(ImageFile, fe)))
                                {
                                    PicItem.Image = Image.FromFile(String.Concat(ImageFile, fe));
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception Exc)
                {
                    Sm.StdMsg(mMsgType.Warning, Exc.ToString());
                }
            }
        }

        #endregion

        #region Event
        private void TxtInStock_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtInStock, 0); 
        }

        private void TxtQty_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtQty, 0);
        }

        private void TxtOutstanding_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtOutstanding, 0);
        }

        private void TxtOrdered_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtOrdered, 0);
        }

        private void TxtMinStock_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMinStock, 0);
        }

        private void TxtReorderPoint_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtReorderPoint, 0);
        }

        private void TxtMth01_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMth01, 0);
        }

        private void TxtMth03_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMth03, 0);
        }

        private void TxtMth06_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMth06, 0);
        }

        private void TxtMth09_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMth09, 0);
        }

        private void TxtMth12_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtMth12, 0);
        }

        #endregion

    }
}
