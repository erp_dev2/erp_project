﻿#region Update
/*
    03/05/2017 [TKG] tambah Employment Period Allowance untuk KMI
    24/10/2017 [ARI] payslip HIN
    21/12/2017 [TKG] tambah informasi kekurangan pajak akhir tahun
    14/02/2018 [ARI] printout payslip TWC
    27/02/2018 [ARI] tambah THP pada printout payslip HIN
    26/03/2018 [TKG] tambah salary adjustment, credit 5-10
    11/04/2018 [ARI] tambah printout KIM
    27/04/2018 [WED] rombak print out TWC
    30/04/2018 [WED] periode di payslip TWC ngikutin periode yang di email payslip (pakai periode bulan depannya)
    11/05/2018 [WED] print out KIM
    14/05/2018 [TKG] untuk ho brutto ditambah sci (HIN)
    15/05/2018 [ARI] rombak printout TWC
    05/06/2018 [ARI] feedback printout KIM
    10/07/2018 [TKG] ubah rumus brutto
    05/09/2018 [HAR] feedback pensiun ambil dari SS employee pensun
    21/09/2018 [HAR] payslip kim ambil en date untuk periodenya
    01/10/2018 [HAR] payslip AWG
    27/11/2018 [HAR] payslip SCU
    18/12/2018 [HAR] SCU:sub total dihilangkan huruf kapital semua
    16/01/2019 [MEY] Payslip VIR
    14/02/2019 [MEY] Payslip DGI
 *  22/02/2019 [HAR] BUG payslip KIM advance payment tidak muncul
 *  27/02/2019 [HAR] feedback label advancepayment (tambah class baru dan detail baru untuk advancepayment)
    01/03/2019 [DITA] penambahan informasi pada payslip desktop (vir)
    27/03/2019 [DITA] penambahan informasi OT pada payslip desktop (vir)
    10/04/2019 [DITA] Rombak Payslip VIR
    11/04/2019 [DITA] BUG Payslip VIR (perhitungan upah, jumlah, dan tunj.pribadi & tunj.perusahaan)
    12/04/2019 [DITA] Feedback Payslip VIR (perhitungan subtotal dan brutto), penambahan potongan tetap
    07/05/2019 [DITA] tambah informasi outstanding OT amount di payslip
    14/05/2019 [HAR] SCU tambah print out annual leave 
    01/07/2019 [DITA] KIM tambah informasi dari remark loan request 
    26/09/2019 [HAR/SRN] payslip sarinah
 *  02/10/2019 [HAR/SRN] feedback payslip srn : terbilang pakai koma, document payslip pakai format
 *  22/10/2019 [RF/MMM] Payslip MMM
 *  18/11/2019 [HAR/SRN] feedback pasyslip srn : merubah urutan, menampilkan 
 *  06/12/2019 [HAR/SRN] bug pasyslip periode masih ambil dari bulan depannya
 *  12/12/2019 [HAR/SRN] Feedback tanda tangan dimunculkan
 *  23/12/2019 [RF/MMM] Feedback menambahkan allowance dan deduction
    29/01/2020 [RF] OT Fleksibel
 */

#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPaySlipParticular1 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mDeptCode = string.Empty;
        private string 
            mIsFormPrintOutPayslip = string.Empty,
            mCompany = string.Empty,
            mAddress = string.Empty,
            mPhone = string.Empty,
            mUserName = string.Empty,
            mEmpSystemTypeBorongan = string.Empty,
            mEmpCodeSincerely = string.Empty;
        private bool mIsNotFilterByAuthorization = false;
      
        #endregion

        #region Constructor

        public FrmPaySlipParticular1(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Pay Slip";
                GetParameter();
                mUserName = Sm.GetValue("Select UserName From TblUser Where UserCode=@Param;", Gv.CurrentUserCode);
                SetGrd();
                BtnPayrunCode_Click(sender, e);
                BtnSave.Enabled = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mEmpSystemTypeBorongan = Sm.GetParameter("EmpSystemTypeBorongan");
            mIsFormPrintOutPayslip = Sm.GetParameter("FormPrintOutPayslip");
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mCompany = Sm.GetParameter("ReportTitle1");
            mAddress = Sm.GetParameter("ReportTitle2");
            mPhone = Sm.GetParameter("ReportTitle4");
            mEmpCodeSincerely = Sm.GetParameter("EmpCodeSincerely");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",
                        
                        
                        //6-7
                        "Join Date",
                        "Resign Date"
                    },
                     new int[] 
                    {
                        //0
                        50, 
                        
                        //1-5
                        100, 250, 100, 150, 150, 
                        
                        //6-7
                        100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtPayrunCode, TxtPayrunPeriod, TxtPayrunName, TxtDeptName, TxtSiteName, 
                  DteStartDt, DteEndDt, LueAGCode 
            });
            Sm.ClearGrd(Grd1, true);
        }

        private void ShowPayrunInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, A.JoinDt, A.ResignDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            if (Sm.GetLue(LueAGCode).Length > 0 &&
                !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                SQL.AppendLine("Inner Join TblAttendanceGrpDtl F On A.EmpCode=F.EmpCode And F.AGCode=@AGCode ");
            SQL.AppendLine("Where A.EmpCode In ( ");
            SQL.AppendLine("    Select EmpCode From TblPayrollProcess1 ");
            SQL.AppendLine("    Where PayrunCode=@PayrunCode ");
            SQL.AppendLine("    And VoucherRequestPayrollDocNo Is Not Null ");
            SQL.AppendLine(") ");
            if (
                Sm.GetLue(LueAGCode).Length > 0 &&
                Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup")
                )
            {
                SQL.AppendLine("And ( ");
                SQL.AppendLine("    A.EmpCode Not In ( ");
                SQL.AppendLine("        Select B.EmpCode ");
                SQL.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                SQL.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                SQL.AppendLine("        ) ");
                if (mDeptCode.Length != 0)
                {
                    SQL.AppendLine("Or ");
                    SQL.AppendLine("    A.EmpCode In ( ");
                    SQL.AppendLine("        Select B.EmpCode ");
                    SQL.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                    SQL.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                    SQL.AppendLine("        And IfNull(A.DeptCode, 'XXX')<>@DeptCode ");
                    SQL.AppendLine("        ) ");
                }
                SQL.AppendLine(") ");
                
            }
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("Order By A.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
            if (Sm.GetLue(LueAGCode).Length > 0)
                Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            if (mDeptCode.Length != 0)
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);

            int i = 0;
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "JoinDt", 
                    
                    //6
                    "ResignDt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = ++i;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                }, true, false, false, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (TxtPayrunCode.Text.Length > 0) ParPrint(TxtPayrunCode.Text);
        }

        #endregion

        #region Additional Method

        private void ParPrint(string Payrun)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string PayslipPrintBy = 
                "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", 
                Sm.ConvertDateTime(Sm.ServerCurrentDateTime()));
            
            #region KMI

            if (Doctitle == "KMI")
            {
                var l = new List<PaySlip>();
                string[] TableName = { "PaySlip" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header untuk KMI

                var SQL = new StringBuilder();

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, Z.CompanyName As Company, Z.CompanyPhone As Phone, Z.CompanyAddress As Address,  ");
                }
                else
                {
                    SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                }
                SQL.AppendLine("A.EmpCode, C.EmpName, C.DisplayName, A.Salary, A.WorkingDay, D.DeptName, E.OptDesc As Type, A.OT1Amt, A.OT2Amt, ");
                SQL.AppendLine("DATE_FORMAT(B.StartDt,'%d/%m/%y')As StartDt, DATE_FORMAT(B.EndDt,'%d/%m/%y')As EndDt, A.OTHolidayAmt, A.HolidayEarning, ");
                SQL.AppendLine("A.PresenceReward, A.IncMinWages, A.IncPerformance, A.ExtraFooding, A.SSEmployerHealth, A.SSEmployerEmployment, A.PLAmt, A.DedProduction, ");
                SQL.AppendLine("A.SSEmployeeHealth, A.SSEmployeeEmployment, A.DedEmployee, A.Tax, A.SalaryAdjustment, A.Amt, A.IncEmployee, A.NPWP, ");
                SQL.AppendLine("F.GrdLvlName, G.PosName, DATE_FORMAT(A.JoinDt,'%d-%M-%Y') As DOH, A.PTKP, DATE_FORMAT(B.StartDt,'%M-%y')As Periode, H.SiteName, ");
                SQL.AppendLine("C.BankAcNo, C.BankAcName, A.OT1Hr, A.OT2Hr, A.OT1Hr + A.OT2Hr As TotalOTHr, A.OT1Amt + A.OT2Amt As TotalOTAmt, A.Transport, A.Meal, ");

                SQL.AppendLine(" Date_Format(A1.Dt,'%y%m%d')As Dt,");
                SQL.AppendLine(" Case When A1.WSIn1 Is Not Null Then Concat(Left(A1.WSIn1, 2), ':', Right(A1.WSIn1, 2)) Else Null End As WSIn, ");
                SQL.AppendLine(" Case When A1.WSOut1 Is Not Null Then Concat(Left(A1.WSOut1, 2), ':', Right(A1.WSOut1, 2)) Else Null End As WSOut, ");
                SQL.AppendLine(" Case When A1.HolInd='Y' Or A1.WSHolidayInd='Y' Then C1.WSCode Else 'H' End As Status, ");
                SQL.AppendLine(" Case When A1.WSIn3 Is Not Null Then Concat(Left(A1.WSIn3, 2), ':', Right(A1.WSIn3, 2)) Else Null End As OTRegulerIn, ");
                SQL.AppendLine(" Case When A1.WSOut3 Is Not Null Then Concat(Left(A1.WSOut3, 2), ':', Right(A1.WSOut3, 2)) Else Null End As OTRegulerOut, ");
                SQL.AppendLine(" Case When  B1.OTStartTm Is Not Null Then Concat(Left( B1.OTStartTm, 2), ':', Right( B1.OTStartTm, 2)) Else Null End As OTExtraDayIn, ");
                SQL.AppendLine(" Case When  B1.OTEndTm Is Not Null Then Concat(Left(B1.OTEndTm, 2), ':', Right(B1.OTEndTm, 2)) Else Null End As OTExtraDayOut, ");
                SQL.AppendLine(" ifnull(Concat(A1.Dt, B1.DoCno), A1.Dt) As KeyGroup, A.EmpAdvancePayment, A.FieldAssignment, A.EmploymentPeriodAllowance ");

                SQL.AppendLine(" From tblpayrollprocess1 A ");
                SQL.AppendLine(" Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL.AppendLine(" Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine(" Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL.AppendLine(" Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL.AppendLine(" Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL.AppendLine(" Left Join tblsite H On C.SiteCode=H.SiteCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");
                SQL.AppendLine(" Inner Join TblPayrollProcess2 A1 On A.EmpCode=A1.EmpCode And A1.PayrunCode=A.PayrunCode");
                SQL.AppendLine(" Left Join TblOTRequestDtl B1");
                SQL.AppendLine(" On A1.EmpCode=B1.EmpCode ");
                SQL.AppendLine(" And Exists( ");
                SQL.AppendLine("    Select T.DocNo ");
                SQL.AppendLine("    From TblOTRequestHdr T ");
                SQL.AppendLine("    Where T.OTDt=A1.Dt ");
                SQL.AppendLine("    And T.Status='A' ");
                SQL.AppendLine("    And T.CancelInd='N' ");
                SQL.AppendLine("    And T.DocNo=B1.DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(" Left Join TblWorkSchedule C1 On A1.WSCode=C1.WSCode ");

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select distinct A.PayrunCode, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                    SQL.AppendLine("    From Tblpayrun A  ");
                    SQL.AppendLine("    Inner Join TblSite B On A.SiteCode=B.SiteCode  ");
                    SQL.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode=C.ProfitCenterCode  ");
                    SQL.AppendLine("    Inner Join TblEntity D On C.EntCode=D.EntCode  ");
                    SQL.AppendLine("    Where A.PayrunCode=@DocNo ");
                    SQL.AppendLine(") Z On A.PayrunCode=Z.PayrunCode ");
                }
              

                SQL.AppendLine(" Where A.PayrunCode=@PayrunCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("And A.EmpCode Not In (Select B.EmpCode From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B Where A.AGCode=B.AGCode And A.ActInd='Y') ");
            
                SQL.AppendLine(" Order By A1.Payruncode, C.EmpName, A1.Dt");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);

                    if (TxtSiteName.Text.Length > 0)
                    {
                        string CompanyLogo = Sm.GetValue(
                           "Select D.EntLogoName " +
                           "From TblPayrun A  " +
                           "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                           "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                           "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                           "Where A.PayrunCode ='" + TxtPayrunCode.Text + "' "
                       );
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }
                    if (Sm.GetLue(LueAGCode).Length > 0)
                        Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company",
                    "Address",
                    "Phone",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "DisplayName",
                    "Salary",
                    "WorkingDay",
                    "DeptName",
                    "Type",

                    //11-15
                    "OT1Amt",
                    "OT2Amt",
                    "StartDt",
                    "EndDt",
                    "OTHolidayAmt",

                    //16-20
                    "HolidayEarning",
                    "PresenceReward",
                    "IncMinWages",
                    "IncPerformance",
                    "ExtraFooding",

                    //21-25
                    "SSEmployerHealth",
                    "SSEmployerEmployment",
                    "PLAmt",
                    "DedProduction",
                    "SSEmployeeHealth",

                    //26-30
                    "SSEmployeeEmployment",
                    "DedEmployee",
                    "Tax",
                    "SalaryAdjustment",
                    "Amt",

                    //31-35
                    "IncEmployee",
                    "NPWP",
                    "GrdLvlName",
                    "PosName",
                    "DOH", 

                    //36-40
                    "PTKP",
                    "Periode",
                    "SiteName",
                    "BankAcNo",
                    "BankAcName",

                    //41-45
                    "OT1Hr",
                    "OT2Hr",
                    "TotalOTHr",
                    "TotalOTAmt",
                    "Transport",

                    //46-50
                    "Meal",
                    "Dt",
                    "WSIn",
                    "WSOut",
                    "Status",

                    //51-55
                    "OTRegulerIn",
                    "OTRegulerOut",
                    "OTExtraDayIn",
                    "OTExtraDayOut",
                    "KeyGroup",

                    //56-58
                    "EmpAdvancePayment",
                    "FieldAssignment",
                    "EmploymentPeriodAllowance"

                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PaySlip()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                EmCode = Sm.DrStr(dr, c[4]),
                                EmpName = Sm.DrStr(dr, c[5]),

                                DisplayName = Sm.DrStr(dr, c[6]),
                                Salary = Sm.DrDec(dr, c[7]),
                                WorkingDay = Sm.DrDec(dr, c[8]),
                                DeptName = Sm.DrStr(dr, c[9]),
                                Type = Sm.DrStr(dr, c[10]),

                                OT1Amt = Sm.DrDec(dr, c[11]),
                                OT2Amt = Sm.DrDec(dr, c[12]),
                                StartDt = Sm.DrStr(dr, c[13]),
                                EndDt = Sm.DrStr(dr, c[14]),
                                OTHolidayAmt = Sm.DrDec(dr, c[15]),

                                HolidayEarning = Sm.DrDec(dr, c[16]),
                                PresenceReward = Sm.DrDec(dr, c[17]),
                                IncMinWages = Sm.DrDec(dr, c[18]),
                                IncPerformance = Sm.DrDec(dr, c[19]),
                                ExtraFooding = Sm.DrDec(dr, c[20]),

                                SSEmployerHealth = Sm.DrDec(dr, c[21]),
                                SSEmployerEmployment = Sm.DrDec(dr, c[22]),
                                PLAmt = Sm.DrDec(dr, c[23]),
                                DedProduction = Sm.DrDec(dr, c[24]),
                                SSEmployeeHealth = Sm.DrDec(dr, c[25]),

                                SSEmployeeEmployment = Sm.DrDec(dr, c[26]),
                                DedEmployee = Sm.DrDec(dr, c[27]),
                                Tax = Sm.DrDec(dr, c[28]),
                                SalaryAdjustment = Sm.DrDec(dr, c[29]),
                                Amt = Sm.DrDec(dr, c[30]),

                                IncEmployee = Sm.DrDec(dr, c[31]),
                                NPWP = Sm.DrStr(dr, c[32]),
                                GrdLvlName = Sm.DrStr(dr, c[33]),
                                PosName = Sm.DrStr(dr, c[34]),
                                DOH = Sm.DrStr(dr, c[35]),

                                PTKP = Sm.DrStr(dr, c[36]),
                                Periode = Sm.DrStr(dr, c[37]),
                                SiteName = Sm.DrStr(dr, c[38]),
                                BankAcNo = Sm.DrStr(dr, c[39]),
                                BankAcName = Sm.DrStr(dr, c[40]),

                                OT1Hr = Sm.DrDec(dr, c[41]),
                                OT2Hr = Sm.DrDec(dr, c[42]),
                                TotalOTHr = Sm.DrDec(dr, c[43]),
                                TotalOTAmt = Sm.DrDec(dr, c[44]),
                                Transport = Sm.DrDec(dr, c[45]),

                                Meal = Sm.DrDec(dr, c[46]),
                                Dt = Sm.DrStr(dr, c[47]),
                                WsIn = Sm.DrStr(dr, c[48]),
                                WsOut = Sm.DrStr(dr, c[49]),
                                Status = Sm.DrStr(dr, c[50]),

                                OTRegulerIn = Sm.DrStr(dr, c[51]),
                                OTRegulerOut = Sm.DrStr(dr, c[52]),
                                OTExtraDayIn = Sm.DrStr(dr, c[53]),
                                OTExtraDayOut = Sm.DrStr(dr, c[54]),
                                KeyGroup = Sm.DrStr(dr, c[55]),

                                EmpAdvancePayment = Sm.DrDec(dr, c[56]),
                                FieldAssignment = Sm.DrDec(dr, c[57]),
                                EmploymentPeriodAllowance = Sm.DrDec(dr, c[58]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")


                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }

            #endregion

            #region IOK

            if (Doctitle == "IOK")
            {
                var l2 = new List<PaySlip2>();
                string[] TableName = { "PaySlip2" };
                List<IList> myLists = new List<IList>();
                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();

                SQL2.AppendLine("Select A.PayrunCode, @CompanyLogo As CompanyLogo, ");
                SQL2.AppendLine("@Company As Company, @Address As Address, @Phone As Phone, ");
                SQL2.AppendLine("C.EmpCodeOld As EmpCode, C.EmpName, C.DisplayName, ");
                SQL2.AppendLine("A.WorkingDay, D.DeptName, E.OptDesc As Type, A.OT1Amt, A.OT2Amt,");
                SQL2.AppendLine("DATE_FORMAT(B.StartDt,'%d/%m/%y')As StartDt, ");
                SQL2.AppendLine("DATE_FORMAT(B.EndDt,'%d/%m/%y')As EndDt, ");
                SQL2.AppendLine("A.OTHolidayAmt, A.HolidayEarning, ");
                SQL2.AppendLine("A.PresenceReward, A.IncMinWages, (A.FixAllowance+A.IncPerformance-IfNull(M.Amt, 0.00)) As IncPerformance, ");
                SQL2.AppendLine("A.ExtraFooding, A.SSEmployerHealth, A.SSEmployerEmployment, A.PLAmt, A.DedProduction, ");
                SQL2.AppendLine("A.SSEmployeeHealth, A.SSEmployeeEmployment, A.DedEmployee, IfNull(A.EOYTax, 0.00)+A.Tax As Tax, A.SalaryAdjustment, ");
                SQL2.AppendLine("A.Amt, IfNull(A.IncEmployee,0) As IncEmployee, A.NPWP, ");
                SQL2.AppendLine("F.GrdLvlName, G.PosName, DATE_FORMAT(A.JoinDt,'%d-%M-%Y') As DOH, A.PTKP, ");
                SQL2.AppendLine("DATE_FORMAT(B.StartDt,'%M-%y')As Periode, H.SiteName, ");
                SQL2.AppendLine("C.BankAcNo, C.BankAcName, A.OT1Hr, A.OT2Hr, (A.OT1Hr + A.OT2Hr) As TotalOTHr, ");
                SQL2.AppendLine("(A.OT1Amt + A.OT2Amt) As TotalOTAmt, A.Transport, A.Meal, A.OTHolidayHr, ");
                SQL2.AppendLine("A.WorkingDay, A.EmpAdvancePayment, J.SaldoAmtAP, ");
                SQL2.AppendLine("IfNull(K.OTHoliday, 0.00) As OTHoliday, IfNull(Z.OTHoliday, 0.00) As OTHolidayBorong, A.Salary As Salary, ");
                SQL2.AppendLine("IfNull(L.IncPerformanceWorkingDay, 0) As IncPerformanceWorkingDay, ");
                SQL2.AppendLine("(IfNull(IncEmployee, 0.00)+IfNull(incproduction, 0.00)) As Incentif, ");
                SQL2.AppendLine("IfNull(M.Amt, 0.00) As ADFixedMeal ");
                SQL2.AppendLine("From TblPayrollProcess1 A ");
                SQL2.AppendLine("Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL2.AppendLine("Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL2.AppendLine("Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL2.AppendLine("Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL2.AppendLine("Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL2.AppendLine("Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL2.AppendLine("Left Join TblSite H On C.SiteCode=H.SiteCode ");

                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL2.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");

                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("   Select T1.EmpCode, (IfNull(T1.Amt, 0)-IfNull(T2.AmtAP, 0)) SaldoAmtAP ");
                SQL2.AppendLine("   From TblAdvancePaymentHdr T1 ");
                SQL2.AppendLine("   Left Join ");
                SQL2.AppendLine("   ( ");
	            SQL2.AppendLine("       Select DocNo, EmpCode, Sum(Amt) As AmtAP ");
                SQL2.AppendLine("       From tbladvancepaymentprocess ");
	            SQL2.AppendLine("       Where DocNo = @AdvancePaymentDocNo ");
	            SQL2.AppendLine("       Group by DocNo, EmpCode ");
                SQL2.AppendLine("   ) T2 On T1.DocNo = T2.DocNo ");
                SQL2.AppendLine("   Where T1.DocNo = @AdvancePaymentDocNo ");
                SQL2.AppendLine(") J On A.EmpCode=J.EmpCode ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("   Select T.EmpCode, ");
                SQL2.AppendLine("   Count(T.OTHolidayHr) As OTHoliday ");
                SQL2.AppendLine("   From TblPayrollProcess2 T ");
                SQL2.AppendLine("   Where T.PayrunCode= @PayrunCode ");
                SQL2.AppendLine("   And T.ProcessInd='Y' ");
                if (!IsEmpSystemTypeBorongan())
                     SQL2.AppendLine("   And T.OTHolidayHr>0 ");
                SQL2.AppendLine("   And (T.WSHolidayInd='Y' Or T.HolInd='Y') ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                {
                    SQL2.AppendLine("And ( ");
                    SQL2.AppendLine("    T.EmpCode Not In ( ");
                    SQL2.AppendLine("        Select B.EmpCode ");
                    SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                    SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                    SQL2.AppendLine("        ) ");
                    if (mDeptCode.Length != 0)
                    {
                        SQL2.AppendLine("Or ");
                        SQL2.AppendLine("    T.EmpCode In ( ");
                        SQL2.AppendLine("        Select B.EmpCode ");
                        SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                        SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                        SQL2.AppendLine("        And IfNull(A.DeptCode, 'XXX')<>@DeptCode ");
                        SQL2.AppendLine("        ) ");
                    }
                    SQL2.AppendLine(") ");
                }

                if (!mIsNotFilterByAuthorization)
                {
                    SQL2.AppendLine("And Exists( ");
                    SQL2.AppendLine("    Select 1 From TblEmployee ");
                    SQL2.AppendLine("    Where EmpCode=T.EmpCode ");
                    SQL2.AppendLine("    And GrdLvlCode In ( ");
                    SQL2.AppendLine("        Select T2.GrdLvlCode ");
                    SQL2.AppendLine("        From TblPPAHdr T1 ");
                    SQL2.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL2.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                    SQL2.AppendLine("    ) ");
                    SQL2.AppendLine(") ");
                }

                SQL2.AppendLine("   Group By T.EmpCode ");
                SQL2.AppendLine(") K On A.EmpCode=K.EmpCode ");


                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("   Select T.EmpCode, ");
                SQL2.AppendLine("   Count(T.ProductionWages) As IncPerformanceWorkingDay ");
                SQL2.AppendLine("   From TblPayrollProcess2 T ");
                SQL2.AppendLine("   Where T.PayrunCode= @PayrunCode ");
                SQL2.AppendLine("   And T.ProcessInd='Y' ");
                SQL2.AppendLine("   And T.IncPerformance>0 ");
                
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                {
                    SQL2.AppendLine("And ( ");
                    SQL2.AppendLine("    T.EmpCode Not In ( ");
                    SQL2.AppendLine("        Select B.EmpCode ");
                    SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                    SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                    SQL2.AppendLine("        ) ");
                    if (mDeptCode.Length != 0)
                    {
                        SQL2.AppendLine("Or ");
                        SQL2.AppendLine("    T.EmpCode In ( ");
                        SQL2.AppendLine("        Select B.EmpCode ");
                        SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                        SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                        SQL2.AppendLine("        And IfNull(A.DeptCode, 'XXX')<>@DeptCode ");
                        SQL2.AppendLine("        ) ");
                    }
                    SQL2.AppendLine(") ");
                }

                if (!mIsNotFilterByAuthorization)
                {
                    SQL2.AppendLine("And Exists( ");
                    SQL2.AppendLine("    Select 1 From TblEmployee ");
                    SQL2.AppendLine("    Where EmpCode=T.EmpCode ");
                    SQL2.AppendLine("    And GrdLvlCode In ( ");
                    SQL2.AppendLine("        Select T2.GrdLvlCode ");
                    SQL2.AppendLine("        From TblPPAHdr T1 ");
                    SQL2.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL2.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                    SQL2.AppendLine("    ) ");
                    SQL2.AppendLine(") ");
                }

                SQL2.AppendLine("   Group By T.EmpCode ");
                SQL2.AppendLine(") L On A.EmpCode=L.EmpCode ");

                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("   Select T2.EmpCode, Sum(T2.Amt) Amt ");
                SQL2.AppendLine("   From TblPayrun T1 ");
                SQL2.AppendLine("   Inner Join TblEmployeeAllowanceDeduction T2 ");
                SQL2.AppendLine("   On ( ");
                SQL2.AppendLine("       (T2.StartDt Is Null And T2.EndDt Is Null) Or ");
                SQL2.AppendLine("       (T2.StartDt Is Not Null And T2.EndDt Is Null And T2.StartDt<=T1.EndDt) Or ");
                SQL2.AppendLine("       (T2.StartDt Is Null And T2.EndDt Is Not Null And T1.EndDt<=T2.EndDt) Or ");
                SQL2.AppendLine("       (T2.StartDt Is Not Null And T2.EndDt Is Not Null And T2.StartDt<=T1.EndDt And T1.EndDt<=T2.EndDt) ");
                SQL2.AppendLine("       ) ");
                SQL2.AppendLine("   And T2.EmpCode In (Select EmpCode From TblpayrollProcess1 Where PayrunCode=@PayrunCode) ");
                SQL2.AppendLine("   And Find_In_Set( ");
                SQL2.AppendLine("        IfNull(T2.ADCode, ''), ");
                SQL2.AppendLine("       (Select ParValue From TblParameter Where ParCode Is Not Null And ParCode='ADCodeFixedMealAllowance') ");
                SQL2.AppendLine("   ) ");
                SQL2.AppendLine("   Where T1.PayrunCode=@PayrunCode ");
                SQL2.AppendLine("   Group By T2.EmpCode ");
                SQL2.AppendLine(") M On A.EmpCode=M.EmpCode ");

                #region tambah borongan

                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("   Select T.EmpCode, ");
                SQL2.AppendLine("   Sum(T.ProductionWages) As OTHoliday ");
                SQL2.AppendLine("   From TblPayrollProcess2 T ");
                SQL2.AppendLine("   Where T.PayrunCode= @PayrunCode ");
                SQL2.AppendLine("   And T.ProcessInd='Y' ");
                if (IsEmpSystemTypeBorongan())
                    SQL2.AppendLine("   And T.ProductionWages>0 ");
                SQL2.AppendLine("   And (T.WSHolidayInd='Y' Or T.HolInd='Y') ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                {
                    SQL2.AppendLine("And ( ");
                    SQL2.AppendLine("    T.EmpCode Not In ( ");
                    SQL2.AppendLine("        Select B.EmpCode ");
                    SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                    SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                    SQL2.AppendLine("        ) ");
                    if (mDeptCode.Length != 0)
                    {
                        SQL2.AppendLine("Or ");
                        SQL2.AppendLine("    T.EmpCode In ( ");
                        SQL2.AppendLine("        Select B.EmpCode ");
                        SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                        SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                        SQL2.AppendLine("        And IfNull(A.DeptCode, 'XXX')<>@DeptCode ");
                        SQL2.AppendLine("        ) ");
                    }
                    SQL2.AppendLine(") ");
                }

                if (!mIsNotFilterByAuthorization)
                {
                    SQL2.AppendLine("And Exists( ");
                    SQL2.AppendLine("    Select EmpCode From TblEmployee ");
                    SQL2.AppendLine("    Where EmpCode=T.EmpCode ");
                    SQL2.AppendLine("    And GrdLvlCode In ( ");
                    SQL2.AppendLine("        Select T2.GrdLvlCode ");
                    SQL2.AppendLine("        From TblPPAHdr T1 ");
                    SQL2.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL2.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                    SQL2.AppendLine("    ) ");
                    SQL2.AppendLine(") ");
                }

                SQL2.AppendLine("   Group By T.EmpCode ");
                SQL2.AppendLine(") Z On A.EmpCode=Z.EmpCode ");

                #endregion 


                SQL2.AppendLine(" Where A.PayrunCode=@PayrunCode ");

                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                {
                    SQL2.AppendLine("And ( ");
                    SQL2.AppendLine("    A.EmpCode Not In ( ");
                    SQL2.AppendLine("        Select B.EmpCode ");
                    SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                    SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                    SQL2.AppendLine("        ) ");
                    if (mDeptCode.Length != 0)
                    {
                        SQL2.AppendLine("Or ");
                        SQL2.AppendLine("    A.EmpCode In ( ");
                        SQL2.AppendLine("        Select B.EmpCode ");
                        SQL2.AppendLine("        From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B ");
                        SQL2.AppendLine("        Where A.AGCode=B.AGCode And A.ActInd='Y' ");
                        SQL2.AppendLine("        And IfNull(A.DeptCode, 'XXX')<>@DeptCode ");
                        SQL2.AppendLine("        ) ");
                    }
                    SQL2.AppendLine(") ");
                }

                if (!mIsNotFilterByAuthorization)
                {
                    SQL2.AppendLine("And Exists( ");
                    SQL2.AppendLine("    Select EmpCode From TblEmployee ");
                    SQL2.AppendLine("    Where EmpCode=A.EmpCode ");
                    SQL2.AppendLine("    And GrdLvlCode In ( ");
                    SQL2.AppendLine("        Select T2.GrdLvlCode ");
                    SQL2.AppendLine("        From TblPPAHdr T1 ");
                    SQL2.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                    SQL2.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                    SQL2.AppendLine("    ) ");
                    SQL2.AppendLine(") ");
                }
                SQL2.AppendLine(" Order By C.EmpName ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@AdvancePaymentDocNo", Sm.GetValue("Select DocNo from TblAdvancePaymentProcess Where payrunCode='"+Payrun+"' "));
                    Sm.CmParam<String>(ref cm2, "@PayrunCode", Payrun);
                    Sm.CmParam<String>(ref cm2, "@UserCode", Gv.CurrentUserCode);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                    Sm.CmParam<String>(ref cm2, "@Company", mCompany);
                    Sm.CmParam<String>(ref cm2, "@Address", mAddress);
                    Sm.CmParam<String>(ref cm2, "@Phone", mPhone);
                    
                    if (Sm.GetLue(LueAGCode).Length > 0)
                        Sm.CmParam<String>(ref cm2, "@AGCode", Sm.GetLue(LueAGCode));
                    if (mDeptCode.Length != 0)
                        Sm.CmParam<String>(ref cm2, "@DeptCode", mDeptCode);

            

                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[]
                {
                //0
                "CompanyLogo",

                //1-5
                "Company",
                "Address",
                "Phone",
                "EmpCode",
                "EmpName",

                //6-10
                "DisplayName",
                "Salary",
                "WorkingDay",
                "DeptName",
                "Type",

                //11-15
                "OT1Amt",
                "OT2Amt",
                "StartDt",
                "EndDt",
                "OTHolidayAmt",

                //16-20
                "HolidayEarning",
                "PresenceReward",
                "IncMinWages",
                "IncPerformance",
                "ExtraFooding",

                //21-25
                "SSEmployerHealth",
                "SSEmployerEmployment",
                "PLAmt",
                "DedProduction",
                "SSEmployeeHealth",

                //26-30
                "SSEmployeeEmployment",
                "DedEmployee",
                "Tax",
                "SalaryAdjustment",
                "Amt",

                //31-35
                "IncEmployee",
                "NPWP",
                "GrdLvlName",
                "PosName",
                "DOH", 

                //36-40
                "PTKP",
                "Periode",
                "SiteName",
                "BankAcNo",
                "BankAcName",

                //41-45
                "OT1Hr",
                "OT2Hr",
                "TotalOTHr",
                "TotalOTAmt",
                "Transport",

                //46-50
                "Meal",
                "OTHolidayHr",
                "WorkingDay",
                "EmpAdvancePayment",
                "SaldoAmtAP",

                //51-55
                "OTHoliday",
                "IncPerformanceWorkingDay",
                "Incentif",
                "OTHolidayBorong",
                "ADFixedMeal"
                });

                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new PaySlip2()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),
                                Company = Sm.DrStr(dr2, c2[1]),
                                Address = Sm.DrStr(dr2, c2[2]),
                                Phone = Sm.DrStr(dr2, c2[3]),
                                EmCode = Sm.DrStr(dr2, c2[4]),
                                EmpName = Sm.DrStr(dr2, c2[5]),
                                DisplayName = Sm.DrStr(dr2, c2[6]),
                                Salary = Sm.DrDec(dr2, c2[7]),
                                WorkingDay = Sm.DrDec(dr2, c2[8]),
                                DeptName = Sm.DrStr(dr2, c2[9]),
                                Type = Sm.DrStr(dr2, c2[10]),
                                OT1Amt = Sm.DrDec(dr2, c2[11]),
                                OT2Amt = Sm.DrDec(dr2, c2[12]),
                                StartDt = Sm.DrStr(dr2, c2[13]),
                                EndDt = Sm.DrStr(dr2, c2[14]),
                                OTHolidayAmt = Sm.DrDec(dr2, c2[15]),
                                HolidayEarning = Sm.DrDec(dr2, c2[16]),
                                PresenceReward = Sm.DrDec(dr2, c2[17]),
                                IncMinWages = Sm.DrDec(dr2, c2[18]),
                                IncPerformance = Sm.DrDec(dr2, c2[19]),
                                ExtraFooding = Sm.DrDec(dr2, c2[20]),
                                SSEmployerHealth = Sm.DrDec(dr2, c2[21]),
                                SSEmployerEmployment = Sm.DrDec(dr2, c2[22]),
                                PLAmt = Sm.DrDec(dr2, c2[23]),
                                DedProduction = Sm.DrDec(dr2, c2[24]),
                                SSEmployeeHealth = Sm.DrDec(dr2, c2[25]),
                                SSEmployeeEmployment = Sm.DrDec(dr2, c2[26]),
                                DedEmployee = Sm.DrDec(dr2, c2[27]),
                                Tax = Sm.DrDec(dr2, c2[28]),
                                SalaryAdjustment = Sm.DrDec(dr2, c2[29]),
                                Amt = Sm.DrDec(dr2, c2[30]),
                                IncEmployee = Sm.DrDec(dr2, c2[31]),
                                NPWP = Sm.DrStr(dr2, c2[32]),
                                GrdLvlName = Sm.DrStr(dr2, c2[33]),
                                PosName = Sm.DrStr(dr2, c2[34]),
                                DOH = Sm.DrStr(dr2, c2[35]),
                                PTKP = Sm.DrStr(dr2, c2[36]),
                                Periode = Sm.DrStr(dr2, c2[37]),
                                SiteName = Sm.DrStr(dr2, c2[38]),
                                BankAcNo = Sm.DrStr(dr2, c2[39]),
                                BankAcName = Sm.DrStr(dr2, c2[40]),
                                OT1Hr = Sm.DrDec(dr2, c2[41]),
                                OT2Hr = Sm.DrDec(dr2, c2[42]),
                                TotalOTHr = Sm.DrDec(dr2, c2[43]),
                                TotalOTAmt = Sm.DrDec(dr2, c2[44]),
                                Transport = Sm.DrDec(dr2, c2[45]),
                                Meal = Sm.DrDec(dr2, c2[46]),
                                OTHolidayHr = Sm.DrDec(dr2, c2[47]),
                                EmpAdvancePayment = Sm.DrDec(dr2, c2[49]),
                                SaldoAmtAP = Sm.DrDec(dr2, c2[50]),
                                OTHoliday = Sm.DrDec(dr2, c2[51]),
                                IncPerformanceWorkingDay = Sm.DrDec(dr2, c2[52]),
                                Incentif = Sm.DrDec(dr2, c2[53]),
                                OTHolidayBorong = Sm.DrDec(dr2, c2[54]),
                                ADFixedMeal = Sm.DrDec(dr2, c2[55]),
                                PrintBy = PayslipPrintBy,
                                UserCode = mUserName
                            });
                        }
                    }
                    dr2.Close();
                }
                myLists.Add(l2);
                if (IsEmpSystemTypeBorongan())
                    //untuk borongan
                    Sm.PrintReport((string.Concat(mIsFormPrintOutPayslip, "2")), myLists, TableName, false);
                else
                    //untuk harian
                    Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion

            #region TWC
            if (Doctitle == "TWC")
            {
                var l = new List<PaySlipTWC>();
                var ldtl = new List<EmpAllowance>();
                var ldtl2 = new List<OTAllowance>();

                string[] TableName = { "PaySlipTWC", "EmpAllowance", "OTAllowance" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, Z.CompanyName As Company, Z.CompanyPhone As Phone, Z.CompanyAddress As Address,  ");
                }
                else
                {
                    SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                }
                SQL.AppendLine("A.EmpCode, C.EmpName, D.DeptName, H.SiteName, A.Salary, A.Functional, A.SSEmployerHealth,  A.SSEmployeeHealth, ");
                SQL.AppendLine("A.SSEmployerEmployment, A.SSEmployeeEmployment, A.Amt, DATE_FORMAT(DATE_ADD(Concat(Left(A.PayrunCode, 6), '01'), Interval 1 MONTH),'%M %Y')As Periode, A.SSEmployerPension, ");
                SQL.AppendLine("A.SSEmployeePension, A.Payruncode, A.PerformanceValue, IfNull(X.Amt,0)As AmtAll, IfNull(Y.Amt,0)As AmtDed, ");
                SQL.AppendLine("A.ProcessUPLAmt, A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.SalaryAdjustment, A.Transport, A.Meal, A.ADOT, A.FixAllowance, ");
                SQL.AppendLine("A.FixDeduction, A.EmpAdvancePayment, A.SSEePension, A.tax, A.taxallowance, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, O.TransportHr, P.MealHr ");

                SQL.AppendLine(" From tblpayrollprocess1 A ");
                SQL.AppendLine(" Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL.AppendLine(" Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine(" Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL.AppendLine(" Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL.AppendLine(" Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL.AppendLine(" Left Join tblsite H On C.SiteCode=H.SiteCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='A' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")X On A.PayrunCode=X.Payruncode And A.EmpCode=X.EmpCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='D' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")Y On A.PayrunCode=Y.Payruncode And A.EmpCode=Y.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
	            SQL.AppendLine("    Select PayrunCode, Empcode, count(transport)As TransportHr ");
	            SQL.AppendLine("    From TblPayrollProcess2 ");
	            SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Transport !=0 ");
	            SQL.AppendLine("    Group by Payruncode, EmpCode ");
	            SQL.AppendLine(" )O On A.PayrunCode=O.PayrunCode And A.EmpCode=O.EmpCode ");
                
                SQL.AppendLine(" Left Join ( ");
	            SQL.AppendLine("    Select PayrunCode, Empcode, count(Meal)As MealHr ");
	            SQL.AppendLine("    From TblPayrollProcess2 ");
	            SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Meal !=0 ");
	            SQL.AppendLine("    Group by Payruncode, EmpCode ");
                SQL.AppendLine(" )P On A.PayrunCode=P.PayrunCode And A.EmpCode=P.EmpCode ");

                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select distinct A.PayrunCode, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                    SQL.AppendLine("    From Tblpayrun A  ");
                    SQL.AppendLine("    Inner Join TblSite B On A.SiteCode=B.SiteCode  ");
                    SQL.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode=C.ProfitCenterCode  ");
                    SQL.AppendLine("    Inner Join TblEntity D On C.EntCode=D.EntCode  ");
                    SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                    SQL.AppendLine(") Z On A.PayrunCode=Z.PayrunCode ");
                }


                SQL.AppendLine(" Where A.PayrunCode=@PayrunCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("And A.EmpCode Not In (Select B.EmpCode From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B Where A.AGCode=B.AGCode And A.ActInd='Y') ");

                SQL.AppendLine(" Order By A.Payruncode, C.EmpName ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);

                    if (TxtSiteName.Text.Length > 0)
                    {
                        string CompanyLogo = Sm.GetValue(
                           "Select D.EntLogoName " +
                           "From TblPayrun A  " +
                           "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                           "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                           "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                           "Where A.PayrunCode ='" + TxtPayrunCode.Text + "' "
                       );
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }
                    if (Sm.GetLue(LueAGCode).Length > 0)
                        Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company", 
                    "Address",
                    "Phone",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "DeptName", 
                    "SiteName",
                    "Salary",
                    "Functional",
                    "SSEmployerHealth",

                    //11-15
                    "SSEmployeeHealth",
                    "SSEmployerEmployment",
                    "SSEmployeeEmployment",
                    "Amt",
                    "Periode",

                    //16-20
                    "SSEmployerPension",
                    "SSEmployeePension",
                    "Payruncode",
                    "PerformanceValue",
                    "AmtAll",

                    //21-22
                    "AmtDed",
                    "ProcessUPLAmt",
                    "OT1Amt",
                    "OT2Amt",
                    "OTHolidayAmt",

                    //26-30
                    "SalaryAdjustment",
                    "Transport",
                    "Meal",
                    "ADOT",
                    "FixAllowance",

                    //31-35
                    "FixDeduction",
                    "EmpAdvancePayment",
                    "SSEePension",
                    "tax",
                    "taxallowance", 

                    //36-40
                    "OT1Hr",
                    "OT2Hr",
                    "OTHolidayHr",
                    "TransportHr",
                    "MealHr"

                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PaySlipTWC()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                EmCode = Sm.DrStr(dr, c[4]),
                                EmpName = Sm.DrStr(dr, c[5]),

                                DeptName = Sm.DrStr(dr, c[6]),
                                SiteName = Sm.DrStr(dr, c[7]),
                                Salary = Sm.DrDec(dr, c[8]),
                                Functional = Sm.DrDec(dr, c[9]),
                                SSEmployerHealth = Sm.DrDec(dr, c[10]),

                                SSEmployeeHealth = Sm.DrDec(dr, c[11]),
                                SSEmployerEmployment = Sm.DrDec(dr, c[12]),
                                SSEmployeeEmployment = Sm.DrDec(dr, c[13]),
                                Amt = Sm.DrDec(dr, c[14]),
                                Periode = Sm.DrStr(dr, c[15]),

                                SSEmployerPension = Sm.DrDec(dr, c[16]),
                                SSEmployeePension = Sm.DrDec(dr, c[17]),
                                Payruncode = Sm.DrStr(dr, c[18]),
                                PerformanceValue = Sm.DrDec(dr, c[19]),
                                AmtAll = Sm.DrDec(dr, c[20]),

                                AmtDed = Sm.DrDec(dr, c[21]),
                                ProcessUPLAmt = Sm.DrDec(dr, c[22]),
                                OT1Amt = Sm.DrDec(dr, c[23]),
                                OT2Amt = Sm.DrDec(dr, c[24]),
                                OTHolidayAmt = Sm.DrDec(dr, c[25]),

                                SalaryAdjustment = Sm.DrDec(dr, c[26]),
                                Transport = Sm.DrDec(dr, c[27]),
                                Meal = Sm.DrDec(dr, c[28]),
                                ADOT = Sm.DrDec(dr, c[29]),
                                FixAllowance = Sm.DrDec(dr, c[30]),

                                FixDeduction = Sm.DrDec(dr, c[31]),
                                EmpAdvancePayment = Sm.DrDec(dr, c[32]),
                                SSEePension = Sm.DrDec(dr, c[33]),
                                tax = Sm.DrDec(dr, c[34]),
                                taxallowance = Sm.DrDec(dr, c[35]),

                                OT1Hr = Sm.DrDec(dr, c[36]),
                                OT2Hr = Sm.DrDec(dr, c[37]),
                                OTHolidayHr = Sm.DrDec(dr, c[38]),
                                TransportHr = Sm.DrDec(dr, c[39]),
                                MealHr = Sm.DrDec(dr, c[40]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                // UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")


                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region EmpAllowance
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.PayrunCode, A.EmpCode, A.Amt, B.ADName, B.ADType, ");
                    SQLDtl.AppendLine("'Rp.' As Curcode ");
                    SQLDtl.AppendLine("From Tblpayrollprocessad A ");
                    SQLDtl.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.AdCode ");
                    SQLDtl.AppendLine("Where A.PayrunCode=@PayrunCode and B.AmtType='1' ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",
                     "CurCode"

                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new EmpAllowance()
                            {
                                PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                                ADName = Sm.DrStr(drDtl, cDtl[2]),
                                Amt = Sm.DrDec(drDtl, cDtl[3]),
                                ADType = Sm.DrStr(drDtl, cDtl[4]),
                                CurCode = Sm.DrStr(drDtl, cDtl[5]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region OTAllowance
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select A.PayrunCode, A.Empcode, B.ADName, Sum(A.Amt)As Amt, Sum(A.Duration)As Duration ");
                    SQLDtl2.AppendLine("From TblPayrollProcessADOT A ");
                    SQLDtl2.AppendLine("Left Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
                    SQLDtl2.AppendLine("Where A.PayrunCode=@PayrunCode ");
                    SQLDtl2.AppendLine("Group By A.Empcode, B.AdName ");
                    SQLDtl2.AppendLine("Order By B.ADName ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@PayrunCode", Payrun);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-4
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "Duration"

                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new OTAllowance()
                            {
                                PayrunCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                ADName = Sm.DrStr(drDtl2, cDtl2[2]),
                                Amt = Sm.DrDec(drDtl2, cDtl2[3]),
                                Duration = Sm.DrDec(drDtl2, cDtl2[4]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion

            #region HIN
            if (Doctitle == "HIN")
            {
                var l3 = new List<PaySlipHIN>();
                string[] TableName = { "PaySlipHIN" };
                List<IList> myLists = new List<IList>();
                var cm3 = new MySqlCommand();

                #region Header

                var SQL3 = new StringBuilder();

                SQL3.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1') As Company, ");
                SQL3.AppendLine("A.PayrunCode, Date_Format(Concat(Left(A.PayrunCode,6),'00'),'%M %Y')As Period, ");
                SQL3.AppendLine("A.EmpCode, C.EmpName, C.EmpCodeOld, Date_Format(C.JoinDt,'%d/%m/%Y')As JoinDt, ");
                SQL3.AppendLine("D.PosName, E.DeptName, F.GrdLvlName, ");
                SQL3.AppendLine("A.Salary, A.SalaryPension, (A.Salary-A.SalaryPension)As SalaryNonPension, ");
                SQL3.AppendLine("(A.SalaryPension+(A.Salary-A.SalaryPension)) As Gapok, ");
                //SQL3.AppendLine("Case When Q.HOInd='Y' Then ");
                //SQL3.AppendLine("    A.Salary ");
                //SQL3.AppendLine("    +A.TaxableFixAllowance ");
                //SQL3.AppendLine("    -A.TaxableFixDeduction ");
                //SQL3.AppendLine("    +A.Transport ");
                //SQL3.AppendLine("    +A.Meal ");
                //SQL3.AppendLine("    +A.ADLeave ");
                //SQL3.AppendLine("    +A.SalaryAdjustment ");
                //SQL3.AppendLine("    +A.ServiceChargeIncentive ");
                //SQL3.AppendLine("    -(A.SSEmployeeHealth ");
                //SQL3.AppendLine("    + A.SSEmployeeEmployment ");
                //SQL3.AppendLine("    + A.SSEePension ");
                //SQL3.AppendLine("    + A.SSEmployeePension ");
                //SQL3.AppendLine("    + A.SSEmployeePension2 ");
                //SQL3.AppendLine("    ) ");
                //SQL3.AppendLine("Else ");
                //SQL3.AppendLine("    A.Salary ");
                //SQL3.AppendLine("    +A.TaxableFixAllowance ");
                //SQL3.AppendLine("    -A.TaxableFixDeduction ");
                //SQL3.AppendLine("    +A.Transport ");
                //SQL3.AppendLine("    +A.Meal ");
                //SQL3.AppendLine("    +A.ADLeave ");
                //SQL3.AppendLine("    +A.SalaryAdjustment ");
                //SQL3.AppendLine("    +A.ServiceChargeIncentive ");
                //SQL3.AppendLine("    - (A.SSEmployeeHealth ");
                //SQL3.AppendLine("    + A.SSEmployeeEmployment ");
                //SQL3.AppendLine("    + A.SSEePension ");
                //SQL3.AppendLine("    + A.SSEmployeePension ");
                //SQL3.AppendLine("    + A.SSEmployeePension2 ");
                //SQL3.AppendLine("    ) ");
                //SQL3.AppendLine("End As Brutto, ");

                SQL3.AppendLine("A.Salary + ");
                SQL3.AppendLine("A.TaxableFixAllowance + ");
                SQL3.AppendLine("A.Transport + ");
                SQL3.AppendLine("A.Meal + ");
                SQL3.AppendLine("A.ADLeave + ");
                SQL3.AppendLine("A.SalaryAdjustment + ");
                SQL3.AppendLine("A.ServiceChargeIncentive + ");
                SQL3.AppendLine("A.SSEmployerHealth + ");
                SQL3.AppendLine("A.SSErLifeInsurance + ");
                SQL3.AppendLine("A.SSErWorkingAccident ");
                SQL3.AppendLine("As Brutto, ");
                SQL3.AppendLine("A.Functional, ");
                SQL3.AppendLine("(A.Transport+A.Meal)As Transport, A.ServiceChargeIncentive, A.Housing, A.MobileCredit,");
                SQL3.AppendLine("(A.Functional+A.Transport+A.Meal+A.ServiceChargeIncentive+A.Housing+A.ADLeave+A.MobileCredit+(A.TaxableFixAllowance-A.Functional-A.Housing-MobileCredit)+A.OT1Amt+A.OT2Amt+A.OTHolidayAmt)As TotNonUpah, ");
                SQL3.AppendLine("A.SSErWorkingAccident AS JKK, A.SSErLifeInsurance As JKM, A.SSErRetirement As JHT, A.SSEmployerHealth As JKN, ");
                SQL3.AppendLine("A.SSErPension, (A.SSEmployerPension2+A.SSEmployerPension)As EmployerPension, ");
                SQL3.AppendLine("(A.SSEmployerHealth+A.SSErWorkingAccident+A.SSErLifeInsurance+A.SSErRetirement+A.SSErPension+A.SSEmployerPension2+A.SSEmployerPension) As TotSubsidi, ");
                SQL3.AppendLine("(A.SSEmployerEmployment+A.SSEmployeeEmployment)As Employment,(A.SSEmployerHealth+A.SSEmployeeHealth)As Health, ");
                SQL3.AppendLine("(A.SSEePension+A.SSErPension)As JaminanPensiun,(A.SSEmployerPension2+A.SSEmployeePension2+A.SSEmployerPension+A.SSEmployeePension)As Pensiun, ");
                SQL3.AppendLine("G.CreditName, H.CreditName As CreditName2, I.CreditName As CreditName3, J.CreditName As CreditName4, K.CreditName As CreditName5, ");
                SQL3.AppendLine("L.CreditName As CreditName6, M.CreditName As CreditName7, N.CreditName As CreditName8, O.CreditName As CreditName9, P.CreditName As CreditName10, ");
                SQL3.AppendLine("(A.SSEmployerEmployment+A.SSEmployeeEmployment+A.SSEePension+A.SSEmployerHealth+A.SSEmployeeHealth+A.SSErPension+A.SSEmployerPension2+A.SSEmployeePension2+A.SSEmployerPension+A.SSEmployeePension+ ");
                SQL3.AppendLine("A.CreditAdvancePayment1+A.CreditAdvancePayment2+A.CreditAdvancePayment3+A.CreditAdvancePayment4+A.CreditAdvancePayment5+ ");
                SQL3.AppendLine("A.CreditAdvancePayment6+A.CreditAdvancePayment7+A.CreditAdvancePayment8+A.CreditAdvancePayment9+A.CreditAdvancePayment10+ ");
                SQL3.AppendLine("A.Zakat+(A.TaxableFixDeduction-A.Zakat))As TotPot, ");
                SQL3.AppendLine("A.Zakat, ");
                SQL3.AppendLine("A.CreditAdvancePayment1, A.CreditAdvancePayment2, A.CreditAdvancePayment3, A.CreditAdvancePayment4, A.CreditAdvancePayment5, ");
                SQL3.AppendLine("A.CreditAdvancePayment6, A.CreditAdvancePayment7, A.CreditAdvancePayment8, A.CreditAdvancePayment9, A.CreditAdvancePayment10, ");
                SQL3.AppendLine("A.SalaryAdjustment, A.Tax, ");
                SQL3.AppendLine("(A.TaxableFixAllowance-A.Functional-A.Housing-MobileCredit) As OtherAllowance, (A.TaxableFixDeduction-A.Zakat) As OtherDeduction, ");
                SQL3.AppendLine("(A.OT1Amt+A.OT2Amt+A.OTHolidayAmt)As OT, A.Amt As THP, A.ADLeave");
                SQL3.AppendLine("from TblPayrollProcess1 A ");
                SQL3.AppendLine("Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL3.AppendLine("Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL3.AppendLine("Left Join tblPosition D On C.PosCode=D.PosCode ");
                SQL3.AppendLine("Left Join tbldepartment E On C.DeptCode=E.DeptCode ");
                SQL3.AppendLine("Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL3.AppendLine("Left Join tblcredit G On A.CreditCode1=G.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit H On A.CreditCode2=H.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit I On A.CreditCode3=I.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit J On A.CreditCode4=J.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit K On A.CreditCode5=K.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit L On A.CreditCode6=L.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit M On A.CreditCode7=M.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit N On A.CreditCode8=N.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit O On A.CreditCode9=O.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit P On A.CreditCode10=P.CreditCode ");
                SQL3.AppendLine("Inner Join TblSite Q On B.SiteCode=Q.SiteCode ");
                SQL3.AppendLine("Where A.PayrunCode=@PayrunCode ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@PayrunCode", Payrun);
                    Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                 
                    //if (Sm.GetLue(LueAGCode).Length > 0)
                    //    Sm.CmParam<String>(ref cm3, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[]
                {
                    // 0
                    "CompanyLogo",

                    //1-5
                    "Company",
                    "PayrunCode",
                    "Period",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "EmpCodeOld",
                    "JoinDt",
                    "PosName",
                    "DeptName",
                    "GrdLvlName",

                    //11-15
                    "Salary",
                    "SalaryPension",
                    "SalaryNonPension",
                    "Gapok",
                    "Functional",

                    //16-20
                    "Transport",
                    "ServiceChargeIncentive",
                    "Housing",
                    "MobileCredit",
                    "TotNonUpah",
                   
                    //21-25
                    "JKK",
                    "JKM",
                    "JHT",
                    "JKN",
                    "SSErPension",
                    
                    //26-30
                    "EmployerPension",
                    "TotSubsidi",
                    "Employment",
                    "Health",
                    "JaminanPensiun",
                    
                    //31-35
                    "Pensiun",
                    "CreditName",
                    "CreditName2",
                    "CreditName3",
                    "CreditName4",

                    //36-40
                    "CreditName5",
                    "CreditName6",
                    "CreditName7",
                    "CreditName8",
                    "CreditName9",

                    //41-45
                    "CreditName10",
                    "TotPot",
                    "Zakat",
                    "CreditAdvancePayment1",
                    "CreditAdvancePayment2",

                    //46-50
                    "CreditAdvancePayment3",
                    "CreditAdvancePayment4",
                    "CreditAdvancePayment5",
                    "CreditAdvancePayment6",
                    "CreditAdvancePayment7",

                    //51-55
                    "CreditAdvancePayment8",
                    "CreditAdvancePayment9",
                    "CreditAdvancePayment10",
                    "SalaryAdjustment",
                    "Tax",

                    //56-60
                    "OtherAllowance",
                    "OtherDeduction",
                    "OT",
                    "THP",
                    "ADLeave",

                    //61
                    "Brutto"
                });

                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            l3.Add(new PaySlipHIN()
                            {
                                CompanyLogo = Sm.DrStr(dr3, c3[0]),

                                Company = Sm.DrStr(dr3, c3[1]),
                                PayrunCode = Sm.DrStr(dr3, c3[2]),
                                Period = Sm.DrStr(dr3, c3[3]),
                                EmCode = Sm.DrStr(dr3, c3[4]),
                                EmpName = Sm.DrStr(dr3, c3[5]),

                                //6-10
                                EmpCodeOld = Sm.DrStr(dr3, c3[6]),
                                JoinDt = Sm.DrStr(dr3, c3[7]),
                                PosName = Sm.DrStr(dr3, c3[8]),
                                DeptName = Sm.DrStr(dr3, c3[9]),
                                GrdLvlName = Sm.DrStr(dr3, c3[10]),

                                //11
                                Salary = Sm.DrDec(dr3, c3[11]),
                                SalaryPension = Sm.DrDec(dr3, c3[12]),
                                SalaryNonPension = Sm.DrDec(dr3, c3[13]),
                                Gapok = Sm.DrDec(dr3, c3[14]),
                                Functional = Sm.DrDec(dr3, c3[15]),

                                Transport = Sm.DrDec(dr3, c3[16]),
                                Service = Sm.DrDec(dr3, c3[17]),
                                Housing = Sm.DrDec(dr3, c3[18]),
                                MobileCredit = Sm.DrDec(dr3, c3[19]),
                                TotNonUpah = Sm.DrDec(dr3, c3[20]),

                                JKK = Sm.DrDec(dr3, c3[21]),
                                JKM = Sm.DrDec(dr3, c3[22]),
                                JHT = Sm.DrDec(dr3, c3[23]),
                                JKN = Sm.DrDec(dr3, c3[24]),
                                SSErPension = Sm.DrDec(dr3, c3[25]),

                                EmployerPension = Sm.DrDec(dr3, c3[26]),
                                TotSubsidi = Sm.DrDec(dr3, c3[27]),
                                Employment = Sm.DrDec(dr3, c3[28]),
                                Health = Sm.DrDec(dr3, c3[29]),
                                JaminanPensiun = Sm.DrDec(dr3, c3[30]),

                                Pensiun = Sm.DrDec(dr3, c3[31]),
                                CreditCode1 = Sm.DrStr(dr3, c3[32]),
                                CreditCode2 = Sm.DrStr(dr3, c3[33]),
                                CreditCode3 = Sm.DrStr(dr3, c3[34]),
                                CreditCode4 = Sm.DrStr(dr3, c3[35]),

                                CreditCode5 = Sm.DrStr(dr3, c3[36]),
                                CreditCode6 = Sm.DrStr(dr3, c3[37]),
                                CreditCode7 = Sm.DrStr(dr3, c3[38]),
                                CreditCode8 = Sm.DrStr(dr3, c3[39]),
                                CreditCode9 = Sm.DrStr(dr3, c3[40]),

                                CreditCode10 = Sm.DrStr(dr3, c3[41]),
                                TotPot = Sm.DrDec(dr3, c3[42]),
                                Zakat = Sm.DrDec(dr3, c3[43]),
                                CreditAdvancePayment1 = Sm.DrDec(dr3, c3[44]),
                                CreditAdvancePayment2 = Sm.DrDec(dr3, c3[45]),

                                CreditAdvancePayment3 = Sm.DrDec(dr3, c3[46]),
                                CreditAdvancePayment4 = Sm.DrDec(dr3, c3[47]),
                                CreditAdvancePayment5 = Sm.DrDec(dr3, c3[48]),
                                CreditAdvancePayment6 = Sm.DrDec(dr3, c3[49]),
                                CreditAdvancePayment7 = Sm.DrDec(dr3, c3[50]),

                                CreditAdvancePayment8 = Sm.DrDec(dr3, c3[51]),
                                CreditAdvancePayment9 = Sm.DrDec(dr3, c3[52]),
                                CreditAdvancePayment10 = Sm.DrDec(dr3, c3[53]),
                                SalaryAdjustment = Sm.DrDec(dr3, c3[54]),
                                Tax = Sm.DrDec(dr3, c3[55]),  

                                OtherAllowance = Sm.DrDec(dr3, c3[56]),
                                OtherDeduction = Sm.DrDec(dr3, c3[57]),
                                OT = Sm.DrDec(dr3, c3[58]),
                                THP = Sm.DrDec(dr3, c3[59]),
                                ADLeave = Sm.DrDec(dr3, c3[60]),
                                Brutto = Sm.DrDec(dr3, c3[61]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(l3);

                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion

            #region KIM
            if (Doctitle == "KIM")
            {
                var l = new List<PaySlipTWC>();
                var ldtl = new List<EmpAllowance>();
                var ldtl2 = new List<EmpAdvancePayment>();
                var l2 = new List<EmployeeSincerely>();

                string[] TableName = { "PaySlipTWC", "EmpAllowance", "EmpAdvancePayment", "EmployeeSincerely" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                SQL.AppendLine("A.EmpCode, C.EmpName, D.DeptName, H.SiteName, A.Salary, A.Functional, A.SSEmployerHealth,  A.SSEmployeeHealth, ");
                SQL.AppendLine("A.SSEmployerEmployment, A.SSEmployeeEmployment, A.Amt, DATE_FORMAT(B.StartDt,'%M %Y')As Periode, A.SSEmployerPension, ");
                SQL.AppendLine("A.SSEmployeePension, A.Payruncode, A.PerformanceValue, IfNull(X.Amt,0)As AmtAll, IfNull(Y.Amt,0)As AmtDed,  ");
                SQL.AppendLine("G.PosName, A.Tax, A.SalaryAdjustment, A.SSEmployeeHealth As SSEmpHealth, Z.Amt as AdvancePay, ");
                SQL.AppendLine("Z.CreditName, Z.AdvancePayment, A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.SSEePension, DATE_FORMAT(B.EndDt,'%M %Y')As Period2 ");
                SQL.AppendLine(" From tblpayrollprocess1 A ");
                SQL.AppendLine(" Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL.AppendLine(" Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine(" Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL.AppendLine(" Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL.AppendLine(" Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL.AppendLine(" Left Join tblsite H On C.SiteCode=H.SiteCode ");
                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, B.ADName, Sum(A.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocessad A ");
                SQL.AppendLine("	Inner Join TblAllowanceDeduction B On A.ADCode=B.AdCode ");
                SQL.AppendLine("	Where B.ADType='A' And B.AmtType='1' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(" )X On A.PayrunCode=X.Payruncode And A.EmpCode=X.EmpCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, B.ADName, Sum(A.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocessad A ");
                SQL.AppendLine("	Inner Join TblAllowanceDeduction B On A.ADCode=B.AdCode ");
                SQL.AppendLine("	Where B.ADType='D' And B.AmtType='1' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(" )Y On A.PayrunCode=Y.Payruncode And A.EmpCode=Y.EmpCode ");

                // advance payment
                SQL.AppendLine(" Left Join ");
                SQL.AppendLine(" ( ");
                //SQL.AppendLine(" Select Z.Payruncode, Z.EmpCode, Z.Amt, Z.AdvancePayment, Z.Creditname ");
                //SQL.AppendLine("    from ( ");
                //SQL.AppendLine("    Select X1.Payruncode, X1.EmpCode, X1.Amt, X1.AdvancePayment, X2.Creditname ");
                //SQL.AppendLine("    from ( ");
                //SQL.AppendLine(" 	    Select A.Payruncode, A.EmpCode, Group_concat(Distinct Format(B.Amt,2) separator '\n' )As Amt, Sum(B.Amt)As AdvancePayment ");
                //SQL.AppendLine(" 	    From Tblpayrollprocess1 A ");
                //SQL.AppendLine(" 	    Inner join tbladvancepaymentprocess B On A.Payruncode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                //SQL.AppendLine(" 	    Where A.Payruncode=@Payruncode ");
                //SQL.AppendLine(" 	    group by A.Empcode, A.Payruncode ");
                //SQL.AppendLine("    )X1 ");
                //SQL.AppendLine("    left join ");
                //SQL.AppendLine("    ( ");
                //SQL.AppendLine("        Select A.EmpCode, Group_concat(Distinct D.CreditName separator '\n')As CreditName "); 
                //SQL.AppendLine("        From Tblpayrollprocess1 A ");
                //SQL.AppendLine(" 	    Inner join tbladvancepaymentprocess B On A.Payruncode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                //SQL.AppendLine("        Inner join tbladvancepaymenthdr C On B.DocNo=C.DocNo ");
                //SQL.AppendLine("        Left join TblCredit D On C.CreditCode=D.CreditCode ");
                //SQL.AppendLine("        Where A.Payruncode=@Payruncode Group by A.EmpCode ");
                //SQL.AppendLine("    )X2 On X1.EmpCode=X2.EmpCode ");
                //SQL.AppendLine(" )Z ");
                SQL.AppendLine("    Select A.Payruncode, A.EmpCode, "); 
                SQL.AppendLine("    Group_concat(Distinct D.CreditName separator '\n' )As creditname, ");
                SQL.AppendLine("    Group_concat(Distinct concat('Rp        ', Format(B.Amt,2)) separator '\n' )As Amt, Sum(B.Amt)As AdvancePayment  ");
                SQL.AppendLine("    From Tblpayrollprocess1 A  ");
                SQL.AppendLine("    Inner join tbladvancepaymentprocess B On A.Payruncode=B.PayrunCode And A.EmpCode=B.EmpCode  ");
                SQL.AppendLine("    Inner Join TblAdvancepaymenthdr C On B.DocNo = C.DocNo ");
                SQL.AppendLine("    Inner Join TblCredit D On C.CreditCode = D.CreditCOde ");
                SQL.AppendLine("    Where A.Payruncode=@Payruncode  ");
                SQL.AppendLine("    Group by A.Empcode, A.Payruncode  "); 

                SQL.AppendLine(" )Z On A.Payruncode=Z.PayrunCode And A.EmpCode=Z.EmpCode ");

                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");

                SQL.AppendLine(" Where A.PayrunCode=@PayrunCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("And A.EmpCode Not In (Select B.EmpCode From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B Where A.AGCode=B.AGCode And A.ActInd='Y') ");

                SQL.AppendLine(" Order By A.Payruncode, C.EmpName ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    if (Sm.GetLue(LueAGCode).Length > 0)
                        Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company", 
                    "Address",
                    "Phone",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "DeptName", 
                    "SiteName",
                    "Salary",
                    "Functional",
                    "SSEmployerHealth",

                    //11-15
                    "SSEmployeeHealth",
                    "SSEmployerEmployment",
                    "SSEmployeeEmployment",
                    "Amt",
                    "Periode",

                    //16-20
                    "SSEmployerPension",
                    "SSEmployeePension",
                    "Payruncode",
                    "PerformanceValue",
                    "AmtAll",

                    //21-25
                    "AmtDed",
                    "PosName",
                    "Tax",
                    "SalaryAdjustment", //rapel
                    "SSEmpHealth", // SSEmpHealth = bpjs kesehatan tambahan
                   
                    //26-30
                    "AdvancePay", 
                    "CreditName",
                    "AdvancePayment",
                    "OT1Amt",
                    "OT2Amt",

                    //31-33
                    "OTHolidayAmt",
                    "SSEePension",
                    "Period2"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PaySlipTWC()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                EmCode = Sm.DrStr(dr, c[4]),
                                EmpName = Sm.DrStr(dr, c[5]),

                                DeptName = Sm.DrStr(dr, c[6]),
                                SiteName = Sm.DrStr(dr, c[7]),
                                Salary = Sm.DrDec(dr, c[8]),
                                Functional = Sm.DrDec(dr, c[9]),
                                SSEmployerHealth = Sm.DrDec(dr, c[10]),

                                SSEmployeeHealth = Sm.DrDec(dr, c[11]),
                                SSEmployerEmployment = Sm.DrDec(dr, c[12]),
                                SSEmployeeEmployment = Sm.DrDec(dr, c[13]),
                                Amt = Sm.DrDec(dr, c[14]),
                                Periode = Sm.DrStr(dr, c[15]),

                                SSEmployerPension = Sm.DrDec(dr, c[16]),
                                SSEmployeePension = Sm.DrDec(dr, c[17]),
                                Payruncode = Sm.DrStr(dr, c[18]),
                                PerformanceValue = Sm.DrDec(dr, c[19]),
                                AmtAll = Sm.DrDec(dr, c[20]),
                                AmtDed = Sm.DrDec(dr, c[21]),
                                PosName = Sm.DrStr(dr, c[22]),
                                tax = Sm.DrDec(dr, c[23]),
                                SalaryAdjustment = Sm.DrDec(dr, c[24]),
                                SSEmpHealth = Sm.DrDec(dr, c[25]),
                                AdvancePay = Sm.DrStr(dr, c[26]),

                                CreditName = Sm.DrStr(dr, c[27]),
                                AdvancePayment = Sm.DrDec(dr, c[28]),
                                OT1Amt = Sm.DrDec(dr, c[29]),
                                OT2Amt = Sm.DrDec(dr, c[30]),
                                OTHolidayAmt = Sm.DrDec(dr, c[31]),
                                SSEePension = Sm.DrDec(dr, c[32]),
                                Periode2 = Sm.DrStr(dr, c[33]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region EmpAllowance
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.PayrunCode, A.EmpCode, C.ADName, B.Amt, C.ADType, If(B.Amt>0,'Rp','')As Curcode ");
                    SQLDtl.AppendLine("From tblpayrollprocess1 A ");
                    SQLDtl.AppendLine("Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                    SQLDtl.AppendLine("Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode and C.AmtType='1' ");
                    SQLDtl.AppendLine("Where A.PayrunCode=@PayrunCode "); // And C.ADType='A' ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",
                     "CurCode"

                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new EmpAllowance()
                            {
                                PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                                ADName = Sm.DrStr(drDtl, cDtl[2]),
                                Amt = Sm.DrDec(drDtl, cDtl[3]),
                                ADType = Sm.DrStr(drDtl, cDtl[4]),
                                CurCode = Sm.DrStr(drDtl, cDtl[5]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region EmpAdvancePayment
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("    Select A.Payruncode, A.EmpCode, C.CreditCode, D.creditname, SUM(B.Amt) Amt, C.Remark ");
                    SQLDtl2.AppendLine("    From Tblpayrollprocess1 A  ");
                    SQLDtl2.AppendLine("    Inner join tbladvancepaymentprocess B On A.Payruncode=B.PayrunCode And A.EmpCode=B.EmpCode  ");
                    SQLDtl2.AppendLine("    Inner Join TblAdvancepaymenthdr C On B.DocNo = C.DocNo ");
                    SQLDtl2.AppendLine("    Inner Join TblCredit D On C.CreditCode = D.CreditCOde ");
                    SQLDtl2.AppendLine("    Where A.Payruncode=@Payruncode  ");
                    SQLDtl2.AppendLine("    Group by A.Payruncode, A.EmpCode, C.CreditCode, D.creditname ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@PayrunCode", Payrun);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "CreditCode",
                     "creditname",
                     "Amt",
                     "Remark",

                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new EmpAdvancePayment()
                            {
                                PayrunCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                APCode = Sm.DrStr(drDtl2, cDtl2[2]),
                                APName = Sm.DrStr(drDtl2, cDtl2[3]),
                                Amt = Sm.DrDec(drDtl2, cDtl2[4]),
                                Remark = Sm.DrStr(drDtl2, cDtl2[5]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Sincerely
                var cm4 = new MySqlCommand();
                var SQL4 = new StringBuilder();

                SQL4.AppendLine("Select A.EmpCode, A.EmpName, B.DeptName, C.PosName ");
                SQL4.AppendLine("From TblEmployee A ");
                SQL4.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL4.AppendLine("Left Join tblposition C On A.PosCode=C.PosCode ");
                SQL4.AppendLine("Where A.EmpCode=@EmpCode ");

                using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn4.Open();
                    cm4.Connection = cn4;
                    cm4.CommandText = SQL4.ToString();
                    Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCodeSincerely);
                    var dr4 = cm4.ExecuteReader();
                    var c4 = Sm.GetOrdinal(dr4, new string[] 
                        {
                         //0-3
                         "EmpCode",
                         "EmpName",
                         "DeptName",
                         "PosName"
                        
                        });
                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            l2.Add(new EmployeeSincerely()
                            {
                                EmpCode = Sm.DrStr(dr4, c4[0]),

                                EmpName = Sm.DrStr(dr4, c4[1]),
                                DeptName = Sm.DrStr(dr4, c4[2]),
                                PosName = Sm.DrStr(dr4, c4[3]),
                            });
                        }
                    }
                    dr4.Close();
                }
                myLists.Add(l2);

                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion

            #region AWG
            if (Doctitle == "AG-YK")
            {
                var l = new List<PaySlipAWG>();
                var ldtl = new List<EmpAllowance>();
                var l2 = new List<EmployeeSincerely>();

                string[] TableName = { "PaySlipAWG", "EmpAllowance", "EmployeeSincerely" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                SQL.AppendLine("A.EmpCode, C.EmpName, D.DeptName, H.SiteName, A.Salary, A.Functional, A.SSEmployerHealth,  A.SSEmployeeHealth, ");
                SQL.AppendLine("A.SSEmployerEmployment, A.SSEmployeeEmployment, A.Amt, DATE_FORMAT(B.StartDt,'%M %Y')As Periode, A.SSEmployerPension, ");
                SQL.AppendLine("A.SSEmployeePension, A.Payruncode, A.PerformanceValue, IfNull(X.Amt,0)As AmtAll, IfNull(Y.Amt,0)As AmtDed,  ");
                SQL.AppendLine("G.PosName, A.Tax, A.SalaryAdjustment, (A.SSEmployeeHealth-A.SSEmployerHealth)As SSEmpHealth, Z.Amt as AdvancePay, ");
                SQL.AppendLine("Z.CreditName, Z.AdvancePayment, A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.OverHrAmt, A.Atdpremi, A.Atdpremi2, A.Atdpremi3, A.ShiftPremi, A.SSEePension ");
                SQL.AppendLine(" From tblpayrollprocess1 A ");
                SQL.AppendLine(" Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL.AppendLine(" Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine(" Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL.AppendLine(" Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL.AppendLine(" Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL.AppendLine(" Left Join tblsite H On C.SiteCode=H.SiteCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, B.ADName, Sum(A.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocessad A ");
                SQL.AppendLine("	Inner Join TblAllowanceDeduction B On A.ADCode=B.AdCode ");
                SQL.AppendLine("	Where B.ADType='A' And B.AmtType='1' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(" )X On A.PayrunCode=X.Payruncode And A.EmpCode=X.EmpCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, B.ADName, Sum(A.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocessad A ");
                SQL.AppendLine("	Inner Join TblAllowanceDeduction B On A.ADCode=B.AdCode ");
                SQL.AppendLine("	Where B.ADType='D' And B.AmtType='1' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(" )Y On A.PayrunCode=Y.Payruncode And A.EmpCode=Y.EmpCode ");

                // advance payment
                SQL.AppendLine(" Left Join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine(" Select Z.Payruncode, Z.EmpCode, Z.Amt, Z.AdvancePayment, Z.Creditname ");
                SQL.AppendLine("    from ( ");
                SQL.AppendLine("    Select X1.Payruncode, X1.EmpCode, X1.Amt, X1.AdvancePayment, X2.Creditname ");
                SQL.AppendLine("    from ( ");
                SQL.AppendLine(" 	    Select A.Payruncode, A.EmpCode, Group_concat(Distinct Format(B.Amt,2) separator '\n' )As Amt, Sum(B.Amt)As AdvancePayment ");
                SQL.AppendLine(" 	    From Tblpayrollprocess1 A ");
                SQL.AppendLine(" 	    Inner join tbladvancepaymentprocess B On A.Payruncode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine(" 	    Where A.Payruncode=@Payruncode ");
                SQL.AppendLine(" 	    group by A.Empcode, A.Payruncode ");
                SQL.AppendLine("    )X1 ");
                SQL.AppendLine("    left join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select A.EmpCode, Group_concat(Distinct D.CreditName separator '\n')As CreditName ");
                SQL.AppendLine("        From Tblpayrollprocess1 A ");
                SQL.AppendLine(" 	    Inner join tbladvancepaymentprocess B On A.Payruncode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("        Inner join tbladvancepaymenthdr C On B.DocNo=C.DocNo ");
                SQL.AppendLine("        Left join TblCredit D On C.CreditCode=D.CreditCode ");
                SQL.AppendLine("        Group by A.EmpCode ");
                SQL.AppendLine("    )X2 On X1.EmpCode=X2.EmpCode ");
                SQL.AppendLine(" )Z ");
                SQL.AppendLine(" )Z On A.Payruncode=Z.PayrunCode And A.EmpCode=Z.EmpCode ");

                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");

                SQL.AppendLine(" Where A.PayrunCode=@PayrunCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("And A.EmpCode Not In (Select B.EmpCode From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B Where A.AGCode=B.AGCode And A.ActInd='Y') ");

                SQL.AppendLine(" Order By A.Payruncode, C.EmpName ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    if (Sm.GetLue(LueAGCode).Length > 0)
                        Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company", 
                    "Address",
                    "Phone",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "DeptName", 
                    "SiteName",
                    "Salary",
                    "Functional",
                    "SSEmployerHealth",

                    //11-15
                    "SSEmployeeHealth",
                    "SSEmployerEmployment",
                    "SSEmployeeEmployment",
                    "Amt",
                    "Periode",

                    //16-20
                    "SSEmployerPension",
                    "SSEmployeePension",
                    "Payruncode",
                    "PerformanceValue",
                    "AmtAll",

                    //21-25
                    "AmtDed",
                    "PosName",
                    "Tax",
                    "SalaryAdjustment", //rapel
                    "SSEmpHealth", // SSEmpHealth = bpjs kesehatan tambahan
                   
                    //26-30
                    "AdvancePay", 
                    "CreditName",
                    "AdvancePayment",
                    "OT1Amt",
                    "OT2Amt",

                    //31-35
                    "OTHolidayAmt",
                    "OverHrAmt", 
                    "Atdpremi", 
                    "Atdpremi2", 
                    "Atdpremi3",
                    //36
                    "ShiftPremi",
                    "SSEePension"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PaySlipAWG()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                EmCode = Sm.DrStr(dr, c[4]),
                                EmpName = Sm.DrStr(dr, c[5]),

                                DeptName = Sm.DrStr(dr, c[6]),
                                SiteName = Sm.DrStr(dr, c[7]),
                                Salary = Sm.DrDec(dr, c[8]),
                                Functional = Sm.DrDec(dr, c[9]),
                                SSEmployerHealth = Sm.DrDec(dr, c[10]),

                                SSEmployeeHealth = Sm.DrDec(dr, c[11]),
                                SSEmployerEmployment = Sm.DrDec(dr, c[12]),
                                SSEmployeeEmployment = Sm.DrDec(dr, c[13]),
                                Amt = Sm.DrDec(dr, c[14]),
                                Periode = Sm.DrStr(dr, c[15]),

                                SSEmployerPension = Sm.DrDec(dr, c[16]),
                                SSEmployeePension = Sm.DrDec(dr, c[17]),
                                Payruncode = Sm.DrStr(dr, c[18]),
                                PerformanceValue = Sm.DrDec(dr, c[19]),
                                AmtAll = Sm.DrDec(dr, c[20]),
                                AmtDed = Sm.DrDec(dr, c[21]),
                                PosName = Sm.DrStr(dr, c[22]),
                                tax = Sm.DrDec(dr, c[23]),
                                SalaryAdjustment = Sm.DrDec(dr, c[24]),
                                SSEmpHealth = Sm.DrDec(dr, c[25]),
                                AdvancePay = Sm.DrStr(dr, c[26]),

                                CreditName = Sm.DrStr(dr, c[27]),
                                AdvancePayment = Sm.DrDec(dr, c[28]),
                                OT1Amt = Sm.DrDec(dr, c[29]),
                                OT2Amt = Sm.DrDec(dr, c[30]),
                                OTHolidayAmt = Sm.DrDec(dr, c[31]),

                                OverHrAmt = Sm.DrDec(dr, c[32]),
                                AtdPremi1 = Sm.DrDec(dr, c[33]),
                                AtdPremi2 = Sm.DrDec(dr, c[34]),
                                AtdPremi3 = Sm.DrDec(dr, c[35]),
                                ShiftPremi = Sm.DrDec(dr, c[36]),
                                SSEePension = Sm.DrDec(dr, c[37]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),


                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region EmpAllowance
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.PayrunCode, A.EmpCode, C.ADName, B.Amt, C.ADType, If(B.Amt>0,'Rp','')As Curcode ");
                    SQLDtl.AppendLine("From tblpayrollprocess1 A ");
                    SQLDtl.AppendLine("Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                    SQLDtl.AppendLine("Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                    SQLDtl.AppendLine("Where A.PayrunCode=@PayrunCode and C.AmtType='1' "); // And C.ADType='A' ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",
                     "CurCode"

                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new EmpAllowance()
                            {
                                PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                                ADName = Sm.DrStr(drDtl, cDtl[2]),
                                Amt = Sm.DrDec(drDtl, cDtl[3]),
                                ADType = Sm.DrStr(drDtl, cDtl[4]),
                                CurCode = Sm.DrStr(drDtl, cDtl[5]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Sincerely
                var cm4 = new MySqlCommand();
                var SQL4 = new StringBuilder();

                SQL4.AppendLine("Select A.EmpCode, A.EmpName, B.DeptName, C.PosName ");
                SQL4.AppendLine("From TblEmployee A ");
                SQL4.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
                SQL4.AppendLine("Left Join tblposition C On A.PosCode=C.PosCode ");
                SQL4.AppendLine("Where A.EmpCode=@EmpCode ");

                using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn4.Open();
                    cm4.Connection = cn4;
                    cm4.CommandText = SQL4.ToString();
                    Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCodeSincerely);
                    var dr4 = cm4.ExecuteReader();
                    var c4 = Sm.GetOrdinal(dr4, new string[] 
                        {
                         //0-3
                         "EmpCode",
                         "EmpName",
                         "DeptName",
                         "PosName"
                        
                        });
                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            l2.Add(new EmployeeSincerely()
                            {
                                EmpCode = Sm.DrStr(dr4, c4[0]),

                                EmpName = Sm.DrStr(dr4, c4[1]),
                                DeptName = Sm.DrStr(dr4, c4[2]),
                                PosName = Sm.DrStr(dr4, c4[3]),
                            });
                        }
                    }
                    dr4.Close();
                }
                myLists.Add(l2);

                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion

            #region SCU
            if (Doctitle == "SCU")
            {
                var l = new List<PaySlipTWC>();
                var ldtl = new List<EmpAllowance>();
                var ldtl2 = new List<OTAllowance>();
                var ldtl3 = new List<EmpDeduction>();

                string[] TableName = { "PaySlipTWC", "EmpAllowance", "OTAllowance", "EmpDeduction" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, Z.CompanyName As Company, Z.CompanyPhone As Phone, Z.CompanyAddress As Address,  ");
                }
                else
                {
                    SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                }
                SQL.AppendLine("A.EmpCode, UPPER(C.EmpName) EmpName, UPPER(D.DeptName) DeptName, UPPER(H.SiteName) SiteName, A.Salary, A.Functional, A.SSEmployerHealth,  A.SSEmployeeHealth, ");
                SQL.AppendLine("A.SSEmployerEmployment, A.SSEmployeeEmployment, A.Amt, UPPER(DATE_FORMAT(Concat(Left(A.PayrunCode, 6), '01'),'%M %Y')) As Periode, A.SSEmployerPension, ");
                SQL.AppendLine("A.SSEmployeePension, A.Payruncode, A.PerformanceValue, IfNull(X.Amt,0)As AmtAll, IfNull(Y.Amt,0)As AmtDed, ");
                SQL.AppendLine("A.ProcessUPLAmt, A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.SalaryAdjustment, A.Transport, A.Meal, A.ADOT, A.FixAllowance, ");
                SQL.AppendLine("A.FixDeduction, A.EmpAdvancePayment, A.SSEePension, A.tax, A.taxallowance, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, O.TransportHr, P.MealHr, ");
                SQL.AppendLine("UPPER(C.DeptCode) DeptCode, UPPER(Q.BankName) BankName, C.BankAcNo, R.AmtInsPntSA, S.AmtInsPntUTD, A.SSEeDPLK, A.SSErDPLK, A.PLAmt, C.EmpCodeOld, F.GrdLvlName, ");
                SQL.AppendLine("(A.Salary-(U.AMTDeduction+A.SSEmployeeEmployment + A.SSEePension + A.SSEeDPLK + A.SSEmployeeHealth)) As AmtUpahDeduction, V.EmpName As EmpCodeSincerely, ");
                SQL.AppendLine("(A.Meal+A.Transport+A.variableallowance+ifnull(X.Amt, 0)) As TotalAmtAllowance, G.PosName, C.SectionCode, W.SectionName, A1.Amt As UTDAmt, A.ADleave ");

                SQL.AppendLine(" From tblpayrollprocess1 A ");
                SQL.AppendLine(" Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL.AppendLine(" Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine(" Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL.AppendLine(" Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL.AppendLine(" Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL.AppendLine(" Left Join tblsite H On C.SiteCode=H.SiteCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='A' And A.payrunCode = @payrunCode ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")X On A.PayrunCode=X.Payruncode And A.EmpCode=X.EmpCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='D' And A.payrunCode = @payrunCode ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")Y On A.PayrunCode=Y.Payruncode And A.EmpCode=Y.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select PayrunCode, Empcode, count(transport)As TransportHr ");
                SQL.AppendLine("    From TblPayrollProcess2 ");
                SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Transport !=0 ");
                SQL.AppendLine("    Group by Payruncode, EmpCode ");
                SQL.AppendLine(" )O On A.PayrunCode=O.PayrunCode And A.EmpCode=O.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select PayrunCode, Empcode, count(Meal)As MealHr ");
                SQL.AppendLine("    From TblPayrollProcess2 ");
                SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Meal !=0 ");
                SQL.AppendLine("    Group by Payruncode, EmpCode ");
                SQL.AppendLine(" )P On A.PayrunCode=P.PayrunCode And A.EmpCode=P.EmpCode ");

                SQL.AppendLine(" Left Join TblBank Q on C.BankCode = Q.BankCode");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("     Select A.payrunCOde, A.EmpCode, D.insPntCode, D.InspntName, SUM(B.AmtInsPnt) As AmtInsPntSA ");
                SQL.AppendLine("     From tblpayrollProcess1 A ");
                SQL.AppendLine("     Inner Join tblEmpInsPntDtl B On A.PayrunCode = B.PayrunCode And A.EmpCode = B.EmpCode ");
                SQL.AppendLine("     Inner Join TblEmpInsPnthdr C On B.DocNo = C.DocNo And C.CancelInd = 'N' ");
                SQL.AppendLine("     Inner Join tblinspnt D On C.InsPntCode = D.InspntCode ");
                SQL.AppendLine("     Where A.PayrunCode=@PayrunCode And D.InsPntCode='01' ");
                SQL.AppendLine("     Group by A.payrunCOde, A.EmpCode, D.InspntName ");
                SQL.AppendLine("     Having Sum(B.AmtInsPnt)<>0.00 ");
                SQL.AppendLine("     Order By D.InspntName ");
                SQL.AppendLine(" )R On  A.PayrunCode=R.PayrunCode And A.EmpCode=R.EmpCode  ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("     Select A.payrunCOde, A.EmpCode, D.insPntCode, D.InspntName, SUM(B.AmtInsPnt) As AmtInsPntUTD ");
                SQL.AppendLine("     From tblpayrollProcess1 A ");
                SQL.AppendLine("     Inner Join tblEmpInsPntDtl B On A.PayrunCode = B.PayrunCode And A.EmpCode = B.EmpCode ");
                SQL.AppendLine("     Inner Join TblEmpInsPnthdr C On B.DocNo = C.DocNo And C.CancelInd = 'N' ");
                SQL.AppendLine("     Inner Join tblinspnt D On C.InsPntCode = D.InspntCode ");
                SQL.AppendLine("     Where A.PayrunCode=@PayrunCode And D.InsPntCode='02' ");
                SQL.AppendLine("     Group by A.payrunCOde, A.EmpCode, D.InspntName ");
                SQL.AppendLine("     Having Sum(B.AmtInsPnt)<>0.00 ");
                SQL.AppendLine("     Order By D.InspntName ");
                SQL.AppendLine(" )S On  A.PayrunCode=S.PayrunCode And A.EmpCode=S.EmpCode  ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, SUM(C.Amt) As AMTDeduction    ");
                SQL.AppendLine("    From    ");
                SQL.AppendLine("    (   ");
                SQL.AppendLine("        Select A.Amt As AmtUpah, A.PayrunCode, A.EmpCode, B.CreditCode, B.Creditname    ");
                SQL.AppendLine("        From tblpayrollprocess1 A   ");
                SQL.AppendLine("        Inner Join TblCredit B On 0=0 And B.ActInd = 'Y'  ");
                SQL.AppendLine("        Where payrunCode = @PayrunCode   ");
                SQL.AppendLine("        union ALl   ");
                SQL.AppendLine("        Select A.Amt, A.PayrunCode,  A.EmpCode, B.AdCode, B.Adname    ");
                SQL.AppendLine("        From TblpayrollProcess1 A    ");
                SQL.AppendLine("        Inner Join TblAllowanceDeduction B On 0=0    ");
                SQL.AppendLine("        Where A.PayrunCode = @PayrunCode And B.Adtype = 'D'    ");
                SQL.AppendLine("    )A   ");
                SQL.AppendLine("    left Join  ");
                SQL.AppendLine("    (   ");
                SQL.AppendLine("        select PayrunCode, CreditCode1 As CreditCode, EmpCode, ifnull(CreditAdvancepayment1, 0) Amt From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        Union ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode2, EmpCode, ifnull(CreditAdvancepayment2, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        Union ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode3, EmpCode, ifnull(CreditAdvancepayment3, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        UNION ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode4, EmpCode, ifnull(CreditAdvancepayment4, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        UNION ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode5, EmpCode, ifnull(CreditAdvancepayment5, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        UNION ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode6, EmpCode, ifnull(CreditAdvancepayment6, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode  ");
                SQL.AppendLine("        Union ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode7, EmpCode, ifnull(CreditAdvancepayment7, 0) From Tblpayrollprocess1  ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        Union ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode8, EmpCode, ifnull(CreditAdvancepayment8, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        UNION ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode9, EmpCode, ifnull(CreditAdvancepayment9, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("        UNION ALL   ");
                SQL.AppendLine("        select PayrunCode, CreditCode10, EmpCode, ifnull(CreditAdvancepayment10, 0) From Tblpayrollprocess1   ");
                SQL.AppendLine("        Where PayrunCode = @PayrunCode   ");
                SQL.AppendLine("    )B On A.PayrunCode = B.payrunCode And A.EmpCode = B.EmpCode And A.CreditCode = B.CreditCode   ");
                SQL.AppendLine("    left Join     ");
                SQL.AppendLine("    (   ");
                SQL.AppendLine("        Select A.payrunCode, A.EmpCode, B.AdCode, B.Amt    ");
                SQL.AppendLine("        From tblpayrollprocess1 A   ");
                SQL.AppendLine("        inner Join tblpayrollProcessAD B on A.PayrunCOde= B.payruNCode  And A.EmpCode = B.EmpCode   ");
                SQL.AppendLine("        Where A.payrunCode = @payrunCode   ");
                SQL.AppendLine("    )C On  A.payrunCode = C.payrunCode  And A.EmpCode = C.EmpCode And A.CreditCode = C.AdCode  ");
                SQL.AppendLine("    Where ADCode not in ('010') ");
                SQL.AppendLine("    Group By  A.EmpCode ");
                SQL.AppendLine(" )U On  A.PayrunCode=U.PayrunCode And A.EmpCode=U.EmpCode  ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select B.EmpName From tblparameter A  ");
                SQL.AppendLine("    Inner Join TblEmployee B On A.ParValue = B.EmpCode ");
                SQL.AppendLine("    Where A.parCode = 'EmpCodeSincerely' ");
                SQL.AppendLine(")V On 0=0 ");
                SQL.AppendLine("Left Join TblSection W On C.SectionCode = W.SectionCode ");

                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select payrunCode, EmpCode, Amt From tblpayrollprocessAd ");
                SQL.AppendLine("    Where payrunCode = @payrunCode And AdCode = '019' ");
                SQL.AppendLine("    group by payrunCode, EmpCode  ");
                SQL.AppendLine(")A1 On A.payrunCode = A1.payrunCode And A.EmpCode = A1.EmpCode ");

                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select distinct A.PayrunCode, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                    SQL.AppendLine("    From Tblpayrun A  ");
                    SQL.AppendLine("    Inner Join TblSite B On A.SiteCode=B.SiteCode  ");
                    SQL.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode=C.ProfitCenterCode  ");
                    SQL.AppendLine("    Inner Join TblEntity D On C.EntCode=D.EntCode  ");
                    SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                    SQL.AppendLine(") Z On A.PayrunCode=Z.PayrunCode ");
                }


                SQL.AppendLine(" Where A.PayrunCode=@PayrunCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("And A.EmpCode Not In (Select B.EmpCode From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B Where A.AGCode=B.AGCode And A.ActInd='Y') ");

                SQL.AppendLine(" Order By A.Payruncode, C.EmpName ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);

                    if (TxtSiteName.Text.Length > 0)
                    {
                        string CompanyLogo = Sm.GetValue(
                           "Select D.EntLogoName " +
                           "From TblPayrun A  " +
                           "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                           "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                           "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                           "Where A.PayrunCode ='" + TxtPayrunCode.Text + "' "
                       );
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }
                    if (Sm.GetLue(LueAGCode).Length > 0)
                        Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company", 
                    "Address",
                    "Phone",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "DeptName", 
                    "SiteName",
                    "Salary",
                    "Functional",
                    "SSEmployerHealth",

                    //11-15
                    "SSEmployeeHealth",
                    "SSEmployerEmployment",
                    "SSEmployeeEmployment",
                    "Amt",
                    "Periode",

                    //16-20
                    "SSEmployerPension",
                    "SSEmployeePension",
                    "Payruncode",
                    "PerformanceValue",
                    "AmtAll",

                    //21-22
                    "AmtDed",
                    "ProcessUPLAmt",
                    "OT1Amt",
                    "OT2Amt",
                    "OTHolidayAmt",

                    //26-30
                    "SalaryAdjustment",
                    "Transport",
                    "Meal",
                    "ADOT",
                    "FixAllowance",

                    //31-35
                    "FixDeduction",
                    "EmpAdvancePayment",
                    "SSEePension",
                    "tax",
                    "taxallowance", 

                    //36-40
                    "OT1Hr",
                    "OT2Hr",
                    "OTHolidayHr",
                    "TransportHr",
                    "MealHr",
                    //41-45
                    "DeptCode",
                    "BankName",
                    "BankAcNo",
                    "AmtInsPntSA", 
                    "AmtInsPntUTD",
                    //46-50
                    "SSEeDPLK",
                    "SSErDPLK",
                    "PLAmt",
                    "EmpCodeOld", 
                    "GrdLvlName",
                    //51-55
                    "AmtUpahDeduction",
                    "EmpCodeSincerely", 
                    "TotalAmtAllowance",
                    "PosName",
                    "SectionCode",
                    //56-57
                    "SectionName",
                    "UTDAmt",
                    "ADleave"
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PaySlipTWC()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                EmCode = Sm.DrStr(dr, c[4]),
                                EmpName = Sm.DrStr(dr, c[5]),

                                DeptName = Sm.DrStr(dr, c[6]),
                                SiteName = Sm.DrStr(dr, c[7]),
                                Salary = Sm.DrDec(dr, c[8]),
                                Functional = Sm.DrDec(dr, c[9]),
                                SSEmployerHealth = Sm.DrDec(dr, c[10]),

                                SSEmployeeHealth = Sm.DrDec(dr, c[11]),
                                SSEmployerEmployment = Sm.DrDec(dr, c[12]),
                                SSEmployeeEmployment = Sm.DrDec(dr, c[13]),
                                Amt = Sm.DrDec(dr, c[14]),
                                Periode = Sm.DrStr(dr, c[15]),

                                SSEmployerPension = Sm.DrDec(dr, c[16]),
                                SSEmployeePension = Sm.DrDec(dr, c[17]),
                                Payruncode = Sm.DrStr(dr, c[18]),
                                PerformanceValue = Sm.DrDec(dr, c[19]),
                                AmtAll = Sm.DrDec(dr, c[20]),

                                AmtDed = Sm.DrDec(dr, c[21]),
                                ProcessUPLAmt = Sm.DrDec(dr, c[22]),
                                OT1Amt = Sm.DrDec(dr, c[23]),
                                OT2Amt = Sm.DrDec(dr, c[24]),
                                OTHolidayAmt = Sm.DrDec(dr, c[25]),

                                SalaryAdjustment = Sm.DrDec(dr, c[26]),
                                Transport = Sm.DrDec(dr, c[27]),
                                Meal = Sm.DrDec(dr, c[28]),
                                ADOT = Sm.DrDec(dr, c[29]),
                                FixAllowance = Sm.DrDec(dr, c[30]),

                                FixDeduction = Sm.DrDec(dr, c[31]),
                                EmpAdvancePayment = Sm.DrDec(dr, c[32]),
                                SSEePension = Sm.DrDec(dr, c[33]),
                                tax = Sm.DrDec(dr, c[34]),
                                taxallowance = Sm.DrDec(dr, c[35]),

                                OT1Hr = Sm.DrDec(dr, c[36]),
                                OT2Hr = Sm.DrDec(dr, c[37]),
                                OTHolidayHr = Sm.DrDec(dr, c[38]),
                                TransportHr = Sm.DrDec(dr, c[39]),
                                MealHr = Sm.DrDec(dr, c[40]),

                                DeptCode = Sm.DrStr(dr, c[41]),
                                Bank = Sm.DrStr(dr, c[42]),
                                BankAccount = Sm.DrStr(dr, c[43]),
                                ShiftAllowance = Sm.DrDec(dr, c[44]),
                                AmtInsPntUTD = Sm.DrDec(dr, c[45]),
                                SSEeDPLK = Sm.DrDec(dr, c[46]),
                                SSErDPLK = Sm.DrDec(dr, c[47]),
                                PLAmt = Sm.DrDec(dr, c[48]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[14])),
                                EmpCodeOld = Sm.DrStr(dr, c[49]),
                                Level = Sm.DrStr(dr, c[50]),
                                TerbilangAmtUpahDeduction = Sm.Terbilang(Sm.DrDec(dr, c[51])),
                                TerbilangAmtUpahDeductionDec = Sm.DrDec(dr, c[51]),
                                EmpCodeSincerely = Sm.DrStr(dr, c[52]),
                                TotalAmtAllowance = Sm.DrDec(dr, c[53]),
                                PosName = Sm.DrStr(dr, c[54]),
                                SectionCode = Sm.DrStr(dr, c[55]),
                                SectionName = Sm.DrStr(dr, c[56]),
                                UTD = Sm.DrDec(dr, c[57]),
                                ADleave = Sm.DrDec(dr, c[58]),
                                Signature = Sm.GetValue("Select Concat(IfNull(B.ParValue, ''), A.Parvalue, '.JPG') " +
                                            "From tblparameter A " +
                                            "Inner Join tblparameter B On 0=0  " +
                                            "Where A.ParCode = 'EmpCodeSincerely' And B.parCode = 'ImgFileSignature'"),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                // UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region EmpAllowance
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                decimal no = 0;
                string EmpCode = string.Empty;
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;


                    SQLDtl.AppendLine("Select Z.PayrunCode, Z.EmpCode, Z.AdCode, UPPER(Z.AdName) AdName,  ifnull(Z1.Amt, ifnull(Z2.Amt,0)) As Amt, ");
                    SQLDtl.AppendLine("(Z3.FixAllowance+Z3.Meal+Z3.Transport+Z3.variableallowance) As totalAmt ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("    Select A.PayrunCode,  A.EmpCode, B.AdCode, B.Adname ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join TblAllowanceDeduction B On 0=0  ");
                    SQLDtl.AppendLine("    Where A.PayrunCode = @PayrunCode And B.Adtype = 'A' And B.AdCode <> '019' ");
                    SQLDtl.AppendLine("    )Z ");
                    SQLDtl.AppendLine("left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("    Select A.payrunCode, A.EmpCode, B.AdCode, B.Amt  ");
                    SQLDtl.AppendLine("    From tblpayrollprocess1 A ");
                    SQLDtl.AppendLine("    inner Join tblpayrollProcessAD B on A.PayrunCOde= B.payruNCode  And A.EmpCode = B.EmpCode ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine(")Z1 On  Z.payrunCode = Z1.payrunCode  And Z.EmpCode = Z1.EmpCode And Z.AdCode = Z1.AdCode ");
                    SQLDtl.AppendLine("Left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue as AdCode, A.Meal As Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join Tblparameter B On parCode = 'ADCodeMeal' ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine("    Union All ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue as AdCode, A.Transport As Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join Tblparameter B On parCode = 'ADCodeTransport' ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine("    Union ALl ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue as AdCode, A.VariableAllowance As Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join Tblparameter B On parCode = 'ADCodeVariable' ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine("    Union ALl ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, A.AdCode, A.Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcessAd A ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode  ");
                    SQLDtl.AppendLine("    And find_in_set(A.AdCode, (select parvalue from tblparameter Where parCode ='AllowanceCodeVariable'))>0  ");
                    SQLDtl.AppendLine(")Z2 On Z.payrunCode = Z2.payrunCode  And Z.EmpCode = Z2.EmpCode And Z.AdCode = Z2.AdCode ");
                    SQLDtl.AppendLine("Inner jOin TblPayrollProcess1 Z3 On Z.payrunCode = Z3.payrunCode And Z.EmpCode = Z3.EmpCode  ");
                    SQLDtl.AppendLine("Order By Z.PayrunCode, Z.EmpCode, Z.AdCode  ");



                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADCode",
                     "ADName",
                     "Amt",
                     "TotalAmt",
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            if (Sm.DrStr(drDtl, cDtl[1]) != EmpCode)
                            {
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]);
                                no = 0;
                            }
                            else
                            {
                                no = no + 1;
                            }
                            ldtl.Add(new EmpAllowance()
                            {
                                No = no,
                                PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                                ADCode = Sm.DrStr(drDtl, cDtl[2]),
                                ADName = Sm.DrStr(drDtl, cDtl[3]),
                                Amt = Sm.DrDec(drDtl, cDtl[4]),
                                TotalAmt = Sm.DrDec(drDtl, cDtl[5]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(drDtl, cDtl[5])),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region OTAllowance
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select A.PayrunCode, A.Empcode, UPPER(B.ADName) ADName, Sum(A.Amt)As Amt, Sum(A.Duration)As Duration ");
                    SQLDtl2.AppendLine("From TblPayrollProcessADOT A ");
                    SQLDtl2.AppendLine("Left Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
                    SQLDtl2.AppendLine("Where A.PayrunCode=@PayrunCode ");
                    SQLDtl2.AppendLine("Group By A.Empcode, B.AdName ");
                    SQLDtl2.AppendLine("Order By B.ADName ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@PayrunCode", Payrun);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-4
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "Duration"

                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new OTAllowance()
                            {
                                PayrunCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                ADName = Sm.DrStr(drDtl2, cDtl2[2]),
                                Amt = Sm.DrDec(drDtl2, cDtl2[3]),
                                Duration = Sm.DrDec(drDtl2, cDtl2[4]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region EmpDeduction
                var cmDtl3 = new MySqlCommand();
                var SQLDtl3 = new StringBuilder();
                decimal no2 = 0;
                //decimal no = 0;
                //string EmpCode = string.Empty;
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;


                    SQLDtl3.AppendLine("Select A.PayrunCode, A.AmtUpah,  A.EmpCode, A.CreditCode As ADCode, UPPER(A.Creditname) As ADName, ifnull(B.Amt, ifnull(C.Amt,0)) As Amt   ");
                    SQLDtl3.AppendLine("From   ");
                    SQLDtl3.AppendLine("(  ");
                    SQLDtl3.AppendLine("    Select A.Amt As AmtUpah, A.PayrunCode, A.EmpCode, B.CreditCode, B.Creditname, 0 as sequence   ");
                    SQLDtl3.AppendLine("    From tblpayrollprocess1 A  ");
                    SQLDtl3.AppendLine("    Inner Join TblCredit B On 0=0 And B.ActInd = 'Y' ");
                    SQLDtl3.AppendLine("    Where payrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    union ALl  ");
                    SQLDtl3.AppendLine("    Select A.Amt, A.PayrunCode,  A.EmpCode, B.AdCode, B.Adname, B.Sequence   ");
                    SQLDtl3.AppendLine("    From TblpayrollProcess1 A   ");
                    SQLDtl3.AppendLine("    Inner Join TblAllowanceDeduction B On 0=0   ");
                    SQLDtl3.AppendLine("    Where A.PayrunCode = @PayrunCode And B.Adtype = 'D'   ");
                    SQLDtl3.AppendLine(")A  ");
                    SQLDtl3.AppendLine("left Join  ");
                    SQLDtl3.AppendLine("(  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode1 As CreditCode, EmpCode, ifnull(CreditAdvancepayment1, 0) Amt From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    Union ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode2, EmpCode, ifnull(CreditAdvancepayment2, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    Union ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode3, EmpCode, ifnull(CreditAdvancepayment3, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    UNION ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode4, EmpCode, ifnull(CreditAdvancepayment4, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    UNION ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode5, EmpCode, ifnull(CreditAdvancepayment5, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    UNION ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode6, EmpCode, ifnull(CreditAdvancepayment6, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    Union ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode7, EmpCode, ifnull(CreditAdvancepayment7, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    Union ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode8, EmpCode, ifnull(CreditAdvancepayment8, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    UNION ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode9, EmpCode, ifnull(CreditAdvancepayment9, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine("    UNION ALL  ");
                    SQLDtl3.AppendLine("    select PayrunCode, CreditCode10, EmpCode, ifnull(CreditAdvancepayment10, 0) From Tblpayrollprocess1  ");
                    SQLDtl3.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl3.AppendLine(")B On A.PayrunCode = B.payrunCode And A.EmpCode = B.EmpCode And A.CreditCode = B.CreditCode  ");
                    SQLDtl3.AppendLine("left Join    ");
                    SQLDtl3.AppendLine("(   ");
                    SQLDtl3.AppendLine("    Select A.payrunCode, A.EmpCode, B.AdCode, B.Amt    ");
                    SQLDtl3.AppendLine("    From tblpayrollprocess1 A   ");
                    SQLDtl3.AppendLine("    inner Join tblpayrollProcessAD B on A.PayrunCOde= B.payruNCode  And A.EmpCode = B.EmpCode   ");
                    SQLDtl3.AppendLine("    Where A.payrunCode = @payrunCode   ");
                    SQLDtl3.AppendLine(")C On  A.payrunCode = C.payrunCode  And A.EmpCode = C.EmpCode And A.CreditCode = C.AdCode   ");
                    SQLDtl3.AppendLine("Where A.CreditCode not in ('010') ");
                    SQLDtl3.AppendLine("Order By A.PayrunCode, A.EmpCode, CAST(A.Sequence as UNSIGNED) ");

                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@PayrunCode", Payrun);

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADCode",
                     "ADName",
                     "Amt",
                     "AmtUpah"
                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            if (Sm.DrStr(drDtl3, cDtl3[1]) != EmpCode)
                            {
                                EmpCode = Sm.DrStr(drDtl3, cDtl3[1]);
                                no2 = 0;
                            }
                            else
                            {
                                no2 = no2 + 1;
                            }
                            ldtl3.Add(new EmpDeduction()
                            {
                                No = no2,
                                PayrunCode = Sm.DrStr(drDtl3, cDtl3[0]),
                                EmpCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                ADCode = Sm.DrStr(drDtl3, cDtl3[2]),
                                ADName = Sm.DrStr(drDtl3, cDtl3[3]),
                                Amt = Sm.DrDec(drDtl3, cDtl3[4]),
                                AmtUpah = Sm.DrDec(drDtl3, cDtl3[5]),
                                //TotalPOT = TotalPOT+Sm.DrDec(drDtl3, cDtl3[4]),
                                Terbilang2 = Sm.Terbilang(Sm.DrDec(drDtl3, cDtl3[5]) - Sm.DrDec(drDtl3, cDtl3[4])),
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion

            #region VIR
            if (Doctitle == "VIR")
            {
                var l3 = new List<PaySlipVIR>();
                //var ldtl = new List<EmpFixedAllowanceVIR>();
                //var ldtl2 = new List<EmpNotFixedAllowanceVIR>();
                var ldtl3 = new List<ADCodeCompanyAllowance>();
                var ldtl4 = new List<ADCodePersonalAllowance>();
                string mADCodeFixedAllowance = string.Empty;
                string mADCodeNotFixedAllowance = string.Empty;
                string mADCodePersonalAllowance = string.Empty;
                string mADCodeCompanyAllowance = string.Empty;
                string[] TableName = { "PaySlipVIR", /*"EmpFixedAllowanceVIR", "EmpNotFixedAllowanceVIR",*/ "ADCodeCompanyAllowance", "ADCodePersonalAllowance" };
                List<IList> myLists = new List<IList>();
                var cm3 = new MySqlCommand();
              
                #region Header

                var SQL3 = new StringBuilder();
                mADCodeFixedAllowance = Sm.GetParameter("ADCodeFixedAllowance");
                mADCodeNotFixedAllowance = Sm.GetParameter("ADCodeNotFixedAllowance");
                mADCodePersonalAllowance = Sm.GetParameter("ADCodePersonalAllowance");
                mADCodeCompanyAllowance = Sm.GetParameter("ADCodeCompanyAllowance");
                SQL3.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1') As Company, ");
                SQL3.AppendLine("A.PayrunCode, Date_Format(Concat(Left(A.PayrunCode,6),'00'),'%M %Y')As Period, ");
                SQL3.AppendLine("A.EmpCode, C.EmpName, A.NPWP,  "); //P.Amt as TunjPerusahaan, Q.Amt as TunjPribadi, ");
                SQL3.AppendLine("D.PosName, A.Salary, E.SiteName as UnitKerja, C1.OptDesc As Status, C2.OptCode AS Keluarga, E.Remark,  0  as Jumlah, "); //(A.Salary + P.Amt + Q.Amt) As Jumlah, ");
                SQL3.AppendLine("(A.Salary+A.FixAllowance+A.SSEmployerHealth+A.SSEmployerEmployment+A.SSErPension+A.SSEmployerPension+A.SSEmployerPension2+A.TaxAllowance+A.SalaryAdjustment) as Bruto, ");
                SQL3.AppendLine("(A.SSEmployerEmployment) As BPJSKetenagakerjaan,  ");
                SQL3.AppendLine("A.SSEmployerHealth As BPJSKesehatanPerusahaan, ");
                SQL3.AppendLine("A.SSEmployerPension2  As TunjAsuransi, ");
                SQL3.AppendLine("A.SSEmployerPension As DPLK, ");
                SQL3.AppendLine("A.TaxAllowance As TunjPajak, ");
                SQL3.AppendLine("A.Tax As Pph21, ");
                SQL3.AppendLine("SSEmployeeEmployment As BPJSKetenagakerjaan1,A.FixDeduction,  ");
                SQL3.AppendLine("A.SSEmployeeHealth As BPJSKesehatanPegawai, ");
                SQL3.AppendLine("F.CreditName, G.CreditName As CreditName2, H.CreditName As CreditName3, I.CreditName As CreditName4, J.CreditName As CreditName5, ");
                SQL3.AppendLine("K.CreditName As CreditName6, L.CreditName As CreditName7, M.CreditName As CreditName8, N.CreditName As CreditName9, O.CreditName As CreditName10, ");
                SQL3.AppendLine("A.CreditAdvancePayment1, A.CreditAdvancePayment2, A.CreditAdvancePayment3, A.CreditAdvancePayment4, A.CreditAdvancePayment5, ");
                SQL3.AppendLine("A.CreditAdvancePayment6, A.CreditAdvancePayment7, A.CreditAdvancePayment8, A.CreditAdvancePayment9, A.CreditAdvancePayment10, ");
                SQL3.AppendLine("A.Amt as PenerimaanBersih,   ");
                SQL3.AppendLine("P.TunjTransport, P.TunjKinerja, P.TunjProyek , P.TunjZona, P.TunjKeahlian, P.SalaryAdjustment, P.Lembur, Q.TunjPerum, Q.TunjJabatan, R.TunjPribadi, S.TunjPerusahaan, O.AngsuranPinjaman, O.IuranKaryawan, O.TabunganQurban, A.OTAmtAdjustment ");
                SQL3.AppendLine("from TblPayrollProcess1 A ");
                SQL3.AppendLine("Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL3.AppendLine("Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL3.AppendLine(" Left Join tbloption C1 On C.EmploymentStatus=C1.OptCode AND C1.OptCat = 'EmploymentStatus' ");
                SQL3.AppendLine(" Left Join tbloption C2 On C.PTKP=C2.OptCode AND C2.OptCat= 'NonTaxableIncome' ");
                SQL3.AppendLine("Left Join tblPosition D On C.PosCode=D.PosCode ");
                SQL3.AppendLine("Left Join tblSite E On B.SiteCode=E.SiteCode ");
                SQL3.AppendLine("Left Join tblcredit F On A.CreditCode1=F.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit G On A.CreditCode2=G.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit H On A.CreditCode3=H.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit I On A.CreditCode4=I.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit J On A.CreditCode5=J.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit K On A.CreditCode6=K.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit L On A.CreditCode7=L.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit M On A.CreditCode8=M.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit N On A.CreditCode9=N.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit O On A.CreditCode10=O.CreditCode ");

                #region Old Code
                /* SQL3.AppendLine(" INNER JOIN ");
                 SQL3.AppendLine("(Select X.amt, X.empcode, X.payruncode, sum(X.amt) as TotalFixed ");
                 SQL3.AppendLine(" From tblpayrollprocessad X ");
                 SQL3.AppendLine("Left Join tblallowancededuction Y ");
                 SQL3.AppendLine("On X.AdCode=Y.AdCode AND Y.AmtType='1' and X.PayrunCode =@PayrunCode  ");
                 SQL3.AppendLine(" Where  Find_In_Set (Y.AdCode,'" + mADCodeFixedAllowance + "') and PayrunCode =@PayrunCode   group by empcode,payruncode ");
                 SQL3.AppendLine(")T2 ON T2.Payruncode=A.PayrunCode and T2.Empcode=A.Empcode     ");
                 SQL3.AppendLine("Inner Join ");
                 SQL3.AppendLine("( ");
                 SQL3.AppendLine("Select X.PayrunCode, X.EmpCode, Sum(X.Amt) As TotalNotFixed ");
                 SQL3.AppendLine("From ( ");
                 SQL3.AppendLine("Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
                 SQL3.AppendLine("From tblpayrollprocessad A ");
                 SQL3.AppendLine("Inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                 SQL3.AppendLine("Where Find_In_Set (C.AdCode,'" + mADCodeNotFixedAllowance + "') ");
                 SQL3.AppendLine("Union All ");
                 SQL3.AppendLine("Select PayrunCode , 'OT1' AS ADCode, EmpCode, 'Lembur 1' AS AdName,'A' AS AdType, OT1Amt AS Amt");
                 SQL3.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode ");
                 SQL3.AppendLine("Union All ");
                 SQL3.AppendLine("Select PayrunCode , 'OT2' AS ADCode, EmpCode, 'Lembur 2' AS AdName,'A' AS AdType, OT2Amt AS Amt ");
                 SQL3.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
                 SQL3.AppendLine("Union All ");
                 SQL3.AppendLine("Select PayrunCode , 'OT3' AS ADCode, EmpCode, 'Lembur 3' AS AdName,'A' AS AdType, OT3Amt AS Amt ");
                 SQL3.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
                 SQL3.AppendLine("Union All ");
                 SQL3.AppendLine("Select PayrunCode , 'OTHoliday1' AS ADCode, EmpCode, 'Lembur Libur 1' AS AdName,'A' AS AdType, OTHoliday1Amt AS Amt ");
                 SQL3.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
                 SQL3.AppendLine("Union All ");
                 SQL3.AppendLine("Select PayrunCode , 'OTHoliday2' AS ADCode, EmpCode, 'Lembur Libur 2' AS AdName,'A' AS AdType, OTHoliday2Amt AS Amt ");
                 SQL3.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
                 SQL3.AppendLine("Union All ");
                 SQL3.AppendLine("Select PayrunCode , 'OTHoliday3' AS ADCode, EmpCode, 'Lembur Libur 3' AS AdName,'A' AS AdType, OTHoliday3Amt AS Amt ");
                 SQL3.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
                 SQL3.AppendLine("Union All ");
                 SQL3.AppendLine("Select PayrunCode , 'Transport' AS ADCode, EmpCode, 'Tunjangan Transport' AS AdName,'A' AS AdType, Transport AS Amt ");
                 SQL3.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode ");
                 //SQL3.AppendLine(" Select empcode, payruncode, sum(amt) as TotalNotFixed from tblpayrollprocessad X ");
                 //SQL3.AppendLine(" Inner Join tblallowancededuction Y ON X.AdCode=Y.AdCode AND Y.AmtType='1' and X.PayrunCode =@PayrunCode ");
                 //SQL3.AppendLine("Where Find_In_Set (Y.AdCode,'" + mADCodeNotFixedAllowance + "') and PayrunCode =@PayrunCode  group by empcode ");
                 SQL3.AppendLine(")X ");
                 SQL3.AppendLine("Group By X.EmpCode ");
                 SQL3.AppendLine(")T1 On T1.Payruncode=A.PayrunCode and T1.Empcode=A.Empcode ");
                 * * */
                #endregion

                SQL3.AppendLine("Left Join ( ");
                SQL3.AppendLine(" Select A.PayrunCode, A.EmpCode, SUM(T.Amt) As TunjTransport,  ");
                SQL3.AppendLine("  SUM(T2.Amt) As TunjKinerja, SUM(T3.Amt) As TunjProyek , SUM(T4.Amt) As TunjZona, SUM(T5.Amt) As TunjKeahlian, T6.SalaryAdjustment, T7.Lembur ");
                SQL3.AppendLine(" From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode  ");
                SQL3.AppendLine(" Left Join  ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '005' ");
                SQL3.AppendLine(" ) T On T.Payruncode=A.PayrunCode and T.Empcode=A.Empcode And A.AdCode = T.AdCode ");
                SQL3.AppendLine(" Left Join  ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '007' ");
                SQL3.AppendLine(" ) T2 On T2.Payruncode=A.PayrunCode and T2.Empcode=A.Empcode And A.AdCode = T2.AdCode 	 ");
                SQL3.AppendLine(" Left Join  ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Left Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = 'TP' ");
                SQL3.AppendLine(" ) T3 On T3.Payruncode=A.PayrunCode and T3.Empcode=A.Empcode And A.AdCode = T3.AdCode ");
                SQL3.AppendLine(" Left Join  ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Left Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '006' ");
                SQL3.AppendLine(" ) T4 On T4.Payruncode=A.PayrunCode and T4.Empcode=A.Empcode And A.AdCode = T4.AdCode ");
                SQL3.AppendLine(" Left Join   ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Left Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = 'TK' ");
                SQL3.AppendLine(" ) T5 On T5.Payruncode=A.PayrunCode and T5.Empcode=A.Empcode And A.AdCode = T5.AdCode  ");
                SQL3.AppendLine(" Left Join (  ");
                SQL3.AppendLine("     Select X.EmpCode, Sum(X.Amt) As SalaryAdjustment, X.PayrunCode ");
                SQL3.AppendLine("              From (  ");
                SQL3.AppendLine("                   Select A.EmpCode, A.Amt, A.PayrunCode ");
                SQL3.AppendLine("                   From TblSalaryAdjustmentHdr A ");
                SQL3.AppendLine("                   Where A.CancelInd='N'  ");
                SQL3.AppendLine("                   Union All  ");
                SQL3.AppendLine("                   Select B.EmpCode, A.Amt, B.PayrunCode  ");
                SQL3.AppendLine("                   From TblSalaryAdjustment2Hdr A ");
                SQL3.AppendLine("                   Inner Join TblSalaryAdjustment2Dtl B ");
                SQL3.AppendLine("                       On A.DocNo=B.DocNo  ");
                SQL3.AppendLine("                   And A.CancelInd='N'  ");
                SQL3.AppendLine("               ) X Group By X.EmpCode  ");
                SQL3.AppendLine("           ) T6 On T6.Payruncode=A.PayrunCode and T6.Empcode=A.Empcode  ");

                SQL3.AppendLine(" Left Join ( ");
                SQL3.AppendLine("     Select X.EmpCode, Sum(X.Amt) As Lembur ,X.Payruncode ");
                SQL3.AppendLine("     From ( ");
                SQL3.AppendLine("         Select  EmpCode, OT1Amt AS Amt, PayrunCode");
                SQL3.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
                SQL3.AppendLine("         Union All ");
                SQL3.AppendLine("         Select  EmpCode, OT2Amt AS Amt, PayrunCode ");
                SQL3.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
                SQL3.AppendLine("         Union All ");
                SQL3.AppendLine("         Select EmpCode, OT3Amt AS Amt, PayrunCode ");
                SQL3.AppendLine("        From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
                SQL3.AppendLine("         Union All ");
                SQL3.AppendLine("         Select EmpCode, OTHoliday1Amt AS Amt, PayrunCode ");
                SQL3.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
                SQL3.AppendLine("         Union All ");
                SQL3.AppendLine("         Select EmpCode, OTHoliday2Amt AS Amt, PayrunCode ");
                SQL3.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
                SQL3.AppendLine("         Union All ");
                SQL3.AppendLine("         Select EmpCode, OTHoliday3Amt AS Amt, PayrunCode ");
                SQL3.AppendLine("        From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
                SQL3.AppendLine("         )X Group BY X.payrunCode, X.EmpCode ");
                SQL3.AppendLine(" )T7 On T7.Payruncode=A.PayrunCode and T7.Empcode=A.Empcode ");
                SQL3.AppendLine(" Group by A.empcode, A.PayrunCode ");
                SQL3.AppendLine(")P On A.PayrunCode = P.PayrunCode And A.EmpCode = P.EmpCode ");
                SQL3.AppendLine("Left Join ( ");
                SQL3.AppendLine(" Select A.PayrunCode, A.EmpCode, Sum(T.Amt) As TunjPerum, Sum(T2.Amt) As TunjJabatan ");
                SQL3.AppendLine(" From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                SQL3.AppendLine(" Left Join ");
                SQL3.AppendLine(" (Select A.PayrunCode,A.ADCode, A.EmpCode, A.Amt From TblPayrollProcessAd A ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '003' ");
                SQL3.AppendLine(" ) T On T.Payruncode=A.PayrunCode and T.Empcode=A.Empcode And A.AdCode = T.AdCode  ");
                SQL3.AppendLine(" Left Join ");
                SQL3.AppendLine(" (Select A.PayrunCode,A.ADCode, A.EmpCode, A.Amt From TblPayrollProcessAd A ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' Where C.ADCode = '004') ");
                SQL3.AppendLine(" T2 On T2.Payruncode=A.PayrunCode and T2.Empcode=A.Empcode And A.AdCode = T2.AdCode  ");
                SQL3.AppendLine(" Group BY A.payrunCode, A.EmpCode	 ");
                SQL3.AppendLine(")Q On A.PayrunCode = Q.PayrunCode And A.EmpCode = Q.EmpCode ");
                SQL3.AppendLine(" Left Join ");
                SQL3.AppendLine("     (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt As TunjPribadi ");
                SQL3.AppendLine("     From tblpayrollprocessad A ");
                SQL3.AppendLine("    inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                SQL3.AppendLine("    Where C.Adcode In ('" + mADCodePersonalAllowance + "') ");
                SQL3.AppendLine(")R On A.PayrunCode = R.PayrunCode And A.EmpCode = R.EmpCode ");
                SQL3.AppendLine(" Left Join ");
                SQL3.AppendLine("     (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt As TunjPerusahaan ");
                SQL3.AppendLine("     From tblpayrollprocessad A ");
                SQL3.AppendLine("    inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                SQL3.AppendLine("    Where C.Adcode In ('" + mADCodeCompanyAllowance + "') ");
                SQL3.AppendLine(")S On A.PayrunCode = S.PayrunCode And A.EmpCode = S.EmpCode ");
                SQL3.AppendLine("Left Join ( ");
                SQL3.AppendLine(" Select A.PayrunCode, A.EmpCode, SUM(T.Amt) As AngsuranPinjaman,  ");
                SQL3.AppendLine("  SUM(T2.Amt) As IuranKaryawan, SUM(T3.Amt) As TabunganQurban  ");
                SQL3.AppendLine(" From TblPayrollProcessAd A   ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode   ");
                SQL3.AppendLine(" Left Join   ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '009'  ");
                SQL3.AppendLine(" ) T On T.Payruncode=A.PayrunCode and T.Empcode=A.Empcode And A.AdCode = T.AdCode  ");
                SQL3.AppendLine(" Left Join   ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  ");
                SQL3.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '008'  ");
                SQL3.AppendLine(" ) T2 On T2.Payruncode=A.PayrunCode and T2.Empcode=A.Empcode And A.AdCode = T2.AdCode 	  ");
                SQL3.AppendLine(" Left Join   ");
                SQL3.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A   ");
                SQL3.AppendLine(" Left Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '012'  ");
                SQL3.AppendLine(" ) T3 On T3.Payruncode=A.PayrunCode and T3.Empcode=A.Empcode And A.AdCode = T3.AdCode  ");
                SQL3.AppendLine(" Group by A.empcode, A.PayrunCode ");

                SQL3.AppendLine(")O On A.PayrunCode = O.PayrunCode And A.EmpCode = O.EmpCode ");
               
                SQL3.AppendLine("Where A.PayrunCode=@PayrunCode ");


                

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@PayrunCode", Payrun);
                    Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                  //if (Sm.GetLue(LueAGCode).Length > 0)
                    //    Sm.CmParam<String>(ref cm3, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[]
                {
                    // 0
                    "CompanyLogo",

                    //1-5
                    "Period",
                    "EmpCode",
                    "EmpName",
                    "PosName",
                    "Status",

                    //6-10
                    "Keluarga",
                    "NPWP",
                    "UnitKerja",
                    "Salary",
                    "Jumlah",

                    //11-15
                    "BPJSKetenagakerjaan",
                    "BPJSKesehatanPerusahaan",
                    "TunjAsuransi",
                    "DPLK",
                    "TunjPajak",

                    //16-20
                    "Pph21",
                    "BPJSKetenagakerjaan1",
                    "BPJSKesehatanPegawai",
                    "CreditName",
                    "CreditName2",

                    //21-25             
                    "CreditName3",
                    "CreditName4",
                    "CreditName5",
                    "CreditName6",
                    "CreditName7",


                    //26-30
                    "CreditName8",
                    "CreditName9",
                    "CreditName10",
                    "CreditAdvancePayment1",
                    "CreditAdvancePayment2",

                    //31-35                 
                    "CreditAdvancePayment3",
                    "CreditAdvancePayment4",
                    "CreditAdvancePayment5",
                    "CreditAdvancePayment6",
                    "CreditAdvancePayment7",

                    //36-40              
                    "CreditAdvancePayment8",
                    "CreditAdvancePayment9",
                    "CreditAdvancePayment10",
                    "PenerimaanBersih",
                    "Company",

                    //41-45
                    "PayrunCode",
                    "Remark",
                    "Bruto",
                    "TunjPerum",
                    "TunjJabatan",

                    //46-50
                    "TunjTransport",
                    "TunjKinerja",
                    "TunjProyek",
                    "TunjZona",
                    "TunjKeahlian",

                    //51-55
                    "SalaryAdjustment",
                    "Lembur",
                    "TunjPribadi",
                    "TunjPerusahaan",
                    "FixDeduction",

                    //56-59
                    "AngsuranPinjaman",
                    "IuranKaryawan",
                    "TabunganQurban",
                    "OTAmtAdjustment"
                });

                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            l3.Add(new PaySlipVIR()
                            {
                                CompanyLogo = Sm.DrStr(dr3, c3[0]),

                                Period = Sm.DrStr(dr3, c3[1]),
                                EmpCode= Sm.DrStr(dr3, c3[2]),
                                EmpName = Sm.DrStr(dr3, c3[3]),
                                PosName = Sm.DrStr(dr3, c3[4]),
                                Status = Sm.DrStr(dr3, c3[5]),

                                //6-10
                                Keluarga = Sm.DrStr(dr3, c3[6]),
                                NPWP = Sm.DrStr(dr3, c3[7]),
                                UnitKerja = Sm.DrStr(dr3, c3[8]),
                                Salary= Sm.DrDec(dr3, c3[9]),
                                Jumlah = Sm.DrDec(dr3, c3[10]),

                                //11
                                BPJSKetenagakerjaan = Sm.DrDec(dr3, c3[11]),
                                BPJSKesehatanPerusahaan = Sm.DrDec(dr3, c3[12]),
                                TunjAsuransi = Sm.DrDec(dr3, c3[13]),
                                DPLK = Sm.DrDec(dr3, c3[14]),
                                TunjPajak = Sm.DrDec(dr3, c3[15]),

                                Pph21 = Sm.DrDec(dr3, c3[16]),
                                BPJSKetenagakerjaan1 = Sm.DrDec(dr3, c3[17]),
                                BPJSKesehatanPegawai = Sm.DrDec(dr3, c3[18]),
                                CreditCode1 = Sm.DrStr(dr3, c3[19]),

                                CreditCode2 = Sm.DrStr(dr3, c3[20]),
                                CreditCode3 = Sm.DrStr(dr3, c3[21]),
                                CreditCode4 = Sm.DrStr(dr3, c3[22]),
                                CreditCode5 = Sm.DrStr(dr3, c3[23]),
                                CreditCode6 = Sm.DrStr(dr3, c3[24]),

                                CreditCode7 = Sm.DrStr(dr3, c3[25]),
                                CreditCode8 = Sm.DrStr(dr3, c3[26]),
                                CreditCode9 = Sm.DrStr(dr3, c3[27]),
                                CreditCode10 = Sm.DrStr(dr3, c3[28]),
                                CreditAdvancePayment1 = Sm.DrDec(dr3, c3[29]),

                                CreditAdvancePayment2 = Sm.DrDec(dr3, c3[30]),
                                CreditAdvancePayment3 = Sm.DrDec(dr3, c3[31]),
                                CreditAdvancePayment4 = Sm.DrDec(dr3, c3[32]),
                                CreditAdvancePayment5 = Sm.DrDec(dr3, c3[33]),
                                CreditAdvancePayment6 = Sm.DrDec(dr3, c3[34]),

                                CreditAdvancePayment7 = Sm.DrDec(dr3, c3[35]),
                                CreditAdvancePayment8 = Sm.DrDec(dr3, c3[36]),
                                CreditAdvancePayment9 = Sm.DrDec(dr3, c3[37]),
                                CreditAdvancePayment10 = Sm.DrDec(dr3, c3[38]),

                                PenerimaanBersih = Sm.DrDec(dr3, c3[39]),
                                PayrunCode = Sm.DrStr(dr3, c3[40]),
                                Company = Sm.DrStr(dr3, c3[41]),
                                Remark = Sm.DrStr(dr3, c3[42]),
                               // TotalFixed = Sm.DrDec(dr3, c3[43]),
                                //TotalNotFixed = Sm.DrDec(dr3, c3[44]),
                                Bruto = Sm.DrDec(dr3, c3[43]),

                                TunjPerum = Sm.DrDec(dr3, c3[44]),
                                TunjJabatan = Sm.DrDec(dr3, c3[45]),
                                Upah = Sm.DrDec(dr3, c3[44]) + Sm.DrDec(dr3, c3[45]),
                                TunjTransport = Sm.DrDec(dr3, c3[46]),
                                TunjKinerja  = Sm.DrDec(dr3, c3[47]),

                                TunjProyek  = Sm.DrDec(dr3, c3[48]),
                                TunjZona = Sm.DrDec(dr3, c3[49]),
                                TunjKeahlian = Sm.DrDec(dr3, c3[50]),
                                KekuranganGaji = Sm.DrDec(dr3, c3[51]),
                                Lembur = Sm.DrDec(dr3, c3[52]),
                                
                                Subtotal = Sm.DrDec(dr3, c3[46]) + Sm.DrDec(dr3, c3[47]) + Sm.DrDec(dr3, c3[48]) + Sm.DrDec(dr3, c3[49])
                                + Sm.DrDec(dr3, c3[50]) + Sm.DrDec(dr3, c3[51]) + Sm.DrDec(dr3, c3[52]),
                                TunjPribadi = Sm.DrDec(dr3, c3[53]),
                                TunjPerusahaan = Sm.DrDec(dr3, c3[54]),
                                FixDeduction = Sm.DrDec(dr3, c3[55]),
                                AngsuranPinjaman = Sm.DrDec(dr3, c3[56]),
                               
                                IuranKaryawan = Sm.DrDec(dr3, c3[57]),
                                TabunganQurban = Sm.DrDec(dr3, c3[58]),
                                OTAmtAdjustment = Sm.DrDec(dr3, c3[59]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(l3);

                #endregion

                #region Old Code
//                #region EmpFixedAllowanceVIR
//                var cmDtl = new MySqlCommand();
//                var SQLDtl = new StringBuilder();
//           //     string mADCodeFixedAllowance = string.Empty;
              
//                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
//                {
//                    cnDtl.Open();
//                    cmDtl.Connection = cnDtl;

//                    #region Old Code
//                    /*mADCodeFixedAllowance = Sm.GetParameter("ADCodeFixedAllowance");
//                    SQLDtl.AppendLine("Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
//                    SQLDtl.AppendLine("From tblpayrollprocessad A ");
//                    SQLDtl.AppendLine("inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");      
//                    //  SQLDtl.AppendLine("Order By C.AdType, C.AdName ");
//                    SQLDtl.AppendLine("Where Find_In_Set (C.AdCode,'" + mADCodeFixedAllowance + "') ");*/
//                    #endregion

//                    SQLDtl.AppendLine(" Select A.PayrunCode,A.ADCode, A.EmpCode, C.AdType, Sum(T.Amt) As TunjPerum, Sum(T2.Amt) As TunjJabatan ");
//                   SQLDtl.AppendLine(" From TblPayrollProcessAd A  ");
//                   SQLDtl.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
//                   SQLDtl.AppendLine(" Left Join ");
//                   SQLDtl.AppendLine(" (Select A.PayrunCode,A.ADCode, A.EmpCode, A.Amt From TblPayrollProcessAd A ");
//                   SQLDtl.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '003' ");
//                   SQLDtl.AppendLine(" ) T On T.Payruncode=A.PayrunCode and T.Empcode=A.Empcode And A.AdCode = T.AdCode  ");	
//                   SQLDtl.AppendLine(" Left Join ");
//                   SQLDtl.AppendLine(" (Select A.PayrunCode,A.ADCode, A.EmpCode, A.Amt From TblPayrollProcessAd A ");
//                   SQLDtl.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' Where C.ADCode = '004') ");
//                   SQLDtl.AppendLine(" T2 On T2.Payruncode=A.PayrunCode and T2.Empcode=A.Empcode And A.AdCode = T2.AdCode  ");
//                   SQLDtl.AppendLine(" Group BY A.payrunCode, A.EmpCode	 ");

//                    cmDtl.CommandText = SQLDtl.ToString();
//                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

//                    var drDtl1 = cmDtl.ExecuteReader();
//                    var cDtl = Sm.GetOrdinal(drDtl1, new string[] 
//                    {
//                     //0
//                     "PayrunCode" ,

//                     //1-4
//                     "EmpCode" ,
//                     "ADType",
//                     "TunjPerum",
//                     "TunjJabatan"
//                    });
//                    if (drDtl1.HasRows)
//                    {
//                        while (drDtl1.Read())
//                        {
//                            ldtl.Add(new EmpFixedAllowanceVIR()
//                            {
//                                PayrunCode = Sm.DrStr(drDtl1, cDtl[0]),
//                                EmpCode = Sm.DrStr(drDtl1, cDtl[1]),
//                                ADType = Sm.DrStr(drDtl1, cDtl[2]),
//                                TunjPerum = Sm.DrDec(drDtl1, cDtl[3]),
//                                TunjJabatan = Sm.DrDec(drDtl1, cDtl[4]),
//                                Upah = Sm.DrDec(drDtl1, cDtl[3]) + Sm.DrDec(drDtl1, cDtl[4])

//                            });
//                        }
//                    }
//                    drDtl1.Close();
//                }
//                myLists.Add(ldtl);
//                #endregion

//                #region EmpNotFixedAllowanceVIR

//                var cmDtl2 = new MySqlCommand();
//                var SQLDtl2 = new StringBuilder();
//              //  string mADCodeNotFixedAllowance = string.Empty;

//                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
//                {
//                    cnDtl2.Open();
//                    cmDtl2.Connection = cnDtl2;
//                    #region Old Code
//                    /* mADCodeNotFixedAllowance = Sm.GetParameter("ADCodeNotFixedAllowance");
//                    SQLDtl2.AppendLine("Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");  
//                    SQLDtl2.AppendLine("From tblpayrollprocessad A ");
//                    SQLDtl2.AppendLine("Inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
//                    SQLDtl2.AppendLine("Where Find_In_Set (C.AdCode,'" + mADCodeNotFixedAllowance + "') ");
//                    SQLDtl2.AppendLine("Union All ");
//                    SQLDtl2.AppendLine("Select PayrunCode , 'OT1' AS ADCode, EmpCode, 'Lembur 1' AS AdName,'A' AS AdType, OT1Amt AS Amt");
//                    SQLDtl2.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode ");
//                    SQLDtl2.AppendLine("Union All ");
//                    SQLDtl2.AppendLine("Select PayrunCode , 'OT2' AS ADCode, EmpCode, 'Lembur 2' AS AdName,'A' AS AdType, OT2Amt AS Amt ");
//                    SQLDtl2.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
//                    SQLDtl2.AppendLine("Union All ");
//                    SQLDtl2.AppendLine("Select PayrunCode , 'OT3' AS ADCode, EmpCode, 'Lembur 3' AS AdName,'A' AS AdType, OT3Amt AS Amt ");
//                    SQLDtl2.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
//                    SQLDtl2.AppendLine("Union All ");
//                    SQLDtl2.AppendLine("Select PayrunCode , 'OTHoliday1' AS ADCode, EmpCode, 'Lembur Libur 1' AS AdName,'A' AS AdType, OTHoliday1Amt AS Amt ");
//                    SQLDtl2.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
//                    SQLDtl2.AppendLine("Union All ");
//                    SQLDtl2.AppendLine("Select PayrunCode , 'OTHoliday2' AS ADCode, EmpCode, 'Lembur Libur 2' AS AdName,'A' AS AdType, OTHoliday2Amt AS Amt ");
//                    SQLDtl2.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
//                    SQLDtl2.AppendLine("Union All ");
//                    SQLDtl2.AppendLine("Select PayrunCode , 'OTHoliday3' AS ADCode, EmpCode, 'Lembur Libur 3' AS AdName,'A' AS AdType, OTHoliday3Amt AS Amt ");
//                    SQLDtl2.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode  ");
//                    SQLDtl2.AppendLine("Union All ");
//                    SQLDtl2.AppendLine("Select PayrunCode , 'Transport' AS ADCode, EmpCode, 'Tunjangan Transport' AS AdName,'A' AS AdType, Transport AS Amt ");
//                    SQLDtl2.AppendLine("From tblpayrollprocess1 Where PayrunCode =@PayrunCode ");
                            
//                 //   SQLDtl.AppendLine("Order By C.AdType, C.AdName ");
//                    */
//                    #endregion
//                    SQLDtl2.AppendLine(" Select A.PayrunCode, A.AdCode, A.EmpCode, C.AdType, SUM(T.Amt) As TunjTransport,  ");
//                    SQLDtl2.AppendLine("  SUM(T2.Amt) As TunjKinerja, SUM(T3.Amt) As TunjProyek , SUM(T4.Amt) As TunjZona, SUM(T5.Amt) As TunjKeahlian, T6.SalaryAdjustment, T7.Lembur "); 
//                   SQLDtl2.AppendLine(" From TblPayrollProcessAd A  "); 
//                   SQLDtl2.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode  "); 
//                   SQLDtl2.AppendLine(" Left Join  "); 
//                   SQLDtl2.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A ");  
//                   SQLDtl2.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '005' ");
//                   SQLDtl2.AppendLine(" ) T On T.Payruncode=A.PayrunCode and T.Empcode=A.Empcode And A.AdCode = T.AdCode "); 
//                   SQLDtl2.AppendLine(" Left Join  "); 
//                   SQLDtl2.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  "); 
//                   SQLDtl2.AppendLine(" Inner Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '007' ");
//                   SQLDtl2.AppendLine(" ) T2 On T2.Payruncode=A.PayrunCode and T2.Empcode=A.Empcode And A.AdCode = T2.AdCode 	 "); 
//                   SQLDtl2.AppendLine(" Left Join  "); 
//                   SQLDtl2.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  "); 
//                   SQLDtl2.AppendLine(" Left Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = 'TP' ");
//                   SQLDtl2.AppendLine(" ) T3 On T3.Payruncode=A.PayrunCode and T3.Empcode=A.Empcode And A.AdCode = T3.AdCode "); 
//                   SQLDtl2.AppendLine(" Left Join  "); 
//                   SQLDtl2.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  "); 
//                   SQLDtl2.AppendLine(" Left Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = '006' ");
//                   SQLDtl2.AppendLine(" ) T4 On T4.Payruncode=A.PayrunCode and T4.Empcode=A.Empcode And A.AdCode = T4.AdCode "); 
//                   SQLDtl2.AppendLine(" Left Join   "); 
//                   SQLDtl2.AppendLine(" (Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt From TblPayrollProcessAd A  "); 
//                   SQLDtl2.AppendLine(" Left Join TblAllowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1'  Where C.ADCode = 'TK' ");
//                   SQLDtl2.AppendLine(" ) T5 On T5.Payruncode=A.PayrunCode and T5.Empcode=A.Empcode And A.AdCode = T5.AdCode  "); 
//                   SQLDtl2.AppendLine(" Left Join (  "); 
//                   SQLDtl2.AppendLine("     Select X.EmpCode, Sum(X.Amt) As SalaryAdjustment, X.PayrunCode "); 
//                   SQLDtl2.AppendLine("              From (  "); 
//                   SQLDtl2.AppendLine("                   Select A.EmpCode, A.Amt, A.PayrunCode "); 
//                   SQLDtl2.AppendLine("                   From TblSalaryAdjustmentHdr A "); 
//                   SQLDtl2.AppendLine("                   Where A.CancelInd='N'  "); 
//                   SQLDtl2.AppendLine("                   Union All  "); 
//                   SQLDtl2.AppendLine("                   Select B.EmpCode, A.Amt, B.PayrunCode  "); 
//                   SQLDtl2.AppendLine("                   From TblSalaryAdjustment2Hdr A "); 
//                   SQLDtl2.AppendLine("                   Inner Join TblSalaryAdjustment2Dtl B "); 
//                   SQLDtl2.AppendLine("                       On A.DocNo=B.DocNo  "); 
//                   SQLDtl2.AppendLine("                   And A.CancelInd='N'  "); 
//                   SQLDtl2.AppendLine("               ) X Group By X.EmpCode  "); 
//                   SQLDtl2.AppendLine("           ) T6 On T6.Payruncode=A.PayrunCode and T6.Empcode=A.Empcode  "); 

//                   SQLDtl2.AppendLine(" Left Join ( ");
//                   SQLDtl2.AppendLine("     Select X.EmpCode, Sum(X.Amt) As Lembur ,X.Payruncode ");
//                   SQLDtl2.AppendLine("     From ( ");
//                   SQLDtl2.AppendLine("         Select  EmpCode, OT1Amt AS Amt, PayrunCode");
//                   SQLDtl2.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
//                   SQLDtl2.AppendLine("         Union All ");
//                   SQLDtl2.AppendLine("         Select  EmpCode, OT2Amt AS Amt, PayrunCode ");
//                   SQLDtl2.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
//                   SQLDtl2.AppendLine("         Union All ");
//                   SQLDtl2.AppendLine("         Select EmpCode, OT3Amt AS Amt, PayrunCode ");
//                   SQLDtl2.AppendLine("        From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
//                   SQLDtl2.AppendLine("         Union All ");
//                   SQLDtl2.AppendLine("         Select EmpCode, OTHoliday1Amt AS Amt, PayrunCode ");
//                   SQLDtl2.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
//                   SQLDtl2.AppendLine("         Union All ");
//                   SQLDtl2.AppendLine("         Select EmpCode, OTHoliday2Amt AS Amt, PayrunCode ");
//                   SQLDtl2.AppendLine("         From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
//                   SQLDtl2.AppendLine("         Union All ");
//                   SQLDtl2.AppendLine("         Select EmpCode, OTHoliday3Amt AS Amt, PayrunCode ");
//                   SQLDtl2.AppendLine("        From tblpayrollprocess1 where PayrunCode =@PayrunCode ");
//                   SQLDtl2.AppendLine("         )X Group BY X.payrunCode, X.EmpCode ");
//                   SQLDtl2.AppendLine(" )T7 On T7.Payruncode=A.PayrunCode and T7.Empcode=A.Empcode ");
//                   SQLDtl2.AppendLine(" Group by A.empcode");

//                    cmDtl2.CommandText = SQLDtl2.ToString();

//                    Sm.CmParam<String>(ref cmDtl2, "@PayrunCode", Payrun);

//                    var drDtl2 = cmDtl2.ExecuteReader();
//                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
//                    {
//                     //0
//                     "PayrunCode" ,

//                     //1-5
//                     "EmpCode" ,
//                     "ADType",
//                     "TunjTransport",
//                     "TunjKinerja",
//                     "TunjProyek",

//                     //6-9
//                     "TunjZona",
//                     "TunjKeahlian",
//                     "SalaryAdjustment",
//                     "Lembur"
                     


//                    });
//                    if (drDtl2.HasRows)
//                    {
//                        while (drDtl2.Read())
//                        {
//                            ldtl2.Add(new EmpNotFixedAllowanceVIR()
//                            {
//                                PayrunCode = Sm.DrStr(drDtl2, cDtl2[0]),
//                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
//                                ADType = Sm.DrStr(drDtl2, cDtl2[2]),
//                                TunjTransport = Sm.DrDec(drDtl2, cDtl2[3]),
//                                TunjKinerja  = Sm.DrDec(drDtl2, cDtl2[4]),
//                                TunjProyek  = Sm.DrDec(drDtl2, cDtl2[5]),
//                                TunjZona = Sm.DrDec(drDtl2, cDtl2[6]),
//                                TunjKeahlian = Sm.DrDec(drDtl2, cDtl2[7]),
//                                KekuranganGaji = Sm.DrDec(drDtl2, cDtl2[8]),
//                                Lembur = Sm.DrDec(drDtl2, cDtl2[9]),
//                                Subtotal = Sm.DrDec(drDtl2, cDtl2[3]) + Sm.DrDec(drDtl2, cDtl2[4]) + Sm.DrDec(drDtl2, cDtl2[5]) + Sm.DrDec(drDtl2, cDtl2[6])
//                                + Sm.DrDec(drDtl2, cDtl2[7]) + Sm.DrDec(drDtl2, cDtl2[8]) + Sm.DrDec(drDtl2, cDtl2[9])
//,
//                            });
//                        }
//                    }
//                    drDtl2.Close();
//                }
//                myLists.Add(ldtl2);
                //                #endregion
               


                //#region ADCodeCompanyAllowance
                //var cmDtl3 = new MySqlCommand();
                //var SQLDtl4 = new StringBuilder();
                //string mADCodeCompanyAllowance = string.Empty;

                //using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                //{
                //    cnDtl3.Open();
                //    cmDtl3.Connection = cnDtl3;
                //    mADCodeCompanyAllowance = Sm.GetParameter("ADCodeCompanyAllowance");
                //    SQLDtl4.AppendLine("     Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
                //    SQLDtl4.AppendLine("     From tblpayrollprocessad A ");
                //    SQLDtl4.AppendLine("    inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                //    SQLDtl4.AppendLine("    Where C.Adcode In ('" + mADCodeCompanyAllowance + "') ");

                //    cmDtl3.CommandText = SQLDtl4.ToString();

                //    Sm.CmParam<String>(ref cmDtl3, "@PayrunCode", Payrun);

                //    var drDtl3 = cmDtl3.ExecuteReader();
                //    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                //    {
                //     //0
                //     "PayrunCode" ,

                //     //1-4
                //     "EmpCode" ,
                //     "ADName",
                //     "Amt",
                //     "ADType",
      

                //    });
                //    if (drDtl3.HasRows)
                //    {
                //        while (drDtl3.Read())
                //        {
                //            ldtl3.Add(new ADCodeCompanyAllowance()
                //            {
                //                PayrunCode = Sm.DrStr(drDtl3, cDtl3[0]),
                //                EmpCode = Sm.DrStr(drDtl3, cDtl3[1]),
                //                ADName = Sm.DrStr(drDtl3, cDtl3[2]),
                //                Amt = Sm.DrDec(drDtl3, cDtl3[3]),
                //                ADType = Sm.DrStr(drDtl3, cDtl3[4]),

                //            });
                //        }
                //    }
                //    drDtl3.Close();
                //}
                //myLists.Add(ldtl3);
                //#endregion

                //#region ADCodePersonalAllowance
                //var cmDtl4 = new MySqlCommand();
                //var SQLDtl5 = new StringBuilder();
                //string mADCodePersonalAllowance = string.Empty;

                //using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                //{
                //    cnDtl4.Open();
                //    cmDtl4.Connection = cnDtl4;
                //    mADCodePersonalAllowance = Sm.GetParameter("ADCodePersonalAllowance");
                //    SQLDtl5.AppendLine("     Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
                //    SQLDtl5.AppendLine("     From tblpayrollprocessad A ");
                //    SQLDtl5.AppendLine("    inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                //    SQLDtl5.AppendLine("    Where C.Adcode In ('" + mADCodePersonalAllowance + "') ");

                //    cmDtl4.CommandText = SQLDtl5.ToString();

                //    Sm.CmParam<String>(ref cmDtl4, "@PayrunCode", Payrun);

                //    var drDtl4 = cmDtl4.ExecuteReader();
                //    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                //    {
                //     //0
                //     "PayrunCode" ,

                //     //1-4
                //     "EmpCode" ,
                //     "ADName",
                //     "Amt",
                //     "ADType",

                //    });
                //    if (drDtl4.HasRows)
                //    {
                //        while (drDtl4.Read())
                //        {
                //            ldtl4.Add(new ADCodePersonalAllowance()
                //            {
                //                PayrunCode = Sm.DrStr(drDtl4, cDtl4[0]),
                //                EmpCode = Sm.DrStr(drDtl4, cDtl4[1]),
                //                ADName = Sm.DrStr(drDtl4, cDtl4[2]),
                //                Amt = Sm.DrDec(drDtl4, cDtl4[3]),
                //                ADType = Sm.DrStr(drDtl4, cDtl4[4]),
                //            });
                //        }
                //    }
                //    drDtl4.Close();
                //}
                //myLists.Add(ldtl4);
                
                #endregion



                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion

            #region DGI
            if (Doctitle == "DGI")
            {
                var l3 = new List<PaySlipVIR>();
                var ldtl = new List<EmpFixedAllowanceDGI>();
                var ldtl2 = new List<EmpNotFixedAllowanceDGI>();
                var ldtl3 = new List<ADCodeCompanyAllowance>();
                var ldtl4 = new List<ADCodePersonalAllowance>();
                string mADCodeFixedAllowance = string.Empty;
                string mADCodeNotFixedAllowance = string.Empty;
                string[] TableName = { "PaySlipVIR", "EmpFixedAllowanceDGI", "EmpNotFixedAllowanceDGI", "ADCodeCompanyAllowance", "ADCodePersonalAllowance" };
                List<IList> myLists = new List<IList>();
                var cm3 = new MySqlCommand();

                #region Header

                var SQL3 = new StringBuilder();
                mADCodeFixedAllowance = Sm.GetParameter("ADCodeFixedAllowance");
                mADCodeNotFixedAllowance = Sm.GetParameter("ADCodeNotFixedAllowance");
                SQL3.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1') As Company, ");
                SQL3.AppendLine("A.PayrunCode, Date_Format(Concat(Left(A.PayrunCode,6),'00'),'%M %Y')As Period, T2.TotalFixed, T1.TotalNotFixed, ");
                SQL3.AppendLine("A.EmpCode, C.EmpName, A.NPWP,  "); //P.Amt as TunjPerusahaan, Q.Amt as TunjPribadi, ");
                SQL3.AppendLine("D.PosName, A.Salary, E.SiteName as UnitKerja, C1.OptDesc As Status, C2.OptCode AS Keluarga, E.Remark,  0  as Jumlah, "); //(A.Salary + P.Amt + Q.Amt) As Jumlah, ");
                SQL3.AppendLine("(A.Salary+A.FixAllowance+A.SSEmployerHealth+A.SSEmployerEmployment+A.SSErPension+A.SSEmployerPension+A.SSEmployerPension2+A.TaxAllowance+A.SalaryAdjustment) as Bruto, ");
                SQL3.AppendLine("(A.SSErlifeInsurance + ");
                SQL3.AppendLine("A. SSErworkingAccident + ");
                SQL3.AppendLine("A.SSErretirement + ");
                SQL3.AppendLine("A.SSErpension) As BPJSKetenagakerjaan,  ");
                SQL3.AppendLine("A.SSEmployerHealth As BPJSKesehatanPerusahaan, ");
                SQL3.AppendLine("A.SSEmployerPension2  As TunjAsuransi, ");
                SQL3.AppendLine("A.SSEmployerPension As DPLK, ");
                SQL3.AppendLine("A.TaxAllowance As TunjPajak, ");
                SQL3.AppendLine("A.Tax As Pph21, ");
                SQL3.AppendLine("(A.SSEepension +A.SSEeretirement) As BPJSKetenagakerjaan1, ");
                SQL3.AppendLine("A.SSEmployeeHealth As BPJSKesehatanPegawai, ");
                SQL3.AppendLine("F.CreditName, G.CreditName As CreditName2, H.CreditName As CreditName3, I.CreditName As CreditName4, J.CreditName As CreditName5, ");
                SQL3.AppendLine("K.CreditName As CreditName6, L.CreditName As CreditName7, M.CreditName As CreditName8, N.CreditName As CreditName9, O.CreditName As CreditName10, ");
                SQL3.AppendLine("A.CreditAdvancePayment1, A.CreditAdvancePayment2, A.CreditAdvancePayment3, A.CreditAdvancePayment4, A.CreditAdvancePayment5, ");
                SQL3.AppendLine("A.CreditAdvancePayment6, A.CreditAdvancePayment7, A.CreditAdvancePayment8, A.CreditAdvancePayment9, A.CreditAdvancePayment10, ");
                SQL3.AppendLine("A.Amt as PenerimaanBersih ");
                SQL3.AppendLine("from TblPayrollProcess1 A ");
                SQL3.AppendLine("Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL3.AppendLine("Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL3.AppendLine(" Left Join tbloption C1 On C.EmploymentStatus=C1.OptCode AND C1.OptCat = 'EmploymentStatus' ");
                SQL3.AppendLine(" Left Join tbloption C2 On C.PTKP=C2.OptCode AND C2.OptCat= 'NonTaxableIncome' ");
                SQL3.AppendLine("Left Join tblPosition D On C.PosCode=D.PosCode ");
                SQL3.AppendLine("Left Join tblSite E On B.SiteCode=E.SiteCode ");
                SQL3.AppendLine("Left Join tblcredit F On A.CreditCode1=F.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit G On A.CreditCode2=G.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit H On A.CreditCode3=H.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit I On A.CreditCode4=I.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit J On A.CreditCode5=J.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit K On A.CreditCode6=K.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit L On A.CreditCode7=L.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit M On A.CreditCode8=M.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit N On A.CreditCode9=N.CreditCode ");
                SQL3.AppendLine("Left Join tblcredit O On A.CreditCode10=O.CreditCode ");
                SQL3.AppendLine(" Left JOIN ");
                SQL3.AppendLine("(Select X.amt, X.empcode, X.payruncode, sum(X.amt) as TotalFixed ");
                SQL3.AppendLine(" From tblpayrollprocessad X ");
                SQL3.AppendLine("Left Join tblallowancededuction Y ");
                SQL3.AppendLine("On X.AdCode=Y.AdCode AND Y.AmtType='1' and X.PayrunCode =@PayrunCode  ");
                SQL3.AppendLine(" Where  Find_In_Set (Y.AdCode,'" + mADCodeFixedAllowance + "') and PayrunCode =@PayrunCode   group by empcode,payruncode ");
                SQL3.AppendLine(")T2 ON T2.Payruncode=A.PayrunCode and T2.Empcode=A.Empcode  ");
                SQL3.AppendLine("Left Join ");
                SQL3.AppendLine("( ");
                SQL3.AppendLine(" Select empcode, payruncode, sum(amt) as TotalNotFixed from tblpayrollprocessad X ");
                SQL3.AppendLine(" Inner Join tblallowancededuction Y ON X.AdCode=Y.AdCode AND Y.AmtType='1' and X.PayrunCode =@PayrunCode ");
                SQL3.AppendLine("Where Find_In_Set (Y.AdCode,'" + mADCodeNotFixedAllowance + "') and PayrunCode =@PayrunCode  group by empcode ");
                SQL3.AppendLine(")T1 On T1.Payruncode=A.PayrunCode and T1.Empcode=A.Empcode ");
                SQL3.AppendLine("Where A.PayrunCode=@PayrunCode ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@PayrunCode", Payrun);
                    Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                    //if (Sm.GetLue(LueAGCode).Length > 0)
                    //    Sm.CmParam<String>(ref cm3, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[]
                {
                    // 0
                    "CompanyLogo",

                    //1-5
                    "Period",
                    "EmpCode",
                    "EmpName",
                    "PosName",
                    "Status",

                    //6-10
                    "Keluarga",
                    "NPWP",
                    "UnitKerja",
                    "Salary",
                    "Jumlah",

                    //11-15
                    "BPJSKetenagakerjaan",
                    "BPJSKesehatanPerusahaan",
                    "TunjAsuransi",
                    "DPLK",
                    "TunjPajak",

                    //16-20
                    "Pph21",
                    "BPJSKetenagakerjaan1",
                    "BPJSKesehatanPegawai",
                    "CreditName",
                    "CreditName2",

                    //21-25             
                    "CreditName3",
                    "CreditName4",
                    "CreditName5",
                    "CreditName6",
                    "CreditName7",
       

                    //26-30
                   "CreditName8",
                    "CreditName9",
                   "CreditName10",
                   "CreditAdvancePayment1",
                   "CreditAdvancePayment2",

                    //31-35                 
                    "CreditAdvancePayment3",
                    "CreditAdvancePayment4",
                    "CreditAdvancePayment5",
                    "CreditAdvancePayment6",
                    "CreditAdvancePayment7",

                    //36-40              
                    "CreditAdvancePayment8",
                    "CreditAdvancePayment9",
                    "CreditAdvancePayment10",
                    "PenerimaanBersih",
                   "Company",

                    //41-45
                   "PayrunCode",
                   "Remark",
                   "TotalFixed",
                   "TotalNotFixed",
                   "Bruto",
                  // "TunjPribadi",
                });

                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            l3.Add(new PaySlipVIR()
                            {
                                CompanyLogo = Sm.DrStr(dr3, c3[0]),

                                Period = Sm.DrStr(dr3, c3[1]),
                                EmpCode = Sm.DrStr(dr3, c3[2]),
                                EmpName = Sm.DrStr(dr3, c3[3]),
                                PosName = Sm.DrStr(dr3, c3[4]),
                                Status = Sm.DrStr(dr3, c3[5]),

                                //6-10
                                Keluarga = Sm.DrStr(dr3, c3[6]),
                                NPWP = Sm.DrStr(dr3, c3[7]),
                                UnitKerja = Sm.DrStr(dr3, c3[8]),
                                Salary = Sm.DrDec(dr3, c3[9]),
                                Jumlah = Sm.DrDec(dr3, c3[10]),

                                //11
                                BPJSKetenagakerjaan = Sm.DrDec(dr3, c3[11]),
                                BPJSKesehatanPerusahaan = Sm.DrDec(dr3, c3[12]),
                                TunjAsuransi = Sm.DrDec(dr3, c3[13]),
                                DPLK = Sm.DrDec(dr3, c3[14]),
                                TunjPajak = Sm.DrDec(dr3, c3[15]),

                                Pph21 = Sm.DrDec(dr3, c3[16]),
                                BPJSKetenagakerjaan1 = Sm.DrDec(dr3, c3[17]),
                                BPJSKesehatanPegawai = Sm.DrDec(dr3, c3[18]),
                                CreditCode1 = Sm.DrStr(dr3, c3[19]),

                                CreditCode2 = Sm.DrStr(dr3, c3[20]),
                                CreditCode3 = Sm.DrStr(dr3, c3[21]),
                                CreditCode4 = Sm.DrStr(dr3, c3[22]),
                                CreditCode5 = Sm.DrStr(dr3, c3[23]),
                                CreditCode6 = Sm.DrStr(dr3, c3[24]),

                                CreditCode7 = Sm.DrStr(dr3, c3[25]),
                                CreditCode8 = Sm.DrStr(dr3, c3[26]),
                                CreditCode9 = Sm.DrStr(dr3, c3[27]),
                                CreditCode10 = Sm.DrStr(dr3, c3[28]),
                                CreditAdvancePayment1 = Sm.DrDec(dr3, c3[29]),

                                CreditAdvancePayment2 = Sm.DrDec(dr3, c3[30]),
                                CreditAdvancePayment3 = Sm.DrDec(dr3, c3[31]),
                                CreditAdvancePayment4 = Sm.DrDec(dr3, c3[32]),
                                CreditAdvancePayment5 = Sm.DrDec(dr3, c3[33]),
                                CreditAdvancePayment6 = Sm.DrDec(dr3, c3[34]),

                                CreditAdvancePayment7 = Sm.DrDec(dr3, c3[35]),
                                CreditAdvancePayment8 = Sm.DrDec(dr3, c3[36]),
                                CreditAdvancePayment9 = Sm.DrDec(dr3, c3[37]),
                                CreditAdvancePayment10 = Sm.DrDec(dr3, c3[38]),

                                PenerimaanBersih = Sm.DrDec(dr3, c3[39]),
                                PayrunCode = Sm.DrStr(dr3, c3[40]),
                                Company = Sm.DrStr(dr3, c3[41]),
                                Remark = Sm.DrStr(dr3, c3[42]),
                                TotalFixed = Sm.DrDec(dr3, c3[43]),
                                TotalNotFixed = Sm.DrDec(dr3, c3[44]),
                                Bruto = Sm.DrDec(dr3, c3[45]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(l3);

                #endregion

                #region EmpFixedAllowanceDGI
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                //     string mADCodeFixedAllowance = string.Empty;

                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;
                    mADCodeFixedAllowance = Sm.GetParameter("ADCodeFixedAllowance");
                    SQLDtl.AppendLine("Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
                    SQLDtl.AppendLine("From tblpayrollprocessad A ");
                    SQLDtl.AppendLine("inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                    //  SQLDtl.AppendLine("Order By C.AdType, C.AdName ");
                    SQLDtl.AppendLine("Where Find_In_Set (C.AdCode,'" + mADCodeFixedAllowance + "') ");
                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

                    var drDtl1 = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl1, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-4
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",
                    });
                    if (drDtl1.HasRows)
                    {
                        while (drDtl1.Read())
                        {
                            ldtl.Add(new EmpFixedAllowanceDGI()
                            {
                                PayrunCode = Sm.DrStr(drDtl1, cDtl[0]),
                                EmpCode = Sm.DrStr(drDtl1, cDtl[1]),
                                ADName = Sm.DrStr(drDtl1, cDtl[2]),
                                Amt = Sm.DrDec(drDtl1, cDtl[3]),
                                ADType = Sm.DrStr(drDtl1, cDtl[4]),
                            });
                        }
                    }
                    drDtl1.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region EmpNotFixedAllowanceDGI
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                //  string mADCodeNotFixedAllowance = string.Empty;

                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;
                    mADCodeNotFixedAllowance = Sm.GetParameter("ADCodeNotFixedAllowance");
                    SQLDtl2.AppendLine("Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
                    SQLDtl2.AppendLine("From tblpayrollprocessad A ");
                    SQLDtl2.AppendLine("inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");

                    //   SQLDtl.AppendLine("Order By C.AdType, C.AdName ");
                    SQLDtl2.AppendLine("Where Find_In_Set (C.AdCode,'" + mADCodeNotFixedAllowance + "') ");
                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@PayrunCode", Payrun);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",


                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new EmpNotFixedAllowanceDGI()
                            {
                                PayrunCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                ADName = Sm.DrStr(drDtl2, cDtl2[2]),
                                Amt = Sm.DrDec(drDtl2, cDtl2[3]),
                                ADType = Sm.DrStr(drDtl2, cDtl2[4]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region ADCodeCompanyAllowance
                var cmDtl3 = new MySqlCommand();
                var SQLDtl4 = new StringBuilder();
                string mADCodeCompanyAllowance = string.Empty;

                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;
                    mADCodeCompanyAllowance = Sm.GetParameter("ADCodeCompanyAllowance");
                    SQLDtl4.AppendLine("     Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
                    SQLDtl4.AppendLine("     From tblpayrollprocessad A ");
                    SQLDtl4.AppendLine("    inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                    SQLDtl4.AppendLine("    Where C.Adcode In ('" + mADCodeCompanyAllowance + "') ");

                    cmDtl3.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@PayrunCode", Payrun);

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-4
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",
      

                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ldtl3.Add(new ADCodeCompanyAllowance()
                            {
                                PayrunCode = Sm.DrStr(drDtl3, cDtl3[0]),
                                EmpCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                ADName = Sm.DrStr(drDtl3, cDtl3[2]),
                                Amt = Sm.DrDec(drDtl3, cDtl3[3]),
                                ADType = Sm.DrStr(drDtl3, cDtl3[4]),

                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

                #region ADCodePersonalAllowance
                var cmDtl4 = new MySqlCommand();
                var SQLDtl5 = new StringBuilder();
                string mADCodePersonalAllowance = string.Empty;

                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;
                    mADCodePersonalAllowance = Sm.GetParameter("ADCodePersonalAllowance");
                    SQLDtl5.AppendLine("     Select A.PayrunCode, A.ADCode, A.EmpCode, C.AdName, C.AdType, A.Amt ");
                    SQLDtl5.AppendLine("     From tblpayrollprocessad A ");
                    SQLDtl5.AppendLine("    inner join tblallowancededuction C ON C.AdCode=A.AdCode AND C.AmtType='1' and A.PayrunCode =@PayrunCode ");
                    SQLDtl5.AppendLine("    Where C.Adcode In ('" + mADCodePersonalAllowance + "') ");

                    cmDtl4.CommandText = SQLDtl5.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@PayrunCode", Payrun);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-4
                     "EmpCode" ,
                     "ADName",
                     "Amt",
                     "ADType",

                    });
                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {
                            ldtl4.Add(new ADCodePersonalAllowance()
                            {
                                PayrunCode = Sm.DrStr(drDtl4, cDtl4[0]),
                                EmpCode = Sm.DrStr(drDtl4, cDtl4[1]),
                                ADName = Sm.DrStr(drDtl4, cDtl4[2]),
                                Amt = Sm.DrDec(drDtl4, cDtl4[3]),
                                ADType = Sm.DrStr(drDtl4, cDtl4[4]),
                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);
                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, true);
            }
            #endregion

            #region SRN
            if (Doctitle == "SRN")
            {
                var l = new List<PaySlipSRN>();
                var ldtl = new List<EmpAllowance>();
                var ldtl2 = new List<EmpDeduction>();
                var ldtl3 = new List<EmpSSPer>();
                var ldtl4 = new List<EmpSSPee>();
                var ldtl5 = new List<EmployeeSincerely>();

                string[] TableName = { "PaySlipSRN", "EmpAllowance", "EmpDeduction", "EmpSSPer", "EmpSSPee" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, Z.CompanyName As Company, Z.CompanyPhone As Phone, Z.CompanyAddress As Address,  ");
                }
                else
                {
                    SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                }
                SQL.AppendLine("A.EmpCode, C.EmpName, D.DeptName, H.SiteName, A.Salary, A.Functional, A.SSEmployerHealth,  A.SSEmployeeHealth, ");
                SQL.AppendLine("A.SSEmployerEmployment, A.SSEmployeeEmployment, A.Amt, DATE_FORMAT(DATE_ADD(Concat(Left(A.PayrunCode, 6), '01'), Interval 0 MONTH),'%M %Y')As Periode, A.SSEmployerPension, ");
                SQL.AppendLine("A.SSEmployeePension, A.Payruncode, A.PerformanceValue, IfNull(X.Amt,0)As AmtAll, IfNull(Y.Amt,0)As AmtDed, ");
                SQL.AppendLine("A.ProcessUPLAmt, A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.SalaryAdjustment, A.Transport, A.Meal, A.ADOT, A.FixAllowance, ");
                SQL.AppendLine("A.FixDeduction, A.EmpAdvancePayment, A.SSEePension, A.tax, A.taxallowance, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, O.TransportHr, P.MealHr, A.Amt, I.DivisionName, Q.SignEmpname, Q.SignPosname, G.PosName, ");
                SQL.AppendLine("IF(BB.ParValue = 'Y', IFNULL(AA.OTAmt,0), (A.OT1Amt + A.OT2Amt + A.OTHolidayAmt)) As OTAmount ");

                SQL.AppendLine(" From tblpayrollprocess1 A ");
                SQL.AppendLine(" Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL.AppendLine(" Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine(" Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL.AppendLine(" Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL.AppendLine(" Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL.AppendLine(" Left Join tblsite H On C.SiteCode=H.SiteCode ");
                SQL.AppendLine(" Left Join TblDivision I On C.DivisionCode = I.DivisionCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='A' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")X On A.PayrunCode=X.Payruncode And A.EmpCode=X.EmpCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='D' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")Y On A.PayrunCode=Y.Payruncode And A.EmpCode=Y.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select PayrunCode, Empcode, count(transport)As TransportHr ");
                SQL.AppendLine("    From TblPayrollProcess2 ");
                SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Transport !=0 ");
                SQL.AppendLine("    Group by Payruncode, EmpCode ");
                SQL.AppendLine(" )O On A.PayrunCode=O.PayrunCode And A.EmpCode=O.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select PayrunCode, Empcode, count(Meal)As MealHr ");
                SQL.AppendLine("    From TblPayrollProcess2 ");
                SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Meal !=0 ");
                SQL.AppendLine("    Group by Payruncode, EmpCode ");
                SQL.AppendLine(" )P On A.PayrunCode=P.PayrunCode And A.EmpCode=P.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select B.Empname As SignEmpname, C.PosName AS SignPosName ");
                SQL.AppendLine("    From Tblparameter A ");
                SQL.AppendLine("    Inner Join TblEmployee B On A.ParValue = B.EmpCode ");
                SQL.AppendLine("    Inner Join TblPosition C On B.PosCode = C.PosCode ");
                SQL.AppendLine("    Where A.parCode = 'EmpCodeSincerely' ");
                SQL.AppendLine(")Q On 0=0 ");

                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select distinct A.PayrunCode, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                    SQL.AppendLine("    From Tblpayrun A  ");
                    SQL.AppendLine("    Inner Join TblSite B On A.SiteCode=B.SiteCode  ");
                    SQL.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode=C.ProfitCenterCode  ");
                    SQL.AppendLine("    Inner Join TblEntity D On C.EntCode=D.EntCode  ");
                    SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                    SQL.AppendLine(") Z On A.PayrunCode=Z.PayrunCode ");
                }

                SQL.AppendLine("Left Join ( ");
	            SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, Sum(A.Hr) As OTHr, Sum(A.Amt) As OTAmt ");
	            SQL.AppendLine("    From TblEmployeeOT A ");
	            SQL.AppendLine("    Group By A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(") AA On A.PayrunCode = AA.PayrunCode And A.EmpCode = AA.EmpCode ");
                SQL.AppendLine("Inner Join TblParameter BB On 0=0 And BB.ParCode = 'IsOTBaseOnOTFormula' ");

                SQL.AppendLine(" Where A.PayrunCode=@PayrunCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("And A.EmpCode Not In (Select B.EmpCode From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B Where A.AGCode=B.AGCode And A.ActInd='Y') ");

                SQL.AppendLine(" Order By A.Payruncode, C.EmpName ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);

                    if (TxtSiteName.Text.Length > 0)
                    {
                        string CompanyLogo = Sm.GetValue(
                           "Select D.EntLogoName " +
                           "From TblPayrun A  " +
                           "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                           "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                           "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                           "Where A.PayrunCode ='" + TxtPayrunCode.Text + "' "
                       );
                        if (CompanyLogo.Length > 0)
                        {
                            Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                        }
                        else
                        {
                            Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                        }
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }
                    if (Sm.GetLue(LueAGCode).Length > 0)
                        Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company", 
                    "Address",
                    "Phone",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "DeptName", 
                    "SiteName",
                    "Salary",
                    "Functional",
                    "SSEmployerHealth",

                    //11-15
                    "SSEmployeeHealth",
                    "SSEmployerEmployment",
                    "SSEmployeeEmployment",
                    "Amt",
                    "Periode",

                    //16-20
                    "SSEmployerPension",
                    "SSEmployeePension",
                    "Payruncode",
                    "PerformanceValue",
                    "AmtAll",

                    //21-22
                    "AmtDed",
                    "ProcessUPLAmt",
                    "OT1Amt",
                    "OT2Amt",
                    "OTHolidayAmt",

                    //26-30
                    "SalaryAdjustment",
                    "Transport",
                    "Meal",
                    "ADOT",
                    "FixAllowance",

                    //31-35
                    "FixDeduction",
                    "EmpAdvancePayment",
                    "SSEePension",
                    "tax",
                    "taxallowance", 

                    //36-40
                    "OT1Hr",
                    "OT2Hr",
                    "OTHolidayHr",
                    "TransportHr",
                    "MealHr",

                    //41-45
                    "DivisionName",
                    "SignEmpname",
                    "SignPosname",
                    "PosName",
                    "OTAmount"
                    
                    });

                    if (dr.HasRows)
                    {
                        int numb= 0;
                        while (dr.Read())
                        {
                            numb = numb+1;
                            l.Add(new PaySlipSRN()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                EmCode = Sm.DrStr(dr, c[4]),
                                EmpName = Sm.DrStr(dr, c[5]),

                                DeptName = Sm.DrStr(dr, c[6]),
                                SiteName = Sm.DrStr(dr, c[7]),
                                Salary = Sm.DrDec(dr, c[8]),
                                Functional = Sm.DrDec(dr, c[9]),
                                SSEmployerHealth = Sm.DrDec(dr, c[10]),

                                SSEmployeeHealth = Sm.DrDec(dr, c[11]),
                                SSEmployerEmployment = Sm.DrDec(dr, c[12]),
                                SSEmployeeEmployment = Sm.DrDec(dr, c[13]),
                                Amt = Sm.DrDec(dr, c[14]),
                                Periode = Sm.DrStr(dr, c[15]),

                                SSEmployerPension = Sm.DrDec(dr, c[16]),
                                SSEmployeePension = Sm.DrDec(dr, c[17]),
                                Payruncode = Sm.DrStr(dr, c[18]),
                                PerformanceValue = Sm.DrDec(dr, c[19]),
                                AmtAll = Sm.DrDec(dr, c[20]),

                                AmtDed = Sm.DrDec(dr, c[21]),
                                ProcessUPLAmt = Sm.DrDec(dr, c[22]),
                                OT1Amt = Sm.DrDec(dr, c[23]),
                                OT2Amt = Sm.DrDec(dr, c[24]),
                                OTHolidayAmt = Sm.DrDec(dr, c[25]),

                                SalaryAdjustment = Sm.DrDec(dr, c[26]),
                                Transport = Sm.DrDec(dr, c[27]),
                                Meal = Sm.DrDec(dr, c[28]),
                                ADOT = Sm.DrDec(dr, c[29]),
                                FixAllowance = Sm.DrDec(dr, c[30]),

                                FixDeduction = Sm.DrDec(dr, c[31]),
                                EmpAdvancePayment = Sm.DrDec(dr, c[32]),
                                SSEePension = Sm.DrDec(dr, c[33]),
                                tax = Sm.DrDec(dr, c[34]),
                                taxallowance = Sm.DrDec(dr, c[35]),

                                OT1Hr = Sm.DrDec(dr, c[36]),
                                OT2Hr = Sm.DrDec(dr, c[37]),
                                OTHolidayHr = Sm.DrDec(dr, c[38]),
                                TransportHr = Sm.DrDec(dr, c[39]),
                                MealHr = Sm.DrDec(dr, c[40]),

                                Terbilang = Sm.Terbilang3(Sm.DrDec(dr, c[14])),
                                DivisionName = Sm.DrStr(dr, c[41]),
                                SignEmpname = Sm.DrStr(dr, c[42]),
                                SignPosname = Sm.DrStr(dr, c[43]),
                                Batch  = String.Concat("PS", (Sm.DrStr(dr, c[18]).Substring(2, 2)), (Sm.DrStr(dr, c[18]).Substring(4, 2)), Sm.Right(string.Concat("0000", numb), 4), Sm.Right(Sm.DrStr(dr, c[4]),3)),
                                PosName = Sm.DrStr(dr, c[44]),
                                Signature = Sm.GetValue("Select Concat(IfNull(B.ParValue, ''), A.Parvalue, '.PNG') " +
                                           "From tblparameter A " +
                                           "Inner Join tblparameter B On 0=0  " +
                                           "Where A.ParCode = 'EmpCodeSincerely' And B.parCode = 'ImgFileSignature'"),
                                OTAmount = Sm.DrDec(dr, c[45]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                // UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")


                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region EmpAllowance
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                decimal no = 0;
                string EmpCode = string.Empty;
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;


                    SQLDtl.AppendLine("Select Z.PayrunCode, Z.EmpCode, Z.AdCode, UPPER(Z.AdName) AdName,  ifnull(Z1.Amt, ifnull(Z2.Amt,0)) As Amt, ");
                    SQLDtl.AppendLine("(Z3.FixAllowance+Z3.Meal+Z3.Transport+Z3.variableallowance) As totalAmt ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("    Select A.PayrunCode,  A.EmpCode, B.AdCode, B.Adname ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join TblAllowanceDeduction B On 0=0  ");
                    SQLDtl.AppendLine("    Where A.PayrunCode = @PayrunCode And B.Adtype = 'A' And B.AdCode <> '019' ");
                    SQLDtl.AppendLine("    )Z ");
                    SQLDtl.AppendLine("left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("    Select A.payrunCode, A.EmpCode, B.AdCode, B.Amt  ");
                    SQLDtl.AppendLine("    From tblpayrollprocess1 A ");
                    SQLDtl.AppendLine("    inner Join tblpayrollProcessAD B on A.PayrunCOde= B.payruNCode  And A.EmpCode = B.EmpCode ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine(")Z1 On  Z.payrunCode = Z1.payrunCode  And Z.EmpCode = Z1.EmpCode And Z.AdCode = Z1.AdCode ");
                    SQLDtl.AppendLine("Left Join  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue as AdCode, A.Meal As Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join Tblparameter B On parCode = 'ADCodeMeal' ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine("    Union All ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue as AdCode, A.Transport As Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join Tblparameter B On parCode = 'ADCodeTransport' ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine("    Union ALl ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue as AdCode, A.VariableAllowance As Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcess1 A ");
                    SQLDtl.AppendLine("    Inner Join Tblparameter B On parCode = 'ADCodeVariable' ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode ");
                    SQLDtl.AppendLine("    Union ALl ");
                    SQLDtl.AppendLine("    Select A.PayrunCode, A.EmpCode, A.AdCode, A.Amt ");
                    SQLDtl.AppendLine("    From TblpayrollProcessAd A ");
                    SQLDtl.AppendLine("    Where A.payrunCode = @payrunCode  ");
                    SQLDtl.AppendLine("    And find_in_set(A.AdCode, (select parvalue from tblparameter Where parCode ='AllowanceCodeVariable'))>0  ");
                    SQLDtl.AppendLine(")Z2 On Z.payrunCode = Z2.payrunCode  And Z.EmpCode = Z2.EmpCode And Z.AdCode = Z2.AdCode ");
                    SQLDtl.AppendLine("Inner jOin TblPayrollProcess1 Z3 On Z.payrunCode = Z3.payrunCode And Z.EmpCode = Z3.EmpCode  ");
                    SQLDtl.AppendLine("Order By Z.PayrunCode, Z.EmpCode, Z.AdCode  ");



                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADCode",
                     "ADName",
                     "Amt",
                     "TotalAmt",
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            if (Sm.DrStr(drDtl, cDtl[1]) != EmpCode)
                            {
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]);
                                no = 0;
                            }
                            else
                            {
                                no = no + 1;
                            }
                            ldtl.Add(new EmpAllowance()
                            {
                                No = no,
                                PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                                ADCode = Sm.DrStr(drDtl, cDtl[2]),
                                ADName = Sm.DrStr(drDtl, cDtl[3]),
                                Amt = Sm.DrDec(drDtl, cDtl[4]),
                                TotalAmt = Sm.DrDec(drDtl, cDtl[5]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(drDtl, cDtl[5])),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region EmpDeduction
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                decimal no2 = 0;
                //decimal no = 0;
                //string EmpCode = string.Empty;
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;


                    SQLDtl2.AppendLine("Select A.PayrunCode, A.AmtUpah,  A.EmpCode, A.CreditCode As ADCode, UPPER(A.Creditname) As ADName, ifnull(B.Amt, ifnull(C.Amt,0)) As Amt   ");
                    SQLDtl2.AppendLine("From   ");
                    SQLDtl2.AppendLine("(  ");
                    SQLDtl2.AppendLine("    Select A.Amt As AmtUpah, A.PayrunCode, A.EmpCode, B.CreditCode, B.Creditname   ");
                    SQLDtl2.AppendLine("    From tblpayrollprocess1 A  ");
                    SQLDtl2.AppendLine("    Inner Join TblCredit B On 0=0 And B.ActInd = 'Y' ");
                    SQLDtl2.AppendLine("    Where payrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    union ALl  ");
                    SQLDtl2.AppendLine("    Select A.Amt, A.PayrunCode,  A.EmpCode, B.AdCode, B.Adname   ");
                    SQLDtl2.AppendLine("    From TblpayrollProcess1 A   ");
                    SQLDtl2.AppendLine("    Inner Join TblAllowanceDeduction B On 0=0   ");
                    SQLDtl2.AppendLine("    Where A.PayrunCode = @PayrunCode And B.Adtype = 'D'   ");
                    SQLDtl2.AppendLine(")A  ");
                    SQLDtl2.AppendLine("left Join  ");
                    SQLDtl2.AppendLine("(  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode1 As CreditCode, EmpCode, ifnull(CreditAdvancepayment1, 0) Amt From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    Union ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode2, EmpCode, ifnull(CreditAdvancepayment2, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    Union ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode3, EmpCode, ifnull(CreditAdvancepayment3, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    UNION ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode4, EmpCode, ifnull(CreditAdvancepayment4, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    UNION ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode5, EmpCode, ifnull(CreditAdvancepayment5, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    UNION ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode6, EmpCode, ifnull(CreditAdvancepayment6, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    Union ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode7, EmpCode, ifnull(CreditAdvancepayment7, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    Union ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode8, EmpCode, ifnull(CreditAdvancepayment8, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    UNION ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode9, EmpCode, ifnull(CreditAdvancepayment9, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine("    UNION ALL  ");
                    SQLDtl2.AppendLine("    select PayrunCode, CreditCode10, EmpCode, ifnull(CreditAdvancepayment10, 0) From Tblpayrollprocess1  ");
                    SQLDtl2.AppendLine("    Where PayrunCode = @PayrunCode  ");
                    SQLDtl2.AppendLine(")B On A.PayrunCode = B.payrunCode And A.EmpCode = B.EmpCode And A.CreditCode = B.CreditCode  ");
                    SQLDtl2.AppendLine("left Join    ");
                    SQLDtl2.AppendLine("(   ");
                    SQLDtl2.AppendLine("    Select A.payrunCode, A.EmpCode, B.AdCode, B.Amt    ");
                    SQLDtl2.AppendLine("    From tblpayrollprocess1 A   ");
                    SQLDtl2.AppendLine("    inner Join tblpayrollProcessAD B on A.PayrunCOde= B.payruNCode  And A.EmpCode = B.EmpCode   ");
                    SQLDtl2.AppendLine("    Where A.payrunCode = @payrunCode   ");
                    SQLDtl2.AppendLine(")C On  A.payrunCode = C.payrunCode  And A.EmpCode = C.EmpCode And A.CreditCode = C.AdCode   ");
                    SQLDtl2.AppendLine("Where A.CreditCode not in ('010') ");
                    SQLDtl2.AppendLine("Order By A.PayrunCode, A.EmpCode, A.CreditCode ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@PayrunCode", Payrun);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADCode",
                     "ADName",
                     "Amt",
                     "AmtUpah"
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            if (Sm.DrStr(drDtl2, cDtl2[1]) != EmpCode)
                            {
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]);
                                no2 = 0;
                            }
                            else
                            {
                                no2 = no2 + 1;
                            }
                            ldtl2.Add(new EmpDeduction()
                            {
                                No = no2,
                                PayrunCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                ADCode = Sm.DrStr(drDtl2, cDtl2[2]),
                                ADName = Sm.DrStr(drDtl2, cDtl2[3]),
                                Amt = Sm.DrDec(drDtl2, cDtl2[4]),
                                AmtUpah = Sm.DrDec(drDtl2, cDtl2[5]),
                                //TotalPOT = TotalPOT+Sm.DrDec(drDtl3, cDtl3[4]),
                                Terbilang2 = Sm.Terbilang(Sm.DrDec(drDtl2, cDtl2[5]) - Sm.DrDec(drDtl2, cDtl2[4])),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region EmpBPJSperusahaan
                var cmDtl3 = new MySqlCommand();
                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select * From ( ");
                    SQLDtl3.AppendLine("Select A.payrunCode, A.EmpCode, UPPER(D.SSPName) As SSPName, SUM(B.EmployerAmt) As AMt ");
                    SQLDtl3.AppendLine("From Tblpayrollprocess1 A  ");
                    SQLDtl3.AppendLine("Inner Join TblEmpSSListDtl B On A.payrunCode = B.payrunCode And A.EmpCode = B.EmpCode ");
                    SQLDtl3.AppendLine("Inner Join TblEmpSSlistHdr C on B.DocNo  =C.DocNo ");
                    SQLDtl3.AppendLine("Inner Join TblSSprogram D On C.SSpCode = D.SSpCode ");
                    SQLDtl3.AppendLine("Where C.Cancelind = 'N' ");
                    SQLDtl3.AppendLine("Group By A.payrunCode, A.EmpCode, D.SSPName ");
                    SQLDtl3.AppendLine("Union ALL ");
                    SQLDtl3.AppendLine("Select A.payrunCode, A.EmpCode, 'PPH 21', A.TaxAllowance As AMt  ");
                    SQLDtl3.AppendLine("From Tblpayrollprocess1 A  ");
                    SQLDtl3.AppendLine(")Z Where Z.payrunCode = @PayrunCode Order By Z.PayrunCode, Z.EmpCode, Z.SSPName ");
                   
                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@PayrunCode", Payrun);

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "SSPName",
                     "Amt",
                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {                           
                            ldtl3.Add(new EmpSSPer()
                            {
                                PayrunCode = Sm.DrStr(drDtl3, cDtl3[0]),
                                EmpCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                SSPName = Sm.DrStr(drDtl3, cDtl3[2]),
                                Amt = Sm.DrDec(drDtl3, cDtl3[3]),
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

                #region EmpBPJSemployee
                var cmDtl4 = new MySqlCommand();
                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    SQLDtl4.AppendLine("Select * From ( ");
                    SQLDtl4.AppendLine("Select A.payrunCode, A.EmpCode,  UPPER(D.SSPName) SSPName, SUM(B.EmployeeAmt) As AMt   ");
                    SQLDtl4.AppendLine("From Tblpayrollprocess1 A   ");
                    SQLDtl4.AppendLine("Inner Join TblEmpSSListDtl B On A.payrunCode = B.payrunCode And A.EmpCode = B.EmpCode ");
                    SQLDtl4.AppendLine("Inner Join TblEmpSSlistHdr C on B.DocNo  =C.DocNo  ");
                    SQLDtl4.AppendLine("Inner Join TblSSprogram D On C.SSpCode = D.SSpCode  ");
                    SQLDtl4.AppendLine("Where C.Cancelind = 'N' And A.PayrunCode = @PayrunCode  ");
                    SQLDtl4.AppendLine("Group By A.payrunCode, A.EmpCode, D.SSPName  ");
                    SQLDtl4.AppendLine(")Z Order By Z.PayrunCode, Z.EmpCode, Z.SSPName ");

                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@PayrunCode", Payrun);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "SSPName",
                     "Amt",
                    });
                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {
                            ldtl4.Add(new EmpSSPee()
                            {
                                PayrunCode = Sm.DrStr(drDtl4, cDtl4[0]),
                                EmpCode = Sm.DrStr(drDtl4, cDtl4[1]),
                                SSPName = Sm.DrStr(drDtl4, cDtl4[2]),
                                Amt = Sm.DrDec(drDtl4, cDtl4[3]),
                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);
                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, true);
            }
            #endregion

            #region MMM
            if (Doctitle == "MMM")
            {
                var l = new List<PaySlipMMM>();
                var ldtl = new List<EmpAllowance>();
                var ldtl2 = new List<EmpDeduction>();
                var ldtl3 = new List<EmpSSPer>();
                var ldtl4 = new List<EmpSSPee>();
                var ldtl5 = new List<EmployeeSincerely>();
                var ldtl6 = new List<EmpInsentiveOrPenalty>();

                string[] TableName = { "PaySlipMMM", "EmpAllowance", "EmpDeduction", "EmpSSPer", "EmpSSPee", "EmpInsentiveOrPenalty" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, Z.CompanyName As Company, Z.CompanyPhone As Phone, Z.CompanyAddress As Address,  ");
                }
                else
                {
                    SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                    SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As Phone, ");
                }
                SQL.AppendLine("A.EmpCode, A.WorkingDay, AA.Absen, BB.IzinSKD, EE.IzinNonSKD, FF.Izin, CC.Cuti, DD.OTAmt, C.EmpName, D.DeptName, H.SiteName, A.Salary, A.Functional, A.SSEmployerHealth,  A.SSEmployeeHealth, ");
                SQL.AppendLine("A.SSEmployerEmployment, A.SSEmployeeEmployment, A.Amt, DATE_FORMAT(DATE_ADD(Concat(Left(A.PayrunCode, 6), '01'), Interval 1 MONTH),'%M %Y')As Periode, A.SSEmployerPension, ");
                SQL.AppendLine("A.SSEmployeePension, A.Payruncode, A.PerformanceValue, IfNull(X.Amt,0)As AmtAll, IfNull(Y.Amt,0)As AmtDed, ");
                SQL.AppendLine("A.ProcessUPLAmt, A.OT1Amt, A.OT2Amt, A.OTHolidayAmt, A.SalaryAdjustment, A.Transport, A.Meal, A.ADOT, A.FixAllowance, ");
                SQL.AppendLine("A.FixDeduction, A.EmpAdvancePayment, A.SSEePension, A.tax, A.taxallowance, A.OT1Hr, A.OT2Hr, A.OTHolidayHr, O.TransportHr, P.MealHr ");

                SQL.AppendLine(" From tblpayrollprocess1 A ");
                SQL.AppendLine(" Inner Join tblpayrun B On A.PayrunCode=B.PayrunCode ");
                SQL.AppendLine(" Inner Join tblemployee C On A.EmpCode=C.EmpCode ");
                SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode=D.DeptCode ");
                SQL.AppendLine(" Left Join TblOption E On C.SystemType=E.OptCode And E.OptCat='EmpSystemType' ");
                SQL.AppendLine(" Left Join tblgradelevelhdr F On C.GrdLvlCode=F.GrdLvlCode ");
                SQL.AppendLine(" Left Join tblposition G On C.PosCode=G.PosCode ");
                SQL.AppendLine(" Left Join tblsite H On C.SiteCode=H.SiteCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='A' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")X On A.PayrunCode=X.Payruncode And A.EmpCode=X.EmpCode ");

                SQL.AppendLine(" Left join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine("	Select A.PayrunCode, A.EmpCode, C.ADName, Sum(B.Amt)As Amt ");
                SQL.AppendLine("	From tblpayrollprocess1 A ");
                SQL.AppendLine("	Left Join tblpayrollprocessad B On A.PayrunCode=B.PayrunCode And A.EmpCode=B.EmpCode ");
                SQL.AppendLine("	Left Join TblAllowanceDeduction C On B.ADCode=C.AdCode ");
                SQL.AppendLine("	where C.ADType='D' ");
                SQL.AppendLine("	Group by A.PayrunCode, A.EmpCode ");
                SQL.AppendLine(")Y On A.PayrunCode=Y.Payruncode And A.EmpCode=Y.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select PayrunCode, Empcode, count(transport)As TransportHr ");
                SQL.AppendLine("    From TblPayrollProcess2 ");
                SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Transport !=0 ");
                SQL.AppendLine("    Group by Payruncode, EmpCode ");
                SQL.AppendLine(" )O On A.PayrunCode=O.PayrunCode And A.EmpCode=O.EmpCode ");

                SQL.AppendLine(" Left Join ( ");
                SQL.AppendLine("    Select PayrunCode, Empcode, count(Meal)As MealHr ");
                SQL.AppendLine("    From TblPayrollProcess2 ");
                SQL.AppendLine("    where Payruncode=@Payruncode And ProcessInd='Y' And Meal !=0 ");
                SQL.AppendLine("    Group by Payruncode, EmpCode ");
                SQL.AppendLine(" )P On A.PayrunCode=P.PayrunCode And A.EmpCode=P.EmpCode ");

                if (Sm.GetLue(LueAGCode).Length > 0 && !Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("Inner Join TblAttendanceGrpDtl I On C.EmpCode=I.EmpCode And I.AGCode=@AGCode ");

                if (TxtSiteName.Text.Length > 0)
                {
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select distinct A.PayrunCode, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                    SQL.AppendLine("    From Tblpayrun A  ");
                    SQL.AppendLine("    Inner Join TblSite B On A.SiteCode=B.SiteCode  ");
                    SQL.AppendLine("    Inner Join TblProfitCenter C On B.ProfitCenterCode=C.ProfitCenterCode  ");
                    SQL.AppendLine("    Inner Join TblEntity D On C.EntCode=D.EntCode  ");
                    SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                    SQL.AppendLine(") Z On A.PayrunCode=Z.PayrunCode ");
                    SQL.AppendLine("Left Join (");
	                SQL.AppendLine("    Select PayrunCode, EmpCode, Count(*) as Absen ");
	                SQL.AppendLine("    From tblpayrollprocess2 ");
	                SQL.AppendLine("    Where WSHolidayInd = 'N' And ActualIn is null And LeaveCode is null And WSIn1 is not null ");
	                SQL.AppendLine("    And PayrunCode=@PayrunCode ");
	                SQL.AppendLine("    Group By PayrunCode, EmpCode ");
                    SQL.AppendLine("    ) AA On A.PayrunCode = AA.PayrunCode And A.EmpCode = AA.EmpCode ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select PayrunCode, EmpCode, Count(*) as IzinSKD ");
	                SQL.AppendLine("    From tblpayrollprocess2 ");
                    SQL.AppendLine("    Where WSHolidayInd = 'N' And ActualIn is null And LeaveCode = '006' And WSIn1 is not null ");
	                SQL.AppendLine("    And PayrunCode=@PayrunCode ");
	                SQL.AppendLine("    Group By PayrunCode, EmpCode ");
                    SQL.AppendLine(") BB On A.PayrunCode = BB.PayrunCode And A.EmpCode = BB.EmpCode ");
                    SQL.AppendLine("Left Join (");
                    SQL.AppendLine("    Select PayrunCode, EmpCode, LeaveCode, Count(*) as Cuti ");
	                SQL.AppendLine("    From tblpayrollprocess2 ");
	                SQL.AppendLine("    Where WSHolidayInd = 'N' And LeaveCode In (Select LeaveCode From tblleave T1 ");
                    SQL.AppendLine("    Inner Join tblleavegrp T2 On T1.LGCode = T2.LGCode ");
		            SQL.AppendLine("    Where T2.LGCode = '005' Order By T1.LeaveCode Asc) And WSIn1 is not null ");
	                SQL.AppendLine("    And PayrunCode=@PayrunCode ");
	                SQL.AppendLine("    And FIND_IN_SET(LeaveCode, (Select ParValue ");
                    SQL.AppendLine("    From tblparameter Where ParCode = 'AnnualLeaveAllowanceCode')) ");
	                SQL.AppendLine("    Group By PayrunCode, EmpCode ");
                    SQL.AppendLine(") CC On A.PayrunCode = CC.PayrunCode And A.EmpCode = CC.EmpCode ");
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select PayrunCode, EmpCode, (OT1Amt + OT2Amt + OT3Amt + OTHolidayAmt) As OTAmt ");
                    SQL.AppendLine("    From tblpayrollprocess1 ");
                    SQL.AppendLine("    Where PayrunCode = @PayrunCode Group By PayrunCode, EmpCode ");
                    SQL.AppendLine(") DD On A.PayrunCode = DD.PayrunCode And A.EmpCode = DD.EmpCode ");
                    SQL.AppendLine(" Left Join ( ");
                    SQL.AppendLine("    Select PayrunCode, EmpCode, Count(*) as IzinNonSKD ");
                    SQL.AppendLine("    From tblpayrollprocess2 ");
                    SQL.AppendLine("    Where WSHolidayInd = 'N' And ActualIn is null And LeaveCode = '007' And WSIn1 is not null ");
                    SQL.AppendLine("    And PayrunCode=@PayrunCode ");
                    SQL.AppendLine("    Group By PayrunCode, EmpCode ");
                    SQL.AppendLine(" ) EE On A.PayrunCode = EE.PayrunCode And A.EmpCode = EE.EmpCode ");
                    SQL.AppendLine(" Left Join ( ");
	                SQL.AppendLine("    Select PayrunCode, EmpCode, Count(*) as Izin ");
                    SQL.AppendLine("    From tblpayrollprocess2 ");
                    SQL.AppendLine("    Where WSHolidayInd = 'N' And ActualIn is null And LeaveCode In (Select LeaveCode From tblleave T1 ");
		            SQL.AppendLine("    Inner Join tblleavegrp T2 On T1.LGCode = T2.LGCode ");
		            SQL.AppendLine("    Where T2.LGCode = '004' Order By T1.LeaveCode Asc) And WSIn1 is not null ");
                    SQL.AppendLine("    And PayrunCode=@PayrunCode ");
                    SQL.AppendLine("    Group By PayrunCode, EmpCode ");
                    SQL.AppendLine(" ) FF On A.PayrunCode = FF.PayrunCode And A.EmpCode = FF.EmpCode ");
                    SQL.AppendLine(" Left Join ( ");
	                SQL.AppendLine("    Select AA.PayrunCode, AA.EmpCode, AA.PresenceReward As UangKedisiplinan From tblpayrollprocess1 AA Where AA.PayrunCode = @PayrunCode ");
	                SQL.AppendLine("    ) XX On A.PayrunCode = XX.PayrunCode And A.EmpCode = XX.EmpCode ");
                    SQL.AppendLine(" Left Join ( ");
	                SQL.AppendLine("    Select CC.PayrunCode, CC.EmpCode, CC.IncEmployee As Bonus From tblpayrollprocess1 CC Where CC.PayrunCode = @PayrunCode ");
	                SQL.AppendLine("    ) YY On A.PayrunCode = YY.PayrunCode And A.EmpCode = YY.EmpCode ");
                    SQL.AppendLine(" Left Join ( ");
	                SQL.AppendLine("    Select BB.PayrunCode, BB.EmpCode, BB.IncPerformance As InsentifMarketing From tblpayrollprocess1 BB Where BB.PayrunCode = @PayrunCode ");
                    SQL.AppendLine("    ) ZZ On A.PayrunCode = ZZ.PayrunCode And A.EmpCode = ZZ.EmpCode ");
                }

                SQL.AppendLine(" Where A.PayrunCode=@PayrunCode ");
                if (Sm.GetLue(LueAGCode).Length > 0 && Sm.CompareStr(Sm.GetLue(LueAGCode), "AllEmployeeWithNoAttendanceGroup"))
                    SQL.AppendLine("And A.EmpCode Not In (Select B.EmpCode From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B Where A.AGCode=B.AGCode And A.ActInd='Y') ");

                SQL.AppendLine(" Order By A.Payruncode, C.EmpName ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@PayrunCode", Payrun);

                    //if (TxtSiteName.Text.Length > 0)
                    //{
                    //    string CompanyLogo = Sm.GetValue(
                    //       "Select D.EntLogoName " +
                    //       "From TblPayrun A  " +
                    //       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                    //       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                    //       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                    //       "Where A.PayrunCode ='" + TxtPayrunCode.Text + "' "
                    //   );
                    //    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    //}
                    //else
                    //{
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    //}
                        if (Sm.GetLue(LueAGCode).Length > 0)
                            Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "Company", 
                    "Address",
                    "Phone",
                    "EmpCode",
                    "EmpName",

                    //6-10
                    "DeptName", 
                    "SiteName",
                    "Salary",
                    "Functional",
                    "SSEmployerHealth",

                    //11-15
                    "SSEmployeeHealth",
                    "SSEmployerEmployment",
                    "SSEmployeeEmployment",
                    "Amt",
                    "Periode",

                    //16-20
                    "SSEmployerPension",
                    "SSEmployeePension",
                    "Payruncode",
                    "PerformanceValue",
                    "AmtAll",

                    //21-22
                    "AmtDed",
                    "ProcessUPLAmt",
                    "OT1Amt",
                    "OT2Amt",
                    "OTHolidayAmt",

                    //26-30
                    "SalaryAdjustment",
                    "Transport",
                    "Meal",
                    "ADOT",
                    "FixAllowance",

                    //31-35
                    "FixDeduction",
                    "EmpAdvancePayment",
                    "SSEePension",
                    "tax",
                    "taxallowance", 

                    //36-40
                    "OT1Hr",
                    "OT2Hr",
                    "OTHolidayHr",
                    "TransportHr",
                    "MealHr",

                    //41-45
                    "WorkingDay",
                    "Absen",
                    "Izin",
                    "Cuti",
                    "OTAmt",

                    //46-47
                    "IzinSKD",
                    "IzinNonSKD",
                    });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PaySlipMMM()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                Company = Sm.DrStr(dr, c[1]),
                                Address = Sm.DrStr(dr, c[2]),
                                Phone = Sm.DrStr(dr, c[3]),
                                EmCode = Sm.DrStr(dr, c[4]),
                                EmpName = Sm.DrStr(dr, c[5]),

                                DeptName = Sm.DrStr(dr, c[6]),
                                SiteName = Sm.DrStr(dr, c[7]),
                                Salary = Sm.DrDec(dr, c[8]),
                                Functional = Sm.DrDec(dr, c[9]),
                                SSEmployerHealth = Sm.DrDec(dr, c[10]),

                                SSEmployeeHealth = Sm.DrDec(dr, c[11]),
                                SSEmployerEmployment = Sm.DrDec(dr, c[12]),
                                SSEmployeeEmployment = Sm.DrDec(dr, c[13]),
                                Amt = Sm.DrDec(dr, c[14]),
                                Periode = Sm.DrStr(dr, c[15]),

                                SSEmployerPension = Sm.DrDec(dr, c[16]),
                                SSEmployeePension = Sm.DrDec(dr, c[17]),
                                Payruncode = Sm.DrStr(dr, c[18]),
                                PerformanceValue = Sm.DrDec(dr, c[19]),
                                AmtAll = Sm.DrDec(dr, c[20]),

                                AmtDed = Sm.DrDec(dr, c[21]),
                                ProcessUPLAmt = Sm.DrDec(dr, c[22]),
                                OT1Amt = Sm.DrDec(dr, c[23]),
                                OT2Amt = Sm.DrDec(dr, c[24]),
                                OTHolidayAmt = Sm.DrDec(dr, c[25]),

                                SalaryAdjustment = Sm.DrDec(dr, c[26]),
                                Transport = Sm.DrDec(dr, c[27]),
                                Meal = Sm.DrDec(dr, c[28]),
                                ADOT = Sm.DrDec(dr, c[29]),
                                FixAllowance = Sm.DrDec(dr, c[30]),

                                FixDeduction = Sm.DrDec(dr, c[31]),
                                EmpAdvancePayment = Sm.DrDec(dr, c[32]),
                                SSEePension = Sm.DrDec(dr, c[33]),
                                tax = Sm.DrDec(dr, c[34]),
                                taxallowance = Sm.DrDec(dr, c[35]),

                                OT1Hr = Sm.DrDec(dr, c[36]),
                                OT2Hr = Sm.DrDec(dr, c[37]),
                                OTHolidayHr = Sm.DrDec(dr, c[38]),
                                TransportHr = Sm.DrDec(dr, c[39]),
                                MealHr = Sm.DrDec(dr, c[40]),

                                WorkingDay = Sm.DrStr(dr, c[41]),
                                Absen = Sm.DrStr(dr, c[42]),
                                Izin = Sm.DrStr(dr, c[43]),
                                Cuti = Sm.DrStr(dr, c[44]),
                                OTAmt = Sm.DrStr(dr, c[45]),

                                IzinSKD = Sm.DrStr(dr, c[46]),
                                IzinNonSKD = Sm.DrStr(dr, c[47]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                // UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")


                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region EmpAllowance
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                decimal no = 0;
                string EmpCode = string.Empty;
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.PayrunCode, A.EmpCode, A.ADCode, B.ADName, A.Amt, Sum(A.Amt) As TotalAmt ");
                    SQLDtl.AppendLine("From tblpayrollprocessad A ");
                    SQLDtl.AppendLine("Inner Join tblallowancededuction B On A.ADCode = B.ADCode ");
                    SQLDtl.AppendLine("Where B.ADType = 'A' And PayrunCode=@PayrunCode ");
                    SQLDtl.AppendLine("Group By A.PayrunCode, A.EmpCode, B.ADName ");
                    SQLDtl.AppendLine("Order By A.EmpCode ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@PayrunCode", Payrun);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADCode",
                     "ADName",
                     "Amt",
                     "TotalAmt",
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            if (Sm.DrStr(drDtl, cDtl[1]) != EmpCode)
                            {
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]);
                                no = 0;
                            }
                            else
                            {
                                no = no + 1;
                            }
                            ldtl.Add(new EmpAllowance()
                            {
                                No = no,
                                PayrunCode = Sm.DrStr(drDtl, cDtl[0]),
                                EmpCode = Sm.DrStr(drDtl, cDtl[1]),
                                ADCode = Sm.DrStr(drDtl, cDtl[2]),
                                ADName = Sm.DrStr(drDtl, cDtl[3]),
                                Amt = Sm.DrDec(drDtl, cDtl[4]),
                                TotalAmt = Sm.DrDec(drDtl, cDtl[5]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(drDtl, cDtl[5])),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region EmpDeduction
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                decimal no2 = 0;
                //decimal no = 0;
                //string EmpCode = string.Empty;
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select A.PayrunCode, A.EmpCode, A.ADCode, B.ADName, A.Amt, Sum(A.Amt) As AmtUpah ");
                    SQLDtl2.AppendLine("From tblpayrollprocessad A ");
                    SQLDtl2.AppendLine("Inner Join tblallowancededuction B On A.ADCode = B.ADCode ");
                    SQLDtl2.AppendLine("Where B.ADType = 'D' And PayrunCode=@PayrunCode ");
                    SQLDtl2.AppendLine("Group By A.PayrunCode, A.EmpCode, B.ADName ");
                    SQLDtl2.AppendLine("Order By A.EmpCode ");
                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@PayrunCode", Payrun);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "ADCode",
                     "ADName",
                     "Amt",
                     "AmtUpah"
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            if (Sm.DrStr(drDtl2, cDtl2[1]) != EmpCode)
                            {
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]);
                                no2 = 0;
                            }
                            else
                            {
                                no2 = no2 + 1;
                            }
                            ldtl2.Add(new EmpDeduction()
                            {
                                No = no2,
                                PayrunCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                EmpCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                ADCode = Sm.DrStr(drDtl2, cDtl2[2]),
                                ADName = Sm.DrStr(drDtl2, cDtl2[3]),
                                Amt = Sm.DrDec(drDtl2, cDtl2[4]),
                                AmtUpah = Sm.DrDec(drDtl2, cDtl2[5]),
                                //TotalPOT = TotalPOT+Sm.DrDec(drDtl3, cDtl3[4]),
                                Terbilang2 = Sm.Terbilang(Sm.DrDec(drDtl2, cDtl2[5]) - Sm.DrDec(drDtl2, cDtl2[4])),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region EmpBPJSperusahaan
                var cmDtl3 = new MySqlCommand();
                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select * From ( ");
                    SQLDtl3.AppendLine("Select A.payrunCode, A.EmpCode, UPPER(D.SSPName) As SSPName, SUM(B.EmployerAmt) As AMt ");
                    SQLDtl3.AppendLine("From Tblpayrollprocess1 A  ");
                    SQLDtl3.AppendLine("Inner Join TblEmpSSListDtl B On A.payrunCode = B.payrunCode And A.EmpCode = B.EmpCode ");
                    SQLDtl3.AppendLine("Inner Join TblEmpSSlistHdr C on B.DocNo  =C.DocNo ");
                    SQLDtl3.AppendLine("Inner Join TblSSprogram D On C.SSpCode = D.SSpCode ");
                    SQLDtl3.AppendLine("Where C.Cancelind = 'N' ");
                    SQLDtl3.AppendLine("Group By A.payrunCode, A.EmpCode, D.SSPName ");
                    SQLDtl3.AppendLine("Union ALL ");
                    SQLDtl3.AppendLine("Select A.payrunCode, A.EmpCode, 'PPH 21', A.TaxAllowance As AMt  ");
                    SQLDtl3.AppendLine("From Tblpayrollprocess1 A  ");
                    SQLDtl3.AppendLine(")Z Where Z.payrunCode = @PayrunCode Order By Z.PayrunCode, Z.EmpCode ");

                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@PayrunCode", Payrun);

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "SSPName",
                     "Amt",
                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ldtl3.Add(new EmpSSPer()
                            {
                                PayrunCode = Sm.DrStr(drDtl3, cDtl3[0]),
                                EmpCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                SSPName = Sm.DrStr(drDtl3, cDtl3[2]),
                                Amt = Sm.DrDec(drDtl3, cDtl3[3]),
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

                #region EmpBPJSemployee
                var cmDtl4 = new MySqlCommand();
                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    SQLDtl4.AppendLine("Select * From ( ");
                    SQLDtl4.AppendLine("Select A.payrunCode, A.EmpCode,  UPPER(D.SSPName) SSPName, SUM(B.EmployeeAmt) As AMt   ");
                    SQLDtl4.AppendLine("From Tblpayrollprocess1 A   ");
                    SQLDtl4.AppendLine("Inner Join TblEmpSSListDtl B On A.payrunCode = B.payrunCode And A.EmpCode = B.EmpCode ");
                    SQLDtl4.AppendLine("Inner Join TblEmpSSlistHdr C on B.DocNo  =C.DocNo  ");
                    SQLDtl4.AppendLine("Inner Join TblSSprogram D On C.SSpCode = D.SSpCode  ");
                    SQLDtl4.AppendLine("Where C.Cancelind = 'N' And A.PayrunCode = @PayrunCode  ");
                    SQLDtl4.AppendLine("Group By A.payrunCode, A.EmpCode, D.SSPName  ");
                    SQLDtl4.AppendLine(")Z Order By Z.PayrunCode, Z.EmpCode ");

                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@PayrunCode", Payrun);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "SSPName",
                     "Amt",
                    });
                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {
                            ldtl4.Add(new EmpSSPee()
                            {
                                PayrunCode = Sm.DrStr(drDtl4, cDtl4[0]),
                                EmpCode = Sm.DrStr(drDtl4, cDtl4[1]),
                                SSPName = Sm.DrStr(drDtl4, cDtl4[2]),
                                Amt = Sm.DrDec(drDtl4, cDtl4[3]),
                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);
                #endregion

                #region EmpInsentiveOrPenalty
                var cmDtl6 = new MySqlCommand();
                var SQLDtl6 = new StringBuilder();
                using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl6.Open();
                    cmDtl6.Connection = cnDtl6;

                    SQLDtl6.AppendLine(" Select B.PayrunCode, B.EmpCode, A.InspntCode, C.InspntName, A.AmtInspnt, Sum(A.AmtInspnt) As TotalInsentif ");
                    SQLDtl6.AppendLine(" From tblempinspnthdr A ");
                    SQLDtl6.AppendLine(" Inner Join tblempinspntdtl B On A.DocNo = B.DocNo ");
                    SQLDtl6.AppendLine(" Inner Join tblinspnt C On A.InspntCode = C.InspntCode ");
                    SQLDtl6.AppendLine(" Where B.PayrunCode = @PayrunCode ");
                    SQLDtl6.AppendLine(" Group By A.InspntCode; ");

                    cmDtl6.CommandText = SQLDtl6.ToString();

                    Sm.CmParam<String>(ref cmDtl6, "@PayrunCode", Payrun);

                    var drDtl6 = cmDtl6.ExecuteReader();
                    var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                    {
                     //0
                     "PayrunCode" ,

                     //1-5
                     "EmpCode" ,
                     "InspntCode",
                     "InspntName",
                     "AmtInspnt",
                     "TotalInsentif",
                    });
                    if (drDtl6.HasRows)
                    {
                        while (drDtl6.Read())
                        {
                            ldtl6.Add(new EmpInsentiveOrPenalty()
                            {
                                PayrunCode = Sm.DrStr(drDtl6, cDtl6[0]),
                                EmpCode = Sm.DrStr(drDtl6, cDtl6[1]),
                                InspntCode = Sm.DrStr(drDtl6, cDtl6[2]),
                                InspntName = Sm.DrStr(drDtl6, cDtl6[3]),
                                AmtInspnt = Sm.DrDec(drDtl6, cDtl6[4]),
                                TotalInsentif = Sm.DrDec(drDtl6, cDtl6[5]),
                            });
                        }
                    }
                    drDtl6.Close();
                }
                myLists.Add(ldtl6);
                #endregion

                Sm.PrintReport(mIsFormPrintOutPayslip, myLists, TableName, false);
            }
            #endregion
        }

        internal void SetLueAGCode(ref LookUpEdit Lue, string DeptCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')=@DeptCode ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AllEmployeeWithNoAttendanceGroup' As Col1, 'All Employee With No Attendance Group' As Col2 ");
            SQL.AppendLine("Order By Col2;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private bool IsEmpSystemTypeBorongan()
        {
            var cm = new MySqlCommand() 
            { CommandText = "Select PayrunCode From TblPayrun Where PayrunCode=@PayrunCode And SystemType=@EmpSystemTypeBorongan;" };
            Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpSystemTypeBorongan", mEmpSystemTypeBorongan);
            return Sm.IsDataExist(cm);
        }


        #region list printout

        private void Process1(ref List<PayrollAD> lPayrollAD)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, A.ADCode, A.Amt, If(A.Amt>0,'Rp','')As Curcode ");
                SQL.AppendLine("    From TblPayrollProcessAD A ");
                SQL.AppendLine("    Where A.payrunCode=@PayrunCode  ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, B.parvalue, A.Meal, If(A.Meal>0,'Rp','')As Curcode ");
                SQL.AppendLine("    From tblpayrollprocess1 A ");
                SQL.AppendLine("    Inner Join tblparameter B on 0=0 And B.parCode='ADCodeMeal' ");
                SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue, A.Transport, If(A.Transport>0,'Rp','')As Curcode  ");
                SQL.AppendLine("    From tblpayrollprocess1 A ");
                SQL.AppendLine("    Inner Join tblparameter B on 0=0 And B.parCode='ADCodeTransport' ");
                SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.PayrunCode, A.EmpCode, B.Parvalue, A.VariableAllowance, If(A.VariableAllowance>0,'Rp','')As Curcode   ");
                SQL.AppendLine("    From tblpayrollprocess1 A ");
                SQL.AppendLine("    Inner Join tblparameter B on 0=0 And B.parCode='ADCodeVariable' ");
                SQL.AppendLine("    Where A.PayrunCode=@PayrunCode ");
                SQL.AppendLine(")Z Order By Z.payrunCode, Z.EmpCode, Z.AdCode ");

                Sm.CmParam<String>(ref cm, "@PayrunCode", TxtPayrunCode.Text);
                
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PayrunCode", "EmpCode", "ADCode", "Amt", "Curcode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lPayrollAD.Add(new PayrollAD()
                        {
                            PayrunCode = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            ADCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            CurCode = Sm.DrStr(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<AD> lAD)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            string entCode = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;

                SQL.AppendLine("Select A.ADCode, A.ADName, A.ADType ");
                SQL.AppendLine("From TblAllowanceDeduction A ");
                //SQL.AppendLine("Where ADCode not in (Select parvalue From tblparameter Where parcode in('ADCodeTransport', 'ADCodeVariable', 'ADCodeMeal')) ");
                SQL.AppendLine("Order By A.ADCode, A.ADType; ");
            
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ADCode", "ADName", "ADType" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lAD.Add(new AD()
                        {
                            ADCode = Sm.DrStr(dr, c[0]),
                            ADName = Sm.DrStr(dr, c[1]),
                            ADType = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }



        #endregion

        #endregion

        #endregion

        #region Event

        private void BtnPayrunCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPaySlipParticular1Dlg(this));
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), mDeptCode);
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPayrunCode, "Payrun Code", false)) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowPayrunInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Report Class

        class PayrollAD
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
        }

        class EmpSSPer
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string SSPName { get; set; }
            public decimal Amt { get; set; }
        }

        class EmpSSPee
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string SSPName { get; set; }
            public decimal Amt { get; set; }
        }

        class AD
        {
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public string ADType { get; set; }

        }

        class PaySlip
        {
            public string CompanyLogo { get; set; }

            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string EmCode { set; get; }
            public string EmpName { get; set; }

            public string DisplayName { get; set; }
            public decimal Salary { get; set; }
            public decimal WorkingDay { get; set; }
            public string DeptName { get; set; }
            public string Type { set; get; }

            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal OTHolidayAmt { get; set; }

            public decimal HolidayEarning { get; set; }
            public decimal PresenceReward { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncPerformance { get; set; }
            public decimal ExtraFooding { get; set; }

            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal PLAmt { get; set; }
            public decimal DedProduction { get; set; }
            public decimal SSEmployeeHealth { get; set; }

            public decimal SSEmployeeEmployment { get; set; }
            public decimal DedEmployee { get; set; }
            public decimal Tax { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal IncEmployee { get; set; }

            public decimal Amt { get; set; }
            public string NPWP { get; set; }
            public string GrdLvlName { get; set; }
            public string PosName { get; set; }
            public string DOH { get; set; }

            public string PTKP { get; set; }
            public string Periode { get; set; }
            public string SiteName { get; set; }
            public string BankAcNo { get; set; }
            public string BankAcName { get; set; }

            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal TotalOTHr { get; set; }
            public decimal TotalOTAmt { get; set; }
            public decimal Transport { get; set; }

            public decimal Meal { get; set; }
            public string Dt { get; set; }
            public string WsIn { get; set; }
            public string WsOut { get; set; }
            public string Status { get; set; }

            public string OTRegulerIn { get; set; }
            public string OTRegulerOut { get; set; }
            public string OTExtraDayIn { get; set; }
            public string OTExtraDayOut { get; set; }

            public string PrintBy { get; set; }
            public string UserCode { get; set; }
            public string KeyGroup { get; set; }
            public decimal EmpAdvancePayment { get; set; }
            public decimal FieldAssignment { get; set; }

            public decimal EmploymentPeriodAllowance { get; set; }
        }

        class PaySlip2
        {
            public string CompanyLogo { get; set; }
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string EmCode { set; get; }
            public string EmpName { get; set; }
            public string DisplayName { get; set; }
            public decimal Salary { get; set; }
            public decimal WorkingDay { get; set; }
            public string DeptName { get; set; }
            public string Type { set; get; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal PresenceReward { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncPerformance { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal SSEmployerHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal PLAmt { get; set; }
            public decimal DedProduction { get; set; }
            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal DedEmployee { get; set; }
            public decimal Tax { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal IncEmployee { get; set; }
            public decimal Amt { get; set; }
            public string NPWP { get; set; }
            public string GrdLvlName { get; set; }
            public string PosName { get; set; }
            public string DOH { get; set; }
            public string PTKP { get; set; }
            public string Periode { get; set; }
            public string SiteName { get; set; }
            public string BankAcNo { get; set; }
            public string BankAcName { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal TotalOTHr { get; set; }
            public decimal TotalOTAmt { get; set; }
            public decimal Transport { get; set; }
            public decimal Meal { get; set; }
            public string Dt { get; set; }
            public string WsIn { get; set; }
            public string WsOut { get; set; }
            public string Status { get; set; }
            public string OTRegulerIn { get; set; }
            public string OTRegulerOut { get; set; }
            public string OTExtraDayIn { get; set; }
            public string OTExtraDayOut { get; set; }
            public decimal OTHolidayHr { get; set; }
            public string PrintBy { get; set; }
            public string UserCode { get; set; }
            public decimal EmpAdvancePayment { get; set; }
            public decimal SaldoAmtAP { get; set; }
            public decimal OTHoliday { get; set; }
            public decimal IncPerformanceWorkingDay { get; set; }
            public decimal Incentif { get; set; }
            public decimal OTHolidayBorong { get; set; }
            public decimal ADFixedMeal { get; set; }
        }

        class PaySlipHIN
        {
            public string CompanyLogo { get; set; }
            public string Company { get; set; }
            public string PayrunCode { get; set; }
            public string Period { get; set; }
            public string EmCode { set; get; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string JoinDt { get; set; }
            public string GrdLvlName { set; get; }
            public string DeptName { get; set; }
            public string PosName { set; get; }
            public decimal Salary { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal SalaryNonPension { get; set; }
            public decimal Functional { get; set; }
            public decimal Transport { get; set; }
            public decimal Service { get; set; }
            public decimal Housing { get; set; }
            public decimal MobileCredit { get; set; }
            public decimal TotNonUpah { get; set; }
            public decimal JKK { get; set; }
            public decimal JKM { get; set; }
            public decimal JHT { get; set; }
            public decimal JKN { get; set; }
            public decimal SSErPension { get; set; }
            public decimal EmployerPension { get; set; }
            public decimal TotSubsidi { get; set; }
            public decimal Employment { get; set; }
            public decimal Health { get; set; }
            public decimal JaminanPensiun { get; set; }
            public decimal Pensiun { get; set; }
            public string CreditCode1 { get; set; }
            public string CreditCode2 { get; set; }
            public string CreditCode3 { get; set; }
            public string CreditCode4 { get; set; }
            public string CreditCode5 { get; set; }
            public string CreditCode6 { get; set; }
            public string CreditCode7 { get; set; }
            public string CreditCode8 { get; set; }
            public string CreditCode9 { get; set; }
            public string CreditCode10 { get; set; }
            public decimal TotPot { get; set; }
            public decimal Gapok { get; set; }
            public decimal Zakat { get; set; }
            public decimal CreditAdvancePayment1 { get; set; }
            public decimal CreditAdvancePayment2 { get; set; }
            public decimal CreditAdvancePayment3 { get; set; }
            public decimal CreditAdvancePayment4 { get; set; }
            public decimal CreditAdvancePayment5 { get; set; }
            public decimal CreditAdvancePayment6 { get; set; }
            public decimal CreditAdvancePayment7 { get; set; }
            public decimal CreditAdvancePayment8 { get; set; }
            public decimal CreditAdvancePayment9 { get; set; }
            public decimal CreditAdvancePayment10 { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal Tax { get; set; }
            public decimal OtherAllowance { get; set; }
            public decimal OtherDeduction { get; set; }
            public decimal OT { get; set; }
            public decimal THP { get; set; }
            public decimal ADLeave { get; set; }
            public decimal Brutto { get; set; }
            public string PrintBy { get; set; }

        }

        class PaySlipTWC
        {
            public string CompanyLogo { get; set; }

            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string EmCode { set; get; }
            public string EmpName { get; set; }

            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public decimal Salary { get; set; }
            public decimal Functional { get; set; }
            public decimal SSEmployerHealth { get; set; }

            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public string Periode { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal AmtAll { get; set; }
            public decimal AmtDed { get; set; }
            public string PosName { get; set; }

            public decimal ProcessUPLAmt { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal Transport { get; set; }
            public decimal Meal { get; set; }
            public decimal ADOT { get; set; }
            public decimal FixAllowance { get; set; }
            public decimal FixDeduction { get; set; }
            public decimal EmpAdvancePayment { get; set; }
            public decimal SSEePension { get; set; }
            public decimal tax { get; set; }
            public decimal taxallowance { get; set; }
            public decimal SSEmpHealth { get; set; }
            public string CreditName { get; set; }
            public string AdvancePay { get; set; }
            public decimal AdvancePayment { get; set; }
            public string Payruncode { get; set; }
            public string PrintBy { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal TransportHr { get; set; }
            public decimal MealHr { get; set; }
            public string Periode2 { get; set; }
            public decimal AmtTambahan { get; set; }
            public decimal SSEeDPLK { get; set; }
            public decimal SSErDPLK { get; set; }
            public decimal VarAllowance { get; set; }
            public string Terbilang { get; set; }
            public decimal IncEmployee { get; set; }
            public string DeptCode { get; set; }
            public string Bank { get; set; }
            public string BankAccount { get; set; }
            public decimal ShiftAllowance { get; set; }
            public decimal UTD { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ADleave { get; set; }
            public decimal AmtInsPntUTD { get; set; }
            public string EmpCodeOld { get; set; }
            public string Level { get; set; }
            public string TerbilangAmtUpahDeduction { get; set; }
            public decimal TerbilangAmtUpahDeductionDec { get; set; }
            public string EmpCodeSincerely { get; set; }
            public decimal TotalAmtAllowance { get; set; }
            public string SectionCode { get; set; }
            public string SectionName { get; set; }
            public string Signature { get; set; }
        }

        class PaySlipSRN
        {
            public string CompanyLogo { get; set; }

            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string EmCode { set; get; }
            public string EmpName { get; set; }

            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public decimal Salary { get; set; }
            public decimal Functional { get; set; }
            public decimal SSEmployerHealth { get; set; }

            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public string Periode { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal AmtAll { get; set; }
            public decimal AmtDed { get; set; }
            public string PosName { get; set; }

            public decimal OTAmount { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal Transport { get; set; }
            public decimal Meal { get; set; }
            public decimal ADOT { get; set; }
            public decimal FixAllowance { get; set; }
            public decimal FixDeduction { get; set; }
            public decimal EmpAdvancePayment { get; set; }
            public decimal SSEePension { get; set; }
            public decimal tax { get; set; }
            public decimal taxallowance { get; set; }
            public decimal SSEmpHealth { get; set; }
            public string CreditName { get; set; }
            public string AdvancePay { get; set; }
            public decimal AdvancePayment { get; set; }
            public string Payruncode { get; set; }
            public string PrintBy { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal TransportHr { get; set; }
            public decimal MealHr { get; set; }
            public string Periode2 { get; set; }
            public decimal AmtTambahan { get; set; }
            public decimal SSEeDPLK { get; set; }
            public decimal SSErDPLK { get; set; }
            public decimal VarAllowance { get; set; }
            public string Terbilang { get; set; }
            public decimal IncEmployee { get; set; }
            public string DeptCode { get; set; }
            public string Bank { get; set; }
            public string BankAccount { get; set; }
            public decimal ShiftAllowance { get; set; }
            public decimal UTD { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ADleave { get; set; }
            public decimal AmtInsPntUTD { get; set; }
            public string EmpCodeOld { get; set; }
            public string Level { get; set; }
            public string TerbilangAmtUpahDeduction { get; set; }
            public decimal TerbilangAmtUpahDeductionDec { get; set; }
            public string EmpCodeSincerely { get; set; }
            public decimal TotalAmtAllowance { get; set; }
            public string SectionCode { get; set; }
            public string SectionName { get; set; }
            public string Signature { get; set; }
            public string DivisionName { get; set; }
            public string SignEmpname { get; set; }
            public string SignPosname { get; set; }
            public string Batch { get; set; }

        }

        class PaySlipAWG
        {
            public string CompanyLogo { get; set; }

            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string EmCode { set; get; }
            public string EmpName { get; set; }

            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public decimal Salary { get; set; }
            public decimal Functional { get; set; }
            public decimal SSEmployerHealth { get; set; }

            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public string Periode { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal AmtAll { get; set; }
            public decimal AmtDed { get; set; }
            public string PosName { get; set; }

            public decimal ProcessUPLAmt { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal Transport { get; set; }
            public decimal Meal { get; set; }
            public decimal ADOT { get; set; }
            public decimal FixAllowance { get; set; }
            public decimal FixDeduction { get; set; }
            public decimal EmpAdvancePayment { get; set; }
            public decimal SSEePension { get; set; }
            public decimal tax { get; set; }
            public decimal taxallowance { get; set; }
            public decimal SSEmpHealth { get; set; }
            public string CreditName { get; set; }
            public string AdvancePay { get; set; }
            public decimal AdvancePayment { get; set; }
            public string Payruncode { get; set; }
            public string PrintBy { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal TransportHr { get; set; }
            public decimal OverHrAmt { get; set; }
            public decimal AtdPremi1 { get; set; }
            public decimal AtdPremi2 { get; set; }
            public decimal AtdPremi3 { get; set; }
            public decimal ShiftPremi { get; set; }
            public string Periode2 { get; set; }

        }

        class PaySlipVIR
        {
            public string CompanyLogo { get; set; }
            public string Company { get; set; }
            public string PayrunCode { get; set; }
            public string Period { get; set; }
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string PosName { set; get; }
            public string Status { get; set; }
            public string Keluarga { get; set; }
            public string UnitKerja { get; set; }
            public decimal Jumlah { get; set; }
            public decimal Salary { get; set; }
            public decimal TotalFixed { get; set; }
            public decimal TotalNotFixed { get; set; }
            public string NPWP { set; get; }
            public string Remark { get; set; }
            //  public decimal Jumlah { set; get; }
            public decimal TunjPerusahaan { get; set; }
            public decimal TunjPribadi { get; set; }
            public decimal BPJSKetenagakerjaan { get; set; }
            public decimal BPJSKesehatanPerusahaan { get; set; }
            public decimal TunjAsuransi { get; set; }
            public decimal DPLK { get; set; }
            public decimal TunjPajak { get; set; }
            public decimal Bruto { get; set; }
            public decimal Pph21 { get; set; }
            public string CreditCode1 { get; set; }
            public string CreditCode2 { get; set; }
            public string CreditCode3 { get; set; }
            public string CreditCode4 { get; set; }
            public string CreditCode5 { get; set; }
            public string CreditCode6 { get; set; }
            public string CreditCode7 { get; set; }
            public string CreditCode8 { get; set; }
            public string CreditCode9 { get; set; }
            public string CreditCode10 { get; set; }
            public decimal CreditAdvancePayment1 { get; set; }
            public decimal CreditAdvancePayment2 { get; set; }
            public decimal CreditAdvancePayment3 { get; set; }
            public decimal CreditAdvancePayment4 { get; set; }
            public decimal CreditAdvancePayment5 { get; set; }
            public decimal CreditAdvancePayment6 { get; set; }
            public decimal CreditAdvancePayment7 { get; set; }
            public decimal CreditAdvancePayment8 { get; set; }
            public decimal CreditAdvancePayment9 { get; set; }
            public decimal CreditAdvancePayment10 { get; set; }
            public decimal BPJSKetenagakerjaan1 { get; set; }
            public decimal BPJSKesehatanPegawai { get; set; }
            public decimal PenerimaanBersih { get; set; }
            public decimal JlhPengeluaran { set; get; }
            public decimal JlhPotKoperasi { set; get; }
            public decimal JlhPotPegawai { set; get; }
            public string PrintBy { get; set; }

            //VIR
            public string ADType { get; set; }
            public decimal TunjPerum { get; set; }
            public decimal TunjJabatan { get; set; }
            public decimal Upah { get; set; }

            public decimal TunjTransport { get; set; }
            public decimal TunjKinerja { get; set; }
            public decimal TunjProyek { get; set; }
            public decimal TunjZona { get; set; }
            public decimal TunjKeahlian { get; set; }
            public decimal Lembur { get; set; }
            public decimal KekuranganGaji { get; set; }
            public decimal Subtotal { get; set; }

            public decimal FixDeduction { get; set; }
            public decimal AngsuranPinjaman { get; set; }
            public decimal IuranKaryawan { get; set; }
            public decimal TabunganQurban { get; set; }
            public decimal OTAmtAdjustment { get; set; }
        }

        class PaySlipMMM
        {
            public string CompanyLogo { get; set; }

            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string EmCode { set; get; }
            public string EmpName { get; set; }

            public string DeptName { get; set; }
            public string SiteName { get; set; }
            public decimal Salary { get; set; }
            public decimal Functional { get; set; }
            public decimal SSEmployerHealth { get; set; }

            public decimal SSEmployeeHealth { get; set; }
            public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployeeEmployment { get; set; }
            public decimal Amt { get; set; }
            public string Periode { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal PerformanceValue { get; set; }
            public decimal AmtAll { get; set; }
            public decimal AmtDed { get; set; }
            public string PosName { get; set; }

            public decimal ProcessUPLAmt { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public decimal SalaryAdjustment { get; set; }
            public decimal Transport { get; set; }
            public decimal Meal { get; set; }
            public decimal ADOT { get; set; }
            public decimal FixAllowance { get; set; }
            public decimal FixDeduction { get; set; }
            public decimal EmpAdvancePayment { get; set; }
            public decimal SSEePension { get; set; }
            public decimal tax { get; set; }
            public decimal taxallowance { get; set; }
            public decimal SSEmpHealth { get; set; }
            public string CreditName { get; set; }
            public string AdvancePay { get; set; }
            public decimal AdvancePayment { get; set; }
            public string Payruncode { get; set; }
            public string PrintBy { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal TransportHr { get; set; }
            public decimal MealHr { get; set; }
            public string Periode2 { get; set; }
            public decimal AmtTambahan { get; set; }
            public decimal SSEeDPLK { get; set; }
            public decimal SSErDPLK { get; set; }
            public decimal VarAllowance { get; set; }
            public string Terbilang { get; set; }
            public decimal IncEmployee { get; set; }
            public string DeptCode { get; set; }
            public string Bank { get; set; }
            public string BankAccount { get; set; }
            public decimal ShiftAllowance { get; set; }
            public decimal UTD { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ADleave { get; set; }
            public decimal AmtInsPntUTD { get; set; }
            public string EmpCodeOld { get; set; }
            public string Level { get; set; }
            public string TerbilangAmtUpahDeduction { get; set; }
            public decimal TerbilangAmtUpahDeductionDec { get; set; }
            public string EmpCodeSincerely { get; set; }
            public decimal TotalAmtAllowance { get; set; }
            public string SectionCode { get; set; }
            public string SectionName { get; set; }
            public string Signature { get; set; }
            public string WorkingDay { get; set; }
            public string Absen { get; set; }
            public string Izin { get; set; }
            public string Cuti { get; set; }
            public string OTAmt { get; set; }
            public string IzinSKD { get; set; }
            public string IzinNonSKD { get; set; }
        }

        class EmpAllowance
        {
            public decimal No { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalAmt { get; set; }
            public string ADType { get; set; }
            public string CurCode { get; set; }
            public string Terbilang { get; set; }

        }

        class EmpDeduction
        {
            public decimal No { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalAmt { get; set; }
            public string ADType { get; set; }
            public string CurCode { get; set; }
            public string Terbilang { get; set; }
            public decimal AmtUpah { get; set; }
            public string Terbilang2 { get; set; }
        }

        class EmployeeSincerely
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
        }

        class OTAllowance
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public string ADType { get; set; }
            public string CurCode { get; set; }
            public decimal Duration { get; set; }
        }

        #region Old Code
        //class EmpFixedAllowanceVIR
        //{
        //    public string PayrunCode { get; set; }
        //    public string EmpCode { get; set; }
        //    public string ADType { get; set; }
        //    public decimal TunjPerum { get; set; }
        //    public decimal TunjJabatan { get; set; }
        //    public decimal Upah { get; set; }

        //}
        //class EmpNotFixedAllowanceVIR
        //{
        //    public string PayrunCode { get; set; }
        //    public string EmpCode { get; set; }
        //    public string ADType { get; set; }
        //    public decimal TunjTransport { get; set; }
        //    public decimal TunjKinerja { get; set; }
        //    public decimal TunjProyek { get; set; }
        //    public decimal TunjZona { get; set; }
        //    public decimal TunjKeahlian { get; set; }
        //    public decimal Lembur{ get; set; }
        //    public decimal KekuranganGaji { get; set; }
        //    public decimal Subtotal { get; set; }

        //}

        #endregion

        class EmpFixedAllowanceDGI
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public string ADType { get; set; }

        }

        class EmpNotFixedAllowanceDGI
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public string ADType { get; set; }
        }

        class ADCodeCompanyAllowance
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public string ADType { get; set; }
        }

        class ADCodePersonalAllowance
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public decimal Amt { get; set; }
            public string ADType { get; set; }
        }

        class EmpAdvancePayment
        {
            public decimal No { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string APCode { get; set; }
            public string APName { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        class EmpInsentiveOrPenalty
        {
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string InspntCode { get; set; }
            public string InspntName { get; set; }
            public decimal AmtInspnt { get; set; }
            public decimal TotalInsentif { get; set; }
        }
        #endregion
    }
}
