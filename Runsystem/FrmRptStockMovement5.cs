﻿#region Update
/*
    24/06/2020 [VIN/SIER] NEW APPS
    13/07/2020 [VIN/SIER] bug filter berdasarkan whs
 * 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockMovement5 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool
            mIsInventoryShowTotalQty = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsInventoryRptFilterByGrpWhs = false,
            mIsFilterByItCt = false,
            mIsInvTrnShowItSpec = false,
            mIsRptStockMovementShowProjectInfo = false;

        #endregion

        #region Constructor

        public FrmRptStockMovement5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCatCode, mIsFilterByItCt ? "Y" : "N");
                if (mIsInventoryRptFilterByGrpWhs)
                    Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                else
                    Sl.SetLueWhsCode(ref LueWhsCode);
                SetLueTransType(ref LueDocType, string.Empty);
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryRptFilterByGrpWhs = Sm.GetParameterBoo("IsInventoryRptFilterByGrpWhs");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsInvTrnShowItSpec = Sm.GetParameterBoo("IsInvTrnShowItSpec");
            mIsRptStockMovementShowProjectInfo = Sm.GetParameterBoo("IsRptStockMovementShowProjectInfo");
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type",   
                        "Document#",
                        "Date",
                        "Warehouse",
                        "Lot",

                        //6-10
                        "Bin",
                        "Item's Code",
                        "Item's Name",
                        "Foreign Name",
                        "Property",
                        
                        //11-15
                        "Batch#",
                        "Source",
                        "Quantity", 
                        "UoM",                     
                        "Quantity", 
                        
                        //16-20
                        "UoM",
                        "Quantity", 
                        "UoM",
                        "Item Group"+Environment.NewLine+"Code",
                        "Item Group"+Environment.NewLine+"Name",

                        //21-25
                        "Sub-Category",
                        "Remark",
                        "Created By",
                        "Specification",
                        "Project Name",

                        //26-27
                        "Customer's PO#",
                        "Customer"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 150, 80, 200, 60, 
                        
                        //6-10
                        60, 80, 250, 230, 80,   
                        
                        //11-15
                        200, 180, 120, 100, 120,

                        //16-20
                        100, 120, 100, 100, 150,
                        
                        //21-25
                        150, 300, 200, 200, 200,

                        //26-27
                        130, 250
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 15, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 23 });
            if (mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 10, 12, 15, 16, 17, 18, 21 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 10, 12, 15, 16, 17, 18, 21 }, false);
            ShowInventoryUomCode();
            if (mIsItGrpCode) Sm.GrdColInvisible(Grd1, new int[] { 19, 20 }, false);
            Grd1.Cols[19].Move(10);
            Grd1.Cols[20].Move(11);
            Grd1.Cols[21].Move(12);
            if (mIsInvTrnShowItSpec)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 24 }, mIsInvTrnShowItSpec);
                Grd1.Cols[24].Move(9);
            }
            Sm.SetGrdProperty(Grd1, false);
            if (!mIsRptStockMovementShowProjectInfo) Sm.GrdColInvisible(Grd1, new int[] { 25, 26 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 10, 12, 21, 23 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, true);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var lsm = PrepareDataStockMovement();
                var li = PrepareDataItem();
                var lw = PrepareDataWarehouse();
                var lo = PrepareDataOption();
                var lp = PrepareDataProperty();


                if (lsm.Count > 0)
                {
                    var Rpt = (from A in lsm
                               join B in li on A.ItCode equals B.ItCode
                               join C in lw on A.WhsCode equals C.WhsCode
                               join D in lo on A.DocType equals D.OptCode
                               join E in lp on A.PropCode equals E.PropCode

                               select new { A, B, C, D, E }
                    ).Select(result =>
                    {
                        result.A.ItName = result.B.ItName;
                        result.A.ForeignName = result.B.ForeignName;
                        result.A.InventoryUomCode = result.B.InventoryUomCode;
                        result.A.InventoryUomCode2 = result.B.InventoryUomCode2;
                        result.A.InventoryUomCode3 = result.B.InventoryUomCode3;
                        result.A.WhsName = result.C.WhsName;
                        result.A.OptDesc = result.D.OptDesc;
                        result.A.PropName = result.E.PropName;
                        result.A.ItGrpCode = result.B.ItGrpCode;
                        result.A.ItGrpName = result.B.ItGrpName;
                        result.A.ItScName = result.B.ItScName;
                        result.A.Specification = result.B.Specification;

                        return result;
                    }).ToList();


                    li.Clear();
                    lw.Clear();
                    lo.Clear();
                    lp.Clear();


                    int Row = 0;
                    Grd1.BeginUpdate();
                    Grd1.Rows.Count = 0;
                    foreach (StockMovement i in lsm.OrderBy(x => x.DocDt).ThenBy(x => x.DocNo))
                    {
                        Grd1.Rows.Add();
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = i.OptDesc;
                        Grd1.Cells[Row, 2].Value = i.DocNo;
                        Grd1.Cells[Row, 3].Value = Sm.ConvertDate(i.DocDt);
                        Grd1.Cells[Row, 4].Value = i.WhsName;
                        Grd1.Cells[Row, 5].Value = i.Lot;
                        Grd1.Cells[Row, 6].Value = i.Bin;
                        Grd1.Cells[Row, 7].Value = i.ItCode;
                        Grd1.Cells[Row, 8].Value = i.ItName;
                        Grd1.Cells[Row, 9].Value = i.ForeignName;
                        Grd1.Cells[Row, 10].Value = i.PropName;
                        Grd1.Cells[Row, 11].Value = i.BatchNo;
                        Grd1.Cells[Row, 12].Value = i.Source;
                        Grd1.Cells[Row, 13].Value = i.Qty;
                        Grd1.Cells[Row, 14].Value = i.InventoryUomCode;
                        Grd1.Cells[Row, 15].Value = i.Qty2;
                        Grd1.Cells[Row, 16].Value = i.InventoryUomCode2;
                        Grd1.Cells[Row, 17].Value = i.Qty3;
                        Grd1.Cells[Row, 18].Value = i.InventoryUomCode3;
                        Grd1.Cells[Row, 19].Value = i.ItGrpCode;
                        Grd1.Cells[Row, 20].Value = i.ItGrpName;
                        Grd1.Cells[Row, 21].Value = i.ItScName;
                        Grd1.Cells[Row, 22].Value = i.Remark;
                        Grd1.Cells[Row, 23].Value = i.CreatedBy;
                        Grd1.Cells[Row, 24].Value = i.Specification;
                        Grd1.Cells[Row, 25].Value = i.ProjectName;
                        Grd1.Cells[Row, 26].Value = i.PONo;
                        Grd1.Cells[Row, 27].Value = i.CtName;

                        Row++;
                    }
                    if (ChkShowTotal.Checked)
                    {
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 13, 15, 17 });
                    }
                    Grd1.EndUpdate();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);

                lsm.Clear();
                li.Clear();
                lw.Clear();
                lo.Clear();
                lp.Clear();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private List<StockMovement> PrepareDataStockMovement()
        {
            string
                Filter = " ",
                DocTypeTemp = string.Empty, DocNoTemp = string.Empty, DNoTemp = string.Empty;
            decimal QtyTemp = 0m, Qty2Temp = 0m, Qty3Temp = 0m;
            bool IsCancelled = false, IsFilteredByItCtCode = Sm.GetLue(LueItCatCode).Length > 0;

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

            if (ChkItCatCode.Checked) Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCatCode));
            if (ChkItName.Checked) Sm.CmParam<String>(ref cm, "@ItName", "%" + TxtItName.Text + "%");
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "T.WhsCode", true);
            Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, "ItCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "DocType", true);
            Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "BatchNo", false);
            Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "Lot", false);
            Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "Bin", false);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T5.CtCode", true);


            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.CancelInd, T.DocType, T.DocNo, T.DNo, T.Qty, T.Qty2, T.Qty3, T.DocDt, ");
            SQL.AppendLine("T.WhsCode, T.Lot, T.Bin, T.ItCode, T.PropCode, T.BatchNo, T.Source, T.Remark, T.CreateBy, ");
            if (mIsRptStockMovementShowProjectInfo)
                SQL.AppendLine("T2.ProjectName, T3.Value1 as PONo ");
            else
                SQL.AppendLine("Null As ProjectName, Null As PONo ");
            SQL.AppendLine(", T5.CtName ");
            SQL.AppendLine("From TblStockMovement T ");
            if (mIsRptStockMovementShowProjectInfo)
            {
                SQL.AppendLine("Left Join TblProjectGroup T2 On T.BatchNo = T2.ProjectCode ");
                SQL.AppendLine("Left Join TblSourceInfo T3 On T.Source = T3.Source");
            }
            SQL.AppendLine("left Join TblStockInboundHdr T4 On T4.DocNo = T.DocNo ");
            SQL.AppendLine("left Join tblstockoutboundhdr T6 ON T6.DocNo = T.DocNo ");
            SQL.AppendLine("inner Join TblCustomer T5 On T5.CtCode = T4.CtCode OR T5.CtCode = T6.CtCode ");

            SQL.AppendLine("Where (T.DocDt>=@DocDt1 And T.DocDt<=@DocDt2) ");
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=T.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByItCt || ChkItCatCode.Checked || ChkItName.Checked)
            {
                SQL.AppendLine("And Exists(Select 1 From TblItem Tbl Where Tbl.ItCode=T.ItCode ");
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=Tbl.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                if (ChkItCatCode.Checked) SQL.AppendLine("And Tbl.ItCtCode=@ItCtCode ");
                if (ChkItName.Checked) SQL.AppendLine("And Tbl.ItName Like @ItName ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By T.DocType, T.DocNo, T.DNo, T.CancelInd Desc;");

            var l = new List<StockMovement>();

            Sm.ShowDataInCtrl(
                ref cm,
                SQL.ToString(),
                new string[]
                {
                    //0
                    "CancelInd", 

                    //1-5
                    "DocType", "DocNo", "DNo", "Qty", "Qty2", 

                    //6-10
                    "Qty3", "DocDt", "WhsCode", "Lot", "Bin", 

                    //11-15
                    "ItCode", "PropCode", "BatchNo", "Source", "Remark",

                    //18
                    "CreateBy", "ProjectName", "PONo", "CtName"
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   if (Sm.DrStr(dr, 0) == "Y")
                   {
                       IsCancelled = true;
                       DocTypeTemp = Sm.DrStr(dr, 1);
                       DocNoTemp = Sm.DrStr(dr, 2);
                       DNoTemp = Sm.DrStr(dr, 3);
                   }
                   else
                   {
                       QtyTemp = Sm.DrDec(dr, 4);
                       Qty2Temp = Sm.DrDec(dr, 5);
                       Qty3Temp = Sm.DrDec(dr, 6);
                       if (
                               (
                                   !IsCancelled ||
                                   (
                                       IsCancelled &&
                                           !(
                                               Sm.CompareStr(DocTypeTemp, Sm.DrStr(dr, 1)) &&
                                               Sm.CompareStr(DocNoTemp, Sm.DrStr(dr, 2)) &&
                                               Sm.CompareStr(DNoTemp, Sm.DrStr(dr, 3))
                                           )
                                   )
                               )
                               &&
                               (QtyTemp != 0 || Qty2Temp != 0 || Qty3Temp != 0)
                           )
                       {
                           DocTypeTemp = Sm.DrStr(dr, 1);
                           DocNoTemp = Sm.DrStr(dr, 2);
                           DNoTemp = Sm.DrStr(dr, 3);

                           l.Add(new StockMovement()
                           {
                               DocType = DocTypeTemp,
                               DocNo = DocNoTemp,
                               DNo = DNoTemp,
                               DocDt = Sm.DrStr(dr, 7),
                               WhsCode = Sm.DrStr(dr, 8),
                               Lot = Sm.DrStr(dr, 9),
                               Bin = Sm.DrStr(dr, 10),
                               ItCode = Sm.DrStr(dr, 11),
                               PropCode = Sm.DrStr(dr, 12),
                               BatchNo = Sm.DrStr(dr, 13),
                               Source = Sm.DrStr(dr, 14),
                               Qty = QtyTemp,
                               Qty2 = Qty2Temp,
                               Qty3 = Qty3Temp,
                               Remark = Sm.DrStr(dr, 15),
                               CreatedBy = Sm.DrStr(dr, 16),
                               ProjectName = Sm.DrStr(dr, 17),
                               PONo = Sm.DrStr(dr, 18),
                               CtName = Sm.DrStr(dr, 19),

                           });
                       }
                       DocTypeTemp = string.Empty;
                       DocNoTemp = string.Empty;
                       DNoTemp = string.Empty;
                       QtyTemp = 0m;
                       Qty2Temp = 0m;
                       Qty3Temp = 0m;
                       IsCancelled = false;
                   }
               }, false
            );
            return l;
        }

        private List<Warehouse> PrepareDataWarehouse()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            var SQL = new StringBuilder();
            var l = new List<Warehouse>();

            SQL.AppendLine("Select T.WhsCode, T.WhsName From TblWarehouse T ");
            if (mIsInventoryRptFilterByGrpWhs)
            {
                SQL.AppendLine("    Where Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("        Where WhsCode=T.WhsCode ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine(";");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(), new string[] { "WhsCode", "WhsName" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Warehouse()
                        {
                            WhsCode = Sm.DrStr(dr, 0),
                            WhsName = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Customer> PrepareDataCustomer()
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            var SQL = new StringBuilder();
            var l = new List<Customer>();

            SQL.AppendLine("Select T.CtCode, T.CtName From TblCustomer T ");
           
            SQL.AppendLine(";");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(), new string[] { "CtCode", "CtName" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Customer()
                        {
                            CtCode = Sm.DrStr(dr, 0),
                            CtName = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }


        private List<Property> PrepareDataProperty()
        {
            var cm = new MySqlCommand();
            var l = new List<Property>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select PropCode, PropName From TblProperty Union All Select '-' As PropCode, '-' As PropName;",
                new string[] { "PropCode", "PropName" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Property()
                        {
                            PropCode = Sm.DrStr(dr, 0),
                            PropName = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Option> PrepareDataOption()
        {
            var cm = new MySqlCommand();
            var l = new List<Option>();
            Sm.ShowDataInCtrl(
                ref cm,
                "Select OptCode, OptDesc From TblOption Where OptCat='InventoryTransType'  ;",
                new string[] { "OptCode", "OptDesc" },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Option()
                        {
                            OptCode = Sm.DrStr(dr, 0),
                            OptDesc = Sm.DrStr(dr, 1)
                        }
                    );
               }, false
            );
            return l;
        }

        private List<Item> PrepareDataItem()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var l = new List<Item>();
            string Filter = string.Empty;

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (ChkItCatCode.Checked) Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCatCode));
            if (ChkItName.Checked) Sm.CmParam<String>(ref cm, "@ItName", "%" + TxtItName.Text + "%");

            SQL.AppendLine("Select A.ItCode, A.ItName, A.ForeignName, A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, A.ItGrpCode, B.ItGrpName, C.ItScName, A.Specification ");
            SQL.AppendLine("From TblItem A ");
            SQL.AppendLine("Left Join TblItemGroup B On A.ItGrpCode = B.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubCategory C On A.ItScCode = C.ItScCode ");
            SQL.AppendLine("Where 0=0 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (ChkItCatCode.Checked || ChkItName.Checked)
            {
                if (ChkItCatCode.Checked) SQL.AppendLine("And A.ItCtCode=@ItCtCode ");
                if (ChkItName.Checked) SQL.AppendLine("And A.ItName Like @ItName ");
            }

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    "ItCode", 
                    "ItName", "ForeignName", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3", 
                    "ItGrpCode", "ItGrpName", "ItScName", "Specification" 
                },
               (MySqlDataReader dr, int[] c) =>
               {
                   l.Add(
                        new Item()
                        {
                            ItCode = Sm.DrStr(dr, 0),
                            ItName = Sm.DrStr(dr, 1),
                            ForeignName = Sm.DrStr(dr, 2),
                            InventoryUomCode = Sm.DrStr(dr, 3),
                            InventoryUomCode2 = Sm.DrStr(dr, 4),
                            InventoryUomCode3 = Sm.DrStr(dr, 5),
                            ItGrpCode = Sm.DrStr(dr, 6),
                            ItGrpName = Sm.DrStr(dr, 7),
                            ItScName = Sm.DrStr(dr, 8),
                            Specification = Sm.DrStr(dr, 9),
                        }
                    );
               }, false
            );
            return l;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        private void SetLueTransType(ref DXE.LookUpEdit Lue, string DocType)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='InventoryTransType' AND optcode IN ('52','53') Order By OptCode ");

            Sm.SetLue2(
               ref Lue,
               SQL.ToString(),
               0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsInventoryRptFilterByGrpWhs)
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            else
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0)
                DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueItCatCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCatCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCatCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's Category");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueTransType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item name");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");

        }

        #endregion

        #region Class

        private class StockMovement
        {
            public string DocType { get; set; }
            public string OptDesc { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string PropCode { get; set; }
            public string PropName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItScName { get; set; }
            public string Remark { get; set; }
            public string CreatedBy { get; set; }
            public string Specification { get; set; }
            public string ProjectName { get; set; }
            public string PONo { get; set; }
            public string CtCode { get; set; }
            public string CtName { get; set; }


        }

        private class Warehouse
        {
            public string WhsCode { get; set; }
            public string WhsName { get; set; }
        }

        private class Customer
        {
            public string CtCode { get; set; }
            public string CtName { get; set; }
        }

        private class Item
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string InventoryUomCode { get; set; }
            public string InventoryUomCode2 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string ItGrpCode { get; set; }
            public string ItGrpName { get; set; }
            public string ItScName { get; set; }
            public string Specification { get; set; }

        }

        private class Option
        {
            public string OptCode { get; set; }
            public string OptDesc { get; set; }
        }

        private class Property
        {
            public string PropCode { get; set; }
            public string PropName { get; set; }
        }

        #endregion

        
    }
}
