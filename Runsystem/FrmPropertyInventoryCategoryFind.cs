﻿#region Update
/*
    30/11/2022 [IBL/BBT] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCategoryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPropertyInventoryCategory mFrmParent;
        private string mSQL = string.Empty;
        #endregion

        #region Constructor

        public FrmPropertyInventoryCategoryFind(FrmPropertyInventoryCategory FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropertyCategoryCode, A.PropertyCategoryName, A.ActInd, A.InitialCategoryInd, A.SalesCategoryInd, A.RentCategoryInd, ");
            SQL.AppendLine("A.MandatoryCertificateInd, A.AcNo1, A.AcNo2, A.AcNo3, ");
            if (mFrmParent.mIsCOAUseAlias)
            {
                SQL.AppendLine("Concat(B.AcDesc, Case When B.Alias Is Null Then '' Else Concat(' [', B.Alias, ']') End) As AcDesc1, ");
                SQL.AppendLine("Concat(C.AcDesc, Case When C.Alias Is Null Then '' Else Concat(' [', C.Alias, ']') End) As AcDesc2, ");
                SQL.AppendLine("Concat(D.AcDesc, Case When D.Alias Is Null Then '' Else Concat(' [', D.Alias, ']') End) As AcDesc3, ");
            }
            else
            {
                SQL.AppendLine("B.AcDesc As AcDesc1, C.AcDesc As AcDesc2, D.AcDesc As AcDesc3, ");
            }
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblPropertyInventoryCategory A ");
            SQL.AppendLine("Inner Join TblCOA B On A.AcNo1 = B.AcNo ");
            SQL.AppendLine("Left Join TblCOA C On A.AcNo2 = C.AcNo ");
            SQL.AppendLine("Left Join TblCOA D On A.AcNo3 = D.AcNo ");

            mSQL = SQL.ToString();
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Property"+Environment.NewLine+"Category Code", 
                        "Property "+Environment.NewLine+"Category Name",
                        "Active",
                        "Initial",
                        "Sales",
                        
                        //6-10
                        "Rent",
                        "Certificate",
                        "COA#"+Environment.NewLine+"(Inventory)",
                        "COA Description"+Environment.NewLine+"(Inventory)",
                        "COA#"+Environment.NewLine+"(Sales)",
                        
                        //11-15
                        "COA Description"+Environment.NewLine+"(Sales)",
                        "COA#"+Environment.NewLine+"(COGS)",
                        "COA Description"+Environment.NewLine+"(COGS)",
                        "Created By",
                        "Created Date",
                       
                        //16 - 19
                        "Created Time",
                        "Last Updated By", 
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    { 
                        //0
                        50, 
                        //1-5
                        130, 250, 60, 60, 60, 
                        //6-10
                        60, 60, 100, 200, 100, 
                        //11-15
                        200, 100, 200, 130, 130,
                        //16-19
                        130, 130, 130, 130, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColCheck(Grd1, new int[] { 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPropCtName.Text, new string[] { "PropertyCategoryCode", "PropertyCategoryName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By PropertyCategoryName;",
                    new string[]
                    {
                        //0
                        "PropertyCategoryCode", 
                            
                        //1-5
                        "PropertyCategoryName", "ActInd", "InitialCategoryInd", "SalesCategoryInd", "RentCategoryInd", 
                        
                        //6-10
                        "MandatoryCertificateInd", "AcNo1", "AcDesc1", "AcNo2", "AcDesc2", 
                        
                        //11-15
                        "AcNo3", "AcDesc3", "CreateBy", "CreateDt", "LastUpBy",
                        
                        //16
                        "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);

                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Button Method

        override protected void ExportToExcel()
        {
            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = "'" + Sm.GetGrdStr(Grd1, Row, 1);
            }
            Grd1.EndUpdate();

            Sm.ExportToExcel(Grd1);

            Grd1.BeginUpdate();
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                Grd1.Cells[Row, 1].Value = Sm.Right(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 1).Length - 1);
            }
            Grd1.EndUpdate();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPropCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPropCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Property Category Name ");
        }

        #endregion

        #endregion
    }
}
