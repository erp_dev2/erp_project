﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmAtd2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmAtd2 mFrmParent;
        string mSQL="";

        #endregion

        #region Constructor

        public FrmAtd2Find(FrmAtd2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, B.Deptname, C.AGName, A.StartDt, A.EndDt,  A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblAttendanceGrpHdr C On A.AGCode=C.AgCode ");
            SQL.AppendLine("Where A.TypeInd = '2' ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document"+Environment.NewLine+"Number",
                        "Document"+Environment.NewLine+"Date",
                        "Cancel",
                        "Department",
                        "Attendance"+Environment.NewLine+"Group",
                        //6-10
                        "Start Date",
                        "End Date",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        //11-12
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 50, 150, 100,   
                         //6-10
                        100, 100, 100, 80, 80,
                        //11-12
                        80, 80, 80,
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 6, 7, 9, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                #region Old
                //Sm.ShowDataInGrid(
                //        ref Grd1, ref cm,
                //        mSQL + Filter + " Order By A.DocNo ",
                //        new string[]
                //        {
                //            //0
                //            "DocNo",

                //            //1-5
                //            "DocDt", "EmpCode", "EmpName", "Dt", "DeptName", 

                //            //6-10
                //            "LIn", "LOut", "AIn", "AOut", "CreateBy",  

                //            //15-17
                //            "CreateDt", "LastUpBy", "LastUpDt"
                //        },
                //        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                //        {
                //            Grd1.Cells[Row, 0].Value = Row + 1;
                //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                //            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                //            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 4);
                //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 8, 6);
                //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 9, 7);
                //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 10, 8);
                //            Sm.SetGrdValue("T2", Grd1, dr, c, Row, 11, 9);
                //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);
                //            Sm.SetGrdValue("D", Grd1, dr, c, Row, 13, 11);
                //            Sm.SetGrdValue("T", Grd1, dr, c, Row, 14, 11);
                //            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 12);
                //            Sm.SetGrdValue("D", Grd1, dr, c, Row, 16, 13);
                //            Sm.SetGrdValue("T", Grd1, dr, c, Row, 17, 13);
                //        }, true, true, false, false
                //    );
                #endregion

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo ",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "DeptName", "AGName", "StartDt",    
                            //6-10
                            "EndDt", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 7, 6);

                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            //e.DoDefault = false;
            //if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            //{
            //    var f = new FrmEmployee(mFrmParent.mMenuCode);
            //    f.Tag = mFrmParent.mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
            //    f.ShowDialog();
            //}
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            //if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            //{
            //    var f = new FrmEmployee(mFrmParent.mMenuCode);
            //    f.Tag = mFrmParent.mMenuCode;
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
            //    f.ShowDialog();
            //}
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

       

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document Number");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }



        #endregion
    }
}
