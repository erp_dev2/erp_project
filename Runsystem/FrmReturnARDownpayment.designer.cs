﻿namespace RunSystem
{
    partial class FrmReturnARDownpayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReturnARDownpayment));
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtEntCode = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnVoucherDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnCtCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.BtnVoucherRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtSummary = new DevExpress.XtraEditors.TextEdit();
            this.TxtVoucherRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LblDeptCode = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueAcNoType = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.LblSettledAmt = new System.Windows.Forms.Label();
            this.TxtSettledAmt = new DevExpress.XtraEditors.TextEdit();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblBankAcCode = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtReturnAmt = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.Label11 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LuePIC = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.iGrid1 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.memoExEdit1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.memoExEdit2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.memoExEdit3 = new DevExpress.XtraEditors.MemoExEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.lookUpEdit5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.lookUpEdit6 = new DevExpress.XtraEditors.LookUpEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.dateEdit3 = new DevExpress.XtraEditors.DateEdit();
            this.label78 = new System.Windows.Forms.Label();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.label81 = new System.Windows.Forms.Label();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit7 = new DevExpress.XtraEditors.LookUpEdit();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.label84 = new System.Windows.Forms.Label();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.label85 = new System.Windows.Forms.Label();
            this.dateEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.label86 = new System.Windows.Forms.Label();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit8 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label87 = new System.Windows.Forms.Label();
            this.lookUpEdit10 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.label88 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.iGrid2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.dateEdit5 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit11 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit12 = new DevExpress.XtraEditors.LookUpEdit();
            this.iGrid3 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.label91 = new System.Windows.Forms.Label();
            this.lookUpEdit14 = new DevExpress.XtraEditors.LookUpEdit();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.memoExEdit4 = new DevExpress.XtraEditors.MemoExEdit();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.lookUpEdit15 = new DevExpress.XtraEditors.LookUpEdit();
            this.label96 = new System.Windows.Forms.Label();
            this.dateEdit6 = new DevExpress.XtraEditors.DateEdit();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.lookUpEdit16 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label99 = new System.Windows.Forms.Label();
            this.lookUpEdit18 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel14 = new System.Windows.Forms.Panel();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.label100 = new System.Windows.Forms.Label();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.label101 = new System.Windows.Forms.Label();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.label102 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.iGrid4 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.iGrid5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgCOAAccount = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgApprovalInformation = new System.Windows.Forms.TabPage();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.TxtCtCtName = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSummary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcNoType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSettledAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReturnAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid3)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid5)).BeginInit();
            this.TpgCOAAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgApprovalInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCtName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(838, 0);
            this.panel1.Size = new System.Drawing.Size(70, 433);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtCtCtName);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.TxtCurCode);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.TxtEntCode);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TxtCtCode);
            this.panel2.Controls.Add(this.BtnVoucherDocNo);
            this.panel2.Controls.Add(this.BtnCtCode);
            this.panel2.Controls.Add(this.TxtVoucherDocNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.BtnVoucherRequestDocNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtSummary);
            this.panel2.Controls.Add(this.TxtVoucherRequestDocNo);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LblDeptCode);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(838, 433);
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(142, 194);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 16;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(91, 20);
            this.TxtCurCode.TabIndex = 32;
            // 
            // TxtEntCode
            // 
            this.TxtEntCode.EnterMoveNextControl = true;
            this.TxtEntCode.Location = new System.Drawing.Point(142, 173);
            this.TxtEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntCode.Name = "TxtEntCode";
            this.TxtEntCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEntCode.Properties.MaxLength = 80;
            this.TxtEntCode.Properties.ReadOnly = true;
            this.TxtEntCode.Size = new System.Drawing.Size(284, 20);
            this.TxtEntCode.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(99, 176);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Entity";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(142, 131);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 80;
            this.TxtCtCode.Properties.ReadOnly = true;
            this.TxtCtCode.Size = new System.Drawing.Size(284, 20);
            this.TxtCtCode.TabIndex = 25;
            this.TxtCtCode.EditValueChanged += new System.EventHandler(this.TxtCtCode_EditValueChanged);
            // 
            // BtnVoucherDocNo
            // 
            this.BtnVoucherDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucherDocNo.Image")));
            this.BtnVoucherDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucherDocNo.Location = new System.Drawing.Point(355, 107);
            this.BtnVoucherDocNo.Name = "BtnVoucherDocNo";
            this.BtnVoucherDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnVoucherDocNo.TabIndex = 23;
            this.BtnVoucherDocNo.ToolTip = "Find Item";
            this.BtnVoucherDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherDocNo.Click += new System.EventHandler(this.BtnVoucherDocNo_Click);
            // 
            // BtnCtCode
            // 
            this.BtnCtCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtCode.Appearance.Options.UseBackColor = true;
            this.BtnCtCode.Appearance.Options.UseFont = true;
            this.BtnCtCode.Appearance.Options.UseForeColor = true;
            this.BtnCtCode.Appearance.Options.UseTextOptions = true;
            this.BtnCtCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtCode.Image")));
            this.BtnCtCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtCode.Location = new System.Drawing.Point(428, 129);
            this.BtnCtCode.Name = "BtnCtCode";
            this.BtnCtCode.Size = new System.Drawing.Size(24, 21);
            this.BtnCtCode.TabIndex = 26;
            this.BtnCtCode.ToolTip = "Find Vendor";
            this.BtnCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtCode.ToolTipTitle = "Run System";
            this.BtnCtCode.Click += new System.EventHandler(this.BtnCtCode_Click);
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(142, 110);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 30;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(213, 20);
            this.TxtVoucherDocNo.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(76, 114);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Voucher#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnVoucherRequestDocNo
            // 
            this.BtnVoucherRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucherRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucherRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucherRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnVoucherRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnVoucherRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucherRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucherRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucherRequestDocNo.Image")));
            this.BtnVoucherRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucherRequestDocNo.Location = new System.Drawing.Point(355, 89);
            this.BtnVoucherRequestDocNo.Name = "BtnVoucherRequestDocNo";
            this.BtnVoucherRequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnVoucherRequestDocNo.TabIndex = 20;
            this.BtnVoucherRequestDocNo.ToolTip = "Find Voucher Request";
            this.BtnVoucherRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucherRequestDocNo.ToolTipTitle = "Run System";
            this.BtnVoucherRequestDocNo.Click += new System.EventHandler(this.BtnVoucherRequestDocNo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 216);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 14);
            this.label4.TabIndex = 33;
            this.label4.Text = "Downpayment Amount";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSummary
            // 
            this.TxtSummary.EnterMoveNextControl = true;
            this.TxtSummary.Location = new System.Drawing.Point(142, 215);
            this.TxtSummary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSummary.Name = "TxtSummary";
            this.TxtSummary.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSummary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSummary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSummary.Properties.Appearance.Options.UseFont = true;
            this.TxtSummary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSummary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSummary.Properties.ReadOnly = true;
            this.TxtSummary.Size = new System.Drawing.Size(169, 20);
            this.TxtSummary.TabIndex = 34;
            // 
            // TxtVoucherRequestDocNo
            // 
            this.TxtVoucherRequestDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo.Location = new System.Drawing.Point(142, 89);
            this.TxtVoucherRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo.Name = "TxtVoucherRequestDocNo";
            this.TxtVoucherRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo.Properties.MaxLength = 30;
            this.TxtVoucherRequestDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo.Size = new System.Drawing.Size(213, 20);
            this.TxtVoucherRequestDocNo.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(27, 93);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 14);
            this.label8.TabIndex = 18;
            this.label8.Text = "Voucher Request#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(83, 195);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 14);
            this.label3.TabIndex = 31;
            this.label3.Text = "Currency";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDeptCode
            // 
            this.LblDeptCode.AutoSize = true;
            this.LblDeptCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode.ForeColor = System.Drawing.Color.Red;
            this.LblDeptCode.Location = new System.Drawing.Point(79, 134);
            this.LblDeptCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode.Name = "LblDeptCode";
            this.LblDeptCode.Size = new System.Drawing.Size(59, 14);
            this.LblDeptCode.TabIndex = 24;
            this.LblDeptCode.Text = "Customer";
            this.LblDeptCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 71);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 14);
            this.label14.TabIndex = 15;
            this.label14.Text = "Reason For Cancellation";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(142, 68);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(245, 20);
            this.MeeCancelReason.TabIndex = 16;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(142, 47);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(115, 20);
            this.TxtStatus.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(96, 49);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 14);
            this.label17.TabIndex = 13;
            this.label17.Text = "Status";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(390, 67);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 17;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(142, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(115, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(105, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(142, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(245, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(65, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.LueAcNoType);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.LblSettledAmt);
            this.panel4.Controls.Add(this.TxtSettledAmt);
            this.panel4.Controls.Add(this.LueBankAcCode);
            this.panel4.Controls.Add(this.LuePaymentType);
            this.panel4.Controls.Add(this.LblBankAcCode);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.LueBankCode);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtReturnAmt);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.Label11);
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.TxtGiroNo);
            this.panel4.Controls.Add(this.DteDueDt);
            this.panel4.Controls.Add(this.LueDeptCode);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.LuePIC);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(464, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(374, 258);
            this.panel4.TabIndex = 35;
            // 
            // LueAcNoType
            // 
            this.LueAcNoType.EnterMoveNextControl = true;
            this.LueAcNoType.Location = new System.Drawing.Point(123, 233);
            this.LueAcNoType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcNoType.Name = "LueAcNoType";
            this.LueAcNoType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcNoType.Properties.Appearance.Options.UseFont = true;
            this.LueAcNoType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcNoType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcNoType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcNoType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcNoType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcNoType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcNoType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcNoType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcNoType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcNoType.Properties.DropDownRows = 25;
            this.LueAcNoType.Properties.NullText = "[Empty]";
            this.LueAcNoType.Properties.PopupWidth = 350;
            this.LueAcNoType.Size = new System.Drawing.Size(244, 20);
            this.LueAcNoType.TabIndex = 57;
            this.LueAcNoType.ToolTip = "F4 : Show/hide list";
            this.LueAcNoType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcNoType.EditValueChanged += new System.EventHandler(this.LueAcNoType_EditValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(85, 236);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 14);
            this.label19.TabIndex = 56;
            this.label19.Text = "Type";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSettledAmt
            // 
            this.LblSettledAmt.AutoSize = true;
            this.LblSettledAmt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSettledAmt.ForeColor = System.Drawing.Color.Black;
            this.LblSettledAmt.Location = new System.Drawing.Point(25, 8);
            this.LblSettledAmt.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSettledAmt.Name = "LblSettledAmt";
            this.LblSettledAmt.Size = new System.Drawing.Size(95, 14);
            this.LblSettledAmt.TabIndex = 35;
            this.LblSettledAmt.Text = "Settled Amount";
            this.LblSettledAmt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSettledAmt
            // 
            this.TxtSettledAmt.EnterMoveNextControl = true;
            this.TxtSettledAmt.Location = new System.Drawing.Point(123, 5);
            this.TxtSettledAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSettledAmt.Name = "TxtSettledAmt";
            this.TxtSettledAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSettledAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSettledAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSettledAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtSettledAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSettledAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSettledAmt.Size = new System.Drawing.Size(169, 20);
            this.TxtSettledAmt.TabIndex = 36;
            this.TxtSettledAmt.Validated += new System.EventHandler(this.TxtSettledAmt_Validated);
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(123, 47);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 25;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 350;
            this.LueBankAcCode.Size = new System.Drawing.Size(244, 20);
            this.LueBankAcCode.TabIndex = 40;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(123, 68);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 25;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 350;
            this.LuePaymentType.Size = new System.Drawing.Size(244, 20);
            this.LuePaymentType.TabIndex = 42;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // LblBankAcCode
            // 
            this.LblBankAcCode.AutoSize = true;
            this.LblBankAcCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankAcCode.ForeColor = System.Drawing.Color.Red;
            this.LblBankAcCode.Location = new System.Drawing.Point(37, 50);
            this.LblBankAcCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankAcCode.Name = "LblBankAcCode";
            this.LblBankAcCode.Size = new System.Drawing.Size(83, 14);
            this.LblBankAcCode.TabIndex = 39;
            this.LblBankAcCode.Text = "Bank Account";
            this.LblBankAcCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(33, 72);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(87, 14);
            this.label21.TabIndex = 41;
            this.label21.Text = "Payment Type";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(123, 89);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 25;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 350;
            this.LueBankCode.Size = new System.Drawing.Size(244, 20);
            this.LueBankCode.TabIndex = 44;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Green;
            this.label13.Location = new System.Drawing.Point(123, 216);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(241, 14);
            this.label13.TabIndex = 55;
            this.label13.Text = "Remark for Voucher Request\'s Description.";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(14, 29);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 14);
            this.label5.TabIndex = 37;
            this.label5.Text = "Returned Amount";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(47, 177);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 14);
            this.label7.TabIndex = 51;
            this.label7.Text = "Department";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtReturnAmt
            // 
            this.TxtReturnAmt.EnterMoveNextControl = true;
            this.TxtReturnAmt.Location = new System.Drawing.Point(123, 26);
            this.TxtReturnAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReturnAmt.Name = "TxtReturnAmt";
            this.TxtReturnAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtReturnAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReturnAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReturnAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtReturnAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtReturnAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtReturnAmt.Size = new System.Drawing.Size(169, 20);
            this.TxtReturnAmt.TabIndex = 38;
            this.TxtReturnAmt.Validated += new System.EventHandler(this.TxtReturnAmt_Validated);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(123, 194);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(244, 20);
            this.MeeRemark.TabIndex = 54;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.ForeColor = System.Drawing.Color.Red;
            this.Label11.Location = new System.Drawing.Point(73, 197);
            this.Label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(47, 14);
            this.Label11.TabIndex = 53;
            this.Label11.Text = "Remark";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(52, 93);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 14);
            this.label18.TabIndex = 43;
            this.label18.Text = "Bank Name";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(4, 113);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Giro Bilyet / Cheque";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(123, 110);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(244, 20);
            this.TxtGiroNo.TabIndex = 46;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(123, 131);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.MaxLength = 8;
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(115, 20);
            this.DteDueDt.TabIndex = 48;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(123, 173);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 25;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 350;
            this.LueDeptCode.Size = new System.Drawing.Size(244, 20);
            this.LueDeptCode.TabIndex = 52;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(61, 134);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 14);
            this.label16.TabIndex = 47;
            this.label16.Text = "Due Date";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePIC
            // 
            this.LuePIC.EnterMoveNextControl = true;
            this.LuePIC.Location = new System.Drawing.Point(123, 152);
            this.LuePIC.Margin = new System.Windows.Forms.Padding(5);
            this.LuePIC.Name = "LuePIC";
            this.LuePIC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.Appearance.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePIC.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePIC.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePIC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePIC.Properties.DropDownRows = 25;
            this.LuePIC.Properties.NullText = "[Empty]";
            this.LuePIC.Properties.PopupWidth = 350;
            this.LuePIC.Size = new System.Drawing.Size(244, 20);
            this.LuePIC.TabIndex = 50;
            this.LuePIC.ToolTip = "F4 : Show/hide list";
            this.LuePIC.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePIC.EditValueChanged += new System.EventHandler(this.LuePIC_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(19, 156);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 14);
            this.label9.TabIndex = 49;
            this.label9.Text = "Person In Charge";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.iGrid1);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(911, 258);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "Vendor\'s Deposit";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // iGrid1
            // 
            this.iGrid1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid1.DefaultRow.Height = 20;
            this.iGrid1.DefaultRow.Sortable = false;
            this.iGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid1.Header.Height = 19;
            this.iGrid1.Location = new System.Drawing.Point(0, 0);
            this.iGrid1.Name = "iGrid1";
            this.iGrid1.RowHeader.Visible = true;
            this.iGrid1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid1.SingleClickEdit = true;
            this.iGrid1.Size = new System.Drawing.Size(911, 258);
            this.iGrid1.TabIndex = 49;
            this.iGrid1.TreeCol = null;
            this.iGrid1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage4.Controls.Add(this.memoExEdit1);
            this.tabPage4.Controls.Add(this.label60);
            this.tabPage4.Controls.Add(this.memoExEdit2);
            this.tabPage4.Controls.Add(this.label61);
            this.tabPage4.Controls.Add(this.memoExEdit3);
            this.tabPage4.Controls.Add(this.label62);
            this.tabPage4.Controls.Add(this.label63);
            this.tabPage4.Controls.Add(this.lookUpEdit4);
            this.tabPage4.Controls.Add(this.label64);
            this.tabPage4.Controls.Add(this.lookUpEdit5);
            this.tabPage4.Controls.Add(this.label65);
            this.tabPage4.Controls.Add(this.lookUpEdit6);
            this.tabPage4.Controls.Add(this.label66);
            this.tabPage4.Controls.Add(this.label67);
            this.tabPage4.Controls.Add(this.label68);
            this.tabPage4.Controls.Add(this.textEdit2);
            this.tabPage4.Controls.Add(this.textEdit3);
            this.tabPage4.Controls.Add(this.textEdit4);
            this.tabPage4.Controls.Add(this.label69);
            this.tabPage4.Controls.Add(this.dateEdit1);
            this.tabPage4.Controls.Add(this.label70);
            this.tabPage4.Controls.Add(this.label71);
            this.tabPage4.Controls.Add(this.textEdit5);
            this.tabPage4.Controls.Add(this.label72);
            this.tabPage4.Controls.Add(this.label73);
            this.tabPage4.Controls.Add(this.dateEdit2);
            this.tabPage4.Controls.Add(this.label74);
            this.tabPage4.Controls.Add(this.label75);
            this.tabPage4.Controls.Add(this.textEdit6);
            this.tabPage4.Controls.Add(this.label76);
            this.tabPage4.Controls.Add(this.label77);
            this.tabPage4.Controls.Add(this.panel5);
            this.tabPage4.Controls.Add(this.dateEdit4);
            this.tabPage4.Controls.Add(this.label86);
            this.tabPage4.Controls.Add(this.textEdit13);
            this.tabPage4.Controls.Add(this.textEdit14);
            this.tabPage4.Controls.Add(this.textEdit15);
            this.tabPage4.Controls.Add(this.lookUpEdit8);
            this.tabPage4.Controls.Add(this.lookUpEdit9);
            this.tabPage4.Controls.Add(this.label87);
            this.tabPage4.Controls.Add(this.lookUpEdit10);
            this.tabPage4.Controls.Add(this.textEdit16);
            this.tabPage4.Controls.Add(this.label88);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(911, 258);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Tax";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // memoExEdit1
            // 
            this.memoExEdit1.EnterMoveNextControl = true;
            this.memoExEdit1.Location = new System.Drawing.Point(414, 233);
            this.memoExEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit1.Name = "memoExEdit1";
            this.memoExEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit1.Properties.MaxLength = 400;
            this.memoExEdit1.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.memoExEdit1.Properties.ShowIcon = false;
            this.memoExEdit1.Size = new System.Drawing.Size(207, 20);
            this.memoExEdit1.TabIndex = 90;
            this.memoExEdit1.ToolTip = "F4 : Show/hide text";
            this.memoExEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit1.ToolTipTitle = "Run System";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(374, 235);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(34, 14);
            this.label60.TabIndex = 89;
            this.label60.Text = "Note";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit2
            // 
            this.memoExEdit2.EnterMoveNextControl = true;
            this.memoExEdit2.Location = new System.Drawing.Point(414, 150);
            this.memoExEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit2.Name = "memoExEdit2";
            this.memoExEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit2.Properties.MaxLength = 400;
            this.memoExEdit2.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.memoExEdit2.Properties.ShowIcon = false;
            this.memoExEdit2.Size = new System.Drawing.Size(207, 20);
            this.memoExEdit2.TabIndex = 76;
            this.memoExEdit2.ToolTip = "F4 : Show/hide text";
            this.memoExEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit2.ToolTipTitle = "Run System";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(374, 152);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(34, 14);
            this.label61.TabIndex = 75;
            this.label61.Text = "Note";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit3
            // 
            this.memoExEdit3.EnterMoveNextControl = true;
            this.memoExEdit3.Location = new System.Drawing.Point(414, 66);
            this.memoExEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit3.Name = "memoExEdit3";
            this.memoExEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit3.Properties.MaxLength = 400;
            this.memoExEdit3.Properties.PopupFormSize = new System.Drawing.Size(250, 20);
            this.memoExEdit3.Properties.ShowIcon = false;
            this.memoExEdit3.Size = new System.Drawing.Size(207, 20);
            this.memoExEdit3.TabIndex = 62;
            this.memoExEdit3.ToolTip = "F4 : Show/hide text";
            this.memoExEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit3.ToolTipTitle = "Run System";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(374, 68);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(34, 14);
            this.label62.TabIndex = 61;
            this.label62.Text = "Note";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(362, 215);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(46, 14);
            this.label63.TabIndex = 87;
            this.label63.Text = "Service";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(414, 212);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 25;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 200;
            this.lookUpEdit4.Size = new System.Drawing.Size(207, 20);
            this.lookUpEdit4.TabIndex = 88;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(362, 132);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(46, 14);
            this.label64.TabIndex = 73;
            this.label64.Text = "Service";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit5
            // 
            this.lookUpEdit5.EnterMoveNextControl = true;
            this.lookUpEdit5.Location = new System.Drawing.Point(414, 129);
            this.lookUpEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit5.Name = "lookUpEdit5";
            this.lookUpEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit5.Properties.DropDownRows = 25;
            this.lookUpEdit5.Properties.NullText = "[Empty]";
            this.lookUpEdit5.Properties.PopupWidth = 200;
            this.lookUpEdit5.Size = new System.Drawing.Size(207, 20);
            this.lookUpEdit5.TabIndex = 74;
            this.lookUpEdit5.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(362, 48);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(46, 14);
            this.label65.TabIndex = 59;
            this.label65.Text = "Service";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit6
            // 
            this.lookUpEdit6.EnterMoveNextControl = true;
            this.lookUpEdit6.Location = new System.Drawing.Point(414, 45);
            this.lookUpEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit6.Name = "lookUpEdit6";
            this.lookUpEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit6.Properties.DropDownRows = 25;
            this.lookUpEdit6.Properties.NullText = "[Empty]";
            this.lookUpEdit6.Properties.PopupWidth = 200;
            this.lookUpEdit6.Size = new System.Drawing.Size(207, 20);
            this.lookUpEdit6.TabIndex = 60;
            this.lookUpEdit6.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(59, 216);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(30, 14);
            this.label66.TabIndex = 85;
            this.label66.Text = "Alias";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(59, 133);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(30, 14);
            this.label67.TabIndex = 71;
            this.label67.Text = "Alias";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(59, 48);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(30, 14);
            this.label68.TabIndex = 57;
            this.label68.Text = "Alias";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit2
            // 
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(92, 213);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.MaxLength = 30;
            this.textEdit2.Size = new System.Drawing.Size(250, 20);
            this.textEdit2.TabIndex = 86;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(92, 129);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.MaxLength = 30;
            this.textEdit3.Size = new System.Drawing.Size(250, 20);
            this.textEdit3.TabIndex = 72;
            // 
            // textEdit4
            // 
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(92, 45);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.MaxLength = 30;
            this.textEdit4.Size = new System.Drawing.Size(250, 20);
            this.textEdit4.TabIndex = 58;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(357, 195);
            this.label69.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(51, 14);
            this.label69.TabIndex = 83;
            this.label69.Text = "Amount";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.EnterMoveNextControl = true;
            this.dateEdit1.Location = new System.Drawing.Point(414, 171);
            this.dateEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(107, 20);
            this.dateEdit1.TabIndex = 80;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(375, 175);
            this.label70.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(33, 14);
            this.label70.TabIndex = 79;
            this.label70.Text = "Date";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(62, 195);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(27, 14);
            this.label71.TabIndex = 81;
            this.label71.Text = "Tax";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(92, 171);
            this.textEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.MaxLength = 30;
            this.textEdit5.Size = new System.Drawing.Size(250, 20);
            this.textEdit5.TabIndex = 78;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(10, 174);
            this.label72.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(79, 14);
            this.label72.TabIndex = 77;
            this.label72.Text = "Tax Invoice#";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(357, 111);
            this.label73.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(51, 14);
            this.label73.TabIndex = 69;
            this.label73.Text = "Amount";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.EnterMoveNextControl = true;
            this.dateEdit2.Location = new System.Drawing.Point(414, 87);
            this.dateEdit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.Appearance.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit2.Size = new System.Drawing.Size(107, 20);
            this.dateEdit2.TabIndex = 66;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(375, 90);
            this.label74.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(33, 14);
            this.label74.TabIndex = 65;
            this.label74.Text = "Date";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(62, 110);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(27, 14);
            this.label75.TabIndex = 67;
            this.label75.Text = "Tax";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(92, 87);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.MaxLength = 30;
            this.textEdit6.Size = new System.Drawing.Size(250, 20);
            this.textEdit6.TabIndex = 64;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(10, 90);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(79, 14);
            this.label76.TabIndex = 63;
            this.label76.Text = "Tax Invoice#";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(357, 27);
            this.label77.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(51, 14);
            this.label77.TabIndex = 55;
            this.label77.Text = "Amount";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.lookUpEdit7);
            this.panel5.Controls.Add(this.label82);
            this.panel5.Controls.Add(this.label83);
            this.panel5.Controls.Add(this.textEdit10);
            this.panel5.Controls.Add(this.textEdit11);
            this.panel5.Controls.Add(this.label84);
            this.panel5.Controls.Add(this.textEdit12);
            this.panel5.Controls.Add(this.label85);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(639, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(272, 258);
            this.panel5.TabIndex = 91;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.dateEdit3);
            this.panel10.Controls.Add(this.label78);
            this.panel10.Controls.Add(this.textEdit7);
            this.panel10.Controls.Add(this.label79);
            this.panel10.Controls.Add(this.label80);
            this.panel10.Controls.Add(this.textEdit8);
            this.panel10.Controls.Add(this.label81);
            this.panel10.Controls.Add(this.textEdit9);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 169);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(272, 89);
            this.panel10.TabIndex = 100;
            // 
            // dateEdit3
            // 
            this.dateEdit3.EditValue = null;
            this.dateEdit3.EnterMoveNextControl = true;
            this.dateEdit3.Location = new System.Drawing.Point(98, 65);
            this.dateEdit3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit3.Name = "dateEdit3";
            this.dateEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.dateEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.dateEdit3.Properties.Appearance.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dateEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit3.Properties.ReadOnly = true;
            this.dateEdit3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit3.Size = new System.Drawing.Size(107, 20);
            this.dateEdit3.TabIndex = 108;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(60, 68);
            this.label78.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(33, 14);
            this.label78.TabIndex = 107;
            this.label78.Text = "Date";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit7
            // 
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(98, 44);
            this.textEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.MaxLength = 30;
            this.textEdit7.Properties.ReadOnly = true;
            this.textEdit7.Size = new System.Drawing.Size(164, 20);
            this.textEdit7.TabIndex = 106;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(14, 47);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(79, 14);
            this.label79.TabIndex = 105;
            this.label79.Text = "Tax Invoice#";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(18, 26);
            this.label80.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(75, 14);
            this.label80.TabIndex = 103;
            this.label80.Text = "Tax Amount";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit8
            // 
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(98, 23);
            this.textEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit8.Properties.ReadOnly = true;
            this.textEdit8.Size = new System.Drawing.Size(164, 20);
            this.textEdit8.TabIndex = 104;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(45, 5);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(48, 14);
            this.label81.TabIndex = 101;
            this.label81.Text = "Balance";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit9
            // 
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(98, 2);
            this.textEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit9.Properties.ReadOnly = true;
            this.textEdit9.Size = new System.Drawing.Size(164, 20);
            this.textEdit9.TabIndex = 102;
            // 
            // lookUpEdit7
            // 
            this.lookUpEdit7.EnterMoveNextControl = true;
            this.lookUpEdit7.Location = new System.Drawing.Point(98, 25);
            this.lookUpEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit7.Name = "lookUpEdit7";
            this.lookUpEdit7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.lookUpEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.lookUpEdit7.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit7.Properties.DropDownRows = 25;
            this.lookUpEdit7.Properties.NullText = "[Empty]";
            this.lookUpEdit7.Properties.PopupWidth = 280;
            this.lookUpEdit7.Properties.ReadOnly = true;
            this.lookUpEdit7.Size = new System.Drawing.Size(164, 20);
            this.lookUpEdit7.TabIndex = 95;
            this.lookUpEdit7.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(38, 29);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(55, 14);
            this.label82.TabIndex = 94;
            this.label82.Text = "Currency";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(61, 50);
            this.label83.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(32, 14);
            this.label83.TabIndex = 96;
            this.label83.Text = "Rate";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit10
            // 
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(98, 68);
            this.textEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit10.Properties.ReadOnly = true;
            this.textEdit10.Size = new System.Drawing.Size(164, 20);
            this.textEdit10.TabIndex = 99;
            // 
            // textEdit11
            // 
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(98, 46);
            this.textEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit11.Size = new System.Drawing.Size(164, 20);
            this.textEdit11.TabIndex = 97;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(34, 71);
            this.label84.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(59, 14);
            this.label84.TabIndex = 98;
            this.label84.Text = "Total Tax";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit12
            // 
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(98, 4);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit12.Properties.ReadOnly = true;
            this.textEdit12.Size = new System.Drawing.Size(164, 20);
            this.textEdit12.TabIndex = 93;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(34, 8);
            this.label85.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(59, 14);
            this.label85.TabIndex = 92;
            this.label85.Text = "Total Tax";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit4
            // 
            this.dateEdit4.EditValue = null;
            this.dateEdit4.EnterMoveNextControl = true;
            this.dateEdit4.Location = new System.Drawing.Point(414, 4);
            this.dateEdit4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit4.Name = "dateEdit4";
            this.dateEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.Appearance.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.dateEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit4.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit4.Size = new System.Drawing.Size(107, 20);
            this.dateEdit4.TabIndex = 52;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(375, 7);
            this.label86.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(33, 14);
            this.label86.TabIndex = 51;
            this.label86.Text = "Date";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit13
            // 
            this.textEdit13.EnterMoveNextControl = true;
            this.textEdit13.Location = new System.Drawing.Point(414, 192);
            this.textEdit13.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit13.Properties.ReadOnly = true;
            this.textEdit13.Size = new System.Drawing.Size(207, 20);
            this.textEdit13.TabIndex = 84;
            // 
            // textEdit14
            // 
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(414, 108);
            this.textEdit14.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit14.Properties.ReadOnly = true;
            this.textEdit14.Size = new System.Drawing.Size(207, 20);
            this.textEdit14.TabIndex = 70;
            // 
            // textEdit15
            // 
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(414, 25);
            this.textEdit15.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit15.Properties.ReadOnly = true;
            this.textEdit15.Size = new System.Drawing.Size(207, 20);
            this.textEdit15.TabIndex = 56;
            // 
            // lookUpEdit8
            // 
            this.lookUpEdit8.EnterMoveNextControl = true;
            this.lookUpEdit8.Location = new System.Drawing.Point(92, 192);
            this.lookUpEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit8.Name = "lookUpEdit8";
            this.lookUpEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit8.Properties.DropDownRows = 25;
            this.lookUpEdit8.Properties.NullText = "[Empty]";
            this.lookUpEdit8.Properties.PopupWidth = 200;
            this.lookUpEdit8.Size = new System.Drawing.Size(250, 20);
            this.lookUpEdit8.TabIndex = 82;
            this.lookUpEdit8.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit9
            // 
            this.lookUpEdit9.EnterMoveNextControl = true;
            this.lookUpEdit9.Location = new System.Drawing.Point(92, 108);
            this.lookUpEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit9.Name = "lookUpEdit9";
            this.lookUpEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit9.Properties.DropDownRows = 25;
            this.lookUpEdit9.Properties.NullText = "[Empty]";
            this.lookUpEdit9.Properties.PopupWidth = 200;
            this.lookUpEdit9.Size = new System.Drawing.Size(250, 20);
            this.lookUpEdit9.TabIndex = 68;
            this.lookUpEdit9.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(62, 27);
            this.label87.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(27, 14);
            this.label87.TabIndex = 53;
            this.label87.Text = "Tax";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit10
            // 
            this.lookUpEdit10.EnterMoveNextControl = true;
            this.lookUpEdit10.Location = new System.Drawing.Point(92, 24);
            this.lookUpEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit10.Name = "lookUpEdit10";
            this.lookUpEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit10.Properties.DropDownRows = 25;
            this.lookUpEdit10.Properties.NullText = "[Empty]";
            this.lookUpEdit10.Properties.PopupWidth = 200;
            this.lookUpEdit10.Size = new System.Drawing.Size(250, 20);
            this.lookUpEdit10.TabIndex = 54;
            this.lookUpEdit10.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit16
            // 
            this.textEdit16.EnterMoveNextControl = true;
            this.textEdit16.Location = new System.Drawing.Point(92, 3);
            this.textEdit16.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit16.Properties.Appearance.Options.UseFont = true;
            this.textEdit16.Properties.MaxLength = 30;
            this.textEdit16.Size = new System.Drawing.Size(250, 20);
            this.textEdit16.TabIndex = 50;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(10, 6);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(79, 14);
            this.label88.TabIndex = 49;
            this.label88.Text = "Tax Invoice#";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.iGrid2);
            this.tabPage5.Controls.Add(this.panel11);
            this.tabPage5.Location = new System.Drawing.Point(4, 26);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(911, 258);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "Discount, Cost, Etc";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // iGrid2
            // 
            this.iGrid2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid2.DefaultRow.Height = 20;
            this.iGrid2.DefaultRow.Sortable = false;
            this.iGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid2.Header.Height = 19;
            this.iGrid2.Location = new System.Drawing.Point(0, 34);
            this.iGrid2.Name = "iGrid2";
            this.iGrid2.RowHeader.Visible = true;
            this.iGrid2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid2.SingleClickEdit = true;
            this.iGrid2.Size = new System.Drawing.Size(911, 224);
            this.iGrid2.TabIndex = 48;
            this.iGrid2.TreeCol = null;
            this.iGrid2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.checkEdit1);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(911, 34);
            this.panel11.TabIndex = 49;
            // 
            // checkEdit1
            // 
            this.checkEdit1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkEdit1.Location = new System.Drawing.Point(6, 6);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.checkEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "For Tax Purpose";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.checkEdit1.Size = new System.Drawing.Size(130, 22);
            this.checkEdit1.TabIndex = 17;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.SkyBlue;
            this.tabPage6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage6.Controls.Add(this.dateEdit5);
            this.tabPage6.Controls.Add(this.lookUpEdit11);
            this.tabPage6.Controls.Add(this.lookUpEdit12);
            this.tabPage6.Controls.Add(this.iGrid3);
            this.tabPage6.Location = new System.Drawing.Point(4, 26);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(911, 258);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Document Info";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // dateEdit5
            // 
            this.dateEdit5.EditValue = null;
            this.dateEdit5.EnterMoveNextControl = true;
            this.dateEdit5.Location = new System.Drawing.Point(510, 20);
            this.dateEdit5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit5.Name = "dateEdit5";
            this.dateEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit5.Properties.Appearance.Options.UseFont = true;
            this.dateEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit5.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit5.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit5.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit5.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit5.Size = new System.Drawing.Size(125, 20);
            this.dateEdit5.TabIndex = 52;
            this.dateEdit5.Visible = false;
            // 
            // lookUpEdit11
            // 
            this.lookUpEdit11.EnterMoveNextControl = true;
            this.lookUpEdit11.Location = new System.Drawing.Point(130, 20);
            this.lookUpEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit11.Name = "lookUpEdit11";
            this.lookUpEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit11.Properties.DropDownRows = 12;
            this.lookUpEdit11.Properties.NullText = "[Empty]";
            this.lookUpEdit11.Properties.PopupWidth = 300;
            this.lookUpEdit11.Size = new System.Drawing.Size(210, 20);
            this.lookUpEdit11.TabIndex = 50;
            this.lookUpEdit11.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit12
            // 
            this.lookUpEdit12.EnterMoveNextControl = true;
            this.lookUpEdit12.Location = new System.Drawing.Point(350, 20);
            this.lookUpEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit12.Name = "lookUpEdit12";
            this.lookUpEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit12.Properties.DropDownRows = 5;
            this.lookUpEdit12.Properties.NullText = "[Empty]";
            this.lookUpEdit12.Properties.PopupWidth = 200;
            this.lookUpEdit12.Size = new System.Drawing.Size(149, 20);
            this.lookUpEdit12.TabIndex = 51;
            this.lookUpEdit12.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // iGrid3
            // 
            this.iGrid3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid3.DefaultRow.Height = 20;
            this.iGrid3.DefaultRow.Sortable = false;
            this.iGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid3.Header.Height = 19;
            this.iGrid3.Location = new System.Drawing.Point(0, 0);
            this.iGrid3.Name = "iGrid3";
            this.iGrid3.RowHeader.Visible = true;
            this.iGrid3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid3.SingleClickEdit = true;
            this.iGrid3.Size = new System.Drawing.Size(907, 254);
            this.iGrid3.TabIndex = 49;
            this.iGrid3.TreeCol = null;
            this.iGrid3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.panel12);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Location = new System.Drawing.Point(4, 26);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(911, 258);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Voucher";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.textEdit17);
            this.panel12.Controls.Add(this.lookUpEdit13);
            this.panel12.Controls.Add(this.label89);
            this.panel12.Controls.Add(this.label90);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Controls.Add(this.label96);
            this.panel12.Controls.Add(this.dateEdit6);
            this.panel12.Controls.Add(this.label97);
            this.panel12.Controls.Add(this.label98);
            this.panel12.Controls.Add(this.lookUpEdit16);
            this.panel12.Controls.Add(this.lookUpEdit17);
            this.panel12.Controls.Add(this.label99);
            this.panel12.Controls.Add(this.lookUpEdit18);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(905, 218);
            this.panel12.TabIndex = 29;
            // 
            // textEdit17
            // 
            this.textEdit17.EnterMoveNextControl = true;
            this.textEdit17.Location = new System.Drawing.Point(126, 90);
            this.textEdit17.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit17.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit17.Properties.Appearance.Options.UseFont = true;
            this.textEdit17.Properties.MaxLength = 80;
            this.textEdit17.Size = new System.Drawing.Size(283, 20);
            this.textEdit17.TabIndex = 58;
            // 
            // lookUpEdit13
            // 
            this.lookUpEdit13.EnterMoveNextControl = true;
            this.lookUpEdit13.Location = new System.Drawing.Point(126, 69);
            this.lookUpEdit13.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit13.Name = "lookUpEdit13";
            this.lookUpEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit13.Properties.DropDownRows = 20;
            this.lookUpEdit13.Properties.NullText = "[Empty]";
            this.lookUpEdit13.Properties.PopupWidth = 300;
            this.lookUpEdit13.Size = new System.Drawing.Size(283, 20);
            this.lookUpEdit13.TabIndex = 56;
            this.lookUpEdit13.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(53, 72);
            this.label89.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(68, 14);
            this.label89.TabIndex = 55;
            this.label89.Text = "Bank Name";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(5, 93);
            this.label90.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(116, 14);
            this.label90.TabIndex = 57;
            this.label90.Text = "Giro Bilyet / Cheque";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.textEdit18);
            this.panel13.Controls.Add(this.label91);
            this.panel13.Controls.Add(this.lookUpEdit14);
            this.panel13.Controls.Add(this.label92);
            this.panel13.Controls.Add(this.label93);
            this.panel13.Controls.Add(this.textEdit19);
            this.panel13.Controls.Add(this.memoExEdit4);
            this.panel13.Controls.Add(this.label94);
            this.panel13.Controls.Add(this.label95);
            this.panel13.Controls.Add(this.lookUpEdit15);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel13.Location = new System.Drawing.Point(481, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(424, 218);
            this.panel13.TabIndex = 52;
            // 
            // textEdit18
            // 
            this.textEdit18.EnterMoveNextControl = true;
            this.textEdit18.Location = new System.Drawing.Point(112, 48);
            this.textEdit18.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit18.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit18.Properties.Appearance.Options.UseFont = true;
            this.textEdit18.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit18.Size = new System.Drawing.Size(234, 20);
            this.textEdit18.TabIndex = 66;
            this.textEdit18.ToolTip = "Based On Outgoing Payment\'s Currency";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(31, 50);
            this.label91.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(77, 14);
            this.label91.TabIndex = 65;
            this.label91.Text = "Paid Amount";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit14
            // 
            this.lookUpEdit14.EnterMoveNextControl = true;
            this.lookUpEdit14.Location = new System.Drawing.Point(112, 6);
            this.lookUpEdit14.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit14.Name = "lookUpEdit14";
            this.lookUpEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit14.Properties.DropDownRows = 20;
            this.lookUpEdit14.Properties.NullText = "[Empty]";
            this.lookUpEdit14.Properties.PopupWidth = 150;
            this.lookUpEdit14.Size = new System.Drawing.Size(155, 20);
            this.lookUpEdit14.TabIndex = 62;
            this.lookUpEdit14.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Red;
            this.label92.Location = new System.Drawing.Point(53, 8);
            this.label92.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(55, 14);
            this.label92.TabIndex = 61;
            this.label92.Text = "Currency";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Red;
            this.label93.Location = new System.Drawing.Point(19, 29);
            this.label93.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(89, 14);
            this.label93.TabIndex = 63;
            this.label93.Text = "Exchange Rate";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit19
            // 
            this.textEdit19.EnterMoveNextControl = true;
            this.textEdit19.Location = new System.Drawing.Point(112, 27);
            this.textEdit19.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit19.Properties.Appearance.Options.UseFont = true;
            this.textEdit19.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit19.Size = new System.Drawing.Size(234, 20);
            this.textEdit19.TabIndex = 64;
            // 
            // memoExEdit4
            // 
            this.memoExEdit4.EditValue = "";
            this.memoExEdit4.EnterMoveNextControl = true;
            this.memoExEdit4.Location = new System.Drawing.Point(112, 90);
            this.memoExEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit4.Name = "memoExEdit4";
            this.memoExEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit4.Properties.MaxLength = 350;
            this.memoExEdit4.Properties.PopupFormSize = new System.Drawing.Size(500, 20);
            this.memoExEdit4.Properties.ShowIcon = false;
            this.memoExEdit4.Size = new System.Drawing.Size(306, 20);
            this.memoExEdit4.TabIndex = 70;
            this.memoExEdit4.ToolTip = "F4 : Show/hide text";
            this.memoExEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit4.ToolTipTitle = "Run System";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Red;
            this.label94.Location = new System.Drawing.Point(41, 93);
            this.label94.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(67, 14);
            this.label94.TabIndex = 69;
            this.label94.Text = "Description";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Red;
            this.label95.Location = new System.Drawing.Point(7, 72);
            this.label95.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(101, 14);
            this.label95.TabIndex = 67;
            this.label95.Text = "Person In Charge";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit15
            // 
            this.lookUpEdit15.EnterMoveNextControl = true;
            this.lookUpEdit15.Location = new System.Drawing.Point(112, 69);
            this.lookUpEdit15.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit15.Name = "lookUpEdit15";
            this.lookUpEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit15.Properties.DropDownRows = 25;
            this.lookUpEdit15.Properties.NullText = "[Empty]";
            this.lookUpEdit15.Properties.PopupWidth = 500;
            this.lookUpEdit15.Size = new System.Drawing.Size(306, 20);
            this.lookUpEdit15.TabIndex = 68;
            this.lookUpEdit15.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Red;
            this.label96.Location = new System.Drawing.Point(36, 9);
            this.label96.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(85, 14);
            this.label96.TabIndex = 49;
            this.label96.Text = "Account Type";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit6
            // 
            this.dateEdit6.EditValue = null;
            this.dateEdit6.EnterMoveNextControl = true;
            this.dateEdit6.Location = new System.Drawing.Point(126, 111);
            this.dateEdit6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit6.Name = "dateEdit6";
            this.dateEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit6.Properties.Appearance.Options.UseFont = true;
            this.dateEdit6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit6.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit6.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit6.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit6.Properties.MaxLength = 8;
            this.dateEdit6.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit6.Size = new System.Drawing.Size(121, 20);
            this.dateEdit6.TabIndex = 60;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(61, 114);
            this.label97.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(59, 14);
            this.label97.TabIndex = 59;
            this.label97.Text = "Due Date";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Red;
            this.label98.Location = new System.Drawing.Point(34, 51);
            this.label98.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(87, 14);
            this.label98.TabIndex = 53;
            this.label98.Text = "Payment Type";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit16
            // 
            this.lookUpEdit16.EnterMoveNextControl = true;
            this.lookUpEdit16.Location = new System.Drawing.Point(126, 48);
            this.lookUpEdit16.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit16.Name = "lookUpEdit16";
            this.lookUpEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit16.Properties.DropDownRows = 20;
            this.lookUpEdit16.Properties.NullText = "[Empty]";
            this.lookUpEdit16.Properties.PopupWidth = 250;
            this.lookUpEdit16.Size = new System.Drawing.Size(283, 20);
            this.lookUpEdit16.TabIndex = 54;
            this.lookUpEdit16.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit17
            // 
            this.lookUpEdit17.EnterMoveNextControl = true;
            this.lookUpEdit17.Location = new System.Drawing.Point(126, 6);
            this.lookUpEdit17.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit17.Name = "lookUpEdit17";
            this.lookUpEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit17.Properties.DropDownRows = 20;
            this.lookUpEdit17.Properties.NullText = "[Empty]";
            this.lookUpEdit17.Properties.PopupWidth = 143;
            this.lookUpEdit17.Size = new System.Drawing.Size(121, 20);
            this.lookUpEdit17.TabIndex = 50;
            this.lookUpEdit17.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Red;
            this.label99.Location = new System.Drawing.Point(68, 30);
            this.label99.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(53, 14);
            this.label99.TabIndex = 51;
            this.label99.Text = "Account";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit18
            // 
            this.lookUpEdit18.EnterMoveNextControl = true;
            this.lookUpEdit18.Location = new System.Drawing.Point(126, 27);
            this.lookUpEdit18.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit18.Name = "lookUpEdit18";
            this.lookUpEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit18.Properties.DropDownRows = 20;
            this.lookUpEdit18.Properties.NullText = "[Empty]";
            this.lookUpEdit18.Properties.PopupWidth = 500;
            this.lookUpEdit18.Size = new System.Drawing.Size(283, 20);
            this.lookUpEdit18.TabIndex = 52;
            this.lookUpEdit18.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel14.Controls.Add(this.textEdit20);
            this.panel14.Controls.Add(this.label100);
            this.panel14.Controls.Add(this.textEdit21);
            this.panel14.Controls.Add(this.label101);
            this.panel14.Controls.Add(this.textEdit22);
            this.panel14.Controls.Add(this.label102);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(3, 221);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(905, 34);
            this.panel14.TabIndex = 30;
            // 
            // textEdit20
            // 
            this.textEdit20.EnterMoveNextControl = true;
            this.textEdit20.Location = new System.Drawing.Point(624, 7);
            this.textEdit20.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit20.Properties.Appearance.Options.UseFont = true;
            this.textEdit20.Properties.MaxLength = 16;
            this.textEdit20.Properties.ReadOnly = true;
            this.textEdit20.Size = new System.Drawing.Size(205, 20);
            this.textEdit20.TabIndex = 76;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(593, 9);
            this.label100.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(22, 14);
            this.label100.TabIndex = 75;
            this.label100.Text = "VC";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit21
            // 
            this.textEdit21.EnterMoveNextControl = true;
            this.textEdit21.Location = new System.Drawing.Point(339, 7);
            this.textEdit21.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit21.Properties.Appearance.Options.UseFont = true;
            this.textEdit21.Properties.MaxLength = 16;
            this.textEdit21.Properties.ReadOnly = true;
            this.textEdit21.Size = new System.Drawing.Size(205, 20);
            this.textEdit21.TabIndex = 74;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(305, 9);
            this.label101.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(22, 14);
            this.label101.TabIndex = 73;
            this.label101.Text = "VR";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit22
            // 
            this.textEdit22.EnterMoveNextControl = true;
            this.textEdit22.Location = new System.Drawing.Point(32, 8);
            this.textEdit22.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.textEdit22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit22.Properties.Appearance.Options.UseFont = true;
            this.textEdit22.Properties.MaxLength = 16;
            this.textEdit22.Properties.ReadOnly = true;
            this.textEdit22.Size = new System.Drawing.Size(205, 20);
            this.textEdit22.TabIndex = 72;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(6, 11);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(27, 14);
            this.label102.TabIndex = 71;
            this.label102.Text = "OP ";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.Color.SkyBlue;
            this.tabPage8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage8.Controls.Add(this.iGrid4);
            this.tabPage8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage8.Location = new System.Drawing.Point(4, 26);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(911, 258);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "PO (Disc Amount/Custom Tax)";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // iGrid4
            // 
            this.iGrid4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid4.DefaultRow.Height = 20;
            this.iGrid4.DefaultRow.Sortable = false;
            this.iGrid4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid4.Header.Height = 21;
            this.iGrid4.Location = new System.Drawing.Point(0, 0);
            this.iGrid4.Name = "iGrid4";
            this.iGrid4.RowHeader.Visible = true;
            this.iGrid4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid4.SingleClickEdit = true;
            this.iGrid4.Size = new System.Drawing.Size(907, 254);
            this.iGrid4.TabIndex = 48;
            this.iGrid4.TreeCol = null;
            this.iGrid4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage9
            // 
            this.tabPage9.Location = new System.Drawing.Point(0, 0);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(200, 100);
            this.tabPage9.TabIndex = 0;
            // 
            // iGrid5
            // 
            this.iGrid5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid5.DefaultRow.Height = 20;
            this.iGrid5.DefaultRow.Sortable = false;
            this.iGrid5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid5.Header.Height = 19;
            this.iGrid5.Location = new System.Drawing.Point(0, 0);
            this.iGrid5.Name = "iGrid5";
            this.iGrid5.RowHeader.Visible = true;
            this.iGrid5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid5.SingleClickEdit = true;
            this.iGrid5.Size = new System.Drawing.Size(911, 258);
            this.iGrid5.TabIndex = 48;
            this.iGrid5.TreeCol = null;
            this.iGrid5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgCOAAccount
            // 
            this.TpgCOAAccount.BackColor = System.Drawing.Color.SkyBlue;
            this.TpgCOAAccount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgCOAAccount.Controls.Add(this.Grd2);
            this.TpgCOAAccount.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgCOAAccount.Location = new System.Drawing.Point(4, 26);
            this.TpgCOAAccount.Name = "TpgCOAAccount";
            this.TpgCOAAccount.Size = new System.Drawing.Size(830, 145);
            this.TpgCOAAccount.TabIndex = 0;
            this.TpgCOAAccount.Text = "COA\'s Account List";
            this.TpgCOAAccount.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(826, 141);
            this.Grd2.TabIndex = 58;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgApprovalInformation);
            this.tabControl1.Controls.Add(this.TpgCOAAccount);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 258);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(838, 175);
            this.tabControl1.TabIndex = 58;
            // 
            // TpgApprovalInformation
            // 
            this.TpgApprovalInformation.Controls.Add(this.Grd1);
            this.TpgApprovalInformation.Location = new System.Drawing.Point(4, 26);
            this.TpgApprovalInformation.Name = "TpgApprovalInformation";
            this.TpgApprovalInformation.Size = new System.Drawing.Size(830, 145);
            this.TpgApprovalInformation.TabIndex = 3;
            this.TpgApprovalInformation.Text = "Approval Information";
            this.TpgApprovalInformation.UseVisualStyleBackColor = true;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(830, 145);
            this.Grd1.TabIndex = 59;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TxtCtCtName
            // 
            this.TxtCtCtName.EnterMoveNextControl = true;
            this.TxtCtCtName.Location = new System.Drawing.Point(142, 152);
            this.TxtCtCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCtName.Name = "TxtCtCtName";
            this.TxtCtCtName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCtName.Properties.MaxLength = 80;
            this.TxtCtCtName.Properties.ReadOnly = true;
            this.TxtCtCtName.Size = new System.Drawing.Size(284, 20);
            this.TxtCtCtName.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(18, 155);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 14);
            this.label12.TabIndex = 27;
            this.label12.Text = "Customer\'s Category";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmReturnARDownpayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 433);
            this.Name = "FrmReturnARDownpayment";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSummary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcNoType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSettledAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReturnAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePIC.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).EndInit();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid3)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid5)).EndInit();
            this.TpgCOAAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgApprovalInformation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCtName.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtEntCode;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtCtCode;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnCtCode;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.SimpleButton BtnVoucherRequestDocNo;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtSummary;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LblDeptCode;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private System.Windows.Forms.Label LblBankAcCode;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtReturnAmt;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label Label11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LuePIC;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label LblSettledAmt;
        internal DevExpress.XtraEditors.TextEdit TxtSettledAmt;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid1;
        private System.Windows.Forms.TabPage tabPage4;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit1;
        private System.Windows.Forms.Label label60;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit2;
        private System.Windows.Forms.Label label61;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit3;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.Label label64;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit5;
        private System.Windows.Forms.Label label65;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit6;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        internal DevExpress.XtraEditors.TextEdit textEdit4;
        private System.Windows.Forms.Label label69;
        internal DevExpress.XtraEditors.DateEdit dateEdit1;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        internal DevExpress.XtraEditors.TextEdit textEdit5;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        internal DevExpress.XtraEditors.DateEdit dateEdit2;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        internal DevExpress.XtraEditors.TextEdit textEdit6;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.DateEdit dateEdit3;
        private System.Windows.Forms.Label label78;
        internal DevExpress.XtraEditors.TextEdit textEdit7;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        internal DevExpress.XtraEditors.TextEdit textEdit8;
        private System.Windows.Forms.Label label81;
        internal DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit7;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        internal DevExpress.XtraEditors.TextEdit textEdit10;
        internal DevExpress.XtraEditors.TextEdit textEdit11;
        private System.Windows.Forms.Label label84;
        internal DevExpress.XtraEditors.TextEdit textEdit12;
        private System.Windows.Forms.Label label85;
        internal DevExpress.XtraEditors.DateEdit dateEdit4;
        private System.Windows.Forms.Label label86;
        internal DevExpress.XtraEditors.TextEdit textEdit13;
        internal DevExpress.XtraEditors.TextEdit textEdit14;
        internal DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit8;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit9;
        private System.Windows.Forms.Label label87;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit10;
        internal DevExpress.XtraEditors.TextEdit textEdit16;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TabPage tabPage5;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid2;
        private System.Windows.Forms.Panel panel11;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.TabPage tabPage6;
        internal DevExpress.XtraEditors.DateEdit dateEdit5;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit11;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit12;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid3;
        private System.Windows.Forms.TabPage tabPage7;
        protected System.Windows.Forms.Panel panel12;
        internal DevExpress.XtraEditors.TextEdit textEdit17;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit13;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        protected System.Windows.Forms.Panel panel13;
        internal DevExpress.XtraEditors.TextEdit textEdit18;
        private System.Windows.Forms.Label label91;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit14;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        internal DevExpress.XtraEditors.TextEdit textEdit19;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit4;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit15;
        private System.Windows.Forms.Label label96;
        internal DevExpress.XtraEditors.DateEdit dateEdit6;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit16;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit17;
        private System.Windows.Forms.Label label99;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit18;
        protected System.Windows.Forms.Panel panel14;
        internal DevExpress.XtraEditors.TextEdit textEdit20;
        private System.Windows.Forms.Label label100;
        internal DevExpress.XtraEditors.TextEdit textEdit21;
        private System.Windows.Forms.Label label101;
        internal DevExpress.XtraEditors.TextEdit textEdit22;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TabPage tabPage8;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid4;
        private System.Windows.Forms.TabPage tabPage9;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid5;
        private System.Windows.Forms.TabPage TpgCOAAccount;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgApprovalInformation;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        internal DevExpress.XtraEditors.TextEdit TxtCtCtName;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueAcNoType;
        private System.Windows.Forms.Label label19;
    }
}