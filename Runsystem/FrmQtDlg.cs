﻿#region update
/*
    19/01/2018 [ari] tambah printout
    24/01/2018 [TKG] tambah otorisasi group thd term of payment
    05/07/2018 [HAR] bug validasi itemcategory harus sama, tambah parameter 
    06/08/2018 [HAR] tambah parameter buat nentuin bisa upload file apa tidak
    06/11/2018 [HAR] upload file ditambah jadi 5, bisa edit buat upload file
    12/11/2018 [HAR] ftp tambah parameter buat nentuin format koneksi FTP
    24/04/2019 [MEY] filter vendor berdasarkan vendor yang aktif
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    07/01/2020 [DITA/SIER] Tambah parameter IsFilterByItCt
    31/01/2020 [TKG/IMS] Tambah filter menampilkan data dari outstanding material request
    06/10/2020 [VIN/IMS] filter category berdasarkan grup user
    27/12/2021 [DEV/SIER] Penambahan field Material Request# dan kolom Material Request# dengan parameter IsQtItemListDlgUseMRFilter
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmQtDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmQt mFrmParent;
        private string mVdCode = string.Empty, mPtCode = string.Empty;
        internal bool mIsFilterByItCt = false, mIsQtItemListDlgUseMRFilter = false;

        #endregion

        #region Constructor

        public FrmQtDlg(FrmQt FrmParent, string VdCode, string PtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
            mPtCode = PtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                //Sl.SetLueItCtCode(ref LueItCtCode);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                if (!mIsQtItemListDlgUseMRFilter) label2.Visible = TxtMaterialRequestDocNo.Visible = ChkMaterialRequestDocNo.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, B.ItCtName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T1.ItCode, T1.ItName, T1.ForeignName, T1.ItCodeInternal, T1.ItCtCode, T1.PurchaseUomCode, T2.UPrice, T2.DocDt, T1.Specification, ");
            if (mIsQtItemListDlgUseMRFilter)
                SQL.AppendLine("    T3.DocNo AS MaterialRequestDocNo ");
            else
                SQL.AppendLine("    NULL AS MaterialRequestDocNo ");
            SQL.AppendLine("    From TblItem T1 ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select T2b.ItCode, T2a.DocDt, T2b.UPrice  ");
            SQL.AppendLine("        From TblQtHdr T2a ");
            SQL.AppendLine("        Inner Join TblQtDtl T2b On T2a.DocNo=T2b.DocNo And T2b.ActInd='Y' "); 
            SQL.AppendLine("        Where T2a.VdCode=@VdCode And T2a.PtCode=@PtCode And T2a.Status='A' ");  
            SQL.AppendLine("    ) T2 On T1.ItCode=T2.ItCode  ");
            if (mIsQtItemListDlgUseMRFilter)
            {
                SQL.AppendLine("        INNER JOIN TblMaterialRequestDtl T3 ON T3.ItCode = T1.ItCode AND T3.Status='A'  ");
                SQL.AppendLine("        INNER JOIN TblMaterialRequestHdr T4 ON T4.DocNo = T3.DocNo ");
            }
            SQL.AppendLine("    Where T1.ActInd='Y'  ");
            SQL.AppendLine("    Union All  ");
            SQL.AppendLine("    Select T.ItCode, T.ItName, T.ForeignName, T.ItCodeInternal, T.ItCtCode, T.PurchaseUomCode, 0 As UPrice, Null As DocDt, T.Specification, ");
            if (mIsQtItemListDlgUseMRFilter)
                SQL.AppendLine("    T3.DocNo AS MaterialRequestDocNo ");
            else
                SQL.AppendLine("    NULL AS MaterialRequestDocNo ");
            SQL.AppendLine("    From TblItem T ");
            if (mIsQtItemListDlgUseMRFilter)
            {
                SQL.AppendLine("        INNER JOIN TblMaterialRequestDtl T2 ON T2.ItCode = T.ItCode AND T2.Status='A' ");
                SQL.AppendLine("        INNER JOIN TblMaterialRequestHdr T3 ON T3.DocNo = T2.DocNo ");
            }
            SQL.AppendLine("    Where T.ActInd='Y' ");
            SQL.AppendLine("    And T.ItCode Not In ");
            SQL.AppendLine("    (  ");
	        SQL.AppendLine("        Select T2.ItCode ");
            SQL.AppendLine("        From TblQtHdr T1 ");
            SQL.AppendLine("        Inner Join TblQtDtl T2 On T1.DocNo=T2.DocNo And T2.ActInd='Y' ");
            SQL.AppendLine("        Where T1.VdCode=@VdCode And T1.PtCode=@PtCode And T1.Status='A' ");  
            SQL.AppendLine("    ) "); 
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
            SQL.AppendLine("Where Position(Concat('##', A.ItCode, '##') In @SelectedItem)<1 ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=B.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (ChkOutstandingMaterialRequest.Checked)
            {
                SQL.AppendLine("And A.ItCode In ( ");
                SQL.AppendLine("    Select Distinct ItCode From (");
                SQL.AppendLine("        Select X2.ItCode, X2.Qty-IfNull(X3.Qty, 0)-IfNull(X4.Qty, 0) As Qty ");
                SQL.AppendLine("        From TblMaterialRequestHdr X1 ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl X2 ");
                SQL.AppendLine("            On X1.DocNo=X2.DocNo ");
                SQL.AppendLine("            And IfNull(X2.Status,'X')='A' ");
                SQL.AppendLine("            And X2.CancelInd='N' ");
                SQL.AppendLine("            And IfNull(X2.ProcessInd, '')<>'F' ");
                SQL.AppendLine("            And X2.TenderDocNo Is Null ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.MRDocNo As DocNo, T1.MRDNo As DNo, Sum(T1.Qty) Qty ");
                SQL.AppendLine("            From TblMRQtyCancel T1 ");
                SQL.AppendLine("            Inner Join TblMaterialRequestDtl T2 ");
                SQL.AppendLine("                On T1.MRDocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.MRDNo=T2.DNo ");
                SQL.AppendLine("                And IfNull(T2.ProcessInd, '')<>'F' ");
                SQL.AppendLine("                And T2.TenderDocNo Is Null ");
                SQL.AppendLine("            Where T1.CancelInd = 'N' ");
                SQL.AppendLine("            Group By T1.MRDocNo, T1.MRDNo ");
                SQL.AppendLine("        ) X3 On X2.DocNo=X3.DocNo And X2.DNo=X3.DNo ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select MaterialRequestDocNo As DocNo, T1.MaterialRequestDNo As DNo, Sum(T1.Qty) As Qty ");
                SQL.AppendLine("            From TblPORequestDtl T1 ");
                SQL.AppendLine("            Inner Join TblMaterialRequestDtl T2 ");
                SQL.AppendLine("                On T1.MaterialRequestDocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.MaterialRequestDNo=T2.DNo ");
                SQL.AppendLine("                And IfNull(T2.ProcessInd, '')<>'F' ");
                SQL.AppendLine("                And T2.TenderDocNo Is Null ");
                SQL.AppendLine("                And T2.CancelInd='N' ");
                SQL.AppendLine("            Where T1.CancelInd='N' And T1.Status<>'C' ");
                SQL.AppendLine("            Group By T1.MaterialRequestDocNo, T1.MaterialRequestDNo ");
                SQL.AppendLine("        ) X4 On X2.DocNo=X4.DocNo And X2.DNo=X4.DNo ");
                SQL.AppendLine("    ) X Where Qty>0.00 ");
                SQL.AppendLine(") ");
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Local Internal",
                        "Material"+Environment.NewLine+"Request#",                        
                        
                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Item's Category",
                        "UoM",
                        "Latest Price"+Environment.NewLine+"(Based On Vendor"+Environment.NewLine+"And Term of Payment)",
                        
                        //11-12
                        "Latest"+Environment.NewLine+"Quotation Date",
                        "Specification"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 80, 20, 200, 150, 
                        
                        //6-10
                        130, 230, 170, 80, 150, 

                        //11-12
                        100, 300
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 11 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            if (mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 8 }, false);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 8 }, false);
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 12 });
            if(!mIsQtItemListDlgUseMRFilter) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParam<String>(ref cm, "@PtCode", mPtCode);

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName", "A.ForeignName" });                
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
                if (mIsQtItemListDlgUseMRFilter) Sm.FilterStr(ref Filter, ref cm, TxtMaterialRequestDocNo.Text, "A.MaterialRequestDocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By A.ItCode;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "MaterialRequestDocNo", "ItName", "ForeignName", "ItCtName", 

                            //6-9
                            "PurchaseUomCode", "UPrice", "DocDt", "Specification", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Grd1.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 12);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 10 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 6 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 10 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), Sm.GetGrdStr(Grd1, Row, 2))) return true;
            return false;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsQtItemListDlgUseMRFilter = Sm.GetParameterBoo("IsQtItemListDlgUseMRFilter");
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");            
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtMaterialRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkMaterialRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Material Request");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
