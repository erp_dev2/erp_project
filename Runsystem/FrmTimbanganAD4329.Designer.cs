﻿namespace RunSystem
{
    partial class FrmTimbanganAD4329
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnStart = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblWeight = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLicenceNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDriverName = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.LueTransportType = new DevExpress.XtraEditors.LookUpEdit();
            this.LueItem = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.txtWeightBefore = new DevExpress.XtraEditors.TextEdit();
            this.txtWeightAfter = new DevExpress.XtraEditors.TextEdit();
            this.txtWeightNetto = new DevExpress.XtraEditors.TextEdit();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DteDocDtAf = new DevExpress.XtraEditors.DateEdit();
            this.lblTimeBf = new System.Windows.Forms.Label();
            this.lblTimeAf = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblJenisTimbangan = new System.Windows.Forms.Label();
            this.LueAreaBongkar = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenceNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTransportType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeightBefore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeightAfter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeightNetto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDtAf.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDtAf.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAreaBongkar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(623, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Size = new System.Drawing.Size(80, 618);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Size = new System.Drawing.Size(80, 31);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Size = new System.Drawing.Size(80, 31);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueAreaBongkar);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblJenisTimbangan);
            this.panel2.Controls.Add(this.lblUser);
            this.panel2.Controls.Add(this.lblTimeAf);
            this.panel2.Controls.Add(this.lblTimeBf);
            this.panel2.Controls.Add(this.DteDocDtAf);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblStatus);
            this.panel2.Controls.Add(this.txtWeightNetto);
            this.panel2.Controls.Add(this.txtWeightAfter);
            this.panel2.Controls.Add(this.txtWeightBefore);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.LueItem);
            this.panel2.Controls.Add(this.LueTransportType);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtDriverName);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtLicenceNo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.BtnStart);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Size = new System.Drawing.Size(623, 618);
            // 
            // BtnStart
            // 
            this.BtnStart.Location = new System.Drawing.Point(473, 309);
            this.BtnStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(82, 87);
            this.BtnStart.TabIndex = 4;
            this.BtnStart.Text = "Timbang";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.lblWeight);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(53, 309);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(415, 86);
            this.panel3.TabIndex = 6;
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblWeight.Font = new System.Drawing.Font("Trebuchet MS", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.Location = new System.Drawing.Point(221, 0);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Padding = new System.Windows.Forms.Padding(57, 13, 6, 13);
            this.lblWeight.Size = new System.Drawing.Size(106, 75);
            this.lblWeight.TabIndex = 5;
            this.lblWeight.Text = "0";
            this.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Font = new System.Drawing.Font("Trebuchet MS", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(327, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(6, 13, 6, 13);
            this.label2.Size = new System.Drawing.Size(84, 75);
            this.label2.TabIndex = 4;
            this.label2.Text = "KG";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(69, 144);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 18);
            this.label4.TabIndex = 14;
            this.label4.Text = "Jenis Kendaraan";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(118, 175);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 18);
            this.label5.TabIndex = 16;
            this.label5.Text = "No Polisi";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLicenceNo
            // 
            this.txtLicenceNo.EnterMoveNextControl = true;
            this.txtLicenceNo.Location = new System.Drawing.Point(190, 171);
            this.txtLicenceNo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtLicenceNo.Name = "txtLicenceNo";
            this.txtLicenceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtLicenceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLicenceNo.Properties.Appearance.Options.UseBackColor = true;
            this.txtLicenceNo.Properties.Appearance.Options.UseFont = true;
            this.txtLicenceNo.Properties.MaxLength = 16;
            this.txtLicenceNo.Size = new System.Drawing.Size(368, 24);
            this.txtLicenceNo.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(137, 206);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 18);
            this.label7.TabIndex = 21;
            this.label7.Text = "Sopir";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(24, 440);
            this.label8.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 18);
            this.label8.TabIndex = 22;
            this.label8.Text = "Berat sebelum bongkar";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(31, 468);
            this.label9.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 18);
            this.label9.TabIndex = 24;
            this.label9.Text = "Berat setelah bongkar";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(94, 509);
            this.label10.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 18);
            this.label10.TabIndex = 26;
            this.label10.Text = "Berat Netto";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDriverName
            // 
            this.txtDriverName.EnterMoveNextControl = true;
            this.txtDriverName.Location = new System.Drawing.Point(190, 201);
            this.txtDriverName.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtDriverName.Name = "txtDriverName";
            this.txtDriverName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtDriverName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDriverName.Properties.Appearance.Options.UseBackColor = true;
            this.txtDriverName.Properties.Appearance.Options.UseFont = true;
            this.txtDriverName.Properties.MaxLength = 16;
            this.txtDriverName.Size = new System.Drawing.Size(368, 24);
            this.txtDriverName.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(89, 242);
            this.label11.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 18);
            this.label11.TabIndex = 29;
            this.label11.Text = "Jenis Antrian";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTransportType
            // 
            this.LueTransportType.EnterMoveNextControl = true;
            this.LueTransportType.Location = new System.Drawing.Point(190, 141);
            this.LueTransportType.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueTransportType.Name = "LueTransportType";
            this.LueTransportType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.Appearance.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceDropDown.BackColor = System.Drawing.Color.White;
            this.LueTransportType.Properties.AppearanceDropDown.BackColor2 = System.Drawing.Color.White;
            this.LueTransportType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceDropDown.Options.UseBackColor = true;
            this.LueTransportType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceDropDownHeader.BackColor = System.Drawing.Color.White;
            this.LueTransportType.Properties.AppearanceDropDownHeader.BackColor2 = System.Drawing.Color.White;
            this.LueTransportType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceDropDownHeader.Options.UseBackColor = true;
            this.LueTransportType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTransportType.Properties.AppearanceFocused.BackColor = System.Drawing.Color.White;
            this.LueTransportType.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.White;
            this.LueTransportType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTransportType.Properties.AppearanceFocused.Options.UseBackColor = true;
            this.LueTransportType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTransportType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTransportType.Properties.DropDownRows = 25;
            this.LueTransportType.Properties.NullText = "[Empty]";
            this.LueTransportType.Properties.PopupWidth = 500;
            this.LueTransportType.Size = new System.Drawing.Size(368, 24);
            this.LueTransportType.TabIndex = 1;
            this.LueTransportType.ToolTip = "F4 : Show/hide list";
            this.LueTransportType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTransportType.EditValueChanged += new System.EventHandler(this.LueTransportType_EditValueChanged);
            // 
            // LueItem
            // 
            this.LueItem.EnterMoveNextControl = true;
            this.LueItem.Location = new System.Drawing.Point(190, 238);
            this.LueItem.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueItem.Name = "LueItem";
            this.LueItem.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItem.Properties.Appearance.Options.UseFont = true;
            this.LueItem.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItem.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItem.Properties.AppearanceDropDown.BackColor = System.Drawing.Color.White;
            this.LueItem.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItem.Properties.AppearanceDropDown.Options.UseBackColor = true;
            this.LueItem.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItem.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItem.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItem.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItem.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItem.Properties.DropDownRows = 25;
            this.LueItem.Properties.NullText = "[Empty]";
            this.LueItem.Properties.PopupWidth = 500;
            this.LueItem.Size = new System.Drawing.Size(368, 24);
            this.LueItem.TabIndex = 4;
            this.LueItem.ToolTip = "F4 : Show/hide list";
            this.LueItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItem.EditValueChanged += new System.EventHandler(this.LueItem_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(185, 545);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(368, 24);
            this.MeeRemark.TabIndex = 9;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(96, 550);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 18);
            this.label12.TabIndex = 34;
            this.label12.Text = "Keterangan";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(190, 87);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(368, 42);
            this.TxtDocNo.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(97, 93);
            this.label13.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 18);
            this.label13.TabIndex = 37;
            this.label13.Text = "No. Antrian";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(347, 437);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseBackColor = true;
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(128, 24);
            this.DteDocDt.TabIndex = 41;
            this.DteDocDt.TabStop = false;
            // 
            // txtWeightBefore
            // 
            this.txtWeightBefore.EditValue = "0";
            this.txtWeightBefore.EnterMoveNextControl = true;
            this.txtWeightBefore.Location = new System.Drawing.Point(183, 437);
            this.txtWeightBefore.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtWeightBefore.Name = "txtWeightBefore";
            this.txtWeightBefore.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtWeightBefore.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightBefore.Properties.Appearance.Options.UseBackColor = true;
            this.txtWeightBefore.Properties.Appearance.Options.UseFont = true;
            this.txtWeightBefore.Properties.Appearance.Options.UseTextOptions = true;
            this.txtWeightBefore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtWeightBefore.Size = new System.Drawing.Size(160, 24);
            this.txtWeightBefore.TabIndex = 6;
            this.txtWeightBefore.TabStop = false;
            this.txtWeightBefore.EditValueChanged += new System.EventHandler(this.txtWeightBefore_EditValueChanged);
            // 
            // txtWeightAfter
            // 
            this.txtWeightAfter.EditValue = "0";
            this.txtWeightAfter.EnterMoveNextControl = true;
            this.txtWeightAfter.Location = new System.Drawing.Point(183, 465);
            this.txtWeightAfter.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtWeightAfter.Name = "txtWeightAfter";
            this.txtWeightAfter.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtWeightAfter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightAfter.Properties.Appearance.Options.UseBackColor = true;
            this.txtWeightAfter.Properties.Appearance.Options.UseFont = true;
            this.txtWeightAfter.Properties.Appearance.Options.UseTextOptions = true;
            this.txtWeightAfter.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtWeightAfter.Size = new System.Drawing.Size(160, 24);
            this.txtWeightAfter.TabIndex = 7;
            this.txtWeightAfter.TabStop = false;
            this.txtWeightAfter.EditValueChanged += new System.EventHandler(this.txtWeightAfter_EditValueChanged);
            // 
            // txtWeightNetto
            // 
            this.txtWeightNetto.EditValue = "0";
            this.txtWeightNetto.EnterMoveNextControl = true;
            this.txtWeightNetto.Location = new System.Drawing.Point(183, 504);
            this.txtWeightNetto.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtWeightNetto.Name = "txtWeightNetto";
            this.txtWeightNetto.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtWeightNetto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeightNetto.Properties.Appearance.Options.UseBackColor = true;
            this.txtWeightNetto.Properties.Appearance.Options.UseFont = true;
            this.txtWeightNetto.Properties.Appearance.Options.UseTextOptions = true;
            this.txtWeightNetto.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtWeightNetto.Size = new System.Drawing.Size(160, 24);
            this.txtWeightNetto.TabIndex = 8;
            this.txtWeightNetto.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(190, 23);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(22, 18);
            this.lblStatus.TabIndex = 44;
            this.lblStatus.Text = "In";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(128, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 18);
            this.label3.TabIndex = 45;
            this.label3.Text = "Status";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDtAf
            // 
            this.DteDocDtAf.EditValue = null;
            this.DteDocDtAf.EnterMoveNextControl = true;
            this.DteDocDtAf.Location = new System.Drawing.Point(347, 465);
            this.DteDocDtAf.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.DteDocDtAf.Name = "DteDocDtAf";
            this.DteDocDtAf.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDtAf.Properties.Appearance.Options.UseFont = true;
            this.DteDocDtAf.Properties.AppearanceDropDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.DteDocDtAf.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDtAf.Properties.AppearanceDropDown.Options.UseBackColor = true;
            this.DteDocDtAf.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDtAf.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDtAf.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDtAf.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDtAf.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDtAf.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDtAf.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDtAf.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDtAf.Size = new System.Drawing.Size(128, 24);
            this.DteDocDtAf.TabIndex = 46;
            this.DteDocDtAf.TabStop = false;
            // 
            // lblTimeBf
            // 
            this.lblTimeBf.AutoSize = true;
            this.lblTimeBf.Location = new System.Drawing.Point(482, 442);
            this.lblTimeBf.Name = "lblTimeBf";
            this.lblTimeBf.Size = new System.Drawing.Size(44, 18);
            this.lblTimeBf.TabIndex = 48;
            this.lblTimeBf.Text = "label1";
            // 
            // lblTimeAf
            // 
            this.lblTimeAf.AutoSize = true;
            this.lblTimeAf.Location = new System.Drawing.Point(482, 471);
            this.lblTimeAf.Name = "lblTimeAf";
            this.lblTimeAf.Size = new System.Drawing.Size(52, 18);
            this.lblTimeAf.TabIndex = 49;
            this.lblTimeAf.Text = "label14";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(185, 581);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(52, 18);
            this.lblUser.TabIndex = 50;
            this.lblUser.Text = "label14";
            // 
            // lblJenisTimbangan
            // 
            this.lblJenisTimbangan.AutoSize = true;
            this.lblJenisTimbangan.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJenisTimbangan.Location = new System.Drawing.Point(190, 49);
            this.lblJenisTimbangan.Name = "lblJenisTimbangan";
            this.lblJenisTimbangan.Size = new System.Drawing.Size(168, 29);
            this.lblJenisTimbangan.TabIndex = 51;
            this.lblJenisTimbangan.Text = "Timbangan 1";
            // 
            // LueAreaBongkar
            // 
            this.LueAreaBongkar.EnterMoveNextControl = true;
            this.LueAreaBongkar.Location = new System.Drawing.Point(190, 269);
            this.LueAreaBongkar.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.LueAreaBongkar.Name = "LueAreaBongkar";
            this.LueAreaBongkar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.Appearance.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceDropDown.BackColor = System.Drawing.Color.White;
            this.LueAreaBongkar.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceDropDown.Options.UseBackColor = true;
            this.LueAreaBongkar.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAreaBongkar.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAreaBongkar.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAreaBongkar.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAreaBongkar.Properties.DropDownRows = 25;
            this.LueAreaBongkar.Properties.NullText = "[Empty]";
            this.LueAreaBongkar.Properties.PopupWidth = 500;
            this.LueAreaBongkar.Size = new System.Drawing.Size(368, 24);
            this.LueAreaBongkar.TabIndex = 5;
            this.LueAreaBongkar.ToolTip = "F4 : Show/hide list";
            this.LueAreaBongkar.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAreaBongkar.EditValueChanged += new System.EventHandler(this.LueAreaBongkar_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(48, 273);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 18);
            this.label1.TabIndex = 52;
            this.label1.Text = "Area Bongkar/Muat";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmTimbanganAD4329
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 618);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmTimbanganAD4329";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicenceNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTransportType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeightBefore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeightAfter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeightNetto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDtAf.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDtAf.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAreaBongkar.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit txtLicenceNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit txtDriverName;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LueItem;
        private DevExpress.XtraEditors.LookUpEdit LueTransportType;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        internal DevExpress.XtraEditors.TextEdit txtWeightNetto;
        internal DevExpress.XtraEditors.TextEdit txtWeightAfter;
        internal DevExpress.XtraEditors.TextEdit txtWeightBefore;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDtAf;
        private System.Windows.Forms.Label lblTimeAf;
        private System.Windows.Forms.Label lblTimeBf;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblJenisTimbangan;
        private DevExpress.XtraEditors.LookUpEdit LueAreaBongkar;
        private System.Windows.Forms.Label label1;


    }
}