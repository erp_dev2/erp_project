﻿#region Update 
/*
    02/01/2017 [HAR] competence dihapus
    29/03/2018 [HAR] tambah inputan internal atau external dari option 
    31/07/2018 [HAR] tambah inputan detail class
    15/08/2018 [WED] tambah input Minimal Score, Tm dan Trainer Name boleh kosong
    05/09/2018 [HAR] tambah inputan site, waktu insert bisa milih all type trainer, waktu edit hanya ambil dari master training
    24/07/2020 [ICA] Minimal Score tidak mandatory dengan parameter IsTrainingScoreNotMandatory
    27/07/2020 [IBL/SIER] tambah pilihan all pada site berdasarkan parameter IsTrainingUseAllSite
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmTraining : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmTrainingFind FrmFind;
        internal bool
           mIsFilterBySiteHR = false,
           mIsFilterBySite = false,
           mIsTrainingScoreNotMandatory = false,
           mIsTrainingUseAllSite = false
           ; 

        #endregion

        #region Constructor

        public FrmTraining(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            GetParameter();
            SetGrd();
            SetLueSiteCode(ref LueSiteCode, "");
            Sl.SetLueTrainerType(ref LueTrainingType);
            SetFormControl(mState.View);

            if (mIsTrainingScoreNotMandatory)
                this.label10.ForeColor = System.Drawing.Color.Black;
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTrainingCode, TxtTrainingName, ChkActInd, LueTrainingType, LueSiteCode, TxtMinScore
                    }, true);
                    TxtTrainingCode.Focus();
                    Grd1.ReadOnly = true;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTrainingCode, TxtTrainingName, LueTrainingType, TxtMinScore, LueSiteCode
                    }, false);
                    TxtTrainingCode.Focus();
                    Grd1.ReadOnly = false;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTrainingName, ChkActInd, LueTrainingType, TxtMinScore
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtTrainingName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTrainingCode, TxtTrainingName, LueTrainingType, LueSiteCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtMinScore }, 0);
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "Dno",
                    
                    //1-5
                    "Class",
                    "Time",
                    "",
                    "Trainer Code",
                    "Trainer Name",
                    //6
                    "Remark"
                },
                 new int[] 
                {
                    //0
                    20, 

                    //1-5
                    120, 80, 20, 120, 250,
                    //
                    200
                }
            );
            Sm.GrdFormatTime(Grd1, new int[] { 2 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 4, 5});
            Sm.GrdColInvisible(Grd1, new int[] { 0, 4 });
            #endregion
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTrainingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTrainingCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTrainingCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblTraining Where TrainingCode=@TrainingCode" };
                Sm.CmParam<String>(ref cm, "@TrainingCode", TxtTrainingCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveTraining());

                if (Grd1.Rows.Count > 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        {
                            cml.Add(SaveTrainingDtl(Row));
                        }
                        string TrainerCode = Sm.GetGrdStr(Grd1, Row, 4);
                        string TCode = String.Empty;
                        int index = 0;
                        int CountData = 0;
                        if (TrainerCode.Length > 0)
                        {
                            for (int i = 1; i < TrainerCode.Length; i++)
                            {
                                if (TrainerCode[i] == '#')
                                {
                                    TCode = TrainerCode.Substring(index, i - index);
                                    index = i + 1;
                                    CountData = CountData + 1;
                                    cml.Add(SaveTrainingDtl2(TxtTrainingCode.Text, Row, CountData, TCode));
                                }
                            }
                        }
                    }
                }

                Sm.ExecCommands(cml);
                ShowData(TxtTrainingCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string TrainingCode)
        {
            try
            {
                ClearData();
                ShowTraining(TrainingCode);
                ShowTrainingDtl(TrainingCode);
                Cursor.Current = Cursors.WaitCursor;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        public void ShowTraining(string TrainingCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@TrainingCode", TrainingCode);
                Sm.ShowDataInCtrl(
                    ref cm,
                    "Select TrainingCode, TrainingName, ActiveInd, TrainingType, SiteCode,  MinScore From TblTraining Where TrainingCode=@TrainingCode",
                    new string[] 
                    {
                        "TrainingCode", 
                        "TrainingName", "ActiveInd", "TrainingType", "SiteCode", "MinScore"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtTrainingCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtTrainingName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        Sm.SetLue(LueTrainingType, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[4]));
                        TxtMinScore.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTrainingDtl(string TrainingCode)
        {
            var cm = new MySqlCommand();
           
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.trainingCode, B.Dno, B.Class, B.Tm, ");  
            SQL.AppendLine("B.TrainerCode As Tcode, group_Concat(D.TrainerName) TName , B.Remark ");
            SQL.AppendLine("from tblTraining A ");
            SQL.AppendLine("Inner Join TbltraininGDtl B On A.trainingCode = B.TrainingCode ");
            SQL.AppendLine("left Join TbltrainingDtl2 C On Concat(B.trainingCode, B.Dno)  = Concat(C.trainingCode, C.Dno) ");
            SQL.AppendLine("left Join TblTrainer D On C.TrainerCode = D.TrainerCode ");
            SQL.AppendLine("Where A.TrainingCode=@TrainingCode ");


            Sm.CmParam<String>(ref cm, "@TrainingCode", TrainingCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + " Group By  B.trainingCode, B.Dno, B.Class, B.Tm ;",
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "Class", "Tm", "Tcode", "TName", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }


        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTrainingCode, "Training code", false) ||
                Sm.IsTxtEmpty(TxtTrainingName, "Training name", false) ||
                Sm.IsLueEmpty(LueTrainingType, "Training type") ||
                (!mIsTrainingScoreNotMandatory) ? Sm.IsTxtEmpty(TxtMinScore, "Min. Score", true) : false ||
                IsTrainingCodeExisted()||
                IsTrainingAlreadyNonActive()||
                IsGrdEmpty();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 class.");
                return true;
            }
            return false;
        }

        private bool IsTrainingCodeExisted()
        {
            if (!TxtTrainingCode.Properties.ReadOnly && Sm.IsDataExist("Select TrainingCode From TblTraining Where TrainingCode='" + TxtTrainingCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Training code ( " + TxtTrainingCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsTrainingAlreadyNonActive()
        {
            if (Sm.IsDataExist("Select TrainingCode From TblTraining Where TrainingCode='" + TxtTrainingCode.Text + "' And ActiveInd='N' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Training code ( " + TxtTrainingCode.Text + " ) already Non Active.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveTraining()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTraining(TrainingCode, TrainingName, ActiveInd, TrainingType, MinScore, SiteCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@TrainingCode, @TrainingName,  @ActiveInd, @TrainingType, @MinScore, @SiteCode, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update TrainingName=@TrainingName, ActiveInd=@ActiveInd, TrainingType=@TrainingType, MinScore = @MinScore, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            SQL.AppendLine("Delete From TbltrainingDtl Where TrainingCode=@TrainingCode; ");
            SQL.AppendLine("Delete From TbltrainingDtl2 Where TrainingCode=@TrainingCode; ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@TrainingCode", TxtTrainingCode.Text);
            Sm.CmParam<String>(ref cm, "@TrainingName", TxtTrainingName.Text);
            Sm.CmParam<String>(ref cm, "@TrainingType", Sm.GetLue(LueTrainingType));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<Decimal>(ref cm, "@MinScore", decimal.Parse(TxtMinScore.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ActiveInd", ChkActInd.Checked ? "Y" : "N");

            return cm;
        }

        private MySqlCommand SaveTrainingDtl(int Row)
        {
            string Tm1 = string.Empty;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblTrainingDtl(TrainingCode, Dno, Class, Tm, TrainerCode, Remark, CreateBy, CreateDt) " +
                    "Values (@TrainingCode, @Dno, @Class, @Tm, @TrainerCode, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@TrainingCode", TxtTrainingCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Class", Sm.GetGrdStr(Grd1, Row, 1));
            Tm1 = Sm.GetGrdStr(Grd1, Row, 2);
            if(Tm1.Trim().Length > 0)
                Sm.CmParam<String>(ref cm, "@Tm", string.Concat(Sm.Left(Tm1, 2), Sm.Right(Tm1, 2)));
            else
                Sm.CmParam<String>(ref cm, "@Tm", string.Empty);
            Sm.CmParam<String>(ref cm, "@TrainerCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveTrainingDtl2(string TrainingCode, int Row, int Count, string TrainerCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblTrainingDtl2(TrainingCode, DNo, DNo2, TrainerCode, CreateBy, CreateDt) " +
                    "Values(@TrainingCode, @DNo, @DNo2, @TrainerCode, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@TrainingCode", TrainingCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DNo2", Sm.Right("00" + Count.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@TrainerCode", TrainerCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsTrainingScoreNotMandatory = Sm.GetParameterBoo("IsTrainingScoreNotMandatory");
            mIsTrainingUseAllSite = Sm.GetParameterBoo("IsTrainingUseAllSite");
        }

        private void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (mIsTrainingUseAllSite)
                {
                    SQL.AppendLine("Select 'all' As Col1, 'ALL' As Col2 ");
                    SQL.AppendLine("Union All ");
                }
                SQL.AppendLine("Select A.Col1, A.Col2 ");
                SQL.AppendLine("From ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T.SiteCode As Col1, T.SiteName As Col2 ");
                SQL.AppendLine("    From TblSite T ");
                if (SiteCode.Length == 0)
                    SQL.AppendLine("    Where T.ActInd='Y' ");
                else
                    SQL.AppendLine("    Where T.SiteCode=@SiteCode ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=T.SiteCode ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By SiteName ");
                SQL.AppendLine(")A; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (SiteCode.Length != 0)
                    Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
                if (mIsFilterBySite)
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #endregion

        #region Event


        #region grid event
        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            string TrainingCode = string.Empty;
            string TrainingType = string.Empty;
            string TypeDoc = "0";

            if (TxtTrainingCode.Properties.ReadOnly == true)
            {
                TrainingCode = TxtTrainingCode.Text;
                TypeDoc = "1";
            }
            else
            {
                TrainingType = Sm.GetLue(LueTrainingType);
            }
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                Sm.FormShowDialog(new FrmTrainingDlg(this, TrainingType, e.RowIndex, TrainingCode, TypeDoc));
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            var Value = string.Empty;
            if (e.ColIndex == 2)
            {
                Value = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                if (Value.Length == 4 || Value.Length == 3)
                {
                    SetTime(Grd1, e.RowIndex, 2, 2);
                }
                else
                {
                    if (!(Value.Length == 5 && Value.Substring(2, 1) == ":"))
                    {
                        Grd1.Cells[e.RowIndex, 2].Value = Grd1.Cells[e.RowIndex, 2].Value = string.Empty;
                    }
                }
                Sm.FocusGrd(Grd1, e.RowIndex, 1);
            }

        }

        public void SetTime(iGrid GrdXXX, int RowXXX, int ColActual, int ColEdit)
        {
            try
            {
                if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 4 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                {
                    GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat(Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                    GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                }
                if (Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit).Length == 3 && Convert.ToDecimal(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit)) <= 2359 && Convert.ToDecimal(Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2)) <= 59)
                {
                    GrdXXX.Cells[RowXXX, ColActual].Value = String.Concat("0", Sm.Left(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 1), ":", Sm.Right(Sm.GetGrdStr(GrdXXX, RowXXX, ColEdit), 2));
                    GrdXXX.Cells[RowXXX, ColEdit].Value = Sm.GetGrdStr(GrdXXX, RowXXX, ColActual);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            string TrainingCode = string.Empty;
            string TrainingType = string.Empty;
            string TypeDoc = "0";

            if (TxtTrainingCode.Properties.ReadOnly == true)
            {
                TrainingCode = TxtTrainingCode.Text;
                TypeDoc = "1";
            }
            else
            {
                TrainingType = Sm.GetLue(LueTrainingType);
            }

            if (e.ColIndex == 3)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmTrainingDlg(this, TrainingType, e.RowIndex, TrainingCode, TypeDoc));
            }
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }

        }
        #endregion

        #region Misc Control Event

        private void TxtTrainingCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTrainingCode);
        }

        private void TxtTrainingName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTrainingName);
        }

        private void LueTrainingType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingType, new Sm.RefreshLue1(Sl.SetLueTrainerType));
        }

        private void TxtMinScore_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtMinScore, 0);
        }

        #endregion

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(SetLueSiteCode), string.Empty);
            }
        }

        #endregion

    }
}
