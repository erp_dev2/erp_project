﻿#region Update
/*
    27/03/2018 [WED] ubah rumus yang competence
    09/04/2018 [HAR] tambah dialog matrix, button show employee ganti print
    16/05/2018 [WED] Matrix juga mengambil potensi
    31/05/2018 [WED] printout header, query ke TblPosition diganti jadi Left Join
    10/09/2018 [WED] tambah button ke master employee
    27/09/2018 [WED] BUG Fixing Performance Review masih salah ambil nomor dokumennya
    19/05/2022 [ICA/TWC] Hanya menarik document performance review yg active
    27/05/2022 [MYA/HIN] Penyesuaian reporting employee performance and competence matrix
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpMatrixPerformanceCompetence : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false, mIsRptKPIUseGoalsSetting = false;
        private int HL =0, 
            HM=0, HH=0, ML=0,  MM=0, MH=0, LL=0,  LM=0, LH = 0;

        #endregion

        #region Constructor

        public FrmRptEmpMatrixPerformanceCompetence(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueEmpStatus(ref LueStatus);
                Sm.SetLue(LueStatus, "1");
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.ReadOnly = false;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 37;
            Grd1.FrozenArea.ColCount = 1;

            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].Value = "No.";
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.MiddleCenter;
            Grd1.Header.Cells[0, 0].SpanRows = 2;
            
            Grd1.Header.Cells[1, 1].Value = "High Performance" + Environment.NewLine + "High Competence";
            Grd1.Header.Cells[1, 1].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 1].SpanCols = 4;
            Grd1.Header.Cells[0, 1].Value = "Employee Code";
            Grd1.Header.Cells[0, 2].Value = "";
            Grd1.Header.Cells[0, 3].Value = "";
            Grd1.Header.Cells[0, 4].Value = "Employee Name";
            Grd1.Cols[1].Width = 130;
            Grd1.Cols[2].Width = 20;
            Grd1.Cols[3].Width = 20;
            Grd1.Cols[4].Width = 200;

            Grd1.Header.Cells[1, 5].Value = "High Performance" + Environment.NewLine + "Medium Competence";
            Grd1.Header.Cells[1, 5].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 5].SpanCols = 4;
            Grd1.Header.Cells[0, 5].Value = "Employee Code";
            Grd1.Header.Cells[0, 6].Value = "";
            Grd1.Header.Cells[0, 7].Value = "";
            Grd1.Header.Cells[0, 8].Value = "Employee Name";
            Grd1.Cols[5].Width = 130;
            Grd1.Cols[6].Width = 20;
            Grd1.Cols[7].Width = 20;
            Grd1.Cols[8].Width = 200;

            Grd1.Header.Cells[1, 9].Value = "High Performance" + Environment.NewLine + "Low Competence";
            Grd1.Header.Cells[1, 9].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 9].SpanCols = 4;
            Grd1.Header.Cells[0, 9].Value = "Employee Code";
            Grd1.Header.Cells[0, 10].Value = "";
            Grd1.Header.Cells[0, 11].Value = "";
            Grd1.Header.Cells[0, 12].Value = "Employee Name";
            Grd1.Cols[9].Width = 130;
            Grd1.Cols[10].Width = 20;
            Grd1.Cols[10].Width = 20;
            Grd1.Cols[12].Width = 200;

            Grd1.Header.Cells[1, 13].Value = "Medium Performance" + Environment.NewLine + "High Competence";
            Grd1.Header.Cells[1, 13].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 13].SpanCols = 4;
            Grd1.Header.Cells[0, 13].Value = "Employee Code";
            Grd1.Header.Cells[0, 14].Value = "";
            Grd1.Header.Cells[0, 15].Value = "";
            Grd1.Header.Cells[0, 16].Value = "Employee Name";
            Grd1.Cols[13].Width = 130;
            Grd1.Cols[14].Width = 20;
            Grd1.Cols[14].Width = 20;
            Grd1.Cols[16].Width = 200;

            Grd1.Header.Cells[1, 17].Value = "Medium Performance" + Environment.NewLine + "Medium Competence";
            Grd1.Header.Cells[1, 17].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 17].SpanCols = 4;
            Grd1.Header.Cells[0, 17].Value = "Employee Code";
            Grd1.Header.Cells[0, 18].Value = "";
            Grd1.Header.Cells[0, 19].Value = "";
            Grd1.Header.Cells[0, 20].Value = "Employee Name";
            Grd1.Cols[17].Width = 130;
            Grd1.Cols[18].Width = 20;
            Grd1.Cols[19].Width = 20;
            Grd1.Cols[20].Width = 200;

            Grd1.Header.Cells[1, 21].Value = "Medium Performance" + Environment.NewLine + "Low Competence";
            Grd1.Header.Cells[1, 21].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 21].SpanCols = 4;
            Grd1.Header.Cells[0, 21].Value = "Employee Code";
            Grd1.Header.Cells[0, 22].Value = "";
            Grd1.Header.Cells[0, 23].Value = "";
            Grd1.Header.Cells[0, 24].Value = "Employee Name";
            Grd1.Cols[21].Width = 130;
            Grd1.Cols[22].Width = 20;
            Grd1.Cols[23].Width = 20;
            Grd1.Cols[24].Width = 200;

            Grd1.Header.Cells[1, 25].Value = "Low Performance" + Environment.NewLine + "High Competence";
            Grd1.Header.Cells[1, 25].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 25].SpanCols = 4;
            Grd1.Header.Cells[0, 25].Value = "Employee Code";
            Grd1.Header.Cells[0, 26].Value = "";
            Grd1.Header.Cells[0, 27].Value = "";
            Grd1.Header.Cells[0, 28].Value = "Employee Name";
            Grd1.Cols[25].Width = 130;
            Grd1.Cols[26].Width = 20;
            Grd1.Cols[27].Width = 20;
            Grd1.Cols[28].Width = 200;

            Grd1.Header.Cells[1, 29].Value = "Low Performance" + Environment.NewLine + "Medium Competence";
            Grd1.Header.Cells[1, 29].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 29].SpanCols = 4;
            Grd1.Header.Cells[0, 29].Value = "Employee Code";
            Grd1.Header.Cells[0, 30].Value = "";
            Grd1.Header.Cells[0, 31].Value = "";
            Grd1.Header.Cells[0, 32].Value = "Employee Name";
            Grd1.Cols[29].Width = 130;
            Grd1.Cols[30].Width = 20;
            Grd1.Cols[31].Width = 20;
            Grd1.Cols[32].Width = 200;

            Grd1.Header.Cells[1, 33].Value = "Low Performance" + Environment.NewLine + "Low Competence";
            Grd1.Header.Cells[1, 33].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Header.Cells[1, 33].SpanCols = 4;
            Grd1.Header.Cells[0, 33].Value = "Employee Code";
            Grd1.Header.Cells[0, 34].Value = "";
            Grd1.Header.Cells[0, 35].Value = "";
            Grd1.Header.Cells[0, 36].Value = "Employee Name";
            Grd1.Cols[33].Width = 130;
            Grd1.Cols[34].Width = 20;
            Grd1.Cols[35].Width = 20;
            Grd1.Cols[36].Width = 200;

            for (int Index = 1; Index < Grd1.Cols.Count; Index++)
                Grd1.Cols[Index].ColHdrStyle.TextAlign = iGContentAlignment.MiddleCenter;
            Sm.GrdColButton(Grd1, new int[] { 2, 3, 6, 7, 10, 11, 14, 15, 18, 19, 22, 23, 26, 27, 30, 31, 34, 35 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 6, 7, 9, 10, 11, 13, 14, 15, 17, 18, 19, 21, 22, 23, 25, 26, 27, 29, 30, 31, 33, 34, 35 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 4, 5, 8, 9, 12, 13, 16, 17, 20, 21, 24, 25, 28, 29, 32, 33 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 5, 6, 7, 9, 10, 11, 13, 14, 15, 17, 18, 19, 21, 22, 23, 25, 26, 27, 29, 30, 31, 33, 34, 35 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueSiteCode, "Site")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Sm.ClearGrd(Grd1, false);

                var l = new List<Employee>();
                var lET = new List<EmployeeTraining>();
                var lEC = new List<EmployeeCompetence>();
                var lEC1 = new List<ListEmpCompetence>();
                var lEP = new List<EmployeePerformance>();
                var lECR = new List<EmployeeCompetenceRecap>();
                var lAP = new List<AssesmentProcess>();
                var lAR = new List<AssesmentRecap>();
                var lA = new List<Assesment>();
                var lOSC = new List<OrgStructCompetence>();
                decimal mHC = 50m, mSC = 50m;

                ListEmployee(ref l);
                if (l.Count > 0)
                {
                    ListEmployeeTraining(ref l, ref lET);
                    ListOrgStructCompetence(ref l, ref lOSC);
                    ListAssesmentProcess(ref l, ref lAP);
                    ListAssesment(ref lAP, ref lA);
                    RecapAssesmentCompetence(ref l, ref lA, ref lAP, ref lOSC, ref lAR);
                    ListEmployeeCompetence(ref lAR, ref lEC);
                    ListEmployeePerformance(ref l, ref lEP);

                    foreach (var x in l)
                    {
                        var a = Sm.GetValue("Select HardCompetence From tblorganizationalstructure Where PosCode In (Select T.PosCode From TblEmployee T Where T.EmpCode = '" + x.EmpCode + "' )");
                        var b = Sm.GetValue("Select SoftCompetence From tblorganizationalstructure Where PosCode In (Select T.PosCode From TblEmployee T Where T.EmpCode = '" + x.EmpCode + "' )");

                        if (a.Length > 0 && b.Length > 0)
                        {
                            if (
                                    Decimal.Parse(Sm.GetValue("Select HardCompetence From tblorganizationalstructure Where PosCode In (Select T.PosCode From TblEmployee T Where T.EmpCode = '" + x.EmpCode + "' )")) != 0 ||
                                    Decimal.Parse(Sm.GetValue("Select SoftCompetence From tblorganizationalstructure Where PosCode In (Select T.PosCode From TblEmployee T Where T.EmpCode = '" + x.EmpCode + "' )")) != 0
                                )
                            {
                                mHC = Decimal.Parse(Sm.GetValue("Select HardCompetence From tblorganizationalstructure Where PosCode In (Select T.PosCode From TblEmployee T Where T.EmpCode = '" + x.EmpCode + "' )"));
                                mSC = Decimal.Parse(Sm.GetValue("Select SoftCompetence From tblorganizationalstructure Where PosCode In (Select T.PosCode From TblEmployee T Where T.EmpCode = '" + x.EmpCode + "' )"));
                            }
                        }

                        lECR.Add(new EmployeeCompetenceRecap()
                        {
                            EmpCode = x.EmpCode,
                            EmpTraining = 0m,
                            PosTraining = 0m,
                            HardCompetence = mHC,
                            Score = 0m,
                            SoftCompetence = mSC,
                            PointHardCompetence = 0m,
                            PointSoftCompetence = 0m,
                            TotalCompetence = 0m,
                            ResultCompetence = "Low"
                        });
                    }

                    if (lET.Count > 0)
                    {
                        RecapEmployeeCompetence(ref lET, ref lECR);
                    }

                    if (lEC.Count > 0)
                    {
                        RecapEmployeeCompetence2(ref lEC, ref lECR);
                    }

                    RecapEmployeeCompetence3(ref lECR);

                    for (int i = 0; i < lECR.Count; i++)
                    {
                        if (lECR[i].ResultCompetence.ToUpper() != "LOW")
                        {
                            l[i].CompetenceLevel = lECR[i].ResultCompetence;
                        }
                    }

                    if (lEP.Count > 0)
                    {
                        for (int j = 0; j < lEP.Count; j++)
                        {
                            for (int k = 0; k < l.Count; k++)
                            {
                                if (lEP[j].EmpCode == l[k].EmpCode)
                                {
                                    if (lEP[j].Grade.ToUpper() != "LOW")
                                    {
                                        l[k].PerformanceLevel = lEP[j].Grade;
                                    }
                                }
                            }
                        }
                    }

                    ShowDataInGrd(ref l);

                    l.Clear();
                    lET.Clear();
                    lEC.Clear();
                    lEC1.Clear();
                    lEP.Clear();
                    lECR.Clear();
                    lAP.Clear();
                    lAR.Clear();
                    lA.Clear();
                    lOSC.Clear();
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsRptKPIUseGoalsSetting = Sm.GetParameterBoo("IsRptKPIUseGoalsSetting");
        }

        private void ListEmployee(ref List<Employee> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode, EmpName, PosCode ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where 0 = 0 ");

            if(Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine("And SiteCode = @SiteCode ");

            if (Sm.GetLue(LueDeptCode).Length > 0)
                SQL.AppendLine("And DeptCode = @DeptCode ");

            if (Sm.GetLue(LueStatus).Length > 0)
            {
                switch (Sm.GetLue(LueStatus))
                {
                    case "1":
                        SQL.AppendLine("And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@CurrentDate)) ");
                        break;
                    case "2":
                        SQL.AppendLine("And ResignDt Is Not Null And ResignDt<=@CurrentDate ");
                        break;
                }
            }

            if(TxtEmpCode.Text.Length > 0)
                SQL.AppendLine("And (EmpCode Like @EmpCode Or EmpName Like @EmpCode Or EmpCodeOld Like @EmpCode) ");

            SQL.AppendLine("Order By EmpName; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@EmpCode", string.Concat("%", TxtEmpCode.Text, "%"));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "EmpName", "PosCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpName = Sm.DrStr(dr, c[1]),
                            PosCode = Sm.DrStr(dr, c[2]),
                            CompetenceLevel = "Low",
                            PerformanceLevel = "Low"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ListEmployeeTraining(ref List<Employee> l, ref List<EmployeeTraining> lET)
        {
            string mEmpCode = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mEmpCode.Length > 0) mEmpCode += ",";
                mEmpCode += l[i].EmpCode;
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("select a.EmpCode, a.PosCode, Sum(a.CountEmpTraining) as CountEmpTraining, b.CountPosTraining ");
            SQL.AppendLine("from  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select X1.EmpCode, X2.PosCode, X1.TrainingCode, 1 As CountEmpTraining ");
	        SQL.AppendLine("    From TblEmployeeTraining X1 ");
	        SQL.AppendLine("    Inner Join TblEmployee X2 On X1.EmpCode = X2.EmpCode And Find_In_Set(X2.EmpCode, @EmpCode) ");
	        SQL.AppendLine("    Inner Join TblPositionTraining X3 On X1.TrainingCode = X3.TrainingCode And X2.PosCode = X3.PosCode ");
	        SQL.AppendLine("    Group By X1.EmpCode, X2.PosCode, X1.TrainingCode ");
            SQL.AppendLine(") a ");
            SQL.AppendLine("inner join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select T1.PosCode, Count(T1.TrainingCode) As CountPosTraining ");
	        SQL.AppendLine("    From TblPositionTraining T1 ");
	        SQL.AppendLine("    Group By T1.PosCode ");
            SQL.AppendLine(") b on a.PosCode = b.PosCode ");
            SQL.AppendLine("Group by a.EmpCode, a.PosCode, b.CountPosTraining; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "PosCode", "CountEmpTraining", "CountPosTraining" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lET.Add(new EmployeeTraining()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            PosCode = Sm.DrStr(dr, c[1]),
                            CountEmpTraining = Sm.DrDec(dr, c[2]),
                            CountPosTraining = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ListAssesmentProcess(ref List<Employee> l, ref List<AssesmentProcess> lAP)
        {
            string mEmpCode = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mEmpCode.Length > 0) mEmpCode += ",";
                mEmpCode += l[i].EmpCode;
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select X2.EmpCode, X2.AsmCode, X2.DocNo, ");
            SQL.AppendLine("X1.Score1, X1.Score2, X1.Score3, X1.Score4, X1.Score5, ");
            SQL.AppendLine("X1.Score6, X1.Score7, X1.Score8, X1.Score9, X1.Score10, X1.Score11, X1.Score12, X1.Score13, X1.Score14, ");
            SQL.AppendLine("X1.Score15, X1.Score16, X1.Score17, X1.Score18, X1.Score19, X1.Score20 ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.EmpCode, T2.AsmCode, Max(T2.DocNo) DocNo ");
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select A.DocNo ");
	        SQL.AppendLine("        From tblassesmentprocesshdr A ");
	        SQL.AppendLine("        Where A.CancelInd = 'N' ");
	        SQL.AppendLine("        And Find_In_Set(A.EmpCode, @EmpCode) ");
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Inner Join TblAssesmentProcessHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Group By T2.EmpCode, T2.AsmCode ");
            SQL.AppendLine(") X2 ");
            SQL.AppendLine("Inner Join TblAssesmentProcessHdr X1 On X2.DocNo = X1.DocNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    //1-5
                    "AsmCode", "DocNo", "Score1", "Score2", "Score3", 
                    //6-10
                    "Score4", "Score5", "Score6", "Score7", "Score8", 
                    //11-15
                    "Score9", "Score10", "Score11", "Score12", "Score13", 
                    //16-20
                    "Score14", "Score15", "Score16", "Score17", "Score18", 
                    //21-22
                    "Score19", "Score20"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lAP.Add(new AssesmentProcess()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            AsmCode = Sm.DrStr(dr, c[1]),
                            DocNo = Sm.DrStr(dr, c[2]),
                            Score1 = Sm.DrDec(dr, c[3]),
                            Score2 = Sm.DrDec(dr, c[4]),
                            Score3 = Sm.DrDec(dr, c[5]),
                            Score4 = Sm.DrDec(dr, c[6]),
                            Score5 = Sm.DrDec(dr, c[7]),
                            Score6 = Sm.DrDec(dr, c[8]),
                            Score7 = Sm.DrDec(dr, c[9]),
                            Score8 = Sm.DrDec(dr, c[10]),
                            Score9 = Sm.DrDec(dr, c[11]),
                            Score10 = Sm.DrDec(dr, c[12]),
                            Score11 = Sm.DrDec(dr, c[13]),
                            Score12 = Sm.DrDec(dr, c[14]),
                            Score13 = Sm.DrDec(dr, c[15]),
                            Score14 = Sm.DrDec(dr, c[16]),
                            Score15 = Sm.DrDec(dr, c[17]),
                            Score16 = Sm.DrDec(dr, c[18]),
                            Score17 = Sm.DrDec(dr, c[19]),
                            Score18 = Sm.DrDec(dr, c[20]),
                            Score19 = Sm.DrDec(dr, c[21]),
                            Score20 = Sm.DrDec(dr, c[22]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ListOrgStructCompetence(ref List<Employee> l, ref List<OrgStructCompetence> lOSC)
        {
            string mEmpCode = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mEmpCode.Length > 0) mEmpCode += ",";
                mEmpCode += l[i].EmpCode;
            }

            var SQL = new StringBuilder();

            //SQL.AppendLine("Select PosCode, CompetenceCode ");
            //SQL.AppendLine("From TblOrganizationalStructureDtl4 ");
            //SQL.AppendLine("Where PosCode In ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T.PosCode ");
            //SQL.AppendLine("    From TblEmployee T ");
            //SQL.AppendLine("    Where Find_In_Set(T.EmpCode, @EmpCode) ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("Group By PosCode, CompetenceCode; ");

            SQL.AppendLine("Select X.PosCode, X.CompetenceCode ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X1.PosCode, X1.CompetenceCode ");
            SQL.AppendLine("    From TblOrganizationalStructureDtl4 X1 ");
            SQL.AppendLine("    Where X1.PosCode In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T.PosCode ");
            SQL.AppendLine("        From TblEmployee T ");
            SQL.AppendLine("        Where Find_In_Set(T.EmpCode, @EmpCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select X2.PosCode, X2.PotenceCode As CompetenceCode ");
            SQL.AppendLine("    From TblOrganizationalStructureDtl7 X2 ");
            SQL.AppendLine("    Where X2.PosCode In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T.PosCode ");
            SQL.AppendLine("        From TblEmployee T ");
            SQL.AppendLine("        Where Find_In_Set(T.EmpCode, @EmpCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Group By X.PosCode, X.CompetenceCode; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PosCode", "CompetenceCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lOSC.Add(new OrgStructCompetence()
                        {
                            PosCode = Sm.DrStr(dr, c[0]),
                            CompetenceCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ListAssesment(ref List<AssesmentProcess> lAP, ref List<Assesment> lA)
        {
            string mAsmCode = string.Empty;
            for (int i = 0; i < lAP.Count; i++)
            {
                if (mAsmCode.Length > 0) mAsmCode += ",";
                mAsmCode += lAP[i].AsmCode;
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select AsmCode, CompetenceCode1, Level1, CompetenceCode2, Level2, CompetenceCode3, Level3, ");
            SQL.AppendLine("CompetenceCode4, Level4, CompetenceCode5, Level5, CompetenceCode6, Level6, ");
            SQL.AppendLine("CompetenceCode7, Level7, CompetenceCode8, Level8, CompetenceCode9, Level9, ");
            SQL.AppendLine("CompetenceCode10, Level10, CompetenceCode11, Level11, CompetenceCode12, Level12, ");
            SQL.AppendLine("CompetenceCode13, Level13, CompetenceCode14, Level14, CompetenceCode15, ");
            SQL.AppendLine("Level15, CompetenceCode16, Level16, CompetenceCode17, Level17, CompetenceCode18, ");
            SQL.AppendLine("Level18, CompetenceCode19, Level19, CompetenceCode20, Level20 ");
            SQL.AppendLine("From TblAssesmentHdr ");
            SQL.AppendLine("Where Find_In_Set(AsmCode, @AsmCode); ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@AsmCode", mAsmCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "AsmCode", 
                    //1-5
                    "CompetenceCode1", "Level1", "CompetenceCode2", "Level2", "CompetenceCode3", 
                    //6-10
                    "Level3", "CompetenceCode4", "Level4", "CompetenceCode5", "Level5", 
                    //11-15
                    "CompetenceCode6", "Level6", "CompetenceCode7", "Level7", "CompetenceCode8", 
                    //16-20
                    "Level8", "CompetenceCode9", "Level9", "CompetenceCode10", "Level10", 
                    //21-25
                    "CompetenceCode11", "Level11", "CompetenceCode12", "Level12", "CompetenceCode13", 
                    //26-30
                    "Level13", "CompetenceCode14", "Level14", "CompetenceCode15", "Level15", 
                    //31-35
                    "CompetenceCode16", "Level16", "CompetenceCode17", "Level17", "CompetenceCode18", 
                    //36-40
                    "Level18", "CompetenceCode19", "Level19", "CompetenceCode20", "Level20",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lA.Add(new Assesment()
                        {
                            AsmCode = Sm.DrStr(dr, c[0]),
                            CompetenceCode1 = Sm.DrStr(dr, c[1]),
                            Level1 = Sm.DrStr(dr, c[2]),
                            CompetenceCode2 = Sm.DrStr(dr, c[3]),
                            Level2 = Sm.DrStr(dr, c[4]),
                            CompetenceCode3 = Sm.DrStr(dr, c[5]),
                            Level3 = Sm.DrStr(dr, c[6]),
                            CompetenceCode4 = Sm.DrStr(dr, c[7]),
                            Level4 = Sm.DrStr(dr, c[8]),
                            CompetenceCode5 = Sm.DrStr(dr, c[9]),
                            Level5 = Sm.DrStr(dr, c[10]),
                            CompetenceCode6 = Sm.DrStr(dr, c[11]),
                            Level6 = Sm.DrStr(dr, c[12]),
                            CompetenceCode7 = Sm.DrStr(dr, c[13]),
                            Level7 = Sm.DrStr(dr, c[14]),
                            CompetenceCode8 = Sm.DrStr(dr, c[15]),
                            Level8 = Sm.DrStr(dr, c[16]),
                            CompetenceCode9 = Sm.DrStr(dr, c[17]),
                            Level9 = Sm.DrStr(dr, c[18]),
                            CompetenceCode10 = Sm.DrStr(dr, c[19]),
                            Level10 = Sm.DrStr(dr, c[20]),
                            CompetenceCode11 = Sm.DrStr(dr, c[21]),
                            Level11 = Sm.DrStr(dr, c[22]),
                            CompetenceCode12 = Sm.DrStr(dr, c[23]),
                            Level12 = Sm.DrStr(dr, c[24]),
                            CompetenceCode13 = Sm.DrStr(dr, c[25]),
                            Level13 = Sm.DrStr(dr, c[26]),
                            CompetenceCode14 = Sm.DrStr(dr, c[27]),
                            Level14 = Sm.DrStr(dr, c[28]),
                            CompetenceCode15 = Sm.DrStr(dr, c[29]),
                            Level15 = Sm.DrStr(dr, c[30]),
                            CompetenceCode16 = Sm.DrStr(dr, c[31]),
                            Level16 = Sm.DrStr(dr, c[32]),
                            CompetenceCode17 = Sm.DrStr(dr, c[33]),
                            Level17 = Sm.DrStr(dr, c[34]),
                            CompetenceCode18 = Sm.DrStr(dr, c[35]),
                            Level18 = Sm.DrStr(dr, c[36]),
                            CompetenceCode19 = Sm.DrStr(dr, c[37]),
                            Level19 = Sm.DrStr(dr, c[38]),
                            CompetenceCode20 = Sm.DrStr(dr, c[39]),
                            Level20 = Sm.DrStr(dr, c[40]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void RecapAssesmentCompetence(
            ref List<Employee> l,
            ref List<Assesment> lA, 
            ref List<AssesmentProcess> lAP, 
            ref List<OrgStructCompetence> lOSC,
            ref List<AssesmentRecap> lAR
            )
        {
            for (int i = 0; i < l.Count; i++)
            {
                for (int j = 0; j < lAP.Count; j++)
                {
                    if(l[i].EmpCode == lAP[j].EmpCode)
                    {
                        for (int k = 0; k < lA.Count; k++)
                        {
                            if(lAP[j].AsmCode == lA[k].AsmCode)
                            {
                                for (int m = 0; m < lOSC.Count; m++)
                                {
                                    if(l[i].PosCode == lOSC[m].PosCode)
                                    {
                                        if(lOSC[m].CompetenceCode == lA[k].CompetenceCode1)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode1,
                                                Level = lA[k].Level1,
                                                Score = lAP[j].Score1
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode2)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode2,
                                                Level = lA[k].Level2,
                                                Score = lAP[j].Score2
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode3)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode3,
                                                Level = lA[k].Level3,
                                                Score = lAP[j].Score3
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode4)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode4,
                                                Level = lA[k].Level4,
                                                Score = lAP[j].Score4
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode5)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode5,
                                                Level = lA[k].Level5,
                                                Score = lAP[j].Score5
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode6)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode6,
                                                Level = lA[k].Level6,
                                                Score = lAP[j].Score6
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode7)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode7,
                                                Level = lA[k].Level7,
                                                Score = lAP[j].Score7
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode8)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode8,
                                                Level = lA[k].Level8,
                                                Score = lAP[j].Score8
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode9)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode9,
                                                Level = lA[k].Level9,
                                                Score = lAP[j].Score9
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode10)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode10,
                                                Level = lA[k].Level10,
                                                Score = lAP[j].Score10
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode11)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode11,
                                                Level = lA[k].Level11,
                                                Score = lAP[j].Score11
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode12)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode12,
                                                Level = lA[k].Level12,
                                                Score = lAP[j].Score12
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode13)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode13,
                                                Level = lA[k].Level13,
                                                Score = lAP[j].Score13
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode14)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode14,
                                                Level = lA[k].Level14,
                                                Score = lAP[j].Score14
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode15)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode15,
                                                Level = lA[k].Level15,
                                                Score = lAP[j].Score15
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode16)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode16,
                                                Level = lA[k].Level16,
                                                Score = lAP[j].Score16
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode17)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode17,
                                                Level = lA[k].Level17,
                                                Score = lAP[j].Score17
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode18)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode18,
                                                Level = lA[k].Level18,
                                                Score = lAP[j].Score18
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode19)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode19,
                                                Level = lA[k].Level19,
                                                Score = lAP[j].Score19
                                            });
                                        }

                                        if (lOSC[m].CompetenceCode == lA[k].CompetenceCode20)
                                        {
                                            lAR.Add(new AssesmentRecap()
                                            {
                                                EmpCode = lAP[j].EmpCode,
                                                CompetenceCode = lA[k].CompetenceCode20,
                                                Level = lA[k].Level20,
                                                Score = lAP[j].Score20
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ListEmployeeCompetence(ref List<AssesmentRecap> lAR, ref List<EmployeeCompetence> lEC)
        {
            var lTEC = new List<TempEmployeeCompetence>();
            string mEmpCode = string.Empty;
            bool IsExists = false;

            for (int i = 0; i < lAR.Count; i++)
            {
                if (mEmpCode.Length <= 0) mEmpCode = lAR[i].EmpCode;
                if (mEmpCode == lAR[i].EmpCode)
                {
                    if (lTEC.Count > 0)
                    {
                        for (int j = 0; j < lTEC.Count; j++)
                        {
                            if (lTEC[j].EmpCode == lAR[i].EmpCode)
                            {
                                lTEC[j].ScorePerLevel += ((lAR[i].Score / Decimal.Parse(lAR[i].Level)) * 100m);
                                lTEC[j].Count += 1;
                                IsExists = true;
                                break;
                            }
                        }

                        if (!IsExists)
                        {
                            lTEC.Add(new TempEmployeeCompetence()
                            {
                                EmpCode = lAR[i].EmpCode,
                                ScorePerLevel = ((lAR[i].Score / Decimal.Parse(lAR[i].Level)) * 100m),
                                Count = 1
                            });
                        }
                    }
                    else
                    {
                        lTEC.Add(new TempEmployeeCompetence()
                        {
                            EmpCode = lAR[i].EmpCode,
                            ScorePerLevel = ((lAR[i].Score / Decimal.Parse(lAR[i].Level)) * 100m),
                            Count = 1
                        });
                    }
                }
                else
                {
                    mEmpCode = lAR[i].EmpCode;
                    IsExists = false;

                    for (int j = 0; j < lTEC.Count; j++)
                    {
                        if (lTEC[j].EmpCode == lAR[i].EmpCode)
                        {
                            lTEC[j].ScorePerLevel += ((lAR[i].Score / Decimal.Parse(lAR[i].Level)) * 100m);
                            lTEC[j].Count += 1;
                            IsExists = true;
                            break;
                        }
                    }

                    if (!IsExists)
                    {
                        lTEC.Add(new TempEmployeeCompetence()
                        {
                            EmpCode = lAR[i].EmpCode,
                            ScorePerLevel = ((lAR[i].Score / Decimal.Parse(lAR[i].Level)) * 100m),
                            Count = 1
                        });
                    }
                }
            }

            if (lTEC.Count > 0)
            {
                foreach (var x in lTEC)
                {
                    lEC.Add(new EmployeeCompetence() 
                    {
                        EmpCode = x.EmpCode,
                        Score = (x.ScorePerLevel / x.Count)
                    });
                }
            }

            lTEC.Clear();
        }

        private void ListEmployeePerformance(ref List<Employee> l, ref List<EmployeePerformance> lEP)
        {
            string mEmpCode = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mEmpCode.Length > 0) mEmpCode += ",";
                mEmpCode += l[i].EmpCode;
            }
            
            var SQL = new StringBuilder();
            if (mIsRptKPIUseGoalsSetting)
            {
                SQL.AppendLine("Select T3.PICCode As EmpCode, T5.GrdStatus as Grade");
                SQL.AppendLine("From TblNewPerformanceReviewHdr T1");
                SQL.AppendLine("Inner Join TblGoalsProcessHdr T2 ON T1.GoalsProcessDocNo = T2.DocNo");
                SQL.AppendLine("Inner Join TblGoalsSettingHdr T3 ON T2.GoalsDocNo = T3.DocNo");
                SQL.AppendLine("Inner Join TblEmployee T4 ON T3.PICCode = T4.EmpCode AND FIND_IN_SET(T3.PICCode, @EmpCode)");
                SQL.AppendLine("Inner Join TblPerformanceGrade T5 ON T1.GrdCode = T5.GrdCode");
            }
            else
            {
                SQL.AppendLine("Select T2.PICCode As EmpCode, T1.Grade ");
                SQL.AppendLine("From TblPerformanceReviewHdr T1 ");
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Max(Concat(A.DocDt, Left(A.DocNo, 4))) As Document, B.PICCode ");
                SQL.AppendLine("    From TblPerformanceReviewHdr A ");
                SQL.AppendLine("    Inner Join TblKPIHdr B On A.KPIDocNo = B.DocNo and B.ActInd = 'Y' and Find_In_Set(B.PICCode, @EmpCode) ");
                SQL.AppendLine("    Group By B.PICCode ");
                SQL.AppendLine(") T2 On Concat(T1.DocDt, Left(T1.DocNo, 4)) = T2.Document And T1.CancelInd = 'N'; ");
            }

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Grade" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lEP.Add(new EmployeePerformance()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Grade = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void RecapEmployeeCompetence(ref List<EmployeeTraining> lET, ref List<EmployeeCompetenceRecap> lECR)
        {
            for (int i = 0; i < lET.Count; i++)
            {
                for (int j = 0; j < lECR.Count; j++)
                {
                    if(lET[i].EmpCode == lECR[j].EmpCode)
                    {
                        lECR[j].EmpTraining = lET[i].CountEmpTraining;
                        lECR[j].PosTraining = lET[i].CountPosTraining;
                    }
                }
            }
        }

        private void RecapEmployeeCompetence2(ref List<EmployeeCompetence> lEC, ref List<EmployeeCompetenceRecap> lECR)
        {
            for (int i = 0; i < lEC.Count; i++)
            {
                for (int j = 0; j < lECR.Count; j++)
                {
                    if (lEC[i].EmpCode == lECR[j].EmpCode)
                    {
                        lECR[j].Score = lEC[i].Score;
                    }
                }
            }
        }

        private void RecapEmployeeCompetence3(ref List<EmployeeCompetenceRecap> lECR)
        {
            for (int i = 0; i < lECR.Count; i++)
            {
                if (lECR[i].EmpTraining > 0 || lECR[i].PosTraining > 0)
                {
                    if(lECR[i].PosTraining > 0 && lECR[i].HardCompetence > 0)
                    {
                        decimal mPercentage = (lECR[i].EmpTraining / lECR[i].PosTraining) * 100m;
                        decimal mPHC = mPercentage * (lECR[i].HardCompetence / 100m);
                        lECR[i].PointHardCompetence = mPHC;
                    }
                }

                if(lECR[i].Score > 0)
                {
                    lECR[i].PointSoftCompetence = lECR[i].Score * (lECR[i].SoftCompetence / 100m);
                }

                lECR[i].TotalCompetence = lECR[i].PointHardCompetence + lECR[i].PointSoftCompetence;

                if(lECR[i].TotalCompetence > 0 && lECR[i].TotalCompetence <= 33.3m)
                {
                    lECR[i].ResultCompetence = "Low";
                }
                else if (lECR[i].TotalCompetence > 33.3m && lECR[i].TotalCompetence <= 66.67m)
                {
                    lECR[i].ResultCompetence = "Medium";
                }
                else if (lECR[i].TotalCompetence > 66.67m)
                {
                    lECR[i].ResultCompetence = "High";
                }
            }
        }

        private void ShowDataInGrd(ref List<Employee> l)
        {
            Grd1.BeginUpdate();
            Grd1.Rows.Count = l.Count();
            int mRow = 0;

            for (int no = 1; no <= l.Count; no++)
            {
                Grd1.Cells[no - 1, 0].Value = no;
            }

            #region Ceplok Data

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "HIGH" && l[i].PerformanceLevel.ToUpper() == "HIGH")
                {
                    Grd1.Cells[mRow, 1].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 4].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "MEDIUM" && l[i].PerformanceLevel.ToUpper() == "HIGH")
                {
                    Grd1.Cells[mRow, 5].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 8].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "LOW" && l[i].PerformanceLevel.ToUpper() == "HIGH")
                {
                    Grd1.Cells[mRow, 9].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 12].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "HIGH" && l[i].PerformanceLevel.ToUpper() == "MEDIUM")
                {
                    Grd1.Cells[mRow, 13].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 16].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "MEDIUM" && l[i].PerformanceLevel.ToUpper() == "MEDIUM")
                {
                    Grd1.Cells[mRow, 17].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 20].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "LOW" && l[i].PerformanceLevel.ToUpper() == "MEDIUM")
                {
                    Grd1.Cells[mRow, 21].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 24].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "HIGH" && l[i].PerformanceLevel.ToUpper() == "LOW")
                {
                    Grd1.Cells[mRow, 25].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 28].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "MEDIUM" && l[i].PerformanceLevel.ToUpper() == "LOW")
                {
                    Grd1.Cells[mRow, 29].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 32].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            mRow = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].CompetenceLevel.ToUpper() == "LOW" && l[i].PerformanceLevel.ToUpper() == "LOW")
                {
                    Grd1.Cells[mRow, 33].Value = l[i].EmpCode;
                    Grd1.Cells[mRow, 36].Value = l[i].EmpName;
                    mRow += 1;
                }
            }

            #endregion

            for (int x = l.Count - 1; x >= 0; x--)
            {
                if (Sm.GetGrdStr(Grd1, x, 1).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 5).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 9).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 13).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 17).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 21).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 25).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 29).Length <= 0 &&
                    Sm.GetGrdStr(Grd1, x, 33).Length <= 0)
                {
                    Grd1.Rows.Count -= 1;
                }
                else
                    break;
            }

            Grd1.EndUpdate();
        }


        #region Compute matrix

        public void ComputeHL()
        {
            HL = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 12).Length > 0)
                {
                    HL = HL + 1;
                }
            }
        }

        public void ComputeHM()
        {
            HM = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 8).Length > 0)
                {
                    HM = HM + 1;
                }
            }
        }

        public void ComputeHH()
        {
            HH = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 4).Length > 0)
                {
                    HH = HH + 1;
                }
            }
        }

        public void ComputeML()
        {
            ML = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 24).Length > 0)
                {
                    ML = ML + 1;
                }
            }
        }

        public void ComputeMM()
        {
            MM = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 20).Length > 0)
                {
                    MM = MM + 1;
                }
            }
        }

        public void ComputeMH()
        {
            MH = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 16).Length > 0)
                {
                    MH = MH + 1;
                }
            }
        }

        public void ComputeLL()
        {
            LL = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 36).Length > 0)
                {
                    LL = LL + 1;
                }
            }
        }

        public void ComputeLM()
        {
            LM = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 32).Length > 0)
                {
                    LM = LM + 1;
                }
            }
        }

        public void ComputeLH()
        {
            LH = 0;
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 28).Length > 0)
                {
                    LH = LH + 1;
                }
            }
        }

        #endregion

        public void ParPrint(string ProfilePicture, string EmpCode)
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<EmpHdr>();
            var l1 = new List<EmpGeneral>();
            var l2 = new List<EmpPersonal>();
            var l3 = new List<EmpWorkExperience>();
            var l4 = new List<EmpEducation>();
            var l5 = new List<EmpCompetence>();
            var l6 = new List<EmpSS>();
            var l7 = new List<EmpPersonalRef>();
            var l8 = new List<EmpFamilySS>();
            var l9 = new List<EmpWL>();
            var l10 = new List<EmpPPS>();
            var l11 = new List<EmpCompetenceOther>();
            var l12 = new List<EmpPotency>();

            string[] TableName = { "EmpHdr", "EmpGeneral", "EmpPersonal", "EmpWorkExperience", "EmpEducation", "EmpCompetence", "EmpSS", "EmpPersonalRef", "EmpFamilySS", "EmpWL", "EmpPPS", "EmpCompetenceOther", "EmpPotency" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'CompanyEmail',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
            SQL.AppendLine("A.EmpCode, A.EmpName, B.DeptName, C.PosName, @ProfilePicture As ProfilePicture ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode = C.PosCode ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@ProfilePicture", ProfilePicture);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyEmail",
                         
                         //6-10
                         "CompanyFax",
                         "Shipper1",
                         "Shipper2",
                         "Shipper3",
                         "Shipper4",
                         
                         //11-15
                         "CompLocation2",
                         "EmpCode",
                         "EmpName",
                         "DeptName",
                         "PosName",

                         //16
                         "ProfilePicture",
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyEmail = Sm.DrStr(dr, c[5]),

                            CompanyFax = Sm.DrStr(dr, c[6]),
                            Shipper1 = Sm.DrStr(dr, c[7]),
                            Shipper2 = Sm.DrStr(dr, c[8]),
                            Shipper3 = Sm.DrStr(dr, c[9]),
                            Shipper4 = Sm.DrStr(dr, c[10]),

                            CompLocation2 = Sm.DrStr(dr, c[11]),
                            EmpCode = Sm.DrStr(dr, c[12]),
                            EmpName = Sm.DrStr(dr, c[13]),
                            DeptName = Sm.DrStr(dr, c[14]),
                            PosName = Sm.DrStr(dr, c[15]),

                            ProfilePicture = Sm.DrStr(dr, c[16]),

                            Printby = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                else
                {
                    l.Add(new EmpHdr()
                    {
                        CompanyLogo = "",

                        CompanyName = "",
                        CompanyAddress = "",
                        CompanyAddressCity = "",
                        CompanyPhone = "",
                        CompanyEmail = "",

                        CompanyFax = "",
                        Shipper1 = "",
                        Shipper2 = "",
                        Shipper3 = "",
                        Shipper4 = "",

                        CompLocation2 = "",
                        EmpCode = "",
                        EmpName = "",
                        DeptName = "",
                        PosName = "",

                        ProfilePicture = "",

                        Printby = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                    });
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region General
            var cml1 = new MySqlCommand();

            var SQL1 = new StringBuilder();
            using (var cn1 = new MySqlConnection(Gv.ConnectionString))
            {
                cn1.Open();
                cml1.Connection = cn1;

                SQL1.AppendLine("Select If(IfNull(A.BirthPlace, '')='', '', Concat(A.BirthPlace, ',')) As BirthPlace, IfNull(DATE_FORMAT(A.BirthDt, '%d %M %Y'), '') As BirthDt, A.Address, B.CityName, A.SubDistrict, A.Village, A.RTRW, A.PostalCode, ");
                SQL1.AppendLine("C.OptDesc As Religion, F.OptDesc As Gender, IfNull(DATE_FORMAT(A.JoinDt, '%d %M %Y'), '') As JoinDt, IfNull(DATE_FORMAT(A.ResignDt, '%d %M %Y'), '') As ResignDt, E.OptDesc As EmploymentStatus, ");
                SQL1.AppendLine("D.OptDesc As MaritalStatus, IfNull(DATE_FORMAT(A.WeddingDt, '%d %M %Y'), '') As MaritalDt, Date_Format(A.LeaveStartDt,'%d %M %Y')As LeaveStartDt ");
                SQL1.AppendLine("From TblEmployee A ");
                SQL1.AppendLine("Left Join TblCity B On A.CityCode = B.CityCode ");
                SQL1.AppendLine("Left Join TblOption C On A.Religion = C.OptCode And C.OptCat = 'Religion' ");
                SQL1.AppendLine("Left Join TblOption D On A.MaritalStatus = D.OptCode And D.OptCat = 'MaritalStatus' ");
                SQL1.AppendLine("Left Join TblOption E On A.EmploymentStatus = E.OptCode And E.OptCat = 'EmploymentStatus' ");
                SQL1.AppendLine("Left Join TblOption F On A.Gender = F.OptCode And F.OptCat = 'Gender' ");
                SQL1.AppendLine("Where A.EmpCode = @EmpCode; ");

                cml1.CommandText = SQL1.ToString();

                Sm.CmParam<String>(ref cml1, "@EmpCode", EmpCode);

                var dr1 = cml1.ExecuteReader();
                var c1 = Sm.GetOrdinal(dr1, new string[] 
                    {
                     //0
                     "BirthPlace" ,

                     //1-5
                     "BirthDt" ,
                     "Address",
                     "CityName",
                     "SubDistrict",
                     "Village",

                     //6-10
                     "RTRW",
                     "PostalCode",
                     "Religion",
                     "Gender",
                     "JoinDt",

                     //11-15
                     "ResignDt",
                     "EmploymentStatus", 
                     "MaritalStatus", 
                     "MaritalDt",
                     "LeaveStartDt"

                    });
                if (dr1.HasRows)
                {
                    int NomorBaris = 0;
                    while (dr1.Read())
                    {
                        NomorBaris = NomorBaris + 1;
                        l1.Add(new EmpGeneral()
                        {
                            BirthPlace = Sm.DrStr(dr1, c1[0]),
                            BirthDt = Sm.DrStr(dr1, c1[1]),
                            Address = Sm.DrStr(dr1, c1[2]),
                            CityName = Sm.DrStr(dr1, c1[3]),
                            SubDistrict = Sm.DrStr(dr1, c1[4]),
                            Village = Sm.DrStr(dr1, c1[5]),
                            RtRw = Sm.DrStr(dr1, c1[6]),
                            PostalCode = Sm.DrStr(dr1, c1[7]),
                            Religion = Sm.DrStr(dr1, c1[8]),
                            Gender = Sm.DrStr(dr1, c1[9]),
                            JoinDt = Sm.DrStr(dr1, c1[10]),
                            ResignDt = Sm.DrStr(dr1, c1[11]),
                            EmploymentStatus = Sm.DrStr(dr1, c1[12]),
                            MaritalStatus = Sm.DrStr(dr1, c1[13]),
                            MaritalDt = Sm.DrStr(dr1, c1[14]),
                            LeaveStartDt = Sm.DrStr(dr1, c1[15]),
                        });
                    }
                }
                dr1.Close();
            }
            myLists.Add(l1);
            #endregion

            #region Personal

            var cml2 = new MySqlCommand();

            var SQL2 = new StringBuilder();
            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cml2.Connection = cn2;

                SQL2.AppendLine("Select A.FamilyName, B.OptDesc As Status, IfNull(DATE_FORMAT(A.BirthDt, '%d %M %Y'), '') As BirthDt ");
                SQL2.AppendLine("From TblEmployeeFamily A ");
                SQL2.AppendLine("Left Join TblOption B On A.Status = B.OptCode And B.OptCat = 'FamilyStatus' ");
                SQL2.AppendLine("Where A.EmpCode = @EmpCode Order By A.DNo; ");

                cml2.CommandText = SQL2.ToString();

                Sm.CmParam<String>(ref cml2, "@EmpCode", EmpCode);

                var dr2 = cml2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                     //0
                     "FamilyName",

                     //1-2
                     "Status", "BirthDt"
                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new EmpPersonal()
                        {
                            FamilyName = Sm.DrStr(dr2, c2[0]),
                            Status = Sm.DrStr(dr2, c2[1]),
                            BirthDt = Sm.DrStr(dr2, c2[2])

                        });
                    }
                }
                else
                {
                    l2.Add(new EmpPersonal()
                    {
                        FamilyName = "",
                        Status = "",
                        BirthDt = ""

                    });
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            #region Work Experience

            var cml3 = new MySqlCommand();

            var SQL3 = new StringBuilder();
            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cml3.Connection = cn3;

                SQL3.AppendLine("Select IfNull(Company, '') As Company, IfNull(Position, '') As Position, IfNull(Period, '') As Period ");
                SQL3.AppendLine("From TblEmployeeWorkExp ");
                SQL3.AppendLine("Where EmpCode = @EmpCode Order By DNo; ");

                cml3.CommandText = SQL3.ToString();

                Sm.CmParam<String>(ref cml3, "@EmpCode", EmpCode);

                var dr3 = cml3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                    {
                     //0
                     "Company",

                     //1-2
                     "Position", "Period"
                    });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new EmpWorkExperience()
                        {
                            Company = Sm.DrStr(dr3, c3[0]),
                            Position = Sm.DrStr(dr3, c3[1]),
                            Period = Sm.DrStr(dr3, c3[2])

                        });
                    }
                }
                else
                {
                    l3.Add(new EmpWorkExperience()
                    {
                        Company = "",
                        Position = "",
                        Period = ""

                    });
                }
                dr3.Close();
            }
            myLists.Add(l3);

            #endregion

            #region Education

            var cml4 = new MySqlCommand();

            var SQL4 = new StringBuilder();
            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cml4.Connection = cn4;

                SQL4.AppendLine("Select IfNull(A.School, '') As School, IfNull(B.OptDesc, '') As Level, IfNull(A.Major, '') As Major ");
                SQL4.AppendLine("From TblEmployeeEducation A ");
                SQL4.AppendLine("Left Join TblOption B On A.Level = B.OptCode And B.OptCat = 'EmployeeEducationLevel' ");
                SQL4.AppendLine("Where A.EmpCode = @EmpCode Order By A.DNo; ");

                cml4.CommandText = SQL4.ToString();

                Sm.CmParam<String>(ref cml4, "@EmpCode", EmpCode);

                var dr4 = cml4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                    {
                     //0
                     "School",

                     //1-2
                     "Level", "Major"
                    });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l4.Add(new EmpEducation()
                        {
                            School = Sm.DrStr(dr4, c4[0]),
                            Level = Sm.DrStr(dr4, c4[1]),
                            Major = Sm.DrStr(dr4, c4[2])

                        });
                    }
                }
                else
                {
                    l4.Add(new EmpEducation()
                    {
                        School = "",
                        Level = "",
                        Major = ""

                    });
                }
                dr4.Close();
            }
            myLists.Add(l4);

            #endregion

            #region Competence By Employee's Position

            var cml5 = new MySqlCommand();

            var SQL5 = new StringBuilder();
            using (var cn5 = new MySqlConnection(Gv.ConnectionString))
            {
                cn5.Open();
                cml5.Connection = cn5;

                SQL5.AppendLine("Select A.CompetenceCode, C.CompetenceName, E.PosName, A.Description, D.MinScore, A.Score ");
                SQL5.AppendLine("From TblEmployeeCompetence A ");
                SQL5.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL5.AppendLine("Inner Join TblCompetence C On A.CompetenceCode = C.CompetenceCode And C.CompetenceLevelType='2' ");
                SQL5.AppendLine("Inner Join TblOrganizationalStructureDtl4 D On A.CompetenceCode = D.CompetenceCode And B.PosCode = D.PosCode ");
                SQL5.AppendLine("Inner Join TblPosition E On D.PosCode = E.PosCode ");
                SQL5.AppendLine("Where A.EmpCode = @EmpCode Order By A.DNo; ");

                cml5.CommandText = SQL5.ToString();

                Sm.CmParam<String>(ref cml5, "@EmpCode", EmpCode);

                var dr5 = cml5.ExecuteReader();
                var c5 = Sm.GetOrdinal(dr5, new string[] 
                    {
                     //0
                     "CompetenceCode",

                     //1-5
                     "CompetenceName", "PosName", "Description", "MinScore", "Score"
                    });
                if (dr5.HasRows)
                {
                    while (dr5.Read())
                    {
                        l5.Add(new EmpCompetence()
                        {
                            CompetenceCode = Sm.DrStr(dr5, c5[0]),
                            CompetenceName = Sm.DrStr(dr5, c5[1]),
                            Position = Sm.DrStr(dr5, c5[2]),
                            Description = Sm.DrStr(dr5, c5[3]),
                            MinScore = Sm.DrDec(dr5, c5[4]),
                            Score = Sm.DrDec(dr5, c5[5])

                        });
                    }
                }
                else
                {
                    l5.Add(new EmpCompetence()
                    {
                        CompetenceCode = "",
                        CompetenceName = "Not Set",
                        Position = "",
                        Description = "",
                        MinScore = 1,
                        Score = 0m

                    });
                }
                dr5.Close();
            }
            myLists.Add(l5);

            #endregion

            #region Social Security

            var cml6 = new MySqlCommand();

            var SQL6 = new StringBuilder();
            using (var cn6 = new MySqlConnection(Gv.ConnectionString))
            {
                cn6.Open();
                cml6.Connection = cn6;

                SQL6.AppendLine("Select B.SSName, A.CardNo, Concat(DATE_FORMAT(A.StartDt, '%d %M %Y'), If(IfNull(A.EndDt, '')='', '', ' - '), IfNull(DATE_FORMAT(A.EndDt, '%d %M %Y'), '')) As Period ");
                SQL6.AppendLine("From TblEmployeeSS A ");
                SQL6.AppendLine("Left Join TblSS B On A.SSCode=B.SSCode ");
                SQL6.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL6.AppendLine("Order By A.DNo;");

                cml6.CommandText = SQL6.ToString();

                Sm.CmParam<String>(ref cml6, "@EmpCode", EmpCode);

                var dr6 = cml6.ExecuteReader();
                var c6 = Sm.GetOrdinal(dr6, new string[] 
                    {
                     //0
                     "SSName",

                     //1-2
                     "CardNo", "Period"
                    });
                if (dr6.HasRows)
                {
                    while (dr6.Read())
                    {
                        l6.Add(new EmpSS()
                        {
                            SSName = Sm.DrStr(dr6, c6[0]),
                            SSCard = Sm.DrStr(dr6, c6[1]),
                            Period = Sm.DrStr(dr6, c6[2])
                        });
                    }
                }
                else
                {
                    l6.Add(new EmpSS()
                    {
                        SSName = "",
                        SSCard = "",
                        Period = ""
                    });
                }
                dr6.Close();
            }
            myLists.Add(l6);

            #endregion

            #region Personal References

            var cml7 = new MySqlCommand();

            var SQL7 = new StringBuilder();
            using (var cn7 = new MySqlConnection(Gv.ConnectionString))
            {
                cn7.Open();
                cml7.Connection = cn7;

                SQL7.AppendLine("Select IfNull(Name, '') As Name, IfNull(Phone, '') As Phone, IfNull(Address, '') As Address ");
                SQL7.AppendLine("From TblEmployeePersonalReference ");
                SQL7.AppendLine("Where EmpCode=@EmpCode ");
                SQL7.AppendLine("Order By DNo;");

                cml7.CommandText = SQL7.ToString();

                Sm.CmParam<String>(ref cml7, "@EmpCode", EmpCode);

                var dr7 = cml7.ExecuteReader();
                var c7 = Sm.GetOrdinal(dr7, new string[] 
                    {
                     //0
                     "Name",

                     //1-2
                     "Address", "Phone"
                    });
                if (dr7.HasRows)
                {
                    while (dr7.Read())
                    {
                        l7.Add(new EmpPersonalRef()
                        {
                            PRName = Sm.DrStr(dr7, c7[0]),
                            PRAddress = Sm.DrStr(dr7, c7[1]),
                            PRPhone = Sm.DrStr(dr7, c7[2])
                        });
                    }
                }
                else
                {
                    l7.Add(new EmpPersonalRef()
                    {
                        PRName = "",
                        PRAddress = "",
                        PRPhone = ""
                    });
                }
                dr7.Close();
            }
            myLists.Add(l7);

            #endregion

            #region Family Social Security

            var cml8 = new MySqlCommand();

            var SQL8 = new StringBuilder();
            using (var cn8 = new MySqlConnection(Gv.ConnectionString))
            {
                cn8.Open();
                cml8.Connection = cn8;

                SQL8.AppendLine("Select IfNull(A.FamilyName, '') As FamilyName, IfNull(B.OptDesc, '') as StatusDesc, ");
                SQL8.AppendLine("IfNull(D.SSName, '') As SSName, IfNull(A.CardNo, '') As CardNo, Concat(DATE_FORMAT(A.StartDt, '%d %M %Y'), If(IfNull(A.EndDt, '')='', '', ' - '), IfNull(DATE_FORMAT(A.EndDt, '%d %M %Y'), '')) As Period ");
                SQL8.AppendLine("From TblEmployeeFamilySS A ");
                SQL8.AppendLine("Left Join TblOption B On A.Status=B.OptCode and B.OptCat='FamilyStatus' ");
                SQL8.AppendLine("Left Join TblSS D On A.SSCode=D.SSCode ");
                SQL8.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL8.AppendLine("Order By A.DNo;");

                cml8.CommandText = SQL8.ToString();

                Sm.CmParam<String>(ref cml8, "@EmpCode", EmpCode);

                var dr8 = cml8.ExecuteReader();
                var c8 = Sm.GetOrdinal(dr8, new string[] 
                    {
                     //0
                     "FamilyName",

                     //1-4
                     "StatusDesc", "SSName", "CardNo", "Period"
                    });
                if (dr8.HasRows)
                {
                    while (dr8.Read())
                    {
                        l8.Add(new EmpFamilySS()
                        {
                            FSSFamilyName = Sm.DrStr(dr8, c8[0]),
                            FSSStatus = Sm.DrStr(dr8, c8[1]),
                            FSSName = Sm.DrStr(dr8, c8[2]),
                            FSSCard = Sm.DrStr(dr8, c8[3]),
                            FSSPeriod = Sm.DrStr(dr8, c8[4])
                        });
                    }
                }
                else
                {
                    l8.Add(new EmpFamilySS()
                    {
                        FSSFamilyName = "",
                        FSSStatus = "",
                        FSSName = "",
                        FSSCard = "",
                        FSSPeriod = ""
                    });
                }
                dr8.Close();
            }
            myLists.Add(l8);

            #endregion

            #region Warning Letter / Infringement

            var cml9 = new MySqlCommand();

            var SQL9 = new StringBuilder();
            using (var cn9 = new MySqlConnection(Gv.ConnectionString))
            {
                cn9.Open();
                cml9.Connection = cn9;

                SQL9.AppendLine("Select C.WLName As TypeDesc, Concat(DATE_FORMAT(A.StartDt, '%d %M %Y'), ' - ', DATE_FORMAT(A.EndDt, '%d %M %Y')) As Period ");
                SQL9.AppendLine("From TblEmpWL A ");
                SQL9.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
                SQL9.AppendLine("Inner Join TblWarningLetter C On A.WLCode = C.WLCode ");
                SQL9.AppendLine("Where A.EmpCode=@EmpCode And A.CancelInd = 'N';");

                cml9.CommandText = SQL9.ToString();

                Sm.CmParam<String>(ref cml9, "@EmpCode", EmpCode);

                var dr9 = cml9.ExecuteReader();
                var c9 = Sm.GetOrdinal(dr9, new string[] 
                    {
                     //0
                     "TypeDesc",

                     //1
                     "Period"
                    });
                if (dr9.HasRows)
                {
                    while (dr9.Read())
                    {
                        l9.Add(new EmpWL()
                        {
                            Type = Sm.DrStr(dr9, c9[0]),
                            Period = Sm.DrStr(dr9, c9[1])
                        });
                    }
                }
                else
                {
                    l9.Add(new EmpWL()
                    {
                        Type = "",
                        Period = ""
                    });
                }
                dr9.Close();
            }
            myLists.Add(l9);

            #endregion

            #region Position's History

            var cml10 = new MySqlCommand();

            var SQL10 = new StringBuilder();
            using (var cn10 = new MySqlConnection(Gv.ConnectionString))
            {
                cn10.Open();
                cml10.Connection = cn10;

                SQL10.AppendLine("Select E.OptDesc As Category, ");
                SQL10.AppendLine("Concat(IfNull(C1.DeptName, ''), If(IfNull(A.DeptCodeOld, '')='', '', ' - '), IfNull(D1.PosName, '')) As TransfFrom, ");
                SQL10.AppendLine("Concat(IfNull(C2.DeptName, ''), If(IfNull(A.DeptCodeNew, '')='', '', ' - '), IfNull(D2.POsName, '')) As TransfTo, ");
                SQL10.AppendLine("DATE_FORMAT(IfNull(A.StartDt, ''), '%d %M %Y') As StartDt ");
                SQL10.AppendLine("From TblPPS A ");
                SQL10.AppendLine("Inner Join TblEmployee B ON A.EmpCode = B.EmpCode ");
                SQL10.AppendLine("Left Join TblDepartment C1 On A.DeptCodeOld = C1.DeptCode ");
                SQL10.AppendLine("Left Join TblPosition D1 On A.PosCodeOld = D1.PosCode ");
                SQL10.AppendLine("Left Join TblDepartment C2 On A.DeptCodeNew = C2.DeptCode ");
                SQL10.AppendLine("Left Join TblPosition D2 On A.PosCodeNew = D2.PosCode ");
                SQL10.AppendLine("Inner Join TblOption E On A.JobTransfer = E.OptCode And E.OptCat = 'EmpJobTransfer' ");
                SQL10.AppendLine("Where ((A.DeptCodeOld <> A.DeptCodeNew) Or (A.PosCodeOld <> A.PosCodeNew)) ");
                SQL10.AppendLine("And A.EmpCode = @EmpCode Order By A.DocNo; ");

                cml10.CommandText = SQL10.ToString();

                Sm.CmParam<String>(ref cml10, "@EmpCode", EmpCode);

                var dr10 = cml10.ExecuteReader();
                var c10 = Sm.GetOrdinal(dr10, new string[] 
                    {
                     //0
                     "Category",

                     //1-3
                     "TransfFrom", "TransfTo", "StartDt"
                    });
                if (dr10.HasRows)
                {
                    while (dr10.Read())
                    {
                        l10.Add(new EmpPPS()
                        {
                            Category = Sm.DrStr(dr10, c10[0]),
                            From = Sm.DrStr(dr10, c10[1]),
                            To = Sm.DrStr(dr10, c10[2]),
                            StartDt = Sm.DrStr(dr10, c10[3])
                        });
                    }
                }
                else
                {
                    l10.Add(new EmpPPS()
                    {
                        Category = "",
                        From = "",
                        To = "",
                        StartDt = ""
                    });
                }
                dr10.Close();
            }
            myLists.Add(l10);

            #endregion

            #region Competence Besides Employee's Position

            var cml11 = new MySqlCommand();

            var SQL11 = new StringBuilder();
            using (var cn11 = new MySqlConnection(Gv.ConnectionString))
            {
                cn11.Open();
                cml11.Connection = cn11;

                SQL11.AppendLine("Select A.CompetenceCode, C.CompetenceName, A.Description, A.Score ");
                SQL11.AppendLine("From TblEmployeeCompetence A ");
                SQL11.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL11.AppendLine("Inner Join TblCompetence C On A.CompetenceCode = C.CompetenceCode And C.CompetenceLevelType='2' ");
                SQL11.AppendLine("Where A.EmpCode = @EmpCode ");
                SQL11.AppendLine("And A.CompetenceCode Not In ");
                SQL11.AppendLine("( ");
                SQL11.AppendLine("  Select T1.CompetenceCode ");
                SQL11.AppendLine("  From TblEmployeeCompetence T1 ");
                SQL11.AppendLine("  Inner Join TblEmployee T2 On T1.EmpCode = T2.EmpCode ");
                SQL11.AppendLine("  Inner Join TblOrganizationalStructureDtl4 T3 On T1.CompetenceCode = T3.CompetenceCode And T2.PosCode = T3.PosCode ");
                SQL11.AppendLine("  Where T1.EmpCode = @EmpCode ");
                SQL11.AppendLine(") ");
                SQL11.AppendLine("Order By A.DNo; ");

                cml11.CommandText = SQL11.ToString();

                Sm.CmParam<String>(ref cml11, "@EmpCode", EmpCode);

                var dr11 = cml11.ExecuteReader();
                var c11 = Sm.GetOrdinal(dr11, new string[] 
                    {
                     //0
                     "CompetenceCode",

                     //1-3
                     "CompetenceName", "Description", "Score"
                    });
                if (dr11.HasRows)
                {
                    while (dr11.Read())
                    {
                        l11.Add(new EmpCompetenceOther()
                        {
                            OCompetenceCode = Sm.DrStr(dr11, c11[0]),
                            OCompetenceName = Sm.DrStr(dr11, c11[1]),
                            ODescription = Sm.DrStr(dr11, c11[2]),
                            OScore = Sm.DrDec(dr11, c11[3])
                        });
                    }
                }
                else
                {
                    l11.Add(new EmpCompetenceOther()
                    {
                        OCompetenceCode = "",
                        OCompetenceName = "Not Set",
                        ODescription = "",
                        OMinScore = 1,
                        OScore = 0m
                    });
                }
                dr11.Close();
            }
            myLists.Add(l11);

            #endregion

            #region Potency By Employee's Position

            var cml12 = new MySqlCommand();

            var SQL12 = new StringBuilder();
            using (var cn12 = new MySqlConnection(Gv.ConnectionString))
            {
                cn12.Open();
                cml12.Connection = cn12;

                SQL12.AppendLine("Select A.CompetenceCode, C.CompetenceName, A.Description, A.Score ");
                SQL12.AppendLine("From TblEmployeeCompetence A ");
                SQL12.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL12.AppendLine("Inner Join TblCompetence C On A.CompetenceCode = C.CompetenceCode And C.CompetenceLevelType='1' ");
                SQL12.AppendLine("Where A.EmpCode=@EmpCode ");
                SQL12.AppendLine("Order By A.DNo; ");

                cml12.CommandText = SQL12.ToString();

                Sm.CmParam<String>(ref cml12, "@EmpCode", EmpCode);

                var dr12 = cml12.ExecuteReader();
                var c12 = Sm.GetOrdinal(dr12, new string[] 
                    {
                     //0
                     "CompetenceCode",

                     //1-3
                     "CompetenceName", "Description", "Score"
                    });
                if (dr12.HasRows)
                {
                    while (dr12.Read())
                    {
                        l12.Add(new EmpPotency()
                        {
                            PotencyCode = Sm.DrStr(dr12, c12[0]),
                            PotencyName = Sm.DrStr(dr12, c12[1]),
                            Description = Sm.DrStr(dr12, c12[2]),
                            Score = Sm.DrDec(dr12, c12[3])
                        });
                    }
                }
                else
                {
                    l12.Add(new EmpPotency()
                    {
                        PotencyCode = "",
                        PotencyName = "Not Set",
                        Description = "",
                        MinScore = 1,
                        Score = 0m
                    });
                }
                dr12.Close();
            }
            myLists.Add(l12);

            #endregion

            Sm.PrintReport("Employee", myLists, TableName, false);
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (Sm.IsGrdColSelected(new int[] { 3, 7, 11, 15, 19, 23, 27, 31, 35 }, e.ColIndex))
            {
                for (int i = 3; i < 36; i += 4)
                {
                    if (e.ColIndex == i && Sm.GetGrdStr(Grd1, e.RowIndex, (i - 2)).Length != 0)
                    {
                        ParPrint("", Sm.GetGrdStr(Grd1, e.RowIndex, (i - 2)));
                    }
                }
            }

            if (Sm.IsGrdColSelected(new int[] { 2, 6, 10, 14, 18, 22, 26, 30, 34 }, e.ColIndex))
            {
                for (int i = 2; i < 35; i += 4)
                {
                    if (e.ColIndex == i && Sm.GetGrdStr(Grd1, e.RowIndex, (i - 1)).Length != 0)
                    {
                        var f = new FrmEmployee("XXX");
                        f.Tag = "XXX";
                        f.Text = Sm.GetMenuDesc("FrmEmployee");
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, (i - 1));
                        f.ShowDialog();
                    }
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 3, 7, 11, 15, 19, 23, 27, 31, 35 }, e.ColIndex))
            {
                for (int i = 3; i < 36; i += 4)
                {
                    if (e.ColIndex == i && Sm.GetGrdStr(Grd1, e.RowIndex, (i - 2)).Length != 0)
                    {
                        ParPrint("", Sm.GetGrdStr(Grd1, e.RowIndex, (i - 2)));
                    }
                }
            }

            if (Sm.IsGrdColSelected(new int[] { 2, 6, 10, 14, 18, 22, 26, 30, 34 }, e.ColIndex))
            {
                for (int i = 2; i < 35; i += 4)
                {
                    if (e.ColIndex == i && Sm.GetGrdStr(Grd1, e.RowIndex, (i - 1)).Length != 0)
                    {
                        var f = new FrmEmployee("XXX");
                        f.Tag = "XXX";
                        f.Text = Sm.GetMenuDesc("FrmEmployee");
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, (i - 1));
                        f.ShowDialog();
                    }
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void BtnMatrix_Click(object sender, EventArgs e)
        {
            ComputeHH();
            ComputeHM();
            ComputeHL();
            ComputeMH();
            ComputeMM();
            ComputeML();
            ComputeLH();
            ComputeLM();
            ComputeLL();

            var f = new FrmEmpMatrix2("", HL, HM, HH, ML, MM, MH, LL, LM, LH);
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.ShowDialog();
        }


        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStatus, new Sm.RefreshLue1(Sl.SetLueEmpStatus));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

        #endregion

        #region Class

        class Employee
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string PosCode { get; set; }
            public string CompetenceLevel { get; set; }
            public string PerformanceLevel { get; set; }
        }

        class EmployeeTraining
        {
            public string EmpCode { get; set; }
            public string PosCode { get; set; }
            public decimal CountEmpTraining { get; set; }
            public decimal CountPosTraining { get; set; }
        }

        class EmployeeCompetence
        {
            public string EmpCode { get; set; }
            public decimal Score { get; set; }
        }

        class TempEmployeeCompetence
        {
            public string EmpCode { get; set; }
            public decimal ScorePerLevel { get; set; }
            public decimal Count { get; set; }
        }

        class EmployeePerformance
        {
            public string EmpCode { get; set; }
            public string Grade { get; set; }
        }

        class EmployeeCompetenceRecap
        {
            public string EmpCode { get; set; }
            public decimal EmpTraining { get; set; }
            public decimal PosTraining { get; set; }
            public decimal HardCompetence { get; set; }
            public decimal Score { get; set; }
            public decimal SoftCompetence { get; set; }
            public decimal PointHardCompetence { get; set; }
            public decimal PointSoftCompetence { get; set; }
            public decimal TotalCompetence { get; set; }
            public string ResultCompetence { get; set; }
        }

        class AssesmentProcess
        {
            public string EmpCode { get; set; }
            public string AsmCode { get; set; }
            public string DocNo { get; set; }
            public decimal Score1 { get; set; }
            public decimal Score2 { get; set; }
            public decimal Score3 { get; set; }
            public decimal Score4 { get; set; }
            public decimal Score5 { get; set; }
            public decimal Score6 { get; set; }
            public decimal Score7 { get; set; }
            public decimal Score8 { get; set; }
            public decimal Score9 { get; set; }
            public decimal Score10 { get; set; }
            public decimal Score11 { get; set; }
            public decimal Score12 { get; set; }
            public decimal Score13 { get; set; }
            public decimal Score14 { get; set; }
            public decimal Score15 { get; set; }
            public decimal Score16 { get; set; }
            public decimal Score17 { get; set; }
            public decimal Score18 { get; set; }
            public decimal Score19 { get; set; }
            public decimal Score20 { get; set; }
        }

        class Assesment
        {
            public string AsmCode { get; set; }
            public string CompetenceCode1 { get; set; }
            public string Level1 { get; set; }
            public string CompetenceCode2 { get; set; }
            public string Level2 { get; set; }
            public string CompetenceCode3 { get; set; }
            public string Level3 { get; set; }
            public string CompetenceCode4 { get; set; }
            public string Level4 { get; set; }
            public string CompetenceCode5 { get; set; }
            public string Level5 { get; set; }
            public string CompetenceCode6 { get; set; }
            public string Level6 { get; set; }
            public string CompetenceCode7 { get; set; }
            public string Level7 { get; set; }
            public string CompetenceCode8 { get; set; }
            public string Level8 { get; set; }
            public string CompetenceCode9 { get; set; }
            public string Level9 { get; set; }
            public string CompetenceCode10 { get; set; }
            public string Level10 { get; set; }
            public string CompetenceCode11 { get; set; }
            public string Level11 { get; set; }
            public string CompetenceCode12 { get; set; }
            public string Level12 { get; set; }
            public string CompetenceCode13 { get; set; }
            public string Level13 { get; set; }
            public string CompetenceCode14 { get; set; }
            public string Level14 { get; set; }
            public string CompetenceCode15 { get; set; }
            public string Level15 { get; set; }
            public string CompetenceCode16 { get; set; }
            public string Level16 { get; set; }
            public string CompetenceCode17 { get; set; }
            public string Level17 { get; set; }
            public string CompetenceCode18 { get; set; }
            public string Level18 { get; set; }
            public string CompetenceCode19 { get; set; }
            public string Level19 { get; set; }
            public string CompetenceCode20 { get; set; }
            public string Level20 { get; set; }
        }

        class OrgStructCompetence
        {
            public string PosCode { get; set; }
            public string CompetenceCode { get; set; }
        }

        class ListEmpCompetence
        {
            public string EmpCode { get; set; }
            public string CompetenceCode { get; set; }
        }

        class AssesmentRecap
        {
            public string EmpCode { get; set; }
            public string CompetenceCode { get; set; }
            public string Level { get; set; }
            public decimal Score { get; set; }
        }

        #endregion

        
        //#region Report Class

        //class EmpHdr
        //{
        //    public string CompanyLogo { set; get; }
        //    public string CompanyName { get; set; }
        //    public string CompanyAddressCity { get; set; }
        //    public string CompanyAddress { get; set; }
        //    public string CompanyPhone { get; set; }
        //    public string CompanyEmail { get; set; }
        //    public string CompanyFax { get; set; }
        //    public string CompLocation2 { get; set; }
        //    public string Shipper1 { get; set; }
        //    public string Shipper2 { get; set; }
        //    public string Shipper3 { get; set; }
        //    public string Shipper4 { get; set; }
        //    public string EmpCode { get; set; }
        //    public string EmpName { get; set; }
        //    public string DeptName { get; set; }
        //    public string PosName { get; set; }
        //    public string Printby { get; set; }
        //    public string ProfilePicture { get; set; }
        //}

        //class EmpGeneral
        //{
        //    public string BirthPlace { get; set; }
        //    public string BirthDt { get; set; }
        //    public string Address { get; set; }
        //    public string CityName { get; set; }
        //    public string SubDistrict { get; set; }
        //    public string Village { get; set; }
        //    public string RtRw { get; set; }
        //    public string PostalCode { get; set; }
        //    public string Religion { get; set; }
        //    public string Gender { get; set; }
        //    public string JoinDt { get; set; }
        //    public string ResignDt { get; set; }
        //    public string EmploymentStatus { get; set; }
        //    public string MaritalStatus { get; set; }
        //    public string MaritalDt { get; set; }
        //}

        //class EmpPersonal
        //{
        //    public string FamilyName { get; set; }
        //    public string Status { get; set; }
        //    public string BirthDt { get; set; }
        //}

        //class EmpWorkExperience
        //{
        //    public string Company { get; set; }
        //    public string Position { get; set; }
        //    public string Period { get; set; }
        //}

        //class EmpEducation
        //{
        //    public string School { get; set; }
        //    public string Level { get; set; }
        //    public string Major { get; set; }
        //}

        //class EmpCompetence
        //{
        //    public string CompetenceCode { get; set; }
        //    public string CompetenceName { get; set; }
        //    public string Position { get; set; }
        //    public string Description { get; set; }
        //    public decimal MinScore { get; set; }
        //    public decimal Score { get; set; }
        //}

        //class EmpSS
        //{
        //    public string SSName { get; set; }
        //    public string SSCard { get; set; }
        //    public string Period { get; set; }
        //}

        //class EmpPersonalRef
        //{
        //    public string PRName { get; set; }
        //    public string PRPhone { get; set; }
        //    public string PRAddress { get; set; }
        //}

        //class EmpFamilySS
        //{
        //    public string FSSFamilyName { get; set; }
        //    public string FSSStatus { get; set; }
        //    public string FSSName { get; set; }
        //    public string FSSCard { get; set; }
        //    public string FSSPeriod { get; set; }
        //}

        //class EmpWL
        //{
        //    public string Type { get; set; }
        //    public string Period { get; set; }
        //}

        //class EmpPPS
        //{
        //    public string Category { get; set; }
        //    public string From { get; set; }
        //    public string To { get; set; }
        //    public string StartDt { get; set; }
        //}

        //class EmpCompetenceOther
        //{
        //    public string OCompetenceCode { get; set; }
        //    public string OCompetenceName { get; set; }
        //    public string ODescription { get; set; }
        //    public decimal OMinScore { get; set; }
        //    public decimal OScore { get; set; }
        //}

        //#endregion
     
    }
   
}
