﻿#region Update
/*
    09/03/2022 [IBL/PHT] New Apps
    05/04/2022 [IBL/PHT] Bug: muncul warning input string was not in a correct format.
    11/04/2022 [IBL/PHT] Bug: tidak terbentuk pps saat ada kenaikan grade.
    19/04/2022 [IBL/PHT] Tambah filter site
                         Grade awal ambil dari tab Additional field First Grade di Master Employee
                         Perhitungan kenaikan dimulai dari Permanent date
                         Perhitungan work period dimulai dari Join Date
    25/04/2022 [IBL/PHT] Bug : Salah ambil subtractor
    27/06/2022 [IBL/PHT] Validasi, harus proses grade advancement dulu, di periode dan site yg terpilih.
                         Save PPSDocNo (yg tergenerate / yg membuat emp tsb tidak terbentuk pps) untuk keperluan reporting
                         Save PPSDocNo dari Salary Advancement walaupun tidak naik pangkat.
                         Tidak terbentuk PPS utk Grade Advancement walaupun naik pangkat.
    06/07/2022 [IBL/PHT] Bug: Column cannot be null saat save TblEmployeeGradeHistory
    19/07/2022 [ICA/PHT] PPS StartDt adalah tanggal 21 dan Date di Grade History ambil dari DocDt PPS
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPeriodicSalaryAdvancement : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private int
            mEvenScaleFirstPayRiseMonth = 0,
            mOddScaleFirstPayRiseMonth = 0,
            mNoOfYrGradeLevelChange = 0;
        private string mActingOfficialIndSource = string.Empty;
        DateTime mProcessDate = DateTime.MinValue;

        #endregion

        #region Constructor

        public FrmPeriodicSalaryAdvancement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method
        protected override void FrmLoad(object sender, EventArgs e)
        {
            GetParameter();
            string CurrentDateTime = Sm.ServerCurrentDateTime();
            Sl.SetLueYr(LueYr, "1996", 10);
            Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
            Sl.SetLueOption(ref LueMth, "PeriodicGradeSalaryAdvancementMth");
            Sl.SetLueSiteCode(ref LueSiteCode);
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParCode In (");
            SQL.AppendLine("'EvenScaleFirstPayRiseMonth', 'OddScaleFirstPayRiseMonth', 'NoOfYrGradeLevelChange','ActingOfficialIndSource' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //String
                            case "ActingOfficialIndSource":
                                if (ParValue.Length == 0) mActingOfficialIndSource = "1";
                                else mActingOfficialIndSource = ParValue;
                                break;
                            //Integer
                            case "EvenScaleFirstPayRiseMonth":
                                if (ParValue.Length == 0) mEvenScaleFirstPayRiseMonth = 19;
                                else mEvenScaleFirstPayRiseMonth = int.Parse(ParValue);
                                break;
                            case "OddScaleFirstPayRiseMonth":
                                if (ParValue.Length == 0) mOddScaleFirstPayRiseMonth = 7;
                                else mOddScaleFirstPayRiseMonth = int.Parse(ParValue);
                                break;
                            case "NoOfYrGradeLevelChange":
                                if (ParValue.Length == 0) mNoOfYrGradeLevelChange = 4;
                                else mNoOfYrGradeLevelChange = int.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        #region Button Method
        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            var lEmployee = new List<Employee>();
            var lEmployeeTemp = new List<Employee>();
            var Msg = string.Empty;

            if ((Sm.StdMsgYN("Process", string.Empty, mMenuCode) == DialogResult.No) || 
                IsProcessDataInvalid()) return;

            mProcessDate = Sm.ConvertDate(Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01");

            Process1(ref lEmployeeTemp);
            if (lEmployeeTemp.Count > 0)
            {
                Process2(ref lEmployeeTemp, ref lEmployee);
                if (IsGradeAdvancementNotProcessedYet(ref lEmployee)) return;
            }

            if (lEmployee.Count > 0)
            {
                Process3(ref lEmployee);
                Process4(ref lEmployee);
                UpdateLog();
            }

            Msg += lEmployee.Count + " data is processed.";
            Msg += Environment.NewLine;
            Msg += lEmployee.Where(w => w.IsAutoCreatePPS).Count() + " data has been formed PPS documents.";

            Sm.StdMsg(mMsgType.Info, Msg);
        }

        #endregion

        #region Process Data
        private bool IsProcessDataInvalid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsProcessDtInvalid();
        }

        private bool IsProcessDtInvalid()
        {
            bool IsDataProcessed = Sm.IsDataExist("Select 1 From TblPeriodicSalaryAdvancementLog Where Left(PSALogDt, 6) >= @Param1 And SiteCode = @Param2", Sm.GetLue(LueYr) + Sm.GetLue(LueMth), Sm.GetLue(LueSiteCode), string.Empty);
            
            if (IsDataProcessed)
            {
                string LastProcessDt = Sm.GetValue("Select PSALogDt From TblPeriodicSalaryAdvancementLog Where SiteCode = @Param " +
                    "And PSALogCode = (Select Max(PSALogCode) From TblPeriodicSalaryAdvancementLog Where SiteCode = @Param); ", Sm.GetLue(LueSiteCode));

                Sm.StdMsg(mMsgType.Warning, "Data on this site ("+LueSiteCode.Text+") in this period already processed." + Environment.NewLine +
                    "You should process data after "+ Sm.MonthName(LastProcessDt.Substring(4,2)) + " " + Sm.Left(LastProcessDt, 4));
                return true;
            }
            return false;
        }

        private bool IsGradeAdvancementNotProcessedYet(ref List<Employee> lEmployee)
        {
            bool IsProcessed = false;
            foreach (var x in lEmployee.Where(w => w.IsGradeRaise))
            {
                bool IsDataProcessed = Sm.IsDataExist("Select 1 From TblPeriodicGradeAdvancementLog Where Left(PGALogDt, 6) >= @Param1 And SiteCode = @Param2", Sm.GetLue(LueYr) + Sm.GetLue(LueMth), Sm.GetLue(LueSiteCode), string.Empty);
                if (!IsDataProcessed)
                {
                    Sm.StdMsg(mMsgType.Warning, "You have to process periodic grade advancement first." + Environment.NewLine +
                    "Site   : " + LueSiteCode.Text + Environment.NewLine +
                    "Period : " + Sm.MonthName(Sm.GetLue(LueMth)) + " " + Sm.GetLue(LueYr));
                    IsProcessed = true;
                    break;
                }
            }
            return IsProcessed;
        }

        private void Process1(ref List<Employee> lEmployeeTemp)
        {
            /*
                Cek Master Employee - Emp belum pernah naik gaji
                Cek dari Periodic Salary Advancement - Emp sudah pernah naik gaji
            */
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            lEmployeeTemp.Clear();

            SQL.AppendLine("Select T1.EmpCode, '000001' As DNo, T1.PayRaiseInd, T1.LeaveStartDt, T1.GrdLvlCode, T1.Scale, ");
            SQL.AppendLine("T1.Subtractor, T1.PPRDocNo, 0.00 As WorkPeriodYr, 0.00 As WorkPeriodMth, '00010101' As LastGradeRaise, ");
            SQL.AppendLine("T1.SysJoinDt As JoinDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select A.EmpCode, 'N' As PayRaiseInd, ");
            SQL.AppendLine("	Case ");
            SQL.AppendLine("	  When Right(A.LeaveStartDt, 2) != '01' ");
            SQL.AppendLine("			Then Replace( Date_Add(Date_Add(A.LeaveStartDt, Interval 1 Month), ");
            SQL.AppendLine("	          Interval (Cast(Right(A.LeaveStartDt, 2) As Int)-1)*-1 Day), '-','') ");
            SQL.AppendLine("	  Else A.LeaveStartDt ");
            SQL.AppendLine("	End As LeaveStartDt, ");
            SQL.AppendLine("	Case ");
            SQL.AppendLine("	  When Right(A.JoinDt, 2) != '01' ");
            SQL.AppendLine("			Then Replace( Date_Add(Date_Add(A.JoinDt, Interval 1 Month), ");
            SQL.AppendLine("	          Interval (Cast(Right(A.JoinDt, 2) As Int)-1)*-1 Day), '-','') ");
            SQL.AppendLine("	  Else A.JoinDt ");
            SQL.AppendLine("	End As SysJoinDt, ");
            SQL.AppendLine("	A.FirstGrdLvlCode As GrdLvlCode, ");
            SQL.AppendLine("	B.Scale, B.Subtractor, C.DocNo As PPRDocNo ");
            SQL.AppendLine("	From TblEmployee A ");
            SQL.AppendLine("	Inner Join TblPeriodicPromotionSettingDtl B On A.FirstGrdLvlCode = B.GrdLvlCode ");
            SQL.AppendLine("	Inner Join TblPeriodicPromotionSettingHdr C On B.DocNo = C.DocNo And C.CancelInd = 'N' ");
            SQL.AppendLine("	Where A.EmpCode Not In (Select Distinct(EmpCode) From TblPeriodicSalaryAdvancement) ");
            SQL.AppendLine("And((A.ResignDt Is Not Null And A.ResignDt >= (Select concat(replace(curdate(), '-', '')))) Or A.ResignDt Is NULL) ");
            SQL.AppendLine("And A.LeaveStartDt Is Not Null ");
            SQL.AppendLine("And A.LeaveStartDt > A.JoinDt ");
            SQL.AppendLine("And A.FirstGrdLvlCode Is Not Null ");
            SQL.AppendLine("And A.SiteCode = @SiteCode ");
            SQL.AppendLine(")T1 ");
            SQL.AppendLine("Where TimeStampDiff(Year,T1.LeaveStartDt, Concat(@Yr,@Mth,'01')) >= 2 ");
            SQL.AppendLine("And ( ");
            SQL.AppendLine("	(T1.Scale = 'O' And TIMESTAMPDIFF(MONTH,T1.LeaveStartDt, Concat(@Yr,@Mth,'01')) >= @OddScaleFirstPayRiseMonth) ");
            SQL.AppendLine("	Or ");
            SQL.AppendLine("	(T1.Scale = 'E' And TIMESTAMPDIFF(MONTH,T1.LeaveStartDt, Concat(@Yr,@Mth,'01')) >= @EvenScaleFirstPayRiseMonth) ");
            SQL.AppendLine(") ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select T1.EmpCode, T1.DNo, T1.PayRaiseInd, T1.LeaveStartDt, T1.GrdLvlCode, T1.Scale, T1.Subtractor, T1.PPRDocNo, ");
            SQL.AppendLine("T1.WorkPeriodYr, T1.WorkPeriodMth, IfNull(T2.LastGradeRaise, T1.LeaveStartDt) As LastGradeRaise, T1.SystemJoinDt As JoinDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select A.EmpCode, Cast(A.DNo As Unsigned)+1 As DNo, 'Y' As PayRaiseInd, A.SystemJoinDt, ");
            SQL.AppendLine("	  Concat(A.SalaryIncreaseYr,A.SalaryIncreaseMth,'01') As LeaveStartDt, A.GrdLvlCode, ");
            SQL.AppendLine("	D.Scale, D.Subtractor, C.DocNo As PPRDocNo, A.WorkPeriodYr, A.WorkPeriodMth ");
            SQL.AppendLine("	From TblPeriodicSalaryAdvancement A ");
            SQL.AppendLine("	Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("	Inner Join TblPeriodicPromotionSettingHdr C On A.PeriodicPromotionSettingDocNo = C.DocNo ");
            SQL.AppendLine("		And C.CancelInd = 'N' ");
            SQL.AppendLine("	Inner Join TblPeriodicPromotionSettingDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("	    And A.GrdLvlCode = D.GrdLvlCode ");
            SQL.AppendLine("	Where B.ResignDt Is Null ");
            SQL.AppendLine("	And B.SiteCode = @SiteCode ");
            SQL.AppendLine("	And A.DNo = (Select Max(DNo) From TblPeriodicSalaryAdvancement Where EmpCode = A.EmpCode) ");
            SQL.AppendLine(")T1 ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select Concat(SalaryIncreaseYr,SalaryIncreaseMth,'01') As LastGradeRaise, A.EmpCode ");
            SQL.AppendLine("	From TblPeriodicSalaryAdvancement A ");
            SQL.AppendLine("	Where ");
            SQL.AppendLine("	( ");
            SQL.AppendLine("	   (Exists (Select 1 From TblPeriodicSalaryAdvancement Where EmpCode = A.EmpCode And GradeRaiseInd = 'Y') ");
            SQL.AppendLine("	   And A.DNo = (Select Max(DNo) From TblPeriodicSalaryAdvancement Where EmpCode = A.EmpCode And GradeRaiseInd = 'Y')) ");
            SQL.AppendLine("	   Or ");
            SQL.AppendLine("	   (Not Exists (Select 1 From TblPeriodicSalaryAdvancement Where EmpCode = A.EmpCode And GradeRaiseInd = 'Y') ");
            SQL.AppendLine("	   And A.DNo = '000001') ");
            SQL.AppendLine("	) ");
            SQL.AppendLine(")T2 On T1.EmpCode = T2.EmpCode ");
            SQL.AppendLine("Where TimeStampDiff(Year,T1.LeaveStartDt, Concat(@Yr,@Mth,'01')) >=2;");

            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<int>(ref cm, "@OddScaleFirstPayRiseMonth", mOddScaleFirstPayRiseMonth);
            Sm.CmParam<int>(ref cm, "@EvenScaleFirstPayRiseMonth", mEvenScaleFirstPayRiseMonth);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] {
                    //0
                    "EmpCode",

                    //1-5
                    "DNo", "PayRaiseInd", "LeaveStartDt", "GrdLvlCode", "JoinDt",
                    
                    //6-10
                    "Scale", "Subtractor", "PPRDocNo", "WorkPeriodYr", "WorkPeriodMth",

                    //11
                    "LastGradeRaise"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                        lEmployeeTemp.Add(new Employee() {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DNo = Sm.Right("000000" + Sm.DrStr(dr, c[1]), 6),
                            IsPayRaiseExist = Sm.DrStr(dr, c[2]) == "Y",
                            FirstYrMth = Sm.ConvertDate(Sm.DrStr(dr, c[3])),
                            LeaveStartDt = Sm.ConvertDate(Sm.DrStr(dr, c[3])),
                            GrdLvlCode = Sm.DrStr(dr, c[4]),
                            SysJoinDt = Sm.DrStr(dr, c[5]),
                            SysJoinDtTemp = Sm.ConvertDate(Sm.DrStr(dr, c[5])),
                            Scale = Sm.DrStr(dr, c[6]),
                            Subtractor = Sm.DrDec(dr, c[7]),
                            SubtractorTemp = 0,
                            PPRDocNo = Sm.DrStr(dr, c[8]),
                            WorkPeriodYr = Sm.DrDec(dr, c[9]),
                            WorkPeriodMth = Sm.DrDec(dr, c[10]),
                            LastGradeRaise = Sm.ConvertDate(Sm.DrStr(dr, c[11])),
                        });
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Employee> lEmployeeTemp, ref List<Employee> lEmployee)
        {
            foreach (var x in lEmployeeTemp)
            {
                string
                    EmpCode = string.Empty,
                    DNo = string.Empty,
                    SysJoinDt = string.Empty,
                    SalaryIncreaseYr = string.Empty,
                    SalaryIncreaseMth = string.Empty,
                    GrdLvlCode = string.Empty,
                    Scale = string.Empty,
                    PPRDocNo = string.Empty,
                    GrdLvlNew = x.GrdLvlCode,
                    ScaleNew = x.Scale;
                int
                    WorkPeriodYr = 0,
                    WorkPeriodMth = 0,
                    Subtractor = 0,
                    SubtractorNew = Convert.ToInt32(x.SubtractorTemp);
                bool
                    IsGradeRaise = false;

                DateTime StartDt = (x.IsPayRaiseExist ? x.LastGradeRaise : x.LeaveStartDt);
                DateTime EndDt;

                if (!x.IsPayRaiseExist)
                {
                    int MonthToAdd = (x.Scale == "O" ? mOddScaleFirstPayRiseMonth : mEvenScaleFirstPayRiseMonth);
                    x.FirstYrMth = x.FirstYrMth.AddMonths(MonthToAdd);
                }
                else
                    x.FirstYrMth = x.FirstYrMth.AddMonths(24);

                for (DateTime date = x.FirstYrMth; date <= mProcessDate; date = date.AddYears(2))
                {
                    string
                        SalaryIncYr = string.Empty,
                        SalaryIncMth = string.Empty,
                        Yr = string.Empty,
                        Mth = string.Empty;
                    int
                        WorkPeriod = 0,
                        WorkPeriodYrTmp = 0,
                        WorkPeriodMthTmp = 0,
                        WorkPeriod2 = Convert.ToInt32(x.WorkPeriodYr);

                    Yr = date.Year.ToString();
                    Mth = Sm.Right("00" + date.Month.ToString(), 2);
                    if (Mth == "01" || Mth == "07")
                    {
                        SalaryIncYr = Yr;
                        SalaryIncMth = Mth;
                    }
                    else if (Decimal.Parse(Mth) > 1 && Decimal.Parse(Mth) < 7)
                    {
                        SalaryIncYr = Yr;
                        SalaryIncMth = "07";
                    }
                    else
                    {
                        SalaryIncYr = (Decimal.Parse(Yr) + 1).ToString();
                        SalaryIncMth = "01";
                    }

                    EndDt = Sm.ConvertDate(SalaryIncYr + SalaryIncMth + "01");
                    WorkPeriod = GetWorkPeriod(StartDt, EndDt);
                    WorkPeriodYrTmp = GetWorkPeriod(x.SysJoinDtTemp, EndDt); //Work Period Year From Join Dt
                    WorkPeriodMthTmp = GetWorkPeriodMth(x.SysJoinDtTemp, EndDt); //Work Period Month From Join Dt
                    WorkPeriod2 = WorkPeriod2 + 2;

                    if (!x.IsPayRaiseExist && WorkPeriodMthTmp == 0)
                    {
                        WorkPeriodYrTmp = WorkPeriodYrTmp - 1;
                        WorkPeriodMthTmp = 12;
                    }
                    if (x.IsPayRaiseExist && x.WorkPeriodMth == 0)
                    {
                        WorkPeriod2 = WorkPeriod2 - 1;
                        x.WorkPeriodMth = 12;
                    }

                    if (WorkPeriod >= 4)
                    {
                        SubtractorNew += Convert.ToInt32(Sm.GetValueDec("Select Subtractor From TblPeriodicPromotionSettingDtl Where DocNo = @Param1 And GrdLvlCode = @Param2", x.PPRDocNo, GrdLvlNew));
                        ScaleNew = Sm.GetValue("Select Scale From TblPeriodicPromotionSettingDtl Where DocNo = @Param1 And GrdLvlCode = @Param2", x.PPRDocNo, GrdLvlNew, string.Empty);
                        GrdLvlNew = Sm.GetValue("Select GrdLvlCode2 From TblPeriodicPromotionSettingDtl Where DocNo = @Param1 And GrdLvlCode = @Param2", x.PPRDocNo, GrdLvlNew, string.Empty);
                        
                        EmpCode = x.EmpCode;
                        DNo = x.DNo;
                        SysJoinDt = x.SysJoinDt;
                        SalaryIncreaseYr = SalaryIncYr;
                        SalaryIncreaseMth = SalaryIncMth;
                        GrdLvlCode = GrdLvlNew;
                        Scale = ScaleNew;
                        PPRDocNo = x.PPRDocNo;
                        WorkPeriodYr = (x.IsPayRaiseExist ? WorkPeriod2 : WorkPeriodYrTmp) - SubtractorNew;
                        WorkPeriodMth = (x.IsPayRaiseExist ? Convert.ToInt32(x.WorkPeriodMth) : WorkPeriodMthTmp);
                        Subtractor = SubtractorNew;
                        IsGradeRaise = true;
                        StartDt = EndDt;
                    }
                    else
                    {
                        EmpCode = x.EmpCode;
                        DNo = x.DNo;
                        SysJoinDt = x.SysJoinDt;
                        SalaryIncreaseYr = SalaryIncYr;
                        SalaryIncreaseMth = SalaryIncMth;
                        GrdLvlCode = GrdLvlNew;
                        Scale = ScaleNew;
                        PPRDocNo = x.PPRDocNo;
                        WorkPeriodYr = (x.IsPayRaiseExist ? WorkPeriod2 : WorkPeriodYrTmp) - SubtractorNew;
                        WorkPeriodMth = (x.IsPayRaiseExist ? Convert.ToInt32(x.WorkPeriodMth) : WorkPeriodMthTmp);
                        Subtractor = SubtractorNew;
                    }

                    
                }

                bool IsGradeExists = Sm.IsDataExist("Select 1 From TblEmployee Where EmpCode=@Param1 And GrdLvlCode = @Param2;", EmpCode, GrdLvlCode, string.Empty),
                     IsEmpPPSExists = Sm.IsDataExist("Select 1 From TblPPS Where EmpCode=@Param1 And StartDt >= @Param2;", EmpCode, Sm.ConvertDate(SalaryIncreaseYr + SalaryIncreaseMth + "21").AddMonths(-2).ToString("yyyyMMdd"), string.Empty);

                lEmployee.Add(new Employee()
                {
                    EmpCode = EmpCode,
                    DNo = DNo,
                    SysJoinDt = SysJoinDt,
                    GrdLvlCode = GrdLvlCode,
                    SalaryIncreaseYr = SalaryIncreaseYr,
                    SalaryIncreaseMth = SalaryIncreaseMth,
                    SalaryIncreaseDt = SalaryIncreaseYr + SalaryIncreaseMth + "21",
                    WorkPeriodYr = WorkPeriodYr,
                    WorkPeriodMth = WorkPeriodMth,
                    Scale = Scale,
                    Subtractor = Subtractor,
                    PPRDocNo = PPRDocNo,
                    IsGradeRaise = IsGradeRaise,
                    IsAutoCreatePPS = !IsEmpPPSExists
                });

                //foreach (var x1 in lEmployee)
                //{
                //    Console.WriteLine(x1.EmpCode + "\t\t\t" + x1.DNo + "\t\t\t" + GrdLvlCode + "\t\t\t" +
                //        SalaryIncreaseYr + "\t\t\t" + SalaryIncreaseMth + "\t\t\t" + WorkPeriodYr + "\t\t\t" + WorkPeriodMth);
                //}
            }
        }

        private void Process3(ref List<Employee> lEmployee)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            int i = 0;

            /*Save Periodic Salary Advancement*/
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");
            foreach (var x in lEmployee)
            {
                if (IsFirstOrExisted)
                {
                    SQL.AppendLine("Insert Into TblPeriodicSalaryAdvancement(EmpCode, DNo, SystemJoinDt, GrdLvlCode, SalaryIncreaseYr, SalaryIncreaseMth, ");
                    SQL.AppendLine("WorkPeriodYr, WorkPeriodMth, Subtractor, PeriodicPromotionSettingDocNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");

                    IsFirstOrExisted = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@EmpCode_" + i.ToString() + ", @DNo_" + i.ToString() + ", @SystemJoinDt_" + i.ToString() + ", @GrdLvlCode_" + i.ToString() + ", ");
                SQL.AppendLine("@SalaryIncYr_" + i.ToString() + ", @SalaryIncMth_" + i.ToString() + ", @WorkPeriodYr_" + i.ToString() + ", @WorkPeriodMth_" + i.ToString() + ", ");
                SQL.AppendLine("@Subtractor_" + i.ToString() + ", @PPRDocNo_" + i.ToString() + ", @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "@EmpCode_" + i.ToString(), x.EmpCode);
                Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), x.DNo);
                Sm.CmParam<String>(ref cm, "@SystemJoinDt_" + i.ToString(), x.SysJoinDt);
                Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + i.ToString(), x.GrdLvlCode);
                Sm.CmParam<String>(ref cm, "@SalaryIncYr_" + i.ToString(), x.SalaryIncreaseYr);
                Sm.CmParam<String>(ref cm, "@SalaryIncMth_" + i.ToString(), x.SalaryIncreaseMth);
                Sm.CmParam<Decimal>(ref cm, "@WorkPeriodYr_" + i.ToString(), x.WorkPeriodYr);
                Sm.CmParam<Decimal>(ref cm, "@WorkPeriodMth_" + i.ToString(), x.WorkPeriodMth);
                Sm.CmParam<Decimal>(ref cm, "@Subtractor_" + i.ToString(), x.Subtractor);
                Sm.CmParam<String>(ref cm, "@PPRDocNo_" + i.ToString(), x.PPRDocNo);
                i++;
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
        }
        
        private void Process4(ref List<Employee> lEmployee)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var SQL4 = new StringBuilder();
            var cm = new MySqlCommand();
            var Date = Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01";


            bool IsFirst = true;
            int i = 0;
            string mPPSDocDt = string.Empty,
                GrdHistoryDNo = string.Empty,
                mStartDt = string.Empty,
                mEndDt = string.Empty;

            foreach (var x in lEmployee)
            {
                if (x.IsAutoCreatePPS)
                {
                    if (IsFirst)
                    {
                        SQL.AppendLine("Set @Dt := CurrentDateTime();");
                        //Save PPS
                        SQL.AppendLine("Insert Into TblPPS(DocNo, DocDt, CancelInd, Status, Jobtransfer, StartDt, EmployeeRequestDocNo, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, ");
                        SQL.AppendLine("DeptCodeOld, PosCodeOld, PositionStatusCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, PGCodeOld, SiteCodeOld, ResignDtOld, SectionCodeOld, LevelCodeOld, ");
                        SQL.AppendLine("DeptCodeNew, PosCodeNew, PositionStatusCodeNew, GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, SiteCodeNew, ResignDtNew, SectionCodeNew, LevelCodeNew, UpdLeaveStartDtInd, Remark,");
                        if (mActingOfficialIndSource == "2") SQL.AppendLine("ActingOfficialIndOld, ");
                        SQL.AppendLine("WorkPeriodYr, WorkPeriodMth, ActingOfficialInd, CreateBy, CreateDt) ");

                        //Save Employee PPS
                        SQL2.AppendLine("Insert Into TblEmployeePPS ");
                        SQL2.AppendLine("(EmpCode, StartDt, EndDt, DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, SectionCode, LevelCode, CreateBy, CreateDt) ");

                        IsFirst = false;
                    }
                    else
                    {
                        SQL.AppendLine("Union All ");
                        SQL2.AppendLine("Union All ");
                    }

                    //Save PPS
                    SQL.AppendLine("Select @PPSDocNo_" + i.ToString() + ", @PPSDocDt_" + i.ToString() + ", 'N', 'A', 'SA', @StartDt_" + i.ToString() + ", Null, @EmpCode_" + i.ToString() + ", Null, Null, ");
                    SQL.AppendLine("DeptCode, PosCode, PositionStatusCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, ResignDt, SectionCode, LevelCode, ");
                    SQL.Append("DeptCode, PosCode, PositionStatusCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, ResignDt, SectionCode, LevelCode, 'N', Null, ");
                    if (mActingOfficialIndSource == "2") SQL.AppendLine("ActingOfficialInd, ");
                    SQL.AppendLine("@WorkPeriodYr_" + i.ToString() + ", @WorkPeriodMthDec_" + i.ToString() + ", ActingOfficialInd, @UserCode, @Dt ");
                    SQL.AppendLine("From TblEmployee Where EmpCode = @EmpCode_" + i.ToString());

                    //Save Employee PPS
                    SQL2.AppendLine("Select @EmpCode_" + i.ToString() + ", @StartDt_" + i.ToString() + ", Null, ");
                    SQL2.AppendLine("DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, ");
                    SQL2.AppendLine("PGCode, SiteCode, SectionCode, LevelCode, @UserCode, @Dt ");
                    SQL2.AppendLine("From TblEmployee Where EmpCode = @EmpCode_" + i.ToString());

                    //Update EndDt EmployeePPS Old
                    SQL3.AppendLine("Update TblEmployeePPS Set ");
                    SQL3.AppendLine("    EndDt=@EndDt_" + i.ToString());
                    SQL3.AppendLine("Where EmpCode=@EmpCode_" + i.ToString());
                    SQL3.AppendLine("And StartDt=( ");
                    SQL3.AppendLine("    Select StartDt From ( ");
                    SQL3.AppendLine("        Select Max(StartDt) StartDt ");
                    SQL3.AppendLine("        From TblEmployeePPS ");
                    SQL3.AppendLine("        Where EmpCode=@EmpCode_" + i.ToString());
                    SQL3.AppendLine("        And StartDt< @StartDt_" + i.ToString());
                    SQL3.AppendLine("    ) T ); ");

                    //Update GradeRaiseInd Periodic Salary Advancement
                    SQL3.AppendLine("Update TblPeriodicSalaryAdvancement ");
                    SQL3.AppendLine("   Set GradeRaiseInd = 'Y', ");
                    SQL3.AppendLine("   PPSDocNo = @PPSDocNo_" + i.ToString() + " ");
                    SQL3.AppendLine("Where EmpCode = @EmpCode_" + i.ToString() + " And DNo = @DNo_" + i.ToString() + "; ");

                    mStartDt = Sm.ConvertDate(x.SalaryIncreaseDt).AddMonths(-2).ToString("yyyyMMdd");
                    mEndDt = Sm.ConvertDate(x.SalaryIncreaseDt).AddMonths(-2).AddDays(-1).ToString("yyyyMMdd");
                    GrdHistoryDNo = Sm.GetValue("select Cast(Max(DNo) As Unsigned)+1 From TblEmployeeGradeHistory Where EmpCode = @Param Limit 1;", x.EmpCode);
                    GrdHistoryDNo = Sm.Right("000" + (GrdHistoryDNo.Length > 0 ? GrdHistoryDNo : "1"), 3);

                    Sm.CmParam<String>(ref cm, "@PPSDocNo_" + i.ToString(), Sm.GenerateDocNo(Date, "PPS", "TblPPS", (i + 1).ToString()));
                    Sm.CmParam<String>(ref cm, "@PPSDocDt_" + i.ToString(), Date);
                    Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), x.DNo);
                    Sm.CmParam<String>(ref cm, "@StartDt_" + i.ToString(), mStartDt);
                    Sm.CmParam<String>(ref cm, "@EndDt_" + i.ToString(), mEndDt);
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + i.ToString(), x.EmpCode);
                    Sm.CmParam<Decimal>(ref cm, "@WorkPeriodYr_" + i.ToString(), x.WorkPeriodYr);
                    Sm.CmParam<Decimal>(ref cm, "@WorkPeriodMthDec_" + i.ToString(), x.WorkPeriodMth);
                    Sm.CmParam<String>(ref cm, "@WorkPeriodMth_" + i.ToString(), Sm.Right("00" + x.WorkPeriodMth.ToString(), 2));
                    Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + i.ToString(), x.GrdLvlCode);
                    Sm.CmParam<String>(ref cm, "@GrdHistoryDNo_" + i.ToString(), GrdHistoryDNo);
                    i++;
                }
                else
                {
                    string LastPPSDocNo = Sm.GetValue("Select DocNo From TblPPS Where EmpCode = @Param And CancelInd = 'N' Order By StartDt Desc Limit 1; ", x.EmpCode);

                    SQL3.AppendLine("Update TblPeriodicSalaryAdvancement ");
                    SQL3.AppendLine("    Set PPSDocNo = '" + LastPPSDocNo + "' ");
                    SQL3.AppendLine("Where EmpCode = '" + x.EmpCode + "' And DNo = '" + x.DNo + "'; ");
                }

                //Save Employee Grade History
                SQL4.AppendLine("Insert Into TblEmployeeGradeHistory ");
                SQL4.AppendLine("(EmpCode, DNo, DocDt, DecisionLetter, Duration, GrdLvlCode, Amt, PeriodicSettingInd, Remark, CreateBy, CreateDt) ");
                SQL4.AppendLine("Select A.EmpCode, Right(Concat('000',Cast(IfNull(C.DNo, 0) As Int)+1), 3) As DNo, B.DocDtNGER, B.DocNo, ");
                SQL4.AppendLine("Concat(B.WorkPeriodYr,' | ',Right(Concat('00',B.WorkPeriodMth),2)) As Duration, B.GrdLvlCodeNew, ");
                SQL4.AppendLine("IfNull(D.Amt, 0.00) As Amt, '" + (x.IsAutoCreatePPS ? "Y" : "N") + "', 'Salary Advancement', A.CreateBy, A.CreateDt ");
                SQL4.AppendLine("From TblPeriodicSalaryAdvancement A ");
                SQL4.AppendLine("Inner Join TblPPS B On A.PPSDocNo = B.DocNo ");
                SQL4.AppendLine("	And A.EmpCode = '" + x.EmpCode + "' And A.DNo = '" + x.DNo + "' ");
                SQL4.AppendLine("Left Join ( ");
                SQL4.AppendLine("	Select EmpCode, Max(DNo) As DNo ");
                SQL4.AppendLine("	From TblEmployeeGradeHistory ");
                SQL4.AppendLine("	Where EmpCode = '" + x.EmpCode + "' ");
                SQL4.AppendLine(") C On A.EmpCode = C.EmpCode ");
                SQL4.AppendLine("Left Join TblGradeSalaryDtl D On B.GrdLvlCodeNew = D.GrdSalaryCode ");
                SQL4.AppendLine("	And D.WorkPeriod = B.WorkPeriodYr ");
                SQL4.AppendLine("	And D.Month = B.WorkPeriodMth; ");
            }

            if (!IsFirst)
            {
                SQL.AppendLine("; ");
                SQL2.AppendLine("; ");
            }

            cm.CommandText = SQL.ToString() + SQL2.ToString() + SQL3.ToString() + SQL4.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            Sm.ExecCommand(cm);
        }

        private void UpdateLog()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string PSALogCode = Sm.GetValue("select Cast(Max(PSALogCode) As Unsigned)+1 From TblPeriodicSalaryAdvancementLog Limit 1");
            PSALogCode = Sm.Right("000000" + (PSALogCode.Length > 0 ? PSALogCode : "1"), 6);

            /*Save Periodic Salary Advancement Log*/
            SQL.AppendLine("Insert Into TblPeriodicSalaryAdvancementLog(PSALogCode, PSALogDt, SiteCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PSALogCode, @PSALogDt, @SiteCode, @UserCode, @Dt);");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@PSALogCode", PSALogCode);
            Sm.CmParam<String>(ref cm, "@PSALogDt", Sm.GetLue(LueYr) + Sm.GetLue(LueMth) + "01");
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ExecCommand(cm);
        }
        #endregion

        #region Additional Method

        private int GetWorkPeriod(DateTime StartDt, DateTime EndDt)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);
            int DiffYr = (zeroTime + (EndDt-StartDt)).Year - 1;
            return DiffYr;
        }

        private int GetWorkPeriodMth(DateTime StartDt, DateTime EndDt)
        {
            int DiffMth = ((EndDt.Year - StartDt.Year) * 12) + EndDt.Month - StartDt.Month;
            return DiffMth % 12;
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMth, new Sm.RefreshLue2(Sl.SetLueOption), "PeriodicGradeSalaryAdvancementMth");
        }

        #endregion

        #endregion

        #region Class

        private class Employee
        {
            public string EmpCode { get; set; }
            public string DNo { get; set; }
            public bool IsPayRaiseExist { get; set; }
            public DateTime SysJoinDtTemp { get; set; }
            public string SysJoinDt { get; set; }
            public DateTime FirstYrMth { get; set; }
            public DateTime LeaveStartDt { get; set; }
            public DateTime LastGradeRaise { get; set; }
            public string SalaryIncreaseYr { get; set; }
            public string SalaryIncreaseMth { get; set; }
            public string SalaryIncreaseDt { get; set; }
            public decimal WorkPeriodYr { get; set; }
            public decimal WorkPeriodMth { get; set; }
            public string GrdLvlCode { get; set; }
            public string Scale { get; set; }
            public decimal Subtractor { get; set; }
            public decimal SubtractorTemp { get; set; }
            public string PPRDocNo { get; set; }
            public bool IsGradeRaise { get; set; }
            public bool IsAutoCreatePPS { get; set; }
        }

        #endregion
    }
}
