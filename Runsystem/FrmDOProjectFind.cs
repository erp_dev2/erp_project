﻿#region Update
/*
    10/12/2019 [WED/IMS] new apps --> DO To Project
    10/10/2021 [IQA/IMS] Menambah ceklist exclude cancel ketika find do to product
    07/07/2021 [VIN/IMS] Bug Filter show data
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOProjectFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOProject mFrmParent;

        #endregion

        #region Constructor

        public FrmDOProjectFind(FrmDOProject FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                var CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.CancelInd, T1.DocDt, T2.WhsName, ");
            SQL.AppendLine("T1.FromTo, T1.Lot, T1.Bin, T1.ItCode, T3.ItName, T4.PropName, T1.BatchNo, T1.Source, ");
            SQL.AppendLine("T1.Qty, T3.InventoryUomCode, T1.Qty2, T3.InventoryUomCode2, T1.Qty3, T3.InventoryUomCode3, ");
            SQL.AppendLine("T1.CreateBy, T1.CreateDt, T1.LastUpBy, T1.LastUpDt, T3.ForeignName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("Select A.DocNo, A.CancelInd, A.DocDt, A.WhsCode, ");
            SQL.AppendLine("'1' As FromTo, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblDOProjectDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) " + Filter);
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select A.DocNo, A.CancelInd, A.DocDt, A.WhsCode, ");
            SQL.AppendLine("'2' As FromTo, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt ");
            SQL.AppendLine("From TblDOProjectHdr A ");
            SQL.AppendLine("Inner Join TblDOProjectDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) " + Filter);
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblWareHouse T2 On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=T2.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblItem T3 On T1.ItCode=T3.ItCode ");
            SQL.AppendLine("Left Join TblProperty T4 On T1.PropCode=T4.PropCode ");
            SQL.AppendLine("Order By T1.CreateDt, T1.DocNo, T1.FromTo;");
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Cancel",
                        "Date",
                        "Warehouse",
                        "Lot",
                        
                        //6-10
                        "Bin",
                        "From/To",
                        "Item's Code",
                        "Item's Name",
                        "Property",

                        //11-15
                        "Batch#",
                        "Source",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Quantity",
                        "UoM",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        
                        //21-25
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Foreign Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 50, 80, 200, 60, 
                        
                        //6-10
                        60, 100, 100, 200, 0,

                        //11-15
                        200, 200, 100, 60, 100,

                        //16-20
                        60, 100, 60, 100, 100,

                        //21-25
                        100, 100, 100, 100, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 8, 10, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 }, false);
            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 25 });
            ShowInventoryUomCode();

            Grd1.Cols[25].Move(10);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 8, 10, 12, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                IsFilterByDateInvalid()
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";

                if (ChkCancel.Checked)
                {
                    var mSQL2 = new StringBuilder();

                    mSQL2.AppendLine("And A.CancelInd = 'N' ");

                    Filter += mSQL2.ToString();
                }

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter),
                        new string[]
                        {
                              //0
                              "DocNo",

                              //1-5
                              "CancelInd", "DocDt", "WhsName", "Lot", "Bin", 
                              
                              //6-10
                              "FromTo", "ItCode", "ItName", "PropName", "BatchNo", 
                              
                              //11-15
                              "Source", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                              
                              //16-20
                              "Qty3", "InventoryUomCode3", "CreateBy", "CreateDt", "LastUpBy", 
                              
                              //21-22
                              "LastUpDt", "ForeignName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Grd.Cells[Row, 7].Value = dr.GetString(c[6])=="1"?"From":"To";
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private bool IsFilterByDateInvalid()
        {
            if (Sm.CompareDtTm(Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        
        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }     

        #endregion

        

        #endregion
    }
}
