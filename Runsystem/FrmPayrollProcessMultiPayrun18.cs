﻿#region Update
/*
    17/11/2020 [TKG/PHT] New Application
    02/12/2020 [TKG/PHT] tambah kolom SSEmployerBPJS+SSEmployeeNonBPJS dan prosestable EePayrollProcessSSProgram
    03/12/2020 [TKG/PHT] bug hitung kolom ss bpjs bni
    20/12/2020 [TKG/PHT] tambah warning letter
    22/12/2020 [TKG/PHT] perhitungan Netto yg divalidasi dengan UMK dan UMPP
    30/12/2020 [TKG/PHT] untuk calon karyawan yg masa kerjanya kurang dari 4 bulan hanya mendapatkan 80% dari gajinya (disetting dari grade salary).
    01/01/2021 [TKG/PHT] tambah tunjangan masa kerja
    02/01/2021 [TKG/PHT] validasi kalau karyawan meninggal.
            ada paramter untuk menampung code job transfer meninggal dunia. ketika pps job transfer meninggal dunia, payroll process selanjutnya, bruto gaji karyawan tersebut tidak dikurangi ssemployee semua program ss.
            validasi meninggalnya --> di parameter baru dan job transfer. parameternya nanti di isi kode untuk meninggal duniapengaruhnya hanya di gaji brutonya saja
    03/01/2021 [TKG/PHT] perubahan proses perhitungan OT
    12/01/2021 [TKG/PHT] perhitungan tax/tax allowance berdasarkan masa kerja karyawan.
    15/01/2021 [TKG/PHT] memisah proses penyimpanan tax+tax allowance dengan thp+brutto+netto
    22/02/2021 [TKG/PHT] Jika karyawan tidak mendapatkan tunjangan SMK, apabila nettonya dibawah UMPP, maka tidak perlu diadjust sesuai dengan UMPP-nya.
    28/02/2021 [TKG/PHT] Mempercepat proses penyimpanan payroll process AD
    29/03/2021 [TKG/PHT] mempercepat save data
    27/04/2021 [TKG/PHT] mempercepat proses save data
    24/05/2021 [TKG/PHT] tambah informasi ss program
    31/05/2021 [TKG/PHT] bug perhitungan ss program WNR
    06/01/2022 [VIN/PHT] bug perhitungan tax:GetYearsOfService
    06/01/2022 [TKG/PHT] Kasih remark pada query save
    25/01/2022 [TKG/PHT] ubah GetParameter()
    23/02/2022 [WED/PHT] tambah perhitungan untuk Allowance TPJ (Tunjangan Penyesuaian Jabatan)
                         ada variable mDeviation untuk hitung selisih antara Netto Actual dengan UMK atau UMPP. Jika UMK < UMPP, maka yg diambil sebagai nilai pembanding adalah UMPP, else UMK. kalau Netto Actual > UMK atau UMPP, maka Deviation = 0, else Deviation = (UMK atau UMPP) - Netto Actual
                         ada variable mADCodePositionAdjustmentAmt untuk nyimpan nilai terbaru dari allowance Tunjangan Penyesuaian Jabatan (TPJ)
    18/03/2022 [ICA/PHT] Mengubah payroll process formula untuk mneghitung brutto dan THP. Brutto dan THP yg disimpan adalah yg sudah dikenakan pajak. 
    22/03/2022 [ICA/PHT] mengubah payroll process formula untuk menghitung netto. Netto yg disimpan adalah yg sudah dikenakan pajak.
    13/04/2022 [WED/PHT] bug fix TPJ
    14/04/2022 [WED/PHT] bug fix query TPJ
    15/04/2022 [WED/PHT] tambah kalkulasi BPJS
    23/05/2022 [WED/PHT] perubahan rule TPJ --> tidak melihat PPS
    24/05/2022 [WED/PHT] yg method save TPJ dipagarin parameter ADCodePositionAdjustment
    10/06/2022 [ICA/PHT] update fix allowance di tblpayrollprocess1 ketika menggunakan ADCodePositionAdjustment
    15/06/2022 [DITA/PHT] based on param : IsPayrollAmtRounded semua amount di bulatkan kebawah
    22/06/2022 [ICA/PHT] update brutto, netto dan thp menggunakan TPJ otomatis ketika menggunakan param ADCodePositionAdjustment
    23/06/2022 [ICA/PHT] tambah perhitungan otomatis BPJS berdasarkan parameter IsPayrollProcessingCalculateBPJSAutomatic
    06/07/2022 [DITA/PHT] hasil perhitungan BPJS otomatis belum mengacu ke param : IsPayrollAmtRounded
    19/07/2022 [ICA/PHT] jika warning letter > 0, netto adjustment tidak di akumulasikan lagi ke Netto actual. 
    20/07/2022 [DITA/PHT] perhitungan nilai baru brutto dan netto actual juga mengikutsertakan EeHealth, EeEmployment EePension, ErPension ketika looping while nya
    04/08/2022 [DITA/PHT] penambahan Netto setelah selesai kalkulasi BPJS
    09/08/2022 [DITA/PHT] penambahan Amt setelah selesai kalkulasi BPJS
    25/08/2022 [DITA/PHT] rubah validasi OR menjadi AND untuk validasi loop kalkulasi BPJS
    26/08/2022 [DITA/PHT] rubah validasi ketika netto untuk BPJS Kes & TK lebih kecil dari UMK, maka ambil dari UMK
    03/10/2022 [DITA/PHT] Pembulatan BPJS Otomsatis menggunakan rounding biasa, bukan truncate lagi
    03/01/2022 [WED/PHT] bug fix fixallowance pembulatan
    16/03/2023 [WED/PHT] bug fix selisih 1 rupiah
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPayrollProcessMultiPayrun18 : Form
    {
        #region Field, Property

        #region Multi Payrun

        private string
            mPayrunCode = string.Empty,
            mStartDt = string.Empty,
            mEndDt = string.Empty;

        #endregion

        private string
            mDeptCode = string.Empty,
            mSystemType = string.Empty,
            mPayrunPeriod = string.Empty,
            mPGCode = string.Empty,
            mSiteCode = string.Empty,
            mOTRequestType = "1",
            mEmpSystemTypeHarian = "1",
            mEmploymentStatusTetap = "1",
            mEmploymentStatusLepas = "2",
            mEmploymentStatusSpesial = "3",
            mPayrunPeriodBulanan = "1",
            mADCodeFieldAssignment = "02",
            mGoHomeEarlyLeaveCode = "017",
            mADCodeTransport = "03",
            mADCodeMeal = "05",
            mSSPCodeForHealth = "BPJS-KES",
            mSSPCodeForEmployment = "BPJS-TNK",
            mSSForRetiring = "Pens",
            mSSOldAgeInsurance = "JHT",
            mSalaryInd = "3",
            mWSCodesForNationalHoliday = string.Empty,
            mWSCodesForHoliday = string.Empty,
            mADCodeEmploymentPeriod = string.Empty,
            mADCodeSalary = string.Empty,
            mSSCodePension = string.Empty,
            mSSCodePension2 = string.Empty,
            mSSCodeHealth = string.Empty,
            mProratedADBasedOnLeave = string.Empty,
            mLeaveForProratedAD = string.Empty,
            mProratedAD = string.Empty,
            mSSYr = string.Empty,
            mSSMth = string.Empty,
            mLateLeaveCode = string.Empty,
            mADCodePresenceReward = string.Empty,
            mOTSpecialSource = string.Empty,
            mWSCodeForHoliday = string.Empty,
            mWSCodeForNationalHoliday =string.Empty,
            mListOfLevelCodeDontGetOTAmt = string.Empty,
            mEmploymentStatusEmployeeCandidate = string.Empty,
            mSSProgramBPJS = string.Empty,
            mADCodePositionAdjustment = string.Empty;

        List<string> mlProratedADBasedOnLeave = new List<string>();
        List<string> mlLeaveForProratedAD = new List<string>();
        
        private decimal
            mDirectLaborDailyWorkHour = 0m,
            mStdHolidayIndex = 2m,
            mWorkingHr = 7m,
            mNoWorkDay2Mingguan = 12,
            mNoWorkDayBulanan = 20,
            mFunctionalExpenses = 5m,
            mFunctionalExpensesMaxAmt = 500000m,
            mOTFormulaHr1 = 1m,
            mOTFormulaHr2 = 24m,
            mOTFormulaIndex1 = 1.5m,
            mOTFormulaIndex2 = 2m,
            mHOWorkingDayPerMth = 20m,
            mSiteWorkingDayPerMth = 21m,
            mHoliday1stHrForHO = 8m,
            mHoliday1stHrForSite = 7m,
            mHoliday2ndHrForHO = 1m,
            mHoliday2ndHrForSite = 1m,
            mHoliday3rdHrForHO = 1m,
            mHoliday4thHrForHO = 1m,
            mHoliday1stHrIndex = 2m,
            mHoliday2ndHrIndex = 3m,
            mHoliday3rdHrIndex = 4m,
            mHoliday4thHrIndex = 5m,
            mNonNPWPTaxPercentage = 120m,
            mADCodeEmploymentPeriodYr = 5m,
            //mSSEmploymentErPerc = 0m,
            //mSSOldAgeEePerc = 0m,
            //mSSPensionEePerc = 0m,
            mFieldAssignmentMinWorkDuration = 0m,
            mXMinToleranceLateMealTransportDeduction = 0m,
            mXPercentageLateMealTransportDeduction = 0m,
            mEmployerPercSSHealth = 0m,
            mEmployerPercSSLifeInsurance = 0m,
            mEmployerPercSSWorkingAccident = 0m,
            mEmployeePercSSOldAgeInsurance = 0m,
            mEmployeePercSSForRetiring = 0m,
            mSalaryMaxLimitSSHealth = 0m,
            mSalaryMaxLimitSSLifeInsurance = 0m,
            mSalaryMaxLimitSSWorkingAccident = 0m,
            mSalaryMaxLimitSSOldAgeInsurance = 0m,
            mSalaryMaxLimitSSForRetiring = 0m,
            mMinLeaveDaysForProratedAD = 12m,
            mEmployeeCandidateSalaryPercentage = 0m,
            mUMK = 0m,
            mUMPP = 0m,
            mOTHolidayMeal = 0m,
            mDeviation = 0m,
            mADCodePositionAdjustmentAmt = 0m
            ;

        private bool
            mIsFilterBySiteHR = false,
            mIsUseLateValidation = false,
            mIsUseOTReplaceLeave = false,
            mIsOTRoutineRounding = false,
            mIsOTBasedOnOTRequest = false,
            mIsWorkingHrBasedOnSchedule = false,
            mIsPayrollProcessUseSiteWorkingDayPerMth = false,
            mHOInd = true,
            mIsOTHolidayDeduct1Hr = false,
            mIsUseLateMealTransportDeduction = false,
            mIsPPProcessAllOutstandingSS = false,
            mIsWSDoubleShiftTwiceAllowance = false,
            mIsPayrunSSYrMthEnabled = false ,
            mIsPayrollAmtRounded = false,
            mIsPayrollProcessingCalculateBPJSAutomatic = false
            //,mIsPayrollUseTaxProgressif = false
            ;


        private List<PayrunEmployee> mlPayrunEmployee = null;
        private List<AttendanceLog> mlAttendanceLog = null;
        private List<OT> mlOT = null;
        private List<OTAdjustment> mlOTAdjustment = null;
        private List<AdvancePaymentProcess> mlAdvancePaymentProcess = null;
        private List<EmployeeMealTransport> mEmployeeMealTransport = null;
        private List<EmployeeDeductionAtd> mlEmployeeDeductionAtd = null;
        private List<OTSpecial> mlOTSpecial = null;
        private List<GradeSalary> mlGradeSalary = null;
     
        #endregion

        #region Constructor

        public FrmPayrollProcessMultiPayrun18(string MenuCode)
        {
            InitializeComponent();
            try
            {
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                GetParameter();
                SetGrd();

                mlAttendanceLog = new List<AttendanceLog>();
                mlOT = new List<OT>();
                mlOTAdjustment = new List<OTAdjustment>();
                mlAdvancePaymentProcess = new List<AdvancePaymentProcess>();
                mlEmployeeDeductionAtd = new List<EmployeeDeductionAtd>();
                mlOTSpecial = new List<OTSpecial>();
                mlGradeSalary = new List<GradeSalary>();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Method

        #region Setting

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsWSDoubleShiftTwiceAllowance', 'IsOTHolidayDeduct1Hr', 'IsPayrunSSYrMthEnabled', 'LateLeaveCode', 'SSProgramBPJS', ");
            SQL.AppendLine("'IsUseLateValidation', 'IsUseOTReplaceLeave', 'IsOTRoutineRounding', 'IsOTBasedOnOTRequest', 'IsUseLateMealTransportDeduction', ");
            SQL.AppendLine("'EmploymentStatusEmployeeCandidate', 'IsPayrollProcessUseSiteWorkingDayPerMth', 'ListOfLevelCodeDontGetOTAmt', 'IsPPProcessAllOutstandingSS', 'IsWorkingHrBasedOnSchedule', ");
            SQL.AppendLine("'ADCodePresenceReward', 'GoHomeEarlyLeaveCode', 'OTSpecialSource', 'WSCodeForHoliday', 'WSCodeForNationalHoliday', ");
            SQL.AppendLine("'ADCodeSalary', 'SSCodePension', 'SSCodePension2', 'ProratedADBasedOnLeave', 'LeaveForProratedAD', ");
            SQL.AppendLine("'WSCodesForNationalHoliday', 'OTRequestType', 'SalaryInd', 'ADCodeEmploymentPeriod', 'SSCodeHealth', ");
            SQL.AppendLine("'SSPCodeForEmployment', 'ADCodeTransport', 'ADCodeMeal', 'SSForRetiring', 'SSOldAgeInsurance', ");
            SQL.AppendLine("'EmpSystemTypeHarian', 'WSCodesForHoliday', 'PayrunPeriodBulanan', 'ADCodeFieldAssignment', 'SSPCodeForHealth', ");
            SQL.AppendLine("'UMPP', 'IsFilterBySiteHR', 'EmploymentStatusSpesial', 'EmploymentStatusLepas', 'EmploymentStatusTetap', ");
            SQL.AppendLine("'Holiday4thHrForHO', 'DirectLaborDailyWorkHour', 'Holiday4thHrIndex', 'EmployeeCandidateSalaryPercentage', 'OTHolidayMeal', ");
            SQL.AppendLine("'ProratedADBasedOnLeave', 'XPercentageLateMealTransportDeduction', 'MinLeaveDaysForProratedAD', 'WorkingHr', 'Holiday3rdHrForHO', ");
            SQL.AppendLine("'NoWorkDayBulanan', 'ADCodeEmploymentPeriodYr', 'FieldAssignmentMinWorkDuration', 'XMinToleranceLateMealTransportDeduction', 'NoWorkDay2Mingguan', ");
            SQL.AppendLine("'SiteWorkingDayPerMth', 'HOWorkingDayPerMth', 'FunctionalExpensesMaxAmt', 'FunctionalExpenses', 'NonNPWPTaxPercentage', ");
            SQL.AppendLine("'Holiday1stHrIndex', 'Holiday2ndHrForSite', 'Holiday2ndHrForHO', 'Holiday1stHrForSite', 'Holiday1stHrForHO', ");
            SQL.AppendLine("'Holiday2ndHrIndex', 'Holiday3rdHrIndex', 'ADCodePositionAdjustment', 'IsPayrollAmtRounded', 'IsPayrollProcessingCalculateBPJSAutomatic' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open(); 
                cm.Connection = cn; 
                cm.CommandText = SQL.ToString(); 
                var dr = cm.ExecuteReader(); 
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim(); 
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //decimal
                            case "Holiday3rdHrIndex": if (ParValue.Length > 0) mHoliday3rdHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrIndex": if (ParValue.Length > 0) mHoliday2ndHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday1stHrIndex": if (ParValue.Length > 0) mHoliday1stHrIndex = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrForSite": if (ParValue.Length > 0) mHoliday2ndHrForSite = decimal.Parse(ParValue); break;
                            case "Holiday2ndHrForHO": if (ParValue.Length > 0) mHoliday2ndHrForHO = decimal.Parse(ParValue); break;
                            case "Holiday1stHrForSite": if (ParValue.Length > 0) mHoliday1stHrForSite = decimal.Parse(ParValue); break;
                            case "Holiday1stHrForHO": if (ParValue.Length > 0) mHoliday1stHrForHO = decimal.Parse(ParValue); break;
                            case "SiteWorkingDayPerMth": if (ParValue.Length > 0) mSiteWorkingDayPerMth = decimal.Parse(ParValue); break;
                            case "HOWorkingDayPerMth": if (ParValue.Length > 0) mHOWorkingDayPerMth = decimal.Parse(ParValue); break;
                            case "FunctionalExpensesMaxAmt": if (ParValue.Length > 0) mFunctionalExpensesMaxAmt = decimal.Parse(ParValue); break;
                            case "FunctionalExpenses": if (ParValue.Length > 0) mFunctionalExpenses = decimal.Parse(ParValue); break;
                            case "NonNPWPTaxPercentage": if (ParValue.Length > 0) mNonNPWPTaxPercentage = decimal.Parse(ParValue); break;
                            case "NoWorkDayBulanan": if (ParValue.Length > 0) mNoWorkDayBulanan = decimal.Parse(ParValue); break;
                            case "ADCodeEmploymentPeriodYr": if (ParValue.Length > 0) mADCodeEmploymentPeriodYr = decimal.Parse(ParValue); break;
                            case "FieldAssignmentMinWorkDuration": if (ParValue.Length>0) mFieldAssignmentMinWorkDuration = decimal.Parse(ParValue); break;
                            case "XMinToleranceLateMealTransportDeduction": if (ParValue.Length > 0) mXMinToleranceLateMealTransportDeduction = decimal.Parse(ParValue); break;
                            case "NoWorkDay2Mingguan": if (ParValue.Length > 0) mNoWorkDay2Mingguan = decimal.Parse(ParValue); break;
                            case "XPercentageLateMealTransportDeduction": if (ParValue.Length > 0) mXPercentageLateMealTransportDeduction = decimal.Parse(ParValue); break;
                            case "MinLeaveDaysForProratedAD": if (ParValue.Length > 0) mMinLeaveDaysForProratedAD = decimal.Parse(ParValue); break;
                            case "WorkingHr": if (ParValue.Length > 0) mWorkingHr = decimal.Parse(ParValue); break;
                            case "Holiday3rdHrForHO": if (ParValue.Length > 0) mHoliday3rdHrForHO = decimal.Parse(ParValue); break;
                            case "Holiday4thHrForHO": if (ParValue.Length > 0) mHoliday4thHrForHO = decimal.Parse(ParValue); break;
                            case "DirectLaborDailyWorkHour": if (ParValue.Length > 0) mDirectLaborDailyWorkHour = decimal.Parse(ParValue); break;
                            case "Holiday4thHrIndex": if (ParValue.Length > 0) mHoliday4thHrIndex = decimal.Parse(ParValue); break;
                            case "EmployeeCandidateSalaryPercentage": if (ParValue.Length > 0) mEmployeeCandidateSalaryPercentage = decimal.Parse(ParValue); break;
                            case "OTHolidayMeal": if (ParValue.Length > 0) mOTHolidayMeal = decimal.Parse(ParValue); break;
                            case "UMPP": if (ParValue.Length > 0) mUMPP = decimal.Parse(ParValue); break;

                            //string
                            case "ProratedADBasedOnLeave": mProratedADBasedOnLeave = ParValue; break;
                            case "EmploymentStatusSpesial": mEmploymentStatusSpesial = ParValue; break;
                            case "EmploymentStatusLepas": mEmploymentStatusLepas = ParValue; break;
                            case "EmploymentStatusTetap": mEmploymentStatusTetap = ParValue; break;
                            case "EmpSystemTypeHarian": mEmpSystemTypeHarian = ParValue; break;
                            case "WSCodesForHoliday": mWSCodesForHoliday = ParValue; break;
                            case "PayrunPeriodBulanan": mPayrunPeriodBulanan = ParValue; break;
                            case "ADCodeFieldAssignment": mADCodeFieldAssignment = ParValue; break;
                            case "SSPCodeForHealth": mSSPCodeForHealth = ParValue; break;
                            case "SSPCodeForEmployment": mSSPCodeForEmployment = ParValue; break;
                            case "ADCodeTransport": mADCodeTransport = ParValue; break;
                            case "ADCodeMeal": mADCodeMeal = ParValue; break;
                            case "SSForRetiring": mSSForRetiring = ParValue; break;
                            case "SSOldAgeInsurance": mSSOldAgeInsurance = ParValue; break;
                            case "WSCodesForNationalHoliday": mWSCodesForNationalHoliday = ParValue; break;
                            case "OTRequestType": mOTRequestType = ParValue; break;
                            case "SalaryInd": mSalaryInd = ParValue; break;
                            case "ADCodeEmploymentPeriod": mADCodeEmploymentPeriod = ParValue; break;
                            case "SSCodeHealth": mSSCodeHealth = ParValue; break;
                            case "ADCodeSalary": mADCodeSalary = ParValue; break;
                            case "SSCodePension": mSSCodePension = ParValue; break;
                            case "SSCodePension2": mSSCodePension2 = ParValue; break;
                            case "LeaveForProratedAD": mLeaveForProratedAD = ParValue; break;
                            case "ADCodePresenceReward": mADCodePresenceReward = ParValue; break;
                            case "GoHomeEarlyLeaveCode": mGoHomeEarlyLeaveCode = ParValue; break;
                            case "OTSpecialSource": mOTSpecialSource = ParValue; break;
                            case "WSCodeForHoliday": mWSCodeForHoliday = ParValue; break;
                            case "WSCodeForNationalHoliday": mWSCodeForNationalHoliday = ParValue; break;
                            case "EmploymentStatusEmployeeCandidate": mEmploymentStatusEmployeeCandidate = ParValue; break;
                            case "ListOfLevelCodeDontGetOTAmt": mListOfLevelCodeDontGetOTAmt = ParValue; break;
                            case "LateLeaveCode": mLateLeaveCode = ParValue; break;
                            case "SSProgramBPJS": mSSProgramBPJS = ParValue; break;
                            case "ADCodePositionAdjustment":
                                mADCodePositionAdjustment = ParValue;
                                //if (mADCodePositionAdjustment.Length == 0) mADCodePositionAdjustment = "AL015";
                                break;

                            //boolean
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsPayrollProcessUseSiteWorkingDayPerMth": mIsPayrollProcessUseSiteWorkingDayPerMth = ParValue == "Y"; break;
                            case "IsPPProcessAllOutstandingSS": mIsPPProcessAllOutstandingSS = ParValue == "Y"; break;
                            case "IsWorkingHrBasedOnSchedule": mIsWorkingHrBasedOnSchedule = ParValue == "Y"; break;
                            case "IsUseLateValidation": mIsUseLateValidation = ParValue == "Y"; break;
                            case "IsUseOTReplaceLeave": mIsUseOTReplaceLeave = ParValue == "Y"; break;
                            case "IsOTRoutineRounding": mIsOTRoutineRounding = ParValue == "Y"; break;
                            case "IsOTBasedOnOTRequest": mIsOTBasedOnOTRequest = ParValue == "Y"; break;
                            case "IsUseLateMealTransportDeduction": mIsUseLateMealTransportDeduction = ParValue == "Y"; break;
                            case "IsOTHolidayDeduct1Hr": mIsOTHolidayDeduct1Hr = ParValue == "Y"; break;
                            case "IsWSDoubleShiftTwiceAllowance": mIsWSDoubleShiftTwiceAllowance = ParValue == "Y"; break;
                            case "IsPayrunSSYrMthEnabled": mIsPayrunSSYrMthEnabled = ParValue == "Y"; break;
                            case "IsPayrollAmtRounded": mIsPayrollAmtRounded = ParValue == "Y"; break;
                            case "IsPayrollProcessingCalculateBPJSAutomatic": mIsPayrollProcessingCalculateBPJSAutomatic = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mProratedADBasedOnLeave.Length > 0)
            {
                foreach (var x in mProratedADBasedOnLeave.Split(','))
                    mlProratedADBasedOnLeave.Add(x);

                foreach (var x in mProratedADBasedOnLeave.Split(','))
                {
                    if (mProratedAD.Length > 0) mProratedAD += ",";
                    mProratedAD += string.Concat("'", x.Trim(), "'");
                }
            }
            if (mLeaveForProratedAD.Length > 0)
            {
                foreach (var x in mLeaveForProratedAD.Split(','))
                    mlLeaveForProratedAD.Add(x);
            }
        }

        private void GetValue()
        {
            var Value = string.Empty;            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmployerPerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSCodeHealth' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployerPercSSHealth = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployerPerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSLifeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployerPercSSLifeInsurance = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployerPerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSWorkingAccident' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployerPercSSWorkingAccident = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSCodeHealth' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSHealth = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSLifeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSLifeInsurance = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSWorkingAccident' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSWorkingAccident = decimal.Parse(Value);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployeePerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSOldAgeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployeePercSSOldAgeInsurance = decimal.Parse(Value);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select EmployeePerc From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSForRetiring' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mEmployeePercSSForRetiring = decimal.Parse(Value);
            
            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSOldAgeInsurance' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSOldAgeInsurance = decimal.Parse(Value);

            SQL.Length = 0;
            SQL.Capacity = 0;

            SQL.AppendLine("Select SalaryMaxLimit From TblSS ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And SSCode In (Select ParValue From TblParameter Where ParCode='SSForRetiring' And ParValue is Not Null);");

            Value = Sm.GetValue(SQL.ToString());
            if (Value.Length > 0) mSalaryMaxLimitSSForRetiring = decimal.Parse(Value);

        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "",

                        //1-5
                        "Payrun"+Environment.NewLine+"Code",
                        "Payrun"+Environment.NewLine+"Name",
                        "Completed",
                        "Start",
                        "End",

                        //6-10
                        "Site Code",
                        "Site",
                        "Department Code",
                        "Department",
                        "System Type Code",

                        //11-15
                        "Type",
                        "Payrun Period Code",
                        "Period",
                        "Group Code",
                        "Group",

                        //16-17
                        "Head Office",
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        20, 

                        //1-5
                        100, 250, 80, 80, 80, 
                        
                        //6-10
                        0, 200, 0, 200, 0,

                        //11-15
                        150, 0, 150, 0, 150,

                        //16-17
                        0, 300
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 0, 3, 16 });
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 10, 11, 12, 13, 14 }, false);
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 11, 13 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 0 });
        }

        //private void SetLuePayrunCode(ref DXE.LookUpEdit Lue)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Distinct PayrunCode As Col1, Concat(PayrunCode, ' : ', PayrunName) As Col2 ");
        //    SQL.AppendLine("From TblPayrun ");
        //    SQL.AppendLine("Where CancelInd='N' ");
        //    SQL.AppendLine("And IfNull(Status, 'O')='O' ");
        //    SQL.AppendLine("Order By Left(PayrunCode, 6) Desc, PayrunName, StartDt;");
        //    Sm.SetLue2(ref Lue, SQL.ToString(), 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        #endregion

        #region Show Data

        private void ShowPayrunInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.PayrunCode, A.PayrunName, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.SiteCode, C.SiteName, A.DeptCode, B.DeptName, ");
            SQL.AppendLine("A.SystemType, D.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("A.PayrunPeriod, E.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("A.PGCode, F.PGName, C.HOInd ");
            SQL.AppendLine("From TblPayrun A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblSite C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("Left Join TblOption D On A.SystemType=D.OptCode And D.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblOption E On A.PayrunPeriod=E.OptCode And E.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr F On A.PGCode=F.PGCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.Status='O' ");
            SQL.AppendLine("And Left(A.PayrunCode, 6)=@YrMth ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (ChkSiteCode.Checked)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null And A.SiteCode=@SiteCode ");
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            }
            SQL.AppendLine("Order By A.PayrunCode;");

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
            
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                     //0
                    "PayrunCode", 

                    //1-5
                    "PayrunName", "StartDt", "EndDt", "SiteCode", "SiteName", 
                    
                    //6-10
                    "DeptCode", "DeptName", "SystemType", "SystemTypeDesc", "PayrunPeriod", 
                    
                    //11-14
                    "PayrunPeriodDesc", "PGCode", "PGName", "HOInd"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = true;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Grd.Cells[Row, 3].Value = false;
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 16, 14);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 0, 3, 16 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Check Data

        private bool IsProcessedDataNotValid()
        {
            return
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "List of payrun is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            bool IsExisted = false;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 0))
                {
                    IsExisted = true;
                    break;
                }
            }
            if (!IsExisted)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 payrun.");
                return true;
            }
            return false;
        }

        #endregion

        #region Save Data

        private MySqlCommand DeletePayrollProcess()
        {
            string EmpCode = string.Empty
                //Filter = string.Empty
                ;
            var cm = new MySqlCommand();

            //for (int i = 0; i < mlPayrunEmployee.Count; i++)
            //{                
            //    if (Filter.Length > 0) Filter += " Or ";
            //    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
            //    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), mlPayrunEmployee[i].EmpCode);   
            //}
            
            //if (Filter.Length > 0)
            //    Filter = " And (" + Filter + ") ";
            //else
            //    Filter = " And 0=1 ";
            

            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAdvancePaymentProcess Where PayrunCode=@PayrunCode;");
            SQL.AppendLine("Delete From TblPayrollProcess1 Where PayrunCode=@PayrunCode;") ;
            SQL.AppendLine("Delete From TblPayrollProcess2 Where PayrunCode=@PayrunCode;");
            SQL.AppendLine("Delete From TblPayrollProcessAD Where PayrunCode=@PayrunCode; ");
            //SQL.AppendLine("Delete From TblPayrollProcessADOT Where IfNull(PayrunCode, '')=@PayrunCode; ");
            SQL.AppendLine("Delete From TblPayrollProcessSSProgram Where PayrunCode=@PayrunCode; ");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode;");

            //SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=Null ");
            //SQL.AppendLine("Where PayrunCode Is Not Null And PayrunCode=@PayrunCode ");
            //SQL.AppendLine("And DocNo In ( ");
            //SQL.AppendLine("    Select A.DocNo ");
            //SQL.AppendLine("    From TblEmpInsPntHdr A ");
            //SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
            //SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
            //SQL.AppendLine("    Where A.CancelInd='N' ");
            //SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            //SQL.AppendLine(");");

            //SQL.AppendLine("Update TblEmpSCIDtl A ");
            //SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
            //SQL.AppendLine("    On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    And B.CancelInd='N' ");
            //SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
            //SQL.AppendLine("Set A.PayrunCode=Null ");
            //SQL.AppendLine("Where A.PayrunCode Is Not Null ");
            //SQL.AppendLine("And A.PayrunCode=@PayrunCode; ");

            //SQL.AppendLine("Update TblAnnualLeaveAllowanceDtl A ");
            //SQL.AppendLine("Inner Join TblAnnualLeaveAllowanceHdr B ");
            //SQL.AppendLine("    On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    And B.CancelInd='N' ");
            //SQL.AppendLine("    And B.Status='A' ");
            //SQL.AppendLine("Set A.PayrunCode=Null ");
            //SQL.AppendLine("Where A.PayrunCode Is Not Null ");
            //SQL.AppendLine("And A.PayrunCode=@PayrunCode; ");
         
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            return cm;
        }

        private MySqlCommand SavePayrollProcess()
        {
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;
            var cm = new MySqlCommand();

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {                
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), mlPayrunEmployee[i].EmpCode);   
            }
            
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=Null ");
            SQL.AppendLine("Where T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='Y' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblEmpSSListDtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmpSSListHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocNo=T.DocNo ");
            if (!mIsPPProcessAllOutstandingSS)
            {
                SQL.AppendLine("    And Yr=Left(@PayrunCode, 4) ");
                SQL.AppendLine("    And Mth=Substring(@PayrunCode, 5, 2) ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustmentHdr T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) ");
            SQL.AppendLine("    Or T.PayrunCode Is Null ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And T.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblSalaryAdjustment2Dtl T Set T.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Where ((T.PayrunCode Is Not Null And T.PayrunCode=@PayrunCode) Or T.PayrunCode Is Null) ");
            SQL.AppendLine("And T.Docno In (");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblSalaryAdjustment2Hdr ");
            SQL.AppendLine("    Where PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("    And CancelInd='N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            //SQL.AppendLine("Update TblEmpInsPntDtl Set PayrunCode=@PayrunCode ");
            //SQL.AppendLine("Where ( ");
            //SQL.AppendLine("    (PayrunCode Is Not Null And PayrunCode=@PayrunCode) ");
            //SQL.AppendLine("    Or PayrunCode Is Null ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine("And DocNo In ( ");
            //SQL.AppendLine("    Select A.DocNo ");
            //SQL.AppendLine("    From TblEmpInsPntHdr A ");
            //SQL.AppendLine("    Inner Join TblInsPnt B On A.InsPntCode=B.InsPntCode ");
            //SQL.AppendLine("    Inner Join TblInsPntCategory C On B.InspntCtCode=C.InspntCtCode ");
            //SQL.AppendLine("    Where A.CancelInd='N' ");
            //SQL.AppendLine("    And A.DocDt Between @StartDt And @EndDt ");
            //SQL.AppendLine(") ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine(";");

            //SQL.AppendLine("Update TblEmpSCIDtl A ");
            //SQL.AppendLine("Inner Join TblEmpSCIHdr B ");
            //SQL.AppendLine("    On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    And B.CancelInd='N' ");
            //SQL.AppendLine("    And B.DocDt Between @StartDt And @EndDt ");
            //SQL.AppendLine("Set A.PayrunCode=@PayrunCode ");
            //SQL.AppendLine("Where (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine(";");

            //SQL.AppendLine("Update TblAnnualLeaveAllowanceDtl A ");
            //SQL.AppendLine("Inner Join TblAnnualLeaveAllowanceHdr B ");
            //SQL.AppendLine("    On A.DocNo=B.DocNo ");
            //SQL.AppendLine("    And B.CancelInd='N' ");
            //SQL.AppendLine("    And B.Status='A' ");
            //SQL.AppendLine("Set A.PayrunCode=@PayrunCode ");
            //SQL.AppendLine("Where (A.PayrunCode Is Null Or (A.PayrunCode Is Not Null And A.PayrunCode=@PayrunCode)) ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePayrollProcess1(ref Result2 r, ref List<PayrollProcessAD> lPayrollProcessAD, ref List<Result1> lResult1)
        { 
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var EmpCode = r.EmpCode;
            int i = 0;
            string Query = string.Empty;
            bool IsFirst = true;

            SQL.AppendLine("/* Payroll Process 1 */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblPayrollProcess1 ");
            SQL.AppendLine("(PayrunCode, EmpCode, JoinDt, ResignDt, PensionInd, NPWP, ");
            SQL.AppendLine("PTKP, Salary, WorkingDay, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
            SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, ");
            SQL.AppendLine("OT1Amt, OT2Amt, OTHolidayAmt, OTMeal, NonTaxableFixAllowance, TaxableFixAllowance, FixAllowance, EmploymentPeriodAllowance, ");
            SQL.AppendLine("IncEmployee, IncMinWages, IncProduction, IncPerformance, PresenceReward, ");
            SQL.AppendLine("HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, Functional, Housing, MobileCredit, Zakat, ServiceChargeIncentive, SSEmployerHealth, SSEmployerEmployment, SSEmployerPension, SSEmployerPension2, NonTaxableFixDeduction, TaxableFixDeduction, FixDeduction, ");
            SQL.AppendLine("DedEmployee, DedProduction, DedProdLeave, EmpAdvancePayment, SSEmployeeHealth, SSEmployeeEmployment, SSEmployeePension, SSEmployeePension2, ");
            SQL.AppendLine("SalaryPension, SSErLifeInsurance, SSEeLifeInsurance, SSErWorkingAccident, SSEeWorkingAccident, SSErRetirement, SSEeRetirement, SSErPension, SSEePension, ");
            SQL.AppendLine("CreditCode1, CreditCode2, CreditCode3, CreditCode4, CreditCode5, CreditCode6, CreditCode7, CreditCode8, CreditCode9, CreditCode10, ");
            SQL.AppendLine("CreditAdvancePayment1, CreditAdvancePayment2, CreditAdvancePayment3, CreditAdvancePayment4, CreditAdvancePayment5, CreditAdvancePayment6, CreditAdvancePayment7, CreditAdvancePayment8, CreditAdvancePayment9, CreditAdvancePayment10, ");
            SQL.AppendLine("SSEmployerDPNPHT, SSEmployeeDPNPHT, ");
            SQL.AppendLine("SSEmployerDPNPNS, SSEmployeeDPNPNS, ");
            SQL.AppendLine("SSEmployerPKES, SSEmployeePKES, ");
            SQL.AppendLine("SSEmployerPRMH, SSEmployeePRMH, ");
            SQL.AppendLine("SSEmployerTSP, SSEmployeeTSP, ");
            SQL.AppendLine("SSEmployerWNR, SSEmployeeWNR, ");
            SQL.AppendLine("ADLeave, SalaryAdjustment, Tax, TaxAllowance, WarningLetter, ServiceYearAllowance, PassAwayInd, Amt, DedAtd, ADOT, SSEmployerBNILife, SSEmployeeBNILife, SSEmployerNonBPJS, SSEmployeeNonBPJS, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PayrunCode, @EmpCode, @JoinDt, @ResignDt, @PensionInd, @NPWP, ");
            SQL.AppendLine("@PTKP, @Salary, @WorkingDay, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
	        SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, ");
	        SQL.AppendLine("@OT1Amt, @OT2Amt, @OTHolidayAmt, @OTMeal, @NonTaxableFixAllowance, @TaxableFixAllowance, @FixAllowance, @EmploymentPeriodAllowance, ");
	        SQL.AppendLine("@IncEmployee, @IncMinWages, @IncProduction, @IncPerformance, @PresenceReward, ");
            SQL.AppendLine("@HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @Functional, @Housing, @MobileCredit, @Zakat, @ServiceChargeIncentive, @SSEmployerHealth, @SSEmployerEmployment, @SSEmployerPension, @SSEmployerPension2, @NonTaxableFixDeduction, @TaxableFixDeduction, @FixDeduction, ");
            SQL.AppendLine("@DedEmployee, @DedProduction, @DedProdLeave, @EmpAdvancePayment, @SSEmployeeHealth, @SSEmployeeEmployment, @SSEmployeePension, @SSEmployeePension2, ");
            SQL.AppendLine("@SalaryPension, @SSErLifeInsurance, @SSEeLifeInsurance, @SSErWorkingAccident, @SSEeWorkingAccident, @SSErRetirement, @SSEeRetirement, @SSErPension, @SSEePension, ");
            SQL.AppendLine("@CreditCode1, @CreditCode2, @CreditCode3, @CreditCode4, @CreditCode5, @CreditCode6, @CreditCode7, @CreditCode8, @CreditCode9, @CreditCode10, ");
            SQL.AppendLine("@CreditAdvancePayment1, @CreditAdvancePayment2, @CreditAdvancePayment3, @CreditAdvancePayment4, @CreditAdvancePayment5, @CreditAdvancePayment6, @CreditAdvancePayment7, @CreditAdvancePayment8, @CreditAdvancePayment9, @CreditAdvancePayment10, ");
            SQL.AppendLine("@SSEmployerDPNPHT, @SSEmployeeDPNPHT, ");
            SQL.AppendLine("@SSEmployerDPNPNS, @SSEmployeeDPNPNS, ");
            SQL.AppendLine("@SSEmployerPKES, @SSEmployeePKES, ");
            SQL.AppendLine("@SSEmployerPRMH, @SSEmployeePRMH, ");
            SQL.AppendLine("@SSEmployerTSP, @SSEmployeeTSP, ");
            SQL.AppendLine("@SSEmployerWNR, @SSEmployeeWNR, ");
            SQL.AppendLine("@ADLeave, @SalaryAdjustment, @Tax, @TaxAllowance, @WarningLetter, @ServiceYearAllowance, @PassAwayInd, @Amt, @DedAtd, @ADOT, @SSEmployerBNILife, @SSEmployeeBNILife, @SSEmployerNonBPJS, @SSEmployeeNonBPJS, @CreateBy, @Dt); ");

            foreach (var x in lPayrollProcessAD.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
            {
                if (IsFirst)
                {
                    SQL.AppendLine("Insert Into TblPayrollProcessAD ");
                    SQL.AppendLine("(PayrunCode, EmpCode, ADCode, Amt, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");

                    IsFirst = false;
                }
                else
                    Query += ", ";
                Query += "(@PayrunCode, @EmpCode, @ADCode" + i.ToString() + ", @Amt" + i.ToString() + ", @CreateBy, @Dt)";

                Sm.CmParam<String>(ref cm, "@ADCode" + i.ToString(), x.ADCode);

                //yg meninggal dunia tidak dibulatkan saat save ke PayrollProcessAD, karena menyebabkan perhitungan truncate nya bermasalah di belakang2 nya -> by wed
                if (x.ADCode == "AL017" || x.ADCode == "AL018" || x.ADCode == "AL019" || x.ADCode == "AL020" || x.ADCode == "AL021" || x.ADCode == "AL022" || x.ADCode == "AL023")
                    Sm.CmParam<Decimal>(ref cm, "@Amt" + i.ToString(), (x.ADType=="A"?r._RecomputedValueForFixedAD:1m) * x.Amt);
                else
                    Sm.CmParam<Decimal>(ref cm, "@Amt" + i.ToString(), mIsPayrollAmtRounded ? Math.Round((x.ADType == "A" ? r._RecomputedValueForFixedAD : 1m) * x.Amt) : (x.ADType == "A" ? r._RecomputedValueForFixedAD : 1m) * x.Amt);

                i++;
            }

            if (Query.Length > 0)
            {
                SQL.AppendLine(Query);
                SQL.AppendLine(";");
            }

            #region Old Code

            //SQL.AppendLine("Select @PayrunCode, @EmpCode, A.ADCode, @RecomputedValueForFixedAD*Sum(Amt) As Amt, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            //SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.AmtType='1' And B.ADType='A' ");
            //SQL.AppendLine("Where ( ");
            //SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            //SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            //SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            //SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("And A.EmpCode=@EmpCode ");
            //SQL.AppendLine("Group BY A.ADCode ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select @PayrunCode, @EmpCode, A.ADCode, Sum(Amt) As Amt, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            //SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.AmtType='1' And B.ADType='D' ");
            //SQL.AppendLine("Where ( ");
            //SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            //SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            //SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            //SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("And A.EmpCode=@EmpCode ");
            //SQL.AppendLine("Group BY A.ADCode; ");

            //SQL.AppendLine("Select @PayrunCode, @EmpCode, A.ADCode, ");
            //SQL.AppendLine("Case When B.ADType='A' Then @RecomputedValueForFixedAD Else 1.00 End *Amt As Amt, ");
            //SQL.AppendLine("@CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            //SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.AmtType='1' And B.ADType In ('A', 'D') ");
            //SQL.AppendLine("Where ( ");
            //SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            //SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            //SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            //SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("And A.EmpCode=@EmpCode; ");

            #endregion

            SQL.AppendLine("Insert Into TblPayrollProcessSSProgram ");
            SQL.AppendLine("(PayrunCode, EmpCode, SSPCode, ErAmt, EeAmt, BPJSInd, CreateBy, CreateDt) ");

            #region Old Code

            //SQL.AppendLine("Select @PayrunCode, @EmpCode, T1.SSPCode, ");
            //SQL.AppendLine("Sum(T2.EmployerAmt) As ErAmt, ");
            //SQL.AppendLine("Sum(T2.EmployeeAmt) As EeAmt, ");
            //SQL.AppendLine("'Y' As BPJSInd, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblEmpSSListHdr T1 ");
            //SQL.AppendLine("Inner Join TblEmpSSListDtl T2 ");
            //SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            //SQL.AppendLine("    And T2.EmpCode=@EmpCode ");
            //SQL.AppendLine("Where T1.CancelInd='N' ");
            //SQL.AppendLine("And Find_In_Set( ");
            //SQL.AppendLine("    IfNull(T1.SSPCode, ''), ");
            //SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSProgramBPJS' And ParValue Is Not Null), '') ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("And T1.SiteCode In ( ");
            //SQL.AppendLine("    Select SiteCode From TblPayrun ");
            //SQL.AppendLine("    Where PayrunCode=@PayrunCode ");
            //SQL.AppendLine("    And SiteCode Is Not Null ");
            //SQL.AppendLine("    ) ");
            //if (mIsPayrunSSYrMthEnabled)
            //{
            //    SQL.AppendLine("    And T1.Yr=@SSYr ");
            //    SQL.AppendLine("    And T1.Mth=@SSMth ");
            //}
            //else
            //{
            //    if (!mIsPPProcessAllOutstandingSS)
            //    {
            //        SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
            //        SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            //    }
            //}
            //SQL.AppendLine("Group By T1.SSPCode ");
            //SQL.AppendLine("Union All ");
            //SQL.AppendLine("Select @PayrunCode, @EmpCode, T1.SSPCode, ");
            //SQL.AppendLine("Sum(T2.EmployerAmt) As ErAmt, ");
            //SQL.AppendLine("Sum(T2.EmployeeAmt) As EeAmt, ");
            //SQL.AppendLine("'N' As BPJSInd, @CreateBy, CurrentDateTime() ");
            //SQL.AppendLine("From TblEmpSSListHdr T1 ");
            //SQL.AppendLine("Inner Join TblEmpSSListDtl T2 ");
            //SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            //SQL.AppendLine("    And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            //SQL.AppendLine("    And T2.EmpCode=@EmpCode ");
            //SQL.AppendLine("Where T1.CancelInd='N' ");
            //SQL.AppendLine("And Not Find_In_Set( ");
            //SQL.AppendLine("    IfNull(T1.SSPCode, ''), ");
            //SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSProgramBPJS' And ParValue Is Not Null), '') ");
            //SQL.AppendLine("    ) ");
            //SQL.AppendLine("And T1.SiteCode In ( ");
            //SQL.AppendLine("    Select SiteCode From TblPayrun ");
            //SQL.AppendLine("    Where PayrunCode=@PayrunCode ");
            //SQL.AppendLine("    And SiteCode Is Not Null ");
            //SQL.AppendLine("    ) ");
            //if (mIsPayrunSSYrMthEnabled)
            //{
            //    SQL.AppendLine("    And T1.Yr=@SSYr ");
            //    SQL.AppendLine("    And T1.Mth=@SSMth ");
            //}
            //else
            //{
            //    if (!mIsPPProcessAllOutstandingSS)
            //    {
            //        SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
            //        SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
            //    }
            //}
            //SQL.AppendLine("    Group By T1.SSPCode; ");

            #endregion

            SQL.AppendLine("Select @PayrunCode, @EmpCode, T1.SSPCode, ");
            SQL.AppendLine("Sum(T2.EmployerAmt) As ErAmt, ");
            SQL.AppendLine("Sum(T2.EmployeeAmt) As EeAmt, ");
            SQL.AppendLine("Case When Find_In_Set(IfNull(T1.SSPCode, ''), IfNull(@SSProgramBPJS, '')) Then 'Y' Else 'N' End As BPJSInd, ");
            SQL.AppendLine("@CreateBy, @Dt ");
            SQL.AppendLine("From TblEmpSSListHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("    And T2.EmpCode=@EmpCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.SiteCode=@SiteCode ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("Group By T1.SSPCode; ");

            //lResult1.ForEach(r => { cml.Add(SavePayrollProcess2(ref r)); });

            IsFirst = true;
            i = 0;
            foreach (var x in lResult1.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
            {
                if (IsFirst)
                {
                    SQL.AppendLine("Insert Into TblPayrollProcess2 ");
                    SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
                    SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
                    SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
                    SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
                    SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
                    SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
                    SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
                    SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
                    SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
                    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
                    SQL.AppendLine("OT2Amt, OTHolidayAmt, OTMeal, OTMonthlyIncome, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
                    SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
                    SQL.AppendLine("DedProdLeave,  ADOT,  DedAtd, OTSpecialind, CreateBy, CreateDt) ");

                    SQL.AppendLine("Values");
                    IsFirst = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine("(@PayrunCode, @EmpCode, ");
                SQL.AppendLine("@Dt_" + i.ToString() + ", @ProcessInd_" + i.ToString() + ", @LatestPaidDt_" + i.ToString() + ", ");
                SQL.AppendLine("@JoinDt_" + i.ToString() + ", @ResignDt_" + i.ToString() + ", @PosCode_" + i.ToString() + ", @DeptCode_" + i.ToString() + ", @GrdLvlCode_" + i.ToString() + ", @LevelCode_" + i.ToString() + ", ");
                SQL.AppendLine("@SystemType_" + i.ToString() + ", @EmploymentStatus_" + i.ToString() + ", @PayrunPeriod_" + i.ToString() + ", @PGCode_" + i.ToString() + ", @SiteCode_" + i.ToString() + ", @WorkingDay_" + i.ToString() + ", ");
                SQL.AppendLine("@WSCode_" + i.ToString() + ", @HolInd_" + i.ToString() + ", @HolidayIndex_" + i.ToString() + ", @WSHolidayInd_" + i.ToString() + ", @WSIn1_" + i.ToString() + ", ");
                SQL.AppendLine("@WSOut1_" + i.ToString() + ", @WSIn2_" + i.ToString() + ", @WSOut2_" + i.ToString() + ", @WSIn3_" + i.ToString() + ", @WSOut3_" + i.ToString() + ", ");
                SQL.AppendLine("@OneDayInd_" + i.ToString() + ", @LateInd_" + i.ToString() + ", @ActualIn_" + i.ToString() + ", @ActualOut_" + i.ToString() + ", @WorkingIn_" + i.ToString() + ", ");
                SQL.AppendLine("@WorkingOut_" + i.ToString() + ", @WorkingDuration_" + i.ToString() + ", @EmpSalary_" + i.ToString() + ", @EmpSalary2_" + i.ToString() + ", @BasicSalary_" + i.ToString() + ", @BasicSalary2_" + i.ToString() + ", @SalaryPension_" + i.ToString() + ", @ProductionWages_" + i.ToString() + ", ");
                SQL.AppendLine("@Salary_" + i.ToString() + ", @LeaveCode_" + i.ToString() + ", @LeaveType_" + i.ToString() + ", @PaidLeaveInd_" + i.ToString() + ", @LeaveStartTm_" + i.ToString() + ",");
                SQL.AppendLine("@LeaveEndTm_" + i.ToString() + ", @LeaveDuration_" + i.ToString() + ", @PLDay_" + i.ToString() + ", @PLHr_" + i.ToString() + ", @PLAmt_" + i.ToString() + ", @ProcessPLAmt_" + i.ToString() + ", @UPLDay_" + i.ToString() + ", @UPLHr_" + i.ToString() + ", @UPLAmt_" + i.ToString() + ", @ProcessUPLAmt_" + i.ToString() + ", ");
                SQL.AppendLine("@OT1Hr_" + i.ToString() + ", @OT2Hr_" + i.ToString() + ", @OTHolidayHr_" + i.ToString() + ", @OT1Amt_" + i.ToString() + ", ");
                SQL.AppendLine("@OT2Amt_" + i.ToString() + ", @OTHolidayAmt_" + i.ToString() + ", @OTMeal_" + i.ToString() + ", @OTMonthlyIncome_" + i.ToString() + ", @OTToLeaveInd_" + i.ToString() + ", @IncMinWages_" + i.ToString() + ", @IncProduction_" + i.ToString() + ", @IncEmployee_" + i.ToString() + ", ");
                SQL.AppendLine("@IncPerformance_" + i.ToString() + ", @PresenceRewardInd_" + i.ToString() + ", @HolidayEarning_" + i.ToString() + ", @ExtraFooding_" + i.ToString() + ", @FieldAssignment_" + i.ToString() + ", @Transport_" + i.ToString() + ", @Meal_" + i.ToString() + ", @ServiceChargeIncentive_" + i.ToString() + ", @DedProduction_" + i.ToString() + ", ");
                SQL.AppendLine("@DedProdLeave_" + i.ToString() + ",  @ADOT_" + i.ToString() + ", @DedAtd_" + i.ToString() + ", @OTSpecialind_" + i.ToString() + ", ");
                SQL.AppendLine("@CreateBy, @Dt) ");

                Sm.CmParam<String>(ref cm, "@Dt_"+i.ToString(), x.Dt);
                Sm.CmParam<String>(ref cm, "@ProcessInd_" + i.ToString(), x.ProcessInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LatestPaidDt_" + i.ToString(), x.LatestPaidDt);
                Sm.CmParam<String>(ref cm, "@JoinDt_" + i.ToString(), x.JoinDt);
                Sm.CmParam<String>(ref cm, "@ResignDt_" + i.ToString(), x.ResignDt);
                Sm.CmParam<String>(ref cm, "@PosCode_" + i.ToString(), x.PosCode);
                Sm.CmParam<String>(ref cm, "@DeptCode_" + i.ToString(), x.DeptCode);
                Sm.CmParam<String>(ref cm, "@GrdLvlCode_" + i.ToString(), x.GrdLvlCode);
                Sm.CmParam<String>(ref cm, "@LevelCode_" + i.ToString(), x.LevelCode);
                Sm.CmParam<String>(ref cm, "@SystemType_" + i.ToString(), x.SystemType);
                Sm.CmParam<String>(ref cm, "@EmploymentStatus_" + i.ToString(), x.EmploymentStatus);
                Sm.CmParam<String>(ref cm, "@PayrunPeriod_" + i.ToString(), x.PayrunPeriod);
                Sm.CmParam<String>(ref cm, "@PGCode_" + i.ToString(), x.PGCode);
                Sm.CmParam<String>(ref cm, "@SiteCode_" + i.ToString(), x.SiteCode);
                Sm.CmParam<Decimal>(ref cm, "@WorkingDay_" + i.ToString(), x.WorkingDay);
                Sm.CmParam<String>(ref cm, "@WSCode_" + i.ToString(), x.WSCode);
                Sm.CmParam<String>(ref cm, "@HolInd_" + i.ToString(), x.HolInd ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@HolidayIndex_" + i.ToString(), x.HolidayIndex);
                Sm.CmParam<String>(ref cm, "@WSHolidayInd_" + i.ToString(), x.WSHolidayInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@WSIn1_" + i.ToString(), x.WSIn1);
                Sm.CmParam<String>(ref cm, "@WSOut1_" + i.ToString(), x.WSOut1);
                Sm.CmParam<String>(ref cm, "@WSIn2_" + i.ToString(), x.WSIn2);
                Sm.CmParam<String>(ref cm, "@WSOut2_" + i.ToString(), x.WSOut2);
                Sm.CmParam<String>(ref cm, "@WSIn3_" + i.ToString(), x.WSIn3);
                Sm.CmParam<String>(ref cm, "@WSOut3_" + i.ToString(), x.WSOut3);
                Sm.CmParam<String>(ref cm, "@OneDayInd_" + i.ToString(), x.OneDayInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LateInd_" + i.ToString(), x.LateInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ActualIn_" + i.ToString(), x.ActualIn);
                Sm.CmParam<String>(ref cm, "@ActualOut_" + i.ToString(), x.ActualOut);
                Sm.CmParam<String>(ref cm, "@WorkingIn_" + i.ToString(), x.WorkingIn);
                Sm.CmParam<String>(ref cm, "@WorkingOut_" + i.ToString(), x.WorkingOut);
                Sm.CmParam<Decimal>(ref cm, "@WorkingDuration_" + i.ToString(), x.WorkingDuration);
                Sm.CmParam<Decimal>(ref cm, "@EmpSalary_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.EmpSalary) : x.EmpSalary);
                Sm.CmParam<Decimal>(ref cm, "@EmpSalary2_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.EmpSalary2) : x.EmpSalary2);
                Sm.CmParam<Decimal>(ref cm, "@BasicSalary_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.BasicSalary) : x.BasicSalary);
                Sm.CmParam<Decimal>(ref cm, "@BasicSalary2_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.BasicSalary2) : x.BasicSalary2);
                Sm.CmParam<Decimal>(ref cm, "@SalaryPension_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.SalaryPension) : x.SalaryPension);
                Sm.CmParam<Decimal>(ref cm, "@ProductionWages_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.ProductionWages) : x.ProductionWages);
                Sm.CmParam<Decimal>(ref cm, "@Salary_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Salary) : x.Salary);
                Sm.CmParam<String>(ref cm, "@LeaveCode_" + i.ToString(), x.LeaveCode);
                Sm.CmParam<String>(ref cm, "@LeaveType_" + i.ToString(), x.LeaveType);
                Sm.CmParam<String>(ref cm, "@PaidLeaveInd_" + i.ToString(), x.PaidLeaveInd ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@LeaveStartTm_" + i.ToString(), x.LeaveStartTm);
                Sm.CmParam<String>(ref cm, "@LeaveEndTm_" + i.ToString(), x.LeaveEndTm);
                Sm.CmParam<Decimal>(ref cm, "@LeaveDuration_" + i.ToString(), x.LeaveDuration);
                Sm.CmParam<Decimal>(ref cm, "@UPLDay_" + i.ToString(), x.UPLDay);
                Sm.CmParam<Decimal>(ref cm, "@UPLHr_" + i.ToString(), x.UPLHr);
                Sm.CmParam<Decimal>(ref cm, "@UPLAmt_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Salary) : x.UPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Salary) : x.ProcessUPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@PLDay_" + i.ToString(), x.PLDay);
                Sm.CmParam<Decimal>(ref cm, "@PLHr_" + i.ToString(), x.PLHr);
                Sm.CmParam<Decimal>(ref cm, "@PLAmt_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Salary) : x.PLAmt);
                Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Salary) : x.ProcessPLAmt);
                Sm.CmParam<Decimal>(ref cm, "@OT1Hr_" + i.ToString(), x.OT1Hr);
                Sm.CmParam<Decimal>(ref cm, "@OT2Hr_" + i.ToString(), x.OT2Hr);
                Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr_" + i.ToString(), x.OTHolidayHr);
                Sm.CmParam<Decimal>(ref cm, "@OT1Amt_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.OT1Amt) : x.OT1Amt);
                Sm.CmParam<Decimal>(ref cm, "@OT2Amt_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.OT2Amt) : x.OT2Amt);
                Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.OTHolidayAmt) : x.OTHolidayAmt);
                Sm.CmParam<Decimal>(ref cm, "@OTMeal_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.OTMeal) : x.OTMeal);
                Sm.CmParam<Decimal>(ref cm, "@OTMonthlyIncome_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.OTMonthlyIncome) : x.OTMonthlyIncome);
                Sm.CmParam<String>(ref cm, "@OTToLeaveInd_" + i.ToString(), x.OTToLeaveInd ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@IncMinWages_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.IncMinWages) : x.IncMinWages);
                Sm.CmParam<Decimal>(ref cm, "@IncProduction_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.IncProduction) : x.IncProduction);
                Sm.CmParam<Decimal>(ref cm, "@IncEmployee_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.IncEmployee) : x.IncEmployee);
                Sm.CmParam<Decimal>(ref cm, "@IncPerformance_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.IncPerformance) : x.IncPerformance);
                Sm.CmParam<String>(ref cm, "@PresenceRewardInd_" + i.ToString(), x.PresenceRewardInd ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@HolidayEarning_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.HolidayEarning) : x.HolidayEarning);
                Sm.CmParam<Decimal>(ref cm, "@ExtraFooding_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.ExtraFooding) : x.ExtraFooding);
                Sm.CmParam<Decimal>(ref cm, "@FieldAssignment_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.FieldAssignment) : x.FieldAssignment);
                Sm.CmParam<Decimal>(ref cm, "@Transport_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Transport) : x.Transport);
                Sm.CmParam<Decimal>(ref cm, "@Meal_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Meal) : x.Meal);
                Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.ServiceChargeIncentive) : x.ServiceChargeIncentive);
                Sm.CmParam<Decimal>(ref cm, "@DedProduction_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.DedProduction) : x.DedProduction);
                Sm.CmParam<Decimal>(ref cm, "@DedProdLeave_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.DedProdLeave) : x.DedProdLeave);
                Sm.CmParam<Decimal>(ref cm, "@ADOT_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.ADOT) : x.ADOT);
                Sm.CmParam<Decimal>(ref cm, "@DedAtd_" + i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.DedAtd) : x.DedAtd);
                Sm.CmParam<String>(ref cm, "@OTSpecialind_" + i.ToString(), x.OTSpecialInd ? "Y" : "N");

                i++;
            }
            SQL.AppendLine(";");


            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
            Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
            Sm.CmParam<String>(ref cm, "@PensionInd", r.PensionInd?"Y":"N");
            Sm.CmParam<String>(ref cm, "@NPWP", r.NPWP);
            Sm.CmParam<String>(ref cm, "@PTKP", r.PTKP);
	        Sm.CmParam<Decimal>(ref cm, "@Salary", mIsPayrollAmtRounded ? Math.Truncate(r.Salary) : r.Salary);
	        Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
	        Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
	        Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
	        Sm.CmParam<Decimal>(ref cm, "@PLAmt", mIsPayrollAmtRounded ? Math.Truncate(r.PLAmt) : r.PLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", mIsPayrollAmtRounded ? Math.Truncate(r.ProcessPLAmt) : r.ProcessPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
	        Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
	        Sm.CmParam<Decimal>(ref cm, "@UPLAmt", mIsPayrollAmtRounded ? Math.Truncate(r.UPLAmt) : r.UPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", mIsPayrollAmtRounded ? Math.Truncate(r.ProcessUPLAmt) : r.ProcessUPLAmt);
            Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
	        Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
	        Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
	        Sm.CmParam<Decimal>(ref cm, "@OT1Amt", mIsPayrollAmtRounded ? Math.Truncate(r.OT1Amt) : r.OT1Amt);
	        Sm.CmParam<Decimal>(ref cm, "@OT2Amt", mIsPayrollAmtRounded ? Math.Truncate(r.OT2Amt) : r.OT2Amt);
	        Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", mIsPayrollAmtRounded ? Math.Truncate(r.OTHolidayAmt) : r.OTHolidayAmt);
            Sm.CmParam<Decimal>(ref cm, "@OTMeal", mIsPayrollAmtRounded ? Math.Truncate(r.OTMeal) : r.OTMeal);
            Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixAllowance", mIsPayrollAmtRounded ? Math.Truncate(r.NonTaxableFixAllowance) : r.NonTaxableFixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@TaxableFixAllowance", mIsPayrollAmtRounded ? Math.Truncate(r.TaxableFixAllowance) : r.TaxableFixAllowance);
            Sm.CmParam<Decimal>(ref cm, "@FixAllowance", mIsPayrollAmtRounded ? Math.Truncate(r.FixAllowance) : r.FixAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@EmploymentPeriodAllowance", mIsPayrollAmtRounded ? Math.Truncate(r.EmploymentPeriodAllowance) : r.EmploymentPeriodAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@IncEmployee", mIsPayrollAmtRounded ? Math.Truncate(r.IncEmployee) : r.IncEmployee);
	        Sm.CmParam<Decimal>(ref cm, "@IncMinWages", mIsPayrollAmtRounded ? Math.Truncate(r.IncMinWages) : r.IncMinWages);
	        Sm.CmParam<Decimal>(ref cm, "@IncProduction", mIsPayrollAmtRounded ? Math.Truncate(r.IncProduction) : r.IncProduction);
	        Sm.CmParam<Decimal>(ref cm, "@IncPerformance", mIsPayrollAmtRounded ? Math.Truncate(r.IncPerformance) : r.IncPerformance);
	        Sm.CmParam<Decimal>(ref cm, "@PresenceReward", mIsPayrollAmtRounded ? Math.Truncate(r.PresenceReward) : r.PresenceReward);
	        Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", mIsPayrollAmtRounded ? Math.Truncate(r.HolidayEarning) : r.HolidayEarning);
	        Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", mIsPayrollAmtRounded ? Math.Truncate(r.ExtraFooding) : r.ExtraFooding);
            Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", mIsPayrollAmtRounded ? Math.Truncate(r.FieldAssignment) : r.FieldAssignment);
            Sm.CmParam<Decimal>(ref cm, "@Transport", mIsPayrollAmtRounded ? Math.Truncate(r.Transport) : r.Transport);
            Sm.CmParam<Decimal>(ref cm, "@Meal", mIsPayrollAmtRounded ? Math.Truncate(r.Meal) : r.Meal);
            Sm.CmParam<Decimal>(ref cm, "@Functional", mIsPayrollAmtRounded ? Math.Truncate(r.Functional) : r.Functional);
            Sm.CmParam<Decimal>(ref cm, "@Housing", mIsPayrollAmtRounded ? Math.Truncate(r.Housing) : r.Housing);
            Sm.CmParam<Decimal>(ref cm, "@MobileCredit", mIsPayrollAmtRounded ? Math.Truncate(r.MobileCredit) : r.MobileCredit);
            Sm.CmParam<Decimal>(ref cm, "@Zakat", mIsPayrollAmtRounded ? Math.Truncate(r.Zakat) : r.Zakat);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerHealth) : r.SSEmployerHealth);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerEmployment) : r.SSEmployerEmployment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerPension) : r.SSEmployerPension);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPension2", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerPension2) : r.SSEmployerPension2);
            Sm.CmParam<Decimal>(ref cm, "@NonTaxableFixDeduction", mIsPayrollAmtRounded ? Math.Truncate(r.NonTaxableFixDeduction) : r.NonTaxableFixDeduction);
            Sm.CmParam<Decimal>(ref cm, "@TaxableFixDeduction", mIsPayrollAmtRounded ? Math.Truncate(r.TaxableFixDeduction) : r.TaxableFixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "@FixDeduction", mIsPayrollAmtRounded ? Math.Truncate(r.FixDeduction) : r.FixDeduction);
	        Sm.CmParam<Decimal>(ref cm, "@DedEmployee", mIsPayrollAmtRounded ? Math.Truncate(r.DedEmployee) : r.DedEmployee);
	        Sm.CmParam<Decimal>(ref cm, "@DedProduction", mIsPayrollAmtRounded ? Math.Truncate(r.DedProduction) : r.DedProduction);
	        Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", mIsPayrollAmtRounded ? Math.Truncate(r.DedProdLeave) : r.DedProdLeave);
            Sm.CmParam<Decimal>(ref cm, "@EmpAdvancePayment", mIsPayrollAmtRounded ? Math.Truncate(r.EmpAdvancePayment) : r.EmpAdvancePayment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeHealth) : r.SSEmployeeHealth);
	        Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeEmployment) : r.SSEmployeeEmployment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeePension) : r.SSEmployeePension);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePension2", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeePension2) : r.SSEmployeePension2);
            Sm.CmParam<Decimal>(ref cm, "@SalaryPension", mIsPayrollAmtRounded ? Math.Truncate(r.SalaryPension) : r.SalaryPension);
            Sm.CmParam<Decimal>(ref cm, "@SSErLifeInsurance", mIsPayrollAmtRounded ? Math.Truncate(r.SSErLifeInsurance) : r.SSErLifeInsurance);
            Sm.CmParam<Decimal>(ref cm, "@SSEeLifeInsurance", mIsPayrollAmtRounded ? Math.Truncate(r.SSEeLifeInsurance) : r.SSEeLifeInsurance);
            Sm.CmParam<Decimal>(ref cm, "@SSErWorkingAccident", mIsPayrollAmtRounded ? Math.Truncate(r.SSErWorkingAccident) : r.SSErWorkingAccident);
            Sm.CmParam<Decimal>(ref cm, "@SSEeWorkingAccident", mIsPayrollAmtRounded ? Math.Truncate(r.SSEeWorkingAccident) : r.SSEeWorkingAccident);
            Sm.CmParam<Decimal>(ref cm, "@SSErRetirement", mIsPayrollAmtRounded ? Math.Truncate(r.SSErRetirement) : r.SSErRetirement);
            Sm.CmParam<Decimal>(ref cm, "@SSEeRetirement", mIsPayrollAmtRounded ? Math.Truncate(r.SSEeRetirement) : r.SSEeRetirement);
            Sm.CmParam<Decimal>(ref cm, "@SSErPension", mIsPayrollAmtRounded ? Math.Truncate(r.SSErPension) : r.SSErPension);
            Sm.CmParam<Decimal>(ref cm, "@SSEePension", mIsPayrollAmtRounded ? Math.Truncate(r.SSEePension) : r.SSEePension);
            Sm.CmParam<Decimal>(ref cm, "@ADLeave", mIsPayrollAmtRounded ? Math.Truncate(r.ADLeave) : r.ADLeave);
	        Sm.CmParam<Decimal>(ref cm, "@SalaryAdjustment", mIsPayrollAmtRounded ? Math.Truncate(r.SalaryAdjustment) : r.SalaryAdjustment);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerBNILife", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerBNILife) : r.SSEmployerBNILife);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeBNILife", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeBNILife) : r.SSEmployeeBNILife);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerDPNPHT", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerDPNPHT) : r.SSEmployerDPNPHT);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeDPNPHT", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeDPNPHT) : r.SSEmployeeDPNPHT);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerDPNPNS", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerDPNPNS) : r.SSEmployerDPNPNS);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeDPNPNS", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeDPNPNS) : r.SSEmployeeDPNPNS);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPKES", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerPKES) : r.SSEmployerPKES);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePKES", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeePKES) : r.SSEmployeePKES);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerPRMH", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerPRMH) : r.SSEmployerPRMH);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeePRMH", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeePRMH) : r.SSEmployeePRMH);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerTSP", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerTSP) : r.SSEmployerTSP);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeTSP", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeTSP) : r.SSEmployeeTSP);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerWNR", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerWNR) : r.SSEmployerWNR);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeWNR", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeWNR) : r.SSEmployeeWNR);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployerNonBPJS", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployerNonBPJS) : r.SSEmployerNonBPJS);
            Sm.CmParam<Decimal>(ref cm, "@SSEmployeeNonBPJS", mIsPayrollAmtRounded ? Math.Truncate(r.SSEmployeeNonBPJS) : r.SSEmployeeNonBPJS);
            Sm.CmParam<String>(ref cm, "@CreditCode1", r.CreditCode1);
            Sm.CmParam<String>(ref cm, "@CreditCode2", r.CreditCode2);
            Sm.CmParam<String>(ref cm, "@CreditCode3", r.CreditCode3);
            Sm.CmParam<String>(ref cm, "@CreditCode4", r.CreditCode4);
            Sm.CmParam<String>(ref cm, "@CreditCode5", r.CreditCode5);
            Sm.CmParam<String>(ref cm, "@CreditCode6", r.CreditCode6);
            Sm.CmParam<String>(ref cm, "@CreditCode7", r.CreditCode7);
            Sm.CmParam<String>(ref cm, "@CreditCode8", r.CreditCode8);
            Sm.CmParam<String>(ref cm, "@CreditCode9", r.CreditCode9);
            Sm.CmParam<String>(ref cm, "@CreditCode10", r.CreditCode10);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment1", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment1) : r.CreditAdvancePayment1);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment2", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment2) : r.CreditAdvancePayment2);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment3", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment3) : r.CreditAdvancePayment3);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment4", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment4) : r.CreditAdvancePayment4);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment5", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment5) : r.CreditAdvancePayment5);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment6", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment6) : r.CreditAdvancePayment6);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment7", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment7) : r.CreditAdvancePayment7);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment8", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment8) : r.CreditAdvancePayment8);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment9", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment9) : r.CreditAdvancePayment9);
            Sm.CmParam<Decimal>(ref cm, "@CreditAdvancePayment10", mIsPayrollAmtRounded ? Math.Truncate(r.CreditAdvancePayment10) : r.CreditAdvancePayment10);
            Sm.CmParam<Decimal>(ref cm, "@WarningLetter", mIsPayrollAmtRounded ? Math.Truncate(r.WarningLetter) : r.WarningLetter);
            Sm.CmParam<Decimal>(ref cm, "@ServiceYearAllowance", mIsPayrollAmtRounded ? Math.Truncate(r.ServiceYearAllowance) : r.ServiceYearAllowance);
	        Sm.CmParam<Decimal>(ref cm, "@Tax", mIsPayrollAmtRounded ? Math.Truncate(r.Tax) : r.Tax);
            Sm.CmParam<Decimal>(ref cm, "@TaxAllowance", mIsPayrollAmtRounded ? Math.Truncate(r.TaxAllowance) : r.TaxAllowance);
            Sm.CmParam<Decimal>(ref cm, "@Amt", mIsPayrollAmtRounded ? Math.Truncate(r.Amt) : r.Amt);
            Sm.CmParam<Decimal>(ref cm, "@DedAtd", mIsPayrollAmtRounded ? Math.Truncate(r.DedAtd) : r.DedAtd);
            Sm.CmParam<Decimal>(ref cm, "@ADOT", mIsPayrollAmtRounded ? Math.Truncate(r.ADOT) : r.ADOT);
            Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", mIsPayrollAmtRounded ? Math.Truncate(r.ServiceChargeIncentive) : r.ServiceChargeIncentive);
            Sm.CmParam<String>(ref cm, "@PassAwayInd", r.PassAwayInd?"Y":"N");
            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<Decimal>(ref cm, "@RecomputedValueForFixedAD", mIsPayrollAmtRounded ? Math.Truncate(r._RecomputedValueForFixedAD) : r._RecomputedValueForFixedAD);
            Sm.CmParam<Decimal>(ref cm, "@RecomputedValueForProratedAD", mIsPayrollAmtRounded ? Math.Truncate(r._RecomputedValueForProratedAD) : r._RecomputedValueForProratedAD);
            Sm.CmParam<String>(ref cm, "@SSYr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@SSMth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@SSProgramBPJS", mSSProgramBPJS);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SavePayrollProcess2(ref Result1 r)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblPayrollProcess2 ");
        //    SQL.AppendLine("(PayrunCode, EmpCode, Dt, ProcessInd, LatestPaidDt, ");
        //    SQL.AppendLine("JoinDt, ResignDt, PosCode, DeptCode, GrdLvlCode, LevelCode, ");
        //    SQL.AppendLine("SystemType, EmploymentStatus, PayrunPeriod, PGCode, SiteCode, WorkingDay, ");
        //    SQL.AppendLine("WSCode, HolInd, HolidayIndex, WSHolidayInd, WSIn1, ");
        //    SQL.AppendLine("WSOut1, WSIn2, WSOut2, WSIn3, WSOut3, ");
        //    SQL.AppendLine("OneDayInd, LateInd, ActualIn, ActualOut, WorkingIn, ");
        //    SQL.AppendLine("WorkingOut, WorkingDuration, EmpSalary, EmpSalary2, BasicSalary, BasicSalary2, SalaryPension, ProductionWages, ");
        //    SQL.AppendLine("Salary, LeaveCode, LeaveType, PaidLeaveInd, LeaveStartTm,");
        //    SQL.AppendLine("LeaveEndTm, LeaveDuration, PLDay, PLHr, PLAmt, ProcessPLAmt, UPLDay, UPLHr, UPLAmt, ProcessUPLAmt, ");
        //    SQL.AppendLine("OT1Hr, OT2Hr, OTHolidayHr, OT1Amt, ");
        //    SQL.AppendLine("OT2Amt, OTHolidayAmt, OTMeal, OTMonthlyIncome, OTToLeaveInd, IncMinWages, IncProduction, IncEmployee, ");
        //    SQL.AppendLine("IncPerformance, PresenceRewardInd, HolidayEarning, ExtraFooding, FieldAssignment, Transport, Meal, ServiceChargeIncentive, DedProduction, ");
        //    SQL.AppendLine("DedProdLeave,  ADOT,  DedAtd, OTSpecialind, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values( ");
        //    SQL.AppendLine("@PayrunCode, @EmpCode, @Dt, @ProcessInd, @LatestPaidDt, ");
        //    SQL.AppendLine("@JoinDt, @ResignDt, @PosCode, @DeptCode, @GrdLvlCode, @LevelCode, ");
        //    SQL.AppendLine("@SystemType, @EmploymentStatus, @PayrunPeriod, @PGCode, @SiteCode, @WorkingDay, ");
        //    SQL.AppendLine("@WSCode, @HolInd, @HolidayIndex, @WSHolidayInd, @WSIn1, ");
        //    SQL.AppendLine("@WSOut1, @WSIn2, @WSOut2, @WSIn3, @WSOut3, ");
        //    SQL.AppendLine("@OneDayInd, @LateInd, @ActualIn, @ActualOut, @WorkingIn, ");
        //    SQL.AppendLine("@WorkingOut, @WorkingDuration, @EmpSalary, @EmpSalary2, @BasicSalary, @BasicSalary2, @SalaryPension, @ProductionWages, ");
        //    SQL.AppendLine("@Salary, @LeaveCode, @LeaveType, @PaidLeaveInd, @LeaveStartTm,");
        //    SQL.AppendLine("@LeaveEndTm, @LeaveDuration, @PLDay, @PLHr, @PLAmt, @ProcessPLAmt, @UPLDay, @UPLHr, @UPLAmt, @ProcessUPLAmt, ");
        //    SQL.AppendLine("@OT1Hr, @OT2Hr, @OTHolidayHr, @OT1Amt, ");
        //    SQL.AppendLine("@OT2Amt, @OTHolidayAmt, @OTMeal, @OTMonthlyIncome, @OTToLeaveInd, @IncMinWages, @IncProduction, @IncEmployee, ");
        //    SQL.AppendLine("@IncPerformance, @PresenceRewardInd, @HolidayEarning, @ExtraFooding, @FieldAssignment, @Transport, @Meal, @ServiceChargeIncentive, @DedProduction, ");
        //    SQL.AppendLine("@DedProdLeave,  @ADOT, @DedAtd, @OTSpecialind, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
        //    Sm.CmParam<String>(ref cm, "@Dt", r.Dt);
        //    Sm.CmParam<String>(ref cm, "@ProcessInd", r.ProcessInd?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@LatestPaidDt", r.LatestPaidDt);
        //    Sm.CmParam<String>(ref cm, "@JoinDt", r.JoinDt);
        //    Sm.CmParam<String>(ref cm, "@ResignDt", r.ResignDt);
        //    Sm.CmParam<String>(ref cm, "@PosCode", r.PosCode);
        //    Sm.CmParam<String>(ref cm, "@DeptCode", r.DeptCode);
        //    Sm.CmParam<String>(ref cm, "@GrdLvlCode", r.GrdLvlCode);
        //    Sm.CmParam<String>(ref cm, "@LevelCode", r.LevelCode);
        //    Sm.CmParam<String>(ref cm, "@SystemType", r.SystemType);
        //    Sm.CmParam<String>(ref cm, "@EmploymentStatus", r.EmploymentStatus);
        //    Sm.CmParam<String>(ref cm, "@PayrunPeriod", r.PayrunPeriod);
        //    Sm.CmParam<String>(ref cm, "@PGCode", r.PGCode);
        //    Sm.CmParam<String>(ref cm, "@SiteCode", r.SiteCode);
        //    Sm.CmParam<Decimal>(ref cm, "@WorkingDay", r.WorkingDay);
        //    Sm.CmParam<String>(ref cm, "@WSCode", r.WSCode);
        //    Sm.CmParam<String>(ref cm, "@HolInd", r.HolInd?"Y":"N");
        //    Sm.CmParam<Decimal>(ref cm, "@HolidayIndex", r.HolidayIndex);
        //    Sm.CmParam<String>(ref cm, "@WSHolidayInd", r.WSHolidayInd?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@WSIn1", r.WSIn1);
        //    Sm.CmParam<String>(ref cm, "@WSOut1", r.WSOut1);
        //    Sm.CmParam<String>(ref cm, "@WSIn2", r.WSIn2);
        //    Sm.CmParam<String>(ref cm, "@WSOut2", r.WSOut2);
        //    Sm.CmParam<String>(ref cm, "@WSIn3", r.WSIn3);
        //    Sm.CmParam<String>(ref cm, "@WSOut3", r.WSOut3);
        //    Sm.CmParam<String>(ref cm, "@OneDayInd", r.OneDayInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@LateInd", r.LateInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@ActualIn", r.ActualIn);
        //    Sm.CmParam<String>(ref cm, "@ActualOut", r.ActualOut);
        //    Sm.CmParam<String>(ref cm, "@WorkingIn", r.WorkingIn);
        //    Sm.CmParam<String>(ref cm, "@WorkingOut", r.WorkingOut);
        //    Sm.CmParam<Decimal>(ref cm, "@WorkingDuration", r.WorkingDuration);
        //    Sm.CmParam<Decimal>(ref cm, "@EmpSalary", r.EmpSalary);
        //    Sm.CmParam<Decimal>(ref cm, "@EmpSalary2", r.EmpSalary2);
        //    Sm.CmParam<Decimal>(ref cm, "@BasicSalary", r.BasicSalary);
        //    Sm.CmParam<Decimal>(ref cm, "@BasicSalary2", r.BasicSalary2);
        //    Sm.CmParam<Decimal>(ref cm, "@SalaryPension", r.SalaryPension);
        //    Sm.CmParam<Decimal>(ref cm, "@ProductionWages", r.ProductionWages);
        //    Sm.CmParam<Decimal>(ref cm, "@Salary", r.Salary);
        //    Sm.CmParam<String>(ref cm, "@LeaveCode", r.LeaveCode);
        //    Sm.CmParam<String>(ref cm, "@LeaveType", r.LeaveType);
        //    Sm.CmParam<String>(ref cm, "@PaidLeaveInd", r.PaidLeaveInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@LeaveStartTm", r.LeaveStartTm);
        //    Sm.CmParam<String>(ref cm, "@LeaveEndTm", r.LeaveEndTm);
        //    Sm.CmParam<Decimal>(ref cm, "@LeaveDuration", r.LeaveDuration);
        //    Sm.CmParam<Decimal>(ref cm, "@UPLDay", r.UPLDay);
        //    Sm.CmParam<Decimal>(ref cm, "@UPLHr", r.UPLHr);
        //    Sm.CmParam<Decimal>(ref cm, "@UPLAmt", r.UPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@ProcessUPLAmt", r.ProcessUPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@PLDay", r.PLDay);
        //    Sm.CmParam<Decimal>(ref cm, "@PLHr", r.PLHr);
        //    Sm.CmParam<Decimal>(ref cm, "@PLAmt", r.PLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@ProcessPLAmt", r.ProcessPLAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@OT1Hr", r.OT1Hr);
        //    Sm.CmParam<Decimal>(ref cm, "@OT2Hr", r.OT2Hr);
        //    Sm.CmParam<Decimal>(ref cm, "@OTHolidayHr", r.OTHolidayHr);
        //    Sm.CmParam<Decimal>(ref cm, "@OT1Amt", r.OT1Amt);
        //    Sm.CmParam<Decimal>(ref cm, "@OT2Amt", r.OT2Amt);
        //    Sm.CmParam<Decimal>(ref cm, "@OTHolidayAmt", r.OTHolidayAmt);
        //    Sm.CmParam<Decimal>(ref cm, "@OTMeal", r.OTMeal);
        //    Sm.CmParam<Decimal>(ref cm, "@OTMonthlyIncome", r.OTMonthlyIncome);
        //    Sm.CmParam<String>(ref cm, "@OTToLeaveInd", r.OTToLeaveInd ? "Y" : "N");
        //    Sm.CmParam<Decimal>(ref cm, "@IncMinWages", r.IncMinWages);
        //    Sm.CmParam<Decimal>(ref cm, "@IncProduction", r.IncProduction);
        //    Sm.CmParam<Decimal>(ref cm, "@IncEmployee", r.IncEmployee);
        //    Sm.CmParam<Decimal>(ref cm, "@IncPerformance", r.IncPerformance);
        //    Sm.CmParam<String>(ref cm, "@PresenceRewardInd", r.PresenceRewardInd ? "Y" : "N");
        //    Sm.CmParam<Decimal>(ref cm, "@HolidayEarning", r.HolidayEarning);
        //    Sm.CmParam<Decimal>(ref cm, "@ExtraFooding", r.ExtraFooding);
        //    Sm.CmParam<Decimal>(ref cm, "@FieldAssignment", r.FieldAssignment);
        //    Sm.CmParam<Decimal>(ref cm, "@Transport", r.Transport);
        //    Sm.CmParam<Decimal>(ref cm, "@Meal", r.Meal);
        //    Sm.CmParam<Decimal>(ref cm, "@ServiceChargeIncentive", r.ServiceChargeIncentive);
        //    Sm.CmParam<Decimal>(ref cm, "@DedProduction", r.DedProduction);
        //    Sm.CmParam<Decimal>(ref cm, "@DedProdLeave", r.DedProdLeave);
        //    Sm.CmParam<Decimal>(ref cm, "@ADOT", r.ADOT);
        //    Sm.CmParam<Decimal>(ref cm, "@DedAtd", r.DedAtd);
        //    Sm.CmParam<String>(ref cm, "@OTSpecialind", r.OTSpecialInd ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveAdvancePaymentProcess(ref AdvancePaymentProcess x)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/* Payroll Process 2 */ ");

            SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
            SQL.AppendLine("Where DocNo=@DocNo And Yr=@Yr And Mth=@Mth; ");

            SQL.AppendLine("Insert Into TblAdvancePaymentProcess ");
            SQL.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @Yr, @Mth, @EmpCode, @Amt, @PayrunCode, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParam<String>(ref cm, "DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "Yr", x.Yr);
            Sm.CmParam<String>(ref cm, "Mth", x.Mth);
            Sm.CmParam<String>(ref cm, "EmpCode", x.EmpCode);
            Sm.CmParam<Decimal>(ref cm, "Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int i = 0;

            SQL.AppendLine("/* Payroll Process 3 */ ");

            SQL.AppendLine("Delete From TblAdvancePaymentProcess ");
            SQL.AppendLine("Where Yr=@Yr And Mth=@Mth ");
            SQL.AppendLine("And DocNo In (");    
            foreach (var x in l)
            {
                if (i != 0) SQL.AppendLine(", ");
                SQL.AppendLine("@DocNo_" + i.ToString());
                Sm.CmParam<String>(ref cm, "DocNo_" + i.ToString(), x.DocNo);
                i++;
            }
            SQL.AppendLine("); ");

            i = 0;
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblAdvancePaymentProcess ");
            SQL.AppendLine("(DocNo, Yr, Mth, EmpCode, Amt, PayrunCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            foreach (var x in l)
            {
                if (i != 0) SQL.AppendLine(", ");
                SQL.AppendLine("(@DocNo__"+i.ToString() + ", @Yr, @Mth, @EmpCode__" + i.ToString() + ", @Amt__" + i.ToString() + ", @PayrunCode, @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "DocNo__" + i.ToString(), x.DocNo);
                Sm.CmParam<String>(ref cm, "EmpCode__" + i.ToString(), x.EmpCode);
                Sm.CmParam<Decimal>(ref cm, "Amt__"+i.ToString(), mIsPayrollAmtRounded ? Math.Truncate(x.Amt) : x.Amt);

                i++;
            }
            SQL.AppendLine(";");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "Yr", Sm.Left(mEndDt, 4));
            Sm.CmParam<String>(ref cm, "Mth", mEndDt.Substring(4, 2));

            return cm;
        }

        private MySqlCommand SavePayrollOTAmt(ref Result2 r)
        {
            var SQL = new StringBuilder();
            string Query = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='06'; ");
            var OTMonthlyIncome = 0m;
            var n = Sm.GetValue(Query, r.PayrunCode, r.EmpCode, string.Empty);

            if (n.Length > 0) OTMonthlyIncome = decimal.Parse(n);

            SQL.AppendLine("/* Payroll Process 4 */ ");

            SQL.AppendLine("Update TblPayrollProcess2 A ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    OTMonthlyIncome=@OTMonthlyIncome, ");
            SQL.AppendLine("    OT1Amt=Round((1.00/173.00)*@OTMonthlyIncome*OT1Hr*@OTFormulaIndex1, 0), ");
            SQL.AppendLine("    OT2Amt=Round((1.00/173.00)*@OTMonthlyIncome*OT2Hr*@OTFormulaIndex2, 0), ");
            SQL.AppendLine("    OTHolidayAmt=Round((1.00/173.00)*@OTMonthlyIncome*OTHolidayHr*@Holiday1stHrIndex, 0) ");
            SQL.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");

            SQL.AppendLine("Update TblPayrollProcess1 A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select Sum(OT1Amt) As OT1Amt, Sum(OT2Amt) As OT2Amt, Sum(OTHolidayAmt) OTHolidayAmt ");
            SQL.AppendLine("    From TblPayrollProcess2 ");
            SQL.AppendLine("    Where PayrunCode=@PayrunCode And EmpCode=@EmpCode ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.OT1Amt=B.OT1Amt, ");
            SQL.AppendLine("    A.OT2Amt=B.OT2Amt, ");
            SQL.AppendLine("    A.OTHolidayAmt=B.OTHolidayAmt ");
            SQL.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<Decimal>(ref cm, "@OTMonthlyIncome", mIsPayrollAmtRounded ? Math.Truncate(OTMonthlyIncome) : OTMonthlyIncome);
            Sm.CmParam<Decimal>(ref cm, "@OTFormulaIndex1", mIsPayrollAmtRounded ? Math.Truncate(mOTFormulaIndex1) : mOTFormulaIndex1);
            Sm.CmParam<Decimal>(ref cm, "@OTFormulaIndex2", mIsPayrollAmtRounded ? Math.Truncate(mOTFormulaIndex2) : mOTFormulaIndex2);
            Sm.CmParam<Decimal>(ref cm, "@Holiday1stHrIndex", mIsPayrollAmtRounded ? Math.Truncate(mHoliday1stHrIndex) : mHoliday1stHrIndex);
            
            return cm;
        }

        private MySqlCommand SavePayrollProcessAmt(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            #region Init
            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var SQL4 = new StringBuilder();
            var SQL5 = new StringBuilder();
            
            string TaxAddQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='01'; ");
            string TaxMinQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='02'; ");
            string AmtTHPQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='03'; ");
            string AmtNettoQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='04'; ");
            string AmtBruttoQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='05'; ");

            SetUMK();

            SQL1.AppendLine(TaxAddQuery);
            SQL2.AppendLine(TaxMinQuery);
            SQL3.AppendLine(AmtTHPQuery);
            SQL4.AppendLine(AmtNettoQuery);
            SQL5.AppendLine(AmtBruttoQuery);
            
            var TaxAddAmt = Sm.GetValue(SQL1.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var TaxMinAmt = Sm.GetValue(SQL2.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var THPAmt = Sm.GetValue(SQL3.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var NettoAmt = Sm.GetValue(SQL4.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var BruttoAmt = Sm.GetValue(SQL5.ToString(), r.PayrunCode, r.EmpCode, string.Empty);

            decimal TaxAdd = 0m, TaxMin = 0m;

            if (TaxAddAmt.Length > 0) TaxAdd = decimal.Parse(TaxAddAmt);
            if (TaxMinAmt.Length > 0) TaxMin = decimal.Parse(TaxMinAmt);
            if (THPAmt.Length > 0) r.Amt = decimal.Parse(THPAmt);
            if (NettoAmt.Length > 0)
            {
                r.Netto = decimal.Parse(NettoAmt);
                r.NettoActual = r.Netto;
            }
            if (BruttoAmt.Length>0) r.Brutto=decimal.Parse(BruttoAmt);

            if (TaxAdd < 0m) TaxAdd = 0m;
            if (TaxMin < 0m) TaxMin = 0m;
            if (r.Amt < 0m) r.Amt = 0m;
            if (r.Netto < 0m) r.Netto = 0m;
            if (r.NettoActual < 0m) r.NettoActual = 0m;

            if (r.Netto < mUMK) r.Netto = mUMK;
            if (r._IsADSMKExisted)
            {
                if (r.Netto < mUMPP) r.Netto = mUMPP;
            }
            #endregion

            ProcessTax(ref r, ref lNTI, ref lTI, ref lTI2, r.Amt, TaxAdd, TaxMin);

            var SQL = new StringBuilder();

            SQL.AppendLine("/* Payroll Process 4 */ ");

            SQL.AppendLine("Update TblPayrollProcess1 Set ");
            SQL.AppendLine("    TaxAllowance=@TaxAllowance, ");
            SQL.AppendLine("    Tax=@Tax ");
            SQL.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");

            SQL.AppendLine("Update TblPayrollProcess1 Set ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    Brutto=@Brutto, ");
            SQL.AppendLine("    Netto=@Netto, ");
            SQL.AppendLine("    NettoActual=@NettoActual ");
            SQL.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");            

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<Decimal>(ref cm, "@TaxAllowance", mIsPayrollAmtRounded ? Math.Truncate(r.TaxAllowance) : r.TaxAllowance);
            Sm.CmParam<Decimal>(ref cm, "@Tax", mIsPayrollAmtRounded ? Math.Truncate(r.Tax) : r.Tax);
            Sm.CmParam<Decimal>(ref cm, "@Amt", mIsPayrollAmtRounded ? Math.Truncate(r.Amt) : r.Amt);
            Sm.CmParam<Decimal>(ref cm, "@Brutto", mIsPayrollAmtRounded ? Math.Truncate(r.Brutto) : r.Brutto);
            Sm.CmParam<Decimal>(ref cm, "@Netto", mIsPayrollAmtRounded ? Math.Truncate(r.Netto) : r.Netto);
            Sm.CmParam<Decimal>(ref cm, "@NettoActual", mIsPayrollAmtRounded ? Math.Truncate(r.NettoActual) : r.NettoActual);

            return cm;
        }
        private MySqlCommand SavePayrollProcessAmtAfterTax(ref Result2 r)
        {
            #region Init
            var SQL1 = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();

            string AmtTHPQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='03'; ");
            string AmtNettoQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='04'; ");
            string AmtBruttoQuery = Sm.GetValue("Select QueryBuild From TblPayrollProcessFormula Where Code ='05'; ");

            SQL1.AppendLine(AmtTHPQuery);
            SQL2.AppendLine(AmtNettoQuery);
            SQL3.AppendLine(AmtBruttoQuery);

            var THPAmt = Sm.GetValue(SQL1.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var NettoAmt = Sm.GetValue(SQL2.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var BruttoAmt = Sm.GetValue(SQL3.ToString(), r.PayrunCode, r.EmpCode, string.Empty);

            if (THPAmt.Length > 0) r.Amt = decimal.Parse(THPAmt);
            if (NettoAmt.Length > 0) r.Netto = decimal.Parse(NettoAmt);
            if (BruttoAmt.Length > 0) r.Brutto = decimal.Parse(BruttoAmt);

            if (NettoAmt.Length > 0)
            {
                r.Netto = decimal.Parse(NettoAmt);
                r.NettoActual = r.Netto;
            }

            if (r.Amt < 0m) r.Amt = 0m;
            if (r.Netto < 0m) r.Netto = 0m;
            if (r.NettoActual < 0m) r.NettoActual = 0m;

            if (r.Netto < mUMK) r.Netto = mUMK;
            if (r._IsADSMKExisted)
            {
                if (r.Netto < mUMPP) r.Netto = mUMPP;
            }
            #endregion

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPayrollProcess1 Set ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    Brutto=@Brutto, ");
            SQL.AppendLine("    Netto=@Netto, ");
            SQL.AppendLine("    NettoActual=@NettoActual ");
            SQL.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", mIsPayrollAmtRounded ? Math.Truncate(r.Amt) : r.Amt);
            Sm.CmParam<Decimal>(ref cm, "@Brutto", mIsPayrollAmtRounded ? Math.Truncate(r.Brutto) : r.Brutto);
            Sm.CmParam<Decimal>(ref cm, "@Netto", mIsPayrollAmtRounded ? Math.Truncate(r.Netto) : r.Netto);
            Sm.CmParam<Decimal>(ref cm, "@NettoActual", mIsPayrollAmtRounded ? Math.Truncate(r.NettoActual) : r.NettoActual);

            return cm;
        }

        private MySqlCommand SavePayrollProcessPositionAdjustmentAndSocialSecurity(ref Result2 r)
        {
            #region TPJ

            mADCodePositionAdjustmentAmt = 0m;
            decimal YearsOfService = 0m;
            YearsOfService = GetYearsOfService2(r.EmpCode);
            decimal mADCodePositionAdjustmentOldAmt = Sm.GetValueDec("Select Amt From TblPayrollProcessAD Where PayrunCode = @Param1 And EmpCode = @Param2 And AdCode = 'AL015';", r.PayrunCode, r.EmpCode, mADCodePositionAdjustment, string.Empty); ;

            if (mADCodePositionAdjustment.Length > 0)
            {
                mADCodePositionAdjustmentAmt = GetPositionAdjustmentAmt(r.EmpCode, r.PayrunCode, YearsOfService);
            }
            else
            {
                mADCodePositionAdjustmentAmt = mADCodePositionAdjustmentOldAmt;
            }

            #endregion

            #region Init
            mDeviation = 0m;
            var SQL1 = new StringBuilder();
            var SQL3 = new StringBuilder();
            var SQL4 = new StringBuilder();
            var SQL5 = new StringBuilder();

            GetSQLQuery(ref SQL1);

            string AmtTHPQuery = string.Empty;
            string AmtNettoQuery = string.Empty;
            string AmtBruttoQuery = string.Empty;

            string AmtTHPQueryCheck = string.Empty, AmtNettoQueryCheck = string.Empty, AmtBruttoQueryCheck = string.Empty;

            //BPJS purposes
            string SSErHealthPercString = string.Empty;
            string SSErEmploymentPercString = string.Empty;
            string SSErPensionPercString = string.Empty;
            string SSErHealthSalaryMaxLimitString = string.Empty;

            string SSEeHealthPercString = string.Empty;
            string SSEeEmploymentPercString = string.Empty;
            string SSEePensionPercString = string.Empty;
            //end BPJS purposes

            AssignQueryValue(ref SQL1,
                ref AmtTHPQuery, ref AmtNettoQuery, ref AmtBruttoQuery,
                ref AmtTHPQueryCheck, ref AmtNettoQueryCheck, ref AmtBruttoQueryCheck,
                ref SSErHealthPercString, ref SSErEmploymentPercString, ref SSErPensionPercString,
                ref SSErHealthSalaryMaxLimitString,
                ref SSEeHealthPercString, ref SSEeEmploymentPercString, ref SSEePensionPercString);

            SQL3.AppendLine(AmtTHPQuery);
            SQL4.AppendLine(AmtNettoQuery);
            SQL5.AppendLine(AmtBruttoQuery);

            var THPAmt = Sm.GetValue(SQL3.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var NettoAmt = Sm.GetValue(SQL4.ToString(), r.PayrunCode, r.EmpCode, string.Empty);
            var BruttoAmt = Sm.GetValue(SQL5.ToString(), r.PayrunCode, r.EmpCode, string.Empty);

            if (THPAmt.Length > 0) r.Amt = decimal.Parse(THPAmt) - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
            if (NettoAmt.Length > 0)
            {
                r.Netto = decimal.Parse(NettoAmt) - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
                r.NettoActual = r.Netto;
            }
            if (BruttoAmt.Length > 0) r.Brutto = decimal.Parse(BruttoAmt) - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;

            if (r.Amt < 0m) r.Amt = 0m;
            if (r.Netto < 0m) r.Netto = 0m;
            if (r.NettoActual < 0m) r.NettoActual = 0m;

            if (r.Netto < mUMK) r.Netto = mUMK;
            if (r._IsADSMKExisted)
            {
                if (r.Netto < mUMPP) r.Netto = mUMPP;
            }
            #endregion

            #region Recalculate TPJ After Calculate Netto
            //YearsOfService = GetYearsOfService2(r.EmpCode);

            //get WLAmt 
            decimal mWLAmt = Sm.GetValueDec("Select WarningLetter From TblPayrollProcess1 Where PayrunCode = @Param1 and EmpCode = @Param2;", r.PayrunCode, r.EmpCode);

            if (r.Netto > r.NettoActual) mDeviation = r.Netto - r.NettoActual;
            //mADCodePositionAdjustmentAmt = GetPositionAdjustmentAmt(r.EmpCode, r.PayrunCode, YearsOfService);
            if (mDeviation != 0m && mWLAmt == 0)
            {
                if (mADCodePositionAdjustment.Length > 0) mADCodePositionAdjustmentAmt += mDeviation;
                r.Amt += mDeviation;
                r.Brutto += mDeviation;
                r.NettoActual += mDeviation;
            }

            #endregion

            #region BPJS

            decimal NewAmt = r.Amt;
            decimal NewNetto = r.Netto;
            decimal NewNettoActual = r.NettoActual;
            decimal NewBrutto = r.Brutto;
            decimal DeviationErHealth = 0m;
            decimal DeviationErEmployment = 0m;
            decimal DeviationErPension = 0m;
            decimal DeviationEeHealth = 0m;
            decimal DeviationEeEmployment = 0m;
            decimal DeviationEePension = 0m;

            decimal LastAmtErHealth = r.SSEmployerHealth;
            decimal LastAmtErEmployment = r.SSEmployerEmployment;
            decimal LastAmtErPension = r.SSErPension;
            decimal LastAmtEeHealth = r.SSEmployeeHealth;
            decimal LastAmtEeEmployment = r.SSEmployeeEmployment;
            decimal LastAmtEePension = r.SSEePension;
            decimal LastNewMultiplierErEmployment = 0m;
            decimal LastNewMultiplierErHealth = 0m;

            var l = new List<RecalculateSocialSecurity>();

            if (mIsPayrollProcessingCalculateBPJSAutomatic)
            {
                string SMKAmtString = Sm.GetValue("Select Sum(Amt) From TblPayrollProcessAD Where PayrunCode = @Param1 And EmpCode = @Param2 And Find_In_Set(ADCode, 'AL004,AL025'); ", r.PayrunCode, r.EmpCode, string.Empty);
                decimal SSErHealthPerc = (SSErHealthPercString.Trim().Length > 0) ? decimal.Parse(SSErHealthPercString) : 0m;
                decimal SSErEmploymentPerc = (SSErEmploymentPercString.Trim().Length > 0) ? decimal.Parse(SSErEmploymentPercString) : 0m;
                decimal SSErPensionPerc = (SSErPensionPercString.Trim().Length > 0) ? decimal.Parse(SSErPensionPercString) : 0m;
                decimal SSErHealthSalaryMaxLimit = (SSErHealthSalaryMaxLimitString.Trim().Length > 0) ? decimal.Parse(SSErHealthSalaryMaxLimitString) : 0m;
                decimal SSEeHealthPerc = (SSEeHealthPercString.Trim().Length > 0) ? decimal.Parse(SSEeHealthPercString) : 0m;
                decimal SSEeEmploymentPerc = (SSEeEmploymentPercString.Trim().Length > 0) ? decimal.Parse(SSEeEmploymentPercString) : 0m;
                decimal SSEePensionPerc = (SSEePensionPercString.Trim().Length > 0) ? decimal.Parse(SSEePensionPercString) : 0m;
                decimal SMKAmt = (SMKAmtString.Trim().Length > 0) ? decimal.Parse(SMKAmtString) : 0m;

                InitSocialSecurity(ref l, r,
                    SSErEmploymentPerc, SSErHealthPerc, SSErPensionPerc,
                    SSEeEmploymentPerc, SSEeHealthPerc, SSEePensionPerc,
                    SMKAmt, mADCodePositionAdjustmentAmt, SSErHealthSalaryMaxLimit
                    );
                if (l.Count > 0)
                {
                    int index = 0;
                    bool IsValid = false;
                    for (int x = 0; x < l.Count; ++x)
                    {
                        DeviationErHealth = l[x].DeviationErHealth;
                        DeviationErEmployment = l[x].DeviationErEmployment;
                        DeviationErPension = l[x].DeviationErPension;
                        DeviationEeHealth = l[x].DeviationEeHealth;
                        DeviationEeEmployment = l[x].DeviationEeEmployment;
                        DeviationEePension = l[x].DeviationEePension;
                        LastAmtErEmployment = l[x].NewAmtErEmployment;
                        LastAmtErHealth = l[x].NewAmtErHealth;
                        LastAmtErPension = l[x].NewAmtErPension;
                        LastAmtEeEmployment = l[x].NewAmtEeEmployment;
                        LastAmtEeHealth = l[x].NewAmtEeHealth;
                        LastAmtEePension = l[x].NewAmtEePension;
                        LastNewMultiplierErEmployment = l[x].NewMultiplierErEmployment;
                        LastNewMultiplierErHealth = l[x].NewMultiplierErHealth;

                        NewNettoActual = decimal.Parse(
                                Sm.GetValue(
                                    SQL4.ToString()
                                    .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                    .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                    .Replace("A.SSErPension", LastAmtErPension.ToString())
                                    .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                    .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                    .Replace("A.SSEePension", LastAmtEePension.ToString())
                                    ,
                                    r.PayrunCode, r.EmpCode, string.Empty
                                    )
                                );

                        NewBrutto = decimal.Parse(
                            Sm.GetValue(
                                SQL5.ToString()
                                .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                .Replace("A.SSErPension", LastAmtErPension.ToString())
                                .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                .Replace("A.SSEePension", LastAmtEePension.ToString())
                                ,
                                r.PayrunCode, r.EmpCode, string.Empty
                                )
                            );

                        NewNettoActual = NewNettoActual - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
                        NewBrutto = NewBrutto - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;

                        if (
                            //(0m <= l[x].DeviationErEmployment && l[x].DeviationErEmployment < 1m) ||
                            //(0m <= l[x].DeviationErHealth && l[x].DeviationErHealth < 1m)
                            Math.Abs(l[x].DeviationErEmployment) < 100m &&
                            Math.Abs(l[x].DeviationErHealth) < 100m &&
                            Math.Abs(l[x].DeviationErPension) < 100m &&
                            Math.Abs(l[x].DeviationEeEmployment) < 100m &&
                            Math.Abs(l[x].DeviationEeHealth) < 100m &&
                            Math.Abs(l[x].DeviationEePension) < 100m
                            )
                        {
                            IsValid = true;
                            break;
                        }
                    }

                    if (!IsValid)
                    {
                        while (!(
                                //(0m <= DeviationErEmployment && DeviationErEmployment < 1m) ||
                                //(0m <= DeviationErHealth && DeviationErHealth < 1m)
                                Math.Abs(DeviationErHealth) < 100m &&
                                Math.Abs(DeviationErEmployment) < 100m &&
                                Math.Abs(DeviationErPension) < 100m &&
                                Math.Abs(DeviationEeHealth) < 100m &&
                                Math.Abs(DeviationEeEmployment) < 100m &&
                                Math.Abs(DeviationEePension) < 100m
                            ))
                        {
                            NewNettoActual = decimal.Parse(
                                Sm.GetValue(
                                    SQL4.ToString()
                                    .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                    .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                    .Replace("A.SSErPension", LastAmtErPension.ToString())
                                    .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                    .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                    .Replace("A.SSEePension", LastAmtEePension.ToString())
                                    ,
                                    r.PayrunCode, r.EmpCode, string.Empty
                                    )
                                );

                            NewBrutto = decimal.Parse(
                                Sm.GetValue(
                                    SQL5.ToString()
                                    .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                    .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                    .Replace("A.SSErPension", LastAmtErPension.ToString())
                                    .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                    .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                    .Replace("A.SSEePension", LastAmtEePension.ToString())
                                    ,
                                    r.PayrunCode, r.EmpCode, string.Empty
                                    )
                                );

                            NewNettoActual = NewNettoActual - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
                            NewBrutto = NewBrutto - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;

                            decimal CurrNewMultiplierErHealth = NewNettoActual - SMKAmt - mADCodePositionAdjustmentAmt;
                            decimal CurrNewMultiplierErEmployment = NewNettoActual - SMKAmt;

                            if (CurrNewMultiplierErEmployment < mUMK) CurrNewMultiplierErEmployment = mUMK;
                            if (CurrNewMultiplierErHealth < mUMK) CurrNewMultiplierErHealth = mUMK;
                            if (CurrNewMultiplierErHealth > SSErHealthSalaryMaxLimit) CurrNewMultiplierErHealth = SSErHealthSalaryMaxLimit;

                            l.Add(new RecalculateSocialSecurity()
                            {
                                PayrunCode = r.PayrunCode,
                                EmpCode = r.EmpCode,

                                NettoActualErEmployment = NewNettoActual,
                                InitMultiplierErEmployment = LastNewMultiplierErEmployment,
                                //InitAmtErEmployment = LastNewMultiplierErEmployment, //LastAmtErEmployment * (SSErEmploymentPerc / 100m),
                                NewMultiplierErEmployment = NewNettoActual - SMKAmt,
                                NewAmtErEmployment = CurrNewMultiplierErEmployment * (SSErEmploymentPerc / 100m), //NewMultiplier * Percentage
                                DeviationErEmployment = LastNewMultiplierErEmployment - CurrNewMultiplierErEmployment, // InitMultiplier - NewMultiplier

                                NettoActualErHealth = NewNettoActual,
                                InitMultiplierErHealth = LastNewMultiplierErHealth,
                                //InitAmtErHealth = LastNewMultiplierErHealth, //LastAmtErHealth * (SSErHealthPerc / 100m),
                                NewMultiplierErHealth = CurrNewMultiplierErHealth,
                                NewAmtErHealth = CurrNewMultiplierErHealth * (SSErHealthPerc / 100m), //NewMultiplier * Percentage
                                DeviationErHealth = LastNewMultiplierErHealth - CurrNewMultiplierErHealth, // InitMultiplier - NewMultiplier

                                NettoActualErPension = NewNettoActual,
                                InitMultiplierErPension = LastNewMultiplierErEmployment,
                                NewAmtErPension = CurrNewMultiplierErEmployment * (SSErPensionPerc / 100m),
                                DeviationErPension = LastNewMultiplierErEmployment - CurrNewMultiplierErEmployment,

                                NettoActualEeEmployment = NewNettoActual,
                                InitMultiplierEeEmployment = LastNewMultiplierErEmployment,
                                NewAmtEeEmployment = CurrNewMultiplierErEmployment * (SSEeEmploymentPerc / 100m),
                                DeviationEeEmployment = LastNewMultiplierErEmployment - CurrNewMultiplierErEmployment,

                                NettoActualEeHealth = NewNettoActual,
                                InitMultiplierEeHealth = LastNewMultiplierErHealth,
                                NewAmtEeHealth = CurrNewMultiplierErHealth * (SSEeHealthPerc / 100m),
                                DeviationEeHealth = LastNewMultiplierErHealth - CurrNewMultiplierErHealth,

                                NettoActualEePension = NewNettoActual,
                                InitMultiplierEePension = LastNewMultiplierErEmployment,
                                NewAmtEePension = CurrNewMultiplierErEmployment * (SSEePensionPerc / 100m),
                                DeviationEePension = LastNewMultiplierErEmployment - CurrNewMultiplierErEmployment
                            });

                            index = l.Count() - 1;

                            DeviationErEmployment = l[index].DeviationErEmployment;
                            DeviationErHealth = l[index].DeviationErHealth;
                            DeviationErPension = l[index].DeviationErPension;
                            DeviationEeEmployment = l[index].DeviationEeEmployment;
                            DeviationEeHealth = l[index].DeviationEeHealth;
                            DeviationEePension = l[index].DeviationEePension;
                            LastAmtErEmployment = l[index].NewAmtErEmployment;
                            LastAmtErHealth = l[index].NewAmtErHealth;
                            LastAmtErPension = l[index].NewAmtErPension;
                            LastAmtEeEmployment = l[index].NewAmtEeEmployment;
                            LastAmtEeHealth = l[index].NewAmtEeHealth;
                            LastAmtEePension = l[index].NewAmtEePension;
                            LastNewMultiplierErEmployment = l[index].NewMultiplierErEmployment;
                            LastNewMultiplierErHealth = l[index].NewMultiplierErHealth;
                        }

                        IsValid = true;
                    }

                    if (IsValid)
                    {
                        LastAmtErPension = LastNewMultiplierErEmployment * (SSErPensionPerc / 100m);
                        LastAmtEeHealth = LastNewMultiplierErHealth * (SSEeHealthPerc / 100m);
                        LastAmtEeEmployment = LastNewMultiplierErEmployment * (SSEeEmploymentPerc / 100m);
                        LastAmtEePension = LastNewMultiplierErEmployment * (SSEePensionPerc / 100m);

                        NewNettoActual = decimal.Parse(
                                Sm.GetValue(
                                    SQL4.ToString()
                                    .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                    .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                    .Replace("A.SSErPension", LastAmtErPension.ToString())
                                    .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                    .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                    .Replace("A.SSEePension", LastAmtEePension.ToString()),
                                    r.PayrunCode, r.EmpCode, string.Empty
                                    )
                                );

                        NewBrutto = decimal.Parse(
                            Sm.GetValue(
                                SQL5.ToString()
                                .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                .Replace("A.SSErPension", LastAmtErPension.ToString())
                                .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                .Replace("A.SSEePension", LastAmtEePension.ToString()),
                                r.PayrunCode, r.EmpCode, string.Empty
                                )
                            );

                        NewNetto = decimal.Parse(
                            Sm.GetValue(
                                SQL4.ToString()
                                .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                .Replace("A.SSErPension", LastAmtErPension.ToString())
                                .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                .Replace("A.SSEePension", LastAmtEePension.ToString()),
                                r.PayrunCode, r.EmpCode, string.Empty
                                )
                            );

                        NewAmt = decimal.Parse(
                            Sm.GetValue(
                                SQL3.ToString()
                                .Replace("A.SSEmployerHealth", LastAmtErHealth.ToString())
                                .Replace("A.SSEmployerEmployment", LastAmtErEmployment.ToString())
                                .Replace("A.SSErPension", LastAmtErPension.ToString())
                                .Replace("A.SSEmployeeHealth", LastAmtEeHealth.ToString())
                                .Replace("A.SSEmployeeEmployment", LastAmtEeEmployment.ToString())
                                .Replace("A.SSEePension", LastAmtEePension.ToString()),
                                r.PayrunCode, r.EmpCode, string.Empty
                                )
                            );

                        NewNettoActual = NewNettoActual - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
                        NewBrutto = NewBrutto - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
                        NewNetto = NewNetto - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
                        NewAmt = NewAmt - mADCodePositionAdjustmentOldAmt + mADCodePositionAdjustmentAmt;
                    }
                }
            }

            #endregion

            #region Recalculate TPJ After BPJS

            //if (NewNetto > NewNettoActual) mADCodePositionAdjustmentAmt += (NewNetto - NewNettoActual);

            #endregion

            var SQL = new StringBuilder();

            SQL.AppendLine("/*PayrollProcessMultiPayrun18 - SavePayrollProcessPositionAdjustmentAndSocialSecurity */ ");

            SQL.AppendLine("Update TblPayrollProcess1 Set ");
            SQL.AppendLine("    Amt=@Amt, ");
            SQL.AppendLine("    Brutto=@Brutto, ");
            SQL.AppendLine("    Netto=@Netto, ");
            SQL.AppendLine("    NettoActual=@NettoActual, ");
            SQL.AppendLine("    FixAllowance=(FixAllowance - @ADCodePositionAdjustmentOldAmt + @ADCodePositionAdjustmentAmt) ");
            if (mIsPayrollProcessingCalculateBPJSAutomatic)
            {
                SQL.AppendLine("    ,SSEmployerHealth = @SSEmployerHealth, ");
                SQL.AppendLine("    SSEmployerEmployment = @SSEmployerEmployment, ");
                SQL.AppendLine("    SSErPension = @SSErPension, ");
                SQL.AppendLine("    SSEmployeeHealth = @SSEmployeeHealth, ");
                SQL.AppendLine("    SSEmployeeEmployment = @SSEmployeeEmployment, ");
                SQL.AppendLine("    SSEePension = @SSEePension ");
            }
            SQL.AppendLine("Where PayrunCode=@PayrunCode And EmpCode=@EmpCode; ");

            SQL.AppendLine("Insert Into TblPayrollProcessAD(PayrunCode, EmpCode, ADCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @PayrunCode, @EmpCode, @ADCodePositionAdjustment, @ADCodePositionAdjustmentAmt, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblParameter Where ParCode = 'ADCodePositionAdjustment' And ParValue Is Not Null ");
            SQL.AppendLine("On Duplicate Key Update ");
            SQL.AppendLine("    Amt = @ADCodePositionAdjustmentAmt, ");
            SQL.AppendLine("    LastUpBy = @CreateBy, ");
            SQL.AppendLine("    LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            decimal FinalBrutto = mIsPayrollAmtRounded ? Math.Truncate(NewBrutto) - r.FixAllowanceDeviationDueToPassedAway : NewBrutto - r.FixAllowanceDeviationDueToPassedAway;
            decimal FinalAmt = mIsPayrollAmtRounded ? Math.Truncate(NewAmt) - r.FixAllowanceDeviationDueToPassedAway : NewAmt - r.FixAllowanceDeviationDueToPassedAway;
            decimal FinalNettoActual = mIsPayrollAmtRounded ? Math.Truncate(NewNettoActual) - r.FixAllowanceDeviationDueToPassedAway : NewNettoActual - r.FixAllowanceDeviationDueToPassedAway;

            if (!mIsPayrollProcessingCalculateBPJSAutomatic)
            {
                decimal BruttoCheck = Decimal.Parse(Sm.GetValue(AmtBruttoQueryCheck, r.PayrunCode, r.EmpCode, string.Empty));
                decimal AmtCheck = Decimal.Parse(Sm.GetValue(AmtTHPQueryCheck, r.PayrunCode, r.EmpCode, string.Empty));
                decimal NettoCheck = Decimal.Parse(Sm.GetValue(AmtNettoQueryCheck, r.PayrunCode, r.EmpCode, string.Empty));

                if (!(FinalBrutto == BruttoCheck && FinalAmt == AmtCheck && FinalNettoActual == NettoCheck))
                {
                    if (FinalBrutto != BruttoCheck) FinalBrutto = BruttoCheck;
                    if (FinalAmt != AmtCheck) FinalAmt = AmtCheck;
                    if (FinalNettoActual != NettoCheck) FinalNettoActual = NettoCheck;
                }
            }

            // kalau nilai Final2 tersebut gak sesuai dengan rumus PayrollProcessFormula yang bagian checker (nanti akan dibikin data baru, isi query nya nanti menyesuaikan kolom2 pengecekkannya delivery), maka selisihnya itu langsung diinputkan ke masing2 final amount

            Sm.CmParam<String>(ref cm, "@PayrunCode", r.PayrunCode);
            Sm.CmParam<String>(ref cm, "@EmpCode", r.EmpCode);
            Sm.CmParam<String>(ref cm, "@ADCodePositionAdjustment", mADCodePositionAdjustment);
            Sm.CmParam<Decimal>(ref cm, "@ADCodePositionAdjustmentAmt", mIsPayrollAmtRounded ? Math.Truncate(mADCodePositionAdjustmentAmt) : mADCodePositionAdjustmentAmt);
            Sm.CmParam<Decimal>(ref cm, "@ADCodePositionAdjustmentOldAmt", mIsPayrollAmtRounded ? Math.Truncate(mADCodePositionAdjustmentOldAmt) : mADCodePositionAdjustmentOldAmt);
            //Sm.CmParam<Decimal>(ref cm, "@Amt", mIsPayrollAmtRounded ? Math.Truncate(NewAmt) - r.FixAllowanceDeviationDueToPassedAway : NewAmt - r.FixAllowanceDeviationDueToPassedAway);
            //Sm.CmParam<Decimal>(ref cm, "@Brutto", mIsPayrollAmtRounded ? Math.Truncate(NewBrutto) - r.FixAllowanceDeviationDueToPassedAway : NewBrutto - r.FixAllowanceDeviationDueToPassedAway);
            //Sm.CmParam<Decimal>(ref cm, "@NettoActual", mIsPayrollAmtRounded ? Math.Truncate(NewNettoActual) - r.FixAllowanceDeviationDueToPassedAway : NewNettoActual - r.FixAllowanceDeviationDueToPassedAway);
            Sm.CmParam<Decimal>(ref cm, "@Amt", FinalAmt);
            Sm.CmParam<Decimal>(ref cm, "@Brutto", FinalBrutto);
            Sm.CmParam<Decimal>(ref cm, "@NettoActual", FinalNettoActual);
            Sm.CmParam<Decimal>(ref cm, "@Netto", mIsPayrollAmtRounded ? Math.Truncate(NewNetto) - r.FixAllowanceDeviationDueToPassedAway : NewNetto - r.FixAllowanceDeviationDueToPassedAway);
            if (mIsPayrollProcessingCalculateBPJSAutomatic)
            {
                Sm.CmParam<Decimal>(ref cm, "@SSEmployerHealth", mIsPayrollAmtRounded ? Math.Round(LastAmtErHealth) : LastAmtErHealth);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployerEmployment", mIsPayrollAmtRounded ? Math.Round(LastAmtErEmployment) : LastAmtErEmployment);
                Sm.CmParam<Decimal>(ref cm, "@SSErPension", mIsPayrollAmtRounded ? Math.Round(LastAmtErPension) : LastAmtErPension);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployeeHealth", mIsPayrollAmtRounded ? Math.Round(LastAmtEeHealth) : LastAmtEeHealth);
                Sm.CmParam<Decimal>(ref cm, "@SSEmployeeEmployment", mIsPayrollAmtRounded ? Math.Round(LastAmtEeEmployment) : LastAmtEeEmployment);
                Sm.CmParam<Decimal>(ref cm, "@SSEePension", mIsPayrollAmtRounded ? Math.Round(LastAmtEePension) : LastAmtEePension);
            }
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            l.Clear();
            return cm;
        }

        private void AssignQueryValue(ref StringBuilder SQL,
            ref string AmtTHPQuery, ref string AmtNettoQuery, ref string AmtBruttoQuery,
            ref string AmtTHPQueryCheck, ref string AmtNettoQueryCheck, ref string AmtBruttoQueryCheck,
            ref string SSErHealthPercString, ref string SSErEmploymentPercString, ref string SSErPensionPercString,
            ref string SSErHealthSalaryMaxLimitString,
            ref string SSEeHealthPercString, ref string SSEeEmploymentPercString, ref string SSEePensionPercString
            )
        {
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocType", "Data" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        switch (Sm.DrStr(dr, c[0]))
                        {
                            case "AmtTHPQuery": AmtTHPQuery = Sm.DrStr(dr, c[1]); break;
                            case "AmtNettoQuery": AmtNettoQuery = Sm.DrStr(dr, c[1]); break;
                            case "AmtBruttoQuery": AmtBruttoQuery = Sm.DrStr(dr, c[1]); break;
                            case "AmtTHPQueryCheck": AmtTHPQueryCheck = Sm.DrStr(dr, c[1]); break;
                            case "AmtNettoQueryCheck": AmtNettoQueryCheck = Sm.DrStr(dr, c[1]); break;
                            case "AmtBruttoQueryCheck": AmtBruttoQueryCheck = Sm.DrStr(dr, c[1]); break;
                            case "SSErHealthPercString": SSErHealthPercString = Sm.DrStr(dr, c[1]); break;
                            case "SSErEmploymentPercString": SSErEmploymentPercString = Sm.DrStr(dr, c[1]); break;
                            case "SSErPensionPercString": SSErPensionPercString = Sm.DrStr(dr, c[1]); break;
                            case "SSErHealthSalaryMaxLimitString": SSErHealthSalaryMaxLimitString = Sm.DrStr(dr, c[1]); break;
                            case "SSEeHealthPercString": SSEeHealthPercString = Sm.DrStr(dr, c[1]); break;
                            case "SSEeEmploymentPercString": SSEeEmploymentPercString = Sm.DrStr(dr, c[1]); break;
                            case "SSEePensionPercString": SSEePensionPercString = Sm.DrStr(dr, c[1]); break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void GetSQLQuery(ref StringBuilder SQL)
        {
            SQL.AppendLine("Select 'AmtTHPQuery' As DocType, QueryBuild As Data From TblPayrollProcessFormula Where Code ='03' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AmtNettoQuery' As DocType, QueryBuild As Data From TblPayrollProcessFormula Where Code ='04' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AmtBruttoQuery' As DocType, QueryBuild As Data From TblPayrollProcessFormula Where Code ='05' ");

            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AmtTHPQueryCheck' As DocType, QueryBuild As Data From TblPayrollProcessFormula Where Code ='03c' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AmtNettoQueryCheck' As DocType, QueryBuild As Data From TblPayrollProcessFormula Where Code ='04c' ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select 'AmtBruttoQueryCheck' As DocType, QueryBuild As Data From TblPayrollProcessFormula Where Code ='05c' ");
            if (mIsPayrollProcessingCalculateBPJSAutomatic)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'SSErHealthPercString' As DocType, Sum(EmployerPerc) As Data From TblSS Where Find_In_Set(SSCode, (Select ParValue From TblParameter Where ParCode = 'SSCodeForHealth')) ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'SSErEmploymentPercString' As DocType, Sum(EmployerPerc) As Data From TblSS Where Find_In_Set(SSCode, (Select ParValue From TblParameter Where ParCode = 'SSCodeForEmployment')) ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'SSErPensionPercString' As DocType, Sum(EmployerPerc) As Data From TblSS Where Find_IN_Set(SSCode, (Select ParValue From TblParameter Where ParCode = 'ssforretiring')) ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'SSErHealthSalaryMaxLimitString' As DocType, Sum(SalaryMaxLimit) As Data From TblSS Where Find_In_Set(SSCode, (Select ParValue From TblParameter Where ParCode = 'SSCodeForHealth')) ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'SSEeHealthPercString' As DocType, Sum(EmployeePerc) As Data From TblSS Where Find_In_Set(SSCode, (Select ParValue From TblParameter Where ParCode = 'SSCodeForHealth')) ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'SSEeEmploymentPercString' As DocType, Sum(EmployeePerc) As Data From TblSS Where Find_In_Set(SSCode, (Select ParValue From TblParameter Where ParCode = 'SSCodeForEmployment')) ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select 'SSEePensionPercString' As DocType, Sum(EmployeePerc) As Data From TblSS Where Find_In_Set(SSCode, (Select ParValue From TblParameter Where ParCode = 'ssforretiring')) ");
            }
        }

        #endregion

        #region Process Data

        private void ProcessData1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select Distinct T1.EmpCode ");
            SQL.AppendLine("    From TblEmployeePPS T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select X.EmpCode, Max(X.StartDt) As StartDt ");
            SQL.AppendLine("        From TblEmployeePPS X ");
            SQL.AppendLine("        Where Exists ( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblEmployee T ");
            SQL.AppendLine("            Where T.EmpCode=X.EmpCode ");
            SQL.AppendLine("            And T.JoinDt<=@EndDt ");
            SQL.AppendLine("            And ( ");
            SQL.AppendLine("                T.ResignDt Is Null Or ");
            SQL.AppendLine("                (T.ResignDt Is Not Null And T.ResignDt>@StartDt) Or ");
            SQL.AppendLine("                ( ");
            SQL.AppendLine("                    T.ResignDt Is Not Null ");
            SQL.AppendLine("                    And T.ResignDt<=@StartDt ");
            SQL.AppendLine("                    And ( ");
            SQL.AppendLine("                        T.EmpCode In ( ");
            SQL.AppendLine("                            Select Distinct A.EmpCode ");
            SQL.AppendLine("                            From TblPPS A ");
            SQL.AppendLine("                            Inner Join TblPayrun B On B.Payruncode=@PayrunCode ");
            SQL.AppendLine("                            Inner Join TblEmployee C  ");
	        SQL.AppendLine("                                On A.EmpCode=C.EmpCode  ");
	        SQL.AppendLine("                                And C.ResignDt Is Not Null  ");
	        SQL.AppendLine("                                And C.ResignDt<=B.EndDt ");
	        SQL.AppendLine("                                And Date_Format(Date_Add(Str_To_Date(C.ResignDt, '%Y%m%d'), Interval 4 Month), '%Y%m%d')>B.StartDt ");
            SQL.AppendLine("                            Where A.CancelInd='N' ");
            SQL.AppendLine("                            And A.Status='A' ");
            SQL.AppendLine("                            And A.JobTransfer In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='JobTransferPassAway') ");
            SQL.AppendLine("                            ) ");

            //SQL.AppendLine("                        T.EmpCode In ( ");
            //SQL.AppendLine("                            Select Tbl2.EmpCode ");
            //SQL.AppendLine("                            From TblEmpSCIHdr Tbl1, TblEmpSCIDtl Tbl2 ");
            //SQL.AppendLine("                            Where Tbl1.DocNo=Tbl2.DocNo ");
            //SQL.AppendLine("                            And Tbl1.DocDt Between @StartDt And @EndDt ");
            //SQL.AppendLine("                            And (Tbl2.PayrunCode Is Null Or (Tbl2.PayrunCode Is Not Null And Tbl2.PayrunCode=@PayrunCode)) ");
            //SQL.AppendLine("                            And Tbl1.CancelInd='N' ");
            //SQL.AppendLine("                            ) Or ");

            //SQL.AppendLine("                        T.EmpCode In ( ");
            //SQL.AppendLine("                            Select EmpCode ");
            //SQL.AppendLine("                            From TblSalaryAdjustmentHdr ");
            //SQL.AppendLine("                            Where CancelInd='N' ");
            //SQL.AppendLine("                            And PaidDt Between @StartDt And @EndDt ");
            //SQL.AppendLine("                            And (PayrunCode Is Null Or (PayrunCode Is Not Null And PayrunCode=@PayrunCode)) ");
            //SQL.AppendLine("                            ) Or ");

            //SQL.AppendLine("                        T.EmpCode In ( ");
            //SQL.AppendLine("                            Select Tbl2.EmpCode ");
            //SQL.AppendLine("                            From TblSalaryAdjustment2Hdr Tbl1, TblSalaryAdjustment2Dtl Tbl2 ");
            //SQL.AppendLine("                            Where Tbl1.DocNo=Tbl2.DocNo ");
            //SQL.AppendLine("                            And Tbl1.PaidDt Between @StartDt And @EndDt ");
            //SQL.AppendLine("                            And (Tbl2.PayrunCode Is Null Or (Tbl2.PayrunCode Is Not Null And Tbl2.PayrunCode=@PayrunCode)) ");
            //SQL.AppendLine("                            And Tbl1.CancelInd='N' ");
            //SQL.AppendLine("                            ) ");

            SQL.AppendLine("                        ) ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("        And X.DeptCode=@DeptCode ");
            SQL.AppendLine("        And X.SystemType=@SystemType ");
            SQL.AppendLine("        And X.PayrunPeriod=@PayrunPeriod ");
            SQL.AppendLine("        And IfNull(X.PGCode, '')=IfNull(@PGCode, '') ");
            SQL.AppendLine("        And IfNull(X.SiteCode, '')=IfNull(@SiteCode, '') ");
            SQL.AppendLine("        And X.StartDt<=@EndDt ");
            SQL.AppendLine("        And (X.EndDt Is Null Or X.EndDt>=@StartDt)");
            SQL.AppendLine("        Group By X.EmpCode ");
            SQL.AppendLine("    ) T3 On T1.EmpCode=T3.EmpCode And T1.StartDt=T3.StartDt ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On B.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On B.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On B.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On B.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On B.SiteCode=J.SiteCode ");
            SQL.AppendLine("Order By A.EmpCode;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@SystemType", mSystemType);
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", mPayrunPeriod);
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        mlPayrunEmployee.Add(
                            new PayrunEmployee(){ EmpCode = Sm.DrStr(dr, c[0]) }
                            );
                }
                dr.Close();
            }
        }

        private void ProcessData2()
        {
            Cursor.Current = Cursors.WaitCursor;
            
            #region Get Table Data

            var lEmployeePPS = new List<EmployeePPS>();
            var lOT = new List<OT>();
            var lNTI = new List<NTI>();
            var lTI = new List<TI>();
            var lTI2 = new List<TI2>();
            var lPayrollProcessAD = new List<PayrollProcessAD>();

            ProcessOT(ref lOT);
            ProcessNTI(ref lNTI);
            ProcessTI(ref lTI);
            ProcessTI2(ref lTI2);
            ProcessAdvancePaymentProcess(ref mlAdvancePaymentProcess);

            #endregion

            var lResult1 = new List<Result1>();

            Process1(ref lResult1);
            Process2(ref lResult1);
            Process3(ref lResult1);

            if (lResult1.Count > 0)
            {
                lResult1.ForEach(r => 
                    { Process4(ref r); });
                lResult1.Sort(
                    delegate(Result1 r1a, Result1 r1b)
                    {
                        int f = r1a.EmpCode.CompareTo(r1b.EmpCode);
                        return f != 0 ? f : r1a.Dt.CompareTo(r1b.Dt);
                    });
            }

            var lResult2 = new List<Result2>();

            Process5(ref lResult2);

            lResult2.ForEach(r => { Process6(ref r, ref lResult1, ref lNTI, ref lTI, ref lTI2); });

            ProcessPayrollProcessAD(ref lPayrollProcessAD);

            #region Save Data

            var cml = new List<MySqlCommand>();
            var cml2 = new List<MySqlCommand>();
            var cml3 = new List<MySqlCommand>();
            var cml4 = new List<MySqlCommand>();
            var cml5 = new List<MySqlCommand>();

            cml.Add(DeletePayrollProcess());
            lResult2.ForEach(r => { cml.Add(SavePayrollProcess1(ref r, ref lPayrollProcessAD, ref lResult1)); });
            //lResult1.ForEach(r => { cml.Add(SavePayrollProcess2(ref r)); });

            cml.Add(SavePayrollProcess());

            if (mlAdvancePaymentProcess.Count > 0)
                cml.Add(SaveAdvancePaymentProcess(ref mlAdvancePaymentProcess));
                //mlAdvancePaymentProcess.ForEach(x => { cml.Add(SaveAdvancePaymentProcess(ref x)); });

            Sm.ExecCommands(cml);

            lResult2.ForEach(r =>{ cml2.Add(SavePayrollOTAmt(ref r)); });

            Sm.ExecCommands(cml2);

            lResult2.ForEach(r =>{ cml3.Add(SavePayrollProcessAmt(ref r, ref lNTI, ref lTI, ref lTI2)); });

            Sm.ExecCommands(cml3);

            lResult2.ForEach(r => { cml4.Add(SavePayrollProcessAmtAfterTax(ref r)); });

            Sm.ExecCommands(cml4);

            lResult2.ForEach(r => { cml5.Add(SavePayrollProcessPositionAdjustmentAndSocialSecurity(ref r)); });

            Sm.ExecCommands(cml5);

            #endregion
        }

        private void ProcessPayrollProcessAD(ref List<PayrollProcessAD> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var lTemp = new List<PayrollProcessAD>();
            string Filter = string.Empty, EmpCode = string.Empty, ADCode =string.Empty;

            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select A.EmpCode, A.ADCode, B.ADType, A.Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.AmtType='1' And B.ADType In ('A', 'D') ");
            SQL.AppendLine("Left Join TblParameter C On C.ParCode = 'IsPayrollAmtRounded' ");
            SQL.AppendLine("Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "ADType", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lTemp.Add(new PayrollProcessAD()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            ADType = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }

            if (lTemp.Count() > 0)
            {
                var lTemp2 =
                    from x in lTemp
                    group x by new { x.EmpCode, x.ADCode, x.ADType } into g
                    select new { g.Key.EmpCode, g.Key.ADCode, g.Key.ADType, Amt = g.Sum(x => x.Amt) };

                foreach (var i in lTemp2.OrderBy(o => o.EmpCode).OrderBy(o=>o.ADCode))
                {
                    l.Add(new PayrollProcessAD()
                    {
                        EmpCode = i.EmpCode,
                        ADCode = i.ADCode,
                        ADType = i.ADType,
                        Amt = i.Amt
                    });
                }
            }
        }

        private void Process1(ref List<Result1> lResult1)
        {
            var lDt = new List<string>();
            GetDt(ref lDt);

            var lEmployeePPS = new List<EmployeePPS>();
            ProcessEmployeePPS(ref lEmployeePPS);

            if (lDt.Count>0 && lEmployeePPS.Count>0)
            {
                var PayrunCodeTemp = mPayrunCode;
                var EmpCodeTemp = string.Empty;
                var DeptCodeTemp = string.Empty;
                var GrdLvlCodeTemp = string.Empty;
                var LevelCodeTemp = string.Empty;
                var EmploymentStatusTemp = string.Empty;
                var SystemTypeTemp = string.Empty;
                var PayrunPeriodTemp = string.Empty;
                var PGCodeTemp = string.Empty;
                var SiteCodeTemp = string.Empty;
                var PosCodeTemp = string.Empty;
                var HOIndTemp = string.Empty;
                var DivisionCodeTemp = string.Empty;

                for (int i = 0; i < mlPayrunEmployee.Count; i++)
                {
                    EmpCodeTemp = mlPayrunEmployee[i].EmpCode;
                    if (EmpCodeTemp.Length != 0)
                    {
                        foreach (string DtTemp in lDt)
                        {
                            DeptCodeTemp = string.Empty;
                            GrdLvlCodeTemp = string.Empty;
                            LevelCodeTemp = string.Empty;
                            EmploymentStatusTemp = string.Empty;
                            SystemTypeTemp = string.Empty;
                            PayrunPeriodTemp = string.Empty;
                            PGCodeTemp = string.Empty;
                            SiteCodeTemp = string.Empty;
                            PosCodeTemp = string.Empty;
                            HOIndTemp = string.Empty;
                            DivisionCodeTemp = string.Empty;

                            foreach (var x in 
                                lEmployeePPS
                                    .Where(Index => Sm.CompareStr(Index.EmpCode, EmpCodeTemp))
                                    .OrderByDescending(x=>x.StartDt)) 
                            {
                                if(Sm.CompareDtTm(DtTemp, x.StartDt)>=0)
                                {
                                    DeptCodeTemp = x.DeptCode;
                                    GrdLvlCodeTemp = x.GrdLvlCode;
                                    LevelCodeTemp = x.LevelCode;
                                    EmploymentStatusTemp = x.EmploymentStatus;
                                    SystemTypeTemp = x.SystemType;
                                    PayrunPeriodTemp = x.PayrunPeriod;
                                    PGCodeTemp = x.PGCode;
                                    SiteCodeTemp = x.SiteCode;
                                    PosCodeTemp = x.PosCode;
                                    HOIndTemp = x.HOInd;
                                    DivisionCodeTemp = x.DivisionCode;
                                    break;
                                };
                            }

                            lResult1.Add(new Result1()
                                {
                                    PayrunCode = PayrunCodeTemp,
                                    EmpCode = EmpCodeTemp,
                                    Dt = DtTemp,
                                    DeptCode=DeptCodeTemp,
                                    GrdLvlCode = GrdLvlCodeTemp,
                                    LevelCode = LevelCodeTemp,
                                    EmploymentStatus=EmploymentStatusTemp,
                                    SystemType=SystemTypeTemp,
                                    PayrunPeriod=PayrunPeriodTemp,
                                    PGCode = PGCodeTemp,
                                    SiteCode = SiteCodeTemp,
                                    PosCode=PosCodeTemp,
                                    _IsHOInd = HOIndTemp=="Y",
                                    ProcessInd = true,
                                    LatestPaidDt = string.Empty,
                                    JoinDt = string.Empty,
                                    ResignDt = string.Empty,
                                    WorkingDay = 0m,
                                    WSCode  = string.Empty,
                                    HolInd = false,
                                    HolidayIndex = mStdHolidayIndex,
                                    WSHolidayInd = false,
                                    WSIn1  = string.Empty,
                                    WSOut1  = string.Empty,
                                    WSIn2  = string.Empty,
                                    WSOut2  = string.Empty,
                                    WSIn3  = string.Empty,
                                    WSOut3  = string.Empty,
                                    OneDayInd = true,
                                    LateInd = false,
                                    ActualIn  = string.Empty,
                                    ActualOut  = string.Empty,
                                    WorkingIn  = string.Empty,
                                    WorkingOut  = string.Empty,
                                    WorkingDuration = 0m,
                                    BasicSalary = 0m,
                                    BasicSalary2 = 0m,
                                    _LatestMonthlyGrdLvlSalary = 0m,
                                    _LatestDailyGrdLvlSalary = 0m,
                                    ProductionWages = 0m,
                                    Salary = 0m,
                                    LeaveCode  = string.Empty,
                                    LeaveType = string.Empty,
                                    PaidLeaveInd = false,
                                    CompulsoryLeaveInd = false,
                                    _DeductTHPInd = false,
                                    LeaveStartTm  = string.Empty,
                                    LeaveEndTm  = string.Empty,
                                    LeaveDuration = 0m,
                                    PLDay = 0m,
                                    PLHr = 0m,
                                    PLAmt = 0m,
                                    ProcessPLAmt = 0m,
                                    OT1Hr = 0m,
                                    OT2Hr = 0m,
                                    OTHolidayHr = 0m,
                                    OT1Amt = 0m,
                                    OT2Amt = 0m,
                                    OTHolidayAmt = 0m,
                                    OTToLeaveInd = false,
                                    IncMinWages = 0m,
                                    IncProduction = 0m,
                                    IncPerformance = 0m,
                                    PresenceRewardInd = false,
                                    HolidayEarning = 0m,
                                    ExtraFooding = 0m,
                                    FieldAssignment = 0m,
                                    DedProduction = 0m,
                                    DedProdLeave = 0m,
                                    EmpSalary  = 0m,
                                    EmpSalary2 = 0m,
                                    SalaryPension = 0m,
                                    UPLDay  = 0m,
                                    UPLHr  = 0m,
                                    UPLAmt = 0m,
                                    ProcessUPLAmt = 0m,
                                    Transport = 0m,
                                    Meal = 0m,
                                    ServiceChargeIncentive = 0m,
                                    IncEmployee = 0m,
                                    ADOT = 0m,
                                    _ExtraFooding = 0m,
                                    _FieldAssignment = 0m,
                                    _IncPerformance = 0m,
                                    _IsFullDayInd = false,
                                    _IsUseLatestGrdLvlSalary = false,
                                    GoHomeEarlyInd = false,
                                    DedAtd = 0m,
                                    LateMinute = 0m,
                                    GoHomeEarlyMinute = 0m,
                                    OTSpecialInd = false,
                                    DivisionCode = DivisionCodeTemp,
                                    GetOTAmt = true,
                                    OTMeal = 0m,
                                    OTMonthlyIncome = 0m
                                });
                        }
                    }
                }
                lDt.Clear();
                lEmployeePPS.Clear();
            }
        }

        private void Process2(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            #region Employee's Work Schedule

            var lEmpWorkSchedule = new List<EmpWorkSchedule>();
            ProcessEmpWorkSchedule(ref lEmpWorkSchedule);
            if (lEmpWorkSchedule.Count > 0)
            {
                var WSCodesForHoliday = mWSCodesForHoliday.Split('#');
                var WSCodesForNationalHoliday = mWSCodesForNationalHoliday.Split('#');

               foreach (var ews in lEmpWorkSchedule)
               {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == ews.EmpCode && Index.Dt == ews.Dt))
                    {
                        r.WSCode = ews.WSCode;
                        r.WSHolidayInd = ews.HolidayInd=="Y";
                        r._DoubleShiftInd = ews.DoubleShiftInd == "Y";
                        r.WSIn1 = ews.In1;
                        r.WSOut1 = ews.Out1;
                        r.WSIn2 = ews.In2;
                        r.WSOut2 = ews.Out2;
                        r.WSIn3 = ews.In3;
                        r.WSOut3 = ews.Out3;
                    }
               }
               lEmpWorkSchedule.Clear();
            }

            #endregion

            #region Employee's Paid Date

            var lEmpPaidDt = new List<EmpPaidDt>();
            ProcessEmpPaidDt(ref lEmpPaidDt);
            if (lEmpPaidDt.Count > 0)
            {
                lResult1.ForEach(A =>
                {
                    foreach (var x in lEmpPaidDt.Where(x => x.EmpCode == A.EmpCode && x.Dt == A.Dt))
                    {
                        A.LatestPaidDt = x.Dt;
                        break;
                    }
                });
               lEmpPaidDt.Clear();
            }

            #endregion

            #region Attendance

            var lAtd = new List<Atd>();
            ProcessAtd(ref lAtd);
            if (lAtd.Count > 0)
            {
                foreach (var atd in lAtd)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == atd.EmpCode && Index.Dt == atd.Dt))
                    {
                        r.ActualIn = atd.ActualIn;
                        r.ActualOut = atd.ActualOut;
                        break;
                    }
                }
                lAtd.Clear();
            }

            #endregion

            #region Leave

            var lLeave = new List<Leave>();
            ProcessLeave(ref lLeave);

            var lLeaveDtl = new List<LeaveDtl>();
            ProcessLeaveDtl(ref lLeaveDtl);

            if (lLeave.Count > 0)
            {
                foreach (var l in lLeave)
                {
                    foreach (var r in lResult1.Where(Index => Index.EmpCode == l.EmpCode && Index.Dt == l.Dt))
                    {
                        r.LeaveCode = l.LeaveCode;
                        r.LeaveType = l.LeaveType;
                        r.LeaveStartTm = l.StartTm;
                        r.LeaveEndTm = l.EndTm;
                        r.LeaveDuration = l.DurationHr;
                        r.PaidLeaveInd = l.PaidInd=="Y";
                        r.CompulsoryLeaveInd = l.CompulsoryLeaveInd == "Y";
                        foreach (var ld in lLeaveDtl.Where(
                            Index => 
                                Index.LeaveCode == l.LeaveCode && 
                                Index.SystemType == r.SystemType &&
                                Index.PayrunPeriod == r.PayrunPeriod
                                ))
                        {
                            r._DeductTHPInd = ld.DeductTHPInd;
                        }
                        break;
                    }
                }
                lLeave.Clear();
                lLeaveDtl.Clear();
            }

            #endregion

            #region Employee

            var lEmployee = new List<Employee>();
            ProcessEmployee(ref lEmployee);
            if (lEmployee.Count > 0)
            {
                foreach (var e in lEmployee)
                {
                    foreach (var r in lResult1.Where(Index => Sm.CompareStr(Index.EmpCode, e.EmpCode)))
                    {
                        r.JoinDt = e.JoinDt;
                        r.ResignDt = e.ResignDt;
                        r._PassAwayInd = e.PassAwayInd;

                        if (Sm.CompareDtTm(r.Dt, r.JoinDt) < 0) r.ProcessInd = false;

                        if (r.ResignDt.Length > 0)
                        {
                            if (!r._PassAwayInd)
                            {
                                if (Sm.CompareDtTm(r.Dt, r.ResignDt) >= 0) r.ProcessInd = false;
                            }
                        }

                        if (r.LatestPaidDt.Length > 0) r.ProcessInd = false;
                        
                    }
                }
                lEmployee.Clear();
            }

            #endregion

            #region EmployeeSalary

            var lEmployeeSalary = new List<EmployeeSalary>();
            ProcessEmployeeSalary(ref lEmployeeSalary);
            if (lEmployeeSalary.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var esl in lEmployeeSalary
                        .Where(Index => Index.EmpCode == r.EmpCode)
                        .OrderByDescending(Index => Index.StartDt))
                    {
                        if (Sm.CompareDtTm(esl.StartDt, r.Dt) <= 0)
                        {
                            r.EmpSalary = esl.Amt;
                            r.EmpSalary2 = esl.Amt2;
                            break;
                        }
                    }
                }
                lEmployeeSalary.Clear();
            }

            #endregion

            #region EmployeeSalaryss

            var lEmployeeSalarySS = new List<EmployeeSalarySS>();
            ProcessEmployeeSalarySSPension(ref lEmployeeSalarySS);
            if (lEmployeeSalarySS.Count > 0)
            {
                foreach (var r in lResult1.OrderByDescending(Index => Index.Dt))
                {
                    foreach (var i in lEmployeeSalarySS
                        .Where(w => Sm.CompareStr(w.EmpCode, r.EmpCode))
                        .OrderByDescending(o => o.StartDt))
                    {
                        if (Sm.CompareDtTm(i.StartDt, r.Dt) <= 0)
                        {
                            r.SalaryPension = i.Amt;
                            break;
                        }
                    }
                }
                lEmployeeSalarySS.Clear();
            }

            #endregion

            //#region Employee Meal Transport

            //ProcessEmployeeMealTransport(ref mEmployeeMealTransport);
           
            //#endregion

            #region Attendance

            mlAttendanceLog.Clear();
            ProcessAttendanceLog(ref mlAttendanceLog);

            #endregion

            #region OT

            mlOT.Clear();
            ProcessOT(ref mlOT);

            #endregion

            //#region OTSpecial

            //ProcessOTSpecial(ref mlOTSpecial);

            //#endregion

            //#region OT Adjustment

            //mlOTAdjustment.Clear();
            //ProcessOTAdjustment(ref mlOTAdjustment);

            //#endregion

            //#region Employee Incentive

            //var lIncEmployee = new List<IncEmployee>();
            //ProcessIncEmployee(ref lIncEmployee);
            //if (lIncEmployee.Count > 0)
            //{
            //    foreach (var ie in lIncEmployee)
            //    {
            //        foreach (var r in lResult1.Where(Index => Index.EmpCode == ie.EmpCode && Index.Dt == ie.DocDt))
            //        {
            //            r.IncEmployee = ie.Amt;
            //            break;
            //        }
            //    }
            //    lIncEmployee.Clear();
            //}

            //#endregion

            //#region Employee's Service Charge Incentive

            //var lEmpSCI = new List<EmpSCI>();
            //ProcessEmpSCI(ref lEmpSCI);
            //if (lEmpSCI.Count > 0)
            //{
            //    foreach (var x in lEmpSCI)
            //    {
            //        foreach (var r in lResult1
            //            .Where(w => 
            //                Sm.CompareStr(w.EmpCode, x.EmpCode) && 
            //                Sm.CompareStr(w.Dt, x.DocDt)))
            //        {
            //            r.ServiceChargeIncentive = x.Amt;
            //            break;
            //        }
            //    }
            //    lEmpSCI.Clear();
            //}

            //#endregion

            //#region Employee Deduction (tunjangan  kehadiran) sekaligus tunjangan kehadiran untuk OT

            //ProcessEmployeeDeductionAtd(ref mlEmployeeDeductionAtd, mADCodePresenceReward);

            //#endregion

            #region Grade Salary

            ProcessGradeSalary(ref mlGradeSalary, ref lResult1);

            #endregion
        }

        private void Process3(ref List<Result1> lResult1)
        {
            if (lResult1.Count == 0) return;

            var EmpCodeTemp = string.Empty;
            var PayrunPeriodTemp = string.Empty;
            string 
                SystemTypeTemp = string.Empty, 
                GrdLvlCodeTemp = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCodeTemp = mlPayrunEmployee[i].EmpCode;
                if (EmpCodeTemp.Length != 0)
                {
                    SystemTypeTemp = string.Empty;
                    PayrunPeriodTemp = string.Empty;
                    GrdLvlCodeTemp = string.Empty;
                    
                    foreach (var x in lResult1.Where(x => string.Compare(x.EmpCode, EmpCodeTemp) == 0).OrderBy(x => x.Dt))
                    {
                        x.Salary = x.EmpSalary2; // Berdasarkan Employee Salary
                        
                        if (x.ProcessInd)
                        {
                            if (!(
                                Sm.CompareStr(x.DeptCode, mDeptCode) &&
                                Sm.CompareStr(x.SystemType, mSystemType) &&
                                Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                                Sm.CompareStr(x.PGCode, mPGCode) &&
                                Sm.CompareStr(x.SiteCode, mSiteCode)
                                ))
                                x.ProcessInd = false;       
                        }
                    }
                }
            }
            
        }

        private void Process4(ref Result1 r)
        {
            string
                ActualDtIn = string.Empty,
                ActualDtOut = string.Empty,
                ActualTmIn = string.Empty,
                ActualTmOut = string.Empty,
                EmpCode = r.EmpCode,
                DeptCode = r.DeptCode,
                SiteCode = r.SiteCode,
                LevelCode = r.LevelCode,
                DivisionCode = r.DivisionCode,
                Dt = r.Dt;

            var Value = 0m;

            #region Set Date2 untuk Shift 3

            var Dt2 = Sm.FormatDateTime(Sm.ConvertDateTime(r.Dt + "0000").AddDays(1)).Substring(0, 8);

            #endregion

            #region Set Join Date And Resign Date

            var JoinDt = Sm.ConvertDateTime(r.JoinDt + "0000");
            var ResignDt = Sm.ConvertDateTime("900012312359");
            if (r.ResignDt.Length > 0) ResignDt = Sm.ConvertDateTime(r.ResignDt + "2359");

            #endregion

            #region Set Actual Date/Time

            if (r.ActualIn.Length > 0)
            {
                ActualDtIn = Sm.Left(r.ActualIn, 8);
                ActualTmIn = r.ActualIn.Substring(8, 4);
            }

            if (r.ActualOut.Length > 0)
            {
                ActualDtOut = Sm.Left(r.ActualOut, 8);
                ActualTmOut = r.ActualOut.Substring(8, 4);
            }

            #endregion

            #region set shift 1,2 atau shift 3

            if (r.WSIn1.Length > 0 && 
                r.WSOut1.Length > 0 && 
                Sm.CompareDtTm(r.WSIn1, r.WSOut1)>0)
                r.OneDayInd = false;

            #endregion

            #region cek apakah karyawan terlambat

            if (ActualTmIn.Length > 0 && r.WSIn1.Length > 0)
            {
                if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                {
                    r.LateInd = true;
                    r.LateMinute = Convert.ToDecimal(string.Concat(ActualDtIn + ActualTmIn))
                        - Convert.ToDecimal(string.Concat(r.Dt + r.WSIn1));
                }
            }

            #endregion

            #region cek apakah karyawan pulang lebh cepat

            if (ActualTmOut.Length > 0 && r.WSOut1.Length > 0)
            {
                if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                {
                    r.GoHomeEarlyInd = true;
                    r.GoHomeEarlyMinute = Convert.ToDecimal(string.Concat(ActualDtOut + ActualTmOut))
                        - Convert.ToDecimal(string.Concat(r.Dt + r.WSOut1));
                }
            }

            #endregion


            #region cek apakah ada leave terlambat atau leave pulang cepat

            if (r.LateInd)
            {
                r.DedAtdIndex = 0m;
                if (r.LeaveCode.Length > 0 && mLateLeaveCode.Contains(r.LeaveCode)) // r.LeaveCode == mLateLeaveCode)
                {
                    r.DedAtdIndex = 0m;
                }
                else
                {
                    if (r.LateMinute > 0 && r.LateMinute <= 30)
                    {
                        r.DedAtdIndex = 0.02m;
                    }
                    else
                    {
                        r.DedAtdIndex = 0.04m;
                    }
                }
            }

            if (r.GoHomeEarlyInd == true)
            {
                if (r.LeaveCode.Length > 0 && mGoHomeEarlyLeaveCode.Contains(r.LeaveCode)) // r.LeaveCode == mGoHomeEarlyLeaveCode)
                {
                    r.DedAtd = 0m;
                }
                else
                {
                    r.DedAtdIndex = r.DedAtdIndex + 0.04m; //kalau di pagi hari dia telat
                }
            }


            #endregion


            #region set jam masuk

            if (r.Dt.Length > 0 && r.WSIn1.Length > 0 && ActualDtIn.Length > 0 && ActualTmIn.Length > 0)
            {
                if (mIsWorkingHrBasedOnSchedule)
                {
                    r.WorkingIn = r.Dt + r.WSIn1;
                }
                else
                {
                    if (Sm.CompareDtTm(r.Dt + r.WSIn1, ActualDtIn + ActualTmIn) < 0)
                        r.WorkingIn = ActualDtIn + ActualTmIn;
                    else
                        r.WorkingIn = r.Dt + r.WSIn1;
                }
            }

            #endregion

            #region set jam keluar

            if (r.Dt.Length > 0 && r.WSOut1.Length > 0 && ActualDtOut.Length > 0 && ActualTmOut.Length > 0)
            {
                if (r.OneDayInd)
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = r.Dt + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(r.Dt + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = r.Dt + r.WSOut1;
                    }
                }
                else
                {
                    if (mIsWorkingHrBasedOnSchedule)
                    {
                        r.WorkingOut = Dt2 + r.WSOut1;
                    }
                    else
                    {
                        if (Sm.CompareDtTm(Dt2 + r.WSOut1, ActualDtOut + ActualTmOut) > 0)
                            r.WorkingOut = ActualDtOut + ActualTmOut;
                        else
                            r.WorkingOut = Dt2 + r.WSOut1;
                    }
                }
            }

            #endregion

            #region set OT rutin

            string ROT1 = string.Empty, ROT2 = string.Empty;

            #region Set OT Rutin (In)

            if (ActualDtIn.Length > 0 && ActualTmIn.Length > 0 && r.WSIn3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    //Shift 1, 2
                    if (Sm.CompareDtTm(r.Dt + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //1500 < 1600
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //1500 > 1400
                        ROT1 = r.Dt + r.WSIn3;
                    }
                }
                else
                {
                    //Shift 3
                    if (Sm.CompareDtTm(Dt2 + r.WSIn3, ActualDtIn + ActualTmIn) < 0)
                    {
                        //0400<0500
                        ROT1 = ActualDtIn + ActualTmIn;
                    }
                    else
                    {
                        //0400>0300
                        ROT1 = Dt2 + r.WSIn3;
                    }
                }
            }

            #endregion

            #region Set OT Rutin (Out)

            if (ActualDtOut.Length > 0 && ActualTmOut.Length > 0 && r.WSOut3.Length > 0)
            {
                if (r.OneDayInd)
                {
                    ////Shift 1, 2
                    if (Sm.CompareDtTm(r.Dt + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                    {
                        //1900<2000
                        ROT2 = r.Dt + r.WSOut3;
                    }
                    else
                    {
                        //1900>1800
                        ROT2 = ActualDtOut + ActualTmOut;
                    }
                }
                else
                {
                    //Shift 3
                    if (Sm.CompareDtTm(Dt2 + r.WSOut3, ActualDtOut + ActualTmOut) < 0)
                    {
                        //0300<0400
                        ROT2 = Dt2 + r.WSOut3;
                    }
                    else
                    {
                        //0300>0200
                        ROT2 = ActualDtOut + ActualTmOut;
                    }
                }
            }

            #endregion

            #endregion

            #region set OT rutin duration

            var ROTDuration = 0m;
            if (ROT1.Length > 0 && ROT2.Length > 0)
            {
                ROTDuration = ComputeDuration(ROT2, ROT1);
                if (ROTDuration < 0) ROTDuration = 0m;
                if (mIsOTRoutineRounding && ROTDuration > 0)
                {
                    ROTDuration = RoundTo1Hour((double)ROTDuration);
                }
            }

            #endregion

            #region set working duration

            decimal 
                WorkingDuration = 0m, 
                BreakDuration = 0m;

            if (r.WorkingIn.Length > 0 && r.WorkingOut.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    //Full Day Leave
                    WorkingDuration = 0m;
                    ROTDuration = 0m;
                }
                else
                {
                    if (r.LeaveType.Length == 0)
                    {
                        //No Leave
                        WorkingDuration = ComputeDuration(r.WorkingOut, r.WorkingIn) + ROTDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                        {
                            // Menghitung lama waktu istirahat
                            if (r.OneDayInd)
                            {
                                // Shift 1, 2
                                if (Sm.CompareDtTm(r.WorkingOut, r.Dt+r.WSIn2) > 0)
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                            }
                            else
                            {
                                //Shift 3
                                if (Sm.CompareDtTm(r.WSIn2, r.WSOut2)<=0)
                                    BreakDuration = ComputeDuration(r.Dt + r.WSOut2, r.Dt + r.WSIn2);
                                else
                                    BreakDuration = ComputeDuration(Dt2 + r.WSOut2, r.Dt + r.WSIn2);
                            }
                        }
                        WorkingDuration -= BreakDuration;
                    }
                    else
                    {
                        //Hourly Leave
                        string
                            LeaveStartDtTm = string.Empty,
                            LeaveEndDtTm = string.Empty,
                            BreakStartDtTm = string.Empty,
                            BreakEndDtTm = string.Empty;

                        if (r.OneDayInd)
                        {
                            //shift 1, 2
                            LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                            LeaveEndDtTm = r.Dt + r.LeaveEndTm;
                            BreakStartDtTm = r.Dt + r.WSIn2;
                            BreakEndDtTm = r.Dt + r.WSOut2;
                        }
                        else
                        {
                            //Shift 3
                            if (Sm.CompareDtTm(r.LeaveStartTm, r.WSIn1) < 0)
                            {
                                LeaveStartDtTm = Dt2 + r.LeaveStartTm;
                                LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                            }
                            else
                            {
                                LeaveStartDtTm = r.Dt + r.LeaveStartTm;
                                if (Sm.CompareDtTm(r.LeaveEndTm, r.WSOut1) <= 0)
                                    LeaveEndDtTm = Dt2 + r.LeaveEndTm;
                                else
                                    LeaveEndDtTm = Dt + r.LeaveEndTm;
                            }

                            if (Sm.CompareDtTm(r.WSIn2, r.WSIn1) < 0)
                                BreakStartDtTm = Dt2 + r.WSIn2;
                            else
                                BreakStartDtTm = Dt + r.WSIn2;
                            
                            if (Sm.CompareDtTm(r.WSOut2, r.WSOut1) < 0)
                                BreakEndDtTm = Dt2 + r.WSOut2;                            
                            else
                                BreakEndDtTm = Dt + r.WSOut2;
                        }
                        if (r.OneDayInd)
                        {
                            if (r.WSOut3.Length > 0)
                            {
                                if (mSalaryInd == "2")
                                {
                                    WorkingDuration = ComputeDuration(Dt + r.WSOut3, Dt + r.WSIn1) + ROTDuration;
                                }
                                else
                                {
                                    WorkingDuration = ComputeDuration(Dt + r.WSOut1, Dt + r.WSIn1);
                                }
                            }
                            else
                                WorkingDuration = ComputeDuration(Dt + r.WSOut1, Dt + r.WSIn1);
                        }
                        else
                        {
                            if (r.WSOut3.Length > 0)
                            {
                                if (mSalaryInd == "2")
                                {
                                    WorkingDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn1) + ROTDuration;
                                }
                                else
                                {
                                    WorkingDuration = ComputeDuration(Dt2 + r.WSOut3, Dt + r.WSIn1);
                                }
                            }
                            else
                                WorkingDuration = ComputeDuration(Dt2 + r.WSOut1, Dt + r.WSIn1);
                        }
                        //WorkingDuration += ROTDuration;
                        WorkingDuration -= r.LeaveDuration;
                        if (r.WSIn2.Length > 0 && r.WSOut2.Length > 0)
                            WorkingDuration -= ComputeDuration(BreakEndDtTm, BreakStartDtTm);
                    }
                }
            }

            if (WorkingDuration > 1000)
            {
                var a = "";
            }

            r.WorkingDuration = Convert.ToDecimal(WorkingDuration);

            #endregion

            #region set leave duration (fullday leave)

            if (r.LeaveType == "F")
            {
                if (!r.WSHolidayInd && !r.HolInd)
                    r.LeaveDuration = mWorkingHr;
                else
                {
                    r.LeaveCode = string.Empty;
                    r.LeaveType = string.Empty;
                    r.PaidLeaveInd = false;
                    r.CompulsoryLeaveInd = false;
                    r.LeaveDuration = 0m;
                }
            }
            
            #endregion

            #region cek apakah ada employee(berdasarkan level) dapat OT

            if (mListOfLevelCodeDontGetOTAmt.Contains(r.LevelCode))
                r.GetOTAmt = false;
            else
                r.GetOTAmt = true;

            #endregion

            #region Menghitung OT

            decimal TotalOTHr = 0m;
            decimal OTHr = 0m;
            string 
                StartTm = string.Empty, 
                EndTm = string.Empty;

                TotalOTHr += ROTDuration;
                foreach (var x in mlOT.Where(x => x.EmpCode == EmpCode && x.Dt == Dt))
                {
                    OTHr = 0m;
                    StartTm = x.Tm1;
                    EndTm = x.Tm2;
                    r.OTSpecialInd = x.OTSpecialInd == "Y"? true : false;

                    if (Dt.Length > 0 && EndTm.Length > 0 && StartTm.Length > 0)
                    {
                        if (EndTm == "0000")
                            OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                        else
                        {
                            if (Sm.CompareDtTm(StartTm, EndTm) <= 0)
                                OTHr = ComputeDuration(Dt + EndTm, Dt + StartTm);
                            else
                                OTHr = ComputeDuration(Dt2 + EndTm, Dt + StartTm);
                        }
                    }
                    if (OTHr < 0) OTHr = 0;
                    TotalOTHr += OTHr;
          
                
            }


            #endregion

            #region Set leave information (PLDay, PLHr, PLAmt)

            if (r.PaidLeaveInd && r.LeaveType.Length>0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.PLDay = 1;
                }
                else
                    r.PLHr = r.LeaveDuration;

                if (r.LeaveDuration != 0m && mWorkingHr != 0m)
                {
                    r.PLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                }
                if (r.PLAmt > 0)
                    r.ProcessPLAmt = r.PLAmt;
            }

            if (!r.PaidLeaveInd && r.LeaveType.Length > 0)
            {
                if (r.LeaveType == "F")
                {
                    if (!r.WSHolidayInd && !r.HolInd)
                        r.UPLDay = 1;
                }
                else
                    r.UPLHr = r.LeaveDuration;

                if (r.LeaveDuration != 0m && mWorkingHr != 0m) 
                    r.UPLAmt = r.EmpSalary2 * (r.LeaveDuration / mWorkingHr);
                
                if (r._DeductTHPInd)
                    r.ProcessUPLAmt = r.UPLAmt;
            }

            #endregion

            #region Menghitung OT Amount

            if (TotalOTHr >= 0.5m)
            {
                decimal TotalOTHrTemp = TotalOTHr;

                if (r.HolInd || r.WSHolidayInd)
                    r.OTHolidayHr = TotalOTHrTemp;
                else
                {
                    if (TotalOTHrTemp <= mOTFormulaHr1)
                    {
                        r.OT1Hr = TotalOTHrTemp;
                        TotalOTHrTemp = 0m;
                    }
                    else
                    {
                        r.OT1Hr = mOTFormulaHr1;
                        TotalOTHrTemp -= mOTFormulaHr1;
                    }

                    if (TotalOTHrTemp > 0m && TotalOTHrTemp <= mOTFormulaHr2)
                    {
                        r.OT2Hr = TotalOTHrTemp;
                        TotalOTHrTemp = 0m;
                    }
                }
            }
            

            //if (r.EmpSalary != 0m && TotalOTHr >= 0.5m)
            //{
            //    decimal TotalOTHrTemp = TotalOTHr;
            //    if (r.OTSpecialInd)
            //    {
            //        foreach (var x in mlOTSpecial.Where(x =>
            //            x.DeptCode == DeptCode &&
            //            x.DivisionCode == DivisionCode &&
            //            x.SiteCode == SiteCode &&
            //            x.SourceCode == LevelCode
            //            ))
            //        {
            //            r.OT2Hr = TotalOTHrTemp;
            //            r.OT2Amt = TotalOTHrTemp * x.OTAmt;
            //            if (TotalOTHrTemp >= 3)
            //                r.Meal = x.MealAmt;
            //        }
            //    }
            //    else
            //        //if (r.GetOTAmt == true)
            //        //{
            //        if (r.HolInd || r.WSHolidayInd)
            //        {
            //            if (r.WSCode == mWSCodeForHoliday)
            //            {
            //                if (r.GetOTAmt == true)
            //                    r.OTHolidayHr = TotalOTHrTemp;

            //                //if (r.GetOTAmt == true)
            //                //    r.OTHolidayAmt = mHoliday1stHrIndex * TotalOTHrTemp * ((r.EmpSalary - r.SalaryPension) / 173);

            //                //foreach (var x in mlEmployeeDeductionAtd.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
            //                //{
            //                //    if (TotalOTHr >= 3)
            //                //    {
            //                //        r.ADOT = x.Amt / 20;
            //                //    }
            //                //}
            //            }
            //            else if (r.WSCode == mWSCodeForNationalHoliday)
            //            {
            //                r.OTHolidayHr = TotalOTHrTemp;

            //                int i = 1;

            //                while (TotalOTHrTemp > 0)
            //                {
            //                    if (i == 1)
            //                    {
            //                        if (TotalOTHrTemp >= mHoliday1stHrForHO)
            //                        {
            //                            //r.OTHolidayAmt = mHoliday1stHrForHO * mHoliday1stHrIndex * ((r.EmpSalary - r.SalaryPension) / 173);
            //                            TotalOTHrTemp -= mHoliday1stHrForHO;
            //                        }
            //                        else
            //                        {
            //                            //r.OTHolidayAmt = TotalOTHrTemp * mHoliday1stHrIndex * ((r.EmpSalary - r.SalaryPension) / 173);
            //                            TotalOTHrTemp = 0m;
            //                        }
            //                    }
            //                    if (i == 2)
            //                    {
            //                        if (TotalOTHrTemp >= mHoliday2ndHrForHO)
            //                        {
            //                            //r.OTHolidayAmt += (mHoliday2ndHrForHO * mHoliday2ndHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
            //                            TotalOTHrTemp -= mHoliday2ndHrForHO;
            //                        }
            //                        else
            //                        {
            //                            //r.OTHolidayAmt += (TotalOTHrTemp * mHoliday2ndHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
            //                            TotalOTHrTemp = 0m;
            //                        }
            //                    }
            //                    if (i == 3)
            //                    {
            //                        if (TotalOTHrTemp >= mHoliday3rdHrForHO)
            //                        {
            //                            //r.OTHolidayAmt += (mHoliday3rdHrForHO * mHoliday3rdHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
            //                            TotalOTHrTemp -= mHoliday3rdHrForHO;
            //                        }
            //                        else
            //                        {
            //                            //r.OTHolidayAmt += (TotalOTHrTemp * mHoliday3rdHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
            //                            TotalOTHrTemp = 0m;
            //                        }
            //                    }
            //                    if (i == 4)
            //                    {
            //                        //r.OTHolidayAmt += (TotalOTHrTemp * mHoliday4thHrIndex * ((r.EmpSalary - r.SalaryPension) / 173));
            //                        TotalOTHrTemp = 0m;
            //                    }
            //                    i++;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if (r.GetOTAmt == true)
            //            {
            //                if (TotalOTHrTemp <= mOTFormulaHr1)
            //                {
            //                    r.OT1Hr = TotalOTHrTemp;
            //                    //r.OT1Amt = TotalOTHrTemp * mOTFormulaIndex1 * ((r.EmpSalary - r.SalaryPension) / 173);
            //                    TotalOTHrTemp = 0m;
            //                }
            //                else
            //                {
            //                    r.OT1Hr = mOTFormulaHr1;
            //                    //r.OT1Amt = mOTFormulaHr1 * mOTFormulaIndex1 * ((r.EmpSalary - r.SalaryPension) / 173);
            //                    TotalOTHrTemp -= mOTFormulaHr1;
            //                }

            //                if (TotalOTHrTemp > 0m && TotalOTHrTemp <= mOTFormulaHr2)
            //                {
            //                    r.OT2Hr = TotalOTHrTemp;
            //                    //r.OT2Amt = TotalOTHrTemp * mOTFormulaIndex2 * ((r.EmpSalary - r.SalaryPension) / 173);
            //                    TotalOTHrTemp = 0m;
            //                }
            //            }
            //            foreach (var x in mlEmployeeDeductionAtd.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
            //            {
            //                if (TotalOTHr >= 3)
            //                {
            //                    r.ADOT = x.Amt / 20;
            //                }
            //            }
            //        }
            //    //}
            //}

            #endregion

            #region menghitung OT Meal

            if (r.HolInd || r.WSHolidayInd)
                if (r.OTHolidayHr > 4m) r.OTMeal = mOTHolidayMeal;

            #endregion

            #region Compute deduction attendance (ptotongan kehadiran)

            //if (r.LateInd == true || r.GoHomeEarlyInd == true)
            //{
            //    foreach (var i in mlEmployeeDeductionAtd.Where(w => Sm.CompareStr(w.EmpCode, EmpCode)))
            //    {
            //        r.DedAtd += r.DedAtdIndex * i.Amt;
            //    }
            //}

            #endregion
            
            #region Unprocessed data

            if (!r.ProcessInd)
            {
                r._LatestMonthlyGrdLvlSalary = 0m;
                r._LatestDailyGrdLvlSalary = 0m;
                r._IsFullDayInd = false;
                r.ActualIn = string.Empty;
                r.ActualOut = string.Empty;
                r.WorkingIn = string.Empty;
                r.WorkingOut = string.Empty;
                r.WorkingDuration = 0m;
                r.ProductionWages = 0m;
                r.LeaveCode = string.Empty;
                r.LeaveType = string.Empty;
                r.PaidLeaveInd = false;
                r.CompulsoryLeaveInd = false;
                r.LeaveStartTm = string.Empty;
                r.LeaveEndTm = string.Empty;
                r.LeaveDuration = 0m;
                r.PLDay = 0m;
                r.PLHr = 0m;
                r.PLAmt  = 0m;
                r.UPLDay = 0m;
                r.UPLHr = 0m;
                r.UPLAmt = 0m;
                r.ProcessPLAmt = 0m;
                r.OT1Hr  = 0m;
                r.OT2Hr  = 0m;
                r.OTHolidayHr  = 0m;
                r.OT1Amt = 0m;
                r.OT2Amt  = 0m;
                r.OTHolidayAmt  = 0m;
                r.OTToLeaveInd  = false;
                r.IncMinWages  = 0m;
                r.IncPerformance  = 0m;
                r.FieldAssignment = 0m;
                r.Meal = 0m;
                r.Transport = 0m;
                //r.ServiceChargeIncentive = 0m;
                r.DedProduction  = 0m;
                r.DedProdLeave  = 0m;
                r.IncEmployee = 0m;
            }

            #endregion
        }

        private void Process5(ref List<Result2> r2)
        {
            string
                PayrunCode = mPayrunCode,
                Filter = string.Empty;
            var cm = new MySqlCommand();

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                if (Filter.Length>0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), mlPayrunEmployee[i].EmpCode);                        
            }
            
            if (Filter.Length>0)
                Filter = " (" + Filter + ")";
            else
                Filter = " (0=1) ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, A.NPWP, A.PTKP, ");
            SQL.AppendLine("IfNull(B.SalaryAdjustment, 0) As SalaryAdjustment, ");
            SQL.AppendLine("IfNull(C.SSEmployerHealth, 0) As SSEmployerHealth, ");
            SQL.AppendLine("IfNull(C.SSEmployeeHealth, 0) As SSEmployeeHealth, ");
            SQL.AppendLine("IfNull(D.SSEmployerEmployment, 0) As SSEmployerEmployment, ");
            SQL.AppendLine("IfNull(D.SSEmployeeEmployment, 0) As SSEmployeeEmployment, ");
            SQL.AppendLine("IfNull(E.SSEmployerPension, 0.00) As SSEmployerPension, ");
            SQL.AppendLine("IfNull(E.SSEmployeePension, 0.00) As SSEmployeePension, ");
            SQL.AppendLine("IfNull(F.SSErLifeInsurance, 0.00) As SSErLifeInsurance, ");
            SQL.AppendLine("IfNull(F.SSEeLifeInsurance, 0.00) As SSEeLifeInsurance, ");
            SQL.AppendLine("IfNull(G.SSErWorkingAccident, 0.00) As SSErWorkingAccident, ");
            SQL.AppendLine("IfNull(G.SSEeWorkingAccident, 0.00) As SSEeWorkingAccident, ");
            SQL.AppendLine("IfNull(H.SSErRetirement, 0.00) As SSErRetirement, ");
            SQL.AppendLine("IfNull(H.SSEeRetirement, 0.00) As SSEeRetirement, ");
            SQL.AppendLine("IfNull(I.SSErPension, 0.00) As SSErPension, ");
            SQL.AppendLine("IfNull(I.SSEePension, 0.00) As SSEePension, ");
            SQL.AppendLine("IfNull(J.Amt, 0.00) As Functional, ");
            SQL.AppendLine("K.Value As PerformanceValue, ");
            SQL.AppendLine("IfNull(L.Amt, 0.00) As FixAllowance, ");
            SQL.AppendLine("IfNull(Abs(L.TAmtOutside - L.TAmtInside), 0.00) As FixAllowanceDeviationDueToPassedAway, ");
            SQL.AppendLine("IfNull(M.Amt, 0.00) As FixDeduction, ");
            SQL.AppendLine("IfNull(N.TaxableFixedAllowance, 0.00) As TaxableFixAllowance, ");
            SQL.AppendLine("IfNull(O.SSEmployerBNILife, 0.00) As SSEmployerBNILife, ");
            SQL.AppendLine("IfNull(O.SSEmployeeBNILife, 0.00) As SSEmployeeBNILife, ");
            SQL.AppendLine("IfNull(P.EmployerAmt, 0.00) As SSEmployerNonBPJS, ");
            SQL.AppendLine("IfNull(P.EmployeeAmt, 0.00) As SSEmployeeNonBPJS, ");
            SQL.AppendLine("IfNull(G.SSEeWorkingAccident, 0.00) As SSEeWorkingAccident, ");
            SQL.AppendLine("IfNull(Q.WarningLetter, 0.00) As WarningLetter, ");
            SQL.AppendLine("IfNull(R.ServiceYearAllowance, 0.00) As ServiceYearAllowance, ");
            SQL.AppendLine("Case When IfNull(S.Amt, 0.00)>0.00 Then 'Y' Else 'N' End As IsADSMKExisted, ");
            SQL.AppendLine("IfNull(T.SSEmployerDPNPHT, 0.00) As SSEmployerDPNPHT, ");
            SQL.AppendLine("IfNull(T.SSEmployeeDPNPHT, 0.00) As SSEmployeeDPNPHT, ");
            SQL.AppendLine("IfNull(U.SSEmployerDPNPNS, 0.00) As SSEmployerDPNPNS, ");
            SQL.AppendLine("IfNull(U.SSEmployeeDPNPNS, 0.00) As SSEmployeeDPNPNS, ");
            SQL.AppendLine("IfNull(V.SSEmployerPKES, 0.00) As SSEmployerPKES, ");
            SQL.AppendLine("IfNull(V.SSEmployeePKES, 0.00) As SSEmployeePKES, ");
            SQL.AppendLine("IfNull(W.SSEmployerPRMH, 0.00) As SSEmployerPRMH, ");
            SQL.AppendLine("IfNull(W.SSEmployeePRMH, 0.00) As SSEmployeePRMH, ");
            SQL.AppendLine("IfNull(X.SSEmployerTSP, 0.00) As SSEmployerTSP, ");
            SQL.AppendLine("IfNull(X.SSEmployeeTSP, 0.00) As SSEmployeeTSP, ");
            SQL.AppendLine("IfNull(Y.SSEmployerWNR, 0.00) As SSEmployerWNR, ");
            SQL.AppendLine("IfNull(Y.SSEmployeeWNR, 0.00) As SSEmployeeWNR ");
            SQL.AppendLine("From TblEmployee A ");

            #region Salary Adjustment

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.EmpCode, Sum(T.Amt) As SalaryAdjustment ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustmentHdr T1 ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        And (T1.PayrunCode Is Null Or (T1.PayrunCode Is Not Null And T1.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T1.") + ") ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T2.EmpCode, T1.Amt ");
            SQL.AppendLine("        From TblSalaryAdjustment2Hdr T1 ");
            SQL.AppendLine("        Inner Join TblSalaryAdjustment2Dtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("            And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("            And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("        Where T1.PaidDt Between @StartDt And @EndDt ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.EmpCode ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode ");

            #endregion

            #region SS Health

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerHealth, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeHealth ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.SSPCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForHealth' And ParValue Is Not Null ");
            SQL.AppendLine("            ) ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") C On A.EmpCode=C.EmpCode ");

            #endregion
            
            #region SS Employment (dikurangi BPJS Pensiun)

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerEmployment, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeEmployment ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode))");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    And Not Find_in_set(T2.SSCode, ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("            )) ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And Find_in_set(T1.SSPCode, ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSPCodeForEmployment' And ParValue is Not Null ");
            SQL.AppendLine("            )) ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");

            #endregion

            #region DPLK

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerPension, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForDPLK' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") E On A.EmpCode=E.EmpCode ");

            #endregion

            #region BPJS Jaminan Kematian

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErLifeInsurance, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeLifeInsurance ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSLifeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");

            #endregion

            #region BPJS Jaminan Kecelakaan Kerja

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErWorkingAccident, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeWorkingAccident ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSWorkingAccident' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") G On A.EmpCode=G.EmpCode ");

            #endregion

            #region BPJS Jaminan Hari Tua

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErRetirement, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEeRetirement ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And T2.SSCode In ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSOldAgeInsurance' And ParValue is Not Null ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") H On A.EmpCode=H.EmpCode ");

            #endregion

            #region BPJS Jaminan Pensiun

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSErPension, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEePension ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("    And Find_in_set(T2.SSCode, ( ");
            SQL.AppendLine("            Select ParValue From TblParameter ");
            SQL.AppendLine("            Where ParCode='SSForRetiring' And ParValue is Not Null ");
            SQL.AppendLine("            )) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") I On A.EmpCode=I.EmpCode ");

            #endregion

            #region Functional

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeFunctional' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") J On A.EmpCode=J.EmpCode ");

            #endregion

            #region Grade Performance

            SQL.AppendLine("Left Join TblEmployeePerformance K On A.EmpCode=K.EmpCode ");

            #endregion

            #region Fixed Allowance

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt, Truncate(Sum(A.Amt), 0) TAmtOutside, Sum(Truncate(A.Amt, 0)) TAmtInside ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") L On A.EmpCode=L.EmpCode ");

            #endregion

            #region Fixed Deduction

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='D' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") M On A.EmpCode=M.EmpCode ");

            #endregion

            #region Taxable Fixed Allowance

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As TaxableFixedAllowance ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction B On A.ADCode=B.ADCode And B.ADType='A' And B.TaxInd='Y' And B.AmtType='1' ");
            SQL.AppendLine("    Where ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And (" + Filter + ") ");
            SQL.AppendLine("    Group BY A.EmpCode ");
            SQL.AppendLine(") N On A.EmpCode=N.EmpCode ");

            #endregion

            #region SS BNI Life/DPN

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerBNILife, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeBNILife ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForBNILife' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") O On A.EmpCode=O.EmpCode ");

            #endregion

            #region Non BPJS

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As EmployerAmt, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As EmployeeAmt ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And Not Find_In_Set( ");
            SQL.AppendLine("    IfNull(SSPCode, ''), ");
            SQL.AppendLine("    IfNull((Select ParValue From TblParameter Where ParCode='SSProgramBPJS' And ParValue Is Not Null), '') ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    And T1.SiteCode In ( ");
            SQL.AppendLine("        Select SiteCode From TblPayrun ");
            SQL.AppendLine("        Where PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And SiteCode Is Not Null ");
            SQL.AppendLine("        ) ");
            if (mIsPayrunSSYrMthEnabled)
            {
                SQL.AppendLine("    And T1.Yr=@SSYr ");
                SQL.AppendLine("    And T1.Mth=@SSMth ");
            }
            else
            {
                if (!mIsPPProcessAllOutstandingSS)
                {
                    SQL.AppendLine("    And T1.Yr=Left(@PayrunCode, 4) ");
                    SQL.AppendLine("    And T1.Mth=Substring(@PayrunCode, 5, 2) ");
                }
            }
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") P On A.EmpCode=P.EmpCode ");

            #endregion

            #region Non BPJS

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, ");
            SQL.AppendLine("    Sum(T2.PDeduction*0.01*IfNull(T3.Amt, 0.00)) As WarningLetter ");
            SQL.AppendLine("    From TblEmpWL T1 ");
            SQL.AppendLine("    Inner Join TblWarningLetter T2 On T1.WLCode=T2.WLCode ");
            SQL.AppendLine("    Inner Join TblEmployeeAllowanceDeduction T3 On T1.EmpCode=T3.EmpCode And T2.ADCode=T3.ADCode ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (T3.StartDt Is Null And T3.EndDt Is Null) Or ");
            SQL.AppendLine("    (T3.StartDt Is Not Null And T3.EndDt Is Null And T3.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (T3.StartDt Is Null And T3.EndDt Is Not Null And @EndDt<=T3.EndDt) Or ");
            SQL.AppendLine("    (T3.StartDt Is Not Null And T3.EndDt Is Not Null And T3.StartDt<=@EndDt And @EndDt<=T3.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And (" + Filter.Replace("A.", "T1.") + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (T1.StartDt Is Null And T1.EndDt Is Null) Or ");
            SQL.AppendLine("    (T1.StartDt Is Not Null And T1.EndDt Is Null And T1.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (T1.StartDt Is Null And T1.EndDt Is Not Null And @EndDt<=T1.EndDt) Or ");
            SQL.AppendLine("    (T1.StartDt Is Not Null And T1.EndDt Is Not Null And T1.StartDt<=@EndDt And @EndDt<=T1.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T1.EmpCode ");
            SQL.AppendLine(") Q On A.EmpCode=Q.EmpCode ");

            #endregion

            #region Tunjangan Masa Kerja

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(IfNull(C.Amt, 0.00)) As ServiceYearAllowance ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T1.EmpCode, T2.EndDt, ");
            SQL.AppendLine("        Case When Substring(T1.JoinDt, 7, 2)<=Substring(T2.EndDt, 7, 2) Then ");
	        SQL.AppendLine("            TimeStampDiff(Year, STR_TO_DATE(T1.JoinDt, '%Y%m%d'), STR_TO_DATE(Concat(Left(T2.EndDt, 6), Right(T1.JoinDt, 2)), '%Y%m%d')) ");
            SQL.AppendLine("        Else ");
	        SQL.AppendLine("            TimeStampDiff(Year, STR_TO_DATE(T1.JoinDt, '%Y%m%d'), STR_TO_DATE(Concat(Left(T2.StartDt, 6), Right(T1.JoinDt, 2)), '%Y%m%d')) ");
            SQL.AppendLine("        End As Yr ");
            SQL.AppendLine("        From TblEmployee T1 ");
            SQL.AppendLine("        Inner Join TblPayrun T2 On T2.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        Where (" + Filter.Replace("A.", "T1.") + ") ");
            SQL.AppendLine("    ) A ");
            SQL.AppendLine("    Inner Join TblAllowanceWorkPeriodHdr B On B.CancelInd='N' And A.EndDt Between B.StartDt And B.EndDt ");
            SQL.AppendLine("    Inner Join TblAllowanceWorkPeriodDtl C On B.DocNo=C.DocNo And A.Yr=C.WorkPeriod ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") R On A.EmpCode=R.EmpCode ");

            #endregion

            #region SMK

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.EmpCode, Sum(A.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("    Inner Join TblParameter B On A.ADCode=B.ParValue And B.ParCode='ADCodeSMK' And B.ParValue is Not Null ");
            SQL.AppendLine("    Where (" + Filter + ") ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By A.EmpCode ");
            SQL.AppendLine(") S On A.EmpCode=S.EmpCode ");

            #endregion

            #region SS DPN-PHT

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerDPNPHT, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeDPNPHT ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForDPN-PHT' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") T On A.EmpCode=T.EmpCode ");

            #endregion

            #region SS DPN-PNS

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerDPNPNS, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeDPNPNS ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForDPN-PNS' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") U On A.EmpCode=U.EmpCode ");

            #endregion

            #region SS P-KES

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerPKES, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeePKES ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForP-KES' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") V On A.EmpCode=V.EmpCode ");

            #endregion

            #region SS P-RMH

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerPRMH, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeePRMH ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForP-RMH' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") W On A.EmpCode=W.EmpCode ");

            #endregion

            #region SS TSP

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerTSP, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeTSP ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForTSP' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") X On A.EmpCode=X.EmpCode ");

            #endregion

            #region SS WNR

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.EmpCode, ");
            SQL.AppendLine("    Sum(T2.EmployerAmt) As SSEmployerWNR, ");
            SQL.AppendLine("    Sum(T2.EmployeeAmt) As SSEmployeeWNR ");
            SQL.AppendLine("    From TblEmpSSListHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 ");
            SQL.AppendLine("        On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        And (T2.PayrunCode Is Null Or (T2.PayrunCode Is Not Null And T2.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine("        And (" + Filter.Replace("A.", "T2.") + ") ");
            SQL.AppendLine("    Inner Join TblParameter T3 ");
            SQL.AppendLine("        On T3.ParCode='SSPCodeForWNR' ");
            SQL.AppendLine("        And T3.ParValue Is Not Null ");
            SQL.AppendLine("        And T1.SSPCode=T3.ParValue ");
            SQL.AppendLine("    Inner Join TblPayrun T4 ");
            SQL.AppendLine("        On T4.PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And T4.SiteCode Is Not Null ");
            SQL.AppendLine("        And T1.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Yr=@SSYr ");
            SQL.AppendLine("    And T1.Mth=@SSMth ");
            SQL.AppendLine("    Group By T2.EmpCode ");
            SQL.AppendLine(") Y On A.EmpCode=Y.EmpCode ");

            #endregion

            SQL.AppendLine("Where " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
                Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
                Sm.CmParam<String>(ref cm, "@SSYr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SSMth", Sm.GetLue(LueMth));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode", 

                    //1-5
                    "JoinDt", 
                    "ResignDt", 
                    "NPWP", 
                    "PTKP", 
                    "SalaryAdjustment", 

                    //6-10
                    "SSEmployerHealth", 
                    "SSEmployeeHealth", 
                    "SSEmployerEmployment", 
                    "SSEmployeeEmployment",
                    "SSEmployerPension",
                    
                    //11-15
                    "SSEmployeePension",
                    "SSErLifeInsurance",
                    "SSEeLifeInsurance",
                    "SSErWorkingAccident",
                    "SSEeWorkingAccident",
                    
                    //16-20
                    "SSErRetirement",
                    "SSEeRetirement",
                    "SSErPension",
                    "SSEePension",
                    "Functional",
                    
                    //21-25
                    "PerformanceValue",
                    "FixAllowance",
                    "FixDeduction",
                    "TaxableFixAllowance",
                    "SSEmployerBNILife",

                    //26-30
                    "SSEmployeeBNILife",
                    "SSEmployerNonBPJS",
                    "SSEmployeeNonBPJS",
                    "WarningLetter",
                    "ServiceYearAllowance",

                    //31-35
                    "IsADSMKExisted",
                    "SSEmployerDPNPHT",
                    "SSEmployeeDPNPHT",
                    "SSEmployerDPNPNS",
                    "SSEmployeeDPNPNS",

                    //36-40
                    "SSEmployerPKES", 
                    "SSEmployeePKES",
                    "SSEmployerPRMH", 
                    "SSEmployeePRMH",
                    "SSEmployerTSP", 

                    //41-44
                    "SSEmployeeTSP",
                    "SSEmployerWNR",
                    "SSEmployeeWNR",
                    "FixAllowanceDeviationDueToPassedAway"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r2.Add(new Result2()
                        {
                            PayrunCode = PayrunCode,
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            NPWP = Sm.DrStr(dr, c[3]),
                            PTKP = Sm.DrStr(dr, c[4]),
                            SalaryAdjustment = Sm.DrDec(dr, c[5]),
                            SSEmployerHealth = Sm.DrDec(dr, c[6]),
                            SSEmployeeHealth = Sm.DrDec(dr, c[7]),
                            SSEmployerEmployment = Sm.DrDec(dr, c[8]),
                            SSEmployeeEmployment = Sm.DrDec(dr, c[9]),
                            SSEmployerPension = Sm.DrDec(dr, c[10]),
                            SSEmployeePension = Sm.DrDec(dr, c[11]),
                            SSErLifeInsurance = Sm.DrDec(dr, c[12]),
                            SSEeLifeInsurance = Sm.DrDec(dr, c[13]),
                            SSErWorkingAccident = Sm.DrDec(dr, c[14]),
                            SSEeWorkingAccident = Sm.DrDec(dr, c[15]),
                            SSErRetirement = Sm.DrDec(dr, c[16]),
                            SSEeRetirement = Sm.DrDec(dr, c[17]),
                            SSErPension = Sm.DrDec(dr, c[18]),
                            SSEePension = Sm.DrDec(dr, c[19]),
                            Functional = Sm.DrDec(dr, c[20]),
                            PerformanceValue = Sm.DrDec(dr, c[21]),
                            FixAllowance = Sm.DrDec(dr, c[22]),
                            FixDeduction = Sm.DrDec(dr, c[23]),
                            TaxableFixAllowance = Sm.DrDec(dr, c[24]),
                            SSEmployerBNILife = Sm.DrDec(dr, c[25]),
                            SSEmployeeBNILife = Sm.DrDec(dr, c[26]),
                            SSEmployerNonBPJS = Sm.DrDec(dr, c[27]),
                            SSEmployeeNonBPJS = Sm.DrDec(dr, c[28]),
                            WarningLetter = Sm.DrDec(dr, c[29]),
                            ServiceYearAllowance = Sm.DrDec(dr, c[30]),
                            _IsADSMKExisted = Sm.DrStr(dr, c[31])=="Y",
                            SSEmployerDPNPHT= Sm.DrDec(dr, c[32]),
                            SSEmployeeDPNPHT= Sm.DrDec(dr, c[33]),
                            SSEmployerDPNPNS= Sm.DrDec(dr, c[34]),
                            SSEmployeeDPNPNS= Sm.DrDec(dr, c[35]),
                            SSEmployerPKES= Sm.DrDec(dr, c[36]),
                            SSEmployeePKES= Sm.DrDec(dr, c[37]),
                            SSEmployerPRMH= Sm.DrDec(dr, c[38]),
                            SSEmployeePRMH= Sm.DrDec(dr, c[39]),
                            SSEmployerTSP= Sm.DrDec(dr, c[40]),
                            SSEmployeeTSP= Sm.DrDec(dr, c[41]),
                            SSEmployerWNR= Sm.DrDec(dr, c[42]),
                            SSEmployeeWNR = Sm.DrDec(dr, c[43]),
                            IncPerformance = 0m,
                            DedEmployee = 0m,
                            NonTaxableFixAllowance = 0m,
                            NonTaxableFixDeduction = 0m,
                            TaxableFixDeduction = 0m,
                            EmploymentPeriodAllowance = 0m,
                            SSEmployerPension2 = 0m,
                            SSEmployeePension2 = 0m,
                            Housing = 0m,
                            MobileCredit = 0m,
                            Zakat = 0m,
                            PerformanceStatus = string.Empty,
                            GradePerformance = string.Empty,
                            DedProduction = 0m,
                            IncProduction = 0m,
                            SSEmployerNonRetiring = 0m,
                            SSEmployeeOldAgeInsurance = 0m,
                            Salary = 0m,
                            WorkingDay = 0m,
                            PLDay = 0m,
                            PLHr = 0m,
                            PLAmt = 0m,
                            UPLDay = 0m,
                            UPLHr = 0m,
                            UPLAmt = 0m,
                            OT1Hr = 0m,
                            OT2Hr = 0m,
                            OTHolidayHr = 0m,
                            OT1Amt = 0m,
                            OT2Amt = 0m,
                            OTHolidayAmt = 0m,
                            IncMinWages = 0m,
                            PresenceReward = 0m,
                            HolidayEarning = 0m,
                            ExtraFooding = 0m,
                            FieldAssignment = 0m,
                            DedProdLeave = 0m,
                            IncEmployee = 0m,
                            ServiceChargeIncentive = 0m,
                            SalaryPension = 0m,
                            CreditCode1 = string.Empty,
                            CreditCode2 = string.Empty,
                            CreditCode3 = string.Empty,
                            CreditCode4 = string.Empty,
                            CreditCode5 = string.Empty,
                            CreditCode6 = string.Empty,
                            CreditCode7 = string.Empty,
                            CreditCode8 = string.Empty,
                            CreditCode9 = string.Empty,
                            CreditCode10 = string.Empty,
                            CreditAdvancePayment1 = 0m,
                            CreditAdvancePayment2 = 0m,
                            CreditAdvancePayment3 = 0m,
                            CreditAdvancePayment4 = 0m,
                            CreditAdvancePayment5 = 0m,
                            CreditAdvancePayment6 = 0m,
                            CreditAdvancePayment7 = 0m,
                            CreditAdvancePayment8 = 0m,
                            CreditAdvancePayment9 = 0m,
                            CreditAdvancePayment10 = 0m,
                            ADOT = 0m,
                            Tax = 0m,
                            TaxAllowance = 0m,
                            Amt = 0m,
                            Brutto = 0m,
                            Netto = 0m,
                            NettoActual = 0m,
                            _RecomputedValueForFixedAD = 1m,
                            _RecomputedValueForProratedAD = 1m,
                            PassAwayInd = false,
                            OTMeal = 0m,
                            FixAllowanceDeviationDueToPassedAway = Sm.DrDec(dr, c[44])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetEmployeeCandidateSalary(ref Result2 r, string LatestGrdSalaryCode)
        {
            if (LatestGrdSalaryCode.Length == 0 ||
                r.JoinDt.Length < 6 ||
                mEndDt.Length < 6)
            {
                r.Salary = 0m;
                return;
            }

            decimal 
                Yr2 = decimal.Parse(Sm.Left(r.JoinDt, 4)),
                Mth2 = decimal.Parse(r.JoinDt.Substring(4, 2)),
                Yr1 = decimal.Parse(Sm.Left(mEndDt, 4)),
                Mth1 = decimal.Parse(mEndDt.Substring(4, 2));

            var v = (((Yr1 - Yr2) * 12) + Mth1 - Mth2);

            if (v>=0m && v<=3m)
            {
                r.Salary = 0m;
                foreach (var x in mlGradeSalary.Where(w=>Sm.CompareStr(w.GrdSalaryCode, LatestGrdSalaryCode) && w.WorkPeriod == 0m && w.Month == v.ToString()))
                {
                    r.Salary = mEmployeeCandidateSalaryPercentage * 0.01m * x.Amt;
                    break;
                }
            }
        }

        private void Process6(ref Result2 r, ref List<Result1> lResult1, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2)
        {
            var EmpCode = r.EmpCode;
            string 
                LatestEmploymentStatus = string.Empty,
                LatestGrdSalaryCode = string.Empty;
            decimal LatestSalary = 0m, LatestSalary2 = 0m;

            foreach (var x in lResult1.Where(x => Sm.CompareStr(x.EmpCode, EmpCode)).OrderBy(Index=>Index.Dt))
            {
                if (x._PassAwayInd)
                {
                    r.PassAwayInd = x._PassAwayInd;
                }
                //else
                //{
                //    r.FixAllowanceDeviationDueToPassedAway = 0m;
                //}

                r.EmploymentStatus = x.EmploymentStatus;
                LatestEmploymentStatus = x.EmploymentStatus;
                LatestGrdSalaryCode = x.GrdLvlCode;
                if (x.ProcessInd)
                {
                    r._IsHOInd = x._IsHOInd;
                    LatestSalary = x.EmpSalary;
                    LatestSalary2 = x.EmpSalary2;
                    //r.Salary += x.Salary;
                    r.PLDay += x.PLDay;
                    r.PLHr += x.PLHr;
                    r.PLAmt += x.PLAmt;
                    r.UPLDay += x.UPLDay;
                    r.UPLHr += x.UPLHr;
                    r.UPLAmt += x.UPLAmt;
                    r.ProcessPLAmt += x.ProcessPLAmt;
                    r.ProcessUPLAmt += x.ProcessUPLAmt;
                    r.OT1Hr += x.OT1Hr;
                    r.OT2Hr += x.OT2Hr;
                    r.OTHolidayHr += x.OTHolidayHr;
                    r.OT1Amt += x.OT1Amt;
                    r.OT2Amt += x.OT2Amt;
                    r.OTHolidayAmt += x.OTHolidayAmt;
                    r.FieldAssignment += x.FieldAssignment;
                    r.Transport += x.Transport;
                    r.Meal += x.Meal;
                    r.IncEmployee += x.IncEmployee;
                    r.ADOT += x.ADOT;
                    r.DedAtd += x.DedAtd;
                    r.OTMeal += x.OTMeal;
                    if (x.WorkingDuration > 0) r.WorkingDay += 1;
                }
            }

            //untuk menghitung salary dan fixed allowance apabila pada 1 period terdiri dari lbh dari 1 payrun
            //kalau resign. (tidak berlaku di pht)
            //kalau payrun info berbeda dalam 1 periode.
            //kalau sudah pernah dibayar.

            Boolean IsPayrunDifferent = false, IsLatestPaidDtExisted = false;
            decimal
                 AbsentDayForJoin = 0m,
                 AbsentDayForResign = 0m,
                 RecomputedProcessedWorkingDay = 0m;

            foreach (var x in lResult1
                .Where(x => Sm.CompareStr(x.EmpCode, EmpCode))
                .OrderBy(Index => Index.Dt))
            {
                if (Sm.CompareDtTm(x.Dt, x.JoinDt) <= 0)
                    AbsentDayForJoin += 1;

                if (x.ResignDt.Length > 0 && Sm.CompareDtTm(x.ResignDt, x.Dt) <= 0)
                    AbsentDayForResign += 1;
                else
                {
                    if (x.LatestPaidDt.Length > 0 && Sm.CompareDtTm(x.LatestPaidDt, x.Dt) == 0)
                        IsLatestPaidDtExisted = true;

                    if (!IsPayrunDifferent &&
                        !(
                        Sm.CompareStr(x.DeptCode, mDeptCode) &&
                        Sm.CompareStr(x.SystemType, mSystemType) &&
                        Sm.CompareStr(x.PayrunPeriod, mPayrunPeriod) &&
                        Sm.CompareStr(x.PGCode, mPGCode) &&
                        Sm.CompareStr(x.SiteCode, mSiteCode)
                        ))

                        IsPayrunDifferent = true;
                }

                if (!(x.HolInd || x.WSHolidayInd) && x.ProcessInd) RecomputedProcessedWorkingDay += 1;
            }

            r.Salary = LatestSalary;

            if (mEmploymentStatusEmployeeCandidate.Length > 0 &&
                Sm.CompareStr(LatestEmploymentStatus, mEmploymentStatusEmployeeCandidate))
                SetEmployeeCandidateSalary(ref r, LatestGrdSalaryCode);
            
            if (RecomputedProcessedWorkingDay > 20m) RecomputedProcessedWorkingDay = 20m;

            if (//AbsentDayForJoin > 0 || AbsentDayForResign > 0 || 
                IsLatestPaidDtExisted || 
                IsPayrunDifferent)
            {
                r._RecomputedValueForFixedAD = RecomputedProcessedWorkingDay /20m;
                r.Salary = RecomputedProcessedWorkingDay * LatestSalary2;
                r.FixAllowance *= r._RecomputedValueForFixedAD;
                r.TaxableFixAllowance *= r._RecomputedValueForFixedAD;
                r.NonTaxableFixAllowance *= r._RecomputedValueForFixedAD;
                r.ServiceYearAllowance *= r._RecomputedValueForFixedAD;
            }
            if (r.Salary < 0m) r.Salary = 0m;
            if (r.TaxableFixAllowance < 0m) r.TaxableFixAllowance = 0m;
            if (r.NonTaxableFixAllowance < 0m) r.NonTaxableFixAllowance = 0m;
            if (r.FixAllowance < 0m) r.FixAllowance = 0m;
            if (r.ServiceYearAllowance < 0m) r.ServiceYearAllowance = 0m;

            decimal EmpAdvancePayment = 0m;
            int CreditAdvancePaymentNo = 0;
            string CreditCode = string.Empty;
            foreach (var x in mlAdvancePaymentProcess.Where(x => Sm.CompareStr(x.EmpCode, EmpCode)).OrderBy(o => o.CreditCode))
            {
                if (x.CreditCode.Length > 0)
                {
                    if (Sm.CompareStr(x.CreditCode, CreditCode))
                    {
                        if (CreditAdvancePaymentNo == 1)
                            r.CreditAdvancePayment1 += x.Amt;

                        if (CreditAdvancePaymentNo == 2)
                            r.CreditAdvancePayment2 += x.Amt;

                        if (CreditAdvancePaymentNo == 3)
                            r.CreditAdvancePayment3 += x.Amt;

                        if (CreditAdvancePaymentNo == 4)
                            r.CreditAdvancePayment4 += x.Amt;

                        if (CreditAdvancePaymentNo == 5)
                            r.CreditAdvancePayment5 += x.Amt;

                        if (CreditAdvancePaymentNo == 6)
                            r.CreditAdvancePayment6 += x.Amt;

                        if (CreditAdvancePaymentNo == 7)
                            r.CreditAdvancePayment7 += x.Amt;

                        if (CreditAdvancePaymentNo == 8)
                            r.CreditAdvancePayment8 += x.Amt;

                        if (CreditAdvancePaymentNo == 9)
                            r.CreditAdvancePayment9 += x.Amt;

                        if (CreditAdvancePaymentNo == 10)
                            r.CreditAdvancePayment10 += x.Amt;
                    }
                    else
                    {
                        CreditAdvancePaymentNo += 1;

                        if (CreditAdvancePaymentNo == 1)
                        {
                            r.CreditCode1 = x.CreditCode;
                            r.CreditAdvancePayment1 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 2)
                        {
                            r.CreditCode2 = x.CreditCode;
                            r.CreditAdvancePayment2 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 3)
                        {
                            r.CreditCode3 = x.CreditCode;
                            r.CreditAdvancePayment3 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 4)
                        {
                            r.CreditCode4 = x.CreditCode;
                            r.CreditAdvancePayment4 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 5)
                        {
                            r.CreditCode5 = x.CreditCode;
                            r.CreditAdvancePayment5 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 6)
                        {
                            r.CreditCode6 = x.CreditCode;
                            r.CreditAdvancePayment6 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 7)
                        {
                            r.CreditCode7 = x.CreditCode;
                            r.CreditAdvancePayment7 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 8)
                        {
                            r.CreditCode8 = x.CreditCode;
                            r.CreditAdvancePayment8 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 9)
                        {
                            r.CreditCode9 = x.CreditCode;
                            r.CreditAdvancePayment9 += x.Amt;
                        }
                        if (CreditAdvancePaymentNo == 10)
                        {
                            r.CreditCode10 = x.CreditCode;
                            r.CreditAdvancePayment10 += x.Amt;
                        }
                    }
                    CreditCode = x.CreditCode;
                }
                EmpAdvancePayment += x.Amt;
            }
            r.EmpAdvancePayment = EmpAdvancePayment;
        }

        #region tax old
        //private void ProcessTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, decimal Salary, decimal penambahTax, decimal pengurangTax)
        //{
        //    r.Amt = Salary;
        //    if (r.NPWP.Length == 0 || r.PTKP.Length == 0) return;

        //    //if (r._IsHOInd)
        //    //    ProcessTax1(ref r, ref lNTI, ref lTI, penambahTax, pengurangTax);
        //    //else
        //    //{
        //    //    ProcessTax2a(ref r, ref lNTI, ref lTI, penambahTax, pengurangTax);
        //    //    ProcessTax2b(ref r, ref lNTI, ref lTI, penambahTax, pengurangTax);
        //    //}

        //    if (r.PTKP.Length > 0 && r.NPWP.Length > 0 && r.Amt > 0)
        //        ComputeTax(ref r, ref lNTI, ref lTI, penambahTax, pengurangTax);
        //}


        //private void ComputeTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, decimal penambahTax, decimal PengurangTax)
        //{
        //    decimal
        //            Value = 0m,
        //            NTIAmt = 0m,
        //            FunctionalExpenses = 0m,
        //            Amt = 0m,
        //            JoinYr = 0m;

        //    string NTI = r.PTKP;

        //    bool IsFullYr = true;

        //    var Penambah = penambahTax;
        //    var Pengurang = PengurangTax;

        //    Amt = Penambah;

        //    FunctionalExpenses = Amt * mFunctionalExpenses * 0.01m;
        //    if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
        //        FunctionalExpenses = mFunctionalExpensesMaxAmt;

        //    Value = Amt - FunctionalExpenses - Pengurang;

        //    if (r.JoinDt.Length >= 4)
        //        JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

        //    if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
        //        Value = Value * 12m;
        //    else
        //    {
        //        if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
        //        {
        //            Value = Value * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
        //            IsFullYr = false;
        //        }
        //    }

        //    foreach (var x in lNTI.Where(x => x.Status == NTI))
        //        NTIAmt = x.Amt;

        //    if (Value > NTIAmt)
        //        Value -= NTIAmt;
        //    else
        //        Value = 0m;

        //    if (Value > 0 && lTI.Count > 0)
        //    {
        //        foreach (TI i in lTI.OrderBy(x => x.SeqNo))
        //        {
        //            if (Value >= i.Amt1 && Value <= i.Amt2)
        //            {
        //                Value = ((Value - (i.Amt1 - 1m)) * (i.TaxRate / 100));
        //                break;
        //            }
        //        }
        //    }
        //    Value = Value / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
        //    if (Value <= 0) Value = 0m;
        //    r.TaxAllowance = 0.05m * (r.Salary + r.FixAllowance);
        //    r.Tax = Math.Round(Value, 0);
        //}
       

        #endregion 

        #region new tax

        private void ProcessTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2, decimal Salary, decimal penambahTax, decimal pengurangTax)
        {
            r.Amt = Salary;
            if (r.PTKP.Length == 0) return;

            if (r.PTKP.Length > 0 && r.Amt > 0)
                ComputeTax(ref r, ref lNTI, ref lTI, ref lTI2, penambahTax, pengurangTax);
        }

        private decimal GetYearsOfService(string JoinDt)
        {
            DateTime
                Dt1 = Sm.ConvertDate(JoinDt),
                Dt2;

            var v = Sm.Right(JoinDt, 2);
            var MonthStartDt = mStartDt.Substring(4, 2);
            var YearStartDt = Sm.Left(mStartDt, 4);

            if (int.Parse(v)>30 && (MonthStartDt == "02" || MonthStartDt == "04" || MonthStartDt == "06" || MonthStartDt == "09" || MonthStartDt == "11"))
            {
                v = Sm.Right(String.Concat("00", DateTime.DaysInMonth(Convert.ToInt32(YearStartDt), Convert.ToInt32(MonthStartDt))), 2);
            }
            if (int.Parse(v) > 28 && MonthStartDt == "02")
            {
                v = Sm.Right(String.Concat("00", DateTime.DaysInMonth(Convert.ToInt32(YearStartDt), Convert.ToInt32(MonthStartDt))), 2);
            }

            if (int.Parse(Sm.Right(mStartDt, 2))<=int.Parse(v))
                Dt2 = Sm.ConvertDate(string.Concat(Sm.Left(mStartDt, 6), v));
            else
                Dt2 = Sm.ConvertDate(string.Concat(Sm.Left(mEndDt, 6), v));

            return (((Dt2.Year - Dt1.Year) * 12) + Dt2.Month - Dt1.Month) + 1;
        }

        private decimal GetYearsOfService2(string EmpCode)
        {
            decimal years = 0m;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select TimeStampDiff(Year, Date_Format(now(), '%Y%m%d') , JoinDt) * -1 As YrDiff ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where EmpCode = @Param ");
            SQL.AppendLine("; ");

            string result = Sm.GetValue(SQL.ToString(), EmpCode);
            if (result.Trim().Length > 0) years = decimal.Parse(result);

            return years;
        }

        private void ComputeTax(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI, ref List<TI2> lTI2, decimal penambahTax, decimal PengurangTax)
        {
            decimal
                    Value = 0m,
                    NTIAmt = 0m,
                    FunctionalExpenses = 0m,
                    Amt = 0m,
                    JoinYr = 0m,
                    YearsOfService = 0m;
            string NTI = r.PTKP;
            //bool IsFullYr = true;
            var Penambah = penambahTax;
            var Pengurang = PengurangTax;

            Amt = Penambah;

            FunctionalExpenses = Amt * mFunctionalExpenses * 0.01m;
            if (FunctionalExpenses > mFunctionalExpensesMaxAmt)
                FunctionalExpenses = mFunctionalExpensesMaxAmt;

            Value = Amt - FunctionalExpenses - Pengurang;

            if (r.JoinDt.Length>0) YearsOfService = GetYearsOfService(r.JoinDt);
            if (YearsOfService > 12m) YearsOfService = 12m;

            Value *= YearsOfService;

            //if (r.JoinDt.Length >= 4)
            //    JoinYr = Decimal.Parse(Sm.Left(r.JoinDt, 4));

            //if (JoinYr < Decimal.Parse(Sm.Left(r.PayrunCode, 4)))
            //    Value = Value * 12m;
            //else
            //{
            //    if (Sm.CompareStr(Sm.Left(r.JoinDt, 4), Sm.Left(r.PayrunCode, 4)))
            //    {
            //        Value = Value * (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1);
            //        IsFullYr = false;
            //    }
            //}

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (Value > NTIAmt)
                Value -= NTIAmt;
            else
                Value = 0m;

            //if (Value > 0 && lTI.Count > 0)
            //{
            //    foreach (TI i in lTI.OrderBy(x => x.SeqNo))
            //    {
            //        if (Value >= i.Amt1 && Value <= i.Amt2)
            //        {
            //            Value = ((Value - (i.Amt1 - 1m)) * (i.TaxRate / 100));
            //            break;
            //        }
            //    }
            //}

            if (Value > 0 && lTI2.Count > 0)
            {
                foreach (TI2 i in lTI2.OrderBy(x => x.SeqNo))
                {
                    if (Value >= i.Amt1 && Value <= i.Amt2)
                    {
                        Value = ((Value - (i.Amt1 - 1m)) * (i.Value1 / i.Value2)) + i.Value3;
                        break;
                    }
                }
            }

            //Value = Value / (IsFullYr ? 12m : (12m - decimal.Parse(r.JoinDt.Substring(4, 2)) + 1));
            Value /= YearsOfService;
            if (Value <= 0) Value = 0m;
            if (r.NPWP.Length == 0) Value *= 1.2m;
            r.TaxAllowance = Math.Round(Value, 0);
            r.Tax = r.TaxAllowance;
        }

        #endregion

        private void ProcessTax1(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            string NTI = r.PTKP, PensionEmployerPerc = string.Empty;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                PPH21Setahun = 0m,
                PPH21Sebulan = 0m;

            TotalImbalan =
                r.Salary +
                r.TaxableFixAllowance -
                r.TaxableFixDeduction +
                r.Transport +
                r.Meal +
                r.ADLeave +
                r.SalaryAdjustment +
                r.ServiceChargeIncentive +
                r.SSEmployerHealth +
                r.SSErLifeInsurance +
                r.SSErWorkingAccident - 
                (
                r.SSEePension
                + r.SSEeRetirement
                + r.SSEmployeePension
                + r.SSEmployeePension2
                )
                ;

            //TotalImbalan =
            //    r.Salary +
            //    r.TaxableFixAllowance -
            //    r.TaxableFixDeduction +
            //    r.Transport +
            //    r.Meal +
            //    r.ADLeave +
            //    r.SalaryAdjustment
            //    - (r.SSEmployeeHealth
            //    + r.SSEmployeeEmployment
            //    + r.SSEePension
            //    + r.SSEmployeePension
            //    + r.SSEmployeePension2
            //    )
            //    // + r.ServiceChargeIncentive
            //    ;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt)
                ? mFunctionalExpensesMaxAmt
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;
            GajiBersihDisetahunkan = 12m * GajiBersihSebulan;

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihDisetahunkan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihDisetahunkan - NTIAmt;

            PPH21Setahun = decimal.Truncate(PKP);
            PPH21Setahun = PPH21Setahun - (PPH21Setahun % 1000);

            if (PPH21Setahun > 0m && lTI.Count > 0)
            {
                TaxTemp = PPH21Setahun;
                Amt2Temp = 0m;
                PPH21Setahun = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            PPH21Setahun += (TaxTemp * i.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Setahun += ((i.Amt2 - Amt2Temp) * i.TaxRate * 0.01m);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }
            PPH21Sebulan = PPH21Setahun / 12m;
            if (PPH21Sebulan <= 0) PPH21Sebulan = 0m;
            r.Tax = PPH21Sebulan;
            r.TaxAllowance = PPH21Sebulan;
        }

        private void ProcessTax2a(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            string NTI = r.PTKP, PensionEmployerPerc = string.Empty;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                PPH21Setahun = 0m,
                PPH21Sebulan = 0m
                ;

            TotalImbalan =
                r.Salary +
                r.TaxableFixAllowance -
                r.TaxableFixDeduction +
                r.Transport +
                r.Meal +
                r.ADLeave +
                r.SalaryAdjustment +
                r.ServiceChargeIncentive +
                r.SSEmployerHealth +
                r.SSErLifeInsurance +
                r.SSErWorkingAccident - 
                (
                r.SSEePension
                + r.SSEeRetirement
                + r.SSEmployeePension
                + r.SSEmployeePension2
                )
                ;

            //TotalImbalan =
            //    r.Salary +
            //    r.TaxableFixAllowance -
            //    r.TaxableFixDeduction +
            //    r.Transport +
            //    r.Meal +
            //    r.SalaryAdjustment +
            //    r.ADLeave +
            //    r.ServiceChargeIncentive
            //    - (r.SSEmployeeHealth
            //    + r.SSEmployeeEmployment
            //    + r.SSEePension
            //    + r.SSEmployeePension
            //    + r.SSEmployeePension2
            //    )
            //    ;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt)
                ? mFunctionalExpensesMaxAmt
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;
            GajiBersihDisetahunkan = 12m * GajiBersihSebulan;

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihDisetahunkan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihDisetahunkan - NTIAmt;

            PPH21Setahun = decimal.Truncate(PKP);
            PPH21Setahun = PPH21Setahun - (PPH21Setahun % 1000);

            if (PPH21Setahun > 0m && lTI.Count > 0)
            {
                TaxTemp = PPH21Setahun;
                Amt2Temp = 0m;
                PPH21Setahun = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            PPH21Setahun += (TaxTemp * i.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Setahun += ((i.Amt2 - Amt2Temp) * i.TaxRate * 0.01m);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }
            PPH21Sebulan = PPH21Setahun / 12m;
            if (PPH21Sebulan <= 0) PPH21Sebulan = 0m;
            r.Tax = PPH21Sebulan;
            r.Amt -= r.Tax;
        }

        private void ProcessTax2b(ref Result2 r, ref List<NTI> lNTI, ref List<TI> lTI)
        {
            string NTI = r.PTKP, PensionEmployerPerc = string.Empty;
            decimal
                TotalImbalan = 0m,
                TotalPengurangan = 0m,
                GajiBersihSebulan = 0m,
                GajiBersihDisetahunkan = 0m,
                NTIAmt = 0m,
                PKP = 0m,
                TaxTemp = PKP,
                Amt2Temp = 0m,
                PPH21Setahun = 0m,
                PPH21Sebulan = 0m
                ;

            TotalImbalan =
               r.Salary +
               r.TaxableFixAllowance -
               r.TaxableFixDeduction +
               r.Transport +
               r.Meal +
               r.ADLeave +
               r.SalaryAdjustment +
                //r.ServiceChargeIncentive +
               r.SSEmployerHealth +
               r.SSErLifeInsurance +
               r.SSErWorkingAccident - 
               (
               r.SSEePension
               + r.SSEeRetirement
               + r.SSEmployeePension
               + r.SSEmployeePension2
               );

            //TotalImbalan =
            //    r.Salary +
            //    r.TaxableFixAllowance -
            //    r.TaxableFixDeduction +
            //    r.Transport +
            //    r.Meal +
            //    r.ADLeave +
            //    r.SalaryAdjustment
            //    - (r.SSEmployeeHealth
            //    + r.SSEmployeeEmployment
            //    + r.SSEePension
            //    + r.SSEmployeePension
            //    + r.SSEmployeePension2
            //    )
            //    // + r.ServiceChargeIncentive
            //    ;

            TotalPengurangan =
                ((mFunctionalExpenses * 0.01m * TotalImbalan > mFunctionalExpensesMaxAmt)
                ? mFunctionalExpensesMaxAmt
                : mFunctionalExpenses * 0.01m * TotalImbalan);

            GajiBersihSebulan = TotalImbalan - TotalPengurangan;
            GajiBersihDisetahunkan = 12m * GajiBersihSebulan;

            foreach (var x in lNTI.Where(x => x.Status == NTI))
                NTIAmt = x.Amt;

            if (GajiBersihDisetahunkan - NTIAmt < 0m)
                PKP = 0m;
            else
                PKP = GajiBersihDisetahunkan - NTIAmt;

            PPH21Setahun = decimal.Truncate(PKP);
            PPH21Setahun = PPH21Setahun - (PPH21Setahun % 1000);

            if (PPH21Setahun > 0m && lTI.Count > 0)
            {
                TaxTemp = PPH21Setahun;
                Amt2Temp = 0m;
                PPH21Setahun = 0m;

                foreach (TI i in lTI.OrderBy(x => x.SeqNo))
                {
                    if (TaxTemp > 0m)
                    {
                        if (TaxTemp <= (i.Amt2 - Amt2Temp))
                        {
                            PPH21Setahun += (TaxTemp * i.TaxRate * 0.01m);
                            TaxTemp = 0m;
                        }
                        else
                        {
                            PPH21Setahun += ((i.Amt2 - Amt2Temp) * i.TaxRate * 0.01m);
                            TaxTemp -= (i.Amt2 - Amt2Temp);
                        }
                    }
                    Amt2Temp = i.Amt2;
                }
            }
            PPH21Sebulan = PPH21Setahun / 12m;
            if (PPH21Sebulan <= 0) PPH21Sebulan = 0m;
            r.TaxAllowance = PPH21Sebulan;
            r.Amt += PPH21Sebulan;
        }

        #endregion

        #region Get Tables Data

        private void ProcessEmployeePPS(ref List<EmployeePPS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0;i<mlPayrunEmployee.Count;i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }
            
            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ");";
            else
                Filter = " Where 0=1;";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.PosCode, A.DeptCode, A.GrdLvlCode, C.LevelCode, A.EmploymentStatus, ");
            SQL.AppendLine("A.SystemType, A.PayrunPeriod, A.PGCode, A.SiteCode, IfNull(B.HOInd, 'N') As HOInd, D.DivisionCode ");
            SQL.AppendLine("From TblEmployeePPS A ");
            SQL.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("Inner Join TblEmployee D On A.EmpCode = D.EmpCode ");
            SQL.AppendLine(Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "StartDt", "EndDt", "PosCode", "DeptCode", "GrdLvlCode", 
                    
                    //6-10
                    "LevelCode", "EmploymentStatus", "SystemType", "PayrunPeriod", "PGCode", 

                    //11-13
                    "SiteCode", "HOInd", "DivisionCode"
                
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeePPS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            PosCode = Sm.DrStr(dr, c[3]),
                            DeptCode = Sm.DrStr(dr, c[4]),
                            GrdLvlCode = Sm.DrStr(dr, c[5]),
                            LevelCode = Sm.DrStr(dr, c[6]),
                            EmploymentStatus = Sm.DrStr(dr, c[7]),
                            SystemType = Sm.DrStr(dr, c[8]),
                            PayrunPeriod = Sm.DrStr(dr, c[9]),
                            PGCode = Sm.DrStr(dr, c[10]),
                            SiteCode = Sm.DrStr(dr, c[11]),
                            HOInd = Sm.DrStr(dr, c[12]),
                            DivisionCode = Sm.DrStr(dr, c[13])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpWorkSchedule(ref List<EmpWorkSchedule> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 0=1;";

            SQL.AppendLine("Select A.EmpCode, A.Dt, A.WSCode, ");
            SQL.AppendLine("B.HolidayInd, B.DoubleShiftInd, ");
            SQL.AppendLine("B.In1, B.Out1, B.In2, B.Out2, B.In3, B.Out3 ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "Dt", "WSCode", "HolidayInd", "DoubleShiftInd", "In1", 
                    
                    //6-10
                    "Out1", "In2", "Out2", "In3", "Out3", 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpWorkSchedule()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            WSCode = Sm.DrStr(dr, c[2]),
                            HolidayInd = Sm.DrStr(dr, c[3]),
                            DoubleShiftInd = Sm.DrStr(dr, c[4]),
                            In1 = Sm.DrStr(dr, c[5]),
                            Out1 = Sm.DrStr(dr, c[6]),
                            In2 = Sm.DrStr(dr, c[7]),
                            Out2 = Sm.DrStr(dr, c[8]),
                            In3 = Sm.DrStr(dr, c[9]),
                            Out3 = Sm.DrStr(dr, c[10])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpPaidDt(ref List<EmpPaidDt> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 0=1;";

            SQL.AppendLine("Select Distinct A.EmpCode, A.Dt ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Inner Join TblPayrun B On A.PayrunCode=B.PayrunCode And B.CancelInd='N' ");
            SQL.AppendLine("Where A.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine("And IfNull(A.PayrunCode, '')<>@PayrunCode ");
            SQL.AppendLine("And A.ProcessInd='Y' ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            Sm.CmParam<string>(ref cm, "@PayrunCode", mPayrunCode);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpPaidDt()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAtd(ref List<Atd> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(B.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ")     ";
            else
                Filter = " And 0=1 ";


            SQL.AppendLine("Select B.EmpCode, B.Dt, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Left(B.InOutA1, 12) End As ActualIn, ");
            SQL.AppendLine("Case When B.InOutA1 Is Null Then Null Else Right(B.InOutA1, 12) End As ActualOut ");
            SQL.AppendLine("From TblAtdHdr A ");
            SQL.AppendLine("Inner Join TblAtdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.CancelInd='N';");

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "ActualIn", "ActualOut" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Atd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            ActualIn = Sm.DrStr(dr, c[2]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[2]),
                            ActualOut = Sm.DrStr(dr, c[3]) == "XXXXXXXXXXXX" ? string.Empty : Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeave(ref List<Leave> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(Tbl.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select T1.EmpCode, T2.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T1.DurationHour, T3.PaidInd, 'N' As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeaveHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeaveDtl T2 On T1.DocNo=T2.DocNo And T2.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T3 On T1.LeaveCode=T3.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A' ");
            SQL.AppendLine(Filter.Replace("Tbl.", "T1."));
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T2.EmpCode, T3.LeaveDt, T1.LeaveCode, T1.LeaveType, T1.StartTm, T1.EndTm, T2.DurationHour, T4.PaidInd, T1.CompulsoryLeaveInd As CompulsoryLeaveInd ");
            SQL.AppendLine("From TblEmpLeave2Hdr T1 ");
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter.Replace("Tbl.", "T2."));
            SQL.AppendLine("Inner Join TblEmpLeave2Dtl2 T3 ");
            SQL.AppendLine("    On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("    And T3.LeaveDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblLeave T4 On T1.LeaveCode=T4.LeaveCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status='A';");

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "LeaveDt", "LeaveCode", "LeaveType", "StartTm", "EndTm", 
                    
                    //6-8
                    "DurationHour", "PaidInd", "CompulsoryLeaveInd"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Leave()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            LeaveCode = Sm.DrStr(dr, c[2]),
                            LeaveType = Sm.DrStr(dr, c[3]),
                            StartTm = Sm.DrStr(dr, c[4]),
                            EndTm = Sm.DrStr(dr, c[5]),
                            DurationHr = Sm.DrDec(dr, c[6]),
                            PaidInd = Sm.DrStr(dr, c[7]),
                            CompulsoryLeaveInd = Sm.DrStr(dr, c[8])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveDtl(ref List<LeaveDtl> l)
        {
            var cm = new MySqlCommand();
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select LeaveCode, SystemType, PayrunPeriod, DeductTHPInd From TblLeaveDtl;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "LeaveCode", 
                    "SystemType", "PayrunPeriod", "DeductTHPInd" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LeaveDtl()
                        {
                            LeaveCode = Sm.DrStr(dr, c[0]),
                            SystemType = Sm.DrStr(dr, c[1]),
                            PayrunPeriod = Sm.DrStr(dr, c[2]),
                            DeductTHPInd = Sm.DrStr(dr, c[3]) == "Y"
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployee(ref List<Employee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.EmpCode, A.JoinDt, A.ResignDt, Case When B.EmpCode Is Null Then 'N' Else 'Y' End As PassAwayInd ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select Distinct T1.EmpCode ");
            SQL.AppendLine("    From TblPPS T1 ");
            SQL.AppendLine("    Inner Join TblPayrun T2 On T2.Payruncode=@PayrunCode ");
            SQL.AppendLine("    Inner Join TblEmployee T3  ");
            SQL.AppendLine("        On T1.EmpCode=T3.EmpCode  ");
            SQL.AppendLine("        And T3.ResignDt Is Not Null  ");
            SQL.AppendLine("        And T3.ResignDt<=T2.EndDt ");
            SQL.AppendLine("        And Date_Format(Date_Add(Str_To_Date(T3.ResignDt, '%Y%m%d'), Interval 4 Month), '%Y%m%d')>T2.StartDt ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And T1.JobTransfer In (Select ParValue From TblParameter Where ParValue Is Not Null And ParCode='JobTransferPassAway') ");
            SQL.AppendLine(") B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "JoinDt", "ResignDt", "PassAwayInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            JoinDt = Sm.DrStr(dr, c[1]),
                            ResignDt = Sm.DrStr(dr, c[2]),
                            PassAwayInd = Sm.DrStr(dr, c[3])=="Y",
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalary(ref List<EmployeeSalary> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ");";
            else
                Filter = " Where 0=1;";

            SQL.AppendLine("Select A.EmpCode, B.StartDt, IfNull(B.Amt, 0) As Amt, IfNull(B.Amt2, 0) As Amt2 ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblEmployeeSalary B On A.EmpCode=B.EmpCode And B.StartDt<=@EndDt ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "Amt", "Amt2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeSalarySSPension(ref List<EmployeeSalarySS> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.EmpCode, A.StartDt, A.EndDt, A.Amt ");
            SQL.AppendLine("From TblEmployeeSalarySS A ");
            SQL.AppendLine("Inner Join TblParameter B On A.SSPCode=B.ParValue And B.ParCode='SSProgramForPension' And B.ParValue Is Not Null ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Null) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Null And A.StartDt<=@EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Null And A.EndDt Is Not Null And @EndDt<=A.EndDt) Or ");
            SQL.AppendLine("    (A.StartDt Is Not Null And A.EndDt Is Not Null And A.StartDt<=@EndDt And @EndDt<=A.EndDt) ");
            SQL.AppendLine("); ");

            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "StartDt", "EndDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeSalarySS()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            StartDt = Sm.DrStr(dr, c[1]),
                            EndDt = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeMealTransport(ref List<EmployeeMealTransport> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, Filter2 = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            if (mADCodeMeal.Length > 0)
            {
                Filter2 = " ADCode=@ADCodeMeal ";
                Sm.CmParam<String>(ref cm, "@ADCodeMeal", mADCodeMeal);
            }

            if (mADCodeTransport.Length > 0)
            {
                if (Filter2.Length > 0) Filter2 += " Or ";
                Filter2 += " ADCode=@ADCodeTransport ";
                Sm.CmParam<String>(ref cm, "@ADCodeTransport", mADCodeTransport);
            }

            if (Filter2.Length > 0)
                Filter2 = " And (" + Filter2 + ") ";
            else
                Filter2 = " And 0=1 ";

            SQL.AppendLine("Select EmpCode, ADCode, StartDt, EndDt, Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(Filter2);
            SQL.AppendLine("Order By ADCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "EmpCode", 
                    "ADCode", "StartDt", "EndDt", "Amt" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeMealTransport()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            StartDt = Sm.DrStr(dr, c[2]),
                            EndDt = Sm.DrStr(dr, c[3]),
                            Amt = Sm.DrDec(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAttendanceLog(ref List<AttendanceLog> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            string EmpCode = string.Empty;
            var EmpCodeTemp = string.Empty;
            var DtTemp = string.Empty;
            var TmTemp = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ");";
            else
                Filter = " And 0=1;";

            SQL.AppendLine("Select EmpCode, Dt, Tm ");
            SQL.AppendLine("From TblAttendanceLog ");
            SQL.AppendLine("Where Dt Between @StartDt And @EndDt ");
            SQL.AppendLine(Filter);

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            var EndDtTemp = mEndDt;

            DateTime EndDt = new DateTime(
                      Int32.Parse(EndDtTemp.Substring(0, 4)),
                      Int32.Parse(EndDtTemp.Substring(4, 2)),
                      Int32.Parse(EndDtTemp.Substring(6, 2)),
                      0, 0, 0).AddDays(1);

            Sm.CmParamDt(ref cm, "@EndDt", EndDt.Year.ToString() +
                        ("00" + EndDt.Month.ToString()).Substring(("00" + EndDt.Month.ToString()).Length - 2, 2) +
                        ("00" + EndDt.Day.ToString()).Substring(("00" + EndDt.Day.ToString()).Length - 2, 2));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Dt", "Tm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCodeTemp = Sm.DrStr(dr, c[0]);
                        DtTemp = Sm.DrStr(dr, c[1]);
                        TmTemp = Sm.DrStr(dr, c[2]);
                        if (TmTemp.Length > 4) TmTemp = Sm.Left(TmTemp, 4);
                        l.Add(new AttendanceLog()
                        {
                            EmpCode = EmpCodeTemp,
                            Dt = DtTemp,
                            Tm = TmTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOT(ref List<OT> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode= string.Empty, OTDt = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T2.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            SQL.AppendLine("Select T1.DocNo, T1.OTDt, T2.EmpCode, T2.OTStartTm, T2.OTEndTm, T1.OTSpecialind ");
            SQL.AppendLine("From TblOTRequestHdr T1 ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T2 On T1.DocNo=T2.DocNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            SQL.AppendLine("And T1.OTDt Between @StartDt And @EndDt;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "OTDt", "EmpCode", "OTStartTm", "OTEndTm", "OTSpecialind" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OT()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4]),
                            OTSpecialInd = Sm.DrStr(dr, c[5])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTSpecial(ref List<OTSpecial> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, OTDt = string.Empty;

            l.Clear();

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

	        SQL.AppendLine("Select SpecialIndCode, SourceCode, DeptCode, SiteCode, DivisionCode, OTAmt, MealAmt ");
	        SQL.AppendLine("From tblotspecialsummary A ");
            SQL.AppendLine("Where SourceCode in ");
            if (mOTSpecialSource == "1")
            {
                SQL.AppendLine("(");
                SQL.AppendLine("    Select B.LevelCode ");
                SQL.AppendLine("    From TblEmployee A ");
                SQL.AppendLine("    Inner Join TBlGradelevelHdr B On A.GrdLvlCode = B.GrdLvlCode Where 0=0 " + Filter);
                SQL.AppendLine(")");
            }
            else
            {
                SQL.AppendLine("(");
                SQL.AppendLine("    Select A.PositionStatusCode ");
                SQL.AppendLine("    From TblEmployee A Where 0=0 " + Filter);
                SQL.AppendLine(")");
            }
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SourceCode", "DeptCode", "SiteCode", "DivisionCode", "OTAmt", "MealAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTSpecial()
                        {
                            SourceCode = Sm.DrStr(dr, c[0]),
                            DeptCode = Sm.DrStr(dr, c[1]),
                            SiteCode = Sm.DrStr(dr, c[2]),
                            DivisionCode = Sm.DrStr(dr, c[3]),
                            OTAmt = Sm.DrDec(dr, c[4]),
                            MealAmt = Sm.DrDec(dr, c[5]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessOTAdjustment(ref List<OTAdjustment> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty, OTDt = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T3.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            SQL.AppendLine("Select T1.OTRequestDocNo, T2.OTDt, T3.EmpCode, T1.StartTm, T1.EndTm ");
            SQL.AppendLine("From TblOTAdjustment T1 ");
            SQL.AppendLine("Inner Join TblOTRequestHdr T2 On T1.OTRequestDocNo=T2.DocNo And T2.OTDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Inner Join TblOTRequestDtl T3 On T1.OTRequestDocNo=T3.DocNo And T1.OTRequestDNo=T3.DNo " + Filter);
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A';");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OTRequestDocNo", "OTDt", "EmpCode", "StartTm", "EndTm" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OTAdjustment()
                        {
                            OTDocNo = Sm.DrStr(dr, c[0]),
                            Dt = Sm.DrStr(dr, c[1]),
                            EmpCode = Sm.DrStr(dr, c[2]),
                            Tm1 = Sm.DrStr(dr, c[3]),
                            Tm2 = Sm.DrStr(dr, c[4])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAdvancePaymentProcess(ref List<AdvancePaymentProcess> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T1.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length>0) 
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select T1.DocNo, T1.EmpCode, T1.CreditCode, T2.Amt ");
            SQL.AppendLine("From TblAdvancePaymentHdr T1 ");
            SQL.AppendLine("Inner Join TblAdvancePaymentDtl T2 ");
            SQL.AppendLine("    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.Yr=@Yr ");
            SQL.AppendLine("    And T2.Mth=@Mth ");
            SQL.AppendLine("Where T1.CancelInd='N' And T1.Status='A' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And ( ");
            SQL.AppendLine("    Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And PayrunCode=@PayrunCode ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine(Filter.Replace("T1.", string.Empty));
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Or Not Exists( ");
            SQL.AppendLine("        Select 1 From TblAdvancePaymentProcess ");
            SQL.AppendLine("        Where DocNo=T2.DocNo ");
            SQL.AppendLine("        And Yr=@Yr ");
            SQL.AppendLine("        And Mth=@Mth ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine(";");

            string Year = Sm.Left(mEndDt, 4), Month = mEndDt.Substring(4, 2);

            Sm.CmParam<String>(ref cm, "@Yr", Year);
            Sm.CmParam<String>(ref cm, "@Mth", Month);
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "EmpCode", "CreditCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AdvancePaymentProcess()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            EmpCode = Sm.DrStr(dr, c[1]),
                            CreditCode = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                            Yr = Year,
                            Mth = Month
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessIncEmployee(ref List<IncEmployee> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(T2.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select T1.DocDt, T2.EmpCode, Sum(T1.AmtInspnt) As Amt ");
            SQL.AppendLine("From TblEmpInsPntHdr T1 ");
            SQL.AppendLine("Inner Join TblEmpInsPntDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Inner Join TblInsPnt T3 On T1.InspntCode=T3.InspntCode ");
            SQL.AppendLine("Inner Join TblInsPntCategory T4 On T3.InspntCtCode=T4.InspntCtCode And T4.InsPntType='01' ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Group By T1.DocDt, T2.EmpCode;");

            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "DocDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new IncEmployee()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmpSCI(ref List<EmpSCI> l)
        {
            l.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, EmpCode = string.Empty;

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(B.EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select A.DocDt, B.EmpCode, Sum(B.Amt) As Amt ");
            SQL.AppendLine("From TblEmpSCIHdr A ");
            SQL.AppendLine("Inner Join TblEmpSCIDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And (B.PayrunCode Is Null Or (B.PayrunCode Is Not Null And B.PayrunCode=@PayrunCode)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt Between @StartDt And @EndDt ");
            SQL.AppendLine("Group By A.DocDt, B.EmpCode;");

            Sm.CmParam<string>(ref cm, "@PayrunCode", mPayrunCode);
            Sm.CmParamDt(ref cm, "@StartDt", mStartDt);
            Sm.CmParamDt(ref cm, "@EndDt", mEndDt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "DocDt", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmpSCI()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            DocDt = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }
        
        private void ProcessNTI(ref List<NTI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Status, B.Amt ");
            SQL.AppendLine("From TblNTIHdr A ");
            SQL.AppendLine("Inner Join TblNTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "Status", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new NTI()
                        {
                            PayrunCode = mPayrunCode,
                            DocNo = Sm.DrStr(dr, c[0]),
                            Status = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI(ref List<TI> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.TaxRate  ");
            SQL.AppendLine("From TblTIHdr A ");
            SQL.AppendLine("Inner Join TblTIDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.ActInd='Y' And A.UsedFor='01'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "SeqNo", "Amt1", "Amt2", "TaxRate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI()
                        {
                            PayrunCode = mPayrunCode,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            TaxRate = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessTI2(ref List<TI2> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.SeqNo, B.Amt1, B.Amt2, B.Value1, B.Value2, B.Value3 ");
            SQL.AppendLine("From TblTI2Hdr A ");
            SQL.AppendLine("Inner Join TblTI2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where ActInd='Y'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DocNo", 
                    "SeqNo", "Amt1", "Amt2", "Value1", "Value2",
                    "Value3"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new TI2()
                        {
                            PayrunCode = mPayrunCode,
                            DocNo = Sm.DrStr(dr, c[0]),
                            SeqNo = Sm.DrStr(dr, c[1]),
                            Amt1 = Sm.DrDec(dr, c[2]),
                            Amt2 = Sm.DrDec(dr, c[3]),
                            Value1 = Sm.DrDec(dr, c[4]),
                            Value2 = Sm.DrDec(dr, c[5]),
                            Value3 = Sm.DrDec(dr, c[6])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessWorkScheduleADMealTransport(ref List<WorkScheduleADMealTransport> l)
        {
            l.Clear();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select WSCode, ADCode From TblWorkScheduleAD ");
            if (mADCodeTransport.Length > 0 || mADCodeMeal.Length > 0)
            {
                SQL.AppendLine("Where ADCode In ('' ");
                if (mADCodeTransport.Length > 0)
                    SQL.AppendLine(", @ADCodeTransport");
                if (mADCodeMeal.Length > 0)
                    SQL.AppendLine(", @ADCodeMeal");
                SQL.AppendLine(") ");
            }
            else
                SQL.AppendLine("Where 0=1 ");
            SQL.AppendLine("Order By WSCode; ");

            Sm.CmParam<String>(ref cm, "@ADCodeTransport", mADCodeTransport);
            Sm.CmParam<String>(ref cm, "@ADCodeMeal", mADCodeMeal);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WSCode", "ADCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WorkScheduleADMealTransport()
                        {
                            WSCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessEmployeeDeductionAtd(ref List<EmployeeDeductionAtd> l, string ADCode)
        {
            l.Clear();

            if (mADCodePresenceReward.Length == 0) return;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, EmpCode = string.Empty;

            Sm.CmParam<String>(ref cm, "@ADCode", ADCode);

            for (int i = 0; i < mlPayrunEmployee.Count; i++)
            {
                EmpCode = mlPayrunEmployee[i].EmpCode;
                if (EmpCode.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(EmpCode=@EmpCode0" + i.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), EmpCode);
                }
            }

            SQL.AppendLine("Select EmpCode, Amt ");
            SQL.AppendLine("From TblEmployeeAllowanceDeduction ");
            SQL.AppendLine("Where ADCode=@ADCode ");
            if (Filter.Length > 0)
                SQL.AppendLine(" And (" + Filter + ") ");
            else
                SQL.AppendLine(" And 0=1 ");
            SQL.AppendLine("Order By EmpCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeDeductionAtd()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessGradeSalary(ref List<GradeSalary> l, ref List<Result1> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            int n = 0;

            l.Clear();
            foreach(var i in l2.Select(x => x.GrdLvlCode).Distinct())
            {
                if (i.Length>0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.GrdSalaryCode=@GrdSalaryCode0" + n.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@GrdSalaryCode0" + n.ToString(), i);
                    n++;
                }
            }

            if (Filter.Length > 0)
                Filter = " Where (" + Filter + ") ";
            else
                Filter = " Where 0=1 ";

            SQL.AppendLine("Select A.GrdSalaryCode, B.WorkPeriod, B.Month, B.Amt ");
            SQL.AppendLine("From TblGradeSalaryHdr A ");
            SQL.AppendLine("Inner Join TblGradeSalaryDtl B on A.GrdSalaryCode=B.GrdSalaryCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "GrdSalaryCode", "WorkPeriod", "Month", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new GradeSalary()
                        {
                            GrdSalaryCode = Sm.DrStr(dr, c[0]),
                            WorkPeriod = Sm.DrDec(dr, c[1]),
                            Month = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private void InitSocialSecurity(ref List<RecalculateSocialSecurity> l, Result2 r,
            decimal SSErEmploymentPerc, decimal SSErHealthPerc, decimal SSErPensionPerc,
            decimal SSEeEmploymentPerc, decimal SSEeHealthPerc, decimal SSEePensionPerc,
            decimal SMKAmt, decimal ADCodePositionAdjustmentAmt, decimal SSErHealthSalaryMaxLimit)
        {
            l.Add(new RecalculateSocialSecurity()
            {
                PayrunCode = r.PayrunCode,
                EmpCode = r.EmpCode,

                //Er Employment
                NettoActualErEmployment = r.NettoActual,
                InitAmtErEmployment = r.SSEmployerEmployment, //* (SSErEmploymentPerc / 100m),
                InitMultiplierErEmployment = r.SSEmployerEmployment / (SSErEmploymentPerc / 100m),
                NewMultiplierErEmployment = r.NettoActual - SMKAmt,
                NewAmtErEmployment = 0m,
                DeviationErEmployment = 0m,

                //Er Health
                NettoActualErHealth = r.NettoActual,
                InitAmtErHealth = r.SSEmployerHealth, //* (SSErHealthPerc / 100m),
                InitMultiplierErHealth = r.SSEmployerHealth / (SSErHealthPerc / 100m),
                NewMultiplierErHealth = r.NettoActual - SMKAmt - ADCodePositionAdjustmentAmt,
                NewAmtErHealth = 0m,
                DeviationErHealth = 0m,

                //ErPension
                NettoActualErPension = r.NettoActual,
                InitAmtErPension = r.SSErPension,
                InitMultiplierErPension = r.SSErPension / (SSErPensionPerc / 100m),
                NewAmtErPension = 0m,
                DeviationErPension = 0m,

                //EeEmployment
                NettoActualEeEmployment = r.NettoActual,
                InitAmtEeEmployment = r.SSEmployeeEmployment,
                InitMultiplierEeEmployment = r.SSEmployeeEmployment / (SSEeEmploymentPerc / 100m),
                NewAmtEeEmployment = 0m,
                DeviationEeEmployment = 0m,

                //EeHealth
                NettoActualEeHealth = r.NettoActual,
                InitAmtEeHealth = r.SSEmployeeHealth,
                InitMultiplierEeHealth = r.SSEmployeeHealth / (SSEeHealthPerc / 100m),
                NewAmtEeHealth = 0m,
                DeviationEeHealth = 0m,

                //EePension
                NettoActualEePension = r.NettoActual,
                InitAmtEePension = r.SSEePension,
                InitMultiplierEePension = r.SSEePension / (SSEePensionPerc / 100m),
                NewAmtEePension = 0m,
                DeviationEePension = 0m
            });

            l.ForEach(x =>
            {
                if (x.NewMultiplierErHealth < mUMK) x.NewMultiplierErHealth = mUMK;
                if (x.NewMultiplierErEmployment < mUMK) x.NewMultiplierErEmployment = mUMK;
                if (x.NewMultiplierErHealth > SSErHealthSalaryMaxLimit) x.NewMultiplierErHealth = SSErHealthSalaryMaxLimit;

                x.DeviationErEmployment = x.InitMultiplierErEmployment - x.NewMultiplierErEmployment;
                x.DeviationErHealth = x.InitMultiplierErHealth - x.NewMultiplierErHealth;
                x.DeviationErPension = x.InitMultiplierErPension - x.NewMultiplierErEmployment;

                x.DeviationEeEmployment = x.InitMultiplierEeEmployment - x.NewMultiplierErEmployment;
                x.DeviationEeHealth = x.InitMultiplierEeHealth - x.NewMultiplierErHealth;
                x.DeviationEePension = x.InitMultiplierEePension - x.NewMultiplierErEmployment;

                x.NewAmtErEmployment = x.NewMultiplierErEmployment * (SSErEmploymentPerc / 100m);
                x.NewAmtErHealth = x.NewMultiplierErHealth * (SSErHealthPerc / 100m);
                x.NewAmtErPension = x.NewMultiplierErEmployment * (SSErPensionPerc / 100m);

                x.NewAmtEeEmployment = x.NewMultiplierErEmployment * (SSEeEmploymentPerc / 100m);
                x.NewAmtEeHealth = x.NewMultiplierErHealth * (SSEeHealthPerc / 100m);
                x.NewAmtEePension = x.NewMultiplierErEmployment * (SSEePensionPerc / 100m);
            });
        }

        private decimal GetPositionAdjustmentAmt(string EmpCode, string PayrunCode, decimal YearsOfService)
        {
            string EmpLevelCode = GetEmpLevelCode(EmpCode);
            string EmpGradeLvlCode = GetEmpGradeLvlCode(EmpCode);
            bool IsGradeLevelDifferent = false;
            bool IsLevelDifferent = false;

            // jadinya gak perlu cek PPSetting
            // mau ada PPSetting ataupun enggak, rumusnya tetep sama
            //GradeAndLevelComparation(EmpCode, ref IsGradeLevelDifferent, ref IsLevelDifferent);

            //if (!IsGradeLevelDifferent) EmpGradeLvlCode = string.Empty;
            //if (!IsLevelDifferent) EmpLevelCode = string.Empty;

            decimal Amt = ComputeAdjustmentAmt(EmpLevelCode, EmpGradeLvlCode, YearsOfService, IsGradeLevelDifferent, IsLevelDifferent);

            return Amt;
        }

        private decimal ComputeAdjustmentAmt(string LevelCode, string GradeLvlCode, decimal YearsOfService, bool IsGradeLevelDifferent, bool IsLevelDifferent)
        {
            string ServiceYrRange = GetPositionAdjustmentServiceYrRange(YearsOfService);
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            decimal Amt = 0m;

            SQL.AppendLine("/*FrmPayrollProcessMultiPayrun18 - ComputeAdjustmentAmt*/ ");
            SQL.AppendLine("Select Sum((IfNull(B.AllowanceAmt, 0.00) +IfNull(B.AllowanceAmt2, 0.00)) +IfNull(C.AllowanceAmt, 0.00) + IfNull(D.AllowanceAmt, 0.00)) Amt ");
            SQL.AppendLine("From TblPositionAdjustmentHdr A ");
            SQL.AppendLine("Left Join TblPositionAdjustmentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.LevelCode = @LevelCode ");
            if (LevelCode == "010") // hanya level code tertentu yang di TblPositionAdjustmentDtl ada serviceyrrange nya
                SQL.AppendLine("    And B.ServiceYrRange = @ServiceYrRange ");
            SQL.AppendLine("Left Join TblPositionAdjustmentDtl2 C On A.DocNo = C.DocNo ");
            SQL.AppendLine("    And C.GrdLvlCode = @GrdLvlCode ");
            SQL.AppendLine("    And C.LevelCode = @LevelCode ");
            SQL.AppendLine("    And C.ServiceYrRange = @ServiceYrRange ");
            SQL.AppendLine("Left Join TblPositionAdjustmentDtl3 D On A.DocNo = D.DocNo ");
            SQL.AppendLine("    And ServiceYr = @YearsOfService ");
            SQL.AppendLine("Where A.CancelInd = 'N' ");
            SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@LevelCode", LevelCode);
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", GradeLvlCode);
            Sm.CmParam<String>(ref cm, "@ServiceYrRange", ServiceYrRange);
            Sm.CmParam<String>(ref cm, "@YearsOfService", YearsOfService.ToString());

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Amt = Sm.DrDec(dr, c[0]);
                    }
                }
                dr.Close();
            }

            return Amt;
        }

        private void GradeAndLevelComparation(string EmpCode, ref bool IsGradeLevelDifferent, ref bool IsLevelDifferent)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string GrdLvlCode = string.Empty, GrdLvlCodeOld = string.Empty, LevelCode = string.Empty, LevelCodeOld = string.Empty;

            SQL.AppendLine("/*FrmPayrollProcessMultiPayrun18 - GradeAndLevelComparation*/ ");
            SQL.AppendLine("select a.GrdLvlCode, b.GrdLvlCodeOld, a.LevelCode, b.LevelCodeOld ");
            SQL.AppendLine("from tblemployeepps a ");
            SQL.AppendLine("inner join tblpps b on a.empcode = b.empcode ");
            SQL.AppendLine("    and a.EmpCode = @EmpCode ");
            SQL.AppendLine("    and a.EndDt is null ");
            SQL.AppendLine("    and a.StartDt = b.StartDt ");
            SQL.AppendLine("    and b.cancelind = 'N' ");
            SQL.AppendLine("    and b.status = 'A' ");
            SQL.AppendLine("    and a.StartDt <= Date_Format(Now(), '%Y%m%d') ");
            SQL.AppendLine("Limit 1; ");

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "GrdLvlCode", "GrdLvlCodeOld", "LevelCode", "LevelCodeOld" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdLvlCode = Sm.DrStr(dr, c[0]);
                        GrdLvlCodeOld = Sm.DrStr(dr, c[1]);
                        LevelCode = Sm.DrStr(dr, c[2]);
                        LevelCodeOld = Sm.DrStr(dr, c[3]);
                    }
                }
                dr.Close();
            }

            IsGradeLevelDifferent = GrdLvlCode != GrdLvlCodeOld;
            IsLevelDifferent = LevelCode != LevelCodeOld;
        }

        private string GetPositionAdjustmentServiceYrRange(decimal YearsOfService)
        {
            var l = new List<OptionPositionAdjustmentServiceYrRange>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/*FrmPayrollProcessMultiPayrun18 - GetPositionAdjustmentServiceYrRange*/ ");
            SQL.AppendLine("Select OptCode, Property1, Property2 ");
            SQL.AppendLine("From TblOption ");
            SQL.AppendLine("Where OptCat = 'PositionAdjustmentServiceYrRange'; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "Property1", "Property2" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new OptionPositionAdjustmentServiceYrRange()
                        {
                            OptCode = Sm.DrStr(dr, c[0]),
                            Property1 = Sm.DrStr(dr, c[1]),
                            Property2 = Sm.DrStr(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                for (int i = l.Count - 1; i >= 0; --i)
                {
                    if (l[i].Property2.Length == 0 && l[i].Property1.Length > 0)
                    {
                        if (YearsOfService >= Decimal.Parse(l[i].Property1))
                            return l[i].OptCode;
                    }
                    else if (l[i].Property2.Length > 0 && l[i].Property1.Length > 0)
                    {
                        if (YearsOfService <= Decimal.Parse(l[i].Property2) && YearsOfService >= Decimal.Parse(l[i].Property1))
                            return l[i].OptCode;
                    }
                }
            }

            l.Clear();

            return "0";
        }

        private string GetEmpLevelCode(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/*FrmPayrollProcessMultiPayrun18 - GetEmpLevelCode*/ ");
            SQL.AppendLine("Select LevelCode ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where EmpCode = @Param; ");

            return Sm.GetValue(SQL.ToString(), EmpCode);
        }

        private string GetEmpGradeLvlCode(string EmpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/*FrmPayrollProcessMultiPayrun18 - GetEmpGradeLvlCode*/ ");
            SQL.AppendLine("Select GrdLvlCode ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where EmpCode = @Param; ");

            return Sm.GetValue(SQL.ToString(), EmpCode);
        }

        private void SetUMK()
        {
            var SQL = new StringBuilder();

            mUMK = 0m;

            SQL.AppendLine("Select IfNull(B.Amt, 0.00) As Amt ");
            SQL.AppendLine("From TblRegionalMinimumWagesHdr A ");
            SQL.AppendLine("Inner Join TblRegionalMinimumWagesDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.SiteCode In (Select SiteCode From TblPayrun Where PayrunCode=@Param1 And SiteCode Is Not Null) ");
            SQL.AppendLine("Inner Join TblSite C on B.SiteCode=C.SiteCode And B.CityCode=C.CityCode And C.CityCode Is Not Null ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.StartDt Is Not Null ");
            SQL.AppendLine("And A.EndDt Is Not Null ");
            SQL.AppendLine("And A.StartDt<=@Param2 And @Param2<=A.EndDt ");
            SQL.AppendLine("Order By A.DocDt Desc, A.DocNo Desc Limit 1;");

            var UMK = Sm.GetValue(SQL.ToString(), mPayrunCode, mEndDt, string.Empty);
            if (UMK.Length > 0) mUMK = decimal.Parse(UMK);
        }

        private decimal ComputeDuration(string Value1, string Value2)
        {
            return Convert.ToDecimal((Sm.ConvertDateTime(Value1) - Sm.ConvertDateTime(Value2)).TotalHours);
        }

        private void GetDt(ref List<string> l)
        {
            string StartDt = mStartDt, EndDt = mEndDt;

            DateTime
                Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    ),
                Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

            var TotalDays = (Dt2 - Dt1).Days;
            l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            for (int i = 1; i <= TotalDays; i++)
            {
                Dt1 = Dt1.AddDays(1);
                l.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );
            }
        }

        private decimal RoundTo30Minutes(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0.5)
                return (decimal)(Value2 + 0.5);
            else
            {
                if (Value - Value2 == 0)
                    return (decimal)(Value);
                else
                {
                    if (Value - Value2 > 0 && Value - Value2 < 0.5)
                        return (decimal)(Value2);
                    else
                        return (decimal)(Value);
                }
            }
        }

        private decimal RoundTo1Hour(double Value)
        {
            int Value2 = (int)(Value);
            if (Value - Value2 > 0)
                return (decimal)(Value2);
            else
                return (decimal)(Value);
        }

        #endregion

        #endregion

        #region Event

        private void FrmPayrollProcess_Load(object sender, EventArgs e)
        {
            try
            {
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                string 
                    Yr = Sm.Left(CurrentDateTime, 4),
                    Mth = CurrentDateTime.Substring(4, 2);

                if (Mth == "01")
                {
                    Yr = (int.Parse(Yr) - 1).ToString();
                    Mth = "12";
                }
                else
                    Mth = (int.Parse(Mth) - 1).ToString();

                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Yr);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, Mth);

                GetParameter();
                GetValue();
                SetGrd();
                mlPayrunEmployee = new List<PayrunEmployee>();
                mlAttendanceLog = new List<AttendanceLog>();
                mlOT = new List<OT>();
                mlOTAdjustment = new List<OTAdjustment>();
                mlAdvancePaymentProcess = new List<AdvancePaymentProcess>();
                mEmployeeMealTransport = new List<EmployeeMealTransport>();
                mlOTSpecial = new List<OTSpecial>();
                mlGradeSalary = new List<GradeSalary>();

                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        Grd1.Cells[Row, 0].Value = !IsSelected;
            }
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                ShowPayrunInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    Grd1.Cells[r, 3].Value = false;
                    Grd1.Cells[r, 17].Value = string.Empty;
                }
                if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsProcessedDataNotValid()) return;

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 0) && Sm.GetGrdStr(Grd1, r, 1).Length>0)
                    {
                        mPayrunCode = string.Empty;
                        mStartDt = string.Empty;
                        mEndDt = string.Empty;
                        mDeptCode = string.Empty;
                        mSiteCode = string.Empty;
                        mSystemType = string.Empty;
                        mPayrunPeriod = string.Empty;
                        mPGCode = string.Empty;
                        mHOInd = true;

                        mlPayrunEmployee.Clear();
                        mlAttendanceLog.Clear();
                        mlOT.Clear();
                        mlOTAdjustment.Clear();
                        mlAdvancePaymentProcess.Clear();
                        mEmployeeMealTransport.Clear();
                        mlOTSpecial.Clear();
                        mlGradeSalary.Clear();

                        mPayrunCode = Sm.GetGrdStr(Grd1, r, 1);
                        mStartDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 4), 8);
                        mEndDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 5), 8);
                        mSiteCode = Sm.GetGrdStr(Grd1, r, 6);
                        mDeptCode = Sm.GetGrdStr(Grd1, r, 8);
                        mSystemType = Sm.GetGrdStr(Grd1, r, 10);
                        mPayrunPeriod = Sm.GetGrdStr(Grd1, r, 12);
                        mPGCode = Sm.GetGrdStr(Grd1, r, 14);
                        mHOInd = Sm.GetGrdBool(Grd1, r, 16);

                        ProcessData1();
                        if (mlPayrunEmployee.Count > 0)
                        {
                            ProcessData2();
                            Grd1.Cells[r, 3].Value = true;
                            Grd1.Cells[r, 17].Value = "Process is completed.";
                        }
                        else
                            Grd1.Cells[r, 17].Value = "No employee.";
                    }
                }
                Sm.StdMsg(mMsgType.Info, "Process is completed.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Class

        private class PayrunEmployee
        {
            public string EmpCode { get; set; }
        }

        private class PayrollProcessAD
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string ADType { get; set; }
            public decimal Amt { get; set; }
        }

        private class Result1
        {
            public bool _PassAwayInd { get; set; }
            public bool _DoubleShiftInd { get; set; }
            public bool _IsUseLatestGrdLvlSalary { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string _LatestLevelCode { get; set; }
            public decimal _LatestMonthlyGrdLvlSalary { get; set; }
            public decimal _LatestDailyGrdLvlSalary { get; set; }
            public bool _IsFullDayInd { get; set; }
            public bool _IsHOInd { get; set; }
            public decimal _ExtraFooding { get; set; }
            public decimal _FieldAssignment { get; set; }
            public decimal _IncPerformance { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public bool ProcessInd { get; set; }
            public string LatestPaidDt { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string PosCode { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string SystemType { get; set; }
            public string EmploymentStatus { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public decimal WorkingDay { get; set; }
            public string WSCode { get; set; }
            public bool HolInd { get; set; }
            public decimal HolidayIndex { get; set; }
            public bool WSHolidayInd { get; set; }
            public string WSIn1 { get; set; }
            public string WSOut1 { get; set; }
            public string WSIn2 { get; set; }
            public string WSOut2 { get; set; }
            public string WSIn3 { get; set; }
            public string WSOut3 { get; set; }
            public bool OneDayInd { get; set; }
            public bool LateInd { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
            public string WorkingIn { get; set; }
            public string WorkingOut { get; set; }
            public decimal WorkingDuration { get; set; }
            public decimal BasicSalary { get; set; }
            public decimal BasicSalary2 { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal ProductionWages { get; set; }
            public decimal Salary { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public bool PaidLeaveInd { get; set; }
            public bool CompulsoryLeaveInd { get; set; }
            public bool _DeductTHPInd { get; set; }
            public string LeaveStartTm { get; set; }
            public string LeaveEndTm { get; set; }
            public decimal LeaveDuration { get; set; }
            public decimal PLDay { get; set; }
            public decimal PLHr { get; set; }
            public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
            public decimal OT2Hr { get; set; }
            public decimal OTHolidayHr { get; set; }
            public decimal OT1Amt { get; set; }
            public decimal OT2Amt { get; set; }
            public decimal OTHolidayAmt { get; set; }
            public bool OTToLeaveInd { get; set; }
            public decimal IncMinWages { get; set; }
            public decimal IncProduction { get; set; }
            public decimal IncPerformance { get; set; }
            public bool PresenceRewardInd { get; set; }
            public decimal HolidayEarning { get; set; }
            public decimal ExtraFooding { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal DedProduction { get; set; }
            public decimal DedProdLeave { get; set; }
            public decimal EmpSalary { get; set; }
            public decimal EmpSalary2 { get; set; }
            public decimal SalaryAD { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal IncEmployee { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal ADOT { get; set; }
            public bool GoHomeEarlyInd { get; set; }
            public decimal DedAtd { get; set; }
            public decimal LateMinute { get; set; }
            public decimal GoHomeEarlyMinute { get; set; }
            public decimal DedAtdIndex { get; set; }
            public bool OTSpecialInd { get; set; }
            public string DivisionCode { get; set; }
            public bool GetOTAmt { get; set; }
            public decimal OTMeal { get; set; }
            public decimal OTMonthlyIncome { get; set; }
        }

        private class Result2
        {
            public bool _IsADSMKExisted { get; set; }
            public decimal _ADTransport { get; set; }
            public bool _IsHOInd { get; set; }
            public decimal _RecomputedValueForFixedAD { get; set; }
            public decimal _RecomputedValueForProratedAD { get; set; }
            public string _LatestGrdLvlCode { get; set; }
            public string PayrunCode { get; set; }
            public string EmpCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string NPWP { get; set; }
            public string PTKP { get; set; }
	        public decimal Salary { get; set; }
	        public decimal WorkingDay { get; set; }
	        public decimal PLDay { get; set; }
	        public decimal PLHr { get; set; }
	        public decimal PLAmt { get; set; }
            public decimal ProcessPLAmt { get; set; }
            public decimal OT1Hr { get; set; }
	        public decimal OT2Hr { get; set; }
	        public decimal OTHolidayHr { get; set; }
	        public decimal OT1Amt { get; set; }
	        public decimal OT2Amt { get; set; }
	        public decimal OTHolidayAmt { get; set; }
            public decimal FixAllowance { get; set; }
            public decimal FixAllowanceDeviationDueToPassedAway { get; set; }
	        public decimal EmploymentPeriodAllowance { get; set; }
	        public decimal IncEmployee { get; set; }
	        public decimal IncMinWages { get; set; }
	        public decimal IncProduction { get; set; }
	        public decimal IncPerformance { get; set; }
	        public decimal PresenceReward { get; set; }
	        public decimal HolidayEarning { get; set; }
	        public decimal ExtraFooding { get; set; }
	        public decimal SSEmployerHealth { get; set; }
	        public decimal SSEmployerEmployment { get; set; }
            public decimal SSEmployerNonRetiring { get; set; }
            public decimal SSEmployeeOldAgeInsurance { get; set; }
	        public decimal FixDeduction { get; set; }
	        public decimal DedEmployee { get; set; }
	        public decimal DedProduction { get; set; }
	        public decimal DedProdLeave { get; set; }
            public decimal EmpAdvancePayment { get; set; }
	        public decimal SSEmployeeHealth { get; set; }
	        public decimal SSEmployeeEmployment { get; set; }
            public decimal SalaryAdjustment { get; set; }
	        public decimal Tax { get; set; }
            public decimal TaxAllowance { get; set; }
            public decimal Amt { get; set; }
            public decimal UPLDay { get; set; }
            public decimal UPLHr { get; set; }
            public decimal UPLAmt { get; set; }
            public decimal ProcessUPLAmt { get; set; }
            public decimal NonTaxableFixAllowance { get; set; }
            public decimal NonTaxableFixDeduction { get; set; }
            public decimal TaxableFixAllowance { get; set; }
            public decimal TaxableFixDeduction { get; set; }
            public decimal FieldAssignment { get; set; }
            public decimal Meal { get; set; }
            public decimal Transport { get; set; }
            public decimal Functional { get; set; }
            public decimal Housing { get; set; }
            public decimal MobileCredit { get; set; }
            public decimal Zakat { get; set; }
            public decimal ServiceChargeIncentive { get; set; }
            public decimal SalaryPension { get; set; }
            public decimal SSErLifeInsurance { get; set; }
            public decimal SSEeLifeInsurance { get; set; }
            public decimal SSErWorkingAccident { get; set; }
            public decimal SSEeWorkingAccident { get; set; }
            public decimal SSErRetirement { get; set; }
            public decimal SSEeRetirement { get; set; }
            public decimal SSErPension { get; set; }
            public decimal SSEePension { get; set; }
            public string CreditCode1 { get; set; }
            public string CreditCode2 { get; set; }
            public string CreditCode3 { get; set; }
            public string CreditCode4 { get; set; }
            public string CreditCode5 { get; set; }
            public string CreditCode6 { get; set; }
            public string CreditCode7 { get; set; }
            public string CreditCode8 { get; set; }
            public string CreditCode9 { get; set; }
            public string CreditCode10 { get; set; }
            public decimal CreditAdvancePayment1 { get; set; }
            public decimal CreditAdvancePayment2 { get; set; }
            public decimal CreditAdvancePayment3 { get; set; }
            public decimal CreditAdvancePayment4 { get; set; }
            public decimal CreditAdvancePayment5 { get; set; }
            public decimal CreditAdvancePayment6 { get; set; }
            public decimal CreditAdvancePayment7 { get; set; }
            public decimal CreditAdvancePayment8 { get; set; }
            public decimal CreditAdvancePayment9 { get; set; }
            public decimal CreditAdvancePayment10 { get; set; }
            public decimal FixAllowance1 { get; set; }
            public decimal FixAllowance2 { get; set; }
            public decimal TaxableFixAllowance1 { get; set; }
            public decimal TaxableFixAllowance2 { get; set; }
            public decimal ADLeave { get; set; }
            public bool PensionInd { get; set; }
            public decimal PerformanceValue { get; set; }
            public string PerformanceStatus { get; set; }
            public string GradePerformance { get; set; }
            public decimal ADOT { get; set; }
            public decimal DedAtd { get; set; }
            public bool OTSpecialInd { get; set; }
            public decimal SSEmployerNonBPJS { get; set; }
            public decimal SSEmployeeNonBPJS { get; set; }
            public decimal TaxableFixedAllowance { get; set; }
            public decimal WarningLetter { get; set; }
            public decimal ServiceYearAllowance { get; set; }
            public decimal Brutto { get; set; }
            public decimal Netto { get; set; }
            public decimal NettoActual { get; set; }
            public bool PassAwayInd { get; set; }
            public decimal OTMeal { get; set; }
            public decimal SSEmployerPension2 { get; set; }
            public decimal SSEmployeePension2 { get; set; }
            public decimal SSEmployerPension { get; set; }
            public decimal SSEmployeePension { get; set; }
            public decimal SSEmployerBNILife { get; set; }
            public decimal SSEmployeeBNILife { get; set; }
            public decimal SSEmployerDPNPHT { get; set; }
            public decimal SSEmployeeDPNPHT { get; set; }
            public decimal SSEmployerDPNPNS { get; set; }
            public decimal SSEmployeeDPNPNS { get; set; }
            public decimal SSEmployerPKES { get; set; }
            public decimal SSEmployeePKES { get; set; }
            public decimal SSEmployerPRMH { get; set; }
            public decimal SSEmployeePRMH { get; set; }
            public decimal SSEmployerTSP { get; set; }
            public decimal SSEmployeeTSP { get; set; }
            public decimal SSEmployerWNR { get; set; }
            public decimal SSEmployeeWNR { get; set; }
        }

        #region Additional Class

        private class OptionPositionAdjustmentServiceYrRange
        { 
            public string OptCode { get; set; }
            public string Property1 { get; set; }
            public string Property2 { get; set; }
        }

        private class AdvancePaymentProcess
        {
            public string DocNo { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string EmpCode { get; set; }
            public string CreditCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmployeeSalary
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public decimal SalaryAD { get; set; }
        }

        private class EmployeeSalarySS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class IncEmployee
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class DedEmployee
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class OT
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
            public string OTSpecialInd { get; set; }
        }

        private class OTSpecial
        {
            public string SourceCode { get; set; }
            public string DeptCode { get; set; }
            public string SiteCode { get; set; }
            public string DivisionCode { get; set; }
            public decimal OTAmt { get; set; }
            public decimal MealAmt { get; set; }
        }

        private class OTAdjustment
        {
            public string OTDocNo { get; set; }
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm1 { get; set; }
            public string Tm2 { get; set; }
        }

        private class NTI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string Status { get; set; }
            public decimal Amt { get; set; }
        }

        private class TI
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal TaxRate { get; set; }
        }

        private class TI2
        {
            public string PayrunCode { get; set; }
            public string DocNo { get; set; }
            public string SeqNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Value1 { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
        }

        private class EmployeePPS
        {
            public string EmpCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string DeptCode { get; set; }
            public string GrdLvlCode { get; set; }
            public string LevelCode { get; set; }
            public string EmploymentStatus { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public string PGCode { get; set; }
            public string SiteCode { get; set; }
            public string PosCode { get; set; }
            public string HOInd { get; set; }
            public string DivisionCode { get; set; }
        }

        private class EmpWorkSchedule
         {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string WSCode { get; set; }
            public string HolidayInd { get; set; }
            public string DoubleShiftInd { get; set; }
            public string In1 { get; set; }
            public string Out1 { get; set; }
            public string In2 { get; set; }
            public string Out2 { get; set; }
            public string In3 { get; set; }
            public string Out3 { get; set; }
        }

        private class Atd
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string ActualIn { get; set; }
            public string ActualOut { get; set; }
        }

        private class Leave
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveType { get; set; }
            public string StartTm { get; set; }
            public string EndTm { get; set; }
            public decimal DurationHr { get; set; }
            public string PaidInd { get; set; }
            public string CompulsoryLeaveInd { get; set; }
        }

        private class LeaveDtl
        {
            public string LeaveCode { get; set; }
            public string SystemType { get; set; }
            public string PayrunPeriod { get; set; }
            public bool DeductTHPInd { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string LatestPaidDt { get; set; }
            public bool PassAwayInd { get; set; }
        }

        private class AttendanceLog
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
            public string Tm { get; set; }
        }

        private class EmployeeMealTransport
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class EmpPaidDt
        {
            public string EmpCode { get; set; }
            public string Dt { get; set; }
        }

        private class EmpSCI
        {
            public string EmpCode { get; set; }
            public string DocDt { get; set; }
            public decimal Amt { get; set; }
        }

        private class WorkScheduleADMealTransport
        {
            public string WSCode { get; set; }
            public string ADCode { get; set; }
        }

        private class EmployeeDeductionAtd
        {
            public string EmpCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class GradeSalary
        {
            public string GrdSalaryCode { get; set; }
            public decimal WorkPeriod { get; set; }
            public string Month { get; set; }
            public decimal Amt { get; set; }
        }

        private class RecalculateSocialSecurity
        {
            public string EmpCode { get; set; }
            public string PayrunCode { get; set; }

            //employer health
            public decimal NettoActualErHealth { get; set; }
            public decimal InitAmtErHealth { get; set; }
            public decimal InitMultiplierErHealth { get; set; }
            public decimal NewMultiplierErHealth { get; set; }
            public decimal NewAmtErHealth { get; set; }
            public decimal DeviationErHealth { get; set; }

            //employer employment
            public decimal NettoActualErEmployment { get; set; }
            public decimal InitAmtErEmployment { get; set; }
            public decimal InitMultiplierErEmployment { get; set; }
            public decimal NewMultiplierErEmployment { get; set; }
            public decimal NewAmtErEmployment { get; set; }
            public decimal DeviationErEmployment { get; set; }

            //employer pension
            public decimal NettoActualErPension { get; set; }
            public decimal InitAmtErPension { get; set; }
            public decimal InitMultiplierErPension { get; set; }
            public decimal NewAmtErPension { get; set; }
            public decimal DeviationErPension { get; set; }

            //employee health
            public decimal NettoActualEeHealth { get; set; }
            public decimal InitAmtEeHealth { get; set; }
            public decimal InitMultiplierEeHealth { get; set; }
            public decimal NewAmtEeHealth { get; set; }
            public decimal DeviationEeHealth { get; set; }

            //employee employment
            public decimal NettoActualEeEmployment { get; set; }
            public decimal InitAmtEeEmployment { get; set; }
            public decimal InitMultiplierEeEmployment { get; set; }
            public decimal NewAmtEeEmployment { get; set; }
            public decimal DeviationEeEmployment { get; set; }

            //employee pension
            public decimal NettoActualEePension { get; set; }
            public decimal InitAmtEePension { get; set; }
            public decimal InitMultiplierEePension { get; set; }
            public decimal NewAmtEePension { get; set; }
            public decimal DeviationEePension { get; set; }

        }

        #endregion

        #endregion
    }
}
