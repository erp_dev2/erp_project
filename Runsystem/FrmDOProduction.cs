﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmDOProduction : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mCtCode = string.Empty, mWhsCode = string.Empty;
        internal FrmDOProductionFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType = "21";

        #endregion

        #region Constructor

        public FrmDOProduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Production";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetNumberOfInventoryUomCode();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item Code",
                        "",
                        
                        //6-10
                        "Item Name",
                        "Batch Number",
                        "Source",
                        "Lot",
                        "Bin",

                        //11-15
                        "Quantity"+Environment.NewLine+"DO",
                        "UoM"+Environment.NewLine+"(Planning)",
                        "Stock",
                        "Quantity"+Environment.NewLine+"(Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",

                        //16-20
                        "Stock 2",
                        "Quantity 2"+Environment.NewLine+"(Inventory)",
                        "Uom 2"+Environment.NewLine+"(Inventory)",
                        "Stock 3",
                        "Quantity 3"+Environment.NewLine+"(Inventory)",

                        //22
                        "Uom 3"+Environment.NewLine+"(Inventory)",
                        "Remark"
                    },
                     new int[] 
                    {
                        20, 
                        50, 50, 20, 100, 20, 
                        300, 200, 200, 100, 100, 
                        80, 80, 80, 80, 80,  
                        80, 80, 80, 80, 80, 
                        80, 400
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 14, 16, 17, 18, 19, 20 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 8, 16, 17, 18, 19, 20, 21 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 13;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Bom Number",
                        "Bom DNo",
                        "PO Dno",
                        "",
                        "Item Code",

                        //6-10
                        "Item Name",
                        "",
                        "Quantity",
                        "Delivered"+Environment.NewLine+"Quantity",
                        "Balanced",

                        //11-12
                        "Quantity DO",
                        "Balanced"
                    },
                     new int[] 
                    {
                        20,  
                        150, 80, 80, 20, 100,  
                        150, 20, 100, 100, 100, 
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 8, 9, 10, 11, 12 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 0, 4, 7 });
            Sm.GrdColInvisible(Grd2, new int[] {  2, 3, 5 }, false);

            #endregion

            #region Grid 3
            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "Item Code",
                        "Item Local"+Environment.NewLine+"Code",
                        "",
                        "Item Name",
                        "UoM"+Environment.NewLine+"(Planning)",

                        //6-10
                        "Quantity",
                        "Delivered"+Environment.NewLine+"Quantity",
                        "Balanced",
                        "Quantity DO",
                        "Balanced"
                    },
                     new int[] 
                    {
                        100,   
                        100, 100, 20, 200, 80,  
                        100, 100, 100, 100, 100
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 6, 7, 8, 9, 10 }, 0);
            Sm.GrdColButton(Grd3, new int[] { 3 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 2 }, false);

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18 }, true);
            
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtPODocNo, LueWhsCode, TxtItCode, TxtItName, TxtQty, TxtPlanningUomCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    BtnPODocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 11, 14, 17, 20, 22 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0, 4, 7 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 3 });
                    BtnPODocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mWhsCode = string.Empty;
            mCtCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtPODocNo, LueWhsCode, TxtItCode, TxtItName, TxtQty, TxtPlanningUomCode, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 13, 15, 16, 17, 19, 20 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 8, 9, 10, 11, 12 });

            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 6, 7, 8, 9, 10 }); 
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOProductionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                tabControl1.SelectTab("TpgBOM");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 14, 17, 20 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 22 }, e);

            if (e.ColIndex == 11)
            {
                ComputeItemDOQty(Sm.GetGrdStr(Grd1, e.RowIndex, 4));
                ComputeItemDOQty2();
            }

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 15)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 14, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 11 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 12), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 11);

            if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 18)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 17, Grd1, e.RowIndex, 14);

            if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 14);

            if (e.ColIndex == 17 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 18), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 17);

            if (e.ColIndex == 1)
            {
                ComputeItemDOQty(Sm.GetGrdStr(Grd1, e.RowIndex, 4));
                ComputeItemDOQty2();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtPODocNo, "Production Order", false) && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmDOProductionDlg2(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd2, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 3);
                f.ShowDialog();
            }

        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsTxtEmpty(TxtPODocNo, "Production Order", false) && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOProductionDlg2(this, Sm.GetLue(LueWhsCode)));
                    }
                }
            }
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd2, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);
                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                            ComputeItemDOQty();
                            ComputeItemDOQty2();
                        }
                    }
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0 && !Sm.IsTxtEmpty(TxtPODocNo, "Production Order", false))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOProductionDlg3(this, TxtPODocNo.Text));
                    }
                }
            }

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd2, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtDocNo.Text.Length == 0)
            {
                if (Grd2.SelectedRows.Count > 0)
                {
                    if (Grd2.Rows[Grd2.Rows[Grd2.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd2.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                int Row = Grd2.SelectedRows[Index].Index;                                
                                Grd2.Rows.RemoveAt(Row);
                                Sm.ClearGrd(Grd3, true);
                                Sm.ClearGrd(Grd1, true);
                            }
                            InsertItem();
                            if (Grd3.Rows.Count <= 0) Grd3.Rows.Add();
                        }
                    }
                }
            }
            Sm.GrdEnter(Grd2, e);
            Sm.GrdTabInLastCell(Grd2, e, BtnFind, BtnSave);

        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsTxtEmpty(TxtPODocNo, "Production Order", false))
                Sm.FormShowDialog(new FrmDOProductionDlg3(this, TxtPODocNo.Text));

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd2, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 4);
                f.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmBom2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f.ShowDialog();
            }
        }


        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            //CalculateQty();
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOProduction", "TblDOProductionHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOPHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveDOPDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 5).Length > 0) cml.Add(SaveDOPDtl2(DocNo, Row));

            cml.Add(SaveStock(DocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse")||
                Sm.IsTxtEmpty(TxtPODocNo, "Production Order#", false) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Bill Of Material.");
                return true;
            }

            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Batch Number : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 11) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) > Sm.GetGrdDec(Grd1, Row, 13))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (Inventory) should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 17) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity 2 (Inventory) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 17) > Sm.GetGrdDec(Grd1, Row, 16))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity 2 (Inventory) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 20) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity 3 (Inventory) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 20) > Sm.GetGrdDec(Grd1, Row, 19))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity 3 (Inventory) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveDOPHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOProductionHdr(DocNo, DocDt, WhsCode, ProductionOrderDocNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @WhsCode, @ProductionOrderDocNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@ProductionOrderDocNo", TxtPODocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOPDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOProductionDtl(DocNo, DNo, CancelInd, ItCode, BatchNo, Source, Lot, Bin, QtyPlanning, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, @BatchNo, @Source, @Lot, @Bin, @QtyPlanning, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@QtyPlanning", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOPDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOProductionDtl2(DocNo, DNo, ProductionOrderDNo, BomDno, Qty, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ProductionOrderDNo, @BomDno, @Qty, @CreateBy, CurrentDateTime()) "+
                    "On Duplicate Key "+
                    "Update Qty=@Qty, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BomDno", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@ProductionOrderDNo", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 11));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOProductionHdr A ");
            SQL.AppendLine("Inner Join TblDOProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join TblStockMovement T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
            SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T2.DocType=@DocType ");
            SQL.AppendLine("    And T2.DocNo=@DocNo ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=T1.Qty+T2.Qty, T1.Qty2=T1.Qty2+T2.Qty2, T1.Qty3=T1.Qty3+T2.Qty3, ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (IsCancelledDataNotValid(DNo) || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDOPDtl(DNo));
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveDOPDtl2(TxtDocNo.Text, Row));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDOProductionDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo);
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelDOPDtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOProductionDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty*-1, Qty2*-1, Qty3*-1, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DocType=@DocType ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.ItCode, A.BatchNo, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, ItCode, BatchNo, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='Y' ");
            SQL.AppendLine("        And Position(Concat('##', DNo, '##') In @DNo)>0 ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.ItCode=B.ItCode ");
            SQL.AppendLine("        And A.BatchNo=B.BatchNo ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0 Or A.Qty2<>0 Or A.Qty3<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.ItCode, A.BatchNo, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.ItCode=T2.ItCode ");
            SQL.AppendLine("    And T1.BatchNo=T2.BatchNo ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOPHdr(DocNo);
                ShowBOMInfo(DocNo);
                InsertItem();
                ShowDOPDtl(DocNo);
                ReComputeStock();
                ComputeItemDOQty();
                ComputeBalanceBOM();
                ComputeItemDOQty2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOPHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ProductionOrderDocNo, A.WhsCode, ");
            SQL.AppendLine("Case When B.DocType = '1' Then C.ItCode ");
            SQL.AppendLine("When B.DocType = '2' Then D.ItCode ");
            SQL.AppendLine("End As ItCode,  ");
            SQL.AppendLine("Case When B.DocType = '1' Then C.ItName ");
            SQL.AppendLine("When B.DocType = '2' Then D.ItName ");
            SQL.AppendLine("End As ItName,  ");
            SQL.AppendLine("Case When B.DocType = '1' then C.PlanningUomCode ");
            SQL.AppendLine("When B.DocType = '2' then D.PlanningUomCode ");
            SQL.AppendLine("End As Uom, B.Qty, A.Remark ");
            SQL.AppendLine("From TblDOProductionHdr A  ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr B On A.ProductionOrderDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode  ");
            SQL.AppendLine("Left Join (   ");
            SQL.AppendLine("    Select A.DocNo, A.Dno, A.ItCode, B.Itname, B.PlanningUomCode  ");
            SQL.AppendLine("    From TblSODtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode) C On B.SODocNo = C.DocNo And B.SODno = C.Dno  ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select A.DocNo, A.Dno, A.ItCode, B.Itname, B.PlanningUomCode  ");
            SQL.AppendLine("    From TblMakeToStockDtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode) D On B.MakeToStockDocNo = D.DocNo And B.MakeToStockDno = D.Dno "); 
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "ProductionOrderDocNo", "WhsCode", "ItCode", "ItName", 
                        
                        //6-8
                        "UoM", "Qty", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtPODocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        TxtItCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPlanningUomCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                    }, true
                );
        }

        private void ShowDOPDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.QtyPlanning, C.PlanningUomCode, B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark ");
            SQL.AppendLine("From TblDOProductionHdr A ");
            SQL.AppendLine("Inner Join TblDOProductionDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItName", "BatchNo", "Source",  
                    
                    //6-10
                    "Lot", "Bin", "QtyPlanning", "PlanningUomCode", "Qty",   
                    
                    //11-15
                    "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                    
                    //16-17
                    "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);

                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);

                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 14, 17, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedBOMNumber()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedBOMNumber2()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            ("'" +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            Sm.GetGrdStr(Grd2, Row, 2) +
                            "'");
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                           ("'" +
                            Sm.GetGrdStr(Grd3, Row, 1) +
                            "'");
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem3()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ComputeItemDOQty()
        {
            decimal Qty = 0m;
            string ItCode = string.Empty;
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                {
                    Qty = 0m;
                    ItCode = Sm.GetGrdStr(Grd3, Row, 1);
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                    {
                        if (
                            Sm.GetGrdStr(Grd1, Row2, 4).Length != 0 &&
                            Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, Row2, 4))&&
                            Sm.GetGrdBool(Grd1, Row2, 1) == false
                            )
                        {
                            Qty += Sm.GetGrdDec(Grd1, Row2, 11);
                        }
                    }
                    Grd3.Cells[Row, 9].Value = Qty;
                    Grd3.Cells[Row, 10].Value = Sm.GetGrdDec(Grd3, Row, 8) - Qty;
                }
            }
        }

        private void ComputeItemDOQty(string ItCode)
        {
            decimal Qty = 0m;
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd3, Row, 1).Length != 0 &&
                    Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd3, Row, 1))
                    )
                {
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                    {
                        if (
                            Sm.GetGrdStr(Grd1, Row2, 4).Length != 0 &&
                            Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, Row2, 4))&&
                            Sm.GetGrdBool(Grd1, Row2, 1) == false
                            )
                        {
                            Qty += Sm.GetGrdDec(Grd1, Row2, 11);
                        }
                    }
                    Grd3.Cells[Row, 9].Value = Qty;
                    Grd3.Cells[Row, 10].Value = Sm.GetGrdDec(Grd3, Row, 8) - Qty;
                    break;
                }
            }
        }

        private void ComputeItemDOQty2()
        {
            //decimal Qty = 0m;
            //for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            //{
            //    if (
            //        Sm.GetGrdStr(Grd2, Row, 5).Length != 0 &&
            //        Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd2, Row, 5))
            //        )
            //    {
            //        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
            //        {
            //            if (
            //                Sm.GetGrdStr(Grd1, Row2, 4).Length != 0 &&
            //                Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, Row2, 4))&&
            //                Sm.GetGrdBool(Grd1, Row2, 1) == false
            //                )
            //            {
            //                Qty += Sm.GetGrdDec(Grd1, Row2, 11);
            //            }
            //        }

            //        Grd2.Cells[Row, 11].Value = Qty;
            //        Grd2.Cells[Row, 12].Value = Sm.GetGrdDec(Grd2, Row, 10) - Qty;
            //        break;
            //    }
            //}

            string ItCode = string.Empty;
            decimal Qty = 0, Qty2 = 0;
            int CurrRow = -1;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    ItCode = Sm.GetGrdStr(Grd3, Row, 1);
                    Qty = Sm.GetGrdDec(Grd3, Row, 9);
                    CurrRow = -1;
                    for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
                    {
                        if (Sm.GetGrdStr(Grd2, Row2, 5).Length > 0)
                        {
                            if (Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd2, Row2, 5)))
                            {
                                Qty2 = Sm.GetGrdDec(Grd2, Row2, 9);
                                if (Qty2 >= Qty)
                                {
                                    Grd2.Cells[Row2, 11].Value = Qty;
                                    Qty = 0m;
                                }
                                else
                                {
                                    Grd2.Cells[Row2, 11].Value = Qty2;
                                    Qty -= Qty2;
                                }
                                Grd2.Cells[Row2, 12].Value = Sm.GetGrdDec(Grd2, Row2, 10) - Sm.GetGrdDec(Grd2, Row2, 11);
                                CurrRow = Row2;
                            }
                        }
                    }
                    if (Qty > 0 && CurrRow >= 0)
                    {
                        Grd2.Cells[CurrRow, 11].Value = Sm.GetGrdDec(Grd2, CurrRow, 11) + Qty;
                        Grd2.Cells[CurrRow, 12].Value = Sm.GetGrdDec(Grd2, CurrRow, 10) - Sm.GetGrdDec(Grd2, CurrRow, 11);
                    }
                }
            }
        }

        internal void ComputeBalanceBOM()
        {
            decimal qty = 0m;
            decimal qtydelivered = 0m;
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                qty = Sm.GetGrdDec(Grd2, Row, 8);
                qtydelivered = Sm.GetGrdDec(Grd2, Row, 9);
                Grd2.Cells[Row, 10].Value = qty - qtydelivered;
            }
        }

        //private void ComputeBalanceBOM2()
        //{
        //    decimal balance = 0m;
        //    decimal qtydo = 0m;
        //    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
        //    {
        //        balance = Sm.GetGrdDec(Grd2, Row, 10);
        //        qtydo = Sm.GetGrdDec(Grd2, Row, 11);
        //        Grd2.Cells[Row, 12].Value = balance - qtydo;
        //    }
        //}

        //private void CalculateQty()
        //{
        //    for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
        //    {
        //        if (Sm.GetGrdDec(Grd2, Row, 12) < 0)
        //        {
        //            string itcode = Sm.GetGrdStr(Grd2, Row, 5);
        //            decimal balanced = Sm.GetGrdDec(Grd2, Row, 12);
        //            int xRow = Row;
        //            for (int Row2 = 0; Row2 < Grd2.Rows.Count - 1; Row2++)
        //            {
        //                if (Sm.CompareStr(itcode, Sm.GetGrdStr(Grd2, Row2, 5)) && Row2 != xRow)
        //                {
        //                    Grd2.Cells[Row2, 11].Value = balanced * (-1);
        //                    Grd2.Cells[xRow, 11].Value = Sm.GetGrdDec(Grd2, xRow, 10);
        //                }
        //            }
        //        }
        //    }
        //}

        internal void InsertItem()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.DocCode, T.ItName, T.ItCodeInternal, T.PlanningUomCOde, Sum(T.Qty) As Qty, SUM(T.QtyDelivered) As QtyDelivered  From (");
            SQL.AppendLine("Select B.DocCode, C.ItName, C.ItCodeInternal, C.PlanningUomCode, SUM(B.Qty) As Qty, ifnull(D.QtyDOP, 0) As  QtyDelivered ");
            SQL.AppendLine("From TblBomHdr A ");
            SQL.AppendLine("Inner Join TblBomdtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblItem C On B.DocCode = C.ItCode ");

            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select Y.BomDocNo, Y.BomDno, Y.ProductionOrderDno, X.DocCode, Y.QtyDOP ");
            SQL.AppendLine("    From tblbomDtl X ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
	        SQL.AppendLine("        Select A.ProductionOrderDocNo, B.ProductionOrderDno, ");
	        SQL.AppendLine("        C.BomDocNo, B.BomDno, SUM(B.Qty) As QtyDOP ");
	        SQL.AppendLine("        From TblDoProductionHdr A ");
	        SQL.AppendLine("        Inner Join TblDOProductionDtl2 B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("        left join ");
	        SQL.AppendLine("        ( ");
		    SQL.AppendLine("            Select A.DocNo, A.Dno, A.BomDocNo ");
		    SQL.AppendLine("            From TblProductionOrderDtl A  ");
	        SQL.AppendLine("        )C On A.ProductionOrderDocNo = C.DocNo And  B.ProductionOrderDno = C.Dno ");
            SQL.AppendLine("        Where A.DocNo<>@DocNo ");
            SQL.AppendLine("        Group By  A.ProductionOrderDocNo, B.ProductionOrderDno, C.BomDocNo, B.BomDno ");
            SQL.AppendLine("    )Y On X.DocNo = Y.BomDocNo And X.Dno = Y.BomDno  ");
            SQL.AppendLine("    Where X.DocType = '1' ");
            SQL.AppendLine(")D On D.BomDocNo = A.DocNo And D.BomDno = B.DNo And D.DocCode = B.DocCode");

            SQL.AppendLine("Where B.DocType = '1' And A.ActiveInd = 'Y' And Concat(A.DocNo, B.DNo) In ("+GetSelectedBOMNumber2()+") ");
            SQL.AppendLine("group By A.DocNo, B.Dno, B.DocCode, C.ItName, C.ItCodeInternal, C.PlanningUomCode  ");
            SQL.AppendLine(" )T Group by T.DocCode, T.ItName, T.ItCodeInternal, T.PlanningUomCOde ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", (TxtDocNo.Text.Length<1?"XXX":TxtDocNo.Text));
            Sm.CmParam<String>(ref cm, "@SelectedBom", GetSelectedBOMNumber());

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                           "DocCode",  

                            //1-5
                           "ItCodeInternal", "ItName", "PlanningUomCode", "Qty", "QtyDelivered"                           
                        }
                        );
                    if (!dr.HasRows)
                    {
                        //Sm.StdMsg(mMsgType.NoData, "");
                    }
                    else
                    {
                        Sm.ClearGrd(Grd3, true);
                        int Row3 = Grd3.Rows.Count - 1;
                        Grd3.ProcessTab = true;
                        Grd3.BeginUpdate();
                        while (dr.Read())
                        {
                            Grd3.Rows.Add();
                            Sm.SetGrdValue("S", Grd3, dr, c, Row3, 1, 0);
                            Sm.SetGrdValue("S", Grd3, dr, c, Row3, 2, 1);
                            Sm.SetGrdValue("S", Grd3, dr, c, Row3, 4, 2);
                            Sm.SetGrdValue("S", Grd3, dr, c, Row3, 5, 3);
                            Sm.SetGrdValue("N", Grd3, dr, c, Row3, 6, 4);
                            Sm.SetGrdValue("N", Grd3, dr, c, Row3, 7, 5);
                            Grd3.Cells[Row3, 8].Value = Sm.GetGrdDec(Grd3, Row3, 6) - Sm.GetGrdDec(Grd3, Row3, 7);
                            Grd3.Cells[Row3, 9].Value = 0m;
                            Grd3.Cells[Row3, 10].Value = 0m;
                            Row3++;
                        }
                        dr.Dispose();
                        Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 6, 7, 8, 9, 10 });
                        Grd3.EndUpdate();
                    }
                }
                cm.Dispose();
            }
        }

        internal void ShowBOMInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select E.DocNo As BomDocNo, B.BOMDNo, B.ProductionOrderDNo, G.DocCode, F.ItName, G.Qty, B.Qty As QtyDO, ifnull(H.QtyDOP, 0) As QtyDelivered "); 
            SQL.AppendLine("From TblDOProductionHdr A ");
            SQL.AppendLine("Inner Join TblDOProductionDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblProductionOrderHdr C On A.ProductionOrderDocNo = C.DOcNo ");
            SQL.AppendLine("Inner Join TblProductionOrderDtl D On C.DocNo = D.DocNo And B.ProductionOrderDNo = D.DNo ");
            SQL.AppendLine("Inner Join TblBOMHdr E On D.BOMDocNo = E.DOcNo ");
            SQL.AppendLine("Inner Join TblBomDtl G On D.BOmDocNo = G.DocNo  And B.BOMDNo = G.Dno ");
            SQL.AppendLine("Inner Join TblItem F On G.DocCode = F.ItCode ");

            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select Y.BomDocNo, Y.BomDno, Y.ProductionOrderDno, X.DocCode, Y.QtyDOP ");
            SQL.AppendLine("    From tblbomDtl X ");
            SQL.AppendLine("    Inner Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.ProductionOrderDocNo, B.ProductionOrderDno, ");
            SQL.AppendLine("        C.BomDocNo, B.BomDno, SUM(B.Qty) As QtyDOP ");
            SQL.AppendLine("        From TblDoProductionHdr A ");
            SQL.AppendLine("        Inner Join TblDOProductionDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        left join ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select A.DocNo, A.Dno, A.BomDocNo ");
            SQL.AppendLine("            From TblProductionOrderDtl A  ");
            SQL.AppendLine("        )C On A.ProductionOrderDocNo = C.DocNo And  B.ProductionOrderDno = C.Dno ");
            SQL.AppendLine("        Where A.DocNo<>@DocNo ");
            SQL.AppendLine("        Group By  A.ProductionOrderDocNo, B.ProductionOrderDno, C.BomDocNo, B.BomDno ");
            SQL.AppendLine("    )Y On X.DocNo = Y.BomDocNo And X.Dno = Y.BomDno  ");
            SQL.AppendLine("    Where X.DocType = '1' ");
            SQL.AppendLine(")H On H.BomDocNo = E.DocNo And H.BomDno = B.BOMDNo And H.DocCode = G.DocCode And H.ProductionOrderDno = B.ProductionOrderDno ");

            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("   Select A.ProductionOrderDocNo,  B.ProductionOrderDNo, E.BOMDocNo, B.BOMDNo, ");
            //SQL.AppendLine("    SUM(B.Qty) As QtyDelivered ");
            //SQL.AppendLine("    From tblDOProductionHdr A ");
            //SQL.AppendLine("    Inner Join TblDOProductiondtl2 B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    Inner Join TblProductionorderhdr D On A.ProductionOrderDocNo = D.DocNo ");
            //SQL.AppendLine("    Inner Join TblProductionOrderDtl E On D.DocNo = E.DocNo And E.DNo = B.BOMDNo ");
            //SQL.AppendLine("    Group By A.ProductionOrderDocNo,  B.ProductionOrderDNo,  B.BOMDNo ");
            //SQL.AppendLine(")H On H.ProductionOrderDocNo = A.ProductionOrderDocNo And H.ProductionOrderDNo = B.Dno And H.BomDno = B.BomDno  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "BomDocNo", 

                    //1-5
                    "bomDno", "ProductionOrderDNo", "DocCode", "ItName", "Qty",

                    //6
                    "QtyDelivered","QtyDO", 
                                        
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ReComputeStock()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItCode, BatchNo, Source, Lot, Bin, ");
            SQL.AppendLine("Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");
            SQL.AppendLine("And Locate(Concat('##', ItCode, BatchNo, Source, Lot, Bin, '##'), @SelectedItem)>0 ");
            SQL.AppendLine("Order By ItCode, BatchNo, Source, Lot, Bin;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@SelectedItem", GetSelectedItem2());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "ItCode",
 
                        //1-5
                        "BatchNo", "Source", "Lot", "Bin", "Qty",  
                        
                        //6-7
                        "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 4), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), Sm.DrStr(dr, 1)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 8), Sm.DrStr(dr, 2)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 9), Sm.DrStr(dr, 3)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Sm.DrStr(dr, 4)))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 5);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 6);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 19, 7);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnPODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOProductionDlg(this));
        }

        private void BtnPODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPODocNo, "Production Order#", false))
            {
                try
                {
                    var f = new FrmProductionOrder(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtPODocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPODocNo, "Item Code", false))
            {
                try
                {
                    var f = new FrmItem(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mItCode = TxtItCode.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }
        #endregion

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        #endregion  

    }
}
