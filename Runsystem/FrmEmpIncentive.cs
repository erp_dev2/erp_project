﻿#region Update
/*
    12/12/2019 [HAR/IMS] tambah informasi total amount di header 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpIncentive : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmEmpIncentiveFind FrmFind;

        #endregion

        #region Constructor

        public FrmEmpIncentive(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion


        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Employee's Social Security";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                Sl.SetLueDeptCode(ref LueDeptCode);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                //if (mDocNo.Length != 0)
                //{
                //    ShowData(mDocNo);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Old"+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Name",
                        "Position",
                        
                        //6-10
                        "Department",
                        "Join"+Environment.NewLine+"Date",
                        "Travel"+Environment.NewLine+"Request",
                        "Leave",
                        "",
                        
                        //11-15
                        "Absent",
                        "Work"+Environment.NewLine+"Permit",
                        "Total Absent",
                        "Salary",
                        "Fix Allowance",

                        //16-20
                        "Total",
                        "Percentage",
                        "Working"+Environment.NewLine+"Duration",
                        "Index Of Working"+Environment.NewLine+"Duration",
                        "Working"+Environment.NewLine+"Contribute",
                        
                        //21-24
                        "Received",
                        "Deduction",
                        "Total",
                        "Payrun Code",
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 20, 100, 200, 150, 
                        
                        //6-10
                        150, 100, 100, 150, 20,  
                        
                        //11-15
                        80, 100, 100, 100, 100,
                        
                        //16-20
                        100, 100, 100, 100, 100, 
                        
                        //21-24
                        100, 100, 100, 100
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 2, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 3, 5 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3,5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueYr, LueMth, LueDeptCode, TxtPercentage, TxtTotalAmt, MeeRemark,
                    }, true);
                    BtnProcess.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueYr, LueMth, TxtPercentage, LueDeptCode, MeeRemark, 
                    }, false);
                    BtnProcess.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueYr, LueMth, LueDeptCode, MeeRemark, 
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtPercentage, TxtTotalAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23});
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            if (BtnSave.Enabled)
            {
                ClearGrd();
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpIncentiveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);

                var DocDt = Sm.GetDte(DteDocDt).Substring(0, 8);
                Sm.SetLue(LueYr, Sm.Left(DocDt, 4));
                Sm.SetLue(LueMth, DocDt.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnExcelClick(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (Sm.GetGrdDec(Grd1, e.RowIndex, 9) == 0m)
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                    else
                        ShowLeave(Sm.GetGrdStr(Grd1, e.RowIndex, 2));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 10)
            {
                if (Sm.GetGrdDec(Grd1, e.RowIndex, 9) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowLeave(Sm.GetGrdStr(Grd1, e.RowIndex, 2));
            }
        }


        private void ShowLeave(string EmpCode)
        {
            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

           
            SQL.AppendLine("    select A.EmpCode, A.leaveCode  ");
            SQL.AppendLine("    Count(B.DocNo) As LeaveAmt  ");
            SQL.AppendLine("    from tblEmpLeavehdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.Docno = B.Docno ");
            SQL.AppendLine("    Where A.CancelInd = 'N' And A.Status = 'A'  ");
            SQL.AppendLine("    And FIND_IN_SET(A.LeaveCode, (Select Parvalue from Tblparameter Where parCode = 'LeaveIncentives')) ");
            SQL.AppendLine("    And Left(B.LeaveDT, 4)= @Yr And  Substring(B.LeaveDT, 5, 2)=@Mth And A.EmpCode = @EmpCode ");
            SQL.AppendLine("    Group By A.EmpCode, A.leaveCode ");
           

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "LeaveCode", "LeaveAmt" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append(Sm.DrStr(dr, c[1]));
                            Msg.Append(" : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length > 0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpIncentive", "TblEmpIncentiveHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpIncentiveHdr(DocNo));
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveEmpIncentiveDtl(DocNo, Row));
            }
            
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsGrdEmpty()||
                IsPeriodNotValid()
                )
                return true;
            return false;
        }

    
        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsPeriodNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpIncentiveHdr ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Year : " + Sm.GetLue(LueYr) + Environment.NewLine +
                    "Month : " + Sm.GetLue(LueMth) + Environment.NewLine +
                    "Department : " + LueDeptCode.GetColumnValue("Col2") + Environment.NewLine + 
                    "This document already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpIncentiveHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpIncentiveHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, DeptCode, Yr, Mth, Percentage, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', @DeptCode, @Yr, @Mth, @Percentage, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<Decimal>(ref cm, "@Percentage", Decimal.Parse(TxtPercentage.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpIncentiveDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpIncentiveDtl(DocNo, DNo, EmpCode, Travel, `Leave`, Absent, WorkPermit, Salary, FixAllowance, Percentage, WorkDuration, IndexWorkDuration, TotalAmt, PayrunCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @Travel, @Leave, @Absent, @WorkPermit, @Salary, @FixAllowance, @Percentage, @WorkDuration, @IndexWorkDuration, @TotalAmt, @PayrunCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Travel", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Leave", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Absent", Sm.GetGrdDec(Grd1, Row, 11)); 
            Sm.CmParam<Decimal>(ref cm, "@WorkPermit", Sm.GetGrdDec(Grd1, Row, 12)); 
            Sm.CmParam<Decimal>(ref cm, "@Salary", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@FixAllowance", Sm.GetGrdDec(Grd1, Row, 15)); 
            Sm.CmParam<Decimal>(ref cm, "@Percentage", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@WorkDuration", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@IndexWorkDuration", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@TotalAmt", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@PayrunCode", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateEmpIncentiveHdr(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsEmpIncentiveNotCancelled() ||
                IsEmpIncentiveCancelledAlready();
        }

        private bool IsEmpIncentiveNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsEmpIncentiveCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpIncentiveHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='Y';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }

            return false;
        }


        private MySqlCommand UpdateEmpIncentiveHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpIncentiveHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpIncentiveHdr(DocNo);
                ShowEmpIncentiveDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpIncentiveHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, Yr, Mth, DeptCode, Percentage, Remark " +
                    "From TblEmpIncentiveHdr Where DocNo=@DocNo; ",
                    new string[] { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "Yr", "Mth", "DeptCode",
 
                        //6
                        "Percentage", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[5]));
                        TxtPercentage.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowEmpIncentiveDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();


            SQL.AppendLine("Select A.Dno, B.EmpCodeOld, B.EmpName, B.JoinDt, D.PosName, ");
            SQL.AppendLine("C.Deptname, B.JoinDt, A.Travel,  A.Leave, ");
            SQL.AppendLine("A.Absent, A.WorkPermit, A.salary, A.FixAllowance, A.Percentage, A.payrunCode, ");
            SQL.AppendLine("A.WorkDuration, A.IndexWorkDuration, A.TotalAmt ");
            SQL.AppendLine("From TblEmpIncentiveDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode" );
            SQL.AppendLine("Left Join tblPosition D On B.PosCode = D.PosCode" );
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    
                    //1-5
                    "EmpCodeOld", "EmpName", "JoinDt", "PosName", "Deptname", 
                    
                    //6-10
                    "JoinDt", "Travel", "Leave", "Absent", "WorkPermit",  

                    //11-15
                    "salary", "FixAllowance", "Percentage", "payrunCode", "WorkDuration", 
                    
                    //16
                    "IndexWorkDuration", "TotalAmt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Grd.Cells[Row, 13].Value = Sm.DrDec(dr, 8) + Sm.DrDec(dr, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Grd.Cells[Row, 16].Value = Sm.DrDec(dr, 11) + Sm.DrDec(dr, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 14);
                    ComputeData(Sm.DrDec(dr, 15), Sm.DrDec(dr, 16), Row, Grd1, Sm.DrDec(dr, 13), (Sm.DrDec(dr, 8) + Sm.DrDec(dr, 9)));
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            Sm.FocusGrd(Grd1, 0, 1);
            ComputeToHeader();
        }

        
        #endregion

        #region Additional Method

        private void GetParameter()
        {
            
        }

        private void ShowEmpInventice()
        {
            try
            {
                int SeqNo = 0;
                
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.EmpCode, B.EmpCodeOld, B.EmpName, B.JoinDt, D.PosName, ");
                SQL.AppendLine("C.Deptname, ifnull(F.TravelDay, 0) as Dinas,  ifnull(E.leaveAmt, 0) As LeaveAmt, ");
                SQL.AppendLine("(G.parvalue-A.WorkingDay) Absent, ((G.parvalue-A.WorkingDay)*H.Parvalue) WorkingHr, ");
                SQL.AppendLine("((G.parvalue-A.WorkingDay)+ifnull(E.leaveAmt, 0)) As totalabsent, A.salary, A.FixAllowance,  ");
                SQL.AppendLine("(A.salary+ A.FixAllowance) As Total1, ((@Percentage/100)*(A.salary+ A.FixAllowance)) As percentage, ");
                SQL.AppendLine("PERIOD_DIFF(left(CurrentDateTime(), 6), Left(B.JoinDt, 6)) MK, ");
                SQL.AppendLine("if(PERIOD_DIFF(left(CurrentDateTime(), 6), Left(B.JoinDt, 6))>=4, 1, 0.5) As IMK, A.payrunCode ");
                SQL.AppendLine("From tblpayrollprocess1 A ");
                SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode ");
                SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
                SQL.AppendLine("left Join ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    select A.EmpCode, Left(B.LeaveDT, 4) As Yr, Substring(B.LeaveDT, 5, 2) As Mth,  ");
	            SQL.AppendLine("    Count(B.DocNo) As LeaveAmt  ");
	            SQL.AppendLine("    from tblEmpLeavehdr A ");
	            SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.Docno = B.Docno ");
	            SQL.AppendLine("    Where A.CancelInd = 'N' And A.Status = 'A'  ");
                SQL.AppendLine("    And FIND_IN_SET(A.LeaveCode, (Select Parvalue from Tblparameter Where parCode = 'LeaveIncentives')) ");
	            SQL.AppendLine("    Group By A.EmpCode,  Left(B.LeaveDT, 4), Substring(B.LeaveDT, 5, 2) ");
                SQL.AppendLine(")E On A.EmpCode = E.EmpCode  And concat(E.Yr, E.Mth) = Left(A.PayrunCode, 6) ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select B.EmpCode, Left(A.StartDT, 4) As Yr, Substring(A.StartDt, 5, 2) As Mth, DateDiff(EndDt, StartDt) TravelDay "); 
	            SQL.AppendLine("    From TbltravelrequestHdr A ");
	            SQL.AppendLine("    Inner JOin TblTravelRequestDtl B On A.DocNo = b.DocNo ");
	            SQL.AppendLine("    Where Status = 'A' And CancelInd = 'N' ");
	            SQL.AppendLine("    Group By EmpCode  ");
                SQL.AppendLine(")F On A.EmpCode = F.EmpCode And concat(F.Yr, F.Mth) = Left(A.PayrunCode, 6) ");
                SQL.AppendLine("Left Join Tblparameter G On 0=0 And G.ParCode = 'NoWorkDayPerMth' ");
                SQL.AppendLine("Left Join Tblparameter H On 0=0 And H.ParCode = 'WorkingHr' ");
                SQL.AppendLine("where B.DeptCode = @DeptCode And Left(A.Payruncode, 6) = @YrMth ");
                

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@YrMth", String.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
                Sm.CmParam<Decimal>(ref cm, "@Percentage", Decimal.Parse(TxtPercentage.Text));

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",  

                    //1-5
                    "EmpCodeOld", "EmpName", "Posname", "DeptName",  "JoinDt", 
                    
                    //6-10
                    "Dinas", "LeaveAmt", "Absent", "WorkingHr", "totalabsent",  
                    
                    //11-15
                    "salary", "FixAllowance", "Total1", "percentage", "MK", 
                    //16
                    "IMK",  "PayrunCode"
                  
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    SeqNo += 1;
                    Grd.Cells[Row, 0].Value = SeqNo;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);//MK
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);//IMK
                    ComputeData(Sm.DrDec(dr, 15), Sm.DrDec(dr, 16), Row, Grd1, Sm.DrDec(dr, 14), Sm.DrDec(dr, 10));
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                }, true, false, true, false
                );
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
                Sm.FocusGrd(Grd1, 0, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeData(decimal MK, decimal IMK, int RowX, iGrid GrdX, decimal Percentage, decimal TotalAbsent)
        {
            if (MK != 0)
            {
                if (MK >= 4)
                    GrdX.Cells[RowX, 20].Value = 1m;//KK
                else
                    GrdX.Cells[RowX, 20].Value = (MK / 3 * 100) * 100;//KK
            }
            else
                GrdX.Cells[RowX, 20].Value = 0m;

            GrdX.Cells[RowX, 21].Value = Math.Round(Percentage * IMK * Sm.GetGrdDec(Grd1, RowX, 20));
            GrdX.Cells[RowX, 22].Value = Math.Round(Sm.GetGrdDec(Grd1, RowX, 21) / ((MK >= 3 ? 3 : MK) * 20 * TotalAbsent));
            if(!BtnInsert.Enabled)
                GrdX.Cells[RowX, 23].Value = Sm.GetGrdDec(Grd1, RowX, 21) - Sm.GetGrdDec(Grd1, RowX, 22);
        }

        private void ComputeToHeader()
        {
            decimal headAmt = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    headAmt += Sm.GetGrdDec(Grd1, Row, 23); 
                }
            }
            TxtTotalAmt.EditValue = Sm.FormatNum(headAmt, 0); 
        }
        #endregion

        #endregion

        #region Event
      

        private void BtnProcess_Click_1(object sender, EventArgs e)
        {
            ClearData2();

            if (Sm.IsLueEmpty(LueDeptCode, "Department") || Sm.IsTxtEmpty(TxtPercentage, "Percentage", true)) 
                return;
            ShowEmpInventice();
            ComputeToHeader();
        }

        private void TxtPercentage_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtPercentage, 0);
            }
        }
        #endregion

       

    }
}
