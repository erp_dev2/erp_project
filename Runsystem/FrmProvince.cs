﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProvince : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmProvinceFind FrmFind;

        #endregion

        #region Constructor

        public FrmProvince(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);

            Sl.SetLueCntCode(ref LueCntCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProvCode, TxtProvName, LueCntCode
                    }, true);
                    TxtProvCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProvCode, TxtProvName, LueCntCode
                    }, false);
                    TxtProvCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtProvName, LueCntCode
                    }, false);
                    TxtProvName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtProvCode, TxtProvName, LueCntCode
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProvinceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProvCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtProvCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblProvince Where ProvCode=@ProvCode" };
                Sm.CmParam<String>(ref cm, "@ProvCode", TxtProvCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblProvince(ProvCode, ProvName, CntCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ProvCode, @ProvName, @CntCode, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ProvName=@ProvName, CntCode=@CntCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ProvCode", TxtProvCode.Text);
                Sm.CmParam<String>(ref cm, "@ProvName", TxtProvName.Text);
                Sm.CmParam<String>(ref cm, "@CntCode", Sm.GetLue(LueCntCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtProvCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ProvCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ProvCode", ProvCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select ProvCode, ProvName, CntCode From TblProvince Where ProvCode=@ProvCode",
                        new string[] 
                        {
                            "ProvCode", "ProvName", "CntCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtProvCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtProvName.EditValue = Sm.DrStr(dr, c[1]);
                            Sm.SetLue(LueCntCode, Sm.DrStr(dr, c[2]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtProvCode, "Province code", false) ||
                Sm.IsTxtEmpty(TxtProvName, "Province name", false) ||
                Sm.IsLueEmpty(LueCntCode, "Country") ||
                IsProvCodeExisted();
        }

        private bool IsProvCodeExisted()
        {
            if (!TxtProvCode.Properties.ReadOnly && Sm.IsDataExist("Select ProvCode From TblProvince Where ProvCode='" + TxtProvCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Province code ( " + TxtProvCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtProvCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProvCode);
        }

        private void TxtProvName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProvName);
        }

        private void LueCntCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCntCode, new Sm.RefreshLue1(Sl.SetLueCntCode));
        }

        #endregion

        #endregion
    }
}
