﻿#region Update
/*
    07/08/2019 [DITA] data tidak muncul pada list dropping request di menu dropping payment (bug alias table)
    22/11/2019 [WED/VIR] Dropping Request yg department belum bisa ditarik
    06/03/2023 [RDA/MNET] fix kolom untuk choose data yang dipilih indexnya masih salah
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDroppingPaymentDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDroppingPayment mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDroppingPaymentDlg(FrmDroppingPayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.PRJIDocNo, Null As DeptName, A.Mth, A.Yr, A.Amt, A.DeptCode, A.BankAcCode, A.BankAcCode2 ");
            SQL.AppendLine(", A.DocType, G.OptDesc AS DroppingRequestType "); //
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr F On A.PRJIDocNo=F.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner JOin TblSOContractRevisionHdr F1 On F.SOContractDocNo = F1.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr C On F1.SOCDocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr D On C.BOQDocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr E On D.LOPDocNo=E.DocNo ");
                SQL.AppendLine("And (E.SiteCode Is Null Or ( ");
                SQL.AppendLine("    E.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("LEFT JOIN tbloption G ON  A.DocType=G.OptCode AND G.OptCat='DroppingRequestDocType' "); //
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.ProcessInd = 'F' ");
            SQL.AppendLine("And A.PaidInd In ('O', 'P') ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.PRJIDocNo, B.DeptName, A.Mth, A.Yr, A.Amt, A.DeptCode, A.BankAcCode, A.BankAcCode2 ");
            SQL.AppendLine(", A.DocType, C.OptDesc AS DroppingRequestType "); //
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("LEFT JOIN tbloption C ON  A.DocType=C.OptCode AND C.OptCat='DroppingRequestDocType' "); //
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.Status = 'A' ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.ProcessInd = 'F' ");
            SQL.AppendLine("And A.PaidInd In ('O', 'P') ");

            SQL.AppendLine(")T ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "",
                    "Date",
                    "Dropping Request Type",
                    "Project Impl.#",

                    //6-10
                    "Department",
                    "Month",
                    "Year",
                    "Amount",
                    "DeptCode",

                    //11-12
                    "BankAcCode",
                    "BankAcCode2"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    140, 20, 80, 200, 170, 

                    //6-10
                    200, 80, 80, 120, 0, 

                    //11-12
                    0, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 9, 10, 11 }, !ChkHideInfoInGrd.Checked);
            if (!mFrmParent.mIsDroppingRequestUseType) Sm.GrdColInvisible(Grd1, new int[] { 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDroppingRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmDroppingRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPRJIDocNo.Text, "T.PRJIDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                if (mFrmParent.mIsDroppingRequestUseType) Filter += " And T.DocType='3' ";


                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DocDt Desc, T.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "DroppingRequestType", "PRJIDocNo", "DeptName", "Mth", 
                        
                        //6-10
                        "Yr", "Amt", "DeptCode", "BankAcCode", "BankAcCode2"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtDRQDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtPRJIDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);
                Sl.SetLueDeptCode2(ref mFrmParent.LueDeptCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10));
                Sm.SetLue(mFrmParent.LueDeptCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10));
                Sm.SetLue(mFrmParent.LueMth, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7));
                Sm.SetLue(mFrmParent.LueYr, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8));
                Sm.SetLue(mFrmParent.LueBankAcCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 11));
                Sm.SetLue(mFrmParent.LueBankAcCode2, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 12));

                this.Hide();
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtPRJIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPRJIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Impl.#");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion

    }
}