﻿#region Update
// 12/11/2017 [HAR] tambah info parent dan level untuk organisasi structure
// 15/12/2017 [HAR] bug filter
// 07/11/2019 [HAR/SIER] tambah inputam PLT indicator
// 29/03/2023 [MAU/PHT] menambahkan kolom Cash Adv. PIC berdasarkan parameter IsVRCashAdvanceUseEmployeePIC
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPositionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPosition _frmParent;

        #endregion

        #region Constructor

        public FrmPositionFind(FrmPosition FrmParent)
        {
            InitializeComponent();
            _frmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = _frmParent.Text;
                Sm.ButtonVisible(_frmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Position"+Environment.NewLine+"Code", 
                        "Position"+Environment.NewLine+"Name",
                        "Level",
                        "Parent",
                        "Min."+Environment.NewLine+"Grade Level",

                        //6-10
                        "Max."+Environment.NewLine+"Grade Level",
                        "Authority"+Environment.NewLine+"Request",
                        "Acting"+Environment.NewLine+"Official(PLT)",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        
                        //11 -15
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Cash Adv. PIC"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 100, 180,  200,  
                        
                        //6-10
                        200, 150, 150, 100, 100, 
                        
                        //11-15
                        100, 100, 100, 100, 180
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 7, 8, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 3 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] {  9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
            if (!_frmParent.mIsVRCashAdvanceUseEmployeePIC) Sm.GrdColInvisible(Grd1, new int[] { 15 }, false);
            Grd1.Cols[15].Move(7);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14  }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                SQL.AppendLine("Select A.PosCode, A.PosName, A.Level, D.PosName Parent, A.RequestItemInd, B.GrdLvlName As GrdLvlNameMinDesc, C.GrdLvlName As GrdLvlNameMaxDesc, ");
                if (_frmParent.mIsVRCashAdvanceUseEmployeePIC)
                    SQL.AppendLine(" A.CashAdvPIC,");
                else
                    SQL.AppendLine(" Null As CashAdvPIC,");
                SQL.AppendLine("A.ActingOfficialInd, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblPosition A ");
                SQL.AppendLine("Left Join TblGradeLevelHdr B On A.GrdLvlCodeMin=B.GrdLvlCode ");
                SQL.AppendLine("Left Join TblGradeLevelHdr C On A.GrdLvlCodeMax=C.GrdLvlCode ");
                SQL.AppendLine("Left Join TblPosition D On A.Parent=D.PosCode ");



                Sm.FilterStr(ref Filter, ref cm, TxtPosName.Text, new string[] { "A.PosCode", "A.PosName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,SQL.ToString() + Filter + " Order By A.PosName",
                        new string[]
                        {
                            //0
                            "PosCode", 
                                
                            //1-5
                            "PosName", "Level", "Parent", "GrdLvlNameMinDesc", "GrdLvlNameMaxDesc", 
                            
                            //6-10
                            "RequestItemInd", "ActingOfficialInd", "CreateBy", "CreateDt", "LastUpBy", 
                            //11-12
                            "LastUpDt", "CashAdvPIC"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 15, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                _frmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Event

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPosName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkPosName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Position");
        }

        #endregion

        #endregion
    }
}
