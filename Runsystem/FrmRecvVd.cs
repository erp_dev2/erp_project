﻿#region Update

#region old
/*
 * 30/05/2017 [TKG] Berdasarkan parameter IsRemarkForJournalMandatory, Remark harus disi atau tidak.
    30/05/2017 [TKG] Update remark di journal 
    13/06/2017 [TKG] Journal otomatis untuk receiving item dengan approval.
    13/07/2017 [WED] Tambah entity di JournalDtl dan Remark di JournalHdr
    14/09/2017 [TKG] Saat pecah PO ditambah site (KSM)
    26/09/2017 [TKG] saat simpan stock, menggunakan parameter MainCurCode
    19/12/2017 [HAR] tambah cancel reason dan validasi jia beda site berdasarkan parameter IsRecvValidateBySite
    23/12/2017 [HAR] bug fixing document (subcategory KSM)
    05/02/2018 [ARI] TAMBAH PRINTOUT KIM
    13/03/2018 [TKG] tambah import indicator dari PO
    15/03/2018 [ARI] jika approval di cancel tdk bisa diprint
    19/03/2018 [TKG] konversi tidak jalan
    18/05/2018 [TKG] Berdasarkan parameter IsAutoGenerateBatchNoEditable, batch# yg digenerate otomatis bisadiedit atau tidak.
    30/05/2018 [HAR] bug saat print alamat vendor.
    20/08/2018 [WED] tambah print out untuk AWG
    02/09/2018 [TKG] Menampilkan nomor urut berdasarkan parameter
    18/09/2018 [TKG] tambah validasi quantity 2 dan 3 tidak boleh 0.
    01/10/2018 [TKG] tambah site
    01/10/2018 [TKG] untuk KIM menggunakan document date sebagai batch# kalau kosong
    04/10/2018 [TKG] bug saat cancel
    05/11/2018 [WED] tambah kolom Project Code
    12/11/2018 [TKG] Ubah proses journal berkaitan dengan approval berdasarkan warehouse
    14/11/2018 [DITA] Validasi save ServiceItemInd
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    25/02/2019 [TKG] tambah informasi kawasan berikat
    11/03/2019 [TKG] bug saat menghitung balance.
    17/03/2019 [TKG] tambah informasi kawasan berikat (non document indicator, packaging, packaging quantity
    15/04/2019 [TKG] tambah nomor pengajuan berikat (KBSubmissionNo)
    14/05/2019 [TKG] tambah validasi kalau masih ada outstanding PO revision.
    30/05/2019 [TKG] tambah customs document code 
    03/07/2019 [TKG] berdasarkan parameter RecvVdCustomsDocCodeManualInput, untuk kode dokumen pabean tertentu informasinya bisa diinput secara manual.
    24/09/2019 [TKG/IMS] Berdasarkan parameter IsProjectGroupEnabled, batch# diisi otomatis saat disimpan berdasarkan project code dan tanggal terima.
    14/10/2019 [WED/MAI] masuk ke stock movement, kolom CustomsDocCode
    23/10/2019 [DITA/MAI] tambah KBContractNo, KBSubmissionNo, KBRegistrationNo saat save
    04/11/2019 [DITA/IMS] tambah informasi Specifications & ItCodeInternal
    16/12/2019 [DITA+WED+VIN/IMS] Print out untuk IMS
    09/01/2020 [WED/MMM] tambah parameter IsRecvVdUseOptionBatchNoFormula, agar batchno nya menyesuaikan rule yg ada, tapi bersifat default
    22/01/2020 [WED/YK] tambah auto input DO, berdasarkan parameter MenuCodeForRecvVdAutoCreateDO
    30/01/2020 [TKG/IMS] Berdasarkan parameter IsRecvVdBatchNoUseProjectCode, batch# diisi kode proyek.
    01/02/2020 [TKG/IMS] tambah informasi dari expedition's received document.
    03/02/2020 [TKG/IMS] tambah informasi quantity dari expedition's received document.
    17/03/2020 [IBL/MMM] tambah kolom expired date
    18/03/2020 [IBL/MMM] tambah keterangan vendor name pada kolom batch#
    26/03/2020 [TKG/IMS] Receiving item from vendor bisa memproses data receiving expedition secara partial.
    30/03/2020 [TKG/YK] bug saat auto do akan mengupdate stock movement dan stock summary.
    30/04/2020 [VIN/SRN] printout item barcode 
    01/05/2020 [TKG/IMS] apabila tidak memilih ekspedisi maka user tidak bisa memilih PO
    20/05/2020 [IBL/IMS] Penyesuaian printout
    26/05/2020 [HAR/ALL] tambah button action berdasarkan parmeter :  mIsButtonUsageHistoryActived
    14/08/2020 [TKG/TWC] Merubah proses validasi cost category ada atau tidak ada dengan langsung mencek di database
    26/08/2020 [WED/MGI] tambah production Group, berdasarkan parameter IsUseProductionWorkGroup
    20/09/2020 [TKG/IMS] Berdasarkan parameter IsDocNoFormatUseFullYear, dokumen# menampilkan tahun secara lengkap (Misal : 2020).
    29/09/2020 [TKG/IMS] GenerateDocNo reset nomor urut per tahun
    02/10/2020 [TKG/IMS] receiving expedition ketika diproses di recv vd sisanya akan hangus kalau tidak diproses, dan harus diproses kembali di recv expedition
    27/10/2020 [TKG/IMS] tambah fasilitas untuk menampilkan total item dengan cara mengklik judul dari kolom quantitynya
    16/11/2020 [ICA/IMS] Menambah POQtyCancel diRecvVdIMSDtl dan mengubah POQty menjadi POQtyCancel di Printout
    16/11/2020 [WED/IMS] perubahan nilai di printout
    17/11/2020 [WED/IMS] perubahan nilai di printout (2)
    19/11/2020 [WED/IMS] bug saat show data expedition (part 1)
    19/11/2020 [WED/IMS] bug saat show data expedition (part 2)
    20/11/2020 [WED/IMS] bug saat show data expedition (part 3)
    20/11/2020 [IBL/ALL] Membuat journal saat cancel RecvVd Auto DO
    01/12/2020 [ICA/SIER] Tambah keterangan category pada SetLueVd berdasarkan parameter IsVendorComboShowCategory
    14/12/2020 [WED/IMS] Journal Detail disambungin ke SO Contract yg ada di MR nya, Journal Header disambungin ke Cost Center berdasarkan department MR nya
    18/12/2020 [WED/IMS] Recv Expedition yang sudah approved
    08/01/2021 [ICA/SRN] Validasi COA saat save dan cancel auto journal based on param mIsCheckCOAJournalNotExists
    14/01/2021 [ICA/KSM] Menambah ifnull VdCtName di SetLueVdCode
    21/01/2021 [WED/IMS] Tambah reject indicator berdasarkan parameter IsRecvExpeditionEnabled
    26/01/2021 [TKG/KSM] bug di ComputeQty()
    04/02/2021 [HAR/SRN] recv from vendor auto DO saat di validasi IsCOAJournalCancelNotValid
    11/02/2021 [VIN/PHT] tambah validasi hanya boleh narik item dengan itemcategory yg sama berdasarkan IsItemCategoryUseCOAAPAR + tambah di SaveJournalAutoDO (credit recvvd)
    22/02/2021 [VIN/SIER] Tambah inputan baru Contract - berdasarkan parameter IsPOUseContract
    24/02/2021 [WED/SIER] BUG salah nangkep goal task. hanya menampilkan informasi Contract nya saja di grid nya  
    26/02/2021 [TKG/IMS] Untuk auto DO, Berdasarkan parameter IsRecvVdAutoDOUseDefaultCostCenter, IsRecvVdAutoDOUseMRDept dan RecvVdAutoDOCostCenterDefault, mengisi secara otomatis cost center dan department
    03/03/2021 [WED/IMS] Lot diisi manual berdasarkan parameter IsRecvExpeditionEnabled
    10/03/2021 [VIN/KIM] Bug: save journal cancel
    11/03/2021 [TKG/IMS] menambah heat number dan attachment file
    18/03/2021 [VIN/IMS] bug: parameter belum dipanggil di GetParameter()
    25/03/2021 [ICA/SRN] ubah Validasi IsCOAJournalNotValid, tidak perlu cek CA yg ada di entity
    31/03/2021 [WED/IMS] print out perbaikan di "Diterima", ambil dari akumulasi recv qty
    31/03/2021 [WED/IMS] perbaikan logic ketika di reject
    04/04/2021 [WED/IMS] qty rejected tidak dihitung saat kalkulasi outstanding
    05/04/2021 [WED/IMS] kalau auto DO, abbreviation nya beda, berdasarkan parameter IsRecvVdAutoDOUseDifferentAbbreviation
    08/04/2021 [WED/IMS] bug print out belom nyantumin RejectedInd nya
    09/04/2021 [VIN/IMS]penyesuaian printout Auto DO
    12/04/2021 [VIN/IMS]penyesuaian source sign Printout auto DO services
    13/04/2021 [VIN/IMS]penyesuaian source entry sheet auto DO services
    15/04/2021 [WED/IMS] saat cancel auto DO, DO nya juga harusnya di cancel
    26/04/2021 [VIN/IMS] dokumen PO for service ditampilkan 
    04/05/2021 [TKG/IMS] merubah cara menyimpan batch# di TblDODeptDtl
    10/06/2021 [VIN/IMS] penyesuaian printout
    15/06/2021 [VIN/IMS] penyesuaian PO for Service
    17/06/2021 [WED/IMS] journal berdasarkan SO Contract, berdasarkan parameter IsRecvVdJournalBasedOnSOContract
    14/07/2021 [VIN/IMS] Bug save JournalBasedOnSOContract
    23/07/2021 [VIN/IMS] FEEDBACK Printout -> qty reject disesuaikan berdasarkan item & batch
    29/07/2021 [VIN/IMS] FEEDBACK Printout -> tambah rejected ind di printout 
    04/08/2021 [VIN/IMS] FEEDBACK Printout -> header PO -> bisa narik PO for Service 
    06/08/2021 [VIN/IMS] FEEDBACK Bisa menampilkan dokumen PO service  
    18/08/2021 [MYA/ALL] Validasi tidak bisa SAVE untuk transaksi yang terbentuk jurnal otomatis ketika terdapat setting COA otomatis yang masih kosong (baik dari Menu Master Data maupun System's Parameter) sehingga terhindar dari jurnal yang tidak seimbang.
    19/08/2021 [VIN/IMS] remark SetCostCategory, Cost category ambil dr dialog
    23/08/2021 [VIN/IMS] bug Printout
    30/09/2021 [MYA/PHT] Menambahkan File upload dokumen di menu Receiving Item From Vendor (3 Field) dan merubah judul fieldnya
    30/09/2021 [MYA/PHT] Membuat file upload dokumen menjadi mandatory untuk jenis file upload BA Pemeriksaan dan BA Serah Terima di menu Receiving Item from Vendor
    18/10/2021 [WED/IMS] yang disimpan ke journal hanya item yang tidak reject
    19/10/2021 [WED/IMS] save journal RecvVd bukan auto DO juga melihat Moving Average Price, untuk yg dengan parameter IsRecvVdJournalBasedOnSOContract
    29/10/2021 [ICA/IMS] save journal RecvVD tidak melihat MAP
    03/11/2021 [ICA/IMS] Bug saat SaveJournalBasedSOC
    05/11/2021 [DITA/IMS] masih ada bug saat save journal based on sc
    10/11/2021 [NJP/PHT] Journal Receiving Item From Vendor Auto DO dirubah dari menyimpan Cost Center Warehouse(Param = 1) di ganti Cost Center dari TblDoDeptHdr (Param = 2) dengan parameter mCostCenterJournalRecvVdSource
    06/11/2021 [NJP/RM] Menambahkan Parameter IsUseAdditionalLabel agar parameter LabelFileForRecvVd bisa berjalan tidah hanya di PHT
    15/12/2021 [ICA/IMS] Bug TotalQtyReceive di printout belum terkalkulasi dengan PO Cancel nya. 
    22/12/2021 [VIN/PHT] Bug save jurnal auto do , hapus warning
    06/01/2022 [ISD/PHT] nambah parameter IsFilterByDept untuk filter di Find
 */
#endregion

/*
    11/01/2022 [ISD/PHT] Kolom Cost Center terfilter berdasar group login dengan parameter IsFilterByCC
    21/01/2022 [TKG/PHT] merubah GetParameter() dan proses save
    27/01/2022 [RIS/PHT] Membuat Outstanding quantity dapat mengambil nilai dari tab revision dengan param mIsRecvVdUsePORevision
    18/02/2022 [ISD/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    10/03/2022 [TKG/PHT] ubah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    30/03/2022 [DITA/PHT] validasi closing journal, bagian get profit center sumber cc nya dibedain untuk yg biasa dan auto do
    04/04/2022 [RIS/PRODUCT] Merubah prosess journal menjadi perdetail 
    20/04/2022 [VIN/ALL] hapus validasi check acno1 entity->journal tidak dipakai
    11/05/2022 [RIS/TWC] Bug : mCostCenterJournalRecvVdSource param '2'
    02/06/2022 [ICA/PRODUCT] save stock dodept setelah approve
    04/08/2022 [DITA/PHT]  Sm.ExecCommands(cml) saat upload file dikeluarkan, dan dijadikan 1 ekesekusi dengan process lainnya
    23/08/2022 [DITA/SKI] tambah param IsJournalRecvVdAutoDOUseCOAEntity untuk magerin jurnal auto do supaya hanya ambil dari coa entity atau cost categor saja
    19/12/2022 [BRI/MNET] format upload di detail berdasarkan param RecvUploadFileFormat
    04/01/2023 [BRI/MNET] bug button download
    04/01/2023 [ICA/MNET] penyesuaian printout MNET
    06/01/2023 [MYA/MNET] Membuat validasi ketika Receiving Item From Vendor sudah di input di Rating for Vendor maka tidak bisa di cancel
    12/01/2023 [RDA/PHT] generate format docno journal baru
    18/01/2023 [RDA/IOK] benerin get parameter value defaultnya gak kebaca
    18/01/2023 [BRI/MNET] bug : upload file
    09/02/2023 [RDA/MNET] cost center untuk journal melihat cccode whs / lop berdasarkan docno DRQ yang exist RecvvdCostCenterJournalFormat
    24/02/2023 [RDA/MNET] penyesuaian debit ac no pada journal (berdasarkan parameter IsMaterialRequestForDroppingRequestEnabled = Y dan RecvvdJournalFormat = 2)
    27/02/2023 [RDA/MNET] fix if null untuk acno ketika project code kosong
    07/03/2023 [HAR/HEX] printout hex
    09/03/2023 [RDA/HEX] feedback printout hex + penambahan gambar ttd pihak pertama
    16/03/2023 [RDA/MNET] bug fix method untuk ubah debit ac no receiving
    05/04/2023 [BRI/PHT] bug duplicate journal
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;
using System.Net;
using System.Threading;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVd : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            compare1 = string.Empty,
            compare2 = string.Empty;
        private string 
            mDocType = "01",
            mDoctitle = string.Empty,
            mPODocTitle = string.Empty,
            mPODocAbbr = string.Empty,
            mIsPOSplitBasedOnTax = string.Empty,
            mEntCode = string.Empty,
            mMainCurCode = string.Empty,
            mRecvVdAutoBatchNoFormat = "1",
            mRecvVdCustomsDocCodeManualInput = string.Empty,
            mRecvVdAutoDOCostCenterDefault = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mMenuCodeForPRService = string.Empty,
            mLabelFileForRecvVd = string.Empty,
            mMandatoryFileForRecvVd = string.Empty,
            mCostCenterJournalRecvVdSource = "1",
            mFormatFTPClient = string.Empty,
            mRecvUploadFileFormat = string.Empty,
            mJournalDocNoFormat = string.Empty,
            mRecvvdCostCenterJournalFormat = "1",
            mRecvvdJournalFormat = string.Empty
            ;
        internal FrmRecvVdFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private int mPOWithTitleLength = 0, mPOWithoutTitleLength = 0, mGrd3 = 0; 
        private bool
            mIsFilterLocalDocNo=false,
            mIsRecvVdSplitOutstandingPO = false,
            mIsRemarkForApprovalMandatory = false,
            mIsRecvVdApprovalBasedOnWhs = false,
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mIsShowSeqNoInRecvVd = false,
            mIsBatchNoUseDocDtIfEmpty = false,
            mIsRecvVdLocalDocNoBasedOnMRLocalDocNo = false,
            mIsMaterialRequestMaxStockValidationEnabled = false,
            mIsProjectGroupEnabled = false,
            mIsMovingAvgEnabled = false,
            mIsUseProductionWorkGroup = false,
            mIsVendorComboShowCategory = false,
            mIsCheckCOAJournalNotExists = false,
            mIsRecvVdAutoDOUseDefaultCostCenter = false,
            mIsRecvVdAutoDOUseMRDept = false,
            mIsRecvVdAllowToUploadFile = false,
            mIsRecvVdAutoDOUseDifferentAbbreviation = false,
            mIsRecvVdJournalBasedOnSOContract = false,
            mIsUseAdditionalLabel = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsApprove = false,
            mIsAllCancel = false,
            mIsMaterialRequestForDroppingRequestEnabled = false
            ;
        internal bool
            mIsFilterBySite = false,
            mIsRecvVdNeedApproval = false,
            mIsAutoGenerateBatchNo = false,
            mIsAutoGenerateBatchNoEditable = false,
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsItGrpCodeShow = false,
            mIsShowForeignName = false,
            mIsComparedToDetailDate = false,
            mIsRecvVdValidateByEntity = false,
            mIsKawasanBerikatEnabled = false,
            mIsBOMShowSpecifications = false,
            mIsRecvVdUseOptionBatchNoFormula = false,
            mMenuCodeForRecvVdAutoCreateDO = false,
            mIsRecvVdBatchNoUseProjectCode = false,
            mIsRecvExpeditionEnabled = false,
            mIsButtonUsageHistoryActived = false,
            mIsItemCategoryUseCOAAPAR = false,
            mIsPOUseContract = false,
            mIsRecvVdHeatNumberEnabled = false,
            mIsFilterByDept = false,
            mIsFilterByCC = false,
            mIsFilterByItCt = false,
            mIsRecvVdUsePORevision = false,
            mIsJournalRecvVdAutoDOUseCOAEntity = false
            ;
        private List<LocalDocument> mlLocalDocument = null;
        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmRecvVd(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Receiving Item From Vendor";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mLabelFileForRecvVd.Length > 0 && (mDoctitle == "PHT" || mIsUseAdditionalLabel))
                {
                    LabelForRecvVd();
                }
                //splitContainer1.SplitterDistance = 200;
                Tp5.PageVisible = (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2");
                Tp4.PageVisible = (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "1");
                Tp3.PageVisible = mIsRecvExpeditionEnabled;
                Tp2.PageVisible = mIsKawasanBerikatEnabled;
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueCCCode(ref LueCCCode, string.Empty, mIsFilterByCC ? "Y" : "N");
                //Sl.SetLueCCCode(ref LueCCCode);
                Sl.SetLueDeptCode2(ref LueDeptCode, string.Empty);
                Sl.SetLueEntCode(ref LueEntCode, string.Empty);
                SetLueLot(ref LueLot);
                SetLueBin(ref LueBin);
                if (mIsFilterBySite) LblSiteCode.ForeColor = Color.Red;
                LueLot.Visible = false;
                LueBin.Visible = false;
                mlLocalDocument = new List<LocalDocument>();
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                if (mIsKawasanBerikatEnabled)
                {
                    TcRecvVd.SelectedTabPage = Tp2;
                    Sl.SetLueOption(ref LueCustomsDocCode, "CustomsDocCode");
                }

                TcRecvVd.SelectedTabPage = Tp1;
                if (!mMenuCodeForRecvVdAutoCreateDO)
                {
                    LblCCCode.Visible = 
                    LueCCCode.Visible = 
                    LblDeptCode.Visible = 
                    LueDeptCode.Visible = 
                    LblEntCode.Visible = 
                    LueEntCode.Visible = false;

                    mIsRecvVdAutoDOUseDefaultCostCenter = false;
                    mIsRecvVdAutoDOUseMRDept = false;
                }

                if (!mIsUseProductionWorkGroup)
                    LblProductionWorkGroup.Visible = 
                    LueProductionWorkGroup.Visible = false;
                else
                    Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 52;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                     //0
                    "DNo",

                    //1-5
                    "Cancel", 
                    "Old Cancel", 
                    "Cancel Reason",
                    "", 
                    "PO#", 

                    //6-10
                    "PO"+Environment.NewLine+"DNo",
                    "Import",
                    "Item's Code", 
                    "Item's Name", 
                    "Batch#",
                    
                    //11-15
                    "Source",
                    "Lot", 
                    "Bin", 
                    "Quantity"+Environment.NewLine+"(Purchase)", 
                    "UoM"+Environment.NewLine+"(Purchase)", 
                    
                    //16-20
                    "Quantity"+Environment.NewLine+"(Inventory)",
                    "UoM"+Environment.NewLine+"(Inventory)", 
                    "Quantity"+Environment.NewLine+"(Inventory)", 
                    "UoM"+Environment.NewLine+"(Inventory)", 
                    "Quantity"+Environment.NewLine+"(Inventory)", 
                    
                    //21-25
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "Remark",
                    "PO's Remark",
                    "ItScCode",
                    "Sub-Category",

                    //26-30
                    "Status",
                    "",
                    "Document#-Source",
                    "Local#",
                    "Foreign Name",

                    //31-35
                    "PO Date",
                    "No",
                    "ProjectCode",
                    "Item's Local Code",
                    "Specification",

                    //36-40
                    "Cost Category",
                    "Cost Category",
                    "LOP#",
                    "Inventory COA#",
                    "Expedition's Received#",

                    //41-45
                    "Expedition's Received DNo",
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Purchase)",
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Inventory)",
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Inventory)",
                    "Exp's Outstanding"+Environment.NewLine+"Quantity (Inventory)",

                    //46-50
                    "Expired",
                    "Rejected",
                    "PO Contract",
                    "Heat Number", 
                    "PO#",

                    //51
                    "SOContract#"
                },
                 new int[] 
                {
                    //0
                    20,

                    //1-5
                    50, 0, 120, 20, 130, 
                    
                    //6-10
                    0, 60, 100, 200, 200,  
                    
                    //11-15
                    150, 130, 130, 80, 80, 
                    
                    //16-20
                    100, 100, 100, 100, 100, 
        
                    //21-25
                    100, 200, 250, 100, 150, 

                    //26-30
                    100, 20, 0, 140, 230, 

                    //31-35
                    80, 50, 0, 180, 300,

                    //36-40
                    0, 200, 200, 200, 150,

                    //41-45
                    0, 130, 130, 130, 130,

                    //46-50
                    110, 60, 120, 200, 165,

                    //51
                    0
                }
            );

            if (mIsPOUseContract)
            {
                Grd1.Cols[48].Move(7);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 48 });

            if (mIsAutoGenerateBatchNo)
            {
                if (!mIsAutoGenerateBatchNoEditable)
                    Sm.GrdColReadOnly(Grd1, new int[] { 10 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8, 9, 11, 15, 17, 19, 21, 23, 24, 25, 26, 28, 29, 30, 31, 34, 35, 36, 37, 38, 39 });
            }
            else
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 5, 6, 7, 8, 9, 11, 15, 17, 19, 21, 23, 24, 25, 26, 28, 29, 30, 31, 34, 35, 36, 37, 38, 39 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18, 20, 42, 43, 44, 45 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 31, 46 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 2, 7, 47 });
            Sm.GrdColButton(Grd1, new int[] { 4, 27 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 32, 48, 51 });
            Sm.GrdColInvisible(Grd1, new int[] { 33, 34, 51 });
            if (!mMenuCodeForRecvVdAutoCreateDO)
                Sm.GrdColInvisible(Grd1, new int[] { 36, 37, 38, 39 });
            Grd1.Cols[31].Move(7);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 35 });
            Sm.GrdColInvisible(Grd1, new int[] { 32 }, mIsShowSeqNoInRecvVd);
            if (mIsShowForeignName)
            {
                Grd1.Cols[30].Move(10);
                if (IsProcFormat == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 25, 26, 27, 28, 31 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 26, 27, 28, 31 }, false);
                    Grd1.Cols[25].Move(11);
                }
            }
            else
            {
                if (IsProcFormat == "0")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 25, 26, 27, 28, 30, 31 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6, 8, 11, 12, 13, 18, 19, 20, 21, 24, 26, 27, 28, 30, 31 }, false);
                    Grd1.Cols[25].Move(10);
                }
            }

            if (mIsRecvVdNeedApproval)
            {
                Grd1.Cols[26].Move(4);
                Grd1.Cols[27].Move(5);
            }

            Grd1.Cols[29].Move(10);
            Grd1.Cols[32].Move(0);
            if (!mIsAutoGeneratePurchaseLocalDocNo) Sm.GrdColInvisible(Grd1, new int[] { 29 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 32 });

            #region RecvExpedition

            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 40, 41, 42, 43, 44, 45 });
            if (!mIsRecvExpeditionEnabled)
                Sm.GrdColInvisible(Grd1, new int[] { 40, 42, 43, 44, 45, 47 });
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 44, 45 });
                Grd1.Cols[40].Move(8);
                Grd1.Cols[42].Move(20);
                Grd1.Cols[43].Move(23);
                Grd1.Cols[44].Move(26);
                Grd1.Cols[47].Move(29);
                Grd1.Cols[45].Move(29);
            }

            #endregion

            if (mIsRecvVdHeatNumberEnabled)
                Grd1.Cols[49].Move(18);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 49 }, false);

            if (mMenuCodeForRecvVdAutoCreateDO && mDoctitle =="IMS")
            {
                Grd1.Cols[50].Move(5);
                Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 50 }, false);


            ShowInventoryUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 13;
            Grd2.FrozenArea.ColCount = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                         //0
                        "PO#", 
                        
                        //1-5
                        "PO"+Environment.NewLine+"DNo",
                        "Item's Code", 
                        "Item's Name", 
                        "UoM", 
                        "Outstanding"+Environment.NewLine+"Quantity", 
                        
                        //6-10
                        "Received"+Environment.NewLine+"Quantity",
                        "Balance",
                        "PO# (Source)",
                        "Foreign Name",
                        "Item Code Internal",

                        //11-12
                        "Specification",
                        "PO#",
                    },
                     new int[] 
                    {
                        //0
                        150,

                        //1-5
                        0, 100, 200, 80, 100, 
                        
                        //6-10
                        100, 100, 150, 230, 180,

                        //11-12
                        300, 165
                    }
                );

            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd2, new int[] { 5, 6, 7 }, 0);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 11 });
            if (mIsShowForeignName)
            {
                Grd2.Cols[9].Move(4);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 10 }, false);
            }
            else
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2, 9, 10 }, false);

            if (mMenuCodeForRecvVdAutoCreateDO && mDoctitle == "IMS")
            {
                Grd2.Cols[12].Move(0);
                Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
            }
            else
                Sm.GrdColInvisible(Grd2, new int[] { 12 }, false);

            #endregion

            #region Grd3

            Grd3.Cols.Count = 8;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "DNo",
                    
                        //1-5
                        "U",
                        "File Name",
                        "D",
                        "Upload By",
                        "Date",
                    
                        //6-7
                        "Time",
                        "File Name2"
                    },
                    new int[]
                    {
                        //0
                        0,
                        //1-5
                        20, 250, 20, 150, 80,
                        //6-7
                        80, 250
                    }
                );

            Sm.GrdColInvisible(Grd3, new int[] { 0, 7 }, false);
            Sm.GrdColButton(Grd3, new int[] { 1 }, "1");
            Sm.GrdColButton(Grd3, new int[] { 3 }, "2");
            Sm.GrdFormatDate(Grd3, new int[] { 5 });
            Sm.GrdFormatTime(Grd3, new int[] { 6 });
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 2, 4, 5, 6, 7 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 11, 12, 13, 31, 34 }, !ChkHideInfoInGrd.Checked);
            if (mIsRecvVdNeedApproval)
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27, 31 }, !ChkHideInfoInGrd.Checked);
            if (!mIsAutoGeneratePurchaseLocalDocNo)
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 10 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, true);
                if (mIsRecvExpeditionEnabled) Sm.GrdColInvisible(Grd1, new int[] { 44 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21 }, true);
                if (mIsRecvExpeditionEnabled) Sm.GrdColInvisible(Grd1, new int[] { 44, 45 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode, LueVdCode, 
                        TxtVdDONo, LueSiteCode, MeeRemark, ChkKBNonDocInd, LueCustomsDocCode,
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty,
                        LueCCCode, LueDeptCode, LueEntCode, LueProductionWorkGroup
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 30, 31, 33, 34, 35, 36, 37, 38, 39, 46, 47, 49, 50 });
                    //if (mIsRecvExpeditionEnabled) Sm.GrdColInvisible(Grd1, new int[] { 42, 43, 44, 45 }, false);
                    Sm.GrdColReadOnly(true, false, Grd3, new int[] { 1 });
                    BtnKBContractNo.Enabled = false;
                    BtnRecvExpeditionDocNo.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    BtnFile4.Enabled = false;
                    BtnDownload4.Enabled = false;
                    BtnFile5.Enabled = false;
                    BtnDownload5.Enabled = false;
                    BtnFile6.Enabled = false;
                    BtnDownload6.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVdAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVdAllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVdAllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVdAllowToUploadFile)
                        BtnDownload4.Enabled = true;
                    ChkFile4.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVdAllowToUploadFile)
                        BtnDownload5.Enabled = true;
                    ChkFile5.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsRecvVdAllowToUploadFile)
                        BtnDownload6.Enabled = true;
                    ChkFile6.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueVdCode, TxtVdDONo, LueSiteCode, 
                        MeeRemark, ChkKBNonDocInd, LueCustomsDocCode, LueCCCode, LueDeptCode, 
                        LueEntCode, LueProductionWorkGroup
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                    if (mIsAutoGenerateBatchNo)
                    {
                        if (mIsAutoGenerateBatchNoEditable)
                            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 10 });
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 12, 13, 14, 16, 18, 20, 22, 46, 47, 49 });
                    }
                    else
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 10, 12, 13, 14, 16, 18, 20, 22, 46, 47, 49 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
                    Sm.GrdColReadOnly(false, false, Grd3, new int[] { 1 });
                    if (mIsRecvExpeditionEnabled)
                    {
                        BtnRecvExpeditionDocNo.Enabled = true;
                        //if (mNumberOfInventoryUomCode == 2) Sm.GrdColInvisible(Grd1, new int[] { 44 }, true);
                        //if (mNumberOfInventoryUomCode == 3) Sm.GrdColInvisible(Grd1, new int[] { 44, 45 }, true);
                    }
                    if (mIsRecvVdAutoDOUseDefaultCostCenter) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCCCode }, true);
                    if (mIsRecvVdAutoDOUseMRDept) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueDeptCode }, true);
                    if (mIsRecvVdAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                        BtnFile4.Enabled = true;
                        BtnDownload4.Enabled = true;
                        BtnFile5.Enabled = true;
                        BtnDownload5.Enabled = true;
                        BtnFile6.Enabled = true;
                        BtnDownload6.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    ChkFile4.Enabled = true;
                    ChkFile5.Enabled = true;
                    ChkFile6.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 3 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    Sm.GrdColReadOnly(false, false, Grd3, new int[] { 1 });
                    mIsApprove = false;
                    mIsAllCancel = true;
                    mGrd3 = 0;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (!Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            mIsAllCancel = false;
                        }
                        if (Sm.GetGrdStr(Grd1, Row, 26) == "Approved" && !Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            mIsApprove = true;
                        }
                    }
                    mGrd3 = Grd3.Rows.Count - 1;
                    if (mIsApprove) Sm.GrdColReadOnly(true, false, Grd3, new int[] { 1 });
                    if (mIsAllCancel) Sm.GrdColReadOnly(true, false, Grd3, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            compare1 = string.Empty;
            compare2 = string.Empty;
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode, LueVdCode, 
                TxtVdDONo, LueSiteCode, MeeRemark, TxtKBContractNo, DteKBContractDt,
                TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, DteKBRegistrationDt, TxtKBSubmissionNo,
                TxtKBPackaging, LueCustomsDocCode, LueCCCode, LueDeptCode, LueEntCode,
                TxtRecvExpeditionDocNo, LueProductionWorkGroup, TxtFile, TxtFile2, TxtFile3,
                TxtFile4, TxtFile5, TxtFile6
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtKBPackagingQty }, 0);
            ChkKBNonDocInd.Checked = false;
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
            ChkFile4.Checked = false;
            PbUpload4.Value = 0;
            ChkFile5.Checked = false;
            PbUpload5.Value = 0;
            ChkFile6.Checked = false;
            PbUpload6.Value = 0;
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 16, 18, 20, 42, 43, 44, 45 });
            SetSeqNo();
            Sm.FocusGrd(Grd1, 0, 0);

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 5, 6, 7 });
            Sm.FocusGrd(Grd2, 0, 0);

            Sm.ClearGrd(Grd3, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvVdFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
            if (mIsButtonUsageHistoryActived)
                Sm.InsertButtonUsageHistory(Gv.VersionNo, mMenuCode, this.Text, "FIND");
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetLueVdCode(ref LueVdCode, "");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                if (mIsRecvVdAutoDOUseDefaultCostCenter && mRecvVdAutoDOCostCenterDefault.Length > 0)
                    Sm.SetLue(LueCCCode, mRecvVdAutoDOCostCenterDefault);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            if (mIsButtonUsageHistoryActived)
                Sm.InsertButtonUsageHistory(Gv.VersionNo, mMenuCode, this.Text, "INSERT");
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            if (mIsButtonUsageHistoryActived)
                Sm.InsertButtonUsageHistory(Gv.VersionNo, mMenuCode, this.Text, "EDIT");
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue(
                            "Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode=B.CCCode  " +
                            "Inner Join TblProfitCenter C on B.ProfitCenterCode=C.ProfitCenterCode " +
                            "Where A.WhsCode=@Param limit 1;",
                            Sm.GetLue(LueWhsCode));

                //mEntCode = Sm.GetLue(LueCCCode);


                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            if (mIsButtonUsageHistoryActived)
                Sm.InsertButtonUsageHistory(Gv.VersionNo, mMenuCode, this.Text, "SAVE");
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
            if (mIsButtonUsageHistoryActived)
                Sm.InsertButtonUsageHistory(Gv.VersionNo, mMenuCode, this.Text, "CANCEL");
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string par = Sm.GetParameter("NumberOfInventoryUomCode");
            string doct = Sm.GetParameter("doctitle");

            if (par == "1")
            {
                ParPrint(1);
            }
            else if (par == "2")
            {
                ParPrint(2);
            }
            else
                ParPrint(3);

            if (mIsButtonUsageHistoryActived)
                Sm.InsertButtonUsageHistory(Gv.VersionNo, mMenuCode, this.Text, "PRINT");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueVdCode, "Vendor") &&
                (!mIsRecvExpeditionEnabled ||
                (mIsRecvExpeditionEnabled && !Sm.IsTxtEmpty(TxtRecvExpeditionDocNo, "Expedition's received#", false)))
                )
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                {
                    if (!mIsFilterBySite || (mIsFilterBySite && !Sm.IsLueEmpty(LueSiteCode, "Site")))
                    {
                        if (mMenuCodeForRecvVdAutoCreateDO)
                        {
                            if (!Sm.IsLueEmpty(LueCCCode, "Cost Center"))
                                Sm.FormShowDialog(new FrmRecvVdDlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LueSiteCode), TxtRecvExpeditionDocNo.Text));
                        }
                        else
                            Sm.FormShowDialog(new FrmRecvVdDlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LueSiteCode), TxtRecvExpeditionDocNo.Text));
                    }
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 7));
            }

            if (e.ColIndex == 46) Sm.DteRequestEdit(Grd1, DteExpiredDt, ref fCell, ref fAccept, e);

            if (e.ColIndex == 27)
            {
                e.DoDefault = false;
                ShowApprovalInfo(e.RowIndex);
            }

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length != 0 &&
                ((e.ColIndex == 1 && Sm.GetGrdBool(Grd1, e.RowIndex, 2)) || e.ColIndex != 1))
                    e.DoDefault = false;

                if(TxtDocNo.Text.Length != 0 && e.ColIndex == 3)
                    e.DoDefault = true;


                if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1)
                    e.DoDefault = false;

                if (TxtDocNo.Text.Length == 0 && !IsPOEmpty(e) && Sm.IsGrdColSelected(new int[] { 8, 12, 13, 14, 16, 18, 20, 22, 49 }, e.ColIndex))
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 13 }, e.ColIndex))
            {
                LueRequestEdit(Grd1, LueBin, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //SetLueBin(ref LueBin);
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 12 }, e.ColIndex))
            {
                if (!mIsRecvExpeditionEnabled) LueRequestEdit(Grd1, LueLot, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                //SetLueLot(ref LueLot);
            }

            if (BtnSave.Enabled && TxtDocNo.Text.Length != 0)
            {
                string Check = Sm.GetValue("SELECT DocNo FROM TblRatingForVendorHdr WHERE PODocNo like '" + Sm.GetGrdStr(Grd1, e.RowIndex, 5) + "' AND CancelInd = 'N'");
                if (e.ColIndex == 1 && Check.Length > 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "This item can't be cancelled because the Purchase Order: " + Sm.GetGrdStr(Grd1, e.RowIndex, 5) + " from this receiving has been rated in Rating For Vendor document : " + Check + ". Please cancel Rating for Vendor document first") ;
                    e.DoDefault = false;
                }
            }

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete && TxtDocNo.Text.Length==0)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            string PODocNo = string.Empty, PODNo = string.Empty;

                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                            {
                                PODocNo = Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 4); 
                                PODNo =  Sm.GetGrdStr(Grd1, Grd1.SelectedRows[Index].Index, 5);

                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);

                                RemovePOInfo(PODocNo, PODNo);
                            }
                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();

                            SetSeqNo();
                        }
                    }
                }
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueVdCode, "Vendor") &&
                (!mIsRecvExpeditionEnabled || 
                (mIsRecvExpeditionEnabled && !Sm.IsTxtEmpty(TxtRecvExpeditionDocNo, "Expedition's received#", false)))
                )
            {
                if (!mIsFilterBySite || (mIsFilterBySite && !Sm.IsLueEmpty(LueSiteCode, "Site")))
                {
                    if (mMenuCodeForRecvVdAutoCreateDO)
                    {
                        if(!Sm.IsLueEmpty(LueCCCode, "Cost Center"))
                            Sm.FormShowDialog(new FrmRecvVdDlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LueSiteCode), TxtRecvExpeditionDocNo.Text));
                    }
                    else
                        Sm.FormShowDialog(new FrmRecvVdDlg(this, Sm.GetLue(LueVdCode), Sm.GetLue(LueSiteCode), TxtRecvExpeditionDocNo.Text));
                }
            }
            if (e.ColIndex == 27) ShowApprovalInfo(e.RowIndex);
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 14, 16, 18, 20, 42, 43, 44, 45 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 22, 49 }, e);

                if (e.ColIndex == 16)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 8, 16, 18, 20, 17, 19, 21);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 8, 16, 20, 18, 17, 21, 19);
                }

                if (e.ColIndex == 18)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 8, 18, 16, 20, 19, 17, 21);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 8, 18, 20, 16, 19, 21, 17);
                }

                if (e.ColIndex == 20)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 8, 20, 16, 18, 21, 17, 19);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 8, 20, 18, 16, 21, 19, 17);
                }

                if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                {
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 14);
                    //Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 8, 16, 18, 20, 17, 19, 21);
                    //Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 8, 16, 20, 18, 17, 21, 19);
                }

                if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                {
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 14);
                    //Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 8, 18, 16, 20, 19, 17, 21);
                    //Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 8, 18, 20, 16, 19, 21, 17);
                }

                if (e.ColIndex == 14 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                {
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 14);
                    //Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 8, 20, 16, 18, 21, 17, 19);
                    //Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 8, 20, 18, 16, 21, 19, 17);
                }

                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 16);

                if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 16);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 21)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 20, Grd1, e.RowIndex, 18);

                if (e.ColIndex==1 || e.ColIndex==14) ComputeQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 14, 16, 18, 20, 42, 43, 44, 45 }, e.ColIndex))
            {
                decimal Total = 0m;

                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, e.ColIndex).Length != 0)
                        Total += Sm.GetGrdDec(Grd1, r, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 2).Length > 0)
                {
                    DownloadFileDtl(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 1)
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd3.Cells[e.RowIndex, 2].Value = OD.FileName;
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 2).Length > 0)
                {
                    DownloadFileDtl(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd3_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedRow = 0;
            for (int Index = Grd3.SelectedRows.Count - 1; Index >= 0; Index--)
                SelectedRow = Grd3.SelectedRows[Index].Index;
            if (SelectedRow >= mGrd3)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdEnter(Grd3, e);
                Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (mIsRecvVdAutoDOUseMRDept) SetDeptDefault();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            if (mIsRecvVdJournalBasedOnSOContract) GetSOContractDocNo();

            string 
                LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text,
                POLocalDocNo = string.Empty,
                PORevision = string.Empty;

            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;
                if (IsLocalDocumentNotValid(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr
                    )) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "RecvVd",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }
                mlLocalDocument.Clear();
            }

            Cursor.Current = Cursors.WaitCursor;

            string SubCategory = Sm.GetGrdStr(Grd1, 0, 24);
            string DocNo = string.Empty; //GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "RecvVd", "TblRecvVdHdr", SubCategory);

            DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "RecvVd", "TblRecvVdHdr", SubCategory);

            if (mMenuCodeForRecvVdAutoCreateDO && mIsRecvVdAutoDOUseDifferentAbbreviation)
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvVdAutoDO", "TblRecvVdHdr");

            string DODept2DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept2", "TblDODeptHdr");

            var cml = new List<MySqlCommand>();

            if (mIsRecvVdSplitOutstandingPO)
            {
                if (mIsAutoGeneratePurchaseLocalDocNo)
                {
                    if (SeqNo.Length > 0)
                    {
                        PORevision = GetPORevision(
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr
                            );
                        SetLocalDocNo(
                            "PO",
                            ref POLocalDocNo,
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr,
                            ref PORevision
                        );
                    }
                }

                var lh = new List<SOPOHdr>();
                var lda = new List<SOPODtla>();
                
                GenerateSOPOHdr(ref lh);
                GenerateSOPODtla(ref lda);
                UpdateBalanceSOPODtla(ref lda);
                var ldb = GenerateSOPODtlb(lh, ref lda);

                var lh2 = new List<SOPOHdr>();
                string PODocNoTemp = string.Empty;
                ldb.ForEach(d =>
                {
                    if (!Sm.CompareStr(PODocNoTemp, d.PODocNo))
                    {
                        foreach (var i in lh.Where(Index => Index.PODocNo2 == d.PODocNo2))
                        {
                            cml.Add(SavePOHdr(
                                d.PODocNo, 
                                d.PODocNo2, 
                                i.PODocNoSource, 
                                i.RevNo,
                                POLocalDocNo,
                                SeqNo,
                                DeptCode,
                                ItSCCode,
                                Mth,
                                Yr,
                                PORevision
                                ));
                            break;
                        }
                    }

                    cml.Add(SavePODtl(
                        d.PODocNo, d.PODNo,
                        d.PODocNo2, d.PODNo2, 
                        d.Qty2
                        ));
    
                    PODocNoTemp = d.PODocNo;
                });

                foreach (var po in ldb.Select(PO => new { PO.PODocNo, PO.PODocNo2 }).Distinct())
                    cml.Add(SavePOHdr(po.PODocNo, po.PODocNo2));
            }
            
            cml.Add(SaveRecvVdHdr(
                DocNo,
                LocalDocNo,
                SeqNo,
                DeptCode,
                ItSCCode,
                Mth,
                Yr,
                Revision
                ));

            cml.Add(SaveRecvVdDtl(DocNo));

            if (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2") cml.Add(SaveRecvVdFile(DocNo));

            //for (int r = 0; r< Grd1.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd1, r, 5).Length > 0) cml.Add(SaveRecvVdDtl(DocNo, r));

            //auto DO
            var lDOHdr = new List<DODept2Hdr>();
            var lDODtl = new List<DODept2Dtl>();

            if (mMenuCodeForRecvVdAutoCreateDO)
            {
                ProcessDODtl(ref lDODtl, DocNo);
                if (lDODtl.Count > 0)
                {
                    bool mFlag = false;
                    for (int i = 0; i < lDODtl.Count; ++i)
                    {
                        if (lDODtl[i].LOPDocNo.Length > 0)
                        {
                            mFlag = true;
                            break;
                        }
                    }

                    if (mFlag)
                    {
                        lDODtl.OrderByDescending(x => x.LOPDocNo);
                        for (int i = 0; i < lDODtl.Count; ++i)
                        {
                            ProcessDODtl2(ref lDODtl);
                        }
                        ProcessDOHdr2(ref lDOHdr, ref lDODtl, DocNo);
                    }
                    else
                    {
                        string mDODeptDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept2", "TblDODeptHdr");
                        UpdateDODtl(ref lDODtl, mDODeptDocNo);
                        ProcessDOHdr(ref lDOHdr, mDODeptDocNo, DocNo);
                    }
                }

                cml.Add(SaveDODept2(ref lDOHdr, ref lDODtl));

                //for (int x = 0; x < lDOHdr.Count; ++x)
                //{
                //    cml.Add(SaveDODept2Hdr(ref lDOHdr, x));
                //    for (int i = 0; i < lDODtl.Count; ++i)
                //    {
                //        if(lDOHdr[x].DocNo == lDODtl[i].DocNo)
                //            cml.Add(SaveDODept2Dtl(ref lDODtl, i));
                //    }
                //}
            }

            cml.Add(SaveStock(DocNo));

            if (mMenuCodeForRecvVdAutoCreateDO)
            {
                for (int x = 0; x < lDOHdr.Count; ++x)
                    cml.Add(SaveStockDODept(lDOHdr[x].DocNo));   
            }

            cml.Add(UpdatePOProcessInd());

            if (mIsRecvExpeditionEnabled) cml.Add(SaveRecvExpedition());

            if (mIsAutoJournalActived)
            {
                if (mIsRecvVdJournalBasedOnSOContract)
                {
                    var lx = new List<JournalBasedSOC>();

                    PrepData(ref lx, DocNo, false);
                    if (lx.Count > 0)
                    {
                        GetJournalDNo(ref lx);
                        GetJournalDocNo(ref lx);

                        string mDODeptDocNo = string.Empty;
                        if (mMenuCodeForRecvVdAutoCreateDO)
                        {
                            foreach (var x in lDOHdr)
                            {
                                if (mDODeptDocNo.Length > 0) mDODeptDocNo += ",";
                                mDODeptDocNo += x.DocNo;
                            }
                        }

                        cml.Add(SaveJournalBasedSOC(ref lx, mDODeptDocNo, false));
                    }

                    lx.Clear();
                }
                else
                {
                    if (!mMenuCodeForRecvVdAutoCreateDO)
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++) 
                        {
                            string JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), (r+1));
                            cml.Add(SaveJournal(DocNo, Sm.Right("00" + (r + 1).ToString(), 3), JournalDocNo));
                        }
                            
                    else
                    {
                        string mDODeptDocNo = string.Empty;
                        foreach (var x in lDOHdr)
                        {
                            if (mDODeptDocNo.Length > 0) mDODeptDocNo += ",";
                            mDODeptDocNo += x.DocNo;
                        }
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            string JournalDocNo = string.Empty;
                            if (mJournalDocNoFormat == "1") //Default
                            {
                                JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), (r + 1));
                            }
                            else if (mJournalDocNoFormat == "2") //PHT
                            {
                                string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd", string.Empty, string.Empty, mJournalDocNoFormat);
                                string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                                JournalDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty));
                            }
                            cml.Add(SaveJournalAutoDO(DocNo, Sm.Right("00" + (r + 1).ToString(), 3), mDODeptDocNo, JournalDocNo));
                        }
                    }
                }
            }

           

            if (mIsRecvVdAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
                UploadFile(DocNo, toUpload);
                cml.Add(UpdateRecvVdFile(DocNo, toUpload.Name));
            }
            if (mIsRecvVdAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
            {
                FileInfo toUpload2 = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
                UploadFile2(DocNo,toUpload2);
                cml.Add(UpdateRecvVdFile2(DocNo, toUpload2.Name));
            }
            if (mIsRecvVdAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
            {
                FileInfo toUpload3 = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
                UploadFile3(DocNo, toUpload3);
                cml.Add(UpdateRecvVdFile3(DocNo, toUpload3.Name));
            }
            if (mIsRecvVdAllowToUploadFile && TxtFile4.Text.Length > 0 && TxtFile4.Text != "openFileDialog1")
            {
                FileInfo toUpload4 = new FileInfo(string.Format(@"{0}", TxtFile4.Text));
                UploadFile4(DocNo, toUpload4);
                cml.Add(UpdateRecvVdFile4(DocNo, toUpload4.Name));
            }
            if (mIsRecvVdAllowToUploadFile && TxtFile5.Text.Length > 0 && TxtFile5.Text != "openFileDialog1")
            {
                FileInfo toUpload5 = new FileInfo(string.Format(@"{0}", TxtFile5.Text));
                UploadFile5(DocNo, toUpload5);
                cml.Add(UpdateRecvVdFile5(DocNo, toUpload5.Name));
            }
            if (mIsRecvVdAllowToUploadFile && TxtFile6.Text.Length > 0 && TxtFile6.Text != "openFileDialog1")
            {
                FileInfo toUpload6 = new FileInfo(string.Format(@"{0}", TxtFile6.Text));
                UploadFile6(DocNo, toUpload6);
                cml.Add(UpdateRecvVdFile6(DocNo, toUpload6.Name));
            }

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
                {
                    if (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2" && Sm.GetGrdStr(Grd3, Row, 2).Length > 0 && Sm.GetGrdStr(Grd3, Row, 2) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 2) != Sm.GetGrdStr(Grd3, Row, 7))
                        {
                            UploadFileDtl(DocNo, Row, Sm.GetGrdStr(Grd3, Row, 2));
                        }
                    }
                }
            }

            Sm.StdMsg(mMsgType.Info,
                "New Document# : " + DocNo + Environment.NewLine +
                "Saving new data is completed.");

            ShowData(DocNo);

            lDODtl.Clear(); lDOHdr.Clear();
        }

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblRecvVdHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length == 0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length > 0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr)
                ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Document# : " + x.DocNo + Environment.NewLine +
                        "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                        "Invalid data.");
                    return true;
                }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 5));
                        No += 1;
                    }
                }
            }
            Filter = " Where (" + Filter + ")";

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblPOHdr " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private MySqlCommand SavePOHdr(
            string PODocNo, 
            string PODocNo2, 
            string PODocNoSource, 
            string RevNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPOHdr ");
            SQL.AppendLine("(DocNo, DocDt, ");
            SQL.AppendLine("LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("DocNoSource, RevNo, VdCode, VdContactPerson, ShipTo, BillTo, CurCode, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, SiteCode, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select @PODocNo2, DocDt, ");
            SQL.AppendLine("@LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("@PODocNoSource, @RevNo, VdCode, VdContactPerson, ShipTo, BillTo, CurCode, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, SiteCode, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt, Null, Null ");
            SQL.AppendLine("From TblPOHdr Where DocNo=@PODocNo;");

            SQL.AppendLine("Update TblPOHdr Set Remark=Concat('Split To PO ', @PODocNo2) Where DocNo=@PODocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo2", PODocNo2);
            Sm.CmParam<String>(ref cm, "@PODocNoSource", PODocNoSource);
            Sm.CmParam<String>(ref cm, "@RevNo", RevNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            return cm;
        }

        private MySqlCommand SavePODtl(string PODocNo, string PODNo, string PODocNo2, string PODNo2, decimal Qty2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPODtl(DocNo, DNo, CancelInd, ProcessInd, PORequestDocNo, PORequestDNo, Qty, Discount, DiscountAmt, RoundingValue, EstRecvDt, Remark, CreateBy, CreateDt, LastUpBy, LastUpDt) ");
            SQL.AppendLine("Select @PODocNo2, @PODNo2, CancelInd, 'O', PORequestDocNo, PORequestDNo, @Qty2, Discount, DiscountAmt, RoundingValue, EstRecvDt, Remark, CreateBy, CreateDt, Null, Null ");
            SQL.AppendLine("From TblPODtl Where DocNo=@PODocNo And DNo=@PODNo; ");

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("    CancelInd = Case When Qty=@Qty2 Then 'Y' Else CancelInd End, Qty=Qty-@Qty2 ");
            SQL.AppendLine("Where DocNo=@PODocNo And DNo=@PODNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("    ProcessInd='F' ");
            SQL.AppendLine("Where DocNo=@PODocNo And DNo=@PODNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@PODNo", PODNo);
            Sm.CmParam<String>(ref cm, "@PODocNo2", PODocNo2);
            Sm.CmParam<String>(ref cm, "@PODNo2", PODNo2);
            Sm.CmParam<decimal>(ref cm, "@Qty2", Qty2);

            return cm;
        }

        private MySqlCommand SavePOHdr(
            string PODocNo,
            string PODocNo2
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOHdr As T1  ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select A.DocNo, Sum( ");
            SQL.AppendLine("        Case When A.CancelInd='Y' Then 0 Else ");
			SQL.AppendLine("            (A.Qty*C.UPrice)-((A.Qty*C.UPrice)*A.Discount/100)-A.DiscountAmt+A.RoundingValue ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C On B.QtDocNo=C.DocNo And B.QtDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@PODocNo ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("Left Join TblTax T4 On T1.TaxCode1=T4.TaxCode ");
            SQL.AppendLine("Left Join TblTax T5 On T1.TaxCode2=T5.TaxCode ");
            SQL.AppendLine("Left Join TblTax T6 On T1.TaxCode3=T6.TaxCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("T1.TaxAmt=(IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)), ");
            SQL.AppendLine("T1.Amt=IfNull(T3.Amt, 0)+((IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)))+T1.CustomsTaxAmt-T1.DiscountAmt ");
            SQL.AppendLine("Where T1.DocNo=@PODocNo; ");

            SQL.AppendLine("Update TblPOHdr As T1  ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select A.DocNo, Sum( ");
            SQL.AppendLine("        Case when A.CancelInd='Y' Then 0 Else ");
            SQL.AppendLine("            (A.Qty*C.UPrice)-((A.Qty*C.UPrice)*A.Discount/100)-A.DiscountAmt+A.RoundingValue ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblQtDtl C On B.QtDocNo=C.DocNo And B.QtDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@PODocNo2 ");
            SQL.AppendLine("    Group By A.DocNo ");
            SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            SQL.AppendLine("Left Join TblTax T4 On T1.TaxCode1=T4.TaxCode ");
            SQL.AppendLine("Left Join TblTax T5 On T1.TaxCode2=T5.TaxCode ");
            SQL.AppendLine("Left Join TblTax T6 On T1.TaxCode3=T6.TaxCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("T1.TaxAmt=(IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)), ");
            SQL.AppendLine("T1.Amt=IfNull(T3.Amt, 0)+((IfNull(T3.Amt, 0)*(IfNull(T4.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T5.TaxRate, 0)/100))+(IfNull(T3.Amt, 0)*(IfNull(T6.TaxRate, 0)/100)))+T1.CustomsTaxAmt-T1.DiscountAmt ");
            SQL.AppendLine("Where T1.DocNo=@PODocNo2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@PODocNo", PODocNo);
            Sm.CmParam<String>(ref cm, "@PODocNo2", PODocNo2);
            return cm;
        }

        private void GenerateSOPOHdr(ref List<SOPOHdr> lh)
        {
            string PODocNoSourceTemp = string.Empty, RevNoTemp = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNoSource, Max(RevNo) RevNo ");
            SQL.AppendLine("From TblPOHdr ");
            SQL.AppendLine("Where Position(Concat('##', DocNoSource, '##') In @Param)>0 ");
            SQL.AppendLine("Group By DocNoSource; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedSOPO());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNoSource", "RevNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        PODocNoSourceTemp = Sm.DrStr(dr, c[0]);
                        RevNoTemp = (Sm.DrDec(dr, c[1]) + 1m).ToString();
                        lh.Add(new SOPOHdr()
                        {
                            PODocNoSource = PODocNoSourceTemp,
                            RevNo=RevNoTemp,
                            PODocNo2 = PODocNoSourceTemp + "/R" + RevNoTemp
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedSOPO()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 8).Length > 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 8) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GenerateSOPODtla(ref List<SOPODtla> lda)
        {
            string PODocNoSource = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocNoSource, ");
            SQL.AppendLine("(B.Qty-IfNull(C.Qty2, 0)-IfNull(D.Qty3, 0)+IfNull(E.Qty4, 0)) As OutstandingQty ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("    From TblRecvVdDtl T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("    From TblPOQtyCancel T1 ");
            SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And B.DNo=D.DNo  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.DocNo, T4.DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 On T2.RecvVdDocNo=T3.DocNo And T2.RecvVdDNo=T3.DNo And T3.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.PODocNo=T4.DocNo And T3.PODNo=T4.DNo And T4.CancelInd='N' ");
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("    Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine(") E On A.DocNo=E.DocNo And B.DNo=E.DNo ");
            SQL.AppendLine("Where Position(Concat('##', A.DocNo, '##') In @Param)>0 ");
            SQL.AppendLine("Order By A.DocNo, B.DNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedSOPO2());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "DocNoSource", "OutstandingQty" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lda.Add(new SOPODtla()
                        {
                            PODocNo = Sm.DrStr(dr, c[0]),
                            PODNo = Sm.DrStr(dr, c[1]),
                            PODocNoSource = Sm.DrStr(dr, c[2]),
                            POOutstandingQty = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private string GetSelectedSOPO2()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd2, Row, 0) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void UpdateBalanceSOPODtla(ref List<SOPODtla> lda)
        {
            lda.ForEach(d =>
                {
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    {
                        if (
                            Sm.CompareStr(d.PODocNo, Sm.GetGrdStr(Grd2, Row, 0)) &&
                            Sm.CompareStr(d.PODNo, Sm.GetGrdStr(Grd2, Row, 1)) 
                            )
                        {
                            d.RecvQty = Sm.GetGrdDec(Grd2, Row, 6);
                            break;
                        }
                    }
                    d.Balance = d.POOutstandingQty - d.RecvQty;
                }
            );
        }

        private List<SOPODtlb> GenerateSOPODtlb(List<SOPOHdr> lh, ref List<SOPODtla> lda)
        {
            int DNo = 0;
            string 
                PODocNoTemp = string.Empty,
                PODocNo2 = string.Empty;

            var ldb = new List<SOPODtlb>();
            lda.ForEach(d =>
                {
                    if (d.Balance>0)
                    {
                        if (Sm.CompareStr(PODocNoTemp, d.PODocNo))
                            DNo+=1;    
                        else
                            DNo=1;

                        PODocNo2 = string.Empty;
                        foreach (var i in lh.Where(Index => Index.PODocNoSource == d.PODocNoSource))
                        {
                            PODocNo2 = i.PODocNo2;
                            break;
                        }

                        ldb.Add(new SOPODtlb()
                        {
                            PODocNo = d.PODocNo,
                            PODNo = d.PODNo,
                            PODocNo2 = PODocNo2,
                            PODNo2 = Sm.Right("00" + DNo, 3),
                            Qty2 = d.Balance
                        });
                    }
                    PODocNoTemp = d.PODocNo;
                }
            );
            return ldb;
        }

        private string GetPORevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPOHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            var Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();

            return Revision;
        }


        #region Old Code

        //private MySqlCommand SplitOutstandingPO_UpdateOustandingPOQty(int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblPODtl Set ");
        //    SQL.AppendLine("    Qty=Qty-@Qty, Remark =Concat(IfNull(Remark, ''), 'Split PO')   ");
        //    SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd2, Row, 0));
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd2, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 7));

        //    return cm;
        //}

        //private void GenerateSOPOHdr(ref List<SOPOHdr> lh, string ItScCode)
        //{
        //    var cm = new MySqlCommand();
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select DocNo, DocDt, Case When IfNull(TaxCode1, '')='' Then 'N' Else 'Y' End TaxInd ");
        //    SQL.AppendLine("From TblPOHdr ");
        //    SQL.AppendLine("Where Position(Concat('##', DocNo, '##') In @Param)>0 ");
        //    SQL.AppendLine("Order By DocNo; ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();
        //        Sm.CmParam<String>(ref cm, "@Param", GetSelectedSOPO());
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[]{ "DocNo", "DocDt", "TaxInd" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                lh.Add(new SOPOHdr()
        //                {
        //                    PODocNo = Sm.DrStr(dr, c[0]),
        //                    PODocDt = Sm.DrStr(dr, c[1]),
        //                    TaxInd = Sm.DrStr(dr, c[2]),
        //                    ItScCode = ItScCode
        //                });
        //            }
        //        }
        //        dr.Close();
        //    }
        //}

        //private string GetSelectedSOPO()
        //{
        //    var SQL = string.Empty;
        //    if (Grd2.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd2.Rows.Count; Row++)
        //        {
        //            if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && Sm.GetGrdDec(Grd2, Row, 7)>0m)
        //            {
        //                if (SQL.Length != 0) SQL += ", ";
        //                SQL += "##" + Sm.GetGrdStr(Grd2, Row, 0) + "##";
        //            }
        //        }
        //    }
        //    return (SQL.Length == 0 ? "##XXX##" : SQL);
        //}

        //private void GenerateSOPODtl(ref List<SOPODtl> ld, int Row)
        //{           
        //    ld.Add(new SOPODtl()
        //    {
        //        PODocNo = Sm.GetGrdStr(Grd2, Row, 0),
        //        PODNo = Sm.GetGrdStr(Grd2, Row, 1),
        //        Qty2 = Sm.GetGrdDec(Grd2, Row, 7)
        //    });
        //}

        //private void ProcessSplitOutstandingPO(List<SOPOHdr> lh, List<SOPODtl> ld)
        //{
        //    int DNo = 1;
        //    string PODocNo = string.Empty;

        //    lh.ForEach(h => 
        //        {
        //            h.PODocNo2 = GeneratePODocNo(
        //                IsProcFormat,
        //                h.PODocDt,
        //                "PO",
        //                "TblPOHdr",
        //                h.TaxInd,
        //                h.ItScCode);

        //            DNo = 1;
        //            ld.ForEach(d =>
        //            {
        //                if (Sm.CompareStr(h.PODocNo, d.PODocNo))
        //                {
        //                    d.PODocNo2 = h.PODocNo2;
        //                    d.PODNo2 = Sm.Right("00" + DNo, 3);
        //                    DNo += 1;
        //                }
        //            });

        //            InsertSplitOutstandingPO(h.PODocNo, h.PODocNo2, ref ld);
        //        });
        //}

        //private string GeneratePODocNo(
        //    string IsProcFormat, 
        //    string DocDt, 
        //    string DocType, 
        //    string Tbl, 
        //    string TaxInd, 
        //    string ItScCode)
        //{
        //    string Yr = DocDt.Substring(2, 2), Mth = DocDt.Substring(4, 2);
            
        //    var SQL = new StringBuilder();

        //    if (mIsPOSplitBasedOnTax == "Y" && TaxInd=="N")
        //    {
        //        if (IsProcFormat == "1")
        //        {
        //            SQL.Append("Select Concat('" + ItScCode + "', '/', ");
        //            SQL.Append("IfNull(( ");
        //            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //            SQL.Append("        Select Convert(left(right(DocNo, '" + mPOWithoutTitleLength + "'), 4), Decimal) As DocNo From " + Tbl);
        //            SQL.Append("        Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //            SQL.Append("        And DocNo Not Like '%" + mPODocTitle + "%' ");
        //            SQL.Append("        Order By left(right(DocNo, '" + mPOWithoutTitleLength + "'), 4) Desc Limit 1 ");
        //            SQL.Append("       ) As Temp ");
        //            SQL.Append("   ), '0001') ");
        //            SQL.Append(", '/', '" + mPODocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "' ");
        //            SQL.Append(") As DocNo");
        //        }
        //        else
        //        {
        //            SQL.Append("Select Concat( ");
        //            SQL.Append("IfNull(( ");
        //            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //            SQL.Append("    Select Convert(left(right(DocNo, '" + mPOWithoutTitleLength + "'), 4), Decimal) As DocNo  From " + Tbl);
        //            SQL.Append("    Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //            SQL.Append("    And DocNo Not Like '%" + mPODocTitle + "%' ");
        //            SQL.Append("    Order By left(right(DocNo, '" + mPOWithoutTitleLength + "'), 4) Desc Limit 1 ");
        //            SQL.Append("       ) As Temp ");
        //            SQL.Append("   ), '0001') ");
        //            SQL.Append(", '/', '" + mPODocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "' ");
        //            SQL.Append(") As DocNo");
        //        }
        //    }
        //    else
        //    {
        //        if (IsProcFormat == "1")
        //        {
        //            SQL.Append("Select Concat('" + ItScCode + "', '/', ");
        //            SQL.Append("IfNull(( ");
        //            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //            SQL.Append("         Select Convert(left(right(DocNo, '" + mPOWithoutTitleLength + "'), 4), Decimal) As DocNo From " + Tbl);
        //            SQL.Append("        Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //            SQL.Append("        And DocNo Like '%" + mPODocTitle + "%' ");
        //            SQL.Append("        Order By left(right(DocNo, '" + mPOWithoutTitleLength + "'), 4) Desc Limit 1 ");
        //            SQL.Append("       ) As Temp ");
        //            SQL.Append("   ), '0001') ");
        //            SQL.Append(", '/', '" + ((mPODocTitle.Length == 0) ? "XXX" : mPODocTitle));
        //            SQL.Append("', '/', '" + mPODocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
        //            SQL.Append(") As DocNo");
        //        }
        //        else
        //        {
        //            SQL.Append("Select Concat( ");
        //            SQL.Append("IfNull(( ");
        //            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
        //            SQL.Append("       Select Convert(left(right(DocNo, '" + mPOWithTitleLength + "'), 4), Decimal) As DocNo From " + Tbl);
        //            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
        //            SQL.Append("       And DocNo Like '%" + mPODocTitle + "%' ");
        //            SQL.Append("       Order By left(right(DocNo, '" + mPOWithTitleLength + "'), 4) Desc Limit 1 ");
        //            SQL.Append("       ) As Temp ");
        //            SQL.Append("   ), '0001') ");
        //            SQL.Append(", '/', '" + ((mPODocTitle.Length == 0) ? "XXX" : mPODocTitle));
        //            SQL.Append("', '/', '" + mPODocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
        //            SQL.Append(") As DocNo");
        //        }
        //    }

        //    return Sm.GetValue(SQL.ToString());
        //}

        //private void InsertSplitOutstandingPO(string PODocNo, string PODocNo2, ref List<SOPODtl> ld)
        //{
        //    var cml = new List<MySqlCommand>();
        //    cml.Add(SavePOHdr(PODocNo, PODocNo2));
        //    ld.ForEach(d =>{
        //        if (Sm.CompareStr(PODocNo, d.PODocNo))
        //            cml.Add(SavePODtl(PODocNo, d.PODNo, PODocNo2, d.PODNo2, d.Qty2)); 
        //    });
        //    Sm.ExecCommands(cml);
        //}

        #endregion

        //private void RecomputeOutstandingRecvExpeditionQty()
        //{
        //    var SQL = new StringBuilder();
        //    string DNo = string.Empty;
        //    decimal QtyPurchase = 0m, Qty = 0m, Qty2 = 0m, Qty3 = 0m;

        //    SQL.AppendLine("Select A.DNo, ");
        //    SQL.AppendLine("A.QtyPurchase-A.RecvVdQtyPurchase As QtyPurchase, ");
        //    SQL.AppendLine("A.Qty-A.RecvVdQty As Qty, ");
        //    SQL.AppendLine("A.Qty2-A.RecvVdQty2 As Qty2, ");
        //    SQL.AppendLine("A.Qty3-A.RecvVdQty3 As Qty3 ");
        //    SQL.AppendLine("From TblRecvExpeditionDtl A ");
        //    SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        var cm = new MySqlCommand()
        //        {
        //            Connection = cn,
        //            CommandText = SQL.ToString()
        //        };
        //        Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvExpeditionDocNo.Text);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "DNo", "QtyPurchase", "Qty", "Qty2", "Qty3" });

        //        if (dr.HasRows)
        //        {
        //            Grd1.BeginUpdate();
        //            while (dr.Read())
        //            {
        //                DNo = Sm.DrStr(dr, 0);
        //                QtyPurchase = Sm.DrDec(dr, 1);
        //                Qty = Sm.DrDec(dr, 2);
        //                Qty2 = Sm.DrDec(dr, 3);
        //                Qty3 = Sm.DrDec(dr, 4);
        //                for (int r = 0; r <= Grd1.Rows.Count - 1; r++)
        //                {
        //                    if (Sm.GetGrdStr(Grd1, r, 41).Length > 0 && Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 41), DNo))
        //                    {
        //                        Grd1.Cells[r, 42].Value = QtyPurchase;
        //                        Grd1.Cells[r, 43].Value = Qty;
        //                        Grd1.Cells[r, 44].Value = Qty2;
        //                        Grd1.Cells[r, 45].Value = Qty3;
        //                        break;
        //                    }
        //                }
        //            }
        //            Grd1.EndUpdate();
        //        }
        //        dr.Close();
        //    }
        //}

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            ComputeOutstandingQty();
            //if (mIsRecvExpeditionEnabled) RecomputeOutstandingRecvExpeditionQty();

            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                (mIsFilterBySite && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ? 
                    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) : 
                    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                (mMenuCodeForRecvVdAutoCreateDO && Sm.IsLueEmpty(LueCCCode, "Cost Center")) ||
                (mIsRecvExpeditionEnabled && Sm.IsTxtEmpty(TxtRecvExpeditionDocNo, "Expedition's received#", false)) ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                (!mMenuCodeForRecvVdAutoCreateDO && IsItemServiceInd()) || 
                IsQtyNotValid() ||
                IsBalanceNotValid() ||
                IsSubcategoryDifferent() ||
                IsDateNotValid() ||
                IsEntityNotValid() ||
                IsPORevisionExisted() ||
                (mMenuCodeForRecvVdAutoCreateDO && IsCostCategoryStillEmpty()) ||
                (IsRecvVdExpeditionAlreadyProcessed()) ||
                (IsRecvVdExpeditionRejectedNotValid()) ||
                (mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsJournalSettingInvalid()) ||
                IsItemCategoryInvalid() ||
                (mIsRecvVdAllowToUploadFile && IsFileMandatory()) ||
                (mIsRecvVdAllowToUploadFile && IsUploadFileNotValid()) ||
                (mIsRecvVdAllowToUploadFile && IsUploadFileNotValid2()) ||
                (mIsRecvVdAllowToUploadFile && IsUploadFileNotValid3()) ||
                (mIsRecvVdAllowToUploadFile && IsUploadFileNotValid4()) ||
                (mIsRecvVdAllowToUploadFile && IsUploadFileNotValid5()) ||
                (mIsRecvVdAllowToUploadFile && IsUploadFileNotValid6()) ||
                (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2" && IsGrd3ValueNotValid())
                ;
        }
        
        private bool IsItemCategoryInvalid()
        {
            if (mIsAutoJournalActived && mMenuCodeForRecvVdAutoCreateDO && mIsItemCategoryUseCOAAPAR && Grd1.Rows.Count > 2)
            {
                string ItCtCode = Sm.GetValue("Select ItCtCode From TblItem Where ItCode = @Param; ", Sm.GetGrdStr(Grd1, 0, 8));

                for (int i = 1; i < Grd1.Rows.Count - 1; ++i)
                {
                    string tempItCtCode = Sm.GetValue("Select ItCtCode From TblItem Where ItCode = @Param; ", Sm.GetGrdStr(Grd1, i, 8));

                    if (tempItCtCode != ItCtCode)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Different item category detected. This will affect the journal process.");
                        Sm.FocusGrd(Grd1, i, 9);
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsRecvVdExpeditionRejectedNotValid()
        {
            if (mIsRecvExpeditionEnabled)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdBool(Grd1, i, 47))
                    {
                        if (Sm.GetGrdDec(Grd1, i, 43) != Sm.GetGrdDec(Grd1, i, 16))
                        {
                            Sm.StdMsg(mMsgType.Warning, "Invalid rejected expedition's item.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsRecvVdExpeditionAlreadyProcessed()
        {
            if (TxtRecvExpeditionDocNo.Text.Length <= 0) return false;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, DocNo = string.Empty, DNo = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (IsRecvVdExpeditionAlreadyProcessed(r))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                        "PO# : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + 
                        "Received By Expedition# : " + Sm.GetGrdStr(Grd1, r, 40) + Environment.NewLine + Environment.NewLine +
                        "This data already processed.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsRecvVdExpeditionAlreadyProcessed(int r)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblRecvVdHdr X1, TblRecvVdDtl X2 ");
            SQL.AppendLine("Where X1.DocNo=X2.DocNo ");
            SQL.AppendLine("And X2.CancelInd='N' And X2.Status In ('O', 'A') ");
            SQL.AppendLine("And X1.RecvExpeditionDocNo Is Not Null ");
            SQL.AppendLine("And X2.RecvExpeditionDocNo=@DocNo ");
            SQL.AppendLine("And X2.RecvExpeditionDNo=@DNo ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 40));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 41));

            return Sm.IsDataExist(cm);
        }

        private bool IsCostCategoryStillEmpty()
        {
            if(mDoctitle !="IMS")
                SetCostCategory();

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 36).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "PO# : " + Sm.GetGrdStr(Grd1, i, 5) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, i, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, i, 9) + Environment.NewLine + Environment.NewLine +
                        "Cost category's COA Setting still empty." + Environment.NewLine +
                        "Please contact Finance/Accounting Department !");
                    return true;
                }
               

                if (Sm.GetGrdStr(Grd1, i, 39).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "PO# : " + Sm.GetGrdStr(Grd1, i, 5) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd1, i, 8) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd1, i, 9) + Environment.NewLine + Environment.NewLine +
                        "COA's account# is empty." + Environment.NewLine +
                        "Please contact Finance/Accounting Department !");
                    return true;
                }
            }

            return false;
        }

        private bool IsPORevisionExisted()
        {
            string DocNo = string.Empty, DNo = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 5).Length > 0)
                {
                    DocNo = Sm.GetGrdStr(Grd1, r, 5);
                    DNo = Sm.GetGrdStr(Grd1, r, 6);
                    if (Sm.IsDataExist(
                        "Select 1 From TblPORevision Where PODocNo=@Param1 And PODNo=@Param2 And CancelInd='N' And Status='O';",
                        DocNo, DNo, string.Empty))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "PO# : " + DocNo + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                            "There's an outstanding revision (PO) for this PO's item.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsItemServiceInd()
        {
            if (TxtRecvExpeditionDocNo.Text.Length > 0) return false;

            string mItCode = string.Empty, mItCode2 = string.Empty;
            int mRow = 0;
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 8).Length > 0)
                    {
                        if (mItCode.Length > 0) mItCode += ",";
                        mItCode += Sm.GetGrdStr(Grd1, i, 8);
                    }
                }
            }

            SQL.AppendLine("Select ItCode from TblItem Where Find_In_Set(ItCode, @Param) And ServiceItemInd='Y' Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), mItCode))
            {
                mItCode2 = Sm.GetValue(SQL.ToString(), mItCode);
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 8) == mItCode2)
                    {
                        mRow = i;
                        break;
                    }
                }

                Sm.StdMsg(mMsgType.Warning,
                    "Row# : " + (mRow + 1) + Environment.NewLine +
                    "Item Code : " + mItCode2 + Environment.NewLine +
                    "Service item cannot be processed.");
                Sm.FocusGrd(Grd1, mRow, 9);

                return true;
            }

            return false;
        }

        private bool IsEntityNotValid()
        {
            string WhsEntCode = string.Empty;
            string POEntCode = string.Empty;
            if (mIsRecvVdValidateByEntity)
            {
                WhsEntCode = Sm.GetValue("Select D.EntName From  tblwarehouse A "+
                                        "Inner Join TblCostcenter B on A.CCCode = B.CCCode "+
                                        "Inner Join TblProfitcenter C On B.ProfitcenterCode = C.ProfitcenterCode "+
                                        "Inner Join TblEntity D On C.EntCode = D.EntCode "+
                                        "Where A.WhsCode = '"+Sm.GetLue(LueWhsCode)+"' ");

                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk bandingin entity PO 
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        POEntCode = Sm.GetValue("Select C.EntName " +
                                        "From TblPOHdr A1 " +
                                        "Inner Join tblSite A On A1.SiteCode = A.SiteCode " +
                                        "Inner Join TblProfitCenter B On A.ProfitCenterCode = B.Profitcentercode " +
                                        "Inner Join TblEntity C On B.EntCode=C.EntCode "+
                                        "Where A1.DocNo = '"+Sm.GetGrdStr(Grd1, Row, 5)+"' ");

                        if (!Sm.CompareStr(WhsEntCode, POEntCode))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                            "Entity PO ( " + Sm.GetGrdStr(Grd1, Row, 5) + " ) : "+POEntCode+".\n"+
                            "Entity Receiving : " + WhsEntCode + ".\n" +
                            "This transaction has different entity.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.GetDte(DteDocDt);

            if (mIsComparedToDetailDate)
            {
                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk mendapatkan tanggal terbaru
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(compare1, Sm.GetGrdDate(Grd1, Row, 31)) < 0)
                        {
                            compare1 = Sm.GetGrdDate(Grd1, Row, 31);
                            compare2 = compare1;
                        }
                        else
                            compare2 = compare1;
                    }
                    mDetailDate = compare2;

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Date should be the same or greater than PO Date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty, Query = string.Empty, Query2 = string.Empty;

            if (mIsRecvExpeditionEnabled)
            {
                Query = "Select 1 From TblRecvExpeditionDtl Where DocNo=@Param1 And DNo=@Param2 And (CancelInd='Y' Or Status = 'C') Limit 1;";
                Query2 = "Select 1 From TblRecvExpeditionDtl Where DocNo=@Param1 And DNo=@Param2 And CancelInd='N' And Status In ('O', 'A') And QtyPurchase>0.00 And RecvVdQtyPurchase>0.00 Limit 1;";
            }
            
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                    "PO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                    "PO's Local# : " + Sm.GetGrdStr(Grd1, Row, 29) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 34) + Environment.NewLine;
                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 5, false, "PO# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 14, true, string.Concat(Msg, Environment.NewLine, "Quantity (Purchase) should not be 0.00.")) ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 16, true, string.Concat(Msg, Environment.NewLine, "Quantity (Inventory) should not be 0.00.")) ||
                    (Grd1.Cols[18].Visible && Sm.IsGrdValueEmpty(Grd1, Row, 18, true, string.Concat(Msg, Environment.NewLine, "Quantity (Inventory) should not be 0.00."))) ||
                    (Grd1.Cols[20].Visible && Sm.IsGrdValueEmpty(Grd1, Row, 20, true, string.Concat(Msg, Environment.NewLine, "Quantity (Inventory) should not be 0.00."))) ||
                    (mIsRemarkForApprovalMandatory && Sm.IsGrdValueEmpty(Grd1, Row, 22, false, string.Concat(Msg, Environment.NewLine, "Remark is empty.")))
                    )
                    return true;

                if (mIsRecvExpeditionEnabled)
                {
                    if (Sm.IsDataExist(Query,
                        Sm.GetGrdStr(Grd1, Row, 40), Sm.GetGrdStr(Grd1, Row, 41), string.Empty,
                        string.Concat(Msg, Environment.NewLine,
                            "Expedition's Received# : " + Sm.GetGrdStr(Grd1, Row, 40), Environment.NewLine,
                            "This data already cancelled.")
                        )) return true;

                    if (Sm.IsDataExist(Query2,
                        Sm.GetGrdStr(Grd1, Row, 40), Sm.GetGrdStr(Grd1, Row, 41), string.Empty,
                        string.Concat(Msg, Environment.NewLine,
                            "Expedition's Received# : " + Sm.GetGrdStr(Grd1, Row, 40), Environment.NewLine,
                            "This data already processed.")
                        )) return true;

                    if (Sm.GetGrdDec(Grd1, Row, 14) > Sm.GetGrdDec(Grd1, Row, 42))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            string.Concat(Msg, Environment.NewLine,
                            "Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 14), 0) + Environment.NewLine +
                            "Expedition's Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 42), 0) + Environment.NewLine +
                            "UoM (Purchase) : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine + Environment.NewLine +
                            "Received quantity is bigger than expedition's received quantity."));
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 16) > Sm.GetGrdDec(Grd1, Row, 43))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            string.Concat(Msg, Environment.NewLine,
                            "Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 16), 0) + Environment.NewLine +
                            "Expedition's Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 43), 0) + Environment.NewLine +
                            "UoM (Inventory) : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine + Environment.NewLine +
                            "Received quantity is bigger than expedition's received quantity."));
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 44))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            string.Concat(Msg, Environment.NewLine,
                            "Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 18), 0) + Environment.NewLine +
                            "Expedition's Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 44), 0) + Environment.NewLine +
                            "UoM (Inventory) : " + Sm.GetGrdStr(Grd1, Row, 19) + Environment.NewLine + Environment.NewLine +
                            "Received quantity is bigger than expedition's received quantity."));
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 20) > Sm.GetGrdDec(Grd1, Row, 45))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            string.Concat(Msg, Environment.NewLine,
                            "Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 20), 0) + Environment.NewLine +
                            "Expedition's Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 45), 0) + Environment.NewLine +
                            "UoM (Inventory) : " + Sm.GetGrdStr(Grd1, Row, 21) + Environment.NewLine + Environment.NewLine +
                            "Received quantity is bigger than expedition's received quantity."));
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                {
                    if (
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 17)) && Sm.GetGrdDec(Grd1, Row, 14) != Sm.GetGrdDec(Grd1, Row, 16)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 18)) && Sm.GetGrdDec(Grd1, Row, 14) != Sm.GetGrdDec(Grd1, Row, 18)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 15), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 14) != Sm.GetGrdDec(Grd1, Row, 20)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 19)) && Sm.GetGrdDec(Grd1, Row, 16) != Sm.GetGrdDec(Grd1, Row, 18)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 17), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 16) != Sm.GetGrdDec(Grd1, Row, 20)) ||
                        (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 19), Sm.GetGrdStr(Grd1, Row, 21)) && Sm.GetGrdDec(Grd1, Row, 18) != Sm.GetGrdDec(Grd1, Row, 20))
                        )
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "PO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine + Environment.NewLine +
                            "Quantity is not valid.");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsBalanceNotValid()
        {
            for (int Row = 0; Row <= Grd2.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && Sm.GetGrdDec(Grd2, Row, 7) < 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "PO# : " + Sm.GetGrdStr(Grd2, Row, 0) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 3) + Environment.NewLine + 
                        "Balance : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 7), 0) + Environment.NewLine +
                        "UoM : " + Sm.GetGrdStr(Grd2, Row, 4) + Environment.NewLine + Environment.NewLine + 
                        "Balance is less than 0.");
                    return true;
                }
            }

            return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 24).Length == 0)
                {
                    Grd1.Cells[Row, 24].Value = Grd1.Cells[Row, 25].Value = "XXX";
                }
            }
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 24);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 24))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        //private bool IsCOAJournalNotValid()
        //{
        //    var l = new List<COA>();
        //    var SQL = new StringBuilder();
        //    string mItCode = string.Empty;
        //    var cm = new MySqlCommand();

        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
        //        {
        //            if (mItCode.Length > 0) mItCode += " Or ";
        //            mItCode += " (A.ItCode=@ItCode00" + r.ToString() + ") ";
        //            Sm.CmParam<String>(ref cm, "@ItCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
        //        }
        //    }

        //    if (!mMenuCodeForRecvVdAutoCreateDO)
        //    {
        //        SQL.AppendLine("Select B.Acno From tblItem A ");
        //        SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
        //        SQL.AppendLine("Where " + mItCode);
        //        SQL.AppendLine("Union All");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter  ");
        //        SQL.AppendLine("Where parcode = 'VendorAcNoUnInvoiceAP' ");
        //    }
        //    else
        //    {
        //        //SQL.AppendLine("Select AcNo2 As AcNo From TblEntity Where EntCOde = @EntCodeWhs ");
        //        //SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select C.AcNo as AcNo ");
        //        SQL.AppendLine("From TblItem A ");
        //        SQL.AppendLine("Inner Join TblItemCostCategory B On A.ItCode = B.ItCode And B.CCCode = @CCCode ");
        //        SQL.AppendLine("Inner Join TblCostCategory C On B.CCtCode = C.CCtCOde And B.CCCode = C.CCCode ");
        //        SQL.AppendLine("Where "+mItCode);

        //        //Credit RecvVd
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select ParValue As AcNo ");
        //        SQL.AppendLine("From TblParameter Where Parcode = 'VendorAcNoUnInvoiceAP' ");
        //    }

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        cm.Connection = cn;
        //        cm.CommandTimeout = 600;
        //        cm.CommandText = SQL.ToString();

        //        Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
        //        Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
        //        if (dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                l.Add(new COA()
        //                {
        //                    AcNo = Sm.DrStr(dr, c[0])

        //                });
        //            }
        //        }
        //        dr.Close();
        //    }

        //    foreach (var x in l.Where(w => w.AcNo.Length <= 0))
        //    {
        //        Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
        //        return true;
        //    }

        //    return false;
        //}

        private bool IsJournalSettingInvalid()
        {
            if ((!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) && !mMenuCodeForRecvVdAutoCreateDO) return false;

            var SQL = new StringBuilder();
            string mVendorAcNoUnInvoiceAP = Sm.GetValue("Select ParValue From TblParameter Where Parcode='VendorAcNoUnInvoiceAP'");
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;


            //if (Sm.GetValue("Select AcNo1 From TblEntity Where EntCode = @Param", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode))).Length == 0)
            //{
            //    Sm.StdMsg(mMsgType.Warning, Msg + "Entity Cost Center's COA account# (" + LueCCCode.Text + ") is empty.");
            //    return true;
            //}

            if (Sm.GetValue("Select AcNo2 From TblEntity Where EntCode = @Param", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode))).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Entity Warehouse's COA account# (" + LueWhsCode.Text + ") is empty.");
                return true;
            }
            if (IsJournalSettingInvalid_CostCategory(Msg)) return true;


            if (mIsItemCategoryUseCOAAPAR)
            {
                if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo8")) return true;
            }
            else
            {
                if (mVendorAcNoUnInvoiceAP.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter VendorAcNoUnInvoiceAP is empty.");
                    return true;
                }

            }

            return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B." + COA + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 8);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + (COA == "AcNo8" ? "(AP Uninvoiced)" : "") + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_CostCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, CCtName = string.Empty;

            SQL.AppendLine("Select B.CCtName ");
            SQL.AppendLine("From TblItemCostCategory A ");
            SQL.AppendLine("Inner Join TblCostCategory B On A.CCtCode = B.CCtCode And A.CCCode = B.CCCode And B.AcNo = '' ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine("    And A.ItCode In ( ");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 8);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            cm.CommandText = SQL.ToString();
            CCtName = Sm.GetValue(cm);
            if (CCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost category's COA account# (" + CCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRecvVdHdr(
            string DocNo,
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("/* RecvVd - RecvVdHdr */ ");
            
            SQL.AppendLine("Insert Into TblRecvVdHdr ");
            SQL.AppendLine("(DocNo, DocDt, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("POInd, WhsCode, VdCode, VdDONo, JournalDocNo, SiteCode, Remark, ProductionWorkGroup, ");
            SQL.AppendLine("CustomsDocCode, KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, KBPackaging, KBPackagingQty, KBNonDocInd, ");
            SQL.AppendLine("RecvExpeditionDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            SQL.AppendLine("'Y', @WhsCode, @VdCode, @VdDONo, Null, @SiteCode, @Remark, @ProductionWorkGroup, ");
            SQL.AppendLine("@CustomsDocCode, @KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @KBSubmissionNo, @KBPackaging, @KBPackagingQty, @KBNonDocInd, ");
            SQL.AppendLine("@RecvExpeditionDocNo, @CreateBy, CurrentDateTime()); ");

            if (mIsRecvVdLocalDocNoBasedOnMRLocalDocNo && LocalDocNo.Length == 0)
            {
                string PODocNo = string.Empty, PODNo = string.Empty, Filter = string.Empty;
                
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    PODocNo = Sm.GetGrdStr(Grd1, r, 5);
                    PODNo = Sm.GetGrdStr(Grd1, r, 6);
                    if (PODocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(T1.DocNo=@PODocNo0" + r.ToString() + " And T1.DNo=@PODNo0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@PODocNo0" + r.ToString(), PODocNo);
                        Sm.CmParam<String>(ref cm, "@PODNo0" + r.ToString(), PODNo);
                    }
                }

                if (Filter.Length > 0)
                    Filter = " Where ( " + Filter + ") ";
                else
                    Filter = " Where 1=0 ";

                SQL.AppendLine("Update TblRecvVdHdr Set ");
                SQL.AppendLine("    LocalDocNo=(");
	            SQL.AppendLine("    Select Group_Concat(Distinct T3.LocalDocNo Separator ', ') As Value ");
                SQL.AppendLine("    From TblPODtl T1 ");
                SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo  ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr T3 On T2.MaterialRequestDocNo=T3.DocNo And T3.LocalDocNo Is Not Null ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(") ");
                SQL.AppendLine("Where LocalDocNo Is Null ");
                SQL.AppendLine("And DocNo=@DocNo; ");
            }
            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdDONo", TxtVdDONo.Text);
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", Sm.GetLue(LueCustomsDocCode));
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@KBPackaging", TxtKBPackaging.Text);
            Sm.CmParam<Decimal>(ref cm, "@KBPackagingQty", decimal.Parse(TxtKBPackagingQty.Text));
            Sm.CmParam<String>(ref cm, "@KBNonDocInd", ChkKBNonDocInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@RecvExpeditionDocNo", TxtRecvExpeditionDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRecvExpedition()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvExpeditionDtl A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.RecvExpeditionDocNo As DocNo, T2.RecvExpeditionDNo As DNo, ");
            SQL.AppendLine("    Sum(T2.QtyPurchase) As QtyPurchase, ");
            SQL.AppendLine("    Sum(T2.Qty) As Qty, ");
            SQL.AppendLine("    Sum(T2.Qty2) As Qty2, ");
            SQL.AppendLine("    Sum(T2.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblRecvVdHdr T1, TblRecvVdDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    And T2.CancelInd='N' ");
            SQL.AppendLine("    And T2.Status In ('O', 'A') ");
            SQL.AppendLine("    And T1.RecvExpeditionDocNo=@RecvExpeditionDocNo ");
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("        And T2.RejectedInd = 'N' ");
            SQL.AppendLine("    Group By T2.RecvExpeditionDocNo, T2.RecvExpeditionDNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.RecvVdQtyPurchase=IfNull(B.QtyPurchase, 0.00), ");
            SQL.AppendLine("    A.RecvVdQty=IfNull(B.Qty, 0.00), ");
            SQL.AppendLine("    A.RecvVdQty2=IfNull(B.Qty2, 0.00), ");
            SQL.AppendLine("    A.RecvVdQty3=IfNull(B.Qty3, 0.00) ");
            SQL.AppendLine("Where A.DocNo=@RecvExpeditionDocNo; ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@RecvExpeditionDocNo", TxtRecvExpeditionDocNo.Text);

            return cm;
        }

        private MySqlCommand SaveRecvVdDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* RecvVd - RecvVdDtl */ ");

            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");
            SQL.AppendLine("Set @ShortName:=IfNull((Select ShortName From TblVendor Where VdCode=@VdCode), ''); ");

            for (int r = 0; r< Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 5).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        #region RecvDtl

                        if (mIsRecvVdBatchNoUseProjectCode)
                        {
                            SQL.AppendLine("Insert Into TblRecvVdDtl ");
                            SQL.AppendLine("(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, Remark, ExpiredDt, ");
                            if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
                            SQL.AppendLine("CreateBy, CreateDt) ");
                        }
                        else
                        {
                            if (mIsProjectGroupEnabled)
                            {
                                SQL.AppendLine("Insert Into TblRecvVdDtl ");
                                SQL.AppendLine("(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, ExpiredDt, Remark, ");
                                if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
                                SQL.AppendLine("CreateBy, CreateDt) ");
                            }
                            else
                            {
                                if (mIsAutoGenerateBatchNo)
                                {
                                    SQL.AppendLine("Insert Into TblRecvVdDtl ");
                                    SQL.AppendLine("(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, Remark, ExpiredDt, ");
                                    if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
                                    SQL.AppendLine("CreateBy, CreateDt) ");
                                }
                                else
                                {
                                    SQL.AppendLine("Insert Into TblRecvVdDtl ");
                                    SQL.AppendLine("(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, Remark, ExpiredDt, ");
                                    if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
                                    SQL.AppendLine("CreateBy, CreateDt) ");
                                }
                            }
                        }

                        #endregion
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    #region RecvDtl

                    if (mIsRecvVdBatchNoUseProjectCode)
                    {
                        SQL.AppendLine(" (@DocNo, @DNo_" + r.ToString() +
                            ", 'N', @CancelReason_" + r.ToString() +
                            ", @Status, @PODocNo_" + r.ToString() +
                            ", @PODNo_" + r.ToString() +
                            ", @ItCode_" + r.ToString() +
                            ", ");
                        if (Sm.GetGrdStr(Grd1, r, 33).Length > 0)
                            SQL.AppendLine("@ProjectCode_" + r.ToString() + ", ");
                        else
                            SQL.AppendLine("IfNull(@BatchNo_"+r.ToString() + ", '-'), ");
                        SQL.AppendLine(
                            "Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() + 
                            "), IfNull(@Lot_" + r.ToString() +
                            ", '-'), IfNull(@Bin_" + r.ToString() +
                            ", '-'), @QtyPurchase_" + r.ToString() + 
                            ", @Qty_" + r.ToString() +
                            ", @Qty2_" + r.ToString() +
                            ", @Qty3_" + r.ToString() +
                            ", @RecvExpeditionDocNo_" + r.ToString() + 
                            ", @RecvExpeditionDNo_" + r.ToString() +
                            ", @Remark_" + r.ToString() + 
                            ", @ExpiredDt_" + r.ToString() + 
                            ", ");
                        if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber_" + r.ToString() + ", ");
                        SQL.AppendLine("@CreateBy, @Dt) ");
                    }
                    else
                    {
                        if (mIsProjectGroupEnabled)
                        {
                            SQL.AppendLine(
                                "(@DocNo, @DNo_" + r.ToString() +
                                ", 'N', @CancelReason_" + r.ToString() +
                                ", @Status, @PODocNo_" + r.ToString() +
                                ", @PODNo_" + r.ToString() +
                                ", @ItCode_" + r.ToString() + 
                                ", Concat(Right(@DocDt, 2), '/', Substring(@DocDt, 5, 2), '/', Left(@DocDt, 4)) " +
                                ", Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() +
                                "), IfNull(@Lot_" + r.ToString() + 
                                ", '-'), IfNull(@Bin_" + r.ToString() +
                                ", '-'), @QtyPurchase_" + r.ToString() +
                                ", @Qty_" + r.ToString() +
                                ", @Qty2_" + r.ToString() +
                                ", @Qty3_" + r.ToString() +
                                ", @RecvExpeditionDocNo_" + r.ToString() +
                                ", @RecvExpeditionDNo_" + r.ToString() +
                                ", @ExpiredDt_" + r.ToString() +
                                ", @Remark_" + r.ToString() + 
                                ",  ");
                            if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber_" + r.ToString() + ",  ");
                            SQL.AppendLine("@CreateBy, @Dt) ");
                            
                        }
                        else
                        {
                            if (mIsAutoGenerateBatchNo)
                            {
                                SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() + 
                                    ", 'N', @CancelReason_" + r.ToString() +
                                    ",  @Status, @PODocNo_" + r.ToString() +
                                    ", @PODNo_" + r.ToString() +
                                    ", @ItCode_" + r.ToString() + ", ");
                                if (Sm.GetGrdStr(Grd1, r, 33).Length > 0)
                                {
                                    SQL.AppendLine("@ProjectCode_" + r.ToString() + ", ");
                                }
                                else
                                {
                                    if (mRecvVdAutoBatchNoFormat == "2")
                                        SQL.AppendLine("Concat(IfNull(@ShortName, ''), '-', @DocDt, '-', @DocNo, '-', @DNo_" + r.ToString() + "), ");
                                    else
                                    {
                                        if (mRecvVdAutoBatchNoFormat == "3")
                                        {
                                            SQL.AppendLine("Concat(Substring(@DocDt, 3, 2), '/', Substring(@DocDt, 5, 2), '/', Substring(@DocDt, 7, 2), '/', Left(@DocNo, 4), '/', @VdCode), ");
                                        }
                                        else
                                        {
                                            if (mIsAutoGenerateBatchNoEditable)
                                            {
                                                if (mIsRecvVdUseOptionBatchNoFormula)
                                                    SQL.AppendLine("@BatchNo_" + r.ToString() + ", ");
                                                else
                                                    SQL.AppendLine("Concat(IfNull(@ShortName, ''), '-', @DocDt, '-', @DocNo, Case When @BatchNo_" + r.ToString() + " Is Null Then '' Else Concat('-', @BatchNo_" + r.ToString() + ") End), ");
                                            }
                                            else
                                                SQL.AppendLine("Concat(IfNull(@ShortName, ''), '-', @DocDt, '-', @DocNo), ");
                                        }
                                    }
                                }
                                SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() +
                                    "), IfNull(@Lot_" + r.ToString() + 
                                    ", '-'), IfNull(@Bin_" + r.ToString() +
                                    ", '-'), @QtyPurchase_" + r.ToString() + 
                                    ", @Qty_" + r.ToString() +
                                    ", @Qty2_" + r.ToString() +
                                    ", @Qty3_" + r.ToString() +
                                    ", @RecvExpeditionDocNo_" + r.ToString() +
                                    ", @RecvExpeditionDNo_" + r.ToString() +
                                    ", @Remark_" + r.ToString() +
                                    ", @ExpiredDt_" + r.ToString() + ", ");
                                if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber_" + r.ToString() + ",  ");
                                SQL.AppendLine("@CreateBy, @Dt) ");
                            }
                            else
                            {
                                SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                                    ", 'N', @CancelReason_" + r.ToString() +
                                    ", @Status, @PODocNo_" + r.ToString() +
                                    ", @PODNo_" + r.ToString() +
                                    ", @ItCode_" + r.ToString() + ", ");
                                if (Sm.GetGrdStr(Grd1, r, 33).Length > 0)
                                {
                                    SQL.AppendLine("@ProjectCode_" + r.ToString() + ",  ");
                                }
                                else
                                {
                                    SQL.AppendLine("IfNull(@BatchNo_" + r.ToString() + ",  ");
                                    SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
                                    SQL.AppendLine("), ");
                                }
                                SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo_" + r.ToString() +
                                    "), IfNull(@Lot_" + r.ToString() +
                                    ", '-'), IfNull(@Bin_" + r.ToString() + 
                                    ",  '-'), @QtyPurchase_" + r.ToString() +
                                    ", @Qty_" + r.ToString() +
                                    ", @Qty2_" + r.ToString() +
                                    ", @Qty3_" + r.ToString() +
                                    ", @RecvExpeditionDocNo_" + r.ToString() +
                                    ", @RecvExpeditionDNo_" + r.ToString() +
                                    ", @Remark_" + r.ToString() +
                                    ", @ExpiredDt_" + r.ToString() + ",  ");
                                if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber_" + r.ToString() + ",  ");
                                SQL.AppendLine("@CreateBy, @Dt) ");
                            }
                        }
                    }

                    #endregion

                    #region DocApproval

                    if (mIsRecvVdNeedApproval)
                    {
                        SQL2.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                        SQL2.AppendLine("Select T.DocType, @DocNo, @DNo_" + r.ToString() + ", T.DNo, @CreateBy, @Dt ");
                        SQL2.AppendLine("From TblDocApprovalSetting T ");
                        SQL2.AppendLine("Where T.DocType='RecvVd' ");

                        if (mIsRecvVdApprovalBasedOnWhs)
                        {
                            SQL2.AppendLine("And T.WhsCode In ( ");
                            SQL2.AppendLine("    Select WhsCode From TblRecvVdHdr Where DocNo=@DocNo ");
                            SQL2.AppendLine(") ");
                        }

                        SQL2.AppendLine("; ");

                        SQL2.AppendLine("Update TblRecvVdDtl Set Status='A' ");
                        SQL2.AppendLine("Where DocNo=@DocNo And DNo=@DNo_" + r.ToString());
                        SQL2.AppendLine(" And Not Exists( ");
                        SQL2.AppendLine("    Select 1 From TblDocApproval ");
                        SQL2.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ); ");
                    }

                    if (mIsRecvExpeditionEnabled)
                    {
                        SQL2.AppendLine("Update TblRecvVdDtl ");
                        SQL2.AppendLine("    Set RejectedInd= @RejectedInd_" + r.ToString());
                        SQL2.AppendLine(" Where DocNo=@DocNo And DNo=@DNo_" + r.ToString() + "; ");
                    }

                    #endregion

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@CancelReason_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<String>(ref cm, "@PODocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 5));
                    Sm.CmParam<String>(ref cm, "@PODNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    if (mIsRecvVdUseOptionBatchNoFormula)
                        Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(),
                            Sm.GetGrdStr(Grd1, r, 10).Length > 0 ?
                            Sm.GetGrdStr(Grd1, r, 10) : "-");
                    else
                        Sm.CmParam<String>(ref cm, "@BatchNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 10));
                    Sm.CmParam<String>(ref cm, "@Lot_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 12));
                    Sm.CmParam<String>(ref cm, "@Bin_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPurchase_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 14));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 16));
                    Sm.CmParam<Decimal>(ref cm, "@Qty2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@Qty3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 22));
                    Sm.CmParam<String>(ref cm, "@ProjectCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 33));
                    Sm.CmParam<String>(ref cm, "@RecvExpeditionDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 40));
                    Sm.CmParam<String>(ref cm, "@RecvExpeditionDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 41));
                    Sm.CmParamDt(ref cm, "@ExpiredDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 46));
                    if (mIsRecvExpeditionEnabled) Sm.CmParam<String>(ref cm, "@RejectedInd_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 47) ? "Y" : "N");
                    if (mIsRecvVdHeatNumberEnabled) Sm.CmParam<String>(ref cm, "@HeatNumber_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 49));
                }
            }


            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQL2.ToString();
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@Status", mIsRecvVdNeedApproval ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@DocType", "01");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRecvVdFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            if (DocNo == "Edit") DocNo = Sm.GetValue("Select DocNo From TblRecvVdHdr Where DocNo = @Param; ", TxtDocNo.Text);

            SQL.AppendLine("/* RecvVd - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblRecvVdFile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 2));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveRecvVdDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/* RecvVd - RecvVdDtl */ ");
        //    SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

        //    if (mIsRecvVdBatchNoUseProjectCode)
        //    {
        //        SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, Remark, ExpiredDt, ");
        //        if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
        //        SQL.AppendLine("CreateBy, CreateDt) ");
        //        SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, @Status, @PODocNo, @PODNo, @ItCode, ");
        //        if (Sm.GetGrdStr(Grd1, Row, 33).Length > 0)
        //            SQL.AppendLine("@ProjectCode, ");
        //        else
        //            SQL.AppendLine("IfNull(@BatchNo, '-'), ");
        //        SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), @QtyPurchase, @Qty, @Qty2, @Qty3, @RecvExpeditionDocNo, @RecvExpeditionDNo, @Remark, @ExpiredDt, ");
        //        if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber, ");
        //        SQL.AppendLine("@CreateBy, CurrentDateTime()); ");
        //    }
        //    else
        //    {
        //        if (mIsProjectGroupEnabled)
        //        {
        //            SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, ExpiredDt, Remark, ");
        //            if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
        //            SQL.AppendLine("CreateBy, CreateDt) ");
        //            SQL.AppendLine("Select @DocNo, @DNo, 'N', @CancelReason, @Status, @PODocNo, @PODNo, @ItCode, ");
        //            SQL.AppendLine("Concat(Right(A.DocDt, 2), '/', Substring(A.DocDt, 5, 2), '/', Left(A.DocDt, 4)) As BatchNo, ");
        //            SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), @QtyPurchase, @Qty, @Qty2, @Qty3, @RecvExpeditionDocNo, @RecvExpeditionDNo, @ExpiredDt, @Remark, ");
        //            if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber, ");
        //            SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //            SQL.AppendLine("From TblRecvVdHdr A ");
        //            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
        //            SQL.AppendLine("Where A.DocNo=@DocNo; ");
        //        }
        //        else
        //        {
        //            if (mIsAutoGenerateBatchNo)
        //            {
        //                SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, Remark, ExpiredDt, ");
        //                if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
        //                SQL.AppendLine("CreateBy, CreateDt) ");
        //                SQL.AppendLine("Select @DocNo, @DNo, 'N', @CancelReason, @Status, @PODocNo, @PODNo, @ItCode, ");
        //                if (Sm.GetGrdStr(Grd1, Row, 33).Length > 0)
        //                {
        //                    SQL.AppendLine("@ProjectCode As BatchNo, ");
        //                }
        //                else
        //                {
        //                    if (mRecvVdAutoBatchNoFormat == "2")
        //                        SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo, '-', @DNo) As BatchNo, ");
        //                    else
        //                    {
        //                        if (mRecvVdAutoBatchNoFormat == "3")
        //                        {
        //                            SQL.AppendLine("Concat(Substring(A.DocDt, 3, 2), '/', Substring(A.DocDt, 5, 2), '/', Substring(A.DocDt, 7, 2), '/', Left(A.DocNo, 4), '/', A.VdCode) As BatchNo, ");
        //                        }
        //                        else
        //                        {
        //                            if (mIsAutoGenerateBatchNoEditable)
        //                            {
        //                                if (mIsRecvVdUseOptionBatchNoFormula)
        //                                    SQL.AppendLine("@BatchNo As BatchNo, ");
        //                                else
        //                                    SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo, Case When @BatchNo Is Null Then '' Else Concat('-', @BatchNo) End) As BatchNo, ");
        //                            }
        //                            else
        //                                SQL.AppendLine("Concat(IfNull(B.ShortName, ''), '-', A.DocDt, '-', A.DocNo) As BatchNo, ");
        //                        }
        //                    }
        //                }
        //                SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), @QtyPurchase, @Qty, @Qty2, @Qty3, @RecvExpeditionDocNo, @RecvExpeditionDNo, @Remark, @ExpiredDt, ");
        //                if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber, ");
        //                SQL.AppendLine("@CreateBy, CurrentDateTime() ");
        //                SQL.AppendLine("From TblRecvVdHdr A ");
        //                SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
        //                SQL.AppendLine("Where A.DocNo=@DocNo; ");
        //            }
        //            else
        //            {
        //                SQL.AppendLine("Insert Into TblRecvVdDtl(DocNo, DNo, CancelInd, CancelReason, Status, PODocNo, PODNo, ItCode, BatchNo, Source, Lot, Bin, QtyPurchase, Qty, Qty2, Qty3, RecvExpeditionDocNo, RecvExpeditionDNo, Remark, ExpiredDt, ");
        //                if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("HeatNumber, ");
        //                SQL.AppendLine("CreateBy, CreateDt) ");
        //                SQL.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, @Status, @PODocNo, @PODNo, @ItCode, ");
        //                if (Sm.GetGrdStr(Grd1, Row, 33).Length > 0)
        //                {
        //                    SQL.AppendLine("@ProjectCode, ");
        //                }
        //                else
        //                {
        //                    SQL.AppendLine("IfNull(@BatchNo, ");
        //                    SQL.AppendLine(mIsBatchNoUseDocDtIfEmpty ? "@DocDt" : "'-'");
        //                    SQL.AppendLine("), ");
        //                }
        //                SQL.AppendLine("Concat(@DocType, '*', @DocNo, '*', @DNo), IfNull(@Lot, '-'), IfNull(@Bin, '-'), ");
        //                SQL.AppendLine("@QtyPurchase, @Qty, @Qty2, @Qty3, @RecvExpeditionDocNo, @RecvExpeditionDNo, @Remark, @ExpiredDt, ");
        //                if (mIsRecvVdHeatNumberEnabled) SQL.AppendLine("@HeatNumber, ");
        //                SQL.AppendLine("@CreateBy, CurrentDateTime()); ");
        //            }
        //        }
        //    }
        //    if (mIsRecvVdNeedApproval)
        //    {
        //        SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
        //        SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
        //        SQL.AppendLine("From TblDocApprovalSetting T ");
        //        SQL.AppendLine("Where T.DocType='RecvVd' ");

        //        if (mIsRecvVdApprovalBasedOnWhs)
        //        {
        //            SQL.AppendLine("And T.WhsCode In ( ");
        //            SQL.AppendLine("    Select WhsCode From TblRecvVdHdr Where DocNo=@DocNo ");
        //            SQL.AppendLine(") ");
        //        }

        //        SQL.AppendLine("; ");

        //        SQL.AppendLine("Update TblRecvVdDtl Set Status='A' ");
        //        SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ");
        //        SQL.AppendLine("And Not Exists( ");
        //        SQL.AppendLine("    Select 1 From TblDocApproval ");
        //        SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo And DNo=@DNo ");
        //        SQL.AppendLine("    ); ");
        //    }

        //    if (mIsRecvExpeditionEnabled)
        //    {
        //        SQL.AppendLine("Update TblRecvVdDtl ");
        //        SQL.AppendLine("    Set RejectedInd= @RejectedInd ");
        //        SQL.AppendLine("Where DocNo = @DocNo ");
        //        SQL.AppendLine("And DNo = @DNo ");
        //        SQL.AppendLine("; ");
        //    }

        //    var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@Status", mIsRecvVdNeedApproval?"O":"A");
        //    Sm.CmParam<String>(ref cm, "@DocType", "01");
        //    Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@PODocNo", Sm.GetGrdStr(Grd1, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@PODNo", Sm.GetGrdStr(Grd1, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 8));
        //    if(mIsRecvVdUseOptionBatchNoFormula)
        //        Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 10) : "-");
        //    else
        //        Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
        //    Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
        //    Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPurchase", Sm.GetGrdDec(Grd1, Row, 14));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 16));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 22));
        //    Sm.CmParam<String>(ref cm, "@ProjectCode", Sm.GetGrdStr(Grd1, Row, 33));
        //    Sm.CmParam<String>(ref cm, "@RecvExpeditionDocNo", Sm.GetGrdStr(Grd1, Row, 40));
        //    Sm.CmParam<String>(ref cm, "@RecvExpeditionDNo", Sm.GetGrdStr(Grd1, Row, 41));
        //    Sm.CmParamDt(ref cm, "@ExpiredDt", Sm.GetGrdDate(Grd1, Row, 46));
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    if (mIsRecvExpeditionEnabled) Sm.CmParam<String>(ref cm, "@RejectedInd", Sm.GetGrdBool(Grd1, Row, 47) ? "Y" : "N");
        //    if (mIsRecvVdHeatNumberEnabled) Sm.CmParam<String>(ref cm, "@HeatNumber", Sm.GetGrdStr(Grd1, Row, 49));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* RecvVd - Stock */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("    And B.RejectedInd = 'N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("    And B.RejectedInd = 'N' ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblStockPrice(ItCode, BatchNo, Source, CurCode, UPrice, ExcRate, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.ItCode, A.BatchNo, A.Source, E.CurCode, ");
            //SQL.AppendLine("(A.QtyPurchase/A.Qty)*F.UPrice, ");

            SQL.AppendLine("((A.QtyPurchase/A.Qty)*F.UPrice)-");
            SQL.AppendLine("(((A.QtyPurchase/A.Qty)*F.UPrice)*C.Discount/100)-");
            SQL.AppendLine("((1/A.Qty)*(A.QtyPurchase/C.Qty)*C.DiscountAmt)+");
            SQL.AppendLine("((1/A.Qty)*(A.QtyPurchase/C.Qty)*C.RoundingValue) ");
            SQL.AppendLine("As UPrice, ");

            SQL.AppendLine("Case When E.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            SQL.AppendLine("        Where RateDt<=G.DocDt And CurCode1=E.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("    ), 0.00) End As ExcRate, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl C On A.PODocNo=C.DocNo And A.PODNo=C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On C.PORequestDocNo=D.DocNo And C.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl F On D.QtDocNo=F.DocNo And D.QtDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblRecvVdHdr G On A.DocNo=G.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.Status='A' And A.CancelInd='N' ");
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("    And A.RejectedInd = 'N' ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockDODept(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* RecvVd - Stock DO */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblRecvVdDtl C On A.RecvVdDocNo = C.DocNo And B.DNo = C.DNo And IfNull(C.Status, 'O') = 'A' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary As T1  ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, B.Lot, B.Bin, B.Source ");
            SQL.AppendLine("    From TblDODeptHdr A ");
            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl C On A.RecvVdDocNo = C.DocNo And C.DNo = B.DNo And IfNull(C.Status, 'O') = 'A' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=0.00, T1.Qty2=0.00, T1.Qty3=0.00;");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", "05");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePOProcessInd()
        {
            var SQL = new StringBuilder();

            if (mIsRecvVdSplitOutstandingPO)
            {
                SQL.AppendLine("Update TblPODtl T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
                SQL.AppendLine("    IfNull(B.RecvVdQty, 0)+IfNull(C.POQtyCancelQty, 0)-IfNull(D.DOVdQty, 0) As Qty2  ");
                SQL.AppendLine("    From TblPODtl A ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As RecvVdQty ");
                SQL.AppendLine("        From TblRecvVdDtl ");
                SQL.AppendLine("        Where Locate(Concat('##', PODocNo, '##'), @Param)>0 ");
                SQL.AppendLine("        And CancelInd='N' And IfNull(Status, 'O')<>'C' ");
                if (mIsRecvExpeditionEnabled)
                    SQL.AppendLine("        And RejectedInd = 'N' ");
                SQL.AppendLine("        Group By PODocNo, PODNo ");
                SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As POQtyCancelQty ");
                SQL.AppendLine("        From TblPOQtyCancel ");
                SQL.AppendLine("        Where Locate(Concat('##', PODocNo, '##'), @Param)>0 ");
                SQL.AppendLine("        And CancelInd='N' ");
                SQL.AppendLine("        Group By PODocNo, PODNo ");
                SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
                SQL.AppendLine("    From TblDOVdHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl T3 ");
                SQL.AppendLine("        On T2.RecvVdDocNo=T3.DocNo ");
                SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
                SQL.AppendLine("        And T3.CancelInd='N' And IfNull(T3.Status, 'O')<>'C' ");
                SQL.AppendLine("        And Locate(Concat('##', T3.PODocNo, '##'), @Param)>0 ");
                if (mIsRecvExpeditionEnabled)
                    SQL.AppendLine("        And T3.RejectedInd = 'N' ");
                SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
                SQL.AppendLine("    Group By T3.PODocNo, T3.PODNo ");
                SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
                SQL.AppendLine("    Where Locate(Concat('##', A.DocNo, '##'), @Param)>0 ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
                SQL.AppendLine("Set T1.ProcessInd= ");
                SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
                SQL.AppendLine("    End; ");
            }
            else
            {
                SQL.AppendLine("Update TblPODtl T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, ");
                SQL.AppendLine("    IfNull(B.RecvVdQty, 0)+IfNull(C.POQtyCancelQty, 0)-IfNull(D.DOVdQty, 0) As Qty2  ");
                SQL.AppendLine("    From TblPODtl A ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As RecvVdQty ");
                SQL.AppendLine("        From TblRecvVdDtl ");
                SQL.AppendLine("        Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
                SQL.AppendLine("        And CancelInd='N' And IfNull(Status, 'O')<>'C' ");
                if (mIsRecvExpeditionEnabled)
                    SQL.AppendLine("        And RejectedInd = 'N' ");
                SQL.AppendLine("        Group By PODocNo, PODNo ");
                SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As POQtyCancelQty ");
                SQL.AppendLine("        From TblPOQtyCancel ");
                SQL.AppendLine("        Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
                SQL.AppendLine("        And CancelInd='N' ");
                SQL.AppendLine("        Group By PODocNo, PODNo ");
                SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As DOVdQty ");
                SQL.AppendLine("    From TblDOVdHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl T3 ");
                SQL.AppendLine("        On T2.RecvVdDocNo=T3.DocNo ");
                SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
                SQL.AppendLine("        And T3.CancelInd='N' And IfNull(T3.Status, 'O')<>'C' ");
                if (mIsRecvExpeditionEnabled)
                    SQL.AppendLine("        And T3.RejectedInd = 'N' ");
                SQL.AppendLine("        And Locate(Concat('##', T3.PODocNo, T3.PODNo, '##'), @Param)>0 ");
                SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
                SQL.AppendLine("    Group By T3.PODocNo, T3.PODNo ");
                SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
                SQL.AppendLine("    Where Locate(Concat('##', A.DocNo, A.DNo, '##'), @Param)>0 ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
                SQL.AppendLine("Set T1.ProcessInd= ");
                SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
                SQL.AppendLine("    Else ");
                SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
                SQL.AppendLine("    End; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            if (mIsRecvVdSplitOutstandingPO)
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedSOPO2());
            else
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedPO());
            
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, string DNo, string JournalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* RecvVd - Journal RecvVd */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");
            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvVdDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo=@DNo ");

            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select Distinct B.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Vendor : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, C.CCCode, B.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo = B.DocNo ");
            if (mRecvvdCostCenterJournalFormat=="1")
            {
                SQL.AppendLine("Left Join TblWarehouse C On A.WhsCode = C.WhsCode ");
            }
            else if (mRecvvdCostCenterJournalFormat=="2")
            {
                SQL.AppendLine("LEFT JOIN( ");
                SQL.AppendLine("	SELECT B.DocNo, B.DNo, IF(H.DroppingRequestDocNo IS NULL, C1.CCCode, N.CCCode) AS CCCode ");
                SQL.AppendLine("	FROM tblrecvvdhdr A ");
                SQL.AppendLine("	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblwarehouse C1 ON A.WhsCode=C1.WhsCode ");
                SQL.AppendLine("	LEFT JOIN tblpodtl C2 ON B.PODocNo=C2.DocNo AND B.PODNo=C2.DNo ");
                SQL.AppendLine("	LEFT JOIN tblpohdr D ON C2.DocNo=D.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblporequestdtl E ON C2.PORequestDocNo=E.DocNo AND C2.PORequestDNo=E.DNo ");
                SQL.AppendLine("	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo ");
                SQL.AppendLine("	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo ");
                SQL.AppendLine("	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblboqhdr M ON L.BOQDocNo=M.DocNo ");
                SQL.AppendLine("	LEFT JOIN tbllophdr N ON M.LOPDocNo=N.DocNo ");
                SQL.AppendLine(")C ON C.DocNo=B.DocNo AND C.DNo=B.DNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo And B.DNo=@DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocType From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, A.Qty*B.UPrice*B.ExcRate As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblRecvVdDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DNo=@DNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        0 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
            SQL.AppendLine("        From TblRecvVdHdr A ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And B.DNo=@DNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");

            if (mMenuCodeForPRService.Length > 0)
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblRecvVdHdr B On A.DocNo = B.JournalDocNo And B.DocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.PODocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo = F.DocNo And E.MaterialRequestDNo = F.DNo ");
                SQL.AppendLine("    And F.MaterialRequestServiceDocNo Is Null ");
                SQL.AppendLine("Inner JOin TblMaterialRequestHdr G On E.MaterialRequestDocNo = G.DocNo ");
                SQL.AppendLine("Set A.SOContractDocNo = G.SOCDocNo ");
                SQL.AppendLine("Where G.SOCDocNo Is Not Null ");
                SQL.AppendLine("And B.DocNo = @DocNo ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblRecvVdHdr B On A.DocNo = B.JournalDocNo And B.DocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.PODocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo = F.DocNo And E.MaterialRequestDNo = F.DNo ");
                SQL.AppendLine("    And F.MaterialRequestServiceDocNo Is Not Null ");
                SQL.AppendLine("Inner JOin TblMaterialRequestServiceHdr G On F.MaterialRequestServiceDocNo = G.DocNo ");
                SQL.AppendLine("Set A.SOContractDocNo = G.SOCDocNo ");
                SQL.AppendLine("Where G.SOCDocNo Is Not Null ");
                SQL.AppendLine("And B.DocNo = @DocNo ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);

            return cm;
        }

        private MySqlCommand SaveJournalAutoDO(string DocNo, string DNo, string DODeptDocNo, string Journal)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* RecvVd - Journal DO */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");
            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvVdDtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@RecvVdDocNo And DNo=@DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@RecvVdDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select Distinct B.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Vendor [Auto DO] : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, C.CCCode, B.Remark, ");
            //SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, @CCCode As CCCode, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B on A.Docno = B.DocNo ");
            if (mRecvvdCostCenterJournalFormat=="1")
            {
                if (mCostCenterJournalRecvVdSource == "1")
                    SQL.AppendLine("Left Join TblWarehouse C On A.WhsCode=C.WhsCode ");
                else if (mCostCenterJournalRecvVdSource == "2")
                    SQL.AppendLine("Left Join tbldodepthdr C On A.DocNo = C.RecvVdDocNo ");
            }
            else if (mRecvvdCostCenterJournalFormat=="2")
            {
                SQL.AppendLine("LEFT JOIN( ");
                SQL.AppendLine("	SELECT B.DocNo, B.DNo, IF(H.DroppingRequestDocNo IS NULL, C1.CCCode, N.CCCode) AS CCCode ");
                SQL.AppendLine("	FROM tblrecvvdhdr A ");
                SQL.AppendLine("	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblwarehouse C1 ON A.WhsCode=C1.WhsCode ");
                SQL.AppendLine("	LEFT JOIN tblpodtl C2 ON B.PODocNo=C2.DocNo AND B.PODNo=C2.DNo ");
                SQL.AppendLine("	LEFT JOIN tblpohdr D ON C2.DocNo=D.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblporequestdtl E ON C2.PORequestDocNo=E.DocNo AND C2.PORequestDNo=E.DNo ");
                SQL.AppendLine("	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo ");
                SQL.AppendLine("	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo ");
                SQL.AppendLine("	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo ");
                SQL.AppendLine("	LEFT JOIN tblboqhdr M ON L.BOQDocNo=M.DocNo ");
                SQL.AppendLine("	LEFT JOIN tbllophdr N ON M.LOPDocNo=N.DocNo ");
                SQL.AppendLine(")C ON C.DocNo=B.DocNo AND C.DNo=B.DNo ");
            }
            SQL.AppendLine("Where A.DocNo=@RecvVdDocNo and B.DNo=@DNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocType From TblDocApproval ");
            SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@RecvVdDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select EntCode, AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            //Debit DO
            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo2 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblDODeptHdr A ");
                SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                SQL.AppendLine("    Union All ");
                if (mIsMaterialRequestForDroppingRequestEnabled && mRecvvdJournalFormat == "2")
                {
                    SQL.AppendLine("    SELECT X1.EntCode, IF(X2.DroppingRequestDocNo is null, X1.AcNo, X2.AcNo) as AcNo, SUM(X1.DAmt) as DAmt, X1.CAmt ");
                    SQL.AppendLine("    FROM( ");
                    SQL.AppendLine("    	 SELECT @EntCodeCC As EntCode,  ");
                    SQL.AppendLine("        T.AcNo,  ");
                    SQL.AppendLine("        Sum(T.Amt) As DAmt,  ");
                    SQL.AppendLine("        0.00 As CAmt  ");
                    SQL.AppendLine("        From (  ");
                    SQL.AppendLine("            Select E.AcNo,  ");
                    SQL.AppendLine("            B.Qty*C.UPrice*C.ExcRate As Amt  ");
                    SQL.AppendLine("            From TblDODeptHdr A  ");
                    SQL.AppendLine("            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo  ");
                    SQL.AppendLine("            Inner Join TblStockPrice C On B.Source=C.Source  ");
                    SQL.AppendLine("            Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode  ");
                    SQL.AppendLine("            Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode  ");
                    SQL.AppendLine("            Inner Join TblItem F On C.ItCode=F.ItCode  ");
                    SQL.AppendLine("            Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N'  ");
                    SQL.AppendLine("            Where Find_In_Set(A.DocNo, @DODeptDocNo)  ");
                    SQL.AppendLine("        ) T Group By T.AcNo  ");
                    SQL.AppendLine("    )X1 ");
                    SQL.AppendLine("    LEFT JOIN ( ");
                    SQL.AppendLine("    	SELECT H.DroppingRequestDocNo, IFNULL(CONCAT(M.ParValue, L.ProjectCode2), M.ParValue) AS AcNo ");
                    SQL.AppendLine("    	FROM tblrecvvdhdr A  ");
                    SQL.AppendLine("    	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblpodtl C ON B.PODocNo=C.DocNo AND B.PODNo=C.DNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblpohdr D ON C.DocNo=D.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblporequestdtl E ON C.PORequestDocNo=E.DocNo AND C.PORequestDNo=E.DNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo  ");
                    SQL.AppendLine("    	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo  ");
                    SQL.AppendLine("    	LEFT Join TblParameter M On M.ParCode='DefferedChargesAcNo' AND M.ParValue IS NOT NULL ");
                    SQL.AppendLine("    	WHERE A.DocNo=@RecvVdDocNo and B.DNo=@DNo ");
                    SQL.AppendLine("    )X2 ON 1=1 ");
                    //SQL.AppendLine("    GROUP BY X2.AcNo ");
                }
                else
                {
                    SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }

                //SQL.AppendLine("    Union All ");

                //SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                //SQL.AppendLine("    D.AcNo2 As AcNo, ");
                //SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                //SQL.AppendLine("    0.00 As CAmt ");
                //SQL.AppendLine("    From TblDODeptHdr A ");
                //SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                //SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                //SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                //SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                //SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                //SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                //SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                //SQL.AppendLine("    Union All ");
                //SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                //SQL.AppendLine("    T.AcNo, ");
                //SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                //SQL.AppendLine("    0.00 As CAmt ");
                //SQL.AppendLine("    From ( ");
                //SQL.AppendLine("        Select E.AcNo, ");
                //SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                //SQL.AppendLine("        From TblDODeptHdr A ");
                //SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                //SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                //SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                //SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                //SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                //SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                //SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                //SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                //SQL.AppendLine("    ) T Group By T.AcNo ");
            }
            else
            {
                if (mIsJournalRecvVdAutoDOUseCOAEntity)
                {
                    SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                    SQL.AppendLine("    0.00 As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    //SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                    SQL.AppendLine("    Where A.DocNo=@DODeptDocNo And B.DNo=@DNo ");
                }
                //SQL.AppendLine("    Union All ");
                else
                {
                    if (mIsMaterialRequestForDroppingRequestEnabled && mRecvvdJournalFormat == "2")
                    {
                        SQL.AppendLine("    SELECT X1.EntCode, IF(X2.DroppingRequestDocNo is null, X1.AcNo, X2.AcNo) as AcNo, SUM(X1.DAmt) as DAmt, X1.CAmt ");
                        SQL.AppendLine("    FROM( ");
                        SQL.AppendLine("        Select @EntCodeCC As EntCode, ");
                        SQL.AppendLine("        T.AcNo, ");
                        SQL.AppendLine("        Sum(T.Amt) As DAmt, ");
                        SQL.AppendLine("        0.00 As CAmt ");
                        SQL.AppendLine("        From ( ");
                        SQL.AppendLine("           Select E.AcNo, ");
                        SQL.AppendLine("           B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("           From TblDODeptHdr A ");
                        SQL.AppendLine("           Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("           Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("           Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("           Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                        //SQL.AppendLine("         Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                        SQL.AppendLine("           Where A.DocNo=@DODeptDocNo And B.DNo=@DNo ");
                        SQL.AppendLine("        ) T Group By T.AcNo ");
                        SQL.AppendLine("    )X1 ");
                        SQL.AppendLine("    LEFT JOIN ( ");
                        SQL.AppendLine("    	SELECT H.DroppingRequestDocNo, IFNULL(CONCAT(M.ParValue, L.ProjectCode2), M.ParValue) AS AcNo ");
                        SQL.AppendLine("    	FROM tblrecvvdhdr A  ");
                        SQL.AppendLine("    	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblpodtl C ON B.PODocNo=C.DocNo AND B.PODNo=C.DNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblpohdr D ON C.DocNo=D.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblporequestdtl E ON C.PORequestDocNo=E.DocNo AND C.PORequestDNo=E.DNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo  ");
                        SQL.AppendLine("    	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo  ");
                        SQL.AppendLine("    	LEFT Join TblParameter M On M.ParCode='DefferedChargesAcNo' AND M.ParValue IS NOT NULL ");
                        SQL.AppendLine("    	WHERE A.DocNo=@RecvVdDocNo and B.DNo=@DNo ");
                        SQL.AppendLine("    )X2 ON 1=1 ");
                        //SQL.AppendLine("    GROUP BY X2.AcNo ");
                    }
                    else
                    {
                        SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                        SQL.AppendLine("    T.AcNo, ");
                        SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                        SQL.AppendLine("    0.00 As CAmt ");
                        SQL.AppendLine("    From ( ");
                        SQL.AppendLine("        Select E.AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                        SQL.AppendLine("        From TblDODeptHdr A ");
                        SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                        SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                        //SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                        SQL.AppendLine("        Where A.DocNo=@DODeptDocNo And B.DNo=@DNo ");
                        SQL.AppendLine("    ) T Group By T.AcNo ");
                    }
                }
            }

            //Credit RecvVd
            if (mIsItemCategoryUseCOAAPAR)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCode As EntCode, E.AcNo8 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvVdHdr A ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode = D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode And E.AcNo8 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo and B.DNo=@DNo ");
            }
            else
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select @EntCode As EntCode, Concat(D.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvVdHdr A ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo and B.DNo=@DNo ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo is Not Null ");
            SQL.AppendLine("    Group By EntCode, AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");

            if (mMenuCodeForPRService.Length > 0)
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblRecvVdHdr B On A.DocNo = B.JournalDocNo And B.DocNo = @RecvVdDocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.PODocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo = F.DocNo And E.MaterialRequestDNo = F.DNo ");
                SQL.AppendLine("    And F.MaterialRequestServiceDocNo Is Null ");
                SQL.AppendLine("Inner JOin TblMaterialRequestHdr G On E.MaterialRequestDocNo = G.DocNo ");
                SQL.AppendLine("Set A.SOContractDocNo = G.SOCDocNo ");
                SQL.AppendLine("Where G.SOCDocNo Is Not Null ");
                SQL.AppendLine("And B.DocNo = @RecvVdDocNo ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblRecvVdHdr B On A.DocNo = B.JournalDocNo And B.DocNo = @RecvVdDocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On B.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.PODocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo = F.DocNo And E.MaterialRequestDNo = F.DNo ");
                SQL.AppendLine("    And F.MaterialRequestServiceDocNo Is Not Null ");
                SQL.AppendLine("Inner JOin TblMaterialRequestServiceHdr G On F.MaterialRequestServiceDocNo = G.DocNo ");
                SQL.AppendLine("Set A.SOContractDocNo = G.SOCDocNo ");
                SQL.AppendLine("Where G.SOCDocNo Is Not Null ");
                SQL.AppendLine("And B.DocNo = @RecvVdDocNo ");
                SQL.AppendLine("; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@RecvVdDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DODeptDocNo", DODeptDocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Journal);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            //Sm.StdMsg(mMsgType.Warning, Sm.GetLue(LueCCCode));
            //Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            return cm;
        }

        private MySqlCommand SaveJournalBasedSOC(ref List<JournalBasedSOC> lx, string DODeptDocNo, bool IsCancelled)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string JNDocNo = string.Empty;
            int index = 0;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("/* RecvVd - Journal SOC */ ");
            foreach (var x in lx.OrderBy(o => o.DocNo).ThenBy(o => o.DNo))
            {
                if (JNDocNo.Length == 0)
                {
                    SQL.AppendLine("Set @Row:=0; ");
                    JNDocNo = x.DocNo;
                }

                if (JNDocNo != x.DocNo)
                {
                    SQL.AppendLine("Set @Row := 0; ");
                    JNDocNo = x.DocNo;
                }

                if (IsCancelled)
                {
                    #region Cancel Journal

                    SQL.AppendLine("Update TblRecvVdJournal ");
                    SQL.AppendLine("Set JournalDocNo2 = @DocNo_" + index.ToString() + ", ");
                    SQL.AppendLine("LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
                    SQL.AppendLine("Where RecvVdDocNo = @RecvVdDocNo_" + index.ToString() + " ");
                    SQL.AppendLine("And RecvVdDNo = @RecvVdDNo_" + index.ToString() + "; ");

                    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select @DocNo_" + index.ToString() + ", ");
                    if (IsClosingJournalUseCurrentDt)
                        SQL.AppendLine("Replace(CurDate(), '-', ''), ");
                    else
                        SQL.AppendLine("DocDt, ");
                    SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
                    SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @CreateBy, CurrentDateTime() ");
                    SQL.AppendLine("From TblJournalHdr ");
                    SQL.AppendLine("Where DocNo In ( ");
                    SQL.AppendLine("    Select Distinct JournalDocNo From TblRecvVdJournal Where RecvVdDocNo=@RecvVdDocNo_" + index.ToString() + " ");
                    SQL.AppendLine("    And RecvVdDNo = @RecvVdDNo_" + index.ToString() + " ");
                    SQL.AppendLine(") ");
                    SQL.AppendLine("On Duplicate Key Update CreateBy = @CreateBy ");
                    SQL.AppendLine("; ");

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select @DocNo_" + index.ToString() + ", Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3), ");
                    SQL.AppendLine("B.AcNo, B.DAmt, B.CAmt, @EntCode, @SOContractDocNo_" + index.ToString() + ", A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblJournalHdr A ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                    if (mMenuCodeForRecvVdAutoCreateDO)
                    {
                        //Credit DO
                        if (mIsMovingAvgEnabled)
                        {
                            SQL.AppendLine("    Select ");
                            SQL.AppendLine("    D.AcNo2 As AcNo, ");
                            SQL.AppendLine("    0.00 As DAmt, ");
                            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                            SQL.AppendLine("    From TblDODeptHdr A ");
                            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @RecvVdDocNo_" + index.ToString() + " ");
                            SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                            SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                            SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("    Select ");
                            SQL.AppendLine("    T.AcNo, ");
                            SQL.AppendLine("    0.00 As DAmt, ");
                            SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                            SQL.AppendLine("    From ( ");
                            SQL.AppendLine("        Select E.AcNo, ");
                            SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @RecvVdDocNo_" + index.ToString() + " ");
                            SQL.AppendLine("            And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                            SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                            SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                            SQL.AppendLine("    ) T Group By T.AcNo ");

                            //SQL.AppendLine("    Union All ");

                            //SQL.AppendLine("    Select ");
                            //SQL.AppendLine("    D.AcNo2 As AcNo, ");
                            //SQL.AppendLine("    0.00 As DAmt, ");
                            //SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                            //SQL.AppendLine("    From TblDODeptHdr A ");
                            //SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            //SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @RecvVdDocNo_" + index.ToString() + " ");
                            //SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            //SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                            //SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                            //SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                            //SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                            //SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                            //SQL.AppendLine("    Union All ");
                            //SQL.AppendLine("    Select ");
                            //SQL.AppendLine("    T.AcNo, ");
                            //SQL.AppendLine("    0.00 As DAmt, ");
                            //SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                            //SQL.AppendLine("    From ( ");
                            //SQL.AppendLine("        Select E.AcNo, ");
                            //SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                            //SQL.AppendLine("        From TblDODeptHdr A ");
                            //SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            //SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @RecvVdDocNo_" + index.ToString() + " ");
                            //SQL.AppendLine("            And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            //SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            //SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                            //SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                            //SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                            //SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                            //SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                            //SQL.AppendLine("    ) T Group By T.AcNo ");
                        }
                        else
                        {
                            SQL.AppendLine("    Select ");
                            SQL.AppendLine("    D.AcNo2 As AcNo, ");
                            SQL.AppendLine("    0.00 As DAmt, ");
                            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                            SQL.AppendLine("    From TblDODeptHdr A ");
                            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @RecvVdDocNo_" + index.ToString() + " ");
                            SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("    Select ");
                            SQL.AppendLine("    T.AcNo, ");
                            SQL.AppendLine("    0.00 As DAmt, ");
                            SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                            SQL.AppendLine("    From ( ");
                            SQL.AppendLine("        Select E.AcNo, ");
                            SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                            SQL.AppendLine("        From TblDODeptHdr A ");
                            SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @RecvVdDocNo_" + index.ToString() + " ");
                            SQL.AppendLine("            And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                            SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                            SQL.AppendLine("    ) T Group By T.AcNo ");
                        }

                        //Debit RecvVd
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                        SQL.AppendLine("        From TblRecvVdHdr A ");
                        SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                        SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                    }
                    else
                    {
                        SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty* ");

                        //if (mIsMovingAvgEnabled)
                        //{
                        //    SQL.AppendLine("        Case D.MovingAvgInd ");
                        //    SQL.AppendLine("        When 'Y' Then IfNull(B1.MovingAvgPrice, 0.00) ");
                        //    SQL.AppendLine("        Else B.UPrice End ");
                        //}
                        //else
                            SQL.AppendLine("        B.UPrice ");

                        SQL.AppendLine("        *B.ExcRate As CAmt ");
                        SQL.AppendLine("        From TblRecvVdDtl A ");

                        //if (mIsMovingAvgEnabled)
                        //    SQL.AppendLine("        Left Join TblStockMovement B1 On B1.DocType = '02' And B1.DocNo = A.DocNo And B1.DNo = A.DNo And B1.CancelInd = 'N' ");

                        SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                        SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                        SQL.AppendLine("        And A.DNo = @RecvVdDNo_" + index.ToString() + " ");
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        B.Qty* ");

                        //if (mIsMovingAvgEnabled)
                        //{
                        //    SQL.AppendLine("        Case F.MovingAvgInd ");
                        //    SQL.AppendLine("        When 'Y' Then IfNull(C1.MovingAvgPrice, 0.00) ");
                        //    SQL.AppendLine("        Else C.UPrice End ");
                        //}
                        //else
                            SQL.AppendLine("        C.UPrice ");

                        SQL.AppendLine("        *C.ExcRate As DAmt, 0.00 As CAmt ");
                        SQL.AppendLine("        From TblRecvVdHdr A ");
                        SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                        SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");

                        //if (mIsMovingAvgEnabled)
                        //    SQL.AppendLine("        Left Join TblStockMovement C1 On C1.DocType = '02' And C1.DocNo = A.DocNo And C1.DNo = B.DNo And C1.CancelInd = 'N' ");

                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And D.ParValue Is Not Null ");
                        SQL.AppendLine("        Inner Join TblItem E On B.ItCode = E.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode = F.ItCtCode ");
                        SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                    }

                    SQL.AppendLine("    ) Tbl ");
                    SQL.AppendLine("    Where AcNo is Not Null ");
                    SQL.AppendLine("    Group By AcNo ");
                    SQL.AppendLine(") B On 1=1 ");
                    SQL.AppendLine("Where A.DocNo=@DocNo_" + index.ToString() + ";");

                #endregion
                }
                else
                {
                    #region Insert

                    SQL.AppendLine("Insert Into TblRecvVdJournal(RecvVdDocNo, RecvVdDNo, JournalDocNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@RecvVdDocNo_" + index.ToString() + ", @RecvVdDNo_" + index.ToString() + ", @DocNo_" + index.ToString() + ", @CreateBy, CurrentDateTime()) ");
                    SQL.AppendLine("On Duplicate Key Update CreateBy = @CreateBy ");
                    SQL.AppendLine("; ");

                    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select @DocNo_" + index.ToString() + ", A.DocDt, ");
                    if (mMenuCodeForRecvVdAutoCreateDO)
                        SQL.AppendLine("Concat('Receiving Item From Vendor [Auto DO] : ', A.DocNo) As JnDesc, ");
                    else
                        SQL.AppendLine("Concat('Receiving Item From Vendor : ', A.DocNo) As JnDesc, ");
                    SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, B.CCCode, A.Remark, ");
                    SQL.AppendLine("A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblRecvVdHdr A ");
                    if (mRecvvdCostCenterJournalFormat=="1")
                    {
                        SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode = B.WhsCode ");
                    }
                    else if (mRecvvdCostCenterJournalFormat=="2")
                    {
                        SQL.AppendLine("LEFT JOIN( ");
                        SQL.AppendLine("	SELECT DISTINCT B.DocNo, B.DNo, IF(H.DroppingRequestDocNo IS NULL, C1.CCCode, N.CCCode) AS CCCode ");
                        SQL.AppendLine("	FROM tblrecvvdhdr A ");
                        SQL.AppendLine("	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tblwarehouse C1 ON A.WhsCode=C1.WhsCode ");
                        SQL.AppendLine("	LEFT JOIN tblpodtl C2 ON B.PODocNo=C2.DocNo AND B.PODNo=C2.DNo ");
                        SQL.AppendLine("	LEFT JOIN tblpohdr D ON C2.DocNo=D.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tblporequestdtl E ON C2.PORequestDocNo=E.DocNo AND C2.PORequestDNo=E.DNo ");
                        SQL.AppendLine("	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo ");
                        SQL.AppendLine("	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tblboqhdr M ON L.BOQDocNo=M.DocNo ");
                        SQL.AppendLine("	LEFT JOIN tbllophdr N ON M.LOPDocNo=N.DocNo ");
                        SQL.AppendLine(")B ON B.DocNo=A.DocNo ");
                    }
                    
                    SQL.AppendLine("Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                    SQL.AppendLine("And Not Exists( ");
                    SQL.AppendLine("    Select DocType From TblDocApproval ");
                    SQL.AppendLine("    Where DocType='RecvVd' And DocNo=@DocNo ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine("On Duplicate Key Update CreateDt = CurrentDateTime(); ");

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select @DocNo_" + index.ToString() + ", Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3), ");
                    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, @SOContractDocNo_" + index.ToString() + ", A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblJournalHdr A ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                    if (mMenuCodeForRecvVdAutoCreateDO)
                    {
                        //Debit DO
                        if (mIsMovingAvgEnabled)
                        {
                            SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                            SQL.AppendLine("    D.AcNo2 As AcNo, ");
                            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                            SQL.AppendLine("    0.00 As CAmt ");
                            SQL.AppendLine("    From TblDODeptHdr A ");
                            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                            SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                            SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                            SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                            SQL.AppendLine("    And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            SQL.AppendLine("    Union All ");
                            if (mIsMaterialRequestForDroppingRequestEnabled && mRecvvdJournalFormat == "2")
                            {
                                SQL.AppendLine("    SELECT X1.EntCode, IF(X2.DroppingRequestDocNo is null, X1.AcNo, X2.AcNo) as AcNo, SUM(X1.DAmt) as DAmt, X1.CAmt ");
                                SQL.AppendLine("    FROM( ");
                                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                                SQL.AppendLine("    T.AcNo, ");
                                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                                SQL.AppendLine("    0.00 As CAmt ");
                                SQL.AppendLine("    From ( ");
                                SQL.AppendLine("        Select E.AcNo, ");
                                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                                SQL.AppendLine("        From TblDODeptHdr A ");
                                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                                SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                                SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                                SQL.AppendLine("    ) T Group By T.AcNo ");
                                SQL.AppendLine("    )X1 ");
                                SQL.AppendLine("    LEFT JOIN ( ");
                                SQL.AppendLine("    	SELECT H.DroppingRequestDocNo, IFNULL(CONCAT(M.ParValue, L.ProjectCode2), M.ParValue) AS AcNo ");
                                SQL.AppendLine("    	FROM tblrecvvdhdr A  ");
                                SQL.AppendLine("    	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblpodtl C ON B.PODocNo=C.DocNo AND B.PODNo=C.DNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblpohdr D ON C.DocNo=D.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblporequestdtl E ON C.PORequestDocNo=E.DocNo AND C.PORequestDNo=E.DNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo  ");
                                SQL.AppendLine("    	LEFT Join TblParameter M On M.ParCode='DefferedChargesAcNo' AND M.ParValue IS NOT NULL ");
                                SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                                SQL.AppendLine("        ANd B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                                SQL.AppendLine("    )X2 ON 1=1 ");
                                SQL.AppendLine("    GROUP BY X2.AcNo ");
                            }
                            else
                            {
                                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                                SQL.AppendLine("    T.AcNo, ");
                                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                                SQL.AppendLine("    0.00 As CAmt ");
                                SQL.AppendLine("    From ( ");
                                SQL.AppendLine("        Select E.AcNo, ");
                                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                                SQL.AppendLine("        From TblDODeptHdr A ");
                                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                                SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                                SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                                SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                                SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                                SQL.AppendLine("    ) T Group By T.AcNo ");
                            }
                        }
                        else
                        {
                            SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                            SQL.AppendLine("    D.AcNo2 As AcNo, ");
                            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                            SQL.AppendLine("    0.00 As CAmt ");
                            SQL.AppendLine("    From TblDODeptHdr A ");
                            SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                            SQL.AppendLine("    Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                            SQL.AppendLine("    And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                            SQL.AppendLine("    Union All ");
                            if (mIsMaterialRequestForDroppingRequestEnabled && mRecvvdJournalFormat == "2")
                            {
                                SQL.AppendLine("    SELECT X1.EntCode, IF(X2.DroppingRequestDocNo is null, X1.AcNo, X2.AcNo) as AcNo, SUM(X1.DAmt) as DAmt, X1.CAmt ");
                                SQL.AppendLine("    FROM( ");
                                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                                SQL.AppendLine("    T.AcNo, ");
                                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                                SQL.AppendLine("    0.00 As CAmt ");
                                SQL.AppendLine("    From ( ");
                                SQL.AppendLine("        Select E.AcNo, ");
                                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                                SQL.AppendLine("        From TblDODeptHdr A ");
                                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                                SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                                SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                                SQL.AppendLine("    ) T Group By T.AcNo ");
                                SQL.AppendLine("    )X1 ");
                                SQL.AppendLine("    LEFT JOIN ( ");
                                SQL.AppendLine("    	SELECT H.DroppingRequestDocNo, IFNULL(CONCAT(M.ParValue, L.ProjectCode2), M.ParValue) AS AcNo ");
                                SQL.AppendLine("    	FROM tblrecvvdhdr A  ");
                                SQL.AppendLine("    	INNER JOIN tblrecvvddtl B ON A.DocNo=B.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblpodtl C ON B.PODocNo=C.DocNo AND B.PODNo=C.DNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblpohdr D ON C.DocNo=D.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblporequestdtl E ON C.PORequestDocNo=E.DocNo AND C.PORequestDNo=E.DNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblporequesthdr F ON E.DocNo=F.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblmaterialrequestdtl G ON E.MaterialRequestDocNo=G.DocNo AND E.MaterialRequestDNo=G.DNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblmaterialrequesthdr H ON G.DocNo=H.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tbldroppingrequesthdr I ON H.DroppingRequestDocNo=I.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblprojectimplementationhdr J ON I.PRJIDocNo=J.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblsocontractrevisionhdr K ON J.SOContractDocNo=K.DocNo  ");
                                SQL.AppendLine("    	LEFT JOIN tblsocontracthdr L ON K.SOCDocNo=L.DocNo  ");
                                SQL.AppendLine("    	LEFT Join TblParameter M On M.ParCode='DefferedChargesAcNo' AND M.ParValue IS NOT NULL ");
                                SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                                SQL.AppendLine("        ANd B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                                SQL.AppendLine("    )X2 ON 1=1 ");
                                SQL.AppendLine("    GROUP BY X2.AcNo ");
                            }
                            else
                            {
                                SQL.AppendLine("    Select @EntCodeCC As EntCode, ");
                                SQL.AppendLine("    T.AcNo, ");
                                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                                SQL.AppendLine("    0.00 As CAmt ");
                                SQL.AppendLine("    From ( ");
                                SQL.AppendLine("        Select E.AcNo, ");
                                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                                SQL.AppendLine("        From TblDODeptHdr A ");
                                SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                                SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                                SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                                SQL.AppendLine("        Where Find_In_Set(A.DocNo, @DODeptDocNo) ");
                                SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                                SQL.AppendLine("    ) T Group By T.AcNo ");
                            }
                        }

                        //Credit RecvVd
                        if (mIsItemCategoryUseCOAAPAR)
                        {
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("        Select @EntCode As EntCode, E.AcNo8 As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                            SQL.AppendLine("        From TblRecvVdHdr A ");
                            SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblItem D On B.ItCode = D.ItCode ");
                            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode And E.AcNo8 Is Not Null ");
                            SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                            SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                        }
                        else
                        {
                            SQL.AppendLine("    Union All ");
                            SQL.AppendLine("        Select @EntCode As EntCode, Concat(D.ParValue, A.VdCode) As AcNo, ");
                            SQL.AppendLine("        0.00 As DAmt, B.Qty*C.UPrice*C.ExcRate As CAmt ");
                            SQL.AppendLine("        From TblRecvVdHdr A ");
                            SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                            SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                            SQL.AppendLine("        And B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                        }
                    }
                    else
                    {
                        SQL.AppendLine("        Select D.AcNo, A.Qty* ");
                        SQL.AppendLine("        B.UPrice ");
                        SQL.AppendLine("        *B.ExcRate As DAmt, 0.00 As CAmt ");
                        SQL.AppendLine("        From TblRecvVdDtl A ");
                        SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                        SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                        SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                        SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                        SQL.AppendLine("        And A.DNo = @RecvVdDNo_" + index.ToString() + " ");
                        SQL.AppendLine("    Union All ");
                        SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                        SQL.AppendLine("        0 As DAmt, B.Qty* ");
                        SQL.AppendLine("        C.UPrice ");
                        SQL.AppendLine("        *C.ExcRate As CAmt ");
                        SQL.AppendLine("        From TblRecvVdHdr A ");
                        SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");

                        //if (mIsMovingAvgEnabled)
                        //    SQL.AppendLine("        Left Join TblStockMovement C1 On C1.DocType = '01' And C1.DocNo = A.DocNo And C1.DNo = B.DNo And C1.CancelInd = 'N' ");

                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                        SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                        SQL.AppendLine("        Where A.DocNo=@RecvVdDocNo_" + index.ToString() + " ");
                        SQL.AppendLine("        ANd B.DNo = @RecvVdDNo_" + index.ToString() + " ");
                    }
                    SQL.AppendLine("    ) Tbl ");
                    SQL.AppendLine("    Where AcNo is Not Null ");
                    SQL.AppendLine("    Group By AcNo ");
                    SQL.AppendLine(") B On 1=1 ");
                    SQL.AppendLine("Where A.DocNo=@DocNo_" + index.ToString() + "; ");

                    #endregion
                }

                Sm.CmParam<String>(ref cm, "@DocNo_" + index.ToString(), x.DocNo);
                Sm.CmParam<String>(ref cm, "@DNo_" + index.ToString(), x.DNo);
                Sm.CmParam<String>(ref cm, "@RecvVdDocNo_" + index.ToString(), x.RecvVdDocNo);
                Sm.CmParam<String>(ref cm, "@RecvVdDNo_" + index.ToString(), x.RecvVdDNo);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo_" + index.ToString(), x.SOCDocNo);

                index += 1;
            }

            Sm.CmParam<String>(ref cm, "@DODeptDocNo", DODeptDocNo);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeCC", Sm.GetEntityCostCenter(Sm.GetLue(LueCCCode)));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);

             cm.CommandText = SQL.ToString();

            return cm;
        }

        private MySqlCommand SaveDODept2(ref List<DODept2Hdr> l, ref List<DODept2Dtl> l2)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true, IsFirstOrExisted2 = true;

            SQL.AppendLine("/* RecvVd - DODept */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int i = 0; i < l.Count; ++i)
            {
                if (IsFirstOrExisted)
                {
                    SQL.AppendLine("Insert Into TblDODeptHdr ");
                    SQL.AppendLine("(DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, EntCode, CCCode, RequestByDocNo, EmpCode, RecvVdDocNo, LOPDocNo, Remark, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");

                    IsFirstOrExisted = false;
                }
                else
                    SQL.AppendLine(", ");

                SQL.AppendLine(
                    "(@DODept2DocNo_"+ i.ToString() +
                    ",  @DocDt_" + i.ToString() +
                    ",  @LocalDocNo_" + i.ToString() +
                    ",  @WhsCode_" + i.ToString() +
                    ",  @DeptCode_" + i.ToString() +
                    ",  @EntCode_" + i.ToString() +
                    ",  @CCCode_" + i.ToString() +
                    ",  Null, @EmpCode_" + i.ToString() +
                    ",  @RecvVdDocNo_" + i.ToString() +
                    ",  @LOPDocNo_" + i.ToString() +
                    ",  @Remark_" + i.ToString() + 
                    ",  @UserCode, @Dt) ");

                
                Sm.CmParam<String>(ref cm, "@DODept2DocNo_" + i.ToString(), l[i].DocNo);
                Sm.CmParamDt(ref cm, "@DocDt_" + i.ToString(), l[i].DocDt);
                Sm.CmParam<String>(ref cm, "@LocalDocNo_" + i.ToString(), l[i].LocalDocNo);
                Sm.CmParam<String>(ref cm, "@WhsCode_" + i.ToString(), l[i].WhsCode);
                Sm.CmParam<String>(ref cm, "@DeptCode_" + i.ToString(), l[i].DeptCode);
                Sm.CmParam<String>(ref cm, "@EntCode_" + i.ToString(), l[i].EntCode);
                Sm.CmParam<String>(ref cm, "@CCCode_" + i.ToString(), l[i].CCCode);
                Sm.CmParam<String>(ref cm, "@EmpCode_" + i.ToString(), Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Remark_" + i.ToString(), l[i].Remark);
                Sm.CmParam<String>(ref cm, "@RecvVdDocNo_" + i.ToString(), l[i].RecvVdDocNo);
                Sm.CmParam<String>(ref cm, "@LOPDocNo_" + i.ToString(), l[i].LOPDocNo);

                for (int j = 0; j < l2.Count; ++j)
                {
                    if (Sm.CompareStr(l[i].DocNo, l2[j].DocNo))
                    {
                        if (IsFirstOrExisted2)
                        {
                            SQL2.AppendLine("Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, RequestByDocNo, EmpCode, AssetCode, Remark, CreateBy, CreateDt) ");
                            SQL2.AppendLine("Values ");

                            IsFirstOrExisted2 = false;
                        }
                        else
                            SQL2.AppendLine(", ");

                        SQL2.AppendLine(
                            "(@DODept2DocNo__" + j.ToString() +
                            ", @DNo__" + j.ToString() +
                            ", 'N', @ItCode__" + j.ToString() +
                            ", 'N', @PropCode__" + j.ToString() +
                            ", (Select BatchNo From TblRecvVdDtl Where Source=@Source__" + j.ToString() + " Limit 1) " +
                            ", @Source__" + j.ToString() +
                            ", @Lot__" + j.ToString() +
                            ", @Bin__" + j.ToString() +
                            ", @Qty__" + j.ToString() +
                            ", @Qty2__" + j.ToString() +
                            ", @Qty3__" + j.ToString() +
                            ", Null, Null, Null, @Remark__" + j.ToString() +
                            ", @UserCode, @Dt) ");

                        Sm.CmParam<String>(ref cm, "@DODept2DocNo__" + j.ToString(), l2[j].DocNo);
                        Sm.CmParam<String>(ref cm, "@DNo__" + j.ToString(), l2[j].DNo);
                        Sm.CmParam<String>(ref cm, "@ItCode__" + j.ToString(), l2[j].ItCode);
                        Sm.CmParam<String>(ref cm, "@PropCode__" + j.ToString(), l2[j].PropCode);
                        Sm.CmParam<String>(ref cm, "@Source__" + j.ToString(), l2[j].Source);
                        Sm.CmParam<String>(ref cm, "@Lot__" + j.ToString(), l2[j].Lot);
                        Sm.CmParam<String>(ref cm, "@Bin__" + j.ToString(), l2[j].Bin);
                        Sm.CmParam<Decimal>(ref cm, "@Qty__" + j.ToString(), l2[j].Qty);
                        Sm.CmParam<Decimal>(ref cm, "@Qty2__" + j.ToString(), l2[j].Qty2);
                        Sm.CmParam<Decimal>(ref cm, "@Qty3__" + j.ToString(), l2[j].Qty3);
                        Sm.CmParam<String>(ref cm, "@Remark__" + j.ToString(), l2[j].Remark);
                    }
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");
            if (!IsFirstOrExisted2) SQL2.AppendLine("; ");

            cm.CommandText = SQL.ToString() + SQL2.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }


        //private MySqlCommand SaveDODept2Hdr(ref List<DODept2Hdr> l, int x)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/* RecvVd - DODeptHdr */ ");
        //    SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

        //    SQL.AppendLine("Insert Into TblDODeptHdr(DocNo, DocDt, LocalDocNo, WhsCode, DeptCode, EntCode, CCCode, RequestByDocNo, EmpCode, RecvVdDocNo, LOPDocNo, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DODept2DocNo, @DocDt, @LocalDocNo, @WhsCode, @DeptCode, @EntCode, @CCCode, Null, @EmpCode, @RecvVdDocNo, @LOPDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DODept2DocNo", l[x].DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", l[x].DocDt);
        //    Sm.CmParam<String>(ref cm, "@LocalDocNo", l[x].LocalDocNo);
        //    Sm.CmParam<String>(ref cm, "@WhsCode", l[x].WhsCode);
        //    Sm.CmParam<String>(ref cm, "@DeptCode", l[x].DeptCode);
        //    Sm.CmParam<String>(ref cm, "@EntCode", l[x].EntCode);
        //    Sm.CmParam<String>(ref cm, "@CCCode", l[x].CCCode);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Gv.CurrentUserCode);
        //    Sm.CmParam<String>(ref cm, "@Remark", l[x].Remark);
        //    Sm.CmParam<String>(ref cm, "@RecvVdDocNo", l[x].RecvVdDocNo);
        //    Sm.CmParam<String>(ref cm, "@LOPDocNo", l[x].LOPDocNo);
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    return cm;
        //}

        private MySqlCommand SaveDODept2Dtl(ref List<DODept2Dtl> l, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* RecvVd - DODeptDtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblDODeptDtl(DocNo, DNo, CancelInd, ItCode, ReplacementInd, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, RequestByDocNo, EmpCode, AssetCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DODept2DocNo, @DNo, 'N', @ItCode, 'N', @PropCode, (Select BatchNo From TblRecvVdDtl Where Source=@Source Limit 1), @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, Null, Null, Null, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DODept2DocNo", l[Row].DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", l[Row].DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", l[Row].ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", l[Row].PropCode);
            Sm.CmParam<String>(ref cm, "@Source", l[Row].Source);
            Sm.CmParam<String>(ref cm, "@Lot", l[Row].Lot);
            Sm.CmParam<String>(ref cm, "@Bin", l[Row].Bin);
            Sm.CmParam<Decimal>(ref cm, "@Qty", l[Row].Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", l[Row].Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", l[Row].Qty3);
            Sm.CmParam<String>(ref cm, "@Remark", l[Row].Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateRecvVdFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateRecvVdFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateRecvVdFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateRecvVdFile4(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName4=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateRecvVdFile5(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName5=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateRecvVdFile6(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdHdr Set ");
            SQL.AppendLine("    FileName6=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid(DNo)) return;

            if (mIsRecvVdJournalBasedOnSOContract) GetSOContractDocNo();

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 5).Length > 0)
                    cml.Add(EditRecvVdDtl(Row));
            }

            if (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2") cml.Add(SaveRecvVdFile("Edit"));


            cml.Add(UpdatePOProcessInd());
            if (mIsRecvExpeditionEnabled) cml.Add(SaveRecvExpedition());

            if (mIsAutoJournalActived)
            {
                if (mIsRecvVdJournalBasedOnSOContract)
                {
                    var lx = new List<JournalBasedSOC>();

                    PrepData(ref lx, TxtDocNo.Text, true);
                    if (lx.Count > 0)
                    {
                        GetJournalDNo(ref lx);
                        GetJournalDocNo(ref lx);

                        cml.Add(SaveJournalBasedSOC(ref lx, string.Empty, true));
                    }

                    lx.Clear();
                }
                else
                {
                    int x = 1;
                    for (int r=0; r<Grd1.Rows.Count-1; r++)
                    {
                        var dno = Sm.GetGrdStr(Grd1, r, 0);
                        if ((Sm.GetGrdBool(Grd1, r, 1) == true) && (Sm.IsDataExist("Select 1 from tblrecvvddtl where docno=@param1 and dno=@param2 and journaldocno2 is null ", TxtDocNo.Text, dno, string.Empty)))
                        {
                            string journaldocno = string.Empty;
                            var DocDt = Sm.GetDte(DteDocDt);
                            var CurrentDt = Sm.ServerCurrentDate();
                            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
                            if (IsClosingJournalUseCurrentDt)
                            {
                                if (mJournalDocNoFormat == "1") //Default
                                {
                                    journaldocno = Sm.GetNewJournalDocNo(CurrentDt, x);
                                }
                                else if (mJournalDocNoFormat == "2") //PHT
                                {
                                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd", string.Empty, string.Empty, mJournalDocNoFormat);
                                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                                    journaldocno = Sm.GetNewJournalDocNoWithAddCodes(CurrentDt, x, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty);
                                }
                            }
                            else
                            {
                                if (mJournalDocNoFormat == "1") //Default
                                {
                                    journaldocno = Sm.GetNewJournalDocNo(DocDt, x);
                                }
                                    else if (mJournalDocNoFormat == "2") //PHT
                                {
                                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd", string.Empty, string.Empty, mJournalDocNoFormat);
                                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                                    journaldocno = Sm.GetNewJournalDocNoWithAddCodes(DocDt, x, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty);
                                }
                            }
                            cml.Add(SaveJournal2(r, Sm.GetValue(journaldocno)));
                            x++;
                        }
                    }
                }
            }
                //if (mIsRecvVdNeedApproval)
                //{
                //    int i = 0;
                //    for (int r = 0; r < Grd1.Rows.Count; r++)
                //    {
                //        if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 5).Length > 0)
                //        {
                //            i++;
                //            cml.Add(SaveJournal2(r, i));
                //        }
                //    }
                //}
                //else
                //    cml.Add(SaveJournal2());
            

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0)
                {
                    if (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2" && Sm.GetGrdStr(Grd3, Row, 2).Length > 0 && Sm.GetGrdStr(Grd3, Row, 2) != "openFileDialog1")
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 2) != Sm.GetGrdStr(Grd3, Row, 7))
                        {
                            UploadFileDtl(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd3, Row, 2));
                        }
                    }
                }
            }

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = "Select DNo, CancelInd From TblRecvVdDtl Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (mIsClosingJournalBasedOnMultiProfitCenter ? 
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) : 
                    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                IsCancelledRecvVdNotExisted(DNo) ||
                IsPurchaseInvoiceExisted(DNo) ||
                IsStockNotValid(DNo)||
                IsDOUsedByAnotherTransaction(DNo) ||
                IsCancelReasonEmpty()||
                (mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalCancelNotValid());
        }

        private bool IsDOUsedByAnotherTransaction(string DNo)
        {
            if (!mMenuCodeForRecvVdAutoCreateDO) return false;

            var SQL = new StringBuilder();
            string DODeptDocNo = Sm.GetValue("Select DocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null And RecvVdDocNo = @Param Limit 1; ", TxtDocNo.Text);

            SQL.AppendLine("Select B.ItCode, C.ItName, B.BatchNo, B.Source, B.Lot, B.Bin ");
            SQL.AppendLine("From TblRecvDeptDtl A, TblDODeptDtl B, TblItem C ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DODeptDocNo=B.DocNo ");
            SQL.AppendLine("And A.DODeptDNo=B.DNo ");
            SQL.AppendLine("And B.ItCode=C.ItCode ");
            SQL.AppendLine("And A.DODeptDocNo=@DODeptDocNo ");
            SQL.AppendLine("And Position(Concat('##', A.DODeptDNo, '##') In @DNo)>0 ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DODeptDocNo", DODeptDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "BatchNo", "Source", "Lot", "Bin" 
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.StdMsg(mMsgType.Warning,
                             "Item's Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                             "Item's Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                             "Batch# : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                             "Source : " + Sm.DrStr(dr, 3) + Environment.NewLine +
                             "Lot : " + Sm.DrStr(dr, 4) + Environment.NewLine +
                             "Bin : " + Sm.DrStr(dr, 5) + Environment.NewLine + Environment.NewLine +
                             "Department already returned this item.");
                        return true;
                    }
                }
                dr.Close();
            }
            return false;
        }

        private bool IsCancelReasonEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                        "Foreign Name : " + Sm.GetGrdStr(Grd1, Row, 30) + Environment.NewLine +
                        "Cancel reason still empty.");
                    Sm.FocusGrd(Grd1, Row, 3);
                    return true;
                }
            }
            return false;
        }

        private bool IsCancelledRecvVdNotExisted(string DNo)
        {
            if (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2" && !mIsApprove) return false;
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsPurchaseInvoiceExisted(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, C.Lot, C.Bin, C.ItCode, D.ItName, D.ForeignName, C.BatchNo ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.RecvVdDocNo=@DocNo ");
            SQL.AppendLine("    And Position(Concat('##', B.RecvVdDNo, '##') In @DNo)>0 ");
            SQL.AppendLine("Inner Join TblRecvVdDtl C On B.RecvVdDocNo=C.DocNo And B.RecvVdDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "DocNo", 

                    //1-5
                    "Lot", "Bin", "ItCode", "ItName", "BatchNo",

                    //6
                    "ForeignName"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.StdMsg(mMsgType.Warning,
                             "Purchase Invoice Document Number : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                             "Lot : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                             "Bin : " + Sm.DrStr(dr, 2) + Environment.NewLine + 
                             "Item Code : " + Sm.DrStr(dr, 3) + Environment.NewLine +
                             "Item Name : " + Sm.DrStr(dr, 4) + Environment.NewLine +
                             "Batch Number : " + Sm.DrStr(dr, 5) + Environment.NewLine + Environment.NewLine +
                             "This received item has been processed into Purchase Invoice.");
                        return true;
                    }
                }
                dr.Close();
            }
            return false;
        }

        private bool IsStockNotValid(string DNo)
        {
            if (mMenuCodeForRecvVdAutoCreateDO) return false;

            StringBuilder SQL = new StringBuilder(), Msg = new StringBuilder();

            SQL.AppendLine("Select B.Lot, B.Bin, B.ItCode, C.ItName, B.BatchNo, ");
            SQL.AppendLine("IfNull(B.Qty, 0) As Qty, IfNull(B.Qty2, 0) As Qty2, IfNull(B.Qty3, 0) As Qty3, ");
            SQL.AppendLine("IfNull(D.Stock, 0) As Stock, IfNull(D.Stock2, 0) As Stock2, IfNull(D.Stock3, 0) As Stock3, C.ForeignName ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Position(Concat('##', B.DNo, '##') In @DNo)>0 ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode And C.FixedItemInd='N' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select D1.WhsCode, D1.Lot, D1.Bin, D1.Source, ");
            SQL.AppendLine("    Sum(D1.Qty) As Stock, Sum(D1.Qty2) As Stock2, Sum(D1.Qty3) As Stock3 ");
            SQL.AppendLine("    From TblStockSummary D1 ");
            SQL.AppendLine("    Inner Join TblRecvVdHdr D2 On D2.DocNo=@DocNo And D1.WhsCode=D2.WhsCode ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl D3 ");
            SQL.AppendLine("        On D2.DocNo=D3.DocNo ");
            SQL.AppendLine("        And Position(Concat('##', D3.DNo, '##') In @DNo)>0 ");
            SQL.AppendLine("        And D1.Lot=D3.Lot ");
            SQL.AppendLine("        And D1.Bin=D3.Bin ");
            SQL.AppendLine("        And D1.Source=D3.Source ");
            SQL.AppendLine("    Group By D1.WhsCode, D1.Lot, D1.Bin, D1.Source ");
            SQL.AppendLine(") D ");
            SQL.AppendLine("    On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("    And B.Lot=D.Lot ");
            SQL.AppendLine("    And B.Bin=D.Bin ");
            SQL.AppendLine("    And B.Source=D.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And (B.Qty>IfNull(D.Stock, 0.00) ");
            if (mNumberOfInventoryUomCode == 2)
                SQL.AppendLine(" Or B.Qty2>IfNull(D.Stock2, 0.00) ");
            if (mNumberOfInventoryUomCode == 3)
                SQL.AppendLine(" Or B.Qty2>IfNull(D.Stock2, 0.00) Or B.Qty3>IfNull(D.Stock3, 0.00) ");

            SQL.AppendLine(") Limit 1;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "Lot", 

                    //1-5
                    "Bin", "ItCode", "ItName", "BatchNo", "Qty",

                    //6-10
                    "Qty2", "Qty3", "Stock", "Stock2", "Stock3",

                    //11
                    "ForeignName"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Msg.AppendLine("Lot : " + Sm.DrStr(dr, 0));
                        Msg.AppendLine("Bin : " + Sm.DrStr(dr, 1));
                        Msg.AppendLine("Item's Code : " + Sm.DrStr(dr, 2));
                        Msg.AppendLine("Item's Name : " + Sm.DrStr(dr, 3));
                        
                        if(mIsShowForeignName)
                            Msg.AppendLine("Foreign Name : " + Sm.DrStr(dr, 11));

                        Msg.AppendLine("Batch# : " + Sm.DrStr(dr, 4));
                        Msg.AppendLine("Received Quantity : " + Sm.FormatNum(Sm.DrDec(dr, 5), 0) + 
                            " ( Stock : " + Sm.FormatNum(Sm.DrDec(dr, 8), 0) + " )");

                        if (mNumberOfInventoryUomCode == 3)
                        {
                            Msg.AppendLine("Received Quantity 2: " + Sm.FormatNum(Sm.DrDec(dr, 6), 0) + " ( Stock : " + Sm.FormatNum(Sm.DrDec(dr, 9), 0) + " )");
                        }

                        if (mNumberOfInventoryUomCode == 3)
                        {
                            Msg.AppendLine("Received Quantity 2: " + Sm.FormatNum(Sm.DrDec(dr, 6), 0) + " ( Stock : " + Sm.FormatNum(Sm.DrDec(dr, 9), 0) + " )");
                            Msg.AppendLine("Received Quantity 3 : " + Sm.FormatNum(Sm.DrDec(dr, 7), 0) + " ( Stock : " + Sm.FormatNum(Sm.DrDec(dr, 10), 0) + " )");
                        }
                        Msg.AppendLine(Environment.NewLine + "Insufficient stock to process the cancellation.");

                        Sm.StdMsg(mMsgType.Warning, Msg.ToString());
                        return true;
                    }
                }
                dr.Close();
            }
            return false;
        }

        private bool IsCOAJournalCancelNotValid()
        {
            var l = new List<COA>();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            var cm = new MySqlCommand();
            string mItCode = string.Empty;
            string cek = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 8).Length > 0)
                {
                    if (mItCode.Length > 0) mItCode += " Or ";
                    mItCode += " (A.ItCode=@ItCode00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                }
            }

            if (mMenuCodeForRecvVdAutoCreateDO)
            {
                //SQL.AppendLine("Select AcNo2 as AcNo From TblEntity Where EntCode=@EntCodeWhs ");
                //SQL.AppendLine("Union All ");
                SQL.AppendLine("Select D.AcNo as AcNo ");
                SQL.AppendLine("From TblDoDeptHdr A ");
                SQL.AppendLine("Inner Join TblDoDeptDtl B On A.DocNo = B.DocNo And A.RecvVdDocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblItemCostCategory C On C.ItCode = B.ItCode And A.CCCode = C.CCCode ");
                SQL.AppendLine("Inner Join TblCostCategory D On C.CCtCode = D.CCtCOde And C.CCCode = D.CCCode ");

                //Debit RecvVd
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select ParValue As AcNo From TblParameter Where Parcode= 'VendorAcNoUnInvoiceAP' ");
            }
            else
            {
                SQL.AppendLine("Select B.AcNo As AcNo ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                SQL.AppendLine("Where "+mItCode);
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select ParValue As AcNo From TblParameter Where Parcode= 'VendorAcNoUnInvoiceAP' ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();


                Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COA()
                        {
                            AcNo = Sm.DrStr(dr, c[0])

                        });
                    }
                }
                dr.Close();
            }

            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
            {
                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                return true;
            }


            return false;
        }

        private MySqlCommand EditRecvVdDtl(int Row)
        {
            var SQL = new StringBuilder();
            string DODeptDocNo = Sm.GetValue("Select DocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null And RecvVdDocNo = @Param Limit 1; ", TxtDocNo.Text);

            SQL.AppendLine("Update TblRecvVdDtl Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason,  LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N'; ");

            if (mMenuCodeForRecvVdAutoCreateDO)
            {
                SQL.AppendLine("Update TblDODeptDtl Set ");
                SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DODeptDocNo ");
                SQL.AppendLine("And DNo=@DNo ");
                SQL.AppendLine("And CancelInd='N'; ");

                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DODeptDocType, A.DocNo, B.DNo, 'Y', A.DocDt, ");
                SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
                SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, @CancelReason, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDODeptHdr A ");
                SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
                SQL.AppendLine("Where A.DocNo=@DODeptDocNo; ");

                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source;");
            }

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'Y', A.DocDt, ");
            SQL.AppendLine("A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, B.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("        And B.RejectedInd = 'N' ");

            //tambahan validasi untuk cancel item yg merupakan fixed asset
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType And Source=B.Source Limit 1 ");
            SQL.AppendLine("        ) ");

            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin And Source=@Source;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DODeptDocNo", DODeptDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@DODeptDocType", "05");
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2(int row, string JournalDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblRecvVdDtl A Set ");
            SQL.AppendLine("    A.JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo and A.DNo=@DNo ");
            SQL.AppendLine(";");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select Distinct @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('Cancelling Receiving Item From Vendor : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, C.CCCode, B.Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B on A.DocNo=B.DocNo ");
            if (mCostCenterJournalRecvVdSource == "1")
                SQL.AppendLine("Left Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            else if (mCostCenterJournalRecvVdSource == "2")
                SQL.AppendLine("Left Join tbldodepthdr C On A.DocNo = C.RecvVdDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo and B.DNo=@DNo; "); 

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mMenuCodeForRecvVdAutoCreateDO)
            {
                //Credit DO
                if (mIsMovingAvgEnabled)
                {
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='N' ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");

                    SQL.AppendLine("    Union All ");

                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                    SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                    SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*IfNull(H.MovingAvgPrice, 0.00) As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("        Inner Join TblItem F On C.ItCode=F.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory G On F.ItCtCode=G.ItCtCode And G.MovingAvgInd='Y' ");
                    SQL.AppendLine("        Inner Join TblItemMovingAvg H On C.ItCode=H.ItCode ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }
                else
                {
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    D.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                    SQL.AppendLine("    From TblDODeptHdr A ");
                    SQL.AppendLine("    Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo And B.DNo=@DNo ");
                    SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select ");
                    SQL.AppendLine("    T.AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                    SQL.AppendLine("    From ( ");
                    SQL.AppendLine("        Select E.AcNo, ");
                    SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                    SQL.AppendLine("        From TblDODeptHdr A ");
                    SQL.AppendLine("        Inner Join TblDODeptDtl B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("            And A.RecvVdDocNo Is Not Null And A.RecvVdDocNo = @DocNo And B.DNo=@DNo ");
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItemCostCategory D On B.ItCode=D.ItCode And A.CCCode=D.CCCode ");
                    SQL.AppendLine("        Inner Join TblCostCategory E On D.CCtCode=E.CCtCode And D.CCCode=E.CCCode ");
                    SQL.AppendLine("    ) T Group By T.AcNo ");
                }

                //Debit RecvVd
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt ");
                SQL.AppendLine("        From TblRecvVdHdr A ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And B.DNo=@DNo ");
            }
            else
            {
                SQL.AppendLine("        Select D.AcNo, 0.00 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
                SQL.AppendLine("        From TblRecvVdDtl A ");
                SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
                SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DNo=@DNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0.00 As CAmt  ");
                SQL.AppendLine("        From TblRecvVdHdr A ");
                SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And B.DNo=@DNo ");
            }

            SQL.AppendLine("    ) Tbl Where AcNo Is not Null Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");

            if (mMenuCodeForPRService.Length > 0)
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On A.DocNo = C.JournalDocNo And C.CancelInd = 'Y' And C.DocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.PODocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo = F.DocNo And E.MaterialRequestDNo = F.DNo ");
                SQL.AppendLine("    And F.MaterialRequestServiceDocNo Is Null ");
                SQL.AppendLine("Inner JOin TblMaterialRequestHdr G On E.MaterialRequestDocNo = G.DocNo ");
                SQL.AppendLine("Set A.SOContractDocNo = G.SOCDocNo ");
                SQL.AppendLine("Where G.SOCDocNo Is Not Null ");
                SQL.AppendLine("And C.DocNo = @DocNo ");
                SQL.AppendLine("And A.SOContractDocNo Is Null ");
                SQL.AppendLine("; ");

                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl C On A.DocNo = C.JournalDocNo And C.CancelInd = 'Y' And C.DocNo = @DocNo ");
                SQL.AppendLine("Inner Join TblPODtl D On C.PODocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo = E.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo = F.DocNo And E.MaterialRequestDNo = F.DNo ");
                SQL.AppendLine("    And F.MaterialRequestServiceDocNo Is Not Null ");
                SQL.AppendLine("Inner JOin TblMaterialRequestServiceHdr G On F.MaterialRequestServiceDocNo = G.DocNo ");
                SQL.AppendLine("Set A.SOContractDocNo = G.SOCDocNo ");
                SQL.AppendLine("Where G.SOCDocNo Is Not Null ");
                SQL.AppendLine("And C.DocNo = @DocNo ");
                SQL.AppendLine("And A.SOContractDocNo Is Null ");
                SQL.AppendLine("; ");
            }
        
            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, row, 0));
            Sm.CmParam<String>(ref cm, "@JournalDocNo", JournalDocNo);
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveJournal2(int r, int Index)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblRecvVdDtl Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From (");
            SQL.AppendLine("        Select JournalDocNo From TblRecvVdDtl ");
            SQL.AppendLine("        Where DocNo=@DocNo And DNo=@DNo And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ) T);");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc), ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblRecvVdDtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And DNo=@DNo And JournalDocNo Is Not Null ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.AcNo, 0 As DAmt, A.Qty*B.UPrice*B.ExcRate As CAmt ");
            SQL.AppendLine("        From TblRecvVdDtl A ");
            SQL.AppendLine("        Inner Join TblStockPrice B On A.Source=B.Source ");
            SQL.AppendLine("        Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory D On C.ItCtCode=D.ItCtCode And D.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DNo=@DNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(D.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As DAmt, 0 As CAmt  ");
            SQL.AppendLine("        From TblRecvVdHdr A ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='VendorAcNoUnInvoiceAP' And IfNull(D.ParValue, '')<>'' ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
     
            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 0));
            if (IsClosingJournalUseCurrentDt)
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, Index));
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(CurrentDt, Index, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }
            }
            else
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, Index));
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, Index, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty)));
                }
            }
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRecvVdHdr(DocNo);
                ShowRecvVdDtl(DocNo);
                if (mIsRecvVdAllowToUploadFile && mRecvUploadFileFormat == "2") ShowRecvVdFile(DocNo);
                ShowPOInfo(DocNo);
                SetSeqNo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvVdHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, A.VdCode, A.VdDONo, A.LocalDocNo, A.SiteCode, ");
            SQL.AppendLine("A.KBContractNo, A.KBContractDt, A.KBPLNo, A.KBPLDt, A.KBRegistrationNo, ");
            SQL.AppendLine("A.KBRegistrationDt, A.KBPackaging, A.KBPackagingQty, A.KBNonDocInd, A.KBSubmissionNo, A.CustomsDocCode, A.Remark ");
            if (mMenuCodeForRecvVdAutoCreateDO)
                SQL.AppendLine(", B.CCCode, B.DeptCode, B.EntCode, ");
            else
                SQL.AppendLine(", null as CCCode, null as DeptCode, null As EntCode, ");
            SQL.AppendLine("A.RecvExpeditionDocNo, A.ProductionWorkGroup, ");
            if (mIsRecvVdAllowToUploadFile)
                SQL.AppendLine("A.FileName, A.FileName2, A.FileName3, A.FileName4, A.FileName5, A.FileName6 ");
            else
                SQL.AppendLine("Null As FileName, Null As FileName2, Null As FileName3, Null As FileName4, Null As FileName5, Null As FileName6 ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            if (mMenuCodeForRecvVdAutoCreateDO)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select Distinct CCCode, DeptCode, EntCode, RecvVdDocNo ");
                SQL.AppendLine("    From TblDODeptHdr ");
                SQL.AppendLine("    Where RecvVdDocNo Is Not Null ");
                SQL.AppendLine("    And RecvVdDocNo = @DocNo ");
                SQL.AppendLine(") B On A.DocNo = B.RecvVdDocNo ");
            }
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] { 
                        //0
                        "DocNo",
                        //1-5
                        "DocDt",  "WhsCode", "VdCode", "VdDONo", "LocalDocNo", 
                        //6-10
                        "SiteCode", "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt", 
                        //11-15
                        "KBRegistrationNo", "KBRegistrationDt", "KBPackaging", "KBPackagingQty", "KBNonDocInd", 
                        //16-20
                        "KBSubmissionNo", "CustomsDocCode", "Remark", "CCCode", "DeptCode",
                        //21-25
                        "EntCode", "RecvExpeditionDocNo", "ProductionWorkGroup", "FileName", "FileName2",
                        //26-29
                        "FileName3", "FileName4", "FileName5", "FileName6"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[3]));
                        TxtVdDONo.EditValue = Sm.DrStr(dr, c[4]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[6]), mIsFilterBySite ? "Y" : "N");
                        TxtKBContractNo.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[8]));
                        TxtKBPLNo.EditValue = Sm.DrStr(dr, c[9]); 
                        Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[10])); 
                        TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[12]));
                        TxtKBPackaging.EditValue = Sm.DrStr(dr, c[13]);
                        TxtKBPackagingQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                        ChkKBNonDocInd.Checked = Sm.DrStr(dr, c[15]) == "Y";
                        TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[16]);
                        Sm.SetLue(LueCustomsDocCode, Sm.DrStr(dr, c[17]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[18]);
                        if (mMenuCodeForRecvVdAutoCreateDO)
                        {
                            Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[19]));
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[20]));
                            Sm.SetLue(LueEntCode, Sm.DrStr(dr, c[21]));
                        }
                        TxtRecvExpeditionDocNo.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[23]));
                        TxtFile.EditValue = Sm.DrStr(dr, c[24]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[25]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[26]);
                        TxtFile4.EditValue = Sm.DrStr(dr, c[27]);
                        TxtFile5.EditValue = Sm.DrStr(dr, c[28]);
                        TxtFile6.EditValue = Sm.DrStr(dr, c[29]);
                    }, true
                );
        }

        private void ShowRecvVdDtl(string DocNo)
        {            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select distinct A.DNo, A.CancelInd, A.cancelReason,  A.PODocNo, A.PODNo, IfNull(E2.DocNo, A.PODocNo) POServiceDocNo, F.DocDt AS PODocDt, F.ImportInd, ");
            SQL.AppendLine("D.ItCode, E.ItName, E.ForeignName, A.BatchNo, A.Source, A.Lot, A.Bin, A.QtyPurchase, E.PurchaseUomCode, ");
            SQL.AppendLine("A.Qty, E.InventoryUomCode, A.Qty2, E.InventoryUomCode2, A.Qty3, E.InventoryUomCode3, A.Remark, A.ExpiredDt,");
            SQL.AppendLine("Trim(Concat(IfNull(F.Remark, ''), ' ', IfNull(B.Remark, ''))) As PORemark, ifnull(E.ItScCode, 'XXX') As ItScCode, ifnull(G.ItScName, 'XXX') As ItScName, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As StatusDesc, F.LocalDocNo, E.ItCodeInternal, E.Specification, ");
            if (mIsPOUseContract)
                SQL.AppendLine("J.OptDesc As POContract, ");
            else
                SQL.AppendLine("Null As POContract, ");
            if (mMenuCodeForRecvVdAutoCreateDO)
                SQL.AppendLine("H.CCtCode, H.CCtName, H.LOPDocNo, H.AcNo, ");
            else
                SQL.AppendLine("null as CCtCode, null as CCtName, null As LOPDocNo, null As AcNo, ");
            SQL.AppendLine("A.RecvExpeditionDocNo, A.RecvExpeditionDNo, ");
            if (mIsRecvExpeditionEnabled)
            {
                SQL.AppendLine("I.QtyPurchase As RecvExpeditionQtyPurchase, ");
                SQL.AppendLine("I.Qty As RecvExpeditionQty, ");
                SQL.AppendLine("I.Qty2 As RecvExpeditionQty2, ");
                SQL.AppendLine("I.Qty3 As RecvExpeditionQty3, ");
                SQL.AppendLine("A.RejectedInd ");
            }
            else
            {
                SQL.AppendLine("0.00 As RecvExpeditionQtyPurchase, ");
                SQL.AppendLine("0.00 As RecvExpeditionQty, ");
                SQL.AppendLine("0.00 As RecvExpeditionQty2, ");
                SQL.AppendLine("0.00 As RecvExpeditionQty3, ");
                SQL.AppendLine("'N' As RejectedInd ");
            }
            if (mIsRecvVdHeatNumberEnabled) 
                SQL.AppendLine(", A.HeatNumber ");
            else
                SQL.AppendLine(", Null As HeatNumber ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Left Join TblMaterialRequestDtl E2 On C.MaterialRequestDocNo=E2.DocNo And C.MaterialRequestDNo=E2.DNo And E2.MaterialRequestServiceDocNo is Not Null");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Inner Join TblPOHdr F On A.PODocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblItemSubcategory G On E.ItScCode = G.ItScCode ");
            if (mMenuCodeForRecvVdAutoCreateDO)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T2.ItCode, T3.CCtCode, T4.CCtName, T1.LOPDocNo, T22.AcNo ");
                SQL.AppendLine("    From TblDODeptHdr T1 ");
                SQL.AppendLine("    Inner Join TblDODeptDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.RecvVdDocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblItem T21 On T2.ItCode = T21.ItCode ");
                SQL.AppendLine("    Inner join TblItemCategory T22 On T21.ItCtCode = T22.ItCtCode ");
                SQL.AppendLine("    Left Join TblItemCostCategory T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("        And T2.ItCode = T3.ItCode ");
                SQL.AppendLine("    Left Join TblCostCategory T4 On T3.CCtCode = T4.CCtCode ");
                SQL.AppendLine("        And T3.CCCode = T4.CCCode ");
                SQL.AppendLine(") H On E.ItCode = H.ItCode ");
            }
            if (mIsRecvExpeditionEnabled)
            {
                SQL.AppendLine("Left Join TblRecvExpeditionDtl I ");
                SQL.AppendLine("    On A.RecvExpeditionDocNo=I.DocNo ");
                SQL.AppendLine("    And A.RecvExpeditionDNo=I.DNo ");
            }

            if (mIsPOUseContract)
                SQL.AppendLine("Left Join TblOption J On F.Contract = J.OptCode And J.OptCat = 'POContract' ");

            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "CancelReason",  "PODocNo", "PODNo", "ItCode",  
                        
                    //6-10
                    "ItName", "BatchNo", "Source", "Lot", "Bin", 

                    //11-15
                    "QtyPurchase",  "PurchaseUomCode", "Qty", "InventoryUomCode", "Qty2",  
                        
                    //16-20
                    "InventoryUomCode2", "Qty3", "InventoryUomCode3", "Remark", "PORemark", 

                    //21-25
                    "ItScCode", "ItScName", "StatusDesc", "LocalDocNo", "ForeignName", 
                    
                    //26-30
                    "PODocDt", "ImportInd", "ItCodeInternal", "Specification", "CCtCode", 

                    //31-35
                    "CCtName", "LOPDocNo", "AcNo", "RecvExpeditionDocNo", "RecvExpeditionDNo",

                    //36-40
                    "RecvExpeditionQtyPurchase", "RecvExpeditionQty", "RecvExpeditionQty2", "RecvExpeditionQty3", "ExpiredDt",

                    //41-44
                    "RejectedInd", "POContract", "HeatNumber", "POServiceDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 25);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 26);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 31);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 32);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 33);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 34);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 35);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 36);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 37);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 38);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 39);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 46, 40);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 47, 41);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 48, 42);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 49, 43);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 44);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 16, 18, 20, 42, 43, 44, 45 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowRecvVdFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                   "Select DNo, FileName, CreateBy, CreateDt From TblRecvVdFile Where DocNo=@DocNo Order By DNo",

                    new string[]
                    {
                        // 0
                        "DNo",

                        //1-3
                        "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd3, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd3, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 7, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowPOInfo(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Select A.DocNo, B.DNo, IfNull(E2.DocNo, A.DocNo) POServiceDocNo,  D.ItCode, E.ItName, E.ForeignName, E.PurchaseUomCode, ");
            if (TxtRecvExpeditionDocNo.Text.Length == 0)
            {
                if(mIsRecvVdUsePORevision)
                {
                    SQL.AppendLine("    IfNull(I.Qty, B.Qty)-IfNull(( ");
                    SQL.AppendLine("        Select Sum(T.QtyPurchase) From TblRecvVdDtl T ");
                    SQL.AppendLine("        Where T.CancelInd='N' And T.PODocNo=A.DocNo And T.PODNo=B.DNo And T.DocNo<>@DocNo ");
                    SQL.AppendLine("        ), 0) - ");
                    SQL.AppendLine("    IfNull((Select Sum(T1.Qty) From TblPOQtyCancel T1 ");
                    SQL.AppendLine("        Where T1.CancelInd='N' And T1.PODocNo=B.DocNo And T1.PODNo=B.DNo), 0) + ");
                    SQL.AppendLine("    IfNull((");
                    SQL.AppendLine("        Select Sum((T3.QtyPurchase/T3.Qty)*T2.Qty)  ");
                    SQL.AppendLine("        From TblDOVdHdr T1, TblDOVdDtl T2, TblRecvVdDtl T3 ");
                    SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("        And T2.CancelInd='N' ");
                    SQL.AppendLine("        And T2.RecvVdDocNo=T3.DocNo ");
                    SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
                    SQL.AppendLine("        And T3.CancelInd='N' ");
                    SQL.AppendLine("        And T3.PODocNo=B.DocNo ");
                    SQL.AppendLine("        And T3.PODNo=C.DNo ");
                    SQL.AppendLine("        And T1.ReplacedItemInd='Y' ");
                    SQL.AppendLine("    ), 0) ");
                    SQL.AppendLine("    As OutstandingQty, ");
                }
                else
                {
                    SQL.AppendLine("    B.Qty-IfNull(( ");
                    SQL.AppendLine("        Select Sum(T.QtyPurchase) From TblRecvVdDtl T ");
                    SQL.AppendLine("        Where T.CancelInd='N' And T.PODocNo=A.DocNo And T.PODNo=B.DNo And T.DocNo<>@DocNo ");
                    SQL.AppendLine("        ), 0) - ");
                    SQL.AppendLine("    IfNull((Select Sum(T1.Qty) From TblPOQtyCancel T1 ");
                    SQL.AppendLine("        Where T1.CancelInd='N' And T1.PODocNo=B.DocNo And T1.PODNo=B.DNo), 0) + ");
                    SQL.AppendLine("    IfNull((");
                    SQL.AppendLine("        Select Sum((T3.QtyPurchase/T3.Qty)*T2.Qty)  ");
                    SQL.AppendLine("        From TblDOVdHdr T1, TblDOVdDtl T2, TblRecvVdDtl T3 ");
                    SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                    SQL.AppendLine("        And T2.CancelInd='N' ");
                    SQL.AppendLine("        And T2.RecvVdDocNo=T3.DocNo ");
                    SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
                    SQL.AppendLine("        And T3.CancelInd='N' ");
                    SQL.AppendLine("        And T3.PODocNo=B.DocNo ");
                    SQL.AppendLine("        And T3.PODNo=C.DNo ");
                    SQL.AppendLine("        And T1.ReplacedItemInd='Y' ");
                    SQL.AppendLine("    ), 0) ");
                    SQL.AppendLine("    As OutstandingQty, ");
                }
            }
            else
            {
                SQL.AppendLine("    (H.QtyPurchase) As OutstandingQty, ");
            }

            SQL.AppendLine("    E.ItCodeInternal, E.Specification  ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B ");
            SQL.AppendLine("        On A.DocNo=B.DocNo ");
            SQL.AppendLine("        And Concat(B.DocNo, B.DNo) In (Select Distinct Concat(PODocNo, PODNo) From TblRecvVdDtl Where DocNo=@DocNo) ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    Left Join TblMaterialRequestDtl E2 On C.MaterialRequestDocNo=E2.DocNo And C.MaterialRequestDNo=E2.DNo And E2.MaterialRequestServiceDocNo is Not Null");
            SQL.AppendLine("    Left Join TblItem E On D.ItCode=E.ItCode ");
            if (TxtRecvExpeditionDocNo.Text.Length > 0)
            {
                SQL.AppendLine("    Inner Join TblRecvVdHdr F On F.DocNo = @DocNo ");
                SQL.AppendLine("    Left Join TblRecvExpeditionHdr G On F.RecvExpeditionDocNo = G.DocNo ");
                SQL.AppendLine("    Left Join TblRecvExpeditionDtl H On G.DocNo = H.DocNo And B.DocNo = H.PODocNo And B.DNo = H.PODNo ");
            }
            if (mIsRecvVdUsePORevision)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    SELECT B.Qty, A.Docno, A.DNo ");
                SQL.AppendLine("    FROM tblpodtl A ");
                SQL.AppendLine("    Left JOIN tblporevision B ON A.DocNo = B.PODocNo AND A.DNo = B.PODNo AND B.status = 'A'  ");
                SQL.AppendLine("    Left JOIN tblrecvvddtl C ON A.DocNo = C.PODocNo AND A.DNo = C.PODNo   ");
                SQL.AppendLine("    WHERE B.DocNo = ( ");
                SQL.AppendLine("        SELECT MAX(D.docno)  ");
                SQL.AppendLine("        FROM tblporevision D ");
                SQL.AppendLine("        Inner JOIN tblpodtl E ON D.PODocNo = E.DocNo AND D.PODNo = E.DNo ");
                SQL.AppendLine("        Inner JOIN tblrecvvddtl F ON E.DocNo = F.PODocNo AND E.DNo = F.PODNo ");
                SQL.AppendLine("        WHERE F.DocNo = @DocNo AND D.`Status` = 'A') ");
                SQL.AppendLine("        AND C.DocNo = @DocNo ");
                SQL.AppendLine(") I On B.DocNo=I.DocNo And B.DNo=I.DNo ");
            }


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DNo", "ItCode", "ItName", "PurchaseUomCode", "OutstandingQty",

                    //6-9
                    "ForeignName", "ItCodeInternal", "Specification", "POServiceDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 12, 9);
                    ComputeQty();
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetSOContractDocNo()
        {
            string PODocNoDNo = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<SOContract>();

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (PODocNoDNo.Length > 0) PODocNoDNo += ",";
                PODocNoDNo += string.Concat(Sm.GetGrdStr(Grd1, i, 5), Sm.GetGrdStr(Grd1, i, 6));
            }

            if (PODocNoDNo.Length > 0)
            {
                SQL.AppendLine("Select A.DocNo, A.DNo, D.SOCDocNo ");
                SQL.AppendLine("From TblPODtl A "); 
                SQL.AppendLine("Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo ");
                SQL.AppendLine("    And Find_In_Set(Concat(A.DocNo, A.DNo), @PODocNoDNo) ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo ");
                SQL.AppendLine("    And C.MaterialRequestServiceDocNo Is Null ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr D On B.MaterialRequestDocNo = D.DocNo ");
                SQL.AppendLine("    And D.SOCDocNo Is Not Null ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select A.DocNo, A.DNo, D.SOCDocNo ");
                SQL.AppendLine("From TblPODtl A ");
                SQL.AppendLine("Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo ");
                SQL.AppendLine("    And Find_In_Set(Concat(A.DocNo, A.DNo), @PODocNoDNo) ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo ");
                SQL.AppendLine("    And C.MaterialRequestServiceDocNo Is Not Null ");
                SQL.AppendLine("Inner Join TblMaterialRequestServiceHdr D On C.MaterialRequestServiceDocNo = D.DocNo ");
                SQL.AppendLine("    And D.SOCDocNo Is Not Null ");
                SQL.AppendLine("; ");

                Sm.CmParam<String>(ref cm, "@PODocNoDNo", PODocNoDNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "SOCDocNo" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new SOContract() 
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                DNo = Sm.DrStr(dr, c[1]),
                                SOCDocNo = Sm.DrStr(dr, c[2])
                            });
                        }
                    }
                    dr.Close();
                }

                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        string PODocNo = Sm.GetGrdStr(Grd1, i, 5);
                        string PODNo = Sm.GetGrdStr(Grd1, i, 6);
                        foreach (var x in l.Where(w => w.DocNo == PODocNo && w.DNo == PODNo))
                        {
                            Grd1.Cells[i, 51].Value = x.SOCDocNo;
                        }
                    }
                }
            }

            l.Clear();
        }

        private string GetProfitCenterCode()
        {
            var Value = string.Empty;

            if(mMenuCodeForRecvVdAutoCreateDO && mCostCenterJournalRecvVdSource == "2")
            {
                Value = Sm.GetLue(LueCCCode);
                if (Value.Length == 0) return string.Empty;

                return
               Sm.GetValue(
                   "Select ProfitCenterCode From TblCostCenter  " +
                   "Where ProfitCenterCode Is Not Null And CCCode=@Param;",
                   Value);
            }
            else
            {
                Value = Sm.GetLue(LueWhsCode);
                if (Value.Length == 0) return string.Empty;

                return
               Sm.GetValue(
                   "Select B.ProfitCenterCode From TblWarehouse A, TblCostCenter B " +
                   "Where A.CCCode=B.CCCode And B.ProfitCenterCode Is Not Null And A.WhsCode=@Param;",
                   Value);
            }
           
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted4()
        {
            if (TxtFile4.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile4.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName4=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted5()
        {
            if (TxtFile5.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile5.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName5=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted6()
        {
            if (TxtFile6.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile6.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 From TblRecvVdHdr ");
                SQL.AppendLine("Where FileName6=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile4(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload4.Value = 0;
                PbUpload4.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload4.Value = PbUpload4.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload4.Value + bytesRead <= PbUpload4.Maximum)
                        {
                            PbUpload4.Value += bytesRead;

                            PbUpload4.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile5(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload5.Value = 0;
                PbUpload5.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload5.Value = PbUpload5.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload5.Value + bytesRead <= PbUpload5.Maximum)
                        {
                            PbUpload5.Value += bytesRead;

                            PbUpload5.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile6(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload6.Value = 0;
                PbUpload6.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload6.Value = PbUpload6.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload6.Value + bytesRead <= PbUpload6.Maximum)
                        {
                            PbUpload6.Value += bytesRead;

                            PbUpload6.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void SetDeptDefault()
        {
            if (!mIsRecvVdAutoDOUseMRDept) return;
            if (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 5).Length > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select C.DeptCode ");
                SQL.AppendLine("From TblPODtl A, TblPORequestDtl B, TblMaterialRequestHdr C ");
                SQL.AppendLine("Where A.DocNo=@Param1 And A.DNo=@Param2 ");
                SQL.AppendLine("And A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                SQL.AppendLine("And B.MaterialRequestDocNo=C.DocNo ");
                SQL.AppendLine("Limit 1; ");

                var DeptCode = Sm.GetValue(SQL.ToString(), Sm.GetGrdStr(Grd1, 0, 5), Sm.GetGrdStr(Grd1, 0, 6), string.Empty);
                if (DeptCode.Length > 0) Sm.SetLue(LueDeptCode, DeptCode);
            }
        }


        private void ProcessFromGrd2(ref List<RecvVdIMSDtl> l)
        {
            if (l.Count > 0)
            {
                var l2 = new List<Grid2>();

                for (int i = 0; i < Grd2.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd2, i, 0).Length > 0)
                    {
                        l2.Add(new Grid2()
                        {
                            PODocNo = Sm.GetGrdStr(Grd2, i, 0),
                            PODNo = Sm.GetGrdStr(Grd2, i, 1),
                            OutstandingQty = Sm.GetGrdDec(Grd2, i, 5),
                            ReceivedQty = Sm.GetGrdDec(Grd2, i, 6),
                            BalanceQty = Sm.GetGrdDec(Grd2, i, 7)
                        });
                    }
                }

                if (l2.Count > 0)
                {
                    foreach (var x in l)
                    {
                        foreach(var y in l2.Where(w => w.PODocNo == x.PODocNo && w.PODNo == x.PODNo))
                        {
                            x.OutstandingQty = y.OutstandingQty;
                            x.ReceivedQty = y.ReceivedQty;
                            x.BalanceQty = y.BalanceQty;
                        }
                    }
                }

                l2.Clear();
            }
        }

        private string GetPODocNoDNo(string DocNo)
        {
            string DocNoDNo = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Group_Concat(Distinct Concat(PODocNo, PODNo)) ");
            SQL.AppendLine("From TblRecvVdDtl ");
            SQL.AppendLine("Where DocNo = @Param ");

            DocNoDNo = Sm.GetValue(SQL.ToString(), DocNo);

            return DocNoDNo;
        }

        private string GetMaxDocNoDt(string DocNo)
        {
            string MaxDocNoDt = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(DocDt, Left(DocNo, 4)) ");
            SQL.AppendLine("From TblRecvVdHdr ");
            SQL.AppendLine("Where DocNo = @Param; ");

            MaxDocNoDt = Sm.GetValue(SQL.ToString(), DocNo);

            return MaxDocNoDt;
        }

        private void SetCostCategory()
        {
            var SQL = new StringBuilder();
            string ItCode = string.Empty, CCtCode = string.Empty, CCtName = string.Empty;
            var cm = new MySqlCommand();

            Grd1.BeginUpdate();
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Grd1.Cells[r, 36].Value = string.Empty;
                Grd1.Cells[r, 37].Value = string.Empty;
            }
            Grd1.EndUpdate();

            SQL.AppendLine("Select A.ItCode, A.CCtCode, B.CCtName ");
            SQL.AppendLine("From TblItemCostCategory A, TblCostCategory B ");
            SQL.AppendLine("Where A.CCCode=@CCCode ");
            SQL.AppendLine("And A.CCtCode=B.CCtCode ");
            SQL.AppendLine("And B.AcNo Is Not Null ");
            SQL.AppendLine("And (1=1 ");

            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    ItCode = Sm.GetGrdStr(Grd1, r, 7);
                    if (ItCode.Length > 0)
                    {
                        SQL.AppendLine(" Or (A.ItCode=@ItCode" + r.ToString() + ") ");
                        Sm.CmParam<String>(ref cm, "@ItCode" + r.ToString(), ItCode);
                    }
                }
                SQL.AppendLine(") Order By A.ItCode;");
            }
            else
                SQL.AppendLine(" And 1=0); ");

            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();   
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "CCtCode", "CCtName" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        CCtCode = Sm.DrStr(dr, 1);
                        CCtName = Sm.DrStr(dr, 2);
                        for (int r = 0; r< Grd1.Rows.Count; r++)
                        {
                            if (Sm.GetGrdStr(Grd1, r, 8).Length > 0 && Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 8), ItCode))
                            {
                                Grd1.Cells[r, 36].Value = CCtCode;
                                Grd1.Cells[r, 37].Value = CCtName;
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #region Journal SO Contract

        private void PrepData(ref List<JournalBasedSOC> lx, string RecvVdDocNo, bool IsCancelled)
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdBool(Grd1, i, 47) == false) // rejected item doesn't included
                {
                    if (IsCancelled)
                    {
                        if (Sm.GetGrdBool(Grd1, i, 1) && !Sm.GetGrdBool(Grd1, i, 2) && Sm.GetGrdStr(Grd1, i, 5).Length > 0)
                        {
                            lx.Add(new JournalBasedSOC()
                            {
                                DocNo = string.Empty,
                                DNo = string.Empty,
                                RecvVdDocNo = RecvVdDocNo,
                                RecvVdDNo = Sm.GetGrdStr(Grd1, i, 0),
                                PODocNo = Sm.GetGrdStr(Grd1, i, 5),
                                PODNo = Sm.GetGrdStr(Grd1, i, 6),
                                SOCDocNo = Sm.GetGrdStr(Grd1, i, 51),
                                Index = 0
                            });
                        }
                    }
                    else
                    {
                        lx.Add(new JournalBasedSOC()
                        {
                            DocNo = string.Empty,
                            DNo = string.Empty,
                            RecvVdDocNo = RecvVdDocNo,
                            RecvVdDNo = Sm.Right(string.Concat("000", (i + 1).ToString()), 3),
                            PODocNo = Sm.GetGrdStr(Grd1, i, 5),
                            PODNo = Sm.GetGrdStr(Grd1, i, 6),
                            SOCDocNo = Sm.GetGrdStr(Grd1, i, 51),
                            Index = 0
                        });
                    }
                }
            }
        }

        private void GetJournalDNo(ref List<JournalBasedSOC> lx)
        {
            string mSOCDocNo = string.Empty;
            int mDNo = 0;
            int mIndex = 1;
            bool IsFirst = true;
            foreach (var x in lx.OrderBy(o => o.SOCDocNo).ThenBy(o => o.RecvVdDNo))
            {
                if (mSOCDocNo.Length == 0 && IsFirst) mSOCDocNo = x.SOCDocNo;

                if (mSOCDocNo == x.SOCDocNo)
                {
                    mDNo += 1;
                    x.DNo = Sm.Right(string.Concat("000", mDNo.ToString()), 3);
                }
                else
                {
                    x.DNo = "001";
                    mDNo = 1;
                    mIndex += 1;
                    mSOCDocNo = x.SOCDocNo;
                }

                if (IsFirst) IsFirst = false;

                x.Index = mIndex;
            }
        }

        private void GetJournalDocNo(ref List<JournalBasedSOC> lx)
        {
            foreach (var x in lx.OrderBy(o => o.SOCDocNo).ThenBy(o => o.RecvVdDNo).ThenBy(o => o.DNo))
            {
                if (mJournalDocNoFormat == "1") //Default
                {
                    x.DocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), x.Index);
                }
                else if (mJournalDocNoFormat == "2") //PHT
                {
                    string Code1 = Sm.GetCode1ForJournalDocNo("FrmRecvVd", string.Empty, string.Empty, mJournalDocNoFormat);
                    string ProfitCenterCode = Sm.GetValue("Select ProfitCenterCode from tblcostcenter where CCCode = @Param", Sm.GetLue(LueCCCode));
                    x.DocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(Sm.GetDte(DteDocDt), 1, Code1, ProfitCenterCode, string.Empty, string.Empty, string.Empty));
                }

                
            }
        }

        #endregion

        private void ProcessDODtl(ref List<DODept2Dtl> lDODtl, string DocNo)
        {
            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 5).Length > 0)
                {
                    lDODtl.Add(new DODept2Dtl()
                    {
                        DocNo = "1",
                        DNo = "1",
                        ItCode = Sm.GetGrdStr(Grd1, i, 8),
                        PropCode = "-",
                        BatchNo = GenerateBatchNo2(i, DocNo, Sm.Right(string.Concat("000", (i + 1)), 3)),
                        Source = string.Concat("01*", DocNo, "*", Sm.Right(string.Concat("000", (i + 1)), 3)),
                        Lot = Sm.GetGrdStr(Grd1, i, 12).Length > 0 ? Sm.GetGrdStr(Grd1, i, 12) : "-",
                        Bin = Sm.GetGrdStr(Grd1, i, 13).Length > 0 ? Sm.GetGrdStr(Grd1, i, 13) : "-",
                        Qty = Sm.GetGrdDec(Grd1, i, 16),
                        Qty2 = Sm.GetGrdDec(Grd1, i, 18),
                        Qty3 = Sm.GetGrdDec(Grd1, i, 20),
                        LOPDocNo = Sm.GetGrdStr(Grd1, i, 38),
                        Remark = string.Concat(Sm.GetGrdStr(Grd1, i, 22), " [Auto Create From RecvVd #", DocNo)
                    });
                }
            }
        }

        private void ProcessDODtl2(ref List<DODept2Dtl> lDODtl)
        {
            int mDocNoIndex = 0;
            int mDNoIndex = 0;
            string mLOPDocNo = string.Empty, mDocNo = string.Empty, mDNo = string.Empty;

            for (int i = 0; i < lDODtl.Count; ++i)
            {
                if (i == 0)
                {
                    mLOPDocNo = lDODtl[i].LOPDocNo;
                    mDocNoIndex = 1;
                }

                if (mLOPDocNo == lDODtl[i].LOPDocNo)
                {
                    mDNoIndex += 1;
                }
                else
                {
                    mDocNoIndex += 1;
                    mDNoIndex = 1;
                    mLOPDocNo = lDODtl[i].LOPDocNo;
                }

                if (mLOPDocNo.Length > 0) mDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODeptLOP", "TblDODeptHdr", mDocNoIndex.ToString());
                else mDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DODept2", "TblDODeptHdr", mDocNoIndex.ToString());
                mDNo = Sm.Right(string.Concat("000", mDNoIndex.ToString()), 3);

                lDODtl[i].DocNo = mDocNo;
                lDODtl[i].DNo = mDNo;
            }
        }

        private void UpdateDODtl(ref List<DODept2Dtl> lDODtl, string DocNo)
        {
            for (int i = 0; i < lDODtl.Count; ++i)
            {
                lDODtl[i].DocNo = DocNo;
                lDODtl[i].DNo = Sm.Right(string.Concat("000", (i + 1)), 3);
            }
        }

        private void ProcessDOHdr(ref List<DODept2Hdr> lDOHdr, string DODeptDocNo, string DocNo)
        {
            lDOHdr.Add(new DODept2Hdr()
            {
                DocNo = DODeptDocNo,
                DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                WhsCode = Sm.GetLue(LueWhsCode),
                LocalDocNo = TxtLocalDocNo.Text,
                DeptCode = Sm.GetLue(LueDeptCode),
                EntCode = Sm.GetLue(LueEntCode),
                CCCode = Sm.GetLue(LueCCCode),
                LOPDocNo = string.Empty,
                RecvVdDocNo = DocNo,
                Remark = string.Concat(MeeRemark.Text, " [Auto Create From RecvVd #", DocNo)
            });
        }

        private void ProcessDOHdr2(ref List<DODept2Hdr> lDOHdr, ref List<DODept2Dtl> lDODtl, string DocNo)
        {
            string mDocNo = string.Empty;
            for (int i = 0; i < lDODtl.Count; ++i)
            {
                if (i == 0)
                {
                    mDocNo = lDODtl[i].DocNo;

                    lDOHdr.Add(new DODept2Hdr()
                    {
                        DocNo = lDODtl[i].DocNo,
                        DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                        WhsCode = Sm.GetLue(LueWhsCode),
                        LocalDocNo = TxtLocalDocNo.Text,
                        DeptCode = Sm.GetLue(LueDeptCode),
                        EntCode = Sm.GetLue(LueEntCode),
                        CCCode = Sm.GetLue(LueCCCode),
                        LOPDocNo = lDODtl[i].LOPDocNo,
                        RecvVdDocNo = DocNo,
                        Remark = string.Concat(MeeRemark.Text, " [Auto Create From RecvVd #", DocNo)
                    });
                }
                else
                {
                    if (mDocNo != lDODtl[i].DocNo)
                    {
                        mDocNo = lDODtl[i].DocNo;

                        lDOHdr.Add(new DODept2Hdr()
                        {
                            DocNo = lDODtl[i].DocNo,
                            DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8),
                            WhsCode = Sm.GetLue(LueWhsCode),
                            LocalDocNo = TxtLocalDocNo.Text,
                            DeptCode = Sm.GetLue(LueDeptCode),
                            EntCode = Sm.GetLue(LueEntCode),
                            CCCode = Sm.GetLue(LueCCCode),
                            LOPDocNo = lDODtl[i].LOPDocNo,
                            RecvVdDocNo = DocNo,
                            Remark = string.Concat(MeeRemark.Text, " [Auto Create From RecvVd #", DocNo)
                        });
                    }
                }
            }
        }

        internal void GenerateBatchNo(int Row)
        {
            if (mIsRecvVdUseOptionBatchNoFormula && 
                mIsAutoGenerateBatchNo && 
                mIsAutoGenerateBatchNoEditable)
            {
                var SQL = new StringBuilder();
                string mMaterialCategoryCode = string.Empty, mMaterialCode = string.Empty,
                    mBatchMaterialCode = string.Empty, mSequenceNo = string.Empty,
                    mBatchNo = string.Empty;
                string mItCode = Sm.GetGrdStr(Grd1, Row, 8);

                mMaterialCode = Sm.GetValue("Select ItGrpCode From TblItem Where ItCode = @Param Limit 1; ", mItCode);
                if (DteDocDt.Text.Length > 0) mBatchMaterialCode = GetDteFormat(DteDocDt.DateTime);

                SQL.AppendLine("Select B.OptDesc ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblOption B On A.ItCtCode = B.OptCode ");
                SQL.AppendLine("    And B.OptCat = 'RecvVdBatchNoFormula' ");
                SQL.AppendLine("    And A.ItCode = @Param ");
                SQL.AppendLine("Limit 1; ");

                mMaterialCategoryCode = Sm.GetValue(SQL.ToString(), mItCode);
                mSequenceNo = GetBatchNoSequenceNo(string.Concat(mMaterialCategoryCode, mMaterialCode, mBatchMaterialCode));

                mBatchNo = string.Concat(mMaterialCategoryCode, mMaterialCode, "-", mBatchMaterialCode, mSequenceNo, "-", LueVdCode.Text);

                Grd1.Cells[Row, 10].Value = mBatchNo;
            }
        }

        //untuk auto DO
        private string GenerateBatchNo2(int Row, string DocNo, string DNo)
        {
            string mBatchNo = string.Empty;
            string mShortName = Sm.GetValue("Select ShortName From TblVendor Where VdCode = @Param ", Sm.GetLue(LueVdCode));
            string mDocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            string mVdCode = Sm.GetLue(LueVdCode);
            
            if (mIsProjectGroupEnabled)
            {
                mBatchNo = string.Concat(Sm.Right(mDocDt, 2), "/", mDocDt.Substring(4, 2), "/", Sm.Left(mDocDt, 4));
            }
            else
            {
                if (mIsAutoGenerateBatchNo)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 33).Length > 0)
                    {
                        mBatchNo = Sm.GetGrdStr(Grd1, Row, 33);
                    }
                    else
                    {
                        if (mRecvVdAutoBatchNoFormat == "2")
                            mBatchNo = string.Concat(mShortName, "-", mDocDt, "-", DocNo, "-", DNo);
                        else
                        {
                            if (mRecvVdAutoBatchNoFormat == "3")
                            {
                                mBatchNo = string.Concat(mDocDt.Substring(2, 2), "/", mDocDt.Substring(4, 2), "/", Sm.Right(mDocDt, 2), "/", Sm.Left(DocNo, 4), "/", mVdCode);
                            }
                            else
                            {
                                if (mIsAutoGenerateBatchNoEditable)
                                {
                                    if (mIsRecvVdUseOptionBatchNoFormula)
                                        mBatchNo = Sm.GetGrdStr(Grd1, Row, 10).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 10) : "-";
                                    else
                                    {
                                        mBatchNo = string.Concat(mShortName, "-", mDocDt, "-", DocNo);
                                        if (Sm.GetGrdStr(Grd1, Row, 10).Length > 0)
                                            mBatchNo += string.Concat("-", Sm.GetGrdStr(Grd1, Row, 10));
                                    }
                                }
                                else
                                    mBatchNo = string.Concat(mShortName, "-", mDocDt, "-", DocNo);
                            }
                        }
                    }
                }
                else
                {
                    if (Sm.GetGrdStr(Grd1, Row, 33).Length > 0)
                    {
                        mBatchNo = Sm.GetGrdStr(Grd1, Row, 33);
                    }
                    else
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 10).Length > 0)
                            mBatchNo = Sm.GetGrdStr(Grd1, Row, 10);
                        else
                            mBatchNo = mIsBatchNoUseDocDtIfEmpty ? mDocDt : "-";
                    }
                }
            }
            

            return mBatchNo;
        }

        private string GetDteFormat(DateTime Value)
        {
            return
                ("00" + Value.Day.ToString()).Substring(("00" + Value.Day.ToString()).Length - 2, 2) +
                ("00" + Value.Month.ToString()).Substring(("00" + Value.Month.ToString()).Length - 2, 2) +
                Sm.Right(Value.Year.ToString(), 2);
        }

        private string GetBatchNoSequenceNo(string BatchNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RIGHT(CONCAT('000', (T.BatchNo + 1)), 3) ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select IfNull(Max(Right(BatchNo, 3)), '000') As BatchNo ");
            SQL.AppendLine("    From TblRecvVdDtl ");
            SQL.AppendLine("    Where BatchNo Is Not Null ");
            SQL.AppendLine("    And Length(BatchNo) > 2 ");
            SQL.AppendLine("    And BatchNo Like @Param ");
            SQL.AppendLine(") T; ");

            return Sm.GetValue(SQL.ToString(), string.Concat(BatchNo, "%"));
        }

        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 32].Value = r + 1;
        }

        public static void SetLueLot(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Lot As Col1 From TblLotHdr Where ActInd='Y' Order By Lot",
                "Lot");
        }

        public static void SetLueBin(ref LookUpEdit Lue)
        {
            Sm.SetLue1(ref Lue,
                "Select Distinct Bin As Col1 from TblBin Where ActInd='Y' Order By Bin;",
                "Bin");
        }
        //private void SetLueContract(ref LookUpEdit Lue, string ContractCode)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
        //    SQL.AppendLine("From TblOption Where OptCat = 'POContract' ");
        //    if (ContractCode.Length > 0)
        //        SQL.AppendLine("Where OptCode = @ContractCode ");
        //    SQL.AppendLine("Order By OptDesc;");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<string>(ref cm, "@ContractCode", ContractCode);

        //    Sm.SetLue2(
        //        ref Lue, ref cm,
        //        0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsUseAdditionalLabel', 'IsFilterByDept', 'IsFilterByCC', 'IsFilterByItCt', 'IsFilterBySite', ");
            SQL.AppendLine("'MainCurCode', 'Doctitle', 'IsRecvVdAutoDOUseMRDept', 'IsRecvVdAutoDOUseDifferentAbbreviation', 'IsRecvVdJournalBasedOnSOContract', ");
            SQL.AppendLine("'IsAutoGeneratePurchaseLocalDocNo', 'RecvVdAutoDOCostCenterDefault', 'IsRecvVdAllowToUploadFile', 'MandatoryFileForRecvVd', 'CostCenterJournalRecvVdSource', ");
            SQL.AppendLine("'ProcFormatDocNo', 'LabelFileForRecvVd', 'IsAutoGenerateBatchNoEditable', 'IsPOUseContract', 'IsAutoGenerateBatchNo', ");
            SQL.AppendLine("'IsButtonUsageHistoryActived', 'IsUseProductionWorkGroup', 'IsVendorComboShowCategory', 'IsCheckCOAJournalNotExists', 'IsItemCategoryUseCOAAPAR', ");
            SQL.AppendLine("'IsBOMShowSpecifications', 'IsShowForeignName', 'IsRecvVdUseOptionBatchNoFormula', 'IsMovingAvgEnabled', 'IsRecvExpeditionEnabled', ");
            SQL.AppendLine("'IsBatchNoUseDocDtIfEmpty', 'IsRecvVdLocalDocNoBasedOnMRLocalDocNo', 'RecvVdCustomsDocCodeManualInput', 'IsItGrpCodeShow', 'PortForFTPClient', ");
            SQL.AppendLine("'IsRecvVdValidateByEntity', 'IsRecvVdSplitOutstandingPO', 'IsPOSplitBasedOnTax', 'RecvVdAutoBatchNoFormat', 'IsShowSeqNoInRecvVd', ");
            SQL.AppendLine("'IsRecvVdRemarkForApprovalMandatory', 'IsAutoJournalActived', 'IsRecvVdApprovalBasedOnWhs', 'IsComparedToDetailDate', 'IsRemarkForJournalMandatory', ");
            SQL.AppendLine("'UsernameForFTPClient', 'PasswordForFTPClient', 'FileSizeMaxUploadFTPClient', 'MenuCodeForPRService', 'IsRecvVdNeedApproval', ");
            SQL.AppendLine("'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'KB_Server', 'MenuCodeForRecvVdAutoCreateDO', 'NumberOfInventoryUomCode', 'IsRecvVdUsePORevision', ");
            SQL.AppendLine("'IsClosingJournalBasedOnMultiProfitCenter', 'IsJournalRecvVdAutoDOUseCOAEntity', 'FormatFTPClient', 'RecvUploadFileFormat', 'JournalDocNoFormat', 'RecvvdCostCenterJournalFormat', ");
            SQL.AppendLine("'IsMaterialRequestForDroppingRequestEnabled', 'RecvvdJournalFormat' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open(); cm.Connection = cn; cm.CommandText = SQL.ToString(); var dr = cm.ExecuteReader(); var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]); ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRecvVdNeedApproval": mIsRecvVdNeedApproval = ParValue == "Y"; break;
                            case "IsRecvVdRemarkForApprovalMandatory": mIsRemarkForApprovalMandatory = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsRecvVdApprovalBasedOnWhs": mIsRecvVdApprovalBasedOnWhs = ParValue == "Y"; break;
                            case "IsComparedToDetailDate": mIsComparedToDetailDate = ParValue == "Y"; break;
                            case "IsRemarkForJournalMandatory": mIsRemarkForJournalMandatory = ParValue == "Y"; break;
                            case "IsRecvVdValidateByEntity": mIsRecvVdValidateByEntity = ParValue == "Y"; break;
                            case "IsRecvVdSplitOutstandingPO": mIsRecvVdSplitOutstandingPO = ParValue == "Y"; break;
                            case "IsShowSeqNoInRecvVd": mIsShowSeqNoInRecvVd = ParValue == "Y"; break;
                            case "IsBatchNoUseDocDtIfEmpty": mIsBatchNoUseDocDtIfEmpty = ParValue == "Y"; break;
                            case "IsRecvVdLocalDocNoBasedOnMRLocalDocNo": mIsRecvVdLocalDocNoBasedOnMRLocalDocNo = ParValue == "Y"; break;
                            case "IsItGrpCodeShow": mIsItGrpCodeShow = ParValue == "Y"; break;
                            case "IsProjectGroupEnabled": mIsProjectGroupEnabled = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsRecvVdUseOptionBatchNoFormula": mIsRecvVdUseOptionBatchNoFormula = ParValue == "Y"; break;
                            case "IsMovingAvgEnabled": mIsMovingAvgEnabled = ParValue == "Y"; break;
                            case "IsRecvVdBatchNoUseProjectCode": mIsRecvVdBatchNoUseProjectCode = ParValue == "Y"; break;
                            case "IsRecvExpeditionEnabled": mIsRecvExpeditionEnabled = ParValue == "Y"; break;
                            case "IsButtonUsageHistoryActived": mIsButtonUsageHistoryActived = ParValue == "Y"; break;
                            case "IsUseProductionWorkGroup": mIsUseProductionWorkGroup = ParValue == "Y"; break;
                            case "IsVendorComboShowCategory": mIsVendorComboShowCategory = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsFilterByCC": mIsFilterByCC = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsUseAdditionalLabel": mIsUseAdditionalLabel = ParValue == "Y"; break;
                            case "IsRecvVdJournalBasedOnSOContract": mIsRecvVdJournalBasedOnSOContract = ParValue == "Y"; break;
                            case "IsRecvVdAutoDOUseDifferentAbbreviation": mIsRecvVdAutoDOUseDifferentAbbreviation = ParValue == "Y"; break;
                            case "IsRecvVdAutoDOUseMRDept": mIsRecvVdAutoDOUseMRDept = ParValue == "Y"; break;
                            case "IsRecvVdAllowToUploadFile": mIsRecvVdAllowToUploadFile = ParValue == "Y"; break;
                            case "IsAutoGeneratePurchaseLocalDocNo": mIsAutoGeneratePurchaseLocalDocNo = ParValue == "Y"; break;
                            case "IsAutoGenerateBatchNo": mIsAutoGenerateBatchNo = ParValue == "Y"; break;
                            case "IsPOUseContract": mIsPOUseContract = ParValue == "Y"; break;
                            case "IsAutoGenerateBatchNoEditable": mIsAutoGenerateBatchNoEditable = ParValue == "Y"; break;
                            case "IsItemCategoryUseCOAAPAR": mIsItemCategoryUseCOAAPAR = ParValue == "Y"; break;
                            case "IsRecvVdUsePORevision": mIsRecvVdUsePORevision = ParValue == "Y"; break;
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsJournalRecvVdAutoDOUseCOAEntity": mIsJournalRecvVdAutoDOUseCOAEntity = ParValue == "Y"; break;
                            case "IsMaterialRequestForDroppingRequestEnabled": mIsMaterialRequestForDroppingRequestEnabled = ParValue == "Y"; break;

                            //string
                            case "NumberOfInventoryUomCode":
                                mNumberOfInventoryUomCode = (ParValue.Length == 0) ? 1 : int.Parse(ParValue);
                                break;
                            case "MenuCodeForRecvVdAutoCreateDO": mMenuCodeForRecvVdAutoCreateDO = ParValue == mMenuCode; break;
                            case "KB_Server": mIsKawasanBerikatEnabled = ParValue.Length > 0; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "CostCenterJournalRecvVdSource": mCostCenterJournalRecvVdSource = ParValue; break;
                            case "LabelFileForRecvVd": mLabelFileForRecvVd = ParValue; break;
                            case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                            case "RecvVdCustomsDocCodeManualInput": mRecvVdCustomsDocCodeManualInput = ParValue; break;
                            case "RecvVdAutoBatchNoFormat": mRecvVdAutoBatchNoFormat = ParValue; break;
                            case "IsPOSplitBasedOnTax": mIsPOSplitBasedOnTax = ParValue; break;
                            case "MenuCodeForPRService": mMenuCodeForPRService = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "RecvVdAutoDOCostCenterDefault": mRecvVdAutoDOCostCenterDefault = ParValue; break;
                            case "MandatoryFileForRecvVd": mMandatoryFileForRecvVd = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "Doctitle": mDoctitle = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                            case "RecvUploadFileFormat": mRecvUploadFileFormat = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                            case "RecvvdCostCenterJournalFormat": mRecvvdCostCenterJournalFormat = ParValue; break;
                            case "RecvvdJournalFormat": mRecvvdJournalFormat = ParValue; break;

                        }
                    }
                }
                dr.Close();
            }
            mPODocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='PO';");
            mPOWithTitleLength = 4 + mPODocTitle.Length + mPODocAbbr.Length + 4 + 4; //4=nomor urut(0001), 4=bln+tahun(1115), 4 = jmlh "/"
            mPOWithoutTitleLength = 4 + mPODocAbbr.Length + 4 + 3;
            if (mRecvVdAutoDOCostCenterDefault.Length>0) mIsRecvVdAutoDOUseDefaultCostCenter = Sm.GetParameterBoo("IsRecvVdAutoDOUseDefaultCostCenter");
            if (!mMenuCodeForRecvVdAutoCreateDO) mIsRecvVdHeatNumberEnabled = Sm.GetParameterBoo("IsRecvVdHeatNumberEnabled");
            if (mFormatFTPClient.Length == 0) mFormatFTPClient = "1";
            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
            if (mRecvUploadFileFormat.Length == 0) mRecvUploadFileFormat = "1";
            if (mRecvvdJournalFormat.Length == 0) mRecvvdJournalFormat = "1";
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private static string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            var SQL = new StringBuilder();

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
                else
                {
                    SQL.Append("Select Concat( ");
                    SQL.Append("IfNull(( ");
                    SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                    SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                    SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                    SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                    SQL.Append("       ) As Temp ");
                    SQL.Append("   ), '0001') ");
                    SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                    SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                    SQL.Append(") As DocNo");
                }
            }

            return Sm.GetValue(SQL.ToString());
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            mNumberOfInventoryUomCode = (NumberOfInventoryUomCode.Length == 0) ? 1 : int.Parse(NumberOfInventoryUomCode);
        }

        internal void ShowPOInfo(int Row)
        {
            bool IsExisted = false;
            decimal ReceivedQty = 0m;

            if (Grd2.Rows.Count != 1)
            {
                for (int Row2 = 0; Row2 <= Grd2.Rows.Count - 1; Row2++)
                {
                    if (Sm.GetGrdStr(Grd2, Row2, 0).Length>0 && 
                        Sm.CompareStr(Sm.GetGrdStr(Grd2, Row2, 0) + Sm.GetGrdStr(Grd2, Row2, 1), Sm.GetGrdStr(Grd1, Row, 5)+Sm.GetGrdStr(Grd1, Row, 6)))
                    {
                        IsExisted = true;
                        ReceivedQty = Sm.GetGrdDec(Grd2, Row2, 7);
                        if (ReceivedQty < 0) ReceivedQty = 0m;
                        break;
                    }
                }
            }
            if (!IsExisted)
            {
                int Row2 = Grd2.Rows.Count - 1;

                Grd2.Cells[Row2, 0].Value = Sm.GetGrdStr(Grd1, Row, 5);
                Grd2.Cells[Row2, 1].Value = Sm.GetGrdStr(Grd1, Row, 6);
                Grd2.Cells[Row2, 2].Value = Sm.GetGrdStr(Grd1, Row, 8);
                Grd2.Cells[Row2, 3].Value = Sm.GetGrdStr(Grd1, Row, 9);
                Grd2.Cells[Row2, 4].Value = Sm.GetGrdStr(Grd1, Row, 15);
                Grd2.Cells[Row2, 5].Value = Grd2.Cells[Row2, 6].Value = Sm.GetGrdDec(Grd1, Row, 14);
                Grd2.Cells[Row2, 7].Value = 0;
                Grd2.Cells[Row2, 8].Value = Sm.GetGrdStr(Grd1, Row, 28);
                Grd2.Cells[Row2, 9].Value = Sm.GetGrdStr(Grd1, Row, 30);
                Grd2.Rows.Add();
            }
            else
            {
                Grd1.Cells[Row, 14].Value = ReceivedQty;
            }
                
            ComputeQty();
        }

        internal void RemovePOInfo(string PODocNo, string PODNo)
        {
            var IsExisted = false;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                {
                    if (
                        Sm.GetGrdStr(Grd1, Row, 5).Length > 0 && 
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 6), PODocNo + PODNo))
                    {
                        IsExisted = true;
                        break;
                    }
                }
            }
            if (!IsExisted)
            {
                for (int Row = Grd2.Rows.Count-1; Row >= 0; Row--)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && 
                        Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 0) + Sm.GetGrdStr(Grd2, Row, 1), PODocNo + PODNo))
                    {
                        Grd2.Rows.RemoveAt(Row);
                        break;
                    }
                }
            }
            else
                ComputeQty();
        }

        internal void ComputeQty()
        {
            decimal ReceivedQty = 0m;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row2 = 0; Row2<= Grd2.Rows.Count - 1; Row2++)
                {
                    if (Sm.GetGrdStr(Grd2, Row2, 0).Length > 0)
                    {
                        ReceivedQty = 0m;
                        for (int Row1 = 0; Row1 <= Grd1.Rows.Count - 1; Row1++)
                        {
                            if (
                                !Sm.GetGrdBool(Grd1, Row1, 1) &&
                                Sm.GetGrdStr(Grd1, Row1, 5).Length > 0 && 
                                Sm.CompareStr(
                                    Sm.GetGrdStr(Grd2, Row2, 0) + Sm.GetGrdStr(Grd2, Row2, 1),
                                    Sm.GetGrdStr(Grd1, Row1, 5) + Sm.GetGrdStr(Grd1, Row1, 6)) &&
                                (!mIsRecvExpeditionEnabled || (mIsRecvExpeditionEnabled && !Sm.GetGrdBool(Grd1, Row1, 47)))
                                )
                                ReceivedQty += Sm.GetGrdDec(Grd1, Row1, 14);
                        }
                        Grd2.Cells[Row2, 6].Value = ReceivedQty;
                        Grd2.Cells[Row2, 7].Value = Sm.GetGrdDec(Grd2, Row2, 5) - ReceivedQty;
                    }
                }
            }
            
        }

        private bool IsPOEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length == 0)
            {
                e.DoDefault = false;
                //Sm.StdMsg(mMsgType.Warning, "PO document number is empty.");
                return true;
            }
            return false;
        }

        private void ComputeOutstandingQty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, ");
            if (mIsRecvVdUsePORevision)
                SQL.AppendLine("(If(A.CancelInd='N', IfNull(Q.Qty, A.Qty), 0)-IfNull(B.Qty2, 0)-IfNull(C.Qty3, 0)+IfNull(D.Qty4, 0)) As OutstandingQty ");
            else
                SQL.AppendLine("(If(A.CancelInd='N', A.Qty, 0)-IfNull(B.Qty2, 0)-IfNull(C.Qty3, 0)+IfNull(D.Qty4, 0)) As OutstandingQty ");
            SQL.AppendLine("From TblPODtl A ");
            SQL.AppendLine("Left Join ( ");
		    SQL.AppendLine("    Select PODocNo As DocNo, PODNo As DNo, Sum(QtyPurchase) As Qty2 ");
		    SQL.AppendLine("    From TblRecvVdDtl ");
            SQL.AppendLine("    Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("    And RejectedInd = 'N' ");
            SQL.AppendLine("    And CancelInd='N' Group By PODocNo, PODNo ");
	        SQL.AppendLine(") B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
	        SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select PODocNo As DocNo, PODNo As DNo, Sum(Qty) As Qty3 ");
            SQL.AppendLine("    From TblPOQtyCancel ");
            SQL.AppendLine("    Where Locate(Concat('##', PODocNo, PODNo, '##'), @Param)>0 ");
            SQL.AppendLine("    And CancelInd='N' Group By PODocNo, PODNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo And A.DNo=C.DNo  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.PODocNo As DocNo, T3.PODNo As DNo, Sum((T3.QtyPurchase/T3.Qty)*T2.Qty) As Qty4 ");
            SQL.AppendLine("    From TblDOVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T3 ");
            SQL.AppendLine("        On T2.RecvVdDocNo=T3.DocNo ");
            SQL.AppendLine("        And T2.RecvVdDNo=T3.DNo ");
            SQL.AppendLine("        And T3.CancelInd='N' ");
            SQL.AppendLine("        And Locate(Concat('##', T3.PODocNo, T3.PODNo, '##'), @Param)>0 ");
            SQL.AppendLine("    Where T1.ReplacedItemInd='Y' And T1.VdCode=@VdCode ");
            SQL.AppendLine("    Group By T3.PODocNo, T3.PODNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            if (mIsRecvVdUsePORevision)
            {
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("    SELECT A.DocNo AS PODoc, A.DNo AS PODNo, B.DocNo AS POREV, B.Qty, B.`Status` ");
                SQL.AppendLine("    FROM tblpodtl A ");
                SQL.AppendLine("    INNER JOIN tblporevision B ON A.DocNo = B.PODocNo AND A.DNo = B.PODNo AND Locate(Concat('##', B.PODocNo, B.PODNo, '##'), @Param)>0 ");
                SQL.AppendLine("    WHERE B.DocNo = (SELECT MAX(docno) FROM tblporevision WHERE podocno = B.podocno AND status = 'A') ");
                SQL.AppendLine(") Q ON A.DocNo = Q.PODoc AND A.DNo = Q.PODNo ");
            }
            SQL.AppendLine("Where Locate(Concat('##', A.DocNo, A.DNo, '##'), @Param)>0 ");
            SQL.AppendLine("Order By A.DocNo;");
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@Param", GetSelectedPO());
                Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "DNo", "OutstandingQty" });

                if (dr.HasRows)
                {
                    Grd2.ProcessTab = true;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row <= Grd2.Rows.Count - 1; Row++)
                        {
                            if (
                                Sm.GetGrdStr(Grd2, Row, 0).Length > 0 && 
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 0), Sm.DrStr(dr, 0)) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd2, Row, 1), Sm.DrStr(dr, 1)))
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, Row, 5, 2);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
            ComputeQty();
        }

        internal string GetSelectedPO()
        {
            var SQL = string.Empty;
            if (Grd2.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                           "##" +
                            Sm.GetGrdStr(Grd2, Row, 0) +
                            Sm.GetGrdStr(Grd2, Row, 1) +
                            "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ParPrint(int parValue)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || (Sm.GetParameter("DocTitle") == "KIM" && ShowPrintApproval()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "RecvVd", "RecvVdDtl", "RecvVdDtl2", "RecvVdDtl3", "RecvVdDtl4", "RecvVdItBarcode", "RecvVdIMS", "RecvVdIMSDtl", "RecvVdSignIMS" };

            string Doctitle = Sm.GetParameter("DocTitle"), mItNameInduk = string.Empty;

            var l = new List<RecvVd>();
            var l2 = new List<RecvVdIMS>();
            var l3 = new List<RecvVdSignIMS>();
            var ldtl = new List<RecvVdDtl>();
            var ldtl2 = new List<RecvVdDtl2>();
            var ldtl3 = new List<RecvVdDtl3>();
            var ldtl4 = new List<RecvVdDtl4>();
            var ldtl5 = new List<RecvVdIMSDtl>();
            var ldtl6 = new List<RecvVdItBarcode>();

            List<IList> myLists = new List<IList>();

            if (Doctitle == "IMS")
            {

                #region Header IMS
                var cm2 = new MySqlCommand();

                var SQL2 = new StringBuilder();

                SQL2.AppendLine("Select @CompanyLogo As CompanyLogo, ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',  ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhoneNumber', ");
                SQL2.AppendLine(" H.ItName, A.DocNo, DATE_FORMAT(A.DocDt, '%d-%b-%Y') DocDt, B.VdName, A.LocalDocNo, C.PODocNo, D.UserName, E.ProjectName, ");
                SQL2.AppendLine(" DATE_FORMAT(F.DocDt, '%d-%b-%Y') ExpeditionDocDt, F.LocalDocNo ExpeditionLocalDocNo, G.DocDt PODocDt  ");
                SQL2.AppendLine("FROM TblRecvVdHdr A ");
                SQL2.AppendLine("INNER JOIN TblVendor B ON A.VdCode = B.VdCode ");

                #region PODocNo Old
                //SQL2.AppendLine("INNER JOIN ");
                //SQL2.AppendLine("( ");
                //SQL2.AppendLine("   SELECT T1.DocNo, GROUP_CONCAT(DISTINCT T1.PODocNo SEPARATOR '\n') PODocNo ");
                //SQL2.AppendLine("   FROM TblRecvVdDtl T1 ");
                //SQL2.AppendLine("   WHERE T1.DocNo = @DocNo ");
                //SQL2.AppendLine("   GROUP BY T1.DocNo ");
                //SQL2.AppendLine(") C ON A.DocNo = C.DocNo ");
                #endregion
                SQL2.AppendLine("INNER JOIN ");
                SQL2.AppendLine("( ");
                SQL2.AppendLine("Select T1.DocNo, GROUP_CONCAT(DISTINCT IFNULL(T4.DocNo, T1.PODocNo) SEPARATOR '\n') PODocNo ");
                SQL2.AppendLine("From TblRecvVdDtl T1  ");
                SQL2.AppendLine("Inner Join tblpodtl T2 ON T1.PODocNo = T2.DocNo AND T1.PODNo = T2.DNo ");
                SQL2.AppendLine("inner Join tblporequestdtl T3 ON T2.PORequestDocNo = T3.DocNo AND T2.PORequestDNo = T3.DNo ");
                SQL2.AppendLine("Left  Join tblmaterialrequestdtl T4 ON T3.MaterialRequestDocNo = T4.DocNo AND T3.MaterialRequestDNo = T4.DNo ");
                SQL2.AppendLine("And T4.MaterialRequestServiceDocNo IS NOT NULL "); 
                SQL2.AppendLine("Where T1.DocNo = @DocNo ");
                SQL2.AppendLine("Group By T1.DocNo ");
                SQL2.AppendLine(") C ON A.DocNo = C.DocNo ");

                
                SQL2.AppendLine("INNER JOIN TblUser D ON A.CreateBy = D.UserCode ");
                SQL2.AppendLine("LEFT JOIN ");
                SQL2.AppendLine("( ");
                SQL2.AppendLine("   SELECT T1.DocNo, GROUP_CONCAT(DISTINCT IFNULL(T8.ProjectName, T7.ProjectName)) ProjectName, ");
                SQL2.AppendLine("   GROUP_CONCAT(DISTINCT (T1.PODocNo)) PODocNo");
                SQL2.AppendLine("   FROM TblRecvVdDtl T1 ");
                SQL2.AppendLine("   INNER JOIN TblPODtl T2 ON T1.PODocNo = T2.DocNo AND T1.PODNo = T2.DNo ");
                SQL2.AppendLine("   AND T1.DocNo = @DocNo ");
                SQL2.AppendLine("   INNER JOIN TblPORequestDtl T3 ON T2.PORequestDocNo = T3.DocNo AND T2.PORequestDNo = T3.DNo ");
                SQL2.AppendLine("   INNER JOIN TblMaterialRequestHdr T4 ON T3.MaterialRequestDocNo = T4.DocNo ");
                SQL2.AppendLine("   INNER JOIN TblSOContractHdr T5 ON T4.SOCDocNo = T5.DocNo ");
                SQL2.AppendLine("   INNER JOIN TblBOQHdr T6 ON T5.BOQDocNo = T6.DOcNo ");
                SQL2.AppendLine("   INNER JOIN TblLOPHdr T7 ON T6.LOPDocNo = T7.DocNo ");
                SQL2.AppendLine("   LEFT JOIN TblProjectGroup T8 ON T7.PGCode = T8.PGCode ");
                SQL2.AppendLine("   GROUP BY T1.DocNo ");
                SQL2.AppendLine(") E ON A.DocNo = E.DocNo ");
                SQL2.AppendLine("LEFT JOIN TblRecvExpeditionHdr F ON A.RecvExpeditionDocNo = F.DocNo ");
                SQL2.AppendLine("LEFT JOIN  ");
                SQL2.AppendLine("( ");
                SQL2.AppendLine("SELECT T1.DocNo, GROUP_CONCAT(DISTINCT DATE_FORMAT(T23.DocDt, '%d-%b-%Y') SEPARATOR '\n') DocDt ");
                SQL2.AppendLine("   FROM TblRecvVdDtl T1  ");
                SQL2.AppendLine("   INNER JOIN TblPODtl T2 ON T1.PODocNo = T2.DocNo AND T1.PODNo = T2.DNo ");
                SQL2.AppendLine("   INNER JOIN TblPOHdr T23 ON T2.DocNo = T23.DocNo  ");
                SQL2.AppendLine("   AND T1.DocNo = @DocNo  ");
                SQL2.AppendLine("   GROUP BY T1.DocNo ");
                SQL2.AppendLine(")G ON G.DocNo=A.DocNo ");
                SQL2.AppendLine("LEFT JOIN ");
                SQL2.AppendLine("( ");
                SQL2.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(DISTINCT T4.ItName SEPARATOR '\n') ItName ");
                SQL2.AppendLine("   FROM TblRecvVdDtl T1  ");
                SQL2.AppendLine("   INNER JOIN tblitem T4 ON T1.ItCode=T4.ItCode  ");
                SQL2.AppendLine("   WHERE T1.DocNo=@DocNo ");
                SQL2.AppendLine("   GROUP BY T1.DocNo ");
                SQL2.AppendLine(")H ON A.DocNo=H.DocNo ");
                SQL2.AppendLine("WHERE A.DocNo = @DocNo ");


                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "DocNo",
                         "DocDt",
                         "VdName",
                         "LocalDocNo",
                         "PODocNo",

                         //6-10
                         "UserName" ,
                         "ProjectName" ,
                         "ExpeditionDocDt",
                         "ExpeditionLocalDocNo",
                         "PODocDt",

                         //11
                         "ItName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhoneNumber"
                         
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new RecvVdIMS()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),
                                DocNo = Sm.DrStr(dr2, c2[1]),
                                DocDt = Sm.DrStr(dr2, c2[2]),
                                VdName = Sm.DrStr(dr2, c2[3]),
                                LocalDocNo = Sm.DrStr(dr2, c2[4]),
                                PODocNo = Sm.DrStr(dr2, c2[5]),
                                UserName = Sm.DrStr(dr2, c2[6]),
                                ProjectName = Sm.DrStr(dr2, c2[7]),
                                ExpeditionDocDt = Sm.DrStr(dr2, c2[8]),
                                ExpeditionLocalDocNo = Sm.DrStr(dr2, c2[9]),
                                PODocDt = Sm.DrStr(dr2, c2[10]),
                                ItName = Sm.DrStr(dr2, c2[11]),
                                CompanyAddress = Sm.DrStr(dr2, c2[12]),
                                CompanyAddressCity = Sm.DrStr(dr2, c2[13]),
                                CompanyPhoneNumber = Sm.DrStr(dr2, c2[14]),
                            });
                        }
                    }
                    dr2.Close();
                }
                myLists.Add(l2);
                #endregion

                #region Detail IMS
                var cmDtl5 = new MySqlCommand();

                var SQLDtl5 = new StringBuilder();
                SQLDtl5.AppendLine("SET @Row=0;  ");
                SQLDtl5.AppendLine("SELECT ");
                if (mMenuCodeForRecvVdAutoCreateDO)
                    SQLDtl5.AppendLine("G.LocalDocNo As RecvExpLocalDocNo, F.VdDONo, DATE_FORMAT(D.EstRecvDt, '%d-%b-%Y') EstRecvDt, (@ROW:=@ROW+1) Number, D5.ItCode ItCodeInduk, D5.ItName ItNameInduk,  ");
                else
                    SQLDtl5.AppendLine("Null As RecvExpLocalDocNo, Null As VdDONo, Null As  EstRecvDt, (@ROW:=@ROW+1) Number, Null As  ItCodeInduk, Null As  ItNameInduk,  ");

                SQLDtl5.AppendLine("B.ItCodeInternal, B.ItName, B.Specification, A.Qty, A.QtyPurchase, B.InventoryUomCode Uom, A.Remark, A.BatchNo, ");
                SQLDtl5.AppendLine("D.Qty POQty, IfNull(E.Qty, 0.00) POQtyCancel, ");
                SQLDtl5.AppendLine("D.Qty-IfNull((  ");
                SQLDtl5.AppendLine("    Select Sum(T.QtyPurchase) From TblRecvExpeditionDtl T  ");
                SQLDtl5.AppendLine("    Where T.CancelInd='N' And T.Status = 'A' And T.PODocNo=D.DocNo And T.PODNo=D.DNo And T.DocNo<>@RecvExpeditionDocNo ");
                SQLDtl5.AppendLine("    ), 0.00) -  ");
                SQLDtl5.AppendLine("    IfNull((  ");
                SQLDtl5.AppendLine("        Select Sum(T1.Qty) From TblPOQtyCancel T1 ");
                SQLDtl5.AppendLine("        Where T1.CancelInd='N' And T1.PODocNo=D.DocNo And T1.PODNo=D.DNo ");
                SQLDtl5.AppendLine("    ), 0.00) ");
                SQLDtl5.AppendLine("As OutstandingQty, ");
                SQLDtl5.AppendLine("( ");
                //SQLDtl5.AppendLine("    Select Sum(T.QtyPurchase) From TblRecvExpeditionDtl T  ");
                //SQLDtl5.AppendLine("    Where T.CancelInd='N' And T.Status = 'A' And T.PODocNo=D.DocNo And T.PODNo=D.DNo And T.DocNo=@RecvExpeditionDocNo ");
                SQLDtl5.AppendLine("    Select IfNull(Sum(T.QtyPurchase), 0.00) ");
                SQLDtl5.AppendLine("    From TblRecvExpeditionDtl T ");
                SQLDtl5.AppendLine("    Inner Join TblRecvVdDtl T1 On T.DocNo = T1.RecvExpeditionDocNo And T.DNo = T1.RecvExpeditionDNo ");
                SQLDtl5.AppendLine("        And T1.DocNo = @DocNo ");
                SQLDtl5.AppendLine("        And T1.RejectedInd = 'N' ");
                SQLDtl5.AppendLine("    Where T.CancelInd='N' And T.Status = 'A' And T.PODocNo=D.DocNo And T.PODNo=D.DNo And T.DocNo=@RecvExpeditionDocNo ");
                SQLDtl5.AppendLine(") As ReceivedQty, IfNull(F.TotalRecvQty, 0.00) TotalRecvQty, A.PODocNo, A.PODNo, H.ProjectName, A.RejectedInd ");
                if (mDoctitle == "IMS" && !mMenuCodeForRecvVdAutoCreateDO)
                    SQLDtl5.AppendLine(", case when I.QtyReject < 0 then 0.00 ELSE IFNULL(I.QtyReject, 0.00 ) end as QtyReject  ");
                else
                    SQLDtl5.AppendLine(", 0 QtyReject ");
                    
                SQLDtl5.AppendLine("FROM TblRecvVdDtl A ");
                SQLDtl5.AppendLine("INNER Join TblItem B ON A.ItCode = B.ItCode ");
                SQLDtl5.AppendLine("Inner Join TblPODtl D On A.PODocNo = D.DocNo And A.PODNo = D.DNo ");
                if (mMenuCodeForRecvVdAutoCreateDO)
                {
                    SQLDtl5.AppendLine("Inner Join tblporequestdtl D2 ON D.PORequestDocNo = D2.DocNo AND D.PORequestDNo = D2.DNo  ");
                    SQLDtl5.AppendLine("Inner Join tblmaterialrequestdtl D3 ON D2.MaterialRequestDocNo = D3.DocNo AND D2.MaterialRequestDNo = D3.DNo  ");
                    SQLDtl5.AppendLine("INNER Join tblmaterialrequestservicedtl D4 ON D3.MaterialRequestServiceDocNo = D4.DocNo AND D3.MaterialRequestServiceDNo = D4.DNo ");
                    SQLDtl5.AppendLine("inner JOIN  ");
                    SQLDtl5.AppendLine("( ");
                    SQLDtl5.AppendLine("SELECT A.ItCode, A.Itname ");
                    SQLDtl5.AppendLine("FROM tblitem A  ");
                    SQLDtl5.AppendLine(")D5 ON D4.ItCode = D5.ItCode ");
                }
                SQLDtl5.AppendLine("AND A.DocNo = @DocNo ");
                SQLDtl5.AppendLine("Left Join TblPOQtyCancel E On E.PODocNo=D.DocNo And E.PODNo=D.DNo ");
                SQLDtl5.AppendLine("Left Join ");
                SQLDtl5.AppendLine("( ");
                SQLDtl5.AppendLine("    Select T1.PODocNo, T1.PODNo, Sum(Qty) TotalRecvQty, T2.VdDONo ");
                SQLDtl5.AppendLine("    From TblRecvVdDtl T1 ");
                SQLDtl5.AppendLine("    Inner Join TblRecvVdHdr T2 On T1.DocNo = T2.DocNo ");
                SQLDtl5.AppendLine("    Where T1.CancelInd = 'N' ");
                SQLDtl5.AppendLine("    And T1.Status In ('O', 'A') ");
                SQLDtl5.AppendLine("    And T1.RejectedInd = 'N' ");
                SQLDtl5.AppendLine("    And Find_In_Set(Concat(T1.PODocNo, T1.PODNo), @PODocNoDNo) ");
                SQLDtl5.AppendLine("    And Concat(T2.DocDt, Left(T2.DocNo, 4)) <= @MaxDocNoDt ");
                SQLDtl5.AppendLine("    Group By T1.PODocNo, T1.PODNo ");
                SQLDtl5.AppendLine(") F On A.PODocNo = F.PODocNo And A.PODNo = F.PODNo ");
                SQLDtl5.AppendLine("INNER JOIN tblrecvexpeditionhdr G ON A.RecvExpeditionDocNo=G.DocNo ");
                SQLDtl5.AppendLine("Left JOIN TblProjectGroup H ON A.BatchNo=H.ProjectCode ");
                if (mDoctitle == "IMS" && !mMenuCodeForRecvVdAutoCreateDO)
                {
                    SQLDtl5.AppendLine("Left Join  ");
                    SQLDtl5.AppendLine("( ");
                    SQLDtl5.AppendLine("Select A.itcode, A.BatchNo, I.qtypurchase - sum(A.qtypurchase)  QtyReject  ");
                    SQLDtl5.AppendLine("From tblrecvvddtl A  ");
                    SQLDtl5.AppendLine("Left Join TblRecvExpeditionDtl I  ");
                    SQLDtl5.AppendLine("    On A.RecvExpeditionDocNo=I.DocNo  ");
                    SQLDtl5.AppendLine("    And A.RecvExpeditionDNo=I.DNo "); 
	                SQLDtl5.AppendLine("     Where A.docno =@DocNo ");
                    SQLDtl5.AppendLine("     Group By A.itcode, A.BatchNo ");
                    SQLDtl5.AppendLine(") I On A.ItCode=I.ItCode And A.BatchNo=I.BatchNo ");
                }
                SQLDtl5.AppendLine("ORDER BY A.DNo; ");

                using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl5.Open();
                    cmDtl5.Connection = cnDtl5;
                    cmDtl5.CommandText = SQLDtl5.ToString();
                    Sm.CmParam<String>(ref cmDtl5, "@PODocNoDNo", GetPODocNoDNo(TxtDocNo.Text));
                    Sm.CmParam<String>(ref cmDtl5, "@MaxDocNoDt", GetMaxDocNoDt(TxtDocNo.Text));
                    Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmDtl5, "@RecvExpeditionDocNo", TxtRecvExpeditionDocNo.Text);
                    var drDtl5 = cmDtl5.ExecuteReader();
                    var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                    {
                        //0
                        "ItCodeInternal",

                        //1-5
                        "ItName",
                        "Specification",
                        "Qty",
                        "UOM",
                        "Remark",

                        //6-10
                        "POQty",
                        "BatchNo",
                        "QtyPurchase",
                        "OutstandingQty",
                        "ReceivedQty",

                        //11-15
                        "POQtyCancel",
                        "TotalRecvQty",
                        "PODocNo",
                        "PODNo",
                        "ItCodeInduk",

                        //16-20
                        "ItNameInduk",
                        "EstRecvDt",
                        "VdDONo",
                        "RecvExpLocalDocNo",
                        "ProjectName",

                        //21-22
                        "QtyReject",
                        "RejectedInd"
                    });
                    int numb;
                    if (mMenuCodeForRecvVdAutoCreateDO)
                        numb = 1;
                    else
                        numb = 0;

                    int nomor = numb;
                    int nomor2 = 0;
                    if (mMenuCodeForRecvVdAutoCreateDO)
                    {
                        if (drDtl5.HasRows)
                        {
                            while (drDtl5.Read())
                            {
                                if (mItNameInduk == Sm.DrStr(drDtl5, cDtl5[15]))
                                {
                                    // nomor = nomor + 1;
                                    ldtl5.Add(new RecvVdIMSDtl()
                                    {
                                        Nomor = nomor,
                                        Nomor2 = nomor2,
                                        ItCodeInternal = Sm.DrStr(drDtl5, cDtl5[0]),
                                        ItName = Sm.DrStr(drDtl5, cDtl5[1]),
                                        Specification = Sm.DrStr(drDtl5, cDtl5[2]),
                                        Qty = Sm.DrDec(drDtl5, cDtl5[3]),
                                        UOM = Sm.DrStr(drDtl5, cDtl5[4]),
                                        Remark = Sm.DrStr(drDtl5, cDtl5[5]),
                                        POQty = Sm.DrDec(drDtl5, cDtl5[6]),
                                        QtyPurchase = Sm.DrDec(drDtl5, cDtl5[8]),
                                        //OutstandingQty = Sm.DrDec(drDtl5, cDtl5[9])-Sm.DrDec(drDtl5, cDtl5[10]),
                                        OutstandingQty = Sm.DrDec(drDtl5, cDtl5[9]),
                                        Project = Sm.DrStr(drDtl5, cDtl5[7]),
                                        //POQtyCancel = Sm.DrDec(drDtl5, cDtl5[11]),
                                        POQtyCancel = Sm.DrDec(drDtl5, cDtl5[6]) - Sm.DrDec(drDtl5, cDtl5[11]),
                                        ReceivedQty = Sm.DrDec(drDtl5, cDtl5[10]),
                                        BalanceQty = Sm.DrDec(drDtl5, cDtl5[9]) - Sm.DrDec(drDtl5, cDtl5[10]),
                                        TotalRecvQty = Sm.DrDec(drDtl5, cDtl5[12]),
                                        TotalUnreceivedQty = Sm.DrDec(drDtl5, cDtl5[6]) - Sm.DrDec(drDtl5, cDtl5[11]) - Sm.DrDec(drDtl5, cDtl5[12]),
                                        PODocNo = Sm.DrStr(drDtl5, cDtl5[13]),
                                        PODNo = Sm.DrStr(drDtl5, cDtl5[14]),
                                        ItCodeInduk = Sm.DrStr(drDtl5, cDtl5[15]),
                                        ItNameInduk = Sm.DrStr(drDtl5, cDtl5[16]),
                                        EstRecvDt = Sm.DrStr(drDtl5, cDtl5[17]),
                                        VdDONo = Sm.DrStr(drDtl5, cDtl5[18]),
                                        RecvExpLocalDocNo = Sm.DrStr(drDtl5, cDtl5[19]),
                                        ProjectName = Sm.DrStr(drDtl5, cDtl5[20]),
                                    });
                                    nomor++;
                                }
                                else
                                {
                                    mItNameInduk = Sm.DrStr(drDtl5, cDtl5[15]);
                                    nomor = 1;
                                    nomor2 = nomor2 + 1;
                                    ldtl5.Add(new RecvVdIMSDtl()
                                    {
                                        Nomor = nomor,
                                        Nomor2 = nomor2,
                                        ItCodeInternal = Sm.DrStr(drDtl5, cDtl5[0]),
                                        ItName = Sm.DrStr(drDtl5, cDtl5[1]),
                                        Specification = Sm.DrStr(drDtl5, cDtl5[2]),
                                        Qty = Sm.DrDec(drDtl5, cDtl5[3]),
                                        UOM = Sm.DrStr(drDtl5, cDtl5[4]),
                                        Remark = Sm.DrStr(drDtl5, cDtl5[5]),
                                        POQty = Sm.DrDec(drDtl5, cDtl5[6]),
                                        QtyPurchase = Sm.DrDec(drDtl5, cDtl5[8]),
                                        //OutstandingQty = Sm.DrDec(drDtl5, cDtl5[9])-Sm.DrDec(drDtl5, cDtl5[10]),
                                        OutstandingQty = Sm.DrDec(drDtl5, cDtl5[9]),
                                        Project = Sm.DrStr(drDtl5, cDtl5[7]),
                                        //POQtyCancel = Sm.DrDec(drDtl5, cDtl5[11]),
                                        POQtyCancel = Sm.DrDec(drDtl5, cDtl5[6]) - Sm.DrDec(drDtl5, cDtl5[11]),
                                        ReceivedQty = Sm.DrDec(drDtl5, cDtl5[10]),
                                        BalanceQty = Sm.DrDec(drDtl5, cDtl5[9]) - Sm.DrDec(drDtl5, cDtl5[10]),
                                        TotalRecvQty = Sm.DrDec(drDtl5, cDtl5[12]),
                                        TotalUnreceivedQty = Sm.DrDec(drDtl5, cDtl5[6]) - Sm.DrDec(drDtl5, cDtl5[11]) - Sm.DrDec(drDtl5, cDtl5[12]),
                                        PODocNo = Sm.DrStr(drDtl5, cDtl5[13]),
                                        PODNo = Sm.DrStr(drDtl5, cDtl5[14]),
                                        ItCodeInduk = Sm.DrStr(drDtl5, cDtl5[15]),
                                        ItNameInduk = Sm.DrStr(drDtl5, cDtl5[16]),
                                        EstRecvDt = Sm.DrStr(drDtl5, cDtl5[17]),
                                        VdDONo = Sm.DrStr(drDtl5, cDtl5[18]),
                                        RecvExpLocalDocNo = Sm.DrStr(drDtl5, cDtl5[19]),
                                        ProjectName = Sm.DrStr(drDtl5, cDtl5[20]),

                                    });
                                    nomor++;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (drDtl5.HasRows)
                        {
                            while (drDtl5.Read())
                            {
                                nomor = nomor + 1;
                                ldtl5.Add(new RecvVdIMSDtl()
                                {
                                    Nomor = nomor,
                                    ItCodeInternal = Sm.DrStr(drDtl5, cDtl5[0]),
                                    ItName = Sm.DrStr(drDtl5, cDtl5[1]),
                                    Specification = Sm.DrStr(drDtl5, cDtl5[2]),
                                    Qty = Sm.DrDec(drDtl5, cDtl5[3]),
                                    UOM = Sm.DrStr(drDtl5, cDtl5[4]),
                                    Remark = Sm.DrStr(drDtl5, cDtl5[5]),
                                    POQty = Sm.DrDec(drDtl5, cDtl5[6]),
                                    QtyPurchase = Sm.DrDec(drDtl5, cDtl5[8]),
                                    //OutstandingQty = Sm.DrDec(drDtl5, cDtl5[9])-Sm.DrDec(drDtl5, cDtl5[10]),
                                    OutstandingQty = Sm.DrDec(drDtl5, cDtl5[9]),
                                    Project = Sm.DrStr(drDtl5, cDtl5[7]),
                                    //POQtyCancel = Sm.DrDec(drDtl5, cDtl5[11]),
                                    POQtyCancel = Sm.DrDec(drDtl5, cDtl5[6]) - Sm.DrDec(drDtl5, cDtl5[11]),
                                    ReceivedQty = Sm.DrDec(drDtl5, cDtl5[10]),
                                    BalanceQty = Sm.DrDec(drDtl5, cDtl5[9]) - Sm.DrDec(drDtl5, cDtl5[10]),
                                    TotalRecvQty = Sm.DrDec(drDtl5, cDtl5[12]),
                                    TotalUnreceivedQty = Sm.DrDec(drDtl5, cDtl5[6]) - Sm.DrDec(drDtl5, cDtl5[11]) - Sm.DrDec(drDtl5, cDtl5[12]),
                                    PODocNo = Sm.DrStr(drDtl5, cDtl5[13]),
                                    PODNo = Sm.DrStr(drDtl5, cDtl5[14]),
                                    ProjectName = Sm.DrStr(drDtl5, cDtl5[20]),
                                    QtyReject = Sm.DrDec(drDtl5, cDtl5[21]),
                                    RejectedInd = Sm.DrStr(drDtl5, cDtl5[22]),


                                });
                            }
                        }
                    }
                    drDtl5.Close();
                }

                ProcessFromGrd2(ref ldtl5);

                myLists.Add(ldtl5);
                #endregion

                #region Signature IMS
                var cm3 = new MySqlCommand();
                var SQL3 = new StringBuilder();
                if (mMenuCodeForRecvVdAutoCreateDO)
                {
                    SQL3.AppendLine("SELECT null as UserCode, (SELECT parvalue FROM tblparameter WHERE parcode ='RecvVdPrintoutSignName') UserName1, ");
                    SQL3.AppendLine("(SELECT parvalue FROM tblparameter WHERE parcode ='RecvVdPrintoutSignName2') UserName2, ");
                    SQL3.AppendLine("(SELECT parvalue FROM tblparameter WHERE parcode ='RecvVdPrintoutSignName3') UserName3, ");
                    SQL3.AppendLine("(SELECT parvalue FROM tblparameter WHERE parcode ='RecvVdPrintoutSignPosition') POSITION1, ");
                    SQL3.AppendLine("(SELECT parvalue FROM tblparameter WHERE parcode ='RecvVdPrintoutSignPosition2') POSITION2, ");
                    SQL3.AppendLine("(SELECT parvalue FROM tblparameter WHERE parcode ='RecvVdPrintoutSignPosition3') POSITION3 ");
                }
                else
                {
                    SQL3.AppendLine("SELECT A.UserCode , Null As UserName1, Null As UserName2, Null As UserName3, Null As POSITION1, Null As POSITION2, Null As POSITION3 ");
                    SQL3.AppendLine("FROM TblDocApproval A ");
                    SQL3.AppendLine("INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType AND A.ApprovalDNo = B.DNo ");
                    SQL3.AppendLine("WHERE B.Level = ");
                    SQL3.AppendLine("( ");
                    SQL3.AppendLine("    SELECT MAX(T1.Level) Level FROM tbldocapprovalsetting T1 ");
                    SQL3.AppendLine("    INNER JOIN tbldocapproval T2 ON T1.DocType = T2.DocType AND T1.DNo = T2.ApprovalDNo ");
                    SQL3.AppendLine("    WHERE T2.DocNo = @DocNo ");
                    SQL3.AppendLine("    AND T2.UserCode IS NOT NULL ");
                    SQL3.AppendLine(") ");
                    SQL3.AppendLine("AND A.DocType = 'RecvVd' ");
                }


                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0
                         "UserCode",

                         //1-5
                         "UserName1",
                         "UserName2",
                         "UserName3",
                         "POSITION1",
                         "POSITION2",

                         //6
                         "POSITION3"
                         
                        });
                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            l3.Add(new RecvVdSignIMS()
                            {
                                UserCode = Sm.DrStr(dr3, c3[0]),

                                UserName1 = Sm.DrStr(dr3, c3[1]),
                                UserName2 = Sm.DrStr(dr3, c3[2]),
                                UserName3 = Sm.DrStr(dr3, c3[3]),
                                POSITION1 = Sm.DrStr(dr3, c3[4]),
                                POSITION2 = Sm.DrStr(dr3, c3[5]),

                                POSITION3 = Sm.DrStr(dr3, c3[6]),

                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(l3);

                #endregion

                if (mMenuCodeForRecvVdAutoCreateDO)
                    Sm.PrintReport("RecvVdIMS_2", myLists, TableName, false);
                else
                    Sm.PrintReport("RecvVdIMS", myLists, TableName, false);
            }
            else
            {
                #region Header
                var cm = new MySqlCommand();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
                SQL.AppendLine("If((Select parvalue From TblParameter Where ParCode = 'IsFilterLocalDocNo')='Y',A.LocalDocNo,A.Docno) As DocNo, ");
                //SQL.AppendLine("Date_Format(A.DocDt,'%d %M %Y') As DocDt,");
                //SQL.AppendLine("B.WhsName,C.VdName,A.VdDoNo,A.Remark as Remark ");
                SQL.AppendLine("Date_Format(A.DocDt,'%d %M %Y') As DocDt, B.WhsName, C.VdName, A.VdDoNo, A.Remark, ");
                SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowForeignName') As IsForeignName, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'RecvVdNotes') As RecvVdNotes, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'AddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'Phone2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'Fax', D.PODocNo, C.Address aS aDDRESSvD, A.VdDONo, ");
                SQL.AppendLine("E.CityName AS VdCtname,  D.PODocDt,D.PORemarkDtl, D.VdContactPerson, F.UserName AS RecvUsername, G.GrpName AS RecvUserGroup ");
                SQL.AppendLine(", H.RecvUserSign ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
                SQL.AppendLine("inner Join TblVendor C On A.VdCode=C.VdCode ");
                SQL.AppendLine("Left join( ");
                SQL.AppendLine("    SELECT A.DocNo, Group_Concat(DISTINCT A.PODOcNo order BY A.PODocNo Separator ', ') As PODocNo, ");
                SQL.AppendLine("    Group_Concat(DISTINCT C.DocDt Separator ', ') As PODocDt, ");
                SQL.AppendLine("    Group_Concat(DISTINCT B.Remark Separator ', ') As PORemarkDtl, ");
                SQL.AppendLine("    Group_Concat(DISTINCT C.VdContactPerson Separator ', ') As VdContactPerson ");
                SQL.AppendLine("    from TblRecvVdDtl A ");
                SQL.AppendLine("    INNER JOIN tblpodtl B ON A.PODocNO = B.DocNo  AND A.PODno = B.Dno ");
                SQL.AppendLine("    INNER JOIN tblpohdr C ON B.DocNo = C.DocNo ");
                SQL.AppendLine("    Group BY A.DocNo ");
                SQL.AppendLine(")D On A.DocNo = D.DocNo ");
                SQL.AppendLine("LEFT JOIN tblcity E ON C.CityCode = E.CityCode ");
                SQL.AppendLine("INNER JOIN tbluser F ON A.CreateBy = F.UserCode ");
                SQL.AppendLine("INNER JOIN tblgroup G ON F.GrpCode = G.GrpCode ");
                SQL.AppendLine("LEFT JOIN(  ");
                SQL.AppendLine("    SELECT X1.UserCode, Concat(IfNull(X2.ParValue, ''), X1.UserCode, '.JPG') AS RecvUserSign  ");
                SQL.AppendLine("    from tbluser X1  ");
                SQL.AppendLine("    left JOIN tblparameter X2 ON X2.ParCode = 'ImgFileSignature'  ");
                SQL.AppendLine(")H ON F.UserCode = H.UserCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Company",
                         //1-5
                         "Address",
                         "Phone",
                         "DocNo",
                         "DocDt",
                         "WhsName",
                         //6-10
                         "VdName" ,
                         "VdDoNo" ,
                         "Remark",
                         "CompanyLogo",
                         "IsForeignName",
                         //11-15
                         "RecvVdNotes",
                         "AddressCity",
                         "Phone2",
                         "Fax",
                         "PODocNo",
                         //16-20
                         "AddressvD",
                         "VdDONo",
                         "VdCtname",
                         "PODocDt",
                         "PORemarkDtl",
                         //21-24
                         "VdContactPerson",
                         "RecvUsername",
                         "RecvUserGroup",
                         "RecvUserSign"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new RecvVd()
                            {
                                Company = Sm.DrStr(dr, c[0]),
                                Address = Sm.DrStr(dr, c[1]),
                                Phone = Sm.DrStr(dr, c[2]),
                                DocNo = Sm.DrStr(dr, c[3]),
                                DocDt = Sm.DrStr(dr, c[4]),
                                WhsName = Sm.DrStr(dr, c[5]),
                                VdName = Sm.DrStr(dr, c[6]),
                                VdDoNo = Sm.DrStr(dr, c[7]),
                                Remark = Sm.DrStr(dr, c[8]),
                                CompanyLogo = Sm.DrStr(dr, c[9]),
                                IsForeignName = Sm.DrStr(dr, c[10]),
                                RecvVdNotes = Sm.DrStr(dr, c[11]),
                                AddressCity = Sm.DrStr(dr, c[12]),
                                Phone2 = Sm.DrStr(dr, c[13]),
                                Fax = Sm.DrStr(dr, c[14]),
                                PODocNo = Sm.DrStr(dr, c[15]),
                                AddressVd = Sm.DrStr(dr, c[16]),
                                VdDONo = Sm.DrStr(dr, c[17]),
                                VdCtname = Sm.DrStr(dr, c[18]),
                                PODocDt = Sm.DrStr(dr, c[19]),
                                PORemarkDtl = Sm.DrStr(dr, c[20]),
                                VdContactPerson = Sm.DrStr(dr, c[21]),
                                RecvUsername = Sm.DrStr(dr, c[22]),
                                RecvUserGroup = Sm.DrStr(dr, c[23]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                RecvUserSign = Sm.DrStr(dr, c[24]),
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail
                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;
                    SQLDtl.AppendLine("Select A.PODocNo, ");
                    SQLDtl.AppendLine("D.ItCode, E.ItName, E.ForeignName, A.BatchNo, A.Source, A.Lot, A.Bin, A.QtyPurchase, E.PurchaseUomCode, ");
                    SQLDtl.AppendLine("A.Qty, E.InventoryUomCode, A.Qty2, E.InventoryUomCode2, A.Qty3, E.InventoryUomCode3, A.Remark, ");
                    SQLDtl.AppendLine("DATE_FORMAT(F.DocDt, '%d/%m/%Y') PODocDt, G.UPrice, (A.QtyPurchase*G.UPrice) Amount ");
                    SQLDtl.AppendLine("From TblRecvVdDtl A ");
                    SQLDtl.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                    SQLDtl.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                    SQLDtl.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                    SQLDtl.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblPOHdr F On A.PODocNo = F.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblQtDtl G On C.QtDocNo = G.DocNo And C.QtDNo = G.DNo ");
                    SQLDtl.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo Order By A.DNo");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "PoDocNo",
                         
                         //1-5
                         "ItCode" ,
                         "ItName",
                         "BatchNo",
                         "Source",
                         "Lot",
                         
                         //6-10
                         "Bin",
                         "QtyPurchase" ,
                         "PurchaseUomCode" ,
                         "Qty" ,
                         "InventoryUomCode" ,
                         
                         //11-15
                         "Qty2" ,
                         "InventoryUomCode2" ,
                         "Qty3" ,
                         "InventoryUomCode3" ,
                         "Remark",

                         //16-17
                         "ForeignName",
                         "PODocDt",
                         "UPrice",
                         "Amount"
                        });
                    if (drDtl.HasRows)
                    {
                        int nomor = 0;
                        while (drDtl.Read())
                        {
                            nomor = nomor + 1;
                            ldtl.Add(new RecvVdDtl()
                            {
                                nomor = nomor,
                                PoDocNo = Sm.DrStr(drDtl, cDtl[0]),
                                ItCode = Sm.DrStr(drDtl, cDtl[1]),
                                ItName = Sm.DrStr(drDtl, cDtl[2]),
                                BatchNo = Sm.DrStr(drDtl, cDtl[3]),
                                Source = Sm.DrStr(drDtl, cDtl[4]),
                                Lot = Sm.DrStr(drDtl, cDtl[5]),
                                Bin = Sm.DrStr(drDtl, cDtl[6]),
                                QtyPurchase = Sm.DrDec(drDtl, cDtl[7]),
                                PurchaseUomCode = Sm.DrStr(drDtl, cDtl[8]),
                                Qty = Sm.DrDec(drDtl, cDtl[9]),
                                InventoryUomCode = Sm.DrStr(drDtl, cDtl[10]),
                                Qty2 = Sm.DrDec(drDtl, cDtl[11]),
                                InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[12]),
                                Qty3 = Sm.DrDec(drDtl, cDtl[13]),
                                InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[14]),
                                Remark2 = Sm.DrStr(drDtl, cDtl[15]),
                                ForeignName = Sm.DrStr(drDtl, cDtl[16]),
                                PODocDt = Sm.DrStr(drDtl, cDtl[17]),
                                UPrice = Sm.DrDec(drDtl, cDtl[18]),
                                Amount = Sm.DrDec(drDtl, cDtl[19])
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail Signature
                var cmDtl2 = new MySqlCommand();

                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                    SQLDtl2.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict,'" + Doctitle + "'As SignInd ");
                    SQLDtl2.AppendLine("From TblRecvVdHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Where DocNo=@DocNo ");

                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0-1
                         "UserCode" ,
                         "UserName",
                         "EmpPict",
                         "SignInd"
                        });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new RecvVdDtl2()
                            {
                                UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                                UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                                EmpPict = Sm.DrStr(drDtl2, cDtl2[2]),
                                SignInd = Sm.DrStr(drDtl2, cDtl2[3]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Detail Approve
                if(Doctitle != "MNET")
                {
                    var cmDtl3 = new MySqlCommand();

                    var SQLDtl3 = new StringBuilder();
                    using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl3.Open();
                        cmDtl3.Connection = cnDtl3;

                        SQLDtl3.AppendLine("Select A.ApprovalDno,  A.UserCode,  B.UserName, ");
                        SQLDtl3.AppendLine("Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict,'" + Doctitle + "'As SignInd ");
                        SQLDtl3.AppendLine("from TblDocApproval A ");
                        SQLDtl3.AppendLine("Inner Join TblUser B On A.UserCode = B.UserCode ");
                        SQLDtl3.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                        SQLDtl3.AppendLine("Where DocType = 'RecvVd' ");
                        SQLDtl3.AppendLine("And DocNo =@DocNo ");
                        SQLDtl3.AppendLine("Group by ApprovalDno Order by ApprovalDno Desc limit 1");

                        cmDtl3.CommandText = SQLDtl3.ToString();
                        Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                        var drDtl3 = cmDtl3.ExecuteReader();
                        var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                            {
                         //0-3
                         "ApprovalDNo" ,
                         "UserCode",
                         "UserName",
                         "EmpPict",
                         "SignInd"

                            });
                        if (drDtl3.HasRows)
                        {
                            while (drDtl3.Read())
                            {
                                ldtl3.Add(new RecvVdDtl3()
                                {
                                    ApprovalDNo = Sm.DrStr(drDtl3, cDtl3[0]),
                                    UserCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                    UserName = Sm.DrStr(drDtl3, cDtl3[2]),
                                    EmpPict = Sm.DrStr(drDtl3, cDtl3[3]),
                                    SignInd = Sm.DrStr(drDtl3, cDtl3[4]),
                                    // StatusDesc = Sm.DrStr(drDtl3, cDtl3[4]),
                                });
                            }
                        }
                        drDtl3.Close();
                    }
                    myLists.Add(ldtl3);
                }
                else
                {
                    var cmDtl3 = new MySqlCommand();

                    var SQLDtl3 = new StringBuilder();
                    using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl3.Open();
                        cmDtl3.Connection = cnDtl3;

                        SQLDtl3.AppendLine("Select T1.UserName, T2.GrpName As PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                        SQLDtl3.AppendLine("From ( ");
                        SQLDtl3.AppendLine("	Select Distinct ");
                        SQLDtl3.AppendLine("	B.UserName, B.UserCode, B.GrpCode, '00' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                        SQLDtl3.AppendLine("	From TblRecvVdHdr A ");
                        SQLDtl3.AppendLine("	Inner Join TblUser B On A.CreateBy=B.UserCode ");
                        SQLDtl3.AppendLine("	Where A.DocNo=@DocNo ");
                        SQLDtl3.AppendLine("    Union All ");
                        SQLDtl3.AppendLine("	Select Distinct ");
                        SQLDtl3.AppendLine("	C.UserName, B.UserCode, C.GrpCode, B.ApprovalDNo As DNo, D.Level, 'Approved By, ' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                        SQLDtl3.AppendLine("	From TblRecvVdHdr A ");
                        SQLDtl3.AppendLine("	Inner Join TblDocApproval B On B.DocType='RecvVd' And A.DocNo=B.DocNo ");
                        SQLDtl3.AppendLine("	Inner Join TblUser C On B.UserCode=C.UserCode ");
                        SQLDtl3.AppendLine("	Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And B.DocType = D.DocType ");
                        SQLDtl3.AppendLine("	Where A.DocNo=@DocNo And B.LastUpDt Is Not Null ");
                        SQLDtl3.AppendLine(") T1 ");
                        SQLDtl3.AppendLine("Inner Join TblGroup T2 On T1.GrpCode = T2.GrpCode ");
                        SQLDtl3.AppendLine("Group  By T1.UserName, T2.GrpName ");

                        cmDtl3.CommandText = SQLDtl3.ToString();
                        Sm.CmParam<String>(ref cmDtl3, "@Space", "-------------------------");
                        Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                        var drDtl3 = cmDtl3.ExecuteReader();
                        var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                            {
                                //0
                                "UserName",

                                //1-5
                                "PosName",
                                "Space",
                                "Level",
                                "Title",
                                "LastupDt"
                            });
                        if (drDtl3.HasRows)
                        {
                            while (drDtl3.Read())
                            {
                                ldtl3.Add(new RecvVdDtl3()
                                {
                                    UserName = Sm.DrStr(drDtl3, cDtl3[0]),
                                    PosName = Sm.DrStr(drDtl3, cDtl3[1]),
                                    Space = "",
                                    DNo = Sm.DrStr(drDtl3, cDtl3[3]),
                                    Title = Sm.DrStr(drDtl3, cDtl3[4]),
                                    LastUpDt = Sm.DrStr(drDtl3, cDtl3[5]),
                                });
                            }
                        }
                        drDtl3.Close();
                    }
                    myLists.Add(ldtl3);
                }
                #endregion

                #region Detail Signature [kim]

                var cmDtl4 = new MySqlCommand();

                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    SQLDtl4.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl4.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt ");
                    SQLDtl4.AppendLine("From ( ");
                    SQLDtl4.AppendLine("    Select Distinct ");
                    SQLDtl4.AppendLine("    B.UserCode, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName, ");
                    SQLDtl4.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Acknowledge By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl4.AppendLine("    From TblRecvVdDtl A ");
                    SQLDtl4.AppendLine("    Inner Join TblDocApproval B On B.DocType='RecvVd' And A.DocNo=B.DocNo And A.DNo=B.DNo ");
                    SQLDtl4.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl4.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'RecvVd' ");
                    SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl4.AppendLine("    Union All ");
                    SQLDtl4.AppendLine("    Select Distinct ");
                    SQLDtl4.AppendLine("    A.CreateBy As UserCode, Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName))) As UserName, ");
                    SQLDtl4.AppendLine("    '00' As DNo, 0 As Level, 'Received By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQLDtl4.AppendLine("    From TblRecvVdDtl A ");
                    SQLDtl4.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    SQLDtl4.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl4.AppendLine(") T1 ");
                    SQLDtl4.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl4.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl4.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl4.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                    SQLDtl4.AppendLine("Order By T1.DNo desc; ");

                    cmDtl4.CommandText = SQLDtl4.ToString();
                    Sm.CmParam<String>(ref cmDtl4, "@Space", "-------------------------");
                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",
                         "LastupDt"
                        });
                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {

                            ldtl4.Add(new RecvVdDtl4()
                            {
                                Signature = Sm.DrStr(drDtl4, cDtl4[0]),
                                UserName = Sm.DrStr(drDtl4, cDtl4[1]),
                                PosName = Sm.DrStr(drDtl4, cDtl4[2]),
                                Space = Sm.DrStr(drDtl4, cDtl4[3]),
                                DNo = Sm.DrStr(drDtl4, cDtl4[4]),
                                Title = Sm.DrStr(drDtl4, cDtl4[5]),
                                LastUpDt = Sm.DrStr(drDtl4, cDtl4[6])
                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);
                #endregion

                #region ItemBarcode

                var cmDtl6 = new MySqlCommand();

                var SQLDtl6 = new StringBuilder();
                using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl6.Open();
                    cmDtl6.Connection = cnDtl6;

                    SQLDtl6.AppendLine("SELECT A.DocNo, E.ItCode, E.Itname, F.OptDesc As Size, G.OptDesc As Color ");
                    SQLDtl6.AppendLine("From TblRecvVdDtl A  ");
                    SQLDtl6.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                    SQLDtl6.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                    SQLDtl6.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                    SQLDtl6.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
                    SQLDtl6.AppendLine("Left Join TblOption F On F.OptCat='ItemInformation1' And E.Information1=F.OptCode ");
                    SQLDtl6.AppendLine("Left Join TblOption G On G.OptCat='ItemInformation2' And E.Information2=G.OptCode ");
                    SQLDtl6.AppendLine("WHERE A.DocNo=@DocNo ");

                    cmDtl6.CommandText = SQLDtl6.ToString();

                    Sm.CmParam<String>(ref cmDtl6, "@DocNo", TxtDocNo.Text);
                    var drDtl6 = cmDtl6.ExecuteReader();
                    var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                        {
                          //0
                         "DocNo",

                         //1-5
                         "ItCode",
                         "ItName",
                         "Size",
                         "Color",
                         //"ForeignName",
                         //"Dno",
                        });
                    if (drDtl6.HasRows)
                    {
                        while (drDtl6.Read())
                        {

                            ldtl6.Add(new RecvVdItBarcode()
                            {

                                DocNo = Sm.DrStr(drDtl6, cDtl6[0]),

                                ItCode = Sm.DrStr(drDtl6, cDtl6[1]),
                                ItName = Sm.DrStr(drDtl6, cDtl6[2]),
                                Size = Sm.DrStr(drDtl6, cDtl6[3]),
                                Color = Sm.DrStr(drDtl6, cDtl6[4]),
                                //ForeignName = Sm.DrStr(dr, c[5]),
                                //Dno = Sm.DrStr(dr, c[6]),
                            });
                        }
                    }
                    drDtl6.Close();
                }
                myLists.Add(ldtl6);
                #endregion

                if (Sm.GetParameter("DocTitle") == "AWG" || Sm.GetParameter("DocTitle") == "AG-YK")
                {
                    switch (parValue)
                    {
                        case 1:
                            Sm.PrintReport("RecvVdAWG", myLists, TableName, false);
                            break;
                        case 2:
                            Sm.PrintReport("RecvVd2AWG", myLists, TableName, false);
                            break;
                        case 3:
                            Sm.PrintReport("RecvVd3AWG", myLists, TableName, false);
                            break;
                    }
                }
                else
                {
                    string RptName = Sm.GetValue("SELECT ParValue FROM TblParameter WHERE ParCode = 'FormPrintOutRecvVd'");
                    if (RptName.Length > 0)
                    {
                        switch (parValue)
                        {
                            case 1:
                                Sm.PrintReport("RecvVd" + RptName, myLists, TableName, false);
                                break;
                            case 2:
                                Sm.PrintReport("RecvVd2" + RptName, myLists, TableName, false);
                                break;
                            case 3:
                                Sm.PrintReport("RecvVd3" + RptName, myLists, TableName, false);
                                break;
                        }
                    }
                    else
                    {
                        switch (parValue)
                        {
                            case 1:
                                Sm.PrintReport("RecvVd", myLists, TableName, false);
                                break;
                            case 2:
                                Sm.PrintReport("RecvVd2", myLists, TableName, false);
                                break;
                            case 3:
                                Sm.PrintReport("RecvVd3", myLists, TableName, false);
                                break;
                        }
                    }
                    if (Doctitle == "SRN") Sm.PrintReport("RecvVdBarcode", myLists, TableName, true);

                }
            }
        }
        
        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();

            
            SQL.AppendLine("Select T.VdCode As Col1 ");
            if(mIsVendorComboShowCategory)
                SQL.AppendLine(", Concat(T.VdName, ' [', ifnull(T2.VdCtName, ''), ']') As Col2 ");
            else
                SQL.AppendLine(", T.VdName As Col2 ");
            SQL.AppendLine("From TblVendor T ");
            SQL.AppendLine("Left Join TblVendorCategory T2 On T.VdCtCode = T2.VdCtCode ");

            if (VdCode.Length == 0)
            {
                SQL.AppendLine("Where T.VdCode In ( ");
                SQL.AppendLine("    Select Distinct A.VdCode ");
                SQL.AppendLine("    From TblPOHdr A ");
                SQL.AppendLine("    Inner Join TblPODtl B ");
                SQL.AppendLine("        On A.DocNo=B.DocNo ");
                SQL.AppendLine("        And B.CancelInd='N' ");
                SQL.AppendLine("        And B.ProcessInd<>'F' ");
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where T.VdCode='" + VdCode + "' ");
            }
            SQL.AppendLine("Order By T.VdName; ");
            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ShowApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, ");
            SQL.AppendLine("Case Status ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("    Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("Remark  ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='RecvVd' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                         new string[]{ "UserCode", "StatusDesc", "LastUpDt", "Remark" }
                    );
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        private bool ShowPrintApproval()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select Status From TblRecvVddtl " +
                    "Where DocNo=@DocNo And Status ='O';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Can't print. This document has not been approved.");
                return true;
            }
            return false;
        }

        private bool IsCustomsDocCodeInputManual()
        {
            if (mRecvVdCustomsDocCodeManualInput.Length > 0)
            {
                var CustomsDocCode = Sm.GetLue(LueCustomsDocCode);
                if (CustomsDocCode.Length == 0) return false;
                var l = new List<string>(mRecvVdCustomsDocCodeManualInput.Split(',').Select(s => s));
                foreach (var s in l)
                    if (Sm.CompareStr(CustomsDocCode, s)) return true;
            }
            return false;
        }

        internal void ShowRecvExpeditionInfo(string DocNo)
        {
            TxtRecvExpeditionDocNo.EditValue = DocNo;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, D.ItCode, E.ItName, E.ForeignName, E.PurchaseUomCode, ");
            SQL.AppendLine("B.Qty-IfNull(( ");
            SQL.AppendLine("    Select Sum(T.QtyPurchase) From TblRecvExpeditionDtl T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.Status = 'A' And T.PODocNo=A.DocNo And T.PODNo=B.DNo And T.DocNo<>@DocNo ");
            SQL.AppendLine("    ), 0.00) - ");
            SQL.AppendLine("   IfNull(( ");
            SQL.AppendLine("        Select Sum(T1.Qty) From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.PODocNo=B.DocNo And T1.PODNo=B.DNo ");
            SQL.AppendLine("    ), 0.00) ");
            SQL.AppendLine("As OutstandingQty, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Sum(T.QtyPurchase) From TblRecvExpeditionDtl T ");
            SQL.AppendLine("    Where T.CancelInd='N' And T.Status = 'A' And T.PODocNo=A.DocNo And T.PODNo=B.DNo And T.DocNo=@DocNo ");
            SQL.AppendLine(") As ReceivedQty, ");
            SQL.AppendLine("E.ItCodeInternal, E.Specification ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Concat(B.DocNo, B.DNo) In (Select Distinct Concat(PODocNo, PODNo) From TblRecvExpeditionDtl Where DocNo=@DocNo) ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DNo", "ItCode", "ItName", "PurchaseUomCode", "OutstandingQty",

                    //6-8
                    "ReceivedQty", "ItCodeInternal", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Grd.Cells[Row, 7].Value = Sm.GetGrdDec(Grd, Row, 5) - Sm.GetGrdDec(Grd, Row, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);

        }

        private void UploadFile(string DocNo, FileInfo toUpload)
        {
            //if (IsUploadFileNotValid()) return;

            //FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            //var cml = new List<MySqlCommand>();
            //cml.Add(UpdateRecvVdFile(DocNo, toUpload.Name));
            //Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo, FileInfo toUpload)
        {
            //if (IsUploadFileNotValid2()) return;

            //FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            //var cml = new List<MySqlCommand>();
            //cml.Add(UpdateRecvVdFile2(DocNo, toUpload.Name));
            //Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo, FileInfo toUpload)
        {
           // if (IsUploadFileNotValid3()) return;

            //FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            //var cml = new List<MySqlCommand>();
            //cml.Add(UpdateRecvVdFile3(DocNo, toUpload.Name));
            //Sm.ExecCommands(cml);
        }

        private void UploadFile4(string DocNo, FileInfo toUpload)
        {
            //if (IsUploadFileNotValid4()) return;

            //FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile4.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile4.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload4.Invoke(
                    (MethodInvoker)delegate { PbUpload4.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload4.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload4.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            //var cml = new List<MySqlCommand>();
            //cml.Add(UpdateRecvVdFile4(DocNo, toUpload.Name));
            //Sm.ExecCommands(cml);
        }

        private void UploadFile5(string DocNo, FileInfo toUpload)
        {
            //if (IsUploadFileNotValid5()) return;

           // FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile5.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile5.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload5.Invoke(
                    (MethodInvoker)delegate { PbUpload5.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload5.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload5.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            //var cml = new List<MySqlCommand>();
            //cml.Add(UpdateRecvVdFile5(DocNo, toUpload.Name));
            //Sm.ExecCommands(cml);
        }

        private void UploadFile6(string DocNo, FileInfo toUpload)
        {
            //if (IsUploadFileNotValid6()) return;

            //FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile6.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile6.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload6.Invoke(
                    (MethodInvoker)delegate { PbUpload6.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload6.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload6.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            //var cml = new List<MySqlCommand>();
            //cml.Add(UpdateRecvVdFile6(DocNo, toUpload.Name));
            //Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsUploadFileNotValid4()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid4() ||
                IsFileSizeNotvalid4() ||
                IsFileNameAlreadyExisted4();
        }

        private bool IsUploadFileNotValid5()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid5() ||
                IsFileSizeNotvalid5() ||
                IsFileNameAlreadyExisted5();
        }

        private bool IsUploadFileNotValid6()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid6() ||
                IsFileSizeNotvalid6() ||
                IsFileNameAlreadyExisted6();
        }


        private bool IsFTPClientDataNotValid()
        {
            if (mIsRecvVdAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvVdAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsRecvVdAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsRecvVdAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvVdAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsRecvVdAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsRecvVdAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid4()
        {

            if (mIsRecvVdAllowToUploadFile && TxtFile4.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile4.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile4.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile4.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid5()
        {

            if (mIsRecvVdAllowToUploadFile && TxtFile5.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile5.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile5.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile5.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid6()
        {

            if (mIsRecvVdAllowToUploadFile && TxtFile6.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile6.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile6.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsRecvVdAllowToUploadFile && TxtFile6.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsRecvVdAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsRecvVdAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsRecvVdAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid4()
        {
            if (mIsRecvVdAllowToUploadFile && TxtFile4.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile4.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid5()
        {
            if (mIsRecvVdAllowToUploadFile && TxtFile5.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile5.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid6()
        {
            if (mIsRecvVdAllowToUploadFile && TxtFile6.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile6.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        #region FTP

        private void DownloadFileDtl(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFileDtl(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateRecvVdFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsRecvVdAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsRecvVdAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsRecvVdAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsRecvVdAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsRecvVdAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsRecvVdAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblRecvVdFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        private MySqlCommand UpdateRecvVdFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvVdFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private bool IsGrd3ValueNotValid()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to upload at least 1 file.");
                return true;
            }
            return false;
        }

        private void LabelForRecvVd()
        {
            string[] mLabelFile = {};
            string[] mMandatoryFile = { };
            
	        if(mLabelFileForRecvVd.Length > 0)
	        {
  		        mLabelFile = mLabelFileForRecvVd.Split(',');
	        }

            Label[] lblFile = new Label[6] { lblFile1, lblFile2, lblFile3, lblFile4, lblFile5, lblFile6 };

            for (int i = 0; i < mLabelFile.Length; i++)
            {
                lblFile[i].Text = mLabelFile[i];
            }

            int distance = mLabelFile.Max(s => s.Length);

            splitContainer1.SplitterDistance = distance * 7;

            for (int i = 0; i < lblFile.Length; i++)
            {
                int x = splitContainer1.SplitterDistance - lblFile[i].Width;
                lblFile[i].Left = x;
            }

            if (mMandatoryFileForRecvVd.Length > 0)
            {
                mMandatoryFile = mMandatoryFileForRecvVd.Split(',');
            }

            foreach (string lbl in mMandatoryFile)
            {
                switch (lbl.Trim().ToUpper())
                {
                    case "FILE1":
                        lblFile1.ForeColor = Color.Red;
                        break;
                    case "FILE2":
                        lblFile2.ForeColor = Color.Red;
                        break;
                    case "FILE3":
                        lblFile3.ForeColor = Color.Red;
                        break;
                    case "FILE4":
                        lblFile4.ForeColor = Color.Red;
                        break;
                    case "FILE5":
                        lblFile5.ForeColor = Color.Red;
                        break;
                    case "FILE6":
                        lblFile6.ForeColor = Color.Red;
                        break;
                }
            }

        }

        private bool IsFileMandatory()
        {
	        string[] mMandatoryFile = {};

	        if(mMandatoryFileForRecvVd.Length  > 0)
	        {
  		        mMandatoryFile = mMandatoryFileForRecvVd .Split(',');
	        }

	        foreach(var mMandatoryInd in mMandatoryFile)
	        {
		        if(mMandatoryInd.Trim().ToUpper() == "FILE1")
		        {
                    if (TxtFile.Text == "" || TxtFile.Text == "openFileDialog1")
                    {
                        Sm.StdMsg(mMsgType.Warning, lblFile1.Text + " is Empty");
                        return true;
                    }
		        }

                else if (mMandatoryInd.Trim().ToUpper() == "FILE2")
                {
                    if (TxtFile2.Text == "" || TxtFile2.Text == "openFileDialog1")
                    {
                        Sm.StdMsg(mMsgType.Warning, lblFile2.Text + " is Empty");
                        return true;
                    }
                }

                else if (mMandatoryInd.Trim().ToUpper() == "FILE3")
                {
                    if (TxtFile3.Text == "" || TxtFile3.Text == "openFileDialog1")
                    {
                        Sm.StdMsg(mMsgType.Warning, lblFile3.Text + " is Empty");
                        return true;
                    }
                }

                else if (mMandatoryInd.Trim().ToUpper() == "FILE4")
                {
                    if (TxtFile4.Text == "" || TxtFile4.Text == "openFileDialog1")
                    {
                        Sm.StdMsg(mMsgType.Warning, lblFile4.Text + " is Empty");
                        return true;
                    }
                }

                else if (mMandatoryInd.Trim().ToUpper() == "FILE5")
                {
                    if (TxtFile5.Text == "" || TxtFile5.Text == "openFileDialog1")
                    {
                        Sm.StdMsg(mMsgType.Warning, lblFile5.Text + " is Empty");
                        return true;
                    }
                }

                else if (mMandatoryInd.Trim().ToUpper() == "FILE6")
                {
                    if (TxtFile6.Text == "" || TxtFile6.Text == "openFileDialog1")
                    {
                        Sm.StdMsg(mMsgType.Warning, lblFile6.Text + " is Empty");
                        return true;
                    }
                }
	        }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueVdCode, "Vendor") && !Sm.IsLueEmpty(LueCustomsDocCode, "Customs document code"))
                Sm.FormShowDialog(new FrmRecvVdDlg2(this, Sm.GetLue(LueCustomsDocCode)));
        }

        private void BtnRecvExpeditionDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueVdCode, "Vendor")) 
                Sm.FormShowDialog(new FrmRecvVdDlg3(this, Sm.GetLue(LueVdCode)));
        }

        private void BtnRecvExpeditionDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRecvExpeditionDocNo, "Expedition's document#", false))
            {
                var f1 = new FrmRecvExpedition(mMenuCode);
                f1.Tag = "***";
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtRecvExpeditionDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Event

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
//                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, mIsFilterByCC ? "Y" : "N");
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    Grd1.Cells[r, 36].Value = null;
                    Grd1.Cells[r, 37].Value = null;
                }
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(Sl.SetLueDeptCode2), string.Empty);
            }
        }

        private void LueEntCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueEntCode, new Sm.RefreshLue2(Sl.SetLueEntCode), string.Empty);
            }
        }

        private void LueCustomsDocCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCustomsDocCode, new Sm.RefreshLue2(Sl.SetLueOption), "CustomsDocCode");
                if (IsCustomsDocCodeInputManual())
                {
                    BtnKBContractNo.Enabled = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty
                    }, false);
                }
                else
                {
                    BtnKBContractNo.Enabled = true;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                        DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging, TxtKBPackagingQty
                    }, true);
                }

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                    DteKBRegistrationDt, TxtKBSubmissionNo, TxtKBPackaging
                });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtKBPackagingQty }, 0);
                ChkKBNonDocInd.Checked = false;
                ClearGrd();
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), "");
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                    DteKBRegistrationDt, TxtKBSubmissionNo, LueCustomsDocCode
                });
            }
        }

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite?"Y":"N");
                TxtRecvExpeditionDocNo.EditValue = null;
                ClearGrd();
            }
        }

        private void LueVdCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TxtRecvExpeditionDocNo.EditValue = null;
                ClearGrd();
            }
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtVdDONo);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueLot_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLot, new Sm.RefreshLue1(SetLueLot));
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue1(SetLueBin));
        }

        private void LueLot_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueBin_Leave(object sender, EventArgs e)
        {
            if (LueBin.Visible && fAccept && fCell.ColIndex == 13)
            {
                if (Sm.GetLue(LueBin).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 13].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 13].Value = LueBin.GetColumnValue("Col1");
                }
                LueBin.Visible = false;
            }
        }

        private void LueLot_Leave(object sender, EventArgs e)
        {
            if (LueLot.Visible && fAccept && fCell.ColIndex == 12)
            {
                if (Sm.GetLue(LueLot).Length == 0)
                    Grd1.Cells[fCell.RowIndex, 12].Value = null;
                else
                {
                    Grd1.Cells[fCell.RowIndex, 12].Value = LueLot.GetColumnValue("Col1");
                }
                LueLot.Visible = false;
            }
        }

        private void TxtKBContractNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBContractNo);
        }

        private void TxtKBPLNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBPLNo);
        }

        private void TxtKBRegistrationNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBRegistrationNo);
        }

        private void TxtKBSubmissionNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBSubmissionNo);
        }

        private void TxtKBPackaging_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtKBPackaging);
        }

        private void TxtKBPackagingQty_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtKBPackagingQty, 0);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                if (mIsAutoGenerateBatchNo &&
                    mIsAutoGenerateBatchNoEditable &&
                    mIsRecvVdUseOptionBatchNoFormula)
                {
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        GenerateBatchNo(i);
                    }
                }
            }
        }

        private void DteExpiredDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteExpiredDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteExpiredDt, ref fCell, ref fAccept);
        }

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsUseProductionWorkGroup)
                    Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
            }
        }
        
        #endregion

        #region Grid Event

        private void Grd2_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 5, 6, 7 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int r = 0; r < Grd2.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, e.ColIndex).Length != 0)
                        Total += Sm.GetGrdDec(Grd2, r, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile.Checked) TxtFile.EditValue = string.Empty;
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile2.Checked) TxtFile2.EditValue = string.Empty;
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile3.Checked) TxtFile3.EditValue = string.Empty;
        }

        private void ChkFile4_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile4.Checked) TxtFile4.EditValue = string.Empty;
        }

        private void ChkFile5_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile5.Checked) TxtFile5.EditValue = string.Empty;
        }

        private void ChkFile6_CheckedChanged(object sender, EventArgs e)
        {
            if (!ChkFile6.Checked) TxtFile6.EditValue = string.Empty;
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile4_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile4.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile4.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile5_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile5.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile5.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile6_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile6.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile6.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload4_Click(object sender, EventArgs e)
        {
            DownloadFile4(mHostAddrForFTPClient, mPortForFTPClient, TxtFile4.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile4.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile4, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload5_Click(object sender, EventArgs e)
        {
            DownloadFile5(mHostAddrForFTPClient, mPortForFTPClient, TxtFile5.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile5.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile5, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload6_Click(object sender, EventArgs e)
        {
            DownloadFile6(mHostAddrForFTPClient, mPortForFTPClient, TxtFile6.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile6.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile6, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Class

        #region Split Outstanding PO

        private class SOPOHdr
        {
            public string PODocNoSource { get; set; }
            public string RevNo { get; set; }
            public string PODocNo2 { get; set; }
        }

        private class SOPODtla
        {
            public string PODocNo { get; set; }
            public string PODNo { get; set; }
            public string PODocNoSource { get; set; }
            public decimal POOutstandingQty { get; set; }
            public decimal RecvQty { get; set; }
            public decimal Balance { get; set; }
        }

        private class SOPODtlb
        {
            public string PODocNo { get; set; }
            public string PODNo { get; set; }
            public string PODocNo2 { get; set; }
            public string PODNo2 { get; set; }
            public decimal Qty2 { get; set; }
        }

        #endregion

        #region Reporting Class

        private class RecvVd
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsName { get; set; }
            public string VdName { get; set; }
            public string VdDoNo { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string LocalDocNo { get; set; }
            public string IsForeignName { get; set; }
            public string RecvVdNotes { get; set; }
            public string AddressCity { get; set; }
            public string Phone2 { get; set; }
            public string Fax { get; set; }
            public string PODocNo { get; set; }
            public string PrintBy { get; set; }
            public string AddressVd { get; set; }
            public string VdDONo { get; set; }
            public string VdCtname { get; set; }
            public string PODocDt { get; set; }
            public string PORemarkDtl { get; set; }
            public string VdContactPerson { get; set; }
            public string RecvUsername  { get; set; }
            public string RecvUserGroup { get; set; }
            public string RecvUserSign { get; set; }
        }

        private class RecvVdDtl
        {
            public int nomor { get; set; }
            public string PoDocNo { get; set; }
            public string PODocDt { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ForeignName { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal QtyPurchase { get; set; }
            public string PurchaseUomCode { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal Qty2 { get; set; }
            public string InventoryUomCode2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUomCode3 { get; set; }
            public string Remark2 { get; set; }
            public decimal UPrice { get; set; }
            public decimal Amount { get; set; }
        }

        private class RecvVdDtl2
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string SignInd { get; set; }
        }

        private class RecvVdDtl3
        {
            public string ApprovalDNo { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string LastUpDt { get; set; }
            public string EmpPict { get; set; }
            public string StatusDesc { get; set; }
            public string SignInd { get; set; }
            public string PosName { get; set; }
            public string DNo { get; set; }
            public string Space { get; set; }
            public string Title { get; set; }
        }

        private class RecvVdIMS
        {
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string VdName2 { get; set; }
            public string LocalDocNo { get; set; }
            public string PODocNo { get; set; }
            public string UserName { get; set; }
            public string ProjectName { get; set; }
            public string ExpeditionDocDt { get; set; }
            public string ExpeditionLocalDocNo { get; set; }
            public string PODocDt { get; set; }
            public string ItName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhoneNumber { get; set; }

        }

        private class RecvVdIMSDtl
        {
            public int Nomor { get; set; }
            public int Nomor2 { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string UOM { get; set; }
            public string Remark { get; set; }
            public string Project { get; set; }
            public decimal QtyPurchase { get; set; }
            public decimal POQty { get; set; }
            public decimal POQtyCancel { get; set; }
            public decimal OutstandingQty { get; set; }
            public decimal BalanceQty { get; set; }
            public decimal ReceivedQty { get; set; }
            public decimal QtyReject { get; set; }
            public decimal TotalRecvQty { get; set; }
            public decimal TotalUnreceivedQty { get; set; }
            public string PODocNo { get; set; }
            public string PODNo { get; set; }
            public string ItCodeInduk { get; set; }
            public string ItNameInduk { get; set; }
            public string EstRecvDt { get; set; }
            public string VdDONo { get; set; }
            public string RecvExpLocalDocNo { get; set; }
            public string ProjectName { get; set; }
            public string RejectedInd { get; set; }

        }

        private class Grid2
        {
            public string PODocNo { get; set; }
            public string PODNo { get; set; }
            public decimal OutstandingQty { get; set; }
            public decimal ReceivedQty { get; set; }
            public decimal BalanceQty { get; set; }
        }

        private class RecvVdSignIMS
        {
            public string UserCode { get; set; }
            public string UserName1 { get; set; }
            public string UserName2 { get; set; }
            public string UserName3 { get; set; }
            public string POSITION1 { get; set; }
            public string POSITION2 { get; set; }
            public string POSITION3 { get; set; }
        }

        #endregion

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        private class RecvVdDtl4
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
        }

        class RecvVdItBarcode
        {
            public string DocNo { set; get; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string Size { get; set; }
            public string Color { get; set; }
            public string ForeignName { get; set; }
            public string Dno { get; set; }
        }

        #region Auto DO

        private class DODept2Hdr
        {
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsCode { get; set; }
            public string LocalDocNo { get; set; }
            public string DeptCode { get; set; }
            public string EntCode { get; set; }
            public string CCCode { get; set; }
            public string LOPDocNo { get; set; }
            public string RecvVdDocNo { get; set; }
            public string Remark { get; set; }
        }

        private class DODept2Dtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItCode { get; set; }
            public string PropCode { get; set; }
            public string BatchNo { get; set; }
            public string Source { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public string LOPDocNo { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string Remark { get; set; }
        }

        #endregion

        private class COA
        {
            public string AcNo { get; set; }
        }

        private class SOContract
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string SOCDocNo { get; set; }
        }

        private class JournalBasedSOC
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string RecvVdDocNo { get; set; }
            public string RecvVdDNo { get; set; }
            public string PODocNo { get; set; }
            public string PODNo { get; set; }
            public string SOCDocNo { get; set; }
            public int Index { get; set; }
        }

        #endregion        
    }
}
