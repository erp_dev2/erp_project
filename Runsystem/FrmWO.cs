﻿#region Update
/*
    18/07/2017 [WED] Hours Meter, Maint Type, Symp Problem bisa insert manual
    18/07/2017 [WED] Update Maint Type & Sym Problem ke WOR, kalau WO Status di WOR nya masih Open
    24/07/2017 [HAR] Tambah validasi hoursmeter tidak boleh kecil
    26/07/2017 [HAR] Tambah parameter validasi hoursmeter 
    01/08/2017 [WED] Tambah warning saat save, jika HM baru lebih dari 24 jam di hari yg sama dengan dokumen sebelumnya
    05/08/2017 [ARI] Tambah Printout
    08/08/2017 [HAR] Tambah parameter wor bisa dipilih sekali atau tidak
    21/08/2017 [HAR] Tambah parameter start date dan start time pas activity ambil dari down date dan down time di WOR
    05/09/2017 [WED] tambah informasi DocNo TO di header
    05/09/2017 [WED] tetap bisa edit HM, Maintenance Type, Symptom Problem, dan Remark, selama belum settle
    11/09/2017 [WED] tambah informasi asset name di header
    18/09/2017 [HAR] mechanic boleh tidak di isi berdasarkan parameter
    19/09/2017 [HAR] description saat insert dan edit bisa diubah, sekaligus update ke WOR, HM waktu edit jika nilainya sama tidak generate ke master HM
    25/09/2017 [HAR] tambah HMDocNo di tabel WOHdr buat dapetin nilai hoursmeter tiap document WO
    28/09/2017 [HAR] tambah button shortcut ke RPL berdasarkan parameter
    12/10/2017 [HAR] tambah fixed indicator
    12/10/2017 [HAR] tambah validasi WOR yang closed
    08/12/2017 [HAR] bug fixing validasi mechanic kosong
    11/12/2017 [HAR] tambah Fixed By
    22/01/2018 [TKG] ubah label activity dan remark
    30/01/2018 [HAR] tmbh form dialog jika DO request multiple untuk 1 WO
    31/01/2018 [WED] tambah field informasi Asset Display Name
    21/02/2018 [HAR] tambah warning jika date document dan start date berbeda
    13/04/2018 [ARI] Symptom di mandatory berdasarkan parameter isSymptomMandatory
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*
    02/12/2021 [SET/IOK] menambahkan kolom Equipment sesuai dengan Equipment yang ada di Work Order Request (WOR) berdasar TO. Kolom tersebut bisa dipilih sama dengan kolom Symptom Problem dengan parameter IsWORUseEquipment
    26/01/2022 [RDA/IOK] Support : membuat field equipment menjadi tidak mandatory (berdasar task menambahkan kolom Equipment sesuai dengan Equipment yang ada di Work Order Request (WOR) berdasar TO)
    24/02/2022 [TRI] jumlah hours meter saat find beda saat show data
    14/06/2022 [BRI/IOK] work order detail editable berdasarkan param IsWorkOrderDetailEditable
    20/06/2022 [BRI/IOK] tambah compute di event grd2
    21/06/2022 [BRI/IOK] validasi cancel data
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWO : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mAssetCode = string.Empty,
            mInitDt = string.Empty,   // tanggal di master Hours Meter
            mInsertedDt = string.Empty, // tanggal saat save
            mSource = string.Empty;  
        iGCell fCell;
        bool fAccept;
        internal FrmWOFind FrmFind;
        private string
              mIsWODocNoUseFormatStandard = string.Empty,
              mIsWOValidateByHoursMeter = string.Empty;

        internal decimal
            mInitHoursMeter = 0m,
            mInsertedHoursMeter = 0m,
            HoursMeter = 0m;

        internal bool mIsEntityMandatory = false, mIsFilterBySite = false, mIsSymptomMandatory;
        public bool mIsWORCanBeSelectedMorethanOnce = false;
        public bool mIsWOStartDtTmAutomaticFromDownDtTm = false;
        internal bool mIsMechanicMandatory = false;
        private bool 
            mIsWOHasShortcuttoRPL = false,
            mIsMechanicWOEditable = false,
            mIsWORUseEquipment = false,
            mIsWorkOrderDetailEditable = false; 
        

        #endregion

        #region Constructor

        public FrmWO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if(!mIsWORUseEquipment)
                {
                    LblEquipmentCode.Visible = LueEquipmentCode.Visible = mIsWORUseEquipment;
                    label7.Location = new Point(9, 50);
                    label9.Location = new Point(32, 71);
                    label11.Location = new Point(12, 92);
                    label12.Location = new Point(48, 113);
                    label6.Location = new Point(87, 134);
                    label10.Location = new Point(68, 155);
                    TxtMRDocNo.Location = new Point(120, 47);
                    TxtDOReqDocNo.Location = new Point(120, 68);
                    TxtDODeptDocNo.Location = new Point(120, 89);
                    TxtRecvDocNo.Location = new Point(120, 110);
                    TxtSite.Location = new Point(120, 131);
                    MeeRemark.Location = new Point(120, 152);
                }
                if (mIsSymptomMandatory) LblSymptom.ForeColor = Color.Red;
                BtnRPL.Visible = mIsWOHasShortcuttoRPL;
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueWOActivity, "WOActivity");
                Sl.SetLueOption(ref LueBreakDown, "BreakdownStatus");
                SetLueMaintenanceType(ref LueMaintenanceType);
                SetLueSymptomProblem(ref LueSymptomProblem);
                LueWOActivity.Visible = false;
                LueBreakDown.Visible = false;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    this.Text = "Work Order";
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "WO#",

                        //1-3
                        "",
                        "Activity",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-3
                        20, 200, 300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3 });

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 15;
            Grd2.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Maintenance Type",
                        "Maintenance Type",
                        "Maintenance",
                        "Maintenance Status",
                        "Start"+Environment.NewLine+"Date",

                        //6-10
                        "Start"+Environment.NewLine+"Time",
                        "End"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Time",
                        "Hours"+Environment.NewLine+"Minute",
                        "",

                        //11-14
                        "Mechanic Code",
                        "Mechanic",
                        "Activity",
                        ""
                    },
                    new int[] 
                    {
                        //0
                        10,
 
                        //1-5
                        10, 300, 150, 150, 100,  

                        //6-10
                        100, 100, 100, 80, 20, 

                        //11-14
                        250, 250, 250, 20
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 10, 14 });
            Sm.GrdFormatDate(Grd2, new int[] { 5, 7 });
            Sm.GrdFormatTime(Grd2, new int[] { 6, 8 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 3 , 11 });
            if (!mIsWorkOrderDetailEditable)
                Sm.GrdColInvisible(Grd2, new int[] { 14 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 11, 12, 13 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, TxtHoursMeter, MeeRemark, TxtTOCode, TxtAssetName, MeeDescription,
                        TxtMRDocNo, TxtRecvDocNo, TxtDODeptDocNo, TxtDOReqDocNo, TxtSite, LueSymptomProblem, LueMaintenanceType, TxtDisplayName,
                        LueEquipmentCode
                    }, true);
                    BtnWORDocNo.Enabled = false;
                    ChkFixedInd.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, MeeDescription, MeeRemark, TxtHoursMeter, LueMaintenanceType, LueSymptomProblem, LueEquipmentCode }, false);
                    BtnWORDocNo.Enabled = true;
                    ChkFixedInd.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 2, 4, 5, 6, 7, 8, 10, 13 });                    
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    if (mIsWorkOrderDetailEditable && !ChkCancelInd.Checked) Sm.GrdColReadOnly(false, true, Grd2, new int[] { 5, 6, 7, 8, 13 });
                    if (!ChkCancelInd.Checked && !ChkSettleInd.Checked)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeDescription, MeeRemark, TxtHoursMeter, LueMaintenanceType, LueSymptomProblem }, false);
                        ChkFixedInd.Enabled = true;
                    }
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, MeeCancelReason, TxtWORDocNo, LueMaintenanceType, 
                 LueSymptomProblem, MeeDescription, MeeRemark, TxtMRDocNo, TxtRecvDocNo, 
                 TxtDODeptDocNo, TxtDOReqDocNo, TxtSite, TxtTOCode, TxtAssetName, MeeDescription,
                 TxtDisplayName
            });
            Sm.SetControlNumValueZero(new List<TextEdit>
            {
                TxtHoursMeter
            }, 0);
            mInitHoursMeter = 0m;
            mInsertedHoursMeter = 0m;
            mInitDt = string.Empty;
            mInsertedDt = string.Empty;
            mAssetCode = string.Empty;
            ChkCancelInd.Checked = false;
            ChkSettleInd.Checked = false;
            ChkFixedInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private static void SetLueMaintenanceType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'MaintenanceType' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private static void SetLueSymptomProblem(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'SymptomProblem' Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueEquipment(ref LookUpEdit Lue)
        {
            try
            {
                string TOCode = TxtTOCode.Text;
                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT A.EquipmentCode Col1, B.EquipmentName Col2 ");
                SQL.AppendLine("FROM tbltodtl A ");
                SQL.AppendLine("INNER JOIN tblequipment B ON A.EquipmentCode = B.EquipmentCode ");
                SQL.AppendLine("WHERE A.AssetCode = '" + TOCode + "'; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsWODocNoUseFormatStandard = Sm.GetParameter("IsWODocNoUseFormatStandard");
            mIsWOValidateByHoursMeter = Sm.GetParameter("IsWOValidateByHoursMeter");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsWORCanBeSelectedMorethanOnce = Sm.GetParameterBoo("IsWORCanBeSelectedMorethanOnce");
            mIsWOStartDtTmAutomaticFromDownDtTm = Sm.GetParameterBoo("IsWOStartDtTmAutomaticFromDownDtTm");
            mIsMechanicMandatory = Sm.GetParameterBoo("IsMechanicMandatory");
            mIsWOHasShortcuttoRPL = Sm.GetParameterBoo("IsWOHasShortcuttoRPL");
            mIsMechanicWOEditable = Sm.GetParameterBoo("IsMechanicWOEditable");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsSymptomMandatory = Sm.GetParameterBoo("IsSymptomMandatory");
            mIsWORUseEquipment = Sm.GetParameterBoo("IsWORUseEquipment");
            mIsWorkOrderDetailEditable = Sm.GetParameterBoo("IsWorkOrderDetailEditable");
        }

        private void ComputeDurationHourWithoutRow()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 5).ToString().Length > 0 &&
                    Sm.GetGrdStr(Grd2, Row, 6).ToString().Length > 0 &&
                    Sm.GetGrdStr(Grd2, Row, 7).ToString().Length > 0 &&
                    Sm.GetGrdStr(Grd2, Row, 8).ToString().Length > 0)
                {
                    string
                    StartDt = Sm.GetGrdDate(Grd2, Row, 5).ToString().Substring(0, 8),
                    StartTm = Sm.GetGrdStr(Grd2, Row, 6).Replace(":", ""),
                    EndDt = Sm.GetGrdDate(Grd2, Row, 7).ToString().Substring(0, 8),
                    EndTm = Sm.GetGrdStr(Grd2, Row, 8).Replace(":", "");


                    DateTime Dt1 = new DateTime(
                        Int32.Parse(StartDt.Substring(0, 4)),
                        Int32.Parse(StartDt.Substring(4, 2)),
                        Int32.Parse(StartDt.Substring(6, 2)),
                        Int32.Parse(StartTm.Substring(0, 2)),
                        Int32.Parse(StartTm.Substring(2, 2)),
                        0
                        );
                    
                    DateTime Dt2 = new DateTime(
                        Int32.Parse(EndDt.Substring(0, 4)),
                        Int32.Parse(EndDt.Substring(4, 2)),
                        Int32.Parse(EndDt.Substring(6, 2)),
                        Int32.Parse(EndTm.Substring(0, 2)),
                        Int32.Parse(EndTm.Substring(2, 2)),
                        0
                        );

                    if (Dt2 < Dt1) Dt2 = Dt2.AddDays(1);
                    double TotalHours = (Dt2 - Dt1).TotalMinutes;

                    if (TotalHours < 0) TotalHours = 0;

                    double hours = Math.Truncate(TotalHours / 60);
                    double minutes = TotalHours % 60;
                    var time = string.Format("{0} : {1}", (hours.ToString().Length==1? string.Concat("0", hours.ToString()):hours.ToString()), (minutes.ToString().Length==1? string.Concat("0", minutes.ToString()):minutes.ToString()));
                    Grd2.Cells[Row, 9].Value = time;
                }
            }
        }

        private void GetTimePrevious()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Row != 0 && Sm.GetGrdStr(Grd2, Row, 5).Length > 0 && Sm.GetGrdStr(Grd2, Row, 7).Length == 0 && Sm.GetGrdStr(Grd2, Row, 8).Length == 0)
                {
                    Sm.CopyGrdValue(Grd2, Row, 6, Grd2, Row-1, 8);
                }
            }
        }

        private void GetDateAndTimeFromWOR(int Row)
        {
            if (Row == 0 && mIsWOStartDtTmAutomaticFromDownDtTm && TxtWORDocNo.Text.Length>0)
            {
                string DownDt = Sm.GetValue("Select DownDt From TblWor Where DocNo = '"+TxtWORDocNo.Text+"' ");
                if(DownDt.Length==8)
                Grd2.Cells[Row, 5].Value = Sm.ConvertDate(DownDt);

                string DownTm = Sm.GetValue("Select DownTm From TblWor Where DocNo = '" + TxtWORDocNo.Text + "' ");
                if(DownTm.Length==4)
                    Grd2.Cells[Row, 6].Value = Sm.Left(DownTm, 2) + ":" + DownTm.Substring(2, 2);
            }
        }

        private void LockGrid()
        {
            if (!mIsMechanicWOEditable)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length != 0)
                    {
                        for (int Col = 0; Col < 13; Col++)
                        {
                            Grd2.Rows[Row].ReadOnly = iGBool.True;
                            Grd2.Rows[Row].CellStyle.BackColor = Color.FromArgb(224, 224, 224);
                        }
                    }
                    else
                    {
                        int[] ColIndex = new int[] { 2, 4, 5, 6, 7, 8, 10, 13 };
                        for (int Col = 0; Col < ColIndex.Length; Col++)
                        {
                            Grd2.Cols[ColIndex[Col]].CellStyle.BackColor = Color.White;
                            Grd2.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.False;
                        }
                    }
                }
            }
            else
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                {
                        int[] ColIndex = new int[] { 10 };
                        for (int Col = 0; Col < ColIndex.Length; Col++)
                        {
                            Grd2.Cols[ColIndex[Col]].CellStyle.BackColor = Color.White;
                            Grd2.Cols[ColIndex[Col]].CellStyle.ReadOnly = iGBool.False;
                        }
                }
            }
        }       

        private string GenerateDocNoNonStandard(string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                SiteName = TxtSite.Text,
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, "+DocSeqNo+") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '"+((SiteName.Length==0 ? "XXX" : SiteName))+"' '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkFixedInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            mInitHoursMeter = Decimal.Parse(TxtHoursMeter.Text);
            mInitDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            HoursMeter = Decimal.Parse(TxtHoursMeter.Text);
            mSource = Sm.GetValue("Select DocNo From TblWOR Where DocNo=@Param And Description='Source : Maintenance Schedule'", TxtWORDocNo.Text);
            if (mIsWorkOrderDetailEditable && mSource.Length == 0)
                Sm.GrdColReadOnly(true, true, Grd2, new int[] { 5, 6, 7, 8, 13 });

            LockGrid();
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mInsertedHoursMeter = Decimal.Parse(TxtHoursMeter.Text);
                mInsertedDt = Sm.Left(Sm.GetDte(DteDocDt), 8);

                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "WOHdr", "WODtl", "WODtl2", "WODtl3" };

            var l = new List<WOHdr>();
            var ldtl = new List<WODtl>();
            var ldtl2 = new List<WODtl2>();
            var ldtl3 = new List<WODtl3>();

            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            if (mIsEntityMandatory)
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, H.CompanyName, H.CompanyAddress, H.CompanyPhone, H.CompanyFax, '' As CompanyAddressCity,");
            }
            else
            {
                SQL.AppendLine(" Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
                SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As CompanyAddressCity, ");
            }
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.WORDocNo, B.Description, ");
            SQL.AppendLine("E.SiteName, Case B.WOStatus When 'O' Then 'Open' When 'C' Then 'Close' End WOStatus, G.OptDesc As MtcStatusDesc, ");
            SQL.AppendLine("H.OptDesc As MtcTypeDesc, I.OptDesc As SymProblemDesc, F.HoursMeter, A.Remark, B.TOCode, J.UserName As CreateBy ");
            SQL.AppendLine("From TblWOHdr A ");
            SQL.AppendLine("Inner Join TblWOR B On A.WORDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblTOHdr C On B.ToCode = C.AssetCode ");
            SQL.AppendLine("Left Join TblLocation D On C.LocCode = D.LocCode ");
            SQL.AppendLine("left Join TblSite E On D.SiteCode = E.SiteCode ");
            SQL.AppendLine("left Join TblHoursMeter F On C.HMDocNo = F.DocNo And C.AssetCode=F.ToCode ");
            SQL.AppendLine("Left Join TblOption G On B.MtcStatus=G.OptCode And G.OptCat ='MaintenanceStatus' ");
            SQL.AppendLine("Left Join TblOption H On B.MtcType=H.OptCode And H.OptCat ='MaintenanceType' ");
            SQL.AppendLine("Left Join TblOption I On B.SymProblem=I.OptCode And I.OptCat ='SymptomProblem' ");
            SQL.AppendLine("Left Join TblUser J On A.CreateBy=J.UserCode ");
            if (mIsEntityMandatory)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, G.EntName As CompanyName, G.EntAddress As CompanyAddress, G.EntPhone As CompanyPhone, G.EntFax As CompanyFax ");
                SQL.AppendLine("    From TblWOHdr A   ");
                SQL.AppendLine("    Inner Join TblWOR B On A.WORDocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblTOHdr C On B.ToCode = C.AssetCode ");
                SQL.AppendLine("    Left Join TblLocation D On C.LocCode = D.LocCode ");
                SQL.AppendLine("    left Join TblSite E On D.SiteCode = E.SiteCode ");
                SQL.AppendLine("    Left Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
                SQL.AppendLine("    Left Join TblEntity G On F.EntCode = G.EntCode ");
                SQL.AppendLine(") H On A.DocNo = H.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (mIsEntityMandatory)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select G.EntLogoName " +
                       "From TblWOHdr A   " +
                       "Inner Join TblWOR B On A.WORDocNo=B.DocNo  " +
                       "Inner Join TblTOHdr C On B.ToCode = C.AssetCode " +
                       "Left Join TblLocation D On C.LocCode = D.LocCode " +
                       "left Join TblSite E On D.SiteCode = E.SiteCode " +
                       "Left Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode " +
                       "Left Join TblEntity G On F.EntCode = G.EntCode " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "CompanyAddressCity",
                         
                         //6-10
                         "DocNo", 
                         "DocDt", 
                         "WORDocNo",
                         "Description", 
                         "SiteName",
                          
                         //11-15
                         "WOStatus",
                         "MtcStatusDesc",
                         "MtcTypeDesc", 
                         "SymProblemDesc",
                         "HoursMeter",

                         //16-18
                         "Remark",
                         "TOCode",
                         "CreateBy"
                       
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WOHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            CompanyAddressCity = Sm.DrStr(dr, c[5]),
                            DocNo = Sm.DrStr(dr, c[6]),

                            DocDt = Sm.DrStr(dr, c[7]),
                            WORDocNo = Sm.DrStr(dr, c[8]),
                            Description = Sm.DrStr(dr, c[9]),
                            SiteName = Sm.DrStr(dr, c[10]),
                            WOStatus = Sm.DrStr(dr, c[11]),

                            MtcStatusDesc = Sm.DrStr(dr, c[12]),
                            MtcTypeDesc = Sm.DrStr(dr, c[13]),
                            SymProblemDesc = Sm.DrStr(dr, c[14]),
                            HoursMeter = Sm.DrStr(dr, c[15]),
                            Remark = Sm.DrStr(dr, c[16]),

                            TOCOde = Sm.DrStr(dr, c[17]),
                            CreateBy = Sm.DrStr(dr, c[18]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                           
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText =
                    "Select A.DocNo, Date_Format(B.DocDt,'%d %M %Y')As DocDt, Date_Format(C.DocDt,'%d %M %Y')As BDDocDt, E.OptDesc As BreakdownStatus, D.OptDesc As WOActivity, " +
                    "Date_Format(A.Dt1,'%d %M %Y') As DtStart, Concat(Left(A.Tm1,2),':',Right(A.Tm1,2)) As StartTm, " +
                    "Date_Format(A.Dt2,'%d %M %Y') As DtEnd, Concat(Left(A.Tm2,2),':',Right(A.Tm2,2)) As EndTm, " +
                    "Concat(Left(A.Tm3,2),':',Right(A.Tm3,2)) As HT, A.Remark As RemarkDtl " +
                    "From TblWODtl A "+
                    "Inner Join TblWOHdr B On A.DocNo=B.DocNo " +
                    "Inner Join TblWOR C On B.WORDocNo=C.DocNo " +
                    "Left Join TblOption D On A.WOActivity=D.OptCode And D.OptCat ='WOActivity' " +
                    "Left Join TblOption E On A.BreakDownStatus=E.OptCode And E.OptCat ='BreakdownStatus' " +
                    "Where A.DocNo=@DocNo ";

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "DocNo",

                         //1-5
                         "DocDt",
                         "BDDocDt",
                         "BreakdownStatus",
                         "WOActivity",
                         "DtStart",

                         //6-10
                         "StartTm",
                         "DtEnd",
                         "EndTm",
                         "HT",
                         "RemarkDtl",

                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new WODtl()
                        {
                            nomor = nomor,
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),

                            DocDt = Sm.DrStr(drDtl, cDtl[1]),
                            BDDocDt = Sm.DrStr(drDtl, cDtl[2]),
                            BreakdownStatus = Sm.DrStr(drDtl, cDtl[3]),
                            WOActivity = Sm.DrStr(drDtl, cDtl[4]),
                            DtStart = Sm.DrStr(drDtl, cDtl[5]),

                            StartTm = Sm.DrStr(drDtl, cDtl[6]),
                            DtEnd = Sm.DrStr(drDtl, cDtl[7]),
                            EndTm = Sm.DrStr(drDtl, cDtl[8]),
                            HT = Sm.DrStr(drDtl, cDtl[9]),
                            RemarkDtl = Sm.DrStr(drDtl, cDtl[10]),

                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;
                SQLDtl2.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(C.ParValue, ''), B.UserCode, '.JPG') As EmpPict, E.PosName ");
                SQLDtl2.AppendLine("From TblWOHdr A  ");
                SQLDtl2.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                SQLDtl2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature'  ");
                SQLDtl2.AppendLine("Left Join TblEmployee D On B.UserCode=D.UserCode ");
                SQLDtl2.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo ");
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0-2
                         "UserCode",

                         "UserName",
                         "EmpPict",
                         "PosName"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new WODtl2()
                        {
                            UserCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            EmpPict = Sm.DrStr(drDtl2, cDtl2[2]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[3]),

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail TotalHm

            string TotalHM = string.Empty;
            int hours = 0;
            decimal minutes = 0m;

            for (int x = 0; x < Grd2.Rows.Count - 1; x++)
            {
                if (Sm.GetGrdStr(Grd2, x, 9).Length > 0)
                {
                    hours += Int32.Parse(Sm.Left(Sm.GetGrdStr(Grd2, x, 9), 2));
                    minutes += Decimal.Parse(Sm.Right(Sm.GetGrdStr(Grd2, x, 9), 2));
                }
            }

            string m = Sm.Left(string.Concat((minutes % 60).ToString(), "00"), 2);
            if (minutes > 59)
            {
                hours = hours + (Int32.Parse(minutes.ToString()) / 60);
            }
            string h = Sm.Right(string.Concat("00", hours.ToString()), 2);

            TotalHM = string.Concat(h, ":", m);

            if (TotalHM.Length > 0)
            {
                ldtl3.Add(new WODtl3()
                {
                    TotalHm = TotalHM
                });
            }
            else
            {
                ldtl3.Add(new WODtl3()
                {
                    TotalHm = string.Empty
                });
            }

            myLists.Add(ldtl3);
            #endregion

            Sm.PrintReport("WO", myLists, TableName, false);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            IsDateDifferent();
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            string HMDocNo = string.Empty;

            if (mIsWODocNoUseFormatStandard == "Y")
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "WO", "TblWOHdr");
            }
            else
            {
                DocNo = GenerateDocNoNonStandard(Sm.GetDte(DteDocDt), "WO", "TblWOHdr");
            }
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveWOHdr(DocNo));
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0) cml.Add(SaveWODtl(DocNo, r));

                string MechanicCode = Sm.GetGrdStr(Grd2, r, 11);
                string MCode = String.Empty; 
                int index = 0;
                int CountData = 0;
                for (int i = 1; i < MechanicCode.Length; i++)
                {
                    if (MechanicCode[i] == '#')
                    {
                        MCode = MechanicCode.Substring(index,  i-index);
                        index = i + 1;
                        CountData = CountData + 1;
                        cml.Add(SaveWODtl2(DocNo, r, CountData, MCode));
                    }
                }
            }

            // kalau WORequest masih Open, update data Maintenance Type dan Symptom Problemnya
            var WOStatus = Sm.GetValue("Select WOStatus From TblWOR Where DocNo = '"+TxtWORDocNo.Text+"'; ");
            if(WOStatus.Length > 0 && WOStatus == "O")
                cml.Add(UpdateWOR());
            
            // kalau HoursMeter awal berbeda dengan Hours Meter yg di save, bikin dokumen Hours Meter baru
            if ((mInitHoursMeter != mInsertedHoursMeter) && mAssetCode.Length > 0)
            {
                HMDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "HoursMeter", "TblHoursMeter");
                cml.Add(SaveHoursMeter(HMDocNo, mAssetCode, DocNo));
            }

            ////jangan terbalik
            //cml.Add(UpdateWORDtl(DocNo));
            //cml.Add(UpdateWORHdr());

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                //(mIsWORUseEquipment && LblEquipmentCode.ForeColor == Color.Red && Sm.IsLueEmpty(LueEquipmentCode, "Equipment")) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                BackDateValidation() ||
                IsHoursMeterNotValid() ||
                (mIsSymptomMandatory && Sm.IsLueEmpty(LueSymptomProblem, "Symptom Problem")) ||
                IsWORAlreadyClosed()||
                ((mInitDt == mInsertedDt) && IsHoursMeterExceed24())
                ;
        }

        private void IsDateDifferent()
        {
            if (Grd2.Rows.Count >1)
            {
                if (Sm.GetDte(DteDocDt) != Sm.GetGrdDate(Grd2, 0, 5))
                {
                    Sm.StdMsg(mMsgType.Info, "Document date and Start date is different");
                }
            }
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 WO request's activity.");
                return true;
            }
            return false;
        }

        private bool IsHoursMeterExceed24()
        {
            if (!ChkSettleInd.Checked && !ChkCancelInd.Checked)
            {
                if (mInsertedHoursMeter - mInitHoursMeter <= 24)
                    return false;
                else
                {
                    // kalau HoursMeter awal selisihnya lebih dari 24 jam dari Hours Meter yg di save, muncul warning

                    return Sm.StdMsgYN("Question",
                        "Initial Hours Meter : " + mInitHoursMeter + Environment.NewLine +
                        "Inserted Hours Meter : " + mInsertedHoursMeter + Environment.NewLine + Environment.NewLine +
                        "Inserted Hours Meter exceeds more than 24 hours from Initial Hours Meter." + Environment.NewLine +
                        "Do you want to continue to save this data ?"
                        ) == DialogResult.No;
                }
            }
            else
                return false;
        }

        private bool IsHoursMeterNotValid()
        {
            if (mIsWOValidateByHoursMeter == "Y" )
            {
                if (!ChkSettleInd.Checked && !ChkCancelInd.Checked && Decimal.Parse(TxtHoursMeter.Text) <= HoursMeter)
                {
                    Sm.StdMsg(mMsgType.Warning, "Hours Meter must be bigger than last hours meter (" + TxtHoursMeter.Text + ") ");
                    return true;
                }
            }
            return false;
        }

        private bool BackDateValidation()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 5).Length != 0 && Sm.GetGrdStr(Grd2, Row, 7).Length != 0)
                {
                    if (Decimal.Parse(Sm.GetGrdDate(Grd2, Row, 5)) > Decimal.Parse(Sm.GetGrdDate(Grd2, Row, 7)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "End date for activity "+Sm.GetGrdStr(Grd2, Row, 2)+" not valid.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (!ChkSettleInd.Checked && !ChkCancelInd.Checked)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 2, false, "Activity is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 4, false, "Breakdown Status is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 5, false, "Start date is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 6, false, "Start time is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 7, false, "End date is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 8, false, "End time is empty.")) return true;
                    if (mIsMechanicMandatory && Sm.IsGrdValueEmpty(Grd2, Row, 12, false, "Mechanic is empty.")) return true;
                }
            }
            return false;
        }


        private bool IsWORAlreadyClosed()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOR Where DocNo=@Param And CancelInd='N' And Status='A' And WOStatus = 'C' ;",
                TxtWORDocNo.Text,
                "Document WO Request already closed.");
        }

        private bool IsEquipmentTOIsEmpty()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EquipmentCode From TblEquipment A ");
            SQL.AppendLine("Inner Join Tbltodtl B On B.EquipmentCode = A.EquipmentCode ");
            SQL.AppendLine("Where B.AssetCode = @assetcode ");
            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@assetCode", TxtTOCode.Text);

            //if (Sm.GetValue(cm).Length != 0)
            //{
            //    if (type == "Save")
            //        return Sm.IsLueEmpty(LueEquipmentCode, "Equipment");
            //    else if (type == "Show")
            //        LblEquipmentCode.ForeColor = Color.Red;
            //}
            //else 
            //    LblEquipmentCode.ForeColor = Color.Black;

            return false;
        }


        private MySqlCommand SaveWOHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            string a = Sm.GetLue(LueEquipmentCode);
            SQL.AppendLine("Insert Into TblWOHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, WORDocNo, EquipmentCode, FixedInd, FixedBy, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @WORDocNo, @EquipmentCode, @FixedInd, @FixedBy, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WORDocNo", TxtWORDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EquipmentCode", Sm.GetLue(LueEquipmentCode));
            Sm.CmParam<String>(ref cm, "@FixedInd", ChkFixedInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@FixedBy", ChkFixedInd.Checked == true ? Gv.CurrentUserCode : "");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWODtl(string DocNo, int Row)
        {
            string Tm1 = string.Empty;
            string Tm2 = string.Empty;
            string Tm3 = string.Empty;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWODtl(DocNo, DNo, WOActivity, BreakdownStatus, Dt1, Tm1, Dt2, Tm2, Tm3, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @WOActivity, @BreakdownStatus, @Dt1, @Tm1, @Dt2, @Tm2, @Tm3, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WOActivity", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@BreakdownStatus", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetGrdDate(Grd2, Row, 5));
            Tm1 = Sm.GetGrdStr(Grd2, Row, 6);
            Sm.CmParam<String>(ref cm, "@Tm1", string.Concat(Sm.Left(Tm1, 2), Sm.Right(Tm1, 2)));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetGrdDate(Grd2, Row, 7));
            Tm2 = Sm.GetGrdStr(Grd2, Row, 8);
            Sm.CmParam<String>(ref cm, "@Tm2", string.Concat(Sm.Left(Tm2, 2), Sm.Right(Tm2, 2)));
            Tm3 = Sm.GetGrdStr(Grd2, Row, 9);
            Sm.CmParam<String>(ref cm, "@Tm3", string.Concat(Sm.Left(Tm3, 2), Sm.Right(Tm3, 2)));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd2, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWODtl2(string DocNo, int Row, int Count, string MechanicCode)
        {
            string Tm = string.Empty;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWODtl2(DocNo, DNo, DNo2, MechanicCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @DNo2, @MechanicCode, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DNo2", Sm.Right("00" + Count.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@MechanicCode", MechanicCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateWOR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWOR Set ");
            SQL.AppendLine("    Description = @Description, ");
            SQL.AppendLine("    MtcType = @MtcType,  ");
            SQL.AppendLine("    SymProblem = @SymProblem,  ");
            SQL.AppendLine("    LastUpBy = @CreateBy,  ");
            SQL.AppendLine("    LastUpDt = CurrentDateTime()  ");
            SQL.AppendLine("Where DocNo = @WORDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Description", MeeDescription.Text);
            Sm.CmParam<String>(ref cm, "@WORDocNo", TxtWORDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MtcType", Sm.GetLue(LueMaintenanceType));
            Sm.CmParam<String>(ref cm, "@SymProblem", Sm.GetLue(LueSymptomProblem));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveHoursMeter(string HMDocNo, string AssetCode, string WODocNo)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblHoursMeter(DocNo, DocDt, TOCode, HoursMeter, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@HMDocNo, @DocDt, @AssetCode, @HoursMeter, @UserCode, CurrentDateTime());");

            SQL.AppendLine("Update TblTOHdr set ");
            SQL.AppendLine("    HMDocNo=@HMDocNo ");
            SQL.AppendLine("Where AssetCode=@AssetCode; ");

            SQL.AppendLine("Update TblWOHdr ");
            SQL.AppendLine("    SET HMDocNo=@HMDocNo ");
            SQL.AppendLine("    Where DocNo=@WODocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@HMDocNo", HMDocNo);
            Sm.CmParam<String>(ref cm, "@WODocNo", WODocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@AssetCode", AssetCode);
            Sm.CmParam<Decimal>(ref cm, "@HoursMeter", decimal.Parse(TxtHoursMeter.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;

        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            string HMDocNo = string.Empty;
            string mTOCode = TxtTOCode.Text;
            bool CancelInd = Sm.GetValue("Select CancelInd From TblWOHdr Where DocNo = '" + TxtDocNo.Text + "'") == "Y";
            bool SettleInd = Sm.GetValue("Select SettleInd From TblWOHdr Where DocNo = '" + TxtDocNo.Text + "'") == "Y";
            
            if (mIsWorkOrderDetailEditable && mSource.Length > 0)
            {
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                    if (mIsWorkOrderDetailEditable && !CancelInd) cml.Add(SaveWODtl3(TxtDocNo.Text, r));
            }

            cml.Add(EditWOHdr(SettleInd, CancelInd));
            for (int r = 0; r < Grd2.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd2, r, 2).Length > 0) cml.Add(SaveWODtl(TxtDocNo.Text, r));
                
                string MechanicCode = Sm.GetGrdStr(Grd2, r, 11);
                string MCode = String.Empty;
                int index = 0;
                int CountData = 0;
                for (int i = 1; i < MechanicCode.Length; i++)
                {
                    if (MechanicCode[i] == '#')
                    {
                        MCode = MechanicCode.Substring(index, i - index);
                        index = i + 1;
                        CountData = CountData + 1;
                        cml.Add(SaveWODtl2(TxtDocNo.Text, r, CountData, MCode));
                    }
                }
            }

            // kalau WORequest masih Open, update data Maintenance Type dan Symptom Problemnya
            var WOStatus = Sm.GetValue("Select WOStatus From TblWOR Where DocNo = '" + TxtWORDocNo.Text + "'; ");
            if (!SettleInd && !CancelInd && (WOStatus.Length > 0 && WOStatus == "O"))
                if(!ChkSettleInd.Checked && !ChkCancelInd.Checked)
                    cml.Add(UpdateWOR());

            // kalau HoursMeter awal berbeda dengan Hours Meter yg di save, bikin dokumen Hours Meter baru
            if (!SettleInd && !CancelInd && ((mInitHoursMeter != mInsertedHoursMeter) && mTOCode.Length > 0))
            {
                if (!ChkSettleInd.Checked && !ChkCancelInd.Checked)
                {
                    HMDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "HoursMeter", "TblHoursMeter");
                    cml.Add(SaveHoursMeter(HMDocNo, mTOCode, TxtDocNo.Text));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            bool CancelInd = Sm.GetValue("Select CancelInd From TblWOHdr Where DocNo = '" + TxtDocNo.Text + "'") == "Y";
            bool SettleInd = Sm.GetValue("Select SettleInd From TblWOHdr Where DocNo = '" + TxtDocNo.Text + "'") == "Y";
            
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //IsDocumentNotCancelled() ||
                IsWOAlreadyCancelled() ||
                (!mIsWorkOrderDetailEditable && IsWOAlreadySettled()) ||
                (!SettleInd && !CancelInd && IsHoursMeterNotValid()) ||
                (!SettleInd && !CancelInd && ((mInitDt == mInsertedDt) && IsHoursMeterExceed24())) ||
                (!SettleInd && !CancelInd && IsGrdValueNotValid())
                ;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsWOAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where DocNo=@Param And CancelInd='Y';",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsWOAlreadySettled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOHdr Where DocNo=@Param And SettleInd='Y';",
                TxtDocNo.Text,
                "This WO already settled.");
        }

        private bool IsWORInvalid()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblWOR Where DocNo=@Param And Description!='Source : Maintenance Schedule';",
                TxtWORDocNo.Text,
                "This WOR not from maintenance schedule.");
        }

        private MySqlCommand EditWOHdr(bool SettleInd, bool CancelInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWOHdr Set ");
            if(!SettleInd && !CancelInd)
                if (!ChkSettleInd.Checked && !ChkCancelInd.Checked)
                    SQL.AppendLine("    Remark=@Remark, ");
            SQL.AppendLine(" CancelInd=@CancelInd, CancelReason=@CancelReason, FixedInd=@FixedInd, FixedBy=@FixedBy, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");
            SQL.AppendLine("Delete From TblWODtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblWODtl2 Where DocNo=@DocNo; ");

            //SQL.AppendLine("Update TblWORDtl Set ");
            //SQL.AppendLine("    WODocNo=Null ");
            //SQL.AppendLine("Where WODocNo Is Not Null And WODocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked == true? "Y":"N");
            Sm.CmParam<String>(ref cm, "@FixedInd", ChkFixedInd.Checked == true ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@FixedBy", ChkFixedInd.Checked == true ? Gv.CurrentUserCode : "");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveWODtl3(string DocNo, int Row)
        {
            var Row2 = Sm.GetValue("Select IfNull(Max(DNo2),0)+1 From TblWODtl3 Where DocNo=@Param1 And DNo=@Param2", DocNo, Sm.GetGrdStr(Grd2, Row, 0), string.Empty);
            string Tm1 = string.Empty;
            string Tm2 = string.Empty;
            string Tm3 = string.Empty;
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblWODtl3(DocNo, DNo, DNo2, Dt1, Tm1, Dt2, Tm2, Remark, CreateBy, CreateDt) " +
                    "Select DocNo, DNo, @DNo2, Dt1, Tm1, Dt2, Tm2, Remark, CreateBy, CreateDt " +
                    "From TblWODtl Where DocNo=@DocNo And DNo=@DNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DNo2", Sm.Right("00" + Row2, 3));
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowWOHdr(DocNo);
                ShowWODtl(DocNo);
                ShowWORHistory(TxtWORDocNo.Text);
                ShowMR(DocNo);
                ShowDOReq(DocNo);
                ShowDODept(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWOHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.SettleInd, A.CancelReason, A.CancelInd, A.WORDocNo, B.Description, ");
            SQL.AppendLine("B.MtcType, B.SymProblem, A.EquipmentCode, G.SiteName, A.Remark, H.HoursMeter, B.TOCode, I.AssetName, A.Fixedind, I.DisplayName ");
            SQL.AppendLine("From TblWOHdr A ");
            SQL.AppendLine("Inner Join TblWOR B On A.WORDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblTOHdr E On B.ToCode = E.AssetCode ");
            SQL.AppendLine("Left Join TblLocation F On E.LocCode = F.LocCode ");
            SQL.AppendLine("left Join TblSite G On F.SiteCode = G.SiteCode ");
            SQL.AppendLine("left Join TblHoursMeter H On A.HmDocNo = H.DocNo  ");
            SQL.AppendLine("Inner Join TblAsset I On B.TOCode = I.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "SettleInd", "CancelReason", "CancelInd", "WORDocNo", 

                    //6-10
                    "Description", "MtcType", "SymProblem", "SiteName", "Remark",

                    //11-15
                    "HoursMeter", "TOCode", "AssetName", "FixedInd", "DisplayName",

                    //16
                    "EquipmentCode"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkSettleInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                    TxtWORDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    MeeDescription.EditValue = Sm.DrStr(dr, c[6]);
                    Sm.SetLue(LueMaintenanceType, Sm.DrStr(dr, c[7]));
                    Sm.SetLue(LueSymptomProblem, Sm.DrStr(dr, c[8]));
                    TxtSite.EditValue = Sm.DrStr(dr, c[9]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    TxtHoursMeter.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtTOCode.EditValue = Sm.DrStr(dr, c[12]);
                    TxtAssetName.EditValue = Sm.DrStr(dr, c[13]);
                    ChkFixedInd.Checked = Sm.DrStr(dr, c[14]) == "Y";
                    TxtDisplayName.EditValue = Sm.DrStr(dr, c[15]);
                    Sm.SetLue(LueEquipmentCode, Sm.DrStr(dr, c[16]));
                }, true
            );
        }

        internal void ShowWORHistory(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WORDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@WODocNo", TxtDocNo.Text);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, C.OptDesc, B.Remark ");
            SQL.AppendLine("From TblWOHdr A ");
            SQL.AppendLine("Inner Join TblWODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblOption C On C.OptCat='WOActivity' And B.WOActivity=C.OptCode ");
            SQL.AppendLine("Where A.WORDocNo=@WORDocNo ");
            if (TxtDocNo.Text.Length > 0) SQL.AppendLine("And A.DocNo<>@WODocNo ");
            SQL.AppendLine("And A.CancelInd='N';");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[]{ "DocNo", "OptDesc", "Remark" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowWODtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WOActivity, B.OptDesc As OptActivity, A.Dt1, A.Tm1, A.Dt2, A.Tm2, A.Tm3,  ");
            SQL.AppendLine("concat(Group_Concat(C.MechanicCode separator '#'), '#') Mcode, group_Concat(D.MechanicName)MName, A.BreakDownStatus, E.OptDesc As OptBreakdown, A.Remark ");
            SQL.AppendLine("From TblWODtl A ");
            SQL.AppendLine("Inner Join TblOption B On B.OptCat='WOActivity' And A.WOActivity=B.OptCode ");
            SQL.AppendLine("Inner Join TblOption E On E.OptCat='BreakdownStatus' And A.BreakdownStatus=E.OptCode ");
            SQL.AppendLine("Left Join tBLwodTL2 C On Concat(A.DocNo, A.Dno) = Concat(C.DocNo, C.Dno) ");
            SQL.AppendLine("Left Join TblMechanic D ON C.MechanicCode = D.MechanicCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString() + " Group BY A.Docno, A.DNo;",
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WOActivity", "OptActivity", "BreakdownStatus", "OptBreakdown", "Dt1", 
                    //6-10
                    "Tm1", "Dt2","Tm2", "Tm3", "MCode", 
                    //11-12
                    "MName", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("T2", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12 );
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowMR(string WODocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblmaterialRequesthdr ");
            SQL.AppendLine("Where CancelInd = 'N' And WODocNo is not null  ");
            SQL.AppendLine("And WODocNo=@WODocNo Limit 1; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WODocNo", WODocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtMRDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    }, false
                );
        }

        private void ShowDOReq(string WODocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select group_concat(distinct DocNo) As DocNo From TblDoRequestDepthdr ");
            SQL.AppendLine("Where WODocNo is not null  ");
            SQL.AppendLine("And WODocNo=@WODocNo Group BY WODocNo limit 1; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WODocNo", WODocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                       TxtDOReqDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    }, false
                );
        }

        private void ShowDODept(string WODocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDoDepthdr ");
            SQL.AppendLine("Where WODocNo is not null  ");
            SQL.AppendLine("And WODocNo=@WODocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WODocNo", WODocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDODeptDocNo.EditValue= Sm.DrStr(dr, c[0]);
                    }, false
                );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueWOActivity_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWOActivity, new Sm.RefreshLue2(Sl.SetLueOption), "WOActivity");
        }

        private void LueWOActivity_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueWOActivity_Leave(object sender, EventArgs e)
        {
            if (LueWOActivity.Visible && fAccept)
            {
                if (Sm.GetLue(LueWOActivity).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 1].Value = null;
                    Grd2.Cells[fCell.RowIndex, 2].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueWOActivity);
                    Grd2.Cells[fCell.RowIndex, 2].Value = LueWOActivity.GetColumnValue("Col2");
                }
                LueWOActivity.Visible = false;
            }

            if (fCell.RowIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                GetDateAndTimeFromWOR(0);
            }
        }

        private void DteDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDt, ref fCell, ref fAccept);
            ComputeDurationHourWithoutRow();
            GetTimePrevious();
        }

        private void DteDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void TmeTm_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.TmeKeyDown(Grd2, ref fAccept, e);
        }

        private void TmeTm_Leave(object sender, EventArgs e)
        {
            Sm.TmeLeave(TmeTm, ref fCell, ref fAccept);
            ComputeDurationHourWithoutRow();
        }

        private void LueBreakDown_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBreakDown, new Sm.RefreshLue2(Sl.SetLueOption), "BreakdownStatus");
        }

        private void LueBreakDown_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }
        private void LueBreakDown_Leave(object sender, EventArgs e)
        {
            if (LueBreakDown.Visible && fAccept)
            {
                if (Sm.GetLue(LueBreakDown).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 3].Value = null;
                    Grd2.Cells[fCell.RowIndex, 4].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 3].Value = Sm.GetLue(LueBreakDown);
                    Grd2.Cells[fCell.RowIndex, 4].Value = LueBreakDown.GetColumnValue("Col2");
                }
                LueBreakDown.Visible = false;
            }
        }

        private void LueMaintenanceType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueMaintenanceType, new Sm.RefreshLue1(SetLueMaintenanceType));
        }

        private void LueSymptomProblem_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSymptomProblem, new Sm.RefreshLue1(SetLueSymptomProblem));
        }

        private void TxtHoursMeter_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtHoursMeter, 0);
        }

        private void TxtWORDocNo_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            //{
            //     LueEquipmentCode
            //});
        }

        private void LueEquipment_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEquipmentCode, new Sm.RefreshLue1(SetLueEquipment));
        }
        
        private void TxtWORDocNo_Validated(object sender, EventArgs e)
        {

        }
        
        private void TxtTOCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsWORUseEquipment)
            {
                SetLueEquipment(ref LueEquipmentCode);
                IsEquipmentTOIsEmpty();
            }
        }

        #endregion

        #region Button Event

        private void BtnWORDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmWODlg(this));
        }

        private void BtnWORDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtWORDocNo, "WO request#", false))
            {
                var f = new FrmWOR(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtWORDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnMR_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtMRDocNo, "Material Request#", false))
            {
                var f = new FrmMaterialRequestWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtMRDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnDOReq_Click(object sender, EventArgs e)
        {
            string Doreq = TxtDOReqDocNo.Text.IndexOf(',').ToString();

            if (Decimal.Parse(Doreq) > 0)
            {
                Sm.FormShowDialog(new FrmWODlg4(this, TxtDocNo.Text));
            }
            else
            {
                if (!Sm.IsTxtEmpty(TxtDOReqDocNo, "DO request#", false))
                {
                    var f = new FrmDORequestDeptWO(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDOReqDocNo.Text;
                    f.ShowDialog();
                }
            }
        }

        private void BtnDODept_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDODeptDocNo, "DO to Department#", false))
            {
                var f = new FrmDODeptWO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDODeptDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnRecv_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRecvDocNo, "Receiving#", false))
            {
                var f = new FrmRecvWhs2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtRecvDocNo.Text;
                f.ShowDialog();
            }
        }


        private void BtnTOCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtTOCode, "TO Code", false))
            {
                var f = new FrmTO(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = TxtTOCode.Text;
                f.ShowDialog();
            }
        }

        private void BtnRPL_Click(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length > 0)
            {
                string StatusWO = Sm.GetValue("Select SettleInd From TblWOhdr Where cancelind = 'N' And DocNo = '" + TxtDocNo.Text + "' ");
                if (StatusWO.Length > 0 && StatusWO == "N")
                {
                    try
                    {
                        string xMenuCode = Sm.GetValue("select MenuCode From tblMenu Where Param ='FrmDORequestDeptWO'");
                        var f = new FrmDORequestDeptWO(xMenuCode);
                        f.mDocType = "1";
                        f.Tag = xMenuCode;
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.TxtWODocNo.Text = TxtDocNo.Text;
                        f.ShowDialog();
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }
                else
                {
                    Sm.StdMsg(mMsgType.Info, "this document already settle");
                }
            }
        }

        #endregion

        #region Grid Event

        #region Grid 1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                var WODocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 0);
                if (WODocNo.Length > 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" "))
                    {
                        var f = new FrmWO("XXX");
                        f.Tag = "XXX";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = WODocNo;
                        f.ShowDialog();
                    }
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            var WODocNo=Sm.GetGrdStr(Grd1, e.RowIndex, 0);
            if (e.ColIndex == 1 && WODocNo.Length > 0)
            {
                var f = new FrmWO("XXX");
                f.Tag = "XXX";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = WODocNo;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid 2

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 2, 4, 5, 6, 7, 8, 10 }, e.ColIndex))
                {
                    if (e.ColIndex == 2) Sm.LueRequestEdit(ref Grd2, ref LueWOActivity, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 4) Sm.LueRequestEdit(ref Grd2, ref LueBreakDown, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 5 || e.ColIndex == 7) Sm.DteRequestEdit(Grd2, DteDt, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 6 || e.ColIndex == 8) Sm.TmeRequestEdit(Grd2, TmeTm, ref fCell, ref fAccept, e);
                    if (e.ColIndex == 10 && ChkSettleInd.Checked == false) Sm.FormShowDialog(new FrmWODlg3(this, e.RowIndex));
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                }
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 14 && TxtDocNo.Text.Length != 0 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmWODlg5(this, TxtDocNo.Text, Sm.GetGrdStr(Grd2, e.RowIndex, 0));
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.ShowDialog();
            }

            if (e.ColIndex == 1 && !Sm.IsTxtEmpty(TxtWORDocNo, "WO request#", false))
                Sm.FormShowDialog(new FrmWODlg2(this, TxtWORDocNo.Text));

            if (!mIsMechanicWOEditable)
            {
                if (e.ColIndex == 10 && Sm.GetGrdStr(Grd2, e.RowIndex, 0).Length == 0 && ChkSettleInd.Checked==false)
                {
                    Sm.FormShowDialog(new FrmWODlg3(this, e.RowIndex));
                }
            }
            else
            {
                if (e.ColIndex == 10 && ChkSettleInd.Checked==false)
                {
                    Sm.FormShowDialog(new FrmWODlg3(this, e.RowIndex));
                }
            }
        }


        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 10 }, e);
            if (mIsWorkOrderDetailEditable) ComputeDurationHourWithoutRow();
        }
    
        #endregion

        #endregion
       
        #endregion

        #region Report Class

        private class WOHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WORDocNo { get; set; }
            public string Description { get; set; }
            public string SiteName { get; set; }
            public string WOStatus { get; set; }
            public string MtcStatusDesc { get; set; }
            public string MtcTypeDesc { get; set; }
            public string SymProblemDesc { get; set; }
            public string HoursMeter { get; set; }
            public string Remark { get; set; }
            public string TOCOde { get; set; }
            public string PrintBy { get; set; }
            public string CreateBy { get; set; }
            public string CompanyAddressCity { get; set; }
        }

        private class WODtl
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string BDDocDt { get; set; }
            public string BreakdownStatus { get; set; }
            public string WOActivity { get; set; }
            public string DtStart { get; set; }
            public string StartTm { get; set; }
            public string DtEnd { get; set; }
            public string EndTm { get; set; }
            public string HT { get; set; }
            public string RemarkDtl { get; set; }
        }

        private class WODtl2
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string PosName { get; set; }
        }

        private class WODtl3
        {
            public string TotalHm { get; set; }
        }

        #endregion
    }
}
