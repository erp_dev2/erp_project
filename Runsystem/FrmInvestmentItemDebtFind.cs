﻿#region Update
/*
 * 20/04/2022 [RDA/PRODUCT] new find
 * 24/05/2022 [SET/Product] merubah source Annual Days Assumption menjadi tbloption -> AnnualDaysAssumption
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmInvestmentItemDebtFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmInvestmentItemDebt mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmInvestmentItemDebtFind(FrmInvestmentItemDebt FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetSQL();
            SetGrd();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.InvestmentDebtCode,A.PortofolioId,A.PortofolioName,A.ForeignName,A.ActInd, ");
            SQL.AppendLine("A.InvestmentCtCode,B.InvestmentCtName,A.CurCode,C.CurName,A.Issuer,A.UomCode,A.IssuedAmt, ");
            SQL.AppendLine("A.Specification,A.UpdatedDt,A.ListingDt,A.MaturityDt,A.InterestRateAmt, ");
            SQL.AppendLine("A.InterestType,A.InterestFreq,D.OptDesc AnnualDays,A.Remark, ");
            SQL.AppendLine("A.CreateBy,A.CreateDt,A.LastUpBy,A.LastUpDt ");
            SQL.AppendLine("FROM tblinvestmentitemdebt A ");
            SQL.AppendLine("INNER JOIN tblinvestmentcategory B ON A.InvestmentCtCode = B.InvestmentCtCode ");
            SQL.AppendLine("INNER JOIN tblcurrency C ON A.CurCode = C.CurCode ");
            SQL.AppendLine("INNER JOIN tbloption D ON A.AnnualDays = D.OptCode AND D.OptCat = 'AnnualDaysAssumption' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Investment"+Environment.NewLine+"Debt Code",
                        "Investment"+Environment.NewLine+"Code",
                        "Investment"+Environment.NewLine+"Name",
                        "Foreign"+Environment.NewLine+"Name",
                        "Active", 

                        //6-10
                        "Investment"+Environment.NewLine+"Category Code",
                        "Investment"+Environment.NewLine+"Category Name",
                        "Currency"+Environment.NewLine+"Code",
                        "Currency",
                        "Issuer",

                        //11-15
                        "UoM",
                        "Issued"+Environment.NewLine+"Amount",
                        "Specification",
                        "Updated"+Environment.NewLine+"Date",
                        "Listing"+Environment.NewLine+"Date",

                        //16-20
                        "Maturity"+Environment.NewLine+"Date",
                        "Interest Rate"+Environment.NewLine+"Amount",
                        "Interest"+Environment.NewLine+"Type",
                        "Interest"+Environment.NewLine+"Frequency",
                        "Annual Days"+Environment.NewLine+"Assumption",

                        //21-25
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",

                        //26-27
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        200, 150, 150, 80, 200,

                        //6-10
                        250, 200, 200, 250, 150,

                        //11-15
                        150, 150, 150, 150, 150,

                        //16-20
                        150, 150, 150, 150, 150,

                        //21-25
                        150, 150, 150, 150, 150,

                        //26-27
                        150, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 14, 15, 16, 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 24, 27 });
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 21, 22, 23, 24, 25, 26, 27 }, false);
            //Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 8, 21, 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtInvestmentDebt.Text, new string[] { "A.InvestmentDebtCode", "A.PortofolioId", "A.PortofolioName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By A.InvestmentDebtCode;",
                        new string[]
                        {
                            //0
                            "InvestmentDebtCode", 

                            //1-5
                            "PortofolioId","PortofolioName","ForeignName","ActInd","InvestmentCtCode",

                            //6-10
                            "InvestmentCtName","CurCode","CurName","Issuer","UomCode",

                            //11-15
                            "IssuedAmt","Specification","UpdatedDt","ListingDt","MaturityDt",

                            //16-20
                            "InterestRateAmt","InterestType","InterestFreq","AnnualDays","Remark",

                            //21-24
                            "CreateBy","CreateDt","LastUpBy","LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 24);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }




        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtInvestmentDebt_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkInvestmentDebt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Investment debt ");
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #endregion
    }
}
