﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptVoucherRequestOutstanding : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty,  mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptVoucherRequestOutstanding(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -3);
            //Sl.SetLueDeptCode(ref LueDeptCode);
            base.FrmLoad(sender, e);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("    Select ");
            SQL.AppendLine("    A.DocNo, A.DocDt, B.OptDesc As DocType, C.OptDesc As AcType, ");
            SQL.AppendLine("    D.OptDesc As PaymentType, A.PIC, A.Amt, A.Remark ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join TblOption B On A.DocType = B.OptCode And B.OptCat = 'VoucherDocType' ");
            SQL.AppendLine("    Left Join TblOption C On A.AcType = C.OptCode And C.OptCat = 'AccountType' ");
            SQL.AppendLine("    Left Join TblOption D On A.PaymentType = D.OptCode And D.OptCat = 'VoucherPaymentType' ");
            //SQL.AppendLine("    Where A.VoucherDocNo Is Null And A.CancelInd = 'N' ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "",
                        "Document"+Environment.NewLine+"Type",
                        "Account"+Environment.NewLine+" Type",

                        //6-9
                        "Payment"+Environment.NewLine+"Type",
                        "Person in"+Environment.NewLine+"Charge",
                        "Amount",
                        "Remark",
                        
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9 }, false);
           // Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            //Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
            //Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where A.VoucherDocNo Is Null And A.CancelInd = 'N' ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                //Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                //Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "E.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DocType", "AcType", "PaymentType", "PIC", 
                            
                            //6-7
                            "Amt", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);

                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);

                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                //Sm.FocusGrd(Grd1, 1, (ChkHideInfoInGrd.Checked ? 1 : 1));
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    e.DoDefault = false;
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
                if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }
 

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }


        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);  
        }

        
        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        #endregion

        #endregion

    }
}
