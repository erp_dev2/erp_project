﻿namespace RunSystem
{
    partial class FrmRptLOPToVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDOCtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtDOVDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkDOCtDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.ChkDOVDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkDRDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtLOPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkLOPDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtBOQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkBOQDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSOCDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkSOCDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBOMDocNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkBOMDocNo = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOCtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOVDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDOCtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDOVDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLOPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBOQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSOCDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOMDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBOMDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtBOMDocNo);
            this.panel2.Controls.Add(this.ChkBOMDocNo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtSOCDocNo);
            this.panel2.Controls.Add(this.ChkSOCDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtBOQDocNo);
            this.panel2.Controls.Add(this.ChkBOQDocNo);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtLOPDocNo);
            this.panel2.Controls.Add(this.ChkLOPDocNo);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Size = new System.Drawing.Size(772, 93);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 380);
            this.Grd1.TabIndex = 32;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 93);
            this.panel3.Size = new System.Drawing.Size(772, 380);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.TxtDOCtDocNo);
            this.panel5.Controls.Add(this.TxtDOVDocNo);
            this.panel5.Controls.Add(this.ChkDOCtDocNo);
            this.panel5.Controls.Add(this.ChkDOVDocNo);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.ChkDRDocNo);
            this.panel5.Controls.Add(this.TxtDRDocNo);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(385, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(383, 89);
            this.panel5.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 14);
            this.label2.TabIndex = 29;
            this.label2.Text = "DO Verification#";
            // 
            // TxtDOCtDocNo
            // 
            this.TxtDOCtDocNo.EnterMoveNextControl = true;
            this.TxtDOCtDocNo.Location = new System.Drawing.Point(118, 3);
            this.TxtDOCtDocNo.Name = "TxtDOCtDocNo";
            this.TxtDOCtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDOCtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDOCtDocNo.Properties.MaxLength = 30;
            this.TxtDOCtDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDOCtDocNo.TabIndex = 24;
            this.TxtDOCtDocNo.Validated += new System.EventHandler(this.TxtDOCtDocNo_Validated);
            // 
            // TxtDOVDocNo
            // 
            this.TxtDOVDocNo.EnterMoveNextControl = true;
            this.TxtDOVDocNo.Location = new System.Drawing.Point(118, 45);
            this.TxtDOVDocNo.Name = "TxtDOVDocNo";
            this.TxtDOVDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDOVDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDOVDocNo.Properties.MaxLength = 30;
            this.TxtDOVDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDOVDocNo.TabIndex = 30;
            this.TxtDOVDocNo.Validated += new System.EventHandler(this.TxtDOVDocNo_Validated);
            // 
            // ChkDOCtDocNo
            // 
            this.ChkDOCtDocNo.Location = new System.Drawing.Point(362, 2);
            this.ChkDOCtDocNo.Name = "ChkDOCtDocNo";
            this.ChkDOCtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDOCtDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDOCtDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDOCtDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDOCtDocNo.Properties.Caption = " ";
            this.ChkDOCtDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDOCtDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkDOCtDocNo.TabIndex = 25;
            this.ChkDOCtDocNo.ToolTip = "Remove filter";
            this.ChkDOCtDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDOCtDocNo.ToolTipTitle = "Run System";
            this.ChkDOCtDocNo.CheckedChanged += new System.EventHandler(this.ChkDOCtDocNo_CheckedChanged);
            // 
            // ChkDOVDocNo
            // 
            this.ChkDOVDocNo.Location = new System.Drawing.Point(362, 44);
            this.ChkDOVDocNo.Name = "ChkDOVDocNo";
            this.ChkDOVDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDOVDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDOVDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDOVDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDOVDocNo.Properties.Caption = " ";
            this.ChkDOVDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDOVDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkDOVDocNo.TabIndex = 31;
            this.ChkDOVDocNo.ToolTip = "Remove filter";
            this.ChkDOVDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDOVDocNo.ToolTipTitle = "Run System";
            this.ChkDOVDocNo.CheckedChanged += new System.EventHandler(this.ChkDOVDocNo_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "DO To Customer#";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 14);
            this.label5.TabIndex = 26;
            this.label5.Text = "Delivery Request#";
            // 
            // ChkDRDocNo
            // 
            this.ChkDRDocNo.Location = new System.Drawing.Point(362, 23);
            this.ChkDRDocNo.Name = "ChkDRDocNo";
            this.ChkDRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDRDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkDRDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDRDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDRDocNo.Properties.Caption = " ";
            this.ChkDRDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDRDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkDRDocNo.TabIndex = 28;
            this.ChkDRDocNo.ToolTip = "Remove filter";
            this.ChkDRDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDRDocNo.ToolTipTitle = "Run System";
            this.ChkDRDocNo.CheckedChanged += new System.EventHandler(this.ChkDRDocNo_CheckedChanged);
            // 
            // TxtDRDocNo
            // 
            this.TxtDRDocNo.EnterMoveNextControl = true;
            this.TxtDRDocNo.Location = new System.Drawing.Point(118, 24);
            this.TxtDRDocNo.Name = "TxtDRDocNo";
            this.TxtDRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDRDocNo.Properties.MaxLength = 30;
            this.TxtDRDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtDRDocNo.TabIndex = 27;
            this.TxtDRDocNo.Validated += new System.EventHandler(this.TxtDRDocNo_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(54, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "LOP#";
            // 
            // TxtLOPDocNo
            // 
            this.TxtLOPDocNo.EnterMoveNextControl = true;
            this.TxtLOPDocNo.Location = new System.Drawing.Point(95, 1);
            this.TxtLOPDocNo.Name = "TxtLOPDocNo";
            this.TxtLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLOPDocNo.Properties.MaxLength = 30;
            this.TxtLOPDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtLOPDocNo.TabIndex = 11;
            this.TxtLOPDocNo.Validated += new System.EventHandler(this.TxtLOPDocNo_Validated);
            // 
            // ChkLOPDocNo
            // 
            this.ChkLOPDocNo.Location = new System.Drawing.Point(339, 0);
            this.ChkLOPDocNo.Name = "ChkLOPDocNo";
            this.ChkLOPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLOPDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkLOPDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLOPDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLOPDocNo.Properties.Caption = " ";
            this.ChkLOPDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLOPDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkLOPDocNo.TabIndex = 12;
            this.ChkLOPDocNo.ToolTip = "Remove filter";
            this.ChkLOPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLOPDocNo.ToolTipTitle = "Run System";
            this.ChkLOPDocNo.CheckedChanged += new System.EventHandler(this.ChkLOPDocNo_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(51, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "BOQ#";
            // 
            // TxtBOQDocNo
            // 
            this.TxtBOQDocNo.EnterMoveNextControl = true;
            this.TxtBOQDocNo.Location = new System.Drawing.Point(95, 24);
            this.TxtBOQDocNo.Name = "TxtBOQDocNo";
            this.TxtBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOQDocNo.Properties.MaxLength = 30;
            this.TxtBOQDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtBOQDocNo.TabIndex = 14;
            this.TxtBOQDocNo.Validated += new System.EventHandler(this.TxtBOQDocNo_Validated);
            // 
            // ChkBOQDocNo
            // 
            this.ChkBOQDocNo.Location = new System.Drawing.Point(339, 23);
            this.ChkBOQDocNo.Name = "ChkBOQDocNo";
            this.ChkBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkBOQDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBOQDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBOQDocNo.Properties.Caption = " ";
            this.ChkBOQDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBOQDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkBOQDocNo.TabIndex = 15;
            this.ChkBOQDocNo.ToolTip = "Remove filter";
            this.ChkBOQDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBOQDocNo.ToolTipTitle = "Run System";
            this.ChkBOQDocNo.CheckedChanged += new System.EventHandler(this.ChkBOQDocNo_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "SO Contract#";
            // 
            // TxtSOCDocNo
            // 
            this.TxtSOCDocNo.EnterMoveNextControl = true;
            this.TxtSOCDocNo.Location = new System.Drawing.Point(95, 45);
            this.TxtSOCDocNo.Name = "TxtSOCDocNo";
            this.TxtSOCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOCDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOCDocNo.Properties.MaxLength = 30;
            this.TxtSOCDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtSOCDocNo.TabIndex = 17;
            this.TxtSOCDocNo.Validated += new System.EventHandler(this.TxtSOCDocNo_Validated);
            // 
            // ChkSOCDocNo
            // 
            this.ChkSOCDocNo.Location = new System.Drawing.Point(339, 44);
            this.ChkSOCDocNo.Name = "ChkSOCDocNo";
            this.ChkSOCDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkSOCDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkSOCDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkSOCDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkSOCDocNo.Properties.Caption = " ";
            this.ChkSOCDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkSOCDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkSOCDocNo.TabIndex = 18;
            this.ChkSOCDocNo.ToolTip = "Remove filter";
            this.ChkSOCDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkSOCDocNo.ToolTipTitle = "Run System";
            this.ChkSOCDocNo.CheckedChanged += new System.EventHandler(this.ChkSOCDocNo_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(51, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "BOM#";
            // 
            // TxtBOMDocNo
            // 
            this.TxtBOMDocNo.EnterMoveNextControl = true;
            this.TxtBOMDocNo.Location = new System.Drawing.Point(95, 67);
            this.TxtBOMDocNo.Name = "TxtBOMDocNo";
            this.TxtBOMDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOMDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOMDocNo.Properties.MaxLength = 30;
            this.TxtBOMDocNo.Size = new System.Drawing.Size(242, 20);
            this.TxtBOMDocNo.TabIndex = 20;
            this.TxtBOMDocNo.Validated += new System.EventHandler(this.TxtBOMDocNo_Validated);
            // 
            // ChkBOMDocNo
            // 
            this.ChkBOMDocNo.Location = new System.Drawing.Point(339, 66);
            this.ChkBOMDocNo.Name = "ChkBOMDocNo";
            this.ChkBOMDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBOMDocNo.Properties.Appearance.Options.UseFont = true;
            this.ChkBOMDocNo.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBOMDocNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBOMDocNo.Properties.Caption = " ";
            this.ChkBOMDocNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBOMDocNo.Size = new System.Drawing.Size(22, 22);
            this.ChkBOMDocNo.TabIndex = 21;
            this.ChkBOMDocNo.ToolTip = "Remove filter";
            this.ChkBOMDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBOMDocNo.ToolTipTitle = "Run System";
            this.ChkBOMDocNo.CheckedChanged += new System.EventHandler(this.ChkBOMDocNo_CheckedChanged);
            // 
            // FrmRptLOPToVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptLOPToVoucher";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOCtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDOVDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDOCtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDOVDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLOPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBOQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOCDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkSOCDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOMDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBOMDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtBOMDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkBOMDocNo;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtSOCDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkSOCDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtBOQDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkBOQDocNo;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtLOPDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkLOPDocNo;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtDOCtDocNo;
        private DevExpress.XtraEditors.TextEdit TxtDOVDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkDOCtDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkDOVDocNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkDRDocNo;
        private DevExpress.XtraEditors.TextEdit TxtDRDocNo;
    }
}