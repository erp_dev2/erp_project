﻿namespace RunSystem
{
    partial class FrmFormulaSpinningNonItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtFSNIName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtFSNICode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkTotalInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtParam1 = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtParam2 = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtParam4 = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtParam3 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtParam6 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtParam5 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeQuery = new DevExpress.XtraEditors.MemoExEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFSNIName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFSNICode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTotalInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeQuery.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 275);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MeeQuery);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TxtParam6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.TxtParam5);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtParam4);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtParam3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtParam2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtParam1);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.ChkTotalInd);
            this.panel2.Controls.Add(this.TxtFSNIName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtFSNICode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 275);
            // 
            // TxtFSNIName
            // 
            this.TxtFSNIName.EnterMoveNextControl = true;
            this.TxtFSNIName.Location = new System.Drawing.Point(145, 31);
            this.TxtFSNIName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFSNIName.Name = "TxtFSNIName";
            this.TxtFSNIName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFSNIName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFSNIName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFSNIName.Properties.Appearance.Options.UseFont = true;
            this.TxtFSNIName.Properties.MaxLength = 40;
            this.TxtFSNIName.Size = new System.Drawing.Size(422, 20);
            this.TxtFSNIName.TabIndex = 14;
            this.TxtFSNIName.Validated += new System.EventHandler(this.TxtFSNIName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(102, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFSNICode
            // 
            this.TxtFSNICode.EnterMoveNextControl = true;
            this.TxtFSNICode.Location = new System.Drawing.Point(145, 9);
            this.TxtFSNICode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFSNICode.Name = "TxtFSNICode";
            this.TxtFSNICode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFSNICode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFSNICode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFSNICode.Properties.Appearance.Options.UseFont = true;
            this.TxtFSNICode.Properties.MaxLength = 16;
            this.TxtFSNICode.Size = new System.Drawing.Size(152, 20);
            this.TxtFSNICode.TabIndex = 10;
            this.TxtFSNICode.Validated += new System.EventHandler(this.TxtFSNICode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(105, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkTotalInd
            // 
            this.ChkTotalInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTotalInd.Location = new System.Drawing.Point(368, 9);
            this.ChkTotalInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTotalInd.Name = "ChkTotalInd";
            this.ChkTotalInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTotalInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTotalInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTotalInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTotalInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTotalInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTotalInd.Properties.Caption = "For Total Calculation";
            this.ChkTotalInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTotalInd.ShowToolTips = false;
            this.ChkTotalInd.Size = new System.Drawing.Size(140, 22);
            this.ChkTotalInd.TabIndex = 12;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(301, 9);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.ShowToolTips = false;
            this.ChkActInd.Size = new System.Drawing.Size(64, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // TxtParam1
            // 
            this.TxtParam1.EnterMoveNextControl = true;
            this.TxtParam1.Location = new System.Drawing.Point(145, 75);
            this.TxtParam1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParam1.Name = "TxtParam1";
            this.TxtParam1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParam1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParam1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParam1.Properties.Appearance.Options.UseFont = true;
            this.TxtParam1.Properties.MaxLength = 100;
            this.TxtParam1.Size = new System.Drawing.Size(422, 20);
            this.TxtParam1.TabIndex = 18;
            this.TxtParam1.Validated += new System.EventHandler(this.TxtParam1_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(66, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Parameter 1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParam2
            // 
            this.TxtParam2.EnterMoveNextControl = true;
            this.TxtParam2.Location = new System.Drawing.Point(145, 97);
            this.TxtParam2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParam2.Name = "TxtParam2";
            this.TxtParam2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParam2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParam2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParam2.Properties.Appearance.Options.UseFont = true;
            this.TxtParam2.Properties.MaxLength = 100;
            this.TxtParam2.Size = new System.Drawing.Size(422, 20);
            this.TxtParam2.TabIndex = 20;
            this.TxtParam2.Validated += new System.EventHandler(this.TxtParam2_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(66, 101);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Parameter 2";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParam4
            // 
            this.TxtParam4.EnterMoveNextControl = true;
            this.TxtParam4.Location = new System.Drawing.Point(145, 141);
            this.TxtParam4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParam4.Name = "TxtParam4";
            this.TxtParam4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParam4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParam4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParam4.Properties.Appearance.Options.UseFont = true;
            this.TxtParam4.Properties.MaxLength = 100;
            this.TxtParam4.Size = new System.Drawing.Size(422, 20);
            this.TxtParam4.TabIndex = 24;
            this.TxtParam4.Validated += new System.EventHandler(this.TxtParam4_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(66, 145);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Parameter 4";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParam3
            // 
            this.TxtParam3.EnterMoveNextControl = true;
            this.TxtParam3.Location = new System.Drawing.Point(145, 119);
            this.TxtParam3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParam3.Name = "TxtParam3";
            this.TxtParam3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParam3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParam3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParam3.Properties.Appearance.Options.UseFont = true;
            this.TxtParam3.Properties.MaxLength = 100;
            this.TxtParam3.Size = new System.Drawing.Size(422, 20);
            this.TxtParam3.TabIndex = 22;
            this.TxtParam3.Validated += new System.EventHandler(this.TxtParam3_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(66, 123);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Parameter 3";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParam6
            // 
            this.TxtParam6.EnterMoveNextControl = true;
            this.TxtParam6.Location = new System.Drawing.Point(145, 185);
            this.TxtParam6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParam6.Name = "TxtParam6";
            this.TxtParam6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParam6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParam6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParam6.Properties.Appearance.Options.UseFont = true;
            this.TxtParam6.Properties.MaxLength = 100;
            this.TxtParam6.Size = new System.Drawing.Size(422, 20);
            this.TxtParam6.TabIndex = 28;
            this.TxtParam6.Validated += new System.EventHandler(this.TxtParam6_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(66, 189);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 14);
            this.label7.TabIndex = 27;
            this.label7.Text = "Parameter 6";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtParam5
            // 
            this.TxtParam5.EnterMoveNextControl = true;
            this.TxtParam5.Location = new System.Drawing.Point(145, 163);
            this.TxtParam5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtParam5.Name = "TxtParam5";
            this.TxtParam5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtParam5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtParam5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtParam5.Properties.Appearance.Options.UseFont = true;
            this.TxtParam5.Properties.MaxLength = 100;
            this.TxtParam5.Size = new System.Drawing.Size(422, 20);
            this.TxtParam5.TabIndex = 26;
            this.TxtParam5.Validated += new System.EventHandler(this.TxtParam5_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(66, 167);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 14);
            this.label8.TabIndex = 25;
            this.label8.Text = "Parameter 5";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(145, 207);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(422, 20);
            this.MeeRemark.TabIndex = 30;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            this.MeeRemark.Validated += new System.EventHandler(this.MeeRemark_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(93, 210);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 29;
            this.label12.Text = "Remark";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeQuery
            // 
            this.MeeQuery.EnterMoveNextControl = true;
            this.MeeQuery.Location = new System.Drawing.Point(145, 53);
            this.MeeQuery.Margin = new System.Windows.Forms.Padding(5);
            this.MeeQuery.Name = "MeeQuery";
            this.MeeQuery.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQuery.Properties.Appearance.Options.UseFont = true;
            this.MeeQuery.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQuery.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeQuery.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQuery.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeQuery.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQuery.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeQuery.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeQuery.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeQuery.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeQuery.Properties.MaxLength = 500;
            this.MeeQuery.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeQuery.Properties.ShowIcon = false;
            this.MeeQuery.Size = new System.Drawing.Size(422, 20);
            this.MeeQuery.TabIndex = 16;
            this.MeeQuery.ToolTip = "F4 : Show/hide text";
            this.MeeQuery.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeQuery.ToolTipTitle = "Run System";
            this.MeeQuery.Validated += new System.EventHandler(this.MeeQuery_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(100, 56);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 14);
            this.label9.TabIndex = 15;
            this.label9.Text = "Query";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmFormulaSpinningNonItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 275);
            this.Name = "FrmFormulaSpinningNonItem";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFSNIName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFSNICode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTotalInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtParam5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeQuery.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtFSNIName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtFSNICode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private DevExpress.XtraEditors.CheckEdit ChkTotalInd;
        private DevExpress.XtraEditors.TextEdit TxtParam6;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit TxtParam5;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit TxtParam4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtParam3;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtParam2;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtParam1;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoExEdit MeeQuery;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label12;
    }
}