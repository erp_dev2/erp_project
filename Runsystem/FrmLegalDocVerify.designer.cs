﻿namespace RunSystem
{
    partial class FrmLegalDocVerify
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLegalDocVerify));
            this.panel6 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtTTCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtQueueNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnQueueNo = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtVehicleRegNo = new DevExpress.XtraEditors.TextEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtVdName = new DevExpress.XtraEditors.TextEdit();
            this.BtnVdCode = new DevExpress.XtraEditors.SimpleButton();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtProvCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtCityCode = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtSDCode = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtVilCode = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtIdentityNo = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.DteDLDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.LueDLCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.TxtVdName1 = new DevExpress.XtraEditors.TextEdit();
            this.BtnVdCode1 = new DevExpress.XtraEditors.SimpleButton();
            this.label27 = new System.Windows.Forms.Label();
            this.MeeAddress1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtProvCode1 = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtCityCode1 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtSDCode1 = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtVilCode1 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtIdentityNo1 = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.DteDLDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.LueDLCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtVdName2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnVdCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.label26 = new System.Windows.Forms.Label();
            this.MeeAddress2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtProvCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtCityCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtSDCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtVilCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtIdentityNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVehicleRegNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProvCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSDCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVilCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDLCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProvCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSDCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVilCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo1.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDLCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProvCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSDCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVilCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.TxtTTCode);
            this.panel6.Controls.Add(this.TxtQueueNo);
            this.panel6.Controls.Add(this.BtnQueueNo);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.TxtVehicleRegNo);
            this.panel6.Controls.Add(this.ChkCancelInd);
            this.panel6.Controls.Add(this.DteDocDt);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.TxtDocNo);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(772, 118);
            this.panel6.TabIndex = 8;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(28, 94);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(60, 14);
            this.label28.TabIndex = 19;
            this.label28.Text = "Transport";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTTCode
            // 
            this.TxtTTCode.EnterMoveNextControl = true;
            this.TxtTTCode.Location = new System.Drawing.Point(94, 92);
            this.TxtTTCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTTCode.Name = "TxtTTCode";
            this.TxtTTCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTTCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTTCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTTCode.Properties.MaxLength = 40;
            this.TxtTTCode.Size = new System.Drawing.Size(274, 20);
            this.TxtTTCode.TabIndex = 20;
            // 
            // TxtQueueNo
            // 
            this.TxtQueueNo.EnterMoveNextControl = true;
            this.TxtQueueNo.Location = new System.Drawing.Point(94, 48);
            this.TxtQueueNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQueueNo.Name = "TxtQueueNo";
            this.TxtQueueNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQueueNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQueueNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQueueNo.Properties.MaxLength = 10;
            this.TxtQueueNo.Size = new System.Drawing.Size(255, 20);
            this.TxtQueueNo.TabIndex = 15;
            // 
            // BtnQueueNo
            // 
            this.BtnQueueNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnQueueNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnQueueNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQueueNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnQueueNo.Appearance.Options.UseBackColor = true;
            this.BtnQueueNo.Appearance.Options.UseFont = true;
            this.BtnQueueNo.Appearance.Options.UseForeColor = true;
            this.BtnQueueNo.Appearance.Options.UseTextOptions = true;
            this.BtnQueueNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnQueueNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnQueueNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnQueueNo.Image")));
            this.BtnQueueNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnQueueNo.Location = new System.Drawing.Point(348, 47);
            this.BtnQueueNo.Name = "BtnQueueNo";
            this.BtnQueueNo.Size = new System.Drawing.Size(24, 21);
            this.BtnQueueNo.TabIndex = 16;
            this.BtnQueueNo.ToolTip = "Find Sales Order\'s Document Number";
            this.BtnQueueNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnQueueNo.ToolTipTitle = "Run System";
            this.BtnQueueNo.Click += new System.EventHandler(this.BtnQueueNo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(2, 51);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 14);
            this.label4.TabIndex = 14;
            this.label4.Text = "Nomor Antrian";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 72);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Nomor Polisi";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVehicleRegNo
            // 
            this.TxtVehicleRegNo.EnterMoveNextControl = true;
            this.TxtVehicleRegNo.Location = new System.Drawing.Point(94, 70);
            this.TxtVehicleRegNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVehicleRegNo.Name = "TxtVehicleRegNo";
            this.TxtVehicleRegNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVehicleRegNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVehicleRegNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVehicleRegNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVehicleRegNo.Properties.MaxLength = 40;
            this.TxtVehicleRegNo.Size = new System.Drawing.Size(274, 20);
            this.TxtVehicleRegNo.TabIndex = 18;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(311, 3);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Batal";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 11;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(94, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(121, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(7, 29);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 14);
            this.label17.TabIndex = 12;
            this.label17.Text = "Tgl. Dokumen";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(94, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(212, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(9, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 14);
            this.label18.TabIndex = 9;
            this.label18.Text = "No. Dokumen";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 118);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(772, 355);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(25, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(743, 347);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Vendor";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.TxtVdName);
            this.panel3.Controls.Add(this.BtnVdCode);
            this.panel3.Controls.Add(this.MeeAddress);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.TxtProvCode);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TxtCityCode);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtSDCode);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.TxtVilCode);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.TxtIdentityNo);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(737, 341);
            this.panel3.TabIndex = 20;
            // 
            // TxtVdName
            // 
            this.TxtVdName.EnterMoveNextControl = true;
            this.TxtVdName.Location = new System.Drawing.Point(98, 12);
            this.TxtVdName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdName.Name = "TxtVdName";
            this.TxtVdName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdName.Properties.Appearance.Options.UseFont = true;
            this.TxtVdName.Properties.MaxLength = 200;
            this.TxtVdName.Size = new System.Drawing.Size(604, 20);
            this.TxtVdName.TabIndex = 22;
            // 
            // BtnVdCode
            // 
            this.BtnVdCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVdCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVdCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVdCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVdCode.Appearance.Options.UseBackColor = true;
            this.BtnVdCode.Appearance.Options.UseFont = true;
            this.BtnVdCode.Appearance.Options.UseForeColor = true;
            this.BtnVdCode.Appearance.Options.UseTextOptions = true;
            this.BtnVdCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVdCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVdCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnVdCode.Image")));
            this.BtnVdCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVdCode.Location = new System.Drawing.Point(704, 11);
            this.BtnVdCode.Name = "BtnVdCode";
            this.BtnVdCode.Size = new System.Drawing.Size(24, 21);
            this.BtnVdCode.TabIndex = 23;
            this.BtnVdCode.ToolTip = "Find Sales Order\'s Document Number";
            this.BtnVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVdCode.ToolTipTitle = "Run System";
            this.BtnVdCode.Click += new System.EventHandler(this.BtnVdCode_Click);
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(98, 56);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 400;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(630, 20);
            this.MeeAddress.TabIndex = 26;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(51, 59);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Alamat";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProvCode
            // 
            this.TxtProvCode.EnterMoveNextControl = true;
            this.TxtProvCode.Location = new System.Drawing.Point(98, 144);
            this.TxtProvCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProvCode.Name = "TxtProvCode";
            this.TxtProvCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProvCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProvCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProvCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProvCode.Properties.MaxLength = 40;
            this.TxtProvCode.Size = new System.Drawing.Size(630, 20);
            this.TxtProvCode.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(48, 147);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 33;
            this.label8.Text = "Provinsi";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCityCode
            // 
            this.TxtCityCode.EnterMoveNextControl = true;
            this.TxtCityCode.Location = new System.Drawing.Point(98, 122);
            this.TxtCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCityCode.Name = "TxtCityCode";
            this.TxtCityCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCityCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCityCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCityCode.Properties.MaxLength = 40;
            this.TxtCityCode.Size = new System.Drawing.Size(630, 20);
            this.TxtCityCode.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(29, 125);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 14);
            this.label7.TabIndex = 31;
            this.label7.Text = "Kabupaten";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSDCode
            // 
            this.TxtSDCode.EnterMoveNextControl = true;
            this.TxtSDCode.Location = new System.Drawing.Point(98, 100);
            this.TxtSDCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSDCode.Name = "TxtSDCode";
            this.TxtSDCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSDCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSDCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSDCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSDCode.Properties.MaxLength = 40;
            this.TxtSDCode.Size = new System.Drawing.Size(630, 20);
            this.TxtSDCode.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(28, 103);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Kecamatan";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVilCode
            // 
            this.TxtVilCode.EnterMoveNextControl = true;
            this.TxtVilCode.Location = new System.Drawing.Point(98, 78);
            this.TxtVilCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVilCode.Name = "TxtVilCode";
            this.TxtVilCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVilCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVilCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVilCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVilCode.Properties.MaxLength = 40;
            this.TxtVilCode.Size = new System.Drawing.Size(630, 20);
            this.TxtVilCode.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(35, 81);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Kelurahan";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdentityNo
            // 
            this.TxtIdentityNo.EnterMoveNextControl = true;
            this.TxtIdentityNo.Location = new System.Drawing.Point(98, 34);
            this.TxtIdentityNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentityNo.Name = "TxtIdentityNo";
            this.TxtIdentityNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdentityNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentityNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdentityNo.Properties.Appearance.Options.UseFont = true;
            this.TxtIdentityNo.Properties.MaxLength = 20;
            this.TxtIdentityNo.Size = new System.Drawing.Size(276, 20);
            this.TxtIdentityNo.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(17, 37);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "No. Identitas";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(48, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 14);
            this.label2.TabIndex = 21;
            this.label2.Text = "Vendor";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(25, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(743, 347);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pemilik Legalitas (Log)";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.DteDLDocDt1);
            this.panel4.Controls.Add(this.LueDLCode1);
            this.panel4.Controls.Add(this.Grd1);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(737, 341);
            this.panel4.TabIndex = 18;
            // 
            // DteDLDocDt1
            // 
            this.DteDLDocDt1.EditValue = null;
            this.DteDLDocDt1.EnterMoveNextControl = true;
            this.DteDLDocDt1.Location = new System.Drawing.Point(404, 189);
            this.DteDLDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDLDocDt1.Name = "DteDLDocDt1";
            this.DteDLDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDLDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDLDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDLDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDLDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDLDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDLDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDLDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDLDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDLDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDLDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDLDocDt1.Size = new System.Drawing.Size(121, 20);
            this.DteDLDocDt1.TabIndex = 37;
            this.DteDLDocDt1.Visible = false;
            this.DteDLDocDt1.Leave += new System.EventHandler(this.DteDLDocDt1_Leave);
            this.DteDLDocDt1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDLDocDt1_KeyDown);
            // 
            // LueDLCode1
            // 
            this.LueDLCode1.EnterMoveNextControl = true;
            this.LueDLCode1.Location = new System.Drawing.Point(123, 189);
            this.LueDLCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueDLCode1.Name = "LueDLCode1";
            this.LueDLCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode1.Properties.Appearance.Options.UseFont = true;
            this.LueDLCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDLCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDLCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDLCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDLCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDLCode1.Properties.DropDownRows = 12;
            this.LueDLCode1.Properties.NullText = "[Empty]";
            this.LueDLCode1.Properties.PopupWidth = 500;
            this.LueDLCode1.Size = new System.Drawing.Size(260, 20);
            this.LueDLCode1.TabIndex = 36;
            this.LueDLCode1.ToolTip = "F4 : Show/hide list";
            this.LueDLCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDLCode1.EditValueChanged += new System.EventHandler(this.LueDLCode1_EditValueChanged);
            this.LueDLCode1.Leave += new System.EventHandler(this.LueDLCode1_Leave);
            this.LueDLCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDLCode1_KeyDown);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 167);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(737, 174);
            this.Grd1.TabIndex = 35;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel8.Controls.Add(this.TxtVdName1);
            this.panel8.Controls.Add(this.BtnVdCode1);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Controls.Add(this.MeeAddress1);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.TxtProvCode1);
            this.panel8.Controls.Add(this.label20);
            this.panel8.Controls.Add(this.TxtCityCode1);
            this.panel8.Controls.Add(this.label21);
            this.panel8.Controls.Add(this.TxtSDCode1);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this.TxtVilCode1);
            this.panel8.Controls.Add(this.label23);
            this.panel8.Controls.Add(this.TxtIdentityNo1);
            this.panel8.Controls.Add(this.label24);
            this.panel8.Controls.Add(this.label25);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(737, 167);
            this.panel8.TabIndex = 20;
            // 
            // TxtVdName1
            // 
            this.TxtVdName1.EnterMoveNextControl = true;
            this.TxtVdName1.Location = new System.Drawing.Point(105, 8);
            this.TxtVdName1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdName1.Name = "TxtVdName1";
            this.TxtVdName1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdName1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdName1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdName1.Properties.Appearance.Options.UseFont = true;
            this.TxtVdName1.Properties.MaxLength = 200;
            this.TxtVdName1.Size = new System.Drawing.Size(553, 20);
            this.TxtVdName1.TabIndex = 37;
            // 
            // BtnVdCode1
            // 
            this.BtnVdCode1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVdCode1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVdCode1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVdCode1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVdCode1.Appearance.Options.UseBackColor = true;
            this.BtnVdCode1.Appearance.Options.UseFont = true;
            this.BtnVdCode1.Appearance.Options.UseForeColor = true;
            this.BtnVdCode1.Appearance.Options.UseTextOptions = true;
            this.BtnVdCode1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVdCode1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVdCode1.Image = ((System.Drawing.Image)(resources.GetObject("BtnVdCode1.Image")));
            this.BtnVdCode1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVdCode1.Location = new System.Drawing.Point(659, 8);
            this.BtnVdCode1.Name = "BtnVdCode1";
            this.BtnVdCode1.Size = new System.Drawing.Size(24, 21);
            this.BtnVdCode1.TabIndex = 36;
            this.BtnVdCode1.ToolTip = "Find Sales Order\'s Document Number";
            this.BtnVdCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVdCode1.ToolTipTitle = "Run System";
            this.BtnVdCode1.Click += new System.EventHandler(this.BtnVdCode1_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(688, 11);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 14);
            this.label27.TabIndex = 23;
            this.label27.Text = "(Log)";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress1
            // 
            this.MeeAddress1.EnterMoveNextControl = true;
            this.MeeAddress1.Location = new System.Drawing.Point(105, 52);
            this.MeeAddress1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress1.Name = "MeeAddress1";
            this.MeeAddress1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress1.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress1.Properties.MaxLength = 400;
            this.MeeAddress1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress1.Properties.ShowIcon = false;
            this.MeeAddress1.Size = new System.Drawing.Size(620, 20);
            this.MeeAddress1.TabIndex = 26;
            this.MeeAddress1.ToolTip = "F4 : Show/hide text";
            this.MeeAddress1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress1.ToolTipTitle = "Run System";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(55, 55);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 14);
            this.label19.TabIndex = 25;
            this.label19.Text = "Alamat";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProvCode1
            // 
            this.TxtProvCode1.EnterMoveNextControl = true;
            this.TxtProvCode1.Location = new System.Drawing.Point(105, 140);
            this.TxtProvCode1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProvCode1.Name = "TxtProvCode1";
            this.TxtProvCode1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProvCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProvCode1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProvCode1.Properties.Appearance.Options.UseFont = true;
            this.TxtProvCode1.Properties.MaxLength = 40;
            this.TxtProvCode1.Size = new System.Drawing.Size(620, 20);
            this.TxtProvCode1.TabIndex = 34;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(52, 143);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 14);
            this.label20.TabIndex = 33;
            this.label20.Text = "Provinsi";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCityCode1
            // 
            this.TxtCityCode1.EnterMoveNextControl = true;
            this.TxtCityCode1.Location = new System.Drawing.Point(105, 118);
            this.TxtCityCode1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCityCode1.Name = "TxtCityCode1";
            this.TxtCityCode1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCityCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCityCode1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCityCode1.Properties.Appearance.Options.UseFont = true;
            this.TxtCityCode1.Properties.MaxLength = 40;
            this.TxtCityCode1.Size = new System.Drawing.Size(620, 20);
            this.TxtCityCode1.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(33, 121);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(66, 14);
            this.label21.TabIndex = 31;
            this.label21.Text = "Kabupaten";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSDCode1
            // 
            this.TxtSDCode1.EnterMoveNextControl = true;
            this.TxtSDCode1.Location = new System.Drawing.Point(105, 96);
            this.TxtSDCode1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSDCode1.Name = "TxtSDCode1";
            this.TxtSDCode1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSDCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSDCode1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSDCode1.Properties.Appearance.Options.UseFont = true;
            this.TxtSDCode1.Properties.MaxLength = 40;
            this.TxtSDCode1.Size = new System.Drawing.Size(620, 20);
            this.TxtSDCode1.TabIndex = 30;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(32, 99);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 14);
            this.label22.TabIndex = 29;
            this.label22.Text = "Kecamatan";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVilCode1
            // 
            this.TxtVilCode1.EnterMoveNextControl = true;
            this.TxtVilCode1.Location = new System.Drawing.Point(105, 74);
            this.TxtVilCode1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVilCode1.Name = "TxtVilCode1";
            this.TxtVilCode1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVilCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVilCode1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVilCode1.Properties.Appearance.Options.UseFont = true;
            this.TxtVilCode1.Properties.MaxLength = 40;
            this.TxtVilCode1.Size = new System.Drawing.Size(620, 20);
            this.TxtVilCode1.TabIndex = 28;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(39, 77);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 14);
            this.label23.TabIndex = 27;
            this.label23.Text = "Kelurahan";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdentityNo1
            // 
            this.TxtIdentityNo1.EnterMoveNextControl = true;
            this.TxtIdentityNo1.Location = new System.Drawing.Point(105, 30);
            this.TxtIdentityNo1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentityNo1.Name = "TxtIdentityNo1";
            this.TxtIdentityNo1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdentityNo1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentityNo1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdentityNo1.Properties.Appearance.Options.UseFont = true;
            this.TxtIdentityNo1.Properties.MaxLength = 20;
            this.TxtIdentityNo1.Size = new System.Drawing.Size(274, 20);
            this.TxtIdentityNo1.TabIndex = 24;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(21, 33);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 14);
            this.label24.TabIndex = 23;
            this.label24.Text = "No. Identitas";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(6, 11);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 14);
            this.label25.TabIndex = 21;
            this.label25.Text = "Pemilik Legalitas";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Location = new System.Drawing.Point(67, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(701, 107);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Pemilik Legalitas (Balok)";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.DteDLDocDt2);
            this.panel5.Controls.Add(this.LueDLCode2);
            this.panel5.Controls.Add(this.Grd2);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(701, 107);
            this.panel5.TabIndex = 18;
            // 
            // DteDLDocDt2
            // 
            this.DteDLDocDt2.EditValue = null;
            this.DteDLDocDt2.EnterMoveNextControl = true;
            this.DteDLDocDt2.Location = new System.Drawing.Point(373, 189);
            this.DteDLDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDLDocDt2.Name = "DteDLDocDt2";
            this.DteDLDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDLDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDLDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDLDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDLDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDLDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDLDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDLDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDLDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDLDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDLDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDLDocDt2.Size = new System.Drawing.Size(121, 20);
            this.DteDLDocDt2.TabIndex = 38;
            this.DteDLDocDt2.Visible = false;
            this.DteDLDocDt2.Leave += new System.EventHandler(this.DteDLDocDt2_Leave);
            this.DteDLDocDt2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDLDocDt2_KeyDown);
            // 
            // LueDLCode2
            // 
            this.LueDLCode2.EnterMoveNextControl = true;
            this.LueDLCode2.Location = new System.Drawing.Point(92, 189);
            this.LueDLCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueDLCode2.Name = "LueDLCode2";
            this.LueDLCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode2.Properties.Appearance.Options.UseFont = true;
            this.LueDLCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDLCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDLCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDLCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDLCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDLCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDLCode2.Properties.DropDownRows = 12;
            this.LueDLCode2.Properties.NullText = "[Empty]";
            this.LueDLCode2.Properties.PopupWidth = 500;
            this.LueDLCode2.Size = new System.Drawing.Size(260, 20);
            this.LueDLCode2.TabIndex = 37;
            this.LueDLCode2.ToolTip = "F4 : Show/hide list";
            this.LueDLCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDLCode2.EditValueChanged += new System.EventHandler(this.LueDLCode2_EditValueChanged);
            this.LueDLCode2.Leave += new System.EventHandler(this.LueDLCode2_Leave);
            this.LueDLCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueDLCode2_KeyDown);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 20;
            this.Grd2.Location = new System.Drawing.Point(0, 167);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(701, 0);
            this.Grd2.TabIndex = 36;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel7.Controls.Add(this.TxtVdName2);
            this.panel7.Controls.Add(this.BtnVdCode2);
            this.panel7.Controls.Add(this.label26);
            this.panel7.Controls.Add(this.MeeAddress2);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.TxtProvCode2);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.TxtCityCode2);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.TxtSDCode2);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.TxtVilCode2);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.TxtIdentityNo2);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(701, 167);
            this.panel7.TabIndex = 20;
            // 
            // TxtVdName2
            // 
            this.TxtVdName2.EnterMoveNextControl = true;
            this.TxtVdName2.Location = new System.Drawing.Point(104, 8);
            this.TxtVdName2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdName2.Name = "TxtVdName2";
            this.TxtVdName2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVdName2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdName2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdName2.Properties.Appearance.Options.UseFont = true;
            this.TxtVdName2.Properties.MaxLength = 200;
            this.TxtVdName2.Size = new System.Drawing.Size(556, 20);
            this.TxtVdName2.TabIndex = 38;
            // 
            // BtnVdCode2
            // 
            this.BtnVdCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVdCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVdCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVdCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVdCode2.Appearance.Options.UseBackColor = true;
            this.BtnVdCode2.Appearance.Options.UseFont = true;
            this.BtnVdCode2.Appearance.Options.UseForeColor = true;
            this.BtnVdCode2.Appearance.Options.UseTextOptions = true;
            this.BtnVdCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVdCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVdCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnVdCode2.Image")));
            this.BtnVdCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVdCode2.Location = new System.Drawing.Point(662, 8);
            this.BtnVdCode2.Name = "BtnVdCode2";
            this.BtnVdCode2.Size = new System.Drawing.Size(24, 21);
            this.BtnVdCode2.TabIndex = 36;
            this.BtnVdCode2.ToolTip = "Find Sales Order\'s Document Number";
            this.BtnVdCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVdCode2.ToolTipTitle = "Run System";
            this.BtnVdCode2.Click += new System.EventHandler(this.BtnVdCode2_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(689, 12);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 14);
            this.label26.TabIndex = 23;
            this.label26.Text = "(Balok)";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress2
            // 
            this.MeeAddress2.EnterMoveNextControl = true;
            this.MeeAddress2.Location = new System.Drawing.Point(105, 52);
            this.MeeAddress2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress2.Name = "MeeAddress2";
            this.MeeAddress2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress2.Properties.MaxLength = 400;
            this.MeeAddress2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress2.Properties.ShowIcon = false;
            this.MeeAddress2.Size = new System.Drawing.Size(629, 20);
            this.MeeAddress2.TabIndex = 27;
            this.MeeAddress2.ToolTip = "F4 : Show/hide text";
            this.MeeAddress2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress2.ToolTipTitle = "Run System";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(55, 55);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 14);
            this.label10.TabIndex = 26;
            this.label10.Text = "Alamat";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProvCode2
            // 
            this.TxtProvCode2.EnterMoveNextControl = true;
            this.TxtProvCode2.Location = new System.Drawing.Point(105, 140);
            this.TxtProvCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProvCode2.Name = "TxtProvCode2";
            this.TxtProvCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtProvCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProvCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProvCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtProvCode2.Properties.MaxLength = 40;
            this.TxtProvCode2.Size = new System.Drawing.Size(629, 20);
            this.TxtProvCode2.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(52, 143);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 14);
            this.label11.TabIndex = 34;
            this.label11.Text = "Provinsi";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCityCode2
            // 
            this.TxtCityCode2.EnterMoveNextControl = true;
            this.TxtCityCode2.Location = new System.Drawing.Point(105, 118);
            this.TxtCityCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCityCode2.Name = "TxtCityCode2";
            this.TxtCityCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCityCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCityCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCityCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtCityCode2.Properties.MaxLength = 40;
            this.TxtCityCode2.Size = new System.Drawing.Size(629, 20);
            this.TxtCityCode2.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(33, 121);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 14);
            this.label12.TabIndex = 32;
            this.label12.Text = "Kabupaten";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSDCode2
            // 
            this.TxtSDCode2.EnterMoveNextControl = true;
            this.TxtSDCode2.Location = new System.Drawing.Point(105, 96);
            this.TxtSDCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSDCode2.Name = "TxtSDCode2";
            this.TxtSDCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSDCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSDCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSDCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtSDCode2.Properties.MaxLength = 40;
            this.TxtSDCode2.Size = new System.Drawing.Size(629, 20);
            this.TxtSDCode2.TabIndex = 31;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(32, 99);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 14);
            this.label13.TabIndex = 30;
            this.label13.Text = "Kecamatan";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVilCode2
            // 
            this.TxtVilCode2.EnterMoveNextControl = true;
            this.TxtVilCode2.Location = new System.Drawing.Point(105, 74);
            this.TxtVilCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVilCode2.Name = "TxtVilCode2";
            this.TxtVilCode2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVilCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVilCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVilCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtVilCode2.Properties.MaxLength = 40;
            this.TxtVilCode2.Size = new System.Drawing.Size(629, 20);
            this.TxtVilCode2.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(39, 77);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 14);
            this.label14.TabIndex = 28;
            this.label14.Text = "Kelurahan";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdentityNo2
            // 
            this.TxtIdentityNo2.EnterMoveNextControl = true;
            this.TxtIdentityNo2.Location = new System.Drawing.Point(105, 30);
            this.TxtIdentityNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentityNo2.Name = "TxtIdentityNo2";
            this.TxtIdentityNo2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdentityNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentityNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdentityNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtIdentityNo2.Properties.MaxLength = 20;
            this.TxtIdentityNo2.Size = new System.Drawing.Size(274, 20);
            this.TxtIdentityNo2.TabIndex = 25;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(21, 33);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 14);
            this.label15.TabIndex = 24;
            this.label15.Text = "No. Identitas";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(6, 11);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 14);
            this.label16.TabIndex = 21;
            this.label16.Text = "Pemilik Legalitas";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmLegalDocVerify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmLegalDocVerify";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVehicleRegNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProvCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSDCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVilCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDLCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProvCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSDCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVilCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo1.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDLDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDLCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProvCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCityCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSDCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVilCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabPage tabPage2;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabPage tabPage3;
        protected System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtQueueNo;
        public DevExpress.XtraEditors.SimpleButton BtnQueueNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtVehicleRegNo;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtProvCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtCityCode;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtSDCode;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtVilCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtIdentityNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtProvCode2;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtCityCode2;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtSDCode2;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtVilCode2;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtIdentityNo2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtProvCode1;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtCityCode1;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtSDCode1;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtVilCode1;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtIdentityNo1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.DateEdit DteDLDocDt2;
        private DevExpress.XtraEditors.LookUpEdit LueDLCode2;
        internal DevExpress.XtraEditors.DateEdit DteDLDocDt1;
        private DevExpress.XtraEditors.LookUpEdit LueDLCode1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        public DevExpress.XtraEditors.SimpleButton BtnVdCode;
        public DevExpress.XtraEditors.SimpleButton BtnVdCode1;
        public DevExpress.XtraEditors.SimpleButton BtnVdCode2;
        internal DevExpress.XtraEditors.TextEdit TxtVdName;
        internal DevExpress.XtraEditors.TextEdit TxtVdName1;
        internal DevExpress.XtraEditors.TextEdit TxtVdName2;
        internal DevExpress.XtraEditors.MemoExEdit MeeAddress;
        internal DevExpress.XtraEditors.MemoExEdit MeeAddress2;
        internal DevExpress.XtraEditors.MemoExEdit MeeAddress1;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtTTCode;
    }
}