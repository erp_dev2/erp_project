﻿namespace RunSystem
{
    partial class FrmSOContract2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSOContract2));
            this.panel4 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnNTPDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtNTPDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtPONo = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtCustomerContactperson = new DevExpress.XtraEditors.TextEdit();
            this.MeeProjectDesc = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtSAName = new DevExpress.XtraEditors.TextEdit();
            this.BtnCustomerShipAddress = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.LueJOType = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnBOQDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnContact = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnCtShippingAddress = new DevExpress.XtraEditors.SimpleButton();
            this.TxtBOQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.LueTaxCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtAmtBOM = new DevExpress.XtraEditors.TextEdit();
            this.LueShpMCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.LueSPCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtAddress = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtCountry = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.TxtCity = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TpCOA = new DevExpress.XtraTab.XtraTabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtCOAAmt = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtVoucherRequestDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpAdditional = new DevExpress.XtraTab.XtraTabPage();
            this.TcOutgoingPayment = new DevExpress.XtraTab.XtraTabControl();
            this.TpBOQ = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpBOQ2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpItem = new DevExpress.XtraTab.XtraTabPage();
            this.DteDeliveryDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.BtnCopyDeliveryDt = new System.Windows.Forms.Button();
            this.BtnCopyBOQ = new System.Windows.Forms.Button();
            this.TpJO = new DevExpress.XtraTab.XtraTabPage();
            this.DteJODocDt = new DevExpress.XtraEditors.DateEdit();
            this.LueJOCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.TpBankGuarantee = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtBankGuaranteeAmt = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtBankGuaranteeNo = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.DteBankGuaranteeDt = new DevExpress.XtraEditors.DateEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpAttachment = new DevExpress.XtraTab.XtraTabPage();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNTPDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomerContactperson.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBOM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcOutgoingPayment)).BeginInit();
            this.TcOutgoingPayment.SuspendLayout();
            this.TpBOQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpBOQ2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel9.SuspendLayout();
            this.TpJO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.TpBankGuarantee.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankGuaranteeAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankGuaranteeNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            this.TpAttachment.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(926, 0);
            this.panel1.Size = new System.Drawing.Size(70, 701);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcOutgoingPayment);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Size = new System.Drawing.Size(926, 574);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 574);
            this.panel3.Size = new System.Drawing.Size(926, 127);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 679);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(926, 127);
            this.Grd1.TabIndex = 111;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.TxtProjectCode);
            this.panel4.Controls.Add(this.BtnNTPDocNo);
            this.panel4.Controls.Add(this.TxtNTPDocNo);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.TxtPONo);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.TxtProjectName);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.TxtCustomerContactperson);
            this.panel4.Controls.Add(this.MeeProjectDesc);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.TxtSAName);
            this.panel4.Controls.Add(this.BtnCustomerShipAddress);
            this.panel4.Controls.Add(this.TxtLocalDocNo);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.LueStatus);
            this.panel4.Controls.Add(this.LueJOType);
            this.panel4.Controls.Add(this.BtnBOQDocNo);
            this.panel4.Controls.Add(this.BtnContact);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.BtnCtShippingAddress);
            this.panel4.Controls.Add(this.TxtBOQDocNo);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.LueCtCode);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.DteDocDt);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.TxtDocNo);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(926, 326);
            this.panel4.TabIndex = 11;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(41, 238);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(81, 14);
            this.label40.TabIndex = 38;
            this.label40.Text = "Project Name";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(129, 214);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 30;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(305, 20);
            this.TxtProjectCode.TabIndex = 37;
            // 
            // BtnNTPDocNo
            // 
            this.BtnNTPDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnNTPDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnNTPDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNTPDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnNTPDocNo.Appearance.Options.UseBackColor = true;
            this.BtnNTPDocNo.Appearance.Options.UseFont = true;
            this.BtnNTPDocNo.Appearance.Options.UseForeColor = true;
            this.BtnNTPDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnNTPDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnNTPDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnNTPDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnNTPDocNo.Image")));
            this.BtnNTPDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnNTPDocNo.Location = new System.Drawing.Point(420, 171);
            this.BtnNTPDocNo.Name = "BtnNTPDocNo";
            this.BtnNTPDocNo.Size = new System.Drawing.Size(15, 21);
            this.BtnNTPDocNo.TabIndex = 32;
            this.BtnNTPDocNo.ToolTip = "Auto get NTP#";
            this.BtnNTPDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnNTPDocNo.ToolTipTitle = "Run System";
            this.BtnNTPDocNo.Click += new System.EventHandler(this.BtnNTPDocNo_Click);
            // 
            // TxtNTPDocNo
            // 
            this.TxtNTPDocNo.EnterMoveNextControl = true;
            this.TxtNTPDocNo.Location = new System.Drawing.Point(129, 172);
            this.TxtNTPDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNTPDocNo.Name = "TxtNTPDocNo";
            this.TxtNTPDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNTPDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNTPDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNTPDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtNTPDocNo.Properties.MaxLength = 80;
            this.TxtNTPDocNo.Properties.ReadOnly = true;
            this.TxtNTPDocNo.Size = new System.Drawing.Size(289, 20);
            this.TxtNTPDocNo.TabIndex = 31;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(6, 175);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(116, 14);
            this.label32.TabIndex = 30;
            this.label32.Text = "Notice to Proceed#";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPONo
            // 
            this.TxtPONo.EnterMoveNextControl = true;
            this.TxtPONo.Location = new System.Drawing.Point(129, 130);
            this.TxtPONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPONo.Name = "TxtPONo";
            this.TxtPONo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPONo.Properties.Appearance.Options.UseFont = true;
            this.TxtPONo.Properties.MaxLength = 80;
            this.TxtPONo.Size = new System.Drawing.Size(305, 20);
            this.TxtPONo.TabIndex = 26;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(26, 133);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(96, 14);
            this.label31.TabIndex = 25;
            this.label31.Text = "Customer\'s PO#";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(129, 235);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 250;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(305, 20);
            this.TxtProjectName.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(44, 217);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 14);
            this.label16.TabIndex = 36;
            this.label16.Text = "Project Code";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCustomerContactperson
            // 
            this.TxtCustomerContactperson.EnterMoveNextControl = true;
            this.TxtCustomerContactperson.Location = new System.Drawing.Point(129, 193);
            this.TxtCustomerContactperson.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCustomerContactperson.Name = "TxtCustomerContactperson";
            this.TxtCustomerContactperson.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCustomerContactperson.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCustomerContactperson.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCustomerContactperson.Properties.Appearance.Options.UseFont = true;
            this.TxtCustomerContactperson.Properties.MaxLength = 100;
            this.TxtCustomerContactperson.Properties.ReadOnly = true;
            this.TxtCustomerContactperson.Size = new System.Drawing.Size(289, 20);
            this.TxtCustomerContactperson.TabIndex = 34;
            // 
            // MeeProjectDesc
            // 
            this.MeeProjectDesc.EnterMoveNextControl = true;
            this.MeeProjectDesc.Location = new System.Drawing.Point(129, 256);
            this.MeeProjectDesc.Margin = new System.Windows.Forms.Padding(5);
            this.MeeProjectDesc.Name = "MeeProjectDesc";
            this.MeeProjectDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.Appearance.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeProjectDesc.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeProjectDesc.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeProjectDesc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeProjectDesc.Properties.MaxLength = 5000;
            this.MeeProjectDesc.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeProjectDesc.Properties.ShowIcon = false;
            this.MeeProjectDesc.Size = new System.Drawing.Size(305, 20);
            this.MeeProjectDesc.TabIndex = 39;
            this.MeeProjectDesc.ToolTip = "F4 : Show/hide text";
            this.MeeProjectDesc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeProjectDesc.ToolTipTitle = "Run System";
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(374, 66);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 20;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(12, 259);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(110, 14);
            this.label23.TabIndex = 38;
            this.label23.Text = "Project Description";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(31, 301);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 14);
            this.label24.TabIndex = 44;
            this.label24.Text = "Joint Operation";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(37, 70);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 14);
            this.label27.TabIndex = 18;
            this.label27.Text = "Cancel Reason";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(129, 67);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 250;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(240, 20);
            this.MeeCancelReason.TabIndex = 19;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // TxtSAName
            // 
            this.TxtSAName.EnterMoveNextControl = true;
            this.TxtSAName.Location = new System.Drawing.Point(129, 277);
            this.TxtSAName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSAName.Name = "TxtSAName";
            this.TxtSAName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSAName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSAName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSAName.Properties.Appearance.Options.UseFont = true;
            this.TxtSAName.Properties.MaxLength = 250;
            this.TxtSAName.Properties.ReadOnly = true;
            this.TxtSAName.Size = new System.Drawing.Size(260, 20);
            this.TxtSAName.TabIndex = 41;
            // 
            // BtnCustomerShipAddress
            // 
            this.BtnCustomerShipAddress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCustomerShipAddress.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCustomerShipAddress.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCustomerShipAddress.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCustomerShipAddress.Appearance.Options.UseBackColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseFont = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseForeColor = true;
            this.BtnCustomerShipAddress.Appearance.Options.UseTextOptions = true;
            this.BtnCustomerShipAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCustomerShipAddress.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCustomerShipAddress.Image = ((System.Drawing.Image)(resources.GetObject("BtnCustomerShipAddress.Image")));
            this.BtnCustomerShipAddress.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCustomerShipAddress.Location = new System.Drawing.Point(397, 280);
            this.BtnCustomerShipAddress.Name = "BtnCustomerShipAddress";
            this.BtnCustomerShipAddress.Size = new System.Drawing.Size(15, 14);
            this.BtnCustomerShipAddress.TabIndex = 42;
            this.BtnCustomerShipAddress.ToolTip = "Find Customer\'s Shipping Information";
            this.BtnCustomerShipAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCustomerShipAddress.ToolTipTitle = "Run System";
            this.BtnCustomerShipAddress.Click += new System.EventHandler(this.BtnCustomerShipAddress_Click_1);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(129, 88);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 80;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(305, 20);
            this.TxtLocalDocNo.TabIndex = 22;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(27, 91);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 14);
            this.label20.TabIndex = 21;
            this.label20.Text = "Local Document";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(129, 46);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 20;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 500;
            this.LueStatus.Size = new System.Drawing.Size(240, 20);
            this.LueStatus.TabIndex = 17;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged_1);
            // 
            // LueJOType
            // 
            this.LueJOType.EnterMoveNextControl = true;
            this.LueJOType.Location = new System.Drawing.Point(129, 298);
            this.LueJOType.Margin = new System.Windows.Forms.Padding(5);
            this.LueJOType.Name = "LueJOType";
            this.LueJOType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.Appearance.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueJOType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueJOType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueJOType.Properties.DropDownRows = 30;
            this.LueJOType.Properties.NullText = "[Empty]";
            this.LueJOType.Properties.PopupWidth = 400;
            this.LueJOType.Size = new System.Drawing.Size(305, 20);
            this.LueJOType.TabIndex = 45;
            this.LueJOType.ToolTip = "F4 : Show/hide list";
            this.LueJOType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueJOType.EditValueChanged += new System.EventHandler(this.LueJOType_EditValueChanged);
            // 
            // BtnBOQDocNo
            // 
            this.BtnBOQDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBOQDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBOQDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOQDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBOQDocNo.Appearance.Options.UseBackColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseFont = true;
            this.BtnBOQDocNo.Appearance.Options.UseForeColor = true;
            this.BtnBOQDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnBOQDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBOQDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBOQDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnBOQDocNo.Image")));
            this.BtnBOQDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBOQDocNo.Location = new System.Drawing.Point(420, 150);
            this.BtnBOQDocNo.Name = "BtnBOQDocNo";
            this.BtnBOQDocNo.Size = new System.Drawing.Size(15, 21);
            this.BtnBOQDocNo.TabIndex = 29;
            this.BtnBOQDocNo.ToolTip = "Show BOQ";
            this.BtnBOQDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBOQDocNo.ToolTipTitle = "Run System";
            this.BtnBOQDocNo.Click += new System.EventHandler(this.BtnBOQDocNo_Click);
            // 
            // BtnContact
            // 
            this.BtnContact.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnContact.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnContact.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnContact.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnContact.Appearance.Options.UseBackColor = true;
            this.BtnContact.Appearance.Options.UseFont = true;
            this.BtnContact.Appearance.Options.UseForeColor = true;
            this.BtnContact.Appearance.Options.UseTextOptions = true;
            this.BtnContact.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnContact.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnContact.Image = ((System.Drawing.Image)(resources.GetObject("BtnContact.Image")));
            this.BtnContact.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnContact.Location = new System.Drawing.Point(420, 193);
            this.BtnContact.Name = "BtnContact";
            this.BtnContact.Size = new System.Drawing.Size(15, 21);
            this.BtnContact.TabIndex = 35;
            this.BtnContact.ToolTip = "Add Contact Person";
            this.BtnContact.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnContact.ToolTipTitle = "Run System";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(31, 196);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 14);
            this.label4.TabIndex = 33;
            this.label4.Text = "Contact Person";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCtShippingAddress
            // 
            this.BtnCtShippingAddress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCtShippingAddress.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCtShippingAddress.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCtShippingAddress.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCtShippingAddress.Appearance.Options.UseBackColor = true;
            this.BtnCtShippingAddress.Appearance.Options.UseFont = true;
            this.BtnCtShippingAddress.Appearance.Options.UseForeColor = true;
            this.BtnCtShippingAddress.Appearance.Options.UseTextOptions = true;
            this.BtnCtShippingAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCtShippingAddress.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCtShippingAddress.Image = ((System.Drawing.Image)(resources.GetObject("BtnCtShippingAddress.Image")));
            this.BtnCtShippingAddress.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCtShippingAddress.Location = new System.Drawing.Point(420, 280);
            this.BtnCtShippingAddress.Name = "BtnCtShippingAddress";
            this.BtnCtShippingAddress.Size = new System.Drawing.Size(15, 14);
            this.BtnCtShippingAddress.TabIndex = 43;
            this.BtnCtShippingAddress.ToolTip = "Add Customer Shipping Address";
            this.BtnCtShippingAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCtShippingAddress.ToolTipTitle = "Run System";
            this.BtnCtShippingAddress.Click += new System.EventHandler(this.BtnCtShippingAddress_Click_1);
            // 
            // TxtBOQDocNo
            // 
            this.TxtBOQDocNo.EnterMoveNextControl = true;
            this.TxtBOQDocNo.Location = new System.Drawing.Point(129, 151);
            this.TxtBOQDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBOQDocNo.Name = "TxtBOQDocNo";
            this.TxtBOQDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBOQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBOQDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBOQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBOQDocNo.Properties.MaxLength = 30;
            this.TxtBOQDocNo.Properties.ReadOnly = true;
            this.TxtBOQDocNo.Size = new System.Drawing.Size(289, 20);
            this.TxtBOQDocNo.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(36, 154);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 14);
            this.label7.TabIndex = 27;
            this.label7.Text = "Bill of Quantity";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(34, 280);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 14);
            this.label11.TabIndex = 40;
            this.label11.Text = "Shipping Name";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(63, 112);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Customer";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(129, 109);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 400;
            this.LueCtCode.Size = new System.Drawing.Size(305, 20);
            this.LueCtCode.TabIndex = 24;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(29, 49);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 14);
            this.label12.TabIndex = 16;
            this.label12.Text = "Approval Status";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(129, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(148, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(89, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(129, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(305, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(49, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label39);
            this.panel6.Controls.Add(this.LueTaxCode);
            this.panel6.Controls.Add(this.label38);
            this.panel6.Controls.Add(this.LuePtCode);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Controls.Add(this.label33);
            this.panel6.Controls.Add(this.TxtAmtBOM);
            this.panel6.Controls.Add(this.LueShpMCode);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.LueSPCode);
            this.panel6.Controls.Add(this.TxtMobile);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtEmail);
            this.panel6.Controls.Add(this.TxtFax);
            this.panel6.Controls.Add(this.TxtAmt);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.TxtPhone);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.TxtAddress);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.TxtCountry);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.TxtPostalCd);
            this.panel6.Controls.Add(this.TxtCity);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(447, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(479, 326);
            this.panel6.TabIndex = 53;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(89, 50);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(27, 14);
            this.label39.TabIndex = 76;
            this.label39.Text = "City";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode
            // 
            this.LueTaxCode.EnterMoveNextControl = true;
            this.LueTaxCode.Location = new System.Drawing.Point(124, 279);
            this.LueTaxCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode.Name = "LueTaxCode";
            this.LueTaxCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode.Properties.DropDownRows = 30;
            this.LueTaxCode.Properties.NullText = "[Empty]";
            this.LueTaxCode.Properties.PopupWidth = 400;
            this.LueTaxCode.Size = new System.Drawing.Size(343, 20);
            this.LueTaxCode.TabIndex = 75;
            this.LueTaxCode.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode.EditValueChanged += new System.EventHandler(this.LueTaxCode_EditValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(89, 281);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 14);
            this.label38.TabIndex = 74;
            this.label38.Text = "Tax";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(124, 257);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 30;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 400;
            this.LuePtCode.Size = new System.Drawing.Size(343, 20);
            this.LuePtCode.TabIndex = 73;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(11, 260);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(105, 14);
            this.label37.TabIndex = 72;
            this.label37.Text = "Term Of Payment";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(8, 239);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(108, 14);
            this.label33.TabIndex = 68;
            this.label33.Text = "Inventory Amount";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmtBOM
            // 
            this.TxtAmtBOM.EnterMoveNextControl = true;
            this.TxtAmtBOM.Location = new System.Drawing.Point(124, 236);
            this.TxtAmtBOM.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmtBOM.Name = "TxtAmtBOM";
            this.TxtAmtBOM.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmtBOM.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmtBOM.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmtBOM.Properties.Appearance.Options.UseFont = true;
            this.TxtAmtBOM.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmtBOM.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmtBOM.Properties.ReadOnly = true;
            this.TxtAmtBOM.Size = new System.Drawing.Size(343, 20);
            this.TxtAmtBOM.TabIndex = 69;
            this.TxtAmtBOM.Validated += new System.EventHandler(this.TxtAmtBOM_Validated);
            // 
            // LueShpMCode
            // 
            this.LueShpMCode.EnterMoveNextControl = true;
            this.LueShpMCode.Location = new System.Drawing.Point(124, 194);
            this.LueShpMCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueShpMCode.Name = "LueShpMCode";
            this.LueShpMCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.Appearance.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShpMCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShpMCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShpMCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShpMCode.Properties.DropDownRows = 30;
            this.LueShpMCode.Properties.NullText = "[Empty]";
            this.LueShpMCode.Properties.PopupWidth = 400;
            this.LueShpMCode.Size = new System.Drawing.Size(343, 20);
            this.LueShpMCode.TabIndex = 65;
            this.LueShpMCode.ToolTip = "F4 : Show/hide list";
            this.LueShpMCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShpMCode.EditValueChanged += new System.EventHandler(this.LueShpMCode_EditValueChanged_1);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(17, 197);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 14);
            this.label22.TabIndex = 64;
            this.label22.Text = "Shipping Method";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(41, 176);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 14);
            this.label21.TabIndex = 62;
            this.label21.Text = "Sales Person";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSPCode
            // 
            this.LueSPCode.EnterMoveNextControl = true;
            this.LueSPCode.Location = new System.Drawing.Point(124, 173);
            this.LueSPCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSPCode.Name = "LueSPCode";
            this.LueSPCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.Appearance.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSPCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSPCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSPCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSPCode.Properties.DropDownRows = 30;
            this.LueSPCode.Properties.NullText = "[Empty]";
            this.LueSPCode.Properties.PopupWidth = 400;
            this.LueSPCode.Size = new System.Drawing.Size(343, 20);
            this.LueSPCode.TabIndex = 63;
            this.LueSPCode.ToolTip = "F4 : Show/hide list";
            this.LueSPCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSPCode.EditValueChanged += new System.EventHandler(this.LueSPCode_EditValueChanged_1);
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(124, 152);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 250;
            this.TxtMobile.Properties.ReadOnly = true;
            this.TxtMobile.Size = new System.Drawing.Size(343, 20);
            this.TxtMobile.TabIndex = 61;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(75, 155);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 14);
            this.label17.TabIndex = 60;
            this.label17.Text = "Mobile";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(82, 134);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 14);
            this.label5.TabIndex = 58;
            this.label5.Text = "Email";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(91, 113);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 14);
            this.label6.TabIndex = 56;
            this.label6.Text = "Fax";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(124, 131);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 250;
            this.TxtEmail.Properties.ReadOnly = true;
            this.TxtEmail.Size = new System.Drawing.Size(343, 20);
            this.TxtEmail.TabIndex = 59;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(124, 110);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 250;
            this.TxtFax.Properties.ReadOnly = true;
            this.TxtFax.Size = new System.Drawing.Size(343, 20);
            this.TxtFax.TabIndex = 57;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(124, 215);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Properties.ReadOnly = true;
            this.TxtAmt.Size = new System.Drawing.Size(343, 20);
            this.TxtAmt.TabIndex = 67;
            this.TxtAmt.Validated += new System.EventHandler(this.TxtAmt_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(74, 92);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 54;
            this.label14.Text = "Phone";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(22, 218);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 14);
            this.label9.TabIndex = 66;
            this.label9.Text = "Service Amount";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(124, 300);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 16000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(343, 20);
            this.MeeRemark.TabIndex = 71;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(124, 89);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 250;
            this.TxtPhone.Properties.ReadOnly = true;
            this.TxtPhone.Size = new System.Drawing.Size(343, 20);
            this.TxtPhone.TabIndex = 55;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(69, 302);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 70;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAddress
            // 
            this.TxtAddress.EnterMoveNextControl = true;
            this.TxtAddress.Location = new System.Drawing.Point(124, 5);
            this.TxtAddress.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAddress.Name = "TxtAddress";
            this.TxtAddress.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAddress.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAddress.Properties.Appearance.Options.UseFont = true;
            this.TxtAddress.Properties.MaxLength = 250;
            this.TxtAddress.Properties.ReadOnly = true;
            this.TxtAddress.Size = new System.Drawing.Size(343, 20);
            this.TxtAddress.TabIndex = 47;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(66, 29);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 14);
            this.label10.TabIndex = 48;
            this.label10.Text = "Country";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCountry
            // 
            this.TxtCountry.EnterMoveNextControl = true;
            this.TxtCountry.Location = new System.Drawing.Point(124, 26);
            this.TxtCountry.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCountry.Name = "TxtCountry";
            this.TxtCountry.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCountry.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCountry.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCountry.Properties.Appearance.Options.UseFont = true;
            this.TxtCountry.Properties.MaxLength = 250;
            this.TxtCountry.Properties.ReadOnly = true;
            this.TxtCountry.Size = new System.Drawing.Size(343, 20);
            this.TxtCountry.TabIndex = 49;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(66, 8);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 46;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(124, 68);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 250;
            this.TxtPostalCd.Properties.ReadOnly = true;
            this.TxtPostalCd.Size = new System.Drawing.Size(343, 20);
            this.TxtPostalCd.TabIndex = 52;
            // 
            // TxtCity
            // 
            this.TxtCity.EnterMoveNextControl = true;
            this.TxtCity.Location = new System.Drawing.Point(124, 47);
            this.TxtCity.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCity.Name = "TxtCity";
            this.TxtCity.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCity.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCity.Properties.Appearance.Options.UseFont = true;
            this.TxtCity.Properties.MaxLength = 250;
            this.TxtCity.Properties.ReadOnly = true;
            this.TxtCity.Size = new System.Drawing.Size(343, 20);
            this.TxtCity.TabIndex = 50;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(45, 71);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 14);
            this.label13.TabIndex = 51;
            this.label13.Text = "Postal Code";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(407, 105);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 109;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(444, 104);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 111;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(57, 127);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(407, 23);
            this.PbUpload3.TabIndex = 112;
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(424, 104);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 110;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(57, 105);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 250;
            this.TxtFile3.Size = new System.Drawing.Size(338, 20);
            this.TxtFile3.TabIndex = 108;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(13, 108);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 14);
            this.label36.TabIndex = 107;
            this.label36.Text = "File 3";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(407, 56);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 103;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(407, 9);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 97;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(444, 55);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 105;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(57, 79);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(407, 23);
            this.PbUpload2.TabIndex = 106;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(424, 55);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 104;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(444, 8);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 99;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(57, 57);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 250;
            this.TxtFile2.Size = new System.Drawing.Size(338, 20);
            this.TxtFile2.TabIndex = 102;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(13, 60);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 14);
            this.label35.TabIndex = 101;
            this.label35.Text = "File 2";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(57, 31);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(407, 23);
            this.PbUpload.TabIndex = 100;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(424, 8);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 98;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(57, 9);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 250;
            this.TxtFile.Size = new System.Drawing.Size(338, 20);
            this.TxtFile.TabIndex = 96;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(13, 11);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(35, 14);
            this.label34.TabIndex = 95;
            this.label34.Text = "File 1";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpCOA
            // 
            this.TpCOA.Appearance.Header.Options.UseTextOptions = true;
            this.TpCOA.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpCOA.Name = "TpCOA";
            this.TpCOA.Size = new System.Drawing.Size(766, 289);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 238);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(766, 51);
            this.panel7.TabIndex = 25;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(536, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(230, 51);
            this.panel8.TabIndex = 29;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(6, 8);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(51, 14);
            this.label28.TabIndex = 30;
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCOAAmt
            // 
            this.TxtCOAAmt.EnterMoveNextControl = true;
            this.TxtCOAAmt.Location = new System.Drawing.Point(63, 5);
            this.TxtCOAAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCOAAmt.Name = "TxtCOAAmt";
            this.TxtCOAAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCOAAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCOAAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtCOAAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCOAAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCOAAmt.Properties.ReadOnly = true;
            this.TxtCOAAmt.Size = new System.Drawing.Size(160, 20);
            this.TxtCOAAmt.TabIndex = 31;
            this.TxtCOAAmt.ToolTip = "Based On Purchase Invoice\'s Currency";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(8, 8);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 14);
            this.label18.TabIndex = 26;
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo2
            // 
            this.TxtVoucherDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherDocNo2.Location = new System.Drawing.Point(124, 26);
            this.TxtVoucherDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo2.Name = "TxtVoucherDocNo2";
            this.TxtVoucherDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherDocNo2.TabIndex = 49;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(57, 29);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 28;
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherRequestDocNo2
            // 
            this.TxtVoucherRequestDocNo2.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo2.Location = new System.Drawing.Point(124, 5);
            this.TxtVoucherRequestDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo2.Name = "TxtVoucherRequestDocNo2";
            this.TxtVoucherRequestDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo2.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo2.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo2.Size = new System.Drawing.Size(212, 20);
            this.TxtVoucherRequestDocNo2.TabIndex = 27;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 19;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 238);
            this.Grd4.TabIndex = 24;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpAdditional
            // 
            this.TpAdditional.Appearance.Header.Options.UseTextOptions = true;
            this.TpAdditional.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpAdditional.Name = "TpAdditional";
            this.TpAdditional.Size = new System.Drawing.Size(766, 289);
            // 
            // TcOutgoingPayment
            // 
            this.TcOutgoingPayment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcOutgoingPayment.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcOutgoingPayment.Location = new System.Drawing.Point(0, 326);
            this.TcOutgoingPayment.Name = "TcOutgoingPayment";
            this.TcOutgoingPayment.SelectedTabPage = this.TpBOQ;
            this.TcOutgoingPayment.Size = new System.Drawing.Size(926, 248);
            this.TcOutgoingPayment.TabIndex = 94;
            this.TcOutgoingPayment.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpBOQ,
            this.TpBOQ2,
            this.TpItem,
            this.TpJO,
            this.TpBankGuarantee,
            this.TpAttachment});
            // 
            // TpBOQ
            // 
            this.TpBOQ.Appearance.Header.Options.UseTextOptions = true;
            this.TpBOQ.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBOQ.Controls.Add(this.Grd2);
            this.TpBOQ.Name = "TpBOQ";
            this.TpBOQ.Size = new System.Drawing.Size(920, 220);
            this.TpBOQ.Text = "BOQ Service";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(920, 220);
            this.Grd2.TabIndex = 95;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // TpBOQ2
            // 
            this.TpBOQ2.Appearance.Header.Options.UseTextOptions = true;
            this.TpBOQ2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBOQ2.Controls.Add(this.Grd6);
            this.TpBOQ2.Name = "TpBOQ2";
            this.TpBOQ2.Size = new System.Drawing.Size(766, 220);
            this.TpBOQ2.Text = "BOQ Inventory";
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(766, 220);
            this.Grd6.TabIndex = 95;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            this.Grd6.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd6_AfterCommitEdit);
            this.Grd6.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd6_EllipsisButtonClick);
            // 
            // TpItem
            // 
            this.TpItem.Appearance.Header.Options.UseTextOptions = true;
            this.TpItem.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpItem.Controls.Add(this.DteDeliveryDt);
            this.TpItem.Controls.Add(this.Grd3);
            this.TpItem.Controls.Add(this.panel9);
            this.TpItem.Name = "TpItem";
            this.TpItem.Size = new System.Drawing.Size(920, 220);
            this.TpItem.Text = "List of Item";
            // 
            // DteDeliveryDt
            // 
            this.DteDeliveryDt.EditValue = null;
            this.DteDeliveryDt.EnterMoveNextControl = true;
            this.DteDeliveryDt.Location = new System.Drawing.Point(262, 59);
            this.DteDeliveryDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDeliveryDt.Name = "DteDeliveryDt";
            this.DteDeliveryDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeliveryDt.Properties.Appearance.Options.UseFont = true;
            this.DteDeliveryDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDeliveryDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDeliveryDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDeliveryDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDeliveryDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeliveryDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDeliveryDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDeliveryDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDeliveryDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDeliveryDt.Size = new System.Drawing.Size(138, 20);
            this.DteDeliveryDt.TabIndex = 99;
            this.DteDeliveryDt.Visible = false;
            this.DteDeliveryDt.Leave += new System.EventHandler(this.DteDeliveryDt_Leave);
            this.DteDeliveryDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteDeliveryDt_KeyDown);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 37);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(920, 183);
            this.Grd3.TabIndex = 98;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            this.Grd3.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd3_ColHdrDoubleClick);
            this.Grd3.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd3_AfterCommitEdit);
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.BtnCopyDeliveryDt);
            this.panel9.Controls.Add(this.BtnCopyBOQ);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(920, 37);
            this.panel9.TabIndex = 95;
            // 
            // BtnCopyDeliveryDt
            // 
            this.BtnCopyDeliveryDt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.BtnCopyDeliveryDt.FlatAppearance.BorderSize = 0;
            this.BtnCopyDeliveryDt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCopyDeliveryDt.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyDeliveryDt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnCopyDeliveryDt.Location = new System.Drawing.Point(121, 4);
            this.BtnCopyDeliveryDt.Name = "BtnCopyDeliveryDt";
            this.BtnCopyDeliveryDt.Size = new System.Drawing.Size(141, 27);
            this.BtnCopyDeliveryDt.TabIndex = 97;
            this.BtnCopyDeliveryDt.Text = "Copy Delivery Date";
            this.BtnCopyDeliveryDt.UseVisualStyleBackColor = false;
            this.BtnCopyDeliveryDt.Click += new System.EventHandler(this.BtnCopyDeliveryDt_Click);
            // 
            // BtnCopyBOQ
            // 
            this.BtnCopyBOQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.BtnCopyBOQ.FlatAppearance.BorderSize = 0;
            this.BtnCopyBOQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCopyBOQ.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyBOQ.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnCopyBOQ.Location = new System.Drawing.Point(5, 4);
            this.BtnCopyBOQ.Name = "BtnCopyBOQ";
            this.BtnCopyBOQ.Size = new System.Drawing.Size(110, 27);
            this.BtnCopyBOQ.TabIndex = 96;
            this.BtnCopyBOQ.Text = "Copy From BOQ";
            this.BtnCopyBOQ.UseVisualStyleBackColor = false;
            this.BtnCopyBOQ.Click += new System.EventHandler(this.BtnCopyBOQ_Click);
            // 
            // TpJO
            // 
            this.TpJO.Appearance.Header.Options.UseTextOptions = true;
            this.TpJO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpJO.Controls.Add(this.DteJODocDt);
            this.TpJO.Controls.Add(this.LueJOCode);
            this.TpJO.Controls.Add(this.Grd5);
            this.TpJO.Name = "TpJO";
            this.TpJO.Size = new System.Drawing.Size(766, 220);
            this.TpJO.Text = "Joint Operation";
            // 
            // DteJODocDt
            // 
            this.DteJODocDt.EditValue = null;
            this.DteJODocDt.EnterMoveNextControl = true;
            this.DteJODocDt.Location = new System.Drawing.Point(352, 27);
            this.DteJODocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJODocDt.Name = "DteJODocDt";
            this.DteJODocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJODocDt.Properties.Appearance.Options.UseFont = true;
            this.DteJODocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJODocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJODocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJODocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJODocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJODocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJODocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJODocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJODocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJODocDt.Size = new System.Drawing.Size(125, 20);
            this.DteJODocDt.TabIndex = 96;
            this.DteJODocDt.Leave += new System.EventHandler(this.DteJODocDt_Leave);
            this.DteJODocDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteJODocDt_KeyDown);
            // 
            // LueJOCode
            // 
            this.LueJOCode.EnterMoveNextControl = true;
            this.LueJOCode.Location = new System.Drawing.Point(79, 26);
            this.LueJOCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueJOCode.Name = "LueJOCode";
            this.LueJOCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.Appearance.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueJOCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJOCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueJOCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueJOCode.Properties.DropDownRows = 30;
            this.LueJOCode.Properties.NullText = "[Empty]";
            this.LueJOCode.Properties.PopupWidth = 300;
            this.LueJOCode.Size = new System.Drawing.Size(264, 20);
            this.LueJOCode.TabIndex = 95;
            this.LueJOCode.ToolTip = "F4 : Show/hide list";
            this.LueJOCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueJOCode.EditValueChanged += new System.EventHandler(this.LueJOCode_EditValueChanged);
            this.LueJOCode.Leave += new System.EventHandler(this.LueJOCode_Leave);
            this.LueJOCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueJOCode_KeyDown);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 220);
            this.Grd5.TabIndex = 97;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // TpBankGuarantee
            // 
            this.TpBankGuarantee.Appearance.Header.Options.UseTextOptions = true;
            this.TpBankGuarantee.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpBankGuarantee.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpBankGuarantee.Appearance.PageClient.Options.UseBackColor = true;
            this.TpBankGuarantee.Controls.Add(this.panel5);
            this.TpBankGuarantee.Name = "TpBankGuarantee";
            this.TpBankGuarantee.Size = new System.Drawing.Size(766, 220);
            this.TpBankGuarantee.Text = "Bank Guarantee";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtBankGuaranteeAmt);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.TxtBankGuaranteeNo);
            this.panel5.Controls.Add(this.label29);
            this.panel5.Controls.Add(this.DteBankGuaranteeDt);
            this.panel5.Controls.Add(this.label26);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.LueBankCode);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 220);
            this.panel5.TabIndex = 95;
            // 
            // TxtBankGuaranteeAmt
            // 
            this.TxtBankGuaranteeAmt.EnterMoveNextControl = true;
            this.TxtBankGuaranteeAmt.Location = new System.Drawing.Point(127, 84);
            this.TxtBankGuaranteeAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankGuaranteeAmt.Name = "TxtBankGuaranteeAmt";
            this.TxtBankGuaranteeAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankGuaranteeAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankGuaranteeAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankGuaranteeAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtBankGuaranteeAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBankGuaranteeAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBankGuaranteeAmt.Properties.MaxLength = 80;
            this.TxtBankGuaranteeAmt.Size = new System.Drawing.Size(202, 20);
            this.TxtBankGuaranteeAmt.TabIndex = 103;
            this.TxtBankGuaranteeAmt.Validated += new System.EventHandler(this.TxtBankGuaranteeAmt_Validated);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(71, 87);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 14);
            this.label30.TabIndex = 102;
            this.label30.Text = "Amount";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankGuaranteeNo
            // 
            this.TxtBankGuaranteeNo.EnterMoveNextControl = true;
            this.TxtBankGuaranteeNo.Location = new System.Drawing.Point(127, 21);
            this.TxtBankGuaranteeNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankGuaranteeNo.Name = "TxtBankGuaranteeNo";
            this.TxtBankGuaranteeNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankGuaranteeNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankGuaranteeNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankGuaranteeNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankGuaranteeNo.Properties.MaxLength = 80;
            this.TxtBankGuaranteeNo.Size = new System.Drawing.Size(264, 20);
            this.TxtBankGuaranteeNo.TabIndex = 97;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(19, 24);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(103, 14);
            this.label29.TabIndex = 96;
            this.label29.Text = "Bank Guarantee#";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBankGuaranteeDt
            // 
            this.DteBankGuaranteeDt.EditValue = null;
            this.DteBankGuaranteeDt.EnterMoveNextControl = true;
            this.DteBankGuaranteeDt.Location = new System.Drawing.Point(127, 63);
            this.DteBankGuaranteeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBankGuaranteeDt.Name = "DteBankGuaranteeDt";
            this.DteBankGuaranteeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBankGuaranteeDt.Properties.Appearance.Options.UseFont = true;
            this.DteBankGuaranteeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBankGuaranteeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBankGuaranteeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBankGuaranteeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBankGuaranteeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBankGuaranteeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBankGuaranteeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBankGuaranteeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBankGuaranteeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBankGuaranteeDt.Size = new System.Drawing.Size(125, 20);
            this.DteBankGuaranteeDt.TabIndex = 101;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(89, 66);
            this.label26.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 14);
            this.label26.TabIndex = 100;
            this.label26.Text = "Date";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(89, 45);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(33, 14);
            this.label25.TabIndex = 98;
            this.label25.Text = "Bank";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(127, 42);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(202, 20);
            this.LueBankCode.TabIndex = 99;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // TpAttachment
            // 
            this.TpAttachment.Appearance.Header.Options.UseTextOptions = true;
            this.TpAttachment.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpAttachment.Appearance.PageClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpAttachment.Appearance.PageClient.Options.UseBackColor = true;
            this.TpAttachment.Controls.Add(this.TxtFile);
            this.TpAttachment.Controls.Add(this.label34);
            this.TpAttachment.Controls.Add(this.BtnFile);
            this.TpAttachment.Controls.Add(this.PbUpload);
            this.TpAttachment.Controls.Add(this.ChkFile3);
            this.TpAttachment.Controls.Add(this.label35);
            this.TpAttachment.Controls.Add(this.BtnDownload3);
            this.TpAttachment.Controls.Add(this.TxtFile2);
            this.TpAttachment.Controls.Add(this.PbUpload3);
            this.TpAttachment.Controls.Add(this.BtnDownload);
            this.TpAttachment.Controls.Add(this.BtnFile3);
            this.TpAttachment.Controls.Add(this.BtnFile2);
            this.TpAttachment.Controls.Add(this.TxtFile3);
            this.TpAttachment.Controls.Add(this.PbUpload2);
            this.TpAttachment.Controls.Add(this.label36);
            this.TpAttachment.Controls.Add(this.BtnDownload2);
            this.TpAttachment.Controls.Add(this.ChkFile2);
            this.TpAttachment.Controls.Add(this.ChkFile);
            this.TpAttachment.Name = "TpAttachment";
            this.TpAttachment.Size = new System.Drawing.Size(766, 220);
            this.TpAttachment.Text = "Attachment File";
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmSOContract2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 701);
            this.Name = "FrmSOContract2";
            this.Text = "SO Contract";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNTPDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCustomerContactperson.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeProjectDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBOQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmtBOM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShpMCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSPCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCOAAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcOutgoingPayment)).EndInit();
            this.TcOutgoingPayment.ResumeLayout(false);
            this.TpBOQ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpBOQ2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpItem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDeliveryDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel9.ResumeLayout(false);
            this.TpJO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJODocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJOCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.TpBankGuarantee.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankGuaranteeAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankGuaranteeNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBankGuaranteeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            this.TpAttachment.ResumeLayout(false);
            this.TpAttachment.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraTab.XtraTabPage TpCOA;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtCOAAmt;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo2;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private DevExpress.XtraTab.XtraTabPage TpAdditional;
        private DevExpress.XtraTab.XtraTabControl TcOutgoingPayment;
        private DevExpress.XtraTab.XtraTabPage TpBOQ;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage TpItem;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        internal DevExpress.XtraEditors.TextEdit TxtSAName;
        public DevExpress.XtraEditors.SimpleButton BtnCustomerShipAddress;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.LookUpEdit LueStatus;
        internal DevExpress.XtraEditors.TextEdit TxtAddress;
        internal DevExpress.XtraEditors.TextEdit TxtCity;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtCountry;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.SimpleButton BtnCtShippingAddress;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCd;
        public DevExpress.XtraEditors.SimpleButton BtnContact;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueShpMCode;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LueSPCode;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        internal DevExpress.XtraEditors.TextEdit TxtFax;
        public DevExpress.XtraEditors.SimpleButton BtnBOQDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtBOQDocNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        public DevExpress.XtraEditors.TextEdit TxtCustomerContactperson;
        private DevExpress.XtraEditors.MemoExEdit MeeProjectDesc;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraTab.XtraTabPage TpJO;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.Label label24;
        public DevExpress.XtraEditors.LookUpEdit LueJOType;
        private DevExpress.XtraTab.XtraTabPage TpBankGuarantee;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.TextEdit TxtBankGuaranteeNo;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.DateEdit DteBankGuaranteeDt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        public DevExpress.XtraEditors.LookUpEdit LueBankCode;
        internal DevExpress.XtraEditors.DateEdit DteJODocDt;
        public DevExpress.XtraEditors.LookUpEdit LueJOCode;
        private DevExpress.XtraEditors.TextEdit TxtBankGuaranteeAmt;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.TextEdit TxtPONo;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraTab.XtraTabPage TpBOQ2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtAmtBOM;
        private System.Windows.Forms.Label label32;
        public DevExpress.XtraEditors.SimpleButton BtnNTPDocNo;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        private System.Windows.Forms.ProgressBar PbUpload2;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        private System.Windows.Forms.ProgressBar PbUpload3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.LookUpEdit LuePtCode;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        internal DevExpress.XtraEditors.LookUpEdit LueTaxCode;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.DateEdit DteDeliveryDt;
        internal DevExpress.XtraEditors.TextEdit TxtNTPDocNo;
        private DevExpress.XtraTab.XtraTabPage TpAttachment;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button BtnCopyBOQ;
        private System.Windows.Forms.Button BtnCopyDeliveryDt;
    }
}