﻿#region Update
/*
  10/12/2019 [DITA/YK] Feedback tambah wbs 2
  21/12/2021 [BRI/YK] Membuat WBS2 supaya bisa ditarik ke Project Delivery berdasarkan param IsWBS2SameAsWBS
  11/08/2022 [TYO/YK] merubah source mFrmParent.Grd1 ke mFrmParent.Grd2
  10/02/2023 [DITA/MNET] tambah SettledRevAmt, EstimatedCost
  21/02/2023 [DITA/MNET] based on param IsWBS2SummarizedWBS1, est price ambil dari field price dan penyesuaian wbs 1 + wbs 2
  24/02/2023 [DITA/MNET] masih ada bug saat memilih wbs1
 * */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectDeliveryDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmProjectDelivery mFrmParent;
        private string mSQL = string.Empty, mPRJIDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectDeliveryDlg2(FrmProjectDelivery FrmParent, string PRJIDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPRJIDocNo = PRJIDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetLueTaskCode(ref LueTaskCode);
                SetLueStageCode(ref LueStageCode);
                SetLueTypeCode(ref LueTypeCode);
                SetGrd();
                SetSQL();

                if (!mFrmParent.mIsWBS2SameAsWBS)
                {
                    label2.Visible = false;
                    LueTypeCode.Visible = false;
                    ChkTypeCode.Visible = false;
                    panel2.Height = 49;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "DNo",
                    "Stage", 
                    "Task",
                    "Bobot (%)", 

                    //6-10
                    "Estimated Price",
                    "SO Contract",
                    "SO Contract Revision",
                    "Type",
                    "SettledRevAmt",

                    //11
                    "EstimatedCost"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 0, 150, 150, 100, 
                    
                    //6-10
                    150, 150, 150, 100,0,
                    //11
                    0
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 10, 11 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            if (!mFrmParent.mIsWBS2SummarizedWBS1)
            {
                SQL.AppendLine("Select A.DocNo, A.DNo, B.StageCode, B.StageName, C.TaskCode, C.TaskName, A.BobotPercentage, A.EstimatedAmt, E.SOCDocNo, E.DocNo As SOCRDocNo , 'WBS1' Type, ");
                SQL.AppendLine("SettledRevAmt, EstimatedCost ");
                SQL.AppendLine("From TblProjectImplementationDtl A  ");
                SQL.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode  ");
                SQL.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode  ");
                SQL.AppendLine("Inner Join TblProjectImplementationHdr D On A.DocNo = D.DocNo  ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo  ");
                SQL.AppendLine("Where A.DocNo = @PRJIDocNo  ");
                SQL.AppendLine("And A.SettledInd = 'N'  ");
                SQL.AppendLine("And A.InvoicedInd = 'N'  ");
                SQL.AppendLine("And Locate(Concat('##', A.DocNo, A.DNo, 'WBS1', '##'), @SelectedPRJIData) < 1  ");
            }
            else
            {
                SQL.AppendLine("Select A.DocNo, A.DNo, B.StageCode, B.StageName, Group_Concat(C.TaskCode) TaskCode, Group_Concat(C.TaskName) TaskName,  A.BobotPercentage, A.Amt EstimatedAmt, E.SOCDocNo, E.DocNo As SOCRDocNo , 'WBS1' Type, ");
                SQL.AppendLine("A.SettledRevAmt, A.EstimatedCost ");
                SQL.AppendLine("From TblProjectImplementationDtl A  ");
                SQL.AppendLine("Inner Join TblProjectImplementationDtl5 A2 On A.DocNo = A2.DocNo And A.StageCode = A2.StageCode ");
                SQL.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode  ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select TaskCode, TaskName ");
                SQL.AppendLine("    From TblProjectTask ");
                SQL.AppendLine(" ) C On A2.TaskCode = C.TaskCode ");
                SQL.AppendLine("Inner Join TblProjectImplementationHdr D On A.DocNo = D.DocNo  ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo  ");
                SQL.AppendLine("Where A.DocNo = @PRJIDocNo  ");
                SQL.AppendLine("And A.SettledInd = 'Y'  ");
                SQL.AppendLine("And A.InvoicedInd = 'N'  ");
                SQL.AppendLine("And Concat(A.DocNo,A.DNo) Not In(Select Concat(B.PRJIDocNo,B.PRJIDNo) From TblProjectDeliveryHdr A Inner Join TblProjectDeliveryDtl B On A.DocNo = B.DocNo where CancelInd = 'N' And Status = 'A' And B.PRJIDocNo = @PRJIDocNo And Type = 'WBS1' ) ");
                SQL.AppendLine("And Locate(Concat('##', A.DocNo, A.DNo, 'WBS1', '##'), @SelectedPRJIData) < 1  ");
                SQL.AppendLine("Group By A.DocNo, A.DNo, B.StageCode ");
            }
            if (mFrmParent.mIsPRJIUseWBS2)
            {
                SQL.AppendLine("UNION ALL ");
                SQL.AppendLine("Select A.DocNo, A.DNo, B.StageCode, B.StageName, C.TaskCode,  C.TaskName, A.BobotPercentage,  ");
                if(mFrmParent.mIsWBS2SummarizedWBS1)
                    SQL.AppendLine("A.Amt EstimatedAmt ,");
                else
                    SQL.AppendLine((mFrmParent.mIsWBS2SameAsWBS ? "A.EstimatedAmt" : "0") + " EstimatedAmt ,");
                SQL.AppendLine("E.SOCDocNo, E.DocNo As SOCRDocNo , 'WBS2' Type, A.Amt As SettledRevAmt, (A.BobotPercentage * F.TotalResource) As EstimatedCost ");
                SQL.AppendLine("From TblProjectImplementationDtl5 A  ");
                SQL.AppendLine("Inner Join TblProjectStage B On A.StageCode = B.StageCode  ");
                SQL.AppendLine("Inner Join TblProjectTask C On A.TaskCode = C.TaskCode  ");
                SQL.AppendLine("Inner Join TblProjectImplementationHdr D On A.DocNo = D.DocNo  ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo  ");
                SQL.AppendLine("Inner Join TblProjectImplementationHdr F On A.DocNo = F.DocNo ");
                SQL.AppendLine("Where A.DocNo = @PRJIDocNo  ");
                SQL.AppendLine("And A.SettledInd = 'N'  ");
                SQL.AppendLine("And A.InvoicedInd = 'N'  ");
                SQL.AppendLine("And Locate(Concat('##', A.DocNo, A.DNo, 'WBS2', '##'), @SelectedPRJIData) < 1  ");
            }
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedPRJIData", mFrmParent.GetSelectedPRJIData());
                Sm.CmParam<String>(ref cm, "@PRJIDocNo", mPRJIDocNo);

                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStageCode), "T.StageCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueTaskCode), "T.TaskCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueTypeCode), "T.Type", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL.ToString() + Filter + " Order By T.DocNo, T.DNo;",
                    new string[] 
                    { 
                        "DNo", 
                        "StageName", "TaskName", "BobotPercentage", "EstimatedAmt", "SOCDocNo",
                        "SOCRDocNo", "Type", "SettledRevAmt", "EstimatedCost"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsPRJIDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 9, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 12, Grd1, Row2, 11);
                        mFrmParent.ComputeAmt();

                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 4, 9 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 WBS.");
        }

        private bool IsPRJIDataAlreadyChosen(int Row)
        {
            string PRJIData = string.Concat(mPRJIDocNo, Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 9));
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count - 1; Index++)
                if (Sm.CompareStr(string.Concat(mFrmParent.TxtPRJIDocNo.Text, Sm.GetGrdStr(mFrmParent.Grd2, Index, 1), Sm.GetGrdStr(mFrmParent.Grd2, Index, 10)), PRJIData)) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        internal void SetLueTypeCode(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select 'WBS1' As Col1, 'WBS1' As Col2 Union All " +
                "Select 'WBS2' As Col1, 'WBS2' As Col2 ; ",
                0, 35, false, true, "Type", "Name", "Col2", "Col1");
        }

        private void SetLueStageCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select StageCode As Col1, StageName As Col2 ");
            SQL.AppendLine("From TblProjectStage ");
            SQL.AppendLine("Where StageCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct StageCode ");
            SQL.AppendLine("    From TblProjectImplementationDtl ");
            SQL.AppendLine("    Where DocNo = @PRJIDocNo ");
            SQL.AppendLine("    And SettledInd = 'N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By StageName; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", mPRJIDocNo);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueTaskCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select TaskCode As Col1, TaskName As Col2 ");
            SQL.AppendLine("From TblProjectTask ");
            SQL.AppendLine("Where TaskCode In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct TaskCode ");
            SQL.AppendLine("    From TblProjectImplementationDtl ");
            SQL.AppendLine("    Where DocNo = @PRJIDocNo ");
            SQL.AppendLine("    And SettledInd = 'N' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By TaskName; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@PRJIDocNo", mPRJIDocNo);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueStageCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueStageCode, new Sm.RefreshLue1(SetLueStageCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStageCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Stage");
        }

        private void LueTaskCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaskCode, new Sm.RefreshLue1(SetLueTaskCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkTaskCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Task");
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTypeCode, new Sm.RefreshLue1(SetLueTypeCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        #endregion

        #endregion

    }
}
