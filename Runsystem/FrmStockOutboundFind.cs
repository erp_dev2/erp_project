﻿#region Update
/*
    12/11/2018 [TKG/SIER] New Application
    14/11/2018 [VIN/SIER] Menambah filter berdasarkan transportation type dan Vehicle reg. number
    06/01/2020 [TKG/SIER] tambah customer.
    13/07/2020 [IBL/SIER] tambah kolom status
    22/07/2020 [IBL/SRN] Mengganti Customer menjadi Vendor berdasarkan parameter StockOutboundFormat
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmStockOutboundFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmStockOutbound mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmStockOutboundFind(FrmStockOutbound FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);

                if (mFrmParent.mStockOutboundFormat == "1")
                {
                    LblCtVd.Text = "Customer";
                    Sl.SetLueCtCode(ref LueCtCode);
                }
                else
                {
                    LblCtVd.Text = "Vendor";
                    Sl.SetLueVdCode(ref LueCtCode);
                }

                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("B.WhsName, C.CtName, D.VdName, A.VehicleRegNo, A.TransportType, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("Case ");
	        SQL.AppendLine("    When A.Status = 'A' Then 'Approved' ");
	        SQL.AppendLine("    When A.Status = 'C' Then 'Cancelled' ");
	        SQL.AppendLine("    When A.Status = 'O' Then 'Outstanding' ");
            SQL.AppendLine("End As Status ");
            SQL.AppendLine("From TblStockOutboundHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Left Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblVendor D On A.VdCode=D.VdCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=B.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Status",
                        "Warehouse",
                        (mFrmParent.mStockOutboundFormat == "1" ? "Customer" : "Vendor"),
                        
                        //6-10
                        "Vehicle Registration#",
                        "Transport Type",
                        "Remark",
                        "Created By", 
                        "Created Date", 

                        //11-14
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 100, 200, 200,  
                        
                        //6-10
                        180, 200, 130, 130, 130, 

                        //11-14
                        130, 130, 130, 130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                if(mFrmParent.mStockOutboundFormat == "1")
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                else
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtVehicleRegNo.Text, "A.VehicleRegNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtTransportType.Text, "A.TransportType", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CtName", "Status", "WhsName", "VehicleRegNo",  
                            
                            //6-10
                            "TransportType", "Remark", "CreateBy", "CreateDt", "LastUpBy", 

                            //11-12
                            "LastUpDt", "VdName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                            if (mFrmParent.mStockOutboundFormat == "1")
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            else
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }
       
        private void TxtVehicleRegNo_Validated_1(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkVehicleRegNo_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vehicle registration#");
        }
        private void TxtTransportType_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkTransportType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Transport type");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if(mFrmParent.mStockOutboundFormat == "1" )
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            else
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, (mFrmParent.mStockOutboundFormat == "1" ? "Customer" : "Vendor"));
        }

        #endregion

        #endregion

    
    }
}
