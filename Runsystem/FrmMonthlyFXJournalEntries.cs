﻿#region Update
/*
    25/10/2017 [TKG] memisahkan proses depresiasi asset dengan selisih kurs.
    13/04/2022 [BRI/AMKA] tambah cost center berdasarkan param CostCenterForForeignCurrencyExchangeGains
    19/07/2022 [RIS/PHT] Menambahkan AcNoForForeignCurrencyExchangeExpense dengan param IsForeignCurrencyExchangeUseExpense
    10/01/2023 [WED/PHT] penomoran journal berdasarkan parameter JournalDocNoFormat
    12/01/2023 [DITA/PHT] tambah validasi closing journal dengan parameter IsClosingJournalBasedOnMultiProfitCenter
    31/01/2023 [SET/PHT] penyesuaian value journal
    16/02/2023 [WED/PHT] bug fix Journal dari Closing Balance : Amt1 dan Amt2 kebalik
    15/03/2023 [WED/PHT] kode journal dirubah dari M menjadi BM atau BK berdasarkan COA, berdasarkan parameter JournalDocNoFormat
    17/03/2023 [BRI/PHT] bug amt null
    17/03/2023 [WED/PHT] bug nilai Journal nya 0
    20/03/2023 [WED/PHT] bug fix journal tidak balance
    17/04/2023 [WED/PHT] method query1 save journal di buat public agar bisa diakses di TrnReprocessJournal
    02/05/2023 [WED/PHT] APDP dan ARDP tidak masuk ke journal berdasarkan parameter ForeignExchangeJournalEntryValuationType
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmMonthlyFXJournalEntries : Form
    {
        #region Field

        private string 
            mMenuCode= string.Empty, 
            mMainCurCode = string.Empty,
            mJournalDocNoFormat = string.Empty,
            mDocTitle = string.Empty,
            mForeignExchangeJournalEntryValuationType = string.Empty
            ;
        private bool
            mIsForeignCurrencyExchangeUseExpense = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false;

        #endregion

        #region Constructor

        public FrmMonthlyFXJournalEntries(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        private void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                ExecQuery();
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In ( ");
            SQL.AppendLine("'MainCurCode', 'IsForeignCurrencyExchangeUseExpense', 'JournalDocNoFormat', 'DocTitle', 'IsClosingJournalBasedOnMultiProfitCenter', ");
            SQL.AppendLine("'ForeignExchangeJournalEntryValuationType' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            #region boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                            case "IsForeignCurrencyExchangeUseExpense": mIsForeignCurrencyExchangeUseExpense = ParValue == "Y"; break;
                            #endregion

                            #region string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "JournalDocNoFormat": mJournalDocNoFormat = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "ForeignExchangeJournalEntryValuationType": mForeignExchangeJournalEntryValuationType = ParValue; break;
                            #endregion
                        }
                    }
                }
                dr.Close();
            }


            //mMainCurCode = Sm.GetParameter("MainCurCode");
            //mIsForeignCurrencyExchangeUseExpense = Sm.GetParameterBoo("IsForeignCurrencyExchangeUseExpense");
            //mJournalDocNoFormat = Sm.GetParameter("JournalDocNoFormat");
            //mDocTitle = Sm.GetParameter("DocTitle");
            //mIsClosingJournalBasedOnMultiProfitCenter = Sm.GetParameterBoo("IsClosingJournalBasedOnMultiProfitCenter");

            if (mJournalDocNoFormat.Length == 0) mJournalDocNoFormat = "1";
            if (mForeignExchangeJournalEntryValuationType.Length == 0) mForeignExchangeJournalEntryValuationType = "1";
        }

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") || IsThereNoDataToBeProcessed()
                ;
        }

        private bool IsThereNoDataToBeProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblClosingBalanceInCashHdr ");
            SQL.AppendLine("Where JournalDocNo Is Null ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "There is no foreign currency exchange gains (Cash and Bank) data to be processed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("INSERT IGNORE INTO `tblparameter` (`ParCode`, `ParDesc`, `ParValue`, `Customize`, `ParCtCode`, `Editable`, `CreateBy`, `CreateDt`, `LastUpBy`, `LastUpDt`) VALUES ('ForeignExchangeJournalEntryValuationType', '1: Default (Cash,ARDP,APDP); Value 2: Custom PHT (Cash)', '1', 'PHT', NULL, 'Y', 'WEDHA', '202305021112', NULL, NULL); ");

            Sm.ExecQuery(SQL.ToString());
        }

        public string GetQueryExec1(string mDocTitle, bool mIsForeignCurrencyExchangeUseExpense, bool IsFromReprocessJournal)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo1, @DocDt, Concat('Foreign Currency Exchange Gains (Cash and Bank) On ', @Mth, ' ', @Yr), ");
            SQL.AppendLine("@MenuCode, Concat((Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ' (Cash And Bank Foreign Currency Exchange)' ), ");
            SQL.AppendLine("@CCCode, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            if (mDocTitle != "PHT")
            {
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
                SQL.AppendLine("        Select AcNo, ");
                SQL.AppendLine("        Case When Amt1>Amt2 Then Abs(Amt1-Amt2) Else 0 End As DAmt, ");
                SQL.AppendLine("        Case When Amt1>Amt2 Then 0 Else Abs(Amt1-Amt2) End As CAmt ");
                SQL.AppendLine("        From (");
                SQL.AppendLine("            Select D.ParValue As AcNo, ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<Concat(Left(@DocDt, 6), '01') ");
                SQL.AppendLine("                And CurCode1=C.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0) * B.Amt As Amt1, ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt ");
                SQL.AppendLine("                And CurCode1=C.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0) * B.Amt As Amt2 ");
                SQL.AppendLine("            From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("            Inner Join TblClosingBalanceInCashDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblBankAccount C ");
                SQL.AppendLine("                On B.BankAcCode=C.BankAcCode ");
                SQL.AppendLine("                And C.CurCode Is Not Null ");
                SQL.AppendLine("                And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("            Inner Join TblParameter D On D.ParCode='AcNoForForeignCurrencyExchangeGains' And D.ParValue Is Not Null ");
                SQL.AppendLine("            Where A.Mth=@Mth ");
                SQL.AppendLine("            And A.Yr=@Yr ");
                SQL.AppendLine("            And A.JournalDocNo Is Null ");
                SQL.AppendLine("        ) T1 ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select AcNo, ");
                SQL.AppendLine("        Case When Amt1>Amt2 Then 0 Else Abs(Amt1-Amt2) End As DAmt, ");
                SQL.AppendLine("        Case When Amt1>Amt2 Then Abs(Amt1-Amt2) Else 0 End As CAmt ");
                SQL.AppendLine("        From (");
                SQL.AppendLine("            Select C.COAAcNo As AcNo, ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<Concat(Left(@DocDt, 6), '01') ");
                SQL.AppendLine("                And CurCode1=C.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0) * B.Amt As Amt1, ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt ");
                SQL.AppendLine("                And CurCode1=C.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0) * B.Amt As Amt2 ");
                SQL.AppendLine("            From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("            Inner Join TblClosingBalanceInCashDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("            Inner Join TblBankAccount C ");
                SQL.AppendLine("                On B.BankAcCode=C.BankAcCode ");
                SQL.AppendLine("                And C.CurCode Is Not Null ");
                SQL.AppendLine("                And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("            Where A.Mth=@Mth ");
                SQL.AppendLine("            And A.Yr=@Yr ");
                SQL.AppendLine("            And A.JournalDocNo Is Null ");
                SQL.AppendLine("        ) T2 ");

                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine("    Where AcNo Is Not Null ");
                SQL.AppendLine("    Group By AcNo ");
            }
            else if (mDocTitle == "PHT")
            {
                SQL.AppendLine("    Select Case");
                SQL.AppendLine("    When DAmt < 0 AND CAmt = 0 Then T3.ParValue ");
                SQL.AppendLine("    When CAmt > 0 AND DAmt = 0 Then T4.Parvalue ");
                SQL.AppendLine("    ELSE COAAcNo ");
                SQL.AppendLine("    END AS AcNo, ");
                SQL.AppendLine("    Abs(DAmt) DAmt, Abs(CAmt) CAmt From ( ");
                SQL.AppendLine("        Select COAAcNo, ");
                SQL.AppendLine("        Case When SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) < 0 Then SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) ELSE 0 END AS DAmt, ");
                SQL.AppendLine("        Case When SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) < 0 Then 0 ELSE SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) END AS CAmt ");
                SQL.AppendLine("        From (");
                SQL.AppendLine("            Select C.COAAcNo, ");
                SQL.AppendLine("            E.Amt As Amt2, ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt ");
                SQL.AppendLine("                And CurCode1=C.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0) * B.Amt As Amt1 ");
                SQL.AppendLine("            From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("            Inner Join TblClosingBalanceInCashDtl B On A.DocNo=B.DocNo And A.CancelInd = 'N' ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                SQL.AppendLine("                And C.CurCode Is Not Null ");
                SQL.AppendLine("                And C.CurCode<>@MainCurCode ");
                //SQL.AppendLine("            Inner Join TblParameter D On D.ParCode='AcNoForForeignCurrencyExchangeGains' And D.ParValue Is Not Null ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                SELECT BankAcCode, T.Opening + sum(AMT) Amt From ( ");
                SQL.AppendLine("                Select A.DOCNO, B.BankAcCode,  IfNull(( ");
                SQL.AppendLine("                    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                    Where Left(RateDt, 6) < Left(@DocDt, 6) And CurCode1=C.CurCode And CurCode2=@MainCurCode Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("                ), 0) * B.Amt  As Opening, 0 As Amt ");
                SQL.AppendLine("                From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("                Inner Join TblClosingBalanceInCashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' ");
                SQL.AppendLine("                Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode And C.CurCode Is Not Null And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("                Where A.DocNo=B.DocNo And Concat(A.Yr, A.Mth)=@YrMth2 ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DOCNO, A.BankAcCode, 0 As Opening, Case A.Actype When 'C' Then -1 Else 1 End * ");
                SQL.AppendLine("                IfNull(If(A.ExcRate != 0, A.ExcRate, ( ");
                SQL.AppendLine("                    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                    Where RateDt <= A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("                )), 0) * B.Amt  As Amt ");
                SQL.AppendLine("                From TblVoucherHdr A ");
                SQL.AppendLine("                Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode And C.CurCode Is Not Null And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("                Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
                SQL.AppendLine("                Where A.DocNo = B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DOCNO, A.BankAcCode2 As BankAcCode, 0 As Opening, (Case A.Actype2 When 'C' Then -1 Else 1 End) * ");
                SQL.AppendLine("                IfNull(If(A.ExcRate != 0, A.ExcRate, ( ");
                SQL.AppendLine("                    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                    Where RateDt <= A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("                )), 0) * B.Amt  As Amt ");
                SQL.AppendLine("                From TblVoucherHdr A ");
                SQL.AppendLine("                Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode And C.CurCode Is Not Null And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("                Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
                SQL.AppendLine("                Where A.DocNo=B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 And A.AcType2 Is Not Null ");
                SQL.AppendLine("                ) T Group By T.BankAcCode ");
                SQL.AppendLine("            ) E ON B.BankAcCode = E.BankAcCode ");
                SQL.AppendLine("            Where A.Mth=@Mth ");
                SQL.AppendLine("            And A.Yr=@Yr ");
                if (!IsFromReprocessJournal) SQL.AppendLine("            And A.JournalDocNo Is Null ");
                SQL.AppendLine("        ) T1 ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select COAAcNo, ");
                SQL.AppendLine("        Case When SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) < 0 Then 0 ELSE SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) END AS DAmt, ");
                SQL.AppendLine("        Case When SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) < 0 Then SUM(IfNull(Amt1, 0.00)-IfNull(Amt2, 0.00)) ELSE 0 END AS CAmt ");
                SQL.AppendLine("        From (");
                SQL.AppendLine("            Select C.COAAcNo, ");
                SQL.AppendLine("            E.Amt As Amt2, ");
                SQL.AppendLine("            IfNull(( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt ");
                SQL.AppendLine("                And CurCode1=C.CurCode ");
                SQL.AppendLine("                And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ), 0) * B.Amt As Amt1 ");
                SQL.AppendLine("            From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("            Inner Join TblClosingBalanceInCashDtl B On A.DocNo=B.DocNo And A.CancelInd = 'N' ");
                SQL.AppendLine("            Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode ");
                SQL.AppendLine("                And C.CurCode Is Not Null ");
                SQL.AppendLine("                And C.CurCode<>@MainCurCode ");
                //SQL.AppendLine("            Inner Join TblParameter D On D.ParCode='AcNoForForeignCurrencyExchangeGains' And D.ParValue Is Not Null ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                SELECT  BankAcCode, T.Opening + sum(AMT) Amt From ( ");
                SQL.AppendLine("                Select  A.DOCNO, B.BankAcCode,  IfNull(( ");
                SQL.AppendLine("                    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                    Where Left(RateDt, 6) < Left(@DocDt, 6) And CurCode1=C.CurCode And CurCode2=@MainCurCode Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("                ), 0) * B.Amt  As Opening, 0 As Amt ");
                SQL.AppendLine("                From TblClosingBalanceInCashHdr A ");
                SQL.AppendLine("                Inner Join TblClosingBalanceInCashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' ");
                SQL.AppendLine("                Inner Join TblBankAccount C On B.BankAcCode=C.BankAcCode And C.CurCode Is Not Null And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("                Where A.DocNo=B.DocNo And Concat(A.Yr, A.Mth)=@YrMth2 ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DOCNO, A.BankAcCode, 0 As Opening, Case A.Actype When 'C' Then -1 Else 1 End * ");
                SQL.AppendLine("                IfNull(If(A.ExcRate != 0, A.ExcRate, ( ");
                SQL.AppendLine("                    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                    Where RateDt <= A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("                )), 0) * B.Amt  As Amt ");
                SQL.AppendLine("                From TblVoucherHdr A ");
                SQL.AppendLine("                Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                Inner Join TblBankAccount C On A.BankAcCode=C.BankAcCode And C.CurCode Is Not Null And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("                Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
                SQL.AppendLine("                Where A.DocNo = B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 ");
                SQL.AppendLine("                Union All ");
                SQL.AppendLine("                Select A.DOCNO, A.BankAcCode2 As BankAcCode, 0 As Opening, (Case A.Actype2 When 'C' Then -1 Else 1 End) * ");
                SQL.AppendLine("                IfNull(If(A.ExcRate != 0, A.ExcRate, ( ");
                SQL.AppendLine("                    Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                    Where RateDt <= A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("                )), 0) * B.Amt  As Amt ");
                SQL.AppendLine("                From TblVoucherHdr A ");
                SQL.AppendLine("                Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("                Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode And C.CurCode Is Not Null And C.CurCode<>@MainCurCode ");
                SQL.AppendLine("                Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
                SQL.AppendLine("                Where A.DocNo=B.DocNo And A.CancelInd='N' And Left(A.DocDt, 6)=@YrMth1 And A.AcType2 Is Not Null ");
                SQL.AppendLine("                ) T Group By T.BankAcCode ");
                SQL.AppendLine("            ) E ON B.BankAcCode = E.BankAcCode ");
                SQL.AppendLine("            Where A.Mth=@Mth ");
                SQL.AppendLine("            And A.Yr=@Yr ");
                if (!IsFromReprocessJournal) SQL.AppendLine("            And A.JournalDocNo Is Null ");
                SQL.AppendLine("        ) T2 ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine("            Inner Join TblParameter T3 On T3.ParCode='AcNoForForeignCurrencyExchangeExpense' And T3.ParValue Is Not Null ");
                SQL.AppendLine("            Inner Join TblParameter T4 On T4.ParCode='AcNoForForeignCurrencyExchangeGains' And T4.ParValue Is Not Null ");
                //SQL.AppendLine("    Where AcNo Is Not Null ");
                //SQL.AppendLine("    Group By AcNo ");
            }

            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo1;");

            SQL.AppendLine("Update TblClosingBalanceInCashHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo1 ");
            SQL.AppendLine("Where JournalDocNo Is Null ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr;");

            if (mIsForeignCurrencyExchangeUseExpense)
            {
                SQL.AppendLine("UPDATE TblJournalHdr A ");
                SQL.AppendLine("INNER JOIN tbljournaldtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("SET A.JnDesc = ");
                SQL.AppendLine("case ");
                SQL.AppendLine("    when B.DAmt > B.CAmt then Concat('Foreign Currency Exchange Expense (Cash & Bank) On ', @Mth, ' ', @Yr) ");
                SQL.AppendLine("    else Concat('Foreign Currency Exchange Gains (Cash & Bank Deposit) On ', @Mth, ' ', @Yr)");
                SQL.AppendLine("End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo1 ");
                SQL.AppendLine("And B.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("); ");

                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join TblParameter B On B.Parcode = 'AcNoForForeignCurrencyExchangeExpense' and B.ParValue Is Not NULL ");
                SQL.AppendLine("INNER JOIN TblParameter C ON C.Parcode = 'AcNoForForeignCurrencyExchangeGains' and C.ParValue Is Not NULL  ");
                SQL.AppendLine("Set A.AcNo = ");
                SQL.AppendLine("Case ");
                SQL.AppendLine("    When A.DAmt > A.CAmt then B.ParValue ");
                SQL.AppendLine("    else C.ParValue ");
                SQL.AppendLine("End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo1 ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("); ");

            }

            return SQL.ToString();
        }

        private void Process()
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No && IsProcessedDataNotValid()) return;

                var Dt = new DateTime(
                       Int32.Parse(Sm.GetLue(LueYr)),
                       Int32.Parse(Sm.GetLue(LueMth)),
                       1, 0, 0, 0
                       );
                Dt = Dt.AddMonths(1);
                Dt = Dt.AddDays(-1);
                string DocDt = Sm.Left(Sm.GenerateDateFormat(Dt), 8);
                string ccCode = Sm.GetParameter("CostCenterForForeignCurrencyExchangeGains");
                string profitCenterCode = Sm.GetValue("Select ProfitCenterCode From TblCostCenter Where CCCode = @Param", ccCode);
                string code1 = Sm.GetCode1ForJournalDocNo("FrmMonthlyFXJournalEntries", string.Empty, string.Empty, mJournalDocNoFormat);

                if (
                (mIsClosingJournalBasedOnMultiProfitCenter ?
                    Sm.IsClosingJournalInvalid(true, false, DocDt, profitCenterCode) :
                    Sm.IsClosingJournalInvalid(DocDt))) return;
                Cursor.Current = Cursors.WaitCursor;

                var Mth = Sm.GetLue(LueMth);
                var Yr = Sm.GetLue(LueYr);

                bool IsData1Existed = Sm.IsDataExist(
                    "Select 1 " +
                    "From TblClosingBalanceInCashHdr " +
                    "Where Mth='" + Mth + "' " +
                    "And Yr='" + Yr + "' " +
                    "And JournalDocNo Is Null;"
                    );

                bool IsData2Existed = (mForeignExchangeJournalEntryValuationType == "2") ? false : Sm.IsDataExist(
                    "Select 1 " +
                    "From TblVendorDepositSummary " +
                    "Where CurCode Is Not Null " +
                    "And CurCode<>@Param " +
                    "And Amt<>0;", mMainCurCode
                    );

                bool IsData3Existed = (mForeignExchangeJournalEntryValuationType == "2") ? false : Sm.IsDataExist(
                    "Select 1 " +
                    "From TblCustomerDepositSummary " +
                    "Where CurCode Is Not Null " +
                    "And CurCode<>@Param " +
                    "And Amt<>0;", mMainCurCode
                    );

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                string JNDocNo1 = string.Empty, JNDocNo2 = string.Empty, JNDocNo3 = string.Empty;

                #region Selisih Kurs Cash and Bank

                if (IsData1Existed)
                {
                    SQL.AppendLine(GetQueryExec1(mDocTitle, mIsForeignCurrencyExchangeUseExpense, false));
                }

                #endregion

                #region Selisih Kurs Vendor Deposit

                if (IsData2Existed)
                {
                    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@JournalDocNo2, @DocDt, Concat('Foreign Currency Exchange Gains (Vendor Deposit) On ', @Mth, ' ', @Yr), ");
                    SQL.AppendLine("@MenuCode, Concat((Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ' (Vendor Deposit Foreign Currency Exchange)' ), ");
                    SQL.AppendLine("@CCCode, @UserCode, CurrentDateTime()); ");

                    SQL.AppendLine("Set @Row:=0; ");

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblJournalHdr A ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                    SQL.AppendLine("        Select AcNo, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then Abs(Amt1-Amt2) Else 0 End As DAmt, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then 0 Else Abs(Amt1-Amt2) End As CAmt ");
                    SQL.AppendLine("        From (");
                    SQL.AppendLine("            Select B.ParValue As AcNo, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<Concat(Left(@DocDt, 6), '01') ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt1, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt2 ");
                    SQL.AppendLine("            From TblVendorDepositSummary A ");
                    SQL.AppendLine("            Inner Join TblParameter B On B.ParCode='AcNoForForeignCurrencyExchangeGains' And B.ParValue Is Not Null ");
                    SQL.AppendLine("            Where A.CurCode Is Not Null ");
                    SQL.AppendLine("            And A.CurCode<>@MainCurCode ");
                    SQL.AppendLine("            And A.Amt<>0 ");
                    SQL.AppendLine("        ) T1 ");

                    SQL.AppendLine("        Union All ");

                    SQL.AppendLine("        Select AcNo, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then 0 Else Abs(Amt1-Amt2) End As DAmt, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then Abs(Amt1-Amt2) Else 0 End As CAmt ");
                    SQL.AppendLine("        From (");
                    SQL.AppendLine("            Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<Concat(Left(@DocDt, 6), '01') ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt1, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt2 ");
                    SQL.AppendLine("            From TblVendorDepositSummary A ");
                    SQL.AppendLine("            Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ");
                    SQL.AppendLine("            Where A.CurCode Is Not Null ");
                    SQL.AppendLine("            And A.CurCode<>@MainCurCode ");
                    SQL.AppendLine("            And A.Amt<>0 ");
                    SQL.AppendLine("        ) T2 ");

                    SQL.AppendLine("    ) Tbl ");
                    SQL.AppendLine("    Where AcNo Is Not Null ");
                    SQL.AppendLine("    Group By AcNo ");
                    SQL.AppendLine(") B On 0=0 ");
                    SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");


                    if (mIsForeignCurrencyExchangeUseExpense)
                    {
                        SQL.AppendLine("UPDATE TblJournalHdr A ");
                        SQL.AppendLine("INNER JOIN tbljournaldtl B ON A.DocNo = B.DocNo ");
                        SQL.AppendLine("SET A.JnDesc = ");
                        SQL.AppendLine("case ");
                        SQL.AppendLine("    when B.DAmt > B.CAmt then Concat('Foreign Currency Exchange Expense (Vendor Deposit) On ', @Mth, ' ', @Yr) ");
                        SQL.AppendLine("    else Concat('Foreign Currency Exchange Gains (Vendor Deposit) On ', @Mth, ' ', @Yr)");
                        SQL.AppendLine("End ");
                        SQL.AppendLine("Where A.DocNo=@JournalDocNo2 ");
                        SQL.AppendLine("And B.AcNo In ( ");
                        SQL.AppendLine("    Select ParValue From TblParameter ");
                        SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                        SQL.AppendLine("    And ParValue Is Not Null ");
                        SQL.AppendLine("); ");

                        SQL.AppendLine("Update TblJournalDtl A ");
                        SQL.AppendLine("Inner Join TblParameter B On B.Parcode = 'AcNoForForeignCurrencyExchangeExpense' and B.ParValue Is Not NULL ");
                        SQL.AppendLine("INNER JOIN TblParameter C ON C.Parcode = 'AcNoForForeignCurrencyExchangeGains' and C.ParValue Is Not NULL  ");
                        SQL.AppendLine("Set A.AcNo = ");
                        SQL.AppendLine("Case ");
                        SQL.AppendLine("    When A.DAmt > A.CAmt then B.ParValue ");
                        SQL.AppendLine("    else C.ParValue ");
                        SQL.AppendLine("End ");
                        SQL.AppendLine("Where A.DocNo=@JournalDocNo2 ");
                        SQL.AppendLine("And A.AcNo In ( ");
                        SQL.AppendLine("    Select ParValue From TblParameter ");
                        SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                        SQL.AppendLine("    And ParValue Is Not Null ");
                        SQL.AppendLine("); ");

                    }
                }

                #endregion

                #region Selisih Kurs Customer Deposit

                if (IsData3Existed)
                {
                    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@JournalDocNo3, @DocDt, Concat('Foreign Currency Exchange Gains (Customer Deposit) On ', @Mth, ' ', @Yr), ");
                    SQL.AppendLine("@MenuCode, Concat((Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ' (Customer Deposit Foreign Currency Exchange)' ), ");
                    SQL.AppendLine("@CCCode, @UserCode, CurrentDateTime()); ");

                    SQL.AppendLine("Set @Row:=0; ");

                    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                    SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
                    SQL.AppendLine("From TblJournalHdr A ");
                    SQL.AppendLine("Inner Join ( ");
                    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                    SQL.AppendLine("        Select AcNo, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then Abs(Amt1-Amt2) Else 0 End As DAmt, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then 0 Else Abs(Amt1-Amt2) End As CAmt ");
                    SQL.AppendLine("        From (");
                    SQL.AppendLine("            Select B.ParValue As AcNo, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<Concat(Left(@DocDt, 6), '01') ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt1, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt2 ");
                    SQL.AppendLine("            From TblCustomerDepositSummary A ");
                    SQL.AppendLine("            Inner Join TblParameter B On B.ParCode='AcNoForForeignCurrencyExchangeGains' And B.ParValue Is Not Null ");
                    SQL.AppendLine("            Where A.CurCode Is Not Null ");
                    SQL.AppendLine("            And A.CurCode<>@MainCurCode ");
                    SQL.AppendLine("            And A.Amt<>0 ");
                    SQL.AppendLine("        ) T1 ");

                    SQL.AppendLine("        Union All ");

                    SQL.AppendLine("        Select AcNo, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then 0 Else Abs(Amt1-Amt2) End As DAmt, ");
                    SQL.AppendLine("        Case When Amt1>Amt2 Then Abs(Amt1-Amt2) Else 0 End As CAmt ");
                    SQL.AppendLine("        From (");
                    SQL.AppendLine("            Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<Concat(Left(@DocDt, 6), '01') ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt1, ");
                    SQL.AppendLine("            IfNull(( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt ");
                    SQL.AppendLine("                And CurCode1=A.CurCode ");
                    SQL.AppendLine("                And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ), 0) * A.Amt As Amt2 ");
                    SQL.AppendLine("            From TblCustomerDepositSummary A ");
                    SQL.AppendLine("            Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                    SQL.AppendLine("            Where A.CurCode Is Not Null ");
                    SQL.AppendLine("            And A.CurCode<>@MainCurCode ");
                    SQL.AppendLine("            And A.Amt<>0 ");
                    SQL.AppendLine("        ) T2 ");

                    SQL.AppendLine("    ) Tbl ");
                    SQL.AppendLine("    Where AcNo Is Not Null ");
                    SQL.AppendLine("    Group By AcNo ");
                    SQL.AppendLine(") B On 0=0 ");
                    SQL.AppendLine("Where A.DocNo=@JournalDocNo3;");

                    if (mIsForeignCurrencyExchangeUseExpense)
                    {
                        SQL.AppendLine("UPDATE TblJournalHdr A ");
                        SQL.AppendLine("INNER JOIN tbljournaldtl B ON A.DocNo = B.DocNo ");
                        SQL.AppendLine("SET A.JnDesc = ");
                        SQL.AppendLine("case ");
                        SQL.AppendLine("    when B.DAmt > B.CAmt then Concat('Foreign Currency Exchange Expense (Customer Deposit) On ', @Mth, ' ', @Yr) ");
                        SQL.AppendLine("    else Concat('Foreign Currency Exchange Gains (Customer Deposit) On ', @Mth, ' ', @Yr)");
                        SQL.AppendLine("End ");
                        SQL.AppendLine("Where A.DocNo=@JournalDocNo3 ");
                        SQL.AppendLine("And B.AcNo In ( ");
                        SQL.AppendLine("    Select ParValue From TblParameter ");
                        SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                        SQL.AppendLine("    And ParValue Is Not Null ");
                        SQL.AppendLine("); ");

                        SQL.AppendLine("Update TblJournalDtl A ");
                        SQL.AppendLine("Inner Join TblParameter B On B.Parcode = 'AcNoForForeignCurrencyExchangeExpense' and B.ParValue Is Not NULL ");
                        SQL.AppendLine("INNER JOIN TblParameter C ON C.Parcode = 'AcNoForForeignCurrencyExchangeGains' and C.ParValue Is Not NULL  ");
                        SQL.AppendLine("Set A.AcNo = ");
                        SQL.AppendLine("Case ");
                        SQL.AppendLine("    When A.DAmt > A.CAmt then B.ParValue ");
                        SQL.AppendLine("    else C.ParValue ");
                        SQL.AppendLine("End ");
                        SQL.AppendLine("Where A.DocNo=@JournalDocNo3 ");
                        SQL.AppendLine("And A.AcNo In ( ");
                        SQL.AppendLine("    Select ParValue From TblParameter ");
                        SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                        SQL.AppendLine("    And ParValue Is Not Null ");
                        SQL.AppendLine("); ");

                    }
                }

                #endregion

                var Query = SQL.ToString();

                if (Query.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Info, "No unprocessing data.");
                    return;
                }

                cm.CommandText = Query;

                int i = 0;

                if (IsData1Existed)
                {
                    i += 1;
                    if (mJournalDocNoFormat == "1")
                        Sm.CmParam<String>(ref cm, "@JournalDocNo1", Sm.GenerateDocNoJournal(DocDt, i));
                    else
                    {
                        JNDocNo1 = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, i, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));
                        Sm.CmParam<String>(ref cm, "@JournalDocNo1", JNDocNo1);
                    }
                }

                if (IsData2Existed)
                {
                    i += 1;
                    if (mJournalDocNoFormat == "1")
                        Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(DocDt, i));
                    else
                    {
                        JNDocNo2 = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, i, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));
                        Sm.CmParam<String>(ref cm, "@JournalDocNo2", JNDocNo2);
                    }
                }

                if (IsData3Existed)
                {
                    i += 1;
                    if (mJournalDocNoFormat == "1")
                        Sm.CmParam<String>(ref cm, "@JournalDocNo3", Sm.GenerateDocNoJournal(DocDt, i));
                    else
                    {
                        JNDocNo3 = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, i, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));
                        Sm.CmParam<String>(ref cm, "@JournalDocNo3", JNDocNo3);
                    }
                }

                decimal Yr1 = Decimal.Parse(Yr) - 1;
                decimal Mth2 = Decimal.Parse(Mth) - 1;
                Sm.CmParamDt(ref cm, "@DocDt", DocDt);
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                Sm.CmParam<String>(ref cm, "@CCCode", ccCode);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@YrMth1", Yr + Mth);
                Sm.CmParam<String>(ref cm, "@YrMth2", (Mth == "01" ? Convert.ToString(Yr1) : Yr) + (Mth == "01" ? "12" : Sm.Right(string.Concat("00", Convert.ToString(Mth2)), 2)  ));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);

                var cml = new List<MySqlCommand>();
                cml.Add(cm);
                Sm.ExecCommands(cml);

                if (mJournalDocNoFormat == "2")
                {
                    ReGenerateJournalDocNo(JNDocNo1, JNDocNo2, JNDocNo3, Mth, Yr, profitCenterCode, DocDt);
                }

                Sm.StdMsg(mMsgType.Info, "Process is completed.");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void ReGenerateJournalDocNo(string JNDocNo1, string JNDocNo2, string JNDocNo3, string Mth, string Yr, string profitCenterCode, string DocDt)
        {
            string AcNoForForeignCurrencyExchangeExpense = Sm.GetParameter("AcNoForForeignCurrencyExchangeExpense");
            string AcNoForForeignCurrencyExchangeGains = Sm.GetParameter("AcNoForForeignCurrencyExchangeGains");

            if (JNDocNo1.Length > 0)
            {
                string code1 = ReGenerateCode1(JNDocNo1, AcNoForForeignCurrencyExchangeExpense, AcNoForForeignCurrencyExchangeGains);                

                if (code1 != "M")
                {
                    string JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));

                    UpdateClosingBalanceJournalDocNo(JNDocNo, JNDocNo1, Mth, Yr);
                }
            }

            if (JNDocNo2.Length > 0)
            {
                string code1 = ReGenerateCode1(JNDocNo2, AcNoForForeignCurrencyExchangeExpense, AcNoForForeignCurrencyExchangeGains);

                if (code1 != "M")
                {
                    string JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));

                    UpdateOldJournalDocNo(JNDocNo, JNDocNo2);
                }
            }

            if (JNDocNo3.Length > 0)
            {
                string code1 = ReGenerateCode1(JNDocNo3, AcNoForForeignCurrencyExchangeExpense, AcNoForForeignCurrencyExchangeGains);

                if (code1 != "M")
                {
                    string JNDocNo = Sm.GetValue(Sm.GetNewJournalDocNoWithAddCodes(DocDt, 1, code1, profitCenterCode, string.Empty, string.Empty, string.Empty));

                    UpdateOldJournalDocNo(JNDocNo, JNDocNo3);
                }
            }
        }

        private void UpdateOldJournalDocNo(string JNDocNo, string JNDocNoOld)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var cml = new List<MySqlCommand>();

            SQL.AppendLine("Update TblJournalHdr Set ");
            SQL.AppendLine("    DocNo = @JNDocNo ");
            SQL.AppendLine("Where DocNo = @JNDocNoOld; ");

            SQL.AppendLine("Update TblJournalDtl Set ");
            SQL.AppendLine("    DocNo = @JNDocNo ");
            SQL.AppendLine("Where DocNo = @JNDocNoOld; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@JNDocNo", JNDocNo);
            Sm.CmParam<String>(ref cm, "@JNDocNoOld", JNDocNoOld);

            cml.Add(cm);

            Sm.ExecCommands(cml);
        }

        private string ReGenerateCode1(string JNDocNo, string AcNoForForeignCurrencyExchangeExpense, string AcNoForForeignCurrencyExchangeGains)
        {
            string code1 = "M";
            bool IsExpenseExists = IsAcNoExists(AcNoForForeignCurrencyExchangeExpense, JNDocNo);
            bool IsGainsExists = IsAcNoExists(AcNoForForeignCurrencyExchangeGains, JNDocNo);

            if (IsExpenseExists) code1 = "BK";
            if (IsGainsExists) code1 = "BM";

            return code1;
        }

        private void UpdateClosingBalanceJournalDocNo(string JNDocNo, string JNDocNoOld, string Mth, string Yr)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var cml = new List<MySqlCommand>();

            SQL.AppendLine("Update TblClosingBalanceInCashHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JNDocNo ");
            SQL.AppendLine("Where JournalDocNo Is Not Null ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("And JournalDocNo = @JNDocNoOld; ");

            SQL.AppendLine("Update TblJournalHdr Set ");
            SQL.AppendLine("    DocNo = @JNDocNo ");
            SQL.AppendLine("Where DocNo = @JNDocNoOld; ");

            SQL.AppendLine("Update TblJournalDtl Set ");
            SQL.AppendLine("    DocNo = @JNDocNo ");
            SQL.AppendLine("Where DocNo = @JNDocNoOld; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@JNDocNo", JNDocNo);
            Sm.CmParam<String>(ref cm, "@JNDocNoOld", JNDocNoOld);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);

            cml.Add(cm);

            Sm.ExecCommands(cml);
        }

        private bool IsAcNoExists(string AcNo, string JNDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo = @Param1 ");
            SQL.AppendLine("And AcNo = @Param2; ");

            return Sm.IsDataExist(SQL.ToString(), JNDocNo, AcNo, string.Empty);
        }

        #endregion

        #endregion

        #region Event

        private void FrmAAD_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            Process();
        }

        #endregion
    }
}
