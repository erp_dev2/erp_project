﻿namespace RunSystem
{
    partial class FrmNewsFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.TxtNewsCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkNewsCode = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkNewsTitle = new DevExpress.XtraEditors.CheckEdit();
            this.TxtNewsTitle = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewsTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsTitle.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtNewsTitle);
            this.panel2.Controls.Add(this.ChkNewsTitle);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.ChkNewsCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtNewsCode);
            this.panel2.Size = new System.Drawing.Size(672, 53);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 420);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 14);
            this.label6.TabIndex = 9;
            this.label6.Text = "News Code";
            // 
            // TxtNewsCode
            // 
            this.TxtNewsCode.EnterMoveNextControl = true;
            this.TxtNewsCode.Location = new System.Drawing.Point(75, 4);
            this.TxtNewsCode.Name = "TxtNewsCode";
            this.TxtNewsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNewsCode.Properties.Appearance.Options.UseFont = true;
            this.TxtNewsCode.Properties.MaxLength = 30;
            this.TxtNewsCode.Size = new System.Drawing.Size(219, 20);
            this.TxtNewsCode.TabIndex = 10;
            this.TxtNewsCode.Validated += new System.EventHandler(this.TxtNewsCode_Validated);
            // 
            // ChkNewsCode
            // 
            this.ChkNewsCode.Location = new System.Drawing.Point(300, 3);
            this.ChkNewsCode.Name = "ChkNewsCode";
            this.ChkNewsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkNewsCode.Properties.Appearance.Options.UseFont = true;
            this.ChkNewsCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkNewsCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkNewsCode.Properties.Caption = " ";
            this.ChkNewsCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkNewsCode.Size = new System.Drawing.Size(19, 22);
            this.ChkNewsCode.TabIndex = 11;
            this.ChkNewsCode.ToolTip = "Remove filter";
            this.ChkNewsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkNewsCode.ToolTipTitle = "Run System";
            this.ChkNewsCode.CheckedChanged += new System.EventHandler(this.ChkNewsCode_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "News Title";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkNewsTitle
            // 
            this.ChkNewsTitle.Location = new System.Drawing.Point(300, 23);
            this.ChkNewsTitle.Name = "ChkNewsTitle";
            this.ChkNewsTitle.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkNewsTitle.Properties.Appearance.Options.UseFont = true;
            this.ChkNewsTitle.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkNewsTitle.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkNewsTitle.Properties.Caption = " ";
            this.ChkNewsTitle.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkNewsTitle.Size = new System.Drawing.Size(19, 22);
            this.ChkNewsTitle.TabIndex = 14;
            this.ChkNewsTitle.ToolTip = "Remove filter";
            this.ChkNewsTitle.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkNewsTitle.ToolTipTitle = "Run System";
            this.ChkNewsTitle.CheckedChanged += new System.EventHandler(this.ChkNewsTitle_CheckedChanged);
            // 
            // TxtNewsTitle
            // 
            this.TxtNewsTitle.EnterMoveNextControl = true;
            this.TxtNewsTitle.Location = new System.Drawing.Point(75, 24);
            this.TxtNewsTitle.Name = "TxtNewsTitle";
            this.TxtNewsTitle.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNewsTitle.Properties.Appearance.Options.UseFont = true;
            this.TxtNewsTitle.Properties.MaxLength = 30;
            this.TxtNewsTitle.Size = new System.Drawing.Size(219, 20);
            this.TxtNewsTitle.TabIndex = 13;
            this.TxtNewsTitle.Validated += new System.EventHandler(this.TxtNewsTitle_Validated);
            // 
            // FrmNewsFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmNewsFind";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkNewsTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewsTitle.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit TxtNewsCode;
        private DevExpress.XtraEditors.CheckEdit ChkNewsCode;
        private DevExpress.XtraEditors.CheckEdit ChkNewsTitle;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtNewsTitle;
    }
}