﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLegalDocVerifyUpdateFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmLegalDocVerifyUpdate mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmLegalDocVerifyUpdateFind(FrmLegalDocVerifyUpdate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LegalDocVerifyDocNo, A.QueueNo, A.VehicleRegNo, C.VdName, ");
            SQL.AppendLine("Case When A.VdCode1 is null Then D.VdName ");
            SQL.AppendLine("When A.VdCode2 is null Then F.Vdname ");
            SQL.AppendLine("When (A.VdCode1 is not null And A.VdCode2 is not null) then Concat(D.Vdname, ', ', F.Vdname) ");
            SQL.AppendLine("End  As VdName2, E.DLName, B.DLDocNo, B.DLDocDt, B.Volume, B.Qty,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  ");
            SQL.AppendLine("From TblLegalDocVerifyUpdateHdr A  ");
            SQL.AppendLine("Inner Join TblLegalDocVerifyUpdateDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Left Join TblVendor C On A.VdCode=C.VdCode  ");
            SQL.AppendLine("Left Join TblVendor D On A.VdCode2=D.VdCode  ");
            SQL.AppendLine("Left Join TblDocLegality E On B.DLCode=E.DLCode ");
            SQL.AppendLine("Left Join TblVendor F On A.VdCode1=F.VdCode  ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Dokumen#", 
                        "Tanggal",
                        "Verifikasi#",
                        "Antrian#",
                        "Nomor"+Environment.NewLine+"Polisi",
                        
                        //6-10
                        "Vendor",
                        "Pemilik"+Environment.NewLine+"Legalitas",
                        "Dokumen",
                        "Dokumen#",
                        "Tgl Dokumen",

                        //11-15
                        "Volume",
                        "Jumlah",
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        
                        //16-18
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 130, 80, 80,  
                        
                        //6-10
                        200, 200, 250, 150, 80, 
                        
                        //11-15
                        80, 80, 100, 100, 100, 
                        
                        //16-18
                        100, 100, 100
                    }
                );
            
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 10, 14, 17 });
            Sm.GrdFormatTime(Grd1, new int[] { 15, 18 });
            Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16, 17, 18 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand(); 

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtQueueNo.Text, new string[] { "A.QueueNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtVehicleRegNo.Text, new string[] { "A.VehicleRegNo" });


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LegalDocVerifyDocNo", "QueueNo", "VehicleRegNo", "VdName", 
                            
                            //6-10
                            "VdName2", "DLName", "DLDocNo", "DLDocDt", "Volume", 
                            
                            //11-15
                            "Qty", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Additional Method

        private void SetLueVdCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct VdCode As Col1, VdName As Col2 ");
            SQL.AppendLine("From TblVendor ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And VdCtCode=(Select ParValue From TblParameter Where ParCode='VendorRMP') ");
            SQL.AppendLine("Order By VdName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueDLCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct DLCode As Col1, DLName As Col2 ");
            SQL.AppendLine("From TblDocLegality Order By DLName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Dokumen#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Tanggal");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkVehicleRegNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Nomor Polisi");
        }

        private void ChkQueueNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Antrian#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtVehicleRegNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtQueueNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
