﻿#region Update
/*
    06/01/2020 [TKG/SIER] tambah panjang, lebar, tinggi, berat. tampilkan lot/bin. sembungikan qty 2 dan qty 3.
    07/01/2020 [DITA/SIER] Tambah parameter IsFilterByItCt
    25/06/2020 [DITA/SIER] Tambah format num untuk net weight, gross weight, length, width, height saat choose data
    24/07/2020 [VIN/SRN] menampilkan  item berdasarkan vendor dengan parameter mStockInboundFormat
    28/07/2020 [WED/SIER] tambah THCAL Weight
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmStockInboundDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmStockInbound mFrmParent;
        internal bool
           mIsFilterByItCt = false;

        #endregion

        #region Constructor

        public FrmStockInboundDlg(FrmStockInbound FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "Item's Name", 
                        "Category",
                        "UoM",

                        //6-10
                        "UoM",
                        "UoM",
                        "Length",
                        "UoM",
                        "Width",

                        //11-15
                        "UoM",
                        "Height",
                        "UoM",
                        "Weight",
                        "UoM",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 250, 200, 0,
                        
                        //6-10
                        0, 0, 80, 80, 80,

                        //11-15
                        80, 80, 80, 80, 80
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 12, 14 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 7 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                SQL.AppendLine("Select  A.ItCode, A.ItName, B.ItCtName, A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, ");
                SQL.AppendLine("A.Length, A.LengthUomCode, A.Width, A.WidthUomCode, A.Height, A.HeightUomCode, A.Weight, A.WeightUomCode " );
                SQL.AppendLine("From TblItem A " );
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode " );

                SQL.AppendLine("Where A.ActInd='Y'  ");
                if (mFrmParent.mStockInboundFormat == "2")
                    SQL.AppendLine("And (A.VdCode Is Null Or (A.VdCode Is Not Null And A.VdCode=@VdCode)) ");

                 if (mIsFilterByItCt)
                 {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                 }
           

                var cm = new MySqlCommand();
                


                string Filter = " And 0=0 ", Filter2 = string.Empty;
                if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                if (mFrmParent.mStockInboundFormat == "2")
                    Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(mFrmParent.LueCtCode));

                if (ChkUnselectedItem.Checked)
                {
                    if (mFrmParent.Grd1.Rows.Count >= 1)
                    {
                        var No = "0001";
                        for (int Row = 0; Row < mFrmParent.Grd1.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(mFrmParent.Grd1, Row, 4).Length != 0)
                            {
                                if (Filter2.Length > 0) Filter2 += " And ";
                                Filter2 += "(A.ItCode<>@ItCode" + No.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ItCode" + No.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, Row, 4));
                                No = ("000" + (int.Parse(No) + 1).ToString()).ToString();
                            }
                        }
                    }
                    if (Filter2.Length != 0) Filter += " And (" + Filter2 + ")";
                }

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString()+ Filter +" Order By A.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode", 

                            //1-5
                            "ItName", "ItCtName", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3",

                            //6-10
                            "Length", "LengthUomCode", "Width", "WidthUomCode", "Height", 
                            
                            //11-13
                            "HeightUomCode", "Weight", "WeightUomCode" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 15);

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 11, 13, 15, 21, 23, 25, 27, 36 });
                        
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 11, 13, 15, 21, 23, 25, 27, 29, 30, 31, 32, 33, 34, 35, 36 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 2).Length>0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for(int Row = 0;Row<Grd1.Rows.Count;Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
