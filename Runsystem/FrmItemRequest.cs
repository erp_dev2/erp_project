﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemRequest : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmItemRequestFind FrmFind;
        private bool mIsItGrpCodeMandatoryInMasterItem = false;
        internal bool mIsFilterByItCt = false;
        private string mItCodeInternalNotEmptyInd = string.Empty;

        #endregion

        #region Constructor

        public FrmItemRequest(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Item Request";

            try
            {
                GetParameter();

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);

                Sl.SetLueItBrCode(ref LueItBrCode);

                if (mIsItGrpCodeMandatoryInMasterItem)
                    LblItGrpCode.ForeColor = Color.Red;

                SetItCodeInternalNotEmptyInd();

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtItName, TxtForeignName, TxtItCodeInternal, LueItCtCode, LueItScCode, 
                        LueItBrCode, LueItGrpCode, MeeSpecification, MeeRemark, ChkInventoryItemInd, ChkCancelInd,
                        ChkSalesItemInd, ChkPurchaseItemInd, ChkFixedItemInd, ChkPlanningItemInd, ChkTaxLiableInd,
                        MeeCancelReason
                    }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtItName, TxtForeignName, TxtItCodeInternal, LueItCtCode,  
                        LueItBrCode, LueItGrpCode, MeeSpecification, MeeRemark, ChkInventoryItemInd, ChkSalesItemInd, ChkPurchaseItemInd, ChkFixedItemInd, ChkPlanningItemInd, ChkTaxLiableInd,
                    }, false);
                    TxtItName.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, MeeCancelReason
                    }, false);
                    MeeCancelReason.Focus();
                    break;
                default:
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtItName, TxtForeignName, TxtItCodeInternal, LueItCtCode, 
                LueItScCode, LueItBrCode, LueItGrpCode, MeeSpecification, MeeRemark, MeeCancelReason
            });

            ChkCancelInd.Checked = ChkInventoryItemInd.Checked = ChkSalesItemInd.Checked =
            ChkPurchaseItemInd.Checked = ChkFixedItemInd.Checked = ChkPlanningItemInd.Checked =
            ChkTaxLiableInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemRequestFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);

                ChkPurchaseItemInd.Checked =
                ChkInventoryItemInd.Checked = true;

                SetLueItCtCode(ref LueItCtCode, string.Empty);
                SetLueItGrpCode(ref LueItGrpCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertItemRequest(sender, e);
                else
                    EditItemRequest();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItName, "Item name", false) ||
                IsItNameExisted() ||
                IsItCodeInternalEmpty() ||
                Sm.IsLueEmpty(LueItCtCode, "Item's category") ||
                (mIsItGrpCodeMandatoryInMasterItem && Sm.IsLueEmpty(LueItGrpCode, "Item's group"));
        }

        private bool IsItNameExisted()
        {
            var cm = new MySqlCommand() { CommandText = "Select DocNo From TblItemRequest Where ItName=@ItName And DocNo<>@DocNo Limit 1;" };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text.Length == 0 ? "XXX" : TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItName", TxtItName.Text);

            string DocNo = Sm.GetValue(cm);
            if (DocNo.Length == 0)
                return false;
            else
            {
                return Sm.StdMsgYN("Question",
                    "Document# : " + DocNo + Environment.NewLine +
                    "Item Name : " + TxtItName.Text + Environment.NewLine + Environment.NewLine +
                    "Existing document With the same name." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
        }

        private bool IsItCodeInternalEmpty()
        {
            if (mItCodeInternalNotEmptyInd != "Y") return false;

            if (TxtItCodeInternal.Text.Length == 0)
            {
                return Sm.StdMsgYN("Question",
                    "Item Name : " + TxtItName.Text + Environment.NewLine + Environment.NewLine +
                    "Item local code is empty." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
            return false;
        }

        private void InsertItemRequest(object sender, EventArgs e)
        {
            if (IsDataNotValid()) return;
            if (Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemRequest", "TblItemRequest");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveItemRequest(DocNo));

            Sm.ExecCommands(cml);
            BtnInsertClick(sender, e);
        }

        private MySqlCommand SaveItemRequest(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItemRequest(DocNo, DocDt, CancelInd, ItName, ForeignName, ItCodeInternal, ItCtCode, ItScCode, ");
            SQL.AppendLine("    ItBrCode, ItGrpCode, Specification, Remark, ");
            SQL.AppendLine("    InventoryItemInd, SalesItemInd, PurchaseItemInd, FixedItemInd, PlanningItemInd, TaxLiableInd, ");
            SQL.AppendLine("    CreateBy, CreateDt)");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @ItName, @ForeignName, @ItCodeInternal, @ItCtCode, @ItScCode, ");
            SQL.AppendLine("    @ItBrCode, @ItGrpCode, @Specification, @Remark, ");
            SQL.AppendLine("    @InventoryItemInd, @SalesItemInd, @PurchaseItemInd, @FixedItemInd, @PlanningItemInd, @TaxLiableInd, ");
            SQL.AppendLine("    @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ItName", TxtItName.Text);
            Sm.CmParam<String>(ref cm, "@ForeignName", TxtForeignName.Text);
            Sm.CmParam<String>(ref cm, "@ItCodeInternal", TxtItCodeInternal.Text);
            Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
            Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetLue(LueItScCode));
            Sm.CmParam<String>(ref cm, "@ItBrCode", Sm.GetLue(LueItBrCode));
            Sm.CmParam<String>(ref cm, "@ItGrpCode", Sm.GetLue(LueItGrpCode));
            Sm.CmParam<String>(ref cm, "@Specification", MeeSpecification.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@InventoryItemInd", ChkInventoryItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SalesItemInd", ChkSalesItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PurchaseItemInd", ChkPurchaseItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@FixedItemInd", ChkFixedItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PlanningItemInd", ChkPlanningItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxLiableInd", ChkTaxLiableInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditItemRequest()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelItemRequest());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsItemRequestNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsItemRequestNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblItemRequest ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select ItCode From TblItem Where ActInd='Y' And ItemRequestDocNo=@Param;",
                TxtDocNo.Text,
                "Data already processed into Master Item.");
        }

        private MySqlCommand CancelItemRequest()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblItemRequest Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowItemRequest(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowItemRequest(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From TblItemRequest T Where T.DocNo=@DocNo ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "ItName", "ForeignName", "ItCtCode", "ItScCode",
                        
                        //6-10
                        "ItBrCode", "Specification", "Remark", "CancelInd", "CancelReason", 
                        
                        //11-15
                        "InventoryItemInd", "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd",
                        
                        //16-18
                        "TaxLiableInd", "ItCodeInternal", "ItGrpCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {                        
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[10]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[9]), "Y");
                        TxtItName.EditValue = Sm.DrStr(dr, c[2]);
                        TxtForeignName.EditValue = Sm.DrStr(dr, c[3]);
                        TxtItCodeInternal.EditValue = Sm.DrStr(dr, c[17]);
                        
                        Sl.SetLueItCtCode(ref LueItCtCode, Sm.DrStr(dr, c[4]));
                        SetLueItScCode(ref LueItScCode, Sm.GetLue(LueItCtCode));
                        Sm.SetLue(LueItScCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueItBrCode, Sm.DrStr(dr, c[6]));
                        MeeSpecification.EditValue = Sm.DrStr(dr, c[7]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[8]);
                        ChkInventoryItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[11]), "Y");
                        ChkSalesItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[12]), "Y");
                        ChkPurchaseItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[13]), "Y");
                        ChkFixedItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[14]), "Y");
                        ChkPlanningItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[15]), "Y");
                        ChkTaxLiableInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[16]), "Y");
                        SetLueItGrpCode(ref LueItGrpCode, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueItGrpCode, Sm.DrStr(dr, c[18]));
                    }, true
                );
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameter("IsFilterByItCt") == "Y";
            mIsItGrpCodeMandatoryInMasterItem = Sm.GetParameter("IsItGrpCodeMandatoryInMasterItem") == "Y";
        }

        #region Set Lue

        private void SetLueItGrpCode(ref LookUpEdit Lue, string ItGrpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("Select ItGrpCode As Col1, Concat(ItGrpName, ' (', ItGrpCode, ')') As Col2 From TblItemGroup Where ActInd = 'Y'  ");
            if (ItGrpCode.Length != 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select ItGrpCode As Col1, Concat(ItGrpName, ' (', ItGrpCode, ')') As Col2 From TblItemGroup Where ItGrpCode = '" + ItGrpCode + "' ");
            }
            SQL.AppendLine(") Tbl Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItScCode(ref LookUpEdit Lue, string ItCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
            SQL.AppendLine("From TblItemSubCategory ");
            SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "' Order By ItScName ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItCtCode As Col1, T.ItCtName As Col2 From TblItemCategory T Where T.ActInd = 'Y' ");
            if (Code.Length > 0)
                SQL.AppendLine("And T.ItCtCode=@Code;");
            else
            {
                if (mIsFilterByItCt)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select ItCtCode From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Order By T.ItCtName;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        private void SetItCodeInternalNotEmptyInd()
        {
            mItCodeInternalNotEmptyInd = Sm.GetParameter("ItCodeInternalNotEmptyInd");
        }

        private void ShowDataInCtrl(ref MySqlCommand cm, string SQL, string[] ColumnTitle, RefreshData rd, bool ShowNoDataInd)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd)
                    {
                        if (mDocNo.Length == 0)
                            Sm.StdMsg(mMsgType.NoData, string.Empty);
                        else
                            Sm.StdMsg(mMsgType.Warning, "No authorized to see this information.");
                    }
                }
                else
                {
                    while (dr.Read()) rd(dr, c);
                }
                dr.Close();
                dr.Dispose();
                cm.Dispose();
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItName);
        }

        private void TxtItCodeInternal_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItCodeInternal);
        }

        private void TxtForeignName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtForeignName);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(SetLueItCtCode), string.Empty);
            var ItCtCode = Sm.GetLue(LueItCtCode);
            LueItScCode.EditValue = null;
            if (ItCtCode.Length != 0) SetLueItScCode(ref LueItScCode, ItCtCode);
            Sm.SetControlReadOnly(LueItScCode, ItCtCode.Length == 0);
        }

        private void LueItScCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItScCode, new Sm.RefreshLue2(SetLueItScCode), Sm.GetLue(LueItCtCode));
        }

        private void LueItBrCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItBrCode, new Sm.RefreshLue1(Sl.SetLueItBrCode));
        }

        private void LueItGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItGrpCode, new Sm.RefreshLue2(SetLueItGrpCode), string.Empty);
        }

        #endregion

        #endregion

    }
}
