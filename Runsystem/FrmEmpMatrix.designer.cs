﻿namespace RunSystem
{
    partial class FrmEmpMatrix2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnLL = new System.Windows.Forms.Button();
            this.BtnLM = new System.Windows.Forms.Button();
            this.BtnLH = new System.Windows.Forms.Button();
            this.BtnMH = new System.Windows.Forms.Button();
            this.BtnMM = new System.Windows.Forms.Button();
            this.BtnHH = new System.Windows.Forms.Button();
            this.BtnML = new System.Windows.Forms.Button();
            this.BtnHM = new System.Windows.Forms.Button();
            this.BtnHL = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(742, 0);
            this.panel1.Size = new System.Drawing.Size(70, 600);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAdd.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAdd.Appearance.Options.UseBackColor = true;
            this.BtnAdd.Appearance.Options.UseFont = true;
            this.BtnAdd.Appearance.Options.UseForeColor = true;
            this.BtnAdd.Appearance.Options.UseTextOptions = true;
            this.BtnAdd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.BtnLL);
            this.panel2.Controls.Add(this.BtnLM);
            this.panel2.Controls.Add(this.BtnLH);
            this.panel2.Controls.Add(this.BtnMH);
            this.panel2.Controls.Add(this.BtnMM);
            this.panel2.Controls.Add(this.BtnHH);
            this.panel2.Controls.Add(this.BtnML);
            this.panel2.Controls.Add(this.BtnHM);
            this.panel2.Controls.Add(this.BtnHL);
            this.panel2.Size = new System.Drawing.Size(742, 600);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Goldenrod;
            this.label1.Location = new System.Drawing.Point(137, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 28);
            this.label1.TabIndex = 29;
            this.label1.Text = "High Perfomance \r\nLow Competence";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label11.Location = new System.Drawing.Point(23, 260);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 17);
            this.label11.TabIndex = 39;
            this.label11.Text = "Performance";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label10.Location = new System.Drawing.Point(321, 563);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 17);
            this.label10.TabIndex = 38;
            this.label10.Text = "Competence + Potency";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Yellow;
            this.label9.Location = new System.Drawing.Point(494, 388);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 28);
            this.label9.TabIndex = 37;
            this.label9.Text = "Low Perfomance \r\nHigh Competence";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Goldenrod;
            this.label8.Location = new System.Drawing.Point(316, 388);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 28);
            this.label8.TabIndex = 36;
            this.label8.Text = "Low Perfomance \r\nMedium Competence";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(494, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 28);
            this.label7.TabIndex = 35;
            this.label7.Text = "Medium Perfomance \r\nHigh Competence";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Goldenrod;
            this.label6.Location = new System.Drawing.Point(137, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 28);
            this.label6.TabIndex = 34;
            this.label6.Text = "Medium Perfomance \r\nLow Competence";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(137, 388);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 28);
            this.label5.TabIndex = 33;
            this.label5.Text = "Low Perfomance \r\nLow Competence";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.RoyalBlue;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(494, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 28);
            this.label4.TabIndex = 32;
            this.label4.Text = "High Perfomance \r\nHigh Competence";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(316, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 28);
            this.label3.TabIndex = 31;
            this.label3.Text = "High Perfomance \r\nMedium Competence";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(316, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 28);
            this.label2.TabIndex = 30;
            this.label2.Text = "Medium Perfomance \r\nMedium Competence";
            // 
            // BtnLL
            // 
            this.BtnLL.BackColor = System.Drawing.Color.Red;
            this.BtnLL.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnLL.Location = new System.Drawing.Point(127, 375);
            this.BtnLL.Name = "BtnLL";
            this.BtnLL.Size = new System.Drawing.Size(180, 180);
            this.BtnLL.TabIndex = 28;
            this.BtnLL.Text = "9";
            this.BtnLL.UseVisualStyleBackColor = false;
            // 
            // BtnLM
            // 
            this.BtnLM.BackColor = System.Drawing.Color.Goldenrod;
            this.BtnLM.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnLM.Location = new System.Drawing.Point(306, 375);
            this.BtnLM.Name = "BtnLM";
            this.BtnLM.Size = new System.Drawing.Size(180, 180);
            this.BtnLM.TabIndex = 27;
            this.BtnLM.Text = "220";
            this.BtnLM.UseVisualStyleBackColor = false;
            // 
            // BtnLH
            // 
            this.BtnLH.BackColor = System.Drawing.Color.Yellow;
            this.BtnLH.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnLH.Location = new System.Drawing.Point(485, 375);
            this.BtnLH.Name = "BtnLH";
            this.BtnLH.Size = new System.Drawing.Size(180, 180);
            this.BtnLH.TabIndex = 26;
            this.BtnLH.Text = "7";
            this.BtnLH.UseVisualStyleBackColor = false;
            // 
            // BtnMH
            // 
            this.BtnMH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnMH.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnMH.Location = new System.Drawing.Point(485, 196);
            this.BtnMH.Name = "BtnMH";
            this.BtnMH.Size = new System.Drawing.Size(180, 180);
            this.BtnMH.TabIndex = 25;
            this.BtnMH.Text = "6";
            this.BtnMH.UseVisualStyleBackColor = false;
            // 
            // BtnMM
            // 
            this.BtnMM.BackColor = System.Drawing.Color.Yellow;
            this.BtnMM.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnMM.Location = new System.Drawing.Point(306, 196);
            this.BtnMM.Name = "BtnMM";
            this.BtnMM.Size = new System.Drawing.Size(180, 180);
            this.BtnMM.TabIndex = 24;
            this.BtnMM.Text = "120";
            this.BtnMM.UseVisualStyleBackColor = false;
            // 
            // BtnHH
            // 
            this.BtnHH.BackColor = System.Drawing.Color.RoyalBlue;
            this.BtnHH.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnHH.Location = new System.Drawing.Point(485, 16);
            this.BtnHH.Name = "BtnHH";
            this.BtnHH.Size = new System.Drawing.Size(180, 180);
            this.BtnHH.TabIndex = 23;
            this.BtnHH.Text = "4";
            this.BtnHH.UseVisualStyleBackColor = false;
            // 
            // BtnML
            // 
            this.BtnML.BackColor = System.Drawing.Color.Goldenrod;
            this.BtnML.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnML.Location = new System.Drawing.Point(127, 196);
            this.BtnML.Name = "BtnML";
            this.BtnML.Size = new System.Drawing.Size(180, 180);
            this.BtnML.TabIndex = 22;
            this.BtnML.Text = "3";
            this.BtnML.UseVisualStyleBackColor = false;
            // 
            // BtnHM
            // 
            this.BtnHM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnHM.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnHM.Location = new System.Drawing.Point(306, 16);
            this.BtnHM.Name = "BtnHM";
            this.BtnHM.Size = new System.Drawing.Size(180, 180);
            this.BtnHM.TabIndex = 21;
            this.BtnHM.Text = "2";
            this.BtnHM.UseVisualStyleBackColor = false;
            // 
            // BtnHL
            // 
            this.BtnHL.BackColor = System.Drawing.Color.Goldenrod;
            this.BtnHL.Font = new System.Drawing.Font("Tahoma", 40F);
            this.BtnHL.Location = new System.Drawing.Point(127, 16);
            this.BtnHL.Name = "BtnHL";
            this.BtnHL.Size = new System.Drawing.Size(180, 180);
            this.BtnHL.TabIndex = 20;
            this.BtnHL.Text = "90";
            this.BtnHL.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Goldenrod;
            this.label12.Location = new System.Drawing.Point(179, 146);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 14);
            this.label12.TabIndex = 40;
            this.label12.Text = "Contributor";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label13.Location = new System.Drawing.Point(355, 146);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 14);
            this.label13.TabIndex = 41;
            this.label13.Text = "Promotable";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.RoyalBlue;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(558, 146);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 14);
            this.label14.TabIndex = 42;
            this.label14.Text = "Star";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Goldenrod;
            this.label15.Location = new System.Drawing.Point(175, 329);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 14);
            this.label15.TabIndex = 43;
            this.label15.Text = "Contributor";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Yellow;
            this.label16.Location = new System.Drawing.Point(357, 329);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 14);
            this.label16.TabIndex = 44;
            this.label16.Text = "Development";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label17.Location = new System.Drawing.Point(540, 329);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 14);
            this.label17.TabIndex = 45;
            this.label17.Text = "Promotable";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(191, 504);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 14);
            this.label18.TabIndex = 46;
            this.label18.Text = "MissFit";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Goldenrod;
            this.label19.Location = new System.Drawing.Point(367, 504);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 14);
            this.label19.TabIndex = 47;
            this.label19.Text = "Concern";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Yellow;
            this.label20.Location = new System.Drawing.Point(532, 504);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 14);
            this.label20.TabIndex = 48;
            this.label20.Text = "Development";
            // 
            // FrmEmpMatrix2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 600);
            this.Name = "FrmEmpMatrix2";
            this.Text = "Talent Mapping Matrix";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnLL;
        private System.Windows.Forms.Button BtnLM;
        public System.Windows.Forms.Button BtnLH;
        private System.Windows.Forms.Button BtnMH;
        private System.Windows.Forms.Button BtnMM;
        private System.Windows.Forms.Button BtnHH;
        private System.Windows.Forms.Button BtnML;
        private System.Windows.Forms.Button BtnHM;
        private System.Windows.Forms.Button BtnHL;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
    }
}