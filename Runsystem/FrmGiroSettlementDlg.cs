﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGiroSettlementDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmGiroSettlement mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGiroSettlementDlg(FrmGiroSettlement FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueBankCode(ref LueBankCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Business Partner Type",
                    "Business Partner Code",
                    "Business Partner Name", 
                    "Bank code",
                    "Bank name",

                    //6-8
                    "Giro#",
                    "Currency",
                    "Amount"
                }, new int[]
                {
                    //0
                    60,

                    //1-5
                    0, 120, 200, 100, 200, 

                    //6-8
                    100, 80, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.BusinessPartnerType, T1.BusinessPartnerCode, ");
            SQL.AppendLine("If(T1.BusinessPartnerType = '1', T2.VdName, T3.CtName) As BusinessPartnerName, T1.BankCode, T4.BankName, ");
            SQL.AppendLine("T1.GiroNo, T1.CurCode, T1.Amt ");
            SQL.AppendLine("From TblGiroSummary T1 ");
            SQL.AppendLine("Left Join TblVendor T2 On T1.BusinessPartnerCode = T2.VdCode ");
            SQL.AppendLine("Left Join TblCustomer T3 On T1.BusinessPartnerCode = T3.CtCode ");
            SQL.AppendLine("Inner Join TblBank T4 On T1.BankCode = T4.BankCode ");
            SQL.AppendLine("Where T1.ActInd = 'Y' ");
            SQL.AppendLine("And Concat(T1.BusinessPartnerCode, T1.BusinessPartnerType, T1.GiroNo, T1.BankCode) Not In ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select Concat(A.BusinessPartnerCode, A.BusinessPartnerType, A.GiroNo, A.BankCode) ");
	        SQL.AppendLine("    From tblgirosummary A ");
	        SQL.AppendLine("    Inner Join TblGiroMovement B On ");
		    SQL.AppendLine("        A.GiroNo = B.GiroNo ");
		    SQL.AppendLine("        And A.BusinessPartnerCode=B.BusinessPartnerCode ");
		    SQL.AppendLine("        And A.BusinessPartnerType=B.BusinessPartnerType ");
		    SQL.AppendLine("        And A.BankCode=B.BankCode ");
		    SQL.AppendLine("        And B.DocType='01' ");
	        SQL.AppendLine("    Inner Join tblvoucherhdr C On B.DocNo = C.DocNo ");
	        SQL.AppendLine("    Inner Join tblapdownpayment D On C.VoucherRequestDocNo=D.VoucherRequestDocNo ");
	        SQL.AppendLine("    Inner Join tblrecvvddtl E On D.PODocNo = E.DocNo And E.CancelInd = 'N' And E.Status In ('O', 'A') ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtBusinessPartnerCode.Text, new string[] { "T1.BusinessPartnerCode", "T2.VdName", "T3.CtName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBankCode), "T1.BankCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtGiroNo.Text, "T1.GiroNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T2.VdName, T3.CtName, T4.BankName, T1.GiroNo; ",
                    new string[] 
                    { 
                        //0
                        "BusinessPartnerType",

                        //1-5
                        "BusinessPartnerCode", "BusinessPartnerName", "BankCode", "BankName", "GiroNo", 

                        //6-7
                        "CurCode", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowGiroSummaryData(
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2),
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1),
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4),
                    Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6)
                );
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control events

        private void TxtBusinessPartnerCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBusinessPartnerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Business Partner");
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkGiroNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Giro#");
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBankCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Bank");
        }

        #endregion

        #endregion

    }
}
