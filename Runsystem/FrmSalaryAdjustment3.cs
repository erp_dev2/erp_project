﻿#region Update
/*
    14/03/2018 [TKG] New Application
    15/03/2018 [TKG] tambah fasilitas generate employee dari csv file
    18/06/2018 [TKG/HIN] tambah indikator nontaxable
    16/02/2022 [TKG/GSS] Merubah GetParameter() dan proses save
    23/02/2022 [VIN/HIP] BUG save detail kurang koma SQL2
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalaryAdjustment3 : RunSystem.FrmBase15
    {
        #region Field
        internal string mMenuCode = string.Empty;
        private bool mIsSalaryAdjustmentNontaxableEnabled = false;
        decimal mEmpCodeLength = 0m;

        #endregion

        #region Constructor

        public FrmSalaryAdjustment3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Group Of Employee's Salary Adjustment";
            try
            {
                GetParameter();
                ChkNontaxable.Visible = mIsSalaryAdjustmentNontaxableEnabled;
                SetGrd();
                ClearGrd();
                Sm.SetDteCurrentDate(DteDocDt);
                DteDocDt.Focus();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'EmpCodeLength', 'IsSalaryAdjustmentNontaxableEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSalaryAdjustmentNontaxableEnabled": mIsSalaryAdjustmentNontaxableEnabled = ParValue == "Y"; break;
                            
                            //string
                            case "EmpCodeLength": if (ParValue.Length>0) mEmpCodeLength = decimal.Parse(ParValue); break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
 
                        //1-5
                        "Employee's Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department", 

                        //6-9
                        "Site", 
                        "Join",
                        "Resign",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        120, 200, 100, 180, 180,
                        
                        //6-9
                        180, 100, 100, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7, 8 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 9 });
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SaveData();        
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void SaveData()
        {
            if (Sm.StdMsgYN("Save", string.Empty ) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string CurrentDateTime = Sm.GetValue("Select CurrentDateTime()");
            string DocDt = Sm.Left(Sm.GetDte(DteDocDt), 8);
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType=@Param;", "SalaryAdjustment");
            string DocNoFormat = string.Concat("/", DocTitle, "/", DocAbbr, "/", Mth, "/", Yr);
            string LatestDocNo = Sm.GetValue("Select Left(DocNo, 4) As LatestDocNo From TblSalaryAdjustmentHdr Where DocNo Like '%"+DocNoFormat+"' Order By DocNo Desc Limit 1;");
            int LatestNo = 0;
            if (LatestDocNo.Length != 0) LatestNo = Int32.Parse(LatestDocNo);
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalaryAdjustment(ref LatestNo, DocNoFormat));

            //for (int r = 0; r < Grd1.Rows.Count; r++)
            //{
            //    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
            //    {
            //        LatestNo += 1;
            //        cml.Add(SaveSalaryAdjustment(
            //            r, 
            //            string.Concat(Sm.Right(string.Concat("0000", LatestNo.ToString()), 4), DocNoFormat)
            //            ));
            //    }
            //}

            Sm.ExecCommands(cml);

            Sm.StdMsg(mMsgType.Info, "Process is completed.");

            Sm.SetDteCurrentDate(DteDocDt);
            MeeDesc.EditValue = null;
            DtePaidDt.EditValue = null;
            MeeRemark.EditValue = null;
            ClearGrd();
        }

        private MySqlCommand SaveSalaryAdjustment(ref int LatestNo, string DocNoFormat)
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Salary Adjustment (Hdr-Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    LatestNo += 1;
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalaryAdjustmentHdr (DocNo, DocDt, CancelInd, EmpCode, Amt, PaidDt, PayrunCode, Nontaxable, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        SQL2.AppendLine("Insert Into TblSalaryAdjustmentDtl (DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
                        SQL2.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                    {
                        SQL.AppendLine(", ");
                        SQL2.AppendLine(", ");
                    }

                    SQL.AppendLine(
                        " (@DocNo_" + r.ToString() +
                        ", @DocDt, 'N', @EmpCode_" + r.ToString() +
                        ", @Amt_" + r.ToString() +
                        ", @PaidDt, Null, @Nontaxable, @Remark, @UserCode, @Dt) ");
                    SQL2.AppendLine(" (@DocNo_" + r.ToString() +
                        ", '001', @Description, @Amt_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DocNo_" + r.ToString(),
                        string.Concat(Sm.Right(string.Concat("0000", LatestNo.ToString()), 4), DocNoFormat)
                        );
                    Sm.CmParam<String>(ref cm, "@EmpCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 9));
                }
            }
            if (!IsFirstOrExisted)
            {
                SQL.AppendLine("; ");
                SQL2.AppendLine("; ");
            }

            cm.CommandText = SQL.ToString() + SQL2.ToString();

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@PaidDt", Sm.GetDte(DtePaidDt));
            Sm.CmParam<String>(ref cm, "@Description", MeeDesc.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Nontaxable", ChkNontaxable.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #region Old Code

        //private MySqlCommand SaveSalaryAdjustment(int r, string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("/* Salary Adjustment (Hdr-Dtl) */ ");
        //    SQL.AppendLine("Set @Dt:=CurrentDateTime();");

        //    SQL.AppendLine("Insert Into TblSalaryAdjustmentHdr (DocNo, DocDt, CancelInd, EmpCode, Amt, PaidDt, PayrunCode, Nontaxable, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @EmpCode, @Amt, @PaidDt, Null, @Nontaxable, @Remark, @UserCode, @Dt); ");

        //    SQL.AppendLine("Insert Into TblSalaryAdjustmentDtl (DocNo, DNo, Description, Amt, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, '001', @Description, @Amt, @UserCode, @Dt); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParamDt(ref cm, "@PaidDt", Sm.GetDte(DtePaidDt));
        //    Sm.CmParam<String>(ref cm, "@Description", MeeDesc.Text);
        //    Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
        //    Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, r, 9));
        //    Sm.CmParam<String>(ref cm, "@Nontaxable", ChkNontaxable.Checked?"Y":"N");
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
        //    return cm;
        //}

        #endregion

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsMeeEmpty(MeeDesc, "Description") ||
                Sm.IsDteEmpty(DtePaidDt, "Payment date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Employee is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, r, 9, true, "Amount should be greater than 0.00.")) return true;                   
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee in the list.");
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmSalaryAdjustment3Dlg(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            if (e.KeyCode == Keys.Enter && Grd1.CurRow.Index != Grd1.Rows.Count - 1)
                Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, Grd1.CurCell.Col.Index);

            if (e.KeyCode == Keys.Tab && Grd1.CurCell != null && Grd1.CurCell.RowIndex == Grd1.Rows.Count - 1)
            {
                int LastVisibleCol = Grd1.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd1.Cols.Count - 1; Col++)
                    if (Grd1.Cols[Col].Visible) LastVisibleCol = Col;

                if (Grd1.CurCell.Col.Order == LastVisibleCol) BtnSave.Focus();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmSalaryAdjustment3Dlg(this));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 9)
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy amount from first record ?") == DialogResult.No) return;
                if (Sm.GetGrdStr(Grd1, 0, 9).Length != 0)
                {
                    decimal Amt = Sm.GetGrdDec(Grd1, 0, 9);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) Grd1.Cells[Row, 9].Value = Amt;
                }
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnRemoveAllEmployee_Click(object sender, EventArgs e)
        {
            ClearGrd();
        }

        private void BtnCsv_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();
            ClearGrd();

            try 
            {
                ProcessCsv1(ref lResult);
                if (lResult.Count > 0)
                {
                    ProcessCsv2(ref lResult);
                    ProcessCsv3(ref lResult);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }


        private void ProcessCsv1(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;
            string EmpCodeTemp = string.Empty;
            var AmtTemp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            EmpCodeTemp = arr[0].Trim();
                            if (mEmpCodeLength != 0)
                            {
                                if (EmpCodeTemp.Length < mEmpCodeLength) 
                                    EmpCodeTemp = Sm.Right(string.Concat("0000000000", EmpCodeTemp), (int)mEmpCodeLength);
                            }
                            if (arr[1].Trim().Length > 0)
                                AmtTemp = decimal.Parse(arr[1].Trim());
                            else
                                AmtTemp = 0m;
                            l.Add(new Result()
                            {
                                EmpCode = EmpCodeTemp,
                                EmpName = string.Empty,
                                EmpCodeOld = string.Empty,
                                PosName = string.Empty,
                                DeptName = string.Empty,
                                SiteName = string.Empty,
                                JoinDt = string.Empty,
                                ResignDt = string.Empty,
                                Amt = AmtTemp
                            });
                        }
                    }
                }
            }
        }

        private void ProcessCsv2(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;
            var Filter = string.Empty;

            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), l[i].EmpCode);
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0<>1 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, ");
            SQL.AppendLine("B.PosName, C.DeptName, D.SiteName, A.JoinDt, A.ResignDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpName", 
                    "EmpCodeOld", 
                    "PosName", 
                    "DeptName", 
                    "SiteName",
                    
                    //6-7
                    "JoinDt", 
                    "ResignDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        {
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].EmpName = Sm.DrStr(dr, c[1]);
                                l[i].EmpCodeOld = Sm.DrStr(dr, c[2]);
                                l[i].PosName = Sm.DrStr(dr, c[3]);
                                l[i].DeptName = Sm.DrStr(dr, c[4]);
                                l[i].SiteName = Sm.DrStr(dr, c[5]);
                                l[i].JoinDt = Sm.DrStr(dr, c[6]);
                                l[i].ResignDt = Sm.DrStr(dr, c[7]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ProcessCsv3(ref List<Result> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[1].Value = l[i].EmpCode;
                r.Cells[2].Value = l[i].EmpName;
                r.Cells[3].Value = l[i].EmpCodeOld;
                r.Cells[4].Value = l[i].PosName;
                r.Cells[5].Value = l[i].DeptName;
                r.Cells[6].Value = l[i].SiteName;
                if (l[i].JoinDt.Length > 0) r.Cells[7].Value = Sm.ConvertDate(l[i].JoinDt);
                if (l[i].ResignDt.Length > 0) r.Cells[8].Value = Sm.ConvertDate(l[i].ResignDt);
                r.Cells[9].Value = l[i].Amt;
                
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public decimal Amt { get; set; }
            public string Nontaxable { get; set; }
        }

        #endregion
    }
}
