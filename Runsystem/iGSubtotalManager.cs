﻿using System;
using System.Drawing;
using System.Collections;
using TenTec.Windows.iGridLib;

using Sm = RunSystem.StdMtd;

/// <summary>
/// Manages subtotal rows in iGrid.NET.
/// </summary>
public class iGSubtotalManager
{
    #region Fields

    /// <summary>
    /// Stores the grids which are managed by this manager and the 
    /// indices of their columns which subtotals are created for.
    /// </summary>
    private static Hashtable fGridsIndices;

    /// <summary>
    /// The style of the subtotal rows.
    /// </summary>
    private static iGCellStyle fCellStyleSubtotal;

    /// <summary>
    /// Indicates whether to show subtotals in cells or
    /// in a row.
    /// </summary>
    private static bool fShowSubtotalsInCells;

    #endregion

    #region Public Methods

    /// <summary>
    /// Shows subtotals in the specified grid for the specified columns.
    /// </summary>
    public static void ShowSubtotals(iGrid grid, int[] colIndices)
    {
        // Check the arguments.

        if (grid == null)
            throw new ArgumentNullException("grid");

        if (colIndices == null)
            throw new ArgumentNullException("colIndices");

        // Copy colIndices to an internal array.
        int[] myColIndices = new int[colIndices.Length];
        Array.Copy(colIndices, myColIndices, colIndices.Length);

        // Add the grid and its column indices into 
        // an internal hash table.
        if (!fGridsIndices.ContainsKey(grid))
        {
            AssignEventHandlers(grid);
            fGridsIndices.Add(grid, myColIndices);
        }
        else
            fGridsIndices[grid] = myColIndices;

        // Show subtotals.
        AdjustSubtotals(grid);
    }

    /// <summary>
    /// Hides subtotals in the specified grid.
    /// </summary>
    public static void HideSubtotals(iGrid grid)
    {
        // Check the arguments.
        if (grid == null)
            throw new ArgumentNullException("grid");

        // Remove subtotals and unattach the grid.
        if (fGridsIndices.ContainsKey(grid))
        {
            RemoveSubtotals(grid);
            RemoveEventHandlers(grid);
            fGridsIndices.Remove(grid);
        }
    }

    /// <summary>
    /// Determines whether the specified row is a subtotal.
    /// </summary>
    public static bool IsSubtotal(iGRow row)
    {
        // Check the arguments.
        if (row == null)
            throw new ArgumentNullException("row");
        return row.Tag as string == "Subtotal";

    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets or sets the background color of subtotal rows.
    /// </summary>
    public static Color BackColor
    {
        get
        {
            return fCellStyleSubtotal.BackColor;
        }
        set
        {
            fCellStyleSubtotal.BackColor = value;
        }
    }

    /// <summary>
    /// Gets or sets the foreground color of subtotal rows.
    /// </summary>
    public static Color ForeColor
    {
        get
        {
            return fCellStyleSubtotal.ForeColor;
        }
        set
        {
            fCellStyleSubtotal.ForeColor = value;
        }
    }

    /// <summary>
    /// Gets or sets a value indicating whether to show subtotals 
    /// in separate cells or drawn them in a row separating 
    /// them with semicolons.
    /// </summary>
    public static bool ShowSubtotalsInCells
    {
        get
        {
            return fShowSubtotalsInCells;
        }
        set
        {
            fShowSubtotalsInCells = value;
        }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Initializes internal fields.
    /// </summary>
    static iGSubtotalManager()
    {
        fGridsIndices = new Hashtable();
        fCellStyleSubtotal = new iGCellStyle();
    }

    /// <summary>
    /// Assign handlers to the iGrid events which are used to create subtotals.
    /// </summary>
    private static void AssignEventHandlers(iGrid grid)
    {
        // Check the arguments.
        if (grid == null)
            throw new ArgumentNullException("grid");

        grid.BeforeContentsGrouped += new EventHandler(BeforeContentsGrouped);
        grid.AfterContentsGrouped += new EventHandler(AfterContentsGrouped);
        grid.AfterCommitEdit += new iGAfterCommitEditEventHandler(AfterCommitEdit);
    }

    /// <summary>
    /// Removes handlers from the iGrid events which were used to create subtotals.
    /// </summary>
    private static void RemoveEventHandlers(iGrid grid)
    {
        // Check the arguments.
        if (grid == null)
            throw new ArgumentNullException("grid");

        grid.BeforeContentsGrouped -= new System.EventHandler(BeforeContentsGrouped);
        grid.AfterContentsGrouped -= new System.EventHandler(AfterContentsGrouped);
    }

    /// <summary>
    /// Removes old subtotals.
    /// </summary>
    private static void BeforeContentsGrouped(object sender, System.EventArgs e)
    {
        iGrid myGrid = sender as iGrid;

        myGrid.BeginUpdate();

        RemoveSubtotals(myGrid);
    }

    /// <summary>
    /// Adds new subtotals.
    /// </summary>
    private static void AfterContentsGrouped(object sender, System.EventArgs e)
    {
        iGrid myGrid = sender as iGrid;

        // Add the subtotals.
        AdjustSubtotals(myGrid);

        myGrid.EndUpdate();
    }

    /// <summary>
    /// Removes the subtotals from the specified grid.
    /// </summary>
    private static void RemoveSubtotals(iGrid grid)
    {
        // Check the arguments.
        if (grid == null)
            throw new ArgumentNullException("grid");

        for (int myRowIndex = grid.Rows.Count - 1; myRowIndex >= 0; myRowIndex--)
            if (IsSubtotal(grid.Rows[myRowIndex]))
                grid.Rows.RemoveAt(myRowIndex);
    }

    /// <summary>
    /// Adds subtotals to the specified grid.
    /// </summary>
    /// 

    private static void AdjustSubtotals(iGrid grid)
    {
        // Check the arguments.

        if (grid == null)
            throw new ArgumentNullException("grid");

        if (!fGridsIndices.Contains(grid))
            throw new ArgumentException("grid");

        // Get the column indices which to create subtotals for.
        int[] myColIndices = fGridsIndices[grid] as int[];

        // Create arrays to store the subtotal values for all the levels.
        decimal[,] mySubtotalValues = new decimal[myColIndices.Length, grid.GroupObject.Count + 1];

        string[] mySubtotalTitle = new string[grid.GroupObject.Count + 1];

        // Create subtotal row pattern.
        iGRowPattern myRowPat = grid.DefaultRow.Clone();
        if (fShowSubtotalsInCells)
            myRowPat.Type = iGRowType.Normal;
        else
            myRowPat.Type = iGRowType.ManualGroupRow;
        myRowPat.Tag = "Subtotal";

        // Stores the old row level.
        int myOldLevel = 0;

        // Enumerate the rows.
        int myRowIndex = 0;
        while (myRowIndex < grid.Rows.Count)
        {
            iGRow myRow = grid.Rows[myRowIndex];
            if (myRow.Visible)
            {
                int myLevel = myRow.Level;
                if (myRow.Type == iGRowType.AutoGroupRow || IsSubtotal(myRow))
                {
                    // If this is a subtotal row.
                    if (IsSubtotal(myRow))
                        myLevel--;

                    // Add the subtotal rows for the previos rows.
                    AdjustSubtotals(grid, ref myRowIndex, mySubtotalValues, myOldLevel, myLevel, myRowPat, mySubtotalTitle);
                }
                else
                {
                    // Calculate the subtotal values for this row.
                    for (int mySubtotalIndex = 0; mySubtotalIndex < myColIndices.Length; mySubtotalIndex++)
                    {
                        Decimal myCurSubtotalValue;
                        try
                        {
                            myCurSubtotalValue = Convert.ToDecimal(myRow.Cells[myColIndices[mySubtotalIndex]].Value);
                        }
                        catch
                        {
                            myCurSubtotalValue = 0;
                        }

                        // Add the sales and debt of this row to the sums of all the levels.
                        for (int myCurLevel = grid.GroupObject.Count; myCurLevel >= 0; myCurLevel--)
                        {

                            //mySubtotalTitle[myCurLevel] = grid.Cols[myCurLevel].Text.ToString() + ": "+grid.Cells[myRowIndex, myCurLevel].Value.ToString();
                            if (myCurLevel > 0 && grid.Cells[myRowIndex, myCurLevel - 1].Value!=null)
                                mySubtotalTitle[myCurLevel] = grid.Cells[myRowIndex, myCurLevel - 1].Value.ToString();
                            mySubtotalValues[mySubtotalIndex, myCurLevel] = mySubtotalValues[mySubtotalIndex, myCurLevel] + myCurSubtotalValue;
                        }
                    }
                }
                myOldLevel = myLevel;
            }
            myRowIndex = (myRowIndex + 1);
        }

        // Add the subtotals for the last rows.
        AdjustSubtotals(grid, ref myRowIndex, mySubtotalValues, myOldLevel, -1, myRowPat, mySubtotalTitle);
    }


    /// <summary>
    /// Adds subtotals to the specified grid for the specified group of rows.
    /// </summary>
    private static void AdjustSubtotals(iGrid grid, ref int rowIndex, Decimal[,] subtotalValues, int oldLevel, int level, iGRowPattern rowPat, String[] subtotalTitle)
    {
        // Check the arguments.

        if (grid == null)
            throw new ArgumentNullException("grid");

        if (!fGridsIndices.Contains(grid))
            throw new ArgumentException("grid");

        if (rowIndex < 0 || rowIndex > grid.Rows.Count)
            throw new ArgumentOutOfRangeException("rowIndex");

        if (subtotalValues == null)
            throw new ArgumentNullException("subtotalValues");

        if (oldLevel < -1 || oldLevel > grid.GroupObject.Count)
            throw new ArgumentOutOfRangeException("oldLevel");

        if (level < -1 || level > grid.GroupObject.Count)
            throw new ArgumentOutOfRangeException("level");

        if (rowPat == null)
            throw new ArgumentNullException("rowPat");

        // Get the column indices which to create subtotals for.
        int[] myColIndices = fGridsIndices[grid] as int[];
        
        int FirstShownCol = 0;
        if (grid.GroupObject.Count > 0)
        	FirstShownCol = grid.GroupObject[grid.GroupObject.Count - 1].ColIndex + 1;
        

        // Add the subtotal rows to a group of normal rows.
        while (oldLevel > level)
        {
            iGRow mySubTotalRow;
            // If the row with index = rowIndex is not a subtotal, add a new row.
            bool myIsSubTotal;
            if (rowIndex < grid.Rows.Count)
                myIsSubTotal = IsSubtotal(grid.Rows[rowIndex]);
            else
                myIsSubTotal = false;

            if (!myIsSubTotal)
            {
                //Changed By TKG
                rowPat.Level = oldLevel <= 0 ? oldLevel : oldLevel - 1;
                //rowPat.Level = oldLevel;
                mySubTotalRow = grid.Rows.Insert(rowIndex, rowPat);
                rowIndex++;
            }
            else
                mySubTotalRow = grid.Rows[rowIndex];

            if (fShowSubtotalsInCells)
            {
                foreach (iGCell myCell in mySubTotalRow.Cells)
                    myCell.Style = fCellStyleSubtotal;

                for (int mySubtotalIndex = 0; mySubtotalIndex < subtotalValues.GetLength(0); mySubtotalIndex++)
                {
                    //mySubTotalRow.Cells[FirstShownCol].Value = "Total";

                    // Updated By TKG
                    //if (mySubTotalRow.Level == 0)
                    //{
                    //    mySubTotalRow.Cells[FirstShownCol].Value = "Total : ";
                    //}
                    //else
                    //{
                    //    mySubTotalRow.Cells[FirstShownCol].Value = "Subtotal ";

                    //    //if (subtotalTitle[oldLevel].Length > 20)
                    //    //    mySubTotalRow.Cells[FirstShownCol].Value = "Subtotal " + subtotalTitle[oldLevel].Substring(0, 20);
                    //    //else
                    //    //    mySubTotalRow.Cells[FirstShownCol].Value = "Subtotal " + subtotalTitle[oldLevel];
                    //}
                    mySubTotalRow.Cells[myColIndices[mySubtotalIndex]].Value = Sm.NumValue3(subtotalValues[mySubtotalIndex, oldLevel].ToString(), grid.Cols[myColIndices[mySubtotalIndex]].CellStyle.FormatString);

                    // Reset the sales and debt sums for this level.
                    subtotalValues[mySubtotalIndex, oldLevel] = 0;
                }
            }
            else
            {
                string mySubtotalRowText = "Total";
                //string mySubtotalRowText = "Subtotals : ";
                for (int mySubtotalIndex = 0; mySubtotalIndex < subtotalValues.GetLength(0); mySubtotalIndex++)
                {
                    decimal myCurSubtotalValue = subtotalValues[mySubtotalIndex, oldLevel];
                    mySubtotalRowText = mySubtotalRowText + grid.Cols[myColIndices[mySubtotalIndex]].Text + " = " + Sm.NumValue(myCurSubtotalValue.ToString(), 1) + "; ";

                    // Reset the sales and debt sums for this level.
                    subtotalValues[mySubtotalIndex, oldLevel] = 0;
                }

                mySubTotalRow.RowTextCell.Style = fCellStyleSubtotal;
                mySubTotalRow.RowTextCell.Value = mySubtotalRowText;
            }
            oldLevel--;
        }
    }

    /// <summary>
    /// Adjusts the subtotal values.
    /// </summary>
    private static void AfterCommitEdit(object sender, TenTec.Windows.iGridLib.iGAfterCommitEditEventArgs e)
    {
        iGrid myGrid = sender as iGrid;

        int[] myColIndices = fGridsIndices[myGrid] as int[];

        foreach (int myColIndex in myColIndices)
            if (myColIndex == e.ColIndex)
            {
                // Adjust the subtotals.
                myGrid.BeginUpdate();
                try
                {
                    AdjustSubtotals(myGrid);
                }
                finally
                {
                    myGrid.EndUpdate();
                }
                break;
            }
    }

    #endregion
}