﻿#region Update
/* 
    21/05/2017 [TKG] Documen type menggunakan 21 bukan 16
    11/06/2017 [TKG] tambah validasi vat settlement ketika data hendak dicancel. 
    14/04/2018 [TKG] menggunakan format khusus voucher atau format standard dokumen run system berdasarkan parameter VoucherCodeFormatType
    26/07/2018 [WED] waktu cancel dokumen juga cek ke approval status nya udah cancel atau belum
    01/08/2018 [WED] tambah inputan Vendor
    24/01/2019 [TKG] bug saat approval
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestTax : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mTaxCode = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmVoucherRequestTaxFind FrmFind;
        internal bool mIsFilterByDept = false;
        private bool mIsVoucherRequestTaxApprovalBasedOnDept = false;
        private string mVoucherCodeFormatType = "1";

        #endregion

        #region Constructor

        public FrmVoucherRequestTax(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Voucher Request For Tax";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                SetLueTaxCode(ref LueTaxCode, string.Empty);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueVoucherDocType(ref LueDocType);
                Sl.SetLueVoucherPaymentType(ref LuePaymentType);
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueVdCode(ref LueVdCode);

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtVoucherRequestDocNo, TxtVoucherDocNo, TxtStatus, TxtVRPPNDocNo }, true);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtAmt, MeeCancelReason, LueDeptCode, 
                        LueDocType, LueCurCode, LuePaymentType, LueBankCode, TxtGiroNo, 
                        DteDueDt, LuePIC, MeeRemark, LueAcType, TxtTaxInvNo, DteTaxInvDt, LueTaxCode,
                        LueBankAcCode, LueVdCode
                    }, true);

                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                    Sm.SetLue(LueDocType, "21");
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueDeptCode, LueCurCode, LuePaymentType, LuePIC, 
                        MeeRemark, LueAcType, TxtAmt, TxtTaxInvNo, DteTaxInvDt, LueTaxCode,
                        LueBankAcCode, LueVdCode
                    }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, LueDeptCode,
                LueDocType, LueCurCode, LuePaymentType, LueBankCode, TxtGiroNo, 
                DteDueDt, LuePIC, MeeRemark, TxtVoucherRequestDocNo, TxtVoucherDocNo,
                LueAcType, TxtTaxInvNo, DteTaxInvDt, LueTaxCode, TxtStatus, LueBankAcCode,
                TxtVRPPNDocNo, LueVdCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestTaxFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueUserCode(ref LuePIC, string.Empty);
                SetLueDeptCode(ref LueDeptCode, string.Empty);
                SetLueTaxCode(ref LueTaxCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequestTax", "TblVoucherRequestTax");
            string VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            bool NoNeedApproval = IsDocApprovalSettingNotExisted();

            var cml = new List<MySqlCommand>();
            
            cml.Add(SaveVoucherRequestTax(DocNo, VoucherRequestDocNo, NoNeedApproval));
            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));
            cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LuePIC, "Person In Charge") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                Sm.IsLueEmpty(LueTaxCode, "Tax") ||
                Sm.IsTxtEmpty(TxtTaxInvNo, "Tax Invoice#", false) ||
                Sm.IsDteEmpty(DteTaxInvDt, "Tax Invoice Date") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LueAcType, "Debit / Credit") ||
                Sm.IsLueEmpty(LueBankAcCode, "Debit / Credit To") ||
                Sm.IsLueEmpty(LueDocType, "Type") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                IsPaymentTypeNotValid() ||
                Sm.IsMeeEmpty(MeeRemark, "Remark");
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocType From TblDocApprovalSetting Where UserCode Is not Null And DocType='VoucherRequestTax' Limit 1; "
            };
            if (!Sm.IsDataExist(cm))
                return true;
            else
                return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.GetLue(LuePaymentType) == "B")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Bilyet#", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank Name")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private MySqlCommand SaveVoucherRequestTax(string DocNo, string VoucherRequestDocNo, bool NoNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestTax ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, Amt, TaxCode, TaxInvNo, TaxInvDt, AcType, BankAcCode, DeptCode, DocType, PaymentType, GiroNo, ");
            SQL.AppendLine("BankCode, DueDt, PIC, CurCode, VdCode, Remark, VoucherRequestDocNo, VoucherRequestPPNDocNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @Amt, @TaxCode, @TaxInvNo, @TaxInvDt, @AcType, @BankAcCode, @DeptCode, '21', @PaymentType, @GiroNo, ");
            SQL.AppendLine("@BankCode, @DueDt, @PIC, @CurCode, @VdCode, @Remark, @VoucherRequestDocNo, NULL, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='VoucherRequestTax' ");

                if (mIsVoucherRequestTaxApprovalBasedOnDept)
                    SQL.AppendLine("And T.DeptCode = (Select DeptCode From TblVoucherRequestTax Where DocNo = @DocNo) ");

                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
                SQL.AppendLine("    From TblVoucherRequestTax A ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
                SQL.AppendLine("        From TblCurrencyRate B1 ");
                SQL.AppendLine("        Inner Join ( ");
                SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
                SQL.AppendLine("            From TblCurrencyRate ");
                SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
                SQL.AppendLine("            Group By CurCode1 ");
                SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
                SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("), 0)); ");
            }

            SQL.AppendLine("Update TblVoucherRequestTax Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestTax' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", NoNeedApproval ? "A" : "O");
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@TaxInvNo", TxtTaxInvNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvDt", Sm.GetDte(DteTaxInvDt));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string VoucherRequestTaxDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, VoucherDocNo, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', @DeptCode, ");
            SQL.AppendLine("'21', @AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@PIC, 0, @CurCode, @Amt, NULL, ");
            SQL.AppendLine("NULL, NULL, NULL, NULL, NULL, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestTax' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestTaxDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestTaxDocNo", VoucherRequestTaxDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, NULL, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", "Voucher Request For Tax - " + MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelVoucherRequestTax());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsVoucherRequestTaxNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedToVoucher() ||
                IsDataEnlistedInVAT() ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted());
        }

        private bool IsVoucherRequestTaxNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblVoucherRequestTax ");
            SQL.AppendLine("Where (CancelInd='Y' Or Status = 'C') And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }

            return false;
        }

        private bool IsVRStillActive()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherRequestHdr Where CancelInd = 'N' And DocNo = @Param;",
                TxtVoucherRequestDocNo.Text,
                "Voucher Request (" + TxtVoucherRequestDocNo.Text + ") is still active.");
        }

        private bool IsDataEnlistedInVAT()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherRequestPPNHdr Where CancelInd = 'N' And DocNo = @Param;",
                TxtVRPPNDocNo.Text,
                "Data already processed into Voucher Request VAT.");
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblVoucherRequestTax " +
                "Where DocNo=@Param And VoucherRequestPPNDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request VAT.");
                return true;
            }

            return false;
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblVoucherRequestTax " +
                "Where DocNo=@Param And VATSettlementDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }

            return false;
        }

        private bool IsDataProcessedToVoucher()
        {            
            return Sm.IsDataExist(
                "Select DocNo From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
                TxtVoucherRequestDocNo.Text,
                "Data already processed into Voucher.");
        }

        private MySqlCommand CancelVoucherRequestTax()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestTax Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo AND Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVoucherRequestTax(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowVoucherRequestTax(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.Amt, A.TaxCode, A.TaxInvNo, A.TaxInvDt, A.CancelInd, A.CancelReason, A.AcType, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' Else 'Cancelled' End As Status, ");
            SQL.AppendLine("A.DeptCode, A.DocType, A.PaymentType, A.GiroNo, A.BankCode, A.DueDt, A.PIC, A.CurCode, A.Remark, ");
            SQL.AppendLine("A.VoucherRequestDocNo, B.VoucherDocNo, A.BankAcCode, A.VoucherRequestPPNDocNo, A.VdCode ");
            SQL.AppendLine("FROM TblVoucherRequestTax A ");
            SQL.AppendLine("LEFT JOIN TblVoucherRequestHdr B ON A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("WHERE A.DocNo=@DocNo;");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "Amt", "CancelInd", "CancelReason", "DeptCode",

                    //6-10
                    "DocType", "PaymentType", "GiroNo", "BankCode", "DueDt",

                    //11-15
                    "PIC", "CurCode", "Remark", "VoucherRequestDocNo", "VoucherDocNo",

                    //16-20
                    "TaxCode", "TaxInvNo", "TaxInvDt", "Status", "AcType",

                    //21-23
                    "BankAcCode", "VoucherRequestPPNDocNo", "VdCode"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[4]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[2]), 0);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                     SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueDocType, Sm.DrStr(dr, c[6]));
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[7]));
                     TxtGiroNo.EditValue = Sm.DrStr(dr, c[8]);
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[9]));
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[10]));
                     Sl.SetLueUserCode(ref LuePIC, Sm.DrStr(dr, c[11]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[12]));
                     MeeRemark.EditValue = Sm.DrStr(dr, c[13]);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[14]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[15]);
                     SetLueTaxCode(ref LueTaxCode, Sm.DrStr(dr, c[16]));
                     Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[16]));
                     TxtTaxInvNo.EditValue = Sm.DrStr(dr, c[17]);
                     Sm.SetDte(DteTaxInvDt, Sm.DrStr(dr, c[18]));
                     TxtStatus.EditValue = Sm.DrStr(dr, c[19]);
                     Sm.SetLue(LueAcType, Sm.DrStr(dr, c[20]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[21]));
                     TxtVRPPNDocNo.EditValue = Sm.DrStr(dr, c[22]);
                     Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[23]));
                 }, true
             );
        }

        private void ShowDocApproval(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQLAppr = new StringBuilder();

            SQLAppr.AppendLine("Select A.ApprovalDNo, B.UserName, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQLAppr.AppendLine("Case When A.LastUpDt Is Not Null Then ");
            SQLAppr.AppendLine("A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQLAppr.AppendLine("From TblDocApproval A ");
            SQLAppr.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQLAppr.AppendLine("Where A.DocType='VoucherRequestTax' ");
            SQLAppr.AppendLine("And Status In ('A', 'C') ");
            SQLAppr.AppendLine("And A.DocNo=@DocNo ");
            SQLAppr.AppendLine("Order By A.ApprovalDNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQLAppr.ToString(),
                    new string[] 
                    { 
                        "ApprovalDNo",
                        "UserName","StatusDesc","LastUpDt", "Remark"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Methods

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle';"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest';"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.AppendLine("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.AppendLine("    (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.AppendLine("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
            SQL.AppendLine("    From TblVoucherRequestHdr ");
            //SQL.AppendLine("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
            //SQL.AppendLine("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
            SQL.AppendLine("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            SQL.AppendLine("Order By SUBSTRING(DocNo,7,"+DocSeqNo+") Desc Limit 1) As temp ");
            SQL.AppendLine("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.AppendLine("), '0001' ");
            SQL.AppendLine(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");

            return Sm.GetValue(SQL.ToString());
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
            SQL.AppendLine("From TblDepartment T ");

            if (DeptCode.Length != 0)
                SQL.AppendLine("Where DeptCode=@DeptCode ");
            else
            {
                SQL.AppendLine("Where ActInd='Y' ");
                if (mIsFilterByDept)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Order By T.DeptName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void SetLueTaxCode(ref DXE.LookUpEdit Lue, string TaxCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.TaxCode As Col1, T.TaxName As Col2 ");
            SQL.AppendLine("From TblTax T ");
            SQL.AppendLine("Where Find_In_Set(IfNull(T.TaxCode, ''), @Param) ");

            if (TaxCode.Length != 0)
                SQL.AppendLine("And T.TaxCode = @TaxCode ");

            SQL.AppendLine("Order By T.TaxName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Param", mTaxCode);
            Sm.CmParam<String>(ref cm, "@TaxCode", TaxCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsVoucherRequestTaxApprovalBasedOnDept = Sm.GetParameterBoo("IsVoucherRequestTaxApprovalBasedOnDept");
            mTaxCode = Sm.GetParameter("TaxCodeForVRTax");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue2(Sl.SetLueUserCode), string.Empty);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDeptCode), string.Empty);
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue2(SetLueTaxCode), string.Empty);
        }

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
            if (Sm.GetLue(LueAcType) == "C")
            {
                LblBankAcCode.Text = "           Credit To";
            }
            else if (Sm.GetLue(LueAcType) == "D")
            {
                LblBankAcCode.Text = "           Debit To";
            }
            else
            {
                LblBankAcCode.Text = "Debit / Credit To";
            }
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new StdMtd.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueBankCode, TxtGiroNo, DteDueDt
            });

            if (Sm.GetLue(LuePaymentType) == "C")
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
            else if (Sm.GetLue(LuePaymentType) == "B")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
            else if (Sm.GetLue(LuePaymentType) == "G")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
            }
            else if (Sm.GetLue(LuePaymentType) == "K")
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
            }
            else
            {
                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtGiroNo);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
        }

        #endregion

        #endregion
    }
}
