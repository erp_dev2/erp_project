﻿namespace RunSystem
{
    partial class FrmMedicalTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMedicalTest));
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtRecruitmentDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtRecruitmentName = new DevExpress.XtraEditors.TextEdit();
            this.TxtGender = new DevExpress.XtraEditors.TextEdit();
            this.TxtUmur = new DevExpress.XtraEditors.TextEdit();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtPosCode = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.TxtCompany = new DevExpress.XtraEditors.TextEdit();
            this.label172 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.BtnShowEmployeeRecruitment = new DevExpress.XtraEditors.SimpleButton();
            this.label71 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgAmnesis = new System.Windows.Forms.TabPage();
            this.TxtDN = new DevExpress.XtraEditors.TextEdit();
            this.TxtTD = new DevExpress.XtraEditors.TextEdit();
            this.TxtTB = new DevExpress.XtraEditors.TextEdit();
            this.TxtBB = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.LblTN = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Lue05 = new DevExpress.XtraEditors.LookUpEdit();
            this.Lue04 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPenyakit03 = new DevExpress.XtraEditors.TextEdit();
            this.Lue03 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPenyakit02 = new DevExpress.XtraEditors.TextEdit();
            this.Lue02 = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Lue01 = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgFisik = new System.Windows.Forms.TabPage();
            this.MeeConclusion = new DevExpress.XtraEditors.MemoExEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.MeeClinic = new DevExpress.XtraEditors.MemoExEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.MeeLab = new DevExpress.XtraEditors.MemoExEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.MeeEkstremitas = new DevExpress.XtraEditors.MemoExEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.MeeStomach = new DevExpress.XtraEditors.MemoExEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.MeeLiver = new DevExpress.XtraEditors.MemoExEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.MeeHeart = new DevExpress.XtraEditors.MemoExEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.MeeNeck = new DevExpress.XtraEditors.MemoExEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.MeeEar = new DevExpress.XtraEditors.MemoExEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.MeeMouth = new DevExpress.XtraEditors.MemoExEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.MeeNose = new DevExpress.XtraEditors.MemoExEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.MeeEye = new DevExpress.XtraEditors.MemoExEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.MeeNutrition = new DevExpress.XtraEditors.MemoExEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.MeeAwareness = new DevExpress.XtraEditors.MemoExEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.MeeCondition = new DevExpress.XtraEditors.MemoExEdit();
            this.label160 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.lookUpEdit5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lookUpEdit6 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lookUpEdit7 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.dateEdit3 = new DevExpress.XtraEditors.DateEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.memoExEdit1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.lookUpEdit8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.lookUpEdit9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.lookUpEdit10 = new DevExpress.XtraEditors.LookUpEdit();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dateEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit11 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit12 = new DevExpress.XtraEditors.LookUpEdit();
            this.iGrid1 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.iGrid2 = new TenTec.Windows.iGridLib.iGrid();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label73 = new System.Windows.Forms.Label();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label74 = new System.Windows.Forms.Label();
            this.lookUpEdit13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label75 = new System.Windows.Forms.Label();
            this.memoExEdit2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label76 = new System.Windows.Forms.Label();
            this.lookUpEdit14 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.label77 = new System.Windows.Forms.Label();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.label78 = new System.Windows.Forms.Label();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.label81 = new System.Windows.Forms.Label();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.label82 = new System.Windows.Forms.Label();
            this.lookUpEdit15 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.label83 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label84 = new System.Windows.Forms.Label();
            this.lookUpEdit16 = new DevExpress.XtraEditors.LookUpEdit();
            this.label85 = new System.Windows.Forms.Label();
            this.lookUpEdit17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label86 = new System.Windows.Forms.Label();
            this.lookUpEdit18 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.lookUpEdit19 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit5 = new DevExpress.XtraEditors.DateEdit();
            this.label89 = new System.Windows.Forms.Label();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit20 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit21 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit6 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.label96 = new System.Windows.Forms.Label();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.label97 = new System.Windows.Forms.Label();
            this.lookUpEdit22 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.lookUpEdit23 = new DevExpress.XtraEditors.LookUpEdit();
            this.label100 = new System.Windows.Forms.Label();
            this.lookUpEdit24 = new DevExpress.XtraEditors.LookUpEdit();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.lookUpEdit25 = new DevExpress.XtraEditors.LookUpEdit();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit26 = new DevExpress.XtraEditors.LookUpEdit();
            this.label105 = new System.Windows.Forms.Label();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.label106 = new System.Windows.Forms.Label();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit27 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit7 = new DevExpress.XtraEditors.DateEdit();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.dateEdit8 = new DevExpress.XtraEditors.DateEdit();
            this.label109 = new System.Windows.Forms.Label();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit29 = new DevExpress.XtraEditors.TextEdit();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.memoExEdit3 = new DevExpress.XtraEditors.MemoExEdit();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.lookUpEdit28 = new DevExpress.XtraEditors.LookUpEdit();
            this.label114 = new System.Windows.Forms.Label();
            this.lookUpEdit29 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.lookUpEdit30 = new DevExpress.XtraEditors.LookUpEdit();
            this.label117 = new System.Windows.Forms.Label();
            this.lookUpEdit31 = new DevExpress.XtraEditors.LookUpEdit();
            this.label118 = new System.Windows.Forms.Label();
            this.lookUpEdit32 = new DevExpress.XtraEditors.LookUpEdit();
            this.label119 = new System.Windows.Forms.Label();
            this.memoExEdit4 = new DevExpress.XtraEditors.MemoExEdit();
            this.label120 = new System.Windows.Forms.Label();
            this.lookUpEdit33 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit31 = new DevExpress.XtraEditors.TextEdit();
            this.label121 = new System.Windows.Forms.Label();
            this.textEdit32 = new DevExpress.XtraEditors.TextEdit();
            this.label122 = new System.Windows.Forms.Label();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.textEdit34 = new DevExpress.XtraEditors.TextEdit();
            this.label125 = new System.Windows.Forms.Label();
            this.textEdit35 = new DevExpress.XtraEditors.TextEdit();
            this.label126 = new System.Windows.Forms.Label();
            this.lookUpEdit34 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit36 = new DevExpress.XtraEditors.TextEdit();
            this.label127 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label128 = new System.Windows.Forms.Label();
            this.lookUpEdit35 = new DevExpress.XtraEditors.LookUpEdit();
            this.label129 = new System.Windows.Forms.Label();
            this.lookUpEdit36 = new DevExpress.XtraEditors.LookUpEdit();
            this.label130 = new System.Windows.Forms.Label();
            this.lookUpEdit37 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit37 = new DevExpress.XtraEditors.TextEdit();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.lookUpEdit38 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit9 = new DevExpress.XtraEditors.DateEdit();
            this.label133 = new System.Windows.Forms.Label();
            this.textEdit38 = new DevExpress.XtraEditors.TextEdit();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.textEdit39 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit39 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit40 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit10 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit40 = new DevExpress.XtraEditors.TextEdit();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.textEdit41 = new DevExpress.XtraEditors.TextEdit();
            this.label140 = new System.Windows.Forms.Label();
            this.textEdit42 = new DevExpress.XtraEditors.TextEdit();
            this.label141 = new System.Windows.Forms.Label();
            this.lookUpEdit41 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit43 = new DevExpress.XtraEditors.TextEdit();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.lookUpEdit42 = new DevExpress.XtraEditors.LookUpEdit();
            this.label144 = new System.Windows.Forms.Label();
            this.lookUpEdit43 = new DevExpress.XtraEditors.LookUpEdit();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.lookUpEdit44 = new DevExpress.XtraEditors.LookUpEdit();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.textEdit44 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit45 = new DevExpress.XtraEditors.LookUpEdit();
            this.label149 = new System.Windows.Forms.Label();
            this.textEdit45 = new DevExpress.XtraEditors.TextEdit();
            this.label150 = new System.Windows.Forms.Label();
            this.textEdit46 = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit46 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit11 = new DevExpress.XtraEditors.DateEdit();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.dateEdit12 = new DevExpress.XtraEditors.DateEdit();
            this.label153 = new System.Windows.Forms.Label();
            this.textEdit47 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit48 = new DevExpress.XtraEditors.TextEdit();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.memoExEdit5 = new DevExpress.XtraEditors.MemoExEdit();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.lookUpEdit47 = new DevExpress.XtraEditors.LookUpEdit();
            this.label158 = new System.Windows.Forms.Label();
            this.lookUpEdit48 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecruitmentDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecruitmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUmur.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompany.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgAmnesis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue05.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue04.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPenyakit03.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue03.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPenyakit02.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue02.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue01.Properties)).BeginInit();
            this.TpgFisik.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeConclusion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeClinic.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLab.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEkstremitas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeStomach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLiver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeHeart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNeck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEar.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeMouth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNose.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEye.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNutrition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAwareness.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCondition.Properties)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit48.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 589);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 589);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.TxtRecruitmentDocNo);
            this.panel3.Controls.Add(this.TxtRecruitmentName);
            this.panel3.Controls.Add(this.TxtGender);
            this.panel3.Controls.Add(this.TxtUmur);
            this.panel3.Controls.Add(this.TxtDeptCode);
            this.panel3.Controls.Add(this.TxtPosCode);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label173);
            this.panel3.Controls.Add(this.TxtCompany);
            this.panel3.Controls.Add(this.label172);
            this.panel3.Controls.Add(this.label171);
            this.panel3.Controls.Add(this.label170);
            this.panel3.Controls.Add(this.label169);
            this.panel3.Controls.Add(this.BtnShowEmployeeRecruitment);
            this.panel3.Controls.Add(this.label71);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label72);
            this.panel3.Controls.Add(this.label66);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 225);
            this.panel3.TabIndex = 9;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(117, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(259, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // TxtRecruitmentDocNo
            // 
            this.TxtRecruitmentDocNo.EnterMoveNextControl = true;
            this.TxtRecruitmentDocNo.Location = new System.Drawing.Point(117, 68);
            this.TxtRecruitmentDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecruitmentDocNo.Name = "TxtRecruitmentDocNo";
            this.TxtRecruitmentDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRecruitmentDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecruitmentDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecruitmentDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRecruitmentDocNo.Properties.MaxLength = 16;
            this.TxtRecruitmentDocNo.Properties.ReadOnly = true;
            this.TxtRecruitmentDocNo.Size = new System.Drawing.Size(220, 20);
            this.TxtRecruitmentDocNo.TabIndex = 15;
            // 
            // TxtRecruitmentName
            // 
            this.TxtRecruitmentName.EnterMoveNextControl = true;
            this.TxtRecruitmentName.Location = new System.Drawing.Point(117, 89);
            this.TxtRecruitmentName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRecruitmentName.Name = "TxtRecruitmentName";
            this.TxtRecruitmentName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRecruitmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRecruitmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRecruitmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtRecruitmentName.Properties.MaxLength = 16;
            this.TxtRecruitmentName.Properties.ReadOnly = true;
            this.TxtRecruitmentName.Size = new System.Drawing.Size(259, 20);
            this.TxtRecruitmentName.TabIndex = 18;
            // 
            // TxtGender
            // 
            this.TxtGender.EnterMoveNextControl = true;
            this.TxtGender.Location = new System.Drawing.Point(117, 110);
            this.TxtGender.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGender.Name = "TxtGender";
            this.TxtGender.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGender.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGender.Properties.Appearance.Options.UseFont = true;
            this.TxtGender.Properties.MaxLength = 16;
            this.TxtGender.Properties.ReadOnly = true;
            this.TxtGender.Size = new System.Drawing.Size(259, 20);
            this.TxtGender.TabIndex = 20;
            // 
            // TxtUmur
            // 
            this.TxtUmur.EnterMoveNextControl = true;
            this.TxtUmur.Location = new System.Drawing.Point(117, 131);
            this.TxtUmur.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUmur.Name = "TxtUmur";
            this.TxtUmur.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUmur.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUmur.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUmur.Properties.Appearance.Options.UseFont = true;
            this.TxtUmur.Properties.MaxLength = 16;
            this.TxtUmur.Properties.ReadOnly = true;
            this.TxtUmur.Size = new System.Drawing.Size(104, 20);
            this.TxtUmur.TabIndex = 22;
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(117, 173);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 16;
            this.TxtDeptCode.Properties.ReadOnly = true;
            this.TxtDeptCode.Size = new System.Drawing.Size(259, 20);
            this.TxtDeptCode.TabIndex = 27;
            // 
            // TxtPosCode
            // 
            this.TxtPosCode.EnterMoveNextControl = true;
            this.TxtPosCode.Location = new System.Drawing.Point(117, 152);
            this.TxtPosCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosCode.Name = "TxtPosCode";
            this.TxtPosCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPosCode.Properties.MaxLength = 16;
            this.TxtPosCode.Properties.ReadOnly = true;
            this.TxtPosCode.Size = new System.Drawing.Size(259, 20);
            this.TxtPosCode.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(23, 70);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 14);
            this.label17.TabIndex = 14;
            this.label17.Text = "Recruitment #";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.ForeColor = System.Drawing.Color.Black;
            this.label173.Location = new System.Drawing.Point(221, 134);
            this.label173.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(37, 14);
            this.label173.TabIndex = 23;
            this.label173.Text = "Years";
            this.label173.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCompany
            // 
            this.TxtCompany.EnterMoveNextControl = true;
            this.TxtCompany.Location = new System.Drawing.Point(117, 194);
            this.TxtCompany.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompany.Name = "TxtCompany";
            this.TxtCompany.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCompany.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompany.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompany.Properties.Appearance.Options.UseFont = true;
            this.TxtCompany.Properties.MaxLength = 30;
            this.TxtCompany.Size = new System.Drawing.Size(259, 20);
            this.TxtCompany.TabIndex = 29;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.ForeColor = System.Drawing.Color.Black;
            this.label172.Location = new System.Drawing.Point(53, 196);
            this.label172.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(57, 14);
            this.label172.TabIndex = 28;
            this.label172.Text = "Company";
            this.label172.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label171.ForeColor = System.Drawing.Color.Black;
            this.label171.Location = new System.Drawing.Point(37, 175);
            this.label171.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(73, 14);
            this.label171.TabIndex = 26;
            this.label171.Text = "Department";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.ForeColor = System.Drawing.Color.Black;
            this.label170.Location = new System.Drawing.Point(61, 155);
            this.label170.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(49, 14);
            this.label170.TabIndex = 24;
            this.label170.Text = "Position";
            this.label170.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label169.ForeColor = System.Drawing.Color.Black;
            this.label169.Location = new System.Drawing.Point(81, 134);
            this.label169.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(29, 14);
            this.label169.TabIndex = 21;
            this.label169.Text = "Age";
            this.label169.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnShowEmployeeRecruitment
            // 
            this.BtnShowEmployeeRecruitment.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnShowEmployeeRecruitment.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnShowEmployeeRecruitment.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnShowEmployeeRecruitment.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnShowEmployeeRecruitment.Appearance.Options.UseBackColor = true;
            this.BtnShowEmployeeRecruitment.Appearance.Options.UseFont = true;
            this.BtnShowEmployeeRecruitment.Appearance.Options.UseForeColor = true;
            this.BtnShowEmployeeRecruitment.Appearance.Options.UseTextOptions = true;
            this.BtnShowEmployeeRecruitment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnShowEmployeeRecruitment.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnShowEmployeeRecruitment.Image = ((System.Drawing.Image)(resources.GetObject("BtnShowEmployeeRecruitment.Image")));
            this.BtnShowEmployeeRecruitment.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnShowEmployeeRecruitment.Location = new System.Drawing.Point(340, 66);
            this.BtnShowEmployeeRecruitment.Name = "BtnShowEmployeeRecruitment";
            this.BtnShowEmployeeRecruitment.Size = new System.Drawing.Size(24, 21);
            this.BtnShowEmployeeRecruitment.TabIndex = 16;
            this.BtnShowEmployeeRecruitment.ToolTip = "Show Employee Recruitment";
            this.BtnShowEmployeeRecruitment.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnShowEmployeeRecruitment.ToolTipTitle = "Run System";
            this.BtnShowEmployeeRecruitment.Click += new System.EventHandler(this.BtnShowEmployeeRecruitment_Click);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(77, 29);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(33, 14);
            this.label71.TabIndex = 12;
            this.label71.Text = "Date";
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(117, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(111, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(46, 8);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(64, 14);
            this.label72.TabIndex = 10;
            this.label72.Text = "Document";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(63, 112);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(47, 14);
            this.label66.TabIndex = 19;
            this.label66.Text = "Gender";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(72, 92);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(6, 27);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(202, 14);
            this.label53.TabIndex = 30;
            this.label53.Text = "1. Have you ever been seriously ill ?";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 225);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 364);
            this.panel4.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgAmnesis);
            this.tabControl1.Controls.Add(this.TpgFisik);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(772, 364);
            this.tabControl1.TabIndex = 14;
            // 
            // TpgAmnesis
            // 
            this.TpgAmnesis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgAmnesis.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgAmnesis.Controls.Add(this.TxtDN);
            this.TpgAmnesis.Controls.Add(this.TxtTD);
            this.TpgAmnesis.Controls.Add(this.TxtTB);
            this.TpgAmnesis.Controls.Add(this.TxtBB);
            this.TpgAmnesis.Controls.Add(this.label14);
            this.TpgAmnesis.Controls.Add(this.label15);
            this.TpgAmnesis.Controls.Add(this.label16);
            this.TpgAmnesis.Controls.Add(this.LblTN);
            this.TpgAmnesis.Controls.Add(this.label13);
            this.TpgAmnesis.Controls.Add(this.label12);
            this.TpgAmnesis.Controls.Add(this.label10);
            this.TpgAmnesis.Controls.Add(this.label6);
            this.TpgAmnesis.Controls.Add(this.Lue05);
            this.TpgAmnesis.Controls.Add(this.Lue04);
            this.TpgAmnesis.Controls.Add(this.TxtPenyakit03);
            this.TpgAmnesis.Controls.Add(this.Lue03);
            this.TpgAmnesis.Controls.Add(this.TxtPenyakit02);
            this.TpgAmnesis.Controls.Add(this.Lue02);
            this.TpgAmnesis.Controls.Add(this.label11);
            this.TpgAmnesis.Controls.Add(this.label9);
            this.TpgAmnesis.Controls.Add(this.label7);
            this.TpgAmnesis.Controls.Add(this.label5);
            this.TpgAmnesis.Controls.Add(this.label4);
            this.TpgAmnesis.Controls.Add(this.label3);
            this.TpgAmnesis.Controls.Add(this.label53);
            this.TpgAmnesis.Controls.Add(this.Lue01);
            this.TpgAmnesis.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgAmnesis.Location = new System.Drawing.Point(4, 26);
            this.TpgAmnesis.Name = "TpgAmnesis";
            this.TpgAmnesis.Size = new System.Drawing.Size(764, 334);
            this.TpgAmnesis.TabIndex = 0;
            this.TpgAmnesis.Text = "Anamnesis";
            this.TpgAmnesis.UseVisualStyleBackColor = true;
            // 
            // TxtDN
            // 
            this.TxtDN.EnterMoveNextControl = true;
            this.TxtDN.Location = new System.Drawing.Point(335, 236);
            this.TxtDN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDN.Name = "TxtDN";
            this.TxtDN.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDN.Properties.Appearance.Options.UseFont = true;
            this.TxtDN.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDN.Size = new System.Drawing.Size(94, 20);
            this.TxtDN.TabIndex = 59;
            this.TxtDN.Validated += new System.EventHandler(this.TxtDN_Validated);
            // 
            // TxtTD
            // 
            this.TxtTD.EnterMoveNextControl = true;
            this.TxtTD.Location = new System.Drawing.Point(335, 215);
            this.TxtTD.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTD.Name = "TxtTD";
            this.TxtTD.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTD.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTD.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTD.Properties.Appearance.Options.UseFont = true;
            this.TxtTD.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTD.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTD.Size = new System.Drawing.Size(94, 20);
            this.TxtTD.TabIndex = 58;
            this.TxtTD.Validated += new System.EventHandler(this.TxtTD_Validated);
            // 
            // TxtTB
            // 
            this.TxtTB.EnterMoveNextControl = true;
            this.TxtTB.Location = new System.Drawing.Point(82, 236);
            this.TxtTB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTB.Name = "TxtTB";
            this.TxtTB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTB.Properties.Appearance.Options.UseFont = true;
            this.TxtTB.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTB.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTB.Size = new System.Drawing.Size(94, 20);
            this.TxtTB.TabIndex = 57;
            this.TxtTB.Validated += new System.EventHandler(this.TxtTB_Validated);
            // 
            // TxtBB
            // 
            this.TxtBB.EnterMoveNextControl = true;
            this.TxtBB.Location = new System.Drawing.Point(82, 215);
            this.TxtBB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBB.Name = "TxtBB";
            this.TxtBB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBB.Properties.Appearance.Options.UseFont = true;
            this.TxtBB.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtBB.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtBB.Size = new System.Drawing.Size(94, 20);
            this.TxtBB.TabIndex = 56;
            this.TxtBB.Validated += new System.EventHandler(this.TxtBB_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(433, 218);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 14);
            this.label14.TabIndex = 52;
            this.label14.Text = "mmHg";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(433, 239);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 14);
            this.label15.TabIndex = 55;
            this.label15.Text = "X / minute";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(296, 239);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 14);
            this.label16.TabIndex = 53;
            this.label16.Text = "Pulse";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblTN
            // 
            this.LblTN.AutoSize = true;
            this.LblTN.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTN.ForeColor = System.Drawing.Color.Black;
            this.LblTN.Location = new System.Drawing.Point(244, 218);
            this.LblTN.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblTN.Name = "LblTN";
            this.LblTN.Size = new System.Drawing.Size(87, 14);
            this.LblTN.TabIndex = 50;
            this.LblTN.Text = "Blood Pressure";
            this.LblTN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(181, 216);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 14);
            this.label13.TabIndex = 46;
            this.label13.Text = "Kg";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(181, 237);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 14);
            this.label12.TabIndex = 49;
            this.label12.Text = "Cm";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(33, 237);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 14);
            this.label10.TabIndex = 47;
            this.label10.Text = "Height";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(29, 216);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 44;
            this.label6.Text = "Weight";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Lue05
            // 
            this.Lue05.EnterMoveNextControl = true;
            this.Lue05.Location = new System.Drawing.Point(295, 151);
            this.Lue05.Margin = new System.Windows.Forms.Padding(5);
            this.Lue05.Name = "Lue05";
            this.Lue05.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue05.Properties.Appearance.Options.UseFont = true;
            this.Lue05.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue05.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Lue05.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue05.Properties.AppearanceDropDown.Options.UseFont = true;
            this.Lue05.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue05.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.Lue05.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue05.Properties.AppearanceFocused.Options.UseFont = true;
            this.Lue05.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Lue05.Properties.DropDownRows = 25;
            this.Lue05.Properties.MaxLength = 2;
            this.Lue05.Properties.NullText = "[Empty]";
            this.Lue05.Properties.PopupWidth = 500;
            this.Lue05.Size = new System.Drawing.Size(223, 20);
            this.Lue05.TabIndex = 43;
            this.Lue05.ToolTip = "F4 : Show/hide list";
            this.Lue05.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.Lue05.EditValueChanged += new System.EventHandler(this.Lue05_EditValueChanged);
            // 
            // Lue04
            // 
            this.Lue04.EnterMoveNextControl = true;
            this.Lue04.Location = new System.Drawing.Point(295, 130);
            this.Lue04.Margin = new System.Windows.Forms.Padding(5);
            this.Lue04.Name = "Lue04";
            this.Lue04.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue04.Properties.Appearance.Options.UseFont = true;
            this.Lue04.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue04.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Lue04.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue04.Properties.AppearanceDropDown.Options.UseFont = true;
            this.Lue04.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue04.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.Lue04.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue04.Properties.AppearanceFocused.Options.UseFont = true;
            this.Lue04.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Lue04.Properties.DropDownRows = 25;
            this.Lue04.Properties.MaxLength = 2;
            this.Lue04.Properties.NullText = "[Empty]";
            this.Lue04.Properties.PopupWidth = 500;
            this.Lue04.Size = new System.Drawing.Size(223, 20);
            this.Lue04.TabIndex = 41;
            this.Lue04.ToolTip = "F4 : Show/hide list";
            this.Lue04.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.Lue04.EditValueChanged += new System.EventHandler(this.Lue04_EditValueChanged);
            // 
            // TxtPenyakit03
            // 
            this.TxtPenyakit03.EnterMoveNextControl = true;
            this.TxtPenyakit03.Location = new System.Drawing.Point(295, 109);
            this.TxtPenyakit03.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPenyakit03.Name = "TxtPenyakit03";
            this.TxtPenyakit03.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPenyakit03.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPenyakit03.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPenyakit03.Properties.Appearance.Options.UseFont = true;
            this.TxtPenyakit03.Properties.MaxLength = 40;
            this.TxtPenyakit03.Size = new System.Drawing.Size(223, 20);
            this.TxtPenyakit03.TabIndex = 39;
            // 
            // Lue03
            // 
            this.Lue03.EnterMoveNextControl = true;
            this.Lue03.Location = new System.Drawing.Point(295, 88);
            this.Lue03.Margin = new System.Windows.Forms.Padding(5);
            this.Lue03.Name = "Lue03";
            this.Lue03.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue03.Properties.Appearance.Options.UseFont = true;
            this.Lue03.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue03.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Lue03.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue03.Properties.AppearanceDropDown.Options.UseFont = true;
            this.Lue03.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue03.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.Lue03.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue03.Properties.AppearanceFocused.Options.UseFont = true;
            this.Lue03.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Lue03.Properties.DropDownRows = 25;
            this.Lue03.Properties.MaxLength = 2;
            this.Lue03.Properties.NullText = "[Empty]";
            this.Lue03.Properties.PopupWidth = 500;
            this.Lue03.Size = new System.Drawing.Size(223, 20);
            this.Lue03.TabIndex = 37;
            this.Lue03.ToolTip = "F4 : Show/hide list";
            this.Lue03.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.Lue03.EditValueChanged += new System.EventHandler(this.Lue03_EditValueChanged);
            // 
            // TxtPenyakit02
            // 
            this.TxtPenyakit02.EnterMoveNextControl = true;
            this.TxtPenyakit02.Location = new System.Drawing.Point(295, 67);
            this.TxtPenyakit02.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPenyakit02.Name = "TxtPenyakit02";
            this.TxtPenyakit02.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPenyakit02.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPenyakit02.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPenyakit02.Properties.Appearance.Options.UseFont = true;
            this.TxtPenyakit02.Properties.MaxLength = 40;
            this.TxtPenyakit02.Size = new System.Drawing.Size(223, 20);
            this.TxtPenyakit02.TabIndex = 35;
            // 
            // Lue02
            // 
            this.Lue02.EnterMoveNextControl = true;
            this.Lue02.Location = new System.Drawing.Point(295, 46);
            this.Lue02.Margin = new System.Windows.Forms.Padding(5);
            this.Lue02.Name = "Lue02";
            this.Lue02.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue02.Properties.Appearance.Options.UseFont = true;
            this.Lue02.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue02.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Lue02.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue02.Properties.AppearanceDropDown.Options.UseFont = true;
            this.Lue02.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue02.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.Lue02.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue02.Properties.AppearanceFocused.Options.UseFont = true;
            this.Lue02.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Lue02.Properties.DropDownRows = 25;
            this.Lue02.Properties.MaxLength = 2;
            this.Lue02.Properties.NullText = "[Empty]";
            this.Lue02.Properties.PopupWidth = 500;
            this.Lue02.Size = new System.Drawing.Size(223, 20);
            this.Lue02.TabIndex = 33;
            this.Lue02.ToolTip = "F4 : Show/hide list";
            this.Lue02.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.Lue02.EditValueChanged += new System.EventHandler(this.Lue02_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(7, 154);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(206, 14);
            this.label11.TabIndex = 42;
            this.label11.Text = "5. Have you ever been head trauma";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(22, 70);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 14);
            this.label9.TabIndex = 34;
            this.label9.Text = "If ever, what disease...?";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(7, 133);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(180, 14);
            this.label7.TabIndex = 40;
            this.label7.Text = "4. Have you ever been coughs ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(23, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 14);
            this.label5.TabIndex = 38;
            this.label5.Text = "If ever, what disease...?";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(7, 91);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 14);
            this.label4.TabIndex = 36;
            this.label4.Text = "3. Have you ever been medical surgery";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 14);
            this.label3.TabIndex = 32;
            this.label3.Text = "2. Have you ever been hospitalized ?";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Lue01
            // 
            this.Lue01.EnterMoveNextControl = true;
            this.Lue01.Location = new System.Drawing.Point(295, 25);
            this.Lue01.Margin = new System.Windows.Forms.Padding(5);
            this.Lue01.Name = "Lue01";
            this.Lue01.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue01.Properties.Appearance.Options.UseFont = true;
            this.Lue01.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue01.Properties.AppearanceDisabled.Options.UseFont = true;
            this.Lue01.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue01.Properties.AppearanceDropDown.Options.UseFont = true;
            this.Lue01.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue01.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.Lue01.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lue01.Properties.AppearanceFocused.Options.UseFont = true;
            this.Lue01.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Lue01.Properties.DropDownRows = 25;
            this.Lue01.Properties.MaxLength = 2;
            this.Lue01.Properties.NullText = "[Empty]";
            this.Lue01.Properties.PopupWidth = 500;
            this.Lue01.Size = new System.Drawing.Size(223, 20);
            this.Lue01.TabIndex = 31;
            this.Lue01.ToolTip = "F4 : Show/hide list";
            this.Lue01.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.Lue01.EditValueChanged += new System.EventHandler(this.Lue01_EditValueChanged);
            // 
            // TpgFisik
            // 
            this.TpgFisik.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgFisik.Controls.Add(this.MeeConclusion);
            this.TpgFisik.Controls.Add(this.label51);
            this.TpgFisik.Controls.Add(this.MeeClinic);
            this.TpgFisik.Controls.Add(this.label52);
            this.TpgFisik.Controls.Add(this.MeeLab);
            this.TpgFisik.Controls.Add(this.label54);
            this.TpgFisik.Controls.Add(this.MeeEkstremitas);
            this.TpgFisik.Controls.Add(this.label47);
            this.TpgFisik.Controls.Add(this.MeeStomach);
            this.TpgFisik.Controls.Add(this.label48);
            this.TpgFisik.Controls.Add(this.MeeLiver);
            this.TpgFisik.Controls.Add(this.label49);
            this.TpgFisik.Controls.Add(this.MeeHeart);
            this.TpgFisik.Controls.Add(this.label50);
            this.TpgFisik.Controls.Add(this.MeeNeck);
            this.TpgFisik.Controls.Add(this.label21);
            this.TpgFisik.Controls.Add(this.MeeEar);
            this.TpgFisik.Controls.Add(this.label22);
            this.TpgFisik.Controls.Add(this.MeeMouth);
            this.TpgFisik.Controls.Add(this.label23);
            this.TpgFisik.Controls.Add(this.MeeNose);
            this.TpgFisik.Controls.Add(this.label24);
            this.TpgFisik.Controls.Add(this.MeeEye);
            this.TpgFisik.Controls.Add(this.label20);
            this.TpgFisik.Controls.Add(this.MeeNutrition);
            this.TpgFisik.Controls.Add(this.label19);
            this.TpgFisik.Controls.Add(this.MeeAwareness);
            this.TpgFisik.Controls.Add(this.label18);
            this.TpgFisik.Controls.Add(this.MeeCondition);
            this.TpgFisik.Controls.Add(this.label160);
            this.TpgFisik.Controls.Add(this.label159);
            this.TpgFisik.Controls.Add(this.label8);
            this.TpgFisik.Location = new System.Drawing.Point(4, 26);
            this.TpgFisik.Name = "TpgFisik";
            this.TpgFisik.Size = new System.Drawing.Size(764, 0);
            this.TpgFisik.TabIndex = 8;
            this.TpgFisik.Text = "Physical";
            this.TpgFisik.UseVisualStyleBackColor = true;
            // 
            // MeeConclusion
            // 
            this.MeeConclusion.EnterMoveNextControl = true;
            this.MeeConclusion.Location = new System.Drawing.Point(116, 305);
            this.MeeConclusion.Margin = new System.Windows.Forms.Padding(5);
            this.MeeConclusion.Name = "MeeConclusion";
            this.MeeConclusion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConclusion.Properties.Appearance.Options.UseFont = true;
            this.MeeConclusion.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConclusion.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeConclusion.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConclusion.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeConclusion.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConclusion.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeConclusion.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeConclusion.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeConclusion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeConclusion.Properties.MaxLength = 80;
            this.MeeConclusion.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeConclusion.Properties.ShowIcon = false;
            this.MeeConclusion.Size = new System.Drawing.Size(303, 20);
            this.MeeConclusion.TabIndex = 89;
            this.MeeConclusion.ToolTip = "F4 : Show/hide text";
            this.MeeConclusion.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeConclusion.ToolTipTitle = "Run System";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(47, 308);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(64, 14);
            this.label51.TabIndex = 88;
            this.label51.Text = "Conclusion";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeClinic
            // 
            this.MeeClinic.EnterMoveNextControl = true;
            this.MeeClinic.Location = new System.Drawing.Point(116, 284);
            this.MeeClinic.Margin = new System.Windows.Forms.Padding(5);
            this.MeeClinic.Name = "MeeClinic";
            this.MeeClinic.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeClinic.Properties.Appearance.Options.UseFont = true;
            this.MeeClinic.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeClinic.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeClinic.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeClinic.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeClinic.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeClinic.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeClinic.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeClinic.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeClinic.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeClinic.Properties.MaxLength = 80;
            this.MeeClinic.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeClinic.Properties.ShowIcon = false;
            this.MeeClinic.Size = new System.Drawing.Size(303, 20);
            this.MeeClinic.TabIndex = 87;
            this.MeeClinic.ToolTip = "F4 : Show/hide text";
            this.MeeClinic.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeClinic.ToolTipTitle = "Run System";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(47, 287);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(64, 14);
            this.label52.TabIndex = 86;
            this.label52.Text = "Clinic Note";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeLab
            // 
            this.MeeLab.EnterMoveNextControl = true;
            this.MeeLab.Location = new System.Drawing.Point(116, 263);
            this.MeeLab.Margin = new System.Windows.Forms.Padding(5);
            this.MeeLab.Name = "MeeLab";
            this.MeeLab.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLab.Properties.Appearance.Options.UseFont = true;
            this.MeeLab.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLab.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeLab.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLab.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeLab.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLab.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeLab.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLab.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeLab.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeLab.Properties.MaxLength = 80;
            this.MeeLab.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeLab.Properties.ShowIcon = false;
            this.MeeLab.Size = new System.Drawing.Size(303, 20);
            this.MeeLab.TabIndex = 85;
            this.MeeLab.ToolTip = "F4 : Show/hide text";
            this.MeeLab.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeLab.ToolTipTitle = "Run System";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(33, 266);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(78, 14);
            this.label54.TabIndex = 84;
            this.label54.Text = "Laboratorium";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeEkstremitas
            // 
            this.MeeEkstremitas.EnterMoveNextControl = true;
            this.MeeEkstremitas.Location = new System.Drawing.Point(116, 242);
            this.MeeEkstremitas.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEkstremitas.Name = "MeeEkstremitas";
            this.MeeEkstremitas.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEkstremitas.Properties.Appearance.Options.UseFont = true;
            this.MeeEkstremitas.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEkstremitas.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEkstremitas.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEkstremitas.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEkstremitas.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEkstremitas.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEkstremitas.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEkstremitas.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEkstremitas.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEkstremitas.Properties.MaxLength = 80;
            this.MeeEkstremitas.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEkstremitas.Properties.ShowIcon = false;
            this.MeeEkstremitas.Size = new System.Drawing.Size(303, 20);
            this.MeeEkstremitas.TabIndex = 83;
            this.MeeEkstremitas.ToolTip = "F4 : Show/hide text";
            this.MeeEkstremitas.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEkstremitas.ToolTipTitle = "Run System";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(42, 245);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(69, 14);
            this.label47.TabIndex = 82;
            this.label47.Text = "Ekstremitas";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeStomach
            // 
            this.MeeStomach.EnterMoveNextControl = true;
            this.MeeStomach.Location = new System.Drawing.Point(116, 221);
            this.MeeStomach.Margin = new System.Windows.Forms.Padding(5);
            this.MeeStomach.Name = "MeeStomach";
            this.MeeStomach.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeStomach.Properties.Appearance.Options.UseFont = true;
            this.MeeStomach.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeStomach.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeStomach.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeStomach.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeStomach.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeStomach.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeStomach.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeStomach.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeStomach.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeStomach.Properties.MaxLength = 80;
            this.MeeStomach.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeStomach.Properties.ShowIcon = false;
            this.MeeStomach.Size = new System.Drawing.Size(303, 20);
            this.MeeStomach.TabIndex = 81;
            this.MeeStomach.ToolTip = "F4 : Show/hide text";
            this.MeeStomach.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeStomach.ToolTipTitle = "Run System";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(56, 224);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(55, 14);
            this.label48.TabIndex = 80;
            this.label48.Text = "Stomach";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeLiver
            // 
            this.MeeLiver.EnterMoveNextControl = true;
            this.MeeLiver.Location = new System.Drawing.Point(116, 200);
            this.MeeLiver.Margin = new System.Windows.Forms.Padding(5);
            this.MeeLiver.Name = "MeeLiver";
            this.MeeLiver.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLiver.Properties.Appearance.Options.UseFont = true;
            this.MeeLiver.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLiver.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeLiver.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLiver.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeLiver.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLiver.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeLiver.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeLiver.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeLiver.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeLiver.Properties.MaxLength = 80;
            this.MeeLiver.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeLiver.Properties.ShowIcon = false;
            this.MeeLiver.Size = new System.Drawing.Size(303, 20);
            this.MeeLiver.TabIndex = 79;
            this.MeeLiver.ToolTip = "F4 : Show/hide text";
            this.MeeLiver.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeLiver.ToolTipTitle = "Run System";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(79, 203);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(32, 14);
            this.label49.TabIndex = 78;
            this.label49.Text = "Liver";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeHeart
            // 
            this.MeeHeart.EnterMoveNextControl = true;
            this.MeeHeart.Location = new System.Drawing.Point(116, 179);
            this.MeeHeart.Margin = new System.Windows.Forms.Padding(5);
            this.MeeHeart.Name = "MeeHeart";
            this.MeeHeart.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeHeart.Properties.Appearance.Options.UseFont = true;
            this.MeeHeart.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeHeart.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeHeart.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeHeart.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeHeart.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeHeart.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeHeart.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeHeart.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeHeart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeHeart.Properties.MaxLength = 80;
            this.MeeHeart.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeHeart.Properties.ShowIcon = false;
            this.MeeHeart.Size = new System.Drawing.Size(303, 20);
            this.MeeHeart.TabIndex = 77;
            this.MeeHeart.ToolTip = "F4 : Show/hide text";
            this.MeeHeart.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeHeart.ToolTipTitle = "Run System";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(74, 182);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(37, 14);
            this.label50.TabIndex = 76;
            this.label50.Text = "Heart";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeNeck
            // 
            this.MeeNeck.EnterMoveNextControl = true;
            this.MeeNeck.Location = new System.Drawing.Point(116, 158);
            this.MeeNeck.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNeck.Name = "MeeNeck";
            this.MeeNeck.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNeck.Properties.Appearance.Options.UseFont = true;
            this.MeeNeck.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNeck.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNeck.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNeck.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNeck.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNeck.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNeck.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNeck.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNeck.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNeck.Properties.MaxLength = 80;
            this.MeeNeck.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeNeck.Properties.ShowIcon = false;
            this.MeeNeck.Size = new System.Drawing.Size(303, 20);
            this.MeeNeck.TabIndex = 75;
            this.MeeNeck.ToolTip = "F4 : Show/hide text";
            this.MeeNeck.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNeck.ToolTipTitle = "Run System";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(77, 161);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 14);
            this.label21.TabIndex = 74;
            this.label21.Text = "Neck";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeEar
            // 
            this.MeeEar.EnterMoveNextControl = true;
            this.MeeEar.Location = new System.Drawing.Point(116, 137);
            this.MeeEar.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEar.Name = "MeeEar";
            this.MeeEar.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEar.Properties.Appearance.Options.UseFont = true;
            this.MeeEar.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEar.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEar.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEar.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEar.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEar.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEar.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEar.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEar.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEar.Properties.MaxLength = 80;
            this.MeeEar.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEar.Properties.ShowIcon = false;
            this.MeeEar.Size = new System.Drawing.Size(303, 20);
            this.MeeEar.TabIndex = 73;
            this.MeeEar.ToolTip = "F4 : Show/hide text";
            this.MeeEar.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEar.ToolTipTitle = "Run System";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(87, 140);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(24, 14);
            this.label22.TabIndex = 72;
            this.label22.Text = "Ear";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeMouth
            // 
            this.MeeMouth.EnterMoveNextControl = true;
            this.MeeMouth.Location = new System.Drawing.Point(116, 116);
            this.MeeMouth.Margin = new System.Windows.Forms.Padding(5);
            this.MeeMouth.Name = "MeeMouth";
            this.MeeMouth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMouth.Properties.Appearance.Options.UseFont = true;
            this.MeeMouth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMouth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeMouth.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMouth.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeMouth.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMouth.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeMouth.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeMouth.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeMouth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeMouth.Properties.MaxLength = 80;
            this.MeeMouth.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeMouth.Properties.ShowIcon = false;
            this.MeeMouth.Size = new System.Drawing.Size(303, 20);
            this.MeeMouth.TabIndex = 71;
            this.MeeMouth.ToolTip = "F4 : Show/hide text";
            this.MeeMouth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeMouth.ToolTipTitle = "Run System";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 119);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(106, 14);
            this.label23.TabIndex = 70;
            this.label23.Text = "Mouth And Teeth";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeNose
            // 
            this.MeeNose.EnterMoveNextControl = true;
            this.MeeNose.Location = new System.Drawing.Point(116, 95);
            this.MeeNose.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNose.Name = "MeeNose";
            this.MeeNose.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNose.Properties.Appearance.Options.UseFont = true;
            this.MeeNose.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNose.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNose.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNose.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNose.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNose.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNose.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNose.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNose.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNose.Properties.MaxLength = 80;
            this.MeeNose.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeNose.Properties.ShowIcon = false;
            this.MeeNose.Size = new System.Drawing.Size(303, 20);
            this.MeeNose.TabIndex = 69;
            this.MeeNose.ToolTip = "F4 : Show/hide text";
            this.MeeNose.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNose.ToolTipTitle = "Run System";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(77, 98);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 14);
            this.label24.TabIndex = 68;
            this.label24.Text = "Nose";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeEye
            // 
            this.MeeEye.EnterMoveNextControl = true;
            this.MeeEye.Location = new System.Drawing.Point(116, 74);
            this.MeeEye.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEye.Name = "MeeEye";
            this.MeeEye.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEye.Properties.Appearance.Options.UseFont = true;
            this.MeeEye.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEye.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEye.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEye.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEye.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEye.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEye.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEye.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEye.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEye.Properties.MaxLength = 80;
            this.MeeEye.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEye.Properties.ShowIcon = false;
            this.MeeEye.Size = new System.Drawing.Size(303, 20);
            this.MeeEye.TabIndex = 67;
            this.MeeEye.ToolTip = "F4 : Show/hide text";
            this.MeeEye.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEye.ToolTipTitle = "Run System";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(84, 77);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(27, 14);
            this.label20.TabIndex = 66;
            this.label20.Text = "Eye";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeNutrition
            // 
            this.MeeNutrition.EnterMoveNextControl = true;
            this.MeeNutrition.Location = new System.Drawing.Point(116, 53);
            this.MeeNutrition.Margin = new System.Windows.Forms.Padding(5);
            this.MeeNutrition.Name = "MeeNutrition";
            this.MeeNutrition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNutrition.Properties.Appearance.Options.UseFont = true;
            this.MeeNutrition.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNutrition.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeNutrition.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNutrition.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeNutrition.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNutrition.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeNutrition.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeNutrition.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeNutrition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeNutrition.Properties.MaxLength = 80;
            this.MeeNutrition.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeNutrition.Properties.ShowIcon = false;
            this.MeeNutrition.Size = new System.Drawing.Size(303, 20);
            this.MeeNutrition.TabIndex = 63;
            this.MeeNutrition.ToolTip = "F4 : Show/hide text";
            this.MeeNutrition.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeNutrition.ToolTipTitle = "Run System";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(57, 56);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 14);
            this.label19.TabIndex = 62;
            this.label19.Text = "Nutrition";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAwareness
            // 
            this.MeeAwareness.EnterMoveNextControl = true;
            this.MeeAwareness.Location = new System.Drawing.Point(116, 32);
            this.MeeAwareness.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAwareness.Name = "MeeAwareness";
            this.MeeAwareness.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAwareness.Properties.Appearance.Options.UseFont = true;
            this.MeeAwareness.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAwareness.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAwareness.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAwareness.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAwareness.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAwareness.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAwareness.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAwareness.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAwareness.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAwareness.Properties.MaxLength = 80;
            this.MeeAwareness.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAwareness.Properties.ShowIcon = false;
            this.MeeAwareness.Size = new System.Drawing.Size(303, 20);
            this.MeeAwareness.TabIndex = 59;
            this.MeeAwareness.ToolTip = "F4 : Show/hide text";
            this.MeeAwareness.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAwareness.ToolTipTitle = "Run System";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(45, 35);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 14);
            this.label18.TabIndex = 58;
            this.label18.Text = "Awareness";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCondition
            // 
            this.MeeCondition.EnterMoveNextControl = true;
            this.MeeCondition.Location = new System.Drawing.Point(116, 11);
            this.MeeCondition.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCondition.Name = "MeeCondition";
            this.MeeCondition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCondition.Properties.Appearance.Options.UseFont = true;
            this.MeeCondition.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCondition.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCondition.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCondition.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCondition.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCondition.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCondition.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCondition.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCondition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCondition.Properties.MaxLength = 80;
            this.MeeCondition.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCondition.Properties.ShowIcon = false;
            this.MeeCondition.Size = new System.Drawing.Size(303, 20);
            this.MeeCondition.TabIndex = 57;
            this.MeeCondition.ToolTip = "F4 : Show/hide text";
            this.MeeCondition.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCondition.ToolTipTitle = "Run System";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.ForeColor = System.Drawing.Color.Black;
            this.label160.Location = new System.Drawing.Point(12, 54);
            this.label160.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(0, 14);
            this.label160.TabIndex = 10;
            this.label160.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(8, 22);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(0, 14);
            this.label159.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(53, 14);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 14);
            this.label8.TabIndex = 56;
            this.label8.Text = "Condition";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.textEdit6);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.textEdit7);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.textEdit8);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.textEdit9);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.lookUpEdit6);
            this.tabPage1.Controls.Add(this.dateEdit1);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.textEdit10);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.lookUpEdit7);
            this.tabPage1.Controls.Add(this.textEdit11);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.dateEdit2);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.dateEdit3);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.textEdit12);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.memoExEdit1);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.label44);
            this.tabPage1.Controls.Add(this.lookUpEdit8);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.lookUpEdit9);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.lookUpEdit10);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(764, 373);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.textEdit3);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.textEdit4);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.lookUpEdit3);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.lookUpEdit4);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.lookUpEdit5);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.textEdit5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(408, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(352, 369);
            this.panel6.TabIndex = 46;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(93, 115);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.MaxLength = 80;
            this.textEdit3.Size = new System.Drawing.Size(243, 20);
            this.textEdit3.TabIndex = 57;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(17, 119);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 14);
            this.label25.TabIndex = 56;
            this.label25.Text = "Bank Branch";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit4
            // 
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(93, 93);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.MaxLength = 80;
            this.textEdit4.Size = new System.Drawing.Size(242, 20);
            this.textEdit4.TabIndex = 55;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(8, 97);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 14);
            this.label26.TabIndex = 54;
            this.label26.Text = "Bank Account";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(23, 75);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 14);
            this.label27.TabIndex = 52;
            this.label27.Text = "Bank Name";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.EnterMoveNextControl = true;
            this.lookUpEdit3.Location = new System.Drawing.Point(93, 71);
            this.lookUpEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.DropDownRows = 12;
            this.lookUpEdit3.Properties.NullText = "[Empty]";
            this.lookUpEdit3.Properties.PopupWidth = 500;
            this.lookUpEdit3.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit3.TabIndex = 53;
            this.lookUpEdit3.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(55, 53);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(36, 14);
            this.label28.TabIndex = 50;
            this.label28.Text = "PTKP";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(93, 49);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 12;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 500;
            this.lookUpEdit4.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit4.TabIndex = 51;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(18, 31);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 14);
            this.label29.TabIndex = 48;
            this.label29.Text = "Payroll Type";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit5
            // 
            this.lookUpEdit5.EnterMoveNextControl = true;
            this.lookUpEdit5.Location = new System.Drawing.Point(93, 27);
            this.lookUpEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit5.Name = "lookUpEdit5";
            this.lookUpEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit5.Properties.DropDownRows = 12;
            this.lookUpEdit5.Properties.NullText = "[Empty]";
            this.lookUpEdit5.Properties.PopupWidth = 500;
            this.lookUpEdit5.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit5.TabIndex = 49;
            this.lookUpEdit5.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(50, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 14);
            this.label30.TabIndex = 46;
            this.label30.Text = "NPWP";
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(93, 5);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.MaxLength = 40;
            this.textEdit5.Size = new System.Drawing.Size(243, 20);
            this.textEdit5.TabIndex = 47;
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(94, 334);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.MaxLength = 40;
            this.textEdit6.Size = new System.Drawing.Size(219, 20);
            this.textEdit6.TabIndex = 45;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(58, 337);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 14);
            this.label31.TabIndex = 44;
            this.label31.Text = "Email";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit7
            // 
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(94, 312);
            this.textEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.MaxLength = 40;
            this.textEdit7.Size = new System.Drawing.Size(185, 20);
            this.textEdit7.TabIndex = 43;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(51, 315);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 14);
            this.label32.TabIndex = 42;
            this.label32.Text = "Mobile";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit8
            // 
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(94, 290);
            this.textEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.MaxLength = 40;
            this.textEdit8.Size = new System.Drawing.Size(185, 20);
            this.textEdit8.TabIndex = 41;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(50, 293);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 14);
            this.label33.TabIndex = 40;
            this.label33.Text = "Phone";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit9
            // 
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(94, 268);
            this.textEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.MaxLength = 20;
            this.textEdit9.Size = new System.Drawing.Size(185, 20);
            this.textEdit9.TabIndex = 39;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(21, 271);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 14);
            this.label34.TabIndex = 38;
            this.label34.Text = "Postal Code";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(65, 249);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(27, 14);
            this.label35.TabIndex = 36;
            this.label35.Text = "City";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit6
            // 
            this.lookUpEdit6.EnterMoveNextControl = true;
            this.lookUpEdit6.Location = new System.Drawing.Point(94, 246);
            this.lookUpEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit6.Name = "lookUpEdit6";
            this.lookUpEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit6.Properties.DropDownRows = 12;
            this.lookUpEdit6.Properties.MaxLength = 16;
            this.lookUpEdit6.Properties.NullText = "[Empty]";
            this.lookUpEdit6.Properties.PopupWidth = 500;
            this.lookUpEdit6.Size = new System.Drawing.Size(295, 20);
            this.lookUpEdit6.TabIndex = 37;
            this.lookUpEdit6.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.EnterMoveNextControl = true;
            this.dateEdit1.Location = new System.Drawing.Point(94, 202);
            this.dateEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit1.Properties.MaxLength = 8;
            this.dateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(102, 20);
            this.dateEdit1.TabIndex = 33;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(30, 205);
            this.label36.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(62, 14);
            this.label36.TabIndex = 32;
            this.label36.Text = "Birth Date";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit10
            // 
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(94, 180);
            this.textEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.MaxLength = 80;
            this.textEdit10.Size = new System.Drawing.Size(298, 20);
            this.textEdit10.TabIndex = 31;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(28, 183);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 14);
            this.label37.TabIndex = 30;
            this.label37.Text = "Birth Place";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(44, 161);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(48, 14);
            this.label38.TabIndex = 28;
            this.label38.Text = "Religion";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit7
            // 
            this.lookUpEdit7.EnterMoveNextControl = true;
            this.lookUpEdit7.Location = new System.Drawing.Point(94, 158);
            this.lookUpEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit7.Name = "lookUpEdit7";
            this.lookUpEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit7.Properties.DropDownRows = 12;
            this.lookUpEdit7.Properties.MaxLength = 2;
            this.lookUpEdit7.Properties.NullText = "[Empty]";
            this.lookUpEdit7.Properties.PopupWidth = 500;
            this.lookUpEdit7.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit7.TabIndex = 29;
            this.lookUpEdit7.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit11
            // 
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(94, 114);
            this.textEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.MaxLength = 40;
            this.textEdit11.Size = new System.Drawing.Size(299, 20);
            this.textEdit11.TabIndex = 25;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(27, 117);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 14);
            this.label39.TabIndex = 24;
            this.label39.Text = "Id Number";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.EnterMoveNextControl = true;
            this.dateEdit2.Location = new System.Drawing.Point(94, 92);
            this.dateEdit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.Appearance.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit2.Properties.MaxLength = 16;
            this.dateEdit2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit2.Size = new System.Drawing.Size(99, 20);
            this.dateEdit2.TabIndex = 23;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(20, 95);
            this.label40.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(72, 14);
            this.label40.TabIndex = 22;
            this.label40.Text = "Resign Date";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit3
            // 
            this.dateEdit3.EditValue = null;
            this.dateEdit3.EnterMoveNextControl = true;
            this.dateEdit3.Location = new System.Drawing.Point(94, 70);
            this.dateEdit3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit3.Name = "dateEdit3";
            this.dateEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.Appearance.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit3.Properties.MaxLength = 16;
            this.dateEdit3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit3.Size = new System.Drawing.Size(99, 20);
            this.dateEdit3.TabIndex = 21;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(34, 73);
            this.label41.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 14);
            this.label41.TabIndex = 20;
            this.label41.Text = "Join Date";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit12
            // 
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(94, 4);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.MaxLength = 16;
            this.textEdit12.Size = new System.Drawing.Size(136, 20);
            this.textEdit12.TabIndex = 15;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(29, 7);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(63, 14);
            this.label42.TabIndex = 14;
            this.label42.Text = "User Code";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit1
            // 
            this.memoExEdit1.EnterMoveNextControl = true;
            this.memoExEdit1.Location = new System.Drawing.Point(94, 224);
            this.memoExEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit1.Name = "memoExEdit1";
            this.memoExEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit1.Properties.MaxLength = 80;
            this.memoExEdit1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit1.Properties.ShowIcon = false;
            this.memoExEdit1.Size = new System.Drawing.Size(295, 20);
            this.memoExEdit1.TabIndex = 35;
            this.memoExEdit1.ToolTip = "F4 : Show/hide text";
            this.memoExEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit1.ToolTipTitle = "Run System";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(42, 227);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(50, 14);
            this.label43.TabIndex = 34;
            this.label43.Text = "Address";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(45, 139);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(47, 14);
            this.label44.TabIndex = 26;
            this.label44.Text = "Gender";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit8
            // 
            this.lookUpEdit8.EnterMoveNextControl = true;
            this.lookUpEdit8.Location = new System.Drawing.Point(94, 136);
            this.lookUpEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit8.Name = "lookUpEdit8";
            this.lookUpEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit8.Properties.DropDownRows = 12;
            this.lookUpEdit8.Properties.MaxLength = 1;
            this.lookUpEdit8.Properties.NullText = "[Empty]";
            this.lookUpEdit8.Properties.PopupWidth = 500;
            this.lookUpEdit8.Size = new System.Drawing.Size(99, 20);
            this.lookUpEdit8.TabIndex = 27;
            this.lookUpEdit8.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(43, 51);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(49, 14);
            this.label45.TabIndex = 18;
            this.label45.Text = "Position";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit9
            // 
            this.lookUpEdit9.EnterMoveNextControl = true;
            this.lookUpEdit9.Location = new System.Drawing.Point(94, 48);
            this.lookUpEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit9.Name = "lookUpEdit9";
            this.lookUpEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit9.Properties.DropDownRows = 12;
            this.lookUpEdit9.Properties.MaxLength = 16;
            this.lookUpEdit9.Properties.NullText = "[Empty]";
            this.lookUpEdit9.Properties.PopupWidth = 500;
            this.lookUpEdit9.Size = new System.Drawing.Size(177, 20);
            this.lookUpEdit9.TabIndex = 19;
            this.lookUpEdit9.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(12, 29);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(80, 14);
            this.label46.TabIndex = 16;
            this.label46.Text = "Departement";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit10
            // 
            this.lookUpEdit10.EnterMoveNextControl = true;
            this.lookUpEdit10.Location = new System.Drawing.Point(94, 26);
            this.lookUpEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit10.Name = "lookUpEdit10";
            this.lookUpEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit10.Properties.DropDownRows = 12;
            this.lookUpEdit10.Properties.MaxLength = 16;
            this.lookUpEdit10.Properties.NullText = "[Empty]";
            this.lookUpEdit10.Properties.PopupWidth = 500;
            this.lookUpEdit10.Size = new System.Drawing.Size(223, 20);
            this.lookUpEdit10.TabIndex = 17;
            this.lookUpEdit10.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage2.Controls.Add(this.dateEdit4);
            this.tabPage2.Controls.Add(this.lookUpEdit11);
            this.tabPage2.Controls.Add(this.lookUpEdit12);
            this.tabPage2.Controls.Add(this.iGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(764, 373);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Employee Familly";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dateEdit4
            // 
            this.dateEdit4.EditValue = null;
            this.dateEdit4.EnterMoveNextControl = true;
            this.dateEdit4.Location = new System.Drawing.Point(359, 26);
            this.dateEdit4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit4.Name = "dateEdit4";
            this.dateEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.Appearance.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit4.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit4.Size = new System.Drawing.Size(104, 20);
            this.dateEdit4.TabIndex = 40;
            // 
            // lookUpEdit11
            // 
            this.lookUpEdit11.EnterMoveNextControl = true;
            this.lookUpEdit11.Location = new System.Drawing.Point(236, 25);
            this.lookUpEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit11.Name = "lookUpEdit11";
            this.lookUpEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit11.Properties.DropDownRows = 12;
            this.lookUpEdit11.Properties.NullText = "[Empty]";
            this.lookUpEdit11.Properties.PopupWidth = 500;
            this.lookUpEdit11.Size = new System.Drawing.Size(122, 20);
            this.lookUpEdit11.TabIndex = 37;
            this.lookUpEdit11.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit12
            // 
            this.lookUpEdit12.EnterMoveNextControl = true;
            this.lookUpEdit12.Location = new System.Drawing.Point(104, 24);
            this.lookUpEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit12.Name = "lookUpEdit12";
            this.lookUpEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit12.Properties.DropDownRows = 12;
            this.lookUpEdit12.Properties.NullText = "[Empty]";
            this.lookUpEdit12.Properties.PopupWidth = 500;
            this.lookUpEdit12.Size = new System.Drawing.Size(131, 20);
            this.lookUpEdit12.TabIndex = 36;
            this.lookUpEdit12.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // iGrid1
            // 
            this.iGrid1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid1.DefaultRow.Height = 20;
            this.iGrid1.DefaultRow.Sortable = false;
            this.iGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid1.Header.Height = 19;
            this.iGrid1.Location = new System.Drawing.Point(0, 0);
            this.iGrid1.Name = "iGrid1";
            this.iGrid1.ReadOnly = true;
            this.iGrid1.RowHeader.Visible = true;
            this.iGrid1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid1.SingleClickEdit = true;
            this.iGrid1.Size = new System.Drawing.Size(760, 369);
            this.iGrid1.TabIndex = 35;
            this.iGrid1.TreeCol = null;
            this.iGrid1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage3.Controls.Add(this.iGrid2);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 373);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Employee Work Exp";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // iGrid2
            // 
            this.iGrid2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid2.DefaultRow.Height = 20;
            this.iGrid2.DefaultRow.Sortable = false;
            this.iGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid2.Header.Height = 19;
            this.iGrid2.Location = new System.Drawing.Point(0, 0);
            this.iGrid2.Name = "iGrid2";
            this.iGrid2.RowHeader.Visible = true;
            this.iGrid2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid2.SingleClickEdit = true;
            this.iGrid2.Size = new System.Drawing.Size(764, 373);
            this.iGrid2.TabIndex = 47;
            this.iGrid2.TreeCol = null;
            this.iGrid2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(125, 237);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.MaxLength = 40;
            this.textEdit1.Size = new System.Drawing.Size(257, 20);
            this.textEdit1.TabIndex = 44;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(44, 240);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 14);
            this.label1.TabIndex = 43;
            this.label1.Text = "Point of Hire";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(74, 135);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(46, 14);
            this.label70.TabIndex = 33;
            this.label70.Text = "Division";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EnterMoveNextControl = true;
            this.lookUpEdit1.Location = new System.Drawing.Point(125, 132);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DropDownRows = 25;
            this.lookUpEdit1.Properties.MaxLength = 16;
            this.lookUpEdit1.Properties.NullText = "[Empty]";
            this.lookUpEdit1.Properties.PopupWidth = 500;
            this.lookUpEdit1.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit1.TabIndex = 34;
            this.lookUpEdit1.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(31, 219);
            this.label73.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(89, 14);
            this.label73.TabIndex = 41;
            this.label73.Text = "Working Group";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EnterMoveNextControl = true;
            this.lookUpEdit2.Location = new System.Drawing.Point(125, 216);
            this.lookUpEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.DropDownRows = 25;
            this.lookUpEdit2.Properties.MaxLength = 16;
            this.lookUpEdit2.Properties.NullText = "[Empty]";
            this.lookUpEdit2.Properties.PopupWidth = 500;
            this.lookUpEdit2.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit2.TabIndex = 42;
            this.lookUpEdit2.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(72, 197);
            this.label74.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(48, 14);
            this.label74.TabIndex = 39;
            this.label74.Text = "Section";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit13
            // 
            this.lookUpEdit13.EnterMoveNextControl = true;
            this.lookUpEdit13.Location = new System.Drawing.Point(125, 195);
            this.lookUpEdit13.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit13.Name = "lookUpEdit13";
            this.lookUpEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit13.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit13.Properties.DropDownRows = 25;
            this.lookUpEdit13.Properties.MaxLength = 16;
            this.lookUpEdit13.Properties.NullText = "[Empty]";
            this.lookUpEdit13.Properties.PopupWidth = 500;
            this.lookUpEdit13.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit13.TabIndex = 40;
            this.lookUpEdit13.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(69, 429);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(51, 14);
            this.label75.TabIndex = 61;
            this.label75.Text = "Domicile";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit2
            // 
            this.memoExEdit2.EnterMoveNextControl = true;
            this.memoExEdit2.Location = new System.Drawing.Point(125, 426);
            this.memoExEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit2.Name = "memoExEdit2";
            this.memoExEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit2.Properties.MaxLength = 400;
            this.memoExEdit2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit2.Properties.ShowIcon = false;
            this.memoExEdit2.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit2.TabIndex = 62;
            this.memoExEdit2.ToolTip = "F4 : Show/hide text";
            this.memoExEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit2.ToolTipTitle = "Run System";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(92, 114);
            this.label76.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(28, 14);
            this.label76.TabIndex = 31;
            this.label76.Text = "Site";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit14
            // 
            this.lookUpEdit14.EnterMoveNextControl = true;
            this.lookUpEdit14.Location = new System.Drawing.Point(125, 111);
            this.lookUpEdit14.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit14.Name = "lookUpEdit14";
            this.lookUpEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit14.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit14.Properties.DropDownRows = 25;
            this.lookUpEdit14.Properties.MaxLength = 16;
            this.lookUpEdit14.Properties.NullText = "[Empty]";
            this.lookUpEdit14.Properties.PopupWidth = 500;
            this.lookUpEdit14.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit14.TabIndex = 32;
            this.lookUpEdit14.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit2
            // 
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(125, 384);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.MaxLength = 10;
            this.textEdit2.Size = new System.Drawing.Size(257, 20);
            this.textEdit2.TabIndex = 58;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(74, 387);
            this.label77.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(46, 14);
            this.label77.TabIndex = 57;
            this.label77.Text = "RT/RW";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit13
            // 
            this.textEdit13.EnterMoveNextControl = true;
            this.textEdit13.Location = new System.Drawing.Point(125, 363);
            this.textEdit13.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit13.Properties.Appearance.Options.UseFont = true;
            this.textEdit13.Properties.MaxLength = 40;
            this.textEdit13.Size = new System.Drawing.Size(257, 20);
            this.textEdit13.TabIndex = 56;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(79, 366);
            this.label78.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(41, 14);
            this.label78.TabIndex = 55;
            this.label78.Text = "Village";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit14
            // 
            this.textEdit14.EnterMoveNextControl = true;
            this.textEdit14.Location = new System.Drawing.Point(125, 342);
            this.textEdit14.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit14.Properties.Appearance.Options.UseFont = true;
            this.textEdit14.Properties.MaxLength = 40;
            this.textEdit14.Size = new System.Drawing.Size(257, 20);
            this.textEdit14.TabIndex = 54;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(51, 344);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(69, 14);
            this.label79.TabIndex = 53;
            this.label79.Text = "Sub District";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(6, 92);
            this.label80.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(114, 14);
            this.label80.TabIndex = 29;
            this.label80.Text = "Employment Status";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit15
            // 
            this.textEdit15.EnterMoveNextControl = true;
            this.textEdit15.Location = new System.Drawing.Point(125, 69);
            this.textEdit15.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit15.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit15.Properties.Appearance.Options.UseFont = true;
            this.textEdit15.Properties.MaxLength = 16;
            this.textEdit15.Size = new System.Drawing.Size(257, 20);
            this.textEdit15.TabIndex = 28;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(37, 71);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(83, 14);
            this.label81.TabIndex = 27;
            this.label81.Text = "Barcode Code";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit16
            // 
            this.textEdit16.EnterMoveNextControl = true;
            this.textEdit16.Location = new System.Drawing.Point(125, 48);
            this.textEdit16.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit16.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit16.Properties.Appearance.Options.UseFont = true;
            this.textEdit16.Properties.MaxLength = 3;
            this.textEdit16.Size = new System.Drawing.Size(257, 20);
            this.textEdit16.TabIndex = 26;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(51, 51);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(69, 14);
            this.label82.TabIndex = 25;
            this.label82.Text = "Short Code";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit15
            // 
            this.lookUpEdit15.EnterMoveNextControl = true;
            this.lookUpEdit15.Location = new System.Drawing.Point(125, 90);
            this.lookUpEdit15.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit15.Name = "lookUpEdit15";
            this.lookUpEdit15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit15.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit15.Properties.DropDownRows = 25;
            this.lookUpEdit15.Properties.MaxLength = 1;
            this.lookUpEdit15.Properties.NullText = "[Empty]";
            this.lookUpEdit15.Properties.PopupWidth = 500;
            this.lookUpEdit15.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit15.TabIndex = 30;
            this.lookUpEdit15.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit17
            // 
            this.textEdit17.EnterMoveNextControl = true;
            this.textEdit17.Location = new System.Drawing.Point(125, 27);
            this.textEdit17.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit17.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit17.Properties.Appearance.Options.UseFont = true;
            this.textEdit17.Properties.MaxLength = 16;
            this.textEdit17.Size = new System.Drawing.Size(257, 20);
            this.textEdit17.TabIndex = 24;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(63, 29);
            this.label83.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(57, 14);
            this.label83.TabIndex = 23;
            this.label83.Text = "Old Code";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.label84);
            this.panel7.Controls.Add(this.lookUpEdit16);
            this.panel7.Controls.Add(this.label85);
            this.panel7.Controls.Add(this.lookUpEdit17);
            this.panel7.Controls.Add(this.label86);
            this.panel7.Controls.Add(this.lookUpEdit18);
            this.panel7.Controls.Add(this.textEdit18);
            this.panel7.Controls.Add(this.label87);
            this.panel7.Controls.Add(this.label88);
            this.panel7.Controls.Add(this.lookUpEdit19);
            this.panel7.Controls.Add(this.dateEdit5);
            this.panel7.Controls.Add(this.label89);
            this.panel7.Controls.Add(this.textEdit19);
            this.panel7.Controls.Add(this.label90);
            this.panel7.Controls.Add(this.label91);
            this.panel7.Controls.Add(this.label92);
            this.panel7.Controls.Add(this.textEdit20);
            this.panel7.Controls.Add(this.lookUpEdit20);
            this.panel7.Controls.Add(this.lookUpEdit21);
            this.panel7.Controls.Add(this.dateEdit6);
            this.panel7.Controls.Add(this.textEdit21);
            this.panel7.Controls.Add(this.label93);
            this.panel7.Controls.Add(this.label94);
            this.panel7.Controls.Add(this.label95);
            this.panel7.Controls.Add(this.textEdit22);
            this.panel7.Controls.Add(this.label96);
            this.panel7.Controls.Add(this.textEdit23);
            this.panel7.Controls.Add(this.label97);
            this.panel7.Controls.Add(this.lookUpEdit22);
            this.panel7.Controls.Add(this.textEdit24);
            this.panel7.Controls.Add(this.label98);
            this.panel7.Controls.Add(this.label99);
            this.panel7.Controls.Add(this.lookUpEdit23);
            this.panel7.Controls.Add(this.label100);
            this.panel7.Controls.Add(this.lookUpEdit24);
            this.panel7.Controls.Add(this.label101);
            this.panel7.Controls.Add(this.label102);
            this.panel7.Controls.Add(this.lookUpEdit25);
            this.panel7.Controls.Add(this.label103);
            this.panel7.Controls.Add(this.label104);
            this.panel7.Controls.Add(this.textEdit25);
            this.panel7.Controls.Add(this.lookUpEdit26);
            this.panel7.Controls.Add(this.label105);
            this.panel7.Controls.Add(this.textEdit26);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(388, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(372, 494);
            this.panel7.TabIndex = 47;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Black;
            this.label84.Location = new System.Drawing.Point(31, 199);
            this.label84.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(78, 14);
            this.label84.TabIndex = 84;
            this.label84.Text = "Payroll Group";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit16
            // 
            this.lookUpEdit16.EnterMoveNextControl = true;
            this.lookUpEdit16.Location = new System.Drawing.Point(115, 195);
            this.lookUpEdit16.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit16.Name = "lookUpEdit16";
            this.lookUpEdit16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit16.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit16.Properties.DropDownRows = 25;
            this.lookUpEdit16.Properties.NullText = "[Empty]";
            this.lookUpEdit16.Properties.PopupWidth = 500;
            this.lookUpEdit16.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit16.TabIndex = 85;
            this.lookUpEdit16.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Black;
            this.label85.Location = new System.Drawing.Point(40, 115);
            this.label85.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(69, 14);
            this.label85.TabIndex = 76;
            this.label85.Text = "Blood Type";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit17
            // 
            this.lookUpEdit17.EnterMoveNextControl = true;
            this.lookUpEdit17.Location = new System.Drawing.Point(115, 111);
            this.lookUpEdit17.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit17.Name = "lookUpEdit17";
            this.lookUpEdit17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit17.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit17.Properties.DropDownRows = 25;
            this.lookUpEdit17.Properties.MaxLength = 2;
            this.lookUpEdit17.Properties.NullText = "[Empty]";
            this.lookUpEdit17.Properties.PopupWidth = 500;
            this.lookUpEdit17.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit17.TabIndex = 77;
            this.lookUpEdit17.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(30, 157);
            this.label86.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(79, 14);
            this.label86.TabIndex = 80;
            this.label86.Text = "System Type";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit18
            // 
            this.lookUpEdit18.EnterMoveNextControl = true;
            this.lookUpEdit18.Location = new System.Drawing.Point(115, 153);
            this.lookUpEdit18.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit18.Name = "lookUpEdit18";
            this.lookUpEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit18.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit18.Properties.DropDownRows = 25;
            this.lookUpEdit18.Properties.NullText = "[Empty]";
            this.lookUpEdit18.Properties.PopupWidth = 500;
            this.lookUpEdit18.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit18.TabIndex = 81;
            this.lookUpEdit18.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit18
            // 
            this.textEdit18.EnterMoveNextControl = true;
            this.textEdit18.Location = new System.Drawing.Point(115, 132);
            this.textEdit18.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit18.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit18.Properties.Appearance.Options.UseFont = true;
            this.textEdit18.Properties.MaxLength = 40;
            this.textEdit18.Size = new System.Drawing.Size(245, 20);
            this.textEdit18.TabIndex = 79;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(11, 136);
            this.label87.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(98, 14);
            this.label87.TabIndex = 78;
            this.label87.Text = "Biological Mother";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(27, 241);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(82, 14);
            this.label88.TabIndex = 88;
            this.label88.Text = "Payrun Period";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit19
            // 
            this.lookUpEdit19.EnterMoveNextControl = true;
            this.lookUpEdit19.Location = new System.Drawing.Point(115, 237);
            this.lookUpEdit19.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit19.Name = "lookUpEdit19";
            this.lookUpEdit19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit19.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit19.Properties.DropDownRows = 25;
            this.lookUpEdit19.Properties.MaxLength = 1;
            this.lookUpEdit19.Properties.NullText = "[Empty]";
            this.lookUpEdit19.Properties.PopupWidth = 500;
            this.lookUpEdit19.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit19.TabIndex = 89;
            this.lookUpEdit19.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit5
            // 
            this.dateEdit5.EditValue = null;
            this.dateEdit5.EnterMoveNextControl = true;
            this.dateEdit5.Location = new System.Drawing.Point(115, 28);
            this.dateEdit5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit5.Name = "dateEdit5";
            this.dateEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit5.Properties.Appearance.Options.UseFont = true;
            this.dateEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit5.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit5.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit5.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit5.Properties.MaxLength = 16;
            this.dateEdit5.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit5.Size = new System.Drawing.Size(125, 20);
            this.dateEdit5.TabIndex = 68;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(23, 31);
            this.label89.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(86, 14);
            this.label89.TabIndex = 67;
            this.label89.Text = "Wedding Date";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit19
            // 
            this.textEdit19.EnterMoveNextControl = true;
            this.textEdit19.Location = new System.Drawing.Point(115, 342);
            this.textEdit19.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit19.Properties.Appearance.Options.UseFont = true;
            this.textEdit19.Properties.MaxLength = 80;
            this.textEdit19.Size = new System.Drawing.Size(245, 20);
            this.textEdit19.TabIndex = 99;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(29, 10);
            this.label90.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(80, 14);
            this.label90.TabIndex = 65;
            this.label90.Text = "Marital Status";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(21, 346);
            this.label91.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(88, 14);
            this.label91.TabIndex = 98;
            this.label91.Text = "Account Name";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(38, 178);
            this.label92.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(71, 14);
            this.label92.TabIndex = 82;
            this.label92.Text = "Grade Level";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit20
            // 
            this.textEdit20.EnterMoveNextControl = true;
            this.textEdit20.Location = new System.Drawing.Point(115, 426);
            this.textEdit20.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit20.Properties.Appearance.Options.UseFont = true;
            this.textEdit20.Properties.MaxLength = 80;
            this.textEdit20.Size = new System.Drawing.Size(245, 20);
            this.textEdit20.TabIndex = 107;
            // 
            // lookUpEdit20
            // 
            this.lookUpEdit20.EnterMoveNextControl = true;
            this.lookUpEdit20.Location = new System.Drawing.Point(115, 7);
            this.lookUpEdit20.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit20.Name = "lookUpEdit20";
            this.lookUpEdit20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit20.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit20.Properties.DropDownRows = 25;
            this.lookUpEdit20.Properties.MaxLength = 1;
            this.lookUpEdit20.Properties.NullText = "[Empty]";
            this.lookUpEdit20.Properties.PopupWidth = 500;
            this.lookUpEdit20.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit20.TabIndex = 66;
            this.lookUpEdit20.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit21
            // 
            this.lookUpEdit21.EnterMoveNextControl = true;
            this.lookUpEdit21.Location = new System.Drawing.Point(115, 174);
            this.lookUpEdit21.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit21.Name = "lookUpEdit21";
            this.lookUpEdit21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit21.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit21.Properties.DropDownRows = 25;
            this.lookUpEdit21.Properties.NullText = "[Empty]";
            this.lookUpEdit21.Properties.PopupWidth = 500;
            this.lookUpEdit21.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit21.TabIndex = 83;
            this.lookUpEdit21.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit6
            // 
            this.dateEdit6.EditValue = null;
            this.dateEdit6.EnterMoveNextControl = true;
            this.dateEdit6.Location = new System.Drawing.Point(243, 91);
            this.dateEdit6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit6.Name = "dateEdit6";
            this.dateEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit6.Properties.Appearance.Options.UseFont = true;
            this.dateEdit6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit6.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit6.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit6.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit6.Properties.MaxLength = 8;
            this.dateEdit6.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit6.Size = new System.Drawing.Size(117, 20);
            this.dateEdit6.TabIndex = 75;
            // 
            // textEdit21
            // 
            this.textEdit21.EnterMoveNextControl = true;
            this.textEdit21.Location = new System.Drawing.Point(115, 321);
            this.textEdit21.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit21.Properties.Appearance.Options.UseFont = true;
            this.textEdit21.Properties.MaxLength = 80;
            this.textEdit21.Size = new System.Drawing.Size(245, 20);
            this.textEdit21.TabIndex = 97;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(229, 93);
            this.label93.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(12, 14);
            this.label93.TabIndex = 67;
            this.label93.Text = "/";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(75, 429);
            this.label94.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(34, 14);
            this.label94.TabIndex = 106;
            this.label94.Text = "Email";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(30, 325);
            this.label95.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(79, 14);
            this.label95.TabIndex = 96;
            this.label95.Text = "Branch Name";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit22
            // 
            this.textEdit22.EnterMoveNextControl = true;
            this.textEdit22.Location = new System.Drawing.Point(115, 91);
            this.textEdit22.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit22.Properties.Appearance.Options.UseFont = true;
            this.textEdit22.Properties.MaxLength = 80;
            this.textEdit22.Size = new System.Drawing.Size(112, 20);
            this.textEdit22.TabIndex = 74;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Black;
            this.label96.Location = new System.Drawing.Point(14, 95);
            this.label96.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(95, 14);
            this.label96.TabIndex = 73;
            this.label96.Text = "Birth Place/Date";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit23
            // 
            this.textEdit23.EnterMoveNextControl = true;
            this.textEdit23.Location = new System.Drawing.Point(115, 363);
            this.textEdit23.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit23.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit23.Properties.Appearance.Options.UseFont = true;
            this.textEdit23.Properties.MaxLength = 80;
            this.textEdit23.Size = new System.Drawing.Size(245, 20);
            this.textEdit23.TabIndex = 101;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Black;
            this.label97.Location = new System.Drawing.Point(61, 74);
            this.label97.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(48, 14);
            this.label97.TabIndex = 71;
            this.label97.Text = "Religion";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit22
            // 
            this.lookUpEdit22.EnterMoveNextControl = true;
            this.lookUpEdit22.Location = new System.Drawing.Point(115, 70);
            this.lookUpEdit22.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit22.Name = "lookUpEdit22";
            this.lookUpEdit22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit22.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit22.Properties.DropDownRows = 25;
            this.lookUpEdit22.Properties.MaxLength = 2;
            this.lookUpEdit22.Properties.NullText = "[Empty]";
            this.lookUpEdit22.Properties.PopupWidth = 500;
            this.lookUpEdit22.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit22.TabIndex = 72;
            this.lookUpEdit22.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit24
            // 
            this.textEdit24.EnterMoveNextControl = true;
            this.textEdit24.Location = new System.Drawing.Point(115, 405);
            this.textEdit24.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit24.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit24.Properties.Appearance.Options.UseFont = true;
            this.textEdit24.Properties.MaxLength = 80;
            this.textEdit24.Size = new System.Drawing.Size(245, 20);
            this.textEdit24.TabIndex = 105;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Black;
            this.label98.Location = new System.Drawing.Point(47, 366);
            this.label98.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(62, 14);
            this.label98.TabIndex = 100;
            this.label98.Text = "Account#";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(41, 303);
            this.label99.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(68, 14);
            this.label99.TabIndex = 94;
            this.label99.Text = "Bank Name";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit23
            // 
            this.lookUpEdit23.EnterMoveNextControl = true;
            this.lookUpEdit23.Location = new System.Drawing.Point(115, 300);
            this.lookUpEdit23.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit23.Name = "lookUpEdit23";
            this.lookUpEdit23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit23.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit23.Properties.DropDownRows = 25;
            this.lookUpEdit23.Properties.NullText = "[Empty]";
            this.lookUpEdit23.Properties.PopupWidth = 500;
            this.lookUpEdit23.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit23.TabIndex = 95;
            this.lookUpEdit23.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Black;
            this.label100.Location = new System.Drawing.Point(2, 283);
            this.label100.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(107, 14);
            this.label100.TabIndex = 92;
            this.label100.Text = "Non-Incoming Tax";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit24
            // 
            this.lookUpEdit24.EnterMoveNextControl = true;
            this.lookUpEdit24.Location = new System.Drawing.Point(115, 279);
            this.lookUpEdit24.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit24.Name = "lookUpEdit24";
            this.lookUpEdit24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit24.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit24.Properties.DropDownRows = 25;
            this.lookUpEdit24.Properties.NullText = "[Empty]";
            this.lookUpEdit24.Properties.PopupWidth = 500;
            this.lookUpEdit24.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit24.TabIndex = 93;
            this.lookUpEdit24.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Black;
            this.label101.Location = new System.Drawing.Point(68, 408);
            this.label101.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(41, 14);
            this.label101.TabIndex = 104;
            this.label101.Text = "Mobile";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(36, 220);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(73, 14);
            this.label102.TabIndex = 86;
            this.label102.Text = "Payroll Type";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit25
            // 
            this.lookUpEdit25.EnterMoveNextControl = true;
            this.lookUpEdit25.Location = new System.Drawing.Point(115, 216);
            this.lookUpEdit25.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit25.Name = "lookUpEdit25";
            this.lookUpEdit25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit25.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit25.Properties.DropDownRows = 25;
            this.lookUpEdit25.Properties.NullText = "[Empty]";
            this.lookUpEdit25.Properties.PopupWidth = 500;
            this.lookUpEdit25.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit25.TabIndex = 87;
            this.lookUpEdit25.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(68, 262);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(41, 14);
            this.label103.TabIndex = 90;
            this.label103.Text = "NPWP";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(62, 52);
            this.label104.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(47, 14);
            this.label104.TabIndex = 69;
            this.label104.Text = "Gender";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit25
            // 
            this.textEdit25.EnterMoveNextControl = true;
            this.textEdit25.Location = new System.Drawing.Point(115, 258);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit25.Properties.Appearance.Options.UseFont = true;
            this.textEdit25.Properties.MaxLength = 40;
            this.textEdit25.Size = new System.Drawing.Size(245, 20);
            this.textEdit25.TabIndex = 91;
            // 
            // lookUpEdit26
            // 
            this.lookUpEdit26.EnterMoveNextControl = true;
            this.lookUpEdit26.Location = new System.Drawing.Point(115, 49);
            this.lookUpEdit26.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit26.Name = "lookUpEdit26";
            this.lookUpEdit26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit26.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit26.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit26.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit26.Properties.DropDownRows = 25;
            this.lookUpEdit26.Properties.MaxLength = 1;
            this.lookUpEdit26.Properties.NullText = "[Empty]";
            this.lookUpEdit26.Properties.PopupWidth = 500;
            this.lookUpEdit26.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit26.TabIndex = 70;
            this.lookUpEdit26.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit26.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(67, 387);
            this.label105.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(42, 14);
            this.label105.TabIndex = 102;
            this.label105.Text = "Phone";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit26
            // 
            this.textEdit26.EnterMoveNextControl = true;
            this.textEdit26.Location = new System.Drawing.Point(115, 384);
            this.textEdit26.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit26.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit26.Properties.Appearance.Options.UseFont = true;
            this.textEdit26.Properties.MaxLength = 80;
            this.textEdit26.Size = new System.Drawing.Size(245, 20);
            this.textEdit26.TabIndex = 103;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(93, 323);
            this.label106.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(27, 14);
            this.label106.TabIndex = 51;
            this.label106.Text = "City";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit27
            // 
            this.textEdit27.EnterMoveNextControl = true;
            this.textEdit27.Location = new System.Drawing.Point(125, 447);
            this.textEdit27.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit27.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit27.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit27.Properties.Appearance.Options.UseFont = true;
            this.textEdit27.Properties.MaxLength = 40;
            this.textEdit27.Size = new System.Drawing.Size(257, 20);
            this.textEdit27.TabIndex = 64;
            // 
            // lookUpEdit27
            // 
            this.lookUpEdit27.EnterMoveNextControl = true;
            this.lookUpEdit27.Location = new System.Drawing.Point(125, 321);
            this.lookUpEdit27.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit27.Name = "lookUpEdit27";
            this.lookUpEdit27.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit27.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit27.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit27.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit27.Properties.DropDownRows = 25;
            this.lookUpEdit27.Properties.MaxLength = 16;
            this.lookUpEdit27.Properties.NullText = "[Empty]";
            this.lookUpEdit27.Properties.PopupWidth = 500;
            this.lookUpEdit27.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit27.TabIndex = 52;
            this.lookUpEdit27.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit27.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit7
            // 
            this.dateEdit7.EditValue = null;
            this.dateEdit7.EnterMoveNextControl = true;
            this.dateEdit7.Location = new System.Drawing.Point(125, 279);
            this.dateEdit7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit7.Name = "dateEdit7";
            this.dateEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit7.Properties.Appearance.Options.UseFont = true;
            this.dateEdit7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit7.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit7.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit7.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit7.Properties.MaxLength = 16;
            this.dateEdit7.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit7.Size = new System.Drawing.Size(125, 20);
            this.dateEdit7.TabIndex = 48;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(61, 450);
            this.label107.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(59, 14);
            this.label107.TabIndex = 63;
            this.label107.Text = "Identity#";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Black;
            this.label108.Location = new System.Drawing.Point(48, 282);
            this.label108.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(72, 14);
            this.label108.TabIndex = 47;
            this.label108.Text = "Resign Date";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit8
            // 
            this.dateEdit8.EditValue = null;
            this.dateEdit8.EnterMoveNextControl = true;
            this.dateEdit8.Location = new System.Drawing.Point(125, 258);
            this.dateEdit8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit8.Name = "dateEdit8";
            this.dateEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit8.Properties.Appearance.Options.UseFont = true;
            this.dateEdit8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit8.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit8.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit8.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit8.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit8.Properties.MaxLength = 16;
            this.dateEdit8.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit8.Size = new System.Drawing.Size(125, 20);
            this.dateEdit8.TabIndex = 46;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Red;
            this.label109.Location = new System.Drawing.Point(62, 261);
            this.label109.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(58, 14);
            this.label109.TabIndex = 45;
            this.label109.Text = "Join Date";
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit28
            // 
            this.textEdit28.EnterMoveNextControl = true;
            this.textEdit28.Location = new System.Drawing.Point(125, 6);
            this.textEdit28.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit28.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit28.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit28.Properties.Appearance.Options.UseFont = true;
            this.textEdit28.Properties.MaxLength = 16;
            this.textEdit28.Size = new System.Drawing.Size(257, 20);
            this.textEdit28.TabIndex = 22;
            // 
            // textEdit29
            // 
            this.textEdit29.EnterMoveNextControl = true;
            this.textEdit29.Location = new System.Drawing.Point(125, 405);
            this.textEdit29.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit29.Name = "textEdit29";
            this.textEdit29.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit29.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit29.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit29.Properties.Appearance.Options.UseFont = true;
            this.textEdit29.Properties.MaxLength = 16;
            this.textEdit29.Size = new System.Drawing.Size(257, 20);
            this.textEdit29.TabIndex = 60;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Black;
            this.label110.Location = new System.Drawing.Point(57, 9);
            this.label110.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(63, 14);
            this.label110.TabIndex = 21;
            this.label110.Text = "User Code";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(49, 408);
            this.label111.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(71, 14);
            this.label111.TabIndex = 59;
            this.label111.Text = "Postal Code";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit3
            // 
            this.memoExEdit3.EnterMoveNextControl = true;
            this.memoExEdit3.Location = new System.Drawing.Point(125, 300);
            this.memoExEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit3.Name = "memoExEdit3";
            this.memoExEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit3.Properties.MaxLength = 80;
            this.memoExEdit3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit3.Properties.ShowIcon = false;
            this.memoExEdit3.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit3.TabIndex = 50;
            this.memoExEdit3.ToolTip = "F4 : Show/hide text";
            this.memoExEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit3.ToolTipTitle = "Run System";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(70, 303);
            this.label112.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(50, 14);
            this.label112.TabIndex = 49;
            this.label112.Text = "Address";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Black;
            this.label113.Location = new System.Drawing.Point(71, 176);
            this.label113.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(49, 14);
            this.label113.TabIndex = 37;
            this.label113.Text = "Position";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit28
            // 
            this.lookUpEdit28.EnterMoveNextControl = true;
            this.lookUpEdit28.Location = new System.Drawing.Point(125, 174);
            this.lookUpEdit28.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit28.Name = "lookUpEdit28";
            this.lookUpEdit28.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit28.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit28.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit28.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit28.Properties.DropDownRows = 25;
            this.lookUpEdit28.Properties.MaxLength = 16;
            this.lookUpEdit28.Properties.NullText = "[Empty]";
            this.lookUpEdit28.Properties.PopupWidth = 500;
            this.lookUpEdit28.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit28.TabIndex = 38;
            this.lookUpEdit28.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit28.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Black;
            this.label114.Location = new System.Drawing.Point(47, 156);
            this.label114.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(73, 14);
            this.label114.TabIndex = 35;
            this.label114.Text = "Department";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit29
            // 
            this.lookUpEdit29.EnterMoveNextControl = true;
            this.lookUpEdit29.Location = new System.Drawing.Point(125, 153);
            this.lookUpEdit29.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit29.Name = "lookUpEdit29";
            this.lookUpEdit29.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit29.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit29.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit29.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit29.Properties.DropDownRows = 25;
            this.lookUpEdit29.Properties.MaxLength = 16;
            this.lookUpEdit29.Properties.NullText = "[Empty]";
            this.lookUpEdit29.Properties.PopupWidth = 500;
            this.lookUpEdit29.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit29.TabIndex = 36;
            this.lookUpEdit29.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit29.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit30
            // 
            this.textEdit30.EnterMoveNextControl = true;
            this.textEdit30.Location = new System.Drawing.Point(125, 237);
            this.textEdit30.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit30.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit30.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit30.Properties.Appearance.Options.UseFont = true;
            this.textEdit30.Properties.MaxLength = 40;
            this.textEdit30.Size = new System.Drawing.Size(257, 20);
            this.textEdit30.TabIndex = 44;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(44, 240);
            this.label115.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(75, 14);
            this.label115.TabIndex = 43;
            this.label115.Text = "Point of Hire";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Black;
            this.label116.Location = new System.Drawing.Point(74, 135);
            this.label116.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(46, 14);
            this.label116.TabIndex = 33;
            this.label116.Text = "Division";
            this.label116.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit30
            // 
            this.lookUpEdit30.EnterMoveNextControl = true;
            this.lookUpEdit30.Location = new System.Drawing.Point(125, 132);
            this.lookUpEdit30.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit30.Name = "lookUpEdit30";
            this.lookUpEdit30.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit30.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit30.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit30.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit30.Properties.DropDownRows = 25;
            this.lookUpEdit30.Properties.MaxLength = 16;
            this.lookUpEdit30.Properties.NullText = "[Empty]";
            this.lookUpEdit30.Properties.PopupWidth = 500;
            this.lookUpEdit30.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit30.TabIndex = 34;
            this.lookUpEdit30.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit30.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(31, 219);
            this.label117.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(89, 14);
            this.label117.TabIndex = 41;
            this.label117.Text = "Working Group";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit31
            // 
            this.lookUpEdit31.EnterMoveNextControl = true;
            this.lookUpEdit31.Location = new System.Drawing.Point(125, 216);
            this.lookUpEdit31.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit31.Name = "lookUpEdit31";
            this.lookUpEdit31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit31.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit31.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit31.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit31.Properties.DropDownRows = 25;
            this.lookUpEdit31.Properties.MaxLength = 16;
            this.lookUpEdit31.Properties.NullText = "[Empty]";
            this.lookUpEdit31.Properties.PopupWidth = 500;
            this.lookUpEdit31.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit31.TabIndex = 42;
            this.lookUpEdit31.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit31.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(72, 197);
            this.label118.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(48, 14);
            this.label118.TabIndex = 39;
            this.label118.Text = "Section";
            this.label118.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit32
            // 
            this.lookUpEdit32.EnterMoveNextControl = true;
            this.lookUpEdit32.Location = new System.Drawing.Point(125, 195);
            this.lookUpEdit32.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit32.Name = "lookUpEdit32";
            this.lookUpEdit32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit32.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit32.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit32.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit32.Properties.DropDownRows = 25;
            this.lookUpEdit32.Properties.MaxLength = 16;
            this.lookUpEdit32.Properties.NullText = "[Empty]";
            this.lookUpEdit32.Properties.PopupWidth = 500;
            this.lookUpEdit32.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit32.TabIndex = 40;
            this.lookUpEdit32.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit32.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.Black;
            this.label119.Location = new System.Drawing.Point(69, 429);
            this.label119.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(51, 14);
            this.label119.TabIndex = 61;
            this.label119.Text = "Domicile";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit4
            // 
            this.memoExEdit4.EnterMoveNextControl = true;
            this.memoExEdit4.Location = new System.Drawing.Point(125, 426);
            this.memoExEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit4.Name = "memoExEdit4";
            this.memoExEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit4.Properties.MaxLength = 400;
            this.memoExEdit4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit4.Properties.ShowIcon = false;
            this.memoExEdit4.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit4.TabIndex = 62;
            this.memoExEdit4.ToolTip = "F4 : Show/hide text";
            this.memoExEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit4.ToolTipTitle = "Run System";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.Black;
            this.label120.Location = new System.Drawing.Point(92, 114);
            this.label120.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(28, 14);
            this.label120.TabIndex = 31;
            this.label120.Text = "Site";
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit33
            // 
            this.lookUpEdit33.EnterMoveNextControl = true;
            this.lookUpEdit33.Location = new System.Drawing.Point(125, 111);
            this.lookUpEdit33.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit33.Name = "lookUpEdit33";
            this.lookUpEdit33.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit33.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit33.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit33.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit33.Properties.DropDownRows = 25;
            this.lookUpEdit33.Properties.MaxLength = 16;
            this.lookUpEdit33.Properties.NullText = "[Empty]";
            this.lookUpEdit33.Properties.PopupWidth = 500;
            this.lookUpEdit33.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit33.TabIndex = 32;
            this.lookUpEdit33.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit33.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit31
            // 
            this.textEdit31.EnterMoveNextControl = true;
            this.textEdit31.Location = new System.Drawing.Point(125, 384);
            this.textEdit31.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit31.Name = "textEdit31";
            this.textEdit31.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit31.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit31.Properties.Appearance.Options.UseFont = true;
            this.textEdit31.Properties.MaxLength = 10;
            this.textEdit31.Size = new System.Drawing.Size(257, 20);
            this.textEdit31.TabIndex = 58;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.Black;
            this.label121.Location = new System.Drawing.Point(74, 387);
            this.label121.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(46, 14);
            this.label121.TabIndex = 57;
            this.label121.Text = "RT/RW";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit32
            // 
            this.textEdit32.EnterMoveNextControl = true;
            this.textEdit32.Location = new System.Drawing.Point(125, 363);
            this.textEdit32.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit32.Name = "textEdit32";
            this.textEdit32.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit32.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit32.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit32.Properties.Appearance.Options.UseFont = true;
            this.textEdit32.Properties.MaxLength = 40;
            this.textEdit32.Size = new System.Drawing.Size(257, 20);
            this.textEdit32.TabIndex = 56;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.Black;
            this.label122.Location = new System.Drawing.Point(79, 366);
            this.label122.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(41, 14);
            this.label122.TabIndex = 55;
            this.label122.Text = "Village";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit33
            // 
            this.textEdit33.EnterMoveNextControl = true;
            this.textEdit33.Location = new System.Drawing.Point(125, 342);
            this.textEdit33.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit33.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit33.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit33.Properties.Appearance.Options.UseFont = true;
            this.textEdit33.Properties.MaxLength = 40;
            this.textEdit33.Size = new System.Drawing.Size(257, 20);
            this.textEdit33.TabIndex = 54;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.Black;
            this.label123.Location = new System.Drawing.Point(51, 344);
            this.label123.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(69, 14);
            this.label123.TabIndex = 53;
            this.label123.Text = "Sub District";
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.Black;
            this.label124.Location = new System.Drawing.Point(6, 92);
            this.label124.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(114, 14);
            this.label124.TabIndex = 29;
            this.label124.Text = "Employment Status";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit34
            // 
            this.textEdit34.EnterMoveNextControl = true;
            this.textEdit34.Location = new System.Drawing.Point(125, 69);
            this.textEdit34.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit34.Name = "textEdit34";
            this.textEdit34.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit34.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit34.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit34.Properties.Appearance.Options.UseFont = true;
            this.textEdit34.Properties.MaxLength = 16;
            this.textEdit34.Size = new System.Drawing.Size(257, 20);
            this.textEdit34.TabIndex = 28;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.Black;
            this.label125.Location = new System.Drawing.Point(37, 71);
            this.label125.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(83, 14);
            this.label125.TabIndex = 27;
            this.label125.Text = "Barcode Code";
            this.label125.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit35
            // 
            this.textEdit35.EnterMoveNextControl = true;
            this.textEdit35.Location = new System.Drawing.Point(125, 48);
            this.textEdit35.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit35.Name = "textEdit35";
            this.textEdit35.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit35.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit35.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit35.Properties.Appearance.Options.UseFont = true;
            this.textEdit35.Properties.MaxLength = 3;
            this.textEdit35.Size = new System.Drawing.Size(257, 20);
            this.textEdit35.TabIndex = 26;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.Black;
            this.label126.Location = new System.Drawing.Point(51, 51);
            this.label126.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(69, 14);
            this.label126.TabIndex = 25;
            this.label126.Text = "Short Code";
            this.label126.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit34
            // 
            this.lookUpEdit34.EnterMoveNextControl = true;
            this.lookUpEdit34.Location = new System.Drawing.Point(125, 90);
            this.lookUpEdit34.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit34.Name = "lookUpEdit34";
            this.lookUpEdit34.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit34.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit34.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit34.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit34.Properties.DropDownRows = 25;
            this.lookUpEdit34.Properties.MaxLength = 1;
            this.lookUpEdit34.Properties.NullText = "[Empty]";
            this.lookUpEdit34.Properties.PopupWidth = 500;
            this.lookUpEdit34.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit34.TabIndex = 30;
            this.lookUpEdit34.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit34.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit36
            // 
            this.textEdit36.EnterMoveNextControl = true;
            this.textEdit36.Location = new System.Drawing.Point(125, 27);
            this.textEdit36.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit36.Name = "textEdit36";
            this.textEdit36.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit36.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit36.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit36.Properties.Appearance.Options.UseFont = true;
            this.textEdit36.Properties.MaxLength = 16;
            this.textEdit36.Size = new System.Drawing.Size(257, 20);
            this.textEdit36.TabIndex = 24;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.Black;
            this.label127.Location = new System.Drawing.Point(63, 29);
            this.label127.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(57, 14);
            this.label127.TabIndex = 23;
            this.label127.Text = "Old Code";
            this.label127.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.label128);
            this.panel8.Controls.Add(this.lookUpEdit35);
            this.panel8.Controls.Add(this.label129);
            this.panel8.Controls.Add(this.lookUpEdit36);
            this.panel8.Controls.Add(this.label130);
            this.panel8.Controls.Add(this.lookUpEdit37);
            this.panel8.Controls.Add(this.textEdit37);
            this.panel8.Controls.Add(this.label131);
            this.panel8.Controls.Add(this.label132);
            this.panel8.Controls.Add(this.lookUpEdit38);
            this.panel8.Controls.Add(this.dateEdit9);
            this.panel8.Controls.Add(this.label133);
            this.panel8.Controls.Add(this.textEdit38);
            this.panel8.Controls.Add(this.label134);
            this.panel8.Controls.Add(this.label135);
            this.panel8.Controls.Add(this.label136);
            this.panel8.Controls.Add(this.textEdit39);
            this.panel8.Controls.Add(this.lookUpEdit39);
            this.panel8.Controls.Add(this.lookUpEdit40);
            this.panel8.Controls.Add(this.dateEdit10);
            this.panel8.Controls.Add(this.textEdit40);
            this.panel8.Controls.Add(this.label137);
            this.panel8.Controls.Add(this.label138);
            this.panel8.Controls.Add(this.label139);
            this.panel8.Controls.Add(this.textEdit41);
            this.panel8.Controls.Add(this.label140);
            this.panel8.Controls.Add(this.textEdit42);
            this.panel8.Controls.Add(this.label141);
            this.panel8.Controls.Add(this.lookUpEdit41);
            this.panel8.Controls.Add(this.textEdit43);
            this.panel8.Controls.Add(this.label142);
            this.panel8.Controls.Add(this.label143);
            this.panel8.Controls.Add(this.lookUpEdit42);
            this.panel8.Controls.Add(this.label144);
            this.panel8.Controls.Add(this.lookUpEdit43);
            this.panel8.Controls.Add(this.label145);
            this.panel8.Controls.Add(this.label146);
            this.panel8.Controls.Add(this.lookUpEdit44);
            this.panel8.Controls.Add(this.label147);
            this.panel8.Controls.Add(this.label148);
            this.panel8.Controls.Add(this.textEdit44);
            this.panel8.Controls.Add(this.lookUpEdit45);
            this.panel8.Controls.Add(this.label149);
            this.panel8.Controls.Add(this.textEdit45);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(388, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(372, 494);
            this.panel8.TabIndex = 47;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.Black;
            this.label128.Location = new System.Drawing.Point(31, 199);
            this.label128.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(78, 14);
            this.label128.TabIndex = 84;
            this.label128.Text = "Payroll Group";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit35
            // 
            this.lookUpEdit35.EnterMoveNextControl = true;
            this.lookUpEdit35.Location = new System.Drawing.Point(115, 195);
            this.lookUpEdit35.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit35.Name = "lookUpEdit35";
            this.lookUpEdit35.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit35.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit35.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit35.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit35.Properties.DropDownRows = 25;
            this.lookUpEdit35.Properties.NullText = "[Empty]";
            this.lookUpEdit35.Properties.PopupWidth = 500;
            this.lookUpEdit35.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit35.TabIndex = 85;
            this.lookUpEdit35.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit35.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.Black;
            this.label129.Location = new System.Drawing.Point(40, 115);
            this.label129.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(69, 14);
            this.label129.TabIndex = 76;
            this.label129.Text = "Blood Type";
            this.label129.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit36
            // 
            this.lookUpEdit36.EnterMoveNextControl = true;
            this.lookUpEdit36.Location = new System.Drawing.Point(115, 111);
            this.lookUpEdit36.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit36.Name = "lookUpEdit36";
            this.lookUpEdit36.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit36.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit36.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit36.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit36.Properties.DropDownRows = 25;
            this.lookUpEdit36.Properties.MaxLength = 2;
            this.lookUpEdit36.Properties.NullText = "[Empty]";
            this.lookUpEdit36.Properties.PopupWidth = 500;
            this.lookUpEdit36.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit36.TabIndex = 77;
            this.lookUpEdit36.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit36.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.Black;
            this.label130.Location = new System.Drawing.Point(30, 157);
            this.label130.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(79, 14);
            this.label130.TabIndex = 80;
            this.label130.Text = "System Type";
            this.label130.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit37
            // 
            this.lookUpEdit37.EnterMoveNextControl = true;
            this.lookUpEdit37.Location = new System.Drawing.Point(115, 153);
            this.lookUpEdit37.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit37.Name = "lookUpEdit37";
            this.lookUpEdit37.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit37.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit37.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit37.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit37.Properties.DropDownRows = 25;
            this.lookUpEdit37.Properties.NullText = "[Empty]";
            this.lookUpEdit37.Properties.PopupWidth = 500;
            this.lookUpEdit37.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit37.TabIndex = 81;
            this.lookUpEdit37.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit37.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit37
            // 
            this.textEdit37.EnterMoveNextControl = true;
            this.textEdit37.Location = new System.Drawing.Point(115, 132);
            this.textEdit37.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit37.Name = "textEdit37";
            this.textEdit37.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit37.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit37.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit37.Properties.Appearance.Options.UseFont = true;
            this.textEdit37.Properties.MaxLength = 40;
            this.textEdit37.Size = new System.Drawing.Size(245, 20);
            this.textEdit37.TabIndex = 79;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.Black;
            this.label131.Location = new System.Drawing.Point(11, 136);
            this.label131.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(98, 14);
            this.label131.TabIndex = 78;
            this.label131.Text = "Biological Mother";
            this.label131.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.Black;
            this.label132.Location = new System.Drawing.Point(27, 241);
            this.label132.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(82, 14);
            this.label132.TabIndex = 88;
            this.label132.Text = "Payrun Period";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit38
            // 
            this.lookUpEdit38.EnterMoveNextControl = true;
            this.lookUpEdit38.Location = new System.Drawing.Point(115, 237);
            this.lookUpEdit38.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit38.Name = "lookUpEdit38";
            this.lookUpEdit38.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit38.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit38.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit38.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit38.Properties.DropDownRows = 25;
            this.lookUpEdit38.Properties.MaxLength = 1;
            this.lookUpEdit38.Properties.NullText = "[Empty]";
            this.lookUpEdit38.Properties.PopupWidth = 500;
            this.lookUpEdit38.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit38.TabIndex = 89;
            this.lookUpEdit38.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit38.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit9
            // 
            this.dateEdit9.EditValue = null;
            this.dateEdit9.EnterMoveNextControl = true;
            this.dateEdit9.Location = new System.Drawing.Point(115, 28);
            this.dateEdit9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit9.Name = "dateEdit9";
            this.dateEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit9.Properties.Appearance.Options.UseFont = true;
            this.dateEdit9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit9.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit9.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit9.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit9.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit9.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit9.Properties.MaxLength = 16;
            this.dateEdit9.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit9.Size = new System.Drawing.Size(125, 20);
            this.dateEdit9.TabIndex = 68;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.Black;
            this.label133.Location = new System.Drawing.Point(23, 31);
            this.label133.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(86, 14);
            this.label133.TabIndex = 67;
            this.label133.Text = "Wedding Date";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit38
            // 
            this.textEdit38.EnterMoveNextControl = true;
            this.textEdit38.Location = new System.Drawing.Point(115, 342);
            this.textEdit38.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit38.Name = "textEdit38";
            this.textEdit38.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit38.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit38.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit38.Properties.Appearance.Options.UseFont = true;
            this.textEdit38.Properties.MaxLength = 80;
            this.textEdit38.Size = new System.Drawing.Size(245, 20);
            this.textEdit38.TabIndex = 99;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.ForeColor = System.Drawing.Color.Black;
            this.label134.Location = new System.Drawing.Point(29, 10);
            this.label134.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(80, 14);
            this.label134.TabIndex = 65;
            this.label134.Text = "Marital Status";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.ForeColor = System.Drawing.Color.Black;
            this.label135.Location = new System.Drawing.Point(21, 346);
            this.label135.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(88, 14);
            this.label135.TabIndex = 98;
            this.label135.Text = "Account Name";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.ForeColor = System.Drawing.Color.Black;
            this.label136.Location = new System.Drawing.Point(38, 178);
            this.label136.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(71, 14);
            this.label136.TabIndex = 82;
            this.label136.Text = "Grade Level";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit39
            // 
            this.textEdit39.EnterMoveNextControl = true;
            this.textEdit39.Location = new System.Drawing.Point(115, 426);
            this.textEdit39.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit39.Name = "textEdit39";
            this.textEdit39.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit39.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit39.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit39.Properties.Appearance.Options.UseFont = true;
            this.textEdit39.Properties.MaxLength = 80;
            this.textEdit39.Size = new System.Drawing.Size(245, 20);
            this.textEdit39.TabIndex = 107;
            // 
            // lookUpEdit39
            // 
            this.lookUpEdit39.EnterMoveNextControl = true;
            this.lookUpEdit39.Location = new System.Drawing.Point(115, 7);
            this.lookUpEdit39.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit39.Name = "lookUpEdit39";
            this.lookUpEdit39.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit39.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit39.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit39.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit39.Properties.DropDownRows = 25;
            this.lookUpEdit39.Properties.MaxLength = 1;
            this.lookUpEdit39.Properties.NullText = "[Empty]";
            this.lookUpEdit39.Properties.PopupWidth = 500;
            this.lookUpEdit39.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit39.TabIndex = 66;
            this.lookUpEdit39.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit39.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit40
            // 
            this.lookUpEdit40.EnterMoveNextControl = true;
            this.lookUpEdit40.Location = new System.Drawing.Point(115, 174);
            this.lookUpEdit40.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit40.Name = "lookUpEdit40";
            this.lookUpEdit40.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit40.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit40.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit40.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit40.Properties.DropDownRows = 25;
            this.lookUpEdit40.Properties.NullText = "[Empty]";
            this.lookUpEdit40.Properties.PopupWidth = 500;
            this.lookUpEdit40.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit40.TabIndex = 83;
            this.lookUpEdit40.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit40.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit10
            // 
            this.dateEdit10.EditValue = null;
            this.dateEdit10.EnterMoveNextControl = true;
            this.dateEdit10.Location = new System.Drawing.Point(243, 91);
            this.dateEdit10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit10.Name = "dateEdit10";
            this.dateEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit10.Properties.Appearance.Options.UseFont = true;
            this.dateEdit10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit10.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit10.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit10.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit10.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit10.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit10.Properties.MaxLength = 8;
            this.dateEdit10.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit10.Size = new System.Drawing.Size(117, 20);
            this.dateEdit10.TabIndex = 75;
            // 
            // textEdit40
            // 
            this.textEdit40.EnterMoveNextControl = true;
            this.textEdit40.Location = new System.Drawing.Point(115, 321);
            this.textEdit40.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit40.Name = "textEdit40";
            this.textEdit40.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit40.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit40.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit40.Properties.Appearance.Options.UseFont = true;
            this.textEdit40.Properties.MaxLength = 80;
            this.textEdit40.Size = new System.Drawing.Size(245, 20);
            this.textEdit40.TabIndex = 97;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.ForeColor = System.Drawing.Color.Black;
            this.label137.Location = new System.Drawing.Point(229, 93);
            this.label137.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(12, 14);
            this.label137.TabIndex = 67;
            this.label137.Text = "/";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.ForeColor = System.Drawing.Color.Black;
            this.label138.Location = new System.Drawing.Point(75, 429);
            this.label138.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(34, 14);
            this.label138.TabIndex = 106;
            this.label138.Text = "Email";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.ForeColor = System.Drawing.Color.Black;
            this.label139.Location = new System.Drawing.Point(30, 325);
            this.label139.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(79, 14);
            this.label139.TabIndex = 96;
            this.label139.Text = "Branch Name";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit41
            // 
            this.textEdit41.EnterMoveNextControl = true;
            this.textEdit41.Location = new System.Drawing.Point(115, 91);
            this.textEdit41.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit41.Name = "textEdit41";
            this.textEdit41.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit41.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit41.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit41.Properties.Appearance.Options.UseFont = true;
            this.textEdit41.Properties.MaxLength = 80;
            this.textEdit41.Size = new System.Drawing.Size(112, 20);
            this.textEdit41.TabIndex = 74;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.ForeColor = System.Drawing.Color.Black;
            this.label140.Location = new System.Drawing.Point(14, 95);
            this.label140.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(95, 14);
            this.label140.TabIndex = 73;
            this.label140.Text = "Birth Place/Date";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit42
            // 
            this.textEdit42.EnterMoveNextControl = true;
            this.textEdit42.Location = new System.Drawing.Point(115, 363);
            this.textEdit42.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit42.Name = "textEdit42";
            this.textEdit42.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit42.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit42.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit42.Properties.Appearance.Options.UseFont = true;
            this.textEdit42.Properties.MaxLength = 80;
            this.textEdit42.Size = new System.Drawing.Size(245, 20);
            this.textEdit42.TabIndex = 101;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.ForeColor = System.Drawing.Color.Black;
            this.label141.Location = new System.Drawing.Point(61, 74);
            this.label141.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(48, 14);
            this.label141.TabIndex = 71;
            this.label141.Text = "Religion";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit41
            // 
            this.lookUpEdit41.EnterMoveNextControl = true;
            this.lookUpEdit41.Location = new System.Drawing.Point(115, 70);
            this.lookUpEdit41.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit41.Name = "lookUpEdit41";
            this.lookUpEdit41.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit41.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit41.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit41.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit41.Properties.DropDownRows = 25;
            this.lookUpEdit41.Properties.MaxLength = 2;
            this.lookUpEdit41.Properties.NullText = "[Empty]";
            this.lookUpEdit41.Properties.PopupWidth = 500;
            this.lookUpEdit41.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit41.TabIndex = 72;
            this.lookUpEdit41.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit41.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit43
            // 
            this.textEdit43.EnterMoveNextControl = true;
            this.textEdit43.Location = new System.Drawing.Point(115, 405);
            this.textEdit43.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit43.Name = "textEdit43";
            this.textEdit43.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit43.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit43.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit43.Properties.Appearance.Options.UseFont = true;
            this.textEdit43.Properties.MaxLength = 80;
            this.textEdit43.Size = new System.Drawing.Size(245, 20);
            this.textEdit43.TabIndex = 105;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.ForeColor = System.Drawing.Color.Black;
            this.label142.Location = new System.Drawing.Point(47, 366);
            this.label142.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(62, 14);
            this.label142.TabIndex = 100;
            this.label142.Text = "Account#";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.ForeColor = System.Drawing.Color.Black;
            this.label143.Location = new System.Drawing.Point(41, 303);
            this.label143.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(68, 14);
            this.label143.TabIndex = 94;
            this.label143.Text = "Bank Name";
            this.label143.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit42
            // 
            this.lookUpEdit42.EnterMoveNextControl = true;
            this.lookUpEdit42.Location = new System.Drawing.Point(115, 300);
            this.lookUpEdit42.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit42.Name = "lookUpEdit42";
            this.lookUpEdit42.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit42.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit42.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit42.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit42.Properties.DropDownRows = 25;
            this.lookUpEdit42.Properties.NullText = "[Empty]";
            this.lookUpEdit42.Properties.PopupWidth = 500;
            this.lookUpEdit42.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit42.TabIndex = 95;
            this.lookUpEdit42.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit42.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.Black;
            this.label144.Location = new System.Drawing.Point(2, 283);
            this.label144.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(107, 14);
            this.label144.TabIndex = 92;
            this.label144.Text = "Non-Incoming Tax";
            this.label144.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit43
            // 
            this.lookUpEdit43.EnterMoveNextControl = true;
            this.lookUpEdit43.Location = new System.Drawing.Point(115, 279);
            this.lookUpEdit43.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit43.Name = "lookUpEdit43";
            this.lookUpEdit43.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit43.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit43.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit43.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit43.Properties.DropDownRows = 25;
            this.lookUpEdit43.Properties.NullText = "[Empty]";
            this.lookUpEdit43.Properties.PopupWidth = 500;
            this.lookUpEdit43.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit43.TabIndex = 93;
            this.lookUpEdit43.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit43.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.ForeColor = System.Drawing.Color.Black;
            this.label145.Location = new System.Drawing.Point(68, 408);
            this.label145.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(41, 14);
            this.label145.TabIndex = 104;
            this.label145.Text = "Mobile";
            this.label145.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.Black;
            this.label146.Location = new System.Drawing.Point(36, 220);
            this.label146.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(73, 14);
            this.label146.TabIndex = 86;
            this.label146.Text = "Payroll Type";
            this.label146.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit44
            // 
            this.lookUpEdit44.EnterMoveNextControl = true;
            this.lookUpEdit44.Location = new System.Drawing.Point(115, 216);
            this.lookUpEdit44.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit44.Name = "lookUpEdit44";
            this.lookUpEdit44.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit44.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit44.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit44.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit44.Properties.DropDownRows = 25;
            this.lookUpEdit44.Properties.NullText = "[Empty]";
            this.lookUpEdit44.Properties.PopupWidth = 500;
            this.lookUpEdit44.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit44.TabIndex = 87;
            this.lookUpEdit44.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit44.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.Location = new System.Drawing.Point(68, 262);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(41, 14);
            this.label147.TabIndex = 90;
            this.label147.Text = "NPWP";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.Black;
            this.label148.Location = new System.Drawing.Point(62, 52);
            this.label148.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(47, 14);
            this.label148.TabIndex = 69;
            this.label148.Text = "Gender";
            this.label148.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit44
            // 
            this.textEdit44.EnterMoveNextControl = true;
            this.textEdit44.Location = new System.Drawing.Point(115, 258);
            this.textEdit44.Name = "textEdit44";
            this.textEdit44.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit44.Properties.Appearance.Options.UseFont = true;
            this.textEdit44.Properties.MaxLength = 40;
            this.textEdit44.Size = new System.Drawing.Size(245, 20);
            this.textEdit44.TabIndex = 91;
            // 
            // lookUpEdit45
            // 
            this.lookUpEdit45.EnterMoveNextControl = true;
            this.lookUpEdit45.Location = new System.Drawing.Point(115, 49);
            this.lookUpEdit45.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit45.Name = "lookUpEdit45";
            this.lookUpEdit45.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit45.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit45.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit45.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit45.Properties.DropDownRows = 25;
            this.lookUpEdit45.Properties.MaxLength = 1;
            this.lookUpEdit45.Properties.NullText = "[Empty]";
            this.lookUpEdit45.Properties.PopupWidth = 500;
            this.lookUpEdit45.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit45.TabIndex = 70;
            this.lookUpEdit45.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit45.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.Black;
            this.label149.Location = new System.Drawing.Point(67, 387);
            this.label149.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(42, 14);
            this.label149.TabIndex = 102;
            this.label149.Text = "Phone";
            this.label149.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit45
            // 
            this.textEdit45.EnterMoveNextControl = true;
            this.textEdit45.Location = new System.Drawing.Point(115, 384);
            this.textEdit45.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit45.Name = "textEdit45";
            this.textEdit45.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit45.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit45.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit45.Properties.Appearance.Options.UseFont = true;
            this.textEdit45.Properties.MaxLength = 80;
            this.textEdit45.Size = new System.Drawing.Size(245, 20);
            this.textEdit45.TabIndex = 103;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.ForeColor = System.Drawing.Color.Black;
            this.label150.Location = new System.Drawing.Point(93, 323);
            this.label150.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(27, 14);
            this.label150.TabIndex = 51;
            this.label150.Text = "City";
            this.label150.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit46
            // 
            this.textEdit46.EnterMoveNextControl = true;
            this.textEdit46.Location = new System.Drawing.Point(125, 447);
            this.textEdit46.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit46.Name = "textEdit46";
            this.textEdit46.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit46.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit46.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit46.Properties.Appearance.Options.UseFont = true;
            this.textEdit46.Properties.MaxLength = 40;
            this.textEdit46.Size = new System.Drawing.Size(257, 20);
            this.textEdit46.TabIndex = 64;
            // 
            // lookUpEdit46
            // 
            this.lookUpEdit46.EnterMoveNextControl = true;
            this.lookUpEdit46.Location = new System.Drawing.Point(125, 321);
            this.lookUpEdit46.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit46.Name = "lookUpEdit46";
            this.lookUpEdit46.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit46.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit46.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit46.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit46.Properties.DropDownRows = 25;
            this.lookUpEdit46.Properties.MaxLength = 16;
            this.lookUpEdit46.Properties.NullText = "[Empty]";
            this.lookUpEdit46.Properties.PopupWidth = 500;
            this.lookUpEdit46.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit46.TabIndex = 52;
            this.lookUpEdit46.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit46.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit11
            // 
            this.dateEdit11.EditValue = null;
            this.dateEdit11.EnterMoveNextControl = true;
            this.dateEdit11.Location = new System.Drawing.Point(125, 279);
            this.dateEdit11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit11.Name = "dateEdit11";
            this.dateEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit11.Properties.Appearance.Options.UseFont = true;
            this.dateEdit11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit11.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit11.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit11.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit11.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit11.Properties.MaxLength = 16;
            this.dateEdit11.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit11.Size = new System.Drawing.Size(125, 20);
            this.dateEdit11.TabIndex = 48;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.ForeColor = System.Drawing.Color.Black;
            this.label151.Location = new System.Drawing.Point(61, 450);
            this.label151.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(59, 14);
            this.label151.TabIndex = 63;
            this.label151.Text = "Identity#";
            this.label151.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.ForeColor = System.Drawing.Color.Black;
            this.label152.Location = new System.Drawing.Point(48, 282);
            this.label152.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(72, 14);
            this.label152.TabIndex = 47;
            this.label152.Text = "Resign Date";
            this.label152.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit12
            // 
            this.dateEdit12.EditValue = null;
            this.dateEdit12.EnterMoveNextControl = true;
            this.dateEdit12.Location = new System.Drawing.Point(125, 258);
            this.dateEdit12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit12.Name = "dateEdit12";
            this.dateEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit12.Properties.Appearance.Options.UseFont = true;
            this.dateEdit12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit12.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit12.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit12.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit12.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit12.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit12.Properties.MaxLength = 16;
            this.dateEdit12.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit12.Size = new System.Drawing.Size(125, 20);
            this.dateEdit12.TabIndex = 46;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.ForeColor = System.Drawing.Color.Red;
            this.label153.Location = new System.Drawing.Point(62, 261);
            this.label153.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(58, 14);
            this.label153.TabIndex = 45;
            this.label153.Text = "Join Date";
            this.label153.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit47
            // 
            this.textEdit47.EnterMoveNextControl = true;
            this.textEdit47.Location = new System.Drawing.Point(125, 6);
            this.textEdit47.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit47.Name = "textEdit47";
            this.textEdit47.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit47.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit47.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit47.Properties.Appearance.Options.UseFont = true;
            this.textEdit47.Properties.MaxLength = 16;
            this.textEdit47.Size = new System.Drawing.Size(257, 20);
            this.textEdit47.TabIndex = 22;
            // 
            // textEdit48
            // 
            this.textEdit48.EnterMoveNextControl = true;
            this.textEdit48.Location = new System.Drawing.Point(125, 405);
            this.textEdit48.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit48.Name = "textEdit48";
            this.textEdit48.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit48.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit48.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit48.Properties.Appearance.Options.UseFont = true;
            this.textEdit48.Properties.MaxLength = 16;
            this.textEdit48.Size = new System.Drawing.Size(257, 20);
            this.textEdit48.TabIndex = 60;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.ForeColor = System.Drawing.Color.Black;
            this.label154.Location = new System.Drawing.Point(57, 9);
            this.label154.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(63, 14);
            this.label154.TabIndex = 21;
            this.label154.Text = "User Code";
            this.label154.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.ForeColor = System.Drawing.Color.Black;
            this.label155.Location = new System.Drawing.Point(49, 408);
            this.label155.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(71, 14);
            this.label155.TabIndex = 59;
            this.label155.Text = "Postal Code";
            this.label155.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit5
            // 
            this.memoExEdit5.EnterMoveNextControl = true;
            this.memoExEdit5.Location = new System.Drawing.Point(125, 300);
            this.memoExEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit5.Name = "memoExEdit5";
            this.memoExEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit5.Properties.MaxLength = 80;
            this.memoExEdit5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit5.Properties.ShowIcon = false;
            this.memoExEdit5.Size = new System.Drawing.Size(257, 20);
            this.memoExEdit5.TabIndex = 50;
            this.memoExEdit5.ToolTip = "F4 : Show/hide text";
            this.memoExEdit5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit5.ToolTipTitle = "Run System";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(70, 303);
            this.label156.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(50, 14);
            this.label156.TabIndex = 49;
            this.label156.Text = "Address";
            this.label156.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.ForeColor = System.Drawing.Color.Black;
            this.label157.Location = new System.Drawing.Point(71, 176);
            this.label157.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(49, 14);
            this.label157.TabIndex = 37;
            this.label157.Text = "Position";
            this.label157.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit47
            // 
            this.lookUpEdit47.EnterMoveNextControl = true;
            this.lookUpEdit47.Location = new System.Drawing.Point(125, 174);
            this.lookUpEdit47.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit47.Name = "lookUpEdit47";
            this.lookUpEdit47.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit47.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit47.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit47.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit47.Properties.DropDownRows = 25;
            this.lookUpEdit47.Properties.MaxLength = 16;
            this.lookUpEdit47.Properties.NullText = "[Empty]";
            this.lookUpEdit47.Properties.PopupWidth = 500;
            this.lookUpEdit47.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit47.TabIndex = 38;
            this.lookUpEdit47.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit47.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.ForeColor = System.Drawing.Color.Black;
            this.label158.Location = new System.Drawing.Point(47, 156);
            this.label158.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(73, 14);
            this.label158.TabIndex = 35;
            this.label158.Text = "Department";
            this.label158.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit48
            // 
            this.lookUpEdit48.EnterMoveNextControl = true;
            this.lookUpEdit48.Location = new System.Drawing.Point(125, 153);
            this.lookUpEdit48.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit48.Name = "lookUpEdit48";
            this.lookUpEdit48.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit48.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit48.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit48.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit48.Properties.DropDownRows = 25;
            this.lookUpEdit48.Properties.MaxLength = 16;
            this.lookUpEdit48.Properties.NullText = "[Empty]";
            this.lookUpEdit48.Properties.PopupWidth = 500;
            this.lookUpEdit48.Size = new System.Drawing.Size(257, 20);
            this.lookUpEdit48.TabIndex = 36;
            this.lookUpEdit48.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit48.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // FrmMedicalTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 589);
            this.Name = "FrmMedicalTest";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecruitmentDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRecruitmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUmur.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompany.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgAmnesis.ResumeLayout(false);
            this.TpgAmnesis.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue05.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue04.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPenyakit03.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue03.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPenyakit02.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue02.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lue01.Properties)).EndInit();
            this.TpgFisik.ResumeLayout(false);
            this.TpgFisik.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeConclusion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeClinic.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLab.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEkstremitas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeStomach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeLiver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeHeart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNeck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEar.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeMouth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNose.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEye.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeNutrition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAwareness.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCondition.Properties)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit48.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgAmnesis;
        private DevExpress.XtraEditors.LookUpEdit Lue01;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit textEdit4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.Label label29;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit5;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        internal DevExpress.XtraEditors.TextEdit textEdit6;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit textEdit7;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit textEdit8;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit textEdit9;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit6;
        internal DevExpress.XtraEditors.DateEdit dateEdit1;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit7;
        internal DevExpress.XtraEditors.TextEdit textEdit11;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.DateEdit dateEdit2;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.DateEdit dateEdit3;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit textEdit12;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit8;
        private System.Windows.Forms.Label label45;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit9;
        private System.Windows.Forms.Label label46;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit10;
        private System.Windows.Forms.TabPage tabPage2;
        internal DevExpress.XtraEditors.DateEdit dateEdit4;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit11;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit12;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid1;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label71;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label72;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label70;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private System.Windows.Forms.Label label73;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private System.Windows.Forms.Label label74;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit13;
        private System.Windows.Forms.Label label75;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit2;
        private System.Windows.Forms.Label label76;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit14;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label77;
        internal DevExpress.XtraEditors.TextEdit textEdit13;
        private System.Windows.Forms.Label label78;
        internal DevExpress.XtraEditors.TextEdit textEdit14;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        internal DevExpress.XtraEditors.TextEdit textEdit15;
        private System.Windows.Forms.Label label81;
        internal DevExpress.XtraEditors.TextEdit textEdit16;
        private System.Windows.Forms.Label label82;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit15;
        internal DevExpress.XtraEditors.TextEdit textEdit17;
        private System.Windows.Forms.Label label83;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label84;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit16;
        private System.Windows.Forms.Label label85;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit17;
        private System.Windows.Forms.Label label86;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit18;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit19;
        internal DevExpress.XtraEditors.DateEdit dateEdit5;
        private System.Windows.Forms.Label label89;
        internal DevExpress.XtraEditors.TextEdit textEdit19;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        internal DevExpress.XtraEditors.TextEdit textEdit20;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit20;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit21;
        internal DevExpress.XtraEditors.DateEdit dateEdit6;
        internal DevExpress.XtraEditors.TextEdit textEdit21;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private System.Windows.Forms.Label label96;
        internal DevExpress.XtraEditors.TextEdit textEdit23;
        private System.Windows.Forms.Label label97;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit22;
        internal DevExpress.XtraEditors.TextEdit textEdit24;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit23;
        private System.Windows.Forms.Label label100;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit24;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit25;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private DevExpress.XtraEditors.TextEdit textEdit25;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit26;
        private System.Windows.Forms.Label label105;
        internal DevExpress.XtraEditors.TextEdit textEdit26;
        private System.Windows.Forms.Label label106;
        internal DevExpress.XtraEditors.TextEdit textEdit27;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit27;
        internal DevExpress.XtraEditors.DateEdit dateEdit7;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        internal DevExpress.XtraEditors.DateEdit dateEdit8;
        private System.Windows.Forms.Label label109;
        internal DevExpress.XtraEditors.TextEdit textEdit28;
        internal DevExpress.XtraEditors.TextEdit textEdit29;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit3;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit28;
        private System.Windows.Forms.Label label114;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit29;
        internal DevExpress.XtraEditors.TextEdit textEdit30;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit30;
        private System.Windows.Forms.Label label117;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit31;
        private System.Windows.Forms.Label label118;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit32;
        private System.Windows.Forms.Label label119;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit4;
        private System.Windows.Forms.Label label120;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit33;
        internal DevExpress.XtraEditors.TextEdit textEdit31;
        private System.Windows.Forms.Label label121;
        internal DevExpress.XtraEditors.TextEdit textEdit32;
        private System.Windows.Forms.Label label122;
        internal DevExpress.XtraEditors.TextEdit textEdit33;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        internal DevExpress.XtraEditors.TextEdit textEdit34;
        private System.Windows.Forms.Label label125;
        internal DevExpress.XtraEditors.TextEdit textEdit35;
        private System.Windows.Forms.Label label126;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit34;
        internal DevExpress.XtraEditors.TextEdit textEdit36;
        private System.Windows.Forms.Label label127;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label128;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit35;
        private System.Windows.Forms.Label label129;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit36;
        private System.Windows.Forms.Label label130;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit37;
        private DevExpress.XtraEditors.TextEdit textEdit37;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit38;
        internal DevExpress.XtraEditors.DateEdit dateEdit9;
        private System.Windows.Forms.Label label133;
        internal DevExpress.XtraEditors.TextEdit textEdit38;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        internal DevExpress.XtraEditors.TextEdit textEdit39;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit39;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit40;
        internal DevExpress.XtraEditors.DateEdit dateEdit10;
        internal DevExpress.XtraEditors.TextEdit textEdit40;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private DevExpress.XtraEditors.TextEdit textEdit41;
        private System.Windows.Forms.Label label140;
        internal DevExpress.XtraEditors.TextEdit textEdit42;
        private System.Windows.Forms.Label label141;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit41;
        internal DevExpress.XtraEditors.TextEdit textEdit43;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit42;
        private System.Windows.Forms.Label label144;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit43;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit44;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private DevExpress.XtraEditors.TextEdit textEdit44;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit45;
        private System.Windows.Forms.Label label149;
        internal DevExpress.XtraEditors.TextEdit textEdit45;
        private System.Windows.Forms.Label label150;
        internal DevExpress.XtraEditors.TextEdit textEdit46;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit46;
        internal DevExpress.XtraEditors.DateEdit dateEdit11;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        internal DevExpress.XtraEditors.DateEdit dateEdit12;
        private System.Windows.Forms.Label label153;
        internal DevExpress.XtraEditors.TextEdit textEdit47;
        internal DevExpress.XtraEditors.TextEdit textEdit48;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit5;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit47;
        private System.Windows.Forms.Label label158;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit48;
        public DevExpress.XtraEditors.SimpleButton BtnShowEmployeeRecruitment;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label171;
        private DevExpress.XtraEditors.TextEdit TxtCompany;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit Lue02;
        internal DevExpress.XtraEditors.TextEdit TxtPenyakit02;
        private DevExpress.XtraEditors.LookUpEdit Lue05;
        private DevExpress.XtraEditors.LookUpEdit Lue04;
        internal DevExpress.XtraEditors.TextEdit TxtPenyakit03;
        private DevExpress.XtraEditors.LookUpEdit Lue03;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label LblTN;
        private System.Windows.Forms.TabPage TpgFisik;
        private DevExpress.XtraEditors.MemoExEdit MeeNutrition;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.MemoExEdit MeeAwareness;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.MemoExEdit MeeCondition;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.MemoExEdit MeeNeck;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.MemoExEdit MeeEar;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.MemoExEdit MeeMouth;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.MemoExEdit MeeNose;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.MemoExEdit MeeEye;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.MemoExEdit MeeEkstremitas;
        private System.Windows.Forms.Label label47;
        private DevExpress.XtraEditors.MemoExEdit MeeStomach;
        private System.Windows.Forms.Label label48;
        private DevExpress.XtraEditors.MemoExEdit MeeLiver;
        private System.Windows.Forms.Label label49;
        private DevExpress.XtraEditors.MemoExEdit MeeHeart;
        private System.Windows.Forms.Label label50;
        private DevExpress.XtraEditors.MemoExEdit MeeClinic;
        private System.Windows.Forms.Label label52;
        private DevExpress.XtraEditors.MemoExEdit MeeLab;
        private System.Windows.Forms.Label label54;
        private DevExpress.XtraEditors.MemoExEdit MeeConclusion;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        public DevExpress.XtraEditors.TextEdit TxtUmur;
        public DevExpress.XtraEditors.TextEdit TxtDeptCode;
        public DevExpress.XtraEditors.TextEdit TxtPosCode;
        public DevExpress.XtraEditors.TextEdit TxtRecruitmentDocNo;
        public DevExpress.XtraEditors.TextEdit TxtRecruitmentName;
        public DevExpress.XtraEditors.TextEdit TxtGender;
        internal DevExpress.XtraEditors.TextEdit TxtTB;
        internal DevExpress.XtraEditors.TextEdit TxtBB;
        internal DevExpress.XtraEditors.TextEdit TxtDN;
        internal DevExpress.XtraEditors.TextEdit TxtTD;

    }
}