﻿namespace RunSystem
{
    partial class FrmItemCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmItemCategory));
            this.TxtItCtName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtItCtCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAStock = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.ChkActiveInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtAcDesc2 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo2 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOACost = new System.Windows.Forms.Label();
            this.TxtAcDesc3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo3 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOACOGM = new System.Windows.Forms.Label();
            this.ChkWastedInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtAcDesc4 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo4 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo4 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOASales = new System.Windows.Forms.Label();
            this.TxtAcDesc5 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo5 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo5 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOACOGS = new System.Windows.Forms.Label();
            this.TxtAcDesc6 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo6 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo6 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOASalesReturn = new System.Windows.Forms.Label();
            this.TxtAcDesc7 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo7 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo7 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAPurchaseReturn = new System.Windows.Forms.Label();
            this.ChkMovingAvgInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtAcDesc10 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo10 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo10 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAAR = new System.Windows.Forms.Label();
            this.TxtAcDesc8 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo8 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo8 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAAPUninvoiced = new System.Windows.Forms.Label();
            this.TxtAcDesc9 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo9 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo9 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAAPInvoiced = new System.Windows.Forms.Label();
            this.TxtAcDesc11 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo11 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo11 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAARUninvoiced = new System.Windows.Forms.Label();
            this.TxtAcDesc12 = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo12 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo12 = new DevExpress.XtraEditors.TextEdit();
            this.LblCOAStockBeginning = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWastedInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMovingAvgInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo12.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 309);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtAcDesc12);
            this.panel2.Controls.Add(this.BtnAcNo12);
            this.panel2.Controls.Add(this.TxtAcNo12);
            this.panel2.Controls.Add(this.LblCOAStockBeginning);
            this.panel2.Controls.Add(this.TxtAcDesc11);
            this.panel2.Controls.Add(this.BtnAcNo11);
            this.panel2.Controls.Add(this.TxtAcNo11);
            this.panel2.Controls.Add(this.LblCOAARUninvoiced);
            this.panel2.Controls.Add(this.TxtAcDesc9);
            this.panel2.Controls.Add(this.BtnAcNo9);
            this.panel2.Controls.Add(this.TxtAcNo9);
            this.panel2.Controls.Add(this.LblCOAAPInvoiced);
            this.panel2.Controls.Add(this.TxtAcDesc10);
            this.panel2.Controls.Add(this.BtnAcNo10);
            this.panel2.Controls.Add(this.TxtAcNo10);
            this.panel2.Controls.Add(this.LblCOAAR);
            this.panel2.Controls.Add(this.TxtAcDesc8);
            this.panel2.Controls.Add(this.BtnAcNo8);
            this.panel2.Controls.Add(this.TxtAcNo8);
            this.panel2.Controls.Add(this.LblCOAAPUninvoiced);
            this.panel2.Controls.Add(this.ChkMovingAvgInd);
            this.panel2.Controls.Add(this.TxtAcDesc7);
            this.panel2.Controls.Add(this.BtnAcNo7);
            this.panel2.Controls.Add(this.TxtAcNo7);
            this.panel2.Controls.Add(this.LblCOAPurchaseReturn);
            this.panel2.Controls.Add(this.TxtAcDesc6);
            this.panel2.Controls.Add(this.BtnAcNo6);
            this.panel2.Controls.Add(this.TxtAcNo6);
            this.panel2.Controls.Add(this.LblCOASalesReturn);
            this.panel2.Controls.Add(this.TxtAcDesc5);
            this.panel2.Controls.Add(this.BtnAcNo5);
            this.panel2.Controls.Add(this.TxtAcNo5);
            this.panel2.Controls.Add(this.LblCOACOGS);
            this.panel2.Controls.Add(this.TxtAcDesc4);
            this.panel2.Controls.Add(this.BtnAcNo4);
            this.panel2.Controls.Add(this.TxtAcNo4);
            this.panel2.Controls.Add(this.LblCOASales);
            this.panel2.Controls.Add(this.ChkWastedInd);
            this.panel2.Controls.Add(this.TxtAcDesc3);
            this.panel2.Controls.Add(this.BtnAcNo3);
            this.panel2.Controls.Add(this.TxtAcNo3);
            this.panel2.Controls.Add(this.LblCOACOGM);
            this.panel2.Controls.Add(this.TxtAcDesc2);
            this.panel2.Controls.Add(this.BtnAcNo2);
            this.panel2.Controls.Add(this.TxtAcNo2);
            this.panel2.Controls.Add(this.LblCOACost);
            this.panel2.Controls.Add(this.ChkActiveInd);
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.LblCOAStock);
            this.panel2.Controls.Add(this.TxtItCtName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtItCtCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(772, 309);
            // 
            // TxtItCtName
            // 
            this.TxtItCtName.EnterMoveNextControl = true;
            this.TxtItCtName.Location = new System.Drawing.Point(208, 31);
            this.TxtItCtName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCtName.Name = "TxtItCtName";
            this.TxtItCtName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCtName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCtName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCtName.Properties.Appearance.Options.UseFont = true;
            this.TxtItCtName.Properties.MaxLength = 80;
            this.TxtItCtName.Size = new System.Drawing.Size(526, 20);
            this.TxtItCtName.TabIndex = 15;
            this.TxtItCtName.Validated += new System.EventHandler(this.TxtItCtName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(74, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Item\'s Category Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCtCode
            // 
            this.TxtItCtCode.EnterMoveNextControl = true;
            this.TxtItCtCode.Location = new System.Drawing.Point(208, 10);
            this.TxtItCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCtCode.Name = "TxtItCtCode";
            this.TxtItCtCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtItCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCtCode.Properties.MaxLength = 16;
            this.TxtItCtCode.Size = new System.Drawing.Size(178, 20);
            this.TxtItCtCode.TabIndex = 10;
            this.TxtItCtCode.Validated += new System.EventHandler(this.TxtItCtCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(77, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Item\'s Category Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(738, 51);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo.TabIndex = 19;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(208, 53);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 40;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo.TabIndex = 17;
            // 
            // LblCOAStock
            // 
            this.LblCOAStock.AutoSize = true;
            this.LblCOAStock.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAStock.ForeColor = System.Drawing.Color.Black;
            this.LblCOAStock.Location = new System.Drawing.Point(65, 57);
            this.LblCOAStock.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAStock.Name = "LblCOAStock";
            this.LblCOAStock.Size = new System.Drawing.Size(138, 14);
            this.LblCOAStock.TabIndex = 16;
            this.LblCOAStock.Text = "COA\'s Account (Stock) ";
            this.LblCOAStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(365, 53);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 80;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc.TabIndex = 18;
            // 
            // ChkActiveInd
            // 
            this.ChkActiveInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActiveInd.Location = new System.Drawing.Point(392, 9);
            this.ChkActiveInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActiveInd.Name = "ChkActiveInd";
            this.ChkActiveInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActiveInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActiveInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActiveInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActiveInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActiveInd.Properties.Caption = "Active";
            this.ChkActiveInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActiveInd.ShowToolTips = false;
            this.ChkActiveInd.Size = new System.Drawing.Size(65, 22);
            this.ChkActiveInd.TabIndex = 11;
            // 
            // TxtAcDesc2
            // 
            this.TxtAcDesc2.EnterMoveNextControl = true;
            this.TxtAcDesc2.Location = new System.Drawing.Point(365, 74);
            this.TxtAcDesc2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc2.Name = "TxtAcDesc2";
            this.TxtAcDesc2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc2.Properties.MaxLength = 80;
            this.TxtAcDesc2.Properties.ReadOnly = true;
            this.TxtAcDesc2.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc2.TabIndex = 22;
            // 
            // BtnAcNo2
            // 
            this.BtnAcNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo2.Appearance.Options.UseBackColor = true;
            this.BtnAcNo2.Appearance.Options.UseFont = true;
            this.BtnAcNo2.Appearance.Options.UseForeColor = true;
            this.BtnAcNo2.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo2.Image")));
            this.BtnAcNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo2.Location = new System.Drawing.Point(738, 72);
            this.BtnAcNo2.Name = "BtnAcNo2";
            this.BtnAcNo2.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo2.TabIndex = 23;
            this.BtnAcNo2.ToolTip = "Find COA\'s Account";
            this.BtnAcNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo2.ToolTipTitle = "Run System";
            this.BtnAcNo2.Click += new System.EventHandler(this.BtnAcNo2_Click);
            // 
            // TxtAcNo2
            // 
            this.TxtAcNo2.EnterMoveNextControl = true;
            this.TxtAcNo2.Location = new System.Drawing.Point(208, 74);
            this.TxtAcNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo2.Name = "TxtAcNo2";
            this.TxtAcNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo2.Properties.MaxLength = 40;
            this.TxtAcNo2.Properties.ReadOnly = true;
            this.TxtAcNo2.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo2.TabIndex = 21;
            // 
            // LblCOACost
            // 
            this.LblCOACost.AutoSize = true;
            this.LblCOACost.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOACost.ForeColor = System.Drawing.Color.Black;
            this.LblCOACost.Location = new System.Drawing.Point(72, 78);
            this.LblCOACost.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOACost.Name = "LblCOACost";
            this.LblCOACost.Size = new System.Drawing.Size(131, 14);
            this.LblCOACost.TabIndex = 20;
            this.LblCOACost.Text = "COA\'s Account (Cost) ";
            this.LblCOACost.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc3
            // 
            this.TxtAcDesc3.EnterMoveNextControl = true;
            this.TxtAcDesc3.Location = new System.Drawing.Point(365, 95);
            this.TxtAcDesc3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc3.Name = "TxtAcDesc3";
            this.TxtAcDesc3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc3.Properties.MaxLength = 80;
            this.TxtAcDesc3.Properties.ReadOnly = true;
            this.TxtAcDesc3.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc3.TabIndex = 25;
            // 
            // BtnAcNo3
            // 
            this.BtnAcNo3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo3.Appearance.Options.UseBackColor = true;
            this.BtnAcNo3.Appearance.Options.UseFont = true;
            this.BtnAcNo3.Appearance.Options.UseForeColor = true;
            this.BtnAcNo3.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo3.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo3.Image")));
            this.BtnAcNo3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo3.Location = new System.Drawing.Point(738, 93);
            this.BtnAcNo3.Name = "BtnAcNo3";
            this.BtnAcNo3.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo3.TabIndex = 26;
            this.BtnAcNo3.ToolTip = "Find COA\'s Account";
            this.BtnAcNo3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo3.ToolTipTitle = "Run System";
            this.BtnAcNo3.Click += new System.EventHandler(this.BtnAcNo3_Click);
            // 
            // TxtAcNo3
            // 
            this.TxtAcNo3.EnterMoveNextControl = true;
            this.TxtAcNo3.Location = new System.Drawing.Point(208, 95);
            this.TxtAcNo3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo3.Name = "TxtAcNo3";
            this.TxtAcNo3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo3.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo3.Properties.MaxLength = 40;
            this.TxtAcNo3.Properties.ReadOnly = true;
            this.TxtAcNo3.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo3.TabIndex = 24;
            // 
            // LblCOACOGM
            // 
            this.LblCOACOGM.AutoSize = true;
            this.LblCOACOGM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOACOGM.ForeColor = System.Drawing.Color.Black;
            this.LblCOACOGM.Location = new System.Drawing.Point(63, 99);
            this.LblCOACOGM.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOACOGM.Name = "LblCOACOGM";
            this.LblCOACOGM.Size = new System.Drawing.Size(140, 14);
            this.LblCOACOGM.TabIndex = 24;
            this.LblCOACOGM.Text = "COA\'s Account (COGM) ";
            this.LblCOACOGM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkWastedInd
            // 
            this.ChkWastedInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkWastedInd.Location = new System.Drawing.Point(460, 9);
            this.ChkWastedInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkWastedInd.Name = "ChkWastedInd";
            this.ChkWastedInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkWastedInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkWastedInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkWastedInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkWastedInd.Properties.Appearance.Options.UseFont = true;
            this.ChkWastedInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkWastedInd.Properties.Caption = "Wasted Item";
            this.ChkWastedInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkWastedInd.ShowToolTips = false;
            this.ChkWastedInd.Size = new System.Drawing.Size(104, 22);
            this.ChkWastedInd.TabIndex = 12;
            // 
            // TxtAcDesc4
            // 
            this.TxtAcDesc4.EnterMoveNextControl = true;
            this.TxtAcDesc4.Location = new System.Drawing.Point(365, 116);
            this.TxtAcDesc4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc4.Name = "TxtAcDesc4";
            this.TxtAcDesc4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc4.Properties.MaxLength = 80;
            this.TxtAcDesc4.Properties.ReadOnly = true;
            this.TxtAcDesc4.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc4.TabIndex = 29;
            // 
            // BtnAcNo4
            // 
            this.BtnAcNo4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo4.Appearance.Options.UseBackColor = true;
            this.BtnAcNo4.Appearance.Options.UseFont = true;
            this.BtnAcNo4.Appearance.Options.UseForeColor = true;
            this.BtnAcNo4.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo4.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo4.Image")));
            this.BtnAcNo4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo4.Location = new System.Drawing.Point(738, 114);
            this.BtnAcNo4.Name = "BtnAcNo4";
            this.BtnAcNo4.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo4.TabIndex = 30;
            this.BtnAcNo4.ToolTip = "Find COA\'s Account";
            this.BtnAcNo4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo4.ToolTipTitle = "Run System";
            this.BtnAcNo4.Click += new System.EventHandler(this.BtnAcNo4_Click);
            // 
            // TxtAcNo4
            // 
            this.TxtAcNo4.EnterMoveNextControl = true;
            this.TxtAcNo4.Location = new System.Drawing.Point(208, 116);
            this.TxtAcNo4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo4.Name = "TxtAcNo4";
            this.TxtAcNo4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo4.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo4.Properties.MaxLength = 40;
            this.TxtAcNo4.Properties.ReadOnly = true;
            this.TxtAcNo4.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo4.TabIndex = 28;
            // 
            // LblCOASales
            // 
            this.LblCOASales.AutoSize = true;
            this.LblCOASales.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOASales.ForeColor = System.Drawing.Color.Black;
            this.LblCOASales.Location = new System.Drawing.Point(69, 119);
            this.LblCOASales.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOASales.Name = "LblCOASales";
            this.LblCOASales.Size = new System.Drawing.Size(134, 14);
            this.LblCOASales.TabIndex = 27;
            this.LblCOASales.Text = "COA\'s Account (Sales) ";
            this.LblCOASales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc5
            // 
            this.TxtAcDesc5.EnterMoveNextControl = true;
            this.TxtAcDesc5.Location = new System.Drawing.Point(365, 137);
            this.TxtAcDesc5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc5.Name = "TxtAcDesc5";
            this.TxtAcDesc5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc5.Properties.MaxLength = 80;
            this.TxtAcDesc5.Properties.ReadOnly = true;
            this.TxtAcDesc5.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc5.TabIndex = 33;
            // 
            // BtnAcNo5
            // 
            this.BtnAcNo5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo5.Appearance.Options.UseBackColor = true;
            this.BtnAcNo5.Appearance.Options.UseFont = true;
            this.BtnAcNo5.Appearance.Options.UseForeColor = true;
            this.BtnAcNo5.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo5.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo5.Image")));
            this.BtnAcNo5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo5.Location = new System.Drawing.Point(738, 135);
            this.BtnAcNo5.Name = "BtnAcNo5";
            this.BtnAcNo5.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo5.TabIndex = 34;
            this.BtnAcNo5.ToolTip = "Find COA\'s Account";
            this.BtnAcNo5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo5.ToolTipTitle = "Run System";
            this.BtnAcNo5.Click += new System.EventHandler(this.BtnAcNo5_Click);
            // 
            // TxtAcNo5
            // 
            this.TxtAcNo5.EnterMoveNextControl = true;
            this.TxtAcNo5.Location = new System.Drawing.Point(208, 137);
            this.TxtAcNo5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo5.Name = "TxtAcNo5";
            this.TxtAcNo5.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo5.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo5.Properties.MaxLength = 40;
            this.TxtAcNo5.Properties.ReadOnly = true;
            this.TxtAcNo5.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo5.TabIndex = 32;
            // 
            // LblCOACOGS
            // 
            this.LblCOACOGS.AutoSize = true;
            this.LblCOACOGS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOACOGS.ForeColor = System.Drawing.Color.Black;
            this.LblCOACOGS.Location = new System.Drawing.Point(65, 140);
            this.LblCOACOGS.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOACOGS.Name = "LblCOACOGS";
            this.LblCOACOGS.Size = new System.Drawing.Size(138, 14);
            this.LblCOACOGS.TabIndex = 31;
            this.LblCOACOGS.Text = "COA\'s Account (COGS) ";
            this.LblCOACOGS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc6
            // 
            this.TxtAcDesc6.EnterMoveNextControl = true;
            this.TxtAcDesc6.Location = new System.Drawing.Point(365, 158);
            this.TxtAcDesc6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc6.Name = "TxtAcDesc6";
            this.TxtAcDesc6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc6.Properties.MaxLength = 80;
            this.TxtAcDesc6.Properties.ReadOnly = true;
            this.TxtAcDesc6.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc6.TabIndex = 37;
            // 
            // BtnAcNo6
            // 
            this.BtnAcNo6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo6.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo6.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo6.Appearance.Options.UseBackColor = true;
            this.BtnAcNo6.Appearance.Options.UseFont = true;
            this.BtnAcNo6.Appearance.Options.UseForeColor = true;
            this.BtnAcNo6.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo6.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo6.Image")));
            this.BtnAcNo6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo6.Location = new System.Drawing.Point(738, 156);
            this.BtnAcNo6.Name = "BtnAcNo6";
            this.BtnAcNo6.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo6.TabIndex = 38;
            this.BtnAcNo6.ToolTip = "Find COA\'s Account";
            this.BtnAcNo6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo6.ToolTipTitle = "Run System";
            this.BtnAcNo6.Click += new System.EventHandler(this.BtnAcNo6_Click);
            // 
            // TxtAcNo6
            // 
            this.TxtAcNo6.EnterMoveNextControl = true;
            this.TxtAcNo6.Location = new System.Drawing.Point(208, 158);
            this.TxtAcNo6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo6.Name = "TxtAcNo6";
            this.TxtAcNo6.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo6.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo6.Properties.MaxLength = 40;
            this.TxtAcNo6.Properties.ReadOnly = true;
            this.TxtAcNo6.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo6.TabIndex = 36;
            // 
            // LblCOASalesReturn
            // 
            this.LblCOASalesReturn.AutoSize = true;
            this.LblCOASalesReturn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOASalesReturn.ForeColor = System.Drawing.Color.Black;
            this.LblCOASalesReturn.Location = new System.Drawing.Point(28, 161);
            this.LblCOASalesReturn.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOASalesReturn.Name = "LblCOASalesReturn";
            this.LblCOASalesReturn.Size = new System.Drawing.Size(175, 14);
            this.LblCOASalesReturn.TabIndex = 35;
            this.LblCOASalesReturn.Text = "COA\'s Account (Sales Return) ";
            this.LblCOASalesReturn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc7
            // 
            this.TxtAcDesc7.EnterMoveNextControl = true;
            this.TxtAcDesc7.Location = new System.Drawing.Point(365, 179);
            this.TxtAcDesc7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc7.Name = "TxtAcDesc7";
            this.TxtAcDesc7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc7.Properties.MaxLength = 80;
            this.TxtAcDesc7.Properties.ReadOnly = true;
            this.TxtAcDesc7.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc7.TabIndex = 41;
            // 
            // BtnAcNo7
            // 
            this.BtnAcNo7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo7.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo7.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo7.Appearance.Options.UseBackColor = true;
            this.BtnAcNo7.Appearance.Options.UseFont = true;
            this.BtnAcNo7.Appearance.Options.UseForeColor = true;
            this.BtnAcNo7.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo7.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo7.Image")));
            this.BtnAcNo7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo7.Location = new System.Drawing.Point(738, 177);
            this.BtnAcNo7.Name = "BtnAcNo7";
            this.BtnAcNo7.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo7.TabIndex = 42;
            this.BtnAcNo7.ToolTip = "Find COA\'s Account";
            this.BtnAcNo7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo7.ToolTipTitle = "Run System";
            this.BtnAcNo7.Click += new System.EventHandler(this.BtnAcNo7_Click);
            // 
            // TxtAcNo7
            // 
            this.TxtAcNo7.EnterMoveNextControl = true;
            this.TxtAcNo7.Location = new System.Drawing.Point(208, 179);
            this.TxtAcNo7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo7.Name = "TxtAcNo7";
            this.TxtAcNo7.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo7.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo7.Properties.MaxLength = 40;
            this.TxtAcNo7.Properties.ReadOnly = true;
            this.TxtAcNo7.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo7.TabIndex = 40;
            // 
            // LblCOAPurchaseReturn
            // 
            this.LblCOAPurchaseReturn.AutoSize = true;
            this.LblCOAPurchaseReturn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAPurchaseReturn.ForeColor = System.Drawing.Color.Black;
            this.LblCOAPurchaseReturn.Location = new System.Drawing.Point(6, 182);
            this.LblCOAPurchaseReturn.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAPurchaseReturn.Name = "LblCOAPurchaseReturn";
            this.LblCOAPurchaseReturn.Size = new System.Drawing.Size(197, 14);
            this.LblCOAPurchaseReturn.TabIndex = 39;
            this.LblCOAPurchaseReturn.Text = "COA\'s Account (Purchase Return) ";
            this.LblCOAPurchaseReturn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkMovingAvgInd
            // 
            this.ChkMovingAvgInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkMovingAvgInd.Location = new System.Drawing.Point(566, 9);
            this.ChkMovingAvgInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkMovingAvgInd.Name = "ChkMovingAvgInd";
            this.ChkMovingAvgInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkMovingAvgInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkMovingAvgInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkMovingAvgInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkMovingAvgInd.Properties.Appearance.Options.UseFont = true;
            this.ChkMovingAvgInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkMovingAvgInd.Properties.Caption = "Moving Average";
            this.ChkMovingAvgInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkMovingAvgInd.ShowToolTips = false;
            this.ChkMovingAvgInd.Size = new System.Drawing.Size(120, 22);
            this.ChkMovingAvgInd.TabIndex = 13;
            // 
            // TxtAcDesc10
            // 
            this.TxtAcDesc10.EnterMoveNextControl = true;
            this.TxtAcDesc10.Location = new System.Drawing.Point(365, 242);
            this.TxtAcDesc10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc10.Name = "TxtAcDesc10";
            this.TxtAcDesc10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc10.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc10.Properties.MaxLength = 80;
            this.TxtAcDesc10.Properties.ReadOnly = true;
            this.TxtAcDesc10.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc10.TabIndex = 49;
            // 
            // BtnAcNo10
            // 
            this.BtnAcNo10.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo10.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo10.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo10.Appearance.Options.UseBackColor = true;
            this.BtnAcNo10.Appearance.Options.UseFont = true;
            this.BtnAcNo10.Appearance.Options.UseForeColor = true;
            this.BtnAcNo10.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo10.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo10.Image")));
            this.BtnAcNo10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo10.Location = new System.Drawing.Point(738, 240);
            this.BtnAcNo10.Name = "BtnAcNo10";
            this.BtnAcNo10.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo10.TabIndex = 50;
            this.BtnAcNo10.ToolTip = "Find COA\'s Account";
            this.BtnAcNo10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo10.ToolTipTitle = "Run System";
            this.BtnAcNo10.Click += new System.EventHandler(this.BtnAcNo10_Click);
            // 
            // TxtAcNo10
            // 
            this.TxtAcNo10.EnterMoveNextControl = true;
            this.TxtAcNo10.Location = new System.Drawing.Point(208, 242);
            this.TxtAcNo10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo10.Name = "TxtAcNo10";
            this.TxtAcNo10.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo10.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo10.Properties.MaxLength = 40;
            this.TxtAcNo10.Properties.ReadOnly = true;
            this.TxtAcNo10.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo10.TabIndex = 48;
            // 
            // LblCOAAR
            // 
            this.LblCOAAR.AutoSize = true;
            this.LblCOAAR.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAAR.ForeColor = System.Drawing.Color.Black;
            this.LblCOAAR.Location = new System.Drawing.Point(81, 246);
            this.LblCOAAR.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAAR.Name = "LblCOAAR";
            this.LblCOAAR.Size = new System.Drawing.Size(122, 14);
            this.LblCOAAR.TabIndex = 47;
            this.LblCOAAR.Text = "COA\'s Account (AR) ";
            this.LblCOAAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc8
            // 
            this.TxtAcDesc8.EnterMoveNextControl = true;
            this.TxtAcDesc8.Location = new System.Drawing.Point(365, 200);
            this.TxtAcDesc8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc8.Name = "TxtAcDesc8";
            this.TxtAcDesc8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc8.Properties.MaxLength = 80;
            this.TxtAcDesc8.Properties.ReadOnly = true;
            this.TxtAcDesc8.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc8.TabIndex = 45;
            // 
            // BtnAcNo8
            // 
            this.BtnAcNo8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo8.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo8.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo8.Appearance.Options.UseBackColor = true;
            this.BtnAcNo8.Appearance.Options.UseFont = true;
            this.BtnAcNo8.Appearance.Options.UseForeColor = true;
            this.BtnAcNo8.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo8.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo8.Image")));
            this.BtnAcNo8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo8.Location = new System.Drawing.Point(738, 198);
            this.BtnAcNo8.Name = "BtnAcNo8";
            this.BtnAcNo8.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo8.TabIndex = 46;
            this.BtnAcNo8.ToolTip = "Find COA\'s Account";
            this.BtnAcNo8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo8.ToolTipTitle = "Run System";
            this.BtnAcNo8.Click += new System.EventHandler(this.BtnAcNo8_Click);
            // 
            // TxtAcNo8
            // 
            this.TxtAcNo8.EnterMoveNextControl = true;
            this.TxtAcNo8.Location = new System.Drawing.Point(208, 200);
            this.TxtAcNo8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo8.Name = "TxtAcNo8";
            this.TxtAcNo8.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo8.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo8.Properties.MaxLength = 40;
            this.TxtAcNo8.Properties.ReadOnly = true;
            this.TxtAcNo8.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo8.TabIndex = 44;
            // 
            // LblCOAAPUninvoiced
            // 
            this.LblCOAAPUninvoiced.AutoSize = true;
            this.LblCOAAPUninvoiced.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAAPUninvoiced.ForeColor = System.Drawing.Color.Black;
            this.LblCOAAPUninvoiced.Location = new System.Drawing.Point(18, 203);
            this.LblCOAAPUninvoiced.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAAPUninvoiced.Name = "LblCOAAPUninvoiced";
            this.LblCOAAPUninvoiced.Size = new System.Drawing.Size(185, 14);
            this.LblCOAAPUninvoiced.TabIndex = 43;
            this.LblCOAAPUninvoiced.Text = "COA\'s Account (AP Uninvoiced) ";
            this.LblCOAAPUninvoiced.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc9
            // 
            this.TxtAcDesc9.EnterMoveNextControl = true;
            this.TxtAcDesc9.Location = new System.Drawing.Point(365, 221);
            this.TxtAcDesc9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc9.Name = "TxtAcDesc9";
            this.TxtAcDesc9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc9.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc9.Properties.MaxLength = 80;
            this.TxtAcDesc9.Properties.ReadOnly = true;
            this.TxtAcDesc9.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc9.TabIndex = 53;
            // 
            // BtnAcNo9
            // 
            this.BtnAcNo9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo9.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo9.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo9.Appearance.Options.UseBackColor = true;
            this.BtnAcNo9.Appearance.Options.UseFont = true;
            this.BtnAcNo9.Appearance.Options.UseForeColor = true;
            this.BtnAcNo9.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo9.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo9.Image")));
            this.BtnAcNo9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo9.Location = new System.Drawing.Point(738, 219);
            this.BtnAcNo9.Name = "BtnAcNo9";
            this.BtnAcNo9.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo9.TabIndex = 54;
            this.BtnAcNo9.ToolTip = "Find COA\'s Account";
            this.BtnAcNo9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo9.ToolTipTitle = "Run System";
            this.BtnAcNo9.Click += new System.EventHandler(this.BtnAcNo9_Click);
            // 
            // TxtAcNo9
            // 
            this.TxtAcNo9.EnterMoveNextControl = true;
            this.TxtAcNo9.Location = new System.Drawing.Point(208, 221);
            this.TxtAcNo9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo9.Name = "TxtAcNo9";
            this.TxtAcNo9.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo9.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo9.Properties.MaxLength = 40;
            this.TxtAcNo9.Properties.ReadOnly = true;
            this.TxtAcNo9.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo9.TabIndex = 52;
            // 
            // LblCOAAPInvoiced
            // 
            this.LblCOAAPInvoiced.AutoSize = true;
            this.LblCOAAPInvoiced.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAAPInvoiced.ForeColor = System.Drawing.Color.Black;
            this.LblCOAAPInvoiced.Location = new System.Drawing.Point(31, 224);
            this.LblCOAAPInvoiced.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAAPInvoiced.Name = "LblCOAAPInvoiced";
            this.LblCOAAPInvoiced.Size = new System.Drawing.Size(172, 14);
            this.LblCOAAPInvoiced.TabIndex = 51;
            this.LblCOAAPInvoiced.Text = "COA\'s Account (AP Invoiced) ";
            this.LblCOAAPInvoiced.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc11
            // 
            this.TxtAcDesc11.EnterMoveNextControl = true;
            this.TxtAcDesc11.Location = new System.Drawing.Point(365, 263);
            this.TxtAcDesc11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc11.Name = "TxtAcDesc11";
            this.TxtAcDesc11.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc11.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc11.Properties.MaxLength = 80;
            this.TxtAcDesc11.Properties.ReadOnly = true;
            this.TxtAcDesc11.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc11.TabIndex = 53;
            // 
            // BtnAcNo11
            // 
            this.BtnAcNo11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo11.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo11.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo11.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo11.Appearance.Options.UseBackColor = true;
            this.BtnAcNo11.Appearance.Options.UseFont = true;
            this.BtnAcNo11.Appearance.Options.UseForeColor = true;
            this.BtnAcNo11.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo11.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo11.Image")));
            this.BtnAcNo11.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo11.Location = new System.Drawing.Point(738, 261);
            this.BtnAcNo11.Name = "BtnAcNo11";
            this.BtnAcNo11.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo11.TabIndex = 54;
            this.BtnAcNo11.ToolTip = "Find COA\'s Account";
            this.BtnAcNo11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo11.ToolTipTitle = "Run System";
            this.BtnAcNo11.Click += new System.EventHandler(this.BtnAcNo11_Click);
            // 
            // TxtAcNo11
            // 
            this.TxtAcNo11.EnterMoveNextControl = true;
            this.TxtAcNo11.Location = new System.Drawing.Point(208, 263);
            this.TxtAcNo11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo11.Name = "TxtAcNo11";
            this.TxtAcNo11.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo11.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo11.Properties.MaxLength = 40;
            this.TxtAcNo11.Properties.ReadOnly = true;
            this.TxtAcNo11.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo11.TabIndex = 52;
            // 
            // LblCOAARUninvoiced
            // 
            this.LblCOAARUninvoiced.AutoSize = true;
            this.LblCOAARUninvoiced.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAARUninvoiced.ForeColor = System.Drawing.Color.Black;
            this.LblCOAARUninvoiced.Location = new System.Drawing.Point(18, 266);
            this.LblCOAARUninvoiced.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAARUninvoiced.Name = "LblCOAARUninvoiced";
            this.LblCOAARUninvoiced.Size = new System.Drawing.Size(185, 14);
            this.LblCOAARUninvoiced.TabIndex = 51;
            this.LblCOAARUninvoiced.Text = "COA\'s Account (AR Uninvoiced) ";
            this.LblCOAARUninvoiced.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc12
            // 
            this.TxtAcDesc12.EnterMoveNextControl = true;
            this.TxtAcDesc12.Location = new System.Drawing.Point(365, 284);
            this.TxtAcDesc12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc12.Name = "TxtAcDesc12";
            this.TxtAcDesc12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc12.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc12.Properties.MaxLength = 80;
            this.TxtAcDesc12.Properties.ReadOnly = true;
            this.TxtAcDesc12.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc12.TabIndex = 57;
            // 
            // BtnAcNo12
            // 
            this.BtnAcNo12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo12.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo12.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo12.Appearance.Options.UseBackColor = true;
            this.BtnAcNo12.Appearance.Options.UseFont = true;
            this.BtnAcNo12.Appearance.Options.UseForeColor = true;
            this.BtnAcNo12.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo12.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo12.Image")));
            this.BtnAcNo12.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo12.Location = new System.Drawing.Point(738, 282);
            this.BtnAcNo12.Name = "BtnAcNo12";
            this.BtnAcNo12.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo12.TabIndex = 58;
            this.BtnAcNo12.ToolTip = "Find COA\'s Account";
            this.BtnAcNo12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo12.ToolTipTitle = "Run System";
            this.BtnAcNo12.Click += new System.EventHandler(this.BtnAcNo12_Click);
            // 
            // TxtAcNo12
            // 
            this.TxtAcNo12.EnterMoveNextControl = true;
            this.TxtAcNo12.Location = new System.Drawing.Point(208, 284);
            this.TxtAcNo12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo12.Name = "TxtAcNo12";
            this.TxtAcNo12.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo12.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo12.Properties.MaxLength = 40;
            this.TxtAcNo12.Properties.ReadOnly = true;
            this.TxtAcNo12.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo12.TabIndex = 56;
            // 
            // LblCOAStockBeginning
            // 
            this.LblCOAStockBeginning.AutoSize = true;
            this.LblCOAStockBeginning.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCOAStockBeginning.ForeColor = System.Drawing.Color.Black;
            this.LblCOAStockBeginning.Location = new System.Drawing.Point(36, 287);
            this.LblCOAStockBeginning.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCOAStockBeginning.Name = "LblCOAStockBeginning";
            this.LblCOAStockBeginning.Size = new System.Drawing.Size(167, 14);
            this.LblCOAStockBeginning.TabIndex = 55;
            this.LblCOAStockBeginning.Text = "COA\'s Account (Stock Beg.) ";
            this.LblCOAStockBeginning.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmItemCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 309);
            this.Name = "FrmItemCategory";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActiveInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkWastedInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkMovingAvgInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo12.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtItCtName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtItCtCode;
        private System.Windows.Forms.Label label1;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label LblCOAStock;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        private DevExpress.XtraEditors.CheckEdit ChkActiveInd;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc3;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo3;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo3;
        private System.Windows.Forms.Label LblCOACOGM;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc2;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo2;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo2;
        private System.Windows.Forms.Label LblCOACost;
        private DevExpress.XtraEditors.CheckEdit ChkWastedInd;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc4;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo4;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo4;
        private System.Windows.Forms.Label LblCOASales;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc7;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo7;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo7;
        private System.Windows.Forms.Label LblCOAPurchaseReturn;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc6;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo6;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo6;
        private System.Windows.Forms.Label LblCOASalesReturn;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc5;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo5;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo5;
        private System.Windows.Forms.Label LblCOACOGS;
        private DevExpress.XtraEditors.CheckEdit ChkMovingAvgInd;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc10;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo10;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo10;
        private System.Windows.Forms.Label LblCOAAR;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc8;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo8;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo8;
        private System.Windows.Forms.Label LblCOAAPUninvoiced;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc9;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo9;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo9;
        private System.Windows.Forms.Label LblCOAAPInvoiced;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc11;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo11;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo11;
        private System.Windows.Forms.Label LblCOAARUninvoiced;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc12;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo12;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo12;
        private System.Windows.Forms.Label LblCOAStockBeginning;
    }
}