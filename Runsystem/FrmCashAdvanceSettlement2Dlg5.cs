﻿#region Update
/*
    11/11/2022 [IBL/BBT] New Apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmCashAdvanceSettlement2Dlg5 : RunSystem.FrmBase2
    {
        #region Field

        private FrmCashAdvanceSettlement2 mFrmParent;
        private string mSQL = string.Empty;
        private int mCurRow = 0;

        #endregion

        #region Constructor

        public FrmCashAdvanceSettlement2Dlg5(FrmCashAdvanceSettlement2 FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = Row;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Budget Category";

                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //mFrmParent.SetLueCCtCode(ref LueCCtCode);
                Sl.SetLueBCCode(ref LueBCCode, string.Empty);
                if (mFrmParent.mIsSystemUseCostCenter) Sl.SetLueCCCode(ref LueCCCode, string.Empty, "Y");
                else Sl.SetLueCCCode(ref LueCCCode);
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Budget Category Code",
                        "Budget Category Name",
                        "Cost Category Code", 
                        "Cost Category Name",
                        "COA",

                        //6-7
                        "COA Description",
                        "Cost Center"

                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        120, 300, 120, 300, 130, 

                        //6-7
                        250, 200
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.BCCode, A.BCName, B.CCtCode, CONCAT(B.CCtName, ' (', IfNull(D.CCName, '-'), ')' ) CCtName, B.AcNo, C.AcDesc, D.CCName  ");
            SQL.AppendLine("FROM TblBudgetCategory A ");
            SQL.AppendLine("INNER JOIN TblCostCategory B ON A.CCtCode = B.CCtCode ");
            SQL.AppendLine("INNER JOIN TblCOA C ON B.AcNo = C.AcNo ");
            SQL.AppendLine("INNER JOIN TblCostCenter D ON B.CCCode = D.CCCode ");
            
            SQL.AppendLine("Where 0 = 0");
            if (mFrmParent.mIsSystemUseCostCenter)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=B.CCCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            if (mFrmParent.mIsCostCategoryFilteredInCashAdvanceSettlement)
                SQL.AppendLine("And D.DeptCode = @DeptCode ");


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@DeptCode", mFrmParent.mDeptCode);

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "C.AcNo", "C.AcDesc" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBCCode), "A.BCCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCCCode), "B.CCCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.BCCode;",
                        new string[] 
                        { 
                        
                            //0
                            "BCCode",
                            
                            //1-4
                            "BCName", "CCtCode", "CCtName", "AcNo", "AcDesc", "CCName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 31, Grd1, Row, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 32, Grd1, Row, 2);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 3, Grd1, Row, 3);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 4, Grd1, Row, 4);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 21, Grd1, Row, 5);
                Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 22, Grd1, Row, 6);

                //Sm.CopyGrdValue(mFrmParent.Grd2, mCurRow, 1, Grd1, Row, 2);
                mFrmParent.ComputeAmt1();
                mFrmParent.SetCostCategoryInfo();
                mFrmParent.SetVoucherInfo();
                mFrmParent.ComputeAmt2();
                mFrmParent.ClearItem();
                this.Close();
            }
        }
        

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event 

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA");
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue2(Sl.SetLueBCCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkBCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Budget Category");
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mFrmParent.mIsSystemUseCostCenter)  Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue3(Sl.SetLueCCCode), string.Empty, "Y");
            else Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Cost center");
        }

        #endregion
    }
    
}
