﻿#region Update
/*
    04/12/2017 [TKG] tambah filter dan informasi item.
    23/05/2018 [WED] tambah informasi volume, qty packaging, dan uom packaging berdasarkan parameter IsPPShowPackagingValue
    10/04/2019 [DITA]  Find PP tambah filter Remark
    10/04/2019 [DITA]  Find PP tambah subtotal
    14/01/2021 [WED/KSM] tambah tarik dari Sales Contract berdasarkan parameter IsSalesContractEnabled
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPPFind(FrmPP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case B.DocType When '1' Then B.ProductionOrderDocNo Else B.JCDocno End As DocNo2, ");
            SQL.AppendLine("Case A.Status When 'P' Then 'Planned' When 'R' Then 'Released' End As StatusDesc, ");
            SQL.AppendLine("Case B.DocType When '1' Then C.ItCode Else D.ItCode End As ItCode, ");
            SQL.AppendLine("Case B.DocType When '1' Then E.ItName Else F.ItName End As ItName, ");
            SQL.AppendLine("Case B.DocType When '1' Then C.Qty Else D.Qty End As Qty, ");
            SQL.AppendLine("Case B.DocType When '1' Then E.PlanningUomCode Else F.PlanningUomCode End As PlanningUomCode, ");
            SQL.AppendLine("Case B.DocType When '1' Then C.Qty*E.PlanningUomCodeConvert12 Else D.Qty*F.PlanningUomCodeConvert12 End As Qty2, ");
            SQL.AppendLine("Case B.DocType When '1' Then E.PlanningUomCode2 Else F.PlanningUomCode2 End As PlanningUomCode2, ");
            SQL.AppendLine("A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            if (mFrmParent.mIsPPShowPackagingValue)
            {
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    SQL.AppendLine(", ");
                    SQL.AppendLine("    Case C.DocType When '1' Then IfNull((G.Volume * G.QtyPackagingUnit), 0) When '3' Then IfNull((H.Volume * H.QtyPackagingUnit), 0) Else 0 End As Volume, ");
                    SQL.AppendLine("    Case C.DocType When '1' Then IfNull(G.QtyPackagingUnit, 0) When '3' Then IfNull(H.QtyPackagingUnit, 0) Else 0 End As QtyPackagingUnit, ");
                    SQL.AppendLine("    Case C.DocType When '1' Then IfNull(G.PackagingUnitUomCode, '') When '3' Then IfNull(H.PackagingUnitUomCode, '') Else '' End As PackagingUnitUomCode ");
                }
                else
                {
                    SQL.AppendLine(", IfNull((G.Volume * G.QtyPackagingUnit), 0) Volume, IfNull(G.QtyPackagingUnit, 0) QtyPackagingUnit, IfNull(G.PackagingUnitUomCode, '') PackagingUnitUomCode ");
                }
            }
            else
            {
                SQL.AppendLine(", 0 As Volume, 0 As QtyPackagingUnit, '' As PackagingUnitUomCode ");
            }
            SQL.AppendLine("From TblPPHdr A ");
            SQL.AppendLine("Inner Join TblPPDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblProductionOrderHdr C On B.ProductionOrderDocNo=C.DocNo And B.DocType='1' ");
            SQL.AppendLine("Left Join TblJCHdr D On B.JCDocNo=D.DocNo And B.DocType='2' ");
            SQL.AppendLine("Left Join TblItem E On C.ItCode=E.ItCode And B.DocType='1' ");
            SQL.AppendLine("Left Join TblItem F On D.ItCode=F.ItCode And B.DocType='2' ");
            if (mFrmParent.mIsPPShowPackagingValue)
            {
                SQL.AppendLine("    Left Join ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select T1.DocNo, T2.DNo, T4.ItCode, T2.QtyPackagingUnit, T2.PackagingUnitUomCode, T6.Volume ");
                SQL.AppendLine("        From TblSOHdr T1 ");
                SQL.AppendLine("        Inner Join TblSODtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        Inner Join TblCtQtDtl T3 On T1.CtQtDocNo = T3.DocNo And T2.CtQtDNo = T3.DNo ");
                SQL.AppendLine("        Inner Join TblItemPriceDtl T4 On T3.ItemPriceDocNo = T4.DocNo And T3.ItemPriceDNo = T4.DNo ");
                SQL.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
                SQL.AppendLine("        Inner Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T6.UomCode = T2.PackagingUnitUomCode ");
                SQL.AppendLine("    ) G On C.SODocNo = G.DocNo And E.ItCode = G.ItCode ");

                if (mFrmParent.mIsSalesContractEnabled)
                {
                    SQL.AppendLine("    Left Join ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select T1.DocNo, T4.Qty As QtyPackagingUnit, T5.SalesUomCode PackagingUnitUomCode, T6.Volume, T7.CtItCode ");
                    SQL.AppendLine("        From TblProductionOrderHdr T1 ");
                    SQL.AppendLine("        Inner Join TblSalesContract T2 On T1.SCDocNo = T2.DocNo ");
                    SQL.AppendLine("        Inner Join TblSalesMemoHdr T3 On T2.SalesMemoDocNo = T3.DocNo ");
                    SQL.AppendLine("        Inner Join TblSalesMemoDtl T4 On T3.DocNo = T4.DocNo And T1.SMDNo = T4.DNo ");
                    SQL.AppendLine("        Inner Join TblItem T5 On T4.ItCode = T5.ItCode ");
                    SQL.AppendLine("        Left Join TblItemPackagingUnit T6 On T5.ItCode = T6.ItCode And T5.SalesUomCode = T6.UomCode ");
                    SQL.AppendLine("        Left Join TblCustomerItem T7 On T3.CtCode = T7.CtCode And T5.ItCode = T7.ItCode ");
                    SQL.AppendLine("    ) H On C.DocNo = E.DocNo ");
                }
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel", 
                        "Status",
                        "Production Order# /" + Environment.NewLine+ "Job Costing#",
                        
                        //6-10
                        "Item's Code",
                        "Item's Name",
                        "Quantity",
                        "UoM",
                        "Quantity",

                        //11-15
                        "UoM",
                        "Remark",
                        "Volume",
                        "Packaging"+Environment.NewLine+"Qty",
                        "Packaging UoM",

                        //16-20
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",

                        //21
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 80, 80, 150, 
                        
                        //6-10
                        80, 200, 100, 80, 100, 
                        
                        //11-15
                        80, 200, 100, 100, 100, 
                        
                        //16-20
                        100, 80, 80, 100, 100, 
                        
                        //21
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 17, 20 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, false);
            if (!mFrmParent.mIsPPShowPackagingValue)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProductionOrderDocNo.Text, "DocNo2", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtRemark.Text, "Remark", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "DocNo2", "ItCode",
                            
                            //6-10
                            "ItName", "Qty", "PlanningUomCode", "Qty2", "PlanningUomCode2", 
                            
                            //11-15
                            "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                            //16-18
                            "Volume", "QtyPackagingUnit", "PackagingUnitUomCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 15);
                        }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 10, 13, 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Event

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtProductionOrderDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProductionOrderDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Production Order#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtRemark_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkRemark_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Remark");
        }
        #endregion

       

        #endregion

       
    }
}
