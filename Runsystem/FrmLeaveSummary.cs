﻿#region Update
/*
    21/09/2018 [TKG] New application
    26/07/2019 [DITA] tambah  fitur upload CSV untuk leave summary
    10/01/2022 [VIN/SIER] tambah  fitur upload CSV untuk leave summary
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmLeaveSummary : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmLeaveSummaryFind FrmFind;
        internal Boolean mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmLeaveSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Updating Annual Leave And Long Service Leave Summary";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "",
                        
                        //1-5
                        "Year",
                        "Leave Code",
                        "Leave Name",
                        "Employee's"+Environment.NewLine+"Code",
                        "Old Code",

                        //6-10
                        "Employee's Name",
                        "Department",
                        "Position",
                        "Join",
                        "Resign",
                        
                        //11-15
                        "Permanent",
                        "Start",
                        "End",
                        "Quota",
                        "Used",
                        
                        //16-17
                        "Unused",
                        "Balance"
                    },
                     new int[]
                    {
                        //0
                        20, 

                        //1-5
                        100, 0, 200, 100, 100, 
                        
                        //6-10
                        200, 200, 200, 100, 100,  
                        
                        //11-15
                        100, 100, 100, 100, 100, 
                        
                        //16-17
                        100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 8, 9, 10 }, false);
            Sm.GrdFormatDate(Grd1, new int[] { 9, 10, 11, 12, 13 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 16, 17 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 17 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueSiteCode, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 14, 15, 16 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueSiteCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 12, 13, 14, 15, 16 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, LueSiteCode, MeeRemark });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 16, 17 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLeaveSummaryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LeaveSummary", "TblLeaveSummaryHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveLeaveSummaryHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveLeaveSummaryDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 leave record.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.IsGrdValueEmpty(Grd1, r, 3, false, "Row : " + r.ToString() + Environment.NewLine + "Employee's name is empty.")) return true;

            return false;
        }


        private MySqlCommand SaveLeaveSummaryHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLeaveSummaryHdr(DocNo, DocDt, SiteCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @SiteCode, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveLeaveSummaryDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLeaveSummaryDtl ");
            SQL.AppendLine("(DocNo, DNo, Yr, EmpCode, LeaveCode, InitialDt, StartDt, EndDt, NoOfDays1, NoOfDays2, NoOfDays3, Balance, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @Yr, @EmpCode, @LeaveCode, @InitialDt, @StartDt, @EndDt, @NoOfDays1, @NoOfDays2, @NoOfDays3, @Balance, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblLeaveSummary Set ");
            SQL.AppendLine("    StartDt=@StartDt, EndDt=@EndDt, NoOfDays1=@NoOfDays1, NoOfDays2=@NoOfDays2, NoOfDays3=@NoOfDays3, Balance=@Balance, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Yr=@Yr And EmpCode=@EmpCode And LeaveCode=@LeaveCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("00", (r + 1).ToString()), 3));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 4));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParamDt(ref cm, "@InitialDt", Sm.GetGrdDate(Grd1, r, 11));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetGrdDate(Grd1, r, 12));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetGrdDate(Grd1, r, 13));
            Sm.CmParam<Decimal>(ref cm, "@NoOfDays1", Sm.GetGrdDec(Grd1, r, 14));
            Sm.CmParam<Decimal>(ref cm, "@NoOfDays2", Sm.GetGrdDec(Grd1, r, 15));
            Sm.CmParam<Decimal>(ref cm, "@NoOfDays3", Sm.GetGrdDec(Grd1, r, 16));
            Sm.CmParam<Decimal>(ref cm, "@Balance", Sm.GetGrdDec(Grd1, r, 17));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowLeaveSummaryHdr(DocNo);
                ShowLeaveSummaryDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLeaveSummaryHdr(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, SiteCode, Remark  ");
            SQL.AppendLine("From TblLeaveSummaryHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                    //0
                    "DocNo", 

                    //1-3
                    "DocDt", "SiteCode", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[2]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                }, true
            );
        }

        private void ShowLeaveSummaryDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Yr, A.LeaveCode, C.LeaveName, ");
            SQL.AppendLine("A.EmpCode, B.EmpCodeOld, B.EmpName, D.DeptName, E.PosName, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, A.InitialDt, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.NoOfDays1, A.NoOfDays2, A.NoOfDays3, A.Balance ");
            SQL.AppendLine("From TblLeaveSummaryDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode  ");
            SQL.AppendLine("Inner Join TblLeave C on A.LeaveCode=C.LeaveCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode  ");
            SQL.AppendLine("Left Join TblPosition E On B.PosCode=E.PosCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "Yr",
                    
                    //1-5
                    "LeaveCode", "LeaveName", "EmpCode", "EmpCodeOld", "EmpName", 
                    
                    //6-10
                    "DeptName",  "PosName", "JoinDt", "ResignDt", "InitialDt", 
                    
                    //11-15
                    "StartDt", "EndDt", "NoOfDays1", "NoOfDays2", "NoOfDays3", 
                    
                    //16
                    "Balance"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 16, 17 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueSiteCode, "Site"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmLeaveSummaryDlg(this, Sm.GetLue(LueSiteCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 14, 15, 16 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 16, 17 });
                    }
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsLueEmpty(LueSiteCode, "Site"))
                Sm.FormShowDialog(new FrmLeaveSummaryDlg(this, Sm.GetLue(LueSiteCode)));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 14, 15, 16 }, e);
            if (Sm.IsGrdColSelected(new int[] { 14, 15, 16 }, e.ColIndex))
                Grd1.Cells[e.RowIndex, 17].Value =
                    Sm.GetGrdDec(Grd1, e.RowIndex, 14) -
                    Sm.GetGrdDec(Grd1, e.RowIndex, 15) -
                    Sm.GetGrdDec(Grd1, e.RowIndex, 16);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueSiteCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
                ClearGrd();
            }
        }

        #endregion

        #region Button Clicks

        private void ProcessLeaveSummary1(ref List<LeaveSummary> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;

            using (var rd = new StreamReader(FileName))
            {
                if (Sm.GetLue(LueSiteCode).Length == 0) Sm.IsLueEmpty(LueSiteCode, "Site");
                else
                {
                    while (!rd.EndOfStream)
                    {
                        var splits = rd.ReadLine().Split(',');

                        if (splits.Count() != 8)
                        {
                            Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                            return;
                        }
                        else
                        {
                            var arr = splits.ToArray();
                            if (arr[0].Trim().Length > 0)
                            {
                                l.Add(new LeaveSummary()
                                {
                                    Yr = splits[0].Trim(),
                                    EmpCode = splits[1].Trim(),
                                    LeaveCode = splits[2].Trim(),
                                    StartDt = splits[3].Trim(),
                                    EndDt = splits[4].Trim(),
                                    Quota = Decimal.Parse(splits[5].Trim().Length > 0 ? splits[5].Trim() : "0"),
                                    Used = Decimal.Parse(splits[6].Trim().Length > 0 ? splits[6].Trim() : "0"),
                                    Unused = Decimal.Parse(splits[7].Trim().Length > 0 ? splits[7].Trim() : "0"),

                                });
                            }
                            else
                            {
                                Sm.StdMsg(mMsgType.NoData, "");
                                return;
                            }

                        }
                    }
                }
            }
        }

        private void ProcessLeaveSummary2(ref List<LeaveSummary> l, ref List<LeaveSummary2> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var ItCode = string.Empty;
            var Filter = string.Empty;

            string mCondition = string.Empty;
            for (int i = 0; i < l.Count; i++)
            {
                if (mCondition.Length > 0) mCondition += ",";
                mCondition += string.Concat(l[i].Yr, l[i].LeaveCode, l[i].EmpCode);
            }

            //if (Filter.Length > 0)
            //    Filter = " Where (" + Filter + ") ";
            //else
            //    Filter = " Where 0=1 ";

            //SQL.AppendLine("Select A.ItCode, A.ItName, B.ItGrpName ");
            //SQL.AppendLine("From TblItem A ");
            //SQL.AppendLine("Left Join TblItemGroup B On A.ItGrpCode=B.ItGrpCode ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine(";");

            SQL.AppendLine("Select A.Yr, A.LeaveCode, B.LeaveName, ");
            SQL.AppendLine("A.EmpCode, C.EmpCodeOld, C.EmpName, D.DeptName, E.PosName, ");
            SQL.AppendLine("C.JoinDt, C.ResignDt, A.InitialDt, A.StartDt, A.EndDt ");
            SQL.AppendLine("From TblLeaveSummary A ");
            SQL.AppendLine("Inner Join TblLeave B On A.LeaveCode = B.LeaveCode ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode And C.SiteCode=@SiteCode ");
            SQL.AppendLine("Inner Join TblDepartment D On C.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine("Where Find_in_set(Concat(A.Yr, A.LeaveCode, A.EmpCode), @Condition) ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();

                Sm.CmParam<String>(ref cm, "@Condition", mCondition);
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    "Yr",
                    "LeaveCode", "LeaveName", "EmpCode", "EmpCodeOld", "EmpName",
                    "DeptName", "PosName", "JoinDt", "ResignDt", "InitialDt",
                    "StartDt", "EndDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new LeaveSummary2()
                        {
                            Yr = Sm.DrStr(dr, c[0]),
                            LeaveCode = Sm.DrStr(dr, c[1]),
                            LeaveName = Sm.DrStr(dr, c[2]),
                            EmpCode = Sm.DrStr(dr, c[3]),
                            EmpCodeOld = Sm.DrStr(dr, c[4]),
                            EmpName = Sm.DrStr(dr, c[5]),
                            DeptName = Sm.DrStr(dr, c[6]),
                            PosName = Sm.DrStr(dr, c[7]),
                            JoinDt = Sm.DrStr(dr, c[8]),
                            ResignDt = Sm.DrStr(dr, c[9]),
                            InitialDt = Sm.DrStr(dr, c[10]),
                            StartDt = string.Empty,
                            EndDt = string.Empty,
                            Quota = 0m,
                            Used = 0m,
                            Unused = 0m,
                            Balance = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessLeaveSummary3(ref List<LeaveSummary> l, ref List<LeaveSummary2> l2)
        {
            for (int i = 0; i < l.Count; i++)
            {
                for (int j = 0; j < l2.Count; j++)
                {
                    if (l[i].Yr == l2[j].Yr && l[i].LeaveCode == l2[j].LeaveCode && l[i].EmpCode == l2[j].EmpCode)
                    {
                        l2[j].Quota = l[i].Quota;
                        l2[j].Used = l[i].Used;
                        l2[j].Unused = l[i].Unused;
                        l2[j].Balance = l2[j].Quota - l2[j].Used - l2[j].Unused;
                        l2[j].StartDt = l[i].StartDt;
                        l2[j].EndDt = l[i].EndDt;
                        break;
                    }
                }
            }
        }

        private void ProcessLeaveSummary4(ref List<LeaveSummary> l, ref List<LeaveSummary2> l2)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l2.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[1].Value = l2[i].Yr;
                r.Cells[2].Value = l2[i].LeaveCode;
                r.Cells[3].Value = l2[i].LeaveName;
                r.Cells[4].Value = l2[i].EmpCode;
                r.Cells[5].Value = l2[i].EmpCodeOld;
                r.Cells[6].Value = l2[i].EmpName;
                r.Cells[7].Value = l2[i].DeptName;
                r.Cells[8].Value = l2[i].PosName;

                if (l2[i].JoinDt.Length > 0) r.Cells[9].Value = Sm.ConvertDate(l2[i].JoinDt);
                else r.Cells[9].Value = string.Empty;

                if (l2[i].ResignDt.Length > 0) r.Cells[10].Value = Sm.ConvertDate(l2[i].ResignDt);
                else r.Cells[10].Value = string.Empty;

                if (l2[i].InitialDt.Length > 0) r.Cells[11].Value = Sm.ConvertDate(l2[i].InitialDt);
                else r.Cells[11].Value = string.Empty;

                //if (l2[i].StartDt.Length > 0) r.Cells[12].Value = Sm.ConvertDate(l2[i].StartDt);
                //else r.Cells[12].Value = string.Empty;

                //if (l2[i].EndDt.Length > 0) r.Cells[13].Value = Sm.ConvertDate(l2[i].EndDt);
                //else r.Cells[13].Value = string.Empty;
                r.Cells[12].Value = Sm.ConvertDate(l2[i].StartDt);
                r.Cells[13].Value = Sm.ConvertDate(l2[i].EndDt);
                r.Cells[14].Value = l2[i].Quota;
                r.Cells[15].Value = l2[i].Used;
                r.Cells[16].Value = l2[i].Unused;
                r.Cells[17].Value = l2[i].Balance;
                
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 16, 17 });
            Grd1.EndUpdate();
        }



        private void BtnCSV_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<LeaveSummary>();
                var l2 = new List<LeaveSummary2>();

                ClearGrd();
                try
                {
                    ProcessLeaveSummary1(ref l);
                    if (l.Count > 0)
                    {
                        ProcessLeaveSummary2(ref l, ref l2);
                        ProcessLeaveSummary3(ref l, ref l2);
                        ProcessLeaveSummary4(ref l, ref l2);
                    }
                    else
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    l.Clear();
                    Cursor.Current = Cursors.Default;
                }
            }

        }
        #endregion


        #endregion

        #region Class
        private class LeaveSummary
        {
            public string Yr { get; set; }
            public string LeaveCode { get; set; }
            public string EmpCode { get; set; }
            public decimal Quota { get; set; }
            public decimal Used { get; set; }
            public decimal Unused { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }

        }

        private class LeaveSummary2
        {
            public string Yr { get; set; }
            public string LeaveCode { get; set; }
            public string LeaveName { get; set; }
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public string InitialDt { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal Quota { get; set; }
            public decimal Used { get; set; }
            public decimal Unused { get; set; }
            public decimal Balance { get; set; }

        }
        #endregion
    }


}
