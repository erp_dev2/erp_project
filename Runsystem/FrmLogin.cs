﻿#region Update
/*
    04/08/2017 [TKG] Set PosNo
    20/09/2017 [HAR] ubah form login
    25/06/2018 [TKG] Berdasarkan parameter IsAppNeedToUpdateNewVersion, apakah user perlu update aplikasi ke versi terbaru.
    02/09/2019 [WED] tidak bisa login jika session masih aktif, berdasarkan parameter IsUseLoginSession
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
    28/05/2020 [DITA/Runmarket] jika runmarket --> background gambar login diganti
    03/07/2020 [HAR/PHT] jika Pht --> background gambar login diganti
    08/01/2021 [DITA/ALL] sesuai parameter : IsPasswordForLoginNeedToEncode dapat memilih login berdasarkan password yg di encode atau tidak
    25/01/2021 [TKG/ALL] input spasi akan dihilangkan di textbox user, password, database, server
    08/03/2021 [TKG/ALL] menggunakan stored procedure untuk cek validasi user dan password
    13/03/2021 [TKG/ALL] mengisi label company name dengan RUN System Inc untuk semua customer.
    09/04/2021 [TKG/PHT] memunculkan warning agar user merubah passwordnya karena password sudah expired
    11/05/2021 [ICA/PADI] jika Padi --> background gambar login diganti
    04/04/2022 [SET/PRODUCT] meengganti lock view login menjadi bisa menampilkan aplikasi lain
    10/05/2022 [BRI/PRODUCT] tambah remark berdasarkan param IsLoginHistoryRecordFailedLogin
    11/05/2022 [DITA/PRODUCT] param IsLoginHistoryRecordFailedLogin dimasukin ke setparameter + waktu insert ke log error juga dipagerin param IsLoginHistoryRecordFailedLogin
    04/08/2022 [MYA/PRODUCT] Perlu adanya Account Lockout Thershold berkaitan dengan pengaturan keamanan terkait sistem informasi aplikasi
    05/08/2022 [MYA/PRODUCT] Perlu adanya Account Lockout Duration berkaitan dengan pengaturan kemanan terkait sistem informasi aplikasi
    09/08/2022 [MYA/PRODUCT] Perlu adanya Reset User Lockout Counter After berkaitan dengan pengaturan keamanan terkait sistem informasi aplikasi
    18/08/2022 [DITA/SIER] bug ketika param didalam method CheckCounterLogin kosong maka akan muncul warning input string was not a correct format
    14/03/2023 [BRI/GSS] bug parameter
    17/03/2023 [HAR/PHT] tambah parameter contact IT dan variabel IsPwdAlreadyExpired
    06/04/2023 [HAR/PHT] validasi contact IT berisi link url support ketika gagal login beberapa kali
    17/04/2023 [WED/ALL] set global var baru IsUsePreparedStatement
*/
#endregion

#region Namespace

using System;
using System.Windows.Forms;
using System.Collections.Generic;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Net;
using System.Drawing;
using System.Text;
using System.IO;
using System.Reflection;
using System.Configuration;
using System.Security.Cryptography;

using Dxe = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Wa = RunSystem.WinAPI;

#endregion

namespace RunSystem
{
    public partial class FrmLogin : Form
    {
        #region Field

        private Form mFrmParent;
        public string mPwdEncode = string.Empty;
        private bool mIsLoginHistoryRecordFailedLogin = false;
        public bool IsPwdAlreadyExpired = false;
        private string mSystemAdministratorContact = string.Empty;

        #endregion

        #region Constructor

        public FrmLogin(Form FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            
        }

        #endregion

        #region Method

        private void GetAppConfigInfo()
        {
            try
            {
                string UserCode = Sm.ReadSetting("UserCode");
                if (UserCode.Length != 0)
                {
                    TxtUserCode.Text = UserCode;
                    ChkKeepUserCode.Checked = true;
                }
                TxtServer.Text = Gv.Server = Sm.ReadSetting("Server");
                TxtDatabase.Text = Gv.Database = Sm.ReadSetting("Database");
                var Port = Sm.ReadSetting("Port")??string.Empty;
                Gv.Port = Port;
                Gv.PosNo = Sm.ReadSetting("PosNo") ?? "1";
                LblCompanyName.Text = "RUN System Inc."; //Gv.CompanyName;
                LblVersionNo.Text = "Version " + Gv.VersionNo;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetAppConfig()
        {
            try
            {
                Sm.WriteSetting("Server", TxtServer.Text);
                Sm.WriteSetting("Database", TxtDatabase.Text);
                Sm.WriteSetting("UserCode", ChkKeepUserCode.Checked ? TxtUserCode.Text : "");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        //private bool IsUserNotValid()
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select ");
        //    SQL.AppendLine("    Case When ");
        //    SQL.AppendLine("        Not Exists( ");
        //    SQL.AppendLine("            Select UserCode From TblUser  ");
        //    SQL.AppendLine("            Where Upper(UserCode)=@UserCode ");
        //    SQL.AppendLine("            And (ExpDt Is Null Or (ExpDt Is Not Null And ExpDt>Replace(CurDate(), '-', ''))) ");
        //    SQL.AppendLine("            ) Then 1  ");
        //    SQL.AppendLine("        Else ");
        //    SQL.AppendLine("            Case When  ");
        //    SQL.AppendLine("                Not Exists( ");
        //    SQL.AppendLine("                    Select UserCode From TblUser Where Upper(UserCode)=@UserCode And Pwd=@Pwd ");
        //    SQL.AppendLine("                    ) Then 2  ");
        //    SQL.AppendLine("            Else ");
        //    SQL.AppendLine("                0 End ");
        //    SQL.AppendLine("    End As ErrType;");

        //    var cm = new MySqlCommand
        //    {
        //        CommandText =SQL.ToString()
        //    };
        //    Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text.ToUpper());
        //    if (Sm.GetParameter("IsPasswordForLoginNeedToEncode") == "Y")
        //        Sm.CmParam<String>(ref cm, "@Pwd", mPwdEncode);
        //    else
        //        Sm.CmParam<String>(ref cm, "@Pwd", TxtPwd.Text);
        //    var ErrType = Sm.GetValue(cm);
            
        //    switch(ErrType)
        //    {
        //        case "1":
        //            Sm.StdMsg(mMsgType.Warning, "You don't have any access to log-in.");
        //            return true;
        //        case "2":
        //            Sm.StdMsg(mMsgType.Warning, "Invalid password.");
        //            return true;
        //    }
        //    return false;
        //}

        private bool IsUserValid()
        {
            if (!CheckLockedAccount())
                return false;

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@p_UserCode", false, 1, TxtUserCode.Text.ToUpper());
            Sm.CmParam<String>(ref cm, "@p_Pwd", false, 1, Sm.GetParameterBoo("IsPasswordForLoginNeedToEncode")?mPwdEncode:TxtPwd.Text);
            Sm.CmParam<String>(ref cm, "@p_Result", false, 2, string.Empty);
            Sm.ExecCommandSP(ref cm, "spCheckingUser");
            
            if (cm.Parameters["@p_Result"].Value.ToString() == "1")
            {
                ResetAccount();
                return true;
            }

            CheckCounterLogin();
            InsertLoginHistory();
            Sm.StdMsg(mMsgType.Warning, "Invalid user/password.");
            UpdateAccountStatus();
            if (mIsLoginHistoryRecordFailedLogin) InsertLogError("Invalid user/password");
            return false;
        }

        private void InsertLog()
        {
            Gv.LogIn = Sm.GetValue("Select Concat(Replace(CurDate(), '-', ''), Replace(CurTime(), ':', ''));");
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLog(UserCode, LogIn, IP, Machine, SysVer, ");
            if (mIsLoginHistoryRecordFailedLogin)
                SQL.AppendLine("Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@UserCode, @LogIn, @IP, @Machine, @SysVer, ");
            if (mIsLoginHistoryRecordFailedLogin)
                SQL.AppendLine("@Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Login", Gv.LogIn);
            Sm.CmParam<String>(ref cm, "@IP", GetIP());
            Sm.CmParam<String>(ref cm, "@Machine", System.Environment.MachineName);
            Sm.CmParam<String>(ref cm, "@SysVer", Gv.VersionNo);
            Sm.CmParam<String>(ref cm, "@Remark", "Success");
            Sm.ExecCommand(cm);
        }

        private void InsertLogError(string mRemark)
        {
            Gv.LogIn = Sm.GetValue("Select Concat(Replace(CurDate(), '-', ''), Replace(CurTime(), ':', ''));");
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLogError(UserCode, LogIn, IP, Machine, SysVer, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@UserCode, @LogIn, @IP, @Machine, @SysVer, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text);
            Sm.CmParam<String>(ref cm, "@Login", Gv.LogIn);
            Sm.CmParam<String>(ref cm, "@IP", GetIP());
            Sm.CmParam<String>(ref cm, "@Machine", System.Environment.MachineName);
            Sm.CmParam<String>(ref cm, "@SysVer", Gv.VersionNo);
            Sm.CmParam<String>(ref cm, "@Remark", mRemark);
            Sm.ExecCommand(cm);
        }

        private void InsertLoginHistory()
        {
            string DNo = Sm.GetValue("Select IFNULL(DNo, 0) From TblLoginHistory Where UserCode = '" + TxtUserCode.Text + "' ORDER BY DNo DESC limit 1");
            if (DNo.Length == 0)
                DNo = "0";
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblLoginHistory(UserCode, DNo, LoginDt) ");
            SQL.AppendLine("Values(@UserCode, @DNo, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text.ToUpper());
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Convert.ToInt32(DNo) + 1), 3));
            Sm.ExecCommand(cm);
        }

        private void UpdateAccountStatus()
        {
            int
                CheckLoginFail,
                mUserLockoutTreshold = 0;

            CheckLoginFail = Convert.ToInt32(Sm.GetValue("Select COUNT(UserCode) From TblLoginHistory Where UserCode = '" + TxtUserCode.Text + "'"));
            mUserLockoutTreshold = Convert.ToInt32(Sm.GetValue("Select ParValue From TblParameter Where Parcode = 'UserLockoutTreshold'"));

            if(CheckLoginFail == mUserLockoutTreshold && mUserLockoutTreshold > 0)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblUser Set ");
                SQL.AppendLine("    StatusInd='N', BlockDt=CurrentDateTime() ");
                SQL.AppendLine("Where UserCode=@UserCode; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text.ToUpper());
                Sm.ExecCommand(cm);
                
            }

        }

        private bool CheckLockedAccount()
        {
            string UserStatus, BlockDt, contactIT = string.Empty;
            int Param = 0;

            UserStatus = Sm.GetValue("Select StatusInd From TblUser Where UserCode = '" + TxtUserCode.Text + "'");
            BlockDt = Sm.GetValue("Select BlockDt From TblUser Where UserCode = '" + TxtUserCode.Text + "'");
            Param = Convert.ToInt32(Sm.GetValue("Select ParValue From TblParameter Where Parcode = 'UserLockoutDuration'"));
            contactIT = Sm.GetParameter("SystemAdministratorContact");

            if (BlockDt.Length > 0)
            {
                DateTime Dt = Sm.ConvertDateTime(BlockDt).AddMinutes(Param);
                DateTime Dt1 = Sm.ConvertDateTime(Sm.ServerCurrentDateTime());

                if (Dt1 >= Dt && Param != 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Update TblUser Set ");
                    SQL.AppendLine("    StatusInd='Y', BlockDt=NULL ");
                    SQL.AppendLine("Where UserCode=@UserCode; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text.ToUpper());
                    Sm.ExecCommand(cm);
                    ResetAccount();
                    return true;
                }
            }

            

            if (UserStatus == "N")
            {
                if (contactIT.Length > 1)
                {
                    if (MessageBox.Show("This user is currently locked because too many failed attempts." + Environment.NewLine + "Click Yes to contact your system administrator", "Info", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start("" + contactIT + "");
                    }
                    return false;
                }
                else
                {
                    Sm.StdMsg(mMsgType.Warning, "This user is currently locked because too many failed attempts.");
                    return false;
                }
            }

            return true;
        }

        private void CheckCounterLogin()
        {
            string LastDt = string.Empty;
            int Param = 0;
            int Param2 = 0;
            int FailCheck = 0;

            string userlockout = Sm.GetValue("Select ParValue From TblParameter Where Parcode = 'UserLockoutTreshold'");
            string resetuser = Sm.GetValue("Select ParValue From TblParameter Where Parcode = 'ResetUserLockoutCounterAfter'");


            Param2 = Convert.ToInt32(userlockout.Length > 0 ? userlockout : "0");
            FailCheck = Convert.ToInt32(Sm.GetValue("Select COUNT(DNo) from TblLoginHistory WHERE UserCode = '" + TxtUserCode.Text + "'"));
            LastDt = Sm.GetValue("Select LoginDt from TblLoginHistory WHERE Usercode = '" + TxtUserCode.Text + "' ORDER BY DNo DESC LIMIT 1;");
            Param = Convert.ToInt32(resetuser.Length > 0 ? resetuser : "0");

            //if (FailCheck == (Param2 - 1))
            {
                if (LastDt.Length > 0)
                {
                    DateTime Dt = Sm.ConvertDateTime(LastDt).AddMinutes(Param);
                    DateTime Dt1 = Sm.ConvertDateTime(Sm.ServerCurrentDateTime());

                    if (Dt1 >= Dt && Param != 0)
                    {
                        ResetAccount();
                    }
                }
            }
        }

        private void ResetAccount()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("DELETE FROM tblloginhistory  ");
            SQL.AppendLine("Where UserCode=@UserCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", TxtUserCode.Text.ToUpper());
            Sm.ExecCommand(cm);
        }

        private string GetIP()
        {
            foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                if (Sm.CompareStr(ip.AddressFamily.ToString(), "InterNetwork")) return ip.ToString();
            return "";
        }

        private void SetParameter()
        {
            Gv.FormatNum0 = "{0:#,##0.00##}";

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = 
                        "Select ParCode, ParValue From TblParameter " +
                        "Where ParCode In ('CompanyLogoWidth', 'CompanyLogoFile', 'CompanyLogoFile2', 'ImgFileItem', 'FormatNum0', 'AutoLogOffInterval', 'IsSaveAppUsageHistoryActivated', 'IsLoginHistoryRecordFailedLogin', 'IsUsePreparedStatement', 'IsActivityLogStored') " +
                        "Order By ParCode"
                };
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                while (dr.Read())
                {
                    switch (Sm.DrStr(dr, c[0]).Trim())
                    {
                        case "CompanyLogoWidth":
                            Gv.CompanyLogoWidth = Sm.DrStr(dr, c[1]).Length == 0 ? 50 : Int32.Parse(Sm.DrStr(dr, c[1]));
                            break;
                        case "CompanyLogoFile":
                            Gv.CompanyLogoFile = Sm.DrStr(dr, c[1]).Length == 0 ? "XXX" : Sm.DrStr(dr, c[1]);
                            break;
                        case "CompanyLogoFile2":
                            Gv.CompanyLogoFile2 = Sm.DrStr(dr, c[1]).Length == 0 ? "XXX" : Sm.DrStr(dr, c[1]);
                            break;
                        case "ImgFileItem":
                            Gv.ImgFileItem = Sm.DrStr(dr, c[1]).Length == 0 ? "XXX" : Sm.DrStr(dr, c[1]);
                            break;
                        case "FormatNum0":
                            Gv.FormatNum0 = Sm.DrStr(dr, c[1]).Length == 0 ? "{0:#,##0.00##}" : Sm.DrStr(dr, c[1]);
                            break;
                        case "AutoLogOffInterval":
                            Gv.AutoLogOff = Sm.DrStr(dr, c[1]).Length == 0 ? 3600000 : 60000 * decimal.Parse(Sm.DrStr(dr, c[1]));
                            break;
                        case "IsSaveAppUsageHistoryActivated":
                            Gv.IsSaveAppUsageHistoryActivated = Sm.DrStr(dr, c[1]) == "Y";
                            break;
                        case "IsLoginHistoryRecordFailedLogin":
                            mIsLoginHistoryRecordFailedLogin = Sm.DrStr(dr, c[1]) == "Y";
                            break;
                        case "IsUsePreparedStatement":
                            Gv.IsUsePreparedStatement = Sm.DrStr(dr, c[1]) == "Y";
                            break;
                        case "IsActivityLogStored":
                            Gv.IsActivityLogStored = Sm.DrStr(dr, c[1]) == "Y";
                            break;
                    }
                }
                dr.Close();
            }
        }

        private void SetPosNo()
        { 
            if (Sm.GetParameterBoo("IsPOSPosNoUseExtFileSetting"))
            {
                var l = new List<string>();
                GetListOfPosNo(ref l);
                foreach (var i in l)
                { 
                    if (Sm.IsFileExist(
                        Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\",
                        string.Concat("RunSystemPosNo", i), 
                        "Txt"))
                    {
                        Gv.PosNo = i;
                        break;
                    }
                }
            }
        }

        private void GetListOfPosNo(ref List<string> l)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var dr = new MySqlCommand()
                {    
                    Connection = cn,
                    CommandTimeout = 600,
                    CommandText = "Select PosNo From TblPosNo Order By PosNo;"
                }.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "PosNo" });
                if (dr.HasRows)
                {
                    while (dr.Read()) l.Add(Sm.DrStr(dr, c[0]));
                }
                dr.Close();
            }
        }

        private void ShowDBProcessListNotification()
        {
            if (!Sm.GetParameterBoo("IsShowDBProcessListNotification")) return;

            var MinRecordForDBProcessListNotification = Sm.GetParameterDec("MinRecordForDBProcessListNotification");
            var cm = new MySqlCommand() { CommandText = "Select Count(1) From Information_Schema.PROCESSLIST Where db Is Not Null;" };
            var Total = Sm.GetValue(cm);
            if (Total.Length > 0)
            {
                if (decimal.Parse(Total) > MinRecordForDBProcessListNotification)
                    Sm.StdMsg(mMsgType.Info, "DB server process : " + Total + " records.");
            }
        }

        private bool IsPwdExpired()
        {
            var IsExpired = Sm.GetValue(
                "Select 1 From TblUser A " +
                "Inner Join TblParameter B On B.ParCode='MaxPwdAge' And B.ParValue is Not Null " +
                "Inner Join (Select Replace(Curdate(), '-', '') As Dt) C On 1=1 " +
                "Where A.CreateDt Is Not Null And A.UserCode=@Param And Date_Add(str_to_date(IfNull(A.PwdLastUpDt, Left(A.CreateDt, 8)), '%Y%m%d'), Interval B.ParValue Day)<str_to_date(C.Dt, '%Y%m%d'); ", 
                TxtUserCode.Text);

            if (IsExpired.Length != 0)
            {
                Sm.StdMsg(mMsgType.Info, 
                    "Your password already expired." + Environment.NewLine + 
                    "You need to change your passsword.");
                IsPwdAlreadyExpired = true;  //ini untuk menginformasikan ke form main kalo pass user expired
            }
            return false;
        }

        #endregion

        #region Event

        #region Form Event

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            //SET
            

            panel1.Left = (this.Width - panel1.Size.Width) / 2 + (this.Left / 2);
            panel1.Top = (this.Height - panel1.Size.Height) / 2 + (this.Top / 2);

            GetAppConfigInfo();
            if (TxtDatabase.Text.Contains("runmarket"))
            {
                panel2.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject("BGLoginRunmarket");
                this.Text = "Run Market";
                this.Icon = (Icon)Properties.Resources.ResourceManager.GetObject("RMIcon");
            }

            if (TxtDatabase.Text.Contains("pht") || TxtDatabase.Text.Contains("phthr") || TxtDatabase.Text.Contains("phtbook") || TxtDatabase.Text.Contains("phthrtest") || TxtDatabase.Text.Contains("phtficotest") || TxtDatabase.Text.Contains("runsystempht"))
            {
                panel2.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject("BGLogin3");
            }

            if ((TxtDatabase.Text.Contains("padi")) || TxtDatabase.Text.Contains("eproc"))
            {
                panel2.BackgroundImage = (Image)Properties.Resources.ResourceManager.GetObject("BGLoginPaDi");
            }
                        
            TxtUserCode.Focus();
        }

        #endregion

        #region Misc Control Event

        private void TxtUserCode_Validated(object sender, EventArgs e)
        {
            TxtUserCode.Text = TxtUserCode.Text.Replace(" ", string.Empty).Trim();
        }

        private void TxtPwd_Validated(object sender, EventArgs e)
        {
            TxtPwd.Text = TxtPwd.Text.Replace(" ", string.Empty).Trim();
        }

        private void TxtDatabase_Validated(object sender, EventArgs e)
        {
            TxtDatabase.Text = TxtDatabase.Text.Replace(" ", string.Empty).Trim();
        }

        private void TxtServer_Validated(object sender, EventArgs e)
        {
            TxtServer.Text = TxtServer.Text.Replace(" ", string.Empty).Trim();
        }

        private void TxtUserCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TxtPwd.Focus();
            }
        }

        private void TxtPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BtnLogin.Focus();
            }
        }

        private void TxtDatabase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TxtServer.Focus();
            }
        }

        private void TxtServer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TxtUserCode.Focus();
            }
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (TxtDatabase.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Database is empty.");
                TxtDatabase.Focus();
                return;
            }
            else if (TxtServer.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Server is empty.");
                TxtServer.Focus();
                return;
            }
            else if (TxtUserCode.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "User ID is empty.");
                TxtUserCode.Focus();
                return;
            }
            else if (TxtPwd.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Password is empty.");
                TxtPwd.Focus();
                return;
            }

            try
            {
                mPwdEncode = Sm.GetHashBase64(Sm.GetHashSha256(TxtPwd.Text));
                Gv.Server = TxtServer.Text;
                Gv.Database = TxtDatabase.Text;
                Gv.AnimateForm = 1;
                Gv.IsUsePreparedStatement = false;
                Gv.IsActivityLogStored = false;

                ConnectDB();
                SetParameter();
                if (IsLoginInvalid() || IsAppVersionInvalid() || !IsUserValid() || IsPwdExpired()) return;

                Gv.ApplicationPath = Application.StartupPath;
                Gv.CurrentUserCode = SetUserCode(TxtUserCode.Text.ToUpper());
                if (IsUseLoginSession()) return;
                SetPosNo();
                ShowWarningLogin();
                InsertLog();
                SetAppConfig();
                ShowDBProcessListNotification();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ConnectDB()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            { cn.Open(); }
        }

        private string SetUserCode(string UserCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select UserCode From TblUser Where Upper(UserCode)=@UserCode;"
            };
            Sm.CmParam<String>(ref cm, "@UserCode", UserCode);
            return Sm.GetValue(cm);   
        }

        private void ShowWarningLogin()
        {
            var WarningLoginHr = Sm.GetParameter("WarningLoginHr");

            if ((WarningLoginHr.Length == 0 ? "0" : WarningLoginHr)!="0")
            {
                var WarningLogin = GetWarningLogin(WarningLoginHr);
                if (WarningLogin.Length != 0)
                {
                    Sm.StdMsg(mMsgType.Info,
                        "You have not closed the application properly on " + WarningLogin);
                }
            }
        }

        private string GetWarningLogin(string WarningLoginHr)
        {
            string WarningLogin = string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct Concat(Machine, ' (', IP, ')') As Place "); 
            SQL.AppendLine("From TblLog  ");
            SQL.AppendLine("Where UserCode=@UserCode  ");
            SQL.AppendLine("And Logout is Null ");
            SQL.AppendLine("And Login Between  ");
            SQL.AppendLine("Date_Format(DATE_ADD(STR_TO_DATE(CurrentDateTime(), '%Y%m%d%H%i'),INTERVAL -" + WarningLoginHr + " HOUR), '%Y%m%d%H%i') ");
            SQL.AppendLine("And CurrentDateTime() ");
            SQL.AppendLine("Order By Login Desc ");
            SQL.AppendLine("Limit 10; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "Place" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        WarningLogin += Environment.NewLine + "- " + Sm.DrStr(dr, 0);
                }
                dr.Close();
            }

            return WarningLogin;
        }

        private bool IsLoginInvalid()
        {
            if (Sm.GetParameterBoo("IsLoginInvalid"))
            {
                Sm.StdMsg(mMsgType.Info, "Under maintenance.");
                if (mIsLoginHistoryRecordFailedLogin) InsertLogError("Under maintenance");
                return true;
            }
            return false;
        }

        private bool IsAppVersionInvalid()
        {
            if (Sm.GetParameterBoo("IsAppNeedToUpdateNewVersion"))
            {
                var NewVersion = Sm.GetParameter("RunSystemDownloadVersion");
                var CurrentVersion = Gv.VersionNo;

                if (NewVersion.Length > 0 && CurrentVersion.Length > 0 &&
                    decimal.Parse(CurrentVersion.Replace(".", "")) < decimal.Parse(NewVersion.Replace(".", "")))
                {
                    Sm.StdMsg(mMsgType.Info,
                        "Your application is out of date." + Environment.NewLine + 
                        "You need to update your application." + Environment.NewLine + 
                        "Please contact system administrator if you need any help.");
                    if (mIsLoginHistoryRecordFailedLogin) InsertLogError("Your application is out of date");
                    return true;
                }
            }
            return false;
        }

        private bool IsUseLoginSession()
        {
            if (Sm.GetParameterBoo("IsUseLoginSession") == true)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                string mUserCode = string.Empty, mLogin = string.Empty, 
                    mIP = string.Empty, mMachine = string.Empty, mSysVer = string.Empty;

                SQL.AppendLine("Select UserCode, Login, IP, Machine, SysVer ");
                SQL.AppendLine("From TblLog ");
                SQL.AppendLine("Where UserCode = @UserCode ");
                SQL.AppendLine("And Logout Is Null ");
                SQL.AppendLine("And Exists (Select 1 From TblParameter Where ParCode = 'IsUseLoginSession' And ParValue = 'Y') ");
                SQL.AppendLine("Order By Login Desc ");
                SQL.AppendLine("Limit 1; ");

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "UserCode", "Login", "IP", "Machine", "SysVer" });
                    if (dr.HasRows)
                    {
                        while(dr.Read())
                        {
                            mUserCode = Sm.DrStr(dr, c[0]);
                            mLogin = string.Concat(Sm.DrStr(dr, c[1]).Substring(6, 2), "/", Sm.DrStr(dr, c[1]).Substring(4, 2), "/", Sm.Left(Sm.DrStr(dr, c[1]), 4), " ", Sm.DrStr(dr, c[1]).Substring(8, 2), ":", Sm.DrStr(dr, c[1]).Substring(10, 2), ":", Sm.Right(Sm.DrStr(dr, c[1]), 2));
                            mIP = Sm.DrStr(dr, c[2]);
                            mMachine = Sm.DrStr(dr, c[3]);
                            mSysVer = Sm.DrStr(dr, c[4]);
                        }
                    }
                    dr.Close();
                }

                if (mUserCode.Length > 0)
                {
                    var mMsgs = new StringBuilder();

                    mMsgs.AppendLine("Usercode \t: " +  mUserCode);
                    mMsgs.AppendLine("Login \t\t: " + mLogin);
                    mMsgs.AppendLine("IP Address \t: " + mIP);
                    mMsgs.AppendLine("Machine \t\t: " + mMachine);
                    mMsgs.AppendLine("System Version \t: " + mSysVer + Environment.NewLine);
                    mMsgs.AppendLine("You haven't logoff from above session. Please contact admin.");

                    Sm.StdMsg(mMsgType.Warning, mMsgs.ToString());
                    TxtUserCode.Focus();

                    return true;
                }                
            }
            return false;
        }

        private void BtnSetting_Click(object sender, EventArgs e)
        {
            bool x = PnlSetting.Visible;
            if (x)
            {
                PnlSetting.Visible = false;
            }
            else
            {
                PnlSetting.Visible = true;
            }
        }

        #endregion

        #endregion
    }
}
