﻿#region Update
/*
    15/12/2020 [DITA/IMS] New apps
    29/01/2021 [VIN/IMS] tambah informasi invoice dan dialog baru
    01/02/2021 [WED/IMS] bug di query join ke socontractdtl
    08/07/2021 [DITA/IMS] tambah informasi local docno dan baris subtotal
 *  14/07/2021 [ICA/IMS] tambah kolom quantity dan total item's amount serta perubahan total amount after tax. 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARDOCTProject : RunSystem.FrmBase6
    {
        #region Field

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingARDOCTProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standar Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocDt, T8.CurName, T10.CtCode, T10.CtName, T11.InvoiceDocNo, T11.InvoiceAmt, T2.PONo, T7.ProjectCode, T7.ProjectName, ");
            SQL.AppendLine("T1.SOContractDocNo, T2.Amt+T2.AmtBOM SOContractAmt, (ifnull(T2.Amt, 0) + ifnull(T2.AmtBOM, 0)) - ifnull(T11.InvoiceAMt, 0) OutstandingAmt, ");
            SQL.AppendLine("T1.DocNo DOCtVerifyDocNo, T9.WhsName, T4.ItCode, T4.ItCodeInternal LocalCode, T4.ItName, T3.Remark, T3.UPrice, T3.TaxAmt, 0.00 As TAxAmt2, T3.UPriceAfTax, T1.LocalDocNo, T1.Qty ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select A.DocNo, A.DocDt, B.DNo, E.WhsCode, D.SOContractDocNo, D.SOContractDNo, A.LocalDocNo, B.Qty ");
            SQL.AppendLine("	From TblDOCtVerifyHdr A ");
            SQL.AppendLine("	Inner Join TblDOCtVerifyDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' ");
            SQL.AppendLine("	Inner Join TblDOCT2Hdr C On A.DOCt2DocNo=C.DocNo ");
            SQL.AppendLine("	Inner Join TblDOCt2Dtl D On C.DocNo = D.DocNo And B.DOCt2DNo = D.DNo ");
            SQL.AppendLine("	Inner Join TblDRHdr E On C.DRDocNo = E.DocNo ");

            SQL.AppendLine("	Union All ");

            SQL.AppendLine("	Select A.DocNo, A.DocDt,  B.DNo, C.WhsCode , C.SOContractDocNo, D.SOContractDNo, A.LocalDocNo, B.Qty ");
            SQL.AppendLine("	From TblDOCtVerifyHdr A ");
            SQL.AppendLine("	Inner Join TblDOCtVerifyDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' ");
            SQL.AppendLine("	Inner Join TblDOCTHdr C On A.DOCtDocNo=C.DocNo ");
            SQL.AppendLine("	Inner Join TblDOCtDtl D On C.DocNo = D.DocNo And B.DOCtDNo = D.DNo ");

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblSOContractHdr T2 On T1.SOContractDocNo = T2.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractDtl T3 On T1.SOContractDocNo = T3.DocNo And T1.SOContractDNo = T3.DNo And T2.CancelInd = 'N' ");
            SQL.AppendLine("INNER JOIN TblItem T4 ON T3.ItCode = T4.ItCode  ");
            SQL.AppendLine("Inner Join TblBOQHdr T5 On T2.BOQDocNo=T5.DocNo   ");
            SQL.AppendLine("Inner Join TblLOphdr T6 On T5.LOPDocNo=T6.DocNO  ");
            SQL.AppendLine("LEFT JOIN TblProjectGroup T7 ON T6.PGCode = T7.PGCode  ");
            SQL.AppendLine("LEFT JOIN TblCurrency T8 ON T5.CurCode = T8.CurCode   ");
            SQL.AppendLine("LEFT JOIN TblWarehouse T9 ON T1.WhsCode = T9.WhsCode  ");
            SQL.AppendLine("Inner Join TblCustomer T10 On T2.CtCode = T10.CtCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select A.DocNo , group_concat(distinct B.DocNo SEPARATOR ',') InvoiceDocNo, Sum(B.Amt) InvoiceAmt ");
            SQL.AppendLine("	From TblSOContractHdr A ");
            SQL.AppendLine("	Inner Join TblSOContractDownpayment2Hdr B On A.DocNo = B.SOContractDocNo And A.CancelInd = 'N' ");
            SQL.AppendLine("	Group By A.DocNo ");
            SQL.AppendLine(")T11 On T2.DocNo = T11.DocNo ");
            SQL.AppendLine("Where T1.DocDt Between @DocDt1 And @DocDt2  ");
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Currency",
                        "Customer",
                        "Customer PO#",
                        "Project Code",

                        //6-10
                        "Project Name",
                        "SO Contract#",
                        "SO Contract"+Environment.NewLine+"Amount",
                        "Outstanding SO Contract"+Environment.NewLine+"Amount",
                        "Invoice#",

                        //11-15
                        "",
                        "Invoice Amount",
                        "DO To Cutomer"+Environment.NewLine+"Verification#",
                        "Warehouse",
                        "Item Code",

                        //16-20
                        "Local Code",
                        "Item Name",
                        "Specification",
                        "Quantity",
                        "Item's Amount",

                        //21-25
                        "Total Item's"+Environment.NewLine+"Amount",
                        "Tax 1",
                        "Tax 2",
                        "Item's Amount"+Environment.NewLine+"After Tax",
                        "Local Document#"

                    },
                    new int[] 
                    {
                        //0
                        25,

                        //1-5
                        120, 100, 210, 100, 100,

                        //6-10
                        150, 200, 120, 150, 200,  

                        //11-15
                        20, 150, 180, 200, 120, 

                        //16-18
                        100, 120, 200, 120, 120, 

                        //21-23
                        150, 120, 120, 120, 130
                        
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 12, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 11 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }, false);
            Grd1.Cols[25].Move(4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtSOContractDocNo.Text, new string[] { "T1.SOContractDocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "T7.ProjectName", "T7.ProjectCode" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "T1.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPONo.Text, new string[] { "T2.PONo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T10.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By T1.DocDt, T1.DocNo;",
                        new string[]
                        { 
                            //0
                            "DocDt", 

                            //1-5
                            "CurName", "CtName", "PONo", "ProjectCode", "ProjectName",
                            
                            //6-10
                            "SOContractDocNo", "SOContractAmt", "OutstandingAmt", "DOCtVerifyDocNo", "WhsName", 

                            //11-15
                            "ItCode", "ItName", "Remark", "UPrice", "TaxAmt",

                            //16-20
                            "TaxAmt2","UPriceAfTax", "LocalCode", "InvoiceDocNo", "InvoiceAmt",

                            //21
                            "LocalDocNo", "Qty"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                            Grd1.Cells[Row, 21].Value = Sm.GetGrdDec(Grd1, Row, 19) * Sm.GetGrdDec(Grd1, Row, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 16);
                            //Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 17);
                            Grd1.Cells[Row, 24].Value = Sm.GetGrdDec(Grd1, Row, 19) * (Sm.GetGrdDec(Grd1, Row, 20) + Sm.GetGrdDec(Grd1, Row, 22) + Sm.GetGrdDec(Grd1, Row, 23));
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);

                        }, true, false, false, false
                    );

                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] {  8, 9, 12, 19, 20, 21, 22, 23, 24 });

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {

                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SLI For Project#");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmRptAgingARDOCTProjectDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 10));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmRptAgingARDOCTProjectDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 10));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;

                f.ShowDialog();
            }
        }
    }
}
