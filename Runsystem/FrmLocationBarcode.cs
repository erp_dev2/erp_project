﻿#region Update
/*
 27/08/2019 [DITA] fitur baru untuk generate barcode bin
 * */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmLocationBarcode : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        //internal Frm FrmFind;
        
        #endregion

        #region Constructor

        public FrmLocationBarcode(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }
      
        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            //Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnFind.Visible = false;
            BtnInsert.Visible = false;
            BtnEdit.Visible = false;
            BtnDelete.Visible = false;
            BtnSave.Visible = false;
            BtnCancel.Visible = false;
            SetFormControl(mState.View);
        }


        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocCode, TxtLocName
                    }, false);
                    TxtLocCode.Focus();
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Button Method
        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion
        #endregion

        #region Additinal Method
        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<LocBarcode>();

            string[] TableName = { "LocBarcode" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select LocCode From TblLocation Where ActInd = 'Y' ");
            if (TxtLocCode.Text.Length > 0)
                SQL.AppendLine("And LocCode Like @LocCode ");
            if (TxtLocName.Text.Length > 0)
                SQL.AppendLine("And LocName Like @LocName ");

            string Filter = " ";
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                if (TxtLocCode.Text.Length > 0) Sm.CmParam<String>(ref cm, "@LocCode", string.Concat("%", TxtLocCode.Text, "%"));
                if (TxtLocName.Text.Length > 0) Sm.CmParam<String>(ref cm, "@LocName", string.Concat("%", TxtLocName.Text, "%"));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "LocCode",


                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LocBarcode()
                        {
                            LocCode = Sm.DrStr(dr, c[0]),


                            //PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            if (l.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty); return;
            }

            Sm.PrintReport("LocBarcode", myLists, TableName, false);
        }

        #endregion

        #region Event

        #region Misc control Event
        private void TxtLocCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtLocName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Location Code");
        }

        private void ChkLocName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Location Name");
        }
        #endregion

        #endregion

        #region Report Class

        class LocBarcode
        {
            public string LocCode { get; set; }
        }
        #endregion

       

    }
}
