﻿#region Update
/*
    02/05/2019 [WED/MAI] new apps
    10/10/2019 [WED/MAI] tambah mth dan yr
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmKBOpeningBalanceFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmKBOpeningBalance mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmKBOpeningBalanceFind(FrmKBOpeningBalance FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Mth, A.Yr, A.CancelInd, B.ItCode, C.ItName, C.ItGrpCode, D.ItGrpName,  ");
            SQL.AppendLine("B.CreateBy, B.CreateDt, IfNull(A.LastUpBy, B.LastUpBy) LastUpBy, IfNull(A.LastUpDt, B.LastUpDt) LastUpDt ");
            SQL.AppendLine("From TblKBOpeningBalanceHdr A ");
            SQL.AppendLine("Inner Join TblKBOpeningBalanceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("And (A.Yr = @Yr) ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblItemGroup D On C.ItGrpCode = D.ItGrpCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Month",
                    "Year",
                    "Cancel", 

                    //6-10
                    "Item Code",
                    "Item Name",
                    "Item Group",
                    "Item Group Name",
                    "Created"+Environment.NewLine+"By",

                    //11-15
                    "Created"+Environment.NewLine+"Date",
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    160, 80, 80, 80, 50, 
                    
                    //6-10
                    100, 280,150, 200, 100, 

                    //11-15
                    100, 100, 100, 100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 8, 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 8, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year")
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                if (Sm.GetLue(LueMth).Length > 0) Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMth), "A.Mth", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtItGrpCode.Text, new string[] { "C.ItGrpCode", "D.ItGrpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",                            
                            //1-5
                            "DocDt", "Mth", "Yr", "CancelInd", "ItCode", 
                            //6-10
                            "ItName", "ItGrpCode", "ItGrpName", "CreateBy", "CreateDt", 
                            //11-12
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            if (Sm.DrStr(dr, 4) == "Y")
                            {
                                Grd.Rows[Row].BackColor = Color.LightCoral;
                                Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            }
                            else
                            {
                                Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion 

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtItGrpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItGrpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item Group");
        }

        #endregion

        #endregion
    }
}
