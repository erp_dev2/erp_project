﻿namespace RunSystem
{
    partial class FrmDOCt7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDOCt7));
            this.panel5 = new System.Windows.Forms.Panel();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.BtnEmpCode4 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtEmpCode4 = new DevExpress.XtraEditors.TextEdit();
            this.BtnEmpCode3 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtEmpCode3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnEmpCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtQueueNo = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtExpDriver = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmpCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnEmpCode1 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtExpPlatNo = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtEmpCode1 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnQueueNo = new DevExpress.XtraEditors.SimpleButton();
            this.TcRecvVd = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtInspectionSheet = new DevExpress.XtraEditors.TextEdit();
            this.LblInspectionSheet = new System.Windows.Forms.Label();
            this.TxtResiNo = new DevExpress.XtraEditors.TextEdit();
            this.lb = new System.Windows.Forms.Label();
            this.BtnExpVdCode = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtExpVdCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnSAName = new DevExpress.XtraEditors.SimpleButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtCtCode = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.LblRemark = new System.Windows.Forms.Label();
            this.TxtSAName = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtCnt = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtSeal = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtDRDocNo = new DevExpress.XtraEditors.TextEdit();
            this.BtnPLDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnPLDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDRDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label18 = new System.Windows.Forms.Label();
            this.BtnDRDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPLDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpPlatNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvVd)).BeginInit();
            this.TcRecvVd.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInspectionSheet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResiNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(912, 0);
            this.panel1.Size = new System.Drawing.Size(67, 596);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Size = new System.Drawing.Size(67, 31);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Size = new System.Drawing.Size(67, 31);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Size = new System.Drawing.Size(67, 31);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Size = new System.Drawing.Size(67, 31);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Size = new System.Drawing.Size(67, 31);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Size = new System.Drawing.Size(67, 31);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Size = new System.Drawing.Size(67, 31);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcRecvVd);
            this.panel2.Size = new System.Drawing.Size(912, 287);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Location = new System.Drawing.Point(0, 287);
            this.panel3.Size = new System.Drawing.Size(912, 309);
            this.panel3.Controls.SetChildIndex(this.panel5, 0);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 574);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(67, 22);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.Location = new System.Drawing.Point(0, 186);
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(912, 123);
            this.Grd1.TabIndex = 67;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Size = new System.Drawing.Size(67, 31);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.xtraTabControl1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(912, 186);
            this.panel5.TabIndex = 31;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(912, 186);
            this.xtraTabControl1.TabIndex = 65;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Appearance.Header.Options.UseTextOptions = true;
            this.xtraTabPage1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabPage1.Controls.Add(this.Grd2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(906, 158);
            this.xtraTabPage1.Text = "Sales Contract";
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(906, 158);
            this.Grd2.TabIndex = 66;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.Header.Options.UseTextOptions = true;
            this.xtraTabPage2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabPage2.Controls.Add(this.Grd3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(906, 158);
            this.xtraTabPage2.Text = "Stock Summary";
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(906, 158);
            this.Grd3.TabIndex = 68;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Appearance.Header.Options.UseTextOptions = true;
            this.xtraTabPage3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabPage3.Controls.Add(this.BtnEmpCode4);
            this.xtraTabPage3.Controls.Add(this.TxtEmpCode4);
            this.xtraTabPage3.Controls.Add(this.BtnEmpCode3);
            this.xtraTabPage3.Controls.Add(this.TxtEmpCode3);
            this.xtraTabPage3.Controls.Add(this.BtnEmpCode2);
            this.xtraTabPage3.Controls.Add(this.TxtQueueNo);
            this.xtraTabPage3.Controls.Add(this.label16);
            this.xtraTabPage3.Controls.Add(this.TxtExpDriver);
            this.xtraTabPage3.Controls.Add(this.TxtEmpCode2);
            this.xtraTabPage3.Controls.Add(this.label7);
            this.xtraTabPage3.Controls.Add(this.BtnEmpCode1);
            this.xtraTabPage3.Controls.Add(this.TxtExpPlatNo);
            this.xtraTabPage3.Controls.Add(this.label15);
            this.xtraTabPage3.Controls.Add(this.label9);
            this.xtraTabPage3.Controls.Add(this.TxtEmpCode1);
            this.xtraTabPage3.Controls.Add(this.label14);
            this.xtraTabPage3.Controls.Add(this.BtnQueueNo);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(906, 158);
            this.xtraTabPage3.Text = "Additional Information";
            // 
            // BtnEmpCode4
            // 
            this.BtnEmpCode4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode4.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode4.Appearance.Options.UseFont = true;
            this.BtnEmpCode4.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode4.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode4.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode4.Image")));
            this.BtnEmpCode4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode4.Location = new System.Drawing.Point(378, 133);
            this.BtnEmpCode4.Name = "BtnEmpCode4";
            this.BtnEmpCode4.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode4.TabIndex = 84;
            this.BtnEmpCode4.ToolTip = "Find Employee";
            this.BtnEmpCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode4.ToolTipTitle = "Run System";
            this.BtnEmpCode4.Click += new System.EventHandler(this.BtnEmpCode4_Click);
            // 
            // TxtEmpCode4
            // 
            this.TxtEmpCode4.EnterMoveNextControl = true;
            this.TxtEmpCode4.Location = new System.Drawing.Point(87, 134);
            this.TxtEmpCode4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode4.Name = "TxtEmpCode4";
            this.TxtEmpCode4.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode4.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode4.Properties.MaxLength = 80;
            this.TxtEmpCode4.Properties.ReadOnly = true;
            this.TxtEmpCode4.Size = new System.Drawing.Size(290, 20);
            this.TxtEmpCode4.TabIndex = 6983;
            // 
            // BtnEmpCode3
            // 
            this.BtnEmpCode3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode3.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode3.Appearance.Options.UseFont = true;
            this.BtnEmpCode3.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode3.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode3.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode3.Image")));
            this.BtnEmpCode3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode3.Location = new System.Drawing.Point(378, 113);
            this.BtnEmpCode3.Name = "BtnEmpCode3";
            this.BtnEmpCode3.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode3.TabIndex = 82;
            this.BtnEmpCode3.ToolTip = "Find Employee";
            this.BtnEmpCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode3.ToolTipTitle = "Run System";
            this.BtnEmpCode3.Click += new System.EventHandler(this.BtnEmpCode3_Click);
            // 
            // TxtEmpCode3
            // 
            this.TxtEmpCode3.EditValue = "81";
            this.TxtEmpCode3.EnterMoveNextControl = true;
            this.TxtEmpCode3.Location = new System.Drawing.Point(87, 113);
            this.TxtEmpCode3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode3.Name = "TxtEmpCode3";
            this.TxtEmpCode3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode3.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode3.Properties.MaxLength = 80;
            this.TxtEmpCode3.Properties.ReadOnly = true;
            this.TxtEmpCode3.Size = new System.Drawing.Size(290, 20);
            this.TxtEmpCode3.TabIndex = 67;
            // 
            // BtnEmpCode2
            // 
            this.BtnEmpCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode2.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode2.Appearance.Options.UseFont = true;
            this.BtnEmpCode2.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode2.Image")));
            this.BtnEmpCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode2.Location = new System.Drawing.Point(378, 92);
            this.BtnEmpCode2.Name = "BtnEmpCode2";
            this.BtnEmpCode2.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode2.TabIndex = 79;
            this.BtnEmpCode2.ToolTip = "Find Employee";
            this.BtnEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode2.ToolTipTitle = "Run System";
            this.BtnEmpCode2.Click += new System.EventHandler(this.BtnEmpCode2_Click);
            // 
            // TxtQueueNo
            // 
            this.TxtQueueNo.EnterMoveNextControl = true;
            this.TxtQueueNo.Location = new System.Drawing.Point(87, 8);
            this.TxtQueueNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQueueNo.Name = "TxtQueueNo";
            this.TxtQueueNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQueueNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQueueNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQueueNo.Properties.MaxLength = 30;
            this.TxtQueueNo.Properties.ReadOnly = true;
            this.TxtQueueNo.Size = new System.Drawing.Size(290, 20);
            this.TxtQueueNo.TabIndex = 69;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(6, 115);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 14);
            this.label16.TabIndex = 66;
            this.label16.Text = "80";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtExpDriver
            // 
            this.TxtExpDriver.EnterMoveNextControl = true;
            this.TxtExpDriver.Location = new System.Drawing.Point(87, 29);
            this.TxtExpDriver.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExpDriver.Name = "TxtExpDriver";
            this.TxtExpDriver.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtExpDriver.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExpDriver.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExpDriver.Properties.Appearance.Options.UseFont = true;
            this.TxtExpDriver.Properties.MaxLength = 20;
            this.TxtExpDriver.Size = new System.Drawing.Size(290, 20);
            this.TxtExpDriver.TabIndex = 72;
            this.TxtExpDriver.Validated += new System.EventHandler(this.TxtExpDriver_Validated);
            // 
            // TxtEmpCode2
            // 
            this.TxtEmpCode2.EnterMoveNextControl = true;
            this.TxtEmpCode2.Location = new System.Drawing.Point(87, 92);
            this.TxtEmpCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode2.Name = "TxtEmpCode2";
            this.TxtEmpCode2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode2.Properties.MaxLength = 80;
            this.TxtEmpCode2.Properties.ReadOnly = true;
            this.TxtEmpCode2.Size = new System.Drawing.Size(290, 20);
            this.TxtEmpCode2.TabIndex = 78;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(46, 32);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 14);
            this.label7.TabIndex = 71;
            this.label7.Text = "Driver";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode1
            // 
            this.BtnEmpCode1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode1.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode1.Appearance.Options.UseFont = true;
            this.BtnEmpCode1.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode1.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode1.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode1.Image")));
            this.BtnEmpCode1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode1.Location = new System.Drawing.Point(378, 71);
            this.BtnEmpCode1.Name = "BtnEmpCode1";
            this.BtnEmpCode1.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode1.TabIndex = 77;
            this.BtnEmpCode1.ToolTip = "Find Employee";
            this.BtnEmpCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode1.ToolTipTitle = "Run System";
            this.BtnEmpCode1.Click += new System.EventHandler(this.BtnEmpCode1_Click);
            // 
            // TxtExpPlatNo
            // 
            this.TxtExpPlatNo.EnterMoveNextControl = true;
            this.TxtExpPlatNo.Location = new System.Drawing.Point(87, 50);
            this.TxtExpPlatNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExpPlatNo.Name = "TxtExpPlatNo";
            this.TxtExpPlatNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtExpPlatNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExpPlatNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExpPlatNo.Properties.Appearance.Options.UseFont = true;
            this.TxtExpPlatNo.Properties.MaxLength = 40;
            this.TxtExpPlatNo.Size = new System.Drawing.Size(290, 20);
            this.TxtExpPlatNo.TabIndex = 74;
            this.TxtExpPlatNo.Validated += new System.EventHandler(this.TxtExpPlatNo_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(33, 74);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 14);
            this.label15.TabIndex = 75;
            this.label15.Text = "Checker";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(5, 53);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 14);
            this.label9.TabIndex = 73;
            this.label9.Text = "Vehicle Plat#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode1
            // 
            this.TxtEmpCode1.EnterMoveNextControl = true;
            this.TxtEmpCode1.Location = new System.Drawing.Point(87, 71);
            this.TxtEmpCode1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode1.Name = "TxtEmpCode1";
            this.TxtEmpCode1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode1.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode1.Properties.MaxLength = 80;
            this.TxtEmpCode1.Properties.ReadOnly = true;
            this.TxtEmpCode1.Size = new System.Drawing.Size(290, 20);
            this.TxtEmpCode1.TabIndex = 76;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(31, 11);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 14);
            this.label14.TabIndex = 68;
            this.label14.Text = "Queue#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnQueueNo
            // 
            this.BtnQueueNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnQueueNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnQueueNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQueueNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnQueueNo.Appearance.Options.UseBackColor = true;
            this.BtnQueueNo.Appearance.Options.UseFont = true;
            this.BtnQueueNo.Appearance.Options.UseForeColor = true;
            this.BtnQueueNo.Appearance.Options.UseTextOptions = true;
            this.BtnQueueNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnQueueNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnQueueNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnQueueNo.Image")));
            this.BtnQueueNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnQueueNo.Location = new System.Drawing.Point(378, 8);
            this.BtnQueueNo.Name = "BtnQueueNo";
            this.BtnQueueNo.Size = new System.Drawing.Size(24, 21);
            this.BtnQueueNo.TabIndex = 70;
            this.BtnQueueNo.ToolTip = "Show Expedition Information";
            this.BtnQueueNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnQueueNo.ToolTipTitle = "Run System";
            this.BtnQueueNo.Click += new System.EventHandler(this.BtnQueueNo_Click);
            // 
            // TcRecvVd
            // 
            this.TcRecvVd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcRecvVd.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcRecvVd.Location = new System.Drawing.Point(0, 0);
            this.TcRecvVd.Name = "TcRecvVd";
            this.TcRecvVd.SelectedTabPage = this.Tp1;
            this.TcRecvVd.Size = new System.Drawing.Size(912, 287);
            this.TcRecvVd.TabIndex = 14;
            this.TcRecvVd.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel6);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(906, 259);
            this.Tp1.Text = "General";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.LueSiteCode);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.ChkFile3);
            this.panel6.Controls.Add(this.ChkFile2);
            this.panel6.Controls.Add(this.BtnDownload3);
            this.panel6.Controls.Add(this.BtnDownload2);
            this.panel6.Controls.Add(this.BtnFile3);
            this.panel6.Controls.Add(this.BtnFile2);
            this.panel6.Controls.Add(this.ChkFile);
            this.panel6.Controls.Add(this.PbUpload3);
            this.panel6.Controls.Add(this.BtnDownload);
            this.panel6.Controls.Add(this.TxtFile3);
            this.panel6.Controls.Add(this.BtnFile);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.PbUpload2);
            this.panel6.Controls.Add(this.TxtFile2);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.PbUpload);
            this.panel6.Controls.Add(this.TxtFile);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.TxtDocNo);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.DteDocDt);
            this.panel6.Controls.Add(this.TxtDRDocNo);
            this.panel6.Controls.Add(this.BtnPLDocNo2);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.BtnPLDocNo);
            this.panel6.Controls.Add(this.BtnDRDocNo);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.BtnDRDocNo2);
            this.panel6.Controls.Add(this.TxtPLDocNo);
            this.panel6.Controls.Add(this.LueWhsCode);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(906, 259);
            this.panel6.TabIndex = 48;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(89, 70);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 25;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 500;
            this.LueSiteCode.Size = new System.Drawing.Size(286, 20);
            this.LueSiteCode.TabIndex = 49;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(57, 73);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 14);
            this.label20.TabIndex = 48;
            this.label20.Text = "Site";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(310, 213);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(22, 22);
            this.ChkFile3.TabIndex = 44;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(310, 171);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(22, 22);
            this.ChkFile2.TabIndex = 38;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(360, 216);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(18, 14);
            this.BtnDownload3.TabIndex = 46;
            this.BtnDownload3.ToolTip = "DownloadFile";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(360, 174);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(18, 14);
            this.BtnDownload2.TabIndex = 40;
            this.BtnDownload2.ToolTip = "DownloadFile";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(341, 216);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(18, 14);
            this.BtnFile3.TabIndex = 45;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(341, 174);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(18, 14);
            this.BtnFile2.TabIndex = 39;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(310, 132);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(22, 22);
            this.ChkFile.TabIndex = 32;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(89, 236);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(286, 17);
            this.PbUpload3.TabIndex = 47;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(360, 135);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(18, 14);
            this.BtnDownload.TabIndex = 34;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(89, 214);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(219, 20);
            this.TxtFile3.TabIndex = 43;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(341, 135);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(18, 14);
            this.BtnFile.TabIndex = 33;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(50, 216);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 42;
            this.label1.Text = "File 3";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(89, 195);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(286, 17);
            this.PbUpload2.TabIndex = 41;
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(89, 173);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(219, 20);
            this.TxtFile2.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(50, 176);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 36;
            this.label2.Text = "File 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(89, 154);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(286, 17);
            this.PbUpload.TabIndex = 35;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(89, 133);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(220, 20);
            this.TxtFile.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(61, 137);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 14);
            this.label3.TabIndex = 30;
            this.label3.Text = "File";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(89, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(286, 20);
            this.TxtDocNo.TabIndex = 13;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtInspectionSheet);
            this.panel4.Controls.Add(this.LblInspectionSheet);
            this.panel4.Controls.Add(this.TxtResiNo);
            this.panel4.Controls.Add(this.lb);
            this.panel4.Controls.Add(this.BtnExpVdCode);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TxtExpVdCode);
            this.panel4.Controls.Add(this.TxtLocalDocNo);
            this.panel4.Controls.Add(this.BtnSAName);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.TxtCtCode);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.LblRemark);
            this.panel4.Controls.Add(this.TxtSAName);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.TxtCnt);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.TxtSeal);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(451, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(455, 259);
            this.panel4.TabIndex = 30;
            // 
            // TxtInspectionSheet
            // 
            this.TxtInspectionSheet.EnterMoveNextControl = true;
            this.TxtInspectionSheet.Location = new System.Drawing.Point(157, 45);
            this.TxtInspectionSheet.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInspectionSheet.Name = "TxtInspectionSheet";
            this.TxtInspectionSheet.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInspectionSheet.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInspectionSheet.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInspectionSheet.Properties.Appearance.Options.UseFont = true;
            this.TxtInspectionSheet.Properties.MaxLength = 30;
            this.TxtInspectionSheet.Size = new System.Drawing.Size(293, 20);
            this.TxtInspectionSheet.TabIndex = 50;
            // 
            // LblInspectionSheet
            // 
            this.LblInspectionSheet.AutoSize = true;
            this.LblInspectionSheet.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInspectionSheet.ForeColor = System.Drawing.Color.Black;
            this.LblInspectionSheet.Location = new System.Drawing.Point(117, 48);
            this.LblInspectionSheet.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblInspectionSheet.Name = "LblInspectionSheet";
            this.LblInspectionSheet.Size = new System.Drawing.Size(38, 14);
            this.LblInspectionSheet.TabIndex = 49;
            this.LblInspectionSheet.Text = "Driver";
            this.LblInspectionSheet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtResiNo
            // 
            this.TxtResiNo.EnterMoveNextControl = true;
            this.TxtResiNo.Location = new System.Drawing.Point(157, 108);
            this.TxtResiNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtResiNo.Name = "TxtResiNo";
            this.TxtResiNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtResiNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtResiNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtResiNo.Properties.Appearance.Options.UseFont = true;
            this.TxtResiNo.Properties.MaxLength = 25;
            this.TxtResiNo.Size = new System.Drawing.Size(293, 20);
            this.TxtResiNo.TabIndex = 56;
            // 
            // lb
            // 
            this.lb.AutoSize = true;
            this.lb.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb.ForeColor = System.Drawing.Color.Black;
            this.lb.Location = new System.Drawing.Point(10, 112);
            this.lb.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(145, 14);
            this.lb.TabIndex = 55;
            this.lb.Text = "Vehicle Registration Plate";
            this.lb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnExpVdCode
            // 
            this.BtnExpVdCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnExpVdCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExpVdCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExpVdCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExpVdCode.Appearance.Options.UseBackColor = true;
            this.BtnExpVdCode.Appearance.Options.UseFont = true;
            this.BtnExpVdCode.Appearance.Options.UseForeColor = true;
            this.BtnExpVdCode.Appearance.Options.UseTextOptions = true;
            this.BtnExpVdCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExpVdCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnExpVdCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnExpVdCode.Image")));
            this.BtnExpVdCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnExpVdCode.Location = new System.Drawing.Point(431, 152);
            this.BtnExpVdCode.Name = "BtnExpVdCode";
            this.BtnExpVdCode.Size = new System.Drawing.Size(19, 11);
            this.BtnExpVdCode.TabIndex = 62;
            this.BtnExpVdCode.ToolTip = "Show Expedition Information";
            this.BtnExpVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnExpVdCode.ToolTipTitle = "Run System";
            this.BtnExpVdCode.Click += new System.EventHandler(this.BtnExpVdCode_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(56, 153);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 14);
            this.label5.TabIndex = 60;
            this.label5.Text = "Expedition Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtExpVdCode
            // 
            this.TxtExpVdCode.EnterMoveNextControl = true;
            this.TxtExpVdCode.Location = new System.Drawing.Point(157, 150);
            this.TxtExpVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExpVdCode.Name = "TxtExpVdCode";
            this.TxtExpVdCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtExpVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExpVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExpVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtExpVdCode.Properties.MaxLength = 80;
            this.TxtExpVdCode.Properties.ReadOnly = true;
            this.TxtExpVdCode.Size = new System.Drawing.Size(266, 20);
            this.TxtExpVdCode.TabIndex = 61;
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(157, 87);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(293, 20);
            this.TxtLocalDocNo.TabIndex = 54;
            // 
            // BtnSAName
            // 
            this.BtnSAName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSAName.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSAName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSAName.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSAName.Appearance.Options.UseBackColor = true;
            this.BtnSAName.Appearance.Options.UseFont = true;
            this.BtnSAName.Appearance.Options.UseForeColor = true;
            this.BtnSAName.Appearance.Options.UseTextOptions = true;
            this.BtnSAName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSAName.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSAName.Image = ((System.Drawing.Image)(resources.GetObject("BtnSAName.Image")));
            this.BtnSAName.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSAName.Location = new System.Drawing.Point(431, 135);
            this.BtnSAName.Name = "BtnSAName";
            this.BtnSAName.Size = new System.Drawing.Size(19, 11);
            this.BtnSAName.TabIndex = 59;
            this.BtnSAName.ToolTip = "Show Shipping Address Information";
            this.BtnSAName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSAName.ToolTipTitle = "Run System";
            this.BtnSAName.Click += new System.EventHandler(this.BtnSAName_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(112, 90);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 14);
            this.label10.TabIndex = 53;
            this.label10.Text = "Local#";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(67, 132);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 14);
            this.label4.TabIndex = 57;
            this.label4.Text = "Shipping Name";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCtCode
            // 
            this.TxtCtCode.EnterMoveNextControl = true;
            this.TxtCtCode.Location = new System.Drawing.Point(157, 66);
            this.TxtCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCtCode.Name = "TxtCtCode";
            this.TxtCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCtCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCtCode.Properties.MaxLength = 80;
            this.TxtCtCode.Properties.ReadOnly = true;
            this.TxtCtCode.Size = new System.Drawing.Size(293, 20);
            this.TxtCtCode.TabIndex = 52;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(157, 171);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 300;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(293, 20);
            this.MeeRemark.TabIndex = 64;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // LblRemark
            // 
            this.LblRemark.AutoSize = true;
            this.LblRemark.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRemark.Location = new System.Drawing.Point(108, 173);
            this.LblRemark.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblRemark.Name = "LblRemark";
            this.LblRemark.Size = new System.Drawing.Size(47, 14);
            this.LblRemark.TabIndex = 63;
            this.LblRemark.Text = "Remark";
            this.LblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSAName
            // 
            this.TxtSAName.EnterMoveNextControl = true;
            this.TxtSAName.Location = new System.Drawing.Point(157, 129);
            this.TxtSAName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSAName.Name = "TxtSAName";
            this.TxtSAName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSAName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSAName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSAName.Properties.Appearance.Options.UseFont = true;
            this.TxtSAName.Properties.MaxLength = 80;
            this.TxtSAName.Properties.ReadOnly = true;
            this.TxtSAName.Size = new System.Drawing.Size(266, 20);
            this.TxtSAName.TabIndex = 58;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(96, 69);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 14);
            this.label6.TabIndex = 51;
            this.label6.Text = "Customer";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt
            // 
            this.TxtCnt.EditValue = "";
            this.TxtCnt.EnterMoveNextControl = true;
            this.TxtCnt.Location = new System.Drawing.Point(157, 3);
            this.TxtCnt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt.Name = "TxtCnt";
            this.TxtCnt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(53)))));
            this.TxtCnt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt.Properties.Appearance.Options.UseForeColor = true;
            this.TxtCnt.Properties.MaxLength = 80;
            this.TxtCnt.Properties.ReadOnly = true;
            this.TxtCnt.Size = new System.Drawing.Size(293, 20);
            this.TxtCnt.TabIndex = 27;
            this.TxtCnt.Validated += new System.EventHandler(this.TxtCnt_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(114, 6);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 14);
            this.label12.TabIndex = 26;
            this.label12.Text = "Admin";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(104, 27);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 14);
            this.label13.TabIndex = 28;
            this.label13.Text = "Checker";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal
            // 
            this.TxtSeal.EditValue = "";
            this.TxtSeal.EnterMoveNextControl = true;
            this.TxtSeal.Location = new System.Drawing.Point(157, 24);
            this.TxtSeal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal.Name = "TxtSeal";
            this.TxtSeal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal.Properties.MaxLength = 80;
            this.TxtSeal.Properties.ReadOnly = true;
            this.TxtSeal.Size = new System.Drawing.Size(293, 20);
            this.TxtSeal.TabIndex = 29;
            this.TxtSeal.Validated += new System.EventHandler(this.TxtSeal_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(12, 10);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 14);
            this.label8.TabIndex = 12;
            this.label8.Text = "Document#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(52, 31);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 14);
            this.label11.TabIndex = 14;
            this.label11.Text = "Date";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(89, 28);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceFocused.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(129, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // TxtDRDocNo
            // 
            this.TxtDRDocNo.EnterMoveNextControl = true;
            this.TxtDRDocNo.Location = new System.Drawing.Point(89, 91);
            this.TxtDRDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDRDocNo.Name = "TxtDRDocNo";
            this.TxtDRDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDRDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDRDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDRDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDRDocNo.Properties.MaxLength = 30;
            this.TxtDRDocNo.Properties.ReadOnly = true;
            this.TxtDRDocNo.Size = new System.Drawing.Size(250, 20);
            this.TxtDRDocNo.TabIndex = 19;
            // 
            // BtnPLDocNo2
            // 
            this.BtnPLDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPLDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPLDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPLDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPLDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnPLDocNo2.Appearance.Options.UseFont = true;
            this.BtnPLDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnPLDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnPLDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPLDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPLDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPLDocNo2.Image")));
            this.BtnPLDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPLDocNo2.Location = new System.Drawing.Point(360, 111);
            this.BtnPLDocNo2.Name = "BtnPLDocNo2";
            this.BtnPLDocNo2.Size = new System.Drawing.Size(17, 14);
            this.BtnPLDocNo2.TabIndex = 25;
            this.BtnPLDocNo2.ToolTip = "Show Packing List Information";
            this.BtnPLDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPLDocNo2.ToolTipTitle = "Run System";
            this.BtnPLDocNo2.Click += new System.EventHandler(this.BtnPLDocNo2_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(7, 93);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 14);
            this.label17.TabIndex = 18;
            this.label17.Text = "Delivery Req.";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPLDocNo
            // 
            this.BtnPLDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPLDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPLDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPLDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPLDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPLDocNo.Appearance.Options.UseFont = true;
            this.BtnPLDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPLDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPLDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPLDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPLDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPLDocNo.Image")));
            this.BtnPLDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPLDocNo.Location = new System.Drawing.Point(341, 111);
            this.BtnPLDocNo.Name = "BtnPLDocNo";
            this.BtnPLDocNo.Size = new System.Drawing.Size(17, 14);
            this.BtnPLDocNo.TabIndex = 24;
            this.BtnPLDocNo.ToolTip = "Find Packing List";
            this.BtnPLDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPLDocNo.ToolTipTitle = "Run System";
            this.BtnPLDocNo.Click += new System.EventHandler(this.BtnPLDocNo_Click);
            // 
            // BtnDRDocNo
            // 
            this.BtnDRDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDRDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDRDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDRDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDRDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDRDocNo.Appearance.Options.UseFont = true;
            this.BtnDRDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDRDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDRDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDRDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDRDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDRDocNo.Image")));
            this.BtnDRDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDRDocNo.Location = new System.Drawing.Point(341, 90);
            this.BtnDRDocNo.Name = "BtnDRDocNo";
            this.BtnDRDocNo.Size = new System.Drawing.Size(17, 14);
            this.BtnDRDocNo.TabIndex = 20;
            this.BtnDRDocNo.ToolTip = "Find Delivery Request";
            this.BtnDRDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDRDocNo.ToolTipTitle = "Run System";
            this.BtnDRDocNo.Click += new System.EventHandler(this.BtnDRDocNo_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(15, 115);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 14);
            this.label18.TabIndex = 22;
            this.label18.Text = "Packing List";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDRDocNo2
            // 
            this.BtnDRDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDRDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDRDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDRDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDRDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDRDocNo2.Appearance.Options.UseFont = true;
            this.BtnDRDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDRDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDRDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDRDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDRDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDRDocNo2.Image")));
            this.BtnDRDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDRDocNo2.Location = new System.Drawing.Point(360, 90);
            this.BtnDRDocNo2.Name = "BtnDRDocNo2";
            this.BtnDRDocNo2.Size = new System.Drawing.Size(17, 14);
            this.BtnDRDocNo2.TabIndex = 21;
            this.BtnDRDocNo2.ToolTip = "Show Delivery Request Information";
            this.BtnDRDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDRDocNo2.ToolTipTitle = "Run System";
            this.BtnDRDocNo2.Click += new System.EventHandler(this.BtnDRDocNo2_Click);
            // 
            // TxtPLDocNo
            // 
            this.TxtPLDocNo.EnterMoveNextControl = true;
            this.TxtPLDocNo.Location = new System.Drawing.Point(89, 112);
            this.TxtPLDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPLDocNo.Name = "TxtPLDocNo";
            this.TxtPLDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPLDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPLDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPLDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPLDocNo.Properties.MaxLength = 30;
            this.TxtPLDocNo.Properties.ReadOnly = true;
            this.TxtPLDocNo.Size = new System.Drawing.Size(250, 20);
            this.TxtPLDocNo.TabIndex = 23;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(89, 49);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 25;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 500;
            this.LueWhsCode.Size = new System.Drawing.Size(286, 20);
            this.LueWhsCode.TabIndex = 17;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(16, 52);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 14);
            this.label19.TabIndex = 16;
            this.label19.Text = "Warehouse";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmDOCt7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(979, 596);
            this.Name = "FrmDOCt7";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpPlatNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvVd)).EndInit();
            this.TcRecvVd.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInspectionSheet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResiNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExpVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSAName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPLDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode4;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode4;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode3;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode3;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode2;
        internal DevExpress.XtraEditors.TextEdit TxtQueueNo;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtExpDriver;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode2;
        private System.Windows.Forms.Label label7;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode1;
        internal DevExpress.XtraEditors.TextEdit TxtExpPlatNo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode1;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnQueueNo;
        private DevExpress.XtraTab.XtraTabControl TcRecvVd;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        private System.Windows.Forms.ProgressBar PbUpload3;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar PbUpload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtInspectionSheet;
        private System.Windows.Forms.Label LblInspectionSheet;
        internal DevExpress.XtraEditors.TextEdit TxtResiNo;
        private System.Windows.Forms.Label lb;
        public DevExpress.XtraEditors.SimpleButton BtnExpVdCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtExpVdCode;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnSAName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtCtCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label LblRemark;
        internal DevExpress.XtraEditors.TextEdit TxtSAName;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtSeal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtCnt;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtDRDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnPLDocNo2;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton BtnPLDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnDRDocNo;
        private System.Windows.Forms.Label label18;
        public DevExpress.XtraEditors.SimpleButton BtnDRDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtPLDocNo;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        internal DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label20;
    }
}