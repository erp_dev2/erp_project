﻿#region update
/*
 29/04/2021 [RDH/SIER] FEEDBACK : menambahkan lup dialog customer
 18/05/2021 [RDH/SIER] FEEDBACK : penyesuaian customer category berdasarkan grop login
 17/03/2022 [VIN/SIER] BUG kolom masih bisa di edit
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIncomingPaymentDlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmIncomingPayment mFrmParent;

        #endregion

        #region Constructor

        public FrmIncomingPaymentDlg4(FrmIncomingPayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Customer";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();

                if (mFrmParent.mIsCustomerComboBasedOnCategory)
                {
                    label7.Visible = LueCtCtCode.Visible = ChkCtCtCode.Visible = true;
                    SetLueCtCtCode(ref LueCtCtCode);
                }
                else
                {
                    label7.Visible = LueCtCtCode.Visible = false;

                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Active",
                        "Agent",
                        "Short Code",

                        //6-10
                        "ctctCode",
                        "Category",
                        "Group",
                        "Address",
                        "Postal Code",
                        
                        //11-15
                        "City",
                        "Country",
                        "Phone",
                        "Fax",
                        "Mobile",
                        
                        //16-20
                        "Email",
                        "NPWP",
                        "Contact Person",
                        "NIB",
                        "Remark",
                        
                        //21-25
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        // 26
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 60, 60, 100, 
                        
                        //6-10
                        0, 200, 200, 300, 90, 
                        
                        //11-15
                        200, 200, 120, 120, 120,  

                        //16-20
                        150, 150, 300, 130, 100, 

                        //21-25
                        300, 100, 100, 100, 100, 

                        //26
                        100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 22, 25 });
            Sm.GrdFormatTime(Grd1, new int[] { 23, 26 });
            Sm.GrdColInvisible(Grd1, new int[] { 20, 21, 22, 23, 24, 25, 26 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });

            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT T2.CtCode, T2.CtName, T2.ActInd, T2.AgentInd, T2.CtShortCode,T3.CtCtCode, T3.CtCtName, T7.CtGrpName, ");
            SQL.AppendLine("T2.Address, T2.PostalCd, T4.CityName, T5.CntName, ");
            SQL.AppendLine("T2.Phone, T2.Fax, T2.Mobile, T2.Email, T2.NPWP, T6.ContactPerson, T2.NIB, T2.Remark,  ");
            SQL.AppendLine("T2.CreateBy, T2.CreateDt, T2.LastUpBy, T2.LastUpDt ");
            SQL.AppendLine("FROM ( ");
            SQL.AppendLine("        Select Distinct CtCode ");
            SQL.AppendLine("        From TblSalesInvoiceHdr ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And IfNull(ProcessInd, 'O') In ('O', 'P') ");
            SQL.AppendLine("        And Amt<>0.00 ");
            if (mFrmParent.mIsUseMInd) SQL.AppendLine("        And MInd='" + mFrmParent.mMInd + "' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Distinct CtCode ");
            SQL.AppendLine("        From TblSalesReturnInvoiceHdr ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And IfNull(IncomingPaymentInd, 'O') In ('O', 'P') ");
            SQL.AppendLine("        Union ALL ");
            SQL.AppendLine("        Select Distinct CtCode ");
            SQL.AppendLine("        From TblSalesInvoice2Hdr ");
            SQL.AppendLine("        Where CancelInd='N' ");
            SQL.AppendLine("        And IfNull(ProcessInd, 'O') In ('O', 'P') ");
            SQL.AppendLine("        And Amt<>0.00 ");
            SQL.AppendLine("        UNION ALL ");
            SQL.AppendLine("        Select Distinct A.CtCode  ");
            SQL.AppendLine("        From ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct X2.ProjectImplementationDocNo, X1.CtCode ");
            SQL.AppendLine("            From TblSalesInvoice5Hdr X1 ");
            SQL.AppendLine("            Inner Join TblSalesInvoice5Dtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("                Where X1.CancelInd = 'N' ");
            SQL.AppendLine("                And IfNull(X1.ProcessInd, 'O') In ('O', 'P') ");
            SQL.AppendLine("                And X1.Amt<>0.00 ");
            SQL.AppendLine("        ) A ");
            if (mFrmParent.mIsFilterBySite)
            {
                if (mFrmParent. mIsIncomingPaymentProjectSystemEnabled)
                {
                    SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.ProjectImplementationDocNo=B.DocNo ");
                    SQL.AppendLine("Inner Join TblSOContractRevisionHdr C On B.SOContractDocNo = C.DocNo ");
                    SQL.AppendLine("Inner Join TblSOContractHdr D On C.SOCDocNo=D.DocNo ");
                    SQL.AppendLine("Inner Join TblBOQHdr E On D.BOQDocNo=E.DocNo ");
                    SQL.AppendLine("Inner Join TblLOPHdr F On E.LOPDocNo=F.DocNo ");
                    SQL.AppendLine("And (F.SiteCode Is Null Or ( ");
                    SQL.AppendLine("    F.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(F.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(")) ");
                }
            }
            SQL.AppendLine("    ) T1 ");
            SQL.AppendLine("    Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            SQL.AppendLine("    Inner Join TblCustomerCategory T3 On T2.CtCtCode = T3.CtCtCode ");
            SQL.AppendLine("Left Join TblCity T4 On T2.CityCode=T4.CityCode ");
            SQL.AppendLine("Left Join TblCountry T5 On T2.CntCode=T5.CntCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, ");
            SQL.AppendLine("    Group_Concat(Concat(ContactPersonName, Case When Position Is Null Then '' Else Concat(' (', Position, ')') End) Order By ContactPersonName Separator ', ') As ContactPerson ");
            SQL.AppendLine("    From TblCustomerContactPerson ");
            SQL.AppendLine("    Group By CtCode ");
            SQL.AppendLine(") T6 On T2.CtCode=T6.CtCode ");
            SQL.AppendLine("Left Join TblCustomerGroup T7 On T2.CtGrpCode=T7.CtGrpCode ");
            SQL.AppendLine(Filter);
            if (mFrmParent.mIsFilterByCtCt)
            {
                SQL.AppendLine("And (T2.CtCtCode Is Null Or (T2.CtCtCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=T2.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ))) ");
            }
            SQL.AppendLine("Order By T2.CtName; ");
            return SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 1=1 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCtCode.Text, new string[] { "T2.CtName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCtCode), "T3.CtCtCode", true);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[]
                        {
                            //0
                            "CtCode", 
                                
                            //1-5
                            "CtName", "ActInd", "AgentInd", "CtShortCode", "CtCtCode",  
                            
                            //6-10
                            "CtCtName", "CtGrpName", "Address", "PostalCd", "CityName",  
                            
                            //11-15
                            "CntName", "Phone", "Fax", "Mobile", "Email", 
                            
                            //16-20
                            "NPWP",  "ContactPerson", "NIB", "Remark", "CreateBy",  
                            
                            //21-23
                            "CreateDt", "LastUpBy", "LastUpdt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 26, 23);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                if (mFrmParent.mIsIncomingPaymentUseCustomerCategory || mFrmParent.mIsCustomerComboBasedOnCategory)
                {
                    Sm.SetLue(mFrmParent.LueCtCtCode, Sm.GetGrdStr(Grd1, Row, 6));
                }
                Sm.SetLue(mFrmParent.LueCtCode, Sm.GetGrdStr(Grd1, Row, 1));
                this.Close();
            }
        }

        #endregion

        #region Additional method
        private void SetLueCtCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T1.CtCtCode As Col1, T1.CtCtName As Col2 ");
            SQL.AppendLine("FROM tblcustomercategory T1 ");
            if (mFrmParent.mIsFilterByCtCt)
            {
                SQL.AppendLine("WHERE  (T1.CtCtCode Is Null OR (T1.CtCtCode Is Not Null And Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("        Where CtCtCode=T1.CtCtCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine("Order By T1.CtCtName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(ref Lue,ref cm,0,35,false,true,"Code","Name","Col2","Col1");
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer Category");
        }
        #endregion

        #endregion
    }
}
