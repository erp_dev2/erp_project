﻿#region Update
/*
    29/11/2017 [TKG] reporting MR to invoice for cashier.
    25/12/2017 [TKG] tambah informasi approval status
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPIToVoucher : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mMainCurCode = string.Empty;
        private bool mIsVoucherBankAccountFilteredByGrp = false;

        #endregion

        #region Constructor

        public FrmRptPIToVoucher(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsVoucherBankAccountFilteredByGrp = Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp");
        }

        private string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            var Filter2 = string.Empty;

            if (Filter.Length > 0) Filter2 = Filter.Replace("A.", "T1.");

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CurCode, A.Amt, A.TaxAmt, A.Downpayment, ");
            SQL.AppendLine("(A.Amt+A.TaxAmt-A.Downpayment) As PurchaseInvoiceAmt, ");
            SQL.AppendLine("B.Amt As OutgoingPaymentAmt, ");  
            SQL.AppendLine("IfNull(C.Amt, 0.00) As VoucherAmt, ");
            SQL.AppendLine("D.Remark As OutgoingPaymentInfo, ");
            SQL.AppendLine("E.Remark As VoucherInfo ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join ( ");
	        SQL.AppendLine("    Select T1.DocNo, Sum(T2.Amt) As Amt ");
	        SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo And T2.InvoiceType='1' ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            if (mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (T3.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=T3.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) ");
            }
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter2);
	        SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T1.DocNo, Sum(T2.Amt) As Amt ");
	        SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo And T2.InvoiceType='1' ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            if (mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (T3.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=T3.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) ");
            }
	        SQL.AppendLine("    Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo And T4.CancelInd='N' ");
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter2);
	        SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T1.DocNo,  ");
	        SQL.AppendLine("    Group_Concat(Distinct Concat(T3.DocNo, ' (', T4.BankAcNm, ')') Separator ', ') As Remark ");
	        SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo And T2.InvoiceType='1' ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            if (mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (T3.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=T3.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) ");
            }
	        SQL.AppendLine("    Inner Join TblBankAccount T4 On T3.BankAcCode=T4.BankAcCode ");
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter2);
	        SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo  ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select T1.DocNo,  ");
	        SQL.AppendLine("    Group_Concat(Distinct Concat(T4.DocNo, ' (', T5.BankAcNm, ')') Separator ', ') As Remark ");
	        SQL.AppendLine("    From TblPurchaseInvoiceHdr T1 ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo And T2.InvoiceType='1' ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            if (mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (T3.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=T3.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) ");
            }
	        SQL.AppendLine("    Inner Join TblVoucherHdr T4 On T3.VoucherRequestDocNo=T4.VoucherRequestDocNo And T4.CancelInd='N' ");
	        SQL.AppendLine("    Inner Join TblBankAccount T5 On T4.BankAcCode=T5.BankAcCode ");
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter2);
	        SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") E On A.DocNo=E.DocNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("And A.DocNo In ( ");
	        SQL.AppendLine("    Select T3.DocNo ");
	        SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
	        SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo ");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceType='1' And T2.InvoiceDocNo=T3.DocNo ");
            SQL.AppendLine("        And T3.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(Filter.Replace("A.", "T3."));
	        SQL.AppendLine("    Where T1.CancelInd='N' ");
            if (mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (T1.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=T1.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ))) ");
            }
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
	        SQL.AppendLine(");");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Invoice#",
                        "Date",
                        "Currency",
                        "Amount"+Environment.NewLine+"Before Tax",
                        "Tax",
                        
                        //6-10
                        "Amount"+Environment.NewLine+"After Tax",
                        "Downpayment",
                        "PI"+Environment.NewLine+"Amount",
                        "OP"+Environment.NewLine+"Amount",
                        "Voucher"+Environment.NewLine+"Amount",
                        
                        //11-13
                        "",
                        "Outgoing"+Environment.NewLine+"Payment",
                        "Voucher"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        
                        //1-5
                        150, 80, 60, 130, 130,

                        //6-10
                        130, 130, 130, 130, 130,

                        //11-13
                        20, 250, 250
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 11 });
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5 , 6, 7, 8, 9, 10, 12, 13 });
        }

        
        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtPurchaseInvoiceDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CurCode", "Amt", "TaxAmt", "Downpayment", 
            
                            //6-10
                            "PurchaseInvoiceAmt", "OutgoingPaymentAmt", "VoucherAmt", "OutgoingPaymentInfo", "VoucherInfo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Grd.Cells[Row, 6].Value = Sm.GetGrdDec(Grd, Row, 4) + Sm.GetGrdDec(Grd, Row, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                ShowApproval(Sm.GetGrdStr(Grd1, e.RowIndex, 1));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
                ShowApproval(Sm.GetGrdStr(Grd1, e.RowIndex, 1));
        }
        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ShowApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var Msg = string.Empty;
            var IsFirst = true;

            SQL.AppendLine("Select T3.DocNo, ");
            SQL.AppendLine("Case T3.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As OutgoingPaymentStatusDesc, ");
            SQL.AppendLine("T4.UserCode, ");
            SQL.AppendLine("Case T4.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As DocApprovalStatusDesc, ");
            SQL.AppendLine("Case When T4.LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(T4.LastUpDt, 7, 2), '/', Substring(T4.LastUpDt, 5, 2), '/', Left(T4.LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, ");
            SQL.AppendLine("T4.Remark  ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr T1 ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.InvoiceDocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("Inner Join TblOutgoingPaymentHdr T3 On T2.DocNo=T3.DocNo And T3.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblDocApproval T4 On T3.DocNo=T4.DocNo And T4.DocType='OutgoingPayment' And T4.Status In ('A', 'C') ");
            SQL.AppendLine("Where T1.DocNo=@DocNo ");
            SQL.AppendLine("Order By T3.DocNo, T4.ApprovalDNo;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandText = SQL.ToString(),
                        CommandTimeout = 600
                    };
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                    using (var dr = cm.ExecuteReader())
                    {
                        var c = Sm.GetOrdinal(dr, new string[] 
                        { 
                            "DocNo", 
                            "OutgoingPaymentStatusDesc", "UserCode", "DocApprovalStatusDesc", "LastUpDt", "Remark" 
                        });
                        while (dr.Read())
                        {
                            if (IsFirst)
                            Msg =
                                "Outgoing Payment# : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                                "Outgoing Payment's Status : " + Sm.DrStr(dr, 1) + Environment.NewLine + Environment.NewLine;

                            Msg +=
                                "User : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                                "Status : " + Sm.DrStr(dr, 3) + " (" + Sm.DrStr(dr, 4) +  ")" + Environment.NewLine +
                                ((Sm.DrStr(dr, 5).Length == 0) ? 
                                string.Empty: 
                                "Remark : " + Sm.DrStr(dr, 5) + Environment.NewLine) + 
                                Environment.NewLine;

                            if (IsFirst) IsFirst = false;
                        }
                        cm.Dispose();
                    }
                }
                if (Msg.Length == 0) Msg = "No data approved/cancelled.";
                Sm.StdMsg(mMsgType.Info, Msg);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtPurchaseInvoiceDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPurchaseInvoiceDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Purchase invoice#");
        }

        #endregion

        #endregion        
    }
}
