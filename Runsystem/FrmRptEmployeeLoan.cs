﻿#region Update
/*
    28/02/2018 [TKG] tambah employee code, employee name, department, total    
    06/03/2018 [TKG] tambah otorisasi site dan department    
    13/03/2018 [TKG] tambah cek cancelind dan status
    05/04/2019 [TKG] resignee tidak dimunculkan
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmployeeLoan : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mIsFilterBySiteHR = false, mIsFilterByDeptHR = false;

        #endregion

        #region Constructor

        public FrmRptEmployeeLoan(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetSQL();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, Sm.Left(CurrentDateTime, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueCreditCode (ref LueCreditCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR?"Y":"N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select * from (");
            SQL.AppendLine("Select DocNo, CreditCode, CreditName, EmpCode, EmpName, DeptName, MthCode, Mth, Yr,");
            SQL.AppendLine("Sum(KantorPusat) As KantorPusat, Sum(InnaTretesHotel) As InnaTretesHotel, Sum(InnaMalioboro) As InnaMalioboro, "); 
            SQL.AppendLine("Sum(InnaSamuderaBeach) As InnaSamuderaBeach, Sum(InayaPutriBali) As InayaPutriBali, "); 
            SQL.AppendLine("Sum(InnaBaliBeach) As InnaBaliBeach, Sum(InnaKuta) As InnaKuta, Sum(InnaBaliHeritage) As InnaBaliHeritage, ");
            SQL.AppendLine("Sum(InnaSindhuBali) As InnaSindhuBali, Sum(InnaMedan) As InnaMedan, Sum(InnaParapat) As InnaParapat, "); 
            SQL.AppendLine("Sum(InnaPadang) As InnaPadang, Sum(InnaTunjungan) As InnaTunjungan, ");
            SQL.AppendLine("Sum(InnaBaliBeachGarden) As InnaBaliBeachGarden, Sum(InnaBaliBeachResort) As InnaBaliBeachResort ");
            SQL.AppendLine("From( ");
            SQL.AppendLine("    Select DocNo, CreditCode, CreditName, EmpCode, EmpName, DeptName, MthCode, MonthName(Str_To_Date(Mth, '%m'))As Mth, Yr, ");
            SQL.AppendLine("    Case SiteCode When '001' Then TotalAmt Else 0.0000 End As KantorPusat, "); 
            SQL.AppendLine("    Case SiteCode When '002' Then TotalAmt Else 0.0000 End As InnaTretesHotel, "); 
            SQL.AppendLine("    Case SiteCode When '003' Then TotalAmt Else 0.0000 End As InnaMalioboro, "); 
            SQL.AppendLine("    Case SiteCode When '004' Then TotalAmt Else 0.0000 End As InnaSamuderaBeach, ");
            SQL.AppendLine("    Case SiteCode When '005' Then TotalAmt Else 0.0000 End As InayaPutriBali, ");
            SQL.AppendLine("    Case SiteCode When '006' Then TotalAmt Else 0.0000 End As InnaBaliBeach, ");
            SQL.AppendLine("    Case SiteCode When '007' Then TotalAmt Else 0.0000 End As InnaKuta, "); 
            SQL.AppendLine("    Case SiteCode When '008' Then TotalAmt Else 0.0000 End As InnaBaliHeritage, "); 
            SQL.AppendLine("    Case SiteCode When '009' Then TotalAmt Else 0.0000 End As InnaSindhuBali, "); 
            SQL.AppendLine("    Case SiteCode When '010' Then TotalAmt Else 0.0000 End As InnaMedan, "); 
            SQL.AppendLine("    Case SiteCode When '011' Then TotalAmt Else 0.0000 End As InnaParapat, "); 
            SQL.AppendLine("    Case SiteCode When '012' Then TotalAmt Else 0.0000 End As InnaPadang, ");
            SQL.AppendLine("    Case SiteCode When '013' Then TotalAmt Else 0.0000 End As InnaTunjungan, "); 
            SQL.AppendLine("    Case SiteCode When '014' Then TotalAmt Else 0.0000 End As InnaBaliBeachGarden, ");
            SQL.AppendLine("    Case SiteCode When '015' Then TotalAmt Else 0.0000 End As InnaBaliBeachResort ");	
            SQL.AppendLine("    From( ");
            SQL.AppendLine("        Select A.DocNo, A.CreditCode, E.CreditName, A.EmpCode, C.EmpName, F.DeptName, C.SiteCode, B.Yr, B.mth As MthCode, B.Mth, sum((B.amt*A.InterestRate*0.01)+B.Amt) As TotalAmt ");
		    SQL.AppendLine("        From TblAdvancePaymentHdr A "); 
		    SQL.AppendLine("        Inner Join TblAdvancepaymentDtl B On A.DocNo=B.DocNo ");
		    SQL.AppendLine("        Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("            And (C.ResignDt Is Null Or (C.ResignDt Is Not Null And C.ResignDt>@CurrentDate)) ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And C.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(C.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("    And C.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select DeptCode From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=IfNull(C.DeptCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
		    SQL.AppendLine("        Left Join TblSite D On C.SiteCode=D.SiteCode ");
		    SQL.AppendLine("        Left Join TblCredit E On A.CreditCode=E.CreditCode ");
            SQL.AppendLine("        Left Join TblDepartment F On C.DeptCode=F.DeptCode ");
            SQL.AppendLine("        Where A.CancelInd='N' And A.Status In ('A', 'O') ");
            SQL.AppendLine("        Group by A.DOcNo, A.CreditCode, E.CreditName, B.Yr, B.Mth ");
	        SQL.AppendLine("    )Tbl ");
            SQL.AppendLine(")X ");
            SQL.AppendLine("Group By DocNo, CreditCode, CreditName, EmpCode, EmpName, DeptName, MthCode, Mth, Yr ");
            SQL.AppendLine(")XX ");

            mSQL = SQL.ToString();
        }

        private void SetGrd(string SiteCode)
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                     //0
                        "No.",

                        //1-5
                        "Document#",
                        "Credit", 
                        "Employee's Code",
                        "Employee's Name",
                        "Department",

                        //6-10
                        "Month",
                        "Year",
                        "Kantor Pusat",
                        "Inna Tretes Hotel",
                        "Grand Inna Malioboro",
                        
                        //11-15
                        "Grand Inna Samudera Beach",
                        "Inaya Putri Bali",
                        "Grand Inna Bali Beach",
                        "Grand Inna Kuta",
                        "Inna Bali Heritage",

                        //16-20
                        "Inna Sindhu Bali Hotel",
                        "Grand Inna Medan",
                        "Inna Parapat Hotel and Resort",
                        "Grand Inna Padang",
                        "Grand Inna Tunjungan",
                        
                        //21-22
                        "Inna Bali Beach Garden",
                        "Inna Bali Beach Resort",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 150, 100, 200, 180, 
                        
                        //6-10
                        100, 80, 120, 120, 150, 
                        
                        //11-15
                        180, 150, 170, 150, 150, 
                        
                        //16-20
                        170, 150, 200, 150, 150, 
                        
                        //21-22
                        170, 170 
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, true);

            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7 }, !ChkHideInfoInGrd.Checked);
            #region kolom yg dihide
            if (SiteCode == "001")
                Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "002")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "003")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "004")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "005")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "006")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "007")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "008")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "009")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "010")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "011")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "012")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "013")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "014")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22 }, !ChkHideInfoInGrd.Checked);
            if (SiteCode == "015")
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowData()
        {
            SetGrd(Sm.GetLue(LueSiteCode));
            try
            {
                if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "where 0=0 ";
                string Filter2 = string.Empty;

                if (Sm.GetLue(LueSiteCode) == "001") { Filter2 = "and KantorPusat>0"; }
                if (Sm.GetLue(LueSiteCode) == "002") { Filter2 = "and InnaTretesHotel>0"; }
                if (Sm.GetLue(LueSiteCode) == "003") { Filter2 = "and InnaMalioboro>0"; }
                if (Sm.GetLue(LueSiteCode) == "004") { Filter2 = "and InnaSamuderaBeach>0"; }
                if (Sm.GetLue(LueSiteCode) == "005") { Filter2 = "and InayaPutriBali>0"; }
                if (Sm.GetLue(LueSiteCode) == "006") { Filter2 = "and InnaBaliBeach>0"; }
                if (Sm.GetLue(LueSiteCode) == "007") { Filter2 = "and InnaKuta>0"; }
                if (Sm.GetLue(LueSiteCode) == "008") { Filter2 = "and InnaBaliHeritage>0"; }
                if (Sm.GetLue(LueSiteCode) == "009") { Filter2 = "and InnaSindhuBali>0"; }
                if (Sm.GetLue(LueSiteCode) == "010") { Filter2 = "and InnaMedan>0"; }
                if (Sm.GetLue(LueSiteCode) == "011") { Filter2 = "and InnaParapat>0"; }
                if (Sm.GetLue(LueSiteCode) == "012") { Filter2 = "and InnaPadang>0"; }
                if (Sm.GetLue(LueSiteCode) == "013") { Filter2 = "and InnaTunjungan>0"; }
                if (Sm.GetLue(LueSiteCode) == "014") { Filter2 = "and InnaBaliBeachGarden>0"; }
                if (Sm.GetLue(LueSiteCode) == "015") { Filter2 = "and InnaBaliBeachResort>0"; }

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMth), "XX.MthCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "XX.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCreditCode), "XX.CreditCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + Filter2,
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "CreditName", "EmpCode", "EmpName", "DeptName", "Mth", 
                            
                            //6-10
                            "Yr", "KantorPusat", "InnaTretesHotel", "InnaMalioboro", "InnaSamuderaBeach", 
                            
                            //11-15
                            "InayaPutriBali", "InnaBaliBeach", "InnaKuta", "InnaBaliHeritage", "InnaSindhuBali", 
                            
                            //16-20
                            "InnaMedan", "InnaParapat", "InnaPadang", "InnaTunjungan", "InnaBaliBeachGarden", 
                            
                            //21
                            "InnaBaliBeachResort"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                          
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void LueCreditCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCreditCode, new Sm.RefreshLue1(Sl.SetLueCreditCode));
        }
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }
        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        #endregion

    }
}
