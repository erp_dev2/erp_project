﻿#region Update
/*
    01/07/2020 [ICA/IMS] New application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCashTypeGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmCashTypeGroupFind FrmFind;

        #endregion

        #region Constructor

        public FrmCashTypeGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCashTypeGroupCode, TxtCashTypeGroupName, MeeTotalLabel
                    }, true);
                    TxtCashTypeGroupCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCashTypeGroupCode, TxtCashTypeGroupName, MeeTotalLabel
                    }, false);
                    TxtCashTypeGroupCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtCashTypeGroupCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCashTypeGroupName, MeeTotalLabel
                    }, false);
                    TxtCashTypeGroupName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCashTypeGroupCode, TxtCashTypeGroupName, MeeTotalLabel
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCashTypeGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCashTypeGroupCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        //override protected void BtnDeleteClick(object sender, EventArgs e)
        //{
        //    if (Sm.IsTxtEmpty(TxtCashTypeGroupCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

        //    Cursor.Current = Cursors.WaitCursor;
        //    try
        //    {
        //        var cm = new MySqlCommand() { CommandText = "Delete From TblCashTypeGroup Where CashTypeGrpCode=@CashTypeGroupCode" };
        //        Sm.CmParam<String>(ref cm, "@CashTypeGroupCode", TxtCashTypeGroupCode.Text);
        //        Sm.ExecCommand(cm);

        //        BtnCancelClick(sender, e);
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //    finally
        //    {
        //        Cursor.Current = Cursors.Default;
        //    }
        //}

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblCashTypeGroup(CashTypeGrpCode, CashTypeGrpName, TotalLabel, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CashTypeGroupCode, @CashTypeGroupName, @TotalLabel, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update CashTypeGrpName=@CashTypeGroupName, TotalLabel=@TotalLabel, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CashTypeGroupCode", TxtCashTypeGroupCode.Text);
                Sm.CmParam<String>(ref cm, "@CashTypeGroupName", TxtCashTypeGroupName.Text);
                Sm.CmParam<String>(ref cm, "@TotalLabel", MeeTotalLabel.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtCashTypeGroupCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CashTypeGroupCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CashTypeGroupCode", CashTypeGroupCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select CashTypeGrpCode, CashTypeGrpName, TotalLabel From TblCashTypeGroup Where CashTypeGrpCode=@CashTypeGroupCode",
                        new string[] 
                        {
                            "CashTypeGrpCode", "CashTypeGrpName", "TotalLabel"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCashTypeGroupCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCashTypeGroupName.EditValue = Sm.DrStr(dr, c[1]);
                            MeeTotalLabel.EditValue = Sm.DrStr(dr, c[2]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCashTypeGroupCode, "Cash type group code", false) ||
                Sm.IsTxtEmpty(TxtCashTypeGroupName, "Cash type group name", false) ||
                IsCashTypeGroupCodeExisted();
        }

        private bool IsCashTypeGroupCodeExisted()
        {
            if (!TxtCashTypeGroupCode.Properties.ReadOnly && Sm.IsDataExist("Select CashTypeGrpName From TblCashTypeGroup Where CashTypeGrpCode='" + TxtCashTypeGroupCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Cash type group code ( " + TxtCashTypeGroupCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Even

        #region Misc Control Even

        private void TxtCashTypeGroupCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCashTypeGroupCode);
        }

        private void TxtCashTypeGroupName_Validated(object sender, CancelEventArgs e)
        {
            Sm.TxtTrim(TxtCashTypeGroupName);
        }

        #endregion

        #endregion

    }
}
