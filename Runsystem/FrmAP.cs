﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmAP : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mAPCode = string.Empty;
        internal FrmAPFind FrmFind;
        
        #endregion

        #region Constructor

        public FrmAP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Access Point";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                //if this application is called from other application
                if (mAPCode.Length != 0)
                {
                    ShowData(mAPCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-4
                        "Code",
                        "Virtual Access Point",
                        "SSID",
                        "Active"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-4
                        0, 300, 130, 80
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        TxtAPCode, TxtAPName, ChkActInd, TxtSSID, LueAntennaCode, 
                        TxtFrequency, LueChannelWidthCode, LueWLanCardCode 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtAPCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        TxtAPCode, TxtAPName, TxtSSID, LueAntennaCode, TxtFrequency, 
                        LueChannelWidthCode, LueWLanCardCode 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtAPCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        TxtAPName, ChkActInd, TxtSSID, LueAntennaCode, TxtFrequency, 
                        LueChannelWidthCode, LueWLanCardCode 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtAPName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtAPCode, TxtAPName, TxtSSID, LueAntennaCode, LueChannelWidthCode, LueWLanCardCode });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtFrequency }, 1);
            ChkActInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAPFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sl.SetLueAntennaCode(ref LueAntennaCode, string.Empty);
                Sl.SetLueChannelWidthCode(ref LueChannelWidthCode, string.Empty);
                Sl.SetLueWLanCardCode(ref LueWLanCardCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAPCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtAPCode, "", false) || IsCodeAlreadyUsed()) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { 
                    CommandText = 
                        "Delete From TblAPHdr Where APCode=@APCode;" +
                        "Delete From TblAPDtl Where APCode=@APCode;"
                };
                Sm.CmParam<String>(ref cm, "@APCode", TxtAPCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveAPHdr());
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveAPDtl(Row));

                Sm.ExecCommands(cml);
                ShowData(TxtAPCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmAPDlg(this));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmAPDlg(this));
        }

        #endregion

        #region Show Data

        public void ShowData(string APCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowAPHdr(APCode);
                ShowAPDtl(APCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAPHdr(string APCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select APCode, APName, ActInd, SSID, ");
            SQL.AppendLine("AntennaCode, Frequency, ChannelWidthCode, WLanCardCode ");
            SQL.AppendLine("From TblAPHdr Where ApCode=@APCode; ");

            Sm.CmParam<String>(ref cm, "@APCode", APCode);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "APCode", 
                        "APName", "ActInd", "SSID", "AntennaCode", "Frequency", 
                        "ChannelWidthCode", "WLanCardCode" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAPCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAPName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                        TxtSSID.EditValue = Sm.DrStr(dr, c[3]);
                        Sl.SetLueAntennaCode(ref LueAntennaCode, Sm.DrStr(dr, c[4]));
                        TxtFrequency.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[5]), 1);
                        Sl.SetLueChannelWidthCode(ref LueChannelWidthCode, Sm.DrStr(dr, c[6]));
                        Sl.SetLueWLanCardCode(ref LueWLanCardCode, Sm.DrStr(dr, c[7]));
                    }, true
                );
        }

        private void ShowAPDtl(string APCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@APCode", APCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.VirtualAPCode, B.VirtualAPName, B.SSID, B.ActInd ");
            SQL.AppendLine("From TblAPDtl A ");
            SQL.AppendLine("Inner Join TblVirtualAP B On A.VirtualAPCode=B.VirtualAPCode ");
            SQL.AppendLine("Where A.APCode=@APCode Order By B.VirtualAPNAme");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { "VirtualAPCode", "VirtualAPName", "SSID", "ActInd" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAPCode, "Access point code", false) ||
                Sm.IsTxtEmpty(TxtAPName, "Access point name", false) ||
                Sm.IsTxtEmpty(TxtSSID, "SSID", false) ||
                Sm.IsLueEmpty(LueAntennaCode, "Antenna") ||
                Sm.IsTxtEmpty(TxtFrequency, "Frequency", true) ||
                Sm.IsLueEmpty(LueChannelWidthCode, "Channel width") ||
                Sm.IsLueEmpty(LueWLanCardCode, "Wireless LAN card") ||
                IsCodeAlreadyExisted();
        }

        private bool IsCodeAlreadyExisted()
        {
            return 
                !TxtAPCode.Properties.ReadOnly && 
                Sm.IsDataExist(
                    "Select APCode From TblAPHdr Where APCode=@Param;",
                    TxtAPCode.Text,
                    "Access point code ( " + TxtAPCode.Text + " ) already existed."
                    );
        }

        private bool IsCodeAlreadyUsed()
        {
            return
                Sm.IsDataExist(
                    "Select APCode From TblDeviceDtl Where APCode=@Param;",
                    TxtAPCode.Text,
                    "Access point already used in device module."
                    );
        }

        private MySqlCommand SaveAPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAPHdr(ApCode, APName, ActInd, SSID, ");
            SQL.AppendLine("AntennaCode, Frequency, ChannelWidthCode, WLanCardCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ApCode, @APName, @ActInd, @SSID, ");
            SQL.AppendLine("@AntennaCode, @Frequency, @ChannelWidthCode, @WLanCardCode, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update APName=@APName, ActInd=@ActInd, SSID=@SSID, ");
            SQL.AppendLine("   AntennaCode=@AntennaCode, Frequency=@Frequency, ");
            SQL.AppendLine("   ChannelWidthCode=@ChannelWidthCode, WLanCardCode=@WLanCardCode, ");
            SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (TxtAPCode.Properties.ReadOnly)
                SQL.AppendLine("Delete From TblAPDtl Where ApCode=@ApCode; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@APCode", TxtAPCode.Text);
            Sm.CmParam<String>(ref cm, "@APName", TxtAPName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@SSID", TxtSSID.Text);
            Sm.CmParam<String>(ref cm, "@AntennaCode", Sm.GetLue(LueAntennaCode));
            Sm.CmParam<Decimal>(ref cm, "@Frequency", decimal.Parse(TxtFrequency.Text));
            Sm.CmParam<String>(ref cm, "@ChannelWidthCode", Sm.GetLue(LueChannelWidthCode));
            Sm.CmParam<String>(ref cm, "@WLanCardCode", Sm.GetLue(LueWLanCardCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAPDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAPDtl(APCode, VirtualAPCode, CreateBy, CreateDt, LastUpBy, LastUpDt)");
            SQL.AppendLine("Select APCode, @VirtualAPCode, CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblAPHdr Where APCode=@APCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ApCode", TxtAPCode.Text);
            Sm.CmParam<String>(ref cm, "@VirtualAPCode", Sm.GetGrdStr(Grd1, Row, 1));
            return cm;
        }

        #endregion

        internal string GetSelectedData()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAPCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAPCode);
        }

        private void TxtAPName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAPName);
        }

        private void TxtSSID_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSSID);
        }

        private void LueAntennaCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAntennaCode, new Sm.RefreshLue2(Sl.SetLueAntennaCode), string.Empty);
        }

        private void TxtFrequency_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFrequency, 1);
        }

        private void LueChannelWidthCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueChannelWidthCode, new Sm.RefreshLue2(Sl.SetLueChannelWidthCode), string.Empty);
        }

        private void LueWLanCardCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWLanCardCode, new Sm.RefreshLue2(Sl.SetLueWLanCardCode), string.Empty);
        }

        #endregion

        private void TxtAPName_EditValueChanged(object sender, EventArgs e)
        {

        }

        #endregion
    }
}
