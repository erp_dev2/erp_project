﻿#region Update
/*
    27/04/2017 [TKG] tambah qty PO yg diputihkan
    19/09/2017 [TKG] tambah outstanding qty antara MR, PO Request, PO dan Receiving
    03/10/2017 [TKG] Filter item category berdasarkan otorisasi group menu. 
    17/11/2017 [ARI] tambah lup MR, POR, PO
    29/11/2017 [ARI] Filter berdasarkan group untuk menampilkan lup MR,POR,PO
    13/12/2017 [ARI] Filter berdasarkan group untuk menampilkan lup Recv
    23/01/2018 [TKG] bug tampilan quantity
    19/03/2018 [TKG] tambah site dan entity
    05/06/2018 [TKG] tambah informasi status approval PO
    09/10/2019 [DITA/MAI] tambah PIC
    23/04/2020 [IBL/IMS] Tambah kolom LOV#
    24/04/2020 [TKG/IMS] bug saat menampilkan LOV#
    27/20/2020 [ICA/IMS] mengubah Material Request menjadi Purchase Request
    18/11/2020 [ICA/IMS] menampilkan kolom QtyCancel
    18/01/2021 [DITA/IMS] tambah kolom specification + local code berdasarkan param : IsBOMShowSpecifications
    23/02/2021 [WED/IMS] tambah kolom Expedition berdasarkan parameter IsRecvExpeditionEnabled
    06/04/2021 [TKG/IMS] tambah kolom Project Name
    28/04/2021 [WED/IMS] penyesuaian data berdasarkan parameter MenuCodeForPRService
    07/05/2021 [WED/IMS] filter masih belum sesuai ketika parameter MenuCodeForPRService ada isinya
    15/06/2021 [ICA/IMS] Penyesuaian kolom received (harusnya menjadi satu) dan hanya satu baris saja per dokumen
    16/06/2021 [ICA/IMS] PO-Recv dan PR-Recv masih belum sesuai
    19/01/2022 [DEV/IMS] Item reject tidak perlu masuk di reporting PR Purchasing and Receiving Monitoring Quantity Status dengan parameter IsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptMaterialRequestDeptStatus : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty, 
            Doctitle = Sm.GetParameter("Doctitle"),
            mMenuCodeForPRService = string.Empty
            ;
        private bool 
            mIsFilterByItCt = false, 
            mIsRptMRDeptStatusShowDocInfoByGrp = false,
            mIsSiteMandatory = false, 
            mIsFilterBySite = false,
            mIsFilterByDept = false,
            mIsRecvExpeditionEnabled = false,
            mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem = false
            ;
        internal bool mIsBOMShowSpecifications = false;

        #endregion

        #region Constructor

        public FrmRptMaterialRequestDeptStatus(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDept ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");

                if (Doctitle == "IMS")
                {
                    label6.Text = "Purchase Request#";
                    label6.Location = new System.Drawing.Point(2, 7);
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsRptMRDeptStatusShowDocInfoByGrp = Sm.GetParameterBoo("IsRptMRDeptStatusShowDocInfoByGrp");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsRecvExpeditionEnabled = Sm.GetParameterBoo("IsRecvExpeditionEnabled");
            mMenuCodeForPRService = Sm.GetParameter("MenuCodeForPRService");
            mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem = Sm.GetParameterBoo("IsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem");
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, GROUP_CONCAT(DISTINCT (T.RecvVdDocNo)) RecvVdDocNoGroup, Sum(IfNull(T.RecvVdQty, 0)) RecvVdQtyAmt, Sum(IfNull(T.DOVdQty, 0)) DOVdQtyAmt From ( ");
            SQL.AppendLine("Select ");
            if (mMenuCodeForPRService.Length > 0)
                SQL.AppendLine("IfNull(B.MaterialRequestServiceDocNo, A.DocNo) As DocNo, ");
            else
                SQL.AppendLine("A.DocNo, ");
            SQL.AppendLine("A.DocDt, A.LocalDocNo, K.DeptName, R.LOVDocNo, A.DeptCode, A.SiteCode, ");
            SQL.AppendLine("B.ItCode, L.ItName, L.ForeignName, L.ItCodeInternal, L.Specification,  ");
            SQL.AppendLine("Case When B.CancelInd='Y' Or B.Status='C' Then 'C' Else B.Status End As MaterialRequestStatus, ");
            SQL.AppendLine("B.Qty, L.PurchaseUomCode, B.UsageDt, D.MaterialRequestApproval, ");
            SQL.AppendLine("E.PORequestDocNo, IfNull(E.PORequestQty, 0) As PORequestQty, E.Status As PORequestStatus, F.PORequestApproval, ");
            if (mMenuCodeForPRService.Length > 0)
                SQL.AppendLine("If(B.MaterialRequestServiceDocNo Is Null, G.PODocNo, A.DocNo) As PODocNo, ");
            else
                SQL.AppendLine("G.PODocNo, ");
            SQL.AppendLine("IfNull(G.POQty, 0) As POQty, IfNull(H.POQtyCancelQty, 0) As POQtyCancelQty, ");
            SQL.AppendLine("P.Status As POStatus, P.POApproval As POApproval, ");
            SQL.AppendLine("I.RecvVdDocNo, IfNull(I.RecvVdQty, 0) As RecvVdQty, IfNull(J.DOVdQty, 0) As DOVdQty, ");
            SQL.AppendLine("A.Remark As HRemark, B.Remark As DRemark, M.SiteName, O.EntName, Q.UserName As PICName, ");
            SQL.AppendLine("C.QtyCancel, ");
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("S.RecvExpDocNo, IfNull(S.RecvExpQty, 0.00) RecvExpQty, T.ProjectName ");
            else
                SQL.AppendLine("Null As RecvExpDocNo, 0.00 As RecvExpQty, Null As ProjectName ");
            SQL.AppendLine("From TblMaterialRequestHdr A ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T3.Qty) As QtyCancel ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblMrQtyCancel T3 On T2.DocNo=T3.MRDocNo And T2.DNo=T3.MRDNo And T3.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    Group_Concat(");
            SQL.AppendLine("        Concat(T4.UserName, ' (', Substring(T3.LastUpDt, 7, 2), '/', Substring(T3.LastUpDt, 5, 2), '/', Left(T3.LastUpDt, 4), ')') ");
            SQL.AppendLine("    Order By T3.ApprovalDNo Separator ', ') ");
            SQL.AppendLine("    As MaterialRequestApproval ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDocApproval T3 On T2.DocNo=T3.DocNo And T2.DNo=T3.DNo And T3.DocType='MaterialRequest' And T3.LastUpDt Is Not Null ");
            SQL.AppendLine("    Inner Join TblUser T4 On T3.UserCode=T4.UserCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
            SQL.AppendLine(") D On B.DocNo=D.DocNo And B.DNo=D.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DocNo, T2.DNo, ");
            SQL.AppendLine("    T3.DocNo As PORequestDocNo, T3.DNo As PORequestDNo, T3.Status, ");
            SQL.AppendLine("    Sum(T3.Qty) As PORequestQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T2.DocNo, T2.DNo, T3.DocNo, T3.DNo, T3.Status ");
            SQL.AppendLine(") E On B.DocNo=E.DocNo And B.DNo=E.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.DocNo, T3.DNo, ");
            SQL.AppendLine("    Group_Concat(");
            SQL.AppendLine("        Concat(T5.UserName, ' (', Substring(T4.LastUpDt, 7, 2), '/', Substring(T4.LastUpDt, 5, 2), '/', Left(T4.LastUpDt, 4), ')') ");
            SQL.AppendLine("    Order By T4.ApprovalDNo Separator ', ') ");
            SQL.AppendLine("    As PORequestApproval ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo And T3.CancelInd='N' And T3.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblDocApproval T4 On T3.DocNo=T4.DocNo And T3.DNo=T4.DNo And T4.DocType='PORequest' And T4.LastUpDt Is Not Null ");
            SQL.AppendLine("    Inner Join TblUser T5 On T4.UserCode=T5.UserCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T3.DocNo, T3.DNo ");
            SQL.AppendLine(") F On E.PORequestDocNo=F.DocNo And E.PORequestDNo=F.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.DocNo, T3.DNo, ");
            SQL.AppendLine("    T4.DocNo As PODocNo, T4.DNo As PODNo, ");
            SQL.AppendLine("    Sum(T4.Qty) As POQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T3.DocNo, T3.DNo, T4.DocNo, T4.DNo ");
            SQL.AppendLine(") G On E.PORequestDocNo=G.DocNo And E.PORequestDNo=G.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.DocNo, T4.DNo, Sum(T5.Qty) As POQtyCancelQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo ");
            SQL.AppendLine("    Inner Join TblPOQtyCancel T5 On T4.DocNo=T5.PODocNo And T4.DNo=T5.PODNo And T5.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T4.DocNo, T4.DNo ");
            SQL.AppendLine(") H On G.PODocNo=H.DocNo And G.PODNo=H.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T4.DocNo, T4.DNo, ");
            SQL.AppendLine("    T5.DocNo As RecvVdDocNo, T5.DNo As RecvVdDNo, ");
            SQL.AppendLine("    Sum(T5.QtyPurchase) As RecvVdQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
            if (mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem)
                SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo=T5.PODocNo And T4.DNo=T5.PODNo And T5.CancelInd='N' And T5.RejectedInd='N' And T5.Status In ('O', 'A') ");
            else
                SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo=T5.PODocNo And T4.DNo=T5.PODNo And T5.CancelInd='N' And T5.Status In ('O', 'A') ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T4.DocNo, T4.DNo, T5.DocNo, T5.DNo ");
            SQL.AppendLine(") I On G.PODocNo=I.DocNo And G.PODNo=I.DNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T5.DocNo, T5.DNo, ");
            SQL.AppendLine("    Sum(Case When T5.Qty=0 Then 0 Else T6.Qty*(T5.QtyPurchase/T5.Qty) End) As DOVdQty ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
            if (mIsPRPurchasingAndRecvMonitoringApprovalStatusNotUseRejectedItem)
                SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo=T5.PODocNo And T4.DNo=T5.PODNo And T5.RejectedInd='N' ");
            else
                SQL.AppendLine("    Inner Join TblRecvVdDtl T5 On T4.DocNo=T5.PODocNo And T4.DNo=T5.PODNo ");
            SQL.AppendLine("    Inner Join TblDOVdDtl T6 On T5.DocNo=T6.RecvVdDocNo And T5.DNo=T6.RecvVdDNo And T6.CancelInd='N' ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T5.DocNo, T5.DNo ");
            SQL.AppendLine(") J On I.RecvVdDocNo=J.DocNo And I.RecvVdDNo=J.DNo ");
            SQL.AppendLine("Inner Join TblDepartment K On A.DeptCode=K.DeptCode ");
            SQL.AppendLine("Inner Join TblItem L On B.ItCode=L.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=L.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Left Join TblSite M On A.SiteCode=M.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter N On M.ProfitCenterCode=N.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity O On N.EntCode=O.EntCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T5.DocNo, T5.Status, ");
            SQL.AppendLine("    Group_Concat(");
            SQL.AppendLine("        Concat(T7.UserName, ' (', Substring(T6.LastUpDt, 7, 2), '/', Substring(T6.LastUpDt, 5, 2), '/', Left(T6.LastUpDt, 4), ')') ");
            SQL.AppendLine("    Order By T6.ApprovalDNo Separator ', ') ");
            SQL.AppendLine("    As POApproval ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.DocNo=T3.MaterialRequestDocNo And T2.DNo=T3.MaterialRequestDNo ");
            SQL.AppendLine("    Inner Join TblPODtl T4 On T3.DocNo=T4.PORequestDocNo And T3.DNo=T4.PORequestDNo And T4.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPOHdr T5 On T4.DocNo=T5.DocNo And T5.Status In ('O', 'A') ");
            SQL.AppendLine("    Inner Join TblDocApproval T6 On T5.DocNo=T6.DocNo And T6.DocType='PO' And T6.LastUpDt Is Not Null ");
            SQL.AppendLine("    Inner Join TblUser T7 On T6.UserCode=T7.UserCode ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T5.DocNo, T5.Status ");
            SQL.AppendLine(") P On G.PODocNo=P.DocNo ");
            SQL.AppendLine("Left Join TblUser Q On Q.UserCode=A.PICCode ");

            SQL.AppendLine("LEFT JOIN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.MRDocNo, T2.MRDNo, ");
            SQL.AppendLine("    Group_Concat(Distinct T3.DocNo Order By T3.DocNo Separator ', ') As LOVDocNo ");
            SQL.AppendLine("    From tblitemvendorcollectionhdr T1 ");
            SQL.AppendLine("    Inner Join tblitemvendorcollectiondtl T2 ON T1.DocNo = T2.DocNo AND T2.CancelInd = 'N' ");
            SQL.AppendLine("    Inner Join tblvendoritctgroupdtl T3 ");
            SQL.AppendLine("        On T2.DocNo = T3.ItemVendorCollectionDocNo ");
            SQL.AppendLine("        And T2.DNo = T3.ItemVendorCollectionDNo ");
            SQL.AppendLine("    Group By T2.MRDocNo, T2.MRDNo ");
            SQL.AppendLine(" ) R ON B.DocNo = R.MRDocNo AND B.DNo = R.MRDNo ");

            if (mIsRecvExpeditionEnabled)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( "); 
                SQL.AppendLine("    Select T3.MaterialRequestDocNo, T3.MaterialRequestDNo, Sum(T1.QtyPurchase) RecvExpQty, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T1.DocNo, '')) RecvExpDocNo ");
                SQL.AppendLine("    From TblRecvExpeditionDtl T1 ");
                SQL.AppendLine("    Inner Join TblPODtl T2 On T1.PODocNo = T2.DocNo And T1.PODNo = T2.DNo ");
                SQL.AppendLine("        And T1.CancelInd = 'N' And T1.Status = 'A' ");
                SQL.AppendLine("    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo And T2.PORequestDNo = T3.DNo ");
                SQL.AppendLine("    Group By T3.MaterialRequestDocNo, T3.MaterialRequestDNo ");
                SQL.AppendLine(") S On B.DocNo = S.MaterialRequestDocNo And B.DNo = S.MaterialRequestDNo ");
                SQL.AppendLine("Left Join TblProjectGroup T On A.PGCode=T.PGCode ");
            }

            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=A.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine(") T ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 47;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        (Doctitle=="IMS") ? "Purchase Request#" : "Material Request#", 
                        "",
                        "Date",
                        "Department",
                        "Local Document#",

                        //6-10
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",
                        "Foreign Name",
                        ((Doctitle=="IMS") ? "Purchase Request" : "Material Request")+Environment.NewLine+"Quantity",
                        "UoM",
                        
                        //11-15
                        "Usage" + Environment.NewLine + "Date",
                        "Status",
                        "Checked By",
                        "",
                        "PO Request#",
                       
                        //16-20
                        "",
                        "PO Request"+Environment.NewLine+"Quantity",
                        "Status",
                        "Checked By", 
                        "",
                       
                        //21-25
                        "PO#",
                        "",
                        "PO"+Environment.NewLine+"Quantity",
                        "Cancelled PO"+Environment.NewLine+"Quantity",
                        "Status",

                        //26-30
                        "Checked By",
                        "",
                        "Received#",
                        "",
                        "Received"+Environment.NewLine+"Quantity",

                        //31-35
                        "Returned"+Environment.NewLine+"Quantity",
                        ((Doctitle=="IMS") ? "PR" : "MR-")+Environment.NewLine+"PO Request",
                        "PO Request"+Environment.NewLine+"-PO",
                        "PO-"+Environment.NewLine+"Received",
                        ((Doctitle=="IMS") ? "PR" : "MR-")+Environment.NewLine+"Received",
                        
                        //36-40
                        "Remark",
                        "Site",
                        "Entity",
                        "PIC Name",
                        "LOV#",

                        //41-45
                        "Cancelled"+Environment.NewLine+"Quantity",
                        "Item's Local "+Environment.NewLine+"Code",
                        "Specification",
                        "Expedition#",
                        "Expedition"+Environment.NewLine+"Quantity",

                        //46
                        "Project Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 20, 80, 180, 150, 

                        //6-10
                        100, 250, 180, 105, 80,

                        //11-15
                        100, 80, 200, 0, 150,

                        //16-20
                        20, 100, 80, 200, 0, 

                        //21-25
                        150, 20, 100, 100, 80, 
                        
                        //26-30
                        200, 0, 150, 20, 100, 
                        
                        //31-35
                        100, 100, 100, 100, 100, 
                        
                        //36-40
                        300, 200, 200, 150, 150,

                        //41-45
                        100, 100, 200, 300, 130,

                        //46
                        200
                    }
                );

            if (mIsRecvExpeditionEnabled)
            {
                Grd1.Cols[45].Move(28);
                Grd1.Cols[44].Move(28);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 44, 45, 46 });

            Grd1.Cols[39].Move(3);
            Grd1.Cols[40].Move(14);
            Grd1.Cols[41].Move(11);
            Sm.GrdColButton(Grd1, new int[] { 2, 14, 16, 20, 22, 27, 29 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 17, 23, 24, 30, 31, 32, 33, 34, 35, 41, 45 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 11 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 17, 18, 19, 21, 23, 24, 25, 26, 28, 30, 31, 32, 33, 34, 35, 36, 37, 38, 40, 41, 42, 43, 44, 45, 46 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 8, 13, 14, 16, 19, 20, 22, 29 }, false);
            if (!Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='PO' Limit 1;"))
                Sm.GrdColInvisible(Grd1, new int[] { 25, 26 }, false);
            if (mIsRptMRDeptStatusShowDocInfoByGrp)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblUser " +
                    "Where UserCode=@Param " +
                    "And GrpCode Is Not Null " +
                    "And Find_In_Set(GrpCode, " +
                    "(Select ParValue From TblParameter Where ParCode='GrpCodeForRptMRDeptStatusShowDocInfo' " +
                    "And ParValue Is Not Null)); ", Gv.CurrentUserCode))
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 2, 16, 22, 29 }, true);
                }
            }
            if (mIsSiteMandatory)
            {
                Grd1.Cols[37].Move(5);
                Grd1.Cols[38].Move(6);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 37, 38 });

            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 42, 43 });
            Grd1.Cols[42].Move(8);
            Grd1.Cols[43].Move(10);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 6, 8, 13, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";
                string HRemark = string.Empty, DRemark = string.Empty;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtMRDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPORDocNo.Text, "T.PORequestDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "T.PODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtRecvVdDocNo.Text, "T.RecvVdDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] {"T.ItCode", "T.ItName", "T.ForeignName"});

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + 
                        " GROUP BY DocNo, DeptCode, SiteCode, ItCode, Qty, PurchaseUomCode, PORequestDocNo, PORequestQty, PORequestStatus, POQty, POQtyCancelQty" +
                        " Order By T.DocDt Desc, T.DocNo;",
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "DeptName", "LocalDocNo", "ItCode", "ItName", 
                            
                            //6-10
                            "ForeignName", "Qty", "PurchaseUomCode", "UsageDt", "MaterialRequestStatus", 
                            
                            //11-15
                            "MaterialRequestApproval", "PORequestDocNo",  "PORequestQty", "PORequestStatus", "PORequestApproval", 
                            
                            //16-20
                            "PODocNo", "POQty", "POQtyCancelQty", "POStatus", "POApproval", 
                            
                            //21-25
                            "RecvVdDocNoGroup", "RecvVdQtyAmt", "DOVdQtyAmt", "HRemark", "DREmark", 
                            
                            //26-30
                            "SiteName", "EntName", "PICName", "LOVDocNo", "QtyCancel",

                            //31-35
                            "ItCodeInternal", "Specification", "RecvExpDocNo", "RecvExpQty", "ProjectName",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                            if (dr[c[11]] != DBNull.Value)
                            {
                                switch (dr.GetString(c[10]))
                                {
                                    case "A": Grd.Cells[Row, 12].Value = "Approved"; break;
                                    case "C": Grd.Cells[Row, 12].Value = "Cancel"; break;
                                    case "O": Grd.Cells[Row, 12].Value = "Outstanding"; break;
                                }
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                            if (dr[c[14]] != DBNull.Value) 
                            {
                                switch (dr.GetString(c[14])) 
                                {
                                    case "A": Grd.Cells[Row, 18].Value = "Approved"; break;
                                    case "C": Grd.Cells[Row, 18].Value = "Cancel"; break;
                                    case "O": Grd.Cells[Row, 18].Value = "Outstanding"; break;
                                }
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 18);

                            if (dr[c[19]] != DBNull.Value)
                            {
                                switch (dr.GetString(c[19]))
                                {
                                    case "A": Grd.Cells[Row, 25].Value = "Approved"; break;
                                    case "C": Grd.Cells[Row, 25].Value = "Cancel"; break;
                                    case "O": Grd.Cells[Row, 25].Value = "Outstanding"; break;
                                }
                            }
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 20);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 22);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 23);

                            Grd.Cells[Row, 32].Value = Sm.GetGrdDec(Grd, Row, 17) - Sm.GetGrdDec(Grd, Row, 9);
                            Grd.Cells[Row, 33].Value = Sm.GetGrdDec(Grd, Row, 23) - Sm.GetGrdDec(Grd, Row, 17);
                            Grd.Cells[Row, 34].Value = Sm.GetGrdDec(Grd, Row, 30) - Sm.GetGrdDec(Grd, Row, 23);
                            Grd.Cells[Row, 35].Value = Sm.GetGrdDec(Grd, Row, 30) - Sm.GetGrdDec(Grd, Row, 9);

                            HRemark = string.Empty;
                            DRemark = string.Empty;
                            if (dr[c[24]] != DBNull.Value) HRemark = dr.GetString(c[24]);
                            if (dr[c[25]] != DBNull.Value) DRemark = dr.GetString(c[25]);
                            Grd.Cells[Row, 36].Value = (string.Concat(HRemark, " ", DRemark)).Trim();
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 27);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 40, 29);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 44, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 46, 35);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 17, 23, 24, 30, 31, 32, 33, 34, 35, 41, 45 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPORequest(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }
            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }
            if (e.ColIndex == 29 && Sm.GetGrdStr(Grd1, e.RowIndex, 28).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 28);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmMaterialRequest(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                var f = new FrmPORequest(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 15);
                f.ShowDialog();
            }
            if (e.ColIndex == 22 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 21);
                f.ShowDialog();
            }
            if (e.ColIndex == 29 && Sm.GetGrdStr(Grd1, e.RowIndex, 28).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 28);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkMRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, (Doctitle=="IMS") ? "Purchase Request#" : "Material request#");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtMRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDept ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtPORDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPORDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO Request#");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Purchase Order#");
        }

        private void TxtRecvVdDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkRecvVdDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Received#");
        }

        #endregion
    }
}
