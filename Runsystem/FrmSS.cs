﻿#region Update
/*
    01/11/2019 [TKG/SRN] tambah informasi salary minimum limit
    12/03/2020 [IBL/SRN] Tambah combo box untuk memilih cost center group
    21/09/2020 [TKG/HIN] tambah 4 angka di belakang koma utk persentase
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSS : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mSSCode = string.Empty;
        internal FrmSSFind FrmFind;
        private bool IsInserted = false, mIsSSCOAInfoMandatory = false;
        internal bool mIsCOAFilteredByGroup = false;

        #endregion

        #region Constructor

        public FrmSS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Social Security";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                Sl.SetLueSSPCode(ref LueSSPCode);
                Sl.SetLueOption(ref LueCostCenterGroup, "CostCenterGroup");
                GetParameter();
                //if this application is called from other application
                if (mSSCode.Length != 0)
                {
                    ShowData(mSSCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSSCode, TxtSSName, LueSSPCode, TxtSalaryMinLimit, TxtSalaryMaxLimit, 
                        TxtEmployerPerc, TxtEmployeePerc, TxtTotalPerc, LueCostCenterGroup
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    BtnAcNo.Enabled = BtnAcNo2.Enabled = false;
                    TxtSSCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSSCode, TxtSSName, LueSSPCode, TxtSalaryMinLimit, TxtSalaryMaxLimit, 
                        TxtEmployerPerc, TxtEmployeePerc, LueCostCenterGroup
                    }, false);
                    BtnAcNo.Enabled = BtnAcNo2.Enabled = true;
                    TxtSSCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSSName, TxtSalaryMinLimit, TxtSalaryMaxLimit, TxtEmployerPerc, TxtEmployeePerc, 
                        LueCostCenterGroup
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    BtnAcNo.Enabled = BtnAcNo2.Enabled = true;
                    TxtSSName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { 
                TxtSSCode, TxtSSName, LueSSPCode, TxtAcNo1, TxtAcDesc1, 
                TxtAcNo2, TxtAcDesc2, LueCostCenterGroup
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtSalaryMinLimit, TxtSalaryMaxLimit }, 0);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEmployerPerc, TxtEmployeePerc, TxtTotalPerc }, 4);
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                IsInserted = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSSCode, "", false)) return;
            SetFormControl(mState.Edit);
            IsInserted = false;
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSSCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblSS Where SSCode=@SSCode;" };
                Sm.CmParam<String>(ref cm, "@SSCode", TxtSSCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblSS(SSCode, SSName, ActInd, SSPCode, SalaryMinLimit, SalaryMaxLimit, EmployerPerc, EmployeePerc, TotalPerc, AcNo1, AcDesc1, AcNo2, AcDesc2, CCGrpCode, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@SSCode, @SSName, @ActInd, @SSPCode, @SalaryMinLimit, @SalaryMaxLimit, @EmployerPerc, @EmployeePerc, @TotalPerc, @AcNo1, @AcDesc1, @AcNo2, @AcDesc2, @CCGrpCode, @UserCode, CurrentDateTime()) ");
                if (!IsInserted)
                {
                    SQL.AppendLine("On Duplicate Key ");
                    SQL.AppendLine("    Update ");
                    SQL.AppendLine("        SSName=@SSName, ActInd=@ActInd, SSPCode=@SSPCode, SalaryMinLimit=@SalaryMinLimit, SalaryMaxLimit=@SalaryMaxLimit, EmployerPerc=@EmployerPerc, EmployeePerc=@EmployeePerc, TotalPerc=@TotalPerc, ");
                    SQL.AppendLine("        AcNo1=@AcNo1, AcDesc1=@AcDesc1, AcNo2=@AcNo2, AcDesc2=@AcDesc2, CCGrpCode=@CCGrpCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                }
                SQL.AppendLine("   ; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@SSCode", TxtSSCode.Text);
                Sm.CmParam<String>(ref cm, "@SSName", TxtSSName.Text);
                Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
                Sm.CmParam<Decimal>(ref cm, "@SalaryMinLimit", decimal.Parse(TxtSalaryMinLimit.Text));
                Sm.CmParam<Decimal>(ref cm, "@SalaryMaxLimit", decimal.Parse(TxtSalaryMaxLimit.Text));
                Sm.CmParam<Decimal>(ref cm, "@EmployerPerc", decimal.Parse(TxtEmployerPerc.Text));
                Sm.CmParam<Decimal>(ref cm, "@EmployeePerc", decimal.Parse(TxtEmployeePerc.Text));
                Sm.CmParam<Decimal>(ref cm, "@TotalPerc", decimal.Parse(TxtTotalPerc.Text));
                Sm.CmParam<String>(ref cm, "@AcNo1", TxtAcNo1.Text);
                Sm.CmParam<String>(ref cm, "@AcDesc1", TxtAcDesc1.Text);
                Sm.CmParam<String>(ref cm, "@AcNo2", TxtAcNo2.Text);
                Sm.CmParam<String>(ref cm, "@AcDesc2", TxtAcDesc2.Text);
                Sm.CmParam<String>(ref cm, "@CCGrpCode", Sm.GetLue(LueCostCenterGroup));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                if (IsInserted)
                    BtnInsertClick(sender, e);
                else
                    ShowData(TxtSSCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string SSCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@SSCode", SSCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select SSCode, SSName, SSPCode, ActInd, " +
                        "SalaryMinLimit, SalaryMaxLimit, EmployerPerc, EmployeePerc, TotalPerc, " +
                        "AcNo1, AcDesc1, AcNo2, AcDesc2, CCGrpCode " +
                        "From TblSS Where SSCode=@SSCode;",
                        new string[] 
                        {
                            //0
                            "SSCode", 

                            //1-5
                            "SSName", "ActInd", "SSPCode", "SalaryMinLimit", "SalaryMaxLimit", 
                            
                            //6-10
                            "EmployerPerc", "EmployeePerc", "TotalPerc", "AcNo1", "AcDesc1", 
                            
                            //11-13
                            "AcNo2", "AcDesc2", "CCGrpCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtSSCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtSSName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            Sm.SetLue(LueSSPCode, Sm.DrStr(dr, c[3]));
                            TxtSalaryMinLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                            TxtSalaryMaxLimit.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                            TxtEmployerPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 4);
                            TxtEmployeePerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 4);
                            TxtTotalPerc.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 4);
                            TxtAcNo1.EditValue = Sm.DrStr(dr, c[9]);
                            TxtAcDesc1.EditValue = Sm.DrStr(dr, c[10]);
                            TxtAcNo2.EditValue = Sm.DrStr(dr, c[11]);
                            TxtAcDesc2.EditValue = Sm.DrStr(dr, c[12]);
                            Sm.SetLue(LueCostCenterGroup, Sm.DrStr(dr, c[13]));
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSSCode, "Social security code", false) ||
                Sm.IsTxtEmpty(TxtSSName, "Social security name", false) ||
                Sm.IsLueEmpty(LueSSPCode, "Social security program") ||
                (mIsSSCOAInfoMandatory && Sm.IsTxtEmpty(TxtAcNo1, "COA account# (Debt)", false)) ||
                (mIsSSCOAInfoMandatory && Sm.IsTxtEmpty(TxtAcDesc1, "COA account description (Debt)", false)) ||
                (mIsSSCOAInfoMandatory && Sm.IsTxtEmpty(TxtAcNo2, "COA account# (Downpayment)", false)) ||
                (mIsSSCOAInfoMandatory && Sm.IsTxtEmpty(TxtAcDesc2, "COA account description (Downpayment)", false)) ||
                IsSSCodeExisted() ||
                IsSSNameExisted();
        }

        private bool IsSSCodeExisted()
        {
            if (!TxtSSCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select SSCode From TblSS Where SSCode=@SSCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@SSCode", TxtSSCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Social Security code ( " + TxtSSCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsSSNameExisted()
        {
            if (!TxtSSCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select SSName From TblSS Where SSName=@SSName Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@SSName", TxtSSName.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Social Security name ( " + TxtSSName.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private void ComputeTotalPerc()
        { 
            decimal EmployerPerc = 0m, EmployeePerc = 0m;

            if (TxtEmployerPerc.Text.Length > 0) EmployerPerc = decimal.Parse(TxtEmployerPerc.Text);
            if (TxtEmployeePerc.Text.Length > 0) EmployeePerc = decimal.Parse(TxtEmployeePerc.Text);

            TxtTotalPerc.Text = Sm.FormatNum(EmployerPerc + EmployeePerc, 4);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsSSCOAInfoMandatory = Sm.GetParameter("IsSSCOAInfoMandatory") == "Y";
            if (mIsSSCOAInfoMandatory)
            {
                LblAcNo1.ForeColor = Color.Red;
                LblAcDesc1.ForeColor = Color.Red;
                LblAcNo2.ForeColor = Color.Red;
                LblAcDesc2.ForeColor = Color.Red;
            }
            mIsCOAFilteredByGroup = Sm.GetParameterBoo("IsCOAFilteredByGroup");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSSCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSSCode);
        }

        private void TxtSSName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSSName);
        }

        private void LueSSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSSPCode, new Sm.RefreshLue1(Sl.SetLueSSPCode));
        }

        private void TxtSalaryMinLimit_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtSalaryMinLimit, 0);
        }

        private void TxtSalaryMaxLimit_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtSalaryMaxLimit, 0);
        }

        private void TxtEmployerPerc_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtEmployerPerc, 4);
                ComputeTotalPerc();
            }
        }

        private void TxtEmployeePerc_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtEmployeePerc, 4);
                ComputeTotalPerc();
            }
        }

        private void TxtAcNo1_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAcNo1);
        }

        private void TxtAcDesc1_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAcDesc1);
        }

        private void TxtAcNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAcNo2);
        }

        private void TxtAcDesc2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAcDesc2);
        }

        private void LueCostCenterGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCostCenterGroup, new Sm.RefreshLue2(Sl.SetLueOption), "CostCenterGroup");
        }

        #endregion        

        #region Button Clicks

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmSSDlg(this, 1));
            }
        }

        private void BtnAcNo2_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmSSDlg(this, 2));
            }
        }

        #endregion

        #endregion
    }
}
