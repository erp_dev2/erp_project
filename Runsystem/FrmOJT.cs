﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmOJT : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmOJTFind FrmFind;

        #endregion

        #region Constructor

        public FrmOJT(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "On Job Training";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, 
                        TxtDeptCode, DteJoinDt, TxtDurationDay
                    }, true);
                Sl.SetLuePosCode(ref LueTrainingPosCode);
                Sl.SetLueDeptCode(ref LueTrainingDeptCode);
                SetLueResult(ref LueResult);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, MeeTrainingDesc, DteStartDt, DteEndDt, 
                        LueTrainingPosCode, LueTrainingDeptCode, LueResult, MeeRemark
                    }, true);
                    BtnEmpCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeTrainingDesc, DteStartDt, DteEndDt, LueTrainingPosCode, 
                        LueTrainingDeptCode, LueResult, MeeRemark
                    }, false);
                    BtnEmpCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd, LueResult }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtEmpCode, TxtEmpName, TxtEmpCodeOld, TxtPosCode, 
                TxtDeptCode, DteJoinDt, DteDocDt, MeeTrainingDesc, DteStartDt, 
                DteEndDt, LueTrainingPosCode, LueTrainingDeptCode, LueResult, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay }, 0);
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOJTFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OJT", "TblOJT");

            var cml = new List<MySqlCommand>();
            
            cml.Add(SaveOJT(DocNo));
            cml.Add(SaveEmployee(DocNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee code", false) ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsDteEmpty(DteEndDt, "End date") ||
                IsTrainingDateNotValid();
        }

        private bool IsTrainingDateNotValid()
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            if (StartDt.Length != 0 && EndDt.Length != 0)
            {
                if (decimal.Parse(StartDt) > decimal.Parse(EndDt))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid leave date.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveOJT(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblOJT ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, EmpCode, PosCode, DeptCode, TrainingDesc, ");
            SQL.AppendLine("StartDt, EndDt, DurationDay, TrainingPosCode, TrainingDeptCode, Result, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @DocDt, 'N', EmpCode, PosCode, DeptCode, @TrainingDesc, ");
            SQL.AppendLine("@StartDt, @EndDt, @DurationDay, @TrainingPosCode, @TrainingDeptCode, @Result, @Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblEmployee Where EmpCode=@EmpCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@TrainingDesc", MeeTrainingDesc.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<Decimal>(ref cm, "@DurationDay", Decimal.Parse(TxtDurationDay.Text));
            Sm.CmParam<String>(ref cm, "@TrainingPosCode", Sm.GetLue(LueTrainingPosCode));
            Sm.CmParam<String>(ref cm, "@TrainingDeptCode", Sm.GetLue(LueTrainingDeptCode));
            Sm.CmParam<String>(ref cm, "@Result", Sm.GetLue(LueResult));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmployee(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployee Set ");
            SQL.AppendLine("    PosCode=If(@TrainingPosCode.Length=0, PosCode, @TrainingPosCode), ");
            SQL.AppendLine("    DeptCode=If(@TrainingDeptCode.Length=0, DeptCode, @TrainingDeptCode), ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode In ( ");
            SQL.AppendLine("    Select EmpCode From TblOJT ");
            SQL.AppendLine("    Where DocNo=@DocNo And Result='P' And CancelInd='N' ");
            SQL.AppendLine("    And (TrainingPosCode Is Not Null ");
            SQL.AppendLine("    Or TrainingDeptCode Is Not Null) ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@TrainingPosCode", Sm.GetLue(LueTrainingPosCode));
            Sm.CmParam<String>(ref cm, "@TrainingDeptCode", Sm.GetLue(LueTrainingDeptCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditOJT());
            cml.Add(SaveEmployee(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataPassedAlready() ||
                IsDataFailedAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblOJT " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataPassedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblOJT " +
                    "Where CancelInd='N' And Result='P' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This training already passed.");
                return true;
            }
            return false;
        }

        private bool IsDataFailedAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblOJT " +
                    "Where CancelInd='N' And Result='F' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This training already failed.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditOJT()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOJT Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, Result=@Result, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Result", Sm.GetLue(LueResult));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOJT(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowOJT(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, CancelInd, ");
            SQL.AppendLine("A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, B.JoinDt, ");
            SQL.AppendLine("A.TrainingDesc, A.StartDt, A.EndDt, A.DurationDay, A.TrainingPosCode, A.TrainingDeptCode, A.Result, A.Remark ");
            SQL.AppendLine("From TblOjt A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "EmpCode", "EmpName", "EmpCodeOld",  
                        
                        //6-10
                        "PosName", "DeptName", "JoinDt", "TrainingDesc", "StartDt", 
 
                        //11-15
                        "EndDt", "DurationDay", "TrainingPosCode", "TrainingDeptCode", "Result",   
                        
                        //16
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                        TxtEmpName.EditValue = Sm.DrStr(dr, c[4]);
                        TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPosCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetDte(DteJoinDt, Sm.DrStr(dr, c[8]));
                        MeeTrainingDesc.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[10]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[11]));
                        TxtDurationDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        Sm.SetLue(LueTrainingPosCode, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueTrainingDeptCode, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueResult, Sm.DrStr(dr, c[15]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                    }, true
                );
        }

        #endregion

        #region Additional Setting

        private void ComputeDurationDay()
        {
            TxtDurationDay.EditValue = 0;

            if (Sm.GetDte(DteStartDt).ToString().Length > 0 && Sm.GetDte(DteEndDt).ToString().Length > 0)
            {
                string
                    StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                    EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                TxtDurationDay.EditValue = (Dt2 - Dt1).Days + 1;

            }
        }

        private void SetLueResult(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 'P' As Col1, 'Passed' As Col2 Union All ");
                SQL.AppendLine("Select 'F' As Col1, 'Failed' As Col2 ; ");
                
                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmOJTDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            DteEndDt.EditValue = DteStartDt.EditValue;
            ComputeDurationDay();
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            ComputeDurationDay();
        }

        private void LueTrainingDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueTrainingPosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingPosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        #endregion

        #endregion


    }
}
