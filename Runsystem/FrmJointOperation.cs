﻿#region Update
/*
   31/07/2019 [DITA] master baru
 * */
#endregion
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJointOperation : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mInd = "0";
        internal FrmJointOperationFind FrmFind;

        #endregion

        #region Constructor

        public FrmJointOperation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Joint Operation";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-3
                        "Contact Person"+Environment.NewLine+"Name",
                        "ID Number",
                        "Contact Number",
                    },
                     new int[] 
                    {
                        0, 
                        200, 100, 100
                    }
                );
            
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0});
        }

      

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtJOCode, TxtJOName, MeeRemark, ChkActInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3 });
                    TxtJOCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtJOCode, TxtJOName, MeeRemark, ChkActInd
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3 });
                    TxtJOCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtJOName, MeeRemark, ChkActInd
                    }, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {  TxtJOCode }, true);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3 });
                    TxtJOName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
               TxtJOCode, TxtJOName, MeeRemark, ChkActInd
            });
            Sm.ClearGrd(Grd1, true);
            mInd = "0";
            ChkActInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJointOperationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                mInd = "1";
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtJOCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mInd == "1")
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        //override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        //{
        //    if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 3, 5, 6, 7, 8, 9 }, e.ColIndex))
        //    {
        //        if (e.ColIndex == 3) LueRequestEdit(Grd1, LueStatus, ref fCell, ref fAccept, e);
        //        if (e.ColIndex == 5) LueRequestEdit(Grd1, LueGender, ref fCell, ref fAccept, e);
        //        if (e.ColIndex == 6) Sm.DteRequestEdit(Grd1, DteBirthDt, ref fCell, ref fAccept, e);
        //        Sm.GrdRequestEdit(Grd1, e.RowIndex);
        //    }
        //}

        //override protected void GrdKeyDown(object sender, KeyEventArgs e)
        //{
        //    Sm.GrdRemoveRow(Grd1, e, BtnSave);
        //    Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        //}

        //override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        //{
        //    Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 1, 7, 8, 9 }, e);
        //}

        #endregion

        #region Save Data

        #region Insert Data
        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveJointOperationHdr());

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    cml.Add(SaveJointOperationDtl(Row));


            Sm.ExecCommands(cml);

            ShowData(TxtJOCode.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtJOCode, "Joint Operation Code", false) ||
                Sm.IsTxtEmpty(TxtJOName, "Joint Operation Name", false) ||
                IsGrdEmpty() ||
                IsJointOperationInvalid()||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in Joint Operation list.");
                return true;
            }
            return false;
        }
        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Joint Operation Name is empty.")) return true;
                }
            }
            return false;
        }

        private bool IsJointOperationInvalid()
        {
            for (int i = 0; i < Grd1.Rows.Count - 1; i++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, i, 1, false, "Contact Person Name is empty.")) { return true; }
                if (Sm.IsGrdValueEmpty(Grd1, i, 2, false, "ID Number is empty.")) {  return true; }
                if (Sm.IsGrdValueEmpty(Grd1, i, 3, false, "Contact Number is empty.")) {  return true; }
            }

            return false;
        }

        private MySqlCommand SaveJointOperationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblJointOperationHdr ");
            SQL.AppendLine("(JOCode, JOName, ActInd,Remark, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@JOCode, @JOName, @ActInd, @Remark, @CreateBy, CurrentDateTime()); ");
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@JOCode", TxtJOCode.Text);
            Sm.CmParam<String>(ref cm, "@JOName", TxtJOName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveJointOperationDtl(int Row)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Insert Into TblJointOperationDtl(JOCode, DNo, ContactPersonName, IDNumber, ContactNumber, ");
            SQLDtl.AppendLine("CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values(@JOCode, @DNo, @ContactPersonName, @IDNumber, @ContactNumber,@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
            Sm.CmParam<String>(ref cm, "@JOCode", TxtJOCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ContactPersonName", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@IDNumber", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@ContactNumber", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion
        #region Edit Data
        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;
            

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(EditJointOperationHdr());
           
                cml.Add(DeleteJointOperationDtl());

                if (Grd1.Rows.Count >= 1)
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                            cml.Add(SaveJointOperationDtl(Row));

            

            Sm.ExecCommands(cml);

            ShowData(TxtJOCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtJOName, "Joint Operation Name", false) ||
                IsJointOperationInvalid() ||
                IsGrdValueNotValid();
            ;
        }

        private MySqlCommand EditJointOperationHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblJointOperationHdr Set ");
            SQL.AppendLine(" JOName=@JOName, ActInd=@ActInd, Remark=@Remark,LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where JOCode=@JOCode ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@JOCode", TxtJOCode.Text);
            Sm.CmParam<String>(ref cm, "@JOName", TxtJOName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteJointOperationDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblJointOperationDtl Where JOCode = @JOCode ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@JOCode", TxtJOCode.Text);

            return cm;
        }
  
      
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string JOCode)
        {
            try
            {
                ClearData();
                ShowJointOperationHdr(JOCode);
                ShowJointOperationDtl(JOCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowJointOperationHdr(string JOCode)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@JOCode", JOCode);

            SQL.AppendLine("Select JOCode, JOName, ActInd, Remark ");
            SQL.AppendLine("From TblJointOperationHdr  ");
            SQL.AppendLine("Where JOCode=@JOCode;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "JOCode", 
                        
                        //1-3
                        "JOName", "ActInd", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtJOCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtJOName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowJointOperationDtl(string JOCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select JOCode, DNo, ContactPersonName, IDNumber, ContactNumber ");
            SQL.AppendLine("From TblJointOperationDtl");
            SQL.AppendLine("Where JOCode=@JOCode ");
            SQL.AppendLine("Order By DNo;");

            Sm.CmParam<String>(ref cm, "@JOCode", JOCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DNo",

                        //1-3
                        "ContactPersonName", "IDNumber", "ContactNumber",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                      
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method
        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }


        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                }
            }
        }

        public static void GrdRequestEdit(iGrid Grd, int RowIndex)
        {
            if ((Grd.Rows.Count == 1) || (Grd.Rows.Count != 1 && RowIndex == Grd.Rows.Count - 1)) Grd.Rows.Add();
        }
        #endregion

      
        #endregion



        #region Event

        #region Misc Control Event
        private void TxtJOCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtJOCode);
        }
        private void TxtJOName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtJOName);
        }

        private void MeeRemark_Validated_1(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeRemark);
        }
       
        #endregion

     
       

        #endregion  

      
    }
}
