﻿namespace RunSystem
{
    partial class FrmMaterialRequest5Dlg8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueJobCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkJobCtCode = new DevExpress.XtraEditors.CheckEdit();
            this.ChkJobCode = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtJobCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJobCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnChoose
            // 
            this.BtnChoose.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChoose.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChoose.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChoose.Appearance.Options.UseBackColor = true;
            this.BtnChoose.Appearance.Options.UseFont = true;
            this.BtnChoose.Appearance.Options.UseForeColor = true;
            this.BtnChoose.Appearance.Options.UseTextOptions = true;
            this.BtnChoose.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueJobCtCode);
            this.panel2.Controls.Add(this.ChkJobCtCode);
            this.panel2.Controls.Add(this.ChkJobCode);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtJobCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Size = new System.Drawing.Size(672, 53);
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(672, 420);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // LueJobCtCode
            // 
            this.LueJobCtCode.EnterMoveNextControl = true;
            this.LueJobCtCode.Location = new System.Drawing.Point(104, 5);
            this.LueJobCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueJobCtCode.Name = "LueJobCtCode";
            this.LueJobCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueJobCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueJobCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueJobCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueJobCtCode.Properties.DropDownRows = 30;
            this.LueJobCtCode.Properties.NullText = "[Empty]";
            this.LueJobCtCode.Properties.PopupWidth = 250;
            this.LueJobCtCode.Size = new System.Drawing.Size(219, 20);
            this.LueJobCtCode.TabIndex = 29;
            this.LueJobCtCode.ToolTip = "F4 : Show/hide list";
            this.LueJobCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueJobCtCode.EditValueChanged += new System.EventHandler(this.LueJobCtCode_EditValueChanged);
            // 
            // ChkJobCtCode
            // 
            this.ChkJobCtCode.Location = new System.Drawing.Point(326, 4);
            this.ChkJobCtCode.Name = "ChkJobCtCode";
            this.ChkJobCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkJobCtCode.Properties.Appearance.Options.UseFont = true;
            this.ChkJobCtCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkJobCtCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkJobCtCode.Properties.Caption = " ";
            this.ChkJobCtCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkJobCtCode.Size = new System.Drawing.Size(20, 22);
            this.ChkJobCtCode.TabIndex = 30;
            this.ChkJobCtCode.ToolTip = "Remove filter";
            this.ChkJobCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkJobCtCode.ToolTipTitle = "Run System";
            this.ChkJobCtCode.CheckedChanged += new System.EventHandler(this.ChkJobCtCode_CheckedChanged);
            // 
            // ChkJobCode
            // 
            this.ChkJobCode.Location = new System.Drawing.Point(326, 24);
            this.ChkJobCode.Name = "ChkJobCode";
            this.ChkJobCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkJobCode.Properties.Appearance.Options.UseFont = true;
            this.ChkJobCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkJobCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkJobCode.Properties.Caption = " ";
            this.ChkJobCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkJobCode.Size = new System.Drawing.Size(20, 22);
            this.ChkJobCode.TabIndex = 33;
            this.ChkJobCode.ToolTip = "Remove filter";
            this.ChkJobCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkJobCode.ToolTipTitle = "Run System";
            this.ChkJobCode.CheckedChanged += new System.EventHandler(this.ChkJobCode_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(13, 8);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 14);
            this.label5.TabIndex = 28;
            this.label5.Text = "Job\'s Category";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJobCode
            // 
            this.TxtJobCode.EnterMoveNextControl = true;
            this.TxtJobCode.Location = new System.Drawing.Point(104, 26);
            this.TxtJobCode.Name = "TxtJobCode";
            this.TxtJobCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJobCode.Properties.Appearance.Options.UseFont = true;
            this.TxtJobCode.Properties.MaxLength = 250;
            this.TxtJobCode.Size = new System.Drawing.Size(219, 20);
            this.TxtJobCode.TabIndex = 32;
            this.TxtJobCode.Validated += new System.EventHandler(this.TxtJobCode_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(74, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 14);
            this.label2.TabIndex = 31;
            this.label2.Text = "Job";
            // 
            // FrmMaterialRequest5Dlg8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 473);
            this.Name = "FrmMaterialRequest5Dlg8";
            this.Text = "List of Job Category";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueJobCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkJobCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJobCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueJobCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkJobCtCode;
        private DevExpress.XtraEditors.CheckEdit ChkJobCode;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtJobCode;
        private System.Windows.Forms.Label label2;
    }
}