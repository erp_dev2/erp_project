﻿namespace RunSystem
{
    partial class FrmAPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAPS));
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteVdInvDt = new DevExpress.XtraEditors.DateEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtVdInvNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtAmountAPS = new DevExpress.XtraEditors.TextEdit();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtOutstandingInv = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDocNoPI = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnPI = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPI2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmountAPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingInv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoPI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(815, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtLocalDocNo);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.BtnPI2);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.BtnPI);
            this.panel2.Controls.Add(this.TxtVdCode);
            this.panel2.Controls.Add(this.TxtDocNoPI);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.TxtVdInvNo);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(815, 138);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 138);
            this.panel3.Size = new System.Drawing.Size(815, 335);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(815, 335);
            this.Grd1.TabIndex = 33;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(122, 48);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(96, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(85, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(122, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(190, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(45, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteVdInvDt
            // 
            this.DteVdInvDt.EditValue = null;
            this.DteVdInvDt.EnterMoveNextControl = true;
            this.DteVdInvDt.Location = new System.Drawing.Point(140, 8);
            this.DteVdInvDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteVdInvDt.Name = "DteVdInvDt";
            this.DteVdInvDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteVdInvDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteVdInvDt.Properties.Appearance.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDownHeaderHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDownHeaderHighlight.Options.UseFont = true;
            this.DteVdInvDt.Properties.AppearanceDropDownHighlight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVdInvDt.Properties.AppearanceDropDownHighlight.Options.UseFont = true;
            this.DteVdInvDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteVdInvDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteVdInvDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVdInvDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteVdInvDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVdInvDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteVdInvDt.Properties.ReadOnly = true;
            this.DteVdInvDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteVdInvDt.Size = new System.Drawing.Size(154, 20);
            this.DteVdInvDt.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(5, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Vendor\'s Invoice Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdInvNo
            // 
            this.TxtVdInvNo.EnterMoveNextControl = true;
            this.TxtVdInvNo.Location = new System.Drawing.Point(122, 114);
            this.TxtVdInvNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdInvNo.Name = "TxtVdInvNo";
            this.TxtVdInvNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdInvNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdInvNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdInvNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVdInvNo.Properties.MaxLength = 30;
            this.TxtVdInvNo.Properties.ReadOnly = true;
            this.TxtVdInvNo.Size = new System.Drawing.Size(190, 20);
            this.TxtVdInvNo.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(11, 117);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 14);
            this.label4.TabIndex = 21;
            this.label4.Text = "Vendor\'s Invoice#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(71, 95);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 19;
            this.label8.Text = "Vendor";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtAmountAPS);
            this.panel5.Controls.Add(this.TxtCurCode);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.TxtOutstandingInv);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.DteVdInvDt);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(455, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(360, 138);
            this.panel5.TabIndex = 27;
            // 
            // TxtAmountAPS
            // 
            this.TxtAmountAPS.EnterMoveNextControl = true;
            this.TxtAmountAPS.Location = new System.Drawing.Point(140, 74);
            this.TxtAmountAPS.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmountAPS.Name = "TxtAmountAPS";
            this.TxtAmountAPS.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmountAPS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmountAPS.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmountAPS.Properties.Appearance.Options.UseFont = true;
            this.TxtAmountAPS.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmountAPS.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmountAPS.Properties.ReadOnly = true;
            this.TxtAmountAPS.Size = new System.Drawing.Size(154, 20);
            this.TxtAmountAPS.TabIndex = 30;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(140, 30);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.MaxLength = 3;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(154, 20);
            this.TxtCurCode.TabIndex = 26;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(78, 31);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 14);
            this.label24.TabIndex = 25;
            this.label24.Text = "Currency";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(16, 76);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(117, 14);
            this.label9.TabIndex = 29;
            this.label9.Text = "Settlement Amount";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOutstandingInv
            // 
            this.TxtOutstandingInv.EnterMoveNextControl = true;
            this.TxtOutstandingInv.Location = new System.Drawing.Point(140, 52);
            this.TxtOutstandingInv.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstandingInv.Name = "TxtOutstandingInv";
            this.TxtOutstandingInv.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOutstandingInv.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstandingInv.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstandingInv.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstandingInv.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstandingInv.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstandingInv.Properties.ReadOnly = true;
            this.TxtOutstandingInv.Size = new System.Drawing.Size(154, 20);
            this.TxtOutstandingInv.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(11, 53);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 14);
            this.label6.TabIndex = 27;
            this.label6.Text = "Outstanding Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(140, 96);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 300;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(212, 20);
            this.MeeRemark.TabIndex = 32;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(86, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 31;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNoPI
            // 
            this.TxtDocNoPI.EnterMoveNextControl = true;
            this.TxtDocNoPI.Location = new System.Drawing.Point(122, 70);
            this.TxtDocNoPI.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNoPI.Name = "TxtDocNoPI";
            this.TxtDocNoPI.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNoPI.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNoPI.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNoPI.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNoPI.Properties.MaxLength = 16;
            this.TxtDocNoPI.Properties.ReadOnly = true;
            this.TxtDocNoPI.Size = new System.Drawing.Size(190, 20);
            this.TxtDocNoPI.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(10, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Purchase Invoice#";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(122, 92);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 80;
            this.TxtVdCode.Properties.ReadOnly = true;
            this.TxtVdCode.Size = new System.Drawing.Size(326, 20);
            this.TxtVdCode.TabIndex = 20;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(318, 5);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(65, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // BtnPI
            // 
            this.BtnPI.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPI.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPI.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPI.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPI.Appearance.Options.UseBackColor = true;
            this.BtnPI.Appearance.Options.UseFont = true;
            this.BtnPI.Appearance.Options.UseForeColor = true;
            this.BtnPI.Appearance.Options.UseTextOptions = true;
            this.BtnPI.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPI.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPI.Image = ((System.Drawing.Image)(resources.GetObject("BtnPI.Image")));
            this.BtnPI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPI.Location = new System.Drawing.Point(315, 69);
            this.BtnPI.Name = "BtnPI";
            this.BtnPI.Size = new System.Drawing.Size(24, 21);
            this.BtnPI.TabIndex = 17;
            this.BtnPI.ToolTip = "Find Purchase Invoice";
            this.BtnPI.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPI.ToolTipTitle = "Run System";
            this.BtnPI.Click += new System.EventHandler(this.BtnPI_Click);
            // 
            // BtnPI2
            // 
            this.BtnPI2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPI2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPI2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPI2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPI2.Appearance.Options.UseBackColor = true;
            this.BtnPI2.Appearance.Options.UseFont = true;
            this.BtnPI2.Appearance.Options.UseForeColor = true;
            this.BtnPI2.Appearance.Options.UseTextOptions = true;
            this.BtnPI2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPI2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPI2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPI2.Image")));
            this.BtnPI2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPI2.Location = new System.Drawing.Point(343, 69);
            this.BtnPI2.Name = "BtnPI2";
            this.BtnPI2.Size = new System.Drawing.Size(24, 21);
            this.BtnPI2.TabIndex = 18;
            this.BtnPI2.ToolTip = "Show Purchase Invoice";
            this.BtnPI2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPI2.ToolTipTitle = "Run System";
            this.BtnPI2.Click += new System.EventHandler(this.BtnPI2_Click);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(122, 27);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 250;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(190, 20);
            this.TxtLocalDocNo.TabIndex = 29;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(52, 30);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 14);
            this.label18.TabIndex = 28;
            this.label18.Text = "Local Doc#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmAPS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 473);
            this.Name = "FrmAPS";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVdInvDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdInvNo.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmountAPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingInv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNoPI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteVdInvDt;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtVdInvNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        internal DevExpress.XtraEditors.TextEdit TxtDocNoPI;
        private System.Windows.Forms.Label label3;
        protected System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtOutstandingInv;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnPI;
        internal DevExpress.XtraEditors.TextEdit TxtAmountAPS;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        public DevExpress.XtraEditors.SimpleButton BtnPI2;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label18;
    }
}