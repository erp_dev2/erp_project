﻿#region Update 
/*
    20/04/2022 [ICA/HIN] new Apps
    20/05/2022 [MYA/HIN] menambah validasi ketika udah di process di performancereview
    20/05/2022 [ICA/HIN] bug GRD nya masih belum sesuai
    22/11/2022 [BRI/HIN] merubah rumus dan label
    29/11/2022 [BRI/HIN] bug pada validasi
    07/12/2022 [WED/HIN] tambah status approval
    10/12/2022 [WED/HIN] rubah judul kolom
    14/12/2022 [WED/HIN] bug docapproval
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmGoalsProcess : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = string.Empty;
        internal bool mIsFilterBySite = false;
        internal FrmGoalsProcessFind FrmFind;
        iGCell fCell;
        bool fAccept;
        bool IsInsert = false;

        #endregion

        #region Constructor

        public FrmGoalsProcess(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueOption(ref LuePeriod, "GoalsProcessPeriode");
                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                   new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and Target",
                        "TypeCode",
                        "Type",

                        //6-9
                        "Realization/"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        200, 120, 150, 100, 150,
                        
                        //6-9
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 4 });

            #endregion

            #region Grid2

            Grd2.Cols.Count = 10;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and Target",
                        "TypeCode",
                        "Type",

                        //6-9
                        "Realization/"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[]
                    {
                        //0
                        0,
 
                        //1-5
                        200, 120, 150, 100, 150,
                        
                        //6-9
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 7, 8 });
            Sm.GrdColInvisible(Grd2, new int[] { 4 });

            #endregion

            #region Grid3

            Grd3.Cols.Count = 10;
            Grd3.FrozenArea.ColCount = 1;
            Grd3.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and Target",
                        "TypeCode",
                        "Type",

                        //6-9
                        "Realization/"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[]
                    {
                        //0
                        0,
 
                        //1-5
                        200, 120, 150, 100, 150,
                        
                        //6-9
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 7, 8 });
            Sm.GrdColInvisible(Grd3, new int[] { 4 });

            #endregion

            #region Grid4

            Grd4.Cols.Count = 10;
            Grd4.FrozenArea.ColCount = 1;
            Grd4.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                   new string[]
                    {
                        //0
                        "DNo",

                        //1-5
                        "Strategic Objective",
                        "Weight",
                        "Measurement and Target",
                        "TypeCode",
                        "Type",

                        //6-9
                        "Realization/"+Environment.NewLine+"Achievment",
                        "Value",
                        "Score",
                        "Evidence",
                    },
                    new int[]
                    {
                       //0
                        0,
 
                        //1-5
                        200, 120, 150, 100, 150,
                        
                        //6-9
                        120, 120, 120, 200
                    }
                );
            Sm.GrdFormatDec(Grd4, new int[] { 2, 6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 7, 8 });
            Sm.GrdColInvisible(Grd4, new int[] { 4 });

            #endregion

            #region Grid5

            Grd5.Cols.Count = 5;
            Grd5.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                Grd5,
                new string[]
                {
                    //0
                    "No",

                    //1-4
                    "User",
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[] { 50, 150, 200, 100, 200 }
            );
            Sm.GrdFormatDate(Grd5, new int[] { 3 });
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 1, 2, 3, 4 });

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
           

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtGoalsDocNo, TxtPICCode, TxtPICName, MeeRemark, TxtGoalsName, 
                        LueYr, LuePeriod, MeeCancelReason, ChkCancelInd
                    }, true);
                    BtnGoals.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 6, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      DteDocDt, MeeRemark, LueYr, LuePeriod
                    }, false);
                    BtnGoals.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 6, 9 });
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                      ChkCancelInd, MeeCancelReason
                    }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 6, 9 });
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 6, 9 });
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtGoalsDocNo, TxtPICCode, TxtPICName,  MeeRemark, TxtGoalsName,
                MeeCancelReason, LueYr, LuePeriod, TxtStatus
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtTotalScore, TxtTotal, TxtTotal2, TxtTotal3, TxtTotal4,
                TxtScore, TxtScore2, TxtScore3, TxtScore4
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 2, 6, 7, 8 });
            Sm.ClearGrd(Grd5, false);
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGoalsProcessFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.EditValue = "Outstanding";
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Method

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if(BtnSave.Enabled && e.ColIndex == 6)
            {
                ComputeValue(Grd1, e.RowIndex);
            }
            ComputeTotalScore(Grd1);
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 6)
            {
                ComputeValue(Grd2, e.RowIndex);
            }
            ComputeTotalScore(Grd2);
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 6)
            {
                ComputeValue(Grd3, e.RowIndex);
            }
            ComputeTotalScore(Grd3);
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 6)
            {
                ComputeValue(Grd4, e.RowIndex);
            }
            ComputeTotalScore(Grd4);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeTotalScore(Grd1);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            ComputeTotalScore(Grd2);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeTotalScore(Grd3);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd4, e, BtnSave);
            ComputeTotalScore(Grd4);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GoalsProcess", "TblGoalsProcessHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveGoalsProcessHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveGoalsProcessDtl(DocNo, Row));
            }
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0) cml.Add(SaveGoalsProcessDtl2(DocNo, Row));
            }
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveGoalsProcessDtl3(DocNo, Row));
            }
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd4, Row, 1).Length > 0) cml.Add(SaveGoalsProcessDtl4(DocNo, Row));
            }
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtGoalsDocNo, "Goals Document", false) ||
                Sm.IsTxtEmpty(TxtGoalsName, "Goals Name", false) ||
                Sm.IsTxtEmpty(TxtPICCode, "PIC Code", false) ||
                Sm.IsTxtEmpty(TxtPICName, "PIC Name", false) ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LuePeriod, "Period") ||
                IsDocNoAlreadyProcess() ||
                IsGoalsDocNoAlreadyProcess() ||
                IsDataCancelledAlready() ||
                IsGrdEmpty();
        }

        private bool IsDocNoAlreadyProcess()
        {
            //nunggu task selanjutnya kelar karena belum tau nama table nya apa
            if (ChkCancelInd.Checked)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select DocNo From TblNewPerformanceReviewHdr Where GoalsProcessDocNo=@DocNo And CancelInd='N'"
                };

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This document already Process.");
                    return true;
                }
            }

            return false;
        }

        private bool IsGoalsDocNoAlreadyProcess()
        {
            if(TxtDocNo.Text.Length == 0)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select DocNo From TblGoalsProcessHdr Where GoalsDocNo=@GoalsDocNo " +
                    "And CancelInd = 'N' And Yr=@Yr And Period=@Period;"
                };

                Sm.CmParam<String>(ref cm, "@GoalsDocNo", TxtGoalsDocNo.Text);
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Period", Sm.GetLue(LuePeriod));
                if(Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Goals document already Process in "+LuePeriod.Text+" - "+Sm.GetLue(LueYr)+".");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1 && Grd2.Rows.Count == 1 && Grd3.Rows.Count == 1 && Grd4.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblGoalsProcessHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveGoalsProcessHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsProcessHdr(DocNo, DocDt, Status, GoalsDocNo, Yr, Period, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', @GoalsDocNo, @Yr, @Period, @Remark, @UserCode, CurrentDateTime()) ");
            //region bisa edit hdr dan detail
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update DocDt=@DocDt, CancelInd=@CancelInd, CancelReason=@CancelReason, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            if (IsInsert)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Inner Join TblGoalsSettingHdr T1 On T1.DocNo = @GoalsDocNo ");
                SQL.AppendLine("Inner Join TblEmployee T2 On T1.PICCode = T2.EmpCode And T.DeptCode = T2.DeptCode And T.SiteCode = T2.SiteCode ");
                SQL.AppendLine("Where T.DocType='GoalsProcess' ");
                SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt); ");

                SQL.AppendLine("Update TblGoalsProcessHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='GoalsProcess' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ); ");
            }
            
            SQL.AppendLine("Delete From TblGoalsProcessDtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblGoalsProcessDtl2 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblGoalsProcessDtl3 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblGoalsProcessDtl4 Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@GoalsDocNo", TxtGoalsDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Period", Sm.GetLue(LuePeriod));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveGoalsProcessDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsProcessDtl(DocNo, DNo, Realization, Value, QualityValue, RealizationProof, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Realization, @Value, @Quality, @RealizationProof, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Realization", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@RealizationProof", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveGoalsProcessDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsProcessDtl2(DocNo, DNo, Realization, Value, QualityValue, RealizationProof, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Realization, @Value, @Quality, @RealizationProof, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Realization", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd2, Row, 8));
            Sm.CmParam<String>(ref cm, "@RealizationProof", Sm.GetGrdStr(Grd2, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand SaveGoalsProcessDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsProcessDtl3(DocNo, DNo, Realization, Value, QualityValue, RealizationProof, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Realization, @Value, @Quality, @RealizationProof, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd3, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Realization", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd3, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd3, Row, 8));
            Sm.CmParam<String>(ref cm, "@RealizationProof", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand SaveGoalsProcessDtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsProcessDtl4(DocNo, DNo, Realization, Value, QualityValue, RealizationProof, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Realization, @Value, @Quality, @RealizationProof, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd4, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Realization", Sm.GetGrdDec(Grd4, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd4, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd4, Row, 8));
            Sm.CmParam<String>(ref cm, "@RealizationProof", Sm.GetGrdStr(Grd4, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowGoalsProcessHdr(DocNo);
                ShowGoalsProcessDtl(DocNo);
                ShowGoalsProcessDtl2(DocNo);
                ShowGoalsProcessDtl3(DocNo);
                ShowGoalsProcessDtl4(DocNo);
                Sm.ShowDocApproval(DocNo, "GoalsProcess", ref Grd5);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowApprovalInfo(string DocNo)
        {

        }

        private void ShowGoalsProcessHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CancelReason, A.GoalsDocNo, A.Yr, A.Period, ");
            SQL.AppendLine("B.GoalsName, B.PICCode, C.EmpName, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' Else '' End As StatusDesc ");
            SQL.AppendLine("From tblGoalsProcessHdr A ");
            SQL.AppendLine("Inner Join TblGoalsSettingHdr B On A.GoalsDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "GoalsDocNo", "Yr",  
                        
                    //6-10
                    "Period", "GoalsName", "PICCode", "EmpName", "Remark",

                    //11
                    "StatusDesc"
                        
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    TxtGoalsDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LuePeriod, Sm.DrStr(dr, c[6]));
                    TxtGoalsName.EditValue = Sm.DrStr(dr, c[7]);
                    TxtPICCode.EditValue = Sm.DrStr(dr, c[8]);
                    TxtPICName.EditValue = Sm.DrStr(dr, c[9]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[11]);
                }, true
            );
        }

        private void ShowGoalsProcessDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("FROM TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            ComputeTotalScore(Grd1);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowGoalsProcessDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("FROM TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl2 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl2 C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            ComputeTotalScore(Grd2);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowGoalsProcessDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("FROM TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl3 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl3 C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            ComputeTotalScore(Grd3);
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowGoalsProcessDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT C.DNo, C.WorkGoals, C.Quality, C.Measure, C.Evaluation, D.OptDesc, ");
            SQL.AppendLine("B.Realization, B.Value, B.QualityValue, B.RealizationProof ");
            SQL.AppendLine("FROM TblGoalsProcessHdr A ");
            SQL.AppendLine("INNER JOIN TblGoalsProcessDtl4 B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblGoalsSettingDtl4 C ON C.DocNo = A.GoalsDocNo AND C.DNo = B.DNo ");
            SQL.AppendLine("LEFT JOIN TblOption D ON D.OptCat = 'GoalsSettingEvaluation' AND D.OptCode = C.Evaluation ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation","OptDesc", 
                    //6-10
                    "Realization", "Value", "QualityValue", "RealizationProof"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            ComputeTotalScore(Grd4);
            Sm.FocusGrd(Grd4, 0, 1);
        }

        internal void ShowGoalsSettingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Grd1.Cells[Row, 6].Value = 0m;
                    Grd1.Cells[Row, 7].Value = 0m;
                    Grd1.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            ComputeTotalScore(Grd1);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowGoalsSettingDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl2 A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd2.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 5);
                    Grd2.Cells[Row, 6].Value = 0m;
                    Grd2.Cells[Row, 7].Value = 0m;
                    Grd2.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            ComputeTotalScore(Grd2);
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ShowGoalsSettingDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl3 A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd3.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 5);
                    Grd3.Cells[Row, 6].Value = 0m;
                    Grd3.Cells[Row, 7].Value = 0m;
                    Grd3.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            ComputeTotalScore(Grd3);
            Sm.FocusGrd(Grd3, 0, 1);
        }

        internal void ShowGoalsSettingDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.WorkGoals, A.Quality, A.Measure, A.Evaluation, B.OptDesc EvaluationName ");
            SQL.AppendLine("From tblGoalsSettingDtl4 A ");
            SQL.AppendLine("Left Join TblOption B On B.OptCat = 'GoalsSettingEvaluation' and A.Evaluation = B.OptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd4.Cells[Row, 0].Value = (Row + 1).ToString();
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 5, 5);
                    Grd4.Cells[Row, 6].Value = 0m;
                    Grd4.Cells[Row, 7].Value = 0m;
                    Grd4.Cells[Row, 8].Value = 0m;
                }, false, false, true, false
            );
            ComputeTotalScore(Grd4);
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
        }

        private void ComputeValue(iGrid Grd, int row)
        {
            decimal Realization = 0m;

            Realization = Sm.GetGrdDec(Grd, row, 6);

            if(Sm.GetGrdStr(Grd, row, 4).Length > 0)
            {
                if (Sm.GetGrdStr(Grd, row, 4) == "KN")
                {
                    if (Realization < 75)
                        Grd.Cells[row, 7].Value = 1m;
                    else if (Realization < 95)
                        Grd.Cells[row, 7].Value = 2m;
                    else if (Realization < 110)
                        Grd.Cells[row, 7].Value = 3m;
                    else if (Realization < 120)
                        Grd.Cells[row, 7].Value = 4m;
                    else
                        Grd.Cells[row, 7].Value = 5m;
                }
                else
                {
                    if (Realization < 30)
                        Grd.Cells[row, 7].Value = 1m;
                    else if (Realization < 50)
                        Grd.Cells[row, 7].Value = 2m;
                    else if (Realization < 80)
                        Grd.Cells[row, 7].Value = 3m;
                    else if (Realization < 95)
                        Grd.Cells[row, 7].Value = 4m;
                    else
                        Grd.Cells[row, 7].Value = 5m;
                }

                Grd.Cells[row, 8].Value = Sm.GetGrdDec(Grd, row, 2) * Sm.GetGrdDec(Grd, row, 7) / 100;
            }
        }

        private void ComputeTotalScore(iGrid Grd)
        {
            decimal Total = 0m, Total2 = 0m, Total3 = 0m, Total4 = 0m,
            Score = 0m, Score2 = 0m, Score3 = 0m, Score4 = 0m;
            TxtTotalScore.Text = string.Empty;
            if (Grd == Grd1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd, Row, 2).Length > 0) Total += Sm.GetGrdDec(Grd, Row, 2);
                    if (Sm.GetGrdStr(Grd, Row, 8).Length > 0) Score += Sm.GetGrdDec(Grd, Row, 8);
                }
                TxtTotal.Text = Convert.ToString(Total);
                TxtScore.Text = Convert.ToString(Score);
            }

            if (Grd == Grd2)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd, Row, 2).Length > 0) Total2 += Sm.GetGrdDec(Grd, Row, 2);
                    if (Sm.GetGrdStr(Grd, Row, 8).Length > 0) Score2 += Sm.GetGrdDec(Grd, Row, 8);
                }
                TxtTotal2.Text = Convert.ToString(Total2);
                TxtScore2.Text = Convert.ToString(Score2);
            }

            if (Grd == Grd3)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd, Row, 2).Length > 0) Total3 += Sm.GetGrdDec(Grd, Row, 2);
                    if (Sm.GetGrdStr(Grd, Row, 8).Length > 0) Score3 += Sm.GetGrdDec(Grd, Row, 8);
                }
                TxtTotal3.Text = Convert.ToString(Total3);
                TxtScore3.Text = Convert.ToString(Score3);
            }

            if (Grd == Grd4)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd, Row, 2).Length > 0) Total4 += Sm.GetGrdDec(Grd, Row, 2);
                    if (Sm.GetGrdStr(Grd, Row, 8).Length > 0) Score4 += Sm.GetGrdDec(Grd, Row, 8);
                }
                TxtTotal4.Text = Convert.ToString(Total4);
                TxtScore4.Text = Convert.ToString(Score4);
            }
            TxtTotalScore.Text = Convert.ToString(
                decimal.Parse(TxtScore.Text) + 
                decimal.Parse(TxtScore2.Text) + 
                decimal.Parse(TxtScore3.Text) + 
                decimal.Parse(TxtScore4.Text));
        }

        #endregion

        #endregion

        #region Event
        private void BtnGoals_Click(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueYr, "Year")) return;
            Sm.FormShowDialog(new FrmGoalsProcessDlg(this));
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

    }
}
