﻿namespace RunSystem
{
    partial class FrmRecvWhs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRecvWhs));
            this.LueBin = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLot = new DevExpress.XtraEditors.LookUpEdit();
            this.TcRecvWhs = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueProductionWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.LblGroup = new System.Windows.Forms.Label();
            this.TxtJournalDocNo2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtJournalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueWhsCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.DteKBDecreeDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtKBInsuranceAmt = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtKBDecreeNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtKBPackagingQty = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.DteKBRegistrationDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.DteKBPLDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteKBContractDt = new DevExpress.XtraEditors.DateEdit();
            this.LueCustomsDocCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtKBSubmissionNo = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.ChkKBNonDocInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtKBPackaging = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnKBContractNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtKBRegistrationNo = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtKBPLNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtKBContractNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvWhs)).BeginInit();
            this.TcRecvWhs.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBDecreeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBDecreeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBInsuranceAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBDecreeNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackagingQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomsDocCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkKBNonDocInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackaging.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(824, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcRecvWhs);
            this.panel2.Size = new System.Drawing.Size(824, 188);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueBin);
            this.panel3.Controls.Add(this.LueLot);
            this.panel3.Location = new System.Drawing.Point(0, 188);
            this.panel3.Size = new System.Drawing.Size(824, 285);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.LueLot, 0);
            this.panel3.Controls.SetChildIndex(this.LueBin, 0);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(824, 285);
            this.Grd1.TabIndex = 47;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // LueBin
            // 
            this.LueBin.EnterMoveNextControl = true;
            this.LueBin.Location = new System.Drawing.Point(411, 21);
            this.LueBin.Margin = new System.Windows.Forms.Padding(5);
            this.LueBin.Name = "LueBin";
            this.LueBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.Appearance.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBin.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBin.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBin.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBin.Properties.DropDownRows = 30;
            this.LueBin.Properties.NullText = "[Empty]";
            this.LueBin.Properties.PopupWidth = 200;
            this.LueBin.Size = new System.Drawing.Size(150, 20);
            this.LueBin.TabIndex = 49;
            this.LueBin.ToolTip = "F4 : Show/hide list";
            this.LueBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBin.EditValueChanged += new System.EventHandler(this.LueBin_EditValueChanged);
            this.LueBin.Leave += new System.EventHandler(this.LueBin_Leave);
            this.LueBin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBin_KeyDown);
            // 
            // LueLot
            // 
            this.LueLot.EnterMoveNextControl = true;
            this.LueLot.Location = new System.Drawing.Point(216, 21);
            this.LueLot.Margin = new System.Windows.Forms.Padding(5);
            this.LueLot.Name = "LueLot";
            this.LueLot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.Appearance.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLot.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLot.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLot.Properties.DropDownRows = 30;
            this.LueLot.Properties.NullText = "[Empty]";
            this.LueLot.Properties.PopupWidth = 200;
            this.LueLot.Size = new System.Drawing.Size(150, 20);
            this.LueLot.TabIndex = 48;
            this.LueLot.ToolTip = "F4 : Show/hide list";
            this.LueLot.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLot.EditValueChanged += new System.EventHandler(this.LueLot_EditValueChanged);
            this.LueLot.Leave += new System.EventHandler(this.LueLot_Leave);
            this.LueLot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLot_KeyDown);
            // 
            // TcRecvWhs
            // 
            this.TcRecvWhs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcRecvWhs.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcRecvWhs.Location = new System.Drawing.Point(0, 0);
            this.TcRecvWhs.Name = "TcRecvWhs";
            this.TcRecvWhs.SelectedTabPage = this.Tp1;
            this.TcRecvWhs.Size = new System.Drawing.Size(824, 188);
            this.TcRecvWhs.TabIndex = 12;
            this.TcRecvWhs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(818, 160);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.TxtJournalDocNo2);
            this.panel5.Controls.Add(this.TxtJournalDocNo);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.LueWhsCode2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.LueWhsCode);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(818, 160);
            this.panel5.TabIndex = 22;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LueProductionWorkGroup);
            this.panel4.Controls.Add(this.LblGroup);
            this.panel4.Location = new System.Drawing.Point(458, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(307, 47);
            this.panel4.TabIndex = 36;
            // 
            // LueProductionWorkGroup
            // 
            this.LueProductionWorkGroup.EnterMoveNextControl = true;
            this.LueProductionWorkGroup.Location = new System.Drawing.Point(47, 6);
            this.LueProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionWorkGroup.Name = "LueProductionWorkGroup";
            this.LueProductionWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionWorkGroup.Properties.DropDownRows = 30;
            this.LueProductionWorkGroup.Properties.NullText = "[Empty]";
            this.LueProductionWorkGroup.Properties.PopupWidth = 300;
            this.LueProductionWorkGroup.Size = new System.Drawing.Size(225, 20);
            this.LueProductionWorkGroup.TabIndex = 38;
            this.LueProductionWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueProductionWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionWorkGroup.EditValueChanged += new System.EventHandler(this.LueProductionWorkGroup_EditValueChanged);
            // 
            // LblGroup
            // 
            this.LblGroup.AutoSize = true;
            this.LblGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGroup.ForeColor = System.Drawing.Color.Red;
            this.LblGroup.Location = new System.Drawing.Point(5, 9);
            this.LblGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblGroup.Name = "LblGroup";
            this.LblGroup.Size = new System.Drawing.Size(40, 14);
            this.LblGroup.TabIndex = 37;
            this.LblGroup.Text = "Group";
            this.LblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtJournalDocNo2
            // 
            this.TxtJournalDocNo2.EnterMoveNextControl = true;
            this.TxtJournalDocNo2.Location = new System.Drawing.Point(88, 130);
            this.TxtJournalDocNo2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo2.Name = "TxtJournalDocNo2";
            this.TxtJournalDocNo2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo2.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo2.Properties.MaxLength = 30;
            this.TxtJournalDocNo2.Properties.ReadOnly = true;
            this.TxtJournalDocNo2.Size = new System.Drawing.Size(186, 20);
            this.TxtJournalDocNo2.TabIndex = 35;
            // 
            // TxtJournalDocNo
            // 
            this.TxtJournalDocNo.EnterMoveNextControl = true;
            this.TxtJournalDocNo.Location = new System.Drawing.Point(88, 109);
            this.TxtJournalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtJournalDocNo.Name = "TxtJournalDocNo";
            this.TxtJournalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtJournalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtJournalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtJournalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtJournalDocNo.Properties.MaxLength = 30;
            this.TxtJournalDocNo.Properties.ReadOnly = true;
            this.TxtJournalDocNo.Size = new System.Drawing.Size(186, 20);
            this.TxtJournalDocNo.TabIndex = 34;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(29, 112);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 14);
            this.label22.TabIndex = 33;
            this.label22.Text = "Journal#";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(61, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 14);
            this.label3.TabIndex = 29;
            this.label3.Text = "To";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode2
            // 
            this.LueWhsCode2.EnterMoveNextControl = true;
            this.LueWhsCode2.Location = new System.Drawing.Point(88, 46);
            this.LueWhsCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode2.Name = "LueWhsCode2";
            this.LueWhsCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode2.Properties.DropDownRows = 30;
            this.LueWhsCode2.Properties.NullText = "[Empty]";
            this.LueWhsCode2.Properties.PopupWidth = 300;
            this.LueWhsCode2.Size = new System.Drawing.Size(298, 20);
            this.LueWhsCode2.TabIndex = 28;
            this.LueWhsCode2.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode2.EditValueChanged += new System.EventHandler(this.LueWhsCode2_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(49, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 14);
            this.label1.TabIndex = 27;
            this.label1.Text = "From";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(88, 67);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 300;
            this.LueWhsCode.Size = new System.Drawing.Size(298, 20);
            this.LueWhsCode.TabIndex = 30;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(88, 88);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(450, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(448, 20);
            this.MeeRemark.TabIndex = 32;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 91);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 14);
            this.label2.TabIndex = 31;
            this.label2.Text = "Remark";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(88, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(139, 20);
            this.DteDocDt.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(50, 28);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 14);
            this.label4.TabIndex = 25;
            this.label4.Text = "Date";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(88, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(186, 20);
            this.TxtDocNo.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(10, 7);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 14);
            this.label11.TabIndex = 23;
            this.label11.Text = "Document#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.PageVisible = false;
            this.Tp2.Size = new System.Drawing.Size(818, 160);
            this.Tp2.Text = "Kawasan Berikat";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.panel8);
            this.panel6.Controls.Add(this.LueCustomsDocCode);
            this.panel6.Controls.Add(this.label16);
            this.panel6.Controls.Add(this.TxtKBSubmissionNo);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.TxtKBPackaging);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.BtnKBContractNo);
            this.panel6.Controls.Add(this.TxtKBRegistrationNo);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.TxtKBPLNo);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtKBContractNo);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(818, 160);
            this.panel6.TabIndex = 22;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label18);
            this.panel8.Controls.Add(this.DteKBDecreeDt);
            this.panel8.Controls.Add(this.TxtKBInsuranceAmt);
            this.panel8.Controls.Add(this.label17);
            this.panel8.Controls.Add(this.TxtKBDecreeNo);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.TxtKBPackagingQty);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.DteKBRegistrationDt);
            this.panel8.Controls.Add(this.label9);
            this.panel8.Controls.Add(this.DteKBPLDt);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.DteKBContractDt);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(372, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(330, 160);
            this.panel8.TabIndex = 45;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(36, 31);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 14);
            this.label18.TabIndex = 47;
            this.label18.Text = "Decree Date";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBDecreeDt
            // 
            this.DteKBDecreeDt.EditValue = null;
            this.DteKBDecreeDt.EnterMoveNextControl = true;
            this.DteKBDecreeDt.Location = new System.Drawing.Point(117, 28);
            this.DteKBDecreeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBDecreeDt.Name = "DteKBDecreeDt";
            this.DteKBDecreeDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBDecreeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBDecreeDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBDecreeDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBDecreeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBDecreeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBDecreeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBDecreeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBDecreeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBDecreeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBDecreeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBDecreeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBDecreeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBDecreeDt.Size = new System.Drawing.Size(209, 20);
            this.DteKBDecreeDt.TabIndex = 48;
            // 
            // TxtKBInsuranceAmt
            // 
            this.TxtKBInsuranceAmt.EnterMoveNextControl = true;
            this.TxtKBInsuranceAmt.Location = new System.Drawing.Point(117, 49);
            this.TxtKBInsuranceAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBInsuranceAmt.Name = "TxtKBInsuranceAmt";
            this.TxtKBInsuranceAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBInsuranceAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBInsuranceAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBInsuranceAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtKBInsuranceAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtKBInsuranceAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtKBInsuranceAmt.Size = new System.Drawing.Size(209, 20);
            this.TxtKBInsuranceAmt.TabIndex = 50;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(4, 51);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 14);
            this.label17.TabIndex = 49;
            this.label17.Text = "Insurance Amount";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBDecreeNo
            // 
            this.TxtKBDecreeNo.EnterMoveNextControl = true;
            this.TxtKBDecreeNo.Location = new System.Drawing.Point(117, 7);
            this.TxtKBDecreeNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBDecreeNo.Name = "TxtKBDecreeNo";
            this.TxtKBDecreeNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBDecreeNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBDecreeNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBDecreeNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBDecreeNo.Properties.MaxLength = 30;
            this.TxtKBDecreeNo.Size = new System.Drawing.Size(209, 20);
            this.TxtKBDecreeNo.TabIndex = 46;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(57, 9);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 14);
            this.label12.TabIndex = 45;
            this.label12.Text = "Decree#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBPackagingQty
            // 
            this.TxtKBPackagingQty.EnterMoveNextControl = true;
            this.TxtKBPackagingQty.Location = new System.Drawing.Point(117, 134);
            this.TxtKBPackagingQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPackagingQty.Name = "TxtKBPackagingQty";
            this.TxtKBPackagingQty.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPackagingQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPackagingQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtKBPackagingQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtKBPackagingQty.Size = new System.Drawing.Size(209, 20);
            this.TxtKBPackagingQty.TabIndex = 58;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(0, 136);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 14);
            this.label15.TabIndex = 57;
            this.label15.Text = "Packaging Quantity";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(11, 116);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 14);
            this.label10.TabIndex = 55;
            this.label10.Text = "Registration Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBRegistrationDt
            // 
            this.DteKBRegistrationDt.EditValue = null;
            this.DteKBRegistrationDt.EnterMoveNextControl = true;
            this.DteKBRegistrationDt.Location = new System.Drawing.Point(117, 112);
            this.DteKBRegistrationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBRegistrationDt.Name = "DteKBRegistrationDt";
            this.DteKBRegistrationDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBRegistrationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBRegistrationDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBRegistrationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBRegistrationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBRegistrationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBRegistrationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBRegistrationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBRegistrationDt.Size = new System.Drawing.Size(209, 20);
            this.DteKBRegistrationDt.TabIndex = 56;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(12, 95);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 14);
            this.label9.TabIndex = 53;
            this.label9.Text = "Packing List Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBPLDt
            // 
            this.DteKBPLDt.EditValue = null;
            this.DteKBPLDt.EnterMoveNextControl = true;
            this.DteKBPLDt.Location = new System.Drawing.Point(117, 91);
            this.DteKBPLDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBPLDt.Name = "DteKBPLDt";
            this.DteKBPLDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBPLDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBPLDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBPLDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBPLDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBPLDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBPLDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBPLDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBPLDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBPLDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBPLDt.Size = new System.Drawing.Size(209, 20);
            this.DteKBPLDt.TabIndex = 54;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(28, 73);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 14);
            this.label8.TabIndex = 51;
            this.label8.Text = "Contract Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteKBContractDt
            // 
            this.DteKBContractDt.EditValue = null;
            this.DteKBContractDt.EnterMoveNextControl = true;
            this.DteKBContractDt.Location = new System.Drawing.Point(117, 70);
            this.DteKBContractDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteKBContractDt.Name = "DteKBContractDt";
            this.DteKBContractDt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.DteKBContractDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteKBContractDt.Properties.Appearance.Options.UseFont = true;
            this.DteKBContractDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteKBContractDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteKBContractDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteKBContractDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteKBContractDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteKBContractDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteKBContractDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteKBContractDt.Size = new System.Drawing.Size(209, 20);
            this.DteKBContractDt.TabIndex = 52;
            // 
            // LueCustomsDocCode
            // 
            this.LueCustomsDocCode.EnterMoveNextControl = true;
            this.LueCustomsDocCode.Location = new System.Drawing.Point(160, 6);
            this.LueCustomsDocCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCustomsDocCode.Name = "LueCustomsDocCode";
            this.LueCustomsDocCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.Appearance.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCustomsDocCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCustomsDocCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCustomsDocCode.Properties.DropDownRows = 30;
            this.LueCustomsDocCode.Properties.NullText = "[Empty]";
            this.LueCustomsDocCode.Properties.PopupWidth = 400;
            this.LueCustomsDocCode.Size = new System.Drawing.Size(177, 20);
            this.LueCustomsDocCode.TabIndex = 13;
            this.LueCustomsDocCode.ToolTip = "F4 : Show/hide list";
            this.LueCustomsDocCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCustomsDocCode.EditValueChanged += new System.EventHandler(this.LueCustomsDocCode_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(7, 10);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(146, 14);
            this.label16.TabIndex = 12;
            this.label16.Text = "Customs Document Code";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBSubmissionNo
            // 
            this.TxtKBSubmissionNo.EnterMoveNextControl = true;
            this.TxtKBSubmissionNo.Location = new System.Drawing.Point(158, 109);
            this.TxtKBSubmissionNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBSubmissionNo.Name = "TxtKBSubmissionNo";
            this.TxtKBSubmissionNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBSubmissionNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBSubmissionNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBSubmissionNo.Properties.MaxLength = 30;
            this.TxtKBSubmissionNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBSubmissionNo.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(76, 113);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 14);
            this.label14.TabIndex = 27;
            this.label14.Text = "Submission#";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.ChkKBNonDocInd);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(702, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(116, 160);
            this.panel7.TabIndex = 45;
            // 
            // ChkKBNonDocInd
            // 
            this.ChkKBNonDocInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkKBNonDocInd.Location = new System.Drawing.Point(3, 130);
            this.ChkKBNonDocInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkKBNonDocInd.Name = "ChkKBNonDocInd";
            this.ChkKBNonDocInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkKBNonDocInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkKBNonDocInd.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ChkKBNonDocInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkKBNonDocInd.Properties.Appearance.Options.UseFont = true;
            this.ChkKBNonDocInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkKBNonDocInd.Properties.Caption = "Non Document";
            this.ChkKBNonDocInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkKBNonDocInd.Size = new System.Drawing.Size(109, 22);
            this.ChkKBNonDocInd.TabIndex = 46;
            // 
            // TxtKBPackaging
            // 
            this.TxtKBPackaging.EnterMoveNextControl = true;
            this.TxtKBPackaging.Location = new System.Drawing.Point(158, 130);
            this.TxtKBPackaging.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPackaging.Name = "TxtKBPackaging";
            this.TxtKBPackaging.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPackaging.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPackaging.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPackaging.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPackaging.Properties.MaxLength = 80;
            this.TxtKBPackaging.Size = new System.Drawing.Size(206, 20);
            this.TxtKBPackaging.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(90, 134);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 14);
            this.label13.TabIndex = 29;
            this.label13.Text = "Packaging";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnKBContractNo
            // 
            this.BtnKBContractNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnKBContractNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnKBContractNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnKBContractNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnKBContractNo.Appearance.Options.UseBackColor = true;
            this.BtnKBContractNo.Appearance.Options.UseFont = true;
            this.BtnKBContractNo.Appearance.Options.UseForeColor = true;
            this.BtnKBContractNo.Appearance.Options.UseTextOptions = true;
            this.BtnKBContractNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnKBContractNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnKBContractNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnKBContractNo.Image")));
            this.BtnKBContractNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnKBContractNo.Location = new System.Drawing.Point(342, 6);
            this.BtnKBContractNo.Name = "BtnKBContractNo";
            this.BtnKBContractNo.Size = new System.Drawing.Size(24, 21);
            this.BtnKBContractNo.TabIndex = 14;
            this.BtnKBContractNo.ToolTip = "Show Kawasan Berikat\'s Document";
            this.BtnKBContractNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnKBContractNo.ToolTipTitle = "Run System";
            this.BtnKBContractNo.Click += new System.EventHandler(this.BtnKBContractNo_Click);
            // 
            // TxtKBRegistrationNo
            // 
            this.TxtKBRegistrationNo.EnterMoveNextControl = true;
            this.TxtKBRegistrationNo.Location = new System.Drawing.Point(158, 88);
            this.TxtKBRegistrationNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBRegistrationNo.Name = "TxtKBRegistrationNo";
            this.TxtKBRegistrationNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBRegistrationNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBRegistrationNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBRegistrationNo.Properties.MaxLength = 30;
            this.TxtKBRegistrationNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBRegistrationNo.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(71, 92);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Registration#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBPLNo
            // 
            this.TxtKBPLNo.EnterMoveNextControl = true;
            this.TxtKBPLNo.Location = new System.Drawing.Point(158, 67);
            this.TxtKBPLNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBPLNo.Name = "TxtKBPLNo";
            this.TxtKBPLNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBPLNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBPLNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBPLNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBPLNo.Properties.MaxLength = 30;
            this.TxtKBPLNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBPLNo.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(72, 71);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Packing List#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtKBContractNo
            // 
            this.TxtKBContractNo.EnterMoveNextControl = true;
            this.TxtKBContractNo.Location = new System.Drawing.Point(158, 46);
            this.TxtKBContractNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtKBContractNo.Name = "TxtKBContractNo";
            this.TxtKBContractNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtKBContractNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtKBContractNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtKBContractNo.Properties.Appearance.Options.UseFont = true;
            this.TxtKBContractNo.Properties.MaxLength = 30;
            this.TxtKBContractNo.Size = new System.Drawing.Size(206, 20);
            this.TxtKBContractNo.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(88, 49);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Contract#";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRecvWhs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 473);
            this.Name = "FrmRecvWhs";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcRecvWhs)).EndInit();
            this.TcRecvWhs.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtJournalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBDecreeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBDecreeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBInsuranceAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBDecreeNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackagingQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBRegistrationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBPLDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteKBContractDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCustomsDocCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBSubmissionNo.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkKBNonDocInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPackaging.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBRegistrationNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBPLNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtKBContractNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueBin;
        private DevExpress.XtraEditors.LookUpEdit LueLot;
        private DevExpress.XtraTab.XtraTabControl TcRecvWhs;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtJournalDocNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode2;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.LookUpEdit LueCustomsDocCode;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtKBSubmissionNo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.CheckEdit ChkKBNonDocInd;
        internal DevExpress.XtraEditors.TextEdit TxtKBPackaging;
        private System.Windows.Forms.Label label13;
        public DevExpress.XtraEditors.SimpleButton BtnKBContractNo;
        internal DevExpress.XtraEditors.TextEdit TxtKBRegistrationNo;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtKBPLNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtKBContractNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.LookUpEdit LueProductionWorkGroup;
        private System.Windows.Forms.Label LblGroup;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.DateEdit DteKBDecreeDt;
        internal DevExpress.XtraEditors.TextEdit TxtKBInsuranceAmt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtKBDecreeNo;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtKBPackagingQty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.DateEdit DteKBRegistrationDt;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.DateEdit DteKBPLDt;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteKBContractDt;
    }
}