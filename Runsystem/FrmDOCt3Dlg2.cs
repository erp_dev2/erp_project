﻿#region Update
/* 
    27/10/2019 [TKG/IOK] merubah proses menggunakan replaced item (DOCt3DocNo tidak digunakan lagi)
    30/01/2020 [TKG/IMS] menambah local code
    22/03/2020 [TKG/IMS] menambah project code, project name, PO# berdasarkan parameter IsDOCt3ShowProjectInfo
    16/06/2020 [DITA/IMS] menampilkan SO Contract Remark
 *  09/07/2020 [HAR/IMS] SO Contract Remark ambil dari detail
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt3Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOCt3 mFrmParent;
        string mSQL = string.Empty, mRecvCtDocNo = string.Empty;

       
        #endregion

        #region Constructor

        public FrmDOCt3Dlg2(FrmDOCt3 FrmParent, string RecvCtDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRecvCtDocNo = RecvCtDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, B.ItCodeInternal, ");
            SQL.AppendLine("A.RecvCtQty-A.DOCt3Qty As Qty, B.InventoryUomCode, ");
            SQL.AppendLine("A.RecvCtQty2-A.DOCt3Qty2 As Qty2, B.InventoryUomCode2, ");
            SQL.AppendLine("A.RecvCtQty3-A.DOCt3Qty3 As Qty3, B.InventoryUomCode3,  ");
            if (mFrmParent.mIsSalesTransactionShowSOContractRemark)
                SQL.AppendLine("C.SOCRemark, ");
            else
                SQL.AppendLine("Null As SOCRemark, ");
            if (mFrmParent.mIsDOCt3ShowProjectInfo)
                SQL.AppendLine("C.ProjectCode, C.ProjectName, C.PONo ");
            else
                SQL.AppendLine("Null ProjectCode, Null As ProjectName, Null As PONo ");
            SQL.AppendLine("From TblRecvCtDtl2 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            if (mFrmParent.mIsDOCt3ShowProjectInfo && mFrmParent.mIsSalesTransactionShowSOContractRemark)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.DocNo, B.ItCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(D.ProjectCode, '-')) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(D.ProjectName, '-')) ProjectName, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(D.PONo, '-')) PONo, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(D.Remark, '-')) SOCRemark ");
                SQL.AppendLine("    From TblRecvCtDtl A "); ;
                SQL.AppendLine("    Left Join TblDOCt2Dtl B On A.DOCtDocNo=B.DocNo And A.DOCtDNo=B.DNo ");
                SQL.AppendLine("    Left Join TblDOCt2Hdr C On A.DOCtDocNo=C.DocNo ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X1.DocNo, X5.ProjectCode, X5.ProjectName, X2.PONo, group_concat(distinct X.Remark separator '#' ) As remark ");
                SQL.AppendLine("        From TblDRDtl X1 ");
                SQL.AppendLine("        Inner Join TblSOContractHdr X2 On X1.SODocNo = X2.DocNo ");
                SQL.AppendLine("        INNER JOIN tblsocontractDtl X ON X1.SODocNo = X.DocNo And X1.SODNo = X.DNo ");
                SQL.AppendLine("        Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
                SQL.AppendLine("        Inner Join TblProjectGroup X5 On X4.PGCode = X5.PGCode ");
                SQL.AppendLine("        Where X1.DocNo In ( ");
                SQL.AppendLine("            Select T3.DRDocNo ");
                SQL.AppendLine("            From TblRecvCtHdr T1 ");
                SQL.AppendLine("            Inner Join TblRecvCtDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("            Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.DRDocNo Is Not Null ");
                SQL.AppendLine("            Where T1.DocNo=@DocNo ");    
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) D On C.DRDocNo=D.DocNo ");
                SQL.AppendLine("    Group By A.DocNo, B.ItCode ");
                SQL.AppendLine(") C ");
                SQL.AppendLine("    On A.DocNo=C.DocNo ");
                SQL.AppendLine("    And A.ItCode=C.ItCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo And A.RecvCtQty<>0.00 And A.RecvCtQty<>A.DOCt3Qty ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "", 
                    "Item's Code",
                    "Item's Name",
                    "Outstanding",
                    "UoM",

                    //6-10
                    "Outstanding",
                    "UoM",
                    "Outstanding",
                    "UoM",
                    "Local Code",

                    //11-14
                    "Project Code",
                    "Project Name",
                    "Customer's PO#",
                    "SO Contract" + Environment.NewLine + "Remark"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 120, 250, 100, 80, 

                    //6-10
                    100, 80, 100, 80, 120,

                    //11-14
                    130, 200, 130, 200
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 6, 8 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 8, 9, 11, 12, 13, 14  }, false);
            Grd1.Cols[10].Move(4);
            if (mFrmParent.mIsDOCt3ShowProjectInfo) Sm.GrdColInvisible(Grd1, new int[] { 11, 12, 13 }, true);
            if (mFrmParent.mIsSalesTransactionShowSOContractRemark) Sm.GrdColInvisible(Grd1, new int[] {14}, true);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                
                var cm = new MySqlCommand();

                if (mFrmParent.Grd2.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(mFrmParent.Grd2, r, 1).Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += "(A.ItCode<>@ItCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd2, r, 1));
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And ( " + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@DocNo", mRecvCtDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "B.ItName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By B.ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode", 
                            
                            //1-5
                            "ItName", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //6-10
                            "Qty3", "InventoryUomCode3", "ItCodeInternal", "ProjectCode", "ProjectName", 
                            
                            //11-12
                            "PONo", "SOCRemark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 11, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 12, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 14, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 15, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 16, Grd1, Row2, 14);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 4, 7, 10 });
                        
                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 3, 4, 6, 7, 9, 10 });
                    }
                }
            }
            if (!IsChoose) 
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            else
                Sm.ClearGrd(mFrmParent.Grd1, true);
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            string Key = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd2.Rows.Count; Index++)
                if (Sm.CompareStr(Key, Sm.GetGrdStr(mFrmParent.Grd2, Index, 1))) return true;
            return false;
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 6, 7 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9 }, true);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
                Grd1.Font = new Font(Grd1.Font.FontFamily.Name.ToString(), int.Parse(Sm.GetLue(LueFontSize)));
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
