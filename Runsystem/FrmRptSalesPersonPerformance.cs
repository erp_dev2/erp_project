﻿#region update
// 27/09/2017 [har] retur pos nilainya minus
#endregion


#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSalesPersonPerformance : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty,
            mAccessInd = string.Empty, 
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSalesPersonPerformance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                
                Sl.SetLueSPCode(ref LueSalesPerson);
                SetGrd();
                SetSQL();

                iGCellStyle myPercentBarStyle = new iGCellStyle();
                myPercentBarStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
                myPercentBarStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)((TenTec.Windows.iGridLib.iGCellFlags.DisplayText | TenTec.Windows.iGridLib.iGCellFlags.DisplayImage)));
                Grd1.Cols[8].CellStyle = myPercentBarStyle;

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Y.SpCode, Y.SpName, Y.Yr, Y.Mth, Y.Amt, Y.AmtMt, Y.AmtMt  ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select X.SpCode, X.Salesname As SpName, X.Yr, X.Mth, SUM(X.Amt) As Amt, X2.Amt As AmtMt ");
            SQL.AppendLine("    From ( ");
	        SQL.AppendLine("        Select C.SpCode, A.DocNo, A.DocDt, A.Salesname, Left(A.DocDt, 4) As Yr,  ");
	        SQL.AppendLine("        SubString(A.DOcDt, 5, 2) As mth, Right(A.DocDt, 2) As Dt, (A.Amt-ifnull(B.Freight, 0)) As Amt ");
	        SQL.AppendLine("        From TblsalesInvoicehdr A ");
	        SQL.AppendLine("        Left Join ( ");
		    SQL.AppendLine("            Select DocNo, SUM(CAmt) As Freight  ");
		    SQL.AppendLine("            from TblSalesInvoiceDtl2 ");
		    SQL.AppendLine("            Where AcInd = 'N' And OptAcDesc = '1'   ");
		    SQL.AppendLine("            Group By DocNo ");
	        SQL.AppendLine("        )B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("        left Join Tblsalesperson C On A.SalesName = C.SPname ");
	        SQL.AppendLine("        Where A.cancelInd = 'N'  And  Left(A.DocDt, 4) = @Yr And SubString(A.DOcDt, 5, 2) = @Mth ");
            
            SQL.AppendLine("        union all ");
            SQL.AppendLine("        Select X1.SpCode, X1.TrnNo, X1.BSDate, X2.Spname,  X1.Yr,  ");
		    SQL.AppendLine("        X1.mth, Right(X1.BSDate, 2) As Dt, X1.PayAmtNett   ");
		    SQL.AppendLine("        From (    ");
            SQL.AppendLine("            Select A.TrnNo, A.BSDate, C.SpCode, Left(A.BSDate, 4) Yr, Substring(A.BSDate, 5, 2) Mth,  ");
            SQL.AppendLine("	        if(A.SlsType='S', B.PayAmtNett, (-1*B.PayAmtNett)) As PayAmtNett, if(A.SlsType='S', B.PayAmtNett, (-1*B.PayAmtNett)) As PayAmtNett2  ");
		    SQL.AppendLine("            From TblPostrnhdr A    ");
		    SQL.AppendLine("            Inner Join TblPosTrnPay B On A.TrnNo=B.TrnNo And A.BsDate = B.BSDate And A.PosNo = B.PosNo  ");  
		    SQL.AppendLine("            Inner Join Tblsalesperson C On A.UserCode = C.UserCode  ");  
            SQL.AppendLine("            Left Join   ");
            SQL.AppendLine("            (  ");
	        SQL.AppendLine("                select DocDt, Group_concat(VoucherDocNo) VoucherDocNo    ");
	        SQL.AppendLine("                From TblVoucherRequestHdr D   ");
	        SQL.AppendLine("                Where  DocType = '10' And D.CancelInd='N'  ");
	        SQL.AppendLine("                Group By DocDt  ");
            SQL.AppendLine("            )D On A.BsDate = D.DocDt   ");
		    SQL.AppendLine("        )X1    ");
            SQL.AppendLine("        Inner Join Tblsalesperson X2 On X1.SPCode = X2.SpCode ");
            SQL.AppendLine("    )X  ");
            SQL.AppendLine("    Left Join tblsalestarget X2 On X.SpCode =X2.SpCode And Concat(X2.Yr, X2.Mth) = Left(X.DocDt, 6)");
            SQL.AppendLine("    Where X.Yr = @Yr And X.Mth =@Mth  ");
            SQL.AppendLine("    Group BY X.SpCode, X.Salesname, X.Yr, X.Mth ");

            SQL.AppendLine(")Y ");
           

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Sales "+Environment.NewLine+"Code",
                        "Sales Person",
                        "Year",
                        "Month",
                        "Detail",
                        //6-8
                        "Amount",
                        "Monthly Target Amount",
                        "Prosentase Amount"+Environment.NewLine+"with Monthly Target",
                       },
                    new int[] 
                    {
                        //0
                        30,
                        //1-5
                        120, 150, 80, 80, 40,    
                        //6-10
                        150, 150, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] {  6, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }      

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Month = Sm.GetLue(LueMth);
           
            if (Year == string.Empty && Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month And Year is Empty.");
            }
            else if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is Empty.");
            }
            else if (Month == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = string.Empty;

                    var cm = new MySqlCommand();

                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSalesPerson), "Y.SPCode", true);
                    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                    
                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Group By Y.SpName, Y.Yr, Y.Mth ",
                            new string[]
                        {
                            //0
                            "SPCode", 
                            //1-5
                            "SpName", "Yr", "Mth", "Amt", "AmtMt"
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);
                                if (Sm.GetGrdDec(Grd1, Row, 7) != 0)
                                {
                                    Grd1.Cells[Row, 8].Value = Convert.ToDouble(Sm.GetGrdDec(Grd1, Row, 6) / Sm.GetGrdDec(Grd1, Row, 7));
                                }
                                else
                                {
                                     Grd1.Cells[Row, 8].Value = Convert.ToDouble(0);
                                }

                            }, true, false, false, false
                        );
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void ChkSalesPerson_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Sales Person");
        }

        private void LueSalesPerson_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSalesPerson, new Sm.RefreshLue1(Sl.SetLueSPCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }


        #endregion

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptSalesPersonPerformanceDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetLue(LueYr), Sm.GetLue(LueMth));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtSalesPerson.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.TxtYear.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtMth.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

          
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptSalesPersonPerformanceDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 1), Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetLue(LueYr), Sm.GetLue(LueMth));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtSalesPerson.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.TxtYear.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.TxtMth.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }

        }
       
        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex == 8  )
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;
                if (myObjValue == null)
                    return;

                Rectangle myBounds = e.Bounds;
                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;
                if (myBounds.Width > 0)
                {
                    e.Graphics.FillRectangle(Brushes.Bisque, myBounds);
                    double myValue = (double)myObjValue;
                    int myWidth = (int)(myBounds.Width * myValue);
                    e.Graphics.FillRectangle(Brushes.SandyBrown, myBounds.X, myBounds.Y, myWidth, myBounds.Height);

                    e.Graphics.DrawRectangle(Pens.SaddleBrown, myBounds);

                    StringFormat myStringFormat = new StringFormat();
                    myStringFormat.Alignment = StringAlignment.Center;
                    myStringFormat.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(string.Format("{0:F2}%", myValue * 100), Font, SystemBrushes.ControlText, new RectangleF(myBounds.X, myBounds.Y, myBounds.Width, myBounds.Height), myStringFormat);
                }
            }

        }

        

        #endregion
    }
}
