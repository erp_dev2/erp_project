﻿#region Update
/*
    21/07/2019 [TKG] New application
    12/08/2019 [TKG] tambah location
    05/09/2019 [TKG] tambah scan location barcode, tambah warehouse
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMTransferBin : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocTitle = string.Empty,
            mDocAbbr = string.Empty,
            mBarcode = string.Empty,
            mLocCode = string.Empty,
            mDocNo = string.Empty, mDocSeqNo = string.Empty; //if this application is called from other application;
        internal FrmIMMTransferBinFind FrmFind;
        private bool mIsNew = false;
        
        #endregion

        #region Constructor

        public FrmIMMTransferBin(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mDocSeqNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Requested#",
                        "DocNo",
                        "SeqNo",
                        "Source",
                        "Product's Code",
                        
                        //6-10
                        "Product's Description",
                        "Color",
                        "Quantity",
                        "Warehouse Code",
                        "From" + Environment.NewLine + "(Warehouse)",

                        //11-15
                        "Location Code",
                        "From" + Environment.NewLine + "(Location)",
                        "From" + Environment.NewLine + "(Bin)",
                        "Warehouse Code",
                        "To" + Environment.NewLine + "(Warehouse)",

                        //16-19
                        "Location Code",
                        "To" + Environment.NewLine + "(Location)",
                        "To" + Environment.NewLine + "(Bin)",
                        "Seller"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        130, 0, 0, 150, 120, 

                        //6-10
                        200, 100, 100, 0, 200, 

                        //11-15
                        0, 200, 100, 0, 200, 
                        
                        //16-19
                        0, 200, 100, 200
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 9, 11, 14, 16 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, TxtLocBarcode, LueBin, TxtBarcode }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, TxtLocBarcode, LueBin, TxtBarcode 
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mIsNew = false;
            mLocCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, LueWhsCode, TxtLocCode, LueBin, TxtLocBarcode, TxtBarcode });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMTransferBinFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueBin(ref LueBin, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ") && !Sm.IsLueEmpty(LueWhsCode, "Warehouse")) Sm.FormShowDialog(new FrmIMMTransferBinDlg(this, Sm.GetLue(LueWhsCode), mLocCode));
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmIMMTransferBinDlg(this, Sm.GetLue(LueWhsCode), mLocCode));
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;
            
            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            decimal DocSeqNo = 0m;

            GenerateDocNo(ref DocNo, ref DocSeqNo);

            var cml = new List<MySqlCommand>();

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveIMMTransferBinDtl(DocNo, DocSeqNo, r));

            cml.Add(SaveIMMTransferBinHdr(DocNo, DocSeqNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private void GenerateDocNo(ref string DocNo, ref decimal DocSeqNo)
        {
            string DocDt = Sm.GetDte(DteDocDt);
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);

            DocNo = string.Concat(mDocTitle, "/", mDocAbbr, "/", Yr, "/", Mth, "/");

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("    Select DocSeqNo From TblIMMTransferBinHdr ");
            SQL.Append("    Where DocNo=@Param Order By DocSeqNo Desc Limit 1 ");
            SQL.Append("   ), 0.00) As DocSeqNo");

            DocSeqNo = Sm.GetValueDec(SQL.ToString(), DocNo);
            DocSeqNo += 1;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 product.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Requested# is empty.") ||
                    IsRequestProcessed(r) ||
                    IsRequestCancelled(r) ||
                    IsStockInvalid(r))
                    return true;
            return false;
        }

        private bool IsRequestProcessed(int r)
        {   
            if (Sm.IsDataExist("Select 1 From TblIMMTransferBinReqDtl Where DocNo=@Param1 And DocSeqNo=@Param2 And CancelInd='N' And TransferBinDocNo Is Not Null;",
                Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 3), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Product's Code : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Product's Description : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Color : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                    "To : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                    "To : " + Sm.GetLue(LueBin) + Environment.NewLine +
                    "To : " + Sm.GetGrdStr(Grd1, r, 17) + Environment.NewLine +
                    "Seller : " + Sm.GetGrdStr(Grd1, r, 19) + Environment.NewLine + Environment.NewLine +
                    "This product've already processed."
                    );
                return true;
            }
            return false;
        }

        private bool IsRequestCancelled(int r)
        {
            if (Sm.IsDataExist("Select 1 From TblIMMTransferBinReqDtl Where DocNo=@Param1 And DocSeqNo=@Param2 And CancelInd='Y';",
                Sm.GetGrdStr(Grd1, r, 2), Sm.GetGrdStr(Grd1, r, 3), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Product's Code : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Product's Description : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Color : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                    "To : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                    "To : " + Sm.GetLue(LueBin) + Environment.NewLine +
                    "To : " + Sm.GetGrdStr(Grd1, r, 17) + Environment.NewLine +
                    "Seller : " + Sm.GetGrdStr(Grd1, r, 19) + Environment.NewLine + Environment.NewLine +
                    "This product've already cancelled."
                    );
                return true;
            }
            return false;
        }

        private bool IsStockInvalid(int r)
        {
            string
                Source = Sm.GetGrdStr(Grd1, r, 4),
                WhsCode = Sm.GetGrdStr(Grd1, r, 9),
                Bin = Sm.GetGrdStr(Grd1, r, 13)
                ;

            if (Sm.IsDataExist("Select 1 From TblIMMStockSummary Where WhsCode=@Param1 And Bin=@Param2 And Source=@Param3 And Qty<=0.00;",
                WhsCode, Bin, Source))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Requested# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Product's Code : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Product's Description : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Color : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine +
                    "From : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                    "To : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                    "To : " + Sm.GetLue(LueBin) + Environment.NewLine +
                    "To : " + Sm.GetGrdStr(Grd1, r, 17) + Environment.NewLine +
                    "Seller : " + Sm.GetGrdStr(Grd1, r, 19) + Environment.NewLine + Environment.NewLine +
                    "Stock is empty."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveIMMTransferBinHdr(string DocNo, decimal DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMTransferBinHdr ");
            SQL.AppendLine("(DocNo, DocSeqNo, DocDt, WhsCode, LocCode, Bin, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @DocDt, @WhsCode, @LocCode, @Bin, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblIMMTransferBinReqDtl A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinDtl B ");
            SQL.AppendLine("    On A.DocNo=B.TransferBinReqDocNo ");
            SQL.AppendLine("    And A.DocSeqNo=B.TransferBinReqDocSeqNo ");
            SQL.AppendLine("    And A.Source=B.Source ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Set A.TransferBinDocNo=@DocNo, A.TransferBinDocSeqNo=@DocSeqNo;");

            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, LocCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '03', @DocNo, @DocSeqNo, B.Source, A.DocDt, B.WhsCodeFrom, B.LocCodeFrom, B.BinFrom, C.ProdCode, -1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMTransferBinHdr A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocSeqNo=@DocSeqNo; ");

            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, LocCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select '04', @DocNo, @DocSeqNo, B.Source, A.DocDt, B.WhsCodeTo, B.LocCodeTo, B.BinTo, C.ProdCode, 1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMTransferBinHdr A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocSeqNo=@DocSeqNo; ");

            SQL.AppendLine("Update TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinDtl B ");
            SQL.AppendLine("    On A.Source=B.Source ");
            SQL.AppendLine("    And A.WhsCode=B.WhsCodeFrom ");
            SQL.AppendLine("    And A.Bin=B.BinFrom ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Set A.Qty=0.00; ");

            SQL.AppendLine("Insert Into TblIMMStockSummary ");
            SQL.AppendLine("(WhsCode, LocCode, Bin, Source, ProdCode, SellerCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCodeTo, Null, A.BinTo, A.Source, B.ProdCode, B.SellerCode, 1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMTransferBinDtl A, TblIMMStockPrice B ");
            SQL.AppendLine("Where A.Source=B.Source ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("And Not Exists(Select 1 From TblIMMStockSummary Where WhsCode=A.WhsCodeTo And Bin=A.BinTo And Source=A.Source);");

            SQL.AppendLine("Update TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinDtl B ");
            SQL.AppendLine("    On A.Source=B.Source ");
            SQL.AppendLine("    And A.WhsCode=B.WhsCodeTo ");
            SQL.AppendLine("    And A.Bin=B.BinTo ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Set A.Qty=1.00, A.LocCode=Null; ");

            SQL.AppendLine("Update TblIMMStockPrice A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinDtl B ");
            SQL.AppendLine("    On A.Source=B.Source ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Inner Join TblBin C On C.Bin=@Bin ");
            SQL.AppendLine("Set A.Status=Case When C.QuarantineInd='Y' Then '3' Else '2' End; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@LocCode", mLocCode);
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetLue(LueBin));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveIMMTransferBinDtl(string DocNo, decimal DocSeqNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMTransferBinDtl(DocNo, DocSeqNo, Source, TransferBinReqDocNo, TransferBinReqDocSeqNo, WhsCodeFrom, LocCodeFrom, BinFrom, WhsCodeTo, LocCodeTo, BinTo, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @Source, @TransferBinReqDocNo, @TransferBinReqDocSeqNo, @WhsCodeFrom, @LocCodeFrom, @BinFrom, @WhsCodeTo, @LocCodeTo, @BinTo, @Qty, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 4));
            Sm.CmParam<String>(ref cm, "@TransferBinReqDocNo", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@TransferBinReqDocSeqNo", Sm.GetGrdStr(Grd1, r, 3));
            Sm.CmParam<String>(ref cm, "@WhsCodeFrom", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<String>(ref cm, "@LocCodeFrom", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParam<String>(ref cm, "@BinFrom", Sm.GetGrdStr(Grd1, r, 13));
            Sm.CmParam<String>(ref cm, "@WhsCodeTo", Sm.GetGrdStr(Grd1, r, 14));
            Sm.CmParam<String>(ref cm, "@LocCodeTo", Sm.GetGrdStr(Grd1, r, 16));
            Sm.CmParam<String>(ref cm, "@BinTo", Sm.GetLue(LueBin));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string DocSeqNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                mDocNo = DocNo;
                mDocSeqNo = DocSeqNo;
                ShowIMMBinTransferHdr(DocNo, DocSeqNo);
                ShowIMMBinTransferDtl(DocNo, DocSeqNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIMMBinTransferHdr(string DocNo, string DocSeqNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocDt, A.WhsCode, A.LocCode, B.LocName, A.Bin " +
                    "From TblIMMTransferBinHdr A " +
                    "Left Join TblIMMLocation B On A.LocCode=B.LocCode " +
                    "Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo;",
                    new string[] { "DocDt", "WhsCode", "LocCode", "LocName", "Bin" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = string.Concat(DocNo, DocSeqNo);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[1]));
                        mLocCode = Sm.DrStr(dr, c[2]);
                        TxtLocCode.EditValue = Sm.DrStr(dr, c[3]);
                        Sl.SetLueBin(ref LueBin, Sm.DrStr(dr, c[4]));
                    }, true
                );
        }

        private void ShowIMMBinTransferDtl(string DocNo, string DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TransferBinReqDocNo, A.TransferBinReqDocSeqNo, B.Source, F.ProdCode, F.ProdDesc, F.Color, ");
            SQL.AppendLine("A.WhsCodeFrom, C.WhsName As WhsNameFrom, A.LocCodeFrom, H.LocName As LocNameFrom, A.BinFrom, ");
            SQL.AppendLine("A.WhsCodeTo, D.WhsName As WhsNameTo, A.LocCodeTo, I.LocName As LocNameTo, A.BinTo, ");
            SQL.AppendLine("A.Qty, G.SellerName ");
            SQL.AppendLine("From TblIMMTransferBinDtl A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinReqDtl B On A.TransferBinReqDocNo=B.DocNo And A.TransferBinReqDocSeqNo=B.DocSeqNo And A.Source=B.Source ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCodeFrom=C.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCodeTo=D.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMStockPrice E On B.Source=E.Source ");
            SQL.AppendLine("Inner Join TblIMMProduct F On E.ProdCode=F.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller G On E.SellerCode=G.SellerCode ");
            SQL.AppendLine("Left Join TblIMMLocation H On A.LocCodeFrom=H.LocCode ");
            SQL.AppendLine("Left Join TblIMMLocation I On A.LocCodeTo=I.LocCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Order By F.ProdDesc, F.ProdCode, B.Source, C.WhsName, B.Bin;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "TransferBinReqDocNo", 

                    //1-5
                    "TransferBinReqDocSeqNo", "Source", "ProdCode", "ProdDesc", "Color", 

                    //6-10
                    "Qty", "WhsCodeFrom", "WhsNameFrom", "LocCodeFrom", "LocNameFrom", 
                    
                    //11-15
                    "BinFrom", "WhsCodeTo", "WhsNameTo", "LocCodeTo", "LocNameTo", 
                    
                    //16-17
                    "BinTo", "SellerName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Grd.Cells[Row, 1].Value = string.Concat(Sm.GetGrdStr(Grd, Row, 2), Sm.GetGrdStr(Grd, Row, 3));
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='IMMTransferBin';");
        }

        private void ShowSourceBarcodeInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DocNo, B.DocSeqNo, B.Source, F.ProdCode, F.ProdDesc, F.Color, ");
            SQL.AppendLine("B.WhsCode As WhsCodeFrom, C.WhsName As WhsNameFrom, A.LocCode As LocCodeFrom, H.LocName As LocNameFrom, B.Bin As BinFrom, ");
            SQL.AppendLine("A.WhsCode As WhsCodeTo, D.WhsName As WhsNameTo, Null As LocCodeTo, Null As LocNameTo, A.Bin As BinTo, ");
            SQL.AppendLine("B.Qty, G.SellerName ");
            SQL.AppendLine("From TblIMMTransferBinReqHdr A ");
            SQL.AppendLine("Inner Join TblIMMTransferBinReqDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo And B.CancelInd='N' ");
            SQL.AppendLine("    And B.Source=@Source ");
            SQL.AppendLine("    And B.TransferBinDocNo Is Null ");
            SQL.AppendLine("Inner Join TblWarehouse C On B.WhsCode=C.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMStockSummary E On B.Source=E.Source And B.WhsCode=E.WhsCode And B.Bin=E.Bin And E.Qty>0.00 ");
            SQL.AppendLine("Inner Join TblIMMProduct F On E.ProdCode=F.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller G On E.SellerCode=G.SellerCode ");
            SQL.AppendLine("Left Join TblIMMLocation H On A.LocCode=H.LocCode ");
            SQL.AppendLine("Where  A.WhsCode=@WhsCode ");
            if (mLocCode.Length>0)
                SQL.AppendLine("And A.LocCode=@LocCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("    Where WhsCode=A.WhsCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = string.Empty, Source = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 4);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (B.Source=@Source0" + r.ToString() + " ) ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not (" + Filter + ") Limit 1;";
                else
                    Filter = " Limit 1;";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Source", TxtBarcode.Text);
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                Sm.CmParam<String>(ref cm, "@LocCode", mLocCode);

                int Row = Grd1.Rows.Count - 1;
                Grd1.BeginUpdate();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString()+Filter;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocSeqNo", "Source", "ProdCode", "ProdDesc", "Color", 

                        //6-10
                        "Qty", "WhsCodeFrom", "WhsNameFrom", "LocCodeFrom", "LocNameFrom", 

                        //11-15
                        "BinFrom", "WhsCodeTo", "WhsNameTo", "LocCodeTo", "LocNameTo", 

                        //16-17
                        "BinTo", "SellerName"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                            Grd1.Cells[Row, 1].Value = string.Concat(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3));
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 17);
                            Row++;
                        }
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                        Console.Beep();
                    }
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Grd1.EndUpdate();
                TxtBarcode.EditValue = null;
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLocBarcodeInfo()
        {
            try
            {
                mLocCode = TxtLocBarcode.Text;
                var LocName = Sm.GetValue("Select LocName From TblIMMLocation Where LocCode=@Param Limit 1; ", mLocCode);
                if (LocName.Length > 0)
                {
                    TxtLocCode.EditValue = LocName;
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                    Console.Beep();
                    mLocCode = string.Empty;
                    TxtLocCode.EditValue = null;
                }
            }
            catch (Exception Exc)
            {
                mLocCode = string.Empty;
                TxtLocCode.EditValue = null;
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                TxtLocBarcode.EditValue = null;
                ClearGrd();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueBin_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBin, new Sm.RefreshLue2(Sl.SetLueBin), string.Empty);
        }

        private void TxtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back) e.SuppressKeyPress = true;
        }

        private void TxtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                mIsNew = true;
                mBarcode = string.Empty;
                ShowSourceBarcodeInfo();
                TxtBarcode.Focus();
            }
            else
            {
                if (mIsNew)
                {
                    mBarcode = string.Empty;
                    TxtBarcode.Text = string.Empty;
                }
                mBarcode = mBarcode + e.KeyChar;
                mIsNew = false;
                TxtBarcode.Focus();
            }
        }

        private void TxtLocBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back) e.SuppressKeyPress = true;
        }

        private void TxtLocBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                mIsNew = true;
                mBarcode = string.Empty;
                ShowLocBarcodeInfo();
                TxtBarcode.Focus();
            }
            else
            {
                if (mIsNew)
                {
                    mBarcode = string.Empty;
                    TxtLocBarcode.Text = string.Empty;
                }
                mBarcode = mBarcode + e.KeyChar;
                mIsNew = false;
                TxtLocBarcode.Focus();
            }
        }

        #endregion

        #endregion
    }
}
