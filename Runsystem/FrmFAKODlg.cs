﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFAKODlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmFAKO mFrmParent;
        string mSQL = string.Empty;
        byte mDocType = 1;

        #endregion

        #region Constructor

        public FrmFAKODlg(FrmFAKO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = "List Of DKO";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, C.CtName, A.Destination, ");
            SQL.AppendLine("E.EmpName As Publisher, D.PbsEmpCode As RegisterNo ");
            SQL.AppendLine("From TblDKO A ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("Inner Join TblPublisher D On A.PbsCode=D.PbsCode ");
            SQL.AppendLine("Inner Join TblEmployee E On D.PbsEmpCode=E.EmpCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo not in (Select DKOdocno from tblfako where cancelind = 'N' And DKOdocno is not null) ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
                Grd1.Cols.Count = 8;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document Number", 
                        "Document Date",
                        "Local Document",
                        "Customer",
                        "Destination",

                        //6
                        "Publisher",
                        "RegisterNo"

                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        140, 100, 100, 200, 150,   
                    
                        //6
                        120, 100
                    }
                );
                Sm.GrdFormatDate(Grd1, new int[] { 2, 3 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6,7 });
                Sm.GrdColInvisible(Grd1, new int[] {  }, false);
                Sm.SetGrdProperty(Grd1, false);
           
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
              
                Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                            new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "LocalDocNo", "CtName", "Destination", "Publisher", 
                          
                            //6
                            "RegisterNo"
                            
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            }, true, false, false, false
                        );
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtDkoNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtCustomer.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.TxtLocalDocDKO.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtPublishName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtRegistNo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);

                this.Close();
            }
            mFrmParent.ShowDataPLDR();

        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }
        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        #endregion

       

    }
}
